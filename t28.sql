﻿START TRANSACTION;






INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20230214060152_t37', '6.0.5');

COMMIT;

START TRANSACTION;

CREATE TABLE `LoginLog` (
    `ID` int NOT NULL AUTO_INCREMENT COMMENT '表索引',
    `OemId` int NOT NULL COMMENT 'OemId',
    `UserId` int NOT NULL COMMENT '用户id',
    `Username` longtext CHARACTER SET utf8mb4 NOT NULL COMMENT '用户名',
    `Type` int NOT NULL COMMENT '1登录,2正常退出,3超时登出,4被挤下线,5密码修改下线,6权限变更下线,7账户禁用下线',
    `LoginIp` longtext CHARACTER SET utf8mb4 NULL COMMENT '登录ip',
    `LastTime` datetime(6) NULL COMMENT '最后操作时间',
    `Remark` longtext CHARACTER SET utf8mb4 NULL COMMENT '备注',
    `Ext` longtext CHARACTER SET utf8mb4 NULL COMMENT '拓展字段',
    `CreatedAt` Timestamp NOT NULL COMMENT '创建时间',
    `UpdatedAt` Timestamp NOT NULL COMMENT '更新时间',
    CONSTRAINT `PK_LoginLog` PRIMARY KEY (`ID`)
) CHARACTER SET=utf8mb4;




INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20230214070423_t38', '6.0.5');

COMMIT;

START TRANSACTION;

ALTER TABLE `LoginLog` DROP COLUMN `LastTime`;

INSERT INTO `OEM` (`ID`, `Alipay`, `CreatedAt`, `DatabaseId`, `Detail`, `Domain`, `LastCreateStatistic`, `LoginPage`, `Name`, `OssId`, `RedisId`, `State`, `Style`, `ThemeId`, `Title`, `UpdatedAt`, `UrlIcon`, `UrlLogo`)
VALUES (101, '', TIMESTAMP '2021-11-19 13:38:00', 1, '嘉杰科技', '["jj4.liulergou.com"]', TIMESTAMP '0001-01-01 00:00:00', 'UniversalLogin', '嘉杰科技', 1, 1, 1, 1, 1, '嘉杰科技', TIMESTAMP '2021-11-19 13:38:00', 'https://img4.waimaomvp.com/7/2023/02/16/images/444450/ERP_1676537757_50AC3C423C2CA336.png', 'https://img4.waimaomvp.com/7/2023/02/16/images/444450/ERP_1676537721_C39F10C793156562.ico');




INSERT INTO `CompanyOem` (`ID`, `AllotTrueName`, `AllotUserId`, `CompanyID`, `CreatedAt`, `OEMID`, `UpdatedAt`, `UserName`)
VALUES (101, 'XIAOMI', 444446, 2, TIMESTAMP '2021-11-19 13:38:00', 101, TIMESTAMP '2021-11-19 13:38:00', 'XIAOMI');

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20230216091023_t39', '6.0.5');

COMMIT;

