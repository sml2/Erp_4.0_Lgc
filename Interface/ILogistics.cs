namespace ERP.Interface;
using Models.Api.Logistics;
using Enums;
using System.Reflection;

public interface ILogistics
{
    PropertyInfo[] GetParameters();
    Platforms Platform { get; }

    Task<PlatformMethodResult> methodDefineds(BaseParameter baseParameter);

    Task<ReportResultList> Send(SendParameter sendParameter);

    Task<ReportResultList> SendReport(SendParameter sendParameter);

    Task<TrackResultList> Track(TrackParameter trackParameter);

    Task<printResultList> Print(MultiPrint multiPrint);
 
    Task<CommonResultList> GetShipTypes(ShipTypeParameter shipTypeParameter);

    Task<CommonResultList> GetWarehouseCode(BaseParameter baseParameter);

    Task<Countries> GetCounties(BaseParameter baseParameter);

    Task<EstimatePriceResult> EstimatePrice(PriceParameter priceParameter);

    //渠道方式
    Task<CommonResultList> LoadChannelCode(BaseParameter baseParameter);
 
    Task<PrintParamResult> GetPrint(FileTypeParameter fileTypeParameter);


    ParameterErrorType CheckParameter(BaseParameter Parameter);

    ParameterErrorType CheckOrderInfoParameter(SendParameter sendParameter);


    Task<CancelResult> CancleOrder(CancleParameter cancleParameter);


    Task<WaybillPrice> GetFee(WaybillInfos Parameter);
}
