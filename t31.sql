﻿START TRANSACTION;

ALTER TABLE `Company`
    ADD `ProductTranslation` int NOT NULL DEFAULT 0 COMMENT '产品翻译(1是2否)';

CREATE TABLE `CompanyResourceConsumptionLog`
(
    `ID`          int       NOT NULL AUTO_INCREMENT COMMENT '表索引',
    `Type`        int       NOT NULL COMMENT '资源类型',
    `Measure`     int       NOT NULL COMMENT '用量 字符数/次数',
    `PackageId`   int       NOT NULL COMMENT '资源包ID',
    `UseUserId`   int       NOT NULL,
    `UseTruename` longtext CHARACTER SET utf8mb4 NOT NULL,
    `CreatedAt`   Timestamp NOT NULL COMMENT '创建时间',
    `UpdatedAt`   Timestamp NOT NULL COMMENT '更新时间',
    `OEMID`       int       NOT NULL,
    `CompanyID`   int       NOT NULL,
    CONSTRAINT `PK_CompanyResourceConsumptionLog` PRIMARY KEY (`ID`),
    CONSTRAINT `FK_CompanyResourceConsumptionLog_Company_CompanyID` FOREIGN KEY (`CompanyID`) REFERENCES `Company` (`ID`) ON DELETE CASCADE,
    CONSTRAINT `FK_CompanyResourceConsumptionLog_CompanyResourcePackages_Packag~` FOREIGN KEY (`PackageId`) REFERENCES `CompanyResourcePackages` (`ID`) ON DELETE CASCADE,
    CONSTRAINT `FK_CompanyResourceConsumptionLog_OEM_OEMID` FOREIGN KEY (`OEMID`) REFERENCES `OEM` (`ID`) ON DELETE CASCADE
) CHARACTER SET=utf8mb4;

UPDATE `Company`
SET `ProductTranslation` = 1
SELECT ROW_COUNT();



CREATE INDEX `IX_CompanyResourceConsumptionLog_CompanyID` ON `CompanyResourceConsumptionLog` (`CompanyID`);

CREATE INDEX `IX_CompanyResourceConsumptionLog_OEMID` ON `CompanyResourceConsumptionLog` (`OEMID`);

CREATE INDEX `IX_CompanyResourceConsumptionLog_PackageId` ON `CompanyResourceConsumptionLog` (`PackageId`);

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20230302110807_t49', '6.0.5');

COMMIT;

START TRANSACTION;

ALTER TABLE `BalanceBills` DROP FOREIGN KEY `FK_BalanceBills_User_UserID`;

ALTER TABLE `BalanceBills` DROP FOREIGN KEY `FK_BalanceBills_UserGroup_GroupID`;

ALTER TABLE `BalanceBills` DROP INDEX `IX_BalanceBills_GroupID`;

ALTER TABLE `BalanceBills` DROP INDEX `IX_BalanceBills_UserID`;

ALTER TABLE `BalanceBills` DROP COLUMN `GroupID`;

ALTER TABLE `BalanceBills` DROP COLUMN `UserID`;

ALTER TABLE `ProductUploadJson` RENAME COLUMN `Pid` TO `ProductID`;

ALTER TABLE `ProductUpload`
    ADD `MarketPlace` longtext CHARACTER SET utf8mb4 NOT NULL;

ALTER TABLE `ProductUpload`
    ADD `ProductType` longtext CHARACTER SET utf8mb4 NOT NULL;

ALTER TABLE `ProductUpload`
    ADD `Unit` longtext CHARACTER SET utf8mb4 NOT NULL;

ALTER TABLE `ProductUpload`
    ADD `productID` int NOT NULL DEFAULT 0;



CREATE INDEX `IX_ProductUploadJson_ProductID` ON `ProductUploadJson` (`ProductID`);

CREATE INDEX `IX_ProductUpload_productID` ON `ProductUpload` (`productID`);

CREATE INDEX `IX_ProductUpload_StoreID` ON `ProductUpload` (`StoreID`);

ALTER TABLE `ProductUpload`
    ADD CONSTRAINT `FK_ProductUpload_product_productID` FOREIGN KEY (`productID`) REFERENCES `product` (`ID`) ON DELETE CASCADE;

ALTER TABLE `ProductUpload`
    ADD CONSTRAINT `FK_ProductUpload_StoreRegion_StoreID` FOREIGN KEY (`StoreID`) REFERENCES `StoreRegion` (`ID`) ON DELETE CASCADE;

ALTER TABLE `ProductUploadJson`
    ADD CONSTRAINT `FK_ProductUploadJson_product_ProductID` FOREIGN KEY (`ProductID`) REFERENCES `product` (`ID`) ON DELETE CASCADE;

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20230307070248_t50', '6.0.5');

COMMIT;

START TRANSACTION;

ALTER TABLE `CompanyResourcePackages`
    ADD `Mold` int NOT NULL DEFAULT 0 COMMENT '类型';



INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20230308081133_t51', '6.0.5');

COMMIT;

