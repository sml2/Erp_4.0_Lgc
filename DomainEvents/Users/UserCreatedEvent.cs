using ERP.Models.DB.Identity;
using MediatR;

namespace ERP.DomainEvents.Users;

public class UserCreatedEvent : INotification
{
    public UserCreatedEvent(User newUser, User createUser)
    {
        NewUser = newUser;
        CreateUser = createUser;
    }

    /// <summary>
    /// 被创建者
    /// </summary>
    public User NewUser { get; }

    /// <summary>
    /// 创建者
    /// </summary>
    public User CreateUser { get; }
}