using ERP.Models.DB.Stores;
using ERP.Models.Stores;
using MediatR;

namespace ERP.DomainEvents;

/// <summary>
/// 店铺创建成功事件
/// </summary>
public class StoreCreatedEvent : INotification
{
    public StoreCreatedEvent(StoreRegion store)
    {
        Store = store;
    }

    public StoreRegion Store { get; }
}