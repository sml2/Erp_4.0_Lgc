﻿using MediatR;

namespace ERP.DomainEvents;
/// <summary>
/// 订单创建成功事件
/// </summary>
public class OrderCreatedEvent : INotification
{
    public OrderCreatedEvent(Models.DB.Orders.Order order)
    {
        Order = order;
    }

    public Models.DB.Orders.Order Order { get; }
    
}