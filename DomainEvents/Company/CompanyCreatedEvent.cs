using ERP.Enums.Identity;
using MediatR;
using User = ERP.Models.DB.Identity.User;

namespace ERP.DomainEvents.Company;

public class CompanyCreatedEvent : INotification
{
    public CompanyCreatedEvent(Models.DB.Users.Company newCompany, User createUser)
    {
        NewCompany = newCompany;
        CreateUser = createUser;
    }

    public Models.DB.Users.Company NewCompany { get; }
    public User CreateUser { get; }
}