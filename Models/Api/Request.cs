﻿
using Newtonsoft.Json;

namespace ERP.Models.Api
{
    /// <summary>
    /// http请求相关参数
    /// </summary>
    public class RequestBase
    {      
        [JsonIgnore]
        public virtual string Url { get; } = string.Empty;

        [JsonIgnore]
        public virtual HttpMethod Method =>HttpMethod.Post;      

        [JsonIgnore]
        public virtual string  ContentType  { get => "application/json"; }
       
       
        public virtual string ToPayload()
        {
            string json = JsonConvert.SerializeObject(this);
            return json;
        }

        public string ToQueryString()
        {
            string query = ConvertHelper.ConvertToQueryString(this);
            return query;
        }
    }
}
