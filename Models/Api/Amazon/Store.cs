using ERP.Models.Stores;
using ERP.Services.Caches;

namespace ERP.Models.Api.Amazon
{

    public class Store
    {
        public Store(AmazonSPKey amazonSPKeyCache,StoreRegion store)
        {
            ID = store.ID;
            Name = store.Name;
            MarketPlaces = store.MarketPlaces;
            AmazonToken = store.GetSPAmazon()!;
            AmazonSPKey = amazonSPKeyCache.Get(AmazonToken.keyID)!;           
        }

        public int ID { get; set; }

        /// <summary>
        /// 店铺名称
        /// </summary>    
        public string Name { get; set; }

        public List<string> MarketPlaces { get; set; }
        
        /// <summary>
        /// 开发者信息
        /// </summary>
        public  ERP.Models.Setting.AmazonSPKey AmazonSPKey { get; set; }       

       /// <summary>
       /// token 信息
       /// </summary>
        public StoreRegion.SPAmazonData? AmazonToken { get; set; }

        public StoreRegion.ShopeeData? ShopeeToken { get; set; }
    }
}