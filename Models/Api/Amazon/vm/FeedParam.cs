﻿using ERP.Enums;
using Newtonsoft.Json;
using OrderSDK.Modles.Amazon.Enums;
using static OrderSDK.Modles.Amazon.ListingsItems.ListingsItemPutRequest;

namespace ERP.Models.Api.Amazon.vm
{
    public class FeedParams
    {
        private string _lang;
        /// <summary>
        /// 语言版本
        /// </summary>
        public string IssueLocale
        {
            get;
            set;
        }

        public string Version { get; set; } = "2.0";

        public List<FeedInfo> feedInfos { get; set; } = new();
    }

    public class FeedInfo
    {
        public int Pid { get; set; }
        public List<FeedParam> feedParams { get; set; }
    }



    public class FeedParam
    {
        public FeedParam(string sku, string productType, string attributes)
        {
            Sku = sku;
            ProductType = productType;
            Attributes = attributes;
        }

        /// <summary>
        /// 
        /// </summary>
        public string Sku { get; set; }

        public int Pid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ProductType { get; set; }

        /// <summary>
        /// json属性
        /// </summary>
        public string Attributes { get; set; }
        public OperationType OperationType { get; set; } = OperationType.Update;
        public RequirementsEnum Requirements { get; set; } = RequirementsEnum.LISTING;
    }

    public class ProductParam
    {
        [NJP("isPush")]
        public bool IsPush { get; set; }

        [NJP("storeID")]
        public int StoreID { get; set; }


        [NJP("pids")]
        public int[] ProductIDs { get; set; }

        [NJP("pathNodes")]
        public string PathNodes { get; set; }

        public long NodeID
        {
            get
            {
                try
                {
                    bool b = long.TryParse(PathNodes.Split(',').Last(), out long nid);
                    if (!b)
                    {
                        throw new Exception("PathNodes error");
                    }
                    return nid;
                }
                catch
                {
                    throw new Exception("PathNodes Exception");
                }
            }
        }
        [NJP("pathName")]
        public string PathName { get; set; }

        /// <summary>
        /// json属性
        /// </summary>
        [NJP("attributes")]
        public string Attributes { get; set; }

        [NJP("marketPlace")]
        public string MarketPlace { get; set; }



        [NJP("language")]
        public string LanguageSign { get; set; }

        public Languages lang
        {
            get
            {
                try
                {
                    return Enum.Parse<Languages>(LanguageSign, true);
                }
                catch
                {
                    throw new Exception("language error");
                }
            }
        }


        /// <summary>
        /// true：修改，false:新增
        /// </summary>
        [NJP("flag")]
        public bool Flag { get; set; }


        /// <summary>
        /// 暂时未使用，标记客户选择的界面模式
        /// </summary>

        [NJP("isDetail")]
        public bool IsDetail { get; set; }
    }

    public class ProductJsonParam
    {
        [NJP("jsonID")]
        public int JsonID { get; set; }

        [NJP("pid")]
        public int ProductID { get; set; }

        /// <summary>
        /// json属性
        /// </summary>
        [NJP("attributes")]
        public string Attributes { get; set; }
    }


    public class Param
    {
        [NJP("pids")]
        public List<int> ProductIDs { get; set; }


    }
}
