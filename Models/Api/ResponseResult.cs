﻿
namespace ERP.Models.Api;

public class ResponseResult<T>
{
    public ResponseResult(Exception e)
    {
        Success = false;
        exception = e;
    }

    public ResponseResult(T result)
    {
        Result = result;
        Success = true;
    }

    public Exception exception { get; set; } = new();

    public bool Success { get; set; }

    public T Result { get; set; } = default!;
}
