﻿
using Newtonsoft.Json;
using System.Collections;
using System.Reflection;

namespace ERP.Models.Api;
public static class ConvertHelper
{
    public static string ConvertToComma(int number)
    {
        return $"{number:n0}";
    }

    public static string ConvertToComma(double number)
    {
        return $"{number:n0}";
    }

    public static string ConvertToQueryString2(object obj)
    {
        PropertyInfo[] properties = obj.GetType().GetProperties();
        string str = "";
        List<string> list = new();
        PropertyInfo[] array = properties;
        foreach (PropertyInfo propertyInfo in array)
        {
            if (!Attribute.IsDefined(propertyInfo, typeof(JsonIgnoreAttribute)))
            {
                string output = "";
                bool num = Attribute.IsDefined(propertyInfo, typeof(JsonPropertyAttribute));
                object value = propertyInfo.GetValue(obj) ?? default!;
                if (num)
                {
                   
                        JsonPropertyAttribute? customAttribute = (propertyInfo).GetCustomAttribute<JsonPropertyAttribute>(inherit: false);
                    
                    if (customAttribute != null)

                        list.Add($"{customAttribute.PropertyName}={value}");
                }
                else if (propertyInfo.GetValue(obj).IsNotNull() && !string.IsNullOrEmpty(propertyInfo.GetValue(obj)!.ToString()))
                {
                    list.Add($"{propertyInfo.Name}={value}");
                }
            }
        }
        return str + string.Join("&", list);
    }



    public static string ConvertToQueryString(object obj)
    {
        string str = "";
        List<string> list = new();
        PropertyInfo[] pi = obj.GetType().GetProperties();
        foreach (PropertyInfo p in pi)
        {
            if (!Attribute.IsDefined(p, typeof(JsonIgnoreAttribute)))
            {
                string output = "";
                var value = p.GetValue(obj);
                if (value != null)
                {
                    var PropertyType = p.PropertyType;
                    var propTypeName = p.PropertyType.Name;
                    string FileldName = p.Name;
                    if (FileldName == "IsNeedRestrictedDataToken" || propTypeName == "RestrictedDataTokenRequest"
                        || FileldName == "RateLimitType"
                        || FileldName == "Method"
                        || FileldName == "Url"
                        || FileldName == "Headers" || FileldName == "ContentType" || FileldName == "TokenDataType")
                        continue;                  
                    if (PropertyType == typeof(DateTime) || PropertyType == typeof(Nullable<DateTime>))
                    {
                        output = ((DateTime)value).ToString("");
                    }
                    else if (propTypeName == typeof(String).Name)
                    {
                        output = value.ToString() ?? string.Empty;
                    }
                    //else if (p.PropertyType.IsEnum || IsNullableEnum(p.PropertyType))
                    //{
                    //    output = value.ToString() ?? string.Empty;
                    //}
                    else if (!p.PropertyType.IsValueType && ( IsEnumerableOfEnum(p.PropertyType) || IsEnumerable(p.PropertyType) ||p.PropertyType.IsGenericType))
                    {
                        var data = ((IEnumerable)value).Cast<object>().Select(a => a.ToString());
                        if (data.Count() > 0)
                        {
                            var result = data.ToArray();
                            output = String.Join(",", result);
                        }
                        else continue;
                    }
                    else
                    {
                        output = JsonConvert.SerializeObject(value);
                    }
                    string propName = string.Empty;
                    JsonPropertyAttribute? customAttribute = (p).GetCustomAttribute<JsonPropertyAttribute>(inherit: false);
                    if (customAttribute != null)
                    {
                        propName= customAttribute.PropertyName!;
                    }
                    else
                    {
                        propName = p.Name;
                    }                    
                    list.Add($"{propName}={output}");                                    
                }
            }
        }
        return str + string.Join("&", list);
    }

    public static Dictionary<string,string> getParameters(object obj)
    {
        string str = "";
        Dictionary<string, string> dic = new();
        PropertyInfo[] pi = obj.GetType().GetProperties();
        foreach (PropertyInfo p in pi)
        {
            if (!Attribute.IsDefined(p, typeof(JsonIgnoreAttribute)))
            {
                string output = "";
                var value = p.GetValue(obj);
                if (value != null)
                {
                    var PropertyType = p.PropertyType;
                    var propTypeName = p.PropertyType.Name;
                    string FileldName = p.Name;
                    if (FileldName == "IsNeedRestrictedDataToken" || propTypeName == "RestrictedDataTokenRequest"
                        || FileldName == "RateLimitType"
                        || FileldName == "Method"
                        || FileldName == "Url"
                        || FileldName == "Headers" || FileldName == "ContentType" || FileldName == "TokenDataType")
                        continue;
                    if (PropertyType == typeof(DateTime) || PropertyType == typeof(Nullable<DateTime>))
                    {
                        output = ((DateTime)value).ToString("");
                    }
                    else if (propTypeName == typeof(String).Name)
                    {
                        output = value.ToString() ?? string.Empty;
                    }
                    else if (p.PropertyType.IsEnum || IsNullableEnum(p.PropertyType))
                    {
                        output = value.ToString() ?? string.Empty;
                    }
                    else if (!p.PropertyType.IsValueType && (IsEnumerableOfEnum(p.PropertyType) || IsEnumerable(p.PropertyType) || p.PropertyType.IsGenericType))
                    {
                        var data = ((IEnumerable)value).Cast<object>().Select(a => a.ToString());
                        if (data.Count() > 0)
                        {
                            var result = data.ToArray();
                            output = String.Join(",", result);
                        }
                        else continue;
                    }
                    else
                    {
                        output = JsonConvert.SerializeObject(value);
                    }
                    var propName = p.Name;
                    dic.Add(propName,output);
                }
            }
        }
        return dic;
    }


    private static bool IsNullableEnum(Type t)
    {
        Type? u = Nullable.GetUnderlyingType(t);
        return (u != null) && u.IsEnum;
    }
    private static bool IsEnumerableOfEnum(Type type)
    {
        return GetEnumerableTypes(type).Any(t => t.IsEnum);
    }

    private static bool IsEnumerable(Type type)
    {
        if (type.IsInterface)
        {
            if (type.IsGenericType
                && (type.GetGenericTypeDefinition() == typeof(IEnumerable<>)
                    || type.GetGenericTypeDefinition() == typeof(IList<>)
                    || type.GetGenericTypeDefinition() == typeof(List<>)))
            {
                return true;
            }
        }

        return false;
    }
    private static IEnumerable<Type> GetEnumerableTypes(Type type)
    {
        if (type.IsInterface)
        {
            if (type.IsGenericType
                && type.GetGenericTypeDefinition() == typeof(IEnumerable<>))
            {
                yield return type.GetGenericArguments()[0];
            }
        }
        foreach (Type intType in type.GetInterfaces())
        {
            if (intType.IsGenericType
                && intType.GetGenericTypeDefinition() == typeof(IEnumerable<>))
            {
                yield return intType.GetGenericArguments()[0];
            }
        }
    }

}
