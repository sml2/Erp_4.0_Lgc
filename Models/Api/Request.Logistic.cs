﻿using ERP.Models.Api.Logistics.Extension;
using Newtonsoft.Json;

namespace ERP.Models.Api
{
    public class RequestLogistic:RequestBase
    {
        [JsonIgnore]
        public virtual SerializableDictionary<string, string> headers { get; } = new();

    }
}
