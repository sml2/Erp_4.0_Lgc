﻿
using Newtonsoft.Json;
using OrderSDK.Modles.Shopee;

namespace ERP.Models.Api
{  
    public class ResponseShopee<T>
    {
        public ResponseShopee(Exception e) 
        {
            Success = false;
            exception = e;
        }

        public ResponseShopee(ShopeeResponseBase<T> result) 
        {
            Result = result;
            Success = true;
        }             

        public Exception exception { get; set; } = new();

        public bool Success { get; set; }

        public ShopeeResponseBase<T> Result { get; set; } = default!;
    }
}
