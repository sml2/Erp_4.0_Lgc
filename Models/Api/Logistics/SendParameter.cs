using ERP.Enums;
using ERP.Models.View.Logistics.WayBill;
using ERP.Models.View.Order.AmazonOrder;
using static EnumExtension;

namespace ERP.Models.Api.Logistics
{
    public class SendParameter : BaseParameter
    {
        /// <summary>
        /// 寄件人信息
        /// </summary>
        public SenderInfo SenderInfo { get; set; } = new();

        /// <summary>
        /// 订单信息
        /// </summary>
        public List<OrderInfo> OrderInfos { get; set; } = new();

        /// <summary>
        /// 修改的订单进度
        /// </summary>
        public List<ProgressValue> OrderProgress { get; set; } = new List<ProgressValue>();

        public SyncToAmazon SyncAmazon { get; set; } = new();

        public int? MergeID { get; set; }

        public SendType Type { get; set; }   // 看看这里能不能自动转换  to  do...  

        /// <summary>
        /// 系统订单号
        /// </summary>
        public string? WaybillOrder { get; set; } 
        /// <summary>
        /// 物流系统订单号
        /// </summary>
        public string? Express { get; set; } 
        /// <summary>
        /// 物流跟踪号
        /// </summary>
        public string? TrackNum { get; set; }

    }
}
