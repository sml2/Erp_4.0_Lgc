﻿namespace ERP.Models.Api.Logistics.EMS
{
    public class PrintUrlRequest: CommonParameter
    {
        public PrintUrlRequest(Parameter p, string logNumber) : base(p)
        {
            this.dataDigest = Sy.Text.Encode.Url_Encode(Signature(logNumber));
            this.barCode = logNumber;          
        }
      
        public override string Url { get => $"https://my.ems.com.cn/pcpErp-web/a/pcp/surface/download"; }

        public override string ToPayload()
        {
            return $"ecCompanyId={ecCompanyId}&dataDigest={dataDigest}&barCode={barCode}&version=2.0&pageType=RM";
        }

        public string dataDigest { get; set; }
        public string barCode { get; set; }
        public string version => "2.0";
        public string pageType => "RM";
    }
}
