﻿namespace ERP.Models.Api.Logistics.EMS
{
    public class GetFeeRequest : CommonParameter
    {
        public GetFeeRequest(Parameter p,List<string> logNumbers) : base(p)
        {
            this.waybillNos = string.Join(',', logNumbers);
            this.dataDigest = Signature(string.Join(',', logNumbers));           
        }
        public GetFeeRequest(Parameter p, string logNumber) : base(p)
        {
            this.waybillNos = logNumber;
            this.dataDigest = Signature(logNumber);
        }

        public override string Url { get => $"https://my.ems.com.cn/pcpErp-web/a/pcp/orderFeeService/getOrderFee"; }         

        public string waybillNos { get; set; }
        public string dataDigest { get; set; }        
    }
}
