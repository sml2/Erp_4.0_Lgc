﻿namespace ERP.Models.Api.Logistics.EMS
{
    public class FeeTrackNum
    {
        public FeeTrackNum() { }
        public FeeTrackNum(double totalFee, string trackNum)
        {
            TotalFee = totalFee;
            TrackNum = trackNum;
        }
        public double TotalFee { get; set; }
        public string TrackNum { get; set; } = string.Empty;
    }
}
