﻿namespace ERP.Models.Api.Logistics.EMS
{
    public class ShipTypesRequest : CommonParameter
    {
        public ShipTypesRequest( Parameter p ):base(p)
        {

        }

        public override string Url { get =>$"https://my.ems.com.cn/pcpErp-web/a/pcp/businessDataService/getBusinessData";}

        public override string ToPayload()
        {
            return "queryType=queryBusinessType";
        }
    }
}
