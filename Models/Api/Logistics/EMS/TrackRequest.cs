﻿using ERP.Models.Api.Logistics.Extension;

namespace ERP.Models.Api.Logistics.EMS
{
    public class TrackRequest:CommonParameter
    {
        public TrackRequest(Parameter p, string trackNumber) : base(p)
        {
            OrderNum = trackNumber;
        }
        public TrackRequest(Parameter p,List<string> orderNums) : base(p)
        {
            OrderNum = string.Join(',', orderNums);
        }
        public string OrderNum { get; set; }     

        public override SerializableDictionary<string, string> headers
        {
            get
            {
                SerializableDictionary<string, string> sh = new SerializableDictionary<string, string>();
                sh.Add("Cookie", $"historD=[\"{OrderNum}\"]");
                return sh;
            }
        }

        public override string Url { get =>$"https://www.ems.com.cn/ems-web/ordermixed/query";}

        public override string ToPayload()
        {
            return $"{{value:[{{mailStatus:\"a\",orderNum:[\"{OrderNum}\"],orderType:\"1\", appleFlag: null}}]}}";
        }

        public List<Value> value { get; set; } = new();
        public class Value
        {
            public string mailStatus => "a";
            public List<string> orderNum { get; set; } = new();
            public string orderType => "2";
            public string appleFlag => string.Empty;
        }
    }
}
