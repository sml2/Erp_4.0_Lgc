﻿using Newtonsoft.Json;

namespace ERP.Models.Api.Logistics.EMS
{
    public class EMSRequest
    {
        public EMSRequest() { }
        public EMSRequest(string logistics_interface, string data_digest, string ecCompanyId)
        {
            this.logistics_interface = logistics_interface;
            this.data_digest = data_digest;
            this.ecCompanyId = ecCompanyId;
        }

        [JsonIgnore]
        public virtual string URL { get; set; } = string.Empty;

        //[JsonIgnore]
        //public virtual HttpMethod Method => HttpMethod.Post;

        public string logistics_interface { get; set; } = string.Empty;
        public string data_digest { get; set; } = string.Empty;
        public string msg_type => "B2C_TRADE";
        public string ecCompanyId { get; set; } = string.Empty;
        public string data_type => "JSON";
    }
}
