
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.EMS
{
    public class AddOrderRequest: CommonParameter
    {
        public AddOrderRequest(Parameter p, SenderInfo senderInfo, OrderInfo orderInfo, ExtParameter Ext) : base(p)
        {
            Id = orderInfo.id;
            //创建订单                     
            created_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            sender_no = p.ecCompanyId;
            mailType = p.mailType;
            wh_code = p.wh_code;
            orderNo = orderInfo.OrderNo;
            logistics_order_no = Helpers.GetOrderNo(orderInfo.OrderNo, orderInfo.SendNum, orderInfo.FailNum); 
            biz_product_no = Ext.ShipMethod;
            weight = Ext.weight;
            volume = Ext.length * Ext.width * Ext.height;
            length = Ext.length;
            width = Ext.width;
            height = Ext.height;
            contents_total_weight = weight;
            contents_total_value = Ext.Price;
            //发件人信息
           sender = new AddOrderRequest.Address();
           sender.name = senderInfo.Name;
           sender.post_code = senderInfo.PostalCode;
           sender.phone = senderInfo.PhoneNumber;
            if (senderInfo.PhoneNumber.Length<13)
            {
                sender.mobile = "86" + senderInfo.PhoneNumber;
            }
           
           sender.email = senderInfo.Mail;
           sender.nation = "CN";
           sender.province = senderInfo.Province;
           sender.city = senderInfo.City;
           sender.address = senderInfo.Address;
           sender.linker = senderInfo.Name;
            //收件人信息
           receiver = new AddOrderRequest.Address();
           receiver.name = orderInfo.ReceiverAddress.Name;
           receiver.post_code = orderInfo.ReceiverAddress.Zip??"";
           receiver.phone = orderInfo.ReceiverAddress.Phone;
           
              receiver.mobile = orderInfo.ReceiverAddress.Phone;
                     
           receiver.email = orderInfo.ReceiverAddress.Email??"";
           receiver.nation = orderInfo.ReceiverAddress.NationShort;
           receiver.province = orderInfo.ReceiverAddress.Province;
           receiver.city = orderInfo.ReceiverAddress.City;
           receiver.linker = orderInfo.ReceiverAddress.Name;
            string Address = orderInfo.ReceiverAddress.Address;
            if (orderInfo.ReceiverAddress.Address2.IsNotWhiteSpace()) Address += $" {orderInfo.ReceiverAddress.Address2}";
            if (orderInfo.ReceiverAddress.Address3.IsNotWhiteSpace()) Address += $" {orderInfo.ReceiverAddress.Address3}";
            receiver.address = Address;
            //包裹信息
            items = new List<AddOrderRequest.Items>();
            foreach (var pro in orderInfo.GoodInfos)
            {
                var item = new AddOrderRequest.Items();
                item.cargo_no = pro.goodId;
                item.cargo_name = Ext.cnname;
                item.cargo_name_en = Ext.enname;
                item.cargo_type_name = "Product";
                item.cargo_quantity = pro.Count;
                double SinglePrice =Helpers.RoundData(Ext.Price / pro.Count);
                item.cargo_value = SinglePrice;
                item.cargo_currency = Ext.Currency;
                item.cost = SinglePrice;
                item.carogo_weight = Convert.ToInt32(weight / pro.Count);
                item.cargo_description = Ext.description;
                item.cargo_serial = Ext.HsCode;
                items.Add(item);
            }
        }

        public override string Url { get => $"https://my.ems.com.cn/pcpErp-web/a/pcp/orderService/OrderReceiveBack";  }

        public override string ToPayload()
        {
            string json = base.ToPayload();
            return  $"logistics_interface={json}&data_digest={Signature(json)}&msg_type=B2C_TRADE&ecCompanyId={ecCompanyId}&data_type=JSON";            
        }

        [JsonIgnore]
        public string orderNo { get; set; }

        [JsonIgnore]
        public int Id { get; set; }
        public string created_time { get; set; }

        public string sender_no { get; set; }

        public string wh_code { get; set; }

        public string mailType { get; set; }

        public string logistics_order_no { get; set; }

        public string batch_no { get; set; } = string.Empty;

        public string biz_product_no { get; set; }
        /// <summary>
        /// 单位:g
        /// </summary>
        public int weight { get; set; }
        /// <summary>
        /// 邮件体积 单位：立方厘米
        /// </summary>
        public double volume { get; set; }
        /// <summary>
        /// 单位：厘米
        /// </summary>
        public double length { get; set; }
        /// <summary>
        /// 单位：厘米
        /// </summary>
        public double width { get; set; }
        /// <summary>
        /// 单位：厘米
        /// </summary>
        public double height { get; set; }
        //public string postage_total { get; set; }
        //public string postage_currency { get; set; }
        /// <summary>
        /// 单位：克(内件单个重量*数量之和)
        /// </summary>
        public int contents_total_weight { get; set; }
        /// <summary>
        /// 默认美元，单位：美元（内件单个价值*数量之和）
        /// </summary>
        public double contents_total_value { get; set; }
        //public string transfer_type { get; set; }
        //public string battery_flag { get; set; }
        //public string pickup_notes { get; set; }
        ///// <summary>
        ///// 报价标志 1:基本 2:保价 3:保险
        ///// </summary>
        //public string insurance_flag { get; set; }
        ///// <summary>
        ///// 单位：元
        ///// </summary>
        //public string insurance_amount { get; set; }
        //public string undelivery_option { get; set; }
        //public string valuable_flag { get; set; }
        /// <summary>
        /// 1.个人申报；2:企业申报；3:个人税款复
        /// </summary>
        public string declare_source => "2";
        public string declare_type => "1";
        public string declare_curr_code => "USD";
        //public string printcode { get; set; }

        //public string barcode { get; set; }
        /// <summary>
        /// 预报关：0-无预报关信息 1-有预报关信息
        /// </summary>
        public string forecastshut => "0";
        /// <summary>
        /// 9610标识 1:是 2：否。目前填 2：否
        /// </summary>
        public string mail_sign => "2";

        public Address sender { get; set; }
        public class Address
        {
            public string name { get; set; } = string.Empty;
            public string post_code { get; set; } = string.Empty;
            public string phone { get; set; } = string.Empty;
            public string mobile { get; set; } = string.Empty;
            public string email { get; set; } = string.Empty;
            //public string id_type { get; set; }
            //public string id_no { get; set; }
            public string nation { get; set; } = string.Empty;
            public string province { get; set; } = string.Empty;
            public string city { get; set; } = string.Empty;
            public string county { get; set; } = string.Empty;
            public string address { get; set; } = string.Empty;
            //public string gis { get; set; }
            public string linker { get; set; } = string.Empty;
        }
        public Address receiver { get; set; }

        public List<Items> items { get; set; }
        public class Items
        {

            public string cargo_no { get; set; } = string.Empty;


            public string cargo_name { get; set; } = string.Empty;
            public string cargo_name_en { get; set; } = string.Empty;
            public string cargo_type_name { get; set; } = string.Empty;
            //public string cargo_type_name_en { get; set; }
            //public string cargo_origin_name { get; set; }
            //public string cargo_link { get; set; }
            public int cargo_quantity { get; set; }
            public double cargo_value { get; set; }
            /// <summary>
            /// 默认美元，申报价值可以和商品单价一样
            /// </summary>
            public double cost { get; set; }
            public string cargo_currency { get; set; }
            /// <summary>
            /// 单位：克
            /// </summary>
            public int carogo_weight { get; set; }
            public string cargo_description { get; set; } = string.Empty;
            public string cargo_serial { get; set; } = string.Empty;
            public string unit => "个";
            //public string intemsize { get; set; }
        }
    }
}
