﻿

using Newtonsoft.Json;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ERP.Models.Api.Logistics.EMS
{
    public class ExtParameter: BaseExtParam
    {
        /// <summary>
        /// 商品申报名(中文)
        /// </summary>        
        [DisplayName(" 商品申报名(中文)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        [MaxLength(50, ErrorMessage = "长度不能超过50")]
        public string cnname { get; set; } = string.Empty;

        /// <summary>
        /// 商品申报名(英文)
        /// </summary>
        [DisplayName(" 商品申报名(英文)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string enname { get; set; }=string.Empty;

        /// <summary>
        /// 商品描述
        /// </summary>
        [DisplayName("商品描述")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string description { get; set; } = string.Empty;

        /// <summary>
        /// 包裹重量(g)
        /// </summary>
        [DisplayName("包裹重量(g)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public int weight { get; set; }

        /// <summary>
        /// 包裹长度(cm)
        /// </summary>
        [DisplayName("包裹长度(cm)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public double length { get; set; }

        /// <summary>
        /// 包裹宽度(cm)
        /// </summary>
        [DisplayName("包裹宽度(cm)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public double width { get; set; }

        /// <summary>
        /// 包裹高度(cm)
        /// </summary>
        [DisplayName("包裹高度(cm)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public double height { get; set; }
      
        /// <summary>
        /// 海关编码
        /// </summary>
        public string HsCode { get; set; } = string.Empty;

        /// <summary>
        /// 货运方式
        /// </summary>
        [DisplayName("货运方式")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string ShipMethod { get; set; } = string.Empty;

        /// <summary>
        /// 申报价格($)
        /// </summary>
        [DisplayName("申报价格($)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public double Price { get; set; }

        [Required(ErrorMessage = "{0} 不能为空")]
        /// <summary>
        /// 币种
        /// </summary>
        public string Currency { get; set; } 
    }
}
