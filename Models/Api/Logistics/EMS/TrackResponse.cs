﻿namespace ERP.Models.Api.Logistics.EMS
{
    public class TrackResponse
    {
        public bool success { get; set; }
        public string message { get; set; } = string.Empty;
        public Value value { get; set; } = new();
        public class Value
        {
            public string receiverAddress { get; set; } = string.Empty;
            public string senderAddress { get; set; } = string.Empty;
            public List<DomesticMailResultDtoList> domesticMailResultDtoList { get; set; } = new();
            public class DomesticMailResultDtoList
            {
                public string resCode { get; set; } = string.Empty;
                public string resMsg { get; set; } = string.Empty;
                public List<ResObj> resObj { get; set; } = new();
                public class ResObj
                {
                    public string traceNo { get; set; } = string.Empty;

                    public string opTime { get; set; } = string.Empty;

                    public string opCode { get; set; } = string.Empty;

                    public string opName { get; set; } = string.Empty;

                    public string opDesc { get; set; } = string.Empty;

                    public string opOrgProvName { get; set; } = string.Empty;

                    public string opOrgCity { get; set; } = string.Empty;

                    public string opOrgCode { get; set; } = string.Empty;

                    public string opOrgName { get; set; } = string.Empty;

                    public string operatorNo { get; set; } = string.Empty;

                    public string operatorName { get; set; } = string.Empty;

                    public string gis { get; set; } = string.Empty;

                    public string transportType { get; set; } = string.Empty;

                    public string arrivalCity { get; set; } = string.Empty;

                    public string receiverAddress { get; set; } = string.Empty;

                    public string senderAddress { get; set; } = string.Empty;

                    public string deliveryUserPhone { get; set; } = string.Empty;

                    public string deliveryUserName { get; set; } = string.Empty;

                    public string deliveryUserCode { get; set; } = string.Empty;

                    public string receiverName { get; set; } = string.Empty;

                    public string senderName { get; set; } = string.Empty;

                    public string delivererCity { get; set; } = string.Empty;

                    public string receiverCity { get; set; } = string.Empty;

                    public string productName { get; set; } = string.Empty;

                    public string senderPhone { get; set; } = string.Empty;

                    public string receivePhone { get; set; } = string.Empty;

                    public string bubbleTime { get; set; } = string.Empty;

                    public string isOne { get; set; } = string.Empty;
                }

                public CourierInfoDto courierInfoDto { get; set; } = new();
                public class CourierInfoDto
                {

                    public string name { get; set; }= string.Empty;

                    public string phone { get; set; } = string.Empty;

                    public string deliveryUserCode { get; set; } = string.Empty;
                }
            }
        }      
    }
}
