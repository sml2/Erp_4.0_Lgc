﻿using ERP.Attributes.Logistics;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace ERP.Models.Api.Logistics.EMS
{
    public class Parameter: BaseParam
    {
        private string id;
        private string key;
        private string code;
        private string type;
        [ViewConfigParameter(nameof(ecCompanyId),Message = "请填写大客户代码",Sort = 0)]
        /// <summary>
        /// 大客户代码
        /// </summary>
        public string ecCompanyId { get { return id; } set { id = value.Trim(); } } 
        [ViewConfigParameter(nameof(Secret),  Sort = 1)]
        public string Secret { get { return key; } set { key = value.Trim(); } } 
        [ViewConfigParameter(nameof(wh_code), Message = "请填写用户揽收机构编号", Sort =2)]
        /// <summary>
        /// 用户揽收机构编号
        /// </summary>
        public string wh_code { get { return code; } set { code = value.Trim(); } } 
        [ViewConfigParameter(nameof(mailType), Message = "电商标识", Sort = 3)]
        /// <summary>
        /// 电商标识
        /// </summary>
        public string mailType { get { return type; } set { type = value.Trim(); } } 
        ///// <summary>
        ///// 1:居民身份证、临时居民身份证、临时或者户口簿
        ///// 2:中国人民解放军身份证件、中国人民武装警察身份证件
        ///// 3:港澳居民来往内地通行证、台湾居民来往内地通行证或者其他有效旅行证件
        ///// 4:外国公民护照
        ///// 5:法律、行政和国家规定的其他有效身份证件
        ///// </summary>
        //public string id_type { get; set; }
        ///// <summary>
        ///// 证件号:寄件人必填
        ///// </summary>
        //public string id_no { get; set; }

        public string Domain
        {
            get
            {
                return "https://www.ems.com.cn";
            }
        }
    }
}
