﻿namespace ERP.Models.Api.Logistics.EMS
{
    public class PrintUrlResponse
    {
        /// <summary>
        /// 响应标志，true表示成功，false表示失败
        /// </summary>
        public string success { get; set; } = string.Empty;
        /// <summary>
        /// PDF面单字节的十六进制数据
        /// </summary>
        public string data { get; set; } = string.Empty;
        /// <summary>
        /// 错误码Code
        /// </summary>
        public string err_code { get; set; } = string.Empty;
        /// <summary>
        /// 错误原因
        /// </summary>
        public string msg { get; set; } = string.Empty;
        public string digest { get; set; } = string.Empty;
    }
}
