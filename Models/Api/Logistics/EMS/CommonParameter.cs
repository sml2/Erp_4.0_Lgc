﻿using Newtonsoft.Json;
using System.Text;
using System.Runtime.InteropServices;

namespace ERP.Models.Api.Logistics.EMS
{
    public class CommonParameter : RequestLogistic
    {
        public CommonParameter(Parameter p)
        {
            this.ecCompanyId = p.ecCompanyId;
            this.Secret = p.Secret;
        }

        public string ecCompanyId { get; set; }
        public string Secret { get; set; }


        public long TimeStamp { get; set; }

        public string MD5 { get; set; } = string.Empty;

        [JsonIgnore]
        public string Platform { get; set; } = string.Empty;

        [JsonIgnore]
        public string DoMain { get => ""; }

        public override string ContentType => "application/x-www-form-urlencoded";

        public string Signature(string Json)
        {
            //string Data = $"{Json}{Secret}";
            //return Sy.Security.MD5.Encrypt(Data);

            string Data = $"{Json}{Secret}";
            byte[] sources = Encoding.UTF8.GetBytes(Data);
            //System.Security.Cryptography.MD5CryptoServiceProvider MD5CSP = new System.Security.Cryptography.MD5CryptoServiceProvider();
            //byte[] targets = MD5CSP.ComputeHash(sources);
            byte[] targets = System.Security.Cryptography.MD5.Create().ComputeHash(sources);
            string value = Convert.ToBase64String(targets);
            return value;
        }
    }
}
