﻿namespace ERP.Models.Api.Logistics.EMS
{
    public class ShipTypeResponse 
    {
        public List<DataItem> data { get; set; } = new();
        public class DataItem
        {
            public string businessCode { get; set; } = string.Empty;

            public string businessName { get; set; }=string.Empty;
            public override string ToString()
            {
                return $"[{businessCode}]{businessName}";
            }
        }      
    }
}
