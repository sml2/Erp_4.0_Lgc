﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.EMS
{
    public class AddOrderResponse
    {
        /// <summary>
        /// 响应标志，true表示成功，false表示失败
        /// </summary>
        public string success { get; set; } = string.Empty;
        /// <summary>
        /// 系统生成的订单号
        /// </summary>
        public string waybillNo { get; set; } = string.Empty;
        /// <summary>
        /// 错误码Code
        /// </summary>
        public string reason { get; set; } = string.Empty;
        /// <summary>
        /// 错误原因
        /// </summary>
        public string msg { get; set; } = string.Empty;

    }
}
