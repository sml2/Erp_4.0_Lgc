﻿namespace ERP.Models.Api.Logistics;

public class TrackResultList : BaseResult
{
    public TrackResultList(Exception e):base(e)
    {
        
    }
    public TrackResultList(List<TrackResult> trackResults)
    {
        TrackResults = trackResults;
    }
    public List<TrackResult> TrackResults { get; set; } = new();
}
