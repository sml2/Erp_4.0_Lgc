﻿namespace ERP.Models.Api.Logistics
{
    public class PriceParameter : BaseParameter
    {
        /// <summary>
        /// 物流窗口填写数据
        /// </summary>
        public string ext { get; set; } = string.Empty;

        /// <summary>
        /// 国家编码
        /// </summary>
        public string? CountryCode { get; set; }

        /// <summary>
        /// 出发地
        /// </summary>
        public string? DepartureCode { get; set; }

        /// <summary>
        /// 目的地,目的国家二字代码
        /// </summary>
        public string? DestinationsCode { get; set; }

        //yw parameter
        public int? DepartureAreaCode { get; set; }
        public string? DepartureArea { get; set; }
        public string? DepartureId { get; set; }
        public string? DepartureName { get; set; }
        public string? DestinationId { get; set; }
        public string? DestinationChinesename { get; set; }
    }
}
