﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static EnumExtension;

namespace ERP.Models.Api.Logistics.HL
{
    public class GetPriceRequest : CommonParameter
    {
        public GetPriceRequest(Parameter p, double weight, double length, double width, double height, string express_type, string country) : base(p)
        {
            this.weight = weight;
            this.height = height;
            this.length = length;
            this.width = width;
            this.country = country;
            this.express_type = express_type;
        }     
        public double weight { get; set; }
        public double length { get; set; }

        public double width { get; set; }

        public double height { get; set; }

        public string express_type { get; set; }
        public string country { get; set; }
        public EunmcargoType cargoType { get; set; } = EunmcargoType.P;

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;
        [JsonIgnore]
        public override string Url => $"http://{DoMain}:8082/defaultPriceSearchJson.htm?weight={weight}&length={length}&width={width}&height={height}&country={country}&express_type={express_type}&cargoType={cargoType.ToString()}";

    }

    public enum EunmcargoType {
        
        [Description("包裹")]
        P,
        [Description("文件")]
        D,
        [Description("PAK袋")]
        B,
    }


    public class GetFeeResponse
    {
        /// <summary>
        /// 重量
        /// </summary>
        public string charge_weight { get; set; } = string.Empty;
        /// <summary>
        /// 速递运费金额
        /// </summary>
        public string freight_amount { get; set; } = string.Empty;
        /// <summary>
        /// 燃油
        /// </summary>
        public string oil_amount { get; set; } = string.Empty;
        /// <summary>
        /// 杂费
        /// </summary>
        public string other_amount { get; set; } = string.Empty;
        /// <summary>
        /// 计费公式说明
        /// </summary>
        public string price_formula { get; set; } = string.Empty;
        /// <summary>
        /// 价格备注
        /// </summary>
        public string price_note { get; set; } = string.Empty;
        /// <summary>
        /// 运输方式名称
        /// </summary>
        public string product_name { get; set; } = string.Empty;
        /// <summary>
        /// 操作备注
        /// </summary>
        public string product_note { get; set; } = string.Empty;
        /// <summary>
        /// 是否可跟踪标记
        /// </summary>
        public string product_tracking { get; set; } = string.Empty;
        /// <summary>
        /// 总金额
        /// </summary>
        public string total_amount { get; set; } = string.Empty;
    }
}
