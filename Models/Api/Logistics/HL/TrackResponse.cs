﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.HL
{

    public class TrackRequest : CommonParameter
    {
        public TrackRequest(Parameter p,List<string> customNumbers) : base(p)
        {
            CustomNumbers = string.Join(',', customNumbers);
        }

        public TrackRequest(Parameter p, string customNumber) : base(p)
        {
            CustomNumbers = customNumber;
        }


        public string CustomNumbers { get; set; }

        public override string Url => $"http://{DoMain}:8082/selectTrack.htm?documentCode={CustomNumbers}";
    }

    public class TrackResponse
    {
        public string ack { get; set; } = string.Empty;
        /// <summary>
        /// 如果未获取到转单号，则该列存放失败原因
        /// </summary>
        public string message { get; set; } = string.Empty;
        public List<Cls_data>? data { get; set; } = new();
        public class Cls_data
        {
            public string businessId { get; set; } = string.Empty;

            /// <summary>
            /// 收件国家代码country code
            /// </summary>
            public string consigneeCountry { get; set; } = string.Empty;

            /// <summary>
            /// 物流方式express type
            /// </summary>
            public string productKindName { get; set; } = string.Empty;

            /// <summary>
            /// 参考号 ref number
            /// </summary>
            public string referenceNumber { get; set; } = string.Empty;

            /// <summary>
            /// 末条轨迹内容 the last track content
            /// </summary>
            public string trackContent { get; set; } = string.Empty;

            /// <summary>
            /// 末条轨迹时间 the last track date
            /// </summary>
            public string trackDate { get; set; } = string.Empty;

            /// <summary>
            /// 末条地点 the last track location
            /// </summary>
            public string trackLocation { get; set; } = string.Empty;

            /// <summary>
            /// 签收人 sign person,if no sign it's empty
            /// </summary>
            public string trackSignperson { get; set; } = string.Empty;

            /// <summary>
            /// 跟踪号
            /// </summary>
            public string trackingNumber { get; set; } = string.Empty;
            public List<Cls_trackDetails> trackDetails { get; set; } = new();
            public class Cls_trackDetails
            {
                public string business_id { get; set; } = string.Empty;
                public string system_id { get; set; } = string.Empty;

                /// <summary>
                /// 轨迹内容
                /// </summary>
                public string track_content { get; set; } = string.Empty;
                public string track_createdate { get; set; } = string.Empty;

                /// <summary>
                /// 轨迹时间
                /// </summary>
                public string track_date { get; set; } = string.Empty;
                public string track_id { get; set; } = string.Empty;
                public string track_kind { get; set; } = string.Empty;

                /// <summary>
                /// 地点
                /// </summary>
                public string track_location { get; set; } = string.Empty;

                /// <summary>
                /// 签收时间 sign date,if no sign it's empty
                /// </summary>
                public string track_signdate { get; set; } = string.Empty;

                /// <summary>
                /// 签收人
                /// </summary>
                public string track_signperson { get; set; } = string.Empty;
            }
        }
    }
}
