﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.HL
{
    public class ExtParameter: BaseExtParam
    {      
        public ExtParameter(string currency, double invoice_amount, string ProNameCn, string ProNameEn, double length, double width,
            double height, double weight,int BoxCount, string shipper_taxnotype, string shipper_taxno, 
           int shipType,string shipMethod)
        {
            this.Currency=currency;
            this.invoice_amount = invoice_amount;
            this.ProNameCn = ProNameCn;
            this.ProNameEn = ProNameEn;
            this.length = length;
            this.width = width;
            this.height = height;
            this.weight = weight;
            this.BoxCount = BoxCount;
            if (Enum.IsDefined(typeof(enumtaxnotype), shipper_taxnotype) && Enum.TryParse<enumtaxnotype>(shipper_taxnotype, out var labelshipper_taxnotype))
            {
                this.shipper_taxnotype = labelshipper_taxnotype;
            }
            else
            {
                Console.WriteLine("HL shipper_taxnotype:"+ shipper_taxnotype);
            }
            this.shipper_taxno = shipper_taxno;
            ShipType = shipType;
            ShipMethod = ExcuteMethod(shipMethod);
        }


        public string ExcuteMethod(string method)
        {
            if(method.Contains('('))
            {
                return method.Split('(')[1].Split(')')[0];
            }
            return method;

        }

        /// <summary>
        /// 申报名称(中文)
        /// </summary>
        public string ProNameCn { get; set; }

        /// <summary>
        ///申报名称(英文)
        /// </summary>
        public string ProNameEn { get; set; }

        /// <summary>
        ///包装盒长度(cm)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包装盒长度(cm)")]
        public double length { get; set; }

        /// <summary>
        /// 包装盒宽度(cm)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包装盒宽度(cm)")]
        public double width { get; set; }

        /// <summary>
        /// 包装盒高度(cm)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包装盒高度(cm)")]
        public double height { get; set; }

        /// <summary>
        /// 商品重量(kg)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("商品重量(g)")]
        public double weight { get; set; }

        /// <summary>
        ///申报商品价值
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("申报商品价值")]
        public double invoice_amount { get; set; }

        [DisplayName("货币种类")]
        public string Currency { get; set; }

        /// <summary>
        /// 包装盒数量
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包装盒数量")]
        public int BoxCount { get; set; }

        /// <summary>
        /// 货运方式中的product_id
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货运方式code")]
        public int ShipType { get; set; }

        /// <summary>
        /// 货运方式 express_type
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货运方式")]
        public string ShipMethod { get; set; }

        /// <summary>
        /// 税号类型
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("税号类型")]
        public enumtaxnotype shipper_taxnotype { get; set; }

        /// <summary>
        /// 税号
        /// </summary>
        //[Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("税号")]
        public string shipper_taxno { get; set; }                     
    }

    public enum enumtaxnotype
    {
        [EnumExtension.Description("IOSS")]
        IOSS,
        [EnumExtension.Description("NO-IOSS")]
        NOIOSS,
        [EnumExtension.Description("OTHER")]
        OTHER,
    }
}
