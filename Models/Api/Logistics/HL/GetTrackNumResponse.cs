﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.HL
{
    public class GetTrackNumResponse
    {
        public string msg { get; set; } = string.Empty;
        public string order_customerinvoicecode { get; set; } = string.Empty;
        public string order_id { get; set; } = string.Empty;
        public string order_referencecode { get; set; } = string.Empty;
        public string order_serveinvoicecode { get; set; } = string.Empty;
        public string status { get; set; } = string.Empty;
    }
}
