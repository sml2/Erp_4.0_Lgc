﻿namespace ERP.Models.Api.Logistics.HL
{
    public class PrintRequest : CommonParameter
    {
        public PrintRequest(Parameter p,string ContentType,string logNumbers) : base(p)
        {
            Sy.String labelPara = ContentType;
            string SpliterS = "[", SpliterE = "]";
            if (labelPara.IsNullOrEmpty() || !labelPara.CanMid(SpliterS, SpliterE))
            {
                this.LabelPara = "";
            }
            else
            {
                string PrintType = labelPara.Mid(SpliterS, SpliterE);
                string Format = labelPara.Before(SpliterS);
                if (Format.IsNullOrEmpty()) this.LabelPara = $"PrintType={PrintType}";
                else this.LabelPara = $"Format={Format}&PrintType={PrintType}";
            }
            this.LogNumbers = logNumbers;
        }

        public string LabelPara { get; set; } = string.Empty;

        public string LogNumbers { get; set; } = string.Empty;

        public override string Url => $"http://{PrintDoMain}:8089/order/FastRpt/PDF_NEW.aspx?{LabelPara}&order_id={LogNumbers}";
    }
}
