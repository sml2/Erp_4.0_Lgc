﻿namespace ERP.Models.Api.Logistics.HL
{
    public class UserRequest : CommonParameter
    {
        public UserRequest(Parameter p) : base(p)
        {

        }        
        public override string Url => $"http://{DoMain}:8082/selectAuth.htm?username={username}&password={password}";
        public override HttpMethod Method => HttpMethod.Get;
    }
}
