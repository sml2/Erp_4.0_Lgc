﻿namespace ERP.Models.Api.Logistics.HL
{
    public class UserAccountInfo
    {
        public string customer_id { get; set; } = string.Empty;
        public string customer_userid { get; set; } = string.Empty;
        public string ack { get; set; } = string.Empty;
    }
}
