﻿using ERP.Attributes.Logistics;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.HL
{
    public class Parameter: BaseParam
    {
        private string name;
        private string password;
        private string ourl;
        private string purl;
        [ViewConfigParameter(nameof(Username), "用户名", Sort = 0)]
        public string Username { get { return name; } set { name = value.Trim(); } } 
        [ViewConfigParameter(nameof(Password), "密码", Sort = 1)]
        public string Password { get { return password; } set { password = value.Trim(); } }
        [ViewConfigParameter(nameof(OrderUrl), "请求链接", Message = "请填写正确的链接", Sort = 2)]
        public string OrderUrl { get { return ourl; } set { ourl = value.Trim(); } } 
        [ViewConfigParameter(nameof(PrintUrl), "打印链接", Message = "请填写正确的链接", Sort = 3)]
        public string PrintUrl { get { return purl; } set { purl = value.Trim(); } }
     
        private string _OrderUrlHost=string.Empty;

      
        public string OrderUrlHost
        {
            get => GetURLHost(OrderUrl, ref _OrderUrlHost, "8082");
            set
            {
                _OrderUrlHost = value;
            }
        }
    
        private string _PrintUrlHost;
        /// <summary>
        /// 打印面单所需的URL
        /// </summary>
        public string PrintUrlHost
        {
            get
            {               
                return GetURLHost(PrintUrl, ref _PrintUrlHost, "8089");
            }
            set
            {
                _PrintUrlHost = value;
            }
        }

        private string GetURLHost(string Raw, ref string Target, string port)
        {
            if (Target.IsNullOrEmpty())
            {
                try
                {
                    //兼容输入http的情况
                    Uri uri = new Uri(Raw);
                    Target = uri.Host;
                }
                catch
                {
                    Target = Raw;
                    //抹除端口信息，强制使用默认值
                    var portInfo = $":{port}";
                    if (Target.Contains(portInfo)) Target = Target.Replace(portInfo, "");
                }
            }
            return Target;
        }
    }
}
