﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.HL
{
    public class AddOrderResponse
    {
        /// <summary>
        /// 状态值
        /// </summary>
        public string ack { get; set; } = string.Empty;
        /// <summary>
        /// 如果未获取到转单号，则该列存放失败原因
        /// </summary>
        public string message { get; set; } = string.Empty;

        public string GetDecodedMessage() => Uri.UnescapeDataString(message);
        /// <summary>
        /// 参考号
        /// </summary>
        public string reference_number { get; set; } = string.Empty;
        /// <summary>
        /// 跟踪号
        /// </summary>
        public string tracking_number { get; set; } = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        public string order_id { get; set; } = string.Empty;
    }
}
