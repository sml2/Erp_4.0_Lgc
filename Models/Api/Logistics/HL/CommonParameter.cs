﻿using Newtonsoft.Json;

namespace ERP.Models.Api.Logistics.HL
{
    public class CommonParameter: RequestLogistic
    {
        public CommonParameter(Parameter p)
        {
            this.username = p.Username;
            this.password = p.Password;
            this.OrderUrl = p.OrderUrl;
            this.PrintUrl = p.PrintUrl;
        }
        [JsonIgnore]
        public string username { get; set; }
        [JsonIgnore]
        public string password { get; set; }
        [JsonIgnore]
        public string OrderUrl { get; set; }
        [JsonIgnore]
        public string PrintUrl { get; set; }

        [JsonIgnore]
        public string DoMain
        {
            get
            {
                try
                {
                    Uri uri = new Uri(OrderUrl);
                    return uri.Host;
                }
                catch
                {
                    return OrderUrl;
                }
            }
        }

        [JsonIgnore]
        public string PrintDoMain
        {
            get
            {
                try
                {
                    Uri uri = new Uri(PrintUrl);
                    return uri.Host;
                }
                catch
                {
                    return PrintUrl;
                }
            }
        }

        [JsonIgnore]
        public string Platform { get; set; } = string.Empty;

    }
}
