﻿namespace ERP.Models.Api.Logistics.HL
{
    public class ContentTypeRequest: CommonParameter
    {
        public ContentTypeRequest(Parameter p) : base(p)
        {
        }

        public override string ContentType => "application/x-www-form-urlencoded";

        public override string Url => $"http://{DoMain}:8082/selectLabelType.htm";
    }

    public class LabelTypeItem 
    {
        public string format_id { get; set; } = string.Empty;
        public string format_name { get; set; } = string.Empty;
        public string format_path { get; set; } = string.Empty;
        public string print_type { get; set; } = string.Empty;
        public override string ToString()
        {
            return $"{format_name}";
        }
        public string PrintData => $"{format_path}[{print_type}]";
    }
}
