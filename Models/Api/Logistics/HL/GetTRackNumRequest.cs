﻿namespace ERP.Models.Api.Logistics.HL
{
    public class GetTRackNumRequest : CommonParameter
    {
        public GetTRackNumRequest(Parameter p,string logNumber) : base(p)
        {
            order_id = logNumber;
        }     
        public string order_id { get; set; }
        public override HttpMethod Method => HttpMethod.Get;
        public override string Url => $"http://{DoMain}:8082/getOrderTrackingNumber.htm?order_id={order_id}";
    }
}
