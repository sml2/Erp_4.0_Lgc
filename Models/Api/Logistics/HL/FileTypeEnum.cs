﻿using static EnumExtension;

namespace ERP.Models.Api.Logistics.HL
{
    public enum FileTypeEnum
    {
        [Description("PDF")]
        PDF,
    }   
}
