﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.HL
{

    public class ShipTypeResponse
    {
        public string express_type { get; set; } = string.Empty;
        public string product_id { get; set; } = string.Empty;
        public string product_shortname { get; set; } = string.Empty;
        public override string ToString()
        {
            return $"{product_shortname}({express_type})";
        }
    }
}
