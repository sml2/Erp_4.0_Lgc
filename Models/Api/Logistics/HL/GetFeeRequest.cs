﻿using Newtonsoft.Json;

namespace ERP.Models.Api.Logistics.HL
{
    public class GetFeeRequest : CommonParameter
    {
        public GetFeeRequest(Parameter p,double weight,string country) : base(p)
        {
            this.weight = weight;
            this.country = country;
        }

        public double weight { get; set; }

        public string country { get; set; }
        public EunmcargoType cargoType { get; set; } = EunmcargoType.P;

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;
        [JsonIgnore]
        public override string Url => $"http://{DoMain}:8082/defaultPriceSearchJson.htm?weight={weight}&country={country}&cargoType={cargoType.ToString()}";
      
    }
}
