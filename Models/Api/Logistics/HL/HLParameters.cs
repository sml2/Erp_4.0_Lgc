﻿namespace ERP.Models.Api.Logistics.HL
{
    public class HLParameters
    {
        /// <summary>
        /// 货运方式
        /// </summary>
        public string ShipCode { get; set; } = string.Empty;

        /// <summary>
        /// 税务类型
        /// </summary>
        public string TaxNoType { get; set; } = string.Empty;

        /// <summary>
        /// 税号
        /// </summary>
        public string TaxNo { get; set; } = string.Empty;
    }
}
