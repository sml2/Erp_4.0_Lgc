using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.HL
{
    public class AddOrderRequest : CommonParameter
    {
        public AddOrderRequest(Parameter p,SenderInfo senderInfo,OrderInfo orderInfo, UserAccountInfo userAccountInfo, ExtParameter Ext) : base(p)
        {
            Id = orderInfo.id;
            buyerid = "";
            //收件人信息
            orderNo = orderInfo.OrderNo;
            string Address = orderInfo.ReceiverAddress.Address;
            if (orderInfo.ReceiverAddress.Address2.IsNotWhiteSpace()) Address += $" {orderInfo.ReceiverAddress.Address2}";
            if (orderInfo.ReceiverAddress.Address3.IsNotWhiteSpace()) Address += $" {orderInfo.ReceiverAddress.Address3}";
              consignee_address = Address;
              consignee_city = orderInfo.ReceiverAddress.City;
              consignee_mobile = orderInfo.ReceiverAddress.Phone;
              consignee_name = orderInfo.ReceiverAddress.Name;
              consignee_postcode = orderInfo.ReceiverAddress.Zip??"";
              consignee_state = orderInfo.ReceiverAddress.Province;
              consignee_telephone = orderInfo.ReceiverAddress.Phone;
              country = orderInfo.ReceiverAddress.NationShort;
            //IOSS信息
                shipper_taxnotype = Ext.shipper_taxnotype.ToString();
                shipper_taxno = Ext.shipper_taxno;
                shipper_taxnocountry = "CN";
            //客户信息
               customer_id = userAccountInfo.customer_id;
            customer_userid = userAccountInfo.customer_userid;
            weight = Helpers.RoundData(Ext.weight / 1000, 2);
            //商品信息
            List<AddOrderRequest.Cls_orderInvoiceParam> orderInvoiceParam = new List<AddOrderRequest.Cls_orderInvoiceParam>();
            int num = orderInfo.GoodInfos.Sum((r) => r.Num);
            foreach (var v in orderInfo.GoodInfos)
            {
                AddOrderRequest.Cls_orderInvoiceParam Order = new AddOrderRequest.Cls_orderInvoiceParam();
                Order.invoice_amount =Helpers.RoundData(Ext.invoice_amount/num);//Ext
                Order.invoice_currency = Ext.Currency;
                Order.invoice_pcs = Ext.BoxCount;
                Order.invoice_title = Ext.ProNameEn;
                Order.invoice_weight = Helpers.RoundData(weight / num);
                Order.item_id = "";
                Order.item_transactionid = "";
                Order.sku = Ext.ProNameCn;
                Order.sku_code = v.Asin??"";
                orderInvoiceParam.Add(Order);
            }
            this.orderInvoiceParam = orderInvoiceParam??new();
           
           order_customerinvoicecode = Helpers.GetOrderNo(orderInfo.OrderNo, orderInfo.SendNum, orderInfo.FailNum); //orderInfo.OrderNo;

            //ONum.Substring(ONum.Length - 10, 10) + "-" + Mod_GlobalFunction.GetTimeStamp(Mod_GlobalFunction.TimestampMode.UtcSecond).ToString();
            //ExtParameter.order_customerinvoicecode;
            product_id = Ext.ShipType;
           
           product_imagepath = "";// iOrderInfo.url;
        }


        public override string ContentType => "application/x-www-form-urlencoded";

        public override string Url => $"http://{DoMain}:8082/createOrderApi.htm";       

        public override string ToPayload()
        {
            return "param="+base.ToPayload();
        }

        [JsonIgnore]
        public int Id { get; set; }

        [JsonIgnore]
        public string orderNo { get; set; }
        public string buyerid { get; set; }
        /// <summary>
        /// 收件地址街道，必填
        /// </summary>
        public string consignee_address { get; set; }
        public string consignee_city { get; set; }
        public string consignee_mobile { get; set; }
        /// <summary>
        /// 收件人,必填
        /// </summary>
        public string consignee_name { get; set; }
        /// <summary>
        /// ZYXT	客户自用系统/其他不在列表中的均使用该代码
        /// </summary>
        public string trade_type => "ZYXT";
        /// <summary>
        /// 邮编，有邮编的国家必填
        /// </summary>
        public string consignee_postcode { get; set; }
        public string consignee_state { get; set; }
        /// <summary>
        /// 收件电话，必填
        /// </summary>
        public string consignee_telephone { get; set; }
        /// <summary>
        /// 收件国家简码，必填
        /// </summary>
        public string country { get; set; } = string.Empty;
        public string shipper_taxnotype { get; set; } = string.Empty;
        public string shipper_taxno { get; set; } = string.Empty;
        public string shipper_taxnocountry { get; set; } = string.Empty;
        /// <summary>
        /// 客户 ID，必填
        /// </summary>
        public string customer_id { get; set; } = string.Empty;
        /// <summary>
        /// 登录人ID，必填
        /// </summary>
        public string customer_userid { get; set; } = string.Empty;
        /// <summary>
        /// 商品信息
        /// </summary>
        public List<Cls_orderInvoiceParam> orderInvoiceParam { get; set; } = new();
        public class Cls_orderInvoiceParam
        {
            /// <summary>
            /// 申报价值，必填"
            /// </summary>
            public double invoice_amount { get; set; }
            /// <summary>
            /// 件数，必填
            /// </summary>
            public int invoice_pcs { get; set; }
            /// <summary>
            /// 品名，必填
            /// </summary>
            public string invoice_title { get; set; } = string.Empty;
            public double invoice_weight { get; set; }

            public string invoice_currency { get; set; }
            public string item_id { get; set; } = string.Empty;
            /// <summary>
            /// 交易ID
            /// </summary>
            public string item_transactionid { get; set; } = string.Empty;
            /// <summary>
            /// 如果是 e 邮宝，e 特快，e包裹则传中文品名invoice_title
            /// </summary>
            public string sku { get; set; } = string.Empty;
            /// <summary>
            /// 配货信息
            /// </summary>
            public string sku_code { get; set; } = string.Empty;
            public string hs_code { get; set; } = string.Empty;
        }
        /// <summary>
        /// 原单号，必填
        /// </summary>
        public string order_customerinvoicecode { get; set; } = string.Empty;
        /// <summary>
        /// 运输方式 ID，必填
        /// </summary>
        public int product_id { get; set; }
        /// <summary>
        /// 总重，选填，如果 sku 上有单重可不填该项
        /// </summary>
        public double weight { get; set; }
        /// <summary>
        /// 图片地址，多图片地址用分号隔开
        /// </summary>
        public string product_imagepath { get; set; } = string.Empty;
    }
}
