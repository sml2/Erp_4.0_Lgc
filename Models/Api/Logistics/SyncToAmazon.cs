﻿namespace ERP.Models.Api.Logistics
{
    public class SyncToAmazon
    {
        public bool IsSync { get; set; }
        public string CarrierName { get; set; } = "Other";
        public string ShippingMethod { get; set; } = "Standard";
    }
}
