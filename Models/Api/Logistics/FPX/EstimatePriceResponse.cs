﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.FPX
{
    public class EstimatePriceResponse
    {
        public string result { get; set; } = string.Empty;
        public string msg { get; set; } = string.Empty;
        public List<ErrorsItem> errors { get; set; } = new();
        public class ErrorsItem
        {
            public string error_code { get; set; } = string.Empty;
            public string error_msg { get; set; } = string.Empty;
        }
        public List<DataItem> data { get; set; } = new();
        public class DataItem
        {
            public string _4px_tracking_no { get; set; } = string.Empty;
            public string ref_no { get; set; } = string.Empty;
            public string logistics_channel_no { get; set; } = string.Empty;
            public string logistics_product_code { get; set; } = string.Empty;
            public double lump_sum_fee { get; set; } 
            public string is_volume_cargo { get; set; } = string.Empty;
            public string charge_weight { get; set; } = string.Empty;
            public string estimated_time { get; set; } = string.Empty;
            public string is_show_track { get; set; } = string.Empty;
            public string remarks { get; set; } = string.Empty;
            public override string ToString()
            {
                return $"总费用(CNY){lump_sum_fee}";
            }
        }
    }
}
