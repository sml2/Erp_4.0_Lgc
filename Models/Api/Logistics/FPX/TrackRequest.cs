﻿using Newtonsoft.Json;

namespace ERP.Models.Api.Logistics.FPX
{
    public class TrackRequest :CommonParameter
    {
        public TrackRequest(Parameter p,string trackNumber) : base(p,MethodType.Track)
        {
            deliveryOrderNo = trackNumber;
        }

        public string deliveryOrderNo { get; set; }

        [JsonIgnore]
        public override string Url => $"{base.HostUrl}?method={base.methodType.GetDescription()}&app_key={base.AppKey}&v={version}&timestamp={TimeStamp}&format=json&sign={base.GetSign(this.ToPayload())}";

    }
}
