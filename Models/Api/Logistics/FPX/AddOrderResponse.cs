﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.FPX
{
    public class AddOrderResponse
    {
        public Data data { get; set; } = new();
        public class Data
        {
            /// <summary>
            /// 直发委托单号
            /// </summary>
            public string ds_consignment_no { get; set; } = string.Empty;
            /// <summary>
            /// 4PX跟踪号
            /// </summary>
            public string _4px_tracking_no { get; set; } = string.Empty;
            /// <summary>
            /// 标签条码号
            /// </summary>
            public string label_barcode { get; set; } = string.Empty;
            /// <summary>
            /// 服务商名称
            /// </summary>
            public string logistics_channel_name { get; set; } = string.Empty;
            /// <summary>
            /// 客户单号/客户参考号
            /// </summary>
            public string ref_no { get; set; } = string.Empty;
            /// <summary>
            /// 物流渠道号码
            /// </summary>
            public string logistics_channel_no { get; set; } = string.Empty;
        }
        public string msg { get; set; } = string.Empty;
        public string result { get; set; } = string.Empty;
        public List<ErrorsItem> errors { get; set; } = new();
        public class ErrorsItem
        {
            public string error_code { get; set; } = string.Empty;
            public string error_msg { get; set; } = string.Empty;
        }      
    }
}
