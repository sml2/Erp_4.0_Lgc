﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ERP.Models.Api.Logistics.FPX
{
    public class ShipTypeResponse
    {
        public List<DataItem> data { get; set; } = new();
        public class DataItem
        {
            public string billing_volume_weight { get; set; } = string.Empty;
            public string logistics_product_code { get; set; } = string.Empty;
            public string logistics_product_name_cn { get; set; } = string.Empty;
            public string logistics_product_name_en { get; set; } = string.Empty;
            public string order_track { get; set; } = string.Empty;
            public string surface_mail { get; set; } = string.Empty;
            public override string ToString()
            {
                return $"{logistics_product_name_cn}[{logistics_product_code}]";
            }
        }
        public string msg { get; set; } = string.Empty;
        public string result { get; set; } = string.Empty;
        public List<ErrorsItem> errors { get; set; } = new();
        public class ErrorsItem
        {
            public string error_code { get; set; } = string.Empty;
            public string error_msg { get; set; } = string.Empty;
        }     
    }
    public class FPX_ShipTypes //: SqlObject<FPX_ShipTypes>
    {
        public FPX_ShipTypes() { }
        //static FPX_ShipTypes()
        //{
        //    string ConfigPath = System.IO.Path.Combine(Sy.Environment.AppPath, "Config", "FPX_ShipTypes.Dat");
        //    Abs_DB DB = new SQLite(CreateSQLBuilder<FPX_ShipTypes>.CreateSQL, ConfigPath);
        //    DB.Execute(GetCreateSQL());
        //    AddDB(DB);
        //}
        public string ShipMethodCode { get; set; } = string.Empty;

        public string ShipMethodNameCN { get; set; } = string.Empty;
        public override string ToString()
        {
            return $"{ShipMethodNameCN}[{ShipMethodCode}]";
        }
    }
}
