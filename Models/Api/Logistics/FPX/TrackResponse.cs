﻿using ERP.Enums.Logistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.FPX
{
    public class TrackResponse
    {
        public List<ErrorsItem> errors { get; set; } = new();
        public class ErrorsItem
        {
            public string error_code { get; set; } = string.Empty;
            public string error_msg { get; set; } = string.Empty;
        }
        public string result { get; set; } = string.Empty;
        public string msg { get; set; } = string.Empty;
        public Data data { get; set; } = null!;
        public class Data
        {
            public string deliveryOrderNo { get; set; } = string.Empty;
            public List<TrackingListItem> trackingList { get; set; } = new();
            public class TrackingListItem
            {
                public string businessLinkCode { get; set; } = string.Empty;
                public string occurDatetime { get; set; } = string.Empty;
                public string occurLocation { get; set; } = string.Empty;
                public string trackingContent { get; set; } = string.Empty;
            }
        }      
    }
}
