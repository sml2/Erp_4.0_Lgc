﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.FPX
{
    public class WareHouseResponse
    {
        public string result { get; set; } = string.Empty;
        public string msg { get; set; } = string.Empty;
        public List<ErrorsItem> errors { get; set; } = new();
        public class ErrorsItem
        {
            public string error_code { get; set; } = string.Empty;
            public string error_msg { get; set; } = string.Empty;
        }
        public List<DataItem> data { get; set; } = new();
        public class DataItem
        {
            public string warehouse_code { get; set; } = string.Empty;
            public string warehouse_name_cn { get; set; } = string.Empty;
            public string warehouse_name_en { get; set; } = string.Empty;
            public string country { get; set; } = string.Empty;
            public string service_code { get; set; } = string.Empty;
            public override string ToString()
            {
                return $"{warehouse_name_cn}[{warehouse_code}]";
            }
        }
    }
    public class WareHouseRequest
    {
        public string service_code { get; set; } = string.Empty;
    }
}
