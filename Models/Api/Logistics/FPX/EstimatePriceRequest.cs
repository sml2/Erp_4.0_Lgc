﻿namespace ERP.Models.Api.Logistics.FPX
{
    public class EstimatePriceRequest
    {
        public string request_no => "";
        public string country_code { get; set; } = string.Empty;
        public string weight { get; set; } = string.Empty;
        public string length { get; set; } = string.Empty;
        public string width { get; set; } = string.Empty;
        public string height { get; set; } = string.Empty;
        public string cargocode => "P";
        public List<string> logistics_product_code { get; set; } = new();
    }
}
