﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static EnumExtension;

namespace ERP.Models.Api.Logistics.FPX
{
    public enum BillTypeEnum
    {
        [Description("无")]
        _0 =0,
        [Description("上门提货费")]
        _100 = 100,
        [Description("散装卸货费")]
        _101 = 101,
        [Description("放空费")]
        _104 = 104,
        [Description("压夜费")]
        _103 = 103,
        [Description("超时费")]
        _102 = 102,
        [Description("SKU查验费")]
        _105 = 105,
        [Description("添加附件费")]
        _109 = 109,
        [Description("拍照费")]
        _108 = 108,
        [Description("更换包装费")]
        _107 = 107,
        [Description("更换条码费")]
        _106 = 106,
        [Description("运费")]
        _110 = 110,
        [Description("超长费")]
        _113 = 113,
        [Description("燃油费")]
        _112 = 112,
        [Description("保险费")]
        _111 = 111,
        [Description("仓储头程杂费")]
        _181 = 181,
        [Description("电池费")]
        _180 = 180,
        [Description("过港费")]
        _117 = 117,
        [Description("排仓费")]
        _116 = 116,
        [Description("安检费")]
        _115 = 115,
        [Description("超重费")]
        _114 = 114,
        [Description("商检费")]
        _120 = 120,
        [Description("查验费")]
        _119 = 119,
        [Description("报关费")]
        _118 = 118,
        [Description("无退税报关")]
        _158 = 158,
        [Description("清关使用费")]
        _157 = 157,
        [Description("提货费")]
        _186 = 186,
        [Description("清关费")]
        _185 = 185,
        [Description("续页费")]
        _121 = 121,
        [Description("关税杂费")]
        _124 = 124,
        [Description("VAT税")]
        _123 = 123,
        [Description("关税")]
        _122 = 122,
        [Description("单独退税自VAT")]
        _161 = 161,
        [Description("非出口退税")]
        _160 = 160,
        [Description("出口退税")]
        _159 = 159,
        [Description("税号使用费")]
        _126 = 126,
        [Description("关税代垫费")]
        _125 = 125,
        [Description("DDP手续费")]
        _162 = 162,
        [Description("卸货费")]
        _127 = 127,
        [Description("劳力费")]
        _271 = 271,
        [Description("仓租费")]
        _128 = 128,
        [Description("库位仓租")]
        _220 = 220,
        [Description("季度仓租")]
        _273 = 273,
        [Description("下架+订单操作费")]
        _129 = 129,
        [Description("出库自提费")]
        _270 = 270,
        [Description("出库操作费")]
        _269 = 269,
        [Description("出库订单费")]
        _268 = 268,
        [Description("卡板费")]
        _275 = 275,
        [Description("缠膜费")]
        _274 = 274,
        [Description("库存盘点费")]
        _134 = 134,
        [Description("托盘费")]
        _133 = 133,
        [Description("销毁费")]
        _165 = 165,
        [Description("添加附件费")]
        _132 = 132,
        [Description("分箱费")]
        _164 = 164,
        [Description("打印条码费")]
        _227 = 227,
        [Description("拍照费")]
        _131 = 131,
        [Description("更换条码费")]
        _163 = 163,
        [Description("合箱费")]
        _226 = 226,
        [Description("管理费")]
        _267 = 267,
        [Description("更换包装费")]
        _130 = 130,
        [Description("超重运费")]
        _272 = 272,
        [Description("运费")]
        _135 = 135,
        [Description("速递运费基础费")]
        _242 = 242,
        [Description("超重费")]
        _137 = 137,
        [Description("非规则货物费")]
        _145 = 145,
        [Description("仓储头程杂费")]
        _153 = 153,
        [Description("排仓费")]
        _249 = 249,
        [Description("国内段运费")]
        _264 = 264,
        [Description("超长费")]
        _136 = 136,
        [Description("卷状超重费")]
        _144 = 144,
        [Description("到付手续费")]
        _152 = 152,
        [Description("报关费")]
        _178 = 178,
        [Description("FBA服务费")]
        _248 = 248,
        [Description("卷状超长费")]
        _143 = 143,
        [Description("过港费")]
        _151 = 151,
        [Description("安检费")]
        _247 = 247,
        [Description("高速公路费")]
        _254 = 254,
        [Description("重派费")]
        _142 = 142,
        [Description("限运目的地费")]
        _150 = 150,
        [Description("提货费")]
        _188 = 188,
        [Description("偏远附加费")]
        _221 = 221,
        [Description("安全附加费")]
        _229 = 229,
        [Description("超值费")]
        _246 = 246,
        [Description("自提服务费")]
        _253 = 253,
        [Description("BFPO地址费")]
        _141 = 141,
        [Description("高风险地区费")]
        _149 = 149,
        [Description("电池费")]
        _187 = 187,
        [Description("报关费")]
        _245 = 245,
        [Description("关税手续费")]
        _252 = 252,
        [Description("特殊操作费")]
        _140 = 140,
        [Description("超尺寸附加费")]
        _148 = 148,
        [Description("清关费")]
        _244 = 244,
        [Description("渠道保险费")]
        _251 = 251,
        [Description("地址更改费")]
        _139 = 139,
        [Description("托盘费")]
        _147 = 147,
        [Description("超长超重燃油费")]
        _155 = 155,
        [Description("税务管理费")]
        _243 = 243,
        [Description("目测年龄费")]
        _279 = 279,
        [Description("偏远费")]
        _138 = 138,
        [Description("燃油费")]
        _146 = 146,
        [Description("超长超重附加费")]
        _154 = 154,
        [Description("FBA超重标签费")]
        _250 = 250,
        [Description("退货操作费")]
        _168 = 168,
        [Description("退货操作附加费按重量")]
        _167 = 167,
        [Description("退货操作附加费按件")]
        _166 = 166,
        [Description("入仓费")]
        _169 = 169,
        [Description("仓储订单操作费")]
        _233 = 233,
        [Description("验货费")]
        _277 = 277,
        [Description("仓储操作手续费")]
        _232 = 232,
        [Description("单品扫描费")]
        _276 = 276,
        [Description("包装费")]
        _176 = 176,
        [Description("仓储入库费按件")]
        _230 = 230,
        [Description("仓储非自带包装操作费")]
        _175 = 175,
        [Description("仓储入库费按票")]
        _174 = 174,
        [Description("仓储自带包装操作费")]
        _172 = 172,
        [Description("仓储操作附加费按重量")]
        _171 = 171,
        [Description("仓储操作附加费按件")]
        _170 = 170,
        [Description("高保-保险")]
        _183 = 183,
        [Description("低保-保险")]
        _182 = 182,
        [Description("6‰保险")]
        _184 = 184,
        [Description("申报值20%关税")]
        _256 = 256,
        [Description("包装物料费")]
        _266 = 266,
        [Description("上门拖车费")]
        _278 = 278,
        [Description("高保-保险")]
        _191 = 191,
        [Description("清关使用费")]
        _224 = 224,
        [Description("申报值5%关税")]
        _255 = 255,
        [Description("低保-保险")]
        _190 = 190,
        [Description("FBA箱唛标签费")]
        _223 = 223,
        [Description("操作手续费")]
        _189 = 189,
        [Description("FBASKU条码费")]
        _222 = 222,
        [Description("低值固定险(8)")]
        _261 = 261,
        [Description("低值固定险(5)")]
        _260 = 260,
        [Description("保险费")]
        _173 = 173,
        [Description("签名服务费")]
        _228 = 228,
        [Description("高值非电子险(0.6%保费)")]
        _259 = 259,
        [Description("挂号费")]
        _156 = 156,
        [Description("平邮保价")]
        _194 = 194,
        [Description("报关手续费")]
        _219 = 219,
        [Description("申报值30%关税")]
        _258 = 258,
        [Description("挂号-快递保价")]
        _193 = 193,
        [Description("DDP手续费")]
        _218 = 218,
        [Description("申报值25%关税")]
        _257 = 257,
        [Description("6‰保险")]
        _192 = 192,
        [Description("单独报关费")]
        _225 = 225,
        [Description("订单操作费")]
        _201 = 201,
        [Description("拆包费")]
        _205 = 205,
        [Description("质检费")]
        _204 = 204,
        [Description("大货服务费")]
        _203 = 203,
        [Description("非销售出库费")]
        _202 = 202,
        [Description("包装物料费")]
        _206 = 206,
        [Description("清关服务费")]
        _207 = 207,
        [Description("自提费")]
        _209 = 209,
        [Description("正向配送费")]
        _208 = 208,
        [Description("干线调拨费")]
        _210 = 210,
        [Description("运费")]
        _211 = 211,
        [Description("多行报关费")]
        _214 = 214,
        [Description("报关手续费")]
        _213 = 213,
        [Description("关税")]
        _212 = 212,
        [Description("6‰保险")]
        _215 = 215,
        [Description("挂号-快递保价")]
        _216 = 216,
        [Description("运费")]
        _234 = 234,
        [Description("仓租")]
        _240 = 240,
        [Description("货物清点费")]
        _239 = 239,
        [Description("拍照费")]
        _238 = 238,
        [Description("保险")]
        _237 = 237,
        [Description("合箱费")]
        _265 = 265,
        [Description("保价")]
        _236 = 236,
        [Description("加固费")]
        _235 = 235,
        [Description("分箱费")]
        _241 = 241,
        [Description("退件服务费")]
        _280 = 280,
        [Description("库内操作费")]
        _281 = 281,
        [Description("会员费")]
        MS,
        [Description("仓储关税")]
        R8,
        [Description("关税手续费")]
        H8,
        [Description("关税")]
        P8,
        [Description("增值税")]
        P6,
        [Description("速递运费")]
        E1,
        [Description("运费")]
        FG1,
        [Description("挂号费")]
        REGISTER,
        [Description("运费")]
        FREIGHT,
        [Description("保险费")]
        E4,
        [Description("物料费")]
        MT1,
        [Description("燃油附加费")]
        H5,
        [Description("仓储操作附加费按件数")]
        T8,
        [Description("仓储操作附加费按重量")]
        T7,
        [Description("仓储操作手续费")]
        T4,
        [Description("箱唛费")]
        PG8,
        [Description("视频费")]
        PG7,
        [Description("FB4退货订单费")]
        PG14,
        [Description("拍照费")]
        PG6,
        [Description("更换包装费")]
        PG5,
        [Description("标签费")]
        PG4,
        [Description("分箱费")]
        PG12,
        [Description("上架费")]
        PG13,
        [Description("清点费")]
        PG3,
        [Description("合箱费")]
        PG11,
        [Description("退货操作费")]
        PG2,
        [Description("销毁费")]
        PG10,
        [Description("退货订单费")]
        PG1,
        [Description("转FB4费用")]
        PG9,
        [Description("入闸费")]
        B4,
        [Description("包装费")]
        A7,
        [Description("补报关费")]
        L1,
        [Description("垫付手续费")]
        M2,
        [Description("停车费")]
        P1,
        [Description("销毁费")]
        R1,
        [Description("电池费")]
        T1,
        [Description("超长超重附加费")]
        P2,
        [Description("仓储零错误入库费")]
        X1,
        [Description("国内段运费")]
        Y2,
        [Description("CGC")]
        C5,
        [Description("清关费")]
        D4,
        [Description("到付退款")]
        E7,
        [Description("退预收关税")]
        A3,
        [Description("垫材费")]
        B3,
        [Description("操作手续费")]
        A6,
        [Description("住宅登记费")]
        H9,
        [Description("DDP手续费")]
        L8,
        [Description("入仓费")]
        M9,
        [Description("检疫费")]
        P9,
        [Description("偏远地区附加费")]
        M1,
        [Description("仓储取消订单重新上架费")]
        T9,
        [Description("扫描费")]
        B8,
        [Description("代做CO")]
        C4,
        [Description("代付关税费")]
        D3,
        [Description("仓储退仓费")]
        E5,
        [Description("超值费")]
        A1,
        [Description("速递运费类")]
        H1,
        [Description("报关费")]
        A5,
        [Description("扣件费")]
        L7,
        [Description("退偏远费")]
        M8,
        [Description("物料费")]
        R7,
        [Description("超长/超重派送费")]
        L9,
        [Description("产地证")]
        D6,
        [Description("到付手续费")]
        C3,
        [Description("派送费")]
        D2,
        [Description("续验费")]
        E6,
        [Description("超过最大限制附加费")]
        N2,
        [Description("转口证")]
        B1,
        [Description("航空运费")]
        A4,
        [Description("杂费")]
        H7,
        [Description("退件费")]
        L6,
        [Description("运费赔偿")]
        M7,
        [Description("特殊报关费")]
        P7,
        [Description("地址更改费")]
        R6,
        [Description("偏远燃油附加费")]
        H6,
        [Description("过港费")]
        B7,
        [Description("入仓登记费")]
        D1,
        [Description("A区>5中港运费")]
        E8,
        [Description("其他费用")]
        AB,
        [Description("香港EMS泡货附加费")]
        B9,
        [Description("到付运费")]
        H3,
        [Description("取件费")]
        L5,
        [Description("B区<=5中港运费")]
        M6,
        [Description("罚款费")]
        R5,
        [Description("打印机租金")]
        X5,
        [Description("商检费")]
        D8,
        [Description("提单费")]
        C1,
        [Description("代垫快递费用")]
        C9,
        [Description("打单费")]
        X7,
        [Description("客户税号支付")]
        BC,
        [Description("超长超重燃油附加费")]
        N1,
        [Description("附加运费")]
        H2,
        [Description("陆运费")]
        H4,
        [Description("转派费")]
        L4,
        [Description("货物赔偿")]
        M5,
        [Description("代理报关费")]
        P5,
        [Description("特殊处理费")]
        R4,
        [Description("仓储头程杂费")]
        X4,
        [Description("消费税")]
        X6,
        [Description("帐户使用费")]
        K1,
        [Description("报关扣仓费")]
        C8,
        [Description("名义成本运费")]
        X8,
        [Description("预收关税")]
        BB,
        [Description("泊车费")]
        B6,
        [Description("改单费")]
        A9,
        [Description("重派费")]
        L3,
        [Description("A区<=5中港运费")]
        M4,
        [Description("安全附加费")]
        P4,
        [Description("办证费")]
        R3,
        [Description("调整费用")]
        T3,
        [Description("仓储保港运输费")]
        X3,
        [Description("帐户燃油附加费")]
        K2,
        [Description("商检换证费")]
        C7,
        [Description("安检费")]
        D7,
        [Description("FBA服务费")]
        BA,
        [Description("反恐信息费")]
        B5,
        [Description("提货费")]
        A8,
        [Description("录单操作费")]
        L2,
        [Description("B区>5中港运费")]
        M3,
        [Description("退件附加费")]
        P3,
        [Description("进口费")]
        R2,
        [Description("折扣")]
        T2,
        [Description("仓储手续费")]
        R9,
        [Description("仓储退件重新上架")]
        X2,
        [Description("保证金")]
        Y1,
        [Description("续页费")]
        C6,
        [Description("叉车费")]
        D5,
        [Description("单独报关费")]
        E3,
        [Description("超重附加费")]
        AW2,
        [Description("燃油附加费")]
        AFF,
        [Description("杂费")]
        TH1,
        [Description("超重附加费")]
        AW1,
        [Description("派送特殊操作费")]
        SP1,
        [Description("高速公路费")]
        HW,
        [Description("超长附加费")]
        AL3,
        [Description("住宅派送费")]
        HD1,
        [Description("保价")]
        TD2,
        [Description("超长附加费")]
        AL2,
        [Description("重派费")]
        SD1,
        [Description("保险")]
        TD1,
        [Description("超长附加费")]
        AL1,
        [Description("地址修改费")]
        UA1,
        [Description("超长超重附加费")]
        ALW,
        [Description("偏远附加费")]
        AF3,
        [Description("地址错误退件费")]
        UA2,
        [Description("偏远附加费")]
        AF2,
        [Description("偏远附加费")]
        AF1,
        [Description("操作费")]
        OP1,
        [Description("杂费")]
        TH2,
        [Description("无主件认领费")]
        TW4,
        [Description("无预报处理费")]
        TW2,
        [Description("仓租")]
        TW1,
        [Description("维修服务费")]
        TH4,
        [Description("维修检测费")]
        TH3,
        [Description("排仓费")]
        A2,
        [Description("挂号费")]
        E2,
        [Description("套餐费")]
        BS,
        [Description("签名服务费")]
        SIGNATURE,
        [Description("仓储费")]
        C2,
        [Description("仓储仓租费按天")]
        T6,
        [Description("仓储仓租费按月")]
        T5,
    }
}
