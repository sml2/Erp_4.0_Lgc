﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.FPX
{
    public class ShipTypeRequest : CommonParameter
    {
        public ShipTypeRequest(Parameter p) : base(p, MethodType.Shiptype)
        {

        }

        [JsonProperty("transport_mode")]
        public string TransportMode = "1";

        [JsonIgnore]
        public override string Url => $"{base.HostUrl}?method={base.methodType.GetDescription()}&app_key={base.AppKey}&v={version}&timestamp={TimeStamp}&format=json&sign={base.GetSign(this.ToPayload())}";

    }
}
