﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static EnumExtension;

namespace ERP.Models.Api.Logistics.FPX
{
    public class ExtParameter: BaseExtParam
    {
        public ExtParameter(string product_name_Cn, string product_name_En, string ioss_no, string hscode_export,
            double weight, double length, double width, double height, 
            string include_battery, string shipMethod, double parcel_value, string currency)
        {
            if (Enum.IsDefined(typeof(battery), include_battery) && Enum.TryParse<battery>(include_battery, out var labelinclude_battery))
            {
                this.IncludeBattery = labelinclude_battery;
            }
            else
            {
                throw new AggregateException($"FPX include_battery:{include_battery} error");
            }

            this.Currency = currency;

            //if (Enum.IsDefined(typeof(enumCurrency), currency) && Enum.TryParse<enumCurrency>(currency, out var labelcurrency))
            //{
            //    Currency = labelcurrency;
            //}
            //else
            //{
            //    throw new AggregateException($"FPX currency:{currency} error");
            //}
            this.ProductNameCn = product_name_Cn;
            this.ProductNameEn = product_name_En;
            this.IossNo = ioss_no;
            this.HscodeExport = hscode_export;
            this.Weight = weight;
            this.Length = length;
            this.Width = width;
            this.Height = height;
            ShipMethod = shipMethod;
            this.ParcelValue = parcel_value;
            
        }

      
        /// <summary>
        /// 货运方式
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货运方式")]
        public string ShipMethod { get; set; }

        /// <summary>
        /// 商品申报名(中文)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("商品申报名(中文)")]
        [JsonProperty("product_name_Cn")]
        public string ProductNameCn { get; set; }

        /// <summary>
        /// 商品申报名(英文)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("商品申报名(英文)")]
        [JsonProperty("product_name_En")]
        public string ProductNameEn { get; set; }

        /// <summary>
        /// 包裹重量(g)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹重量(g)")]
        [JsonProperty("weight")]
        public double Weight { get; set; }

        /// <summary>
        /// 包裹长度(cm)
        /// </summary>    
        [DisplayName("包裹长度(cm)")]
        [JsonProperty("length")]
        public double Length { get; set; }

        /// <summary>
        /// 包裹宽度(cm)
        /// </summary>     
        [DisplayName("包裹宽度(cm)")]
        [JsonProperty("width")]
        public double Width { get; set; }

        /// <summary>
        /// 包裹高度(cm)
        /// </summary>        
        [DisplayName("包裹高度(cm)")]
        [JsonProperty("height")]
        public double Height { get; set; }

        /// <summary>
        /// 申报总价格，最多保留4位小数
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("申报总价格")]
        [JsonProperty("parcel_value")]
        public double ParcelValue { get; set; }

        /// <summary>
        /// 申报币种
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("申报币种")]
        public string Currency { get; set; }


        /// <summary>
        /// IOSS税号
        /// </summary>
        [DisplayName("IOSS税号")]
        [JsonProperty("ioss_no")]
        public string IossNo { get; set; }


        /// <summary>
        /// 海关编码
        /// </summary>
        [DisplayName("海关编码")]
        [JsonProperty("hscode_export")]
        public string HscodeExport { get; set; }
               
        /// <summary>
        /// 是否包含电池
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("是否包含电池")]
        [JsonProperty("include_battery")]
        public battery IncludeBattery { get; set; }

    
    }
    public enum battery
    {
        [EnumExtension.Description("不含电池")]
        No=0,
        [EnumExtension.Description("含电池,为内置电池")]
        Yes_966,
        [EnumExtension.Description("含电池,为配套电池")]
        Yes_967,
    }


    public enum enumCurrency
    {
        [EnumExtension.Description("美元(USD)")]
        USD,
        [EnumExtension.Description("欧元(EUR)")]
        EUR,
        [EnumExtension.Description("英镑(GBP)")]
        GBP,
        [EnumExtension.Description("人民币(CNY)")]
        CNY,
        [EnumExtension.Description("澳元(AUD)")]
        AUD,
        [EnumExtension.Description("加元(CAD)")]
        CAD,        
    }
}
