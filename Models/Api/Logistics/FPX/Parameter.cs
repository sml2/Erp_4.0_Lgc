﻿using ERP.Attributes.Logistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.FPX
{
    public class Parameter: BaseParam
    {
        private string key;
        private string secret;
        [ViewConfigParameter(nameof(AppKey), Sort = 0)]
        public string AppKey { get { return key; } set { key = value.Trim(); } }
        [ViewConfigParameter(nameof(AppSecret), Message = "请填写密码", Sort = 1)]
        public string AppSecret { get { return secret; } set { secret = value.Trim(); } }
    }
}
