﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.FPX
{
    public class ProductCodeResponse
    {
        public string id { get; set; } = string.Empty;
        public string value0 { get; set; } = string.Empty;
        public string value1 { get; set; } = string.Empty;
    }
}
