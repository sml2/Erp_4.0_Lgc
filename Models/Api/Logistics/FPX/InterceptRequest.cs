﻿using Newtonsoft.Json;

namespace ERP.Models.Api.Logistics.FPX
{
    public class InterceptRequest: CommonParameter
    {
        public InterceptRequest(Parameter p, string CustomNum) : base(p,MethodType.Cancle)
        {
            request_no = CustomNum;            
        }
        public string request_no { get; set; }

        /// <summary>
        /// 拦截指令（可选值，拦截：Y；取消拦截：N）
        /// </summary>
        public string is_hold { get; set; } = "Y";

        public string hold_reason { get; set; } = "取消订单";


        [JsonIgnore]
        public override string Url => $"{base.HostUrl}?method={base.methodType.GetDescription()}&app_key={base.AppKey}&v={version}&timestamp={TimeStamp}&format=json&sign={base.GetSign(this.ToPayload())}";

    }

    public class InterceptResponse
    {
        public List<ErrorsItem> errors { get; set; } = new();
        public class ErrorsItem
        {
            public string error_code { get; set; } = string.Empty;
            public string error_msg { get; set; } = string.Empty;
        }
        public string result { get; set; } = string.Empty;
        public string msg { get; set; } = string.Empty;
        public Data? data { get; set; } = null!;
        public class Data
        {
           
        }
    }
}
