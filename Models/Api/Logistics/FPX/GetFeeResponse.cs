﻿namespace ERP.Models.Api.Logistics.FPX
{
    public class GetFeeResponse
    {
        public List<ErrorsItem> errors { get; set; } = new();
        public class ErrorsItem
        {
            public string error_code { get; set; } = string.Empty;
            public string error_msg { get; set; } = string.Empty;
        }
        public string result { get; set; }=string.Empty;

        public string msg { get; set; } = string.Empty;

        public Data? data { get; set; }
        public class Data
        {
            public List<BillinglistItem> billinglist { get; set; } = new();
            public class BillinglistItem
            {
                public string order_no { get; set; } = string.Empty;
                /// <summary>
                /// 费用项
                /// </summary>
                public string billing_type { get; set; } = string.Empty;
                /// <summary>
                /// 扣费类型。如：CA(现金);CR(信用额度)。
                /// </summary>
                public string deduction_type { get; set; } = string.Empty;
                /// <summary>
                /// 计费时间。*注：时间格式：传入时间值需要转换为long类型格式。
                /// </summary>
                public string billing_date { get; set; } = string.Empty;
                /// <summary>
                /// 计费币种。如：USD(美元);CNY(人民币)。如：CNY
                /// </summary>
                public string currency { get; set; } = string.Empty;
                /// <summary>
                /// 计费金额
                /// </summary>
                public string billing_amount { get; set; } = string.Empty;
            }
        }      
    }
}
