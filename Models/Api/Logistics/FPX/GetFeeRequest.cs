﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ERP.Models.Api.Logistics.FPX
{
    public class GetFeeRequest:CommonParameter
    {
        public GetFeeRequest(Parameter p,string CustomNum) : base(p,MethodType.Fee )
        {
           order_no = CustomNum;
            business_type = "I";
        }
        public string order_no { get; set; }
        public string ref_no { get; set; } = string.Empty;
        /// <summary>
        /// 业务类型。可选值：I（入库委托）；O（出库委托）；T（调拨委托）
        /// </summary>
        public string business_type { get; set; }

        [JsonIgnore]
        public override string Url => $"{base.HostUrl}?method={base.methodType.GetDescription()}&app_key={base.AppKey}&v={version}&timestamp={TimeStamp}&format=json&sign={base.GetSign(this.ToPayload())}";

    }
}
