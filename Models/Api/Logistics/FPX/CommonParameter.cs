﻿using Newtonsoft.Json;
using System.Text;

namespace ERP.Models.Api.Logistics.FPX
{
    public abstract class CommonParameter : RequestLogistic
    {
        public CommonParameter(Parameter p, MethodType _methodType)
        {
            methodType = _methodType;
            this.AppKey = p.AppKey;
            this.AppSecret = p.AppSecret;
            TimeStamp = Mod_GlobalFunction.GetTimeStamp(Mod_GlobalFunction.TimestampMode.Millisecond).ToString();
        }

        [JsonIgnore]
        /// <summary>
        /// 1 正式环境，0 测试环境
        /// </summary>
        private int EnviremnetFlag = 1;       

        [JsonIgnore]
        public string HostUrl
        {
            get
            {
                if (EnviremnetFlag == 1)
                {
                    return "https://open.4px.com/router/api/service";
                }
                else
                {                             
                    return "https://open-test.4px.com/router/api/service";
                }
            }
        }

        [JsonIgnore]
        public MethodType methodType { get; set; }

        [JsonIgnore]
        public string AppKey { get; set; }

        [JsonIgnore]
        public string AppSecret { get; set; }

        [JsonIgnore]
        public string TimeStamp { get; set; }

        [JsonIgnore]
        public string version { get; set; } = "1.0.0";

        /// <summary>
        /// app_key format method timestamp v data appSecret
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public string GetSign(string data)
        {
            string text = $"app_key{AppKey}formatjsonmethod{methodType.GetDescription()}timestamp{TimeStamp}v{version}{data}{AppSecret}";
            return Helpers.Md5Lower(text);
        }
    }
}
