﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.FPX
{
    public class PrintLabelResponse
    {
        public Data data { get; set; } = new();
        public class Data
        {
            public string label_barcode { get; set; } = string.Empty;
            public Label_url_info label_url_info { get; set; } = new();
            public class Label_url_info
            {
                public string logistics_label { get; set; } = string.Empty;
                public string custom_label { get; set; } = string.Empty;
                public string package_label { get; set; }=string.Empty;
            }
        }
        public string msg { get; set; } = string.Empty;
        public string result { get; set; } = string.Empty;
        public List<ErrorsItem> errors { get; set; } = new();
        public class ErrorsItem
        {
            public string error_code { get; set; } = string.Empty;
            public string error_msg { get; set; } = string.Empty;
        }
    }
}
