﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.FPX
{
    public enum LabelFileTypeEnum
    {
        [Description("PDF")]
        PDF,
    }
}
