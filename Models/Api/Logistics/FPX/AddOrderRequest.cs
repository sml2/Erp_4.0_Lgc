﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static ERP.Models.Api.Logistics.FPX.AddOrderRequest;

namespace ERP.Models.Api.Logistics.FPX
{
    public class AddOrderRequest : CommonParameter
    {
        public AddOrderRequest(Parameter p, SenderInfo senderInfo, OrderInfo orderInfo, ExtParameter Ext) : base(p, MethodType.Send)
        {
            Id = orderInfo.id;
            orderNo = orderInfo.OrderNo;
            //4px_tracking_no String(64)	否   300482723836    4PX跟踪号（预分配号段的客户可传此值）
            //ref_no String(64)	是 RF760473585SG   参考号（客户自有系统的单号，如客户单号）
            RefNo = Helpers.GetOrderNo(orderInfo.OrderNo, orderInfo.SendNum, orderInfo.FailNum);

            //label_barcode String(64)	否   3000000000001   面单条码（预分配号段的客户可传此值）
            //business_type String(10)	是 BDS 业务类型(4PX内部调度所需，如需对接传值将说明，默认值：BDS。)
            //duty_type String(10)	是 U   税费费用承担方式(可选值：U、P); U：DDU由收件人支付关税; P：DDP 由寄件方支付关税 （如果物流产品只提供其中一种，则以4PX提供的为准）
            //cargo_type String(10)	否   5   货物类型（1：礼品; 2：文件; 3：商品货样; 5：其它；默认值：5）
            //vat_no String(32)	否 SDF2324 VAT税号(数字或字母)；欧盟国家(含英国)使用的增值税号；
            //eori_no String(32)	否 SDF2324 EORI号码(数字或字母)；欧盟入关时需要EORI号码，用于商品货物的清关
            //ioss_no String(32)  否 FR-123456   IOSS号码
            IossNo = Ext.IossNo;
            //buyer_id    String(64)  否   23432432432432sd 买家ID(数字或字母)
            //sales_platform String(128)	否 AMAZON  销售平台（点击查看详情）
            //trade_id String(32)	否 csd2342344  交易号ID(数字或字母)
            //seller_id String(32)	否 csd2342344  卖家ID(数字或字母)
            //is_commercial_invoice String(3)	否 N   能否提供商业发票（Y / N） Y：能提供商业发票(则系统不会生成形式发票)；N：不能提供商业发票(则系统会生成形式发票)； 默认为/N；/DHL产品必填，如产品代码A1 / A5；
            //parcel_qty Number(32)	否   3   包裹件数（一个订单有多少件包裹，就填写多少件数，请如实填写包裹件数，否则DHL无法返回准确的子单号数和子单号标签；DHL产品必填，如产品代//码A1 / A5；）
            //freight_charges Double(32)	否   48.4    运费(客户填写自己估算的运输费用；支持的币种，根据物流产品 + 收件人国家配置)
            //currency_freight String(10)	否 USD 运费币种(按照ISO标准三字码；支持的币种，根据物流产品 + 收件人国家配置)
            //declare_insurance Double(32)	否   23.4    申报保险费（是否必填，根据物流产品 + 目的国配置；根据欧盟IOSS政策，货值 / 运费 / 保险费可单独申报）支持小数点后2位
            //currency_declare_insurance  String(10)  否 USD 申报保险费币种（按照ISO标准，币种需和进出口国申报币种一致）


            ////物流服务信息
            //logistics_service_info Object(10)	是 物流服务信息  物流服务信息            
            //logistics_product_code  String(64)  是 A1  物流产品代码(点击查看详情)
            logisticsServiceInfo.LogisticsProductCode = Ext.ShipMethod;
            //customs_service String(5)	否 N   单独报关（Y：单独报关；N：不单独报关） 默认值：N
            //signature_service   String(1)   否 N   签名服务（Y / N)；默认值：N
            //value_added_services    String(128) 否 无需填写    其他服务（待完善)
            //

            ////退件信息
            //is_return_on_domestic String(3)	是 N   境内 / 国内异常处理策略(Y：退件--实际是否支持退件，以及退件策略、费用，参考报价表；N：销毁；U：其他--等待客户指令) 默认值：N；
            //domestic_return_addr Object(10)	否 国内退件地址  境内 / 国内退件接收地址信息（退件地址非必填；若填写，则姓名 / 电话 / 邮编 / 国家 / 城市 / 详细地址均需填写）
            //is_return_on_oversea String(3)	是 N   境外 / 国外异常处理策略(Y：退件--实际是否支持退件，以及退件策略、费用，参考报价表；N：销毁；U：其他--等待客户指令) 默认值：N；
            //oversea_return_addr Object(10)	否 海外退件地址  境外 / 国外退件接收地址信息（退件地址非必填；若填写，则姓名 / 电话 / 邮编 / 国家 / 城市 / 详细地址均需填写）



            ////包裹参数
            //parcel_list List(10)	是 包裹列表    包裹列表
            //product_list    List(10)    否   投保物品信息[@即将废弃]投保物品信息（投保、查验、货物丢失作为参考依据）

            ParceListItem item = new ParceListItem();
            //weight  Number(32)  是   104 预报重量（g）
            item.Weight = Ext.Weight;
            //length Double(32)	否   10  包裹长（cm）
            item.Length = Ext.Length;
            //width Double(32)	否   10  包裹宽（cm）
            item.Width = Ext.Width;
            //height Double(32)	否   10  包裹高（cm）
            item.Height = Ext.Height;
            //parcel_value Double(32)	是   23.2301 包裹申报价值（最多4位小数）
            item.ParcelValue = Ext.ParcelValue;
            //currency String(10)	是 USD 包裹申报价值币别（按照ISO标准三字码；支持的币种，根据物流产品 + 收件人国家配置；币种需和进出口国申报币种一致）
            item.Currency = Ext.Currency;
            //include_battery String(3)	是 Y   是否含电池（Y / N）
            //battery_type String(32)	否   966 带电类型(具体产品可支持的带电类型请咨询销售）            
            if (Ext.IncludeBattery == battery.Yes_966)
            {
                item.IncludeBattery = "Y";
                item.BatteryType = "966";
            }
            else if (Ext.IncludeBattery == battery.Yes_967)
            {
                item.IncludeBattery = "Y";
                item.BatteryType = "967";
            }
            else
            {
                item.IncludeBattery = "N";
            }


            //declare_product_info    List(10)    是   海关申报信息  海关申报信息(每个包裹的申报信息，方式1：填写申报产品代码和申报数量；方式2：填写其他详细申报信息)                       
            int num = orderInfo.GoodInfos.Sum((r) => r.Num);
            foreach (var v in orderInfo.GoodInfos)
            {
                ////投保物品信息
                //product_list List(10)	否 投保物品信息[@即将废弃]投保物品信息（投保、查验、货物丢失作为参考依据）            
                //sku_code String(64)	否 iPhone5[@即将废弃]投保SKU（客户自定义SKUcode）（数字或字母或空格）
                //standard_product_barcode String(64)	否   692213214242123[@即将废弃]投保商品标准条码（UPC、EAN、JAN…）
                //product_name String(64)	否 苹果手机[@即将废弃]投保商品名称
                //product_description String(128) 否 iPhone5[@即将废弃]投保商品描述
                //product_unit_price  Double(32)  否   23[@即将废弃]投保商品单价（按对应币别的法定单位，最多4位小数点）
                //currency String(10)	否 USD[@即将废弃]投保商品单价币别（按照ISO标准三字码；支持的币种，根据物流产品 + 收件人国家配置；币种需和进出口国申报币种一致）
                //qty Number(32)	否   2[@即将废弃]投保商品数量（单位为pcs）

                ////海关申报信息
                //declare_product_code String(32)	否 A1  申报产品代码（在4PX已备案申报产品的代码）                                                                                                        
                ParceListItem.DeclareProductInfoItem DeItem = new ParceListItem.DeclareProductInfoItem();
                //declare_product_name_cn String(64)	否 手机  申报品名(当地语言)
                DeItem.DeclareProductNameCn = Ext.ProductNameCn;
                //declare_product_name_en String(64)	否 Phone   申报品名（英语）
                DeItem.DeclareProductNameEn = Ext.ProductNameEn;
                //uses String(32)	否 通讯  用途
                //specification   String(32)  否   34×24×12    规格
                //component   String(32)  否 金属、塑料 成分
                //unit_net_weight Number(32)	否   1000    单件商品净重（默认以g为单位）
                //unit_gross_weight Number(32)	否   2300    单件商品毛重（默认以g为单位）
                //material String(64)	否 金属、塑料 材质
                //declare_product_code_qty Number(32)	是   3   申报数量
                DeItem.DeclareProductCodeQty = v.Count;
                //unit_declare_product    String(32)  否 PCS 单位（点击查看详情；默认值：PCS）
                //origin_country String(64)	否 CN  原产地（ISO标准2字码）点击查看详情
                //country_export  String(32)  否 CN  出口国 / 起始国 / 发件人国家（ISO标准2字码）
                //country_import String(32)	否 CN  进口国 / 目的国 / 收件人国家（ISO标准2字码）
                //hscode_export String(32)	否   9004909000  出口国 / 起始国 / 发件人国家_海关编码(只支持数字)
                DeItem.HscodeExport = Ext.HscodeExport;
                //hscode_import String(32)	否   9004909000  进口国 / 目的国 / 收件人国家_海关编码(只支持数字)
                //declare_unit_price_export Double(64)	是   23  出口国 / 起始国 / 发件人国家_申报单价（按对应币别的法定单位，最多4位小数点）
                double Unit_Price = Helpers.RoundData(Convert.ToDouble(Ext.ParcelValue) / num);
                DeItem.DeclareUnitPriceExport = Unit_Price;
                //currency_export String(10)	是 USD 出口国 / 起始国 / 发件人国家_申报单价币种（按照ISO标准；支持的币种，根据物流产品 + 收件人国家配置；币种需和进口国申报币种一致）
                DeItem.CurrencyExport = Ext.Currency;
                //declare_unit_price_import Double(64)	是   23  进口国 / 目的国 / 收件人国家_申报单价（按对应币别的法定单位，最多4位小数点）
                DeItem.DeclareUnitPriceImport = Unit_Price;
                //currency_import String(10)	是 USD 进口国 / 目的国 / 收件人国家_申报单价币种（按照ISO标准；支持的币种，根据物流产品 + 收件人国家配置；币种需和出口国申报币种一致）
                DeItem.CurrencyImport = Ext.Currency;
                //brand_export String(128)	是 none    出口国 / 起始国 / 发件人国家_品牌(必填；若无，填none即可)
                //brand_import String(128)	是 狮子牌 进口国 / 目的国 / 收件人国家_品牌(必填；若无，填none即可)
                //sales_url String(128)	否 https://ju.taobao.com/jusp/other/mingpin/tp.htm?spm=875.7931836/B.2017039.4.5ff54265S5IEz9&pos=2&acm=201708280-2.1003.2.3116407&scm=1003.2.201708280-2.OTHER_1519569914238_3116407	商品销售URL
                //package_remarks String(200)	否 skutest 配货字段（打印标签选择显示配货信息是将会显示：package_remarks* qty）               
                item.DeclareProductInfo.Add(DeItem);
            }
            ParcelList.Add(item);

            IsInsure = "N";
            //insurance_info Object(10)	是 保险信息    保险信息（投保时必须填写）
            //insure_type String(10)	否   5Y 保险类型（XY: 4PX保价；XP: 第三方保险） 5Y, 5元每票 8Y, 8元每票 6P, 0.6 % 保费
            //insure_value Double(32)	否   23  保险价值
            //currency    String(10)  否 USD 保险币别（按照ISO标准，目前只支持USD）
            //insure_person String(64)	否 张三  投保人 / 公司
            //certificate_type String(32)	否 ID  投保人证件类型（暂时只支持身份证，类型为：ID）
            //certificate_no String(64)	否   3.62432199412121e+17    投保人证件号码
            //category_code   String(64)  否 无需填写    保险类目ID（保险的类目，暂时不填，默认取第一个类目）
            //insure_product_name String(400)	否 计算机 投保货物名称
            //package_qty String(800) 否 纸盒包装，1台 / 箱   投保包装及数量



            ////发件人
            //sender Object(10)	是 发件人信息   发件人信息           
            //first_name  String(32)  是 张三  名 / 姓名
            sender.FirstName = senderInfo.Name;
            //last_name String(32)	否 张   姓
            //company String(64)  否 北京市xxx有限公司  公司名
            sender.Company =string.IsNullOrWhiteSpace(senderInfo.Company)? senderInfo.Name : senderInfo.Company;
            //phone   String(32)  否   18166668888 电话（必填）
            sender.Phone = string.IsNullOrWhiteSpace(senderInfo.PhoneNumber)?null: senderInfo.PhoneNumber;
            //phone2 String(32)	否   0755 - 6600888    电话2
            //email   String(32)  否 xxxxxx@4px.com 邮箱
            sender.Email = senderInfo.Mail;
            //post_code String(32)	否   0   邮编
            sender.PostCode = senderInfo.PostalCode;
            //country String(10)  是 CN  国家（国际二字码 标准ISO 3166 - 2 ）
            sender.Country = "CN";
            //state String(64)	否 广东省 州 / 省
            sender.State = string.IsNullOrWhiteSpace(senderInfo.Province)?null: senderInfo.Province;
            //city String(64)	是 深圳市 城市
            sender.City =senderInfo.City;
            //district    String(64)  否 南山区 区、县
            //street  String(64)  否 南山街道南山路南山X座1001 街道 / 详细地址
            sender.Street = string.IsNullOrWhiteSpace(senderInfo.Address)?null: senderInfo.Address;
            //house_number String(32)	否   1001    门牌号
            //certificate_info    Object(10)  否 证件信息    证件信息，根据海关要求变化
            

            ////收件人
            //recipient_info Object(10)	是 收件人信息   收件人信息
            
            //first_name  String(32)  是 zhangsan    名 / 姓名
            recipientInfo.FirstName =string.IsNullOrWhiteSpace(orderInfo.ReceiverAddress.Name)?"name isNull": orderInfo.ReceiverAddress.Name;
            //last_name String(32)	否 zhang   姓
            recipientInfo.Company = recipientInfo.FirstName;
            //company String(64)  否 zhangsan co.,ltd 公司名
            //phone String(32)	是   18166668888 电话（必填）
            recipientInfo.Phone = string.IsNullOrWhiteSpace(orderInfo.ReceiverAddress.Phone)?"none": orderInfo.ReceiverAddress.Phone;
            //phone2 String(32)	否   0755 - 6600888    电话2
            //email   String(32)  否 xxxxxx@4px.com 邮箱
            recipientInfo.Email = string.IsNullOrWhiteSpace(orderInfo.ReceiverAddress.Email)?null: orderInfo.ReceiverAddress.Email;
            //post_code String(32)	否   0   邮编,非必填（部分产品需要填，具体以返回提示为准）
            recipientInfo.PostCode = string.IsNullOrWhiteSpace(orderInfo.ReceiverAddress.Zip)?null: orderInfo.ReceiverAddress.Zip;
            //country String(10)	是 US  国家（国际二字码 标准ISO 3166 - 2 ）
            recipientInfo.Country = orderInfo.ReceiverAddress.NationShort;
            //state String(64)	否 MI  州 / 省
            recipientInfo.State = orderInfo.ReceiverAddress.City;
            //city String(64)	是 CLINTON TOWNSHIP 城市
            recipientInfo.City = orderInfo.ReceiverAddress.City;
            //district String(64)	否 CLINTON TOWNSHIP 区、县（可对应为adress 2）
            recipientInfo.District = string.IsNullOrWhiteSpace(orderInfo.ReceiverAddress.District)?null: orderInfo.ReceiverAddress.District;
            string Address = orderInfo.ReceiverAddress.Address;
            if (orderInfo.ReceiverAddress.Address2.IsNotWhiteSpace()) Address += $" {orderInfo.ReceiverAddress.Address2}";
            if (orderInfo.ReceiverAddress.Address3.IsNotWhiteSpace()) Address += $" {orderInfo.ReceiverAddress.Address3}";
            //street String(64)	是   2A rue Jeanne d'Arc 89 56 district	街道/详细地址（可对应为adress 1）
            recipientInfo.Street =string.IsNullOrWhiteSpace(Address)?"none": Address;
            //house_number String(32)	否   2A 门牌号
            //certificate_info Object(10)	否 证件信息    证件信息，根据海关要求变化
            

            ////到仓方式
            //deliver_type_info Object(10)	是 到仓方式    货物到仓方式信息           
            //deliver_type    String(32)  是   1   到仓方式（1:上门揽收；2:快递到仓；3:自送到仓；5:自送门店）
            deliverTypeInfo.DeliverType = "1";
            //warehouse_code String(32)	否 PL1US1  收货仓库 / 门店代码（仓库代码）
            //pick_up_info Object(10)	否 揽收信息    上门揽收信息
            //express_to_4px_info Object(10)  否 快递到仓信息  快递到仓信息
            //self_send_to_4px_info   Object(10)  否 自己送仓信息  自己送仓信息           

            //投递信息           
            deliverToRecipientInfo.DeliverType = "HOME_DELIVERY";            
        }

        [JsonIgnore]
        public override string Url => $"{base.HostUrl}?method={base.methodType.GetDescription()}&app_key={base.AppKey}&v={version}&timestamp={TimeStamp}&format=json&sign={base.GetSign(this.ToPayload())}";


        [JsonIgnore]
        public string orderNo { get; set; }
        [JsonIgnore]
        public int Id { get; set; }

        /// <summary>
        /// 4px_tracking_no String(64)  否	300482723836	4PX跟踪号（预分配号段的客户可传此值）
        /// </summary>
        [JsonProperty("4px_tracking_no")]
        public string? FpxTrackingNo { get; set; }


        /// <summary>
        /// ref_no String(64)  是   RF760473585SG   参考号（客户自有系统的单号，如客户单号）
        /// </summary>
        [JsonProperty("ref_no")]
        [Required]
        public string RefNo { get; set; }


        /// <summary>
        /// label_barcode String(64)  否	   3000000000001	面单条码（预分配号段的客户可传此值）
        /// </summary>
        [JsonProperty("label_barcode")]
        public string? LabelBarcode { get; set; }


        /// <summary>
        /// business_type String(10)  是 BDS 业务类型(4PX内部调度所需，如需对接传值将说明，默认值：BDS。)
        /// </summary>
        [JsonProperty("business_type")]
        [Required]
        public string BusinessType { get; set; } = "BDS";



        /// <summary>
        ///  duty_type String(10)  是    税费费用承担方式(可选值：U、P); U：DDU由收件人支付关税; P：DDP 由寄件方支付关税 （如果物流产品只提供其中一种，则以4PX提供的为准）
        /// </summary>
        [JsonProperty("duty_type")]
        [Required]
        public string DutyType { get; set; } = "U";


        /// <summary>
        /// cargo_type String(10)  否	5	货物类型（1：礼品;2：文件;3：商品货样;5：其它；默认值：5）
        /// </summary>
        [JsonProperty("cargo_type")]
        public string? CargoType { get; set; } = "5";

        /// <summary>
        ///  vat_no String(32)  否 SDF2324 VAT税号(数字或字母)；欧盟国家(含英国)使用的增值税号；
        /// </summary>
        [JsonProperty("vat_no")]
        public string? VatNo { get; set; }


        /// <summary>
        /// eori_no String(32)  否 SDF2324 EORI号码(数字或字母)；欧盟入关时需要EORI号码，用于商品货物的清关
        /// </summary>
        [JsonProperty("eori_no")]
        public string? EoriNo { get; set; }


        /// <summary>
        /// ioss_no String(32)  否 FR-123456	IOSS号码
        /// </summary>
        [JsonProperty("ioss_no")]
        public string? IossNo { get; set; }

        /// <summary>
        ///  buyer_id    String(64)  否	23432432432432sd 买家ID(数字或字母)
        /// </summary>
        [JsonProperty("buyer_id")]
        public string? BuyerId { get; set; }


        /// <summary>
        /// sales_platform String(128) 否 AMAZON  销售平台（点击查看详情）
        /// </summary>
        [JsonProperty("sales_platform")]
        public string? SalesPlatform { get; set; }


        /// <summary>
        ///  trade_id String(32)  否 csd2342344  交易号ID(数字或字母)
        /// </summary>
        [JsonProperty("trade_id")]
        public string? TradeId { get; set; }


        /// <summary>
        /// seller_id String(32)  否 csd2342344  卖家ID(数字或字母)
        /// </summary>
        [JsonProperty("seller_id")]
        public string? SellerId { get; set; }


        /// <summary>
        /// is_commercial_invoice String(3)   否 N   能否提供商业发票（Y/N） Y：能提供商业发票(则系统不会生成形式发票)；N：不能提供商业发票(则系统会生成形式发票)； 默认为N；DHL产品必填，如产品代码A1/A5；
        /// </summary>
        [JsonProperty("is_commercial_invoice")]
        public string? IsCommercialInvoice { get; set; }


        /// <summary>
        /// parcel_qty Number(32)  否	3	包裹件数（一个订单有多少件包裹，就填写多少件数，请如实填写包裹件数，否则DHL无法返回准确的子单号数和子单号标签；DHL产品必填，如产品代码A1/A5；）
        /// </summary>
        [JsonProperty("parcel_qty")]
        public int? ParcelQty { get; set; }


        /// <summary>
        /// freight_charges Double(32)  否	48.4	运费(客户填写自己估算的运输费用；支持的币种，根据物流产品+收件人国家配置)
        /// </summary>
        [JsonProperty("freight_charges")]
        public double? FreightCharges { get; set; }


        /// <summary>
        /// currency_freight String(10)  否 USD 运费币种(按照ISO标准三字码；支持的币种，根据物流产品+收件人国家配置)
        /// </summary>
        [JsonProperty("currency_freight")]
        public string? CurrencyFreight { get; set; }


        /// <summary>
        /// declare_insurance Double(32)  否	23.4	申报保险费（是否必填，根据物流产品+目的国配置；根据欧盟IOSS政策，货值/运费/保险费可单独申报）支持小数点后2位
        /// </summary>
        [JsonProperty("declare_insurance")]
        public double? DeclareInsurance { get; set; }


        /// <summary>
        /// currency_declare_insurance  String(10)  否 USD 申报保险费币种（按照ISO标准，币种需和进出口国申报币种一致）
        /// </summary>
        [JsonProperty("currency_declare_insurance")]
        public string? CurrencyDeclareInsurance { get; set; }


        /// <summary>
        /// logistics_service_info Object(10)  是 物流服务信息  物流服务信息
        /// </summary>
        [JsonProperty("logistics_service_info")]
        [Required]
        public LogisticsServiceInfo logisticsServiceInfo { get; set; } = new();
        public class LogisticsServiceInfo
        {
            /// <summary>
            /// logistics_product_code  String(64)  是 A1  物流产品代码(点击查看详情)
            /// </summary>
            [JsonProperty("logistics_product_code")]
            [Required]
            public string LogisticsProductCode { get; set; }

            /// <summary>
            /// customs_service String(5)   否 N   单独报关（Y：单独报关；N：不单独报关） 默认值：N
            /// </summary>
            [JsonProperty("customs_service")]
            public string? CustomsService { get; set; } = "N";

            /// <summary>
            /// signature_service   String(1)   否 N   签名服务（Y/N)；默认值：N
            /// </summary>
            [JsonProperty("signature_service")]
            public string? SignatureService { get; set; } = "N";


            /// <summary>
            /// value_added_services    String(128) 否 无需填写    其他服务（待完善)
            /// </summary>
            [JsonProperty("value_added_services")]
            public string? ValueAddedServices { get; set; }
        }

        /// <summary>
        /// return_info	Object(10)	是	退件信息	退件信息
        /// </summary>
        [JsonProperty("return_info")]
        [Required]
        public ReturnInfo returnInfo { get; set; } = new();
        public class ReturnInfo
        {
            /// <summary>
            ///  is_return_on_domestic String(3)   是 N   境内/国内异常处理策略(Y：退件--实际是否支持退件，以及退件策略、费用，参考报价表；N：销毁；U：其他--等待客户指令) 默认值：N；
            /// </summary>
            [JsonProperty("is_return_on_domestic")]
            [Required]
            public string IsReturnOnDomestic { get; set; } = "N";


            /// <summary>
            /// domestic_return_addr  Object(10) 否	国内退件地址	境内/国内退件接收地址信息（退件地址非必填；若填写，则姓名/电话/邮编/国家/城市/详细地址均需填写）
            /// </summary>
            [JsonProperty("domestic_return_addr")]
            public Address? domesticReturnAddr { get; set; }


            /// <summary>
            /// is_return_on_oversea String(3)   是 N   境外/国外异常处理策略(Y：退件--实际是否支持退件，以及退件策略、费用，参考报价表；N：销毁；U：其他--等待客户指令) 默认值：N；
            /// </summary>
            [JsonProperty("is_return_on_oversea")]
            public string isReturnOnOversea { get; set; } = "N";

            /// <summary>
            /// oversea_return_addr Object(10)  否 海外退件地址  境外/国外退件接收地址信息（退件地址非必填；若填写，则姓名/电话/邮编/国家/城市/详细地址均需填写）
            /// </summary>
            [JsonProperty("oversea_return_addr")]
            public Address? OverseaReturnAddr { get; set; }

            public class Address
            {
                /// <summary>
                /// first_name String(32)  是 张三  名/姓名
                /// </summary>
                [JsonProperty("first_name")]
                [Required]
                public string FirstName { get; set; }

                /// <summary>
                /// last_name   String(32)  否 张   姓
                /// </summary>
                [JsonProperty("last_name")]
                public string? LastName { get; set; }

                /// <summary>
                /// company String(64)  否 北京市xxx有限公司  公司名
                /// </summary>
                [JsonProperty("company")]
                public string? Company { get; set; }

                /// <summary>
                /// phone   String(32)  是	18166668888	电话（必填）
                /// </summary>
                [JsonProperty("Phone")]
                [Required]
                public string Phone { get; set; }

                /// <summary>
                /// phone2 String(32)  否	0755-6600888	电话2
                /// </summary>
                [JsonProperty("phone2")]
                public string? Phone2 { get; set; }

                /// <summary>
                /// email   String(32)  否 xxxxxx@4px.com 邮箱
                /// </summary>
                [JsonProperty("email")]
                public string? Email { get; set; }

                /// <summary>
                /// post_code String(32)  是		邮编
                /// </summary>
                [JsonProperty("post_code")]
                [Required]
                public string PostCode { get; set; }

                /// <summary>
                /// country String(10)  是 CN  国家（国际二字码 标准ISO 3166-2 ）
                /// </summary>
                [JsonProperty("country")]
                [Required]
                public string Country { get; set; }

                /// <summary>
                /// state String(64)  否 广东省 州/省
                /// </summary>
                [JsonProperty("state")]
                public string? State { get; set; }

                /// <summary>
                ///city    String(64)  是 深圳市 城市
                /// </summary>
                [JsonProperty("city")]
                [Required]
                public string City { get; set; }

                /// <summary>
                /// district    String(64)  否 南山区 区、县
                /// </summary>
                [JsonProperty("district")]
                public string? District { get; set; }

                /// <summary>
                /// street  String(64)  是 南山街道南山路南山X座1001 街道/详细地址
                /// </summary>
                [JsonProperty("street")]
                [Required]
                public string Street { get; set; }

                /// <summary>
                /// house_number    String(32)  否	1001	门牌号
                /// </summary>
                [JsonProperty("house_number")]
                public string? HouseNumber { get; set; }
            }
        }



        /// <summary>
        /// parcel_list List(10)    是 包裹列表    包裹列表
        /// </summary>
        [Required]
        [JsonProperty("parcel_list")]
        public List<ParceListItem> ParcelList { get; set; } = new();
        public class ParceListItem
        {
            /// <summary>
            /// weight  Number(32)  是	104	预报重量（g）
            /// </summary>
            [Required]
            [JsonProperty("weight")]
            public double Weight { get; set; }

            /// <summary>
            /// length Double(32)  否	10	包裹长（cm）
            /// </summary>
            [JsonProperty("length")]
            public double? Length { get; set; }

            /// <summary>
            /// width Double(32)  否	10	包裹宽（cm）
            /// </summary>
            [JsonProperty("width")]
            public double? Width { get; set; }

            /// <summary>
            /// height Double(32)  否	10	包裹高（cm）
            /// </summary>
            [JsonProperty("height")]
            public double? Height { get; set; }

            /// <summary>
            /// parcel_value Double(32)  是	23.2301	包裹申报价值（最多4位小数）
            /// </summary>
            [JsonProperty("parcel_value")]
            [Required]
            public double ParcelValue { get; set; }

            /// <summary>
            /// currency String(10)  是 USD 包裹申报价值币别（按照ISO标准三字码；支持的币种，根据物流产品+收件人国家配置；币种需和进出口国申报币种一致）
            /// </summary>
            [JsonProperty("currency")]
            [Required]
            public string Currency { get; set; }

            /// <summary>
            /// include_battery String(3)   是 Y   是否含电池（Y/N）
            /// </summary>
            [JsonProperty("include_battery")]
            [Required]
            public string IncludeBattery { get; set; }

            /// <summary>
            /// battery_type String(32)  否	966	带电类型(具体产品可支持的带电类型请咨询销售）
            /// </summary>
            [JsonProperty("battery_type")]
            public string? BatteryType { get; set; }


            /// <summary>
            /// product_list List(10)    否 投保物品信息[@即将废弃]投保物品信息（投保、查验、货物丢失作为参考依据）
            /// </summary>
            [JsonProperty("product_list")]
            public List<ProductListItem>? ProductList { get; set; }

            public class ProductListItem
            {
                /// <summary>
                /// sku_code String(64)  否 iPhone5[@即将废弃]投保SKU（客户自定义SKUcode）（数字或字母或空格）
                /// </summary>
                [JsonProperty("sku_code")]
                public string? SkuCode { get; set; }

                /// <summary>
                /// standard_product_barcode String(64)  否	692213214242123	[@即将废弃] 投保商品标准条码（UPC、EAN、JAN…）
                /// </summary>
                [JsonProperty("standard_product_barcode")]
                public string? StandardProductBarcode { get; set; }

                /// <summary>
                /// product_name String(64)  否 苹果手机[@即将废弃]投保商品名称
                /// </summary>
                [JsonProperty("product_name")]
                public string? ProductName { get; set; }

                /// <summary>
                /// product_description String(128) 否 iPhone5[@即将废弃]投保商品描述
                /// </summary>
                [JsonProperty("product_description")]
                public string? ProductDescription { get; set; }

                /// <summary>
                /// product_unit_price  Double(32)  否	23	[@即将废弃] 投保商品单价（按对应币别的法定单位，最多4位小数点）
                /// </summary>
                [JsonProperty("product_unit_price")]
                public double? ProductUnitPrice { get; set; }

                /// <summary>
                /// currency String(10)  否 USD[@即将废弃]投保商品单价币别（按照ISO标准三字码；支持的币种，根据物流产品+收件人国家配置；币种需和进出口国申报币种一致）
                /// </summary>
                [JsonProperty("currency")]
                public string? Currency { get; set; } = string.Empty;

                /// <summary>
                /// qty Number(32)  否	2	[@即将废弃] 投保商品数量（单位为pcs）
                /// </summary>
                [JsonProperty("qty")]
                public int? Qty { get; set; }
            }


            /// <summary>
            /// declare_product_info List(10)    是 海关申报信息  海关申报信息(每个包裹的申报信息，方式1：填写申报产品代码和申报数量；方式2：填写其他详细申报信息)
            /// </summary>
            [JsonProperty("declare_product_info")]
            [Required]
            public List<DeclareProductInfoItem> DeclareProductInfo { get; set; } = new();

            public class DeclareProductInfoItem
            {
                /// <summary>
                /// declare_product_code String(32)  否 A1  申报产品代码（在4PX已备案申报产品的代码）
                /// </summary>
                [JsonProperty("declare_product_code")]
                public string? DeclareProductCode { get; set; }

                /// <summary>
                /// declare_product_name_cn String(64)  否 手机  申报品名(当地语言)
                /// </summary>
                [JsonProperty("declare_product_name_cn")]
                public string? DeclareProductNameCn { get; set; } = string.Empty;

                /// <summary>
                /// declare_product_name_en String(64)  否 Phone   申报品名（英语）
                /// </summary>
                [JsonProperty("declare_product_name_en")]
                public string? DeclareProductNameEn { get; set; } = string.Empty;

                /// <summary>
                /// uses String(32)  否 通讯  用途
                /// </summary>
                [JsonProperty("uses")]
                public string? Uses { get; set; }

                /// <summary>
                /// specification   String(32)  否	34×24×12	规格
                /// </summary>
                [JsonProperty("specification")]
                public string? Specification { get; set; }


                ///// <summary>
                ///// component   String(32)  否 金属、塑料 成分
                ///// </summary>
                [JsonProperty("component")]
                public string? Component { get; set; }

                /// <summary>
                /// unit_net_weight Number(32)  否	1000	单件商品净重（默认以g为单位）
                /// </summary>
                [JsonProperty("unit_net_weight")]
                public int? UnitNetWeight { get; set; }

                /// <summary>
                /// unit_gross_weight Number(32)  否	2300	单件商品毛重（默认以g为单位）
                /// </summary>
                [JsonProperty("unit_gross_weight")]
                public int? UnitGrossWeight { get; set; }

                /// <summary>
                /// material String(64)  否 金属、塑料 材质
                /// </summary>
                [JsonProperty("material")]
                public string? Material { get; set; }

                /// <summary>
                /// declare_product_code_qty Number(32)  是	3	申报数量
                /// </summary>
                [Required]
                [JsonProperty("declare_product_code_qty")]
                public int DeclareProductCodeQty { get; set; }


                ///// <summary>
                ///// unit_declare_product    String(32)  否 PCS 单位（点击查看详情；默认值：PCS）
                ///// </summary>
                [JsonProperty("unit_declare_product")]
                public string? UnitDeclareProduct { get; set; }


                ///// <summary>
                ///// origin_country String(64)  否 CN  原产地（ISO标准2字码）点击查看详情
                ///// </summary>
                [JsonProperty("origin_country")]
                public string? OriginCountry { get; set; }


                ///// <summary>
                ///// country_export  String(32)  否 CN  出口国/起始国/发件人国家（ISO标准2字码）
                ///// </summary>
                [JsonProperty("country_export")]
                public string? CountryExport { get; set; }

                ///// <summary>
                ///// country_import String(32)  否 CN  进口国/目的国/收件人国家（ISO标准2字码）
                ///// </summary>
                [JsonProperty("country_import")]
                public string? CountryImport { get; set; }

                /// <summary>
                /// hscode_export String(32)  否	9004909000	出口国/起始国/发件人国家_海关编码(只支持数字)
                /// </summary>
                [JsonProperty("hscode_export")]
                public string? HscodeExport { get; set; }

                /// <summary>
                /// hscode_import String(32)  否	9004909000	进口国/目的国/收件人国家_海关编码(只支持数字)
                /// </summary>
                [JsonProperty("hscode_import")]
                public string? HscodeImport { get; set; }

                /// <summary>
                /// declare_unit_price_export Double(64)  是	23	出口国/起始国/发件人国家_申报单价（按对应币别的法定单位，最多4位小数点）
                /// </summary>
                [Required]
                [JsonProperty("declare_unit_price_export")]
                public double DeclareUnitPriceExport { get; set; }

                /// <summary>
                /// currency_export String(10)  是 USD 出口国/起始国/发件人国家_申报单价币种（按照ISO标准；支持的币种，根据物流产品+收件人国家配置；币种需和进口国申报币种一致）
                /// </summary>
                [Required]
                [JsonProperty("currency_export")]
                public string CurrencyExport { get; set; }

                /// <summary>
                /// declare_unit_price_import Double(64)  是	23	进口国/目的国/收件人国家_申报单价（按对应币别的法定单位，最多4位小数点）
                /// </summary>
                [JsonProperty("declare_unit_price_import")]
                [Required]
                public double DeclareUnitPriceImport { get; set; }

                /// <summary>
                /// currency_import String(10)  是 USD 进口国/目的国/收件人国家_申报单价币种（按照ISO标准；支持的币种，根据物流产品+收件人国家配置；币种需和出口国申报币种一致）
                /// </summary>
                [Required]
                [JsonProperty("currency_import")]
                public string CurrencyImport { get; set; } = "none";

                /// <summary>
                /// brand_export String(128) 是 none    出口国/起始国/发件人国家_品牌(必填；若无，填none即可)
                /// </summary>
                [Required]
                [JsonProperty("brand_export")]
                public string BrandExport { get; set; } = "none";

                /// <summary>
                /// brand_import String(128) 是 狮子牌 进口国/目的国/收件人国家_品牌(必填；若无，填none即可)
                /// </summary>
                [Required]
                [JsonProperty("brand_import")]
                public string BrandImport { get; set; } = "none";

                /// <summary>
                /// sales_url String(128) 否 https://ju.taobao.com/jusp/other/mingpin/tp.htm?spm=875.7931836/B.2017039.4.5ff54265S5IEz9&pos=2&acm=201708280-2.1003.2.3116407&scm=1003.2.201708280-2.OTHER_1519569914238_3116407	商品销售URL
                /// </summary>
                [JsonProperty("sales_url")]
                public string? SalesUrl { get; set; }

                /// <summary>
                /// package_remarks String(200) 否 skutest 配货字段（打印标签选择显示配货信息是将会显示：package_remarks* qty）
                /// </summary>
                [JsonProperty("package_remarks")]
                public string? PackageRemarks { get; set; }
            }
        }




        /// <summary>
        ///is_insure	String(3)	是	Y	是否投保(Y、N)
        /// </summary>
        [Required]
        [JsonProperty("is_insure")]
        public string IsInsure { get; set; } = "N";

        /// <summary>
        ///  ext String(2000)  否 扩展字段
        /// </summary>
        [JsonProperty("ext")]
        public string? Ext { get; set; }

        /// <summary>
        /// sort_code String(255) 否 分拣分区
        /// </summary>
        [JsonProperty("sort_code")]
        public string? SortCode { get; set; }

        /// <summary>
        /// insurance_info Object(10)  是 保险信息    保险信息（投保时必须填写）
        /// </summary>
        [Required]
        [JsonProperty("insurance_info")]
        public Insuranceinfo InsuranceInfo { get; set; } = new();
        public class Insuranceinfo
        {
            /// <summary>
            /// insure_type String(10)  否	5Y 保险类型（XY:4PX保价；XP:第三方保险） 5Y, 5元每票 8Y, 8元每票 6P, 0.6%保费
            /// </summary>
            [JsonProperty("insure_type")]
            public string? InsureType { get; set; }

            /// <summary>
            /// insure_value    Double(32)  否	23	保险价值
            /// </summary>
            [JsonProperty("insure_value")]
            public double? InsureValue { get; set; }

            /// <summary>
            /// currency    String(10)  否 USD 保险币别（按照ISO标准，目前只支持USD）
            /// </summary>
            [JsonProperty("currency")]
            public string? Currency { get; set; }

            ///// <summary>
            ///// insure_person String(64)  否 张三  投保人/公司
            ///// </summary>
            [JsonProperty("insure_person")]
            public string? InsurePerson { get; set; }

            /// <summary>
            /// certificate_type    String(32)  否 ID  投保人证件类型（暂时只支持身份证，类型为：ID）
            /// </summary>
            [JsonProperty("certificate_type")]
            public string? CertificateType { get; set; }

            /// <summary>
            /// certificate_no String(64)  否	3.62432199412121e+17	投保人证件号码
            /// </summary>
            [JsonProperty("certificate_no")]
            public string? CertificateNo { get; set; }

            /// <summary>
            /// category_code   String(64)  否 无需填写    保险类目ID（保险的类目，暂时不填，默认取第一个类目）
            /// </summary>
            [JsonProperty("category_code")]
            public string? CategoryCode { get; set; }

            ///// <summary>
            ///// insure_product_name String(400) 否 计算机 投保货物名称
            ///// </summary>
            [JsonProperty("insure_product_name")]
            public string? InsureProductName { get; set; }

            /// <summary>
            /// package_qty String(800) 否 纸盒包装，1台/箱 投保包装及数量
            /// </summary>
            [JsonProperty("package_qty")]
            public string? PackageQty { get; set; }
        }

        /// <summary>
        /// sender Object(10)  是 发件人信息   发件人信息
        /// </summary>
        [JsonProperty("sender")]
        public Sender sender { get; set; } = new();
    
        public class Sender
        {
            //shipperName,深圳泰鑫隆科技贸易有限公司,发件人名称不能超过35个字符;

            /// <summary>
            /// first_name  String(35)  是 张三  名/姓名
            /// </summary>
            [JsonProperty("first_name")]
            [Required]
            [MaxLength(32,ErrorMessage = "发件人名称不能超过35个字符")]
            [RegularExpression(@"^[a-zA-Z\s]{1,35}$", ErrorMessage = "发件人名称不能超过35个字符,只能输入英文")]
            public string FirstName { get; set; }

            /// <summary>
            /// last_name   String(32)  否 张   姓
            /// </summary>
            [JsonProperty("last_name")]
            public string? LastName { get; set; }

            // shipperCompany,深圳泰鑫隆科技贸易有限公司,不能包含中文且不能超过50个字符;

            /// <summary>
            /// company String(64)  否 北京市xxx有限公司  公司名
            /// </summary>
            [JsonProperty("company")]
            [RegularExpression(@"^[a-zA-Z\s]{1,50}$", ErrorMessage = "发件人公司不能超过50个字符,只能输入英文")]
            [Required]
            public string? Company { get; set; }

            /// <summary>
            /// phone   String(32)  否	18166668888	电话
            /// </summary>
            [JsonProperty("Phone")]
            public string? Phone { get; set; }

            /// <summary>
            /// phone2 String(32)  否	0755-6600888	电话2
            /// </summary>
            [JsonProperty("phone2")]
            public string? Phone2 { get; set; }

            /// <summary>
            /// email   String(32)  否 xxxxxx@4px.com 邮箱
            /// </summary>
            [JsonProperty("email")]
            public string? Email { get; set; }

            /// <summary>
            /// post_code String(32)  否	0	邮编
            /// </summary>
            [JsonProperty("post_code")]
            public string? PostCode { get; set; }

            /// <summary>
            ///country String(10)  是 CN  国家（国际二字码 标准ISO 3166-2 ）
            /// </summary>
            [JsonProperty("country")]
            [Required]
            public string Country { get; set; }

            //shipperProvince,广东,不能包含中文切不能超过30个字符;

            /// <summary>
            /// state String(64)  否 广东省 州/省
            /// </summary>
            [JsonProperty("state")]
            [RegularExpression(@"^[a-zA-Z\s]{0,30}$", ErrorMessage = "shipperProvince不能超过30个字符,只能输入英文")]
            //不能包含中文切不能超过30个字符
            public string? State { get; set; }

            //shipperCity,深圳,不能包含中文切不能超过30个字符;

            /// <summary>
            ///city    String(64)  是 深圳市 城市
            /// </summary>
            [JsonProperty("city")]
            [Required]
            [RegularExpression(@"^[a-zA-Z\s]{1,30}$", ErrorMessage = "shippercity不能超过30个字符,只能输入英文")]
            public string City { get; set; }
            /// <summary>
            /// district    String(64)  否 南山区 区、县
            /// </summary>
            [JsonProperty("district")]
            public string? District { get; set; }

            //shipperAddress1,龙西大发新区路口一栋,只支持英文+半角符号且大于20小于105字符;

            /// <summary>
            /// street  String(64)  否 南山街道南山路南山X座1001 街道/详细地址
            /// </summary>
            [JsonProperty("street")]
            [Required]
            [RegularExpression(@"^[a-zA-Z\s~!@#\$%\^&\*\(\)\+=\|\\\}\]\{\[:;<,>\?\/""]{20,105}$", ErrorMessage = "shipperAddress只支持英文+半角符号且大于20小于105字符")]
            public string? Street { get; set; }

            /// <summary>
            /// house_number    String(32)  否	1001	门牌号
            /// </summary>
            [JsonProperty("house_number")]
            public string? HouseNumber { get; set; }

            /// <summary>
            /// certificate_info    Object(10)  否 证件信息    证件信息，根据海关要求变化
            /// </summary>
            [JsonProperty("certificate_info")]
            public CertificateInfo? CertificateInfo { get; set; }

        }

        public class CertificateInfo
        {
            /// <summary>
            ///  id_type String(32)  否 ID  证件类型（点击查看详情）
            /// </summary>
            [JsonProperty("id_type")]
            public string? IdType { get; set; }

            /// <summary>
            /// id_no String(32)  否	‘362432199412122000	证件号
            /// </summary>
            [JsonProperty("id_no")]
            public string? IdNo { get; set; }

            /// <summary>
            /// id_front_url    String(128) 否 https://ju.taobao.com/jusp/other/mingpin/tp.htm?spm=875.7931836/B.2017039.4.5ff54265S5IEz9&pos=2&acm=201708280-2.1003.2.3116407&scm=1003.2.201708280-2.OTHER_1519569914238_3116407	证件正面照URL
            /// </summary>
            [JsonProperty("id_front_url")]
            public string? IdFrontUrl { get; set; } = string.Empty;

            /// <summary>
            /// id_back_url String(128) 否 https://ju.taobao.com/jusp/other/mingpin/tp.htm?spm=875.7931836/B.2017039.4.5ff54265S5IEz9&pos=2&acm=201708280-2.1003.2.3116407&scm=1003.2.201708280-2.OTHER_1519569914238_3116407	证件背面照URL

            /// </summary>
            [JsonProperty("id_back_url")]
            public string? IdBackUrl { get; set; } = string.Empty;
        }


        /// <summary>
        /// recipient_info Object(10)  是 收件人信息   收件人信息
        /// </summary>
        [JsonProperty("recipient_info")]
        public RecipientInfo recipientInfo { get; set; } = new();
        public class RecipientInfo
        {
            //consigneeName,无,收件人的姓名必须是英文，不包括特殊符号，必须大于0小于等于35位。

            /// <summary>
            /// first_name String(32)  是 张三  名/姓名
            /// </summary>
            [JsonProperty("first_name")]
            [Required]
            [MaxLength(30, ErrorMessage = "收件人姓名不能超过30个字符")]
            [RegularExpression(@"^[a-zA-Z\s]{1,30}$", ErrorMessage = "收件人姓名只能输入英文,不能超过30个字符")]
            public string FirstName { get; set; }

            /// <summary>
            /// last_name   String(32)  否 张   姓
            /// </summary>
            [JsonProperty("last_name")]
            public string? LastName { get; set; }

            //收件人公司不少于两个单词且总长度不小于5位（含空格）
            //consigneeCompany,,收件人的公司名必须是英文，不包括特殊符号，必须大于0小于等于45位。
            /// <summary>
            /// company String(64)  否 北京市xxx有限公司  公司名
            /// </summary>
            [JsonProperty("company")]
            [Required]
            [RegularExpression(@"^[a-zA-Z\s]{1,45}$", ErrorMessage = "收件人公司必须大于0小于等于45位,只能输入英文")]
            public string Company { get; set; }
           

            /// <summary>
            /// phone   String(32)  是	18166668888	电话（必填）
            /// </summary>
            [JsonProperty("Phone")]
            [Required]
            public string Phone { get; set; }

            /// <summary>
            /// phone2 String(32)  否	0755-6600888	电话2
            /// </summary>
            [JsonProperty("phone2")]
            public string? Phone2 { get; set; }

            /// <summary>
            /// email   String(32)  否 xxxxxx@4px.com 邮箱
            /// </summary>
            [JsonProperty("email")]
            public string? Email { get; set; }

            /// <summary>
            /// post_code String(32)  否		邮编,非必填（部分产品需要填，具体以返回提示为准）
            /// </summary>
            [JsonProperty("post_code")]
            public string? PostCode { get; set; }

            /// <summary>
            /// country String(10)  是 CN  国家（国际二字码 标准ISO 3166-2 ）
            /// </summary>
            [JsonProperty("country")]
            [Required]
            public string Country { get; set; }
            /// <summary>
            /// state String(64)  否 广东省 州/省
            /// </summary>
            [JsonProperty("state")]
            public string? State { get; set; }
            /// <summary>
            ///city    String(64)  是 深圳市 城市
            /// </summary>
            [JsonProperty("city")]
            [Required]
            public string City { get; set; }
            /// <summary>
            /// district    String(64)  否 南山区 区、县
            /// </summary>
            [JsonProperty("district")]
            public string? District { get; set; }

            //consigneeAddress1,无,收件人地址必须是英文，不包括特殊符号，大于1且小于等于100位。
            /// <summary>
            /// street  String(64)  是 南山街道南山路南山X座1001 街道/详细地址
            /// </summary>
            [JsonProperty("street")]
            [Required]
            [RegularExpression(@"^[a-zA-Z\s]{1,100}$", ErrorMessage = "收件人地址必须大于1小于等于100位,只能输入英文")]
            public string Street { get; set; }

            /// <summary>
            /// house_number    String(32)  否	1001	门牌号
            /// </summary>
            [JsonProperty("house_number")]
            public string? HouseNumber { get; set; }

            /// <summary>
            /// certificate_info    Object(10)  否 证件信息    证件信息，根据海关要求变化
            /// </summary>
            [JsonProperty("certificate_info")]
            public CertificateInfo? certificateInfo { get; set; }
        }


        /// <summary>
        /// deliver_type_info	Object(10)	是	到仓方式	货物到仓方式信息
        /// </summary>
        [JsonProperty("deliver_type_info")]
        public DeliverTypeInfo deliverTypeInfo { get; set; } = new();

        public class DeliverTypeInfo
        {
            /// <summary>
            ///  deliver_type String(32)  是	1	到仓方式（1:上门揽收；2:快递到仓；3:自送到仓；5:自送门店）
            /// </summary>
            [JsonProperty("deliver_type")]
            [Required]
            public string DeliverType { get; set; }

            /// <summary>
            /// warehouse_code String(32)  否 PL1US1  收货仓库/门店代码（仓库代码）
            /// </summary>
            [JsonProperty("warehouse_code")]
            public string? WarehouseCode { get; set; }

            /// <summary>
            /// pick_up_info Object(10)  否 揽收信息    上门揽收信息
            /// </summary>
            [JsonProperty("pick_up_info")]
            public PickUpInfo? pickUpInfo { get; set; }
            public class PickUpInfo
            {
                /// <summary>
                /// expect_pick_up_earliest_time    Long(32)    否	1432710115000	期望提货最早时间（*注：时间格式的传入值需要转换为long类型格式。）
                /// </summary>
                [JsonProperty("expect_pick_up_earliest_time")]
                public long? ExpectPickUpEarliestTime { get; set; }

                /// <summary>
                /// expect_pick_up_latest_time Long(32)    否	1432710115000	期望提货最迟时间（*注：时间格式的传入值需要转换为long类型格式。）
                /// </summary>
                [JsonProperty("expect_pick_up_latest_time")]
                public string? ExpectPickUpLatestTime { get; set; }

                /// <summary>
                /// pick_up_address_info	Object(10)	否	收货地址	收货地址
                /// </summary>
                [JsonProperty("pick_up_address_info")]
                public PickUpAddressInfo? pickUpAddressInfo { get; set; } = new();
                public class PickUpAddressInfo
                {
                    /// <summary>
                    ///  first_name String(32)  否 张三  名/姓名
                    /// </summary>
                    [JsonProperty("first_name")]
                    public string? FirstName { get; set; }

                    /// <summary>
                    ///last_name   String(32)  否 张   姓
                    /// </summary>
                    [JsonProperty("last_name")]
                    public string? LastName { get; set; }

                    /// <summary>
                    /// company String(64)  否 北京市xxx有限公司  公司名
                    /// </summary>
                    [JsonProperty("company")]
                    public string? Company { get; set; }

                    /// <summary>
                    /// phone   String(32)  否	18166668888	电话
                    /// </summary>
                    [JsonProperty("Phone")]
                    public string? Phone { get; set; }

                    /// <summary>
                    /// phone2 String(32)  否	0755-6600888	电话2
                    /// </summary>
                    [JsonProperty("phone2")]
                    public string? Phone2 { get; set; }

                    /// <summary>
                    /// email   String(32)  否 xxxxxx@4px.com 邮箱
                    /// </summary>
                    [JsonProperty("email")]
                    public string? Email { get; set; }

                    /// <summary>
                    /// post_code String(32)  否	0	邮编
                    /// </summary>
                    [JsonProperty("post_code")]
                    public string? PostCode { get; set; }

                    /// <summary>
                    /// country String(10)  否 CN  国家（国际二字码 标准ISO 3166-2 ）
                    /// </summary>
                    [JsonProperty("country")]
                    public string? Country { get; set; }
                    /// <summary>
                    /// state String(64)  否 广东省 州/省
                    /// </summary>
                    [JsonProperty("state")]
                    public string? State { get; set; }
                    /// <summary>
                    ///city    String(64)  否 深圳市 城市
                    /// </summary>
                    [JsonProperty("city")]
                    public string? City { get; set; }
                    /// <summary>
                    /// district    String(64)  否 南山区 区、县
                    /// </summary>
                    [JsonProperty("district")]
                    public string? District { get; set; }
                    /// <summary>
                    /// street  String(64)  否 南山街道南山路南山X座1001 街道/详细地址
                    /// </summary>
                    [JsonProperty("street")]
                    public string? Street { get; set; }

                    /// <summary>
                    /// house_number    String(32)  否	1001	门牌号
                    /// </summary>
                    [JsonProperty("house_number")]
                    public string? HouseNumber { get; set; }
                }
            }

            /// <summary>
            ///  express_to_4px_info Object(10)  否 快递到仓信息  快递到仓信息
            /// </summary>
            [JsonProperty("express_to_4px_info")]
            public ExpressTo4pxInfo? expressTo4pxInfo { get; set; }
            public class ExpressTo4pxInfo
            {
                /// <summary>
                /// express_company String(64)  否 UPS 快递公司
                /// </summary>
                [JsonProperty("express_company")]
                public string? ExpressCompany { get; set; }

                /// <summary>
                /// tracking_no String(64)  否	2Zds234234 追踪号
                /// </summary>
                [JsonProperty("tracking_no")]
                public string? TrackingNo { get; set; }
            }

            /// <summary>
            ///  self_send_to_4px_info Object(10)  否 自己送仓信息  自己送仓信息
            /// </summary>
            [JsonProperty("self_send_to_4px_info")]
            public SelfSendTo4pxInfo? selfSendTo4pxInfo { get; set; }
            public class SelfSendTo4pxInfo
            {
                /// <summary>
                /// booking_earliest_time   Long(32)    否	1432710115000	预约送仓最早时间（*注：时间格式的传入值需要转换为long类型格式。）
                /// </summary>
                [JsonProperty("booking_earliest_time")]
                public long? BookingEarliestTime { get; set; }

                /// <summary>
                /// booking_latest_time Long(32)    否	1432710115000	预约送仓最晚时间（*注：时间格式的传入值需要转换为long类型格式。）
                /// </summary>
                [JsonProperty("booking_latest_time")]
                public long? BookingLatestTime { get; set; }
            }
        }


        /// <summary>
        /// deliver_to_recipient_info Object(10)  是 投递信息    投递信息
        /// </summary>
        [JsonProperty("deliver_to_recipient_info")]
        public DeliverToRecipientInfo deliverToRecipientInfo { get; set; } = new();

        public class DeliverToRecipientInfo
        {
            /// <summary>
            /// deliver_type    String(32)  否 HOME_DELIVERY   投递类型：HOME_DELIVERY-投递到门；SELF_PICKUP_STATION-投递门店（自提点）；SELF_SERVICE_STATION-投递自提柜(自助点）；默认：HOME_DELIVERY；注：目前暂时不支持投递门店、投递自提柜
            /// </summary>
            [JsonProperty("deliver_type")]
            public string? DeliverType { get; set; } = "HOME_DELIVERY";

            /// <summary>
            /// station_code    String(32)  否 LG6028  自提门店/自提点的信息(选择自提时必传，点击获取详情)
            /// </summary>
            [JsonProperty("station_code")]
            public string? StationCode { get; set; }
        }
    }
}