﻿namespace ERP.Models.Api.Logistics.FPX
{
    public enum MethodType
    {
        None = 0,
        [EnumExtension.Description("ds.xms.logistics_product.getlist")]
        Shiptype,
        [EnumExtension.Description("ds.xms.order.create")]
        Send,
        [EnumExtension.Description("com.basis.billing.getbilling")]
        Fee,
        [EnumExtension.Description("tr.order.tracking.get")]
        Track,
        [EnumExtension.Description("ds.xms.label.get")]
        Print,
        [EnumExtension.Description("ds.xms.order.hold")]
        Cancle,
    }
}

