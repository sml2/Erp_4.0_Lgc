﻿namespace ERP.Models.Api.Logistics.FPX
{
    public class FPXParameters
    {
        /// <summary>
        /// 货运方式
        /// </summary>
        public string ShipCode { set; get; } = string.Empty;

        /// <summary>
        /// IOSS税号
        /// </summary>
        public string IossNo { set; get; } = string.Empty;
    }
}
