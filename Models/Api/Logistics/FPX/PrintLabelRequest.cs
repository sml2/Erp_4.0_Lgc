﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ERP.Models.Api.Logistics.FPX
{
    public class PrintLabelRequest:CommonParameter
    {
        public PrintLabelRequest(Parameter p,string CustomNumber) : base(p,MethodType.Print)
        {
            request_no = CustomNumber;
        }

        /// <summary>
        /// 请求单号（支持4PX单号、客户单号和面单号）
        /// </summary>
        public string request_no { get; set; }


        [JsonIgnore]
        public override string Url => $"{base.HostUrl}?method={base.methodType.GetDescription()}&app_key={base.AppKey}&v={version}&timestamp={TimeStamp}&format=json&sign={base.GetSign(this.ToPayload())}";

        //public string response_label_format { get; set; }
        //public string label_size { get; set; }
        ///// <summary>
        ///// 是否打印配货信息
        ///// </summary>
        //public string is_print_pick_info => "N";
        ///// <summary>
        ///// 是否打印当前时间
        ///// </summary>
        //public string is_print_time => "N";
        //public string is_print_buyer_id => "N";
        ///// <summary>
        ///// 是否打印报关单
        ///// </summary>
        //public string is_print_declaration_list => "N";
        //public string create_package_label => "N";
        //public string is_print_pick_barcode => "N";
    }
}
