﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.FPX
{
    public enum LabelEnum
    {
        [Description("标签纸80.5mm×90mm")]
        label_80x9,
        [Description("A4纸")]
        a4
    }
}
