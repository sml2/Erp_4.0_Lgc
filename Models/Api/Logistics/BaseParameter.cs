﻿namespace ERP.Models.Api.Logistics
{
    public class BaseParameter
    {
        /// <summary>
        /// 物流简称
        /// </summary>
        public Platforms LogName { get; set; }
        /// <summary>
        /// 接口授权账户信息
        /// 
        /// </summary>
        public string Parameter { get; set; } = string.Empty;
        
        /// <summary>
        /// 订单被分配给的用户id
        /// </summary>
        public int? DistributedUserId;

        public BaseParameter(Platforms logName, string parameter)
        {
            LogName = logName;
            Parameter = parameter;
        }

        public BaseParameter()
        {

        }
    }


}
