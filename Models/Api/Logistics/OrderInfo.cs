namespace ERP.Models.Api.Logistics
{
    /// <summary>
    /// 订单信息
    /// </summary>
    public class OrderInfo
    {
        /// <summary>
        /// 货运方式中文名
        /// </summary>
        public string ShipMethod { get; set; } = string.Empty; 

        /// <summary>
        /// 物流窗口填写数据
        /// </summary>
        public string ext { get; set; } = string.Empty;

        /// <summary>
        /// 订单主键
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// 系统运单号
        /// </summary>
        public string? OrderNo { get; set; } = string.Empty;

        /// <summary>
        /// 缩略图
        /// </summary>
        public string? Url { get; set; }      

        /// <summary>
        /// 收件人信息
        /// </summary>
        public ReceiverInfo ReceiverAddress { get; set; } = new();

        /// <summary>
        ///  运单已发送次数
        /// </summary>
        public int SendNum { get; set; }

        private int _failNum;

        public int FailNum
        {
            get
            {
                return _failNum + 1;
            }
            set { _failNum = value; }
        }

        public List<GoodInfo> GoodInfos { get; set; } = new();
              
        /// <summary>
        /// 平台ID
        /// </summary>
        public int PlatformId { get; set; }
    }
}
