﻿namespace ERP.Models.Api.Logistics;

public class EstimatePriceResult : BaseResult
{
    public EstimatePriceResult(Exception e) : base(e)
    {
    }
    public EstimatePriceResult (Exception e,string response):base(e)
    {
        Response = response;
    }

    public EstimatePriceResult (List<CommonResult> priceInfos, string response)
    {
        Success = true;
        PriceInfos = priceInfos;
        Response = response;
    }
    public List<CommonResult> PriceInfos { get; set; } = new();
    public string Response { get; set; } = string.Empty;
}
