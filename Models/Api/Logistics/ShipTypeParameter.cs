﻿namespace ERP.Models.Api.Logistics
{
    public class ShipTypeParameter : BaseParameter
    {
        /// <summary>
        ///YunTu :城市简码
        /// </summary>
        public string? CountryCode { get; set; }

        /// <summary>
        /// kjhy接口的参数：运输方式
        /// </summary>
        public string? transportType { get; set; }
    }


}
