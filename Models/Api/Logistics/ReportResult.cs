using ERP.Enums.Logistics;
using Newtonsoft.Json;

namespace ERP.Models.Api.Logistics;

public class ReportResult: BaseResult
{   
    public ReportResult(int orderId,string customNumber, Exception e):base(e)
    {
        OrderId = orderId;
        CustomNumber = customNumber;
    }

    public ReportResult(int orderId,string customNumber, Exception e,string response) : base(e)
    {
        OrderId = orderId;
        CustomNumber = customNumber;
        Response = response;
    }
   
    public ReportResult(int orderId, string customNumber, string orderNum, string trackNum, string unit, double price, string ext , 
       string response)
    {
        OrderId = orderId;
        CustomNumber = customNumber;
        OrderNum = orderNum == null ? "" : orderNum;
        TrackNum = trackNum == null ? "" : trackNum;
        Unit = unit;
        Price = price;           
        Ext = ext;
        Response = response;       
    }

    public ReportResult(int orderId, string customNumber, string orderNum, string trackNum)
    {
        OrderId = orderId;
        CustomNumber = customNumber;
        OrderNum = orderNum;
        TrackNum = trackNum;
    }


    //public ReportResult(ManualParameter manualParameter)
    //{
    //    OrderId = manualParameter.OrderInfo.id;
    //    CustomNumber = manualParameter.CustomNum;
    //    OrderNum = manualParameter.OrderNum;
    //    TrackNum = manualParameter.TrackNum;                   
    //}   

    public int OrderId { get; set; }

    /// <summary>
    /// 物流系统平台单号
    /// </summary>
    public string CustomNumber { get; set; } = string.Empty;
    /// <summary>
    /// 物流系统运单号
    /// </summary>
    public string OrderNum { get; set; } = string.Empty;  

    /// <summary>
    /// 物流系统的跟踪号
    /// </summary>
    public string? TrackNum { get; set; }
    public string Unit { get; set; } = "CNY";
    public double Price { get; set; } = 0;       
    public string TrackInfo { get; set; } = string.Empty;
    public string TrackButtonName { get; set; } = "跟踪";
    public string Show_PicURL { get; set; } = string.Empty;
    public States State { get; set; } = States.COMMITTED;
    public string Note { get; set; } = string.Empty;
    public string Date { get; set; } = Mod_GlobalFunction.GetTimeStamp(Mod_GlobalFunction.TimestampMode.UtcSecond).ToString();
    public string Ext { get; set; } = string.Empty;
    public string LabelUrl { get; set; } = string.Empty;
    /// <summary>
    /// 订单号已经存在
    /// </summary>
    public int FailNum { get; set; } = 0;

    // [JsonIgnore]
    public string Response { get; set; } = string.Empty;
}


public class ReportResultList
{
    public ReportResultList()
    {

    }
    public ReportResultList(Exception e)
    {
        exception = e;
    }

    public ReportResultList(Exception e,string response)
    {
        exception = e;
        Response = response;
    }

    public ReportResultList(List<ReportResult> reportResults)
    {
        ReportResults = reportResults;
    }

    public ReportResultList(ReportResult reportResult)
    {
        ReportResults =new List<ReportResult>() { reportResult };
    }

    public Exception exception { get; set; } = new();

    public List<ReportResult> ReportResults { get; set; } = new();

    [JsonIgnore]
    public string Response { get; set; } = string.Empty;
}
