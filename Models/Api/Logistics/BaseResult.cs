﻿namespace ERP.Models.Api.Logistics;

public class BaseResult
{
    public BaseResult()
    {
        Success = true;
    }
    public BaseResult(Exception e)
    {
        exception = e;
        Success = false;       
    }

    public Exception exception { get; set; } = new();

    public bool Success { get; set; }

   
}



public class CancelResult:BaseResult
{
    public CancelResult(string res)
    {
        Response = res;
    }

    public CancelResult(Exception e,string res=""):base(e)
    {
        Response = res;
    }

    public string Response { get; set; } = string.Empty;
}


public class BaseParam
{
}


public class BaseExtParam
{
}