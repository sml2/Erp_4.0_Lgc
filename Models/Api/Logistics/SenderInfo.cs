﻿namespace ERP.Models.Api.Logistics
{
    /// <summary>
    /// 寄件人信息
    /// </summary>
    public class SenderInfo
    {
        /// <summary>
        /// 寄件人姓名
        /// </summary>        
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// 寄件人邮箱
        /// </summary>
        public string Mail { get; set; } = string.Empty;

        /// <summary>
        /// 寄件人邮编
        /// </summary>
        public string PostalCode { get; set; } = string.Empty;

        /// <summary>
        /// 寄件人手机
        /// </summary>
        public string PhoneNumber { get; set; } = string.Empty;

        /// <summary>
        /// 寄件人所在区域
        /// </summary>
        public string? Region { get; set; }

        /// <summary>
        /// 寄件人州省
        /// </summary>
        public string Province { get; set; } = string.Empty;

        /// <summary>
        /// 寄件人所在城市
        /// </summary>
        public string City { get; set; } = string.Empty;

        /// <summary>
        /// 寄件人地址
        /// </summary>
        public string Address { get; set; } = string.Empty;

        /// <summary>
        /// 寄件人公司
        /// </summary>
        public string? Company { get; set; }
    }


}
