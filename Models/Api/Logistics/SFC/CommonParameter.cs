﻿namespace ERP.Models.Api.Logistics.SFC
{
    public class CommonParameter: RequestLogistic
    {
        public CommonParameter(Parameter P)
        {
            token = P.token;
            appKey = P.appKey;
            userId = P.userId;
        }
        public string token { get; set; }
        public string appKey { get; set; }
        public string userId { get; set; }
    }
}
