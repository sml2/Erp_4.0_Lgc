﻿using ERP.Attributes.Logistics;

namespace ERP.Models.Api.Logistics.SFC
{
    public class Parameter: BaseParam
    {
        private string id;
        private string key;
        private string tok;
        [ViewConfigParameter(nameof(userId), "用户ID", Sort = 0)]
        public string userId { get { return id; } set { id = value.Trim(); } }
        [ViewConfigParameter(nameof(appKey), Sort = 1)]
        public string appKey { get { return key; } set { key = value.Trim(); } } 

        [ViewConfigParameter(nameof(token), Sort = 2)]
        public string token { get { return tok; } set { tok = value.Trim(); } } 
    }
}
