using static EnumExtension;

namespace ERP.Models.Api.Logistics.SFC
{
    public class printUrlRequest: CommonParameter
    {
        public printUrlRequest(Parameter parameter,List<string> customNumbers,FileTypeEnum filetype):base(parameter)
        {
            OrderCodeList = string.Join(',',customNumbers);
            FileType = filetype.GetDescription();
        }

        public string FileType { get; set; }

        public string OrderCodeList { get; set; }

        //public override string Url => $"http://www.sendfromchina.com/api/label?orderCodeList={OrderCodeList}&printType=1&print_type={FileType}&printSize=3&printSort=1";


        public override string Url => $"http://www.sfcservice.com/order/print/index/?orderCodeList={OrderCodeList}&printType=1&isPrintDeclare=1&declare=0&ismerge=1&urluserid=OTY5&print_type={FileType}&printSize=3";
    }

    public enum FileTypeEnum
    {       
        [Description("pdf")]
        Pdf=1,
        [Description("html")]
        Html
    }
}
