
using Newtonsoft.Json;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ERP.Models.Api.Logistics.SFC
{
    public class ExtParameter: BaseExtParam
    {        
        /// <summary>
        /// 商品简介
        /// </summary>
        public string goodsDescription { get; set; } = string.Empty;

        /// <summary>
        /// 商品自定义标签
        /// </summary>
        public string detailCustomLabel { get; set; } = string.Empty;

        /// <summary>
        /// 申报名称 (英文)
        /// </summary>
        public string detailDescription { get; set; } = string.Empty;

        /// <summary>
        /// 申报名称 (中文)
        /// </summary>
        public string detailDescriptionCN { get; set; } = string.Empty;

        /// <summary>
        /// 海关编码
        /// </summary>
        public string HsCode { get; set; } = string.Empty;

        /// <summary>
        /// IOSS税号
        /// </summary>
        public string IOSS { get; set; } = string.Empty;

        /// <summary>
        /// 包裹边长(cm)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹边长(cm)")]
        public int Length { get; set; }

        /// <summary>
        /// 包裹边宽(cm)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹边宽(cm)")]
        public int Width { get; set; }

        /// <summary>
        /// 包裹边高(cm)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹边高(cm)")]
        public int Height { get; set; }

        /// <summary>
        /// 包裹重量(kg)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹重量(kg)")]
        public double Weight { get; set; }

        /// <summary>
        /// 货运方式
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货运方式")]
        public string ShipMethod { get; set; } = string.Empty;

        /// <summary>
        /// 最大报关金额($) 
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("最大报关金额($)")]
        //[Range(typeof(double), "0.001", "175", ErrorMessage = "最大申报金额必须大于0且小于175")]
        public double MaxPrice { get; set; }


        [Required(ErrorMessage = "{0} 不能为空")]
        /// <summary>
        /// 币种
        /// </summary>
        public string Currency { get; set; }

        public string IDCR { get; set; }
        public string taxesNumber { get; set; }
    }
}
