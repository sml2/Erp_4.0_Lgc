﻿namespace ERP.Models.Api.Logistics.SFC
{
    public class SFCParameters
    {
        /// <summary>
        /// 货运方式
        /// </summary>
        public string ShipCode { get; set; } = string.Empty;

        /// <summary>
        /// IOSS税号
        /// </summary>
        public string IOSS { get; set; } = string.Empty;
    }
}
