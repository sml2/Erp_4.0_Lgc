﻿namespace ERP.Models.Api.Logistics
{
    public class TrackParameter:BaseParameter
    {
        public List<TrackOrderInfo> TrackOrderInfoLst { get; set; } = new();
    }
}
