namespace ERP.Models.Api.Logistics
{
    /// <summary>
    /// 商品信息
    /// </summary>
    public class GoodInfo
    {
        public int? OrderId { get; set; }

        public string? OrderNo { get; set; }
        public string ID { get; set; } = string.Empty;
        public string goodId { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public int Num { get; set; }
        public string? Asin { get; set; }
        /// <summary>
        /// 单位
        /// </summary>
        public string Unit { get; set; } = string.Empty;
        /// <summary>
        /// 单价
        /// </summary>
        public double Price { get; set; }
        public double Total { get; set; }
        /// <summary>
        /// 已发货数量
        /// </summary>
        public int Delivery { get; set; }

        /// <summary>
        /// 待发货数量
        /// </summary>
        public int SendNum { get; set; }
        /// <summary>
        /// 客户将要发货数量(WinUI Input)
        /// </summary>
        public int Count { get; set; }
        public string? Url { get; set; }
        public string? Sku { get; set; }
        public string FromURL { get; set; } = string.Empty;
        public int StockOutNum { get; set; } //出库数
    }


}
