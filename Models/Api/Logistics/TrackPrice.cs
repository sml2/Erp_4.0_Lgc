﻿using Newtonsoft.Json;

namespace ERP.Models.Api.Logistics;

public class TrackPrice
{
    public TrackPrice()
    {

    }
    public TrackPrice( int waybillId, string customNumber,Exception e)
    {
        Success = false;
        WaybillId = waybillId;
        CustomNumber = customNumber;
        exception = e;       
    }
    public TrackPrice(int waybillId, string customNumber, Exception e, string response)
    {
        Success = false;
        WaybillId = waybillId;
        CustomNumber = customNumber;
        exception = e;                       
        Response = response;
    }

    public TrackPrice(int waybillId, string customNumber, string? logNumber, string? trackNum, string unit, double price, string? weight, string? priceTypeDesc)
    {
        Success = true;
        WaybillId = waybillId;
        CustomNumber = customNumber;
        LogNumber = logNumber;
        TrackNum = trackNum;
        Unit = unit;
        Price = price;
        Weight = weight;
        PriceTypeDesc = priceTypeDesc;
    }   
    public TrackPrice(int waybillId, string customNumber, string? logNumber, string? trackNum, string unit, double price, string? weight, string? priceTypeDesc, string response,DateTime? deductionTime) : this(waybillId,customNumber,logNumber,trackNum,unit,price,weight,priceTypeDesc,response)
    {
        DeductionTime = deductionTime;
    }

    public TrackPrice(int waybillId, string customNumber, string? logNumber, string? trackNum, string unit, double price, string? weight, string? priceTypeDesc, string response)
    {
        Success = true;
        WaybillId = waybillId;
        CustomNumber = customNumber;
        LogNumber = logNumber;
        TrackNum = trackNum;
        Unit = unit;
        Price = price;
        Weight = weight;
        PriceTypeDesc = priceTypeDesc;
        Response = response;
    }

    /// <summary>
    /// 运单主键ID
    /// </summary>
    public int WaybillId { get; set; }

    /// <summary>
    /// 本系统订单号码
    /// </summary>
    public string? CustomNumber { get; set; }

    /// <summary>
    /// 物流系统主键
    /// </summary>
    public string? LogNumber { get; set; }

    /// <summary>
    /// 物流系统追踪号
    /// </summary>
    public string? TrackNum { get; set; }

    /// <summary>
    /// 币种
    /// </summary>
    public string Unit { get; set; } = "CNY";

    /// <summary>
    /// 费用
    /// </summary>
    public double Price { get; set; }

    public string? Weight { get; set; }

    /// <summary>
    /// 价格类型说明
    /// </summary>
    public string? PriceTypeDesc { get; set; }

    [JsonIgnore]
    public string Response { get; set; }=string.Empty;

    public Exception exception { get; set; } = new();
    
    public DateTime? DeductionTime { get; set; }
    public bool Success { get; set; }
}
