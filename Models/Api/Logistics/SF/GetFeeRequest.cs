﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.SF
{
    public class GetFeeRequest:CommonParameter
    {
        public GetFeeRequest(Parameter parameter, List<string> trackNumbers) : base(parameter)
        {
            api_user = parameter.UserName;
            api_key = GetMD5();
            order_user = parameter.UserName;
            weightAndFreightList = new List<GetFeeRequest.trackingNoListItem>();
            trackNumbers.ForEach(x => weightAndFreightList.Add(new trackingNoListItem(x)));
        }

        public override string Url => "https://orapi.trackmeeasy.com/lomp-ext/sf-express/kts/weightAndFreightSearch";

        public string api_user { get; set; }
        public string api_key { get; set; }
        public string order_user { get; set; }
        public List<trackingNoListItem> weightAndFreightList { get; set; }
        public class trackingNoListItem 
        {
            public trackingNoListItem(string trackNumber)
            {
                tracking_no = trackNumber;
            }
            public string tracking_no { get; set; }
        }
    }
}
