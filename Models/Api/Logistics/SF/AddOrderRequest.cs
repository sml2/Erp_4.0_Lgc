﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ERP.Models.Api.Logistics.SF
{
    //[XmlRoot(ElementName = "Request")]
    //public class AddOrderRequest:CommonParameter
    //{     
       

    //    public string Head { get; set; }
    //    public BodyItem Body { get; set; }
    //    public class BodyItem
    //    {
    //        [XmlElement("Order")]
    //        public OrderItem Order { get; set; }
    //        public class OrderItem
    //        {
    //            [XmlAttribute]
    //            public string orderid { get; set; }
    //            [XmlAttribute]
    //            public string platform_order_id { get; set; }
    //            /// <summary>
    //            /// 0000	    未知电商平台	
    //            /// 0001	    aliexpress  速卖通平台
    //            /// 0002	    wish        wish平台
    //            /// 0003	    amazon      亚马逊平台
    //            /// 0004	    ebay        ebay平台
    //            /// 0005	    joom        JOOM平台
    //            /// 0006	    rumall      Rumall平台
    //            /// </summary>
    //            [XmlAttribute]
    //            public string platform_code { get; set; }
    //            /// <summary>
    //            /// 0000	未知ERP（默认值）
    //            /// </summary>
    //            [XmlAttribute]
    //            public string erp_code { get; set; }
    //            /// <summary>
    //            /// 货运方式
    //            /// </summary>
    //            [XmlAttribute]
    //            public string express_type { get; set; }
    //            #region 寄件人信息
    //            [XmlAttribute]
    //            public string j_company { get; set; }
    //            [XmlAttribute]
    //            public string j_contact { get; set; }
    //            [XmlAttribute]
    //            public string j_mobile { get; set; }
    //            [XmlAttribute]
    //            public string j_tel { get; set; }
    //            [XmlAttribute]
    //            public string j_province { get; set; }
    //            [XmlAttribute]
    //            public string j_city { get; set; }
    //            [XmlAttribute]
    //            public string j_address { get; set; }
    //            [XmlAttribute]
    //            public string j_country { get; set; }
    //            //[XmlAttribute]
    //            //public string j_county { get; set; }
    //            [XmlAttribute]
    //            public string j_post_code { get; set; }
    //            #endregion 
    //            #region 收件人信息
    //            [XmlAttribute]
    //            public string d_company { get; set; }
    //            [XmlAttribute]
    //            public string d_mobile { get; set; }
    //            [XmlAttribute]
    //            public string d_contact { get; set; }
    //            [XmlAttribute]
    //            public string d_tel { get; set; }
    //            [XmlAttribute]
    //            public string d_province { get; set; }
    //            [XmlAttribute]
    //            public string d_city { get; set; }
    //            [XmlAttribute]
    //            public string d_address { get; set; }
    //            [XmlAttribute]
    //            public string d_email { get; set; }
    //            [XmlAttribute]
    //            public string d_country { get; set; }
    //            //[XmlAttribute]
    //            //public string d_county { get; set; }
    //            [XmlAttribute]
    //            public string d_post_code { get; set; }
    //            #endregion 
    //            [XmlAttribute]
    //            public int parcel_quantity { get; set; }
    //            [XmlAttribute]
    //            public int pay_method { get; set; }
    //            #region 包裹信息
    //            [XmlAttribute]
    //            public double declared_value { get; set; }
    //            [XmlAttribute]
    //            public string declared_value_currency { get; set; }
    //            [XmlAttribute]
    //            public string custid { get; set; }
    //            [XmlAttribute]
    //            public double cargo_total_weight { get; set; }
    //            [XmlAttribute]
    //            public double cargo_length { get; set; }
    //            [XmlAttribute]
    //            public double cargo_width { get; set; }
    //            [XmlAttribute]
    //            public double cargo_height { get; set; }
    //            [XmlAttribute]
    //            public string operate_flag { get; set; }
    //            [XmlAttribute]
    //            public string isBat { get; set; }
    //            /// <summary>
    //            /// 税号
    //            /// </summary>
    //            [XmlAttribute]
    //            public string tax_number { get; set; }
    //            /// <summary>
    //            /// IOSS
    //            /// </summary>
    //            [XmlAttribute]
    //            public string abn { get; set; }
    //            [XmlAttribute]
    //            public string order_insurance { get; set; }
    //            #endregion
    //            #region 货物信息
    //            [XmlElement]
    //            public List<CargoItem> Cargo { get; set; }
    //            [XmlType(AnonymousType = true)]
    //            [XmlRoot("Cargo", IsNullable = false)]
    //            public class CargoItem
    //            {
    //                [XmlAttribute]
    //                public string cname { get; set; }
    //                [XmlAttribute]
    //                public string name { get; set; }
    //                [XmlAttribute]
    //                public int count { get; set; }
    //                [XmlAttribute]
    //                public string unit { get; set; }
    //                [XmlAttribute]
    //                public double weight { get; set; }
    //                [XmlAttribute]
    //                public double amount { get; set; }
    //                /// <summary>
    //                /// USD
    //                /// </summary>
    //                [XmlAttribute]
    //                public string currency { get; set; }
    //                [XmlAttribute]
    //                public string hscode { get; set; }
    //                [XmlAttribute]
    //                public string order_url { get; set; }
    //            }
    //            #endregion
    //        }
    //    }
    //}


    [XmlRoot(ElementName = "Request")]
    public class AddOrderRequest:CommonParameter
    {
        public AddOrderRequest()
        {

        }
        public AddOrderRequest(Parameter parameter, SenderInfo senderInfo, OrderInfo orderInfo, ExtParameter Ext) : base(parameter)
        {
            Id = orderInfo.id;
            Head = parameter.UserName;
            Body = new AddOrderRequest.BodyItem();
            Body.Order = new AddOrderRequest.BodyItem.OrderItem();
            Body.Order.platform_order_id = orderInfo.OrderNo;
            Body.Order.orderid = Helpers.GetOrderNo(orderInfo.OrderNo, orderInfo.SendNum, orderInfo.FailNum);
            Body.Order.platform_code = "0003";
            Body.Order.erp_code = "0000";
            Body.Order.express_type = Ext.ShipMethod;
            //寄件人信息
            Body.Order.j_company = senderInfo.Company ?? "";
            Body.Order.j_contact = senderInfo.Name;
            Body.Order.j_mobile = senderInfo.PhoneNumber;
            Body.Order.j_tel = senderInfo.PhoneNumber;
            Body.Order.j_province = senderInfo.Province;
            Body.Order.j_city = senderInfo.City;
            Body.Order.j_address = senderInfo.Address;
            Body.Order.j_country = "CN";
            Body.Order.j_post_code = senderInfo.PostalCode;
            //收货人信息
            Body.Order.d_mobile = orderInfo.ReceiverAddress.Phone;
            Body.Order.d_contact = orderInfo.ReceiverAddress.Name;
            Body.Order.d_tel = orderInfo.ReceiverAddress.Phone;
            Body.Order.d_province = orderInfo.ReceiverAddress.Province;
            Body.Order.d_city = orderInfo.ReceiverAddress.City;
            string Address = orderInfo.ReceiverAddress.City;
            if (orderInfo.ReceiverAddress.Address2.IsNotWhiteSpace()) Address += $" {orderInfo.ReceiverAddress.Address2}";
            if (orderInfo.ReceiverAddress.Address3.IsNotWhiteSpace()) Address += $" {orderInfo.ReceiverAddress.Address3}";
            Body.Order.d_address = Address;
            Body.Order.d_email = orderInfo.ReceiverAddress.Email ?? "";
            Body.Order.d_country = orderInfo.ReceiverAddress.NationShort;
            Body.Order.d_post_code = orderInfo.ReceiverAddress.Zip ?? "";
            Body.Order.parcel_quantity = 1;
            Body.Order.pay_method = 1;
            //货物信息
            Body.Order.declared_value = Ext.Declare;
            Body.Order.declared_value_currency = Ext.Currency;
            Body.Order.custid = parameter.custid;
            Body.Order.cargo_total_weight =Helpers.RoundData(Ext.Weight/1000,2);
            Body.Order.cargo_length = Ext.Length;
            Body.Order.cargo_width = Ext.Width;
            Body.Order.cargo_height = Ext.Height;
            Body.Order.operate_flag = "1";
            Body.Order.isBat = Ext.isBat.ToString();
            Body.Order.tax_number = Ext.TaxNumber;
            Body.Order.abn = Ext.Abn;
            Body.Order.order_insurance = Ext.Insurance;
            Body.Order.Cargo = new List<AddOrderRequest.BodyItem.OrderItem.CargoItem>();
            //商品信息
            int AllCount = orderInfo.GoodInfos.Sum((r) => r.Num);
            foreach (var p in orderInfo.GoodInfos)
            {
                AddOrderRequest.BodyItem.OrderItem.CargoItem Cargo = new AddOrderRequest.BodyItem.OrderItem.CargoItem();
                Cargo.name = Ext.Name;
                Cargo.count = p.Count;
                Cargo.unit = "piece";
                Cargo.weight =Helpers.RoundData(Ext.Weight/1000 / AllCount);
                Cargo.amount =Helpers.RoundData(Ext.Declare / AllCount);
                Cargo.currency = Ext.Currency;
                Cargo.cname = Ext.NameCn;
                Cargo.hscode = Ext.HsCode;
                Body.Order.Cargo.Add(Cargo);
            }
        }


        [XmlIgnore]       

        public override string Url => "http://ktswebservice.main.sf.com/";


        [XmlIgnore]

        public int Id { get; set; }
        //[XmlAttribute]
        //public string service => "OrderService";
        //[XmlAttribute]
        //public string lang => "zh_CN";
        /// <summary>
        /// UserName
        /// </summary>
        public string Head { get; set; }
        public BodyItem Body { get; set; }
        public class BodyItem
        {
            [XmlElement("Order")]
            public OrderItem Order { get; set; } = new();
            public class OrderItem
            {
                [XmlAttribute]
                public string orderid { get; set; } = string.Empty;
                [XmlAttribute]
                public string platform_order_id { get; set; } = string.Empty;
                /// <summary>
                /// 0000	    未知电商平台	
                /// 0001	    aliexpress  速卖通平台
                /// 0002	    wish        wish平台
                /// 0003	    amazon      亚马逊平台
                /// 0004	    ebay        ebay平台
                /// 0005	    joom        JOOM平台
                /// 0006	    rumall      Rumall平台
                /// </summary>
                [XmlAttribute]
                public string platform_code { get; set; } = string.Empty;
                /// <summary>
                /// 0000	未知ERP（默认值）
                /// </summary>
                [XmlAttribute]
                public string erp_code { get; set; } = string.Empty;
                /// <summary>
                /// 货运方式
                /// </summary>
                [XmlAttribute]
                public string express_type { get; set; } = string.Empty;
                #region 寄件人信息
                [XmlAttribute]
                public string j_company { get; set; } = string.Empty;
                [XmlAttribute]
                public string j_contact { get; set; } = string.Empty;
                [XmlAttribute]
                public string j_mobile { get; set; } = string.Empty;
                [XmlAttribute]
                public string j_tel { get; set; } = string.Empty;
                [XmlAttribute]
                public string j_province { get; set; } = string.Empty;
                [XmlAttribute]
                public string j_city { get; set; } = string.Empty;
                [XmlAttribute]
                public string j_address { get; set; } = string.Empty;
                [XmlAttribute]
                public string j_country { get; set; } = string.Empty;
                //[XmlAttribute]
                //public string j_county { get; set; }
                [XmlAttribute]
                public string j_post_code { get; set; } = string.Empty;
                #endregion 
                #region 收件人信息
                [XmlAttribute]
                public string d_company { get; set; } = string.Empty;
                [XmlAttribute]
                public string d_mobile { get; set; } = string.Empty;
                [XmlAttribute]
                public string d_contact { get; set; } = string.Empty;
                [XmlAttribute]
                public string d_tel { get; set; } = string.Empty;
                [XmlAttribute]
                public string d_province { get; set; } = string.Empty;
                [XmlAttribute]
                public string d_city { get; set; } = string.Empty;
                [XmlAttribute]
                public string d_address { get; set; } = string.Empty;
                [XmlAttribute]
                public string d_email { get; set; } = string.Empty;
                [XmlAttribute]
                public string d_country { get; set; } = string.Empty;
                //[XmlAttribute]
                //public string d_county { get; set; }
                [XmlAttribute]
                public string d_post_code { get; set; } = string.Empty;
                #endregion 
                [XmlAttribute]
                public int parcel_quantity { get; set; }
                [XmlAttribute]
                public int pay_method { get; set; }
                #region 包裹信息
                [XmlAttribute]
                public double declared_value { get; set; }
                [XmlAttribute]
                public string declared_value_currency { get; set; } = string.Empty;
                [XmlAttribute]
                public string custid { get; set; } = string.Empty;

                /// <summary>
                /// kg
                /// </summary>
                [XmlAttribute]               
                public double cargo_total_weight { get; set; }
                [XmlAttribute]
                public double cargo_length { get; set; }
                [XmlAttribute]
                public double cargo_width { get; set; }
                [XmlAttribute]
                public double cargo_height { get; set; }
                [XmlAttribute]
                public string operate_flag { get; set; } = string.Empty;
                [XmlAttribute]
                public string isBat { get; set; } = string.Empty;
                /// <summary>
                /// 税号
                /// </summary>
                [XmlAttribute]
                public string tax_number { get; set; } = string.Empty;
                /// <summary>
                /// IOSS
                /// </summary>
                [XmlAttribute]
                public string abn { get; set; } = string.Empty;
                [XmlAttribute]
                public string order_insurance { get; set; } = string.Empty;
                #endregion
                #region 货物信息
                [XmlElement]
                public List<CargoItem> Cargo { get; set; } = new();
                [XmlType(AnonymousType = true)]
                [XmlRoot("Cargo", IsNullable = false)]
                public class CargoItem
                {
                    [XmlAttribute]
                    public string cname { get; set; } = string.Empty;
                    [XmlAttribute]
                    public string name { get; set; } = string.Empty;
                    [XmlAttribute]
                    public int count { get; set; }
                    [XmlAttribute]
                    public string unit { get; set; } = string.Empty;
                    [XmlAttribute]
                    public double weight { get; set; }
                    [XmlAttribute]
                    public double amount { get; set; }
                    /// <summary>
                    /// USD
                    /// </summary>
                    [XmlAttribute]
                    public string currency { get; set; } = string.Empty;
                    [XmlAttribute]
                    public string hscode { get; set; } = string.Empty;
                    [XmlAttribute]
                    public string order_url { get; set; } = string.Empty;
                }
                #endregion
            }
        }
    }

}