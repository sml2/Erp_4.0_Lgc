﻿using System.Collections;
using System.Xml.Serialization;

namespace ERP.Models.Api.Logistics.SF
{
    public class CommonParameter: RequestLogistic
    {
        public CommonParameter()
        {

        }
        public CommonParameter(Parameter p)
        {
            UserName = p.UserName;
            this.checkword = p.checkword;
            this.custid = p.custid;
        }

        /// <summary>
        /// 接入编码
        /// </summary>
        [XmlIgnore]
        public string UserName { get; set; } = string.Empty;//xunliyou
        /// <summary>
        /// 接口校验码：78be99af393330d9d7ff936ea9577d01
        /// </summary>
        [XmlIgnore]
        public string checkword { get; set; } = string.Empty;
        /// <summary>
        /// 客户月结卡号
        /// </summary>
        [XmlIgnore]
        public string custid { get; set; } = string.Empty;

        public string GetMD5()
        {
            return Sy.Security.MD5.Encrypt($"{UserName}{checkword}");
        }
    }
}
