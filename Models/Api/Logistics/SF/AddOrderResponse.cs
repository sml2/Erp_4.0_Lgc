﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ERP.Models.Api.Logistics.SF
{
    [XmlRoot(ElementName = "Response")]
    public class AddOrderResponse
    {
        public string Head { get; set; } = string.Empty;
        public BodyItem Body { get; set; } = null!;
        public class BodyItem
        {
            public OrderResponseItem OrderResponse { get; set; } = new();
            public class OrderResponseItem 
            {
                [XmlAttribute]
                public string orderid { get; set; } = string.Empty;
                [XmlAttribute]
                public string mailno { get; set; } = string.Empty;
                [XmlAttribute]
                public string agent_mailno { get; set; } = string.Empty;
                [XmlAttribute]
                public string direction_code { get; set; } = string.Empty;
            }
        }
        public ErrorItem ERROR { get; set; } = new();
        public class ErrorItem
        {
            [XmlAttribute]
            public string code { get; set; } = string.Empty;
            [XmlText]
            public string ErrorValue { get; set; } = string.Empty;
        }
    }
}
