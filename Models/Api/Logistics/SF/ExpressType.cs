﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.SF
{
    public class ShipTypesResponse
    {
        public ShipTypesResponse() { }
        public ShipTypesResponse(string shipCode, string shipName)
        {
            ShipCode = shipCode;
            ShipName = shipName;
        }

        public string ShipCode { get; set; } = string.Empty;
        public string ShipName { get; set; } = string.Empty;
        public override string ToString()
        {
            return $"{ShipName}[{ShipCode}]";
        }
    }
    public enum ExpressType
    {
        国际小包平邮 = 9,
        国际小包挂号,
        国际小包陆运平邮 = 23,
        国际小包陆运挂号,
        国际经济小包平邮,
        国际经济小包挂号,
        国际专线小包平邮,
        国际专线小包挂号,
        国际电商专递标准,
        国际精品小包 = 32,
        国际南美小包挂号 = 38,
        国际卢邮小包挂号 = 44,
        国际比邮小包平邮 = 47,
        国际比邮小包特惠挂号,
        国际特货小包平邮 = 63,
        国际特货小包挂号,
        国际电商专递_CD = 72,
        国际电商专递_快速 = 74,
        国际铁路经济小包平邮 =93,
        国际铁路经济小包挂号,

    }
}
