﻿namespace ERP.Models.Api.Logistics.SF;

public class GetFeeResponse
{
    public int code { get; set; }

    public string message { get; set; } = string.Empty;

    public string order_user { get; set; } = string.Empty;

    public List<WeightAndFreightListItem> weightAndFreightList { get; set; } = new();
    public class WeightAndFreightListItem
    {

        public string debit_time { get; set; } = string.Empty;

        public double freight { get; set; }

        public double tariff { get; set; }

        public double clearance { get; set; }

        public string TotalDetail => $"运费:{freight}\r\n关税费:{tariff}\r\n清关费:{clearance}";

        public double Total => freight + tariff + clearance;

        public double clearance_service { get; set; }

        public string message { get; set; } = string.Empty;

        public string tracking_no { get; set; } = string.Empty;

        public double weight { get; set; }

        public string weight_time { get; set; } = string.Empty;
    }
}
