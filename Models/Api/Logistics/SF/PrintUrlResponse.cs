﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.SF
{ 
    public class PrintUrlResponse
    {
        /// <summary>
        /// 是否成功标志，0代表失败；1代表成功；2代表重复订单
        /// </summary>
        public bool success { get; set; }
        public string version { get; set; }=string.Empty;
        public List<Cls_array> array { get; set; } = new();
        public class Cls_array
        {
            public string mailno { get; set; } = string.Empty;
            public string orderid { get; set; } = string.Empty;
            public int configId { get; set; }
            public string url { get; set; } = string.Empty;
        }
    }
}
