﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.SF
{
    public class TrackRequest
    {
        /// <summary>
        /// 系统应用标识, GTS系统生成并提供
        /// </summary>
        public string sysCode => "0000";

        public string sign { get; set; } = string.Empty;

        public int billNoType => 1;

        public List<DataItem> data { get; set; } = new();
        public class DataItem
        {
            /// <summary>
            /// 顺丰单号
            /// </summary>
            public string sfWaybillNo { get; set; } = string.Empty;
            /// <summary>
            /// SF国际路由操作码
            /// </summary>
            public string opCode { get; set; } = string.Empty;
            /// <summary>
            /// 网点编码
            /// </summary>
            public string zoneCode => "";
            /// <summary>
            /// 操作员工号
            /// </summary>
            public string barOprCode => "";

            public string localTm => DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            public string gmt => "GMT+"; 
        }
    }
}
