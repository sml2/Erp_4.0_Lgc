﻿namespace ERP.Models.Api.Logistics.SF
{
    public class SFParameters
    {
        /// <summary>
        /// 货运方式
        /// </summary>
        public string ShipCode { get; set; } = string.Empty;

        /// <summary>
        /// 税号
        /// </summary>
        public string TaxNumber { get; set; } = string.Empty;

        /// <summary>
        /// ABN号
        /// </summary>
        public string ABNNo { get; set; } = string.Empty;


        /// <summary>
        /// 是否带电：不带电0，带点1
        /// </summary>
        public string Isbat { set; get; } = string.Empty;

        /// <summary>
        /// 是否报保值，
        /// </summary>
        public bool Insurance { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        public double NumInsurance { get; set; }
    }
}
