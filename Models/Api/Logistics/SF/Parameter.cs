﻿using ERP.Attributes.Logistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.SF
{
    public class Parameter: BaseParam
    {
        private string name;
        private string word;
        private string id;
        [ViewConfigParameter(nameof(UserName), Sort = 0)]
        /// <summary>
        /// 接入编码
        /// </summary>
        public string UserName { get { return name; } set { name = value.Trim(); } } 
        [ViewConfigParameter(nameof(checkword), Sort = 1)]
        /// <summary>
        /// 接口校验码：78be99af393330d9d7ff936ea9577d01
        /// </summary>
        public string checkword { get { return word; } set { word = value.Trim(); } } 
        [ViewConfigParameter(nameof(custid), Sort = 2,Required = false)]
        /// <summary>
        /// 客户月结卡号
        /// </summary>
        public string custid { get { return id; } set { id = value.Trim(); } } 
    }
}
