﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using Newtonsoft.Json;
using static EnumExtension;

namespace ERP.Models.Api.Logistics.SF
{
    public class ExtParameter: BaseExtParam
    {      

        public ExtParameter(string nameCn, string name, string taxNumber, string abn, string hsCode, int length, int width, 
            int height, double weight, string shipMethod, string isBat, double declare, bool isInsurance, string insurance,string currency)
        {
            this.Currency = currency;
            NameCn = nameCn;
            Name = name;
            TaxNumber = taxNumber;
            Abn = abn;
            HsCode = hsCode;
            Length = length;
            Width = width;
            Height = height;
            Weight = weight;
            ShipMethod = shipMethod;
            if (Enum.IsDefined(typeof(WithBatteryType), isBat) && Enum.TryParse<WithBatteryType>(isBat, out var labelwithBatteryType))
            {
                this.isBat = labelwithBatteryType;
            }
            else
            {
                Console.WriteLine("SF isBat:" + isBat) ;
            }
            Declare = declare;
            IsInsurance = isInsurance;
            Insurance = insurance;
        }

        /// <summary>
        /// 货品申报名(中文)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货品申报名(中文)")]
        public string NameCn { get; set; }

        /// <summary>
        /// 货品申报名(英文)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货品申报名(英文)")]
        public string Name { get; set; }

        /// <summary>
        /// 税号
        /// </summary>
        public string TaxNumber { get; set; }

        /// <summary>
        /// ABN号
        /// </summary>
        public string Abn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string HsCode { get; set; }

        /// <summary>
        /// 包裹边长(cm)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹边长(cm)")]
        public int Length { get; set; }

        /// <summary>
        /// 包裹边宽(cm)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹边宽(cm)")]
        public int Width { get; set; }

        /// <summary>
        /// 包裹边高(cm)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹边高(cm)")]
        public int Height { get; set; }

        /// <summary>
        /// 包裹重量(kg)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹重量(kg)")]
        public double Weight { get; set; }

        /// <summary>
        /// 货运方式
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货运方式")]
        public string ShipMethod { get; set; }

        /// <summary>
        /// 是否带电
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("是否带电")]
        public WithBatteryType isBat { get; set; }

        /// <summary>
        ///申报价格($)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("申报价格($)")]
        public double Declare { get; set; }

        /// <summary>
        /// 是否保值
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("是否保值")]
        public bool IsInsurance { get; set; }

        /// <summary>
        /// 保值($)
        /// </summary>       
        public string Insurance { get; set; }

        [Required(ErrorMessage = "{0} 不能为空")]
        /// <summary>
        /// 币种
        /// </summary>
        public string Currency { get; set; }

    }

    public enum WithBatteryType
    {
        [EnumExtension.Description("不带电")]
        NOBattery,  //不带电
        [EnumExtension.Description("带电")]
        WithBattery, //带电     
    }
}
