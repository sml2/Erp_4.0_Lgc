﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.SF
{
    public class PrintUrlRequest :CommonParameter
    {
        public PrintUrlRequest(Parameter parameter,string customNumber,string orderNumber) : base(parameter)
        {
            orderid = customNumber;
            mailno = orderNumber;            
        }

        public override string Url => "http://sfapi.sf-international.com/ruserver/api/getLabelUrl.action?orderid={customNumber}&mailno={orderNumber}&onepdf={onepdf}&jianhuodan={jianhuodan}&username={UserName}&signature={GetMD5()}";

        public override HttpMethod Method => HttpMethod.Get;
        public string orderid { get; set; }
        public string mailno { get; set; }
        public string onepdf { get; set; } = "true";
        public string jianhuodan { get; set; } = "false";
        public string username { get; set; } = string.Empty;
        /// <summary>
        /// username+checkword（signature值）
        /// </summary>
        public string signature { get; set; } = string.Empty;
    }


    public class NewPrintUrlRequest : CommonParameter
    {

    }

}
