namespace ERP.Models.Api.Logistics
{
    public class ReceiverInfo
    {           
        public string Name { get; set; } = string.Empty;
        public string? Phone { get; set; } 
        public int? NationId { get; set; }
        public string? NationShort { get; set; } = string.Empty;
        public string? Nation { get; set; } = string.Empty;
        public string? Province { get; set; } = string.Empty;
        public string City { get; set; } = string.Empty;
        public string? District { get; set; }
        public string? Address { get; set; } = string.Empty;
        public string? Address2 { get; set; }
        public string? Address3 { get; set; }
        public string? Zip { get; set; }
        public string? Email { get; set; }

        public void GetReceiver(ERP.Models.DB.Orders.Address receiver)
        {
            this.Name = receiver.Name;
            this.Phone = receiver.Phone;
            this.NationId = receiver.NationId;
            this.NationShort = receiver.NationShort;
            this.Nation = receiver.Nation;
            this.Province = receiver.Province;
            this.City = receiver.City;
            this.District = receiver.District;
            this.Address = receiver.Address1;
            this.Address2 = receiver.Address2;
            this.Address3 = receiver.Address3;
            this.Zip = receiver.Zip;
            this.Email = receiver.Email;
        }
    }
}
