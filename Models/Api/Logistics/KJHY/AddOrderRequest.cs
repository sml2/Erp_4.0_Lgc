using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.KJHY
{
    public class AddOrderRequest:CommonParameter
    {
        public AddOrderRequest(Parameter p, SenderInfo senderInfo, OrderInfo orderInfo, ExtParameter Ext) : base(p)
        {
            Id = orderInfo.id;
            transport_id = Ext.ShipMethod;
            order_number = Helpers.GetOrderNo(orderInfo.OrderNo, orderInfo.SendNum, orderInfo.FailNum);// orderInfo.OrderNo;
            from_address = new AddOrderRequest.Address();
            from_address.country_code = "CN";
            from_address.province = senderInfo.Province;
            from_address.city = senderInfo.City;
            from_address.address1 = senderInfo.Address;
            from_address.zip_code = senderInfo.PostalCode;
            from_address.contacts = senderInfo.Name;
            from_address.tel = senderInfo.PhoneNumber;
            from_address.email = senderInfo.Mail;
            to_address = new AddOrderRequest.Address();
            to_address.country_code = orderInfo.ReceiverAddress.NationShort;
            to_address.province = orderInfo.ReceiverAddress.Province;
            to_address.city = orderInfo.ReceiverAddress.City;
            var Address = orderInfo.ReceiverAddress.Address;
            var Address2 = $"{ orderInfo.ReceiverAddress.Address2} { orderInfo.ReceiverAddress.Address3}";
            to_address.address1 = Address;
            to_address.address2 = Address2;
            to_address.zip_code = orderInfo.ReceiverAddress.Zip??"";
            to_address.contacts = orderInfo.ReceiverAddress.Name;
            to_address.tel = orderInfo.ReceiverAddress.Phone;
            to_address.email = orderInfo.ReceiverAddress.Email??"";
            to_address.tax_number = Ext.TaxNumber;
            this.cartons = new List<AddOrderRequest.CartonsItem>();
            var carton = new AddOrderRequest.CartonsItem();
            carton.number = "";
            carton.length = Ext.length;
            carton.width = Ext.width;
            carton.height = Ext.height;
            carton.weight =Helpers.RoundData(Ext.weight/1000,2);
            carton.skus = new List<AddOrderRequest.CartonsItem.SkusItem>();
            int num = orderInfo.GoodInfos.Sum((r) => r.Num);
            foreach (var Pro in orderInfo.GoodInfos)
            {
                var sku = new AddOrderRequest.CartonsItem.SkusItem();
                sku.name_cn = Ext.cnname;
                sku.name_en = Ext.enname;
                sku.picture_url = Pro.Url??"";
                sku.unit_price = Helpers.RoundData(Ext.Price / num,2);
                sku.units = Ext.Currency;
                sku.qty = Pro.Count;
                sku.hs_code = Ext.HsCode;
                sku.material_cn = Ext.description;
                sku.material_en = Ext.descriptionEn;
                sku.purpose = Ext.descriptionEn;
                sku.goods_type = Ext.GoodsType.ToString();
                sku.model = "model";
                carton.skus.Add(sku);
            }
            cartons.Add(carton);
        }

        [JsonIgnore]
        public string orderNo { get; set; } = string.Empty;

        [JsonIgnore]
        public int Id { get; set; }
        /// <summary>
        /// 订单号
        /// </summary>
        public string order_number { get; set; }
        /// <summary>
        /// 渠道ID
        /// </summary>
        public string transport_id { get; set; }
        ///// <summary>
        ///// VAT税号(数字或字母)
        ///// 欧盟国家(含英国)使用的增值税号
        ///// </summary>
        //public string vat_no { get; set; }
        ///// <summary>
        ///// EORI号码(数字或字母)
        ///// 欧盟入关时需要EORI号码，用于商品货物的清关
        ///// </summary>
        //public string eori_no { get; set; }
        /// <summary>
        /// 发件信息
        /// </summary>
        public Address from_address { get; set; }
        /// <summary>
        /// 收件信息
        /// </summary>
        public Address to_address { get; set; }
        public class Address
        {
            /// <summary>
            /// 国家简码
            /// </summary>
            public string country_code { get; set; } = string.Empty;
            /// <summary>
            /// 省份
            /// </summary>
            public string province { get; set; } = string.Empty;
            /// <summary>
            /// 城市
            /// </summary>
            public string city { get; set; } = string.Empty;
            /// <summary>
            /// 地址1
            /// </summary>
            public string address1 { get; set; } = string.Empty;
            /// <summary>
            /// 地址2
            /// </summary>
            public string address2 { get; set; } = string.Empty;
            /// <summary>
            /// 邮编
            /// </summary>
            public string zip_code { get; set; }=string.Empty;
            /// <summary>
            /// 联系人
            /// </summary>
            public string contacts { get; set; } = string.Empty;
            /// <summary>
            /// 联系电话
            /// </summary>
            public string tel { get; set; } = string.Empty;
            /// <summary>
            /// 邮箱
            /// </summary>
            public string email { get; set; } = string.Empty;
            /// <summary>
            /// 税号(to_address所有)
            /// </summary>
            public string tax_number { get; set; } = string.Empty;
            /// <summary>
            /// 公司(to_address所有)
            /// </summary>
            public string company { get; set; } = string.Empty;
        }
        /// <summary>
        /// 包裹信息
        /// </summary>
        public List<CartonsItem> cartons { get; set; }
        public class CartonsItem
        {
            /// <summary>
            /// 箱号:按照1、2、3递增
            /// </summary>
            public string number { get; set; } = string.Empty;
            /// <summary>
            /// 长
            /// </summary>
            public double length { get; set; }
            /// <summary>
            /// 宽
            /// </summary>
            public double width { get; set; }
            /// <summary>
            /// 高
            /// </summary>
            public double height { get; set; }
            /// <summary>
            /// 重量kg
            /// </summary>
            public double weight { get; set; }
            /// <summary>
            /// 商品信息
            /// </summary>
            public List<SkusItem> skus { get; set; } = new();
            public class SkusItem
            {
                /// <summary>
                /// sku编码
                /// </summary>
                public string sku { get; set; } = string.Empty;
                /// <summary>
                /// 图片地址
                /// </summary>
                public string picture_url { get; set; } = string.Empty;
                /// <summary>
                /// 英文品名
                /// </summary>
                public string name_en { get; set; } = string.Empty;
                /// <summary>
                /// 中文品名
                /// </summary>
                public string name_cn { get; set; } = string.Empty;
                /// <summary>
                /// 单位
                /// </summary>
                public string units { get; set; }
                /// <summary>
                /// 申报单价：默认美元
                /// </summary>
                public double unit_price { get; set; }
                /// <summary>
                /// 数量
                /// </summary>
                public int qty { get; set; }
                /// <summary>
                /// 重量
                /// </summary>
                public string weight { get; set; } = string.Empty;
                /// <summary>
                /// 品牌
                /// </summary>
                public string brand { get; set; } = string.Empty;
                /// <summary>
                /// 海关编码
                /// </summary>
                public string hs_code { get; set; } = string.Empty;
                /// <summary>
                /// 材质中文
                /// </summary>
                public string material_cn { get; set; } = string.Empty;
                /// <summary>
                /// 材质英文
                /// </summary>
                public string material_en { get; set; } = string.Empty;
                /// <summary>
                /// 用途
                /// </summary>
                public string purpose { get; set; } = string.Empty;
                ///// <summary>
                ///// 销售链接
                ///// </summary>
                //public string asin { get; set; }
                /// <summary>
                /// 货物类型：普货、带电、带磁、带电带磁
                /// </summary>
                public string goods_type { get; set; } = string.Empty;
                /// <summary>
                /// 型号
                /// </summary>
                public string model { get; set; } = string.Empty;
            }
        }
    }
}
