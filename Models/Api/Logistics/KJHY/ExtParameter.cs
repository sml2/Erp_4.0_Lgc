﻿using Newtonsoft.Json;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using static EnumExtension;

namespace ERP.Models.Api.Logistics.KJHY
{
    public class ExtParameter: BaseExtParam
    {
        public ExtParameter(string taxNumber, string cnname, string enname, string description, string descriptionEn,
            double weight, double length, double width, double height, string hsCode, string transportType,
            string shipMethod, string goodsType, double price,string currency)
        {
            this.Currency = currency;
            TaxNumber = taxNumber;
            this.cnname = cnname;
            this.enname = enname;
            this.description = description;
            this.descriptionEn = descriptionEn;
            this.weight = weight;
            this.length = length;
            this.width = width;
            this.height = height;
            HsCode = hsCode;
            ShipMethod = shipMethod;
            Price = price;
            if (Enum.IsDefined(typeof(enumTransportType), transportType) && Enum.TryParse<enumTransportType>(transportType, out var labeltransportType))
            {
                TransportType = labeltransportType;
            }
            else
            {
                Console.WriteLine("KJHY transportType:"+ transportType);
            }

            if (Enum.IsDefined(typeof(enumGoodsType), goodsType) && Enum.TryParse<enumGoodsType>(goodsType, out var labelgoodsType))
            {
                GoodsType = labelgoodsType;
            }
            else
            {
                Console.WriteLine("KJHY goodsType:" + goodsType);
            }
        }

        /// <summary>
        /// 税号
        /// </summary>
        public string TaxNumber { get; set; }

        /// <summary>
        ///商品申报名(中文)
        /// </summary>
        public string cnname { get; set; }

        /// <summary>
        /// 商品申报名(英文)
        /// </summary>
        public string enname { get; set; }

        /// <summary>
        /// 商 品 描 述(中文)
        /// </summary>

        public string description { get; set; }

        /// <summary>
        /// 商 品 描 述(英文)
        /// </summary>
        public string descriptionEn { get; set; }

        /// <summary>
        /// 包裹重量(kg)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹重量(kg)")]
        public double weight { get; set; }

        /// <summary>
        /// 包裹长度(cm)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹长度(cm)")]
        public double length { get; set; }

        /// <summary>
        /// 包裹宽度(cm)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹宽度(cm)")]
        public double width { get; set; }

        /// <summary>
        /// 包裹高度(cm)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹高度(cm)")]
        public double height { get; set; }

        /// <summary>
        /// 海关编码
        /// </summary>
        public string HsCode { get; set; }

        /// <summary>
        /// 运输方式
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("运输方式")]
        public enumTransportType TransportType { get; set; }

        /// <summary>
        /// 货运方式
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货运方式")]
        public string ShipMethod { get; set; }

        /// <summary>
        /// 货物类型
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货物类型")]
        public enumGoodsType GoodsType { get; set; }

        /// <summary>
        /// 申报总价($)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("申报总价")]
        public double Price { get; set; }

        [Required(ErrorMessage = "{0} 不能为空")]
        /// <summary>
        /// 币种
        /// </summary>
        public string Currency { get; set; }
    }

    public enum enumTransportType
    {
        [EnumExtension.Description("大货服务(FBA)")]
        FBA, 
        [EnumExtension.Description("小包服务(LP)")]
        LP,
    }

    public enum enumGoodsType
    {
        [EnumExtension.Description("普货")]
        CommonGoods,
        [EnumExtension.Description("带电")]
        Electrified,
        [EnumExtension.Description("带磁")]
        MagneticBelt,
        [EnumExtension.Description("带电带磁")]
        ChargedMagneticBelt,
    }
}
