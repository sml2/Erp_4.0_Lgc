﻿using Newtonsoft.Json;

namespace ERP.Models.Api.Logistics.KJHY
{
    public class CommonParameter : RequestLogistic
    {      
        public CommonParameter(Parameter p)
        {
            this.appid = p.appid;
            this.appsecret = p.appsecret;
            this.accessToken = p.accessToken;
            this.expires_in = p.expires_in;
        }

        /// <summary>
        /// 用户唯一凭证
        /// </summary>
        public string appid { get; set; }
        /// <summary>
        /// 用户凭证秘钥
        /// </summary>
        public string appsecret { get; set; }
        /// <summary>
        /// 到期时间(秒)
        /// </summary>
        public  int expires_in { get; set; }

        public string accessToken { get; set; }

        [JsonIgnore]
        public string Platform { get; set; } = string.Empty;
    }
}
