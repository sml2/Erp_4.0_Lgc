﻿namespace ERP.Models.Api.Logistics.KJHY
{
    public class AccessTokenRequest : CommonParameter
    {
        public AccessTokenRequest(Parameter p) : base(p)
        {

        }

        public override HttpMethod Method => HttpMethod.Get;

        public override string Url => $"https://open.kjhaoyun.com/api/v1/token?appid={appid}&appsecret={appsecret}";
    }
}
