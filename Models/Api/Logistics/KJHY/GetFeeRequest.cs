﻿namespace ERP.Models.Api.Logistics.KJHY
{
    public class GetFeeRequest
    {
        public GetFeeRequest() { }
        public GetFeeRequest(string waybillNos, string dataDigest, string ecCompanyId)
        {
            this.waybillNos = waybillNos;
            this.dataDigest = dataDigest;
            this.ecCompanyId = ecCompanyId;
        }
        public string waybillNos { get; set; } = string.Empty;
        public string dataDigest { get; set; } = string.Empty;
        public string ecCompanyId { get; set; } = string.Empty;
    }
}
