﻿namespace ERP.Models.Api.Logistics.KJHY
{
    public class KJHYParameters
    {
        /// <summary>
        /// 货运方式
        /// </summary>
        public string ShipCode { get; set; } = string.Empty;

        /// <summary>                            
        /// 运输方式                             
        /// </summary>                           
        public string TransportType { get; set; } = string.Empty;

        /// <summary>                            
        /// 货物类型                             
        /// </summary>                           
        public string  GoodsType { get; set; } = string.Empty;

        /// <summary>                            
        /// 税号                                 
        /// </summary>                           
        public string TaxNumber { get; set; } = string.Empty;  
    }
}
