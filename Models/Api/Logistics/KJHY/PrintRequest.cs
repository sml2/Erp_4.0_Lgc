﻿namespace ERP.Models.Api.Logistics.KJHY
{
    public class PrintRequest : CommonParameter
    {
        public PrintRequest(Parameter p) : base(p)
        {

        }

        public override string Url => $"https://my.ems.com.cn/pcpErp-web/a/pcp/surface/download";
    }
}
