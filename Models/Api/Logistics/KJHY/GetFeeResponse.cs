﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ERP.Models.Api.Logistics.KJHY
{
    public class GetFeeResponse
    {
        public string return_success { get; set; }=string.Empty;
        public string return_reason { get; set; } = string.Empty;
        public string return_msg { get; set; } = string.Empty;
        public List<Data> data { get; set; }=new();
        public class Data
        {

            public double postageTotal { get; set; }

            public double realWeight { get; set; }

            public string traceNo { get; set; } = string.Empty;

            public string postDate { get; set; } = string.Empty;
        }
    }
}
