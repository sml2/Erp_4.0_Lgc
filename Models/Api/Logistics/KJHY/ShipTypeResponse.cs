﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ERP.Models.Api.Logistics.KJHY
{
    public class ShipTypeResponse 
    {
        public string code { get; set; } = string.Empty;
        public string msg { get; set; } = string.Empty;
        public DataItem data { get; set; } = null!;
        public class DataItem
        {
            public List<transportsItem> transports { get; set; } = new();
            public class transportsItem
            {
                public string id { get; set; } = string.Empty;
                public string name { get; set; } = string.Empty;
                public string type { get; set; } = string.Empty;
                public string service_provider { get; set; } = string.Empty;
                public override string ToString()
                {
                    return $"[{id}]{name}";
                }
            }
        }      
    }
}
