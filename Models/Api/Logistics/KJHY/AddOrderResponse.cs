﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.KJHY
{
    public class AddOrderResponse
    {
        public string code { get; set; } = string.Empty;
        /// <summary>
        /// 请求成功
        /// </summary>
        public string msg { get; set; } = string.Empty;

        public Data data { get; set; } = null!;
        public class Data
        {
            public List<OrdersItem> orders { get; set; } = new();
            public class OrdersItem
            {
                public string code { get; set; } = string.Empty;

                public string order_id { get; set; } = string.Empty;

                public string order_number { get; set; } = string.Empty;

                public string tracking_number { get; set; } = string.Empty;

                public string label_A4_url { get; set; } = string.Empty;

                public string label_10X10_url { get; set; } = string.Empty;
                /// <summary>
                /// 跨境好运
                /// </summary>
                public string msg { get; set; } = string.Empty;
            }
        }     
    }
}
