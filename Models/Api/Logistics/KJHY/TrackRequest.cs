﻿using ERP.Models.Api.Logistics.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.KJHY
{
    public class TrackRequest:CommonParameter
    {
        public TrackRequest(Parameter p,List<string> customNumbers) : base(p)
        {
            order_ids = customNumbers;
        }

        public TrackRequest(Parameter p, string customNumber) : base(p)
        {
            order_ids = new List<string>() { customNumber };
        }

        public List<string> order_ids { get; set; }

        public override string Url => "https://open.kjhaoyun.com/api/v1/orders/info";     

        public override SerializableDictionary<string, string> headers
        {
            get
            {
                SerializableDictionary<string, string> header = new SerializableDictionary<string, string>();
                header.Add("access_token", accessToken);
                return header;
            }
        }
    }
}
