﻿using ERP.Models.Api.Logistics.Extension;

namespace ERP.Models.Api.Logistics.KJHY
{
    public class ShipTypeRequest : CommonParameter
    {
        public ShipTypeRequest(Parameter p,string transportType) : base(p)
        {
            TransportType = transportType;
        }

        /// <summary>
        /// 运输方式
        /// </summary>
        public string TransportType { get; set; }

        public override string Url => $"https://open.kjhaoyun.com/api/v1/transport/list?transportType={TransportType}";
        
        public override SerializableDictionary<string, string> headers
        {
            get
            {
                SerializableDictionary<string, string> header = new SerializableDictionary<string, string>();
                header.Add("access_token", accessToken);
                return header;
            }
        }
    }
}
