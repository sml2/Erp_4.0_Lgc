﻿namespace ERP.Models.Api.Logistics.KJHY
{
    public class AccessTokenResponse
    {
        public string access_token { get; set; } = "";
        /// <summary>
        /// 过期时间：秒，默认两个小时
        /// </summary>
        public int expires_in { get; set; }
    }
}
