﻿namespace ERP.Models.Api.Logistics.KJHY;

public class TrackResponse
{
    public string code { get; set; } = string.Empty;
    /// <summary>
    /// 请求成功
    /// </summary>
    public string msg { get; set; } = string.Empty;

    public Data data { get; set; } = null!;
    public class Data
    {
        public List<OrdersItem> orders { get; set; } = new();
        public class OrdersItem
        {
            public string code { get; set; } = string.Empty;

            public string order_id { get; set; } = string.Empty;

            public string msg { get; set; } = string.Empty;

            public int order_status { get; set; }

            public int pay_status { get; set; }

            public string tracking_number { get; set; } = string.Empty;

            public int actual_total_weight { get; set; }

            public int cahrge_weight { get; set; }

            public List<Charge_detailsItem> charge_details { get; set; } = new();//Count
            public class Charge_detailsItem
            {
                /// <summary>
                /// 物流费
                /// </summary>
                public string charge_name { get; set; } = string.Empty;

                public string amount { get; set; } = string.Empty;
            }
            public double TotalFee
            {
                get
                {
                    double TempFee = 0;
                    if (charge_details.IsNotNull() && charge_details.Count > 0)
                    {
                        charge_details.ForEach((r) => TempFee += Convert.ToDouble(r.amount));
                    }
                    return TempFee;
                }
            }
            public List<CartonsItem> cartons { get; set; } = new();
            public class CartonsItem
            {
                /// <summary>
                /// 箱号:按照1、2、3递增
                /// </summary>
                public string number { get; set; } = string.Empty;
                /// <summary>
                /// 长
                /// </summary>
                public string length { get; set; } = string.Empty;
                /// <summary>
                /// 宽
                /// </summary>
                public string width { get; set; } = string.Empty;
                /// <summary>
                /// 高
                /// </summary>
                public string height { get; set; } = string.Empty;
                /// <summary>
                /// 重量
                /// </summary>
                public string weight { get; set; } = string.Empty;
                /// <summary>
                /// 实际长
                /// </summary>
                public string actual_length { get; set; } = string.Empty;
                /// <summary>
                /// 实际宽
                /// </summary>
                public string actual_width { get; set; } = string.Empty;
                /// <summary>
                /// 实际高
                /// </summary>
                public string actual_height { get; set; } = string.Empty;
                /// <summary>
                /// 实际重量
                /// </summary>
                public string actual_weight { get; set; } = string.Empty;
            }

            public List<Node_logsItem> node_logs { get; set; } = new();
            public class Node_logsItem
            {
                /// <summary>
                /// 已测量
                /// </summary>
                public string node_info { get; set; } = string.Empty;

                public string node_time { get; set; } = string.Empty;

                public string node_estimate_time { get; set; } = string.Empty;
            }

            public List<Logistics_logsItem> logistics_logs { get; set; } = new();
            public class Logistics_logsItem
            {
                /// <summary>
                /// [Louisville, KY, US]抵达设施
                /// </summary>
                public string logistics_info { get; set; } = string.Empty;

                public string logistics_time { get; set; } = string.Empty;
            }
        }
    }       
}
