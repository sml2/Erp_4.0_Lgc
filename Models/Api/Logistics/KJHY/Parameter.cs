﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

using ERP.Services.Api.Logistics;
using ERP.Attributes.Logistics;

namespace ERP.Models.Api.Logistics.KJHY
{      
    public class Parameter: BaseParam
    {
        private string id;
        private string secret;
        [ViewConfigParameter(nameof(appid), $"凭证[{nameof(appid)}]", Sort = 0)]
        /// <summary>
        /// 用户唯一凭证
        /// </summary>
        public string appid { get { return id; } set { id = value.Trim(); } } 
        [ViewConfigParameter(nameof(appsecret), $"秘钥[{nameof(appsecret)}]", Sort = 1)]
        /// <summary>
        /// 用户凭证秘钥
        /// </summary>
        public string appsecret { get { return secret; } set { secret = value.Trim(); } } 
        /// <summary>
        /// 到期时间(秒)
        /// </summary>
        public  int expires_in { get; set; }
        /// <summary>
        /// 记录获取到accessToken的时间
        /// </summary>
        public DateTime Time { get; set; }
        private string _accessToken { get; set; } = string.Empty;

        public string TransportType { get; set; } = string.Empty;

        public string accessToken { get; set; } = string.Empty;
    }
}
