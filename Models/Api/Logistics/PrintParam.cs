﻿namespace ERP.Models.Api.Logistics
{
    public class PrintParam
    {      
        /// <summary>
        /// 本系统订单号
        /// </summary>
        public string CustomNumber { get; set; } = string.Empty;

        /// <summary>
        /// SF 追踪号
        /// </summary>
        public string? TrackNumber { get; set; }

        public string? LogictisNumber { get; set; }  
        
        public string? Express { get; set; }
    }


}
