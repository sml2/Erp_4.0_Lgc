﻿namespace ERP.Models.Api.Logistics;

public class PlatformMethodResult
{
    public PlatformMethodResult(PlatformMethod p)
    {
        success = true;
        PlatformMethod = p;
    }
    public PlatformMethodResult(Exception e)
    {
        success = false;
        exception = e;
    }
    public bool success { get; set; }
    public Exception exception { get; set; } = new();

    public PlatformMethod PlatformMethod { get; set; } = new(); 
}
public class PlatformMethod
{    
    public PlatformMethod(){}
    public PlatformMethod(Platforms platforms, bool sendEnabled, bool sendReportEnabled, bool trackEnabled, bool shipTypeEnabled,
        bool warehouseEnabled, bool getCountryEnabled, bool estimatePriceEnabled, bool printEnabled,bool cancleEnabled, bool channelCode,
        bool contentType, bool fileType, bool paperType,bool isUrl)
    {
        this.platforms = platforms;
        SendEnabled = sendEnabled;
        SendReportEnabled = sendReportEnabled;
        TrackEnabled = trackEnabled;
        ShipTypeEnabled = shipTypeEnabled;
        WarehouseEnabled = warehouseEnabled;
        GetCountryEnabled = getCountryEnabled;
        EstimatePriceEnabled = estimatePriceEnabled;
        PrintEnabled = printEnabled;
        CancleEnabled = cancleEnabled;
        ChannelCode = channelCode;
        ContentType = contentType;
        FileType = fileType;
        PaperType = paperType;
        IsUrl = isUrl;
    }

    public Platforms platforms { get; set; }
    public bool SendEnabled { get; set; }

    public bool SendReportEnabled { get; set; }

    public bool TrackEnabled { get; set; }

    public bool ShipTypeEnabled { get; set; }

    public bool WarehouseEnabled { get; set; }

    public bool GetCountryEnabled { get; set; }

    public bool EstimatePriceEnabled { get; set; }

    public bool PrintEnabled { get; set; }

    public bool CancleEnabled { get; set; }

    public bool IsUrl { get; set; }

    public bool ChannelCode { get; set; }
    public bool ContentType { get; set; }
  
    /// <summary>
    /// 面单文件类型
    /// </summary>
    public  bool FileType { get; set; }
    /// <summary>
    /// 面单纸张类型
    /// </summary>
    public  bool PaperType { get; set; }

}