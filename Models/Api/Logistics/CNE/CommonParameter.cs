﻿
using Newtonsoft.Json;
using static ERP.Models.Api.Logistics.CNE.ExtParameter;

namespace ERP.Models.Api.Logistics.CNE
{
    /// <summary>
    /// 接口相关的公共参数
    /// </summary>
    public abstract class CommonParameter: RequestLogistic
    {       
        public CommonParameter(Parameter p, Platforms platform)
       {            
            this.icID = p.icID;
            TimeStamp = Helpers.GetTimeStamp();
            MD5 = Sy.Security.MD5.Encrypt(icID.ToString() + TimeStamp + p.SecretKey).ToLower();
            Platform = platform;
            if (platform == Platforms.CNE_HZGJ)
            {
                DoMain = $"http://47.115.54.35";
            }
            else
            {
                DoMain = $"http://api.cne.com";
            }            
        }   
          
        public abstract string RequestName { get;}

        public string  icID { get; set; }

        public long TimeStamp { get; set; }

        public string MD5 { get; set; }
        
        [JsonIgnore]
        public Platforms Platform { get; set; }

        [JsonIgnore]
        public string DoMain { get; set; }
    }
}
