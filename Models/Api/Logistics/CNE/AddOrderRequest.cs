
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.CNE
{
    public class AddOrderRequest : CommonParameter
    {       
        public AddOrderRequest(Parameter p,Platforms platform): base(p, platform)
        {

        }            
        public override string RequestName { get => "PreInputSet"; }
              
        public override string Url { get => base.DoMain+ "/cgi-bin/EmsData.dll?DoApi"; }
        
        public override HttpMethod Method { get => HttpMethod.Post;}

        //public override string ContentType => "application/x-www-form-urlencoded";

        public List<Cls_RecList> RecList { get; set; } = new();              
        public enum EnumnItemType
        {
            /// <summary>
            /// 文件
            /// </summary>
            Document = 0,
            /// <summary>
            /// 包裹
            /// </summary>
            Package = 1,
            /// <summary>
            /// 防水袋
            /// </summary>
            WaterproofBag = 2,
        }
    }

    public class Cls_RecList
    {
        public Cls_RecList(SenderInfo senderInfo, OrderInfo orderInfo, ExtParameter ext)
        {           
            #region
            switch (ext.taxType)
            {
                case ExtParameter.enumTax.iossCode:
                    this.iossCode = ext.taxNumber;
                    break;
                case ExtParameter.enumTax.vatCode:
                    this.vatCode = ext.taxNumber;

                    break;
                case ExtParameter.enumTax.cRTaxCode:
                    this.cRTaxCode = ext.taxNumber;
                    break;
            }
            Id = orderInfo.id;
            orderNo = orderInfo.OrderNo;
            this.cEmsKind = ext.ShipMethod;
            this.cRNo = Helpers.GetOrderNo(orderInfo.OrderNo, orderInfo.SendNum, orderInfo.FailNum); //orderInfo.OrderNo;
            this.cDes = orderInfo.ReceiverAddress.NationShort;
            this.fWeight =Helpers.RoundData(ext.fWeight/1000,2);
            this.iLong = ext.iLong;
            this.iWidth = ext.iWidth;
            this.iHeight = ext.iHeight;
            this.cReceiver = orderInfo.ReceiverAddress.Name;
            this.cRUnit = "";
            string Address = orderInfo.ReceiverAddress.Address;
            if (orderInfo.ReceiverAddress.Address2.IsNotWhiteSpace()) Address += $" {orderInfo.ReceiverAddress.Address2}";
            if (orderInfo.ReceiverAddress.Address3.IsNotWhiteSpace()) Address += $" {orderInfo.ReceiverAddress.Address3}";
            this.cRAddr = Address;
            this.cRCity = orderInfo.ReceiverAddress.City;
            this.cRProvince = orderInfo.ReceiverAddress.Province;
            this.cRPostcode = orderInfo.ReceiverAddress.Zip ?? string.Empty; 
            this.cRCountry = orderInfo.ReceiverAddress.NationShort ?? string.Empty;
            this.cRPhone = orderInfo.ReceiverAddress.Phone;
            this.cRSms = orderInfo.ReceiverAddress.Phone;
            this.cREMail = orderInfo.ReceiverAddress.Email ?? string.Empty;
            //sender
            this.cSender = senderInfo.Name;
            this.cSUnit = "";
            this.cSAddr = senderInfo.Address;
            this.cSCity = senderInfo.City;
            this.cSProvince = senderInfo.Province;
            this.cSPostCode = senderInfo.PostalCode;
            this.YcSCountry = senderInfo.Region ?? string.Empty;
            this.cSPhone = senderInfo.PhoneNumber;
            this.cSSms = senderInfo.PhoneNumber;
            this.cSEmail = senderInfo.Mail;
            //商品
            var goodsList = new List<Cls_RecList.Cls_GoodsList>();
            int num = orderInfo.GoodInfos.Sum((r) => r.Num);
            orderInfo.GoodInfos.ForEach((r) =>
            {
                Cls_RecList.Cls_GoodsList Good = new Cls_RecList.Cls_GoodsList(
                    ext.ProNameCn,
                    ext.ProNameEn, r.Count, Convert.ToDecimal(Helpers.RoundData(ext.fxPrice/r.Num)), ext.Currency, cxGCodeA: r?.Sku, ext.HasCode);
                goodsList.Add(Good);
            });
            this.GoodsList = goodsList;
            #endregion
        }

        [JsonIgnore]
        public string orderNo { get; set; }

        [JsonIgnore]
        public int Id { get; set; }

        /// <summary>
        /// 快件类型
        /// </summary>                        
        [DisplayName("快件类型")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public int nItemType { get; set; } = 1;

        /// <summary>
        /// 渠道编码
        /// </summary>
        [DisplayName("货运方式")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string cEmsKind { get; set; }

        /// <summary>
        /// 订单所属平台
        /// </summary>            
        [DisplayName("订单所属平台")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string cAddrFrom { get; set; } = "OTHER";


        /// <summary>
        /// 件数，默认1,此为单个订单包裹数量，非SKU数量,(大于1时，为一票多件业务,暂不支持)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("单个订单包裹数量")]
        public int iItem { get; set; } = 1;


        /// <summary>
        /// 客户系统订单号
        /// </summary>
        [MaxLength(32, ErrorMessage = "最长只能输入32位，且只允许数字、字母、中划线")]
        [DisplayName("客户系统订单号")]
        public string cRNo { get; set; }


        /// <summary>
        /// 收件人国家:ISO 二字码 
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [StringLength(2, ErrorMessage = "请填入两位国家ISO二字码")]
        public string cDes { get; set; }

        /// <summary>
        /// 收件人姓名
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [StringLength(64, MinimumLength = 3, ErrorMessage = "收件人姓名字符长度只能在（3-64）范围内")]
        [DisplayName("收件人姓名")]
        public string cReceiver { get; set; }


        /// <summary>
        /// 收件人公司
        /// </summary>
        [StringLength(64, ErrorMessage = "收件人公司字符长度不能超过64")]
        [DisplayName("收件人公司")]
        public string cRUnit { get; set; }

        /// <summary>
        /// 收件人地址
        /// </summary>
        [StringLength(256, MinimumLength = 5, ErrorMessage = "收件人地址字符长度只能在（5-256）范围内")]
        [DisplayName("收件人地址")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string cRAddr { get; set; }

        /// <summary>
        /// 收件人城市
        /// </summary>
        [StringLength(64, MinimumLength = 3, ErrorMessage = "收件人城市字符长度只能在（3-64）范围内")]
        [DisplayName("收件人城市")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string cRCity { get; set; }

        /// <summary>
        /// 收件人省州
        /// </summary>
        [StringLength(64, ErrorMessage = "收件人省州字符长度不能超过64")]
        [DisplayName("收件人省州")]
        public string cRProvince { get; set; }


        /// <summary>
        /// 收件人邮编
        /// </summary>
        [StringLength(16, ErrorMessage = "收件人邮编字符长度不能超过16")]
        [DisplayName("收件人邮编")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string cRPostcode { get; set; }


        /// <summary>
        /// 收件人国家
        /// </summary>
        [StringLength(64, ErrorMessage = "收件人国家字符长度不能超过64，且需是国家英文名称")]
        [DisplayName("收件人国家")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string cRCountry { get; set; }

        /// <summary>
        /// 收件人电话
        /// </summary>
        [StringLength(32, ErrorMessage = "收件人电话字符长度不能超过32")]
        [DisplayName("收件人电话")]
        public string cRPhone { get; set; }


        /// <summary>
        /// 收件人手机
        /// </summary>
        [StringLength(32, ErrorMessage = "收件人手机字符长度不能超过32")]
        [DisplayName("收件人手机")]
        public string cRSms { get; set; }


        /// <summary>
        /// 收件人邮箱
        /// </summary>
        [StringLength(128, ErrorMessage = "收件人邮箱字符长度不能超过128")]
        [DisplayName("收件人邮箱")]
        public string cREMail { get; set; }


        /// <summary>
        /// 收件人税号
        /// </summary>
        [DisplayName("收件人税号")]
        public string cRTaxCode { get; set; } = string.Empty;

        /// <summary>
        /// 包裹申报重量，单位KG
        /// </summary>            
        [Range(typeof(decimal), "0.00001", "99999.999", ErrorMessage = "包裹申报重量不能超出范围[0.00001-99999.999]，单位G")]
        [DisplayName("包裹申报重量")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public decimal fWeight { get; set; }


        /// <summary>
        /// 长，单位：cm
        /// </summary>
        [Range(typeof(decimal), "0.01", "99999999.99", ErrorMessage = "包裹申报长度范围不能超出[0.01-99999999.99]，单位cm")]
        [DisplayName("长")]
        public decimal iLong { get; set; }


        /// <summary>
        /// 宽
        /// </summary>
        [Range(typeof(decimal), "0.01", "99999999.99", ErrorMessage = "包裹申报宽度范围不能超出[0.01-99999999.99]，单位cm")]
        [DisplayName("宽")]
        public decimal iWidth { get; set; }

        /// <summary>
        /// 高
        /// </summary>
        [Range(typeof(decimal), "0.01", "99999999.99", ErrorMessage = "包裹申报高度范围不能超出[0.01-99999999.99]，单位cm")]
        [DisplayName("高")]
        public decimal iHeight { get; set; }


        //未实现：cMark 部分label标签标记,cMemo 发货备注            

        /// <summary>
        /// 接口提供商
        /// </summary>
        [DisplayName("接口提供商")]
        public string NcReserve { get; set; } = "waimaomvp";

        /// <summary>
        /// 英国VAT税号、欧盟国家使用的增值税号
        /// </summary>
        [StringLength(32, ErrorMessage = "VAT税号字符长度不能超过32")]
        [DisplayName("VAT税号")]
        public string vatCode { get; set; } = string.Empty;

        /// <summary>
        /// ioss号码
        /// </summary>
        [StringLength(12, ErrorMessage = "ioss号码字符长度不能超过12")]
        [DisplayName("ioss号码")]
        public string iossCode { get; set; } = string.Empty;


        /// <summary>
        /// 寄件人姓名
        /// </summary>
        [StringLength(64, ErrorMessage = "寄件人姓名字符长度不能超过64")]
        [DisplayName("寄件人姓名")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string cSender { get; set; }


        /// <summary>
        /// 寄件人公司
        /// </summary>
        [StringLength(64, ErrorMessage = "寄件人公司字符长度不能超过64")]
        [DisplayName("寄件人公司")]
        public string cSUnit { get; set; }



        /// <summary>
        /// 寄件人地址
        /// </summary>
        [StringLength(256, ErrorMessage = "寄件人地址字符长度不能超过256")]
        [DisplayName("寄件人地址")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string cSAddr { get; set; }

        /// <summary>
        /// 寄件人城市
        /// </summary>
        [StringLength(64, ErrorMessage = "寄件人城市字符长度不能超过64")]
        [DisplayName("寄件人城市")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string cSCity { get; set; }


        /// <summary>
        /// 寄件人省州
        /// </summary>
        [StringLength(64, ErrorMessage = "寄件人省州字符长度不能超过64")]
        [DisplayName("寄件人省州")]
        public string cSProvince { get; set; } = string.Empty;


        /// <summary>
        /// 寄件人邮编
        /// </summary>
        [StringLength(32, ErrorMessage = "寄件人邮编字符长度不能超过32")]
        [DisplayName("寄件人邮编")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string cSPostCode { get; set; }=string.Empty;


        /// <summary>
        /// 寄件人国家
        /// </summary>
        [StringLength(32, ErrorMessage = "寄件人国家字符长度不能超过32，且需是国家英文名称")]
        [DisplayName("寄件人国家")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string YcSCountry { get; set; }



        /// <summary>
        /// 寄件人电话
        /// </summary>
        [StringLength(32, ErrorMessage = "寄件人电话字符长度不能超过32")]
        [DisplayName("寄件人电话")]
        public string cSPhone { get; set; }


        /// <summary>
        /// 寄件人手机
        /// </summary>
        [StringLength(32, ErrorMessage = "寄件人手机字符长度不能超过32")]
        [DisplayName("寄件人手机")]
        public string cSSms { get; set; }


        /// <summary>
        /// 寄件人邮箱
        /// </summary>
        [StringLength(maximumLength:32, ErrorMessage = "寄件人邮箱字符长度不能超过32")]
        [DisplayName("收件人邮箱")]
        public string cSEmail { get; set; }


        //未实现：returnAddress  退件地址， labelContent   打印参数


        public List<Cls_GoodsList> GoodsList { get; set; }

        public class Cls_GoodsList
        {
            public Cls_GoodsList(string cxGoods, string cxGoodsA, int ixQuantity, decimal fxPrice, string cxMoney,
                string? cxGCodeA, string hsCode)
            {
                this.cxGoods = cxGoods;
                this.cxGoodsA = cxGoodsA;
                this.ixQuantity = ixQuantity;
                this.fxPrice = fxPrice;
                this.cxMoney = cxMoney;
                this.cxGCodeA = cxGCodeA ?? "";
                this.hsCode = hsCode;
            }

            /// <summary>
            /// 产品中文描述
            /// </summary>
            [StringLength(64, ErrorMessage = "产品中文描述字符长度不能超过64")]
            [DisplayName("产品中文描述")]
            [Required(ErrorMessage = "{0} 不能为空")]
            public string cxGoods { get; set; }

            /// <summary>
            /// 产品英文描述
            /// </summary>
            [StringLength(64, ErrorMessage = "产品英文描述字符长度不能超过64")]
            [DisplayName("产品英文描述")]
            [Required(ErrorMessage = "{0} 不能为空")]
            public string cxGoodsA { get; set; }

            /// <summary>
            /// 产品数量 
            /// </summary>
            [MinLength(1, ErrorMessage = "产品数量最少不能小于1")]
            [DisplayName("产品数量")]
            [Required(ErrorMessage = "{0} 不能为空")]
            public int ixQuantity { get; set; }


            /// <summary>
            /// 产品单价
            /// </summary>
            [Range(typeof(decimal), "0.01", "999999.99", ErrorMessage = "产品单价范围不能超出[0.01-999999.99]")]
            [DisplayName("产品单价")]
            public decimal fxPrice { get; set; }

            /// <summary>
            /// 申报币种
            /// </summary>           
            [DisplayName("申报币种")]
            [Required(ErrorMessage = "{0} 不能为空")]
            public string cxMoney { get; set; }

            /// <summary>
            /// 产品sku
            /// </summary>                
            [DisplayName("产品sku")]
            public string cxGCodeA { get; set; }

            /// <summary>
            /// 海关编码
            /// </summary>               
            [DisplayName("海关编码")]
            public string hsCode { get; set; }
        }

    }
}
