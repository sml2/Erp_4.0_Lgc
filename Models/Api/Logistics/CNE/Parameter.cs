﻿using ERP.Attributes.Logistics;
namespace ERP.Models.Api.Logistics.CNE;


public class  Parameter: BaseParam
{
    private string id;
    private string key;
    [ViewConfigParameter(nameof(icID))]
    public string icID { get { return id; } set { id = value.Trim(); } } 
    [ViewConfigParameter(nameof(SecretKey))]
    public string SecretKey { get { return key; } set { key = value.Trim(); } } 
}
