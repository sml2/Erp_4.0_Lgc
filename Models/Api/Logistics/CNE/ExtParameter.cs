﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static EnumExtension;

namespace ERP.Models.Api.Logistics.CNE
{
    public class ExtParameter: BaseExtParam
    {
        public ExtParameter(string proNameCn, string proNameEn, decimal fWeight, decimal iLong, decimal iWidth, decimal iHeight,
            string hasCode, double fxPrice, string taxType, string taxNumber, string shipMethod)
        {
            this.ProNameCn = proNameCn;
            this.ProNameEn = proNameEn;
            this.fWeight = fWeight;
            this.iLong = iLong;
            this.iWidth = iWidth;
            this.iHeight = iHeight;
            this.ShipMethod = shipMethod;
            this.HasCode = hasCode;
            this.fxPrice = fxPrice;
            this.taxNumber = taxNumber;
            if (Enum.IsDefined(typeof(enumTax), taxType) && Enum.TryParse<enumTax>(taxType, out var taxTypeEnum))
            {
                this.taxType = taxTypeEnum;
            }
            else
            {
                Console.WriteLine("CNE ExtParameter taxType ERROR:"+ taxType);
            }
        }

        /// <summary>
        /// 货品申报名(中文)
        /// </summary>
        [DisplayName("货品申报名(中文)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string ProNameCn { get; set; }

        /// <summary>
        /// 货品申报名(英文)
        /// </summary>
        [DisplayName("货品申报名(英文)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string ProNameEn { get; set; }

        /// <summary>
        /// 海关编码
        /// </summary>
        [DisplayName("海关编码")]
        public string HasCode { get; set; }



        /// <summary>
        /// 三种税号类型
        /// </summary>
        public enumTax taxType { get; set; }

        /// <summary>
        /// 税号
        /// </summary>
        [DisplayName("税号")]
        //[Required(ErrorMessage = "{0} 不能为空")]
        public string taxNumber { get; set; }

        /// <summary>
        /// 包裹长度(cm)
        /// </summary>
        [DisplayName("包裹长度(cm)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public decimal iLong { get; set; }

        /// <summary>
        /// 包裹宽度(cm)
        /// </summary>
        [DisplayName("包裹宽度(cm)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public decimal iWidth { get; set; }

        /// <summary>
        /// 包裹高度(cm)
        /// </summary>
        [DisplayName("包裹高度(cm)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public decimal iHeight { get; set; }

        /// <summary>
        /// 包裹重量(kg)
        /// </summary>
        [DisplayName("包裹重量(kg)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public decimal fWeight { get; set; }
       

        /// <summary>
        /// 货运方式
        /// </summary>
        [DisplayName("货运方式")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string ShipMethod { get; set; }


        /// <summary>
        /// 申报价格($)
        /// </summary>
        [DisplayName("申报价格($)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public double fxPrice { get; set; }


        [Required(ErrorMessage = "{0} 不能为空")]
        /// <summary>
        /// 币种
        /// </summary>
        public string Currency { get; set; } 

        public enum enumTax
        {
            [EnumExtension.Description("iossCode")]
            iossCode,
            [EnumExtension.Description("vatCode")]
            vatCode,
            [EnumExtension.Description("cRTaxCode")]
            cRTaxCode,
        }       
    }
}
