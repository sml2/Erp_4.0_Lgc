﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.CNE
{
    public class EstimatePriceResponse
    {
        public bool success { get; set; }

        public object code { get; set; } = new();

        public object msg { get; set; } = new();

        public Cls_Data data { get; set; } = new();
        public class Cls_Data
        {

            public int ReturnValue { get; set; }

            public object cMess { get; set; } = new();

            public List<Cls_RecList> RecList { get; set; } = new();
            public class Cls_RecList
            {
                public string cEmsKind { get; set; } = string.Empty;
                public string cEmsKindi { get; set; } = string.Empty;
                /// <summary>
                /// 报价总额
                /// </summary>
                public double fPrice { get; set; }

                public object fsPrice { get; set; } = new();
                /// <summary>
                /// 燃油费用
                /// </summary>
                public double fAddOn { get; set; }
                /// <summary>
                /// 报关费用
                /// </summary>
                public double fCustoms { get; set; }

                public double fOther { get; set; }

                public object fDiscount { get; set; } = new();

                public int nDaysB { get; set; }

                public int nDaysE { get; set; }

                public object fcWeight { get; set; } = new();

                public object fcLong { get; set; } = new();

                public object fcCubic { get; set; } = new();

                public string cMemo { get; set; } = string.Empty;

                public string cMethod { get; set; } = string.Empty;

                public string cStation { get; set; } = string.Empty;
                public override string ToString()
                {
                    return $"\r\n货运方式为[{cEmsKind}]的费用如下:\r\n报价总额:{fPrice}\r\n燃油费用:{fAddOn}\r\n报关费用:{fCustoms}\r\n";
                }
            }
        }
    }
}
