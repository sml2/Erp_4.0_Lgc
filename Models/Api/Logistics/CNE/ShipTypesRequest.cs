﻿using Newtonsoft.Json;

namespace ERP.Models.Api.Logistics.CNE
{
    public class ShipTypesRequest : CommonParameter
    {
        public ShipTypesRequest(Parameter p, Platforms platform) : base(p, platform)
        {

        }      
        public override string Url { get => base.DoMain + "/cgi-bin/EmsData.dll?DoApi"; }
      
        public override HttpMethod Method { get => HttpMethod.Post; }

        public override string RequestName => "EmsKindList";

        public override string ToPayload()
        {
            string json = JsonConvert.SerializeObject(this);
            return json;
        }
    }
}
