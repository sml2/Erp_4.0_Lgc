﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.CNE
{
    public class EstimatePriceRequest
    {
        [JsonIgnore]
        public virtual string URL => "";

        [JsonIgnore]
        public virtual HttpMethod Method => HttpMethod.Post;
        public string cqDes { get; set; } = string.Empty;

        public int nqItemType { get; set; } 

        public string fqWeight { get; set; } = string.Empty;

        public string fqLong { get; set; } = string.Empty;

        public string fqWidth { get; set; } = string.Empty;

        public string fqHeight { get; set; } = string.Empty;
    }
}
