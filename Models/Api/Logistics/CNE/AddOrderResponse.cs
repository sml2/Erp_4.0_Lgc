﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ERP.Exceptions.Logistics;

namespace ERP.Models.Api.Logistics.CNE
{
    public class AddOrderResponse
    {
        /// <summary>
        /// 状态,大于0：成功小于等于0：失败
        /// </summary>
        public int ReturnValue { get; set; }

        /// <summary>
        /// 下单时，成功下单的数量
        /// </summary>
        public int OK { get; set; }

        /// <summary>
        /// 错误描述
        /// </summary>
        public string cMess { get; set; } = string.Empty;

        /// <summary>
        /// 返回数据内容
        /// </summary>
        public List<Cls_ErrList> ErrList { get; set; } = new();
        public class Cls_ErrList
        {
            /// <summary>
            /// 数组索引值,必有
            /// </summary>
            public int iIndex { get; set; }

            /// <summary>
            /// 唯一记录ID。 （记录 ID，一条预录单记录，都有一个唯一的不可更改的 iID,系统识别码，对于记录的删除、修改则以此识别。），,必有 
            /// </summary>
            public long iID { get; set; }

            /// <summary>
            /// CNE内部单号，,必有
            /// </summary>
            public string cNum { get; set; } = string.Empty;

            /// <summary>
            /// 运单号，物流追踪号码，,必有
            /// </summary>
            public string cNo { get; set; } = string.Empty;

            /// <summary>
            /// 客户系统下单时传过来的单号
            /// </summary>
            public string cRNo { get; set; } = string.Empty;

            /// <summary>
            /// 平台单号
            /// </summary>
            public string cPNo { get; set; } = string.Empty;

            /// <summary>
            /// 错误描述
            /// </summary>
            public string cMess { get; set; } = string.Empty;


            public string printUrl { get; set; } = string.Empty;
        }    
    }
}
