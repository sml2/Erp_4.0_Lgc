﻿namespace ERP.Models.Api.Logistics.CNE
{
    public class GetDestinationResponse
    {
        public bool success { get; set; }

        public object code { get; set; } = new();

        public string msg { get; set; } = string.Empty;

        public List<Cls_Data> data { get; set; } = new();
        public class Cls_Data
        {

            public string id { get; set; } = string.Empty;

            public string countryNameEn { get; set; } = string.Empty;

            public string countryNameCh { get; set; } = string.Empty;

            public object countryName2 { get; set; } = new();

            public object countryName3 { get; set; } = new();

            public object countryName4 { get; set; } = new();

            public string countryCodeD { get; set; } = string.Empty;

            public string countryCodeT { get; set; } = string.Empty;

            public string countryNum { get; set; } = string.Empty;

            public object area { get; set; } = new();

            public string currency { get; set; } = string.Empty;

            public bool needCity { get; set; }

            public bool needState { get; set; }

            public string zipFormat { get; set; } = string.Empty;

            public double valueLimit { get; set; }

            public double valueLimitMin { get; set; }

            public double valueLimitMax { get; set; }
            public override string ToString()
            {
                return countryNameCh;
            }
        }
    }
}
