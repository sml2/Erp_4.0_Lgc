﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.CNE
{
    public class ShipTypesResponse
    {
        public int ReturnValue { get; set; }
        public string cMess { get; set; } = string.Empty;
        public List<Cls_List> List { get; set; } = new();    
    }
    public class Cls_List
    {
        public string oName { get; set; } = string.Empty;

        public string cName { get; set; } = string.Empty;
        public override string ToString()
        {
            return cName;
        }
    }
}
