﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.CNE
{
    public class TrackRequest : CommonParameter
    {
        public TrackRequest(Parameter p,string logNumber, Platforms platform) : base(p, platform)
        {
            this.cNo = logNumber;
        }
      
        public override string Url { get => "https://apitracking.cne.com/client/track"; }
        

        public override HttpMethod Method { get => HttpMethod.Post; }

        public override string RequestName => "ClientTrack";

        [DisplayName("物流单号")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string cNo { get; set; }

        [DisplayName("语言")]       
        public string lan { get; set; } = "en";
    }
}
