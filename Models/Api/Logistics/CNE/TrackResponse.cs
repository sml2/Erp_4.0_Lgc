﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static EnumExtension;

namespace ERP.Models.Api.Logistics.CNE
{
    public class TrackResponse
    {
        public int ReturnValue { get; set; }
        public string cMess { get; set; } = string.Empty;

        /// <summary>
        /// 订单信息
        /// </summary>
        public Cls_Response_Info Response_Info { get; set; } = new();
        public class Cls_Response_Info
        {           
            public string trackingNbr { get; set; } = string.Empty;

            public string status { get; set; } = string.Empty;

            public string deliveryDate { get; set; } = string.Empty;

            public string signature { get; set; } = string.Empty;

            public string Number_t { get; set; } = string.Empty;
        }
        public List<TrackingEventList> trackingEventList { get; set; } = new();
        public class TrackingEventList
        {            
            public DateTime date { get; set; }

            public string place { get; set; } = string.Empty;

            public string details { get; set; } = string.Empty;

            public string state { get; set; } = string.Empty;
        }
        public string Extra_Header { get; set; } = string.Empty;



        public enum EnumStatus
        {
            [Description("已入仓")]
            Warehoused =0,
            [Description("已出仓")]
            OutOfWarehouse =1,
            [Description("转运中")]
            InTransit =2,
            [Description("送达")]
            Delivered =3,
            [Description("超时")]
            Overtime =4,
            [Description("扣关")]
            Detained =5,
            [Description("地址错误")]
            AddressError =6,
            [Description("快件丢失")]
            Missing =7,
            [Description("退件")]
            Returned =8,
            [Description("其它异常")]
            OtherError =9,
            [Description("销毁")]
            Destroy =10,          
        }                       
    }
}
