﻿namespace ERP.Models.Api.Logistics.CNE
{
    /// <summary>
    /// 打印面单结果：1.pdf的bin[]   2.url   3.
    /// </summary>
    public class PrintLabelResponse
    {
        public List<string> labelUrlList { get; set; } = new();
        public int ReturnValue { get; set; }
        public string cMess { get; set; } = string.Empty;
    }
}
