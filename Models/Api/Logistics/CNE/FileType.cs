﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static EnumExtension;

namespace ERP.Models.Api.Logistics.CNE
{
    public enum FileType
    {
        [Description("PDF")]
        pdf,
        [Description("PNG")]
        png,
        [Description("JPEG")]
        jpeg,
        [Description("JPG")]
        jpg,
    }
}
