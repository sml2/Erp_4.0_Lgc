﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static EnumExtension;

namespace ERP.Models.Api.Logistics.CNE
{
    public enum PaperTypeEnum
    {
        [Description("标签10X10,有配货")]
        label10x10_1,        
        [Description("标签10X10,无配货")]
        label10x10_0,
        [Description("标签10X15,有配货")]
        label10x15_1,
        [Description("标签10X15,无配货")]
        label10x15_0,
    }
}
