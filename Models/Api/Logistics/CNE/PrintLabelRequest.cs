﻿using ERP.Models.Api.Logistics.Extension;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.CNE
{  

    /// <summary>
    /// cne
    /// </summary>
    public class PrintNewLabelRequest: CommonParameter
    {
        public PrintNewLabelRequest(Parameter p,string _ptmp,List<string> _cnos,Platforms platform) : base(p, platform)
        {
            ptemp = _ptmp;
            cNos = String.Join(',',_cnos);
            signature=Sy.Security.MD5.Encrypt(icID.ToString() + cNos + p.SecretKey).ToLower();
        }
      
        public override string Url => $"https://label.cne.com/CnePrint?icID={icID}&cNos={cNos}&ptemp={ptemp}&signature={signature}";

        /// <summary>
        /// 打印模板的名称
        /// </summary>
        public string ptemp { get; set; }

        /// <summary>
        /// 物流单号列表，多个物流单号以英文逗号分隔
        /// </summary>
        public string cNos { get; set; }

        /// <summary>
        /// icID、cNos、apiToken拼成待加密字符串，然后md5生成32位小写加密结果
        /// </summary>
        public string signature { get; set; }

        public override string RequestName => "LabelPrint";     
      
    }



    /// <summary>
    /// HZGJ
    /// </summary>
    public class PrintLabelRequest: CommonParameter
    {
        public PrintLabelRequest(Parameter p,List<string> logNumbers,string modelName,Platforms platform) : base(p, platform)
        {
            aNo = logNumbers;
            cModelName = modelName;
        }

        public override string RequestName => "sGetLabel";

        public override string Url => $"{DoMain}/cgi-bin/GInfo.dll?DoApi";

        public List<string> cNoList { get; set; }=new();

        public string fileType { get; set; } = string.Empty;

        public string labelType { get; set; } = string.Empty;

        public string pickList { get; set; } = string.Empty;
        /// <summary>
        /// 运单号类别，默认0。(0:内单号；1:转单号)
        /// </summary>
        public int iNoType { get; set; } = 0;
        /// <summary>
        /// 记录查询源选择,默认0。(0:正式快递业务记录；1:预录单记录)
        /// </summary>
        public int iTable { get; set; } = 1;
        /// <summary>
        /// PDF模板名称，1-30字节,必须。可以通过GInfo接口
        /// </summary>
        public string cModelName { get; set; } = "标准Label_10X10";

        public List<string> aNo { get; set; } =new();   
    }
}
