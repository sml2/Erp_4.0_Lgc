﻿namespace ERP.Models.Api.Logistics
{
    public class ReportParameter : BaseParameter
    {
        /// <summary>
        /// 寄件人信息
        /// </summary>
        public SenderInfo SenderInfo { get; set; } = new();

        /// <summary>
        /// 订单信息
        /// </summary>
        public OrderInfo OrderInfo { get; set; } = new();
    }
}
