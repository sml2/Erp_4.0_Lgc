﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ERP.Models.Api.Logistics.OCS
{
    public class CWBLIST
    {
        [DisplayName("运单号")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string CWB_NO { get; set; } = string.Empty;

        [DisplayName("运单类型")]
        public string? CWB_TYPE { get; set; }

        [DisplayName("服务类型")]
        public string? CWB_SERVICE { get; set; }

        [DisplayName("件数")]
        public int PCS { get; set; }

        [DisplayName("始发地三字码")]
        public string? ORIGIN { get; set; }

        [DisplayName("目的地三字码")]
        public string? DEST { get; set; }

        [DisplayName("申报金额")]
        public decimal VALUE { get; set; }

        [DisplayName("金额单位")]
        public string? VALUE_CUR { get; set; }

        [DisplayName("付款方式")]
        public string? PAYMENT { get; set; }

        [DisplayName("备注")]
        public string? COMMENTS { get; set; }

        [DisplayName("发件人所属客户帐号")]
        public string? CONSIGNOR_CUSTOMER_CODE { get; set; }

        [DisplayName("发件人所属客户名称")]
        public string? CONSIGNOR_CUSTOMER_NAME { get; set; }

        [DisplayName("发件人名称")]
        public string? CONSIGNOR_NAME { get; set; }

        [DisplayName("发件人地址")]
        public string? CONSIGNOR_ADDR { get; set; }

        [DisplayName("发件人城市")]
        public string? CONSIGNOR_CITY { get; set; }

        [DisplayName("发件人邮编")]
        public string? CONSIGNOR_POSTCODE { get; set; }

        [DisplayName("发件人国家")]
        public string? CONSIGNOR_COUNTRY { get; set; }

        [DisplayName("发件人联系人")]
        public string? CONSIGNOR_CONTACT { get; set; }

        [DisplayName("发件人电话")]
        public string? CONSIGNOR_TEL { get; set; }

        [DisplayName("收件人客户名称")]
        public string? CONSIGNEE_NAME { get; set; }

        [DisplayName("收件人地址")]
        public string? CONSIGNEE_ADDR { get; set; }

        [DisplayName("收件人城市")]
        public string? CONSIGNEE_CITY { get; set; }

        [DisplayName("收件人邮编")]
        public string? CONSIGNEE_POSTCODE { get; set; }

        [DisplayName("收件人国家")]
        public string? CONSIGNEE_COUNTRY { get; set; }

        [DisplayName("收件人联系人")]
        public string? CONSIGNEE_CONTACT { get; set; }

        [DisplayName("收件人电话")]
        public string? CONSIGNEE_TEL { get; set; }

        [DisplayName("DDP")]
        public string? DDP { get; set; }

        [DisplayName("电商订单号")]
        public string? ORDER_NO { get; set; }
    }


    public class WEIGHTLIST
    {
        [DisplayName("运单号")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string CWB_NO { get; set; } = string.Empty;

        [DisplayName("实重")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public decimal PWEIGHT { get; set; }
        //numeric	8,2	N

        [DisplayName("体积重")]
        public decimal VWEIGHT { get; set; }
        //numeric	8,2	Y

        [DisplayName("计费重")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public decimal SWEIGHT { get; set; }
        //numeric	8,2	N
    }

    public class RWDLIST
    {
        [DisplayName("运单号")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string CWB_NO { get; set; } = string.Empty;

        [DisplayName("实重")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public decimal PWEIGHT { get; set; }

        /// <summary>
        ///  [DisplayName("实重")]
        /// </summary>
        [DisplayName("长")]
        public decimal LENGTH { get; set; }

        [DisplayName("宽")]
        public decimal WIDTH { get; set; }

        [DisplayName("高")]
        public decimal HEIGHT { get; set; }

        [DisplayName("件数")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public int PCS { get; set; }

        [DisplayName("体积重")]
        public decimal VWEIGHT { get; set; }
    }

    public class INVLIST
    {
        [DisplayName("运单号")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string CWB_NO { get; set; } = string.Empty;

        [DisplayName("贸易方式")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string TRADE_ITEM { get; set; } = string.Empty;

        [DisplayName("运输备注")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string SHIPPERS_REF { get; set; } = string.Empty;

        [DisplayName("始发国")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string ORGIN_COUNTRY { get; set; } = string.Empty;

        [DisplayName("目的国")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string DEST_COUNTRY { get; set; } = string.Empty;

        [DisplayName("原产地")]
        public string MADE_IN { get; set; } = string.Empty;

        [DisplayName("电话")]
        public string TELE { get; set; } = string.Empty;

        [DisplayName("附加信息")]
        public string ADDITIONAL_INF { get; set; } = string.Empty;

        [DisplayName("进口商编号")]
        public string IMPORTER_ID { get; set; } = string.Empty;

        [DisplayName("发票页角")]
        public string FOOTER { get; set; } = string.Empty;
    }


    public class INVDLIST
    {
        [DisplayName("运单号")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string CWB_NO { get; set; } = string.Empty;

        [DisplayName("品名")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string GOODS_NAME { get; set; } = string.Empty;

        [DisplayName("描述")]
        public string DESCRIPTION { get; set; } = string.Empty;

        [DisplayName("重量")]
        public decimal WEIGHT { get; set; }

        [DisplayName("重量单位")]
        public decimal WEIGHT_UNIT { get; set; }

        [DisplayName("价值")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public decimal VALUE { get; set; }

        [DisplayName("价值单位")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string VALUE_CURRENCY { get; set; } = string.Empty;

        [DisplayName("数量")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public decimal QUANTITY { get; set; }

        [DisplayName("数量单位")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string QUANTITY_UNIT { get; set; } = string.Empty;

        [DisplayName("科目")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string ITEM { get; set; } = string.Empty;


        [DisplayName("卖头")]
        public string MARKS { get; set; } = string.Empty;

        [DisplayName("HS_CODE")]
        public string HS_CODE { get; set; } = string.Empty;

    }

    public class ACCLIST
    {
        [DisplayName("账号")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string ACCOUNT { get; set; } = string.Empty; //  Z4JzfY

        [DisplayName("密码")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string PASSWORD { get; set; } = string.Empty;  //Z4JzfY     

        [DisplayName("文件格式")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public enumFILETYPE FILETYPE { get; set; }

        [DisplayName("渠道")]
        public string CHANNEL { get; set; } = "EC";
    }

    public enum enumFILETYPE
    {
        [EnumExtension.Description("PDF")]
        PDF,
        [EnumExtension.Description("JPEG")]
        JPEG,
        [EnumExtension.Description("TIFF")]
        TIFF,
    }


    public class MS_GENERAL
    {
        public string KEY_TYPE { get; set; }
        public string KEY_VALUE { get; set; }
        public string KEY_NAME { get; set; }
        public string KEY_DESCRIPTION { get; set; }

        public MS_GENERAL(string kEY_TYPE, string kEY_VALUE, string kEY_NAME, string kEY_DESCRIPTION)
        {
            KEY_TYPE = kEY_TYPE;
            KEY_VALUE = kEY_VALUE;
            KEY_NAME = kEY_NAME;
            KEY_DESCRIPTION = kEY_DESCRIPTION;
        }

        public List<MS_GENERAL> CURRENCY = new List<MS_GENERAL>()
        {
        new MS_GENERAL("CURRENCY","01","USD","美元"),
new MS_GENERAL("CURRENCY","02","JPY","日元"),
new MS_GENERAL("CURRENCY","03","EUR","欧元"),
new MS_GENERAL("CURRENCY","04","CNY","人民币"),
new MS_GENERAL("CURRENCY","05","HKD","港币"),
new MS_GENERAL("CURRENCY","06","GBP","英镑"),
new MS_GENERAL("CURRENCY","07","AUD","澳元"),
new MS_GENERAL("CURRENCY","08","NZD","新西兰元"),
new MS_GENERAL("CURRENCY","09","TWD","新台币"),
new MS_GENERAL("CURRENCY","10","SGD","新加坡元"),
new MS_GENERAL("CURRENCY","100","MAD","摩洛哥迪拉姆"),
new MS_GENERAL("CURRENCY","101","MRO","乌吉亚"),
new MS_GENERAL("CURRENCY","102","XOF","非共体法郎"),
new MS_GENERAL("CURRENCY","103","GMD","法拉西"),
new MS_GENERAL("CURRENCY","104","GWP","几内亚比索"),
new MS_GENERAL("CURRENCY","105","GNF","几内亚西里"),
new MS_GENERAL("CURRENCY","106","SLL","利昂"),
new MS_GENERAL("CURRENCY","107","LRD","利比里亚元"),
new MS_GENERAL("CURRENCY","108","GHC","塞地"),
new MS_GENERAL("CURRENCY","109","NGN","奈拉"),
new MS_GENERAL("CURRENCY","11","MYR","马元"),
new MS_GENERAL("CURRENCY","110","XAF","中非金融合作法郎"),
new MS_GENERAL("CURRENCY","111","CDF","中非金融合作法郎"),
new MS_GENERAL("CURRENCY","112","ZAR","兰特"),
new MS_GENERAL("CURRENCY","113","DJF","吉布提法郎"),
new MS_GENERAL("CURRENCY","114","SOS","索马里先令"),
new MS_GENERAL("CURRENCY","115","KES","肯尼亚先令"),
new MS_GENERAL("CURRENCY","116","UGX","乌干达先令"),
new MS_GENERAL("CURRENCY","117","TZS","坦桑尼亚先令"),
new MS_GENERAL("CURRENCY","118","RWF","卢旺达法郎"),
new MS_GENERAL("CURRENCY","119","BIF","布隆迪法郎"),
new MS_GENERAL("CURRENCY","12","IDR","印度尼西亚盾"),
new MS_GENERAL("CURRENCY","120","ZRZ","扎伊尔"),
new MS_GENERAL("CURRENCY","121","ZMK","赞比亚克瓦查"),
new MS_GENERAL("CURRENCY","122","MGF","马达加斯加法郎"),
new MS_GENERAL("CURRENCY","123","SCR","塞舌尔卢比"),
new MS_GENERAL("CURRENCY","124","MUR","毛里求斯卢比"),
new MS_GENERAL("CURRENCY","125","ZWD","津巴布韦元"),
new MS_GENERAL("CURRENCY","126","KMF","科摩罗法郎"),
new MS_GENERAL("CURRENCY","127","FJD","斐济元"),
new MS_GENERAL("CURRENCY","128","SBD","所罗门元"),
new MS_GENERAL("CURRENCY","13","INR","印度卢比"),
new MS_GENERAL("CURRENCY","14","CAD","加拿大元"),
new MS_GENERAL("CURRENCY","15","MOP","澳门元"),
new MS_GENERAL("CURRENCY","16","KRW","韩元"),
new MS_GENERAL("CURRENCY","17","KPW","朝鲜元"),
new MS_GENERAL("CURRENCY","18","VND","越南盾"),
new MS_GENERAL("CURRENCY","19","LAK","基普"),
new MS_GENERAL("CURRENCY","20","KHR","瑞尔"),
new MS_GENERAL("CURRENCY","21","PHP","菲律宾比索"),
new MS_GENERAL("CURRENCY","22","THB","泰铢"),
new MS_GENERAL("CURRENCY","23","MMK","缅元"),
new MS_GENERAL("CURRENCY","24","LKR","斯里兰卡卢比"),
new MS_GENERAL("CURRENCY","25","MVR","马尔代夫卢比"),
new MS_GENERAL("CURRENCY","26","PKR","巴基斯坦卢比"),
new MS_GENERAL("CURRENCY","27","NPR","尼泊尔卢比"),
new MS_GENERAL("CURRENCY","28","AFA","阿富汗尼"),
new MS_GENERAL("CURRENCY","29","IRR","伊朗里亚尔"),
new MS_GENERAL("CURRENCY","30","IQD","伊拉克第纳尔"),
new MS_GENERAL("CURRENCY","31","SYP","叙利亚镑"),
new MS_GENERAL("CURRENCY","32","LBP","黎巴嫩镑"),
new MS_GENERAL("CURRENCY","33","JOD","约旦第纳尔"),
new MS_GENERAL("CURRENCY","34","SAR","亚尔"),
new MS_GENERAL("CURRENCY","35","KWD","科威特第纳尔"),
new MS_GENERAL("CURRENCY","36","BHD","巴林第纳尔"),
new MS_GENERAL("CURRENCY","37","QAR","卡塔尔里亚尔"),
new MS_GENERAL("CURRENCY","38","OMR","阿曼里亚尔"),
new MS_GENERAL("CURRENCY","39","YER","也门里亚尔"),
new MS_GENERAL("CURRENCY","40","YDD","也门第纳尔"),
new MS_GENERAL("CURRENCY","41","TRL","土耳其镑"),
new MS_GENERAL("CURRENCY","42","CYP","塞浦路斯镑"),
new MS_GENERAL("CURRENCY","43","ISK","冰岛克朗"),
new MS_GENERAL("CURRENCY","44","DKK","丹麦克朗"),
new MS_GENERAL("CURRENCY","45","NOK","挪威克朗"),
new MS_GENERAL("CURRENCY","46","SEK","瑞典克朗"),
new MS_GENERAL("CURRENCY","47","FIM","芬兰马克"),
new MS_GENERAL("CURRENCY","48","RUB","卢布"),
new MS_GENERAL("CURRENCY","49","PLZ","兹罗提"),
new MS_GENERAL("CURRENCY","50","CZK","捷克克朗"),
new MS_GENERAL("CURRENCY","51","HUF","福林"),
new MS_GENERAL("CURRENCY","52","DEM","马克"),
new MS_GENERAL("CURRENCY","53","ATS","奥地利先令"),
new MS_GENERAL("CURRENCY","54","CHF","瑞士法郎"),
new MS_GENERAL("CURRENCY","55","NLG","荷兰盾"),
new MS_GENERAL("CURRENCY","56","BEF","比利时法郎"),
new MS_GENERAL("CURRENCY","57","LUF","卢森堡法郎"),
new MS_GENERAL("CURRENCY","58","IEP","爱尔兰镑"),
new MS_GENERAL("CURRENCY","59","FRF","法郎"),
new MS_GENERAL("CURRENCY","60","ESP","比塞塔"),
new MS_GENERAL("CURRENCY","61","PTE","埃斯库多"),
new MS_GENERAL("CURRENCY","62","ITL","里拉"),
new MS_GENERAL("CURRENCY","63","MTL","马耳他镑"),
new MS_GENERAL("CURRENCY","64","YUN","南斯拉夫新第纳尔"),
new MS_GENERAL("CURRENCY","65","ROL","列伊"),
new MS_GENERAL("CURRENCY","66","BGL","列弗"),
new MS_GENERAL("CURRENCY","67","ALL","列克"),
new MS_GENERAL("CURRENCY","68","GRD","德拉马克"),
new MS_GENERAL("CURRENCY","69","MXP","墨西哥比索"),
new MS_GENERAL("CURRENCY","70","GTQ","格查尔"),
new MS_GENERAL("CURRENCY","71","SVC","萨尔瓦多科朗"),
new MS_GENERAL("CURRENCY","72","BZD","伦皮拉"),
new MS_GENERAL("CURRENCY","73","NIO","科多巴"),
new MS_GENERAL("CURRENCY","74","CRC","哥斯达黎加科朗"),
new MS_GENERAL("CURRENCY","75","PAB","巴拿马巴波亚"),
new MS_GENERAL("CURRENCY","76","CUP","古巴比索"),
new MS_GENERAL("CURRENCY","77","BSD","巴哈马元"),
new MS_GENERAL("CURRENCY","78","JMD","牙买加元"),
new MS_GENERAL("CURRENCY","79","HTG","古德"),
new MS_GENERAL("CURRENCY","80","DOP","多米尼加比索"),
new MS_GENERAL("CURRENCY","81","TTD","特立尼达多巴哥元"),
new MS_GENERAL("CURRENCY","82","BBD","巴巴多斯元"),
new MS_GENERAL("CURRENCY","83","COP","哥伦比亚比索"),
new MS_GENERAL("CURRENCY","84","VEB","博利瓦"),
new MS_GENERAL("CURRENCY","85","GYD","圭亚那元"),
new MS_GENERAL("CURRENCY","86","SRG","苏里南盾"),
new MS_GENERAL("CURRENCY","87","PEN","新索尔"),
new MS_GENERAL("CURRENCY","88","ECS","苏克雷"),
new MS_GENERAL("CURRENCY","89","BRL","新克鲁赛罗"),
new MS_GENERAL("CURRENCY","90","BOB","玻利维亚比索"),
new MS_GENERAL("CURRENCY","91","CLP","智利比索"),
new MS_GENERAL("CURRENCY","92","ARP","阿根廷比索"),
new MS_GENERAL("CURRENCY","93","PYG","巴拉圭瓜拉尼"),
new MS_GENERAL("CURRENCY","94","UYU","乌拉圭新比索"),
new MS_GENERAL("CURRENCY","95","EGP","埃及镑"),
new MS_GENERAL("CURRENCY","96","LYD","利比亚第纳尔"),
new MS_GENERAL("CURRENCY","97","SDD","苏丹镑"),
new MS_GENERAL("CURRENCY","98","TND","突尼斯第纳尔"),
new MS_GENERAL("CURRENCY","99","DZD","阿尔及利亚第纳尔") };
        List<MS_GENERAL> CWB_PAYMENT = new List<MS_GENERAL>()
        {

new MS_GENERAL("CWB_PAYMENT","CA","CASH","现金付费"),
new MS_GENERAL("CWB_PAYMENT","CC","CONSIGNEE","收货方付费"),
new MS_GENERAL("CWB_PAYMENT","PP","CONSIGNOR","发货方付费"),
        };

        List<MS_GENERAL> CWB_SERVICE = new List<MS_GENERAL>()
        {

new MS_GENERAL("CWB_SERVICE","01","Express Service","快件服务"),
new MS_GENERAL("CWB_SERVICE","02","Express-URG","特快服务"),
new MS_GENERAL("CWB_SERVICE","03","Economic Service","经济服务"),
new MS_GENERAL("CWB_SERVICE","Z9","Cargo Service","普货服务"),
        };

        List<MS_GENERAL> CWB_TYPE = new List<MS_GENERAL>()
        {
new MS_GENERAL("CWB_TYPE","01","DOC","资料"),
new MS_GENERAL("CWB_TYPE","02","SPS","包裹"),
new MS_GENERAL("CWB_TYPE","03","B2C","B2C"),
        };

        List<MS_GENERAL> TRADE_ITEM = new List<MS_GENERAL>()
        {
new MS_GENERAL("TRADE_ITEM","01","FOB","FOB"),
new MS_GENERAL("TRADE_ITEM","02","CIF","CIF"),
new MS_GENERAL("TRADE_ITEM","03","C&F","C&F"),
new MS_GENERAL("TRADE_ITEM","04","CFR","CFR"),
        };

        List<MS_GENERAL> PACKAGE_UNIT = new List<MS_GENERAL>()
        {
new MS_GENERAL("PACKAGE_UNIT","01","Package","包"),
new MS_GENERAL("PACKAGE_UNIT","02","Bundle","捆"),
new MS_GENERAL("PACKAGE_UNIT","03","Thousand","千个"),

        };

        List<MS_GENERAL> SYSTEM_WEIGHT_UNIT = new List<MS_GENERAL>()
        {
            new MS_GENERAL("SYSTEM_WEIGHT_UNIT","01","KG","重量单位"),
        };
    }
}
