﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.Y7F
{
    public class TrackResponse
    {
        public object msg { get; set; } = new();
        public bool flag { get; set; }
        public List<Cls_trackingInformations> trackingInformations { get; set; } = new();
        public class Cls_trackingInformations
        {
            public List<Cls_trackingInfoDetails> trackingInfoDetails { get; set; } = new();
            public class Cls_trackingInfoDetails
            {
                public string insideNumber { get; set; } = string.Empty;
                public string createTime { get; set; } = string.Empty;
                public object location { get; set; } = new();
                public string description { get; set; } = string.Empty;
                public string trackingStatus { get; set; } = string.Empty;
                public object deliveryNumber { get; set; }=new();
                public object deliveryCompany { get; set; } = new();
                public object statusDesc { get; set; } = new();
            }
            public string orderNumber { get; set; } = string.Empty;
            public bool stopTrack { get; set; }
        }        
    }
}
