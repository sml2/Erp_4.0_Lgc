﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.Y7F
{
    public class ShipTypesResponse
    {
        public string msg { get; set; } = string.Empty;
        public bool flag { get; set; }
        public List<Cls_productInfos> productInfos { get; set; } = new();
        public class Cls_productInfos
        {
            public string productName { get; set; } = string.Empty;
            public string productCode { get; set; } = string.Empty;
            public List<string> countryCodes { get; set; } = Array.Empty<string>().ToList();

            public override string ToString()
            {
                return $"{productName}({productCode})";
            }
        }
    }
}
