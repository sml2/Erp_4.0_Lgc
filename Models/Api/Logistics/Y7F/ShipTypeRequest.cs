﻿namespace ERP.Models.Api.Logistics.Y7F
{
    public class ShipTypeRequest : CommonParameter
    {
        public ShipTypeRequest(Parameter p) : base(p)
        {

        }

        public override string Url => "http://api.17feia.com/eship-api/v1/products";

    }
}
