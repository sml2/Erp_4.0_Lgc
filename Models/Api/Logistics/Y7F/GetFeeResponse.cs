﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.Y7F
{
    public class GetFeeResponse
    {
        public object rows { get; set; } = new();
        public int total { get; set; }
        public object code { get; set; } = new();
        public object msg { get; set; } = new();
        public bool flag { get; set; }
        public Cls_obj obj { get; set; } = null!;
        public class Cls_obj 
        {
            public double totalWeight { get; set; }
            public double totalPrice { get; set; }
            public double totalSurchargePrice { get; set; }
            public double expressFee { get; set; }
            public double firstWeight { get; set; }
            public double firstPrice { get; set; }
            public double stepWeight { get; set; }
            public double stepWeightFee { get; set; }
            public double actualWeight { get; set; }
            public double volumeWeight { get; set; }
            public double orderCostPrice { get; set; }
            public string expressDetail { get; set; } = string.Empty;
            public object surchargeDetails { get; set; } = string.Empty;
        }       
    }
}
