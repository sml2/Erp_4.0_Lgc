﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.Y7F
{
    public class GetCountriesResponse
    {
        public Cls_Obj obj { get; set; } = null!;
        public class Cls_Obj
        {
            public List<DestinationsItem> destinations { get; set; } = new();
            public List<DeparturesItem> departures { get; set; } = new();
            public class DestinationsItem
            {
                public string code { get; set; } = string.Empty;
                public string quanPin { get; set; } = string.Empty;
                public string name { get; set; } = string.Empty;
                public string nameEn { get; set; } = string.Empty;
            }
            public class DeparturesItem
            {
                public string code { get; set; } = string.Empty;
                public string name { get; set; } = string.Empty;
            }
        }
        public string flag { get; set; } = string.Empty;
        public string msg { get; set; } = string.Empty;
        public string code { get; set; } = string.Empty;
        public int total { get; set; }
        public List<string> rows { get; set; } = new(); 
    }
}
