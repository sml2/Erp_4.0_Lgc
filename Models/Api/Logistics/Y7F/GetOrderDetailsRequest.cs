﻿namespace ERP.Models.Api.Logistics.Y7F
{
    public class GetOrderDetailsRequest : CommonParameter
    {
        public GetOrderDetailsRequest(Parameter p, List<string> logNumbers) : base(p)
        {
            Lognumbers = logNumbers;
        }
        public GetOrderDetailsRequest(Parameter p, string logNumber) : base(p)
        {
            Lognumbers = new List<string>() { logNumber };
        }

        public override string Url => "http://api.17feia.com/eship-api/v1/apiSearch/orderDetailSearch";

        public List<string> Lognumbers { get; set; }
        //public string apiName { get; set; }
        //public string apiToken { get; set; }
    }
}
