﻿

using Newtonsoft.Json;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ERP.Models.Api.Logistics.Y7F
{
    public class ExtParameter: BaseExtParam
    {
        /// <summary>
        /// 商品名称(英文)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货品申报名(英文)")]
        public string nameEn { get; set; } = string.Empty;

        /// <summary>
        /// 商品名称(中文)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货品申报名(中文)")]
        public string name { get; set; } = string.Empty;

        /// <summary>
        /// 包装盒长度(cm)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包装盒长度(cm)")]
        public double boxLength { get; set; }

        /// <summary>
        /// 上报价格
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("上报价格")]
        public double reportPrice { get; set; }

        /// <summary>
        /// 包装盒重量(kg)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包装盒重量(kg)")]
        public double boxWeight { get; set; }

        /// <summary>
        /// 包装盒宽度(cm)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包装盒宽度(cm)")]
        public double boxWidth { get; set; }

        /// <summary>
        /// 包装盒高度(cm)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包装盒高度(cm)")]
        public double boxHeight { get; set; }

        /// <summary>
        /// 包装盒数量
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包装盒数量")]
        public int boxCount { get; set; }

        /// <summary>
        /// 货运方式
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货运方式")]
        public string ShipMethod { get; set; } = string.Empty;

        /// <summary>
        /// 货运方式名称
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货运方式名称")]
        public string ShipMethodName { get; set; } = string.Empty;
    }
}
