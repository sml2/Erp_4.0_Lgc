﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.Y7F
{
    public class PrintUrlResponse
    {
        public object msg { get; set; } = new();
        public bool flag { get; set; }
        public List<Cls_pdfUrls> pdfUrls { get; set; } = new();
        public class Cls_pdfUrls
        {
            public string url { get; set; } = string.Empty;
            public string orderNumber { get; set; } = string.Empty;
            public string deliveryNumber { get; set; } = string.Empty;
            public object msg { get; set; } = string.Empty;
        }
    }
}
