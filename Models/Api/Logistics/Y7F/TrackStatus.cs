﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.Y7F
{
    public enum TrackStatus
    {
        [Description("查询不到")]
        NO_RESULT,
        [Description("仓库收货")]
        TAKE_DELIVERY = 100,
        [Description("运输途中")]
        IN_TRANSIT = 300,
        [Description("到达待取")]
        WAIT_TO_PICKUP = IN_TRANSIT,
        [Description("投递失败")]
        DELIVERY_FAILED = IN_TRANSIT,
        [Description("成功签收")]
        SIGNING_SUCCESS = 400,
        [Description("可能异常")]
        POSSIBLE_ABNORMALITY,
        [Description("运输过久")]
        LONG_TRANSPORT = IN_TRANSIT,
        [Description("飞机起飞")]
        TAKE_OFF = IN_TRANSIT,
        [Description("飞机落地")]
        LANDING = IN_TRANSIT,
        [Description("清关中")]
        CLEARANCE = IN_TRANSIT
    }
}
