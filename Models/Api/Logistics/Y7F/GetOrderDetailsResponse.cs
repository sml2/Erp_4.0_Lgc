﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.Y7F
{
    public class GetOrderDetailsResponse
    {
        public object msg { get; set; } = new();
        public bool flag { get; set; }
        public List<OrderDetailsItem> orderDetails { get; set; } = new();
        public class OrderDetailsItem
        {
            public object msg { get; set; } = new();
            public string orderId { get; set; } = string.Empty;
            public string insideNumber { get; set; } = string.Empty;
            public object deliveryNumber { get; set; } = new();
            public string deliveryCompany { get; set; } = string.Empty;
            public string productName { get; set; } = string.Empty;
            public double actualWeight { get; set; }
            public double volumeWeight { get; set; }
            public double chargedWeight { get; set; }
            public string status { get; set; } = string.Empty;
            public string destinationNo { get; set; } = string.Empty;
            public string originNo { get; set; } = string.Empty;
            public string originName { get; set; } = string.Empty;
            public string destinationName { get; set; } = string.Empty;
            public string createTime { get; set; } = string.Empty;
            public object scanTime { get; set; } = new();
            public object signed { get; set; } = new();
            public string referenceNo { get; set; } = string.Empty;
            public object vatNo { get; set; } = new object();
            public object eoriNo { get; set; } = new object();
            public object remakr { get; set; } = new object();
            public object deliveryTime { get; set; } = new();
            public DeliveryAddress deliveryAddress { get; set; } = new();
            public class DeliveryAddress
            {
                public string consignee { get; set; } = string.Empty;
                public object companyName { get; set; } = new();
                public string province { get; set; } = string.Empty;
                public string city { get; set; } = string.Empty;
                public string address { get; set; } = string.Empty;
                public object address2 { get; set; } = new object();
                public string postcode { get; set; } = string.Empty;
                public string cellphoneNo { get; set; } = string.Empty;
                public object email { get; set; } = new();
                public string houseNo { get; set; } = string.Empty;
                public string country { get; set; } = string.Empty;
                public object customsClearanceNo { get; set; } = new object();
            }

            public SenderAddress senderAddress { get; set; } = new();
            public class SenderAddress
            {
                public object sender { get; set; } = new();
                public object companyName { get; set; } = new();
                public object province { get; set; } = new();
                public object city { get; set; } = new();
                public object address { get; set; } = new();
                public object postcode { get; set; } = new();
                public object cellphoneNo { get; set; } = new();
                public object countryCode { get; set; } = new();
            }
            public List<GoodsItem> goods { get; set; } = new();
            public class GoodsItem
            {
                public string nameEn { get; set; } = string.Empty;
                public string name { get; set; } = string.Empty;
                public object sku { get; set; } = new();
                public int quantity { get; set; }
                public double reportPrice { get; set; }
                public double boxWeight { get; set; }
                public double boxLength { get; set; }
                public double boxWidth { get; set; }
                public double boxHeight { get; set; }
                public int boxCount { get; set; }
            }
       }      
    }
}
