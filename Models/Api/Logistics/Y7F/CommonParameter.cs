﻿namespace ERP.Models.Api.Logistics.Y7F
{
    public class CommonParameter:RequestLogistic
    {
        public CommonParameter(Parameter p)
        {
            this.apiName =p.apiName;
            this.apiToken =p.apiToken;
        }
        public string apiName { get; set; }
        public string apiToken { get; set; }
    }
}
