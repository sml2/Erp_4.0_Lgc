﻿namespace ERP.Models.Api.Logistics.Y7F
{
    public class GetEstimatePriceRequest : CommonParameter
    {
        public GetEstimatePriceRequest(Parameter p, string departureCode, string destinationsCode, ExtParameter ext) : base(p)
        {
            DepartureCode = departureCode;
            DestinationsCode = destinationsCode;
            boxWeight = ext.boxWeight.ToString();
            boxLength = ext.boxLength.ToString();
            boxWidth = ext.boxWidth.ToString();
            boxHeight = ext.boxHeight.ToString();
        }

        public string DepartureCode { get; set; }
        public string DestinationsCode { get; set; }
        public string boxWeight { get; set; }
        public string boxLength { get; set; }
        public string boxWidth { get; set; }
        public string boxHeight { get; set; }

        public override string Url => "http://www.17feia.com/customer/v1/freightCalculation/calFreight";

        public override string ToPayload()
        {
            return $"departureCode={DepartureCode}&destinationCode={DestinationsCode}&weight={boxWeight}&length={boxLength}&width={boxWidth}&height={boxHeight}";
        }
    }
}
