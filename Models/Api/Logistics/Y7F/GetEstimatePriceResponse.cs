﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.Y7F
{
    public class GetEstimatePriceResponse
    {
        public object obj { get; set; } = new();
        public bool flag { get; set; }
        public object msg { get; set; } = new();
        public object code { get; set; } = new();
        public int total { get; set; }
        public List<Cls_Rows> rows { get; set; } = new();
        public class Cls_Rows
        {
            public List<Cls_FeeStandard> feeStandard { get; set; } = new();
            public class Cls_FeeStandard
            {
                public double firstWeight { get; set; }
                public double followWeight { get; set; }
                public double followWeightPrice { get; set; }
                public double minWeight { get; set; }
                public double maxWeight { get; set; }
                public double firstWeightPrice { get; set; }
            }
            public double totalPrice { get; set; }
            public string originAddress { get; set; } = string.Empty;
            public string predictTime { get; set; } = string.Empty;
            public object lineFeature { get; set; } = new();
            public Cls_FeeDetail feeDetail { get; set; } = new();
            public class Cls_FeeDetail
            {
                public double finalWeight { get; set; }
                public double otherFee { get; set; }
                public double basicFee { get; set; }
                public double volumeWeight { get; set; }
                public List<Cls_surchargeDetails> surchargeDetails { get; set; } = new();
                public class Cls_surchargeDetails
                {
                    public string surchargeId { get; set; } = string.Empty;
                    public int surchargePrice { get; set; }
                    public string surchargeName { get; set; } = string.Empty;
                    public string surchargeDesc { get; set; } = string.Empty;
                    public string surchargeType { get; set; } = string.Empty;
                    public string surChildType { get; set; } = string.Empty;
                    public string originalSurchargeDesc { get; set; } = string.Empty;
                }
            }
            public string calcType { get; set; } = string.Empty;
            public object limitSend { get; set; }=new();
            public string declarationFee { get; set; } = string.Empty;
            public string calFreightResult { get; set; } = string.Empty;
            public string productType { get; set; } = string.Empty;
        }
    }
}
