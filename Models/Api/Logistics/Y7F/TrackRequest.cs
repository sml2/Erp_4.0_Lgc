﻿using System.Collections.Generic;

namespace ERP.Models.Api.Logistics.Y7F
{   
    public class TrackRequest : CommonParameter
    {
        public TrackRequest(Parameter p, List<string> logNumbers) : base(p)
        {
            LogNumbers = logNumbers;
        }
        public TrackRequest(Parameter p, string logNumber) : base(p)
        {
            LogNumbers = new List<string>() { logNumber };
        }

        public override string Url => "http://api.17feia.com/eship-api/v1/apiSearch/requestTrackInfo";

        public List<string> LogNumbers { get; set; }
        //public string apiName { get; set; }
        //public string apiToken { get; set; }
    }
}
