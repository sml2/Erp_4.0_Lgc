﻿namespace ERP.Models.Api.Logistics.Y7F
{
    public class GetFeeRequest : CommonParameter
    {
        public GetFeeRequest(Parameter p,string productId) : base(p)
        {
            ProductId = productId;
        }

        public string ProductId { get; set; }

        public override string Url => $"http://customer.17feia.com/customer/v1/ordeSearch/orderFeeDetail/{ProductId}";

        public override HttpMethod Method => HttpMethod.Get;

    }
}
