﻿namespace ERP.Models.Api.Logistics.Y7F
{
    public class GetCountriesRequest : CommonParameter
    {
        public GetCountriesRequest(Parameter p) : base(p)
        {

        }

        public override string Url => "http://www.17feia.com/customer/v1/freightCalculation/getDeparturesAndDestinations";

        public override HttpMethod Method => HttpMethod.Get;
    }
}
