﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.Y7F
{
    public class PrintUrlRequest : CommonParameter
    {
        public PrintUrlRequest(Parameter p,List<string> CustomNumbers) : base(p)
        {
            orderNumbers = CustomNumbers;
        }
        public override string Url => "http://api.17feia.com/eship-api/v1/apiSearch/requestPdfUrl";

        public List<string> orderNumbers { get; set; }      
    }
}
