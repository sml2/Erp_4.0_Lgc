﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.Y7F
{
    public class AddOrderResponse
    {
        public object msg { get; set; } = new();
        public bool flag { get; set; }
        public int total { get; set; }
        public int success { get; set; }
        public int fail { get; set; }
        public List<Cls_SuccessOrders> successOrders { get; set; } = new();
        public class Cls_SuccessOrders
        {
            public string insideNumber { get; set; } = string.Empty;
            public string referenceNo { get; set; } = string.Empty;
            public string pdfPath { get; set; } = string.Empty;
            public object deliveryNumber { get; set; }=new();
            public string id { get; set; } = string.Empty;
            public string supplyItemId { get; set; } = string.Empty;
            public string productId { get; set; } = string.Empty;
            public string deliveryCompany { get; set; } = string.Empty;
            public bool update { get; set; }
        }
        public List<Cls_FailOrders> failOrders { get; set; } = new();
        public class Cls_FailOrders
        {
            public string referenceNo { get; set; } = string.Empty;
            public string errorMessage { get; set; } = string.Empty;
        }

        public Tuple<string, List<ReportResult>> CheckResult()
        {
            string msg = "";
            List<ReportResult> reportResults = new List<ReportResult>();
          
            return Tuple.Create(msg, reportResults);
        }
    }
}
