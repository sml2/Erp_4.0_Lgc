using Newtonsoft.Json;
using System.Collections.Generic;

namespace ERP.Models.Api.Logistics.Y7F
{
    public class AddOrderRequest : CommonParameter
    {
        public AddOrderRequest(Parameter parameter) : base(parameter)
        { 
        }
     
        public override string Url => "http://api.17feia.com/eship-api/v1/orders";
        public List<Cls_apiOrders> apiOrders { get; set; } = new();
        public class Cls_apiOrders
        {
            public Cls_apiOrders(SenderInfo senderInfo, OrderInfo orderInfo, ExtParameter Ext) 
            {
                orderNo = orderInfo.OrderNo;
                productName = Ext.ShipMethodName;
                productCode = Ext.ShipMethod;
                destinationNo = orderInfo.ReceiverAddress.NationShort;
                takeAwayType = "ESHIP";//货运方式
                referenceNo = Helpers.GetOrderNo(orderInfo.OrderNo, orderInfo.SendNum, orderInfo.FailNum);// orderInfo.OrderNo;
                orderFromType = "SINGLE";
                //商品信息（一箱多件）
                List<AddOrderRequest.Cls_apiOrders.Cls_goods> goods = new List<AddOrderRequest.Cls_apiOrders.Cls_goods>();
                int Num = orderInfo.GoodInfos.Sum((r) => r.Num);
                foreach (var r in orderInfo.GoodInfos)
                {
                    Cls_apiOrders.Cls_goods good = new Cls_apiOrders.Cls_goods();
                    good.nameEn = Ext.nameEn;
                    good.name = Ext.name;
                    good.quantity = r.Count;
                    //orderProducts.Length
                    good.reportPrice =Helpers.RoundData(Ext.reportPrice / Num);
                    good.boxWeight =Helpers.RoundData(Ext.boxWeight/1000/ Num, 2);
                    good.boxLength = Ext.boxLength;
                    good.boxWidth = Ext.boxWidth;
                    good.boxHeight = Ext.boxHeight;
                    good.boxCount = Ext.boxCount;
                    goods.Add(good);
                }
                this.goods = goods;
                //接收者信息
                AddOrderRequest.Cls_apiOrders.Cls_AddressInfo DeliveryAddressInfo = new AddOrderRequest.Cls_apiOrders.Cls_AddressInfo();
                DeliveryAddressInfo.consignee = orderInfo.ReceiverAddress.Name;
                DeliveryAddressInfo.province = orderInfo.ReceiverAddress.Province;
                DeliveryAddressInfo.city = orderInfo.ReceiverAddress.City;
                DeliveryAddressInfo.address = orderInfo.ReceiverAddress.Address;
                DeliveryAddressInfo.postcode = orderInfo.ReceiverAddress.Zip??"";
                DeliveryAddressInfo.cellphoneNo = orderInfo.ReceiverAddress.Phone;
                string Address = orderInfo.ReceiverAddress.Address;
                if (orderInfo.ReceiverAddress.Address2.IsNotWhiteSpace()) Address += $" {orderInfo.ReceiverAddress.Address2}";
                if (orderInfo.ReceiverAddress.Address3.IsNotWhiteSpace()) Address += $" {orderInfo.ReceiverAddress.Address3}";
                DeliveryAddressInfo.houseNo = Address;//iOrderInfo.receiver_district ??
                deliveryAddress = DeliveryAddressInfo;
                //发件人信息
                AddOrderRequest.Cls_apiOrders.Cls_AddressInfo SenderAddressInfo = new AddOrderRequest.Cls_apiOrders.Cls_AddressInfo();
                SenderAddressInfo.sender = senderInfo.Name;
                SenderAddressInfo.province = senderInfo.Region??"";
                SenderAddressInfo.city = senderInfo.City;
                SenderAddressInfo.address = senderInfo.Address;
                SenderAddressInfo.postcode = senderInfo.PostalCode;
                SenderAddressInfo.cellphoneNo = senderInfo.PhoneNumber;
                SenderAddressInfo.countryCode = "CN";
                senderAddress = SenderAddressInfo;               
            }

            [JsonIgnore]
            public string orderNo { get; set; }
            public string productName { get; set; }
            public string productCode { get; set; }
            public string destinationNo { get; set; }
            public string takeAwayType { get; set; }
            public string referenceNo { get; set; }
            public string orderFromType { get; set; }
            public List<Cls_goods> goods { get; set; }
            public class Cls_goods
            {
                public string nameEn { get; set; } = string.Empty;
                public string name { get; set; } = string.Empty;
                public int quantity { get; set; }
                public double reportPrice { get; set; }
                public double boxWeight { get; set; }
                public double boxLength { get; set; }
                public double boxWidth { get; set; }
                public double boxHeight { get; set; }
                public int boxCount { get; set; }
            }
            public Cls_AddressInfo deliveryAddress { get; set; }
            public Cls_AddressInfo senderAddress { get; set; }
            public class Cls_AddressInfo
            {
                /// <summary>
                /// 接受者Name
                /// </summary>
                public string consignee { get; set; } = string.Empty;
                /// <summary>
                /// 发送者Name
                /// </summary>
                public string sender { get; set; } = string.Empty;
                public string province { get; set; } = string.Empty;
                public string city { get; set; } = string.Empty;
                public string address { get; set; } = string.Empty;
                public string postcode { get; set; } = string.Empty;
                public string cellphoneNo { get; set; } = string.Empty;
                public string countryCode { get; set; } = string.Empty;
                public string houseNo { get; set; } = string.Empty;
            }
        }
    }
}
