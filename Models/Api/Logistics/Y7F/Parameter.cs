﻿using ERP.Attributes.Logistics;

namespace ERP.Models.Api.Logistics.Y7F
{
    public class Parameter : BaseParam
    {
        private string name;
        private string token;
        [ViewConfigParameter(nameof(apiName), Sort = 0)]
        public string apiName { get { return name; } set { name = value.Trim(); } }
        [ViewConfigParameter(nameof(apiToken), Sort = 1)]
        public string apiToken { get { return token; } set { token = value.Trim(); } }
    }
}
