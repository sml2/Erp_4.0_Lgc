﻿namespace ERP.Models.Api.Logistics;

public class CommonResultList:BaseResult 
{
    public CommonResultList(Exception e):base(e)
    {

    }
    public CommonResultList(List<CommonResult> commonResults )
    {
        Success = true;
        CommonResults = commonResults;
    }
    public List<CommonResult> CommonResults { get; set; } = new();
}

public class PrintParamResult : BaseResult
{
    public PrintParamResult()
    {

    }

    public PrintParamResult(Exception e) : base(e)
    {

    }

    public PrintParamResult(bool contentType, bool fileType, bool paperType, List<CommonResult> contentTypes, List<CommonResult> fileTypes,  List<CommonResult> paperTypes)
    {
        ContentType = contentType;
        FileType = fileType;
        PaperType = paperType;
        this.fileTypes = fileTypes;
        this.contentTypes = contentTypes;
        this.paperTypes = paperTypes;
    }

    public bool ContentType { get; set; }
    public bool FileType { get; set; }
    public  bool PaperType { get; set; }

    public List<CommonResult> fileTypes { get; set; }=new();

    public List<CommonResult> contentTypes { get; set; } = new();

    public List<CommonResult> paperTypes { get; set; } = new();
}

public class CommonResult
{
    public CommonResult(string code, string name)
    {
        Code = code;
        Name = name;
    }
    public string Code { get; set; }

    public string Name { get; set; }
    public override string ToString()
    {
        return Name;
    }
}

public class ShipTypeUBI : CommonResult
{
    public ShipTypeUBI(string code, string name, List<string> serviceOptions, List<string> origins) : base(code, name)
    {
        Origins = origins;
        ServiceOptions = serviceOptions;
    }
    public List<string> Origins { get; set; }

    public List<string> ServiceOptions { get; set; }
}
