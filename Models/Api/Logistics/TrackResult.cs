

using ERP.Enums.Logistics;
using Newtonsoft.Json;

namespace ERP.Models.Api.Logistics;

public class TrackResult:BaseResult
{
    public TrackResult(int waybillId,string customNumber,Exception e):base(e)
    {
        WaybillId = waybillId;
        CustomNumber = customNumber;
    }
    public TrackResult(int waybillId, string customNumber, Exception e,string response) : base(e)
    {
        WaybillId = waybillId;
        CustomNumber = customNumber;
        Response = response;
    }
    public TrackResult(int waybillId, string customNumber, string? logNumber, string? trackNum, string? trackInfo, string? rawState, States? state, List<TrackInfo> trackInfos, string response)
    {
        WaybillId = waybillId;
        CustomNumber = customNumber;
        Express = logNumber;
        TrackNum = trackNum;
        TrackInfo = trackInfo;
        RawState = rawState;
        State = state;
        Response = response;
        TrackInfos = trackInfos;
    }

    public TrackResult(int waybillId, string customNumber, string? logNumber, string? trackNum, string? trackInfo, string? rawState, States? state, TrackPrice? price, List<TrackInfo> trackInfos, string response)
    {
        WaybillId = waybillId;
        CustomNumber = customNumber;
        Express = logNumber;
        TrackNum = trackNum;
        TrackInfo = trackInfo;
        RawState = rawState;
        State = state;
        Price = price;
        Response = response;
        TrackInfos = trackInfos;
    }

    public int WaybillId { get; set; }

    /// <summary>
    /// 物流平台单号
    /// </summary>
    public string CustomNumber { get; set; }

    /// <summary>
    /// 物流运单号
    /// </summary>
    public string? Express { get; set; }

    /// <summary>
    /// 物流追踪号
    /// </summary>
    public string? TrackNum { get; set; }
    
    /// <summary>
    /// 物流轨迹信息
    /// </summary>
    public string? TrackInfo { get; set; }

    public List<TrackInfo> TrackInfos { get; set; } = new();

    /// <summary>
    /// 物流接口原轨迹状态信息
    /// </summary>
    public string? RawState { get; set; }

    /// <summary>
    /// 物流状态
    /// </summary>
    public States? State { get; set; }

    public  TrackPrice? Price { get; set; }

    [JsonIgnore]
    public string Response { get; set; } = string.Empty;
}


public class WaybillPrice: BaseResult
{
    public WaybillPrice(Exception e) : base(e)
    {

    }

    public WaybillPrice(TrackPrice? price):base()
    {
        Price = price;
    }
    public TrackPrice? Price { get; set; }
}



public class TrackInfo
{
    public TrackInfo(string time, string state, string location, string description)
    {
        Time = time;
        State = state;
        Location = location;
        Description = description;
    }

    public string Time { get; set; }

    public string State { get; set; }

    public string Location { get; set; }

    public string Description { get; set; }

}