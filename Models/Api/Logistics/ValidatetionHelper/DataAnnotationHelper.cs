﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ERP.Models.Api.Logistics
{    
    public class ErrorList
    {
        public ErrorList(List<ErrorMember> errors)
        {
            Errors = errors;
        }
        public List<ErrorMember> Errors { get; set; }   
    }

    public class ErrorMember
    {
        public string ErrorMemberName { get; set; } = string.Empty;
        public string ErrorMessage { get; set; } = string.Empty;
    }

    public static class DataAnnotationHelper
    {            
        public static List<ErrorMember> IsValid<T>(this T o, bool only1Level = false)
        {
            List<ErrorMember> result =new List<ErrorMember>();
            IsValid(o??default!, result, only1Level);
            return result;
        }
      
        private static void IsValid(object o, List<ErrorMember> result, bool only1Level)
        {
            var validationContext = new ValidationContext(o);
            var objectType = validationContext.ObjectType;
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(o, validationContext, results, true);
            string displayName = validationContext.DisplayName;
            if (!isValid)
            {
                foreach (var item in results)
                {
                    result.Add(new ErrorMember()
                    {
                        ErrorMessage = item.ErrorMessage ?? "",
                        ErrorMemberName = displayName
                    });
                }
            }
            if (!only1Level)
            {
                foreach (var p in objectType.GetProperties())
                {
                    if (p.CustomAttributes.Count() > 0 && p.PropertyType.IsClass && !p.PropertyType.Equals(typeof(string)))
                    {
                        object? pObj = p.GetValue(o, null);
                        if (pObj != null)
                        {
                            IsValid(pObj, result, only1Level);
                        }
                    }
                }
            }
        }
    }
}
