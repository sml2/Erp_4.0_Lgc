﻿namespace ERP.Models.Api.Logistics;


public class printResultList : BaseResult
{
    public printResultList(Exception e) : base(e)
    {

    }

    public printResultList(Exception e, string response) : base(e)
    {
        Response = response;
    }

    public printResultList(List<printResult> printResults)
    {
        PrintResults = printResults;
    }
    public List<printResult> PrintResults { get; set; } = new();

    public string Response { set; get; }=string.Empty;
}


public class printResult : BaseResult
{
    public printResult(List<string> customNumbers, Exception e, string response) : base(e)
    {
        CustomNumbers = customNumbers;
        Response = response;
    }

    public printResult(List<string> customNumbers, string url, string response)
    {
        CustomNumbers = customNumbers;
        Url = url;
        Response = response;
    }

    public printResult(List<string> customNumbers, string base64string, string contentType, string ext, string? url, string response)
    {
        CustomNumbers = customNumbers;
        Base64string = base64string;
        Response = response;
        ContentType = contentType;
        Ext = ext;
        Url = url;
    }

    public printResult(List<string> customNumbers, Stream stream, string contentType, string ext, string response)
    {
        CustomNumbers = customNumbers;
        Stream = stream;
        Response = response;
        ContentType = contentType;
        Ext = ext;
    }

    public printResult(List<string> customNumbers, Stream stream, string contentType, string ext,string url, string response)
    {
        CustomNumbers = customNumbers;
        Stream = stream;
        Response = response;
        ContentType = contentType;
        Ext = ext;
        Url = url;
    }

    public List<string> CustomNumbers { get; set; } = new();

    /// <summary>
    /// 文件后缀名
    /// </summary>
    public string Ext { get; set; } = ".pdf";

    /// <summary>
    /// 文件类型
    /// </summary>
    public string ContentType { get; set; } = "application/pdf";


    /// <summary>
    /// 文件流
    /// </summary>
    public Stream? Stream { get; set; }

    /// <summary>
    /// 文件url
    /// </summary>
    public string? Url { get; set; }


    public string? Base64string { get; set; }
    public string Response { get; set; } = string.Empty;
}
