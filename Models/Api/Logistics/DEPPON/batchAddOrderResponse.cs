﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.DEPPON
{
    public class batchAddOrderResponse
    {
        /// <summary>
        /// 响应标志，Success表示成功，Failure表示失败
        /// </summary>
        public string ask { get; set; } = string.Empty;
        public string message { get; set; } = string.Empty;
        public List<ResultItem> Result { get; set; } = new();
        public class ResultItem
        {
            /// <summary>
            /// 响应标志，Success表示成功，Failure表示失败
            /// </summary>
            public string ask { get; set; } = string.Empty;
            public string message { get; set; } = string.Empty;
            /// <summary>
            /// 系统生成的订单号
            /// </summary>
            public string order_code { get; set; } = string.Empty;
            /// <summary>
            /// 客户参考号
            /// </summary>
            public string reference_no { get; set; } = string.Empty;
            /// <summary>
            /// 服务商单号
            /// </summary>
            public string shipping_method_no { get; set; } = string.Empty;
            /// <summary>
            /// 1-已产生跟踪号，2-等待后续更新跟踪号,3-不需要跟踪号
            /// </summary>
            public string track_status { get; set; } = string.Empty;
            /// <summary>
            /// 0-不需要分配地址，1-需要分配地址
            /// </summary>
            public int sender_info_status { get; set; }
            /// <summary>
            /// 代理单号—转单号
            /// </summary>
            public string agent_number { get; set; } = string.Empty;
            public ErrorItem Error { get; set; }=new();
        }
        public List<ErrorItem> Error { get; set; } = new();
        public class ErrorItem
        {
            public string errMessage { get; set; } = string.Empty;
            public string errCode { get; set; } = string.Empty;
        }      
    }
}
