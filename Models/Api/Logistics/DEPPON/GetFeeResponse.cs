﻿
namespace ERP.Models.Api.Logistics.DEPPON
{
    public class GetFeeResponse
    {
        public string ask { get; set; } = string.Empty;
        public string message { get; set; } = string.Empty;
        public List<DataItem> Data { get; set; }= new();
        public class DataItem
        {
            public string WaybillNumber { get; set; } = string.Empty;
            public string TrackingNumber { get; set; } = string.Empty;
            public string CustomerOrderNumber { get; set; } = string.Empty;
            public string CountryCode { get; set; } = string.Empty;
            public string ChineseName { get; set; } = string.Empty;
            public string ShippingMethodName { get; set; } = string.Empty;
            public string SettleWeight { get; set; } = string.Empty;
            public double Freight { get; set; }
            public double Register { get; set; }
            public double HandlingFee { get; set; }
            public double FuelCharge { get; set; }
            public double Surcharge { get; set; }
            public double TotalFee { get; set; }
            public double SpecialFee { get; set; }
            public double OtherFee { get; set; }
        }
        public ErrorItem Error { get; set; } = null!;
        public class ErrorItem
        {
            public string errMessage { get; set; } = string.Empty;
            public string errCode { get; set; } = string.Empty;
        }
    }
}