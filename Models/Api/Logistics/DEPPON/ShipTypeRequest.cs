﻿using Newtonsoft.Json;
namespace ERP.Models.Api.Logistics.DEPPON
{
    public class ShipTypeRequest : CommonParameter
    {
        public ShipTypeRequest(Parameter p, string country_code) : base(p)
        {
            this.country_code = country_code;
        }

        [JsonIgnore]
        public override string Method { get => "getShippingMethod"; }
        public string country_code { get; set; }
    }
}
