﻿using ERP.Attributes.Logistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.DEPPON
{
    public class Parameter: BaseParam
    {
        private string key;
        private string token;
        private string mcode;
        private string id;
        private string code;
        [ViewConfigParameter(nameof(AppKey))]
        public string AppKey { get { return key; } set { key = value.Trim(); } } 
        [ViewConfigParameter(nameof(AppToken))]
        public string AppToken { get { return token; } set { token = value.Trim(); } } 

        public string CustomCode { get { return mcode; } set { mcode = value.Trim(); } }
        public string CustomID { get { return id; } set { id = value.Trim(); } }
        public string CountryCode { get { return code; } set { code = value.Trim(); } }
    }
}
