﻿using ERP.Models.Api.Logistics;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using static Mod_GlobalVariables;

namespace ERP.Models.Api.Logistics.DEPPON
{
    public class ShipTypeResponse 
    {
        /// <summary>
        /// 响应标志，Success表示成功，Failure表示失败
        /// </summary>
        public string ask { get; set; } = string.Empty;
        public string message { get; set; } = string.Empty;
        public List<DataItem> Data { get; set; } = new();
        public class DataItem
        {
            public string code { get; set; } = string.Empty;
            public string cn_name { get; set; } = string.Empty;
            public string en_name { get; set; } = string.Empty;
            public string group_code { get; set; } = string.Empty;
            public string track_status { get; set; } = string.Empty;
            public string aging { get; set; } = string.Empty;
            public string note { get; set; } = string.Empty;
            public string Price { get; set; } = string.Empty;
            public void Print(LogSub Log)
            {
                //string p = price.IsNotWhiteSpace() ? $"[{FullName}]物流方式的总估价为:{price}" : "";
                string p = PriceData.IsNotNull() ? $"线路:{cn_name}\r\n基础运费:{PriceData.FreightFee}\r\n燃油费:{PriceData.FuelFee}\r\n其他费用:{PriceData.OtherFee}\r\n挂号费:{PriceData.RegisteredFee}\r\n总费用:{PriceData.TotalFee}\r\n体积重计费:{PriceData.VolumeCharge}\r\n关税预付服务费:{PriceData.tariffPrepayFee}" : "无估价";
                Log(p);
            }
            private GetEstimatePriceResponse.DataItem PriceData = null!;
            public void SetPrice(GetEstimatePriceResponse.DataItem PriceData)
            {
                this.PriceData = PriceData;
                Price = PriceData.TotalFee.ToString();
            }
            public override string ToString()
            {
                string p = Price.IsNotWhiteSpace() ? $"(估价:{Price})" : "";
                return cn_name + $"[{code}]{p}";
            }
        }
        public List<ErrorItem> Error { get; set; } = new();
        public class ErrorItem
        {
            public string errMessage { get; set; } = string.Empty;
            public string errCode { get; set; } = string.Empty;
        }       
    }
}
