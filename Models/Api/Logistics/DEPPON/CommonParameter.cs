﻿

using Newtonsoft.Json;

namespace ERP.Models.Api.Logistics.DEPPON
{
    public abstract class CommonParameter: RequestLogistic
    {
        public CommonParameter(Parameter p)
        {
            AppKey = p.AppKey;
            AppToken = p.AppToken;           
        }
        [JsonIgnore]
        public new abstract string Method { get; }
        [JsonIgnore]
        public string AppKey { get; set; }
        [JsonIgnore]
        public string AppToken { get; set; }
        [JsonIgnore]
        public string CustomCode { get; set; } = string.Empty;

        [JsonIgnore]
        public string CustomID { get; set; } = string.Empty;
        [JsonIgnore]
        public string CountryCode { get; set; } = string.Empty;


        [JsonIgnore]
        public long TimeStamp { get; set; }
        [JsonIgnore]

        public string MD5 { get; set; }=string.Empty;

        [JsonIgnore]
        public string Platform { get; set; } = string.Empty;
      
    }
}
