﻿using ERP.Models.Api.Logistics;
using Newtonsoft.Json;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ERP.Models.Api.Logistics.DEPPON
{
    public class ExtParameter: BaseExtParam
    {
        /// <summary>
        /// 商品申报名(中文)
        /// </summary>
        [DisplayName("货品申报名(中文)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string cnname { get; set; } = string.Empty;

        /// <summary>
        /// 商品申报名(英文)
        /// </summary>
        [DisplayName("商品申报名(英文)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string enname { get; set; } = string.Empty;

        /// <summary>
        /// 包裹重量(kg)
        /// </summary>
        [DisplayName("包裹重量(g)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public double weight { get; set; }

        /// <summary>
        /// 包裹长度(cm)
        /// </summary>
        [DisplayName("包裹长度(cm)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public double length { get; set; }

        /// <summary>
        /// 包裹宽度(cm)
        /// </summary>
        [DisplayName("包裹宽度(cm)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public double width { get; set; }

        /// <summary>
        /// 包裹高度(cm)
        /// </summary>
        [DisplayName("包裹高度(cm)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public double height { get; set; }
      
        /// <summary>
        /// 海关编码
        /// </summary>
        public string HsCode { get; set; }=string.Empty;

        /// <summary>
        /// true-退回，false-不退回
        /// </summary>
        [DisplayName("是否退回包裹")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public bool is_return { get; set; }

        /// <summary>
        /// 货运方式
        /// </summary>
        [DisplayName("货运方式")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string ShipMethod { get; set; } = string.Empty;

        /// <summary>
        /// 申报总价($)
        /// </summary>
        [DisplayName("申报总价($)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public double Price { get; set; }


        [Required(ErrorMessage = "{0} 不能为空")]
        /// <summary>
        /// 币种
        /// </summary>
        public string Currency { get; set; }
    }
}
