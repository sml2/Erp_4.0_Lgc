﻿namespace ERP.Models.Api.Logistics.DEPPON
{
    public class TrackRequest : CommonParameter
    {
        public TrackRequest(Parameter p, string logNumber) : base(p)
        {
            codes = new List<string>() { logNumber };
        }
        public TrackRequest(Parameter p,List<string> logNumbers) : base(p)
        {
            codes = logNumbers;
        }

        public override string Method { get => "getCargoTrack"; }

        public List<string> codes { get; set; }
    }
}
