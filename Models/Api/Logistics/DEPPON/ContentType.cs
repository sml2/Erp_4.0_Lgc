﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static EnumExtension;

namespace ERP.Models.Api.Logistics.DEPPON
{
    public enum ContentType
    {
        [Description("标签")]
        Label = 1,
        [Description("标签+报关单")]
        LabelAndCustoms = 4,
    }
}
