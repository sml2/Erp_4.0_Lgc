﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static EnumExtension;

namespace ERP.Models.Api.Logistics.DEPPON
{
    public enum PaperType
    {
        [Description("标签10X10")]
        label10x10_1 = 1,
        [Description("A4纸")]
        label10x10_0=2,
        [Description("标签10X15")]
        label10x15_1=3,
    }
}
