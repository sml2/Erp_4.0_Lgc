﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.DEPPON
{
    public class PrintUrlResponse 
    {
        public string ask { get; set; } = string.Empty;
        public string message { get; set; } = string.Empty;
        public List<resultItem> result { get; set; }=new();
        public class resultItem
        {
            public List<printInfoItem> printInfo { get; set; } = new();
            public class printInfoItem
            {
                /// <summary>
                /// 单个订单标签状态，0为获取失败，1为获取成功
                /// </summary>
                public string state { get; set; } = string.Empty;
                public string code { get; set; } = string.Empty;
                public string errCode { get; set; } = string.Empty;
                public string msg { get; set; } = string.Empty;
            }
            public string url { get; set; } = string.Empty;
        }
        /// <summary>
        /// 1.全部获取成功；2.部分获取成功；3.全部获取失败
        /// </summary>
        public string state { get; set; } = string.Empty;
        public List<ErrorItem> Error { get; set; } = new();
        public class ErrorItem
        {
            public string errMessage { get; set; } = string.Empty;
            public string errCode { get; set; } = string.Empty;
        }       
    }
}
