﻿using ERP.Models.Api.Logistics;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.DEPPON
{
    public class GetEstimatePriceRequest
    {
        [JsonIgnore]
        public virtual string URL { get; set; } = string.Empty;

        //[JsonIgnore]
        //public virtual HttpMethod Method => HttpMethod.Post;

        public string country_code { get; set; } = string.Empty;
        /// <summary>
        /// 单位KG 最多三位小数
        /// </summary>
        public double weight { get; set; }
        public double length { get; set; }
        public double width { get; set; }
        public double height { get; set; }
        /// <summary>
        /// 包裹类型，D-文件，L-信封，W-包裹
        /// </summary>
        public string shipping_type_id => "W";
    }
}
