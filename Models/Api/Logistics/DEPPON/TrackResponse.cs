﻿namespace ERP.Models.Api.Logistics.DEPPON
{

    public class TrackResponse
    {
        public string ask { get; set; } = string.Empty;
        public string message { get; set; } = string.Empty;
        public List<DataItem> Data { get; set; } = new();
        public class DataItem
        {
            public string Code { get; set; } = string.Empty;
            public string Country_code { get; set; } = string.Empty;
            public string New_date { get; set; } = string.Empty;
            public string New_Comment { get; set; } = string.Empty;
            public string Status { get; set; } = string.Empty;
            public List<DetailItem> Detail { get; set; }= new();
            public class DetailItem
            {
                public string Occur_date { get; set; }= string.Empty;
                public string Comment { get; set; } = string.Empty;

            }
        }
        public List<ErrorItem> Error { get; set; }=new();
        public class ErrorItem
        {
            public string errMessage { get; set; } = string.Empty;
            public string errCode { get; set; } = string.Empty;
        }
    }
}
