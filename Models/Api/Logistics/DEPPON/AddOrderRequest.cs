using ERP.Models.Api.Logistics;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.DEPPON
{
    public class AddOrderRequest: CommonParameter
    {
        public AddOrderRequest(Parameter p,SenderInfo senderInfo,OrderInfo orderInfo,ExtParameter Ext) : base(p)
        {           
            Id = orderInfo.id;
            orderNo = orderInfo.OrderNo;
            reference_no = Helpers.GetOrderNo(orderInfo.OrderNo, orderInfo.SendNum, orderInfo.FailNum);// orderInfo.OrderNo;
            shipping_method = Ext.ShipMethod;
            country_code = orderInfo.ReceiverAddress.NationShort;
            order_weight =Helpers.RoundData(Ext.weight/1000,2);
            order_pieces = 1;
            length = Convert.ToInt32(Ext.length);
            width =  Convert.ToInt32(Ext.width);
            height = Convert.ToInt32(Ext.height);
            is_return = Ext.is_return?1:0;
            //收件人信息
           Consignee = new AddOrderRequest.ConsigneeItem();
           Consignee.consignee_province = orderInfo.ReceiverAddress.Province;
           Consignee.consignee_city = orderInfo.ReceiverAddress.City;
            string Address1 = orderInfo.ReceiverAddress.Address;
            string Address2 = orderInfo.ReceiverAddress.Address2??"";
            string Address3 = orderInfo.ReceiverAddress?.Address3 ?? "";
            if (Address1.IsNullOrEmpty())
            {
                Address1 = orderInfo.ReceiverAddress?.Address2 ?? "";
                Address2 = orderInfo.ReceiverAddress?.Address3 ?? "";
                Address3 = "";
            }
            if (Address1.IsNullOrEmpty())
            {
                Address1 = orderInfo.ReceiverAddress?.Address3 ?? "";
                Address2 = "";
                Address3 = "";
            }
            Consignee.consignee_street = Address1;
            Consignee.consignee_street2 = Address2;
            Consignee.consignee_street3 = Address3;
            Consignee.consignee_postcode = orderInfo.ReceiverAddress?.Zip??string.Empty;
            Consignee.consignee_name = orderInfo.ReceiverAddress!.Name;
            Consignee.consignee_telephone = orderInfo.ReceiverAddress.Phone;
            Consignee.consignee_mobile = orderInfo.ReceiverAddress.Phone;
            Consignee.consignee_email = orderInfo.ReceiverAddress?.Email?? string.Empty;
            Shipper = new AddOrderRequest.ShipperItem();           
           Shipper.shipper_province = senderInfo.Region?? string.Empty;
           Shipper.shipper_city = senderInfo.City;
           Shipper.shipper_street = senderInfo.Address;
           Shipper.shipper_postcode = senderInfo.PostalCode;
           Shipper.shipper_name = senderInfo.Name;
           Shipper.shipper_telephone = senderInfo.PhoneNumber;
           Shipper.shipper_mobile = senderInfo.PhoneNumber;
           Shipper.shipper_email = senderInfo.Mail;
            //包裹信息
            ItemArr = new List<AddOrderRequest.ItemArrItem>();
            var count = orderInfo.GoodInfos.Sum(x=>x.Count);
            foreach (var pro in orderInfo.GoodInfos)
            {
                var item = new AddOrderRequest.ItemArrItem();
                item.invoice_enname = Ext.enname;
                item.invoice_cnname = Ext.cnname;
                item.invoice_weight = Helpers.RoundData(order_weight / count, 2);
                item.invoice_quantity = orderInfo.GoodInfos.Count;
                item.invoice_unitcharge =Helpers.RoundData(Ext.Price / count, 2);
                item.invoice_currencycode = Ext.Currency;
                item.hs_code = Ext.HsCode;
                ItemArr.Add(item);
            }
        }

        public override  string Method { get=> "CreateOrder"; }
      
        [JsonIgnore]
        public int Id { get; set; }

        [JsonIgnore]
        public string orderNo { get; set; }
        public string reference_no { get; set; }
        public string shipping_method { get; set; }
        public string country_code { get; set; }
        /// <summary>
        /// 单位KG 最多三位小数
        /// </summary>
        public double order_weight { get; set; }
        /// <summary>
        /// 外包装件数
        /// </summary>
        public int order_pieces { get; set; }
        public int length { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        /// <summary>
        /// 是否退回,包裹无人签收时是否退回，1-退回，0-不退回
        /// </summary>
        public int is_return { get; set; }
        /// <summary>
        /// 收件人信息
        /// </summary>
        public ConsigneeItem Consignee { get; set; }
        public class ConsigneeItem
        {
            //public string consignee_company { get; set; }
            public string consignee_province { get; set; } =string.Empty;
            public string consignee_city { get; set; }     =string.Empty;
            public string consignee_street { get; set; }   =string.Empty;
            public string consignee_street2 { get; set; }  =string.Empty;
            public string consignee_street3 { get; set; }  =string.Empty;
            public string consignee_postcode { get; set; } =string.Empty;
            public string consignee_name { get; set; }     =string.Empty;
            public string consignee_telephone { get; set; }=string.Empty;
            public string consignee_mobile { get; set; }   =string.Empty;
            public string consignee_email { get; set; } = string.Empty;
        }
        /// <summary>
        /// 发件人信息
        /// </summary>
        public ShipperItem Shipper { get; set; }
        public class ShipperItem
        {           
            public string shipper_province { get; set; }  =string.Empty;
            public string shipper_city { get; set; } =string.Empty;
            public string shipper_street { get; set; }    =string.Empty;
            public string shipper_postcode { get; set; }  =string.Empty;
            public string shipper_name { get; set; }      =string.Empty;
            public string shipper_telephone { get; set; } =string.Empty;
            public string shipper_mobile { get; set; }    =string.Empty;
            public string shipper_email { get; set; } = string.Empty;
        }
        /// <summary>
        /// 海关申报信息
        /// </summary>
        public List<ItemArrItem> ItemArr { get; set; }
        public class ItemArrItem
        {
            public string invoice_enname { get; set; } = string.Empty;
            public string invoice_cnname { get; set; } = string.Empty;
            /// <summary>
            /// KG
            /// </summary>
            public double invoice_weight { get; set; }
            public int invoice_quantity { get; set; }
            public string unit_code => "PCE";
            /// <summary>
            /// 单价
            /// </summary>
            public double invoice_unitcharge { get; set; }
            public string invoice_currencycode { get; set; }
            public string hs_code { get; set; } = string.Empty;
        }       
    }
}
