﻿namespace ERP.Models.Api.Logistics.DEPPON
{
    public class GetFeeRequest : CommonParameter
    {
        public GetFeeRequest(Parameter p, string customNumber) : base(p)
        {
            this.reference_no = customNumber;
        }
        public GetFeeRequest( Parameter p, List<string> customNumbers):base(p)
        {
            this.reference_no = string.Join(',', customNumbers);
        }
        public override string Method  { get => "getReceivingExpense"; }
        public string reference_no { get; set; } = string.Empty;
    }
}
