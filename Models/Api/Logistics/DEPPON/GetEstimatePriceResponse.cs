﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.DEPPON
{
    public class GetEstimatePriceResponse 
    {
        public string ask { get; set; } = string.Empty;
        public string message { get; set; } = string.Empty;
        public List<DataItem> Data { get; set; } = new();
        public class DataItem
        {
            /// <summary>
            /// 计费重量
            /// </summary>
            public double ChargeWeight { get; set; }
            /// <summary>
            /// 时效
            /// </summary>
            public string Effectiveness { get; set; } = string.Empty;
            /// <summary>
            /// 运费
            /// </summary>
            public double FreightFee { get; set; }
            /// <summary>
            /// 燃油费
            /// </summary>
            public double FuelFee { get; set; }
            /// <summary>
            /// 其他费用
            /// </summary>
            public double OtherFee { get; set; }
            /// <summary>
            /// 产品排序权重值
            /// </summary>
            public int ProductSort { get; set; }
            /// <summary>
            /// 挂号费
            /// </summary>
            public double RegisteredFee { get; set; }
            /// <summary>
            /// 备注
            /// </summary>
            public string Remark { get; set; } = string.Empty;
            /// <summary>
            /// 服务中文名称
            /// </summary>
            public string ServiceCnName { get; set; } = string.Empty;
            /// <summary>
            /// 服务英文名称
            /// </summary>
            public string ServiceEnName { get; set; } = string.Empty;
            /// <summary>
            /// 总费用
            /// </summary>
            public double TotalFee { get; set; }
            /// <summary>
            /// 可追踪
            /// </summary>
            public string Traceability { get; set; } = string.Empty;
            /// <summary>
            /// 体积重计费
            /// </summary>
            public double VolumeCharge { get; set; }
            /// <summary>
            /// 可服务的运输方式代码
            /// </summary>
            public string ServiceCode { get; set; } = string.Empty;
            /// <summary>
            /// 关税预付服务费
            /// </summary>
            public double tariffPrepayFee { get; set; }
        }
        public List<ErrorItem> Error { get; set; } = new();
        public class ErrorItem
        {
            public string errMessage { get; set; } = string.Empty;
            public string errCode { get; set; } = string.Empty;
        }
    }
}
