﻿namespace ERP.Models.Api.Logistics.DEPPON
{
    public class PrintUrlRequest : CommonParameter
    {
        public PrintUrlRequest(Parameter p,List<string> CustomNumbers, int PaperType, int ContentType) : base(p)
        {
            codes = CustomNumbers;
            label_type = PaperType;
            label_content_type = ContentType;
          
            //if (Enum.IsDefined(typeof(LabelFileTypeEnum), FileType) && Enum.TryParse<LabelFileTypeEnum>(FileType, out var labelFileTypeEnum))
            //{
            //    label_type = (int)labelFileTypeEnum;
            //}           
        }
      
        public override string Method => "batchGetLabel";

        public List<string> codes { get; set; }
     
        public int? label_type { get; set; }
      
        public int? label_content_type { get; set; }                   
    }
}
