﻿namespace ERP.Models.Api.Logistics.SDGJ
{
    public class GetFeeRequest : CommonParameter
    {
        public GetFeeRequest(Parameter p,string CustomNumber, Platforms platform) : base(p,platform)
        {
           this.CustomNumber = CustomNumber;           
        }

        public string CustomNumber { get; set; }      

        public override string Url => base.Url;

        public override string serviceMethod => "getbusinessfee_detail";
      
        public override string ToPayload()
        {
            return $"appToken={appToken}&appKey={appKey}&serviceMethod={serviceMethod}&paramsJson={{\"reference_no\":\"{CustomNumber}\",\"shipping_method_no\":\"\"}}";//两个参数选其一即可;
        }
    }
}
