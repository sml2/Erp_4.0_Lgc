﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.SDGJ
{
    public class PrintUrlResponse
    {
        /// <summary>
        /// 是否成功标志，0代表失败；1代表成功；2代表重复订单
        /// </summary>
        public int success { get; set; }
        public string cnmessage { get; set; } = string.Empty;
        public string enmessage { get; set; } = string.Empty;
        public List<Cls_data> data { get; set; } = new();
        public class Cls_data
        {
            public string lable_file_type { get; set; } = string.Empty;
            public string lable_content_type { get; set; } = string.Empty;
            public string lable_file { get; set; } = string.Empty;
        }       
    }
}
