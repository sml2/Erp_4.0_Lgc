﻿namespace ERP.Models.Api.Logistics.SDGJ
{
    public class TrackInfoRequest : CommonParameter
    {
        public TrackInfoRequest(Parameter p, List<string> TrackNumbers, Platforms pl) : base(p,pl)
        {
            TrackNumber = string.Join(',', TrackNumbers);
        }

        public TrackInfoRequest(Parameter p,string TrackNumber, Platforms pl) : base(p, pl)
        {
            this.TrackNumber = TrackNumber;
        }

        public string TrackNumber { get; set; }
        public override string serviceMethod => "gettrack";

        public override string ToPayload()
        {
            string url = $"appToken={appToken}&appKey={appKey}&serviceMethod={serviceMethod}&paramsJson={{\"tracking_number\":\"{TrackNumber}\"}}";
            return url;
        }
    }
}
