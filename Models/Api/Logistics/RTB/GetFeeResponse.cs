﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.SDGJ
{
    public class GetFeeResponse
    {
        public int success { get; set; }
        public string cnmessage { get; set; } = string.Empty;
        public string enmessage { get; set; } = string.Empty;
        public List<DataItem> data { get; set; } = new();
        public class DataItem
        {
            public string fee_kind_code { get; set; } = string.Empty;
            public string fee_kind_name { get; set; } = string.Empty;
            public string currency_code { get; set; } = string.Empty;
            public string currency_name { get; set; } = string.Empty;
            public string currency_rate { get; set; } = string.Empty;
            public string create_date { get; set; } = string.Empty;
            public string occur_date { get; set; } = string.Empty;
            public string bill_date { get; set; } = string.Empty;
            public string currency_amount { get; set; } = string.Empty;
            public string amount { get; set; } = string.Empty;
            public override string ToString()
            {
                return $"{fee_kind_name}:{currency_code}{currency_amount}";
            }
        }      
    }
}
