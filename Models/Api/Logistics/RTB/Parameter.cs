﻿using ERP.Attributes.Logistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.SDGJ
{
    public class Parameter: BaseParam
    {
        private string id;
        private string token;
        [ViewConfigParameter(nameof(appKey), Sort = 0)]
        public string appKey { get { return id; } set { id = value.Trim(); } } 
        [ViewConfigParameter(nameof(appToken), Sort = 1)]
        public string appToken { get { return token; } set { token = value.Trim(); } } 
    }
}
