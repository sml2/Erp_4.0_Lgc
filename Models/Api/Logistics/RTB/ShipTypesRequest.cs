﻿namespace ERP.Models.Api.Logistics.SDGJ
{
    public class ShipTypesRequest : CommonParameter
    {
        public ShipTypesRequest(Parameter p, Platforms pl) : base(p,pl)
        {  
            
        }

        public override string Url { get => $"{DoMain}/webservice/PublicService.asmx/ServiceInterfaceUTF8";}
     
        public override string serviceMethod => "getshippingmethod";

        public override string ToPayload()
        {
            return $"appToken={appToken}&appKey={appKey}&serviceMethod={serviceMethod}&paramsJson=";
        }
    }
}
