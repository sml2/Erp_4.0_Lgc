﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.SDGJ
{
    public class GetTrackInfoResponse
    {
        public int success { get; set; }
        public string cnmessage { get; set; } = string.Empty;
        public string enmessage { get; set; } = string.Empty;
        public List<Cls_data> data { get; set; } = new();
        public class Cls_data
        {
            public string server_hawbcode { get; set; } = string.Empty;
            public string destination_country { get; set; } = string.Empty;
            public string track_status { get; set; } = string.Empty;
            public string track_status_name { get; set; } = string.Empty;
            public string signatory_name { get; set; } = string.Empty;
            public List<Cls_details> details { get; set; } = new();
            public class Cls_details
            {
                public string track_occur_date { get; set; } = string.Empty;
                public string track_location { get; set; } = string.Empty;
                public string track_description { get; set; } = string.Empty;
            }
        }      
    }
}
