﻿namespace ERP.Models.Api.Logistics.SDGJ
{
    public class SDGJParameters
    {
        /// <summary>
        /// 货运方式
        /// </summary>
        public string ShipCode { get; set; } = string.Empty;

        /// <summary>
        /// IOSS单号
        /// </summary>
        public string IOSSNo { get; set; } = string.Empty;

        /// <summary>
        /// 是否使用IOSS备案
        /// </summary>
        public bool IsEnableFiling { get; set; }
    }
}
