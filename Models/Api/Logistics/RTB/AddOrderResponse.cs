﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.SDGJ
{
    public class AddOrderResponse
    {
        /// <summary>
        /// 是否成功标志，0代表失败；1代表成功；2代表重复订单
        /// </summary>
        public int success { get; set; }
        public string cnmessage { get; set; } = string.Empty;
        public string enmessage { get; set; } = string.Empty;
        public Cls_data data { get; set; } = new();
        public class Cls_data
        {
            /// <summary>
            /// 订单ID
            /// </summary>
            public string order_id { get; set; } = string.Empty;
            /// <summary>
            /// 客户订单ID
            /// </summary>
            public string refrence_no { get; set; } = string.Empty;
            /// <summary>
            /// 服务商ID
            /// </summary>
            public string shipping_method_no { get; set; } = string.Empty;
            /// <summary>
            /// 渠道转单号
            /// </summary>
            public string channel_hawbcode { get; set; } = string.Empty;
        }       
    }
}
