﻿namespace ERP.Models.Api.Logistics.SDGJ
{
    public class TrackNumRequest : CommonParameter
    {
        public TrackNumRequest(Parameter p,string CustomOrderID, Platforms pl) : base(p,pl)
        {
            this.CustomOrderID = CustomOrderID;
        }

        public string CustomOrderID { get; set; }
        public override string serviceMethod => "gettrackingnumber";

        public override string ToPayload()
        {
            string url = $"appToken={appToken}&appKey={appKey}&serviceMethod={serviceMethod}&paramsJson={{\"reference_no\":\"{CustomOrderID}\"}}";
            return url;
        }
    }
}
