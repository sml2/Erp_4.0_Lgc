﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static EnumExtension;

namespace ERP.Models.Api.Logistics.SDGJ
{
    public enum ContentType
    {
        [Description("标签")]
        Label=1,
        [Description("报关单")]
        Customs_declaration,
        [Description("配货单")]
        Distribution_order,
        [Description("标签+报关单")]
        LabelAndCustoms_declaration,
        [Description("标签+配货单")]
        LabelAndDistribution_order,
        [Description("标签+报关单+配货单")]
        LabelAndCustoms_declarationAndDistribution_order,
    }

    public enum PaperType
    {
        [Description("标签纸(10*10厘米)")]
        Type1 = 1,
        [Description("A4纸(21*29.7厘米)")]
        Type2 = 2,
    }

    public enum LabelFileTypeEnum
    {
        [Description("PNG")]
        PNG = 1,
        [Description("PDF")]
        PDF,
    }
}
