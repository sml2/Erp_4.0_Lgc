using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.SDGJ.YDH
{
    public class YDHAddOrderRequest : CommonParameter
    {
        public override string ToPayload()
        {
            string url=$"appToken={appToken}&appKey={appKey}&serviceMethod={serviceMethod}&paramsJson={base.ToPayload()}";
            return url;
        }

        public YDHAddOrderRequest(Parameter p, SenderInfo senderInfo, OrderInfo orderInfo, ExtParameter ext, Platforms platform) : base(p,platform)
        {
            MoneyRecord money = new Data.MoneyMeta(ext.Currency, ext.Declare);        
            var amount = Convert.ToDecimal(money.ToString("USD",false));

             Id = orderInfo.id;
            orderNo = orderInfo.OrderNo;
            reference_no = Helpers.GetOrderNo(orderInfo.OrderNo, orderInfo.SendNum, orderInfo.FailNum);// orderInfo.OrderNo;
            shipping_method = ext.ShipMethod;
            order_weight =Helpers.RoundData(ext.Weight/1000,2);
            IossNum = ext.IOSS;
            insurance_value = Convert.ToDouble(ext.Declare);

            is_enable_filing = ext.IsEnableFiling ? 1 : 0;
            //收货人信息
            consignee = new YDHAddOrderRequest.Cls_consignee();
            consignee.consignee_province = orderInfo.ReceiverAddress.Province;
            consignee.consignee_city = orderInfo.ReceiverAddress.City;
            string Address = orderInfo.ReceiverAddress.Address;
            if (orderInfo.ReceiverAddress.Address2.IsNotWhiteSpace()) Address += $" {orderInfo.ReceiverAddress.Address2}";
            if (orderInfo.ReceiverAddress.Address3.IsNotWhiteSpace()) Address += $" {orderInfo.ReceiverAddress.Address3}";
            consignee.consignee_street = Address;
            consignee.consignee_postcode = orderInfo.ReceiverAddress.Zip??"";
            consignee.consignee_name = orderInfo.ReceiverAddress.Name;
            consignee.consignee_telephone = orderInfo.ReceiverAddress.Phone;
            consignee.consignee_mobile = orderInfo.ReceiverAddress.Phone;
            consignee.consignee_countrycode = orderInfo.ReceiverAddress.NationShort;//城市简码
            consignee.consignee_tariff = ext.ReceiveVatNumber;
            
            shipper = new YDHAddOrderRequest.Cls_shipper();
            shipper.shipper_countrycode = "CN";
            shipper.shipper_city = senderInfo.City;
            shipper.shipper_street = senderInfo.Address;
            shipper.shipper_postcode = senderInfo.PostalCode;
            shipper.shipper_name = senderInfo.Name;
            shipper.shipper_mobile = senderInfo.PhoneNumber;
            shipper.shipper_telephone = senderInfo.PhoneNumber;
            //shipper.shipper_areacode = senderInfo.PostalCode;
            //商品信息
            invoice = new List<YDHAddOrderRequest.Cls_invoice>();
            int num = orderInfo.GoodInfos.Sum((r) => r.Count);
            foreach (var v in orderInfo.GoodInfos)
            {
                YDHAddOrderRequest.Cls_invoice Invoice = new YDHAddOrderRequest.Cls_invoice();
                Invoice.sku = v.goodId;
                Invoice.invoice_enname = ext.ApplicationName;
                Invoice.invoice_cnname = ext.ApplicationNameCn;
                int OrderCount = orderInfo.GoodInfos.Count;
                Invoice.invoice_quantity = v.Count;
                Invoice.invoice_unitcharge =Helpers.RoundData(Convert.ToDouble(amount / num),2);
                Invoice.hs_code = ext.HsCode;
                Invoice.invoice_currencycode="USD";
                invoice.Add(Invoice);
            }
        }

        public override string serviceMethod => "createorder";


        [JsonIgnore]
        public int Id { get; set; }

        [JsonIgnore]
        public string orderNo { get; set; }
        /// <summary>
        /// 客户参考号（必要参数）
        /// </summary>
        [DisplayName("客户参考号")]
        [Required(ErrorMessage = "{0}不能为空")]
        public string reference_no { get; set; }

        /// <summary>
        /// 货运方式代码（必要参数）
        /// </summary>
        [DisplayName("货运方式代码")]
        [Required(ErrorMessage = "{0}不能为空")]
        public string shipping_method { get; set; } = string.Empty;

        /// <summary>
        /// 服务商单号
        /// </summary>
        public string shipping_method_no { get; set; } 


        /// <summary>
        /// 订单重量，单位KG
        /// </summary>
        public double order_weight { get; set; } = 0.2;

        /// <summary>
        /// 外包装件数,默认1
        /// </summary>
        public int order_pieces { get; set; } = 1;


        //public string cargotype { get; set; }

     
        public enum enumcargotype
        {
            [Description("包裹")]
             W = 1,
            [Description("文件")]
            D = 2,
            [Description("袋子")]
            B = 3,         
        }

        public string mail_cargo_type { get; set; } = "4";

        /// <summary>
        /// 包裹申报种类1：Gif礼品 2：CommercialSample 商品货样 3：Document 文件 4：Other 其他默认4
        /// </summary>
       

        public enum enummail_cargo_type
        {
            [Description("礼品")]
            Gif = 1,
            [Description("商品货样")]
            CommercialSample = 2,
            [Description("文件")]
            Document = 3,
            [Description("其他")]
            Other = 4,
        }

        /// <summary>
        /// 英国VAT税号
        /// </summary>
        public string VatNum { get; set; } 


        /// <summary>
        /// 欧盟IOSS税号
        /// </summary>
        public string IossNum { get; set; } 


        public string return_sign { get; set; } 

        /// <summary>
        /// 是否需要标识退件退回
        /// </summary>
        public enumreturn_sign ereturn_sign { get; set; }

        public enum enumreturn_sign {
            [Description("Y")]
            Y,
            [Description("N")]
            N
        }

        /// <summary>
        /// EORI
        /// </summary>
        public string buyer_id { get; set; } 

        /// <summary>
        /// 订单备注
        /// </summary>
        public string order_info { get; set; } 

        /// <summary>
        /// 平台ID（如果您是电商平台，请联系我们添加并确认您对应的平台ID）
        /// </summary>
        public string platform_id { get; set; } 

        /// <summary>
        /// 自定义单号
        /// </summary>
        public string custom_hawbcode { get; set; }

        /// <summary>
        /// 额外服务
        /// </summary>
        public List<cls_extra_service> extra_service { get; set; } = null;

        public double insurance_value { get; set; }

      
        public int is_enable_filing { get; set; }

        /// <summary>
        /// 发件人信息
        /// </summary>
        [DisplayName("发件人信息")]
        [Required(ErrorMessage = "{0}不能为空")]
        public Cls_shipper shipper { get; set; }
        public class Cls_shipper
        {
            /// <summary>
            /// 发件人姓名
            /// </summary>
            [DisplayName("发件人姓名")]
            [Required(ErrorMessage = "{0}不能为空")]
            public string shipper_name { get; set; }

            /// <summary>
            /// 发件人公司
            /// </summary>
            public string shipper_company { get; set; } 

            /// <summary>
            /// 发件人国家二字代码
            /// </summary>
            [DisplayName("发件人国家二字代码")]
            [Required(ErrorMessage = "{0}不能为空")]
            public string shipper_countrycode { get; set; } 

            /// <summary>
            /// 发件人省
            /// </summary>
            public string shipper_province { get; set; }

            /// <summary>
            /// 发件人城市
            /// </summary>
            public string shipper_city { get; set; } 

            /// <summary>
            /// 发件人地址
            /// </summary>
            [DisplayName("发件人地址")]
            [Required(ErrorMessage = "{0}不能为空")]
            public string shipper_street { get; set; } 

            /// <summary>
            /// 发件人邮编
            /// </summary>
            [DisplayName("发件人邮编")]
            [Required(ErrorMessage = "{0}不能为空")]
            public string shipper_postcode { get; set; } 

            /// <summary>
            /// 发件人区域代码
            /// </summary>
            public string shipper_areacode { get; set; } 
          

            /// <summary>
            /// 发件人电话
            /// </summary>
            [DisplayName("发件人电话")]
            [Required(ErrorMessage = "{0}不能为空")]
            public string shipper_telephone { get; set; }

            /// <summary>
            /// 发件人手机
            /// </summary>
            [DisplayName("发件人手机")]
            [Required(ErrorMessage = "{0}不能为空")]
            public string shipper_mobile { get; set; } 

            /// <summary>
            /// 发件人传真
            /// </summary>
            public string shipper_fax { get; set; } 

        }

        /// <summary>
        /// 收货人信息
        /// </summary>
        public Cls_consignee consignee { get; set; }
   
        public class Cls_consignee
        {
            /// <summary>
            /// 收件人姓名
            /// </summary>
            [DisplayName("收件人姓名")]
            [Required(ErrorMessage = "{0}不能为空")]
            public string consignee_name { get; set; } 


            /// <summary>
            /// 收件人公司名
            /// </summary>
            public string consignee_company { get; set; } 

            /// <summary>
            /// 收件人国家二字代码
            /// </summary>
            [DisplayName("收件人国家二字代码")]
            [Required(ErrorMessage = "{0}不能为空")]
            public string consignee_countrycode { get; set; } 


            /// <summary>
            /// 收件人省
            /// </summary>
            [DisplayName("收件人省")]
            [Required(ErrorMessage = "{0}不能为空")]
            public string consignee_province { get; set; }

            /// <summary>
            /// 收件人城市
            /// </summary>
            [DisplayName("收件人城市")]
            [Required(ErrorMessage = "{0}不能为空")]
            public string consignee_city { get; set; } 

            /// <summary>
            /// 收件人地址
            /// </summary>
            [DisplayName("收件人地址")]
            [Required(ErrorMessage = "{0}不能为空")]
            public string consignee_street { get; set; } 

            /// <summary>
            /// 收件人邮编
            /// </summary>
            [DisplayName("收件人邮编")]
            [Required(ErrorMessage = "{0}不能为空")]
            public string consignee_postcode { get; set; } 


            /// <summary>
            /// 收件人门牌号
            /// </summary>
            public string consignee_doorplate { get; set; }


            /// <summary>
            /// 收件人区域代码
            /// </summary>
            public string consignee_areacode { get; set; }

            /// <summary>
            /// 收件人电话
            /// </summary>
            [DisplayName("收件人电话")]
            [Required(ErrorMessage = "{0}不能为空")]
            public string consignee_telephone { get; set; } 

            /// <summary>
            /// 收件人手机
            /// </summary>
            [DisplayName("收件人手机")]
            [Required(ErrorMessage = "{0}不能为空")]
            public string consignee_mobile { get; set; }

            /// <summary>
            /// 收件人邮箱
            /// </summary>
            public string consignee_email { get; set; } 


            /// <summary>
            /// 收件人传真
            /// </summary>
            public string consignee_fax { get; set; } 


            public string consignee_certificatetype { get; set; }

            /// <summary>
            /// 收件人传真
            /// </summary>
           // public enumconsignee_certificatetype consignee_certificatetype { get; set; }

            /// <summary>
            /// 证件号码
            /// </summary>
            public string consignee_certificatecode { get; set; } 

            /// <summary>
            /// 证件有效期
            /// </summary>
            public string consignee_credentials_period { get; set; } 

            /// <summary>
            /// 税号
            /// </summary>
            public string consignee_tariff { get; set; } 


        }

        public enum enumconsignee_certificatetype
        {
            [Description("身份证")]
            ID,
            [Description("护照")]
            pp,
        }

        /// <summary>
        /// 海关申报信息
        /// </summary>
        [DisplayName("海关申报信息")]
        [Required(ErrorMessage = "{0}不能为空")]
        public List<Cls_invoice> invoice { get; set; }


        public class Cls_invoice
        {
            /// <summary>
            /// SKU
            /// </summary>
            public string sku { get; set; }

            /// <summary>
            /// 英文品名
            /// </summary>
            [DisplayName("英文品名")]
            [Required(ErrorMessage = "{0}不能为空")]
            public string invoice_enname { get; set; } 

            /// <summary>
            /// 中文品名
            /// </summary>
            [DisplayName("中文品名")]
            [Required(ErrorMessage = "{0}不能为空")]
            public string invoice_cnname { get; set; }

            /// <summary>
            /// 数量
            /// </summary>
            [DisplayName("数量")]
            [Required(ErrorMessage = "{0}不能为空")]
            public int invoice_quantity { get; set; }

            public string unit_code { get; set; } = "PCE";

            /// <summary>
            /// 单位 MTR：米 | PCE：件 | SET：套 默认PCE
            /// </summary>
            public enumunit_code eunit_code { get; set; } = enumunit_code.PCE;
            
            public enum enumunit_code
            {
                [Description("米")]
                MTR =1,
                [Description("件")]
                PCE = 1,
                [Description("套")]
                SET = 1,
            }

            /// <summary>
            /// 单价(单个商品价格)
            /// </summary>
            [DisplayName("单价(单个商品价格)")]
            [Required(ErrorMessage = "{0}不能为空")]
            public double invoice_unitcharge { get; set; }

           

            
            /// <summary>
            /// 币别
            /// </summary>
            public string invoice_currencycode { get; set; } 


            /// <summary>
            /// 海关编码
            /// </summary>
            public string hs_code { get; set; } 

            /// <summary>
            /// 配货信息
            /// </summary>
            public string invoice_note { get; set; }

            /// <summary>
            /// 销售地址
            /// </summary>
            public string invoice_url { get; set; } 

            /// <summary>
            /// 商品图片地址
            /// </summary>
            public string invoice_info { get; set; } 

            /// <summary>
            /// 材质
            /// </summary>
            public string invoice_material { get; set; } 

            /// <summary>
            /// 规格
            /// </summary>
            public string invoice_spec { get; set; } 

        }


    
    }

    public class cls_extra_service
    {
        /// <summary>
        /// 额外服务类型代码
        /// </summary>
        public string extra_servicecode { get; set; }

        /// <summary>
        /// 额外服务值（有些额外服务类型必须传此值）
        /// </summary>
        public string extra_servicevalue { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string extra_servicenote { get; set; }
    }

    public enum enumCurrencyCode
    {
        [Description("USD")]
        USD = 1,
        [Description("JPY")]
        JPY = 2,
        [Description("RMB")]
        RMB = 3,
        [Description("EUR")]
        EUR = 4,
        [Description("GBP")]
        GBP = 5,
        [Description("HKD")]
        HKD = 6,
        [Description("RUB")]
        RUB = 7,
        [Description("SGD")]
        SGD = 8,
    }


}
