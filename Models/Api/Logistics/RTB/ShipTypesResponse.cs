﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Mod_GlobalVariables;

namespace ERP.Models.Api.Logistics.SDGJ
{
    public class ShipTypesResponse 
    {
        public int success { get; set; }
        public string cnmessage { get; set; } = string.Empty;
        public string enmessage { get; set; } = string.Empty;
        public List<Cls_data> data { get; set; } = new();
        public class Cls_data
        {
            public void Print(LogSub Log)
            {
                //string p = price.IsNotWhiteSpace() ? $"[{FullName}]物流方式的总估价为:{price}" : "";
                string p = data.IsNotNull() ? $"费用明细：\r\n运费:{data.FreightFee}\r\n燃油费:{data.FuelFee}\r\n挂号费:{data.RegisteredFee}\r\n其他费用:{data.OtherFee}\r\n总费用:{data.TotalFee}" : "无估价";
                Log(p);
            }
            private string price = null!;
            private GetEstimatePriceResponse.Cls_data data = null!;
            public void SetAllPrice(GetEstimatePriceResponse.Cls_data data)
            {
                this.data = data;
                price = data.TotalFee.ToString();
            }
            public string code { get; set; } = string.Empty;
            public string cnname { get; set; } = string.Empty;
            public string enname { get; set; } = string.Empty;
            public string note { get; set; } = string.Empty;
            public override string ToString()
            {
                string p = price.IsNotNull() ? $"(估价:{price})" : "";
                return $"{cnname}[{code}]{p}";
            }
        }
    }
}
