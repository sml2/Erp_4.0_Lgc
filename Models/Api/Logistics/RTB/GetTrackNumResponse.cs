﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.SDGJ
{
    public class GetTrackNumResponse
    {
        public int success { get; set; }
        public string cnmessage { get; set; } = string.Empty;
        public string enmessage { get; set; } = string.Empty;
        public Cls_data data { get; set; } = null!;
        public class Cls_data
        {
            public string refrence_no { get; set; } = string.Empty;
            /// <summary>
            /// 服务商单号(物流跟踪号)
            /// </summary>
            public string shipping_method_no { get; set; } = string.Empty;
            /// <summary>
            /// 渠道转单号
            /// </summary>
            public string channel_hawbcode { get; set; } = string.Empty;
        }    
    }
}
