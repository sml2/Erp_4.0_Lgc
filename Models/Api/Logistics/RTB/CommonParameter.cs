﻿using Newtonsoft.Json;

namespace ERP.Models.Api.Logistics.SDGJ
{
    public abstract  class CommonParameter: RequestLogistic
    {
        public CommonParameter(Parameter p,Platforms platform)
        {
            this.appKey = p.appKey;
            this.appToken = p.appToken;
            Platform = platform;
            if (platform == Platforms.RTB_YDH)
            {
                DoMain = $"http://customer.ydhex.com";
            }
            else if (platform == Platforms.RTB_ZHXT)
            {
                DoMain = $"http://lfn.rtb56.com";
            }
            else if (platform == Platforms.RTB_JFCC)
            {
                DoMain = $"http://sxjf.rtb56.com";
            }
            else
            {
                DoMain = $"http://szhyt.rtb56.com";
            }
        }

        [JsonIgnore]
        public abstract string serviceMethod { get; }

        public string appKey { get; set; }
       
        public string appToken { get; set; }

        [JsonIgnore]
        public Platforms Platform { get; set; }

        [JsonIgnore]
        public string DoMain { get; set; }

        [JsonIgnore]
        public override HttpMethod Method { get => HttpMethod.Post; }


        public override string ContentType => "application/x-www-form-urlencoded";

        [JsonIgnore]
        public override string Url { get => $"{DoMain}/webservice/PublicService.asmx/ServiceInterfaceUTF8"; }  
    }
}
