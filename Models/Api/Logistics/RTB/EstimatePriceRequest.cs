﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.SDGJ
{
    public class EstimatePriceRequest:CommonParameter
    {
        public EstimatePriceRequest(Parameter p, ExtParameter Ext,string countryCode, Platforms platform) : base(p,platform)
        {
            country_code = countryCode;
            weight = Ext.Weight.ToString();          
            length = Ext.Length.ToString();
            width = Ext.Width.ToString();
            height = Ext.Height.ToString();
        }

        public override string serviceMethod => "feetrail";

        public override string ToPayload()
        {
            string url = $"appToken={appToken}&appKey={appKey}&serviceMethod={serviceMethod}&paramsJson={base.ToPayload()}";
            return url;
        }

        /// <summary>
        /// 目的国家二字代码
        /// </summary>
        [DisplayName("目的国家二字代码")]      
        public string country_code { get; set; } = string.Empty;

        /// <summary>
        /// 重量（单位：kg）
        /// </summary>
        [DisplayName("重量（单位：kg）")]      
        public string weight { get; set; }


        /// <summary>
        /// 运输方式代码
        /// </summary>
        [DisplayName("重量（单位：kg）")]      
        public string shipping_method { get; set; } = string.Empty;

        /// <summary>
        /// 目的地邮编
        /// </summary>    
        public string post_code { get; set; } = string.Empty;

        /// <summary>
        /// 起运地 默认为：用户绑定的起运地
        /// </summary>
        public string location { get; set; } = string.Empty;

        /// <summary>
        /// 货物类型（W:包裹 D:文件） 默认为：W
        /// </summary>
        public string cargo_type { get; set; } = "W";

        /// <summary>
        /// 长（CM）
        /// </summary>
        public string length { get; set; }

        /// <summary>
        /// 宽（CM）
        /// </summary>
        public string width { get; set; }

        /// <summary>
        /// 高（CM）
        /// </summary>
        public string height { get; set; }

        /// <summary>
        /// 额外服务代码集合
        /// </summary>
        public List<cls_extra_service> extra_service { get; set; } = new();

    }
}
