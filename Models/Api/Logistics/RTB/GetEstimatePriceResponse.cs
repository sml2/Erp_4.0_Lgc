﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.SDGJ
{
    public class GetEstimatePriceResponse
    {
        public int success { get; set; }
        public string cnmessage { get; set; } = string.Empty;
        public string enmessage { get; set; } = string.Empty;
        public List<Cls_data> data { get; set; } = new();
        public class Cls_data
        {
            public string ServiceCode { get; set; } = string.Empty;
            public string ServiceCnName { get; set; } = string.Empty;
            public string ServiceEnName { get; set; } = string.Empty;
            public string ChargeWeight { get; set; } = string.Empty;
            public string Effectiveness { get; set; } = string.Empty;
            public string Formula { get; set; } = string.Empty;
            /// <summary>
            /// 运费
            /// </summary>
            public string FreightFee { get; set; } = string.Empty;
            /// <summary>
            /// 燃油费
            /// </summary>
            public string FuelFee { get; set; } = string.Empty;
            /// <summary>
            /// 挂号费
            /// </summary>
            public string RegisteredFee { get; set; } = string.Empty;
            /// <summary>
            /// 其他费用
            /// </summary>
            public string OtherFee { get; set; } = string.Empty;
            public List<Cls_OtherFeeDetails> OtherFeeDetails { get; set; } = new();
            public class Cls_OtherFeeDetails
            {
                public string FeeAmount { get; set; } = string.Empty;
                public string FeeCnName { get; set; } = string.Empty;
                public string FeeEnName { get; set; } = string.Empty;
            }
            /// <summary>
            /// 总费用
            /// </summary>
            public double TotalFee { get; set; }
            public int ProductSort { get; set; }
            public string Remark { get; set; } = string.Empty;
            public string Traceability { get; set; } = string.Empty;
            public string VolumeCharge { get; set; } = string.Empty;
            public override string ToString()
            {
                return $"[{ServiceCnName}({ServiceCode})]费用明细：\r\n运费:{FreightFee}\r\n燃油费:{FuelFee}\r\n挂号费:{RegisteredFee}\r\n其他费用:{OtherFee}\r\n总费用:{TotalFee}";
            }
        }

        public Tuple<string, List<string>> CheckResult()
        {
            string msg = "";
            List<string> datas = new List<string>();
          
            return Tuple.Create(msg, datas);
        }
    }
  

}
