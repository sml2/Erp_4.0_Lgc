using Aop.Api.Domain;
using Newtonsoft.Json;
using NPOI.OpenXmlFormats.Dml.Diagram;

namespace ERP.Models.Api.Logistics.SDGJ
{
    public class InterceptRequest: CommonParameter
    {
        public InterceptRequest(Parameter p, string CustomNumber, Platforms platform) : base(p, platform)
        {
            this.CustomNumber = CustomNumber;
        }

        public string CustomNumber { get; set; }

        public override string Url => base.Url;

        public override string serviceMethod => "removeorder";

        public override string ToPayload()
        {
            return $"appToken={appToken}&appKey={appKey}&serviceMethod={serviceMethod}&paramsJson={{\"reference_no\":\"{CustomNumber}\"}}";
        }
    }

    public class InterceptResponse
    {
        /// <summary>
        /// 是否成功标志，0代表失败；1代表成功
        /// </summary>
        public int success { get; set; }

        /// <summary>
        /// 中文消息
        /// </summary>
        public string cnmessage { get; set; }

        /// <summary>
        /// 英文消息
        /// </summary>
        public string enmessage { get; set; }
    }

    public class BillingRequest : CommonParameter
    {       
        public BillingRequest(Parameter p, string CustomNumber,string begin,string end, Platforms platform) : base(p, platform)
        {
            this.CustomNumber = CustomNumber;
            BeginDate = begin;
            EndDate = end;
        }

        /// <summary>
        /// 客户参考号
        /// </summary>
        public string CustomNumber { get; set; } = "";

        /// <summary>
        /// 服务商单号
        /// </summary>
        public string ShippingMethodNo { get; set; } = "";

        /// <summary>
        /// 到货开始时间
        /// </summary>
        public string BeginDate { get; set; } = "";

        /// <summary>
        /// 到货结束时间 
        /// </summary>
        public string EndDate { get; set; } = "";

        public override string Url => base.Url;

        public override string serviceMethod => "changefee";


        //{"reference_no":"65216541854168","shipping_method_no":"","begin_date":"2022-01-01 00:00:00","end_date":"2022-01-10 23:59:59"}

        public override string ToPayload()
        {
            return $"appToken={appToken}&appKey={appKey}&serviceMethod={serviceMethod}&paramsJson={{\"reference_no\":\"{CustomNumber}\",\"shipping_method_no\":\"{ShippingMethodNo}\",\"begin_date\":\"{BeginDate}\",\"end_date\":\"{EndDate}\"}}";
        }
    }



    public class BillResponse
    {
        [JsonProperty("success")]
        /// <summary>
        /// 是否成功标志，0代表失败；1代表成功
        /// </summary>
        public int  Success { get; set; }

        [JsonProperty("cnmessage")]
        /// <summary>
        /// 中文消息
        /// </summary>
        public string  Cnmessage { get; set; }

        [JsonProperty("enmessage")]
        /// <summary>
        /// 英文消息
        /// </summary>
        public string Enmessage { get; set; }

        [JsonProperty("data")]
        /// <summary>
        /// 成功时返回的数据
        /// </summary>
        public List<Data> Datas { get; set; }
        

        public class Data
        {
            [JsonProperty("customer_order_number")]
            /// <summary>
            /// 客户单号
            /// </summary>
            public string CustomerOrderNumber { get; set; }

            [JsonProperty("channel_hawbcode")]
            
            /// <summary>
            /// 运单号
            /// </summary>
            public string ChannelHawbcode { get; set; }

            [JsonProperty("fee_type")]
            /// <summary>
            /// 账户类型
            /// </summary>
            public string FeeType { get; set; }

            [JsonProperty("fk_name")]
            /// <summary>
            /// 费用类型
            /// </summary>
            public string FkName { get; set; }


            [JsonProperty("amount")]
            /// <summary>
            /// 金额
            /// </summary>
            public string Amount { get; set; }

            [JsonProperty("currency")]
            /// <summary>
            /// 币种
            /// </summary>
            public string Currency { get; set; }

            [JsonProperty("sc_note")]
            /// <summary>
            /// 备注
            /// </summary>
            public string ScNote   { get; set; }

            [JsonProperty("arrival_date")]
            /// <summary>
            /// 到货时间
            /// </summary>
            public string ArrivalDate     { get; set; }

            [JsonProperty("createdate")]
            /// <summary>
            /// 费用变动时间
            /// </summary>
            public string CreateDate   { get; set; }
        }
    }
}
