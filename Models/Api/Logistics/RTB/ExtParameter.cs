﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.SDGJ
{
    public class ExtParameter: BaseExtParam
    {
        /// <summary>
        /// 货品申报名(中文)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货品申报名(中文)")]
        public string ApplicationNameCn { get; set; } = string.Empty;

        /// <summary>
        /// 货品申报名(英文)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货品申报名(英文)")]
        public string ApplicationName { get; set; } = string.Empty;

        /// <summary>
        /// 海关编码
        /// </summary>     
        public string HsCode { get; set; } = string.Empty;

        /// <summary>
        /// IOSS税号
        /// </summary>
        public string IOSS { get; set; } = string.Empty;

        /// <summary>
        /// 使用备案IOSS
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("使用备案IOSS")]
        public bool IsEnableFiling { get; set; }


        [DisplayName("收件人税号")]
        public string ReceiveVatNumber { get; set; }

        /// <summary>
        /// 包裹边长(cm)
        /// </summary>
        //[Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹边长(cm)")]
        public int Length { get; set; }

        /// <summary>
        /// 包裹边宽(cm)
        /// </summary>
       // [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹边宽(cm)")]
        public int Width { get; set; }

        /// <summary>
        /// 包裹边高(cm)
        /// </summary>
       // [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹边高(cm)")]
        public int Height { get; set; }

        /// <summary>
        /// 包裹重量(kg)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹重量")]
        public double Weight { get; set; }

        /// <summary>
        /// 申报价格($)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("申报价格")]
        public decimal Declare { get; set; }

        /// <summary>
        /// 货运方式
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货运方式")]
        public string ShipMethod { get; set; } = string.Empty;

        [Required(ErrorMessage = "{0} 不能为空")]
        /// <summary>
        /// 币种
        /// </summary>
        public string Currency { get; set; }        
    }
}
