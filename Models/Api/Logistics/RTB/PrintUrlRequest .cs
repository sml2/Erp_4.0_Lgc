﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.SDGJ
{
    public class PrintUrlRequest : CommonParameter
    {
        public override string ToPayload()
        {
            return $"appToken={appToken}&appKey={appKey}&serviceMethod={serviceMethod}&paramsJson={base.ToPayload()}";            
        }

        public PrintUrlRequest(Parameter p,string FileType,string ContentType,string PaperType,List<string> customNumbers, Platforms pl) : base(p,pl)
        {
            var configInfo = new PrintUrlRequest.Cls_configInfo();
            configInfo.lable_file_type = FileType;
            configInfo.lable_content_type = ContentType;
            configInfo.lable_paper_type = PaperType;            
            configInfo.additional_info = new PrintUrlRequest.Cls_configInfo.Cls_additional_info();
            configInfo.additional_info.lable_print_invoiceinfo = "N";
            configInfo.additional_info.lable_print_buyerid = "N";
            configInfo.additional_info.lable_print_datetime = "Y";
            configInfo.additional_info.customsdeclaration_print_actualweight = "N";
            this.configInfo = configInfo;
            listorder = new List<PrintUrlRequest.Cls_listorder>();
           
            foreach(var customNumber in customNumbers)
            {
                PrintUrlRequest.Cls_listorder order = new PrintUrlRequest.Cls_listorder();
                order.reference_no = customNumber;
                listorder.Add(order);
            }                  
            this.listorder = listorder;
        }

        public PrintUrlRequest(Parameter p, string FileType, string PaperType, string ContentType,  string customNumber, Platforms pl) : base(p, pl)
        {
            var configInfo = new PrintUrlRequest.Cls_configInfo();
            configInfo.lable_file_type = FileType;
            configInfo.lable_content_type = ContentType;
            configInfo.lable_paper_type = PaperType;
            configInfo.additional_info = new PrintUrlRequest.Cls_configInfo.Cls_additional_info();
            configInfo.additional_info.lable_print_invoiceinfo = "N";
            configInfo.additional_info.lable_print_buyerid = "N";
            configInfo.additional_info.lable_print_datetime = "Y";
            configInfo.additional_info.customsdeclaration_print_actualweight = "N";
            this.configInfo = configInfo;
            listorder = new List<PrintUrlRequest.Cls_listorder>();
            PrintUrlRequest.Cls_listorder order = new PrintUrlRequest.Cls_listorder();
            order.reference_no = customNumber;
            listorder.Add(order);
            this.listorder = listorder;
        }

        public override string serviceMethod => "getnewlabel";

        public Cls_configInfo configInfo { get; set; }
        public class Cls_configInfo
        {
            /// <summary>
            /// 标签文件类型1：PNG文件 2：PDF文件
            /// </summary>
            [DisplayName("标签文件类型")]
            [Required(ErrorMessage = "{0} 不能为空")]
            public string lable_file_type { get; set; } = string.Empty;

            /// <summary>
            /// 纸张类型1：标签纸 2：A4纸
            /// </summary>           
            public string lable_paper_type { get; set; } = string.Empty;

            /// <summary>
            /// 标签内容类型代码 1：标签 2：报关单 3：配货单 4：标签+报关单 5：标签+配货单 6：标签+报关单+配货单
            /// </summary>
            [DisplayName("标签内容类型代码")]
            [Required(ErrorMessage = "{0} 不能为空")]
            public string lable_content_type { get; set; } = string.Empty;

            public Cls_additional_info additional_info { get; set; } = new();
            public class Cls_additional_info
            {
                /// <summary>
                /// 标签上打印配货信息 (Y:打印 N:不打印) 默认 N:不打印
                /// </summary>
                public string lable_print_invoiceinfo { get; set; } = string.Empty;
                /// <summary>
                /// 标签上是否打印买家ID (Y:打印 N:不打印) 默认 N:不打印
                /// </summary>
                public string lable_print_buyerid { get; set; } = string.Empty;
                /// <summary>
                /// 标签上是否打印日期 (Y:打印 N:不打印) 默认 Y:打印
                /// </summary>
                public string lable_print_datetime { get; set; } = string.Empty;
                /// <summary>
                /// 报关单上是否打印实际重量 (Y:打印 N:不打印) 默认 N:不打印
                /// </summary>
                public string customsdeclaration_print_actualweight { get; set; } = string.Empty;
            }
        }
        public List<Cls_listorder> listorder { get; set; }

        public class Cls_listorder
        {
            /// <summary>
            /// 客户参考号
            /// </summary>
            public string reference_no { get; set; } = string.Empty;
        }
    }
}
