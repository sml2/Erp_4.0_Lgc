﻿namespace ERP.Models.Api.Logistics
{
    public class TrackOrderInfo
    {              
        /// <summary>
        /// 运单主键id
        /// </summary>
        public int waybillId { get; set; }
        /// <summary>
        /// 本系统订单号
        /// </summary>
        public string CustomNumber { get; set; }=string.Empty;

        /// <summary>
        /// 国际物流运单/面单号
        /// </summary>
        public string? LogictisNumber { get; set; }
       
        /// <summary>
        /// 物流跟踪号
        /// </summary>
        public string? TrackNumber { get; set; }
     
        /// <summary>
        /// 价格为零，要从Api读取运费（如果物流平台支持）
        /// </summary>
        public double? Price { get; set; }
        public string? Unit { get; set; }
    }


}
