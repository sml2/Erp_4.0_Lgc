﻿using System.ComponentModel.DataAnnotations;

namespace ERP.Models.Api.Logistics.Extension
{
    public class CountLengthAttribute: ValidationAttribute
    {
        private int MaxSize;

        private int MinSize;

        public CountLengthAttribute(int maxSize,int minSize)
        {
            MaxSize = maxSize;
            MinSize = minSize;
        }
        public override bool IsValid(object? value)
        {
            if (value == null)
            {
                return true;
            }
            var content = value as List<string>;            
            if (content?.Count > MaxSize)
            {
                return false;
            }
            else if(content?.Count < MinSize)
            {
                return false;
            }
            else
            {
                return true;
            }            
        }
    }
}
