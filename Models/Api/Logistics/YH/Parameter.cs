﻿namespace ERP.Models.Api.Logistics.YH;
using ERP.Attributes.Logistics;

public class  Parameter: BaseParam
{
    private string key;
    private string token;
    private string id;
    [ViewConfigParameter(nameof(AppKey))]
    public string AppKey { get { return key; } set { key = value.Trim(); } } 
    [ViewConfigParameter(nameof(AppToken))]
    public string AppToken { get { return token; } set { token = value.Trim(); } } 

    [ViewConfigParameter(nameof(authorizationId), $"用户{nameof(authorizationId)}", Sort = 2, Required = false)]
    public string authorizationId { get { return id; } set { id = value.Trim(); } }
}
