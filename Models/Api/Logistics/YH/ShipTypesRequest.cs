﻿namespace ERP.Models.Api.Logistics.YH
{
    public class ShipTypesRequest: CommonParameter
    {
        public ShipTypesRequest(Parameter p) : base(p)
        {
            this.AppToken = p.AppToken;
            this.AppKey = p.AppKey;
        }

        public override string Url { get => base.DoMain + "/api/findWarehouse?AppKey=" + AppKey + "&AppToken=" + AppToken; }

        public override HttpMethod Method { get => HttpMethod.Get; }

        public new string AppToken { get; set; }
        public new string AppKey { get; set; }
    }
}
