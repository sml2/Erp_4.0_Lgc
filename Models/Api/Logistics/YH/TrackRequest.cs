﻿namespace ERP.Models.Api.Logistics.YH
{
    public class TrackRequest : CommonParameter
    {
        public TrackRequest(Parameter p,string ordeSn) : base(p)
        {
            this.AppToken = p.AppToken;
            this.AppKey = p.AppKey;
            this.OrdeSn = ordeSn;
        }

        public override string Url { get => base.DoMain + "/api/findLogistics?AppKey=" + AppKey + "&AppToken=" + AppToken + "&orderSn=" + OrdeSn; }

        public override HttpMethod Method { get => HttpMethod.Get; }

        /// <summary>
        /// 应用token
        /// 必填
        /// </summary>
        public new string AppToken { get; set; } 

        /// <summary>
        /// 应用key
        /// 必填
        /// </summary>
        public new string AppKey { get; set; } 

        /// <summary>
        /// 订单号 采购单号未填写时必填 两个都填写时以采购单为主
        /// </summary>
        public string OrdeSn { get; set; }

        /// <summary>
        /// 采购单号 订单号未填写是必填
        /// </summary>
        public string PurchaseSn { get; set; } = string.Empty;
    }
}
