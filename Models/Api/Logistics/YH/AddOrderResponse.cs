﻿namespace ERP.Models.Api.Logistics.YH
{
    public class AddOrderResponse:Response<orderinfo>
    {        
       
    }

    public class orderinfo
    {
        /// <summary>
        /// 订单号
        /// </summary>
        public string ordeSn { get; set; } = string.Empty;

        /// <summary>
        /// 采购单号
        /// </summary>
        public string purchaseSn { get; set; } = string.Empty;
    }
}
