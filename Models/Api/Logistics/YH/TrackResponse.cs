﻿namespace ERP.Models.Api.Logistics.YH
{
    public class TrackResponse : Response<cl_data>
    {
    }

    public class cl_data
    {
        /// <summary>
        /// 物流数据
        /// </summary>
        public List<logistic> logisticsList { get; set; } = new();
    }

    public class logistic
    {
        /// <summary>
        /// 实际收取运费
        /// </summary>
        public decimal actualFreightFee { get; set; }

        /// <summary>
        /// 平台预估运费
        /// </summary>
        public decimal platformFee { get; set; }

        /// <summary>
        /// 仓库打包费
        /// </summary>
        public decimal warehouseCost { get; set; }

        /// <summary>
        /// 物流商名称
        /// </summary>
        public string providerName { get; set; } = string.Empty;

        /// <summary>
        /// 物流商code
        /// </summary>
        public string providerCode { get; set; } = string.Empty;

        /// <summary>
        /// 物流货代方式名称
        /// </summary>
        public string logisticsProductName { get; set; } = string.Empty;

        /// <summary>
        /// 物流货代方式code
        /// </summary>
        public string logisticsProductCode { get; set; } = string.Empty;

        /// <summary>
        /// 参考号
        /// </summary>
        public string refNo { get; set; } = string.Empty;

        /// <summary>
        /// 追踪号
        /// </summary>
        public string trackingNo { get; set; } = string.Empty;

        /// <summary>
        /// 面单号
        /// </summary>
        public string dsConsignmentNo { get; set; } = string.Empty;

        public string status { get; set; } = string.Empty;

        /// <summary>
        /// 承运商
        /// </summary>
        public string carrier { get; set; }

        /// <summary>
        /// 配送方式
        /// </summary>
        public string deliveryMthod { get; set; }

        public override string ToString()
        {
            return $"物流商名称:{providerName},平台预估运费:{platformFee},实际收取运费:{actualFreightFee},仓库打包费:{warehouseCost},物流货代方式名称:{logisticsProductName},承运商:{carrier},配送方式:{deliveryMthod}";
        }
    }
}
