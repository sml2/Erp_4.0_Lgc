﻿namespace ERP.Models.Api.Logistics.YH
{
    public class Response<T>
    {
        public string message { get; set; } = string.Empty;

        /// <summary>
        /// 响应消息：200成功，其他失败
        /// </summary>
        public int state { get; set; }

        /// <summary>
        /// 响应标识 true 标识成功
        /// </summary>
        public bool success { get; set; }

        /// <summary>
        /// 时间戳
        /// </summary>
        public long timestamp { get; set; }


        public T data { get; set; }
    }
}
