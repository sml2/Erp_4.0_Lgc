﻿namespace ERP.Models.Api.Logistics.YH
{
    public class ShipTypeResponse: Response<List<ShipType>>
    {        
    }
    public class ShipType
    {
        /// <summary>
        /// 区
        /// </summary>
        public string areaName { get; set; } = string.Empty;

        /// <summary>
        /// 市
        /// </summary>
        public string cityName { get; set; } = string.Empty;

        /// <summary>
        /// 邮编
        /// </summary>
        public string post { get; set; } = string.Empty;

        /// <summary>
        /// 省
        /// </summary>
        public string provinceName { get; set; } = string.Empty;
        /// <summary>
        /// 详细地址
        /// </summary>
        public string street { get; set; } = string.Empty;

        /// <summary>
        /// 仓库code
        /// </summary>
        public string warehouseCode { get; set; } = string.Empty;

        /// <summary>
        /// 仓库名称
        /// </summary>
        public string warehouseName { get; set; }=string.Empty;

        public override string ToString()
        {
            return warehouseName;
        }
    }

}
