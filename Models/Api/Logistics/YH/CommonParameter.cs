﻿
using Newtonsoft.Json;
using static ERP.Models.Api.Logistics.YH.ExtParameter;

namespace ERP.Models.Api.Logistics.YH
{
    /// <summary>
    /// 接口相关的公共参数
    /// </summary>
    public abstract class CommonParameter: RequestLogistic
    {       
        public CommonParameter(Parameter p)
       {            
            this.AppKey = p.AppKey;
            this.AppToken = p.AppToken;                             
        }

        public string AppKey { get; set; }
        public string AppToken { get; set; }

        [JsonIgnore]
        public string DoMain
        {
            get
            {
                return "https://store.cloudboxfx.com";
            }
        }
    }
}
