﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.YH
{
    public class AddOrderRequest : CommonParameter
    {
        public AddOrderRequest(Parameter p, SenderInfo senderInfo, OrderInfo orderInfo, ExtParameter Ext) : base(p)
        {           
            this.Id = orderInfo.id;
            this.ext = JsonConvert.SerializeObject(Ext);            
            this.amazonOrderId= Helpers.GetOrderNo(orderInfo.OrderNo, orderInfo.SendNum, orderInfo.FailNum);
            if(!p.authorizationId.IsNullOrWhiteSpace())
            {
                this.authorizationId = p.authorizationId;
            }
            
            this.totalAmount = Ext.TotalAmount;
            this.originCountry = orderInfo.ReceiverAddress.NationShort;
            this.customsCode = Ext.CustomsCode;
            this.chineseName = Ext.ChineseName;
            this.englishName = Ext.EnglishName;
            this.currency =  Ext.Currency.Split('(')[1].Split(')')[0];
            this.exchangeRate = Ext.ExchangeRate==0?null: Ext.ExchangeRate;
            this.warehouseCode = Ext.ShipMethod;

            this.isStock = Ext.IsStock ? 1 : 0;
            this.isPush = Ext.IsPush ? 1 : 0;
            if (!Ext.IsPush)
            {                             
                Ext.WaybillName = "";
                Ext.WaybillNumber = "";
            }
            if (!Ext.IsStock)
            {
                Ext.manualSku = "";
            }
            this.manualSku = Ext.manualSku;
            this.Remarks = Ext.Remarks;
            AddOrderRequest.ReceiptAddress address = new AddOrderRequest.ReceiptAddress();
            address.area = orderInfo.ReceiverAddress.Province;
            address.city = orderInfo.ReceiverAddress.City;
            string Address = orderInfo.ReceiverAddress.Address;
            if (orderInfo.ReceiverAddress.Address2.IsNotWhiteSpace()) Address += $" {orderInfo.ReceiverAddress.Address2}";
            if (orderInfo.ReceiverAddress.Address3.IsNotWhiteSpace()) Address += $" {orderInfo.ReceiverAddress.Address3}";
            address.addressDetails = Address;
            address.contactName = orderInfo.ReceiverAddress.Name;
            address.country = orderInfo.ReceiverAddress.NationShort;
            address.phone = orderInfo.ReceiverAddress.Phone;
            address.postCode = orderInfo.ReceiverAddress.Zip ??string.Empty;
            address.taxNumber = Ext.TaxNumber ?? string.Empty;
            address.province = orderInfo.ReceiverAddress.Province;
            this.receiptAddress = address;
            //商品
            var GoodsList = new List<AddOrderRequest.Item>();
            int num = orderInfo.GoodInfos.Sum((r) => r.SendNum);
            orderInfo.GoodInfos.ForEach((r) =>
            {
                AddOrderRequest.Item Good = new AddOrderRequest.Item();
                Good.productName = Ext.ChineseName;
                Good.sku = r.Sku ?? string.Empty;
                Good.asin = r.Asin ?? string.Empty;
                Good.productNum = r.Count;
                Good.price =Helpers.RoundData(Ext.TotalAmount / num);
                Good.imgUrl = r.Url ?? string.Empty;
                Good.waybillName = Ext.WaybillName;
                Good.waybillNumber = Ext.WaybillNumber;
                GoodsList.Add(Good);
            });
            this.itemList = GoodsList;
        }                   
              
        public override string Url { get => base.DoMain+ "/api/createPurchaseOrder"; }
        
        public override HttpMethod Method { get => HttpMethod.Post;}

        public string Remarks { get; set; }

        [JsonIgnore]
        public int Id { get; set; }

        [JsonIgnore]
        public string ext { get; set; }       
       
        [DisplayName("仓库")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string warehouseCode { get; set; } = "";
        
        [DisplayName("订单编号")]
        public string? orderSn { get; set; } = "";
       
        [DisplayName("亚马逊订单号")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string amazonOrderId { get; set; } = "";
       
        [DisplayName("授权店铺标识")]
        public string? authorizationId { get; set; } = "";

        /// <summary>
        /// 配送渠道:AFN（亚马逊配送）,MFN（商家配送）
        /// </summary>
        [DisplayName("配送渠道")]
        public string channel { get; set; } = "MFN";

        /// <summary>
        /// 创建订单时必填
        /// </summary>
        [DisplayName("订单原始总金额")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public decimal totalAmount { get; set; }

        /// <summary>
        /// 创建订单时必填
        /// </summary>
        [DisplayName("币种汇率")]       
        public decimal? exchangeRate { get; set; }

        /// <summary>
        /// 币种 创建订单时必填
        /// </summary>
        [DisplayName("币种")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string currency { get; set; } = "";

        /// <summary>
        ///  创建订单时必填
        /// </summary>
        [DisplayName("来源国家二字码")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string originCountry { get; set; }

        /// <summary>
        /// 订单税费
        /// </summary>
        [DisplayName("订单税费")]
        public decimal taxAmount { get; set; }

        /// <summary>
        /// 订单原始金额
        /// </summary>
        [DisplayName("订单原始金额")]
        public decimal originaTotalAmount { get; set; }


        /// <summary>
        /// 海关编码
        /// 必填
        /// </summary>
        [DisplayName("海关编码")]
        public string customsCode { get; set; }

        /// <summary>
        /// 是否推送至仓库0否 1是
        /// 必填
        /// </summary>
        [DisplayName("是否推送至仓库")]
        public int isPush { get; set; } = 0;

        [DisplayName("是否发库存")]
        public int isStock { get; set; } = 0;

        [DisplayName("库存匹配的SKU 发库存时必填")]
        public string manualSku { get; set; }

        /// <summary>
        /// 报关中文名称
        /// 必填
        /// </summary>
        [DisplayName("报关中文名称")]
        public string chineseName { get; set; } = "";

        /// <summary>
        /// 报关英文名称
        /// 必填
        /// </summary>
        [DisplayName("报关英文名称")]
        public string englishName { get; set; } = "";

        /// <summary>
        /// 收货地址 创建订单时必填
        /// </summary>
        public ReceiptAddress receiptAddress { get; set; }


        public class ReceiptAddress
        {
            /// <summary>
            /// 区
            /// </summary>
            public string area { get; set; } = "";


            /// <summary>
            /// 国家二字编码 创建订单时必填
            /// </summary>
            [DisplayName("国家二字编码")]
            [Required(ErrorMessage = "{0} 不能为空")]
            public string country { get; set; } = string.Empty;


            /// <summary>
            /// 省
            /// </summary>
            public string province { get; set; } = "";

            /// <summary>
            /// 市 创建订单时必填
            /// </summary>
            [MaxLength(200, ErrorMessage = "不能为空且字符长度不能大于200")]
            [DisplayName("市")]
            [Required(ErrorMessage = "{0} 不能为空")]
            public string city { get; set; } = string.Empty;

            /// <summary>
            /// 收货人电话 创建订单时必填
            /// </summary>
            [MaxLength(50, ErrorMessage = "不能为空且字符长度不能大于50")]
            [DisplayName("收货人电话")]
            [Required(ErrorMessage = "{0} 不能为空")]
            public string phone { get; set; } = string.Empty;

            /// <summary>
            /// 收货人姓名 创建订单时必填
            /// </summary>
            [MaxLength(100, ErrorMessage = "不能为空且字符长度不能大于100")]
            [DisplayName("收货人姓名")]
            [Required(ErrorMessage = "{0} 不能为空")]
            public string contactName { get; set; } = string.Empty;


            /// <summary>
            /// 门牌号
            /// </summary>
            [DisplayName("门牌号")]
            public string houseNumber { get; set; } = "";

            /// <summary>
            /// 税号
            /// </summary>
            [DisplayName("税号")]
            public string taxNumber { get; set; } = "";

            /// <summary>
            /// 详细地址 创建订单时必填
            /// </summary>           
            [MaxLength(500, ErrorMessage = "不能为空且字符长度不能大于500")]
            [DisplayName("收货地址详情")]
            [Required(ErrorMessage = "{0} 不能为空")]
            public string addressDetails { get; set; } = string.Empty;

            /// <summary>
            /// 邮编 创建订单时必填
            /// </summary>
            [MaxLength(20, ErrorMessage = "不能为空且字符长度不能大于20")]
            [DisplayName("收货邮编")]
            [Required(ErrorMessage = "{0} 不能为空")]
            public string postCode { get; set; } = string.Empty;

        }


        /// <summary>
        /// 采购商品详情
        /// </summary>
        public List<Item> itemList { get; set; }


        public class Item
        {
            /// <summary>
            /// 商品名称
            /// 必填
            /// </summary>
            [DisplayName("商品名称")]
            [Required(ErrorMessage = "{0} 不能为空")]
            public string productName { get; set; } = string.Empty;

            /// <summary>
            /// 采购价格
            /// 必填
            /// </summary>
            [DisplayName("采购价格")]
            [Required(ErrorMessage = "{0} 不能为空")]
            public decimal price { get; set; }

            /// <summary>
            /// 物流商 推送至仓库时必填
            /// </summary>
            public string waybillName { get; set; } = "";


            /// <summary>
            /// 物流单号 推送至仓库时必填
            /// </summary>
            public string waybillNumber { get; set; } = "";

            /// <summary>
            /// Sku
            /// 必填
            /// </summary>
            [DisplayName("商品Sku")]
            [Required(ErrorMessage = "{0} 不能为空")]
            public string sku { get; set; } = string.Empty;

            /// <summary>
            /// 商品asin 创建订单是必填
            /// </summary>
            [DisplayName("商品asin")]
            [Required(ErrorMessage = "{0} 不能为空")]
            public string asin { get; set; } = string.Empty;

            /// <summary>
            /// 商品数量
            /// 必填
            /// </summary>
            [DisplayName("商品数量")]
            [Required(ErrorMessage = "{0} 不能为空")]
            public int productNum { get; set; }

            /// <summary>
            /// 商品图片
            /// </summary>
            [DisplayName("商品图片")]
            public string imgUrl { get; set; } = "";

            /// <summary>
            /// 商品原始金额
            /// </summary>
            [DisplayName("商品原始金额")]
            public decimal originalUnitPrice { get; set; }
        }
    }   
}
