﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static EnumExtension;

namespace ERP.Models.Api.Logistics.YH
{
    public class ExtParameter: BaseExtParam
    {
        public ExtParameter(string chineseName, string englishName, string? taxNumber, string customsCode, string shipMethod, 
            decimal totalAmount, string currency, decimal exchangeRate)
        {
            ChineseName = chineseName;
            EnglishName = englishName;
            TaxNumber = taxNumber;
            CustomsCode = customsCode;
            ShipMethod = shipMethod;
            TotalAmount = totalAmount;
            Currency = currency;
            ExchangeRate = exchangeRate;
        }       
       
        [DisplayName("报关名称(中文)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string ChineseName { get; set; }

        [DisplayName("报关名称(英文)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string EnglishName { get; set; }
        
        [DisplayName("收件人税号")]
        public string? TaxNumber { get; set; } = "";

        [MaxLength(100, ErrorMessage = "不能为空且字符长度不能大于100")]
        [DisplayName("海关编码")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string CustomsCode { get; set; }
        
        [DisplayName("货运方式")]

        [Required(ErrorMessage = "请选择{0}")]
        public string ShipMethod { get; set; }
                    
        [DisplayName("订单金额")]
        [Required(ErrorMessage = "{0}不能为空且必须大于零")]
        public decimal TotalAmount { get; set; }
        
        [DisplayName("订单币种")]
        [Required(ErrorMessage = "{0}不能为空")]        
        public string Currency { get; set; }
        
        [DisplayName("订单汇率")]     
        public decimal? ExchangeRate { get; set; }


        [DisplayName("是否推送到仓库")]
        [Required(ErrorMessage = "{0}不能为空")]
        public bool IsPush { get; set; }

        [DisplayName("物流商")]        
        public string WaybillName { get; set; }

        [DisplayName("物流单号")]       
        public string WaybillNumber { get; set; }

        [DisplayName("是否发库存")]
        [Required(ErrorMessage = "{0}不能为空")]
        public bool IsStock { get; set; }

        [DisplayName("manualSku")]
        public string manualSku { get; set; }

        [DisplayName("备注")]       
        public string Remarks { get; set; }
    }
}
