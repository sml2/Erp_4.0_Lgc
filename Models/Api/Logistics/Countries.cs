﻿namespace ERP.Models.Api.Logistics
{
    public class Countries
    {
        public Countries( Exception e )
        {
            exception = e;
            Success = false;
        }
        public bool Success { get; set; }
        public Exception exception { get; set; } = new();

        public Countries(List<CommonResult> destination, List<CommonResult> departure)
        {
            Success = true;
            Destination = destination;
            Departure = departure;
        }

        public Countries(List<CommonResult> destination)
        {
            Success = true;
            Destination = destination;          
        }

        /// <summary>
        /// 目的地
        /// </summary>
        public List<CommonResult> Destination { get; set; } = new();

        /// <summary>
        /// 出发地
        /// </summary>
        public List<CommonResult> Departure { get; set; } = new();
    }
}
