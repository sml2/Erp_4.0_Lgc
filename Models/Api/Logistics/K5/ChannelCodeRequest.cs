﻿namespace ERP.Models.Api.Logistics.K5
{
    public class ChannelCodeRequest : CommonParameter
    {
        public ChannelCodeRequest(Parameter P) : base(P)
        {

        }

        public override string Url => $"{Domain}PostInterfaceService?method=searchStartChannel";

    }
}
