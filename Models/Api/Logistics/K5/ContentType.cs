﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static EnumExtension;

namespace ERP.Models.Api.Logistics.K5
{
    public enum ContentType
    {
        //1：地址标签 2：报关单 3：配货信息 
        [Description("地址标签")]
        And1,
        [Description("报关单")]
        And2,
        [Description("配货信息")]
        And3,
        [Description("地址标签+报关单")]
        And1And2,
        [Description("地址标签+配货信息")]
        And1And3,
        [Description("报关单+配货信息")]
        And2And3,
        [Description("地址标签+报关单+配货信息")]
        And1And2And3,
    }

    public enum PaperType
    {
        [Description("A4")]
        A4,
    }

}
