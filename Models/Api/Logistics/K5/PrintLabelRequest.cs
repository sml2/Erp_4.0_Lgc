﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.K5
{

    /// <summary>
    /// 打印面单+获取追踪号
    /// </summary>
    public class PrintLabelRequest:CommonParameter
    {
        public PrintLabelRequest(Parameter P,List<string> logNumbers,string PaperType,string ContentType) : base(P)
        {
            CorpBillidDatas = new List<PrintLabelRequest.CorpBillidDatasItem>();
            if(logNumbers.Count>0)
            {
                foreach(var s in logNumbers)
                {
                    var OrderItem = new PrintLabelRequest.CorpBillidDatasItem();
                    OrderItem.CorpBillid = s;
                    CorpBillidDatas.Add(OrderItem);
                }
            }                  
            PrintPaper = PaperType;
            PrintContent = ContentType;
        }

        public PrintLabelRequest(Parameter P, string logNumber, string PaperType, string ContentType) : base(P)
        {
            CorpBillidDatas = new List<PrintLabelRequest.CorpBillidDatasItem>();
            var OrderItem = new PrintLabelRequest.CorpBillidDatasItem();
            OrderItem.CorpBillid = logNumber;
            CorpBillidDatas.Add(OrderItem);
            PrintPaper = PaperType;
            PrintContent = ContentType;
        }

        public override string Url => $"{Domain}PostInterfaceService?method=printOrderLabel";
        
        public List<CorpBillidDatasItem> CorpBillidDatas { get; set; }
        public class CorpBillidDatasItem
        {
            public string CorpBillid { get; set; } = string.Empty;
        }
        public string OrderType { get; set; } = "1";

        [DisplayName("打印纸")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string PrintPaper { get; set; }

        [DisplayName("打印内容")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string PrintContent { get; set; }
    }
}
