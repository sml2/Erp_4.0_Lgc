using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.K5
{
    public class AddOrderRequest : CommonParameter
    {
        public AddOrderRequest(Parameter P, List<OrderDatasItem> orderDatas) : base(P)
        {
            OrderDatas = orderDatas;
        }

        public override string Url => $"{Domain}PostInterfaceService?method=createOrder";
        /// <summary>
        /// 订单类型 1：快件订单 2：快递制单-非实时返回单号 3：仓储订单 4：快递制单-实时返回单号(等待时间较长)。此方法选择 4，后续如需调用其他方法，例如调用删除接口，其他方法 OrderType 请选择 2。 
        /// </summary>
        public string OrderType { get; set; } = "1";

        public List<OrderDatasItem> OrderDatas { get; set; }
        public class OrderDatasItem
        {
            public OrderDatasItem(SenderInfo senderInfo, OrderInfo orderInfo, ExtParameter Ext)
            {               
                CustomerNumber = Helpers.GetOrderNo(orderInfo.OrderNo, orderInfo.SendNum, orderInfo.FailNum);
                ChannelCode = Ext.ShipMethod;
                CountryCode = orderInfo.ReceiverAddress.NationShort;
                TotalWeight =Helpers.RoundData(Ext.Weight/1000,2);
                TotalValue = Ext.Price;
                Number = 1;
                VatNumber = Ext.VatNumber;
                TariffType = "";
                Recipient = new OrderDatasItem.RecipientItem();
                Recipient.Name = orderInfo.ReceiverAddress.Name;
                string Address1 = orderInfo.ReceiverAddress.Address;
                string Address2 = $"{orderInfo.ReceiverAddress.Address2} {orderInfo.ReceiverAddress.Address3}";
                if (Address1.IsNullOrEmpty())
                {
                    Address1 = orderInfo.ReceiverAddress.Address2??"";
                    Address2 = orderInfo.ReceiverAddress.Address3??"";
                }
                if (Address1.IsNullOrEmpty())
                {
                    Address1 = orderInfo.ReceiverAddress.Address3 ?? "";
                    Address2 = "";
                }
                Recipient.Addres1 = Address1;
                Recipient.Addres2 = Address2;
                Recipient.Tel = orderInfo.ReceiverAddress.Phone;
                Recipient.Mobile = orderInfo.ReceiverAddress.Phone;
                Recipient.Province = orderInfo.ReceiverAddress.Province;
                Recipient.City = orderInfo.ReceiverAddress.City;
                Recipient.Post = orderInfo.ReceiverAddress?.Zip?? "";
                Recipient.Email = orderInfo.ReceiverAddress?.Email??"";
                Recipient.Conta = Ext.ReceiveVatNumber;
                Sender = new AddOrderRequest.OrderDatasItem.SenderItem();
                Sender.Name = senderInfo.Name;
                Sender.Addres = senderInfo.Address;
                Sender.Mobile = senderInfo.PhoneNumber;
                Sender.Province = senderInfo.Province;
                Sender.City = senderInfo.City;
                Sender.Post = senderInfo.PostalCode;
                Sender.Email = senderInfo.Mail;
                OrderItems = new List<AddOrderRequest.OrderDatasItem.OrderItemsItem>();
                int num = orderInfo.GoodInfos.Sum((r) => r.Num);
                foreach (var v in orderInfo.GoodInfos)
                {
                    var OrderItem = new AddOrderRequest.OrderDatasItem.OrderItemsItem();
                    OrderItem.Cnname = Ext.ProNameCn;
                    OrderItem.Enname = Ext.ProNameEn;
                    OrderItem.Price =Helpers.RoundData(Ext.Price / num);
                    OrderItem.Weight = Helpers.RoundData(TotalWeight /num);
                    OrderItem.Num = v.Count;
                    OrderItem.Money = Ext.Currency;
                    OrderItem.CustomsCode = Ext.HasCode;
                    OrderItems.Add(OrderItem);
                }
                Volumes = new List<AddOrderRequest.OrderDatasItem.VolumesItem>();
                var Volume = new AddOrderRequest.OrderDatasItem.VolumesItem();
                Volume.Weight = TotalWeight;
                Volume.Number = 1;
                Volume.Length = Ext.Length;
                Volume.Width = Ext.Width;
                Volume.Height = Ext.Height;
                Volumes.Add(Volume);
            }

            [JsonIgnore]
            public string orderNo { get; set; } = string.Empty;
            public string CustomerNumber { get; set; }

            public string TrackNumber { get; set; } = string.Empty;
            /// <summary>
            /// 渠道代码 
            /// </summary>
            public string ChannelCode { get; set; }

            public string CountryCode { get; set; }

            /// <summary>
            /// kg
            /// </summary>
            public double TotalWeight { get; set; }
            /// <summary>
            /// 订单总申报价值
            /// </summary>
            public double TotalValue { get; set; }

            public int Number { get; set; }
            public string GoodsType => "WPX";

            /// <summary>
            /// 寄件人税号
            /// </summary>
            public string VatNumber { get; set; }
            /// <summary>
            /// 关税类型（快件订单对接中邮渠道填写特殊类型。
            /// 1300：预缴增值税 IOSS,
            /// 1301：预缴增值税 no-IOSS,
            /// 1302：预缴增值税 other）            
            /// </summary>
            public string TariffType { get; set; }
            public RecipientItem Recipient { get; set; }
            public class RecipientItem
            {
                public string Name { get; set; } = string.Empty;
                public string Addres1 { get; set; } = string.Empty;
                public string Addres2 { get; set; } = string.Empty;
                public string Tel { get; set; } = string.Empty;
                public string Mobile { get; set; } = string.Empty;
                /// <summary>
                /// 省州
                /// </summary>
                public string Province { get; set; } = string.Empty;
                /// <summary>
                /// 城市
                /// </summary>
                public string City { get; set; } = string.Empty;
                /// <summary>
                /// 邮编
                /// </summary>
                public string Post { get; set; } = string.Empty;
                public string Email { get; set; } = string.Empty;

                /// <summary>
                /// 税号（收件人 Vat)
                /// </summary>
                public string Conta { get; set; } = string.Empty;
            }
            public SenderItem Sender { get; set; }
            public class SenderItem
            {
                public string Name { get; set; } = string.Empty;

                public string Addres { get; set; } = string.Empty;

                public string Mobile { get; set; } = string.Empty;
                // public string Tel { get; set; }
                public string Post { get; set; } = string.Empty;
                public string Province { get; set; } = string.Empty;

                public string City { get; set; } = string.Empty;

                public string Email { get; set; } = string.Empty;
            }
            public List<OrderItemsItem> OrderItems { get; set; }
            public class OrderItemsItem
            {
                /// <summary>
                /// OrderType 为仓储订单必传
                /// </summary>
                public string Sku { get; set; } = string.Empty;

                public string Cnname { get; set; } = string.Empty;

                public string Enname { get; set; } = string.Empty;

                public double Price { get; set; }

                public double Weight { get; set; }

                public int Num { get; set; }
                /// <summary>
                /// 货币单位
                /// </summary>
                public string Money { get; set; } = string.Empty;
                /// <summary>
                /// 海关编码 
                /// </summary>
                public string CustomsCode { get; set; } = string.Empty;
            }
            public List<VolumesItem> Volumes { get; set; }
            public class VolumesItem
            {
                public double Weight { get; set; }

                public int Number { get; set; }

                public double Length { get; set; }

                public double Width { get; set; }

                public double Height { get; set; }
            }
        }
    }
}
