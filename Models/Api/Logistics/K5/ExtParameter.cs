﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.K5
{
    public class ExtParameter: BaseExtParam
    {
        /// <summary>
        /// 货品申报名(中文)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货品中文申报名")]
        public string ProNameCn { get; set; } = string.Empty;

        /// <summary>
        /// 货品申报名(英文)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货品英文申报名")]
        public string ProNameEn { get; set; } = string.Empty;

        /// <summary>
        /// 海关编码
        /// </summary>
        public string HasCode { get; set; } = string.Empty;

        /// <summary>
        /// 寄件人税号
        /// </summary>
        //[Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("寄件人税号")]
        public string VatNumber { get; set; } = string.Empty;


        [DisplayName("收件人税号")]
        public string ReceiveVatNumber { get; set; } = string.Empty;


        /// <summary>
        ///  包裹边长(cm)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹边长(cm)")]
        public double Length { get; set; }

        /// <summary>
        /// 包裹边宽(cm)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹边宽(cm)")]
        public double Width { get; set; }


        /// <summary>
        /// 包裹边高(cm)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹边高(cm)")]
        public double Height { get; set; }

        /// <summary>
        ///包裹重量(kg)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹重量(kg)")]
        public double Weight { get; set; }

        /// <summary>
        /// 货运方式
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货运方式")]
        public string ShipMethod { get; set; } = string.Empty;

        /// <summary>
        /// 申报价格($)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("申报价格")]
        public double Price { get; set; }

        [Required(ErrorMessage = "{0} 不能为空")]
        /// <summary>
        /// 币种
        /// </summary>
        public string Currency { get; set; }

    }
}
