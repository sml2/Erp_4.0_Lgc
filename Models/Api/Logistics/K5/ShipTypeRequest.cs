﻿namespace ERP.Models.Api.Logistics.K5
{
    public class ShipTypeRequest : CommonParameter
    {
        public ShipTypeRequest(Parameter P) : base(P)
        {
            Verify = new VerifyItem(P.ClientID, P.Token);
        }
      
        public override string Url =>$"{Domain}PostInterfaceService?method=searchStartChannel";
    }
}
