﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.K5
{
    public class PrintLabelResponse
    {
        public string statusCode { get; set; } = string.Empty;
        public string message { get; set; } = string.Empty;
        public string url { get; set; } = string.Empty;
        public string notExistsCorpbillid { get; set; } = string.Empty;
    }
}
