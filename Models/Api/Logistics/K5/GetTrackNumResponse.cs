﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.K5
{
    public class GetTrackNumResponse
    {
        public string statusCode { get; set; } = string.Empty;
        public string message { get; set; }=string.Empty;
        public List<ReturnDatas> returnDatas { get; set; } = new();
        public class ReturnDatas
        {
            public string statusCode { get; set; } = string.Empty;

            public string corpBillid { get; set; } = string.Empty;

            public string trackNumber { get; set; } = string.Empty;

            public string customerNumber { get; set; } = string.Empty;

            public string message { get; set; } = string.Empty;
        }
    }
}
