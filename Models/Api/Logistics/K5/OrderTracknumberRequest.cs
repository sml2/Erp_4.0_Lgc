﻿namespace ERP.Models.Api.Logistics.K5
{
    public class OrderTracknumberRequest : CommonParameter
    {
        public OrderTracknumberRequest(Parameter P, List<string> logNumbers) : base(P)
        {
            CorpBillidDatas = new List<OrderTracknumberRequest.CorpBillidDatasItem>();
            if (logNumbers.Count > 0)
            {
                foreach (var s in logNumbers)
                {
                    var OrderItem = new OrderTracknumberRequest.CorpBillidDatasItem();
                    OrderItem.CorpBillid = s;
                    CorpBillidDatas.Add(OrderItem);
                }
            }
            OrderType = "1";            
        }

        public OrderTracknumberRequest(Parameter P, string logNumbers) : base(P)
        {
            CorpBillidDatas = new List<OrderTracknumberRequest.CorpBillidDatasItem>();
            var OrderItem = new OrderTracknumberRequest.CorpBillidDatasItem();
            OrderItem.CorpBillid = logNumbers;
            CorpBillidDatas.Add(OrderItem);
            OrderType = "1";
        }

        public override string Url => $"{Domain}PostInterfaceService?method=searchOrderTracknumber";
       
        public List<CorpBillidDatasItem> CorpBillidDatas { get; set; }
        public class CorpBillidDatasItem
        {
            public string CorpBillid { get; set; } = string.Empty;
        }
        public string OrderType { get; set; }    
    }
}
