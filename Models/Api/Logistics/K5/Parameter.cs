﻿using Aspose.Cells;
using ERP.Attributes.Logistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ERP.Models.Api.Logistics.K5
{
    public class Parameter: BaseParam
    {
        private string domain;
        [ViewConfigParameter(nameof(Domain), "域名", Message = "请填写正确的域名，域名格式(xxx.kingtrans.cn)", Sort = 0)]
        /// <summary>
        /// xxx.kingtrans.cn 为代理的域名 
        /// </summary>
        public string Domain { get { return domain; } set { domain = value.Trim(); } } 
        public string? GetSite()
        {
            //防止跟随协议型url出现 "//:domain"
            string domain = Domain.Trim('/');
            //兼容客户输入协议类型的可能
            Sy.String Temp = (domain.StartsWith("http://", StringComparison.OrdinalIgnoreCase) || domain.StartsWith("https://", StringComparison.OrdinalIgnoreCase)) ? domain : $"http://{domain}";
            //删除URL路径
            var SB = new StringBuilder();
            if (Temp.IsNotNull())
            {
                var ds = Temp.Split("/");
                //if (ds.Length <= 2) { }//无需判断，上文决定Length一定大于等于3
                for (int i = 0; i < 3; i++)
                {
                    SB.Append($"{ds[i]}/");
                }
            }
            else
            {
                return null;
            }
            return SB.ToString();
        }
        private string id;
        [ViewConfigParameter(nameof(ClientID), Sort = 1)]
        public string ClientID { get { return id; } set { id = value.Trim(); } }

        private string token;
        [ViewConfigParameter(nameof(Token), Sort = 2)]
        public string Token { get { return token; } set { token = value.Trim(); } } 
    }  
}
