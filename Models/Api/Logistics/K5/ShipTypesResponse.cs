﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.K5
{

    public class ShipTypesResponse
    {
        public string statusCode { get; set; } = string.Empty;
        public string message { get; set; } = string.Empty;
        public List<ReturnDatas> returnDatas { get; set; } = new();
        public class ReturnDatas
        {
            public string code { get; set; } = string.Empty;

            public string cnname { get; set; } = string.Empty;

            public string enname { get; set; } = string.Empty;

            public string status { get; set; } = string.Empty;

            public string ifColl { get; set; } = string.Empty;
            public override string ToString()
            {
                return $"{cnname}[{code}]";
            }
        }    
    }
}
