﻿namespace ERP.Models.Api.Logistics.K5
{
    public class K5Parameters
    {
        /// <summary>
        /// 货运方式
        /// </summary>
        public string ShipCode { get; set; } = string.Empty;

        /// <summary>
        /// 寄件人税号
        /// </summary>
        public string VatNumber { get; set; } = string.Empty;

    }
}
