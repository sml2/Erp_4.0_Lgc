﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.K5
{
    public class TrackResponse
    {
        public string statusCode { get; set; } = string.Empty;
        public string message { get; set; } = string.Empty;
        public List<ReturnDatas> returnDatas { get; set; } = new();
        public class ReturnDatas
        {
            public string statusCode { get; set; } = string.Empty;
            public string trackNumber { get; set; } = string.Empty;
            public string message { get; set; } = string.Empty;
            public Track track { get; set; }=new();
            public class Track
            {
                public string billid { get; set; }=string.Empty;
                public string transBillid { get; set; } = string.Empty;
                public string country { get; set; } = string.Empty;
                public string destination { get; set; } = string.Empty;
                public string dateTime { get; set; } = string.Empty;
                public string status { get; set; } = string.Empty;
            }
            public List<Items> items { get; set; } = new();
            public class Items
            {
                public string dateTime { get; set; } = string.Empty;
                public string location { get; set; } = string.Empty;
                public string info { get; set; } = string.Empty;
                public override string ToString()
                {
                    return $"时间:[{dateTime}]地点:{location},轨迹描述:{info}";
                }
            }
        }      
    }
}


