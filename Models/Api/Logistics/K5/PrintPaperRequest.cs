﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.K5
{
    public class PrintPaperRequest:CommonParameter
    {
        public PrintPaperRequest(Parameter P,string code) : base(P)
        {
            ChannelCode = code;
        }

        public string ChannelCode { get; set; }

        public override string Url => $"{Domain}PostInterfaceService?method=searchPrintPaper";
    }
}
