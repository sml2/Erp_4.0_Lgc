﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.K5
{
    public class EstimatePriceRequest:CommonParameter
    {
        public EstimatePriceRequest(Parameter P, string countryCode, string weight) : base(P)
        {
            Verify = new VerifyItem(P.ClientID, P.Token);
            Data = new DataItem(countryCode, weight);
        }        
        public override string Url => $"{Domain}PostInterfaceService?method=searchPrice";
        
        public DataItem Data { get; set; }
        public class DataItem
        {
            public DataItem(string countryCode, string weight)
            {
                CountryCode = countryCode;
                Weight = weight;
            }

            public string CountryCode { get; set; }

            public string Weight { get; set; }
        }
    }
}
