﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.K5
{
    public class TrackRequest:CommonParameter
    {
        public TrackRequest(Parameter P, List<string> trackNumbers) : base(P)
        {
            List<DatasItem> datas = new List<DatasItem>();
            if (trackNumbers.Count>0)
            {
                foreach(var t in trackNumbers)
                {
                    datas.Add(new DatasItem(t));
                }
            }
            Datas = datas;
        }

        public TrackRequest(Parameter P, string trackNumber) : base(P)
        {
            List<DatasItem> datas = new List<DatasItem>();
            datas.Add(new DatasItem(trackNumber));
            Datas = datas;
        }

        public override string Url => $"{Domain}PostInterfaceService?method=searchTrack";
        public List<DatasItem> Datas { get; set; }
        public class DatasItem
        {
            public DatasItem(string trackNumber)
            {
                TrackNumber = trackNumber;
            }

            public string TrackNumber { get; set; }
        }
    }
}
