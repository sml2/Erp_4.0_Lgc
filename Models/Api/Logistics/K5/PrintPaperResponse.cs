﻿namespace ERP.Models.Api.Logistics.K5
{
    public class PrintPaperResponse
    {
        public string statusCode { get; set; } = string.Empty;
        public string message { get; set; } = string.Empty;
        public List<ReturnDatas> returnDatas { get; set; } = new();
        public class ReturnDatas
        {
            public string paperCode { get; set; } = string.Empty;
            public string paperName { get; set; } = string.Empty;
            public override string ToString()
            {
                return $"{paperName}";
            }
        }
    }
}
