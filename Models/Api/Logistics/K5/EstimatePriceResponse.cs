﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.K5
{
    public class EstimatePriceResponse
    {
        public string statusCode { get; set; } = string.Empty;
        public string message { get; set; } = string.Empty;
        public List<ReturnDatas> returnDatas { get; set; } = new();
        public class ReturnDatas
        {
            public string countryCode { get; set; } = string.Empty;

            public string countryName { get; set; } = string.Empty;

            public string channelCode { get; set; } = string.Empty;

            public string channelName { get; set; } = string.Empty;

            public string ccyCode { get; set; } = string.Empty;

            public string ccyName { get; set; } = string.Empty;

            public string priceName { get; set; } = string.Empty;

            public string weight { get; set; } = string.Empty;

            public string mweight { get; set; } = string.Empty;

            public string formula { get; set; } = string.Empty;

            public string rebate { get; set; } = string.Empty;

            public string fuel { get; set; } = string.Empty;

            public string feeAmt { get; set; } = string.Empty;

            public string sendtime { get; set; } = string.Empty;

            public string priceNote { get; set; } = string.Empty;

            public string productName { get; set; } = string.Empty;

            public string mcalid { get; set; } = string.Empty;
            public override string ToString()
            {
                //fuel：可能为包含燃油
                return $"{channelName}[{channelCode}]的运费为:{ccyCode}{feeAmt},燃油费为:{fuel}";//{ccyCode}
            }
        }         
    }
}
