﻿using Newtonsoft.Json;
using System.Text;

namespace ERP.Models.Api.Logistics.K5
{
    public class CommonParameter: RequestLogistic
    {
        public CommonParameter(Parameter P)
        {
            Verify = new VerifyItem(P.ClientID, P.Token);
             Domain = P.Domain;
        }
        /// <summary>
        /// xxx.kingtrans.cn 为代理的域名 
        /// </summary>
        public string Domain { get; set; }
        public VerifyItem Verify { get; set; }
        public class VerifyItem
        {
            public VerifyItem() { }
            public VerifyItem(string clientid, string token)
            {
                Clientid = clientid;
                Token = token;
            }
            public string Clientid { get; set; } = string.Empty;
            public string Token { get; set; } = string.Empty;
        }
        [JsonIgnore]
        public string Platform { get; set; } = string.Empty;
    }
}
