﻿namespace ERP.Models.Api.Logistics
{
    public class MultiPrint : BaseParameter
    {
        public List<PrintParam>? printParams { get; set; }

        public int waybillId { get; set; }

        /// <summary>
        /// 文件类型：PDF，PNG
        /// </summary>
        public string? FileType { get; set; }

        /// <summary>
        /// 标签内容类型
        /// </summary>
        public string? ContentType { get; set; }

        /// <summary>
        /// 纸张类型
        /// </summary>
        public string? PaperType { get; set; }       
    }


}
