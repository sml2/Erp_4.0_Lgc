﻿using ERP.Enums.Logistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static EnumExtension;


namespace ERP.Models.Api.Logistics.YunTu
{
    public class TrackResponse
    {
        public string RequestId { get; set; } = string.Empty;
        public string Timestamp { get; set; } = string.Empty;
        public string Code { get; set; } = string.Empty;
        public string Message { get; set; } = string.Empty;
        public Cls_Item Item { get; set; } = null!;
        public class Cls_Item
        {
            public string CountryCode { get; set; } = string.Empty;
            public string WayBillNumber { get; set; } = string.Empty;
            public string TrackingNumber { get; set; } = string.Empty;
            public object CreatedBy { get; set; } = string.Empty;
            /// <summary>
            /// 包裹状态0-未知，1-已提交 2-运输中 3-已签收，4-已收货，5-订单取消，6-投递失败，7-已退回
            /// </summary>
            public int PackageState { get; set; }
            public string OriginCountryCode { get; set; } = string.Empty;
            public string IntervalDays { get; set; } = string.Empty;
            public List<Cls_OrderTrackingDetails> OrderTrackingDetails { get; set; } = new();
            public class Cls_OrderTrackingDetails
            {
                public string ProcessDate { get; set; } = string.Empty;
                public string ProcessContent { get; set; } = string.Empty;
                public string ProcessLocation { get; set; } = string.Empty;

            }
       }              
    }

    public enum EnumPackageState
    {
        [Description("未知")]
        UnKnown = 0,
        [Description("已提交")]
        Reported = 1,
        [Description("运输中")]
        Transporting = 2,
        [Description("已签收")]
        Signed = 3,
        [Description("已收货")]
        GoodsReceived = 4,
        [Description("订单取消")]
        Cancel = 5,
        [Description("投递失败")]
        DeliveryFailed = 6,
        [Description("已退回")]
        Returned = 7,
    }
}
