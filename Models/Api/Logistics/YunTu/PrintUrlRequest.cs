﻿using Newtonsoft.Json;

namespace ERP.Models.Api.Logistics.YunTu
{
    public class PrintUrlRequest : CommonParameter
    {
        public PrintUrlRequest(Parameter p, string logNumber) : base(p)
        {
            CustomNumbers = new List<string> { logNumber };
        }
        public List<string> CustomNumbers { get; set; }

        [JsonIgnore]
        public override string Url => $"{Domain}/api/Label/Print";

        public override string ToPayload()
        {
            return JsonConvert.SerializeObject(CustomNumbers);
        }
    }
}
