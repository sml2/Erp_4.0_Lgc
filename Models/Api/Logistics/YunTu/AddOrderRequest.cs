using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.YunTu
{   
    public class AddOrderRequest:CommonParameter
    {
        public AddOrderRequest(Parameter p, SenderInfo senderInfo, OrderInfo orderInfo,ExtParameter Ext) : base(p)
        {         
            Id = orderInfo.id;
            orderNo = orderInfo.OrderNo;
            CustomerOrderNumber = Helpers.GetOrderNo(orderInfo.OrderNo, orderInfo.SendNum, orderInfo.FailNum);// orderInfo.OrderNo;
            ShippingMethodCode = Ext.ShipMethod;
            PackageCount = 1;//orderProducts.Length//包裹件数
            Length = Ext.Length;
            Width = Ext.Width;
            Height = Ext.Height;
            Weight = Helpers.RoundData(Convert.ToDecimal(Ext.Weight)/1000, 3);//Ext
            TaxNumber = Ext.RecipientTaxNumber;
            //收件人信息
            Receiver = new AddOrderRequest.Cls_ShippingInfos();
            Receiver.CountryCode = orderInfo.ReceiverAddress.NationShort;//城市简码
            Receiver.FirstName = orderInfo.ReceiverAddress.Name;
            string Address = orderInfo.ReceiverAddress.Address;
            if (orderInfo.ReceiverAddress.Address2.IsNotWhiteSpace()) Address += $" {orderInfo.ReceiverAddress.Address2}";
            if (orderInfo.ReceiverAddress.Address3.IsNotWhiteSpace()) Address += $" {orderInfo.ReceiverAddress.Address3}";
            Receiver.Street = Address;//传过来的值已加上iOrderInfo.receiver_district
            Receiver.City = orderInfo.ReceiverAddress.City;
            Receiver.State = orderInfo.ReceiverAddress.Province;
            Receiver.Zip = orderInfo.ReceiverAddress.Zip ?? "";
            Receiver.Phone = orderInfo.ReceiverAddress.Phone;
            Receiver.CertificateCode = Ext.CertificateCode;
            //寄件人信息
            Sender = new AddOrderRequest.Cls_SenderInfo();
            Sender.CountryCode = "CN";//城市简码
            Sender.FirstName = senderInfo.Name;
            Sender.Street = senderInfo.Address;
            Sender.City = senderInfo.City;
            Sender.State = senderInfo.Province;
            Sender.Zip = senderInfo.PostalCode;
            Sender.Phone = senderInfo.PhoneNumber;
            //商品信息
            Parcels = new List<AddOrderRequest.Cls_ApplicationInfos>();
            int num = orderInfo.GoodInfos.Sum((r) => r.Num);
            foreach (var r in orderInfo.GoodInfos)
            {
                AddOrderRequest.Cls_ApplicationInfos Pro = new AddOrderRequest.Cls_ApplicationInfos();
                Pro.EName = Ext.EName;
                string AppNameCn = Ext.CName;
                Pro.CName = AppNameCn;//r.proname.Substring(0, 50);
                Pro.HSCode = Ext.HsCode;
                Pro.Quantity = r.Count;
                Pro.UnitPrice = Helpers.RoundData(Convert.ToDecimal(Ext.NumDeclare / num)) ;
                Pro.UnitWeight = Helpers.RoundData(Convert.ToDecimal(Weight / num));
                Pro.Remark = "CreateOrder";
                Pro.SKU = r.Sku ?? string.Empty;
                Pro.CurrencyCode = Ext.Currency;
                //Pro.SKU = r.asin;
                Parcels.Add(Pro);
            }
            if (Ext.IossCode.IsNotWhiteSpace())
            {
                IossCode = Ext.IossCode;
                IossTag = Ext.IossTag?1:0;
            }
            else
            {
                OrderExtra = new List<AddOrderRequest.Cls_OrderExtra>() { new AddOrderRequest.Cls_OrderExtra("V1", "云途预缴") };
            }
        }      

        [JsonIgnore]
        public int Id { get; set; }

        [JsonIgnore]
        public string orderNo { get; set; }
        /// <summary>
        /// 客户订单号
        /// </summary>
        public string CustomerOrderNumber { get; set; }
        /// <summary>
        /// 货运方式
        /// </summary>
        public string ShippingMethodCode { get; set; }
        /// <summary>
        /// 运单包裹的件数，必须大于 0 的整数 
        /// </summary>
        public int PackageCount { get; set; }
        public string TotalFee => string.Empty;
        public string TotalQty => string.Empty;
        public string SettleWeight => string.Empty;
        /// <summary>
        /// 收件人税号 null
        /// </summary>
        public string  TaxNumber { get; set; }
        #region
        /// <summary>
        /// 预计包裹单边长（非必填项）
        /// </summary>
        public int Length { get; set; }
        /// <summary>
        /// 预计包裹单边宽（非必填项）
        /// </summary>
        public int Width { get; set; }
        /// <summary>
        /// 预计包裹单边高（非必填项）
        /// </summary>
        public int Height { get; set; }
        /// <summary>
        /// 预计包裹的总重量（KG），最多3位小数
        /// </summary>
        public decimal Weight { get; set; }
        public int PackageVolume => Length * Width * Height == 0 ? 1 : Length * Width * Height;
        #endregion
        public Cls_ShippingInfos Receiver { get; set; }

        public class Cls_ShippingInfos
        {
            public string CountryCode { get; set; } = string.Empty;
            public string FirstName { get; set; } = string.Empty;
            public string Street { get; set; } = string.Empty;
            //public string ShippingAddress1 { get; set; }
            //public string ShippingAddress2 { get; set; }
            public string City { get; set; } = string.Empty;
            public string State { get; set; } = string.Empty;
            public string Zip { get; set; } = string.Empty;
            public string Phone { get; set; } = string.Empty;
            public string CertificateCode { get; set; }
        }
        /// <summary>
        /// 非必填项
        /// </summary>
        public Cls_SenderInfo Sender { get; set; }
        public class Cls_SenderInfo
        {
            public string CountryCode { get; set; } = string.Empty;
            public string FirstName { get; set; } = string.Empty;
            public string Street { get; set; } = string.Empty;
            public string City { get; set; } = string.Empty;
            public string State { get; set; } = string.Empty;
            public string Zip { get; set; } = string.Empty;
            public string Phone { get; set; } = string.Empty;
        }
        /// <summary>
        /// 申报类型 用于打印（非必填项）
        /// </summary>
        public int ApplicationType => 1;
        /// <summary>
        /// 是否退回（非必填项）
        /// </summary>
        public bool IsReturn => true;
        /// <summary>
        /// 包裹投保类型（非必填项）
        /// </summary>
        public int InsuranceType => 1;
        /// <summary>
        /// 保险的最高额（非必填项）
        /// </summary>
        public decimal InsureAmount => 1000;
        /// <summary>
        /// 包裹中特殊货品类型，可调用货品类型查询服务 查询，可以不填写，表示普通货品 
        /// </summary>
        public int SensitiveTypeID => 1;
        public List<Cls_ApplicationInfos> Parcels { get; set; }
        public class Cls_ApplicationInfos
        {
            /// <summary>
            /// 包裹中货品(英文) 申报名称
            /// </summary>
            public string EName { get; set; } = string.Empty;
            /// <summary>
            /// 包裹的申报中文名称（非必填项）
            /// </summary>
            public string CName { get; set; } = string.Empty;
            /// <summary>
            /// 包裹中货品 申报编码（非必填项）
            /// </summary>
            public string HSCode { get; set; } = string.Empty;
            public int Quantity { get; set; }
            public decimal UnitPrice { get; set; }
            /// <summary>
            /// 单位重量
            /// </summary>
            public decimal UnitWeight { get; set; }
            /// <summary>
            /// 用于打印配货单（非必填项）
            /// </summary>
            public string Remark { get; set; } = string.Empty;
            //public string ProductUrl { get; set; }
            public string SKU { get; set; } = string.Empty;
            public string CurrencyCode { get; set; } = string.Empty;
        }
        public List<Cls_OrderExtra> OrderExtra { get; set; } = new();
        public class Cls_OrderExtra
        {
            public Cls_OrderExtra(string extraCode, string extraName)
            {
                ExtraCode = extraCode;
                ExtraName = extraName;
            }

            public string ExtraCode { get; set; }
            public string ExtraName { get; set; }
        }
        public string IossCode { get; set; }=string.Empty;
        public int IossTag { get; set; }
        //public string SourceCode { get; set; }
        //public List<Cls_ChildOrders> ChildOrders { get; set; }
        //public class Cls_ChildOrders
        //{
        //    public string BoxNumber { get; set; }
        //    public int Length { get; set; }
        //    public int Width { get; set; }
        //    public int Height { get; set; }
        //    public decimal BoxWeight { get; set; }
        //    public List<Cls_ChildDetails> ChildDetails { get; set; }
        //    public class Cls_ChildDetails
        //    {
        //        public string SKU { get; set; }
        //        public int Quantity { get; set; }
        //    }
        //}
    }
}
