﻿namespace ERP.Models.Api.Logistics.YunTu
{
    public class GetEstimatePriceRequest : CommonParameter
    {
        public GetEstimatePriceRequest(Parameter p, ExtParameter Ext, string countryCode) : base(p)
        {
            CountryCode = countryCode;
            Weight = Ext.Weight.ToString();
            Length = Ext.Length.ToString();
            Width = Ext.Width.ToString();
            Height = Ext.Height.ToString();
        }

        public string CountryCode { get; set; }
        public string Weight { get; set; }
        public string Length { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }

        public override HttpMethod Method => HttpMethod.Get;
        public override string Url => $"{Domain}/api/Freight/GetPriceTrial?CountryCode={CountryCode}&Weight={Weight}&Length={Length}&Width={Width}&Height={Height}&ShippingTypeId=1";
    }
}
