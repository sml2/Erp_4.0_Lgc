﻿using Newtonsoft.Json;

namespace ERP.Models.Api.Logistics.YunTu
{
    /// <summary>
    /// <a href="https://yunexpress-fileupload.oss-cn-shenzhen.aliyuncs.com/%E4%BA%91%E9%80%94%E7%89%A9%E6%B5%81API%E6%8E%A5%E5%8F%A3%E5%BC%80%E5%8F%91%E8%A7%84%E8%8C%83OMS20230706.pdf">文档链接</a>
    /// </summary>
    public class YunTuRequest : CommonParameter
    {
        public YunTuRequest(Parameter p) : base(p)
        {
        }

        public List<AddOrderRequest> Datas { get; set; } = new();

        [JsonIgnore]
        public override string Url => $"{Domain}/api/WayBill/CreateOrder";

        public override string ToPayload()
        {
            string json = JsonConvert.SerializeObject(this.Datas);
            return json;
        }
    }
}
