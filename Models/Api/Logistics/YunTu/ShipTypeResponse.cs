﻿
using Sy.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ERP.Models.Api.Logistics.YunTu
{

    public class ShipTypeResponse
    {
        public string RequestId { get; set; } = string.Empty;
        public string Code { get; set; } = string.Empty;
        public string Message { get; set; } = string.Empty;
        public string TimeStamp { get; set; } = string.Empty;
        public List<Cls_Item> Items { get; set; } = new();

        public class Cls_Item
        {
            private string price=string.Empty;

            private GetEstimatePriceResponse.Cls_Item item=new();
            public void SetAllPrice(GetEstimatePriceResponse.Cls_Item item)
            {
                this.item = item;
                price = item.TotalFee;
            }
            public string Code { get; set; } = string.Empty;
            public string CName { get; set; } = string.Empty;
            public string EName { get; set; } = string.Empty;
            public bool HasTrackingNumber { get; set; }
            public string DisplayName { get; set; } = string.Empty;
            public override string ToString()
            {
                string p = price.IsNotWhiteSpace() ? $"(估价:{price})" : "";
                return CName + $"[{Code}]{p}";
            }
        }     
    }

}
