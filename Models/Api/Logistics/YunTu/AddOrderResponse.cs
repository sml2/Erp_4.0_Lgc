﻿namespace ERP.Models.Api.Logistics.YunTu
{
    public class AddOrderResponse
    {
        public string RequestId { get; set; } = string.Empty;
        public string Timestamp { get; set; } = string.Empty;
        public string Code { get; set; } = string.Empty;
        /// <summary>
        /// 提交失败/提交成功
        /// </summary>
        public string Message { get; set; } = string.Empty;
        public List<Cls_Item> Item { get; set; } = new();//Count
        public class Cls_Item
        {
            public string CustomerOrderNumber { get; set; } = string.Empty;
            public int Success { get; set; }
            public object TrackType { get; set; } = new();
            public string Remark { get; set; } = string.Empty;
            //public string TrackStatus { get; set; }
            /// <summary>
            /// 提交失败/提交成功
            /// </summary>
            //public string Feedback { get; set; }
            public object AgentNumber { get; set; } = new();
            public string WayBillNumber { get; set; } = string.Empty;
            public int RequireSenderAddress { get; set; }
            //public List<string> LabelUrl { get; set; }
            public string TrackingNumber { get; set; } = string.Empty;
            public object ShipperBoxs { get; set; } = new();
        }


        public Tuple<string, List<ReportResult>> CheckResult()
        {
            string msg = "";
            List<ReportResult> results = new List<ReportResult>();
          
            return Tuple.Create(msg, results);
        }
    }
}
