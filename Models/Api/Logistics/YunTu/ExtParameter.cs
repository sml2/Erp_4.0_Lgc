using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.YunTu
{
    public class ExtParameter: BaseExtParam
    {       
        /// <summary>
        /// 货品申报名(中文)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货品申报名(中文)")]
        [StringLength(maximumLength: 50, ErrorMessage = "货品申报名(中文)长度不能超过50个字符")]
        public string CName { get; set; } = string.Empty;

        /// <summary>
        /// 货品申报名(英文)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货品申报名(英文)")]
        public string EName { get; set; } = string.Empty;

        /// <summary>
        /// 海关编码
        /// </summary>       
        [DisplayName("海关编码")]
        public string HsCode { get; set; } = string.Empty;

        /// <summary>
        /// 是否明文传输
        /// </summary>
        //[Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("是否明文传输")]
        public bool IossTag { get; set; }

        /// <summary>
        /// IOSS税号
        /// </summary>
        //[Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("IOSS税号")]
        public string IossCode { get; set; } = string.Empty;

        /// <summary>
        /// 边长(cm)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("边长(cm)")]
        public int Length { get; set; }

        /// <summary>
        /// 边宽 (cm)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("边宽(cm)")]
        public int Width { get; set; }

        /// <summary>
        /// 边高(cm)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("边高(cm)")]
        public int Height { get; set; }

        /// <summary>
        /// 包裹重量(kg)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹重量(g)")]
        public double Weight { get; set; }

        /// <summary>
        /// 收 件 人 税 号
        /// </summary>
        //[Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("收件人税号")]
        public string RecipientTaxNumber { get; set; } = string.Empty;

        /// <summary>
        /// 货运方式
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货运方式")]
        public string ShipMethod { get; set; } = string.Empty;


        /// <summary>
        /// 申报价格
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("申报价格")]
        public decimal NumDeclare { get; set; }

        /// <summary>
        /// 申报单位
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("申报单位")]
        public string Currency { get; set; } = string.Empty;

        /// <summary>
        /// 收件人ID
        /// </summary>
        [DisplayName("收件人ID")]
        public string CertificateCode { get; set; } = string.Empty;
    }
}
