﻿using System.ComponentModel.DataAnnotations;

namespace ERP.Models.Api.Logistics.YunTu
{
    public class InterceptRequest: CommonParameter
    {
        public InterceptRequest(Parameter p, string logisticNumber) : base(p)
        {
            OrderNumber = logisticNumber;
            OrderType = logisticNumber.StartsWith("YT") ? 1 : 2;
        }

        /// <summary>
        /// 1-云途单号,2-客户订单号,3-跟踪号
        /// </summary>
        [Required]
        public int OrderType { get; set; }

        /// <summary>
        /// 单号
        /// </summary>
        [Required]
        public string OrderNumber  { get; set; }

        /// <summary>
        /// 拦截原因
        /// </summary>
        [Required]
        public string Remark { get; set; } = "取消运单";

        public override HttpMethod Method => HttpMethod.Post;

        public override string Url => $"{Domain}/api/WayBill/Intercept";
        
    }
}
