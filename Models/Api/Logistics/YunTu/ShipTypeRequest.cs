﻿namespace ERP.Models.Api.Logistics.YunTu
{
    public class ShipTypeRequest : CommonParameter
    {
        public ShipTypeRequest(Parameter p,string countryCode) : base(p)
        {
            CountryCode = countryCode;
        }

        public string CountryCode { get; set; }

        public override string Url => $"{Domain}/api/Common/GetShippingMethods?CountryCode={CountryCode}";
       
        public override HttpMethod Method => HttpMethod.Get;       
    }

}
