﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ERP.Models.Api.Logistics.YunTu
{
    public class GetFeeResponse
    {
        //{"Item":{"ShippingMethodName":"省内EUB","CountryCode":"ES","ChineseName":"西班牙","CustomerOrderNumber":"406-7915470-5816342-5","WayBillNumber":"YT2128812102001563","TrackingNumber":"LV308392221CN","GrossWeight":"0.671","ChargeWeight":"0.671","Freight":40.26,"FuelSurcharge":0.0,"RegistrationFee":14.0,"ProcessingFee":0.0,"OtherFee":0.0,"TotalFee":54.26,"OccurrenceTime":"2021/10/20 23:00:00","StandardMoney":54.26},"Code":"0000","Message":"提交成功!","RequestId":"0HMCKDC989JJE:00000002","TimeStamp":"2021-10-22T16:11:49.1749983+00:00"}
        //{"Item":null,"Code":"25","Message":"订单不存在,402-3489751-7464303","RequestId":"0HMCKDC98ATH1:00000002","TimeStamp":"2021-10-22T16:24:32.5432631+00:00"}
        public string RequestId { get; set; } = string.Empty;
        public string Timestamp { get; set; } = string.Empty;
        public string Code { get; set; } = string.Empty;
        /// <summary>
        /// 提交失败/提交成功
        /// </summary>
        public string Message { get; set; } = string.Empty;
        public Cls_Item Item { get; set; } = null!;
        public class Cls_Item
        {
            public object WayBillNumber { get; set; } = new();
            public string CustomerOrderNumber { get; set; } = string.Empty;
            public string TrackingNumber { get; set; } = string.Empty;
            public string CountryCode { get; set; } = string.Empty;
            public string ChineseName { get; set; } = string.Empty;
            public string ShippingMethodName { get; set; } = string.Empty;
            public string GrossWeight { get; set; } = string.Empty;
            public string ChargeWeight { get; set; } = string.Empty;
            public double Freight { get; set; }
            public double FuelSurcharge { get; set; }
            public double RegistrationFee { get; set; }
            public double ProcessingFee { get; set; }
            public double OtherFee { get; set; }
            public double TotalFee { get; set; }
            public string OccurrenceTime { get; set; } = string.Empty;
            public double StandardMoney { get; set; }         
        }
    }

}
