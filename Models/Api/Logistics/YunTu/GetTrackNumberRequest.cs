﻿namespace ERP.Models.Api.Logistics.YunTu
{
    public class GetTrackNumberRequest : CommonParameter
    {
        public GetTrackNumberRequest(Parameter p, List<string> CustomNumbers) : base(p)
        {
            CustomOrderID = string.Join(',',CustomNumbers);
        }
        public GetTrackNumberRequest(Parameter p, string CustomNumber) : base(p)
        {
            CustomOrderID = CustomNumber;
        }
        public string CustomOrderID { get; set; } = string.Empty;
        public override string Url => $"{Domain}/api/Waybill/GetTrackingNumber?CustomerOrderNumber={CustomOrderID}";
    }
}
