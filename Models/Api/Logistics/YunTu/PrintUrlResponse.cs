﻿
using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.YunTu
{
    public class PrintUrlResponse
    {
        /// <summary>
        /// 订单标签打印信息
        /// </summary>
        public List<OrderLabelPrint>? Item { get; set; }

        /// <summary>
        /// 结果代码
        /// </summary>
        public string? Code { get; set; }

        /// <summary>
        /// 结果描述
        /// </summary>
        public string? Message { get; set; }

        /// <summary>
        /// 请求唯一标识
        /// </summary>
        public string? RequestId { get; set; }

        /// <summary>
        /// 时间戳
        /// </summary>
        public string? TimeStamp { get; set; }

        public class OrderLabelPrint
        {
            /// <summary>
            /// 打印标签地址
            /// </summary>
            public string? Url { get; set; }
            /// <summary>
            /// 订单详细信息
            /// </summary>
            public List<OrderInfofo> OrderInfofos { get; set; } = new();
            public class OrderInfofo
            {               
                /// <summary>
                /// 客户订单号
                /// </summary>
                public string? CustomerOrderNrNumber { get; set; }

                /// <summary>
                /// 错误信息
                /// </summary>
                public string Errrror { get; set; }

                /// <summary>
                /// 错误代码 100-正确，200-不存在打印模板
                /// </summary>
                public int Code { get; set; } 
            }
        }
    }
}
