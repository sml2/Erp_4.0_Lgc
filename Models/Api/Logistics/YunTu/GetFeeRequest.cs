﻿namespace ERP.Models.Api.Logistics.YunTu
{
    public class GetFeeRequest : CommonParameter
    {
        public GetFeeRequest(Parameter p,string trackNumber) : base(p)
        {
            TrackNumber = trackNumber;
        }
        public string TrackNumber { get; set; }

        public override HttpMethod Method =>HttpMethod.Get;
        public override string Url => $"{Domain}/api/Freight/GetShippingFeeDetail?wayBillNumber={TrackNumber}";
    }
}
