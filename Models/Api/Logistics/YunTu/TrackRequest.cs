﻿namespace ERP.Models.Api.Logistics.YunTu
{
    public class TrackRequest : CommonParameter
    {
        public TrackRequest(Parameter p, string logNumber) : base(p)
        {
            LogNumber = logNumber;
        }
        public string LogNumber { get; set; }

        public override string Url => $"{Domain}/api/Tracking/GetTrackInfo?OrderNumber={LogNumber}";

        public override HttpMethod Method => HttpMethod.Get;
    }
}
