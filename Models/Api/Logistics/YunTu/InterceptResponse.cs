﻿namespace ERP.Models.Api.Logistics.YunTu
{
    public class InterceptResponse
    {
        public string RequestId { get; set; } = string.Empty;
        public string Timestamp { get; set; } = string.Empty;

        /// <summary>
        /// 提交成功，0000
        /// </summary>
        public string Code { get; set; } = string.Empty;
        /// <summary>
        /// 提交失败/提交成功
        /// </summary>
        public string Message { get; set; } = string.Empty;
        public Cls_OrderIntercept OrderIntercept { get; set; } = null!;
        public class Cls_OrderIntercept
        {
            public string Rueslt { get; set; }

            public int Type { get; set; }

            public string OrderNumber { get; set; }

            public string Remark { get; set; }
        }
    }
}
