﻿
using ERP.Attributes.Logistics;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace ERP.Models.Api.Logistics.YunTu
{
    public class Parameter: BaseParam
    {
        private string u;

        private string key;
        [ViewConfigParameter(nameof(user), $"用户{nameof(user)}", Sort = 0)]
        public string user { get { return u; } set { u = value.Trim(); } } 
        [ViewConfigParameter(nameof(secret_key),$"用户{nameof(secret_key)}", Sort = 1)]
        public string secret_key { get { return key; } set { key = value.Trim(); } } 

        public string Domain
        {
            get
            {
                return "http://oms.api.yunexpress.com"; 
            }
        }

       public  string CountryCode { get; set; } = string.Empty;
    }
}
