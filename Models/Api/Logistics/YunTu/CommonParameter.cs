﻿using ERP.Models.Api.Logistics.Extension;
using Newtonsoft.Json;
using Sy.Text;

namespace ERP.Models.Api.Logistics.YunTu
{
    public class CommonParameter: RequestLogistic
    {
        public CommonParameter(Parameter p)
        {
            this.user = p.user;
            this.secret_key = p.secret_key;          
        }
        
        public override SerializableDictionary<string, string> headers
        {
            get
            {
                SerializableDictionary<string, string> header = new SerializableDictionary<string, string>();
                header.Add("Authorization", "Basic " + Encode.Base64_Encode($"{user}&{ secret_key}"));
                return header;
            }
        }

        [JsonIgnore]
        public string user { get; set; }
        [JsonIgnore]
        public string secret_key { get; set; }
        [JsonIgnore]
        public string Domain { get => "http://oms.api.yunexpress.com"; }
             
    }
}
