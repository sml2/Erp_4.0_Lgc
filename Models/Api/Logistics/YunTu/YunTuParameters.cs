﻿namespace ERP.Models.Api.Logistics.YunTu
{
    public class YunTuParameters
    {
        /// <summary>
        /// 货运方式
        /// </summary>
        public string ShipCode { get; set; } = string.Empty;

        /// <summary>
        /// 是否明文传输
        /// </summary>
        public bool IossTag { get; set; }

        /// <summary>
        /// 税号
        /// </summary>
        public string TaxNumber { get; set; } = string.Empty;
    }
}
