﻿namespace ERP.Models.Api.Logistics.WANB
{
    public class ResponseTrace
    {
        public string Match { get; set; } = string.Empty;

        public string TrackingNumber { get; set; } = string.Empty;

        public string Status { get; set; } = string.Empty;

        public Metadata Metadata { get; set; } = null!;

        public TrackingPoint[] TrackPoints { get; set; }=Array.Empty<TrackingPoint>();

        public class TrackingPoint
        {
            /// <summary>
            /// ISO 8601 标准格式化时间
            /// </summary>
            public string Time { get; set; } = string.Empty;

            /// <summary>
            /// 关键节点状态
            /// </summary>
            public string Status { get; set; } = string.Empty;

            /// <summary>
            /// 地点
            /// </summary>
            public string Location { get; set; } = string.Empty;

            /// <summary>
            /// 轨迹内容
            /// </summary>
            public string Content { get; set; } = string.Empty;
        }


        public enum EnumStatus
        {
            [EnumExtension.Description("收到数据")]
            DataReceived,
            [EnumExtension.Description("运输途中")]
            InTransit,
            [EnumExtension.Description("到达待取")]
            DeliveryReady,
            [EnumExtension.Description("尝试投递失败。部分渠道会尝试投递数次")]
            DeliveryTried,
            [EnumExtension.Description("已妥投")]
            Delivered,
            [EnumExtension.Description("投递失败。 地址问题、尝试投递数次均失败、收件人搬家或拒收等")]
            DeliveryFailed,
            [EnumExtension.Description("已退回")]
            Returned,
            [EnumExtension.Description("包裹遗失")]
            Lost,
        }       
    }
}
