﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using static ERP.Models.Api.Logistics.WANB.ExtParameter;

namespace ERP.Models.Api.Logistics.WANB
{   
    public class AddOrderRequest:CommonParameter
    {
        public AddOrderRequest(Parameter p, SenderInfo senderInfo, OrderInfo orderInfo, ExtParameter ext) : base(p)
        {
            Id = orderInfo.id;
            string orderNo= Helpers.GetOrderNo(orderInfo.OrderNo, orderInfo.SendNum, orderInfo.FailNum);
            ReferenceId = "waimaomvp" + orderNo;
            this.orderNo = orderNo;
            var sellingPlatformOrder = new SellingPlatformOrder();
            sellingPlatformOrder.OrderId = orderNo;
            this.SellingPlatformOrder = sellingPlatformOrder;
            //收件人地址信息
            var CreateResult = Address.Create(orderInfo.ReceiverAddress.Name,
                                    orderInfo.ReceiverAddress.Address,
                                    orderInfo.ReceiverAddress.Address2 ?? string.Empty,
                                    orderInfo.ReceiverAddress.Address3 ?? string.Empty,
                                    orderInfo.ReceiverAddress.City,
                                    orderInfo.ReceiverAddress.Province,
                                    orderInfo.ReceiverAddress.NationShort,
                                    orderInfo.ReceiverAddress.Nation,
                                    orderInfo.ReceiverAddress.Zip ?? string.Empty,
                                    orderInfo.ReceiverAddress.Name,
                                    orderInfo.ReceiverAddress.Phone,
                                    orderInfo.ReceiverAddress.Email ?? string.Empty,
                                    ext.RecipientTaxNumber);           

            this.ShippingAddress = CreateResult.Address;
            //包裹内件明细
            int allNum = 0;
            List<ParcelItemDetail> parcelItemDetailList = new List<ParcelItemDetail>();

            if (orderInfo.GoodInfos is not null && orderInfo.GoodInfos.Count() > 0)
            {
                allNum = orderInfo.GoodInfos.Sum(s => s.Count);

                foreach (var good in orderInfo.GoodInfos)
                {
                    var money = new Money();
                    money.Code = ext.GoodsValueCurrency;//必填
                    if (decimal.TryParse(good.Price.ToString(), out decimal value))
                    {
                        money.Value =Helpers.RoundData(ext.GoodsValue / allNum);
                    }
                    var parcelItemDetail = new ParcelItemDetail();
                    parcelItemDetail.GoodsId = good.goodId;
                    parcelItemDetail.GoodsTitle = good.Name;
                    parcelItemDetail.DeclaredNameEn = ext.EName;//
                    parcelItemDetail.DeclaredNameCn = ext.CName;//
                    parcelItemDetail.DeclaredValue = money;
                    parcelItemDetail.WeightInKg =Helpers.RoundData(ext.Weight/1000,2);
                    parcelItemDetail.Quantity = good.Count;
                    parcelItemDetail.HSCode = ext.HsCode;
                    parcelItemDetail.CaseCode = "";   //箱号(一票多件)
                    parcelItemDetail.SalesUrl = good.Url ?? "";
                    parcelItemDetail.IsSensitive = ext.IsSensitive;//是否为敏感货物/带电/带磁等
                    parcelItemDetail.Brand = "";//
                    parcelItemDetail.Model = "";//
                    parcelItemDetail.MaterialCn = "";
                    parcelItemDetail.MaterialEn = "";
                    parcelItemDetail.UsageCn = "";
                    parcelItemDetail.UsageEn = "";
                    parcelItemDetailList.Add(parcelItemDetail);
                }
                
            }            
            this.ItemDetails = parcelItemDetailList.ToArray();
            //包裹重量(单位:KG)
            WeightInKg = ext.Weight/1000;
            //包裹总金额
            var totalMoney = new Money();
            totalMoney.Code = ext.GoodsValueCurrency;
            totalMoney.Value = ext.GoodsValue;
            TotalValue = totalMoney;
            //包裹尺寸
            var cubeSize = new CubeSize();
            cubeSize.Height = ext.Height;
            cubeSize.Length = ext.Length;
            cubeSize.Width = ext.Width;          
            TotalVolume = cubeSize;
            //包裹是否含有带电产品
            WithBatteryType = ((enumTax)Convert.ToInt32(ext.WithBatteryType)).ToString();//包裹是否含有带电产品;
            Notes = "";
            BatchNo = "";
            WarehouseCode = ext.WarehouseCode;
            //发货产品服务代码  必填
            ShippingMethod = ext.ShipMethod;
            //包裹类型  必填
            ItemType = ((enumTax)Convert.ToInt32(ext.ItemType)).ToString();
            TrackingNumber = "";
            IsMPS = false;
            //request.MPSType:包裹一票多件类型
            MPSType = ParcelMPSType.Normal;//
            List<ParcelCaseInfo> parcelCaseInfos = new List<ParcelCaseInfo>();
            var ParcelCaseInfo = new ParcelCaseInfo()
            {
                Code = "1",
                WeightInKg = ext.Weight/1000,
                Volume = cubeSize
            };
            parcelCaseInfos.Add(ParcelCaseInfo);
           Cases = parcelCaseInfos.ToArray(); //快递一票多件,箱子列表
           AllowRemoteArea = true;
            //自动确认交运包裹
            AutoConfirm = true;

            //发件人信息
            var parcelShipperInfo = new ParcelShipperInfo();
            parcelShipperInfo.CountryCode = "";
            parcelShipperInfo.Province = senderInfo.Province;
            parcelShipperInfo.City = senderInfo.City;
            parcelShipperInfo.Postcode = senderInfo.PostalCode;
            parcelShipperInfo.Name = senderInfo.Name;
            parcelShipperInfo.Address = senderInfo.Address;
            parcelShipperInfo.ContactInfo = senderInfo.PhoneNumber;
            List<ParcelShipperTaxation> TaxationsList = new List<ParcelShipperTaxation>();
            var parcelShipperTaxation = new ParcelShipperTaxation();
            //税号类型
            parcelShipperTaxation.Number = ext.TaxNumber;           
            parcelShipperTaxation.TaxType = ((enumTax)Convert.ToInt32(ext.TaxType)).ToString();                       
            TaxationsList.Add(parcelShipperTaxation);
            parcelShipperInfo.Taxations = TaxationsList.ToArray();
           this.ShipperInfo = parcelShipperInfo;        
        }

        [JsonIgnore]
        public int Id { get; set; }

        [JsonIgnore]
        public string orderNo { get; set; }
        public override string Url { get => $"{Domain}/api/parcels"; }
       
        public override HttpMethod Method => HttpMethod.Post;

        /// <summary>
        /// 客户订单号，在整个系统内不得重复。为了尽量避免冲突，推荐您加贵司名称英文（或拼音）简写的前缀
        /// 必须
        /// </summary>      
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("客户订单号")]
        public string ReferenceId { get; set; }

        /// <summary>
        /// 销售平台订单信息
        /// </summary>      
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("销售平台订单信息")]
        public SellingPlatformOrder SellingPlatformOrder { get; set; }

        /// <summary>
        /// 收件人地址信息 必须
        /// </summary>      
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("收件人地址信息")]
        public Address ShippingAddress { get; set; }

        /// <summary>
        /// 包裹重量(单位:KG)  必须 
        /// </summary>       
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹重量(单位:KG)")]     
        public decimal WeightInKg { get; set; }

        /// <summary>
        /// 包裹件内明细  必须 
        /// </summary>      
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹件内明细")]
        public ParcelItemDetail[] ItemDetails { get; set; }

        /// <summary>
        /// 包裹总金额  必须 
        /// </summary>      
        [Required(ErrorMessage = "{0} 不能为空")]      
        [DisplayName("包裹总金额")]
        public Money TotalValue { get; set; }


        /// <summary>
        /// 包裹尺寸  必须 
        /// </summary>      
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹尺寸")]
        public CubeSize TotalVolume { get; set; }

        /// <summary>
        /// 包裹是否含有带电产品  必须 
        /// </summary>       
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹是否含有带电产品")]
        public string WithBatteryType { get; set; }

        /// <summary>
        /// 包裹备注
        /// </summary>
        public string Notes { get; set; }

        /// <summary>
        /// 批次号或邮袋号
        /// </summary>
        public string BatchNo { get; set; }

        /// <summary>
        /// 交货仓库代码，请参考查询仓库接口   必须 
        /// </summary>       
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("交货仓库代码")]
        public string WarehouseCode { get; set; }

        /// <summary>
        /// 发货产品服务代码 必须 
        /// </summary>    
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("发货产品服务代码")]
        public string ShippingMethod { get; set; }

        /// <summary>
        /// 包裹类型 必须 
        /// </summary>      
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹类型")]
        public string ItemType { get; set; }

        /// <summary>
        ///订单交易类型(B2B, B2C)，默认为 
        /// </summary>
        public string TradeType { get; set; } = "B2C";

        /// <summary>
        ///预分配挂号
        /// </summary>
        public string TrackingNumber { get; set; }

        /// <summary>
        ///快递一票多件(multiple package shipment)
        /// </summary>
        public bool IsMPS { get; set; }


        /// <summary>
        ///快递一票多件类型
        /// </summary>
        public ParcelMPSType MPSType { get; set; }


        /// <summary>
        ///快递一票多件,箱子列表
        /// </summary>
        public ParcelCaseInfo[] Cases { get; set; }

        /// <summary>
        ///是否允许偏远区域下单，默认值为 true
        ///如果设置值为false，并且地址属于偏远区域，此接口则会返回代码为 0x10008E 的错误
        /// </summary>
        public bool AllowRemoteArea { get; set; } = true;

        /// <summary>
        ///自动确认交运包裹，或此值为true，则无须再调用确认交运包裹接口
        /// </summary>
        public bool AutoConfirm { get; set; }

        /// <summary>
        ///发件人地址与税号信息
        ///代理客户必须填写真实的发件人地址与联系方式，电商卖家客户可以不填。
        ///发件人信息必须提供 英文姓名、英文地址以及联系方式
        ///英国脱欧后，进入英国货物需要提供 VatNo
        ///欧盟2021税改后，进入欧盟货物需要提供 IOSS
        ///B2B类型或者B2C的高价值包裹必须填写 EORI
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("发件人地址与税号信息")]
        public ParcelShipperInfo ShipperInfo { get; set; }

    }

    public class SellingPlatformOrder
    {
        /// <summary>
        /// 销售平台官网首页链接 必填
        /// </summary>             
        public string SellingPlatform { get; set; } = "http://erp3.waimaomvp.com/";

        /// <summary>
        /// 销售平台订单号	必填
        /// </summary>      
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("销售平台订单号")]
        public string OrderId { get; set; } = string.Empty;
    }

    public class Address
    {
        static internal CreateResult Create(string Company, string address1, string address2, string address3,
                                            string City, string Province, string CountryCode, string Country, string Postcode, string Contacter,
                                            string Tel, string Email, string TaxId)
        {
            var shippingAddress = new Address();
            shippingAddress.Company = Company;
            //地址
            var lst = new List<string>() { address1, address2, address3 }.Where(s => s.IsNotWhiteSpace()).ToArray();
            if (lst.Length == 0)
            {
                return new CreateResult("三个详细地址都为空，请检查数据", new());
            }
            else
            {
                Stack<Action<string>> StcSeter = new Stack<Action<string>>();
                StcSeter.Push((s) => shippingAddress.Street3 = s);
                StcSeter.Push((s) => shippingAddress.Street2 = s);
                StcSeter.Push((s) => shippingAddress.Street1 = s);
                foreach (var item in lst)
                {
                    StcSeter.Pop()(item);
                }
            }
            shippingAddress.City = City;
            shippingAddress.Province = Province;
            shippingAddress.CountryCode = CountryCode;
            shippingAddress.Country = Country;
            shippingAddress.Postcode = Postcode;
            shippingAddress.Contacter = Contacter;
            shippingAddress.Tel = Tel;
            shippingAddress.Email = Email;
            shippingAddress.TaxId = TaxId;//
            return new CreateResult("", shippingAddress);
        }
        /// <summary>
        /// 联系人公司	
        /// </summary>             
        public string Company { get; set; } = string.Empty;

        /// <summary>
        /// 街道1  必须
        /// </summary>      
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName(" 街道1")]
        public string Street1 { get; set; } = string.Empty;

        /// <summary>
        /// 街道2
        /// </summary>
        public string Street2 { get; set; } = string.Empty;

        /// <summary>
        /// 街道3
        /// </summary>
        public string Street3 { get; set; } = string.Empty;

        /// <summary>
        /// 城市  必须
        /// </summary>      
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("城市")] 
        public string City { get; set; } = string.Empty;

        /// <summary>
        ///  州/省,视所属国家有无州省而定
        /// </summary>
        public string Province { get; set; } = string.Empty;

        /// <summary>
        ///国家代码（ISO 3166-1 alpha-2标准）,创建包裹时国家代码与国家英文名称二者至少得有一个
        /// </summary>
        public string CountryCode { get; set; } = string.Empty;

        /// <summary>
        /// 国家英文名称
        /// </summary>
        public string Country { get; set; } = string.Empty;

        /// <summary>
        /// 邮编,视所属国家有无邮编而定
        /// </summary>
        public string Postcode { get; set; } = string.Empty;

        /// <summary>
        /// 收件人   必须
        /// </summary>     
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("收件人")]
        public string Contacter { get; set; } = string.Empty;

        /// <summary>
        /// 收件人联系电话
        /// </summary>
        public string Tel { get; set; } = string.Empty;

        /// <summary>
        /// 收件人邮箱
        /// </summary>
        public string Email { get; set; } = string.Empty;

        /// <summary>
        /// 收件人/收件公司税号。不同国家叫法不完全一样，如美国为 Tax Identification Number，巴西为 CPF/CNPJ。
        /// </summary>
        public string TaxId { get; set; } = string.Empty;
    }

    public class ParcelItemDetail
    {
        /// <summary>
        /// 货物编号
        /// </summary>
        public string GoodsId { get; set; } = string.Empty;

        /// <summary>
        /// 货物描述 必须 
        /// </summary>      
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货物描述")]
        public string GoodsTitle { get; set; } = string.Empty;

        /// <summary>
        /// 英文申报名称 必须 
        /// </summary>   
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("英文申报名称")]
        public string DeclaredNameEn { get; set; } = string.Empty;

        /// <summary>
        /// 中文申报名称 必须 
        /// </summary>      
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("中文申报名称")]
        public string DeclaredNameCn { get; set; } = string.Empty;

        /// <summary>
        /// 单件申报价值 必须 
        /// </summary>      
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("单件申报价值")]
        public Money DeclaredValue { get; set; } = new();

        /// <summary>
        /// 单件重量(KG) 必须 
        /// </summary>      
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("单件重量(KG)")]     
        public decimal WeightInKg { get; set; }

        /// <summary>
        /// 件数 必须 
        /// </summary>      
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("件数")]     
        public int Quantity { get; set; }

        /// <summary>
        /// 海关编码 
        /// </summary>
        public string HSCode { get; set; } = string.Empty;

        /// <summary>
        /// 箱号(一票多件)
        /// </summary>
        public string CaseCode { get; set; } = string.Empty;

        /// <summary>
        /// 销售平台链接
        /// </summary>
        public string SalesUrl { get; set; } = string.Empty;

        /// <summary>
        /// 是否为敏感货物/带电/带磁等
        /// </summary>
        public bool IsSensitive { get; set; }

        /// <summary>
        /// 品牌
        /// </summary>
        public string Brand { get; set; } = string.Empty;

        /// <summary>
        /// 型号
        /// </summary>
        public string Model { get; set; } = string.Empty;

        /// <summary>
        /// 材质（中文）
        /// </summary>
        public string MaterialCn { get; set; } = string.Empty;

        /// <summary>
        /// 材质（英文）
        /// </summary>
        public string MaterialEn { get; set; } = string.Empty;

        /// <summary>
        /// 用途（中文）
        /// </summary>
        public string UsageCn { get; set; } = string.Empty;

        /// <summary>
        /// 用途（英文）
        /// </summary>
        public string UsageEn { get; set; } = string.Empty;

    }

    public class Money
    {
        /// <summary>
        ///货币类型:USD, GBP, CNY, 必须
        /// </summary>      
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货币类型")]
        public string Code { get; set; } = string.Empty;

        /// <summary>
        ///金额, 必须
        /// </summary>       
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("金额")]     
        public decimal Value { get; set; }
    }

    public class ParcelCaseInfo
    {
        /// <summary>
        /// 箱号  必须
        /// </summary>    
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("箱号")]
        public string Code { get; set; } = string.Empty;

        /// <summary>
        /// 箱子重量  必须
        /// </summary>     
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("箱子重量")]      
        public decimal WeightInKg { get; set; }

        /// <summary>
        /// 体积   必须
        /// </summary>       
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("体积")]
        public CubeSize Volume { get; set; } = new();
    }

    public class CubeSize
    {
        /// <summary>
        /// 高 必须  
        /// </summary>       
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("高")]      
        public decimal Height { get; set; }

        /// <summary>
        /// 长 必须  
        /// </summary>       
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("长")]      
        public decimal Length { get; set; }

        /// <summary>
        /// 宽 必须  
        /// </summary>       
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("宽")]      
        public decimal Width { get; set; }

        /// <summary>
        /// 计量单位 必须  
        /// </summary>      
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("计量单位")]
        public string Unit { get; set; } = "CM";
    }

    public class ParcelShipperInfo
    {
        /// <summary>
        /// 发货人国家代码
        /// </summary>
        public string CountryCode { get; set; }=string.Empty;

        /// <summary>
        /// 发货人省份
        /// </summary>
        public string Province { get; set; } = string.Empty;

        /// <summary>
        /// 发货人城市
        /// </summary>
        public string City { get; set; } = string.Empty;

        /// <summary>
        /// 发货人邮编
        /// </summary>
        public string Postcode { get; set; } = string.Empty;

        /// <summary>
        /// 发货人姓名
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// 发货人地址
        /// </summary>
        public string Address { get; set; } = string.Empty;

        /// <summary>
        /// 联系信息(邮箱 或者 电话)
        /// </summary>
        public string ContactInfo { get; set; } = string.Empty;

        /// <summary>
        /// 发货人税号信息，可传多个
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("发货人税号信息")]
        public ParcelShipperTaxation[] Taxations { get; set; } = Array.Empty<ParcelShipperTaxation>();
    }

    public class ParcelShipperTaxation
    {
        /// <summary>
        /// 税号类型  必须
        /// </summary>      
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("税号类型")]
        public string TaxType { get; set; } = string.Empty;

        /// <summary>
        /// 税号  必须
        /// </summary>   
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("税号")]
        public string Number { get; set; } = string.Empty;
    }
}
