﻿namespace ERP.Models.Api.Logistics.WANB
{
    public class Cls_InnerException 
    {      
        public string Message { get; set; } = string.Empty;

        public string ExceptionMessage { get; set; } = string.Empty;

        public string ExceptionType { get; set; } = string.Empty;

        public string StackTrace { get; set; } = string.Empty;

        public Cls_InnerException? InnerException { get; set; } 
    }
}
