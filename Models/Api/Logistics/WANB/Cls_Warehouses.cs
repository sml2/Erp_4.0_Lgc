﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ERP.Models.Api.Logistics.WANB
{

    public class WarehouseRequest : CommonParameter
    {
        public WarehouseRequest(Parameter p) : base(p)
        {
        }
        public override string Url => $"{Domain}/api/warehouses";

        public override HttpMethod Method => HttpMethod.Get;

    }

    public class Cls_Warehouses
    {
        public Cls_Warehouses()
        {

        }
        [JsonProperty("Warehouses")]
        public List<Warehouse> Warehouses { get; set; } = new();       
    }

    public class Warehouse 
    {
        [JsonProperty("Code")]
        public string Code { get; set; } 

        [JsonProperty("Name")]
        public string Name { get; set; }

        public override string ToString()
        {
            return $"{Name}";
        }
    }
}
