﻿namespace ERP.Models.Api.Logistics.WANB
{
    public class RequestTrace : CommonParameter
    {
        public RequestTrace(Parameter p,string NumberID) : base(p)
        {
            TrackingNumber = NumberID;
        }
        public string TrackingNumber { get; set; }

        //trackingNumber的值可为处理号、跟踪号或者客单号，客户订单号："waimaomvp" + orderInfo.OrderNo;
        public override string Url { get => $"{Domain}/api/trackPoints?trackingNumber={TrackingNumber}"; }

        public override HttpMethod Method => HttpMethod.Get;
    }
}
