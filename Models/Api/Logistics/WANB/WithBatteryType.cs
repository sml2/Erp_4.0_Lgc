﻿using static EnumExtension;

namespace ERP.Models.Api.Logistics.WANB
{
    public enum WithBatteryType
    {        
        [Description("不带电")]
        NOBattery,  //不带电
        [Description("带电")]
        WithBattery, //带电
        [Description("纯电")]
        Battery   //纯电池
    }
}
