﻿namespace ERP.Models.Api.Logistics.WANB
{
    public class RequestPackageInfo : CommonParameter
    {
        public RequestPackageInfo(Parameter p, string logictisNumber) : base(p)
        {
            ProcessCode = logictisNumber;
        }

        public string ProcessCode  { get;set;}

        public override string Url { get => $"{Domain}api/parcels/{ProcessCode}";}

        public override HttpMethod Method => HttpMethod.Get;

    }
}
