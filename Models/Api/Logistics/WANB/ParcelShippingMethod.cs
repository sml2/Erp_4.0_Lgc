﻿
using System.Collections.Generic;

namespace ERP.Models.Api.Logistics.WANB
{


    public class Cls_ParcelShippingMethod
    {
        public ParcelShippingMethod[] ShippingMethods { get; set; }=Array.Empty<ParcelShippingMethod>();
      
    }

    public class ParcelShippingMethod
    {
        /// <summary>
        /// 服务代码  必须  
        /// </summary>
        public string Code { get; set; } = string.Empty;

        /// <summary>
        /// 服务名称	  必须  
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// 是否挂号  必须  
        /// </summary>
        public bool IsTracking { get; set; }

        /// <summary>
        /// 是否计泡  必须  
        /// </summary>
        public bool IsVolumeWeight { get; set; }

        /// <summary>
        /// 最大体积重量限制
        /// </summary>
        public decimal MaxVolumeWeightInCm { get; set; }

        /// <summary>
        /// 最大重量限制(KG)
        /// </summary>
        public decimal? MaxWeightInKg { get; set; }

        /// <summary>
        /// 可到达区域
        /// </summary>
        public string Region { get; set; } = string.Empty;

        /// <summary>
        /// 最大重量限制(KG)
        /// </summary>
        public string Description { get; set; } = string.Empty;

        public override string ToString()
        {
            return $"{Name}({Code})";
        }
    }
}
