﻿namespace ERP.Models.Api.Logistics.WANB
{
    public class Metadata
    {
        /// <summary>
        /// 包裹处理号
        /// </summary>
        public string TrackItemId { get; set; } = string.Empty;

        /// <summary>
        /// 客单号
        /// </summary>
        public string ReferenceId { get; set; } = string.Empty;

        /// <summary>
        /// 包裹最终跟踪号
        /// </summary>
        public string TrackingNumber { get; set; } = string.Empty;

        /// <summary>
        ///航班号 
        /// </summary>
        public string FlightNumber { get; set; } = string.Empty;

        /// <summary>
        /// 物流产品代码
        /// </summary>
        public string ShippingProductId { get; set; } = string.Empty;

        /// <summary>
        /// 物流服务代码
        /// </summary>
        public string ShippingServiceId { get; set; } = string.Empty;

        /// <summary>
        /// 包裹发件人所在国家代码
        /// </summary>
        public string OriginCountryCode { get; set; } = string.Empty;

        /// <summary>
        /// 包裹收件人所在国家代码
        /// </summary>
        public string DestinationCountryCode { get; set; } = string.Empty;
    }


}
