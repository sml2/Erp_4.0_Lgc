﻿using ERP.Models.Api.Logistics.Extension;
using Newtonsoft.Json;
using System.Text;

namespace ERP.Models.Api.Logistics.WANB
{
    public class CommonParameter: RequestLogistic
    {
        public CommonParameter(Parameter p)
        {
            this.AccountNo = p.AccountNo;
            this.Token = p.Token;
            Nounce = Helpers.GetSign();                       
        }
       
        public override SerializableDictionary<string, string> headers
        {
            get
            {
                SerializableDictionary<string, string> header = new SerializableDictionary<string, string>();
                header.Add("Authorization", $"Hc-OweDeveloper {this.AccountNo};{this.Token};{Nounce}");
                return header;
            }
        }



        public string AccountNo { get; set; } = string.Empty;

        public string Token { get; set; } = string.Empty;

        /// <summary>
        /// 随机数
        /// 只能为字母、数字、短横线(-)或者下划线(_)等
        /// D8E671B7323A49FF9223AEB457257F05
        /// </summary>
        public string Nounce
        {
            get;
            set;
        }

        [JsonIgnore]
        public string Platform { get; set; } = string.Empty;


        [JsonIgnore]
        public string Domain
        {
            get
            {
                return "http://api.wanbexpress.com"; //正式环境:http://api.wanbexpress.com，
                                                         //测试环境：http://api-sbx.wanbexpress.com
            }
        }      
    }
}
