﻿using System;

namespace ERP.Models.Api.Logistics.WANB
{
    public class ResponsePackageInfo
    {
        /// <summary>
        /// 客户代码
        /// </summary>
        public string AccountNo { get; set; } = string.Empty;

        /// <summary>
        /// 客户订单号
        /// </summary>
        public string ReferenceId { get; set; } = string.Empty;

        /// <summary>
        /// 包裹处理号
        /// </summary>
        public string ProcessCode { get; set; } = string.Empty;

        /// <summary>
        /// 检索号
        /// </summary>
        public string IndexNumber { get; set; } = string.Empty;

        /// <summary>
        /// 销售平台订单信息
        /// </summary>
        public SellingPlatformOrder SellingPlatformOrder { get; set; } = new();

        /// <summary>
        /// 收件人地址信息
        /// </summary>
        public Address ShippingAddress { get; set; }=new();

        /// <summary>
        /// 包裹状态
        /// </summary>
        public string Status { get; set; } = string.Empty;

        /// <summary>
        /// 包裹预报重量(单位:KG)
        /// </summary>
        public decimal WeightInKg { get; set; }

        /// <summary>
        /// 包裹复核重量(单位:KG)
        /// </summary>
        public decimal? CheckWeightInKg { get; set; }

        /// <summary>
        /// 包裹件内明细
        /// </summary>
        public ParcelItemDetail[] ItemDetails { get; set; } = Array.Empty<ParcelItemDetail>();

        /// <summary>
        /// 一票多件分箱信息
        /// </summary>
        public ParcelCase[] Cases { get; set; } = Array.Empty<ParcelCase>();

        /// <summary>
        /// 包裹申报金额
        /// </summary>
        public Money TotalValue { get; set; } = new();

        /// <summary>
        /// 包裹预报尺寸
        /// </summary>
        public CubeSize TotalVolume { get; set; } = new();

        /// <summary>
        /// 包裹复核尺寸
        /// </summary>
        public CubeSize CheckTotalVolume { get; set; } = new();

        /// <summary>
        /// 包裹带电信息
        /// </summary>
        public WithBatteryType WithBatteryType { get; set; }

        /// <summary>
        /// 包裹备注
        /// </summary>
        public string Notes { get; set; } = string.Empty;

        /// <summary>
        /// 批次
        /// </summary>
        public string BatchNo { get; set; } = string.Empty;

        /// <summary>
        /// 送货仓库代码
        /// </summary>
        public string WarehouseCode { get; set; } = string.Empty;

        /// <summary>
        /// 送货仓库名称
        /// </summary>
        public string WarehouseName { get; set; } = string.Empty;

        /// <summary>
        /// 申请的发货方式
        /// </summary>
        public ParcelShippingMethod ShippingMethod { get; set; } = new();

        /// <summary>
        /// 包裹类型
        /// </summary>
        public ParcelItemType ItemType { get; set; }

        /// <summary>
        /// 关税结算方式
        /// </summary>
        public DutyPaymentMethod DutyPaymentMethod { get; set; }

        /// <summary>
        /// 实际的发货方式
        /// </summary>
        public ParcelShippingMethod ParcelShippingMethod { get; set; } = new();

        /// <summary>
        /// 最终跟踪号
        /// TrackingNoProcessResult.Code 为 Success 时，此属性才会有值。请始终先参考 TrackingNoProcessResult 属性查看跟踪号分配信息
        ///当TrackingNoProcessResult.Code为Success，并且TrackingNoProcessResult.IsVirtual为true 时，说明当前单号为虚拟单号。请先打单发货，待我司操作之后才会分配最终的派送单号。 您需要视平台标记发货方式而定，有选择性地调用本接口来查询包裹真实派送单号。当我司操作货物并且成功获取到真实派送单号之后，TrackingNoProcessResult.Code为Success，TrackingNoProcessResult.IsVirtual为false。
        ///一般只有美国USPS渠道才会出现此情况。
        /// </summary>
        public string FinalTrackingNumber { get; set; } = string.Empty;

        /// <summary>
        ///跟踪号分配信息
        ///Code 为 Success 表示跟踪号分配成功，可通过 FinalTrackingNumber 获取跟踪号。
        ///Code 为 Processing 时，你需要隔一阵子再次查询获取包裹接口以获取跟踪号。 通常会在5分钟内完成。
        ///Code 为 Error 时，请参考 Message 查看具体错误信息
        /// </summary>
        public TrackingNoProcessResult TrackingNoProcessResult { get; set; } = new();

        /// <summary>
        /// 包裹分区码
        ///要求客户分区装箱时，可以通过此字段获取包裹所属的分区。
        ///跟踪号分配成功状态下，此分区码方才有效。
        /// </summary>
        public string SortCode { get; set; } = string.Empty;

        /// <summary>
        /// 快递一票多件(multiple package shipment)
        /// </summary>
        public bool IsMPS { get; set; }

        /// <summary>
        /// 快递一票多件类型
        /// </summary>
        public ParcelMPSType MPSType { get; set; }

        /// <summary>
        /// 发件信息
        /// </summary>
        public ParcelShipperInfo ShipperInfo { get; set; } = new();

        /// <summary>
        /// 是否偏远
        /// </summary>
        public bool IsRemoteArea { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateOn { get; set; }

        /// <summary>
        /// 收货时间
        /// </summary>
        public DateTime ReceiveOn { get; set; }

        
        /// <summary>
        /// 出库时间
        /// </summary>
        public DateTime ShipOn { get; set; }
    }
}
