﻿using Newtonsoft.Json;

namespace ERP.Models.Api.Logistics.WANB
{
    public class InterceptRequest: CommonParameter
    {
        public InterceptRequest(Parameter p,string processCode) : base(p)
        {
            ProcessCode = processCode;
        }
        [JsonIgnore]
        public string ProcessCode { get; set; }
        [JsonIgnore]
        public override string Url { get => $"{Domain}/api/parcels/{ProcessCode}/cancellation"; }
        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;
    }


    public class InterceptResponse
    {
        /// <summary>
        /// 截件状态 Requested: 已申请截件, Accepted: 截件成功 Rejected: 截件失败        
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// 截件申请时间, ISO8601 标准格式
        /// </summary>
        public string RequestOn { get; set; }

        /// <summary>
        /// 截件处理时间, ISO8601 标准格式
        /// </summary>
        public string ResponseOn { get; set; }

        /// <summary>
        /// 处理消息
        /// </summary>
        public string Message { get; set; }
    }
}
