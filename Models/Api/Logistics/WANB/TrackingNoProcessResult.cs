﻿using System.ComponentModel.DataAnnotations;

namespace ERP.Models.Api.Logistics.WANB
{
    public class TrackingNoProcessResult
    {
        /// <summary>
        /// 代码  必须
        /// </summary>
        [Required]
        public string Code { get; set; }=string.Empty;

        /// <summary>
        /// 消息文本  必须  
        /// </summary>
        [Required]
        public string Message { get; set; } = string.Empty;

        /// <summary>
        /// 是否为虚拟单号   必须
        /// </summary>
        [Required]
        public bool IsVirtual { get; set; }
    }
}
