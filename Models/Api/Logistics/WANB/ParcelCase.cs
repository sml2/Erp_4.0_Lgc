﻿namespace ERP.Models.Api.Logistics.WANB
{
    public class ParcelCase
    {
        /// <summary>
        /// 客户箱号
        /// </summary>
        public string CustomerCaseNo { get; set; } = string.Empty;

        /// <summary>
        /// 内部箱号
        /// </summary>
        public string CaseCode { get; set; } = string.Empty;

        /// <summary>
        /// 箱跟踪号
        /// </summary>
        public string TrackingNumber { get; set; } = string.Empty;

        /// <summary>
        /// 预报重
        /// </summary>
        public decimal WeightInKg { get; set; }

        /// <summary>
        /// 核重
        /// </summary>
        public decimal CheckWeightInKg { get; set; }

        /// <summary>
        /// 预报尺寸
        /// </summary>
        public CubeSize Size { get; set; } = new();

        /// <summary>
        /// 核验尺寸
        /// </summary>
        public CubeSize CheckSize { get; set; } = new();
    }
}
