﻿using Newtonsoft.Json;

namespace ERP.Models.Api.Logistics.WANB
{
    public class ResponseBaseData<T>       
    {
        public ResponseBaseData()
        {

        }

        [JsonProperty("Succeeded")]
        public bool Succeeded { get; set; }

        [JsonProperty("Error")]
        public Error? Error { get; set; } = new();

        [JsonProperty("Data")]
        public T? Data { get; set; } = default!;

        [JsonProperty("SystemError")]
        public SystemError? SystemError { get; set; } = new();

    }
}
