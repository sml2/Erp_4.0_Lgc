﻿
namespace ERP.Models.Api.Logistics.WANB
{
    public class ResponseAddOrder
    {
        public ResponseAddOrder()
        {

        }
        /// <summary>
        /// 包裹处理号
        /// </summary>
        public string ProcessCode { get; set; }=string.Empty;

        /// <summary>
        /// 检索号
        /// </summary>
        public string IndexNumber { get; set; } = string.Empty;

        /// <summary>
        /// 客户订单号
        /// </summary>
        public string ReferenceId { get; set; } = string.Empty;

        /// <summary>
        /// 跟踪号
        ///注意： 部分渠道在创建包裹时不一定能够立即返回跟踪号，您需要调用获取包裹接口来查询包裹信息并试图获得跟踪号
        /// </summary>
        public string TrackingNumber { get; set; } = string.Empty;

        /// <summary>
        /// 是否为虚拟跟踪号
        ///如果为 true，请先打单发货，待我司操作之后才会分配最终的派送单号。您需要视平台标记发货方式而定，有选择性地调用 获取包裹 接口来查询包裹真实派送单号。一般只有美国USPS渠道才会出现此情况。
        ///如果为false，可忽略
        /// </summary>
        public bool IsVirtualTrackingNumber { get; set; }

        /// <summary>
        /// 包裹分区码
        ///要求客户分区装箱时，可以通过此字段获取包裹所属的分区。
        ///跟踪号分配成功状态下，此分区码方才有效。
        /// </summary>
        public string SortCode { get; set; } = string.Empty;

        /// <summary>
        /// 是否偏远
        /// </summary>
        public bool IsRemoteArea { get; set; }

        /// <summary>
        /// 状态，如果请求中 AutoConfirm 参数为 true，返回的状态为 Confirmed，否则为 Original
        /// </summary>
        public string Status { get; set; } = string.Empty;

    }
}
