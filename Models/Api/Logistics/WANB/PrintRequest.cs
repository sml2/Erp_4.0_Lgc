﻿namespace ERP.Models.Api.Logistics.WANB
{
    public class PrintRequest : CommonParameter
    {
        public PrintRequest(Parameter p,string codes) : base(p)
        {
            Codes = codes;
        }

        public string Codes { get; set; }
        public override string Url { get => $"{ Domain}/api/parcels/labels?processCodes={Codes}";}

        public override HttpMethod Method => HttpMethod.Get;
    }
}
