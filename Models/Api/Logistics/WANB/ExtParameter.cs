﻿using static EnumExtension;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ERP.Models.Api.Logistics.WANB
{
    public class ExtParameter: BaseExtParam
    {
        public ExtParameter(string cName, string eName, string hsCode, decimal weight, decimal length, decimal width, 
            decimal height, bool isSensitive, string withBatteryType, string itemType, decimal goodsValue, 
            string taxType, string taxNumber, string goodsValueCurrency, string shipMethod, string warehouseCode,
            string recipientTaxNumber)
        {
            CName = cName;
            EName = eName;
            HsCode = hsCode;
            Weight = weight;
            Length = length;
            Width = width;
            Height = height;
            IsSensitive = isSensitive;
            GoodsValue = goodsValue;
            if (Enum.IsDefined(typeof(WithBatteryType), withBatteryType) && Enum.TryParse<WithBatteryType>(withBatteryType, out var labelwithBatteryType))
            {
                WithBatteryType = labelwithBatteryType;
            }
            else
            {
                Console.WriteLine("WANB WithBatteryType:"+ WithBatteryType);
            }

            if (Enum.IsDefined(typeof(ParcelItemType), itemType) && Enum.TryParse<ParcelItemType>(itemType, out var labelParcelItemType))
            {
                ItemType = labelParcelItemType;
            }
            else
            {
                Console.WriteLine("WANB ParcelItemType:" + itemType);
            }            

            if (Enum.IsDefined(typeof(enumTax), taxType) && Enum.TryParse<enumTax>(taxType, out var labelenumTax))
            {
                TaxType = labelenumTax;
            }
            else
            {
                Console.WriteLine("WANB enumTax:" + taxType);
            }
            TaxNumber = taxNumber;
            GoodsValueCurrency = goodsValueCurrency;
            ShipMethod = shipMethod;
            WarehouseCode = warehouseCode;
            RecipientTaxNumber = recipientTaxNumber;
        }

        /// <summary>
        /// 货品中文申报名
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货品中文申报名")]
        public string CName { get; set; }

        /// <summary>
        /// 货品英文申报名
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货品英文申报名")]
        public string EName { get; set; }

        public string HsCode { get; set; }
        /// <summary>
        /// 包裹重量g
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹重量")]
        public decimal Weight { get; set; }
        /// <summary>
        /// 包裹长度
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹长度")]
        public decimal Length { get; set; }
        /// <summary>
        /// 包裹宽度
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹宽度")]
        public decimal Width { get; set; }
        /// <summary>
        /// 包裹高度
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹高度")]
        public decimal Height { get; set; }

        /// <summary>
        /// 是否为敏感货物/带电/带磁等 
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("是否为敏感货物/带电/带磁等")]
        public bool IsSensitive { get; set; }

        /// <summary>
        /// 是否含有带电产品
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("是否含有带电产品")]
        public WithBatteryType WithBatteryType { get; set; }

        /// <summary>
        /// 包裹类型
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹类型")]
        public ParcelItemType ItemType { get; set; }

        /// <summary>
        /// 申报总价格
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("申报总价格")]
        public decimal GoodsValue { get; set; }

        /// <summary>
        /// 税号类型
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("税号类型")]
        public enumTax TaxType { get; set; }

        /// <summary>
        /// 税号
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("税号")]
        public string TaxNumber { get; set; }

        /// <summary>
        /// 申报货币类型
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("申报货币类型")]
        public string GoodsValueCurrency { get; set; }

        /// <summary>
        /// 货运方式    
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货运方式")]
        public string ShipMethod { get; set; }

        /// <summary>
        /// 交货仓库代码
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("交货仓库代码")]
        public string WarehouseCode { get; set; }

        /// <summary>
        /// 收件人税号
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("收件人税号")]
        public string RecipientTaxNumber { get; set; }

        public enum enumTax
        {
            [EnumExtension.Description("VatNo")]
            VatNo,
            [EnumExtension.Description("EORI")]
            EORI,
            [EnumExtension.Description("IOSS")]
            IOSS,
        }   
    }

}
