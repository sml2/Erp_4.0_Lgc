﻿namespace ERP.Models.Api.Logistics.WANB
{
    public class ShippingMethodRequest : CommonParameter
    {
        public ShippingMethodRequest(Parameter p) : base(p)
        {

        }
        public override string Url => $"{Domain}/api/services";

        public override HttpMethod Method => HttpMethod.Get;
    }
}
