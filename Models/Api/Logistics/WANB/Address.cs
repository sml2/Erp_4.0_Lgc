﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ERP.Models.Api.Logistics.WANB
{
    public class CreateResult {
       public string Message;
       public Address Address;

        public CreateResult(string message, Address address)
        {
            Message = message;
            Address = address;
        }
    }
 
}
