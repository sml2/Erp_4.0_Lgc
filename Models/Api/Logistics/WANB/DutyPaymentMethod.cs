﻿namespace ERP.Models.Api.Logistics.WANB
{
    public enum DutyPaymentMethod
    {
        DDP,
        DDU
    }
}
