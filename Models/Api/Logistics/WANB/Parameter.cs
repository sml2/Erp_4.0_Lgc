﻿
using ERP.Attributes.Logistics;
using System;
using System.Text;

namespace ERP.Models.Api.Logistics.WANB
{
    public class Parameter: BaseParam
    {
        private string no;
        /// <summary>
        /// 客户代号
        /// </summary>
        [ViewConfigParameter(nameof(AccountNo), Sort = 0)]
        public string AccountNo { get { return no; } set { no = value.Trim(); } }

        private string token;
        /// <summary>
        /// 令牌
        /// </summary>
        [ViewConfigParameter(nameof(Token), Sort = 1)]
        public string Token { get { return token; } set { token = value.Trim(); } } 
    }    
}
