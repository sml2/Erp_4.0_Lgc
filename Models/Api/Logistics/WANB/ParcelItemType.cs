﻿using static EnumExtension;

namespace ERP.Models.Api.Logistics.WANB
{
    public enum ParcelItemType
    {
        [Description("文件")]
        DOC,
        [Description("包裹")]
        SPX
    }
}
