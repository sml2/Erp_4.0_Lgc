﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static EnumExtension;

using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ERP.Models.Api.Logistics.UBI
{
    public class ExtParameter: BaseExtParam
    {
        /// <summary>
        /// 商 品 描 述(英文)
        /// </summary>       
        public string description { get; set; } = string.Empty;

        /// <summary>
        /// 商 品 描 述(中文)
        /// </summary>
        public string nativeDescription { get; set; } = string.Empty;

        /// <summary>
        /// 包裹重量(kg)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹重量(kg)")]
        public double weight { get; set; }

        /// <summary>
        /// 包裹长度(cm)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹长度(cm)")]
        public double length { get; set; }

        /// <summary>
        /// 包裹宽度(cm)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹宽度(cm)")]
        public double width { get; set; }

        /// <summary>
        /// 包裹高度(cm)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹高度(cm)")]
        public double height { get; set; }

        /// <summary>
        /// 海关编码
        /// </summary>
        public string hsCode { get; set; } = string.Empty;

        /// <summary>
        /// 申报总价格(USD)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("申报总价格")]
        public double invoiceValue { get; set; }

        /// <summary>
        /// 货运方式
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货运方式")]
        public string ShipMethod { get; set; } = string.Empty;

        /// <summary>
        ///  服务类型
        /// </summary> 
        public string serviceOption { get; set; } = string.Empty;

        /// <summary>
        /// 服务类型
        /// </summary>  
        public string facility { get; set; } = string.Empty;

        public string dimensionUnit { get; set; } = "cm";
        public string weightUnit { get; set; } = "kg";

        [Required(ErrorMessage = "{0} 不能为空")]
        /// <summary>
        /// 币种
        /// </summary>
        public string Currency { get; set; }
    }
}
