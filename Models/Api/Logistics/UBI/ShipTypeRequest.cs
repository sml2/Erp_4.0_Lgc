﻿namespace ERP.Models.Api.Logistics.UBI
{
    public class ShipTypeRequest : CommonParameter
    {
        public ShipTypeRequest(Parameter p) : base(p)
        {

        }    
        
        public override HttpMethod Method => HttpMethod.Get;

        public override string Url => "https://cn.etowertech.com/services/shipper/service-catalog";
    }
}
