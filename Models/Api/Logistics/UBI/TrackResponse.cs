﻿namespace ERP.Models.Api.Logistics.UBI
{
    public class TrackResponse
    {
        public string status { get; set; } = string.Empty;

        public List<ErrorItem> errors { get; set; } = new();

        public class ErrorItem
        {
            public int code { get; set; }

            public string message { get; set; } = string.Empty;
        }
        public List<DataItem> data { get; set; } = new();
        public class DataItem
        {
            public string orderId { get; set; } = string.Empty;

            public string status { get; set; } = string.Empty;

            public List<ErrorItem> errors { get; set; } = new();

            public List<EventsItem> events { get; set; } = new();
            public class EventsItem
            {
                public string trackingNo { get; set; } = string.Empty;

                public string eventTime { get; set; } = string.Empty;

                public string eventCode { get; set; } = string.Empty;

                public string activity { get; set; } = string.Empty;

                public string location { get; set; } = string.Empty;

                public string referenceTrackingNo { get; set; } = string.Empty;

                public string destCountry { get; set; } = string.Empty;

                public string country { get; set; } = string.Empty;

                public string timeZone { get; set; } = string.Empty;
            }
        }       
    }
}
