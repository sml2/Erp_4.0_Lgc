﻿namespace ERP.Models.Api.Logistics.UBI
{
    public class UBIParameters
    {
        /// <summary>
        /// 货运方式
        /// </summary>
        public string ShipCode { get; set; } = string.Empty;

        /// <summary>
        /// 服务选项
        /// </summary>
        public string ServiceOption { get; set; } = string.Empty;

        /// <summary>
        /// 设施
        /// </summary>
        public string Facility { get; set; } = string.Empty;
    }
}
