﻿using ERP.Models.Api.Logistics.Extension;
using Newtonsoft.Json;

namespace ERP.Models.Api.Logistics.UBI
{
    public class CommonParameter : RequestLogistic
    {
        public CommonParameter(Parameter p)
        {
            Secret = p.Secret;
            Token = p.Token;
        }
        [JsonIgnore]
        public string Secret { get; set; }
        [JsonIgnore]
        public string Token { get; set; }       

        public override SerializableDictionary<string, string> headers
        {
            get
            {
                string method = Method.ToString().ToUpper();
                string dt = string.Format("{0:R}", DateTime.UtcNow);
                string Sign = $"{method}\n{dt}\n{Url}";
                Sign = Anth.HmacSha1Sign(Sign, Secret);
                SerializableDictionary<string, string> header = new SerializableDictionary<string, string>();
                header.Add($"X-WallTech-Date", dt);
                header.Add("UserAgent", "Mozilla 5.0");
                header.Add($"Authorization", $"WallTech {Token}:{Sign}");
                return header;
            }
        }
    }
}
