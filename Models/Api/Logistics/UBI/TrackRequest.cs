﻿namespace ERP.Models.Api.Logistics.UBI
{
    public class TrackRequest : CommonParameter
    {
        public TrackRequest(Parameter p,List<string> trackNums) : base(p)
        {
            TrackNums = trackNums;
        }

        public TrackRequest(Parameter p, string  trackNum) : base(p)
        {
            TrackNums = new List<string>() { trackNum };
        }

        public List<string> TrackNums { get; set; }

        public override string Url => "https://cn.etowertech.com/services/shipper/trackingEvents";
    }
}
