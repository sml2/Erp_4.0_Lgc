﻿using Newtonsoft.Json;

namespace ERP.Models.Api.Logistics.UBI
{
    public class UBIRequest : CommonParameter
    {
        public UBIRequest(Parameter p) : base(p)
        {
        }

        public List<AddOrderRequest> requests { get; set; } = new();
        public override string Url => "https://cn.etowertech.com/services/shipper/orders";

        public override string ToPayload()
        {
            string json = JsonConvert.SerializeObject(this.requests);
            return json;            
        }
    }
}
