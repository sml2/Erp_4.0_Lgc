﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.UBI
{
    public class AddOrderRequest:CommonParameter
    {
        public AddOrderRequest(Parameter p,SenderInfo senderInfo,OrderInfo orderInfo, ExtParameter Ext) : base(p)
        {
            Id = orderInfo.id;
            referenceNo = Helpers.GetOrderNo(orderInfo.OrderNo, orderInfo.SendNum, orderInfo.FailNum);// orderInfo.OrderNo;
            //收件人信息
            recipientName = orderInfo.ReceiverAddress.Name;
            string Address1 = orderInfo.ReceiverAddress.Address;
            string Address2 = orderInfo.ReceiverAddress.Address2??"";
            string Address3 = orderInfo.ReceiverAddress.Address3??"";
            if (Address1.IsNullOrEmpty())
            {
                Address1 = orderInfo.ReceiverAddress.Address2 ?? "";
                Address2 = orderInfo.ReceiverAddress.Address3 ?? "";
                Address3 = "";
            }
            if (Address1.IsNullOrEmpty())
            {
                Address1 = orderInfo.ReceiverAddress.Address3 ?? "";
                Address2 = "";
                Address3 = "";
            }
            addressLine1 = Address1;
            addressLine2 = Address2;
            addressLine3 = Address3;
            city = orderInfo.ReceiverAddress.City;
            country = orderInfo.ReceiverAddress.NationShort;
            email = orderInfo.ReceiverAddress.Email ?? "";
            phone = orderInfo.ReceiverAddress.Phone;
            postcode = orderInfo.ReceiverAddress.Zip ?? "";
            state = orderInfo.ReceiverAddress.Province;
            //页面信息
            description = Ext.description;
            nativeDescription = Ext.nativeDescription;
            facility = Ext.facility;
            serviceCode = Ext.ShipMethod;
            serviceOption = Ext.serviceOption;
            invoiceValue = Ext.invoiceValue;
            invoiceCurrency = Ext.Currency;

            platform = "";
            weightUnit = Ext.weightUnit;
            weight = Helpers.RoundData(Ext.weight/1000,2);
            length = Ext.length;
            width = Ext.width;
            height = Ext.height;
            dimensionUnit = Ext.dimensionUnit;
            volume =Helpers.RoundData((Ext.length / 1000) * (Ext.width / 1000) * (Ext.height));
            //发件人信息
             shipperName = senderInfo.Name;
             shipperAddressLine1 = senderInfo.Address;
             shipperCity = senderInfo.City;
             shipperState = senderInfo.Province;
             shipperPostcode = senderInfo.PostalCode;
             shipperCountry = senderInfo.Region??"";
             shipperPhone = senderInfo.PhoneNumber;
             orderItems = new List<AddOrderRequest.OrderItemsItem>();
            int index = 1;
            int Num = 0;
            orderInfo.GoodInfos.ForEach((r) => Num += r.Num);
            foreach (var r in orderInfo.GoodInfos)
            {
                AddOrderRequest.OrderItemsItem item = new AddOrderRequest.OrderItemsItem();
                item.itemNo = index.ToString("00000");
                item.sku = r.Asin??"";
                if (item.sku.IsNullOrEmpty()) item.sku = "1";
                item.description = Ext.description;
                item.nativeDescription = Ext.nativeDescription;
                item.hsCode = Ext.hsCode;
                item.originCountry = "CN";
                item.unitValue =Helpers.RoundData(Convert.ToDouble(Ext.invoiceValue / Num));
                item.itemCount = r.Count;
                index++;
                orderItems.Add(item);
            }           
        }

        public override string Url => "https://cn.etowertech.com/services/shipper/orders";

        [JsonIgnore]
        public int Id { get; set; }

        public string referenceNo { get; set; }
        #region 收件人
        public string recipientName { get; set; }
        public string addressLine1 { get; set; }

        public string addressLine2 { get; set; }

        public string addressLine3 { get; set; }

        public string city { get; set; }

        public string country { get; set; }

        public string email { get; set; }

        public string phone { get; set; }

        public string postcode { get; set; }

        //public string recipientCompany { get; set; }

        public string state { get; set; }

        #endregion

        public string description { get; set; }
        /// <summary>
        /// 包含中文字符
        /// </summary>
        public string nativeDescription { get; set; }
        #region 货运方式信息
        /// <summary>
        /// 设施
        /// </summary>
        public string facility { get; set; }

        public string serviceCode { get; set; }

        public string serviceOption { get; set; }
        ///// <summary>
        ///// 指令
        ///// </summary>
        //public string instruction { get; set; }
        /// <summary>
        /// 发票货币单位
        /// </summary>
        public string invoiceCurrency { get; set; }
        /// <summary>
        /// 发票值
        /// </summary>
        public double invoiceValue { get; set; }

        public string weightUnit { get; set; }

        /// <summary>
        /// kg
        /// </summary>
        public double weight { get; set; }

        public string dimensionUnit { get; set; }

        public double length { get; set; }

        public double width { get; set; }

        public double height { get; set; }

        public double volume { get; set; }
        public string platform { get; set; }

        #endregion
        //public string sku { get; set; }

        #region 发件人信息
        public string shipperName { get; set; }

        public string shipperAddressLine1 { get; set; }

        //public string shipperAddressLine2 { get; set; }

        //public string shipperAddressLine3 { get; set; }

        public string shipperCity { get; set; }

        public string shipperState { get; set; }

        public string shipperPostcode { get; set; }

        public string shipperCountry { get; set; }

        public string shipperPhone { get; set; }
        #endregion
        
        //public string recipientTaxId { get; set; }
        
        //public string authorityToLeave { get; set; }

        //public string incoterm { get; set; }

        //public string lockerService { get; set; }

        //public ExtendData extendData { get; set; }
        //public class ExtendData
        //{

        //    public string nationalNumber { get; set; }

        //    public string nationalIssueDate { get; set; }

        //    public string cyrillicName { get; set; }

        //    public string imei { get; set; }

        //    public string isImei { get; set; }

        //    public string vendorid { get; set; }

        //    public string gstexemptioncode { get; set; }

        //    public string abnnumber { get; set; }

        //    public string sortCode { get; set; }

        //    public int coveramount { get; set; }
        //}

        public List<OrderItemsItem> orderItems { get; set; }
        public class OrderItemsItem
        {

            public string itemNo { get; set; } = string.Empty;

            public string sku { get; set; } = string.Empty;

            public string description { get; set; } = string.Empty;

            public string nativeDescription { get; set; } = string.Empty;

            public string hsCode { get; set; } = string.Empty;

            public string originCountry { get; set; } = string.Empty;

            public double unitValue { get; set; }

            public int itemCount { get; set; }

            //public double weight { get; set; }

            //public string productURL { get; set; }
        }
    }
}
