﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.UBI
{
    /// <summary>
    /// 打印面单+获取追踪号
    /// </summary>
    public class PrintLabelRequest:CommonParameter
    {
        public PrintLabelRequest(Parameter p,string logNumber, int labelType ,bool packinglist , string labelFormat ) : base(p)
        {
            orderIds =new List<string> {logNumber };
            this.labelType = labelType;
            this.packinglist = packinglist;
            merged = false;
            this.labelFormat = labelFormat;
        }

        public override string Url => "https://cn.etowertech.com/services/shipper/labels";

        public List<string> orderIds { get; set; }

        public int labelType { get; set; } = 1;

        public bool packinglist { get; set; } = false;

        public bool merged { get; set; }

        [DisplayName("面单类型")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string labelFormat { get; set; }
    }
}
