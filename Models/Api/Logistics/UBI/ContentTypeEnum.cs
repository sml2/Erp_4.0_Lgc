﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static EnumExtension;

namespace ERP.Models.Api.Logistics.UBI
{
    public enum ContentTypeEnum
    {
        //1=(10cm * 15cm)
        //4=(10cm * 10cm)，
        [Description("打印拣货清单(10cm*15cm)")]
        Label1,
        [Description("不打印拣货清单(10cm*15cm)")]
        Label2,
        [Description("打印拣货清单(10cm*10cm)")]
        Label3,
        [Description("不打印拣货清单(10cm*10cm)")]
        Label4,
    }
}
