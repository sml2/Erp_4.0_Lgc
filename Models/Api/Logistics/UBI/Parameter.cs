﻿using Aspose.Cells;
using ERP.Attributes.Logistics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.UBI
{
    public class Parameter: BaseParam
    {
        private string secret;
        [ViewConfigParameter(nameof(Secret), Sort = 0)]
        public string Secret { get { return secret; } set { secret = value.Trim(); } }

        private string token;
        [ViewConfigParameter(nameof(Token), Sort = 1)]
        public string Token { get { return token; } set { token = value.Trim(); } } 
    }
    public static class Anth
    {
        public static string HmacSha1Sign(string text, string Secret)
        {
            string? rtn = null!;
            if (text.IsNotNull() && Secret.IsNotNull())
            {
                using (HMACSHA1 myhmacsha1 = new HMACSHA1(Encoding.UTF8.GetBytes(Secret)))
                {
                    using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(text)))
                    {
                        byte[] hashValue = myhmacsha1.ComputeHash(stream);
                        rtn = Convert.ToBase64String(hashValue);
                    }
                }
            }
            return rtn;
        }
    }
}
