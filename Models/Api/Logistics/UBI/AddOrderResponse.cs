﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ERP.Models.Api.Logistics.UBI
{
    public class AddOrderResponse
    {
        public string status { get; set; } = string.Empty;

        public List<ErrorItem> errors { get; set; } = new();

        public List<DataItem> data { get; set; } = new();
        public class DataItem
        {

            public string status { get; set; } = string.Empty;

            public List<ErrorItem> errors { get; set; } = new();

            public string orderId { get; set; } = string.Empty;

            public string referenceNo { get; set; } = string.Empty;

            public string trackingNo { get; set; } = string.Empty;
        }
        public class ErrorItem
        {
            public int code { get; set; }

            public string message { get; set; } = string.Empty;
        }       
    }
}
