﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ERP.Models.Api.Logistics.UBI
{
    public class ShipTypeResponse
    {
        public string status { get; set; } = string.Empty;

        public List<ErrorItem> errors { get; set; } = new();

        public List<DataItem> data { get; set; } = new();
        public class DataItem
        {
            public string serviceCode { get; set; } = string.Empty;

            public string serviceName { get; set; } = string.Empty;

            public string serviceProvider { get; set; } = string.Empty;

            public string serviceProviderCode { get; set; } = string.Empty;

            public string serviceDescription { get; set; } = string.Empty;

            public string customerSupportPhone { get; set; } = string.Empty;

            public string customerSupportEMail { get; set; }= string.Empty;

            public List<OriginsItem> origins { get; set; } = new();
            public class OriginsItem
            {
                public string country { get; set; } = string.Empty;

                public List<FacilitiesItem> facilities { get; set; } = new();
                public class FacilitiesItem
                {

                    public string code { get; set; } = string.Empty;

                    public FacilityAddress facilityAddress { get; set; } = new();
                    public class FacilityAddress
                    {

                        public string contact { get; set; } = string.Empty;

                        public string company { get; set; } = string.Empty;

                        public string addressLine1 { get; set; } = string.Empty;

                        public string addressLine2 { get; set; } = string.Empty;

                        public string district { get; set; } = string.Empty;

                        public string city { get; set; } = string.Empty;

                        public string province { get; set; } = string.Empty;

                        public string postCode { get; set; } = string.Empty;

                        public string countryCode { get; set; } = string.Empty;

                        public string email { get; set; } = string.Empty;

                        public string contactNumber { get; set; } = string.Empty;
                    }

                    public List<string> pickups { get; set; } = Array.Empty<string>().ToList();
                }
                public override string ToString()
                {
                    return country;
                }
            }

            public List<DestinationsItem> destinations { get; set; } = new();
            public class DestinationsItem
            {
                public string country { get; set; } = string.Empty;
            }

            public List<string> serviceOptions { get; set; } = new();
            public override string ToString()
            {
                return $"{serviceName}[{serviceCode}]";
            }
        }
        public class ErrorItem
        {
            public int code { get; set; }

            public string message { get; set; } = string.Empty;
        }       
    }
}
