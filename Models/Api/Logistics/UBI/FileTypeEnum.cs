﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static EnumExtension;

namespace ERP.Models.Api.Logistics.UBI
{
    public enum FileTypeEnum
    {
        [Description("PDF")]
        PDF,
        [Description("JPG")]
        JPG,
    }
}
