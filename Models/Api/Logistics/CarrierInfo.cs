namespace ERP.Models.Api.Logistics
{
    public class CarrierInfo
    {
        public static readonly List<string> ComCarrierName = new List<string> { "Hermes", "CAINIAO","UBI", "USPS", "UPS", "UPS Freight", "FedEx", "Fedex Freight", "DHL", "DHL eCommerce", "UPS Mail Innovations", "FedEx SmartPost", "OSM", "OnTrac", "StreamLite", "Newgistics", "Blue Package", "Canada Post", "Lasership", "A-1", "Urban Express", "SF Express", "4PX", "Hongkong Post", "Roadrunner", "XPO Freight", "Yellow Freight", "AAA Cooper", "South Eastern Freight Lines", "Watkins and Shepard", "Pilot Freight", "CEVA", "Estes", "Saia", "China Post", "India Post", "Royal Mail", "Yun Express", "Conway", "R+L", "ABF", "Hunter Logistics", "Old Dominion", "SFC", "Yanwen", "First Mile", "Asendia", "Best Buy", "JCEX", "CNE", "YDH" };
        
        public static readonly List<string> CaCarrierName = new List<string> { "Hermes", "CAINIAO", "Canada Post", "DHL", "DHL eCommerce", "FedEx", "PUROLATOR", "UBI", "UPS", "SF Express", "4PX", "Hongkong Post", "China Post", "India Post", "Royal Mail", "Yun Express", "SFC", "Yanwen", "Asendia", "CNE" };

        public static readonly List<string> MxCarrierName = new List<string>
{ "Hermes","CAINIAO","DHL", "DHL eCommerce", "Estafeta", "FedEx","UBI", "UPS", "SF Express", "4PX", "Hongkong Post", "China Post", "India Post", "Royal Mail", "SFC", "Yanwen", "Yun Express", "CNE"};

        public static readonly List<string> CoUkCarrierName = new List<string>
{ "Hermes","CAINIAO","Chronopost", "City Link", "DHL", "DHL eCommerce", "DPD", "Deutsche Post", "Fastway", "FedEx", "GLS", "GO!", "Hermes Logistik Gruppe", "LaPoste", "Parcelforce", "Parcelnet", "PosteItaliane", "RoyalMail", "SDA", "Smartmail", "TNT", "Target", "UPS", "Yodel", "Endopack", "MRW", "Nacex", "Seur", "Chrono Express", "Correos", "SF Express", "4PX", "Hongkong Post", "Arrow XL", "Panther", "China Post", "India Post", "SFC", "Post NL", "Yanwen", "DX Freight", "Yun Express", "Geodis Calberson", "XDP", "Tuffnells", "VIR", "Asendia", "WanbExpress", "WINIT", "Self Delivery", "JCEX", "BJS", "CNE","JT","UBI"};

        public static readonly List<string> DeCarrierName = new List<string>
{ "Hermes","CAINIAO","City Link", "Chronopost", "DHL", "DHL eCommerce", "DHL Home Delivery", "DHL Freight", "GEL Express", "DPD", "Deutsche Post", "Fastway", "FedEx", "GLS", "GO!", "Hermes Logistik Gruppe", "Hermes Einrichtungsservice", "LaPoste", "Parcelforce", "Parcelnet", "PosteItaliane", "RoyalMail", "SDA", "Smartmail", "TNT", "Target", "UPS", "Yodel", "Endopack", "MRW", "Nacex", "Seur", "Chrono Express", "Correos", "SF Express", "4PX", "Hongkong Post", "Kuehne+Nagel", "China Post", "Rieck", "India Post", "IDS Netzwerk", "DSV", "DB Schenker", "Raben Group", "SFC", "Post NL", "Yanwen", "Yun Express", "Rhenus", "AT POST", "Geodis Calberson", "VIR", "Asendia", "WanbExpress", "WINIT", "Self Delivery", "JCEX", "CNE","UBI"};

        public static readonly List<string> FrCarrierName = new List<string>
{ "Hermes","CAINIAO","Chronopost", "City Link", "DHL", "DHL eCommerce", "DPD", "Deutsche Post", "Fastway", "FedEx", "GLS", "GO!", "Hermes Logistik Gruppe", "LaPoste", "Parcelforce", "Parcelnet", "PosteItaliane", "RoyalMail", "SDA", "Smartmail", "TNT", "Target", "UPS", "Yodel", "Endopack", "MRW", "Nacex", "Seur", "Chrono Express", "Correos", "Colissimo", "Coliposte", "SF Express", "4PX", "Hongkong Post", "China Post", "India Post", "SFC", "Yanwen", "Yun Express", "Post NL", "Geodis Calberson", "VIR", "Asendia", "WanbExpress", "WINIT", "Self Delivery", "JCEX", "CNE","JT","UBI"};

public static readonly List<string> ItCarrierName = new List<string>
{ "Hermes","BRT", "CAINIAO","Chronopost", "City Link", "DHL", "DHL eCommerce", "DPD", "Deutsche Post", "Fastway", "FedEx", "GLS", "GO!", "Hermes Logistik Gruppe", "LaPoste", "Nexive", "Parcelforce", "Parcelnet", "PosteItaliane", "RoyalMail", "SDA", "Smartmail", "TNTIT", "Target", "UPS", "Yodel", "Endopack", "MRW", "Nacex", "Seur", "Chrono Express", "SF Express", "Correos", "4PX", "Hongkong Post", "China Post", "India Post", "Yun Express", "SFC", "Post NL", "Yanwen", "Energo", "Geodis Calberson", "VIR", "Asendia", "WanbExpress", "WINIT", "Self Delivery", "JCEX", "CNE","UBI"};

public static readonly List<string> EsCarrierName = new List<string>
{"Hermes","CAINIAO", "Correos Express", "Chronopost", "City Link", "DHL", "DHL eCommerce", "DPD", "Deutsche Post", "Envialia", "Fastway", "FedEx", "GLS", "GO!", "LaPoste", "Hermes Logistik Gruppe", "Parcelforce", "Parcelnet", "PosteItaliane", "RoyalMail", "SDA", "Smartmail", "TIPSA", "TNT", "Target", "UPS", "Yodel", "Endopack", "MRW", "Nacex", "Seur", "Chrono Express", "Correos", "SF Express", "4PX", "Hongkong Post", "China Post", "India Post", "Yun Express", "SFC", "Post NL", "Yanwen", "Home Logistics", "Geodis Calberson", "VIR", "Asendia", "WanbExpress", "WINIT", "Self Delivery", "CTTExpress", "JCEX", "CELERITAS", "CNE","UBI"};


        public static readonly List<string> JpCarrierName = new List<string>
{ "Hermes","日本郵便", "佐川急便", "ヤマト運輸", "4PX", "Asendia", "CAINIAO","China Post", "DHL", "DHL eCommerce", "FedEx", "Hongkong Post", "India Post", "日本通運", "Royal Mail", "SF Express", "SFC", "西濃運輸", "YDH", "Yanwen", "Yun Express", "CNE","UBI"};
//{"ヤマト運輸", "佐川急便", "日本通运", "日本 Seino 货运", "日本邮政", "DHL eCommerce", "顺丰速运", "递四方速递", "中国香港邮政", "印度邮政", "英国皇家邮政", "DHL", "中国邮政", "Yun Express", "FedEx", "SFC", "Yanwen", "YDH", "Asendia"}



   public static readonly List<string> NlCarrierName = new List<string>
{ "Hermes","Asendia", "CAINIAO","China Post", "Chrono Express", "Chronopost", "City Link", "Correos", "DB Schenker", "Deutsche Post", "DHL", "DHL Ecommerce", "DHL Freight", "DHL Home Delivery", "DPD", "DSV", "Endopack", "FedEx", "Fastway", "GEL Express", "GLS", "GO", "Hermes Einrichtungsservice", "Hermes Logistik Gruppe", "Hongkong Post", "IDS Netzwek", "India Post", "Kuehne+Nagel", "La Poste", "MRW", "Nacex", "Parcelforce", "Parcelnet", "Poste Italiane", "Post NL", "Raben Group", "Royal Mail", "Rieck", "SDA", "Seur", "SFC", "SF Express", "Smartmail", "Target", "TNT", "UPS", "Yanwen", "Yodel", "Yun Express", "4PX", "CNE","UBI"};

        public static readonly List<string> SeCarrierName = new List<string>
{ "Hermes","CAINIAO","GLS", "Yun Express", "Bartolini", "4PX", "Arco Spedizioni", "Asendia", "BRT", "CEVA", "CNE", "China Post", "Chrono Express", "Chronopost", "Chukou1", "City Link", "Correos", "DHL", "DHL eCommerce", "DPD", "Deutsche Post", "Digital Delivery", "Endopack", "Energo", "Equick", "FAST EST", "FERCAM", "Fastway", "FedEx", "GO!", "Geodis Calberson", "Hermes Logistik Gruppe", "HongKong Post", "India Post", "JCEX", "La Poste", "Liccardi", "MRW", "Milkman", "Nacex", "Parcelforce", "Parcelnet", "Post NL", "Poste Ltaliane", "Royal Mail", "SDA", "SF Express", "SFC", "Self Delivery", "Seur", "Smartmail", "TNTIT", "Target", "UBI", "UPS", "VIR", "WINIT", "WanbExpress", "Yanwen", "Yodel", "Zust Ambrosetti", "Aramex", "ATS", "ECMS", "Geodis", "iParcel", "Landmark", "Post Nord", "RoyalMail"};


public static readonly List<string> AeCarrierName = new List<string>
{ "Hermes","Aramex", "Alloy", "BRT","CAINIAO", "Citi trans", "DHL", "DHL eCommerce", "DHL Freight", "DPD", "Emirates Post", "FedEx", "First Flight China", "GLS", "ICC Worldwide", "JCEX", "MRW", "QExpress", "Seur", "SFC", "Ship Global US", "Shunfeng Express", "Speedex", "TNT", "VNLIN", "Yanwen", "Yodel", "Yun Express", "CNE","UBI"};


        public static readonly List<string> AuCarrierName = new List<string>
{ "Hermes","Australia Post", "Australia Post-Consignment", "Australia Post-ArticlelD","CAINIAO", "StarTrack-Consignrnent", "StarTrack-ArticlelD", "Fastway", "TNT", "Toll Global Express", "DHL", "Sendle", "Assett", "UPS", "Landmark", "ECMS", "HS code", "geodis", "CDC", "SFC", "中国邮政", "印度邮政", "英国皇家邮政", "SENDLE", "Couriers Please", "4PX", "Yun Express", "CNE","UBI", "其他"};


public static Dictionary<string, List<string>> DicCarrierName = new Dictionary<string, List<string>>()
{
        { "美国", ComCarrierName},
        { "加拿大", CaCarrierName},
        { "墨西哥", MxCarrierName},
        { "英国", CoUkCarrierName},
        { "德国", DeCarrierName},
        { "法国", FrCarrierName},
        { "意大利", ItCarrierName},
        { "西班牙", EsCarrierName},
        { "日本", JpCarrierName},
        { "澳洲", AuCarrierName},
        { "荷兰", NlCarrierName},
        { "瑞典", SeCarrierName},
        { "中东阿联酋", AeCarrierName},
        { "沙特", AeCarrierName}
};


        public List<string> GetCarrierName(string country)
        {
            return DicCarrierName[country];
        }
    }
}
