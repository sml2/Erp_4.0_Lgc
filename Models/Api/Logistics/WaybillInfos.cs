﻿using ERP.Models.DB.Logistics;

namespace ERP.Models.Api.Logistics
{
    public class WaybillInfos: BaseParameter
    {
        /// <summary>
        /// 运单主键id
        /// </summary>
        public int waybillId { get; set; }
        /// <summary>
        /// 本系统订单号
        /// </summary>
        public string CustomNumber { get; set; } = string.Empty;

        /// <summary>
        /// 国际物流运单/面单号
        /// </summary>
        public string? LogictisNumber { get; set; }

        /// <summary>
        /// 物流跟踪号
        /// </summary>
        public string? TrackNumber { get; set; }

        public WaybillInfos()
        {
            
        }

        public WaybillInfos(Waybill m)
        {
            waybillId = m.ID;
            LogName = m.Logistic.Platform;
            Parameter = m.LogisticExt;
            CustomNumber = m.WaybillOrder ?? string.Empty;
            LogictisNumber = m.Express;
            TrackNumber = m.Tracking;
        }
       
    }
}
