namespace ERP.Models.Api.Logistics
{
    public class CancleParameter : BaseParameter
    {
        public CancleParameter(int id, string number, Platforms LogName, string Parameter)
        {
            waybillID = id;
            LogictisNumber = number;
            base.LogName = LogName;
            base.Parameter = Parameter;
        }
        public int waybillID { get; set; }
        public string LogictisNumber { get; set; }
    }

    public class BillParameter : BaseParameter
    {
        public BillParameter()
        {
                
        }
        public BillParameter(int id, string number,string beginDate,string endDate, Platforms LogName, string Parameter)
        {
            waybillID = id;
            CustomNumber = number;
            base.LogName = LogName;
            // ShippingMethodNo = shippingMethodNo;
            BeginDate = beginDate;
            EndDate = endDate;
            base.Parameter = Parameter;
        }
        public int waybillID { get; set; }
        /// <summary>
        /// 客户参考号
        /// </summary>
        public string CustomNumber { get; set; } = "";

        /// <summary>
        /// 服务商单号
        /// </summary>
        public string ShippingMethodNo { get; set; } = "";

        /// <summary>
        /// 到货开始时间
        /// </summary>
        public string BeginDate { get; set; } = "";

        /// <summary>
        /// 到货结束时间 
        /// </summary>
        public string EndDate { get; set; } = "";
    }
}
