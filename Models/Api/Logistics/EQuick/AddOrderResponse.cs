﻿using System.Xml.Serialization;

namespace ERP.Models.Api.Logistics.EQuick
{
    [XmlRoot(ElementName = "Response")]
    public class AddOrderResponse
    {
        public Cls_BookingShipment BookingShipment { get; set; } = new();
        public class Cls_BookingShipment
        {
            /// <summary>
            /// 客户参考操作日期，默认取当前日期，方便双方核对
            /// </summary>
            public string BookingCustDate { get; set; } = string.Empty;

            /// <summary>
            /// 客户参考号码，方便双方核对
            /// </summary>
            public string BookingCustRefNo { get; set; } = string.Empty;

            /// <summary>
            /// 客户参考VMI号，方便双方核对
            /// </summary>
            public string BookingCustRefVMI { get; set; } = string.Empty;

            /// <summary>
            /// 客户编号，不建议填写，系统自动取认证头文件中的用户名
            /// </summary>
            public string CustomNo { get; set; } = string.Empty;

            /// <summary>
            /// (必填)数据来源标识，固定值，建议对接方平台英文简称或企业简称
            /// </summary>
            public string EOCSource { get; set; } = string.Empty;

            /// <summary>
            /// quick单号，填写Equick预先分配的单号或不填
            /// </summary>
            public string EquickWBNo { get; set; } = string.Empty;

            /// <summary>
            /// 发货人EORI，部分国家（英国）选择填写
            /// </summary>
            public string FmCustEORI { get; set; } = string.Empty;

            /// <summary>
            /// 发货人IOSS，部分国家（欧盟国家）选择填写
            /// </summary>
            public string FmCustIOSS { get; set; } = string.Empty;

            /// <summary>
            /// 发货人税号，部分国家选择填写
            /// </summary>
            public string FmCustTaxNo { get; set; } = string.Empty;

            /// <summary>
            /// 货物国家代码，默认取CN或目的地国家代码
            /// </summary>
            public string GoodsCountry { get; set; } = string.Empty;

            /// <summary>
            /// 货物货币名称，3位代码，默认取USD
            /// </summary>
            public string GoodsCurrency { get; set; } = string.Empty;

            /// <summary>
            /// 货物描述，尽量如实填写以备海关查验
            /// </summary>
            public string GoodsDesc { get; set; } = string.Empty;

            /// <summary>
            /// 货物分类，统一填写M，不必填写
            /// </summary>
            public string GoodsKind { get; set; } = string.Empty;

            /// <summary>
            /// (必填)货物中文名称，AU及NZ需说明材质及新旧
            /// </summary>
            public string GoodsNameCHS { get; set; } = string.Empty;

            /// <summary>
            /// (必填)货物英文名称，AU及NZ需说明材质及新旧
            /// </summary>
            public string GoodsNameENG { get; set; } = string.Empty;

            /// <summary>
            /// 费方式，统一填写P，不必填写
            /// </summary>
            public string GoodsPayment { get; set; } = string.Empty;

            /// <summary>
            /// 货物件数，包裹内实际货物件数，尽量如实填写以备海关查验，若不填写则默认1
            /// </summary>
            public int GoodsPieceNum { get; set; }

            /// <summary>
            /// 货物报价方式
            /// </summary>
            public string GoodsQuoteKind { get; set; } = string.Empty;

            /// <summary>
            /// 投保金额
            /// </summary>
            public float GoodsRiskAmount { get; set; }

            /// <summary>
            /// 投保币种，3位代码
            /// </summary>
            public string GoodsRiskCurrency { get; set; } = string.Empty;

            /// <summary>
            /// 保险类型
            /// </summary>
            public string GoodsRiskType { get; set; } = string.Empty;

            /// <summary>
            /// 是否购买保险，Y/N，以实际业务发生为准，不必填写
            /// </summary>
            public string GoodsRiskWhether { get; set; } = string.Empty;

            /// <summary>
            /// SKU，加拿大专线必须录入
            /// </summary>
            public string GoodsSKU { get; set; } = string.Empty;

            /// <summary>
            /// 货物派送指示，不必填写
            /// </summary>
            public string GoodsSchedule { get; set; } = string.Empty;

            /// <summary>
            /// 税金支付，SHIPPER/CONSIGNEE，以实际业务发生为准，不必填写
            /// </summary>
            public string GoodsTaxPayment { get; set; } = string.Empty;

            /// <summary>
            /// 货物产品，DD/DA/AA/AD，不必填写
            /// </summary>
            public string GoodsTerm { get; set; } = string.Empty;

            /// <summary>
            /// (必填)货物申报价值
            /// </summary>
            public float GoodsValue { get; set; }

            /// <summary>
            /// (必填)货物申报币种
            /// </summary>
            public string GoodsValueCurrency { get; set; } = string.Empty;

            /// <summary>
            /// HSCODE，尽量如实填写以备海关查验
            /// </summary>
            public string HSCodeNo { get; set; } = string.Empty;

            /// <summary>
            /// (必填)
            /// </summary>
            public string QuickType { get; set; } = string.Empty;

            /// <summary>
            /// 是否接收退件，Y/N，目前统一为N
            /// </summary>
            public string ReturnQuest { get; set; } = string.Empty;

            /// <summary>
            /// 签名服务，Y/N，目前统一为 N
            /// </summary>
            public string SignreQuest { get; set; } = string.Empty;

            /// <summary>
            /// (必填)收货公司名称
            /// </summary>
            public string ToCompanyName { get; set; } = string.Empty;

            /// <summary>
            /// 收货地址一 由地址第一行转换而来，可以不填写
            /// </summary>
            public string ToCustAdd01 { get; set; } = string.Empty;

            /// <summary>
            /// 收货地址二 由地址第二行转换而来，可以不填写
            /// </summary>
            public string ToCustAdd02 { get; set; } = string.Empty;

            /// <summary>
            /// 收货地址三 由地址第三行转换而来，可以不填写
            /// </summary>
            public string ToCustAdd03 { get; set; } = string.Empty;

            /// <summary>
            /// 收货地址四 由地址第四行转换而来，可以不填写
            /// </summary>
            public string ToCustAdd04 { get; set; } = string.Empty;

            /// <summary>
            /// (必填)收货地址,用换行符（\n）切割，前两行不超过40个字符
            /// </summary>
            public string ToCustAddress { get; set; } = string.Empty;

            /// <summary>
            /// 州名，部分业务必须录入，AU、US、CA等国家必须录入代码
            /// </summary>
            public string ToCustCanton { get; set; } = string.Empty;

            /// <summary>
            ///(必填)城市
            /// </summary>
            public string ToCustCity { get; set; } = string.Empty;

            /// <summary>
            /// (必填)国家代码，两位代码
            /// </summary>
            public string ToCustCountry { get; set; } = string.Empty;

            /// <summary>
            /// 传真
            /// </summary>
            public string ToCustFax { get; set; } = string.Empty;

            /// <summary>
            /// 收货人身份号
            /// </summary>
            public string ToCustIDCardNo { get; set; } = string.Empty;

            /// <summary>
            /// (必填)收货人
            /// </summary>
            public string ToCustLinkman { get; set; } = string.Empty;

            /// <summary>
            /// 电子邮件
            /// </summary>
            public string ToCustMail { get; set; } = string.Empty;

            /// <summary>
            /// 收货人税号，部分国家（巴西、挪威、英国等）必须填写
            /// </summary>
            public string ToCustTaxNo { get; set; } = string.Empty;

            /// <summary>
            /// 电话，部分业务必须录入，US须10位数字号码
            /// </summary>
            public string ToCustTel { get; set; } = string.Empty;

            /// <summary>
            /// (必填)邮编
            /// </summary>
            public string ToCustZipCode { get; set; } = string.Empty;

            /// <summary>
            /// 部分渠道国外追踪号，若填写则必须填写Equick预先分配的单号，或者不填写
            /// </summary>
            public string TrackTraceLabelNo { get; set; } = string.Empty;

            /// <summary>
            /// (必填)实际重量，单位：千克
            /// </summary>
            public float WHTFactWHT { get; set; }

            /// <summary>
            /// 高(CM) ，以EQUICK实际收到货物为准
            /// </summary>
            public float WHTHeight { get; set; }

            /// <summary>
            /// 长(CM)，以EQUICK实际收到货物为准
            /// </summary>
            public float WHTLength { get; set; }

            /// <summary>
            /// 宽(CM) ，以EQUICK实际收到货物为准
            /// </summary>
            public float WHTWidth { get; set; }           
        }

        public Cls_ShipmentCompleted ShipmentCompleted { get; set; } = new();
        public class Cls_ShipmentCompleted
        {
            /// <summary>
            /// 请求运费函数返回结果
            /// </summary>
            public string BookingFreightCompleted { get; set; } = string.Empty;

            /// <summary>
            /// Equick单号 ReturnValue<=0，返回创建成功之单号 ReturnValue>0，返回空，错误信息见ReturnMessage
            /// </summary>
            public string EquickWBNo { get; set; } = string.Empty;

            /// <summary>
            /// 返回值： -1 – 创建成功
            /// 0 – 创建可能成功，有异常信息提示
            /// >0 – 创建失败，数字值为创建失败信息数
            /// </summary>
            public int ReturnValue { get; set; }

            public string ReturnMessage { get; set; } = string.Empty;
        }              
    }
}
