﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.EQuick
{
    public class GetEstimatePriceResponse
    {
        public string Code { get; set; } = string.Empty;
        /// <summary>
        /// 提交失败/提交成功
        /// </summary>
        public string Message { get; set; } = string.Empty;
        public List<Cls_Item> Items { get; set; } = new();
        public class Cls_Item
        {
            /// <summary>
            /// 
            /// </summary>
            public string Code { get; set; } = string.Empty;
            /// <summary>
            /// 
            /// </summary>
            public string CName { get; set; } = string.Empty;
            /// <summary>
            /// 
            /// </summary>
            public string EName { get; set; } = string.Empty;
            /// <summary>
            /// 基础运费
            /// </summary>
            public string ShippingFee { get; set; } = string.Empty;
            /// <summary>
            /// 挂号费
            /// </summary>
            public string RegistrationFee { get; set; } = string.Empty;
            /// <summary>
            /// 燃油费
            /// </summary>
            public string FuelFee { get; set; } = string.Empty;
            /// <summary>
            /// 关税预付服务费
            /// </summary>
            public string TariffPrepayFee { get; set; } = string.Empty;
            /// <summary>
            /// 杂费
            /// </summary>
            public string SundryFee { get; set; } = string.Empty;
            /// <summary>
            /// 
            /// </summary>
            public string TotalFee { get; set; } = string.Empty;
            /// <summary>
            /// 预计时效
            /// </summary>
            public object DeliveryDays { get; set; } = string.Empty;
            /// <summary>
            /// 说明(当Remarks为空时不显示）
            /// </summary>
            public string Remarks { get; set; } = string.Empty;
        }
    }
}
