﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.EQuick
{   
    public class TrackResponse
    {
        public List<Trace> datas { get; set; } = new();
        
        public class Trace
        {
            /// <summary>
            /// 目的地单号
            /// </summary>
            public string DestinationWBNo { get; set; } = string.Empty;

            /// <summary>
            /// 签名
            /// </summary>
            public string SignatureBy { get; set; } = string.Empty;

            /// <summary>
            /// 追踪说明
            /// </summary>
            public string TraceContent { get; set; } = string.Empty;

            /// <summary>
            /// 追踪国家
            /// </summary>
            public string TraceCountry { get; set; } = string.Empty;

            /// <summary>
            /// 追踪时间
            /// </summary>
            public string TraceDateTime { get; set; } = string.Empty;

            /// <summary>
            /// >=200 – 国外部分 >=100<=199 – 国内部分 特别关注： 299 – 妥投 298 – 派送异常
            /// </summary>
            public int TraceKind { get; set; }

            /// <summary>
            /// 追踪地点
            /// </summary>
            public int TraceLocation { get; set; }

            /// <summary>
            /// 产品代码
            /// </summary>
            public int quickType { get; set; }
        }               

        
    }
}
