﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.EQuick
{
    public class TrackNumberRequest
    {
        public class Cls_EOCDestinationLabelNoRequest
        {
            public string EquickWBNo { get; set; } = string.Empty;
        }
        public Cls_EOCDestinationLabelNoRequest EOCDestinationLabelNoRequest { get; set; } = new();
    }
}
