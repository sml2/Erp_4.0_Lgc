﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.EQuick
{
    public class ExtParameter: BaseExtParam
    {
        public ExtParameter(string fmCustIOSS, string fmCustEORI, string fmCustTaxNo, string cName, string eName,
            string hsCode, string quickType, float weight, float length, float width, float height, float goodsValue, 
            string goodsCurrency)
        {
            FmCustIOSS = fmCustIOSS;
            FmCustEORI = fmCustEORI;
            FmCustTaxNo = fmCustTaxNo;
            CName = cName;
            EName = eName;
            HsCode = hsCode;
            QuickType = quickType;
            Weight = weight;
            Length = length;
            Width = width;
            Height = height;
            GoodsValue = goodsValue;
            GoodsCurrency = goodsCurrency;
            //if (Enum.IsDefined(typeof(enumCurrency), goodsCurrency) && Enum.TryParse<enumCurrency>(goodsCurrency, out var labelgoodsCurrency))
            //{
            //    this.GoodsCurrency = labelgoodsCurrency;
            //}
            //else
            //{
            //    Console.WriteLine("EQuick goodsCurrency:" + goodsCurrency);
            //}          
        }

        /// <summary>
        /// 发货人 IOSS
        /// </summary>      
        public string FmCustIOSS { get; set; }

        /// <summary>
        /// 发货人 EORI
        /// </summary>
        public string FmCustEORI { get; set; }

        /// <summary>
        /// 发货人 税号
        /// </summary>
        public string FmCustTaxNo { get; set; }

        /// <summary>
        /// 商品申报名(中文)
        /// </summary>
        [DisplayName("商品申报名(中文)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string CName { get; set; }

        /// <summary>
        /// 商品申报名(英文)
        /// </summary>
        [DisplayName("商品申报名(英文)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string EName { get; set; }

        /// <summary>
        /// 海关编码
        /// </summary>
        public string HsCode { get; set; }

        /// <summary>
        /// 运输方式
        /// </summary>
        [DisplayName("运输方式")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string QuickType { get; set; }

        /// <summary>
        /// 包裹重量(kg)
        /// </summary>
        [DisplayName("包裹重量(g)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public float Weight { get; set; }

        /// <summary>
        /// 包裹边长(cm)
        /// </summary>
        [DisplayName("包裹边长(cm)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public float Length { get; set; }

        /// <summary>
        /// 包 裹 边 宽 (cm)
        /// </summary>
        [DisplayName("包裹边宽(cm)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public float Width { get; set; }

        /// <summary>
        /// 包 裹 边 高 (cm)
        /// </summary>
        [DisplayName("包裹边高(cm)")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public float Height { get; set; }

        /// <summary>
        /// 货物申报价值
        /// </summary>
        [DisplayName("货物申报价值")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public float GoodsValue { get; set; }

        /// <summary>
        /// 货物申报币种
        /// </summary>
        [DisplayName("货物申报币种")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string GoodsCurrency { get; set; }        
    }
    //public enum enumCurrency
    //{
    //    [EnumExtension.Description("美元(USD)")]
    //    USD,
    //    [EnumExtension.Description("欧元(EUR)")]
    //    EUR,
    //}

}
