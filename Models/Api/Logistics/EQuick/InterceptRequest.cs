﻿using Newtonsoft.Json;

namespace ERP.Models.Api.Logistics.EQuick
{
    public class InterceptRequest: CommonParameter
    {
        public InterceptRequest(Parameter p,string logisticNumber) : base(p)
        {
            EquickWBNo = logisticNumber;
        }
        public string EquickWBNo { get; set; }

        [JsonIgnore]
        public override string Url { get => $"{Domain}/RequestBookingKeap"; }
    }


    public class InterceptResponse
    {
        /// <summary>
        /// 
        /// </summary>
        public string EquickWBNo { get; set; }

        public string ReturnMessage { get; set; }

        /// <summary>
        /// 0成功 1为失败
        /// </summary>
        public int ReturnValue { get; set; }
    }

}
