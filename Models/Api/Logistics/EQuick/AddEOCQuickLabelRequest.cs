﻿namespace ERP.Models.Api.Logistics.EQuick
{
    public class EOCQuickLabelRequest:CommonParameter
    {
        public EOCQuickLabelRequest(Parameter p,string orderNumber) : base(p)
        {
            Authentication = new EOCQuickLabelRequest.Cls_Authentication();
            Authentication.CustomNo = p.CustomNo;
            Authentication.Password = Sy.Security.MD5.Encrypt(p.Password);
            EquickWBNo = orderNumber;
        }

        public override string Url { get => $"{Domain}/RequestEOCQuickLabel";}

        public class Cls_Authentication
        {
            public string CustomNo { get; set; } = string.Empty;

            public string Password { get; set; } = string.Empty;
        }
        public Cls_Authentication Authentication { get; set; }

        public string EquickWBNo { get; set; }
        public string QuickLabelType { get; set; } = "Lbl10x10";
    }  
}
