﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.EQuick
{
    public class PrintUrlResponse
    {
        public string ResultCode { get; set; } = string.Empty;
        /// <summary>
        /// 提交失败/提交成功
        /// </summary>
        public string ResultDesc { get; set; } = string.Empty;
        public List<Cls_Item> Item { get; set; } = new();
        public class Cls_Item
        {
            public string Url { get; set; } = string.Empty;
            public List<Cls_LabelPrintInfos> LabelPrintInfos { get; set; } = new();
            public class Cls_LabelPrintInfos
            {
                public string OrderNumber { get; set; } = string.Empty;
                public int ErrorCode { get; set; }
                public string ErrorBody { get; set; } = string.Empty;
            }

        }
    }
}
