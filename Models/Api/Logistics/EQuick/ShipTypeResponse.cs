﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Mod_GlobalVariables;

namespace ERP.Models.Api.Logistics.EQuick
{

    public class ShipTypeResponse 
    {     
        public class ShipType
        {
            public string EOCQuickType { get; set; } = string.Empty;

            public string EOCQuickTypeName { get; set; } = string.Empty;

            public string QuickTypeKind { get; set; } = string.Empty;
            public override string ToString()
            {
                return EOCQuickTypeName;
            }
        }
        //public string RequestId { get; set; }
        //public string Code { get; set; }
        //public string Message { get; set; }
        //public string TimeStamp { get; set; }
        //public List<Cls_Item> Items { get; set; }
        //public class Cls_Item
        //{
        //    private string price;

        //    private GetEstimatePriceResponse.Cls_Item item;
        //    public void SetAllPrice(GetEstimatePriceResponse.Cls_Item item)
        //    {
        //        this.item = item;
        //        price = item.TotalFee;
        //    }
        //    public string Code { get; set; }
        //    public string CName { get; set; }
        //    public string EName { get; set; }
        //    public bool HasTrackingNumber { get; set; }
        //    public string DisplayName { get; set; }
        //    public override string ToString()
        //    {
        //        //string p = price.IsNotWhiteSpace() ? $"({price})" : "";
        //        string p = price.IsNotWhiteSpace() ? $"(估价:{price})" : "";
        //        return CName + $"[{Code}]{p}";
        //    }
        //}
      
    }
}
