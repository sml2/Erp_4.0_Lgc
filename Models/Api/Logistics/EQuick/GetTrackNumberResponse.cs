﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.EQuick
{
    public class GetTrackNumberResponse
    {
        public string RequestId { get; set; } = string.Empty;
        public string TimeStamp { get; set; } = string.Empty;
        public string Code { get; set; } = string.Empty;
        /// <summary>
        /// 提交失败/提交成功
        /// </summary>
        public string Message { get; set; } = string.Empty;
        public List<Cls_Item> Item { get; set; } = new();
        public class Cls_Item
        {
            public int Status { get; set; }
            public string CustomerOrderNumber { get; set; } = string.Empty;
            public string TrackingNumber { get; set; } = string.Empty;
            public string WayBillNumber { get; set; } = string.Empty;
            public object Remark { get; set; } = string.Empty;
        }
    }
}
