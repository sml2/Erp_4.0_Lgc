﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.EQuick
{
    public class TrackRequest :CommonParameter
    {
        public TrackRequest(Parameter p,string logNumber,string trackNumber) : base(p)
        {           
            EOCTrackTraceDataSetRequest = new TrackRequest.Cls_EOCTrackTraceDataSetRequest();
            EOCTrackTraceDataSetRequest.TrackTraceLabelNo = trackNumber;
            EOCTrackTraceDataSetRequest.EquickWBNo = logNumber;
        }

        public override string Url { get => $"{Domain}/TrackTraceDataSet";}

        public class Cls_EOCTrackTraceDataSetRequest
        {
            public string EquickWBNo { get; set; } = string.Empty;

            public string TrackTraceLabelNo { get; set; } = string.Empty;
        }
        public Cls_EOCTrackTraceDataSetRequest EOCTrackTraceDataSetRequest
        { get; set; }

    }
}
