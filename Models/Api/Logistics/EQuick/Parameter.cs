﻿
using ERP.Attributes.Logistics;

namespace ERP.Models.Api.Logistics.EQuick
{
    public class Parameter: BaseParam
    {
        private string id;
        private string password;
        [ViewConfigParameter(nameof(CustomNo), Message = "请填写用户名", Sort = 0)]
        /// <summary>
        /// 用户名
        /// </summary>
        public string CustomNo { get { return id; } set { id = value.Trim(); } } 

        [ViewConfigParameter(nameof(Password), Message = "请填写密码", Sort = 10)]
        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get { return password; } set { password = value.Trim(); } }

        public string Domain
        {
            get
            {
                return "http://api.equick.cn:8080";
            }
        }

        //private string Host = "http://api.equick.cn:8080/";//测试环境:http://api.equick.cn:18080/v2/api-docs
        //                                                   //正式环境URL: http://api.equick.cn:8080/   
        //                                                   //正式环境URL: https://api.equick.cn/
        //                                                   
    }
}
