﻿namespace ERP.Models.Api.Logistics.EQuick
{
    public class EOCQuickLabelResponse
    {
        public Cls_EOCQuickLabel QuickLabel { get; set; } = null!;
        public class Cls_EOCQuickLabel
        {
            public string EquickWbNo { get; set; } = string.Empty;
            public string LabelData { get; set; } = string.Empty;
            public string TrackTraceLabelNo { get; set; } = string.Empty;
        }
        public Cls_QuickLabelCompleted QuickLabelCompleted { get; set; } = new();
        public class Cls_QuickLabelCompleted
        {
            public string ReturnMessage { get; set; } = string.Empty;
            public int ReturnValue { get; set; }
        }
    }
}
