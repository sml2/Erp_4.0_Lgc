﻿using Newtonsoft.Json;

namespace ERP.Models.Api.Logistics.EQuick
{
    public class CommonParameter: RequestLogistic
    {
        public CommonParameter(Parameter p)
        {
            CustomNo = p.CustomNo;
            Password =p.Password;
        }

        /// <summary>
        /// 用户名
        /// </summary>
        public string CustomNo { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        [JsonIgnore]
        public string Domain
        {
            get
            {
                return "http://api.equick.cn:8080";
            }
        }

        //private string Host = "http://api.equick.cn:8080/";//测试环境:http://api.equick.cn:18080/v2/api-docs
        //                                                   //正式环境URL: http://api.equick.cn:8080/   
        //                                                   //正式环境URL: https://api.equick.cn/
        //              
    }
}
