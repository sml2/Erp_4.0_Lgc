﻿namespace ERP.Models.Api.Logistics.EQuick
{
    public class ShipTypeRequest : CommonParameter
    {
        public ShipTypeRequest(Parameter p) : base(p)
        {

        }

        public override string Url { get => $"{Domain}/BookingQuickTypeDataSet";}
    }
}
