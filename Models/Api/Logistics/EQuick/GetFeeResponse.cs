﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ERP.Models.Api.Logistics.EQuick
{
    [XmlRoot(ElementName = "Response")]
    public class GetFeeResponse
    {
        public Cls_BookingFreight BookingFreight { get; set; } = new();
        public class Cls_BookingFreight
        {
            public double BookingFee { get; set; }
            public double BookingFeeWht { get; set; }
            public string EquickWBNo { get; set; } = string.Empty;
            public double OtherFreight { get; set; }
            public string OtherFreightCause { get; set; } = string.Empty;
            public double TotFreight  { get; set; }
            public double WhtFactWht { get; set; }
            public double WhtHeight { get; set; }
            public double WhtLength { get; set; }
            public double WhtVOLWht { get; set; }
            public double WhtWidth { get; set; }
        }
        public Cls_BookingFreightCompleted BookingFreightCompleted { get; set; } = new();

        public class Cls_BookingFreightCompleted
        {
            public string ReturnMessage { get; set; } = string.Empty;

            //返回值： -1 – 请求查询成功0 – 请求查询成功但运费未计算 >0 – 请求查询失败，数字值为失败信息编号
            public int ReturnValue { get; set; }
        }
    }
}
