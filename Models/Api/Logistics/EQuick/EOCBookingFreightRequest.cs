﻿namespace ERP.Models.Api.Logistics.EQuick
{
  public  class EOCBookingFreightRequest:CommonParameter
    {
        public EOCBookingFreightRequest(Parameter p,string equickWBNo) : base(p)
        {
           EquickWBNo = equickWBNo;
           Authentication = new EOCBookingFreightRequest.Cls_Authentication();
           Authentication.CustomNo = p.CustomNo;
           Authentication.Password = Sy.Security.MD5.Encrypt(p.Password);
        }

        public override string Url { get =>$"{Domain}/RequestBookingFreight";}


        public class Cls_Authentication
        {
            public string CustomNo { get; set; } = string.Empty;
            public string Password { get; set; } = string.Empty;
        }

        public Cls_Authentication Authentication { get; set; }
        public string EquickWBNo { get; set; } = string.Empty;
    }
}
