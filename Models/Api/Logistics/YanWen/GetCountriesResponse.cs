﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.YanWen
{

    public class GetCountriesResponse
    {
        /// <summary>
        /// 国家 Id
        /// </summary>
        [JsonProperty("id")]
        public int ID { get; set; }

        /// <summary>
        /// 国家代码
        /// </summary>
        [JsonProperty("code")]
        public string Code { get; set; }

        /// <summary>
        /// 国家中文名称
        /// </summary>
        [JsonProperty("nameCh")]
        public string NameCh { get; set; }

        ///国家英文名称
        [JsonProperty("nameEn")]
        public string NameEn { get; set; }

        public override string ToString()
        {
            return $"{NameCh}({NameEn})";
        }
    }
}
