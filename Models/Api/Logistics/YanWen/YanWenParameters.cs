﻿namespace ERP.Models.Api.Logistics.YanWen
{
    public class YanWenParameters
    {
        /// <summary>
        /// 货运方式
        /// </summary>
        public string ShipCode { get; set; } = string.Empty;

        /// <summary>
        /// 寄件人税号
        /// </summary>
        public string SenderTaxNumber { get; set; } = string.Empty;

        /// <summary>
        /// 收件人税号
        /// </summary>
        public string ReceiveTaxNumber { get; set; } = string.Empty;
    }
}
