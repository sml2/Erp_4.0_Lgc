﻿using Aspose.Cells;
using ERP.Models.Api.Logistics.Extension;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;


namespace ERP.Models.Api.Logistics.YanWen
{
    public class ShipTypeResponse:List<ShipType>
    {        
    }

    public class ShipType
    {
        /// <summary>
        /// 产品编号Id
        /// </summary>
        [JsonProperty("id")]
        public int ID { get; set; }

        /// <summary>
        /// 产品中文名称
        /// </summary>
        [JsonProperty("nameCh")]
        public string NameCh { get; set; }

        /// <summary>
        /// 产品英文名称
        /// </summary>
        [JsonProperty("nameEn")]
        public string NameEn { get; set; }
    }
}
