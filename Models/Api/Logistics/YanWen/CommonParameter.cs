﻿using ERP.Models.Api.Logistics.YanWen.Enum;
using Newtonsoft.Json;
using static Org.BouncyCastle.Bcpg.Attr.ImageAttrib;

namespace ERP.Models.Api.Logistics.YanWen
{
    public class CommonParameter: RequestLogistic
    {
        public CommonParameter()
        {

        }
        public CommonParameter(Parameter p, ServicePoint service)
        {
            Userid = p.Userid;
            ApiToken = p.ApiToken;           
            servicePoint = service;
            Timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds();//.ToUnixTimeSeconds();
            MethodName = service.GetDescription();
           
        }
        /// <summary>
        /// 客户号(必填)
        /// </summary>        
        [JsonIgnore]
        [JsonProperty("user_id")]
        public string Userid { get; set; } = string.Empty;

        [JsonIgnore]
        [JsonProperty("format")]
        public string Format = "json";

        [JsonIgnore]
        [JsonProperty("method")]
        public string MethodName = "";

        [JsonIgnore]
        [JsonProperty("timestamp")]        
        public long Timestamp { get; set; }

        [JsonIgnore]
        [JsonProperty("version")]
        public string Version = "V1.0";

        [JsonIgnore]
        public string ApiToken { get; set; } = string.Empty;

        [JsonIgnore]
        public string Account { get; set; } = string.Empty;

        [JsonIgnore]
        public ServicePoint servicePoint;

        [JsonIgnore]
        public string Sign { get; set; } = string.Empty;

        [JsonIgnore]
        /// <summary>
        /// 1 正式环境，0 测试环境
        /// </summary>
        private int EnviremnetFlag = 1;

        //正式环境：Https://open.yw56.com.cn 

        //测试环境：Https://ejf-fat.yw56.com.cn

        [JsonIgnore]
        public string HostUrl
        {
            get
            {
                if (EnviremnetFlag == 1)
                {
                    return "Https://open.yw56.com.cn";
                }
                else
                {
                    return "Https://ejf-fat.yw56.com.cn";
                }
            }
        }
             

        /// <summary>
        /// MD5(apitoken+user_id+data+format+method+timestamp+version+apitoken)
        /// </summary>
        /// <param name="data"></param>
        public string GetSign(string data)
        {            
            var text = $"{ApiToken}{Userid}{data}{Format}{servicePoint.GetDescription()}{Timestamp}{Version}{ApiToken}";
            return Helpers.Md5Lower(text);
        }       
    }  
}
