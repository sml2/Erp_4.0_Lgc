﻿using ERP.Models.Api.Logistics.Extension;
using ERP.Models.Api.Logistics.YanWen.Enum;
using Newtonsoft.Json;

namespace ERP.Models.Api.Logistics.YanWen
{
    public class ShipTypeRequest : CommonParameter
    {
        public ShipTypeRequest(Parameter p) : base(p, ServicePoint.channelGetlist)
        {
        }

        //正式环境：Https://open.yw56.com.cn 

        //测试环境：Https://ejf-fat.yw56.com.cn
        [JsonIgnore]
        public override string Url => $"{base.HostUrl}/api/order?user_id={base.Userid}&method={base.MethodName}&format={base.Format}&timestamp={base.Timestamp}&sign={base.GetSign(this.ToPayload())}&version={base.Version}";


    }
}
