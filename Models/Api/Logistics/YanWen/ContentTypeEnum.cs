﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static EnumExtension;

namespace ERP.Models.Api.Logistics.YanWen
{
    public enum ContentTypeEnum
    {
        [Description("A4运单")]
        A4L,
        [Description("A4运单+拣货单")]
        A4LI,
        [Description("A4运单+报关签条")]
        A4LC,
        [Description("A4运单+报关签条+拣货单")]
        A4LCI,
        [Description("A6运单")]
        A6L,
        [Description("A6运单+拣货单")]
        A6LI,
        [Description("A6运单+报关签条")]
        A6LC,
        [Description("A6运单+报关签条+拣货单")]
        A6LCI,
        [Description("A10*10运单")]
        A10x10L,
        [Description("A10*10运单+拣货单")]
        A10x10LI,
        [Description("A10*10运单+报关签条")]
        A10x10LC,
        [Description("A10*10运单+报关签条+拣货单")]
        A10x10LCI
    }
}
