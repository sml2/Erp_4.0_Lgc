﻿
using ERP.Attributes.Logistics;

namespace ERP.Models.Api.Logistics.YanWen;

public class Parameter: BaseParam
{
    private string id;
    private string token;
    [ViewConfigParameter(nameof(Userid), Sort = 0)]
    public string Userid { get { return id; } set { id = value.Trim(); } } 
    [ViewConfigParameter(nameof(ApiToken), Sort = 1)]
    public string ApiToken { get { return token; } set { token = value.Trim(); } } 
    //[ViewConfigParameter(nameof(Account), Sort = 2)]
    //public string Account { get; set; } = string.Empty;
    //public string Password { get; set; }
}
