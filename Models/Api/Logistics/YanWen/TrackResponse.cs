﻿using ERP.Models.Api.Logistics.Extension;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ERP.Models.Api.Logistics.YanWen
{
    public class TrackResponse
    {

        /// <summary>
        /// int 0 is successful, others fail
        /// </summary>
        public int code { get; set; }

        public string message { get; set; } = string.Empty;

        public List<ResultItem> result { get; set; } = new();
        public class ResultItem
        {
            [JsonProperty("tracking_number")]
            public string TrackingNumber { get; set; } = string.Empty;

            /// <summary>
            /// 运单号
            /// </summary>
            [JsonProperty("waybill_number")]
            public string WaybillNumber { get; set; } = string.Empty;

            /// <summary>
            /// 转单号
            /// </summary>
            [JsonProperty("exchange_number")]
            public string ExchangeNumber { get; set; } = string.Empty;

            [JsonProperty("tracking_status")]
            public string TrackingStatus  { get; set; } = string.Empty;

            [JsonProperty("LastMileTrackingExpected")]
            public bool last_mile_tracking_expected { get; set; }

            [JsonProperty("elapsedMilliseconds")]
            public int ElapsedMilliseconds { get; set; }

            [JsonProperty("checkpoints")]
            public List<CheckpointsItem> Checkpoints { get; set; } = new();
            public class CheckpointsItem
            {
                [JsonProperty("time_stamp")]
                public string TimeStamp { get; set; } = string.Empty;

                [JsonProperty("time_zone")]
                public string TimeZone { get; set; } = string.Empty;

                [JsonProperty("tracking_status")]
                public string TrackingStatus { get; set; } = string.Empty;

                [JsonProperty("message")]
                public string Message { get; set; } = string.Empty;

                [JsonProperty("location")]
                public string Location { get; set; } = string.Empty;
            }
        }
    }


    public enum Track
    {
        /// <summary>
        /// Order processed by shipper
        /// </summary>
        [Description("EJF 下单/平台发送订单")]
        OR10,
        /// <summary>
        /// Yanwen Pickup scan
        /// </summary>
        [Description("燕文揽收")]
        PU10,
        /// <summary>
        /// Processing information input
        /// </summary>
        [Description("录入成功")]
        SC10,
        /// <summary>
        ///  Yanwen facility - Outbound
        /// </summary>
        [Description("口岸仓发运")]
        SC20,
        /// <summary>
        /// Package return, Re-packing
        /// </summary>
        [Description("退件组包")]
        SC30,
        /// <summary>
        /// Package return, Delivered with POD
        /// </summary>
        [Description("退件签收成功")]
        SC36,
        /// <summary>
        /// Package return, Delivery failed without POD
        /// </summary>
        [Description("退件签收失败")]
        SC37,
        /// <summary>
        /// Destination country carrier tracking number
        /// </summary>
        [Description("转单号")]
        SC40,
        /// <summary>
        ///  Pre-shipment info sent to carrier
        /// </summary>
        [Description("预上网")]
        SC45,
        /// <summary>
        /// Customs declaration EDI - Export
        /// </summary>
        [Description("提交出口报关信息")]
        EC10,
        /// <summary>
        /// International shipment release - Export
        /// </summary>
        [Description("中港报关/国内报关")]
        EC20,
        /// <summary>
        /// Custom clearance failed - Export
        /// </summary>
        [Description("中港报关/国内报关-异常件")]
        EC30,
        /// <summary>
        ///  Port of departure - Received by carrier
        /// </summary>
        [Description("航空/班轮/铁路公司接收（交航/交运）")]
        LH10,
        /// <summary>
        ///  Port of departure - Arrival
        /// </summary>
        [Description("到达运输始发点（机场/港口/火车站）")]
        LH11,
        /// <summary>
        ///  Port of departure - Departure
        /// </summary>
        [Description("离开运输始发点（机场/港口/火车站）")]
        LH20,
        /// <summary>
        /// In transit - Arrival
        /// </summary>
        [Description("到达运输中转点（可重复使用）")]
        LH21,
        /// <summary>
        ///  In transit - Departure
        /// </summary>
        [Description("离开运输中转点（可重复使用）")]
        LH22,
        /// <summary>
        /// Port of destination - Arrival
        /// </summary>
        [Description("到达运输终点（机场/港口/火车站）")]
        LH30,
        /// <summary>
        /// Arrived at international hub
        /// </summary>
        [Description("抵达目的地仓库")]
        LH33,
        /// <summary>
        /// Pick up by local carrier at destination port
        /// </summary>
        [Description("目的港提货")]
        LH38,
        /// <summary>
        ///  Batch delivery to carrier
        /// </summary>
        [Description("EP 批次交货（国内或国外）")]
        LH40,
        /// <summary>
        ///  Port of destination - Departure
        /// </summary>
        [Description("离开运输终点（机场/港口/火车站）")]
        LH45,
        /// <summary>
        /// Custom Clearance in process - Import
        /// </summary>
        [Description("开始进口清关")]
        IC50,
        /// <summary>
        ///  International shipment release - Import
        /// </summary>
        [Description("进口清关完成")]
        IC60,
        /// <summary>
        /// Custom clearance failed - Import
        /// </summary>
        [Description("进口清关失败")]
        IC70,
        /// <summary>
        ///  Package data received
        /// </summary>
        [Description("EP 小件操作扫描（上网）")]
        LM03,
        /// <summary>
        /// Destination Country - Arrival
        /// </summary>
        [Description("到达目的国")]
        LM10,
        /// <summary>
        ///  Distribution center - Outbound
        /// </summary>
        [Description("离开目的国派送处理中心")]
        LM11,
        /// <summary>
        /// In transit - Inbound
        /// </summary>
        [Description("到达目的国中转中心（可重复使用）")]
        LM12,
        /// <summary>
        /// In transit - Outbound
        /// </summary>
        [Description("离开目的国中转中心（可重复使用）")]
        LM13,
        /// <summary>
        /// In transit to next facility
        /// </summary>
        [Description("目的国国内中转")]
        LM15,
        /// <summary>
        /// Destination Scan
        /// </summary>
        [Description("到达目的国最后派送点")]
        LM20,
        /// <summary>
        /// Out for delivery
        /// </summary>
        [Description("离开目的国最后派送点")]
        LM25,
        /// <summary>
        /// Available for pick up
        /// </summary>
        [Description("到达待取")]
        LM30,
        /// <summary>
        /// Attempted delivery
        /// </summary>
        [Description("尝试投递")]
        LM35,
        /// <summary>
        /// DELIVERED
        /// </summary>
        [Description("派送成功/妥投")]
        LM40,
        /// <summary>
        /// Undelivered
        /// </summary>
        [Description("妥投失败")]
        LM50,
        /// <summary>
        /// Delivery extension
        /// </summary>
        [Description("派送延期")]
        LM60,
        /// <summary>
        /// Consignee information is required
        /// </summary>
        [Description("无法交付，需要进一步确认收件人信息")]
        LM70,
        /// <summary>
        /// Consignee refused to sign for the package
        /// </summary>
        [Description("包裹拒收，收件人拒绝签收")]
        LM75,
        /// <summary>
        /// Consignee refused to sign for the package 
        /// </summary>
        [Description("包裹拒收，收件人拒绝签收")]
        LM85,
        /// <summary>
        /// Package returned
        /// </summary>
        [Description("包裹退回")]
        LM90,
        /// <summary>
        /// OTHER
        /// </summary>
        [Description("其他信息")]
        OTHER,
    }
}