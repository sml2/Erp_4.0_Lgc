﻿using ERP.Models.Api.Logistics.Extension;
using ERP.Models.Api.Logistics.YanWen.Enum;
using Newtonsoft.Json;

namespace ERP.Models.Api.Logistics.YanWen
{
    public class PrintRequest : CommonParameter
    {
        public PrintRequest(Parameter p,string waybillNumber) : base(p,ServicePoint.getLabel)
        {
            WaybillNumber = waybillNumber;           
        }

        /// <summary>
        /// 运单号 必填
        /// </summary>
        [JsonProperty("waybillNumber")]
        public string WaybillNumber { get; set; }

        /// <summary>
        ///  Integer 否 打印拣货单 1:是; 0:否
        /// </summary>
        public int printRemark { get; set; } = 0;

        //正式环境：Https://open.yw56.com.cn 

        //测试环境：Https://ejf-fat.yw56.com.cn
        [JsonIgnore]
        public override string Url => $"{base.HostUrl}/api/order?user_id={base.Userid}&method={base.MethodName}&format={base.Format}&timestamp={base.Timestamp}&sign={base.GetSign(this.ToPayload())}&version={base.Version}";

    }
}
