﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.YanWen
{
    public class ExtParameter: BaseExtParam
    {                           
        /// <summary>
        /// 货运方式
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货运方式")]
        public string ShipMethod { get; set; } = string.Empty;


        /// <summary>
        ///  goodsNameCh String 是 200 中文品名
        /// </summary>       
        [StringLength(200, ErrorMessage = "中文品名最大长度不能超过200个字符")]
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货品申报名(中文)")]
        public string GoodsNameCn { get; set; } = string.Empty;


        /// <summary>
        /// goodsNameEn String 是 200 英文品名
        /// </summary>      
        [StringLength(200, ErrorMessage = "英文品名最大长度不能超过200个字符")]
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("货品申报名(英文)")]
        public string GoodsNameEn { get; set; } = string.Empty;


        /// <summary>
        /// ioss String 50 IOSS 税号
        /// </summary>        
        [StringLength(50, ErrorMessage = "税号最大长度不能超过50个字符")]
        public string? IOSS { get; set; }


        /// <summary>
        /// hscode String 50 海关编码
        /// </summary>       
        [StringLength(50, ErrorMessage = "海关编码最大长度不能超过50个字符")]
        public string? HsCode { get; set; } = string.Empty;
      
        /// <summary>
        /// 申报总数量
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包装盒数量")]
        public int BoxCount { get; set; } = 1;

        /// <summary>
        /// 包裹重量(kg)
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("包裹重量(g)")]
        public int Weight { get; set; }


        /// <summary>
        /// currency String 是 10 币种代码；传 USD, EUR, GBP, CNY, AUD, CAD;
        /// </summary>       
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("申报货币")]
        [StringLength(10, ErrorMessage = "币种代码最大长度不能超过10个字符")]
        public string Currency { get; set; } = string.Empty;

        /// <summary>
        /// 申报总价格
        /// </summary>
        [Required(ErrorMessage = "{0} 不能为空")]
        [DisplayName("申报总价格")]
        [Range(typeof(decimal), "0.01", "9999999999999999.99", ErrorMessage = "申报总价值不能超出[0.01-9999999999999999.99]")]
        public decimal Declare { get; set; }


        /// <summary>
        /// 包裹宽(cm)
        /// </summary>       
        [DisplayName("包裹宽(cm)")]
        public int? Width { get; set; }

        /// <summary>
        /// 包裹长(cm)
        /// </summary>       
        [DisplayName("包裹长(cm)")]
        public int? Length { get; set; }

        /// <summary>
        /// 包裹高(cm)
        /// </summary>       
        [DisplayName("包裹高(cm)")]
        public int? Height { get; set; }


       
        /// <summary>
        /// taxNumber String 50 收件人税号
        /// </summary>       
        [StringLength(50, ErrorMessage = "收件人税号最大长度不能超过50个字符")]
        [DisplayName("收件人税号")]
        public string? TaxNumber { get; set; }

       
        /// <summary>
        /// RetaxNumber String 50 收件人税号
        /// </summary>       
        [StringLength(50, ErrorMessage = "寄件人税号最大长度不能超过50个字符")]
        [DisplayName("寄件人税号")]
        public string? RetaxNumber { get; set; }


        /// <summary>
        /// hasBattery Integer 是 是否带电 1:是 0:否
        /// </summary>       
        [Required]
        public int HasBattery { get; set; } = 0;



    }
}
