﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.YanWen
{
    public class EstimatePriceResponse
    {
        public Result result { get; set; } = default!;
        public class Result
        {
            public List<string> unmatchedInformation { get; set; } = new();
            public List<ItemsItem> items { get; set; } = new();
            public class ItemsItem
            {
                public string priceCode { get; set; } = string.Empty;
                public string productCode { get; set; } = string.Empty;
                public string rountingCode { get; set; } = string.Empty;
                public List<ExpenseItemsItem> expenseItems { get; set; } = new();
                public class ExpenseItemsItem
                {
                    public bool isReturn { get; set; }
                    public double money { get; set; }
                    public double moneyOfOriginal { get; set; }
                    public int calcType { get; set; }
                    public bool isDiscount { get; set; }
                    public string expenseItemCode { get; set; } = string.Empty;
                    /// <summary>
                    /// 单件处理费
                    /// </summary>
                    public string expenseItemName { get; set; } = string.Empty;
                    public bool isRebate { get; set; }
                    public double cnyMoney { get; set; }
                }
                public bool discountFlag { get; set; }
                public double throwWeightFactor { get; set; }
                public double productFuelRate { get; set; }
                public bool isThrowweight { get; set; }
                public string remark { get; set; } = string.Empty;
                public string priceUdo { get; set; } = string.Empty;
                public ProductInfo productInfo { get; set; } = new();
                public class ProductInfo
                {
                    public bool calcGweight { get; set; }
                    public int calcGweightRate { get; set; }
                    public bool cpFlag { get; set; }
                    public string desc { get; set; } = string.Empty;
                    public double fuelRate { get; set; }
                    public int level1Catalog { get; set; }
                    public string productcode { get; set; } = string.Empty;
                    /// <summary>
                    /// 香港DHL
                    /// </summary>
                    public string productname { get; set; } = string.Empty;
                    public string quoteType { get; set; } = string.Empty;
                    /// <summary>
                    /// 专线/快递
                    /// </summary>
                    public string saleProductTypeName { get; set; } = string.Empty;
                    public bool track { get; set; }
                }
            }
            public int itemCount { get; set; }
        }
        public int code { get; set; }
        public string appId { get; set; } = string.Empty;
        public string requestParam { get; set; } = string.Empty;
    }
}
