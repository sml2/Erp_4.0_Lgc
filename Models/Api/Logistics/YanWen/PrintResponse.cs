﻿using Newtonsoft.Json;

namespace ERP.Models.Api.Logistics.YanWen
{
    public class PrintResponse
    {
        /// <summary>
        /// waybillNumber String 运单号
        /// </summary>
        [JsonProperty("waybillNumber")]
        public string WaybillNumber { get; set; }

        /// <summary>
        /// 是否成功
        /// </summary>
        [JsonProperty("isSuccess")]
        public bool IsSuccess { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        [JsonProperty("errorMsg")]
        public string ErrorMsg { get; set; }

        /// <summary>
        /// 文件 base64 字符
        /// </summary>
        [JsonProperty("base64String")]
        public string Base64String { get; set; }

        /// <summary>
        /// 标签尺寸类型
        /// </summary>
        [JsonProperty("labelType")]
        public string LabelType { get; set; }          
    }
}
