﻿using ERP.Models.Api.Logistics.Extension;
using ERP.Models.Api.Logistics.YanWen.Enum;
using Microsoft.CodeAnalysis.VisualBasic.Syntax;

namespace ERP.Models.Api.Logistics.YanWen
{
    public class TrackRequest : CommonParameter
    {
        public TrackRequest(Parameter p,string logNumbers) : base(p, ServicePoint.orderGetlist)
        {
            nums = logNumbers;
        }      
        public override string Url => "http://api.track.yw56.com.cn/api/tracking?nums=" + nums;

        public string nums { get; set; }

        public override HttpMethod Method => HttpMethod.Get;

        public override SerializableDictionary<string, string> headers
        {
            get
            {
                SerializableDictionary<string, string> header = new SerializableDictionary<string, string>();
                header.Add("Authorization", base.Userid);
                return header;
            }
        }
    }
}
