﻿using Newtonsoft.Json;


namespace ERP.Models.Api.Logistics.YanWen
{
    public class CommonResponse<T>
    {
        /// <summary>
        /// 是否成功 true:成功; false:失败;
        /// </summary>
        [JsonProperty("success")]
        public bool Success { get; set; }

        /// <summary>
        /// 消息编码 0:成功; >0:失败; <0:系统异常;
        /// </summary>
        [JsonProperty("code")]
        public string Code { get; set; }

        /// <summary>
        /// 消息内容
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }

        /// <summary>
        /// 实体信息 接口返回的数据对象
        /// </summary>
        [JsonProperty("data")]
        public T Data { get; set; }
    }

    public class CommonResponse
    {
        /// <summary>
        /// 是否成功 true:成功; false:失败;
        /// </summary>
        [JsonProperty("success")]
        public bool Success { get; set; }

        /// <summary>
        /// 消息编码 0:成功; >0:失败; <0:系统异常;
        /// </summary>
        [JsonProperty("code")]
        public string Code { get; set; }

        /// <summary>
        /// 消息内容
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }        
    }
}
