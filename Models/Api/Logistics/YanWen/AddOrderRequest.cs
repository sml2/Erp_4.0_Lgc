using Aspose.Cells;
using ERP.Models.Api.Logistics.YanWen.Enum;
using ERP.Models.DB.Orders;
using ERP.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ERP.Models.Api.Logistics.YanWen
{
    public class AddOrderRequest: CommonParameter
    {
        public AddOrderRequest()
        {

        }       
        public AddOrderRequest(Parameter p, SenderInfo senderInfo, OrderInfo orderInfo, ExtParameter Ext) : base(p, ServicePoint.create)
        {           
            Id = orderInfo.id;
            orderNo = orderInfo.OrderNo;

            //channelId String 是 10 产品编号(产品 id)
            ChannelId = Ext.ShipMethod;
            // orderSource String 20 订单来源
            //orderNumber String 是 50 订单号
            OrderNumber = Helpers.GetOrderNo(orderInfo.OrderNo, orderInfo.SendNum, orderInfo.FailNum);// orderInfo.OrderNo;
            //dateOfReceipt Date 收款到账日期 格式 yyyy - MM - dd
            //remark String 200 拣货单信息（打印标签选择打印拣货单显示此字段信息）

            parcelInfo = new ParcelInfo();
            //hasBattery Integer 是 是否带电 1:是 0:否
            parcelInfo.HasBattery = Ext.HasBattery;  
            //currency String 是 10 币种代码；传 USD, EUR, GBP, CNY, AUD, CAD;
            parcelInfo.Currency = Ext.Currency; 
            //totalPrice Decimal 是(18, 2) 申报总价值
            parcelInfo.TotalPrice = Ext.Declare; 
            //totalQuantity Integer 是 申报总数量
            parcelInfo.TotalQuantity = Ext.BoxCount;
            //totalWeight Integer 是 总重量(单位: g)
            parcelInfo.TotalWeight = Ext.Weight; 
            //height Integer 包裹高(单位: cm)
            parcelInfo.Height = Ext.Height;  
            //width Integer 包裹宽(单位: cm)
            parcelInfo.Width = Ext.Width;  
            //length Integer 包裹长(单位: cm)
            parcelInfo.Length = Ext.Length;  
            //ioss String 50 IOSS 税号
            parcelInfo.Ioss = Ext.IOSS;

            Receiver = new AddOrderRequest.Cls_Receiver();
            //name String 是 50 收件人姓名
            Receiver.Name = orderInfo.ReceiverAddress.Name;
            //phone String 50 收件人电话
            Receiver.Phone = orderInfo.ReceiverAddress.Phone;
            //email String 100 收件人邮箱
            Receiver.Email = orderInfo.ReceiverAddress?.Email ?? "";
            //company String 100 收件人公司
            //country String 是 10 目的国 id 或目的国二字码
            Receiver.Country = orderInfo.ReceiverAddress!.NationShort;
            //state String 50 收件人州(省)
            Receiver.State = orderInfo.ReceiverAddress!.Province;
            //city String 50 收件人城市
            Receiver.City = orderInfo.ReceiverAddress.City;
            //zipCode String 50 邮编
            Receiver.ZipCode = orderInfo.ReceiverAddress?.Zip;
            //houseNumber String 50 收件人门牌号
            //address String 是 200 收件人地址
            Receiver.Address = $"{orderInfo.ReceiverAddress.Address}{orderInfo.ReceiverAddress.Address2}{orderInfo.ReceiverAddress.Address3}";
            //taxNumber String 50 收件人税号
            Receiver.TaxNumber = Ext.RetaxNumber;

            Sender = new AddOrderRequest.Cls_Sender();
            //name String 50 发件人姓名
            Sender.Name = senderInfo.Name;
            //phone String 50 发件人电话
            Sender.Phone = senderInfo.PhoneNumber;
            //company String 100 发件人公司
            Sender.Company = senderInfo.Company;
            //email String 100 发件人邮箱
            Sender.Email = senderInfo.Mail;
            //country String 10 发件人国家 CN           
            //state String 50 发件人州(省)
            Sender.State = senderInfo.Province;
            //city String 50 发件人城市
            Sender.City = senderInfo.City;
            //zipCode String 50 发件人邮编
            Sender.ZipCode = senderInfo.PostalCode;
            //houseNumber String 50 发件人门牌号
            //address String 200 发件人地址
            Sender.Address = senderInfo.Address;
            //taxNumber String 50 发件人税号
            Sender.TaxNumber= Ext.TaxNumber;

            var products = new List<Productinfo>();
            int num = orderInfo.GoodInfos.Sum((r) => r.Num);
            if (num <= 0)
            {
                throw new AggregateException("The purchase quantity must be greater than 0");
            }
            int i = 0;
            foreach (var v in orderInfo.GoodInfos)
            {
                if (i >= 5) { throw new AggregateException(" yanwen Productinfo maxlength exceeded"); }
                Productinfo good = new Productinfo();
                //goodsNameCh String 是 200 中文品名
                good.GoodsNameCh = Ext.GoodsNameCn;
                //goodsNameEn String 是 200 英文品名
                good.GoodsNameEn = Ext.GoodsNameEn;
                //price Decimal 是(18,2) 申报单价
                good.Price = Helpers.RoundData(Ext.Declare / num);
                //quantity Integer 是 数量
                good.Quantity = v.Count;
                //weight Integer 是 单件重量(单位: g)
                good.Weight =Convert.ToInt32(Ext.Weight / num);
                //hscode String 50 海关编码
                good.Hscode = Ext.HsCode;
                //url String 2000 商品链接
                //material String 500 商品材质                                                                                        
                products.Add(good);
                i++;
            }
            parcelInfo.products = products;
        }


        //正式环境：Https://open.yw56.com.cn 

        //测试环境：Https://ejf-fat.yw56.com.cn
        [JsonIgnore]
        public override string Url => $"{base.HostUrl}/api/order?user_id={base.Userid}&method={base.MethodName}&format={base.Format}&timestamp={base.Timestamp}&sign={base.GetSign(this.ToPayload())}&version={base.Version}";




        [JsonIgnore]
        public int Id { get; set; }

        [JsonIgnore]
        public string orderNo { get; set; } = string.Empty;
      
        /// <summary>
        ///  channelId String 是 10 产品编号(产品 id)
        /// </summary>
        [JsonProperty("channelId")]
        [StringLength(10, ErrorMessage = "产品编号最大长度不能超过10个字符")]
        [Required]
        public string ChannelId { get; set; } = string.Empty;

        /// <summary>
        ///orderSource String 20 订单来源
        /// </summary>
        [JsonProperty("orderSource")]
        [StringLength(20, ErrorMessage = "订单来源最大长度不能超过20个字符")]
        public string? OrderSource { get; set; } = string.Empty;


        /// <summary>
        /// orderNumber String 是 50 订单号
        /// </summary>
        [JsonProperty("orderNumber")]
        [Required]
        [StringLength(50, ErrorMessage = "运单号最大长度不能超过50个字符")]       
        public string OrderNumber { get; set; } = string.Empty;


        /// <summary>
        /// dateOfReceipt Date 收款到账日期 格式 yyyy-MM-dd
        /// </summary>
        [JsonProperty("dateOfReceipt")]       
        public DateTime? DateOfReceipt { get; set; }


        /// <summary>
        ///remark String 200 拣货单信息（打印标签选择打印拣货单显示此字段信息）
        /// </summary>
        [JsonProperty("remark")]
        [StringLength(200, ErrorMessage = "拣货单信息最大长度不能超过20个字符")]
        public string? Remark { get; set; }

              
        [JsonProperty("receiverInfo")]
        public Cls_Receiver Receiver { get; set; } = new();
        public class Cls_Receiver
        {         
            /// <summary>
            /// name String 是 50 收件人姓名
            /// </summary>
            [JsonProperty("name")]
            [StringLength(50, ErrorMessage = "收件人姓名最大长度不能超过50个字符")]
            [Required]
            public string Name { get; set; } = string.Empty;

            /// <summary>
            /// phone String 50 收件人电话
            /// </summary>
            [JsonProperty("phone")]
            [StringLength(50, ErrorMessage = "收件人电话最大长度不能超过50个字符")]      
            public string? Phone { get; set; } = string.Empty;

            /// <summary>
            /// email String 100 收件人邮箱
            /// </summary>
            [JsonProperty("email")]
            [StringLength(100, ErrorMessage = "收件人邮箱最大长度不能超过100个字符")]
            public string? Email { get; set; } = string.Empty;

            /// <summary>
            /// company String 100 收件人公司
            /// </summary>
            [JsonProperty("company")]
            [StringLength(100, ErrorMessage = "收件人公司最大长度不能超过100个字符")]
            public string? Company { get; set; } = string.Empty;

            /// <summary>
            ///country String 是 10 目的国 id 或目的国二字码
            /// </summary>
            [JsonProperty("country")]
            [StringLength(10, ErrorMessage = "目的国二字码最大长度不能超过10个字符")]
            [Required]
            public string Country { get; set; }


            /// <summary>
            /// state String 50 收件人州(省)
            /// </summary>
            [JsonProperty("state")]
            [StringLength(50, ErrorMessage = "收件人州(省)最大长度不能超过50个字符")]           
            public string? State { get; set; }


            /// <summary>
            /// city String 50 收件人城市
            /// </summary>
            [JsonProperty("city")]
            [StringLength(50, ErrorMessage = "收件人城市最大长度不能超过50个字符")]           
            public string? City { get; set; }

            /// <summary>
            /// zipCode String 50 邮编
            /// </summary>
            [JsonProperty("zipCode")]
            [StringLength(50, ErrorMessage = "邮编最大长度不能超过50个字符")]
            public string? ZipCode { get; set; }

            /// <summary>
            /// houseNumber String 50 收件人门牌号
            /// </summary>
            [JsonProperty("houseNumber")]
            [StringLength(50, ErrorMessage = "收件人门牌号最大长度不能超过50个字符")]
            public string? HouseNumber { get; set; }

            /// <summary>
            /// address String 是 200 收件人地址
            /// </summary>
            [JsonProperty("address")]
            [StringLength(200, ErrorMessage = "收件人地址最大长度不能超过200个字符")]
            public string? Address { get; set; }

            /// <summary>
            /// taxNumber String 50 收件人税号
            /// </summary>
            [JsonProperty("taxNumber")]
            [StringLength(50, ErrorMessage = "收件人税号最大长度不能超过50个字符")]
            public string? TaxNumber { get; set; }           
            
        }

        [JsonProperty("senderInfo")]
        public Cls_Sender Sender { get; set; } = new();

        public class Cls_Sender
        {
            /// <summary>
            ///  name String 50 发件人姓名
            /// </summary>
            [JsonProperty("name")]
            [StringLength(50, ErrorMessage = "发件人姓名最大长度不能超过50个字符")]
            public string? Name { get; set; }

            /// <summary>
            /// phone String 50 发件人电话
            /// </summary>
            [JsonProperty("phone")]
            [StringLength(50, ErrorMessage = "发件人电话最大长度不能超过50个字符")]
            public string? Phone { get; set; }

            /// <summary>
            /// company String 100 发件人公司
            /// </summary>
            [JsonProperty("company")]
            [StringLength(100, ErrorMessage = "发件人公司最大长度不能超过100个字符")]
            public string? Company { get; set; } = string.Empty;

            /// <summary>
            /// email String 100 发件人邮箱
            /// </summary>
            [JsonProperty("email")]
            [StringLength(100, ErrorMessage = "发件人邮箱最大长度不能超过100个字符")]
            public string? Email { get; set; } = string.Empty;

            /// <summary>
            /// country String 10 发件人国家 CN
            /// </summary>
            [JsonProperty("country")]
            [StringLength(10, ErrorMessage = "发件人国家不能超过10个字符")]          
            public string? Country { get; set; }

            /// <summary>
            /// state String 50 发件人州(省)
            /// </summary>
            [JsonProperty("state")]
            [StringLength(50, ErrorMessage = "发件人州(省)最大长度不能超过50个字符")]
            public string? State { get; set; }


            /// <summary>
            /// city String 50 发件人城市
            /// </summary>
            [JsonProperty("city")]
            [StringLength(50, ErrorMessage = "发件人城市最大长度不能超过50个字符")]
            public string? City { get; set; }

            /// <summary>
            /// zipCode String 50 发件人邮编
            /// </summary>
            [JsonProperty("zipCode")]
            [StringLength(50, ErrorMessage = "发件人邮编最大长度不能超过50个字符")]
            public string? ZipCode { get; set; }

            /// <summary>
            /// houseNumber String 50 发件人门牌号
            /// </summary>
            [JsonProperty("houseNumber")]
            [StringLength(50, ErrorMessage = "发件人门牌号最大长度不能超过50个字符")]
            public string? HouseNumber { get; set; }

            /// <summary>
            ///  address String 200 发件人地址
            /// </summary>
            [JsonProperty("address")]
            [StringLength(200, ErrorMessage = "发件人地址最大长度不能超过50个字符")]
            public string? Address { get; set; }

            /// <summary>
            /// taxNumber String 50 发件人税号
            /// </summary>
            [JsonProperty("taxNumber")]
            [StringLength(50, ErrorMessage = "发件人税号最大长度不能超过50个字符")]
            public string? TaxNumber { get; set; }
        }

        /// <summary>
        /// parcelInfo object 包裹信息
        /// </summary>
        public ParcelInfo parcelInfo { get; set; }

        public class ParcelInfo
        {
            /// <summary>
            /// productList object 商品信息（支持 5 组）
            /// </summary>
            [JsonProperty("productList")]
            public List<Productinfo> products = new();
            
            /// <summary>
            /// hasBattery Integer 是 是否带电 1:是 0:否
            /// </summary>
            [JsonProperty("hasBattery")]
            [Required]
            public int HasBattery { get; set; }

            /// <summary>
            /// currency String 是 10 币种代码；传 USD, EUR, GBP, CNY, AUD, CAD;
            /// </summary>
            [JsonProperty("currency")]
            [StringLength(10, ErrorMessage = "币种代码最大长度不能超过10个字符")]
            [Required]
            public string Currency { get; set; }

            /// <summary>
            ///  totalPrice Decimal 是(18,2) 申报总价值
            /// </summary>
            [JsonProperty("totalPrice")]
            [Range(typeof(decimal), "0.01", "9999999999999999.99", ErrorMessage = "申报总价值不能超出[0.01-9999999999999999.99]")]
            [Required]
            public decimal TotalPrice { get; set; }


            /// <summary>
            /// totalQuantity Integer 是 申报总数量
            /// </summary>
            [JsonProperty("totalQuantity")]           
            [Required]
            public int TotalQuantity { get; set; }


            /// <summary>
            /// totalWeight Integer 是 总重量(单位:g)
            /// </summary>
            [JsonProperty("totalWeight")]
            [Required]
            public int TotalWeight { get; set; }

            /// <summary>
            /// height Integer 包裹高(单位:cm)
            /// </summary>
            [JsonProperty("height")]          
            public int? Height { get; set; }

            /// <summary>
            /// width Integer 包裹宽(单位:cm)
            /// </summary>
            [JsonProperty("width")]
            public int? Width { get; set; }

            /// <summary>
            /// length Integer 包裹长(单位:cm)
            /// </summary>
            [JsonProperty("length")]
            public int? Length { get; set; }

            /// <summary>
            /// ioss String 50 IOSS 税号
            /// </summary>
            [JsonProperty("ioss")]
            [StringLength(50, ErrorMessage = "税号最大长度不能超过50个字符")]
            public string? Ioss { get; set; }
            
        }               
    }

  

    public class Productinfo
    {
      
        /// <summary>
        ///  goodsNameCh String 是 200 中文品名
        /// </summary>
        [JsonProperty("goodsNameCh")]
        [StringLength(200, ErrorMessage = "中文品名最大长度不能超过200个字符")]
        [Required]
        public string GoodsNameCh { get; set; }


        /// <summary>
        /// goodsNameEn String 是 200 英文品名
        /// </summary>
        [JsonProperty("goodsNameEn")]
        [StringLength(200, ErrorMessage = "英文品名最大长度不能超过200个字符")]
        [Required]
        public string GoodsNameEn { get; set; }

        /// <summary>
        /// price Decimal 是(18,2) 申报单价
        /// </summary>
        [JsonProperty("price")]
        [Range(typeof(decimal), "0.01", "9999999999999999.99", ErrorMessage = "申报单价范围不能超出[0.01-9999999999999999.99]")]
        [Required]
        public decimal Price { get; set; }


        /// <summary>
        /// quantity Integer 是 数量
        /// </summary>
        [JsonProperty("quantity")]
        [Required]
        public int Quantity { get; set; }

        /// <summary>
        /// weight Integer 是 单件重量(单位:g)
        /// </summary>
        [JsonProperty("weight")]
        [Required]
        public int Weight { get; set; }

        /// <summary>
        /// hscode String 50 海关编码
        /// </summary>
        [JsonProperty("hscode")]
        [StringLength(50, ErrorMessage = "海关编码最大长度不能超过50个字符")]
        public string? Hscode { get; set; }

        /// <summary>
        /// url String 2000 商品链接
        /// </summary>
        [JsonProperty("url")]
        [StringLength(2000, ErrorMessage = "商品链接最大长度不能超过2000个字符")]
        public string? Url { get; set; }

        /// <summary>
        /// material String 500 商品材质
        /// </summary>
        [JsonProperty("material")]
        [StringLength(500, ErrorMessage = "商品材质最大长度不能超过500个字符")]
        public string? Material { get; set; }

    }
}
