﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ERP.Models.Api.Logistics.YanWen
{    
    public class AddOrderResponse
    {       
        /// <summary>
        ///运单号
        ///</summary>     
        [JsonProperty("waybillNumber")]
        public string WaybillNumber { get; set; }

        /// <summary>
        ///订单号
        ///</summary>     
        [JsonProperty("orderNumber")]
        public string OrderNumber { get; set; }


        /// <summary>
        /// 转单号
        ///</summary>     
        [JsonProperty("referenceNumber")]
        public string ReferenceNumber { get; set; }                   
    }
}
