﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.Api.Logistics.YanWen
{
    public class GetFeeResponse
    {
        public double totalAmount { get; set; }

        public int total { get; set; }

        public int totalrows { get; set; }

        public List<RowsItem> rows { get; set; } = new();
        public class RowsItem
        {

            public string exchangeNumber { get; set; } = string.Empty;

            public double calcPrice { get; set; }

            public string waybillNumber { get; set; } = string.Empty;
            /// <summary>
            /// 法国
            /// </summary>
            public string regionName { get; set; } = string.Empty;

            public double discountPrice { get; set; }

            public string OrderNumber { get; set; } = string.Empty;

            public string quoteType { get; set; } = string.Empty;
            /// <summary>
            /// 中邮上海SAL大包
            /// </summary>
            public string productName { get; set; } = string.Empty;
            /// <summary>
            /// 尹彩霞
            /// </summary>
            public string merchantName { get; set; } = string.Empty;

            public double acctachPrice { get; set; }

            public string areaCode { get; set; } = string.Empty;

            public string productCode { get; set; } = string.Empty;

            public long calcTime { get; set; }
            /// <summary>
            /// 华南
            /// </summary>
            public string areaName { get; set; } = string.Empty;

            public string MerchantCode { get; set; } = string.Empty;

            public int rowNum { get; set; }

            public double arPrice { get; set; }

            public string yanwenNumber { get; set; } = string.Empty;

            public double MoneyTrunk { get; set; }

            public double calcWeight { get; set; }
        }
    }
}
