﻿using ERP.Models.Api.Logistics.YanWen.Enum;
using Newtonsoft.Json;
using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ERP.Models.Api.Logistics.YanWen
{
    public class InterceptRequest : CommonParameter
    {
        public InterceptRequest(Parameter p, string waybillNumber,string note="") :base(p,ServicePoint.cancel)
        {
            WaybillNumber = waybillNumber;
            Note = note;
        }

        /// <summary>
        /// 运单号 必填
        /// </summary>
        [JsonProperty("waybillNumber")]
        public string WaybillNumber { get; set; }


        /// <summary>
        /// 取消原因 
        /// </summary>
        [JsonProperty("note")]
        public string? Note { get; set; }
        
        //文档地址需下载 https://www.yw56.com.cn/webfile/API%E6%8E%A5%E5%8F%A3/

        //正式环境：Https://open.yw56.com.cn 

        //测试环境：Https://ejf-fat.yw56.com.cn
        [JsonIgnore]
        public override string Url => $"{base.HostUrl}/api/order?user_id={base.Userid}&method={base.MethodName}&format={base.Format}&timestamp={base.Timestamp}&sign={base.GetSign(this.ToPayload())}&version={base.Version}";


    }
}
