﻿using System.ComponentModel;


namespace ERP.Models.Api.Logistics.YanWen.Enum
{
    public enum ServicePoint
    {
        [EnumExtension.Description("common.country.getlist")]       
        /// <summary>
        /// common.country.getlist 查询国家列表
        /// </summary>
        countryGetlist = 1,
        [EnumExtension.Description("express.channel.getlist")]      
        /// <summary>
        /// express.channel.getlist 查询已开通的产品列表
        /// </summary>
        channelGetlist,
        [EnumExtension.Description("express.order.create")]
        /// <summary>
        /// express.order.create 创建运单
        /// </summary>
        create,
        [EnumExtension.Description("express.order.label.get")]
        /// <summary>
        /// express.order.label.get 打印标签
        /// </summary>
        getLabel,
        [EnumExtension.Description("express.order.cancel")]
        /// <summary>
        /// express.order.cancel 取消运单
        /// </summary>
        cancel,
        [EnumExtension.Description("express.order.get")]
        /// <summary>
        /// express.order.get 查询运单详情
        /// </summary>
        get,
        [EnumExtension.Description("express.order.getlist")]
        /// <summary>
        /// express.order.getlist 批量查询单号详情（订单号/运单号）
        /// </summary>
        orderGetlist,

    }
}
