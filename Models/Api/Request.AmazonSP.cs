﻿
using Newtonsoft.Json;
using OrderSDK.Modles.Amazon.Enums;

namespace ERP.Models.Api
{
    public class RequestAmazonSP : RequestBase
    {        
        /// <summary>
        /// Query 参数
        /// </summary>      
        [JsonIgnore]
        public virtual bool QueryRequest { get; set; } = false;
        
        /// <summary>
        /// Query 参数
        /// </summary>      
        [JsonIgnore]
        public virtual bool BodyRequest { get; set; } = false;

        [JsonIgnore]
        public virtual RateLimitType RateLimitType { get; set; } = RateLimitType.UNSET;
      
        [JsonIgnore]
        public virtual Dictionary<string, string> Headers { get; set; } = new();      
       
        [JsonIgnore]
        public virtual TokenDataType TokenDataType { get; set; } = TokenDataType.Normal;     
                     
        public virtual string ToPayload()
        {
            string json = JsonConvert.SerializeObject(this);
            return json;
        }

        public Dictionary<string, string> ToQueryString()
        {
           return ConvertHelper.getParameters(this);
        }
    }
}
