namespace ERP.Models.Biter;

public class BaseBiter
{
    protected void SetFlag(long column, long flag, bool value)
    {
        if (value)
        {
            column |= flag;
        }
        else
        {
            column &= ~flag;
        }
    }

    protected bool IsFlagSet(long column, long flag) => (column & flag) == flag;

    protected long? GetInt(long value, long mask, int bit) => (value & mask) >> bit;

    protected void SetInt(long column, long value, long mask, int bit) =>
        column = (column & (~(long)mask)) ^ (value << (int)bit);
}