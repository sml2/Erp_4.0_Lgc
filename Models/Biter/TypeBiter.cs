using ERP.Enums.Product;
using ERP.Services.Caches;

namespace ERP.Models.Biter;

public class TypeBiter : BaseBiter
{
    protected long _flag;

    public TypeBiter()
    {
        _flag = (long)Types.Default;
    }

    public TypeBiter(long flag)
    {
        _flag = flag;
    }

    // 未审核
    public const int AUDIT_UNREVIEWED = 0;

    // 已审核
    public const int AUDIT_AUDITED = 2;

    // 侵权
    public const int AUDIT_INFRINGEMENT = 3;

    #region Type_Column

    public void SetCollect(bool value) => SetFlag(_flag, (long)Types.Collect, value);
    public bool IsCollect() => IsFlagSet(_flag, (long)Types.Collect);


    public void SetFromShare(bool value) => SetFlag(_flag, (long)Types.FromShare, value);
    public void IsFromShare() => IsFlagSet(_flag, (long)Types.FromShare);

    public void SetShare(bool value) => SetFlag(_flag, (long)Types.Share, value);
    public bool IsShare() => IsFlagSet(_flag, (long)Types.Share);


    public void SetDistribution(bool value) => SetFlag(_flag, (long)Types.Distribution, value);
    public bool IsDistribution() => IsFlagSet(_flag, (long)Types.Distribution);

    /**
     * const FLAG_IMGSYNC = 16;                                // BIT #05 of $flags has the value 16
    public function hasImageSync():bool{return $this->isFlagSet(self::FLAG_IMGSYNC);}
    public function setImageSync(bool $value){$this->setFlag(self::FLAG_IMGSYNC, $value);}
     */
    public void SetEdited(bool value) => SetFlag(_flag, (long)Types.Edited, value);

    public bool IsEdited() => IsFlagSet(_flag, (long)Types.Edited);

    public void SetRecycle(bool value) => SetFlag(_flag, (long)Types.Recycle, value);
    public bool IsRecycle() => IsFlagSet(_flag, (long)Types.Recycle);

    public void SetLock(bool value) => SetFlag(_flag, (long)Types.Lock, value);
    public bool IsLock() => IsFlagSet(_flag, (long)Types.Lock);

    public void SetRepeat(bool value) => SetFlag(_flag, (long)Types.Repeat, value);
    public bool IsRepeat() => IsFlagSet(_flag, (long)Types.Repeat);


    public void SetOngoing(bool value) => SetFlag(_flag, (long)Types.OnGoing, value);
    public bool IsOngoing() => IsFlagSet(_flag, (long)Types.OnGoing);

    public void SetUpload(bool value) => SetFlag(_flag, (long)Types.Uplaod, value);
    public bool IsUpload() => IsFlagSet(_flag, (long)Types.Uplaod);

    public void SetExport(bool value) => SetFlag(_flag, (long)Types.Export, value);
    public bool IsExport() => IsFlagSet(_flag, (long)Types.Export);

    public void SetAmazonProductFlag(bool value) => SetFlag(_flag, (long)Types.AmazonProduct, value);
    public bool IsAmazonProductFlag(bool value) => IsFlagSet(_flag, (long)Types.AmazonProduct);

    public void SetB2C(bool value) => SetFlag(_flag, (long)Types.B2C, value);
    public bool IsB2C() => IsFlagSet(_flag, (long)Types.B2C);

    public void SetB2CRelease(bool value) => SetFlag(_flag, (long)Types.B2CRelease, value);
    public bool IsB2CRelease() => IsFlagSet(_flag, (long)Types.B2CRelease);

    public void SetAudit(int value) => SetInt(_flag, value, (long)Types.MASK_AUDIT, (int)Types.Audit);
    public long? GetAudit() => GetInt(_flag, (long)Types.MASK_AUDIT, (int)Types.Audit);

    #endregion

    public long GetFlag()
    {
        return _flag;
    }
}