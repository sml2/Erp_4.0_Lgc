using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
namespace ERP.Models.Setting;
using Models.Abstract;
using Extensions;
using Enums.ID;

//[Index(nameof(Short))]
//public class Nation : BaseModel
//{
//    public Nation() { }
//    public Nation(int iD, string name, string @short, string sfc)
//    {
//        ID = iD;
//        Name = name;
//        Short = @short;
//        Sfc = sfc;
//    }
//    public Nation(Nations nation, string sfc) : this((int)nation,  nation.GetDescription(), nation.ToString(), sfc) { }

//    [Comment("名称")]
//    public string Name { get; set; } = string.Empty;

//    [Comment("简称")]
//    public string Short { get; set; } = string.Empty;


//    // 搬到 platformData
//    [Comment("三态速递提供的国家英文名称")]
//    public string Sfc { get; set; } = string.Empty;
//    public override string ToString()
//    {
//        return $"{Name}[{Short}]";
//    }
//}