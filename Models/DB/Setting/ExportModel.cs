using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

//namespace ERP.Models.DB.Setting
//{
//    // 导出表模板
//    [Table("export_excel")]
//    public class ExportModel : BaseModel
//    {

//        [Comment("导出excel表 名称")]
//        public string Name { get; set; }=string.Empty;

//        [Comment("导出excel表 标识")]
//        public int Sign { get; set; } = 1;

//        [Comment("导出excel表 vue组件名称")]
//        public string Filename { get; set; } = string.Empty;

//        [Comment("排序字段")]
//        public int Sort { get; set; } = 0;

//        [Comment("备注")]
//        public string? Remark { get; set; }

//        [Comment("平台id")]
//        public int? PlatformId { get; set; }
//        public Platform? Platforms { get; set; }

//        [Comment("状态1启用2禁用")]
//        public StateEnum State { get; set; }

//        [Comment("导出类型1产品")]
//        public TypeEnum Type { get; set; } = TypeEnum.Porduct;

//        [Comment("性质1公共2私人")]
//        public QualityEnum Quality { get; set; } = QualityEnum.Public;

//        public enum QualityEnum
//        {
//            [Description("公共")]
//            Public = 1,
//            [Description("私人")]
//            Private = 2
//        }

//        public enum TypeEnum
//        {
//            Porduct = 1
//        }
//    }
//}


//------------------------------
//--Table structure for erp3_export_excel
//------------------------------
//DROP TABLE IF EXISTS `erp3_export_excel`;
//CREATE TABLE `erp3_export_excel`  (
//  `id` bigint unsigned NOT NULL,
//  `d_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '导出excel表 名称',
//  `d_sign` int(0) NOT NULL DEFAULT 1 COMMENT '导出excel表 标识',
//  `d_filename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '导出excel表 vue组件名称',
//  `d_sort` int(0) NOT NULL DEFAULT 0 COMMENT '排序字段',
//  `d_remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '备注信息',
//  `i_platform_id` bigint unsigned NOT NULL COMMENT '平台id',
//  `t_state` tinyint unsigned NOT NULL COMMENT '状态1启用2禁用',
//  `t_type` tinyint unsigned NOT NULL COMMENT '导出类型1产品',
//  `t_quality` tinyint unsigned NOT NULL COMMENT '性质1公共2私人',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
// PRIMARY KEY(`id`) USING BTREE,
// INDEX `platform_id`(`i_platform_id`) USING BTREE,
// INDEX `quality`(`t_quality`) USING BTREE,
// INDEX `sort`(`d_sort`) USING BTREE,
// INDEX `state`(`t_state`) USING BTREE,
// INDEX `type`(`t_type`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 63 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '数据字典_导出表模板' ROW_FORMAT = Dynamic;