using ERP.Enums;
using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ERP.Extensions;
using NPOI.SS.Formula.Functions;

//语种
//namespace ERP.Models.Setting
//{
//    [Table("language"), Index(nameof(Code))]
//    public class Language : BaseModel
//    {
//        public Language()
//        {
//        }

//        public Language(Languages m)
//        {
//            var now = new DateTime(2021, 11, 19, 13, 38, 0);
//            ID = (int)Math.Log2((long)m) + 1;
//            Name = m.GetDescriptionByKey("Name");
//            Code = m.GetDescriptionByKey("Code");
//            Enum = "1";
//            Flag = m;
//            CreatedAt = now;
//            UpdatedAt = now;
//        }

//        [Key]
//        public new int ID { get; set; }

//        [Comment("名称")]
//        public string Name { get; set; } = string.Empty;

//        [Comment("简码")]
//        public string Code { get; set; } = string.Empty;

//        [Comment("客户端枚举值")]
//        public string Enum { get; set; } = string.Empty;

//        [Comment("标志位")]
//        public Languages Flag { get; set; }
//    }
//}


//------------------------------
//--Table structure for erp3_language
//------------------------------
//DROP TABLE IF EXISTS `erp3_language`;
//CREATE TABLE `erp3_language`  (
//  `id` bigint unsigned NOT NULL,
//  `d_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '名称',
//  `d_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '简码',
//  `t_enum` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '客户端枚举值',
//  `b_flag` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '标志位',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
// PRIMARY KEY(`id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '数据字典_语言管理' ROW_FORMAT = Dynamic;