using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

//namespace ERP.Models.DB.Setting
//{
//    [Table("platform")]
//    [Comment("平台管理_平台信息")]
//    public class Platform : Abstract.BaseModel
//    {
//        [Column(TypeName = "varchar(255)"), Comment("平台名称")]
//        public string Name { get; set; } = string.Empty;

//        [Column(TypeName = "varchar(255)"), Comment("英文名称")]
//        public string English { get; set; } = string.Empty;

//        [Column(TypeName = "tinyint"), Comment("禁用标识 1否 2是")]
//        public StateEnum State { get; set; } = StateEnum.Open;


//        [Column(TypeName = "bigint(0)"), Comment("排序")]
//        public int Sort { get; set; }

//        [Column(TypeName = "tinyint"), Comment("类型1采集专用2通用3导出专用4私人定制（舍弃）")]
//        public Types Type { get; set; }

//        public enum Types
//        {
//            [Comment("采集专用")] Collection = 1,
//            [Comment("通用")] Universal,
//            [Comment("导出专用")] Export,
//            [Comment("私人定制")] Private
//        };

//        [Column(TypeName = "tinyint"), Comment("允许采集0不允许1允许")]
//        public bool Collect { get; set; }

//        [Column(TypeName = "tinyint"), Comment("允许导出0不允许1允许")]
//        public bool Export { get; set; }

//    }
//}


//------------------------------
//--Table structure for erp3_platform
//------------------------------
//DROP TABLE IF EXISTS `erp3_platform`;
//CREATE TABLE `erp3_platform`  (
//  `id` bigint unsigned NOT NULL,
//  `d_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '平台名称',
//  `d_english` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '英文名称',
//  `t_state` tinyint unsigned NOT NULL COMMENT '禁用标识 1 否 2是',
//  `d_sort` bigint(0) NOT NULL DEFAULT 0 COMMENT '排序',
//  `t_type` tinyint unsigned NOT NULL COMMENT '类型1采集专用2通用3导出专用4私人定制（舍弃）',
//  `t_collect` tinyint unsigned NOT NULL COMMENT '允许采集0不允许1允许',
//  `t_export` tinyint unsigned NOT NULL COMMENT '允许导出0不允许1允许',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
// PRIMARY KEY(`id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '平台管理_平台信息' ROW_FORMAT = Dynamic;