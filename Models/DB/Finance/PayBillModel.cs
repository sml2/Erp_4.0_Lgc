using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using ERP.Extensions;

namespace ERP.Models.Finance;

[Index(nameof(OutHashCode), nameof(TradeNo), nameof(PayConfigId), nameof(State), nameof(Type))]
public class PayBillModel : UserModel
{
    public PayBillModel()
    {
        
    }
    public PayBillModel(Models.DB.Identity.User user)
    {
        var now = DateTime.Now;
        UserID = user.Id;
        GroupID = user.GroupID;
        CompanyID = user.CompanyID;
        OEMID = user.OEMID;
        State = PayBillStates.No;
        CreatedAt = now;
        UpdatedAt = now;
    }
    public PayBillModel(ISession session)
    {
        var now = DateTime.Now;
        UserID = session.GetUserID();
        GroupID = session.GetGroupID();
        CompanyID = session.GetCompanyID();
        OEMID = session.GetOEMID();
        State = PayBillStates.No;
        CreatedAt = now;
        UpdatedAt = now;
    }

    /// <summary>
    /// 支付名称
    /// </summary>

    [Comment("支付名称")]
    public string Name { get; set; } = null!;

    /// <summary>
    /// 充值金额
    /// </summary>

    [Comment("充值金额")]
    public Money TotalAmount { get; set; }
    
    [NotMapped]
    public decimal TotalAmountUi
    {
        get => TotalAmount.To("CNY");
    }

    /// <summary>
    /// 充值金额
    /// </summary>

    [Comment("充值金额单位")]
    public string? Unit { get; set; }

    /// <summary>
    /// 账单流水号
    /// </summary>

    [Comment("账单流水号")]
    public string? TradeNo { get; set; }

    /// <summary>
    /// 生成订单编号
    /// </summary>

    [Comment("生成订单编号")]
    public string? OutTradeNo { get; set; }

    public ulong OutHashCode { get; set; }

    /// <summary>
    /// 描述
    /// </summary>

    [Comment("描述")]
    public string? Describe { get; set; }

    /// <summary>
    /// 支付json参数
    /// </summary>

    [Comment("支付json参数")]
    public string? PayOther { get; set; }

    /// <summary>
    /// 支付配置id
    /// </summary>

    [Comment("支付配置id")]
    public int PayConfigId { get; set; }

    /// <summary>
    /// 充值类型
    /// </summary>
    [Comment("充值类型")]
    public PayBillTypes Type { get; set; }

    /// <summary>
    /// 支付状态
    /// </summary>

    [Comment("支付状态")]
    public PayBillStates State { get; set; }//1未支付2已支付

    /// <summary>
    /// 到账时间
    /// </summary>

    [Comment("到账时间")]
    public DateTime AccountDate { get; set; }
}

public enum PayBillTypes
{
    Wallet,
    Balance,
}

public enum PayBillStates
{
    No,
    Yes,
}

//-- ----------------------------
//-- Table structure for erp3_pay_bill
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_pay_bill`;
//CREATE TABLE `erp3_pay_bill`  (
//  `id` bigint unsigned NOT NULL,
//  `u_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '支付名称',
//  `d_total_amount` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '充值金额(基准货币)',
//  `d_unit` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'CNY' COMMENT '充值金额 单位',
//  `d_trade_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '账单流水号',
//  `d_out_trade_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '生成订单编号',
//  `d_out_hash_code` bigint unsigned NOT NULL COMMENT 'out_trade_no hashCode',
//  `d_describe` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '描述',
//  `u_pay_other` json NULL COMMENT '支付json参数',
//  `i_pay_config_id` int unsigned NOT NULL COMMENT '支付配置id',
//  `i_user_id` int unsigned NOT NULL COMMENT '用户id',
//  `i_group_id` int unsigned NOT NULL COMMENT '用户组id',
//  `i_company_id` int unsigned NOT NULL COMMENT '公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT 'OEM',
//  `t_type` tinyint unsigned NOT NULL COMMENT '充值类型1钱包2余额',
//  `t_state` tinyint unsigned NOT NULL COMMENT '1未支付2已支付',
//  `d_account_date` datetime(0) NULL DEFAULT NULL COMMENT '到账时间',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,Wallet
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `out_hash_code`(`d_out_hash_code`) USING BTREE,
// INDEX `out_trade_no`(`d_out_trade_no`) USING BTREE,
// INDEX `pay_config_id`(`i_pay_config_id`) USING BTREE,
// INDEX `state`(`t_state`) USING BTREE,
// INDEX `type`(`t_type`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 366 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '在线支付_支付账单记录' ROW_FORMAT = Dynamic;