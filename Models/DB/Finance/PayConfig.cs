using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;

namespace ERP.Models.Finance;

[Index(nameof(Type))]
public class PayConfig : UserModel
{

    /// <summary>
    /// 支付名称
    /// </summary>

    [Comment("支付名称")]
    public string? Name { get; set; }//支付名称

    /// <summary>
    /// json参数
    /// </summary>

    [Comment("json参数")]
    public string? Config { get; set; }//json参数

    public enum Types
    {
        [Description("新版支付宝")]
        ALIPAY_OLD = 1,

        [Description("老版支付宝")]
        ALIPAY_NEW = 2,

        [Description("微信支付")]
        WEIXIN_PAY = 3
    }

    /// <summary>
    /// 支付方式
    /// </summary>

    [Comment("支付方式")]
    public Types Type { get; set; }

    //public enum States
    //{
    //    [Description("正常")]
    //    TRUE = 1,

    //    [Description("禁止")]
    //    FALSE = 2
    //}

    /// <summary>
    /// 状态 true 正常 | false禁止
    /// </summary>

    [Comment("状态")]
    public bool State { get; set; }

    /// <summary>
    /// 排序字段
    /// </summary>
    /// 
    [Comment("排序字段")]
    public int Sort { get; set; }//排序字段
}


//-- ----------------------------
//-- Table structure for erp3_pay_config
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_pay_config`;
//CREATE TABLE `erp3_pay_config`  (
//  `id` bigint unsigned NOT NULL,
//  `d_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '支付名称',
//  `d_config` json NULL COMMENT 'json参数',
//  `i_company_id` int unsigned NOT NULL COMMENT '公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT 'OEM',
//  `t_type` tinyint unsigned NOT NULL COMMENT '支付方式1支付宝-老版2支付宝-新版3微信',
//  `t_state` tinyint unsigned NOT NULL COMMENT '状态1正常2禁止',
//  `d_sort` int (0) NOT NULL DEFAULT 0 COMMENT '排序字段',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `sort`(`d_sort`) USING BTREE,
// INDEX `state`(`t_state`) USING BTREE,
// INDEX `type`(`t_type`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '在线支付_支付配置' ROW_FORMAT = Dynamic;