using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using ERP.Models.Abstract;
using ERP.Models.DB.Setting;
using ERP.Models.Setting;

namespace ERP.Models.Export;
public class Export : UserModel
{
    /// <summary>
    /// 导出名称
    /// </summary>

    [Comment("导出名称")]
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// 导出Excel表 标识
    /// </summary>

    [Comment("导出Excel表 标识")]
    public int Sign { get; set; }

    /// <summary>
    /// 导出excel表 vue组件名称
    /// </summary>

    [Comment("导出excel表 vue组件名称")]

    public string FileName { get; set; } = string.Empty;

    /// <summary>
    /// 排序字段
    /// </summary>

    [Comment("排序字段")]
    public long Sort { get; set; }

    /// <summary>
    /// 备注信息
    /// </summary>

    [Comment("备注信息")]

    public string? Remark { get; set; }

    /// <summary>
    /// 平台ID
    /// </summary>

    [Comment("平台信息")]
    public int PlatformId { get; set; }
    public Platform? Platforms { get; set; }

    public enum States
    {
        [Description("启用")]
        ENABLE = 1,

        [Description("禁用")]
        DISABLE = 2
    }

    /// <summary>
    /// 状态1启用2禁用
    /// </summary>

    [Comment("状态1启用2禁用")]
    public States State { get; set; }

    /// <summary>
    /// 导出类型1产品
    /// </summary>

    [Comment("导出类型1产品")]
    public int Type { get; set; }

    public enum Qualitys
    {
        [Description("公共")]
        PUBLIC = 1,

        [Description("私人")]
        PRIVATE = 2
    }

    /// <summary>
    /// 性质1公共2私人
    /// </summary>

    [Comment("性质1公共2私人")]
    public Qualitys Quality { get; set; }
}

