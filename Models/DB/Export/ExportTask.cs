﻿using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.Export;

[Index(nameof(State))]
[Index(nameof(Locked))]
[Index(nameof(Type))]
[Comment("导出任务后台队列表")]
public class ExportTask : UserModel
{
    public int? SyncExportId { get; set; }
    public SyncExport? SyncExport { get; set; }
    
    /// <summary>
    /// 导出订单id的json数组
    /// </summary>
    public string? OrderIds { get; set; }
    
    public string? Params { get; set; }
    
    /// <summary>
    /// 导出任务类型<br/>
    /// 如果任务类型是产品, 则<see cref="SyncExportId"/>和<see cref="SyncExport"/>有值.<br/>
    /// 如果任务类型是订单, 则<see cref="OrderIds"/>和<see cref="Params"/>有值.
    /// </summary>
    public TaskType Type { get; set; }
    
    [Comment("总产品数量")]
    public int TotalCount { get; set; }
    [Comment("完成数量")]
    public int CompletedCount { get; set; }
    
    public TaskState State { get; set; }
    
    public bool Locked { get; set; }
    
    /// <summary>
    /// 任务状态
    /// </summary>
    public enum TaskState
    {
        /// <summary>
        /// 默认状态, 等待任务开始
        /// </summary>
        Waiting,
        /// <summary>
        /// 导出正在进行中
        /// </summary>
        Running,
        /// <summary>
        /// 导出完成, 创建本地临时文件
        /// </summary>
        Exported,
        /// <summary>
        /// 导出表格已被下载, 任务结束
        /// </summary>
        Downloaded,
        Deleted,
        Failed,
    }

    /// <summary>
    /// 任务类型
    /// </summary>
    public enum TaskType
    {
        /// <summary>
        /// 产品导出
        /// </summary>
        Products,
        /// <summary>
        /// 订单导出
        /// </summary>
        Orders,
    }
    
    /// <summary>
    /// 失败消息
    /// </summary>
    public string? FailedMessage { get; set; }
    
    public string? TempRowIndex { get; set; }

    /// <summary>
    /// 临时文件本地路径
    /// </summary>
    public string TempPath => Path.Combine("temp", ID.ToString());
    public string TempExtPath => Path.Combine("temp", $"{ID}_ext");

    public string? ProductErrors { get; set; }

    /// <summary>
    /// 标记任务失败
    /// </summary>
    /// <param name="failedMessage">失败消息</param>
    public void MarkFailed(string failedMessage)
    {
        State = TaskState.Failed;
        FailedMessage = failedMessage;
    }
    
    /// <summary>
    /// 生成导出表格的文件名称
    /// </summary>
    /// <returns></returns>
    /// <exception cref="ArgumentOutOfRangeException"></exception>
    public string GenerateExportFileName()
    {
        return Type switch
        {
            TaskType.Products => SyncExport!.GenerateExportFileName(),
            TaskType.Orders => $"Erp_Order_{DateTime.Now:yyyyMMddHHmmss}.xls",
            _ => throw new ArgumentOutOfRangeException()
        };
    }
    
    public override string ToString()
    {
        return $"#{ID}";
    }
}