﻿using ERP.Extensions;

using ERP.Models.Abstract;

using System.ComponentModel;

using Microsoft.EntityFrameworkCore;

namespace ERP.Models.Export;

[Index(nameof(PlatformId), nameof(State), nameof(Username))]
public class ExportProductModel : UserModel
{
    /// <summary>
    /// 导出模板ID
    /// </summary>

    [Comment("导出模板ID")]
    public int ExportId { get; set; }

    /// <summary>
    /// 被分配用户用户名
    /// </summary>

    [Comment("被分配用户用户名")]
    public string? Username { get; set; }

    /// <summary>
    /// 平台ID
    /// </summary>

    [Comment("平台ID")]
    public int PlatformId { get; set; }

    public enum States
    {
        [Description("启用")]
        ENABLE = 1,

        [Description("禁用")]
        DISABLE = 2
    }

    /// <summary>
    /// 状态1启用2禁用
    /// </summary>

    [Comment("状态1启用2禁用")]
    public States State { get; set; }
}

//-- ----------------------------
//-- Table structure for erp3_export_product
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_export_product`;
//CREATE TABLE `erp3_export_product`  (
//  `id` bigint unsigned NOT NULL,
//  `i_export_id` int unsigned NOT NULL COMMENT '导出模板id',
//  `u_username` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '被分配用户用户名',
//  `i_company_id` int unsigned NOT NULL COMMENT '所属公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `u_platform_id` bigint unsigned NOT NULL COMMENT '平台id',
//  `t_state` tinyint unsigned NOT NULL COMMENT '状态1启用2禁用',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `platform_id`(`u_platform_id`) USING BTREE,
// INDEX `state`(`t_state`) USING BTREE,
// INDEX `username`(`u_username`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 1269 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '数据字典_用户导出表模板' ROW_FORMAT = Dynamic;
