using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using ERP.Models.Abstract;
using System.ComponentModel.DataAnnotations.Schema;
using ERP.Models.DB.Stores;
using ERP.Enums;
using ERP.Enums.Rule.Config.Internal;
using ERP.Export.Templates;
using ERP.Extensions;
using ERP.Models.Setting;
using ERP.Models.View.Products.Export;
using ERP.Models.Stores;

namespace ERP.Models.Export;

[Index(nameof(Eid), nameof(ExportSign), nameof(LanguageId), nameof(PlatformId))]
public class SyncExportProduct : UserModel
{
    public SyncExportProduct()
    {
    }
    public SyncExportProduct(ISession session)
    {
        UserID = session.GetUserID();
        GroupID = session.GetGroupID();
        CompanyID = session.GetCompanyID();
        OEMID = session.GetOEMID();
    }

    /// <summary>
    /// 任务集ID
    /// </summary>
    [Comment("任务集ID")]
    public int Eid { get; set; }
    [ForeignKey(nameof(Eid))]
    public SyncExport Export { get; set; }

    /// <summary>
    /// 同步产品ID
    /// </summary>
    [Comment("同步产品ID")]
    public int Sid { get; set; }
    [ForeignKey(nameof(Sid))]
    public SyncProductModel SyncProductModel { get; set; }
    /// <summary>
    /// 导出产品语言
    /// </summary>
    [Comment("导出产品语言")]
    public Languages Language { get; set; } = Languages.Default;

    /// <summary>
    /// 导出产品语言ID
    /// </summary>
    [Comment("导出产品语言ID")]
    public int? LanguageId { get; set; }
    [ForeignKey(nameof(LanguageId))]
    public Language? LanguageModel { get; set; }
    /// <summary>
    /// 导出模板sign
    /// </summary>
    [Comment("导出模板sign")]
    public TemplateType ExportSign { get; set; }

    /// <summary>
    /// 导出模板平台id
    /// </summary>
    [Comment("导出模板平台id")]
    public int PlatformId { get; set; }
}

public static class SyncExportProductDbSetExtension
{
    public static IQueryable<SyncExportProduct> WhereRange(this IQueryable<SyncExportProduct> query, DataRange_2b? range, int companyId, int groupId, int userId) =>
        range switch
        {
            DataRange_2b.Company => query.Where(m => m.CompanyID == companyId),
            DataRange_2b.Group => query.Where(m => m.CompanyID == companyId && m.GroupID == groupId),
            _ => query.Where(m => m.CompanyID == companyId && m.UserID == userId),
        };
}

//-- ----------------------------
//-- Table structure for erp3_sync_export_product
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_sync_export_product`;
//CREATE TABLE `erp3_sync_export_product`  (
//  `id` bigint unsigned NOT NULL,
//  `i_eid` int unsigned NOT NULL COMMENT '任务集id',
//  `i_sid` int unsigned NOT NULL COMMENT '同步产品id',
//  `u_language` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '导出产品语言',
//  `i_language_id` int (0) NOT NULL DEFAULT 0 COMMENT '产品语言id',
//  `u_export_sign` int unsigned NOT NULL COMMENT '导出模板sign',
//  `i_platform_id` int unsigned NOT NULL COMMENT '导出模板平台id',
//  `i_user_id` int unsigned NOT NULL COMMENT '用户id',
//  `i_group_id` int (0) NOT NULL DEFAULT 0 COMMENT '创建用户组id',
//  `i_company_id` int unsigned NOT NULL COMMENT '所属公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `eid`(`i_eid`) USING BTREE,
// INDEX `export_sign`(`u_export_sign`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `language_id`(`i_language_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `platform_id`(`i_platform_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 18101945 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '产品导出_导出任务产品关联关系' ROW_FORMAT = Dynamic;