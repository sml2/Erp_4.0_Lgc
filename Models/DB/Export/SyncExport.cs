﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Text.RegularExpressions;
using ERP.Enums.Rule.Config.Internal;
using ERP.Export.ExportCommon;
using ERP.Export.Templates;
using ERP.Models.Abstract;
using ERP.Models.View.Products.Export;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ERP.Models.Export;

/// <summary>
/// 任务集
/// </summary>
[Index(nameof(ExportSign))]
[Index(nameof(LanguageId))]
[Index(nameof(PlatformId))]
[Index(nameof(Sort))]
[Comment("产品导出_导出任务集")]
public class SyncExport : UserModel
{
    public SyncExport()
    {
    }
    

    public SyncExport(AddSyncExportDto req)
    {
        Name = req.Name;
        Amount = 0;
        Param = Newtonsoft.Json.JsonConvert.SerializeObject(req.Params);
        Append = Newtonsoft.Json.JsonConvert.SerializeObject(req.Append);
        Sort = DateTime.Now.GetTimeStamp();
        ExportSign = req.Format;
        PlatformId = req.PlatformId;
        CreatedAt = DateTime.Now;
        UpdatedAt = DateTime.Now;
    }

    /// <summary>
    /// 导出产品任务集名称
    /// </summary>
    [Comment("导出产品任务集名称")]
    public string? Name { get; set; }

    /// <summary>
    /// 导出任务集产品数量
    /// </summary>
    [Comment("导出任务集产品数量")]
    public int Amount { get; set; }

    /// <summary>
    /// 排序字段
    /// </summary>
    [Comment("排序字段")]
    public int Sort { get; set; }

    /// <summary>
    /// 导出条件参数json（头部）
    /// </summary>
    [Comment("导出条件参数json（头部）")]
    public string? Param { get; set; }

    private ExcleRole? _paramObj;
    
    /// <summary>
    /// <see cref="Param" />反序列化后的对象
    /// </summary>
    [NotMapped]
    public ExcleRole? ParamObj => _paramObj ??= JsonConvert.DeserializeObject<ExcleRole>(Param!);

    /// <summary>
    /// 导出条件附加参数json
    /// </summary>
    [Comment("导出条件附加参数json")]
    public string? Append { get; set; }

    /// <summary>
    /// 产品语言id
    /// </summary>
    [Comment("产品语言id")]
    public int LanguageId { get; set; }

    /// <summary>
    /// 导出模板sign
    /// </summary>
    [Comment("导出模板sign")]
    public TemplateType ExportSign { get; set; }

    /// <summary>
    /// 导出模板平台id
    /// </summary>
    [Comment("导出模板平台id")]
    public int PlatformId { get; set; }
    
    /// <summary>
    /// 多语言关联的SyncExportId
    /// </summary>
    [Comment("多语言关联的SyncExportId")]
    public int? RelationId { get; set; }

    /// <summary>
    /// 检测参数数据是否可以导出
    /// </summary>
    /// <param name="error"></param>
    /// <returns></returns>
    public bool CanExport([NotNullWhen(false)]out string? error)
    {
        error = null;
        
        if (Append is null)
        {
            error = "导出条件参数为空";
            return false;
        }
        if (Param is null) 
        {
            error = "导出条件条件参数为空";
            return false;
        }

        if (!Enum.IsDefined(typeof(TemplateType), ExportSign))
        {
            error = "导出模板Sign为空";
            return false;
        }

        if (ParamObj is null)
        {
            error = "导出条件参数不可为空";
            return false;
        }

        return true;
    }

    /// <summary>
    /// 生成导出表格的文件名
    /// </summary>
    /// <exception cref="ArgumentNullException">需要在生成文件名前验证Param是否存在</exception>
    /// <returns></returns>
    public string GenerateExportFileName()
    {
        if (ParamObj is null)
            throw new ArgumentNullException(nameof(ParamObj), "需要在生成文件名前验证Param是否存在");
        var sb = new StringBuilder();
        sb.Append($"Erp_{Mod_GlobalFunction.FileNameReplace(Name)}_");
        sb.Append(ParamObj.Language);
        if (!string.IsNullOrWhiteSpace(Append))
        {
            var append = JObject.Parse(Append);
            var unit = append.Property("unit", StringComparison.OrdinalIgnoreCase);
            if (unit is not null && !string.IsNullOrWhiteSpace(unit.Value.Value<string>()))
                sb.Append($"_{unit.Value.Value<string>()}");
        }
        sb.Append($"_{DateTime.Now:yyyyMMddHHmmss}.");
        sb.Append(ProductExportHelper.GetExportFileExtension(ExportSign));
        var result = sb.ToString();
        
        // hktvmall表格名称不能存在中文字符
        if (ExportSign == TemplateType.Hktvmall)
            result = Regex.Replace(result, "[^A-Za-z0-9_\\.]", "");
        return result;
    }
}

public static class SyncExportDbSetExtension
{
    public static IQueryable<SyncExport> WhereRange(this IQueryable<SyncExport> query, DataRange_2b range, int companyId, int groupId, int userId) =>
        range switch
        {
            DataRange_2b.Company => query.Where(m => m.CompanyID == companyId),
            DataRange_2b.Group => query.Where(m => m.CompanyID == companyId && m.GroupID == groupId),
            _ => query.Where(m => m.CompanyID == companyId && m.UserID == userId),
        };
}
