﻿using ERP.Models.Abstract;
using ERP.Models.DB.Setting;
using ERP.Models.Setting;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.Models.Export
{
    // 用户导出表模板
    [Table("export_excel_users")]
    public class ExportProduct : UserModel
    {
        [Comment("导出模板id"),]
        public int ExportId { get; set; }
        [ForeignKey(nameof(ExportId))]
        public ExportModel? ExportsModel { get; set; }

        [Comment("被分配用户用户名")]
        public string Username { get; set; } = string.Empty;

        [Comment("平台id")]
        public int PlatformId { get; set; }
        [ForeignKey(nameof(PlatformId))]
        public Platform? Platforms { get; set; }

        [Comment("状态1启用2禁用")]
        public StateEnum State { get; set; } = StateEnum.Open;
    }
}

//------------------------------
//--Table structure for erp3_sync_export_product
//------------------------------
//DROP TABLE IF EXISTS `erp3_sync_export_product`;
//CREATE TABLE `erp3_sync_export_product`  (
//  `id` bigint unsigned NOT NULL,
//  `i_eid` int unsigned NOT NULL COMMENT '任务集id',
//  `i_sid` int unsigned NOT NULL COMMENT '同步产品id',
//  `u_language` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '导出产品语言',
//  `i_language_id` int(0) NOT NULL DEFAULT 0 COMMENT '产品语言id',
//  `u_export_sign` int unsigned NOT NULL COMMENT '导出模板sign',
//  `i_platform_id` int unsigned NOT NULL COMMENT '导出模板平台id',
//  `i_user_id` int unsigned NOT NULL COMMENT '用户id',
//  `i_group_id` int(0) NOT NULL DEFAULT 0 COMMENT '创建用户组id',
//  `i_company_id` int unsigned NOT NULL COMMENT '所属公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
// PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `eid`(`i_eid`) USING BTREE,
// INDEX `export_sign`(`u_export_sign`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `language_id`(`i_language_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `platform_id`(`i_platform_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 18101945 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '产品导出_导出任务产品关联关系' ROW_FORMAT = Dynamic;