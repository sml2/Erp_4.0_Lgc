//using Microsoft.EntityFrameworkCore;
//using ERP.Models.ProductNodeType.Enum;
//using System;
//using System.ComponentModel.DataAnnotations.Schema;
//using ERP.Enums;
//using System.Diagnostics.CodeAnalysis;
//using ERP.Models.DB.ProductNodeType;

//namespace ERP.Models.ProductNodeType
//{
//    public class CategoryAttrRelation
//    {
//        public CategoryAttrRelation()
//        {

//        }

//        public CategoryAttrRelation(long cid, ProductNodeTypeAttribute attributeModel,long? attributeId,
//            Platforms enumPlatform,EnumIsMandatory enumIsMandatory,long? oldAttrID,int? amzFormRawID, int? amzRSRID,string? valueDatas)
//        {
//            CId = cid;
//            attribute = attributeModel;
//            AttributeId = attributeId;
//            Platform = enumPlatform;
//            ExcuteTime = DateTime.Now;
//            IsMandatory = enumIsMandatory;
//            OldAttrId = oldAttrID;
//            AmzFormRawID = amzFormRawID;
//            AmzRSRID = amzRSRID;
//            Values = valueDatas;
//        }


//        public long Id { set; get; }

//        [Comment("分类目录表的主键ID")]
//        public long CId { set; get; }
//        [ForeignKey("CId")]
//        public ProductNodeTypeCategory? category { get; set; }

//        [Comment("属性表的主键ID")]
//        public long AttrId { set; get; }

//        [ForeignKey("AttrId")]
//        public ProductNodeTypeAttribute attribute { set; get; } = null!;

//        [Comment("属性ID")]
//        public long? AttributeId { set; get; }

//        [Comment("使用状态")]
//        /// <summary>
//        /// 使用状态
//        /// </summary>
//        public EnumStatus Status { set; get; } = EnumStatus.ADD;

//        [Comment("旧的属性表的主键ID")]
//        public long? OldAttrId { get; set; }

//        [Comment("新的属性表的主键ID")]
//        public long? NewAttrId { get; set; }

//        [ForeignKey("NewAttrId")]
//        public ProductNodeTypeAttribute? NewAttribute { set; get; }

//        [Comment("执行时间")]
//        public DateTime? ExcuteTime { get; set; }

//        [Comment("所属平台")]
//        /// <summary>
//        /// 所属平台
//        /// </summary>
//        public Platforms Platform { get; set; }


//        [Comment("values")]
//        /// <summary>
//        /// 值json
//        /// </summary>
//        public string? Values { get; set; }

//        [Comment("是否必填")]
//        /// <summary>
//        /// 是否必填
//        /// </summary>
//        public EnumIsMandatory IsMandatory { get; set; }

//        [Comment("amazon-ProductExcelID")]
//        public int? AmzFormRawID { get; set; }

//        [Comment("amazon-HtmlID")]
//        public int? AmzRSRID { get; set; }
//    }
//}
