//using Microsoft.EntityFrameworkCore;
//using ERP.Models.ProductNodeType.Enum;
//using System;
//using System.Collections.Generic;
//using System.ComponentModel.DataAnnotations;
//using System.ComponentModel.DataAnnotations.Schema;
//using ERP.Enums;
//using ERP.Models.ProductNodeType;

//namespace ERP.Models.DB.ProductNodeType
//{
//    [Index(nameof(AmzRSRID), nameof(AmzFormRawID))]
//    public class ProductNodeTypeCategory
//    {
//        public ProductNodeTypeCategory()
//        {

//        }
//        public ProductNodeTypeCategory(string oname, string dname, Platforms enumPlatform, long? categoryId, long? parentCategoryId)
//        {
//            OriginalName = oname;
//            DisplayName = dname;
//            Platform = enumPlatform;
//            CategoryId = categoryId;
//            CategoryParentId = parentCategoryId;
//        }
//        public ProductNodeTypeCategory(string oname, string dname, Platforms enumPlatform, int formID, int rID)
//        {
//            OriginalName = oname;
//            DisplayName = dname;
//            Platform = enumPlatform;
//            AmzFormRawID = formID;
//            AmzRSRID = rID;
//        }
//        public long Id { get; set; }

//        [Comment("字段名称")]
//        public string OriginalName { set; get; } = string.Empty;

//        [Comment("展示名称")]
//        public string DisplayName { set; get; }=string.Empty;

//        [Comment("添加时间")]
//        public DateTime AddTime { get; set; } = DateTime.Now;

//        [Comment("所属平台")]
//        /// <summary>
//        /// 所属平台
//        /// </summary>
//        public Platforms Platform { get; set; }

//        [Comment("分类目录ID")]
//        public long? CategoryId { set; get; }

//        [Comment("所属父目录ID")]
//        public long? CategoryParentId { set; get; }

//        [Comment("该分类下所有属性和值")]
//        /// <summary>
//        /// 该分类下所有属性和值
//        /// </summary>
//        public string? Datas { get; set; }
//        [Comment("amazon-ProductExcelID")]
//        public int? AmzFormRawID { get; set; }
//        [Comment("amazon-HtmlID")]
//        public int? AmzRSRID { get; set; }

//        [InverseProperty("Category")]
//        public List<AreaCategoryRelation> Categorys { get; set; } = new();

//        [InverseProperty("NewCategory")]
//        public List<AreaCategoryRelation> NewCategorys { get; set; } = new();

//        public List<CategoryAttrRelation> CategoryAttrRelations { get; set; } = new();
//    }


    

//}