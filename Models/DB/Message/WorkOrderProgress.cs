﻿using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;
using ERP.Models.DB.Identity;

namespace ERP.Models.Message;

[Index(nameof(State))]
public class WorkOrderProgress : UserModel
{
    public WorkOrderProgress()
    {
    }
    public WorkOrderProgress(string name, User user)
    {
        Name = name;
        State = States.ENABLE;
        UserID = user.Id;
        TrueName = user.TrueName;
        CompanyID = user.CompanyID;
        GroupID = user.GroupID;
        OEMID = user.OEMID;
    }


    /// <summary>
    /// 进度名称
    /// </summary>

    [Comment("进度名称")]
    public string? Name { get; set; }

    /// <summary>
    /// 创建用户姓名
    /// </summary>

    [Comment("创建用户姓名")]
    public string? TrueName { get; set; }

    public enum States
    {
        [Description("启用")]
        ENABLE = 1,

        [Description("禁用")]
        DISABLE = 2
    }

    /// <summary>
    /// 状态 1启用 2禁用
    /// </summary>

    [Comment("状态 1启用 2禁用")]
    public States State { get; set; }

    /// <summary>
    /// 排序
    /// </summary>

    [Comment("排序")]
    public int Sort { get; set; }
}

//-- ----------------------------
//-- Table structure for erp3_work_order_progress
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_work_order_progress`;
//CREATE TABLE `erp3_work_order_progress`  (
//  `id` bigint unsigned NOT NULL,
//  `d_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '进度名称',
//  `i_user_id` int unsigned NOT NULL COMMENT '创建用户id',
//  `i_company_id` int unsigned NOT NULL COMMENT '公司id',
//  `i_group_id` int unsigned NOT NULL COMMENT '当前用户组id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `u_truename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '创建用户姓名',
//  `t_state` tinyint unsigned NOT NULL COMMENT '状态 1启用 2禁用',
//  `sort` int unsigned NOT NULL COMMENT '排序',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `state`(`t_state`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '信息中心_工单问题类型' ROW_FORMAT = Dynamic;
