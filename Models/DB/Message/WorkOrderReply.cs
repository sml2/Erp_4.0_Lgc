﻿using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;
using ERP.Models.DB.Identity;

namespace ERP.Models.Message;
public class WorkOrderReply : UserModel
{
    public WorkOrderReply(){}
    public WorkOrderReply(int id, string content, User user)
    {
        WorkOrderId = id;
        Content = content;
        UserID = user.Id;
        GroupID = user.GroupID;
        CompanyID = user.CompanyID;
        OEMID = user.OEMID;
        TrueName = user.TrueName;
    }

    /// <summary>
    /// 工单id
    /// </summary>

    [Comment("工单id")]
    public int WorkOrderId { get; set; }

    /// <summary>
    /// 回复用户姓名
    /// </summary>

    [Comment("回复用户姓名")]
    public string? TrueName { get; set; }

    /// <summary>
    /// 回复内容
    /// </summary>

    [Comment("回复内容")]
    public string? Content { get; set; }
}


//-- ----------------------------
//-- Table structure for erp3_work_order_reply
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_work_order_reply`;
//CREATE TABLE `erp3_work_order_reply`  (
//  `id` bigint unsigned NOT NULL,
//  `i_work_order_id` int unsigned NOT NULL COMMENT '工单id',
//  `i_user_id` int unsigned NOT NULL COMMENT '回复用户id',
//  `i_company_id` int unsigned NOT NULL COMMENT '公司id',
//  `i_group_id` int unsigned NOT NULL COMMENT '当前用户组id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `u_truename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '回复用户姓名',
//  `d_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '回复内容',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `work_order_id`(`i_work_order_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '信息中心_工单回复详细信息' ROW_FORMAT = Dynamic;

//SET FOREIGN_KEY_CHECKS = 1;
