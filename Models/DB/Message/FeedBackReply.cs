﻿using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using ERP.Models.DB.Identity;

namespace ERP.Models.Message;

[Index(nameof(FeedbackId))]
public class FeedBackReply : UserModel
{
    public FeedBackReply() {}
    public FeedBackReply(int feedbackId, string reply, User user)
    {
        FeedbackId = feedbackId;
        Content = reply;
        UserID = user.Id;
        OEMID = user.OEMID;
        CompanyID = user.CompanyID;
        GroupID = user.GroupID;
        UserName = user.TrueName;
    }

    /// <summary>
    /// 反馈问题id
    /// </summary>

    [Comment("反馈问题id")]
    public int FeedbackId { get; set; }
    /// <summary>
    /// 回复用户姓名
    /// </summary>

    [Comment("回复用户姓名")]
    public string? UserName { get; set; }

    /// <summary>
    /// 回复内容
    /// </summary>

    [Comment("回复内容")]
    public string? Content { get; set; }
}

//-- ----------------------------
//-- Table structure for erp3_feedback_reply
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_feedback_reply`;
//CREATE TABLE `erp3_feedback_reply`  (
//  `id` bigint unsigned NOT NULL,
//  `i_feedback_id` int unsigned NOT NULL COMMENT '反馈问题id',
//  `i_user_id` int unsigned NOT NULL COMMENT '回复用户id',
//  `i_company_id` int unsigned NOT NULL COMMENT '公司id',
//  `i_group_id` int unsigned NOT NULL COMMENT '当前用户组id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `u_user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '回复用户姓名',
//  `d_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '回复内容',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `feedback_id`(`i_feedback_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '反馈回复' ROW_FORMAT = Dynamic;