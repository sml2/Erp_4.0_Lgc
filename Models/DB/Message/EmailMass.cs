﻿using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;
using ERP.Models.DB.Identity;

namespace ERP.Models.Message;

[Index(nameof(HashCode))]
public class EmailMass: UserModel
{
    public EmailMass() {}
    
    public EmailMass(string postbox, string nick, User user)
    {
        Update(postbox, nick);
        UserID = user.Id;
        CompanyID = user.CompanyID;
        ReportId = user.Company?.ReportId ?? 0;
        GroupID = user.GroupID;
        OEMID = user.OEMID;
    }

    /// <summary>
    /// 邮箱地址
    /// </summary>

    [Comment("邮箱地址")]
    public string? PostBox { get; set; }

    /// <summary>
    /// 邮箱用户昵称
    /// </summary>

    [Comment("邮箱用户昵称")]
    public string? Nick { get; set; }

    /// <summary>
    /// 上报id
    /// </summary>

    [Comment("上报id")]
    public int ReportId { get; set; }

    /// <summary>
    /// 唯一识别码
    /// </summary>

    [Comment("唯一识别码")]
    public ulong HashCode { get; set; }

    public void Update(string postbox, string nick)
    {
        PostBox = postbox.Trim();
        HashCode = Helpers.CreateHashCode(PostBox);
        Nick = nick.Trim();
    }
}

//-- ----------------------------
//-- Table structure for erp3_email_mass
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_email_mass`;
//CREATE TABLE `erp3_email_mass`  (
//  `id` bigint unsigned NOT NULL,
//  `i_user_id` int unsigned NOT NULL COMMENT '所属用户id',
//  `i_company_id` int unsigned NOT NULL COMMENT '所属公司id',
//  `i_group_id` int unsigned NOT NULL COMMENT '当前用户组id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `d_postbox` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '邮箱地址',
//  `u_nick` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '邮箱用户昵称',
//  `i_report_id` int unsigned NOT NULL COMMENT '上报id',
//  `d_hashcode` bigint unsigned NOT NULL COMMENT '唯一识别码',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `hashcode`(`d_hashcode`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 55 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '邮箱管理_邮件群发地址' ROW_FORMAT = Dynamic;