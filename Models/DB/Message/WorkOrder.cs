﻿using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;
using ERP.Models.DB.Identity;
using ERP.Enums.Message;

namespace ERP.Models.Message;

[Index(nameof(ParentCompanyId), nameof(ReceivePopup), nameof(SendPopup))]
public class WorkOrder : UserModel
{
    public WorkOrder() {}
    public WorkOrder(string title, string content, int categoryId, int progressId, DateTime deadline, User user,Commits commit)
    {
        Title = title.Trim();
        Content = content.Trim();
        CategoryId = categoryId;
        ProgressId = progressId;
        DeadLine = deadline;
        UserID = user.Id;
        CompanyID = user.CompanyID;
        GroupID = user.GroupID;
        OEMID = user.OEMID;
        ParentCompanyId = 0;
        SendNum = 0;
        ReceiveNum = 1;
        Commit =commit;
        State = States.ToBeProcessed;
        SendPopup = false;
        ReceivePopup = true;
    }

    /// <summary>
    /// 工单标题
    /// </summary>

    [Comment("工单标题")]
    public string Title { get; set; }=string.Empty; 

    /// <summary>
    /// 工单问题类型id
    /// </summary>

    [Comment("工单问题类型id")]
    public int CategoryId { get; set; }

    /// <summary>
    /// 工单问题类型
    /// </summary>

    [Comment("工单问题类型")]
    public string? Category { get; set; }

    /// <summary>
    /// 工单进度状态id
    /// </summary>

    [Comment("工单进度状态id")]
    public int ProgressId { get; set; }

    /// <summary>
    /// 工单进度状态
    /// </summary>

    public string? Progress { get; set; }

    /// <summary>
    /// 创建用户未读回复
    /// </summary>

    [Comment("创建用户未读回复")]
    public int SendNum { get; set; }

    /// <summary>
    /// 接收用户未读信息
    /// </summary>

    [Comment("接收用户未读信息")]
    public int ReceiveNum { get; set; }

    /// <summary>
    /// 处理期限日期时间戳
    /// </summary>

    [Comment("处理期限日期时间戳")]
    public DateTime DeadLine { get; set; }

    /// <summary>
    /// 创建公司的上级公司id
    /// </summary>

    [Comment("创建公司的上级公司id")]
    public int ParentCompanyId { get; set; }

    /// <summary>
    /// 提交1当前公司2上级公司
    /// </summary>

    [Comment("提交1当前公司2上级公司")]
    public Commits Commit { get; set; }

    /// <summary>
    /// 工单内容
    /// </summary>

    [Comment("工单内容")]
    public string Content { get; set; } = string.Empty;

    public enum States
    {
        [Description("待处理")]
        ToBeProcessed = 1,

        [Description("处理中")]
        Processing = 2,

        [Description("完成")]
        Complete = 3,

        [Description("撤回")]
        Withdraw = 4
    }

    /// <summary>
    /// 状态 1待处理2处理中3完成4撤回
    /// </summary>

    [Comment("状态 1待处理2处理中3完成4撤回")]
    public States State { get; set; }

    //public enum SendPopups
    //{
    //    [Description("需要")]
    //    YES = 1,

    //    [Description("不需要")]
    //    NO = 2
    //}

    /// <summary>
    /// 创建用户弹窗提醒 yes 需要 | no 不需要
    /// </summary>

    [Comment("创建用户弹窗提醒 yes 需要 | no 不需要")]
    public bool SendPopup { get; set; }

    //public enum ReceivePopups
    //{
    //    [Description("需要")]
    //    YES = 1,

    //    [Description("不需要")]
    //    NO = 2
    //}

    /// <summary>
    /// 接收用户弹窗提醒 yes 需要 | no 不需要
    /// </summary>

    [Comment("接收用户弹窗提醒 yes 需要 | no 不需要")]
    public bool ReceivePopup { get; set; }

    public void Withdraw()
    {
        State = States.Withdraw;
    }
}

//-- ----------------------------
//-- Table structure for erp3_work_order
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_work_order`;
//CREATE TABLE `erp3_work_order`  (
//  `id` bigint unsigned NOT NULL,
//  `d_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '工单标题',
//  `i_category_id` int unsigned NOT NULL COMMENT '工单问题类型id',
//  `d_category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '工单问题类型',
//  `i_progress_id` int unsigned NOT NULL COMMENT '工单进度状态id',
//  `d_progress` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '工单进度状态',
//  `d_send_num` int unsigned NOT NULL COMMENT '创建用户未读回复',
//  `d_receive_num` int unsigned NOT NULL COMMENT '接收用户未读信息',
//  `d_deadline` datetime(0) NULL DEFAULT NULL COMMENT '处理期限日期时间戳',
//  `i_user_id` int unsigned NOT NULL COMMENT '创建用户id',
//  `i_company_id` int unsigned NOT NULL COMMENT '创建用户公司id',
//  `i_group_id` int unsigned NOT NULL COMMENT '当前用户组id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `i_parent_company_id` int unsigned NOT NULL COMMENT '创建公司的上级公司id',
//  `t_commit` tinyint unsigned NOT NULL COMMENT '提交1当前公司2上级公司',
//  `d_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '工单内容',
//  `t_state` tinyint unsigned NOT NULL COMMENT '状态 1待处理2处理中3完成4撤回',
//  `t_send_popup` tinyint unsigned NOT NULL COMMENT '创建用户弹窗提醒1需要2不需要',
//  `t_receive_popup` tinyint unsigned NOT NULL COMMENT '接收用户弹窗提醒1需要2不需要',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `commit`(`t_commit`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `parent_company_id`(`i_parent_company_id`) USING BTREE,
// INDEX `receive_popup`(`t_receive_popup`) USING BTREE,
// INDEX `send_popup`(`t_send_popup`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '信息中心_工单列表' ROW_FORMAT = Dynamic;