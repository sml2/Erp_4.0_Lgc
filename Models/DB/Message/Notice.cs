﻿using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using ERP.Enums.Message;
using ERP.Models.DB.Identity;

namespace ERP.Models.Message;

public class Notice : UserModel
{
    public Notice(){}
    public Notice(string title, string content, States state, Types type, Sizes size, DateTime? noticeBegin, DateTime? noticeEnd, User user)
    {
        Title = title;
        Content = content;
        State = state;
        Type = type;
        Size = size;
        NoticeBegin = noticeBegin;
        NoticeEnd = noticeEnd;
        UserID = user.ID;
        CompanyID = user.CompanyID;
        GroupID = user.GroupID;
        OEMID = user.OEMID;
        ReportId = user.Company.ReportId;
        TrueName = user.TrueName;
    }

    /// <summary>
    /// 上报id
    /// </summary>

    [Comment("上报id")]
    public int ReportId { get; set; }

    /// <summary>
    /// 标题
    /// </summary>

    [Comment("标题")]
    public string? Title { get; set; }

    /// <summary>
    /// 内容
    /// </summary>

    [Comment("内容")]
    public string? Content { get; set; }

    /// <summary>
    /// 真实用户名
    /// </summary>

    [Comment("真实用户名")]
    public string? TrueName { get; set; }

    /// <summary>
    /// 高
    /// </summary>

    [Comment("高")]
    public string? Height { get; set; }

    /// <summary>
    /// 宽
    /// </summary>

    [Comment("宽")]
    public string? Width { get; set; }

 

    /// <summary>
    /// 弹窗大小1大2中3小
    /// </summary>

    [Comment("弹窗大小1大2中3小")]
    public Sizes Size { get; set; }

 

    /// <summary>
    /// 1启用2草稿
    /// </summary>

    [Comment("1启用2草稿")]
    public States State { get; set; }

   

    /// <summary>
    /// 公告类型:1.系统所有人员2.公司外部3.小组内4.公司内部
    /// </summary>

    [Comment("公告类型:1.系统所有人员2.公司外部3.小组内4.公司内部")]
    public Types Type { get; set; }

    /// <summary>
    /// 通知开始时间
    /// </summary>

    [Comment("通知开始时间")]

    public DateTime? NoticeBegin { get; set; }

    /// <summary>
    /// 通知结束时间
    /// </summary>

    [Comment("通知结束时间")]
    public DateTime? NoticeEnd { get; set; }

    /// <summary>
    /// 不再弹出
    /// </summary>

    [Comment("不再弹出")]
    public string? DoNotShowAgain { get; set; }
}


//-- ----------------------------
//-- Table structure for erp3_notice
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_notice`;
//CREATE TABLE `erp3_notice`  (
//  `id` bigint unsigned NOT NULL,
//  `i_user_id` int unsigned NOT NULL COMMENT '用户id',
//  `i_group_id` int unsigned NOT NULL COMMENT '用户组id',
//  `i_company_id` int unsigned NOT NULL COMMENT '所属公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `i_report_id` int unsigned NOT NULL COMMENT '上报id',
//  `d_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '标题',
//  `d_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '内容',
//  `d_truename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '真实用户名',
//  `d_height` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '90%' COMMENT '高',
//  `d_width` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '90%' COMMENT '宽',
//  `d_size` tinyint unsigned NOT NULL COMMENT '弹窗大小1大2中3小',
//  `t_state` tinyint unsigned NOT NULL COMMENT '1启用2草稿',
//  `t_type` tinyint unsigned NOT NULL COMMENT '公告类型:1.系统所有人员2.公司外部3.小组内4.公司内部',
//  `d_notice_begin` datetime(0) NULL DEFAULT NULL COMMENT '通知开始时间',
//  `d_notice_end` datetime(0) NULL DEFAULT NULL COMMENT '通知结束时间',
//  `d_do_not_show_again` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '不再弹出',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户管理_通知公告' ROW_FORMAT = Dynamic;
