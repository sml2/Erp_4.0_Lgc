﻿using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;

namespace ERP.Models.Message;

[Index(nameof(State))]
public class EmailInfo : UserModel
{
    /// <summary>
    /// 邮箱地址
    /// </summary>

    [Comment("邮箱地址")]
    public string? EmailAddress { get; set; }

    /// <summary>
    /// 邮件主题
    /// </summary>

    [Comment("邮件主题")]
    public string? Theme { get; set; }

    /// <summary>
    /// 内容
    /// </summary>

    [Comment("内容")]
    public string? Content { get; set; }

    /// <summary>
    /// 邮件时间
    /// </summary>

    [Comment("邮件时间")]
    public DateTime SendTime { get; set; }

    /// <summary>
    /// 邮箱ID
    /// </summary>

    [Comment("邮箱ID")]
    public int EmailId { get; set; }

    /// <summary>
    /// 添加时间戳
    /// </summary>

    [Comment("添加时间戳")]
    public DateTime TimesTemp { get; set; }

    public enum States
    {
        [Description("发件")]
        DELIVERY = 1,

        [Description("收件")]
        RECEIPT = 2
    }

    /// <summary>
    /// 1发件 2收件
    /// </summary>

    [Comment("1发件 2收件")]
    public States State { get; set; }

    /// <summary>
    /// 邮件标识
    /// </summary>

    [Comment("邮件标识")]
    public string? Tag { get; set; }

    /// <summary>
    /// 唯一标识码
    /// </summary>

    [Comment("唯一标识码")]
    public int HashCode { get; set; }

    /// <summary>
    /// yes 是 | no 否
    /// </summary>

    [Comment("yes 是 | no 否")]
    public bool IsHtml { get; set; }

    /// <summary>
    /// 邮件回复人
    /// </summary>

    [Comment("邮件回复人")]
    public string? Replyto { get; set; }

    /// <summary>
    /// 我的昵称
    /// </summary>

    [Comment("我的昵称")]
    public string? MyNick { get; set; }

    /// <summary>
    /// 他人昵称
    /// </summary>

    [Comment("他人昵称")]
    public string? OtherNick { get; set; }

    /// <summary>
    /// 邮箱回复人昵称
    /// </summary>

    [Comment("邮箱回复人昵称")]
    public string? ReplaytoNick { get; set; }
}

//-- ----------------------------
//-- Table structure for erp3_email_info
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_email_info`;
//CREATE TABLE `erp3_email_info`  (
//  `id` bigint unsigned NOT NULL,
//  `i_user_id` int unsigned NOT NULL COMMENT '用户id',
//  `i_group_id` int unsigned NOT NULL COMMENT '当前用户组id',
//  `i_company_id` int unsigned NOT NULL COMMENT '当前拉取用户所在公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `d_email_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '邮箱地址',
//  `u_theme` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '邮件主题',
//  `d_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '内容',
//  `u_send_time` datetime(0) NULL DEFAULT NULL COMMENT '邮件时间',
//  `i_email_id` int unsigned NOT NULL COMMENT '邮箱id',
//  `d_timestamp` datetime(0) NULL DEFAULT NULL COMMENT '添加时间戳',
//  `t_state` tinyint unsigned NOT NULL COMMENT '1发件 2收件',
//  `d_tag` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '邮件标识',
//  `d_hash_code` bigint unsigned NOT NULL COMMENT '唯一标识码',
//  `t_is_html` tinyint unsigned NOT NULL COMMENT '1是 2否',
//  `u_replyto` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '邮箱回复人',
//  `u_my_nick` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '我的昵称',
//  `u_other_nick` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '他人昵称',
//  `u_replyto_nick` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '邮箱回复人昵称',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `email_id`(`i_email_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `state`(`t_state`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '邮箱管理_邮件内容' ROW_FORMAT = Dynamic;