﻿using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;
using System.Text.Json;
using ERP.Models.DB.Identity;

namespace ERP.Models.Message;

public class FeedBack : UserModel
{
    public FeedBack() {}
    public FeedBack(string title, Types type, string content, List<string> imgs, States state, User user)
    {
        Title = title;
        Type = type;
        Content = content;
        State = state;
        Imgs = JsonSerializer.Serialize(imgs);
        UserID = user.Id;
        CompanyID = user.CompanyID;
        GroupID = user.GroupID;
        OEMID = user.OEMID;
        UserID = user.Id;
        UserName = user.UserName;
        // $oem_name = Cache::instance()->oemAll()->get()[$oem_id]['name'];//本地获取
        //        $oem_name = Cache::instance()->currentOem()->get()['name'];//线上获取方式(本地域名不对，会获取错误)
        // TODO: 获取oem_name
        OemName = user.OEM.Name;
    }

    /// <summary>
    /// 反馈问题标题
    /// </summary>

    [Comment("反馈问题标题")]
    public string? Title { get; set; }

    /// <summary>
    /// 反馈问题内容
    /// </summary>

    [Comment("反馈问题内容")]
    public string? Content { get; set; }

    /// <summary>
    /// 反馈问题图片
    /// </summary>

    [Comment("反馈问题图片")]
    public string? Imgs { get; set; }

    public enum Types
    {
        //问题场景 web端 1 | client端 2
        [Description("web端")]
        WEB = 1,

        [Description("client端")]
        CLIENT = 2
    }

    /// <summary>
    /// 反馈问题类型
    /// </summary>

    [Comment("反馈问题类型")]
    public Types Type { get; set; }

    public bool IsComplete => State == States.COMPLETED;

    public enum States
    {
        [Description("未处理")]
        NOT_PROCESSED = 1,

        [Description("处理中")]
        IN_PROCESS = 2,

        [Description("已完成")]
        COMPLETED = 3,

        [Description("撤回")]
        REVOKE = 4
    }

    /// <summary>
    /// 处理状态
    /// </summary>

    [Comment("处理状态")]
    public States State { get; set; }

    /// <summary>
    /// 反馈问题用户名
    /// </summary>

    [Comment("反馈问题用户名")]
    public string? UserName { get; set; }

    /// <summary>
    /// 反馈问题OEM名称
    /// </summary>

    [Comment("反馈问题OEM名称")]
    public string? OemName { get; set; }
}

//-- ----------------------------
//-- Table structure for erp3_feedback
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_feedback`;
//CREATE TABLE `erp3_feedback`  (
//  `id` bigint unsigned NOT NULL,
//  `i_user_id` int unsigned NOT NULL COMMENT '用户id',
//  -- `i_group_id` int unsigned NOT NULL COMMENT '当前用户组id',
//  -- `i_group_id` int unsigned NOT NULL COMMENT '当前用户组id',
//  `i_company_id` int unsigned NOT NULL COMMENT '公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `d_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '反馈问题标题',
//  `d_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '反馈问题内容',
//  `d_imgs` json NOT NULL COMMENT '反馈问题图片',
//  `d_types` int unsigned NOT NULL COMMENT '反馈问题类型',
//  `t_state` int unsigned NOT NULL COMMENT '处理状态',
//  `u_user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '反馈问题用户名',
//  `u_oem_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '反馈问题oem名称',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 116 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户反馈问题列表' ROW_FORMAT = Dynamic;