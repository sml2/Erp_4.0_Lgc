//using Microsoft.EntityFrameworkCore;
//namespace ERP.Models.DB;
//using Models.Setting;
//using Models.Abstract;
//using Models.ProductNodeType;
//public partial class PlatformData : BaseModel
//{
//    public PlatformData() { }

//    public int? NationId { get; set; }

//    [Comment("所属国家")]
//    /// <summary>
//    /// 所属平台
//    /// </summary>
//    public Nation? Nation { get; set; }

//    [Comment("所属平台")]
//    /// <summary>
//    /// 所属平台
//    /// </summary>
//    public Platforms Platform { get; set; }

//    [Comment("平台数据")]
//    public string Data { get; set; }=string.Empty;
    
//    public void SetData<T>(T data)
//    {
//        //Data = System.Text.Json.JsonSerializer.Serialize(data);
//        Data = Newtonsoft.Json.JsonConvert.SerializeObject(data);
//    }
//    /// <summary>
//    /// 安全转换，但可能为空
//    /// </summary>
//    /// <typeparam name="T"></typeparam>
//    /// <param name="maker"></param>
//    /// <returns></returns>
//    private T? GetDataSafe<T>(Action<T>? maker = null)
//    {
//        var data = System.Text.Json.JsonSerializer.Deserialize<T>(Data);
//        if (maker is not null && data is not null) maker(data);
//        return data;
//    }   
//    /// <summary>
//    /// 非安全转换
//    /// </summary>
//    /// <typeparam name="T"></typeparam>
//    /// <param name="maker"></param>
//    /// <returns></returns>
//    /// <exception cref="NullReferenceException"></exception>
//    private T GetData<T>(Action<T>? maker = null)
//    {
//        var data = GetDataSafe<T>(maker);
//        if (data is not null)
//        {
//            return data;
//        }
//        else
//        {
//            throw new NullReferenceException();
//        }
//    }
//    internal AmazonData? GetAmazonData() => GetDataSafe<AmazonData>();
//    internal LogisticsData? GetLogisticsData() => GetDataSafe<LogisticsData>();
//    internal AmazonDataWithID? GetAmazonDataWithID() => GetDataSafe<AmazonDataWithID>(d => d.ID = ID);
//    internal ShopeeData? GetShopeeDataSafe() => GetDataSafe<ShopeeData>();
//    internal ShopeeData GetShopeeData() => GetData<ShopeeData>();
//    internal SFCData? GetSFCData() => GetDataSafe<SFCData>();

//    public List<AreaCategoryRelation> AreaCategoryRelations { get; set; } = new();
//    public override string ToString()
//    {
//        return $"ID[{ID}] Platform[{Platform}] Nation[{Nation}]";
//    }
//}

//public class ErrorDatas
//{
//    public ErrorDatas()
//    {

//    }      

//    public ErrorDatas(Platforms platform, int? amzFormRawID, int? amzRSRID, string message)
//    {
//        Platform = platform;
//        AmzFormRawID = amzFormRawID;
//        AmzRSRID = amzRSRID;
//        Message = message;
//    }      
//    public int ID { get; set; }
//    public Platforms Platform { get; set; }
//    public int? AmzFormRawID { get; set; }
//    public int? AmzRSRID { get; set; }
//    public string Message { get; set; } = string.Empty;

//    public DateTime Datetime { get; set; } = DateTime.Now;
//}
