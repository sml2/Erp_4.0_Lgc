﻿using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.Models.Product;

[Table("ean_upc")]
[Comment("产品管理_ean/upc")]
[Index(nameof(HashCode), IsUnique = true)]
[Obsolete("废弃",true)]
public class EanUpc : UserModel
{
    [Comment("码值")]
    public string Value { get; set; } = string.Empty;

    [Comment("分类名称")]
    public int HashCode { get; set; }

    [Comment("类型1ean2upc")]
    public Types Type { get; set; }

    public enum Types
    {
        [Description("EAN")] EAN = 1,
        [Description("UPC")] UPC
    }
}
//-- ----------------------------
//-- Table structure for erp3_ean_upc
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_ean_upc`;
//CREATE TABLE `erp3_ean_upc`  (
//  `id` bigint unsigned NOT NULL,
//  `i_oem_id` bigint unsigned NOT NULL COMMENT 'oem_id',
//  `i_user_id` bigint unsigned NOT NULL COMMENT '创建用户id',
//  `i_group_id` bigint unsigned NOT NULL COMMENT '分组id',
//  `i_company_id` bigint unsigned NOT NULL COMMENT '公司id',
//  `t_type` tinyint unsigned NOT NULL COMMENT '类型1ean2upc',
//  `d_value` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '码值',
//  `d_hash_code` bigint unsigned NOT NULL,
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY (`id`) USING BTREE,
//  UNIQUE INDEX `hash_code`(`d_hash_code`) USING BTREE,
//  INDEX `company_id`(`i_company_id`) USING BTREE,
//  INDEX `group_id`(`i_group_id`) USING BTREE,
//  INDEX `oem_id`(`i_oem_id`) USING BTREE,
//  INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 74593013 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '产品管理_ean/upc' ROW_FORMAT = Dynamic;