﻿using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.Models.Product;

[Comment("找不到相关产品数据#1临时存储")][Index(nameof(CountryID))]
public class InvalidProductsRecords : UserRelated
{
    [Comment("国家id")]
    public int CountryID { get; set; }
    [Comment("查询关键词")]
    public string Keywords { get; set; } = string.Empty;
    [Comment("详细信息"),Column(TypeName = "Json")]
    public string Details { get; set; } = string.Empty;
    [Comment("标题")]
    public string Title { get; set; } = string.Empty;
    [Comment("链接")]
    public string URL { get; set; } = string.Empty;
    [Comment("描述")]
    public string Description { get; set; } = string.Empty;
}
//-- ----------------------------
//-- Table structure for erp3_invalid_products_records
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_invalid_products_records`;
//CREATE TABLE `erp3_invalid_products_records`  (
//  `id` bigint unsigned NOT NULL,
//  `d_keywords` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '查询关键词',
//  `i_country_id` int(0) NOT NULL COMMENT '国家id',
//  `d_details` json NOT NULL COMMENT '详细信息',
//  `d_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '标题',
//  `d_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '链接',
//  `d_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '描述',
//  `i_user_id` int(0) NOT NULL DEFAULT 0 COMMENT '创建用户id',
//  `i_group_id` int(0) NOT NULL DEFAULT 0 COMMENT '创建用户组id',
//  `i_company_id` int(0) NOT NULL DEFAULT 0 COMMENT '公司id',
//  `i_oem_id` int(0) NOT NULL DEFAULT 0 COMMENT 'oem_id',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY (`id`) USING BTREE,
//  INDEX `company_id`(`i_company_id`) USING BTREE,
//  INDEX `country_id`(`i_country_id`) USING BTREE,
//  INDEX `group_id`(`i_group_id`) USING BTREE,
//  INDEX `oem_id`(`i_oem_id`) USING BTREE,
//  INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 926 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '找不到相关产品数据#1临时存储' ROW_FORMAT = Dynamic;
