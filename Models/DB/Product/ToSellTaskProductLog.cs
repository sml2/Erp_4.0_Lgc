﻿using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

namespace ERP.Models.Product;

[Index(nameof(TaskProductId), nameof(Pid))]
public class ToSellTaskProductLog : UserModel
{
    /// <summary>
    /// 日志备注信息
    /// </summary>

    [Comment("日志备注信息")]
    public string? Remark { get; set; }

    /// <summary>
    /// 跟卖产品id
    /// </summary>

    [Comment("跟卖产品id")]
    public int Pid { get; set; }

    /// <summary>
    /// 跟卖产品上传任务下任务产品id
    /// </summary>

    [Comment("跟卖产品上传任务下任务产品id")]
    public int TaskProductId { get; set; }

    /// <summary>
    /// 修改产品用户姓名
    /// </summary>

    [Comment("修改产品用户姓名")]
    public string? TrueName { get; set; }
}

//-- ----------------------------
//-- Table structure for erp3_to_sell_task_product_log
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_to_sell_task_product_log`;
//CREATE TABLE `erp3_to_sell_task_product_log`  (
//  `id` bigint unsigned NOT NULL,
//  `d_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '日志备注信息',
//  `i_pid` int (0) NOT NULL DEFAULT 0 COMMENT '跟卖产品id',
//  `i_task_product_id` int (0) NOT NULL DEFAULT 0 COMMENT '跟卖产品上传任务下任务产品id',
//  `u_truename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '修改产品用户姓名',
//  `i_user_id` int (0) NOT NULL DEFAULT 0 COMMENT '创建用户id',
//  `i_group_id` int (0) NOT NULL DEFAULT 0 COMMENT '创建用户组id',
//  `i_company_id` int (0) NOT NULL DEFAULT 0 COMMENT '公司id',
//  `i_oem_id` int (0) NOT NULL DEFAULT 0 COMMENT 'oem_id',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `pid`(`i_pid`) USING BTREE,
// INDEX `task_product_id`(`i_task_product_id`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 10470 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '产品管理_跟卖产品上传操作日志' ROW_FORMAT = Dynamic;
