﻿using ERP.Models.Product;

namespace ERP.Models.DB.Product
{
    public class ProductInfo
    {
        public ProductInfo(ProductModel productModel, ProductItemModel productItemModel)
        {
            ProductModel = productModel;
            ProductItemModel = productItemModel;
        }

        public ProductModel ProductModel { get; set; }

        public ProductItemModel ProductItemModel { get; set; }
    }
}
