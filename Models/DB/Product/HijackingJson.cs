﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
namespace ERP.Models.DB.Product
{
    public class UpdateHijackingJsons: List<UpdateHijackingJson>
    {        
    }

    public class DeleteHijackingJsons : List<DeleteHijackingJson>
    {
    }

    public class AddHijackingJsons : List<AddHijackingJson>
    { }


    public class listprices : List<list_price>
    {
    }


    /// <summary>
    /// 修改，上架，下架
    /// </summary>
    public class UpdateHijackingJson
    {
        [JsonIgnore]
        public string Sku { get; set; }

        [JsonIgnore]
        public string ProductType { get; set; }

        [JsonProperty("fulfillment_availability")]
        public List<fulfillment_availability> fulfillmentAvailability { get; set; }
        

        [JsonProperty("list_price")]
        public List<list_price> listPrices { get; set; }

      

        [JsonProperty("merchant_suggested_asin")]
        public List<merchant_suggested_asinC> merchantSuggestedAsin { get; set; }


        [JsonProperty("condition_type")]
        public List<Condition_type> conditionType { get; set; }

    }

    public class Condition_type
    {
        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("marketplace_id")]
        public string MarketplaceId { get; set; }
    }

    public class fulfillment_availability
    {
        [JsonProperty("fulfillment_channel_code")]
        public string fulfillmentChannelCode = "DEFAULT";

        [JsonProperty("lead_time_to_ship_max_days")]
        public int leadTimeToShipMaxDays { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }
    }

    public class list_price
    {
        [JsonProperty("value")]
        public decimal value { get; set; }
        [JsonProperty("currency")]
        public string currency { get; set; }
        [JsonProperty("marketplace_id")]
        public string marketplaceId { get; set; }
    }
    public class merchant_suggested_asinC
    {
        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("marketplace_id")]
        public string MarketplaceId { get; set; }
    }

    /// <summary>
    /// 删除
    /// </summary>
    public class DeleteHijackingJson 
    {
        [JsonProperty("merchant_suggested_asin")]
        public List<merchant_suggested_asinC> merchantSuggestedAsin { get; set; }

        public class merchant_suggested_asinC
        {
            [JsonProperty("value")]
            public string Value { get; set; }

            [JsonProperty("marketplace_id")]
            public string MarketplaceId { get; set; }
        }
        [JsonIgnore]
        public string Sku { get; set; }

        [JsonIgnore]
        public string ProductType { get; set; }       
    }



    public class AddHijackingJson: DeleteHijackingJson
    { }


}
