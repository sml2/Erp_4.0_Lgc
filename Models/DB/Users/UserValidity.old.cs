﻿//-- ----------------------------
//-- Table structure for erp3_user_validity
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_user_validity`;
//CREATE TABLE `erp3_user_validity`  (
//  `id` bigint unsigned NOT NULL,
//  `d_validity` int unsigned NOT NULL COMMENT '到期时间戳',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 10149 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户管理_到期时间' ROW_FORMAT = Dynamic;