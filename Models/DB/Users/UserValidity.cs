﻿using Microsoft.EntityFrameworkCore;
namespace ERP.Models.DB.Users;
using Abstract;

[Comment("用户管理_到期时间")]
public class UserValidity : CompanyModel
{
    /// <summary>
    /// 到期时间戳
    /// </summary>

    [Comment("到期时间戳")]
    public DateTime Validity { get; set; }
}
