﻿//-- ----------------------------
//-- Table structure for erp3_user_operate_log
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_user_operate_log`;
//CREATE TABLE `erp3_user_operate_log`  (
//  `id` bigint unsigned NOT NULL,
//  `i_user_id` int unsigned NOT NULL COMMENT '被操作用户id',
//  `i_operate_user_id` int unsigned NOT NULL COMMENT '操作用户id',
//  `u_operate_truename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作用户姓名',
//  `d_operate_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作用户IP',
//  `t_type` tinyint unsigned NOT NULL COMMENT '日志类型1用户2公司',
//  `d_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '备注',
//  `d_ext` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '拓展字段',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `type`(`t_type`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 16833 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户管理_用户信息日志' ROW_FORMAT = Dynamic;