﻿using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
namespace ERP.Models.DB.Users;

[Comment("用户管理_套餐表")]
public class Package : UserModel
{

    [Comment("名称")]
    public string? Name { get; set; }

    [Comment("权限组名称")]
    public int RuleTemplateId { get; set; }

    [Comment("备注")]
    public string? Remark { get; set; }

    [Comment("小米开放注册用户 赠送积分50")]
    public int GiveIntegral { get; set; }

    [Comment("推荐用户注册赠送积分 (首页推荐、未登录充值页 UI)")]
    public int Integral { get; set; }



    [Comment("真实有效期时间id")]
    public int RealityValidityId { get; set; }

    [Comment("真实到期时间戳")]
    public DateTime RealityValidity { get; set; }

    public int Uid { get; set; }
    public string? TrueName { get; set; }
    public string Pids { get; set; } = string.Empty;
    public int OpenUserNum { get; set; }
    public bool SecondPackageOem { get; set; }
}

