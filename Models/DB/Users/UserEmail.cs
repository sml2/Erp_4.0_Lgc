﻿using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;

namespace ERP.Models.DB.Users;

public class UserEmail : UserModel
{
    /// <summary>
    /// 邮箱名称
    /// </summary>

    [Comment("邮箱名称")]
    public string? Name { get; set; }

    /// <summary>
    /// 令牌
    /// </summary>

    [Comment("令牌")]
    public string? Token { get; set; }

    /// <summary>
    /// 邮箱配置id
    /// </summary>

    [Comment("邮箱配置id")]
    public int TypeId { get; set; }

    /// <summary>
    /// 同步标识
    /// </summary>

    [Comment("同步标识")]
    public string? Tag { get; set; }

    public enum States
    {
        [Description("启用")]
        ENABLE = 1,

        [Description("禁用")]
        DISABLE = 2
    }

    /// <summary>
    /// 1 启用 2禁用
    /// </summary>

    [Comment("1 启用 2禁用")]
    public States State { get; set; }

    /// <summary>
    /// 排序
    /// </summary>

    [Comment("排序")]
    public int Sort { get; set; }

    /// <summary>
    /// 邮箱地址
    /// </summary>

    [Comment("邮箱地址")]
    public string Address { get; set; } = string.Empty;

    /// <summary>
    /// 昵称
    /// </summary>

    [Comment("昵称")]
    public string? Nick { get; set; }

    /// <summary>
    /// 更新时间
    /// </summary>

    [Comment("更新时间")]
    public int Time { get; set; }

    /// <summary>
    /// 同步时间
    /// </summary>

    [Comment("同步时间")]
    public DateTime? SyncTime { get; set; }

    /// <summary>
    /// 同步消息
    /// </summary>

    [Comment("同步消息")]
    public string? SyncMsg { get; set; }
}


//-- ----------------------------
//-- Table structure for erp3_user_email
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_user_email`;
//CREATE TABLE `erp3_user_email`  (
//  `id` bigint unsigned NOT NULL,
//  `i_user_id` int unsigned NOT NULL COMMENT '用户id',
//  `i_group_id` int unsigned NOT NULL COMMENT '用户组id',
//  `i_company_id` int unsigned NOT NULL COMMENT '公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT 'oem_id',
//  `d_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '邮箱名称',
//  `d_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '令牌',
//  `i_type_id` int unsigned NOT NULL COMMENT '邮箱配置id',
//  `t_tag` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '同步标识',
//  `t_state` tinyint unsigned NOT NULL COMMENT '1 启用 2禁用',
//  `d_sort` int unsigned NOT NULL COMMENT '排序',
//  `d_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '邮箱地址',
//  `d_nick` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '昵称',
//  `d_time` int (0) NOT NULL DEFAULT 0 COMMENT '更新时间',
//  `d_sync_time` datetime(0) NULL DEFAULT NULL COMMENT '同步时间',
//  `d_sync_msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '同步消息',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `email_id`(`i_type_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户邮箱_配置信息' ROW_FORMAT = Dynamic;
