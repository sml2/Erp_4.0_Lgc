﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
namespace ERP.Models.DB.Users;
using Abstract;
[Comment("用户管理_用户信息日志")]
[Index(nameof(Type))]
public class UserOperateLog : UserModel
{
    /// <summary>
    /// 操作用户id
    /// </summary>

    [Comment("操作用户id")]
    public int OperateUserId { get; set; }

    /// <summary>
    /// 操作用户姓名
    /// </summary>

    [Comment("操作用户姓名")]
    public string? OperateTrueName { get; set; }

    public enum Types
    {
        [Description("用户")]
        USER = 1,

        [Description("公司")]
        COMPANY = 2
    }

    /// <summary>
    /// 操作用户IP
    /// </summary>

    [Comment("操作用户IP")]
    public string? OperateIp { get; set; }

    public Types Type { get; set; }

    /// <summary>
    /// 备注
    /// </summary>

    [Comment("备注")]
    public string? Remark { get; set; }

    /// <summary>
    /// 拓展字段
    /// </summary>

    [Comment("拓展字段")]
    public string? Ext { get; set; }
}