﻿using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;

namespace ERP.Models.DB.Users;

[Index(nameof(AllotTrueName), nameof(UserName))]
public class CompanyOem : CompanyModel
{
    /// <summary>
    /// 被分配用户用户名
    /// </summary>

    [Comment("被分配用户用户名")]
    public string? UserName { get; set; }

    /// <summary>
    /// 分配用户id
    /// </summary>

    [Comment("分配用户id")]
    public int AllotUserId { get; set; }

    /// <summary>
    /// 分配用户姓名
    /// </summary>

    [Comment("分配用户姓名")]
    public string? AllotTrueName { get; set; }
}

//-- ----------------------------
//-- Table structure for erp3_company_oem
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_company_oem`;
//CREATE TABLE `erp3_company_oem`  (
//  `id` bigint unsigned NOT NULL,
//  `i_oem_id` int unsigned NOT NULL COMMENT 'OEM',
//  `i_company_id` int unsigned NOT NULL COMMENT '拥有公司id',
//  `u_username` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '被分配用户用户名',
//  `i_allot_user_id` int unsigned NOT NULL COMMENT '分配用户id',
//  `u_allot_truename` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '分配用户姓名',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `allot_truename`(`u_allot_truename`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `username`(`u_username`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 77 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = 'OEM管理_用户分配' ROW_FORMAT = Dynamic;