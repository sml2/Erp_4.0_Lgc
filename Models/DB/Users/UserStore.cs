using ERP.Models.DB.Identity;
using ERP.Interface;
using ERP.Models.Abstract;
using ERP.Models.Stores;
using ERP.Models.Setting;
using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;

namespace ERP.Models.DB.Users;

public class UserStore : UserModel
{
    public UserStore() : base() { }
    public UserStore(User? user, Group? userGroup, Company? company, OEM? oem) : base(user, userGroup, company, oem) { }
    public UserStore(int UID, int groupID, int companyID, int oemID) : base(UID, groupID, companyID, oemID) { }
    public UserStore(ISessionProvider sessionProvider) : base(sessionProvider) { }
    public UserStore(ISession session) : base(session) { }

    public int CorrelationUserID { get; set; }
    public User CorrelationUser { get; set; } = default!;

    public int CorrelationStoreID { get; set; }
    public StoreRegion CorrelationStore { get; set; } = default!;
 

    /// <summary>
    /// 店铺平台
    /// </summary>
    [Comment("店铺平台")]
    public Platforms Platform { get; set; }

    /// <summary>
    /// 自动创建
    /// </summary>
    [Comment("自动创建")]
    public bool IsAutoCreated { get; set; } = false;
}