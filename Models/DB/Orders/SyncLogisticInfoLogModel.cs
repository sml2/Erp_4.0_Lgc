using ERP.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ERP.Models.View.Order.AmazonOrder;

namespace ERP.Models.DB.Orders
{
    public class SyncLogisticInfoLogModel : BaseModel
    {
        public SyncLogisticInfoLogModel()
        {
        }

        public SyncLogisticInfoLogModel(DeliveryGoodVM vm,int userId)
        {
            HandWrite = vm.HandWrite;
            CarrierName = vm.CarrierName;
            Country = vm.Country!;
            ShippingMethod = vm.ShippingMethod;
            UserID = userId;
        }

        

        public bool HandWrite { get; set; }
        public string CarrierName { get; set; }
        public string Country { get; set; }
        public int UserID { get; set; }
        public string? ShippingMethod { get; set; }
    }
}