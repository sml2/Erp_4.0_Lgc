using ERP.Enums.Orders;
using ERP.Models.Abstract;
using ERP.Models.DB.Orders;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.Models.DB.Orders;

[Index(nameof(OperateCompanyId), nameof(OperateUserId), nameof(WaybillCompanyId))]
public class Merge : UserModel
{
    /// <summary>
    /// 合并订单用户id
    /// </summary>

    [Comment("合并订单用户id")]
    public int OperateUserId { get; set; }//合并订单用户id

    /// <summary>
    /// 合并订单公司id
    /// </summary>

    [Comment("合并订单公司id")]
    public int OperateCompanyId { get; set; }//合并订单公司id

    /// <summary>
    /// 合并模式
    /// </summary>

    [Comment("合并模式")]
    public MergeTypes Type { get; set; }//合并模式 1仅主订单 2独立 3所有共享 4主订单+本订单

    /// <summary>
    /// 添加运单公司id
    /// </summary>

    [Comment("添加运单公司id")]
    public int WaybillCompanyId { get; set; }//添加运单公司id

    [Comment("下级上报合并订单公司ID")]
    public int OrderCompanyID { get; set; } = 0;

    /// <summary>
    /// 合并订单信息
    /// </summary>

    [Comment("合并订单信息")]
    [Column(TypeName = "json")]
    public string? OrderInfo { get; set; }//合并订单信息 pending_id待合并订单id order_id platform_id table_name url user_id拉取订单用户id nation_id国家id shop_id拉取订单店铺id order_no订单编号 product_num订单下产品数量 is_main 是否主产品1否2是 pay_money订单价格+货币单位 order_time订单创建时间戳时间戳 latest_ship_time承诺的订单发货时间范围的最后一天时间戳
}

//-- ----------------------------
//-- Table structure for erp3_order_merge
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_order_merge`;
//CREATE TABLE `erp3_order_merge`  (
//  `id` bigint unsigned NOT NULL,
//  `i_user_id` int unsigned NOT NULL COMMENT '订单所属用户id',
//  `i_company_id` int unsigned NOT NULL COMMENT '订单所属公司id',
//  `i_group_id` int unsigned NOT NULL COMMENT '当前用户组id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `i_operate_user_id` int unsigned NOT NULL COMMENT '合并订单用户id',
//  `i_operate_company_id` int unsigned NOT NULL COMMENT '合并订单公司id',
//  `t_type` tinyint unsigned NOT NULL COMMENT '合并模式 1仅主订单 2独立 3所有共享 4主订单+本订单',
//  `i_waybill_company_id` int unsigned NOT NULL COMMENT '添加运单公司id',
//  `d_order_info` json NULL COMMENT '合并订单信息 pending_id待合并订单id order_id platform_id table_name url user_id拉取订单用户id nation_id国家id shop_id拉取订单店铺id order_no订单编号 product_num订单下产品数量 is_main 是否主产品1否2是 pay_money订单价格+货币单位 order_time订单创建时间戳时间戳 latest_ship_time承诺的订单发货时间范围的最后一天时间戳 ',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `operate_company_id`(`i_operate_company_id`) USING BTREE,
// INDEX `operate_user_id`(`i_operate_user_id`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE,
// INDEX `waybill_company_id`(`i_waybill_company_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 58 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '订单管理_合并订单管理' ROW_FORMAT = Dynamic;
