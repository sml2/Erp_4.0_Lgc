using static ERP.Models.DB.Orders.Order;

namespace ERP.Models.DB.Orders;

public class OrderGoodSku : OrderHash
{
    public OrderGoodSku()
    {
    }
    public OrderGoodSku(int orderId, Good good) : base(orderId, good.Sku ?? string.Empty, good.QuantityOrdered)
    {
    }
}
