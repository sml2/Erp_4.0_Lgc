﻿
using ERP.Models.Stores;
using static OrderSDK.Modles.Shopee.Payment.ResponseGetEscrowDetail;

namespace ERP.Models.DB.Orders;
public partial class Order
{
    public Order(StoreRegion store, Shopee o, orderIncome orderIncome)
    {
        Plateform = store.Platform;
        EntryMode = Enums.Orders.EntryModes.APIAutomatic;

        UserID = store.UserID;
        GroupID = store.GroupID;
        CompanyID = store.CompanyID;
        OEMID = store.OEMID;

        Store = store;
        ShopID = Store.GetShopeeData()!.ShopID.ToString();
        OrderNo = o.OrderSn;
        CreateTime = Shopee.ConvertLongToDateTime(o.CreateTime);
        throw new NotImplementedException(nameof(o.OrderStatus));
        //State = o.OrderStatus;
        BuyerUserName = o.BuyerUserName;
        LastUpdateDate = Shopee.ConvertLongToDateTime(o.UpdateTime);
        PaymentMethod = o.PaymentMethod;
        InvoiceNumber = o.invoice_data.Number;
        OrderTotal = new MoneyMeta(o.Currency, o.TotalAmount);
        ShippingMoney = new MoneyMeta(o.Currency, o.ActualShippingFee);

        var address = o.RecipientAddress;
        Receiver = new()
        {
            Name = address.Name,
            Phone = address.Phone,
            Zip = address.Zipcode,
            Province = address.Town,
            District = address.District,
            City = address.City,
            Nation = address.State,
            NationShort = address.Region,
            Address1 = address.FullAddress,
        };



        GoodsScope(GoodList =>
        {
            var itemOrders = o.ItemList;
            foreach (var item in itemOrders)
            {
                Good or = new Good();
                or.Name = item.ItemName;
                or.Sku = item.ItemSku;
                or.ImageURL = item.ImageInfo.image_url;
                or.QuantityOrdered = item.ModelQuantityPurchased;
                or.QuantityShipped = item.ModelQuantityPurchased;
                //产品价格
                or.ItemPrice = new MoneyMeta()
                {
                    CurrencyCode = o.Currency,
                    Amount = Convert.ToDecimal(item.ModelOriginalPrice),
                };
                //折后价格
                or.promotionDiscount = (new MoneyMeta(o.Currency, item.ModelDiscountedPrice)).ToRecode();
                GoodList.Add(or);
            }
        });

        //对卖家使用的税和退费

        //手续费 如果有为空报错 可以修改模型或者改用?.Linq
        double fee = 0;
        //退款
        double refund = 0;

        //税 2021.4.8 董哥要求
        //平台运费补贴给卖方
        //购物者以主要货币向卖方收取的额外服务费用
        //shopee 平台佣金
        //购物者向卖家收取的额外服务费用
        //卖方为订单支付的交易费
        //印尼跨境费
        //卖方作为托管的一部分必须承担的最终调整金额
        fee = orderIncome.ShopeeShippingRebate + orderIncome.ServiceFeePri + orderIncome.CommissionFee + orderIncome.ServiceFee +
             orderIncome.SellerTransactionFee + orderIncome.EscrowTax + orderIncome.FinalShippingFee;

        //卖方退费
        //如果以主要货币部分返还，则返还给卖方的金额
        //部分退货时退还给卖方的金额。
        refund = orderIncome.ShopeeDiscount + orderIncome.SellerReturnRefundPri + orderIncome.SellerReturnRefund;
    }



}