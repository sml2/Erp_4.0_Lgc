
using Newtonsoft.Json;
using ERP.Enums.Orders;
using newAmazon = OrderSDK.Modles.Amazon;
using ERP.Models.DB.Stores;
using ERP.Services.Api.Amazon;
using ERP.Extensions;
using ERP.Models.Stores;

namespace ERP.Models.DB.Orders;


public partial class Order
{   
    public Order(StoreRegion store, newAmazon.Order.Order orderBasic, List<newAmazon.Order.OrderItem> orderItems, newAmazon.Finances.ShipmentEventList? shipmentEventList,
      newAmazon.Finances.ShipmentEventList? refundEventList, Services.Caches.Nation nations, Services.Caches.PlatformData cache)
    {
        Plateform = store.Platform;
        UserID = store.UserID;
        GroupID = store.GroupID;
        CompanyID = store.CompanyID;
        OEMID = store.OEMID;
        MergeId = -1;
        Store = store;
        ShopID = Store.GetSPAmazon()!.SellerID;
        OrderNo = orderBasic.AmazonOrderId;
        OrderNoHash = Helpers.CreateHashCode(OrderNo);
        MarketPlace = orderBasic.MarketplaceId;
        EntryMode = Enums.Orders.EntryModes.APIAutomatic;
        BuyerUserName = orderBasic.BuyerInfo.BuyerName;
        PaymentMethod = orderBasic.PaymentMethod.IsNotNull() ? orderBasic.PaymentMethod.Value.GetName() : string.Empty;
        CreateTimeRaw = orderBasic.PurchaseDate;
        PayTimeRaw = orderBasic.PurchaseDate;
        if (!string.IsNullOrWhiteSpace(orderBasic.PurchaseDate))
        {
            CreateTime = Convert.ToDateTime(orderBasic.PurchaseDate);
            PayTime = Convert.ToDateTime(orderBasic.PurchaseDate);
        }            
        if (!string.IsNullOrWhiteSpace(orderBasic.LatestShipDate))
        {
            LatestShipTime = Convert.ToDateTime(orderBasic.LatestShipDate);
        }
        if (!string.IsNullOrWhiteSpace(orderBasic.LastUpdateDate))
        {
            LastUpdateDate = Convert.ToDateTime(orderBasic.LastUpdateDate);
        }      
        DefaultShipFromLocationAddress = JsonConvert.SerializeObject(orderBasic.DefaultShipFromLocationAddress);
        var address = orderBasic.ShippingAddress;

        if (orderBasic.ShippingAddress.IsNotNull())
        {
            var nation = nations.Get(address.CountryCode) ?? nations.Get(cache.GetAmazonNationId(store.Platform, orderBasic.MarketplaceId));
            Receiver = new()
            {
                Name = address.Name,
                Phone = address.Phone,
                Email = orderBasic.BuyerInfo.BuyerEmail,
                Zip = address.PostalCode,
                NationId = nation?.ID,
                Nation = nation?.Name,
                NationShort = nation?.Short,
                Province = address.StateOrRegion,
                City = string.IsNullOrWhiteSpace(address.City) ? address.StateOrRegion : address.City,
                County = address.County,
                District = address.District,
                Address1 = address.AddressLine1,
                Address2 = address.AddressLine2,
                Address3 = address.AddressLine3,
            };
        }
        else
        {
            var nation = nations.Get(cache.GetAmazonNationId(store.Platform, orderBasic.MarketplaceId));
            Receiver = new()
            {
                Name = "",
                Phone = "",
                Email = "",
                Zip = "",
                NationId = nation?.ID,
                Nation = nation?.Name,
                NationShort = nation?.Short,
                Province = "",
                City = "",
                County = "",
                District = "",
                Address1 = "",
                Address2 = "",
                Address3 = "",
            };
        }
        OrderTotal = orderBasic.OrderTotal.IsNotNull() ? orderBasic.OrderTotal.Mate : new Data.MoneyMeta();

        GoodsScope(GoodList =>
        {
            var NumTemp = 0;
            foreach (var item in orderItems)
            {
                //delivery
                //unit -> mate
                Good good = new();
                good.ID = item.ASIN;//asin
                good.FromURL = $"{cache.GetOrg(store.Platform, orderBasic.MarketplaceId)}/dp/{item.ASIN}";//from_url
                good.Name = item.Title;//name
                good.Sku = item.SellerSKU;//seller_sku
                var ItemAttributes = item.AttributeSet;
                good.ImageURL = ItemAttributes.SmallImage.IsNull() ? "" : ItemAttributes.SmallImage.URL;
                good.Brand = ItemAttributes.Brand.IsNull() ? "" : ItemAttributes.Brand;
                good.Size = ItemAttributes.Size.IsNull() ? "" : ItemAttributes.Size;
                good.Color = ItemAttributes.Color.IsNull() ? "" : ItemAttributes.Color;
                NumTemp += item.QuantityOrdered;
                good.QuantityOrdered = item.QuantityOrdered;//num
                good.QuantityShipped = item.QuantityShipped;//num-send_num? 已发货数量
                                                            //产品价格
                good.ItemPrice = item.ItemPrice.IsNotNull() ? item.ItemPrice.Mate : new Data.MoneyMeta();// var product_money
                good.ShippingPrice = item.ShippingPrice.IsNotNull() ? item.ShippingPrice.Mate : new Data.MoneyMeta();// var shipping_money,amazon_shipping_money
                good.UnitPrice = good.QuantityOrdered > 0 ? good.ItemPrice with { Money = good.ItemPrice.Money / good.QuantityOrdered } : -1; //price
                good.TotalPrice = good.ItemPrice with { Money = good.ItemPrice.Money + good.ShippingPrice.Money };//total
                                                                                                                  //折后价格
                good.promotionDiscount = item.PromotionDiscount.IsNotNull() ? item.PromotionDiscount.Mate : new Data.MoneyMeta();
                good.ShippingTax = item.ShippingTax.IsNotNull() ? item.ShippingTax.Mate : new Data.MoneyMeta();//shipping_tax
                good.ShippingDiscountTax = item.ShippingDiscountTax.IsNotNull() ? item.ShippingDiscountTax.Mate : new Data.MoneyMeta();
                good.PromotionDiscountTax = item.PromotionDiscountTax.IsNotNull() ? item.PromotionDiscountTax.Mate : new Data.MoneyMeta();
                good.ItemTax = item.ItemTax.IsNotNull() ? item.ItemTax.Mate : new Data.MoneyMeta();//item_tax
                GoodList.Add(good);
            }
            //ShippingMoney = new(GoodList.Sum(x => x.ShippingPrice.Money));

            //无需断言两者相等 中间会有很多杂项因素(税费，佣金等)导致价格不匹配
            //Debug.Assert( OrderTotal.Money == ShippingMoney.Money + ProductMoney.Money );
            ProductNum = NumTemp;

        }, Modes.Save);

        Delivery = orderBasic.NumberOfItemsShipped;
        DeliveryNo = orderBasic.NumberOfItemsUnshipped;
        State = ConvertState.GetState(orderBasic.OrderStatus);
        ShippingType = ConvertState.ShippingTypes(orderBasic.FulfillmentChannel);
        //public MoneyMeta Mate { get => new(CurrencyCode, Amount); }
        
        FeesEstimate = orderItems.Sum(s => s.TotalFeesEstimate.IsNull() ? 0 : new MoneyMeta(s.TotalFeesEstimate.CurrencyCode, (decimal)s.TotalFeesEstimate.Amount));
        if (ShippingType == ShippingTypes.AFN)
        {
            Bits |= (BITS)ShippingTypes.AFN;
        }
        else
        {
            switch (State)
            {
                case States.CANCELED:
                    Bits |= BITS.Progresses_Error_Cancel;
                    break;
                case States.UNPAID:
                    Bits |= BITS.Progresses_Error_Unpaid;
                    break;
                case States.DELIVERY:
                case States.PARTIALLYSHIPPED:
                case States.SENT:
                    if (ProductNum == Delivery)
                        Bits |= BITS.Progresses_Complete;
                    else
                    {
                        Bits |= BITS.Progresses_Unprocessed;
                        if (Delivery == 0)
                        {
                            Bits |= BITS.Progresses_Unprocessed;
                        }
                        else {
                            Bits |= BITS.Progresses_Shipment_Success;
                        }
                    }
                    break;

                default:
                    Bits |= BITS.Progresses_Unprocessed;
                    break;
            }
        };

        //手续费 如果有为空报错 可以修改模型或者改用?.Linq
        double fee = 0;
        decimal x1 = 0;
        decimal x2 = 0;
        decimal x3 = 0;
        //退款
        double refund = 0;
        var ShipmentItems = shipmentEventList;
        if (ShipmentItems is not null && ShipmentItems.Count > 0)
        {
            foreach (var ShipmentItem in ShipmentItems)
            {
                if (ShipmentItem.IsNotNull() && ShipmentItem.ShipmentItemList.IsNotNull() && ShipmentItem.ShipmentItemList.Count > 0)
                {
                    foreach (var ItemTaxWithheldList in ShipmentItem.ShipmentItemList)
                    {
                        if (ItemTaxWithheldList.IsNotNull() && ItemTaxWithheldList.ItemTaxWithheldList.IsNotNull() && ItemTaxWithheldList.ItemTaxWithheldList.Count > 0)
                        {
                            foreach (var TaxesWithheld in ItemTaxWithheldList.ItemTaxWithheldList)
                            {
                                if (TaxesWithheld.IsNotNull() && TaxesWithheld.TaxesWithheld.IsNotNull() && TaxesWithheld.TaxesWithheld.Count > 0)
                                {
                                    foreach (var ChargeAmount in TaxesWithheld.TaxesWithheld)
                                    {
                                        if (ChargeAmount.IsNotNull())
                                        {
                                            fee += ChargeAmount.ChargeAmount.CurrencyAmount;
                                        }
                                    }
                                }
                            }

                        }
                        if (ItemTaxWithheldList.IsNotNull() && ItemTaxWithheldList.ItemFeeList.IsNotNull() && ItemTaxWithheldList.ItemFeeList.Count > 0)
                        {
                            //佣金
                            foreach (var FeeComponent in ItemTaxWithheldList.ItemFeeList)
                            {
                                if (FeeComponent.IsNotNull())
                                {
                                    fee += FeeComponent.FeeAmount.CurrencyAmount;
                                }
                            }
                        }
                    }
                }
            }
        }

        if (refundEventList is not null && refundEventList.Count > 0)
        {
            foreach (var ShipmentItem in refundEventList)
            {
                if (ShipmentItem.ShipmentItemList.IsNotNull() && ShipmentItem.ShipmentItemList.Count > 0)
                {
                    foreach (var ShipmentItemList in ShipmentItem.ShipmentItemList)
                    {
                        if (ShipmentItemList.ItemTaxWithheldList.IsNotNull() && ShipmentItemList.ItemTaxWithheldList.Count > 0)
                        {
                            foreach (var ItemTaxWithheldList in ShipmentItemList.ItemTaxWithheldList)
                            {
                                if (ItemTaxWithheldList.TaxesWithheld.IsNotNull() && ItemTaxWithheldList.TaxesWithheld.Count > 0)
                                {
                                    foreach (var ChargeComponent in ItemTaxWithheldList.TaxesWithheld)
                                    {
                                        //税 2021.4.8 董哥要求
                                        if (ChargeComponent.ChargeAmount.IsNotNull())
                                        {
                                            fee += ChargeComponent.ChargeAmount.CurrencyAmount;
                                        }
                                    }

                                }
                            }
                        }
                        if (ShipmentItemList.ItemFeeAdjustmentList.IsNotNull())
                        {
                            foreach (var ItemFeeAdjustmentList in ShipmentItemList.ItemFeeAdjustmentList)
                            {
                                //退款手续费
                                if (ItemFeeAdjustmentList.IsNotNull())
                                {
                                    fee += ItemFeeAdjustmentList.FeeAmount.CurrencyAmount;
                                }
                            }
                        }
                        if (ShipmentItemList.ItemChargeList.IsNotNull())
                        {
                            foreach (var ItemChargeList in ShipmentItemList.ItemChargeList)
                            {
                                //退款金额
                                if (ItemChargeList.IsNotNull())
                                {
                                    refund += ItemChargeList.ChargeAmount.CurrencyAmount;
                                }
                            }
                        }
                    }
                }
            }
        }
        
        Fee = new MoneyMeta(orderBasic.OrderTotal.CurrencyCode,(decimal)fee);
        if (Fee.Raw.Amount > 0)
        {
            State = States.SENT;
        }

        Refund = new MoneyMeta(orderBasic.OrderTotal.CurrencyCode, (decimal)refund);
        var p = OrderTotal.Raw.Amount - Fee.Raw.Amount - Refund.Raw.Amount;
        Profit = new MoneyMeta(orderBasic.OrderTotal.CurrencyCode, p); 
    }

    /// <summary>
    /// 根据订单基本信息构造
    /// </summary>
    /// <param name="store"></param>
    /// <param name="orderBasic"></param>
    /// <param name="nations"></param>
    /// <param name="cache"></param>
    public Order(StoreRegion store, newAmazon.Order.Order  orderBasic, Services.Caches.Nation nations, Services.Caches.PlatformData cache)
    {
        Plateform = store.Platform;
        UserID = store.UserID;
        GroupID = store.GroupID;
        CompanyID = store.CompanyID;
        OEMID = store.OEMID;
        MergeId = -1;
        Store = store;
        StoreId = store.ID;
        ShopID = Store.GetSPAmazon()!.SellerID;
        OrderNo = orderBasic.AmazonOrderId;
        OrderNoHash = Helpers.CreateHashCode(OrderNo);
        MarketPlace = orderBasic.MarketplaceId;
        EntryMode = Enums.Orders.EntryModes.APIAutomatic;
        BuyerUserName = orderBasic.BuyerInfo.BuyerName;
        PaymentMethod = orderBasic.PaymentMethod.IsNotNull() ? orderBasic.PaymentMethod.Value.GetName() : String.Empty;
        CreateTimeRaw = orderBasic.PurchaseDate;
        CreateTime = Convert.ToDateTime(orderBasic.PurchaseDate);
        PayTimeRaw = orderBasic.PurchaseDate;
        PayTime = Convert.ToDateTime(orderBasic.PurchaseDate);
        LatestShipTime = Convert.ToDateTime(orderBasic.LatestShipDate);
        LastUpdateDate = Convert.ToDateTime(orderBasic.LastUpdateDate);
        pullOrderState = PullOrderState.Item;
        PullCount = 1;
        DefaultShipFromLocationAddress = JsonConvert.SerializeObject(orderBasic.DefaultShipFromLocationAddress);
        OrderTotal = orderBasic.OrderTotal.IsNotNull() ? orderBasic.OrderTotal.Mate : new Data.MoneyMeta();
        Delivery = orderBasic.NumberOfItemsShipped;
        DeliveryNo = orderBasic.NumberOfItemsUnshipped;
        State = ConvertState.GetState(orderBasic.OrderStatus);
        ShippingType = ConvertState.ShippingTypes(orderBasic.FulfillmentChannel);
       
        if (ShippingType == ShippingTypes.AFN)
        {
            Bits = (BITS)ShippingTypes.AFN;
        }
        else
        {
            switch (State)
            {
                case States.CANCELED:
                    Bits = BITS.Progresses_Error_Cancel;
                    break;
                case States.UNPAID:
                    Bits = BITS.Progresses_Error_Unpaid;
                    break;
                case States.DELIVERY:
                case States.PARTIALLYSHIPPED:
                case States.SENT:
                    if (ProductNum == Delivery)
                        Bits = BITS.Progresses_Complete;
                    else
                    {
                        Bits = BITS.Progresses_Unprocessed;
                        if (Delivery == 0)
                        {
                            Bits |= BITS.Progresses_Unprocessed;
                        }
                        else
                        {
                            Bits |= BITS.Progresses_Shipment_Success;
                        }
                    }
                    break;

                default:
                    Bits = BITS.Progresses_Unprocessed;
                    break;
            }
        };        
    }
}