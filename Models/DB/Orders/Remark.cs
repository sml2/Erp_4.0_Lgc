using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel.DataAnnotations;

namespace ERP.Models.DB.Orders;

[Index(nameof(OrderId))]
public class Remark : UserModel
{
    /// <summary>
    /// 订单id
    /// </summary>

    [Comment("订单id")]
    public int OrderId { get; set; }//订单id

    /// <summary>
    /// 备注信息
    /// </summary>

    [Comment("备注信息")]
    public string? Value { get; set; }//备注信息

    /// <summary>
    /// 添加备注用户名
    /// </summary>

    [Comment("添加备注用户名")]
    public string? UserName { get; set; }//添加备注用户名

    /// <summary>
    /// 添加备注用户姓名
    /// </summary>

    [Comment("添加备注用姓名")]

    public string? Truename { get; set; }//添加备注用户姓名
}


//-- ----------------------------
//-- Table structure for erp3_amazon_order_remark
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_amazon_order_remark`;
//CREATE TABLE `erp3_amazon_order_remark`  (
//  `id` bigint unsigned NOT NULL,
//  `i_user_id` int unsigned NOT NULL COMMENT '添加备注用户id',
//  `i_group_id` int unsigned NOT NULL COMMENT '当前用户组id',
//  `i_company_id` int unsigned NOT NULL COMMENT '当前拉取用户所在公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `i_order_id` int unsigned NOT NULL COMMENT '订单id',
//  `d_remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '备注信息',
//  `d_user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '添加备注用户名',
//  `d_truename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '添加备注用户姓名',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `order_id`(`i_order_id`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 11361 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '亚马逊订单_订单备注' ROW_FORMAT = Dynamic;