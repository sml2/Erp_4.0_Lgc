﻿//-- ----------------------------
//-- Table structure for erp3_amazon_order
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_amazon_order`;
//CREATE TABLE `erp3_amazon_order`  (
//  `id` bigint unsigned NOT NULL,
//  `i_user_id` int unsigned NOT NULL COMMENT '当前拉取用户id',
//  `i_group_id` int unsigned NOT NULL COMMENT '当前用户组id',
//  `i_company_id` int unsigned NOT NULL COMMENT '当前拉取用户所在公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `u_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '订单下第一个商品远程图片url',
//  `d_goods` json NULL COMMENT '订单商品快照 num:1,url:商品图片,asin:B0859RZXKG,name:名称,unit:货币单位,price:单价,total:总价,good_id:goodid5ee880f21e51f,from_url:商品网址,send_num:待发货数,seller_sku:sku,delivery:已发货数',
//  `t_entry_mode` tinyint unsigned NOT NULL COMMENT '录入方式 1自定义录入 ，2API录入，',
//  `d_order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '订单编号',
//  `d_order_no_hash` bigint unsigned NOT NULL COMMENT 'order_no  createHashCode',
//  `t_shipping_type` tinyint unsigned NOT NULL COMMENT '订单配送方式1亚马逊配送 (AFN) 2卖家自行配送 (MFN)',
//  `d_user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '买家名称',
//  `d_pay_time` datetime(0) NULL DEFAULT NULL COMMENT '订单付款时间戳',
//  `d_buyer_message` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '买家附言',
//  `d_buyer_invoice` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '买家发票信息',
//  `d_sign_time` datetime(0) NULL DEFAULT NULL COMMENT '买家签收时间',
//  `d_receiver_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '收货人的电话',
//  `i_receiver_nation_id` int unsigned NOT NULL COMMENT '国家id(数据字典nation 主键)',
//  `d_receiver_nation_short` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '国家简码',
//  `d_receiver_nation` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '收货人所在国家',
//  `d_receiver_province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '收货人所在州/省/地区',
//  `d_receiver_city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '收货人所在城市',
//  `d_receiver_district` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '收货人所在区',
//  `d_receiver_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '收货人详细地址',
//  `d_receiver_address2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '收货人详细地址',
//  `d_receiver_address3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '收货人详细地址',
//  `d_receiver_zip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '收货人邮编',
//  `d_receiver_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '收货人姓名',
//  `d_receiver_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '收货人邮箱',
//  `i_store_id` int unsigned NOT NULL COMMENT '卖家店铺id（平台-商店）',
//  `d_seller_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '卖家店铺seller_id',
//  `d_seller_memo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '卖家对订单的备注',
//  `d_consign_time` datetime(0) NULL DEFAULT NULL COMMENT '卖家发货时间',
//  `d_product_money` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '商品总价(基准货币)',
//  `d_profit` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '订单获利(基准货币)',
//  `d_order_money` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '订单总价(成交价-基准货币)',
//  `d_shipping_money` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '订单运费(基准货币)',
//  `d_pay_money` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '订单实付金额(基准货币)',
//  `t_order_state` tinyint(0) NOT NULL DEFAULT 1 COMMENT '订单状态 1未付款2取消6配货7待发8已发9交付',
//  `t_review_state` tinyint unsigned NOT NULL COMMENT '订单评价状态 0为未评价 1为已评价 2为已追评',
//  `d_create_time` datetime(0) NULL DEFAULT NULL COMMENT '订单创建时间戳',
//  `d_finish_time` datetime(0) NULL DEFAULT NULL COMMENT '订单完成时间',
//  `d_coin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '货币单位(拉取单位)',
//  `d_delivery_time` datetime(0) NULL DEFAULT NULL COMMENT '承诺发货时间',
//  `d_shipping_time` datetime(0) NULL DEFAULT NULL COMMENT '发货时间',
//  `d_last_time` datetime(0) NULL DEFAULT NULL COMMENT '承诺送达时间',
//  `d_delivery` int unsigned NOT NULL COMMENT '已配送商品数量',
//  `d_delivery_no` int unsigned NOT NULL COMMENT '未配送商品数量',
//  `d_product_num` int unsigned NOT NULL COMMENT '购买商品数量',
//  `d_fee` decimal(22, 6) NOT NULL DEFAULT 0.000000 COMMENT '手续费(基准货币)',
//  `d_rate` decimal(22, 6) NOT NULL DEFAULT 0.000000 COMMENT '汇率(拉取订单当时汇率)',
//  `d_fake_product_time` datetime(0) NULL DEFAULT NULL COMMENT '假发货时间',
//  `d_true_product_time` datetime(0) NULL DEFAULT NULL COMMENT '真发货时间',
//  `d_deliver_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '最后一次发货亚马逊返回查询id',
//  `t_submit_state` tinyint unsigned NOT NULL COMMENT '所属最后一次真假发货提交状态1暂无提交2已提交3提交成功4提价失败公司id',
//  `d_latest_ship_time` datetime(0) NULL DEFAULT NULL COMMENT '您承诺的订单发货时间范围的最后一天',
//  `d_earliest_ship_time` datetime(0) NULL DEFAULT NULL COMMENT '您承诺的订单发货时间范围的第一天',
//  `u_truename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '当前拉取用户姓名',
//  `d_timestamp` datetime(0) NULL DEFAULT NULL COMMENT '拉取订单时间戳',
//  `d_refund` decimal(22, 6) NOT NULL DEFAULT 0.000000 COMMENT '订单退款金额(基准货币)',
//  `d_refund_bill` int unsigned NOT NULL COMMENT '退款账单id(舍弃)',
//  `d_loss` decimal(22, 6) NOT NULL DEFAULT 0.000000 COMMENT '订单损耗CNY(基准货币)',
//  `d_purchase_fee` decimal(22, 6) NOT NULL DEFAULT 0.000000 COMMENT '采购花费(基准货币)',
//  `b_progress` bigint unsigned NOT NULL COMMENT '订单进度 0 => 未 处 理, 1 => 处 理 中, 2 => 完 成 (交付), 3 =>退款, 4 =>取 消, 5 =>补 发, 6 =>F B A, 7 => 仓库已发货, 8 => 仓库已收货,9 => 异常订单',
//  `i_merge_id` int(0) NOT NULL DEFAULT -1 COMMENT '合并订单id',
//  `t_is_main` tinyint unsigned NOT NULL COMMENT '1副订单2主订单',
//  `i_operate_user_id` int unsigned NOT NULL COMMENT '操作用户id',
//  `d_purchase_num` int unsigned NOT NULL COMMENT '采购确认商品数',
//  `d_delivery_num` int unsigned NOT NULL COMMENT '物流发货数',
//  `d_delivery_fail` int unsigned NOT NULL COMMENT '物流发货失败数',
//  `d_process_waybill` tinyint unsigned NOT NULL COMMENT '第一个添加运单用户0未添加1内部2上级',
//  `d_process_purchase` tinyint unsigned NOT NULL COMMENT '第一个添加采购用户0未添加1内部2上级',
//  `i_purchase_company_id` int unsigned NOT NULL COMMENT '采购上报公司id',
//  `i_waybill_company_id` int unsigned NOT NULL COMMENT '物流上报公司id',
//  `u_express` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '运单主键ID(逗号连接)',
//  `u_purchase_tracking_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '采购主键ID(逗号连接)',
//  `t_is_batch_ship` tinyint unsigned NOT NULL COMMENT '是否加入批量待发货订单列表 1否2是',
//  `d_distribution_address` json NULL COMMENT '分销收货地址',
//  `t_send_mode` tinyint(1) NOT NULL DEFAULT 1 COMMENT '发货模式1代发货2下级发货',
//  `t_purchase_audit` tinyint(1) NOT NULL DEFAULT 0 COMMENT '采购是否需要审核',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  `d_invoice` json NULL COMMENT '发票',
//  `d_default_ship_from_location_address` json NULL COMMENT '店铺默认发货地址',
//  PRIMARY KEY (`id`) USING BTREE,
//  INDEX `company_id`(`i_company_id`) USING BTREE,
//  INDEX `group_id`(`i_group_id`) USING BTREE,
//  INDEX `is_main`(`t_is_main`) USING BTREE,
//  INDEX `merge_id`(`i_merge_id`) USING BTREE,
//  INDEX `oem_id`(`i_oem_id`) USING BTREE,
//  INDEX `operate_user_id`(`i_operate_user_id`) USING BTREE,
//  INDEX `order_no`(`d_order_no`) USING BTREE,
//  INDEX `order_no_hash`(`d_order_no_hash`) USING BTREE,
//  INDEX `order_state`(`t_order_state`) USING BTREE,
//  INDEX `process_waybill`(`d_process_waybill`) USING BTREE,
//  INDEX `progress`(`b_progress`) USING BTREE,
//  INDEX `purchase_company_id`(`i_purchase_company_id`) USING BTREE,
//  INDEX `store_id`(`i_store_id`) USING BTREE,
//  INDEX `timestamp`(`d_timestamp`) USING BTREE,
//  INDEX `user_id`(`i_user_id`) USING BTREE,
//  INDEX `waybill_company_id`(`i_waybill_company_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 208457 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '亚马逊订单_订单列表' ROW_FORMAT = Dynamic;
