﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using OrderSDK.Modles.Amazon.Order;

namespace ERP.Models.DB.Orders;
using Extensions;
public partial class Order
{
    public string GetGoodsRaw() => InternalGoods;
    public int GoodsCount() => GoodsScope().Count;
    public bool HasGoods() => GoodsCount() > 0;

    /// <summary>
    /// 订单商品快照 
    /// </summary>
    private string InternalGoods = "[]";
    [BackingField(nameof(InternalGoods)),Column(nameof(Goods)), Comment("订单商品快照")]
    [NJI, SJI]
    public string GoodData { get => throw new InvalidOperationException(); set => throw new InvalidOperationException(); } 

    private List<Good>? Goods;
    public enum Modes
    {
        None,
        Serialize,
        UpdateGoodsMoney,
        SerializeAndUpdateGoodsMoney,
        Save = SerializeAndUpdateGoodsMoney,          
    }
    public delegate List<Good> GoodsHandleRET(List<Good> goods);
    public ReadOnlyCollection<Good> GoodsScope(GoodsHandleRET? handle = null, Modes mode = Modes.Serialize)
    {
        if (Goods is null)
            Goods = JsonConvert.DeserializeObject<List<Good>>(InternalGoods)!;
        if (handle is not null)
        {            
            var Temp = handle(Goods);
            Goods = Temp is null ? new() : Temp;
            if (mode == Modes.SerializeAndUpdateGoodsMoney || mode == Modes.Serialize)
            {
                InternalGoods = JsonConvert.SerializeObject(Goods);
                string? UrlTemp = null;
                foreach (var item in Goods)
                {
                    if (item.ImageURL.IsNotWhiteSpace())
                    {
                        UrlTemp = item.ImageURL;
                        break;
                    }
                }
                Url = UrlTemp;
            }
          
            if (mode == Modes.SerializeAndUpdateGoodsMoney || mode == Modes.UpdateGoodsMoney)
            {
                GoodsMoney = Goods.Sum(x => x.TotalPrice);
            }           
        }
        return Goods.AsReadOnly();
    }

    public void ClearGoods()
    {
        Goods = new();
    }

    public delegate void GoodsHandleOUT(ref List<Good> goods);
    public ReadOnlyCollection<Good> GoodsScope(GoodsHandleOUT handle, Modes mode = Modes.Serialize) => GoodsScope((List<Good> good) => { handle(ref good); return good; }, mode);
    public delegate void GoodsHandle(List<Good> goods);
    public ReadOnlyCollection<Good> GoodsScope(GoodsHandle handle, Modes mode = Modes.Serialize) => GoodsScope((List<Good> good) => { handle(good); return good; }, mode);
    //,,,,unit:货币单位,price:单价
    /// <summary>
    /// 订单产品快照
    /// </summary>
    [NotMapped]
    public class Good
    {

        public string? ID { get; set; }//good_id:goodid5ee880f21e51f，asin:B0859RZXKG
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }= string.Empty;//name:名称
        /// <summary>
        /// SKU
        /// </summary>
        public string? Sku { get; set; } //seller_sku:sku

        private string? _orderItemId;
        public string? OrderItemId
        {
            get => string.IsNullOrEmpty(_orderItemId) ? ID : _orderItemId;
            set => _orderItemId = value;
        } //seller_sku:sku

        /// <summary>
        /// 图片
        /// </summary>
        public string ImageURL { set; get; } = string.Empty;//url？
        /// <summary>
        /// 品牌
        /// </summary>
        public string Brand { set; get; } = string.Empty;
        /// <summary>
        ///尺寸/大小/型号
        /// </summary>
        public string Size { set; get; } = string.Empty;
        /// <summary>
        /// 颜色
        /// </summary>
        public string Color { set; get; } = string.Empty;
        /// <summary>
        /// 单价 <see cref="ItemPrice"/>/<see cref="QuantityOrdered"/>
        /// </summary>
        public MoneyRecord UnitPrice { get; set; } = MoneyRecord.Empty;

        /// <summary>
        /// 价格总价
        /// </summary>
        public MoneyRecord ItemPrice { get; set; } = MoneyRecord.Empty;//total:总价

        /// <summary>
        /// 运费
        /// </summary>
        public MoneyRecord ShippingPrice { get; set; } = MoneyRecord.Empty;

        /// <summary>
        /// 促销折扣
        /// </summary>
        public MoneyRecord promotionDiscount { get; set; } = MoneyRecord.Empty;

        /// <summary>
        /// 产品税
        /// </summary>
        public MoneyRecord? ItemTax { get; set; } = MoneyRecord.Empty;

        /// <summary>
        /// 船运税
        /// </summary>
        public MoneyRecord? ShippingTax { get; set; } = MoneyRecord.Empty;

        /// <summary>
        /// 航运折扣税
        /// </summary>
        public MoneyRecord? ShippingDiscountTax { get; set; } = MoneyRecord.Empty;

        /// <summary>
        /// 促销折扣税
        /// </summary>
        public MoneyRecord? PromotionDiscountTax { get; set; } = MoneyRecord.Empty;

        /// <summary>
        /// 订购数量
        /// </summary>
        public int QuantityOrdered { get; set; }//num:1

        /// <summary>
        /// 装运数量，已发货数量
        /// </summary>
        public int QuantityShipped { get; set; }//delivery:已发货数

        /// <summary>
        /// 待发货数
        /// </summary>
        public int WaitForSendNum
        {
            get => QuantityOrdered - QuantityShipped;
            set => QuantityShipped = QuantityOrdered - value;
        }//send_num:待发货数

        public string? FromURL { get; set; }  //from_url:商品网址
        
        /// <summary>
        /// 总价 <see cref="ItemPrice"/>+<see cref="ShippingPrice"/>
        /// </summary>
        public MoneyRecord TotalPrice { get; set; } = MoneyRecord.Empty;

        public string? CustomizedURL { get; set; }
    }


    /// <summary>
    /// 订单下第一个商品远程图片url
    /// </summary>

    [Comment("订单下第一个商品远程图片url")]
    public string? Url { get; set; }

    /// <summary>
    /// 商品总价(基准货币)
    /// </summary>
    [Comment("商品总价(基准货币)")]
    public MoneyRecord GoodsMoney { get; set; } = MoneyRecord.Empty;
}

