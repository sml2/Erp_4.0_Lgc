using System.ComponentModel.DataAnnotations.Schema;
using ERP.Models.Abstract;
using ERP.Models.DB.Logistics;
using ERP.Models.DB.Stores;
using Microsoft.EntityFrameworkCore;
using ERPEnumExtension = ERP.Extensions.EnumExtension;
using ERP.Enums.Orders;
using Newtonsoft.Json;
using static ERP.Models.DB.Identity.User;
using ERP.Models.Stores;

namespace ERP.Models.DB.Orders;

[Index(nameof(pullOrderState))]
[Index(nameof(IsMain), nameof(MergeId), nameof(OperateUserId), nameof(OrderNo), nameof(OrderNoHash), nameof(ProcessWaybill), nameof(PurchaseCompanyId), nameof(StoreId), nameof(Timestamp), nameof(WaybillCompanyId))]
public partial class Order : UserModel
{

    [NotMapped]
    public MergeTypes MergeType { get; set; }

    /// <summary>
    /// 订单编号
    /// </summary>
    [Comment("订单编号")]
    public string? OrderNo { get; set; }

    /// <summary>
    /// 买方名称
    /// </summary>
    [Comment("买方名称")]
    public string? BuyerUserName { get; set; }

    /// <summary>
    /// 订单创建时间戳
    /// </summary>
    [Comment("订单创建时间戳")]
    public DateTime CreateTime { get; set; }
    [Comment("订单创建时间戳")]
    public string? CreateTimeRaw { get; set; }

    /// <summary>
    /// 付款时间
    /// </summary>
    [Comment("付款时间")]
    public string? PayTimeRaw { get; set; }

    /// <summary>
    /// 最后更新日期
    /// </summary>
    [Comment("最后更新日期")]
    public DateTime LastUpdateDate { get; set; }

    /// <summary>
    /// 订单注释
    /// </summary>
    [Comment("订单注释")]
    public string? Note { set; get; }

    /// <summary>
    /// 付款方式
    /// </summary>
    [Comment("付款方式")]
    public string? PaymentMethod { get; set; }

    /// <summary>
    /// 发票号
    /// </summary>
    [Comment("发票号")]
    public string? InvoiceNumber { get; set; }

    public int ReceiverID { get; set; }
    internal Address ReceiverInternal = null!;
    /// <summary>
    /// 收件人信息
    /// </summary>
    [Comment("收件人信息")]
    public Address Receiver { get => ReceiverInternal; set => Address.Set(this, value); }

    /// <summary>
    /// 录入方式 1自定义录入 ，2API录入
    /// </summary>
    [Comment("录入方式")]
    public EntryModes EntryMode { get; set; }

    [Comment("createHashCode")]
    public ulong OrderNoHash { get; set; }//order_no  createHashCode

    /// <summary>
    /// 订单配送方式1亚马逊配送 (AFN) 2卖家自行配送 (MFN)
    /// </summary>
    [Comment("订单配送方式")]
    public ShippingTypes? ShippingType { get; set; }

    /// <summary>
    /// 订单付款时间戳
    /// </summary>
    [Comment("订单付款时间戳")]
    public DateTime PayTime { get; set; }

    /// <summary>
    /// 买家附言
    /// </summary>
    [Comment("买家附言")]
    public string? BuyerMessage { get; set; }

    /// <summary>
    /// 买家发票信息
    /// </summary>
    [Comment("买家发票信息")]
    public string? BuyerInvoice { get; set; }

    /// <summary>
    /// 买家签收时间
    /// </summary>
    [Comment("买家签收时间")]
    public DateTime SignTime { get; set; }

    /// <summary>
    /// 卖家店铺id（平台-商店）
    /// </summary>
    [Comment("卖家店铺id")]
    public int StoreId { get; set; }

    //public string StoreName = null;
    public StoreRegion? Store { get; set; }

    [NotMapped]
    public int MergeCount = 0;
    /// <summary>
    /// 卖家店铺seller_id
    /// </summary>
    [Comment("卖家店铺唯一标示")]
    public string? ShopID { get; set; }

    /// <summary>
    /// 卖家对订单的备注
    /// </summary>
    [Comment("卖家对订单的备注")]
    public string? SellerMemo { get; set; }

    /// <summary>
    /// 卖家发货时间
    /// </summary>
    [Comment("卖家发货时间")]
    public DateTime ConsignTime { get; set; }

    /// <summary>
    /// 运费
    /// </summary>
    [Comment("运费")]
    public MoneyRecordFinancialAffairs ShippingMoney { get; set; } = MoneyRecordFinancialAffairs.Empty;

    /// <summary>
    /// 订单获利(基准货币)
    /// </summary>
    [Comment("订单获利(基准货币)")]
    public MoneyRecordFinancialAffairs Profit { get; set; } = MoneyRecordFinancialAffairs.Empty;

    /// <summary>
    /// 订单总价 (订单实付金额、成交价、基准货币)
    /// </summary>
    [Comment("订单总价")]
    public MoneyRecordFinancialAffairs OrderTotal { get; set; } = MoneyRecordFinancialAffairs.Empty;

    public static Dictionary<int, string> GetSupportsOrderStates()
    {
        return OrderState.SearchState;
    }

    /// <summary>
    /// 订单费用估算，来自接口
    /// </summary>
    [Comment("订单费用估算")]
    public MoneyRecord? FeesEstimate { get; set; } 

    /// <summary>
    /// 订单状态 1未付款2取消6配货7待发8已发9交付
    /// </summary>
    public States State { get; set; }

    /// <summary>
    /// 订单评价状态 0为未评价 1为已评价 2为已追评
    /// </summary>

    [Comment("订单评价状态")]
    public ReviewStates ReviewState { get; set; }//订单评价状态 0为未评价 1为已评价 2为已追评

    /// <summary>
    /// 货币单位(拉取单位)
    /// </summary>
    [Comment("货币单位")]
    public string? Coin { get; set; }

    /// <summary>
    /// 承诺发货时间
    /// </summary>
    [Comment("承诺发货时间")]
    public DateTime? DeliveryTime { get; set; }

    /// <summary>
    /// 发货时间
    /// </summary>
    [Comment("发货时间")]
    public DateTime? ShippingTime { get; set; }

    /// <summary>
    /// 承诺送达时间
    /// </summary>
    [Comment("承诺送达时间")]
    public DateTime? LastTime { get; set; }

    /// <summary>
    /// 已配送商品数量
    /// </summary>
    [Comment("订单已配送商品数量")]
    public int? Delivery { get; set; }

    /// <summary>
    /// 未配送商品数量
    /// </summary>
    [Comment("订单未配送商品数量")]
    public int? DeliveryNo { get; set; }

    /// <summary>
    /// 购买商品数量
    /// </summary>
    [Comment("订单上的购买商品数量")]
    public int ProductNum { get; set; }


    /// <summary>
    /// 手续费(基准货币)
    /// </summary>
    [Comment("手续费")]
    public MoneyRecordFinancialAffairs Fee { get; set; } = MoneyRecordFinancialAffairs.Empty;


    /// <summary>
    /// 假发货时间
    /// </summary>里
    [Comment("假发货时间")]
    public DateTime? FakeProductTime { get; set; }

    /// <summary>
    /// 真发货时间
    /// </summary>
    [Comment("真发货时间")]
    public DateTime? TrueProductTime { get; set; }

    /// <summary>
    /// 最后一次发货亚马逊返回查询id
    /// </summary>
    [Comment("最后一次发货亚马逊返回查询id")]
    public int DeliverId { get; set; }

    /// <summary>
    /// 所属最后一次真假发货提交状态1暂无提交2已提交3提交成功4提价失败公司id
    /// </summary>
    [Comment("所属最后一次真假发货提交状态")]
    public int SubmitState { get; set; }

    /// <summary>
    /// 您承诺的订单发货时间范围的最后一天
    /// </summary>
    [Comment("您承诺的订单发货时间范围的最后一天")]
    public DateTime? LatestShipTime { get; set; }

    /// <summary>
    /// 您承诺的订单发货时间范围的第一天
    /// </summary>
    [Comment("您承诺的订单发货时间范围的第一天")]
    public DateTime? EarliestShipTime { get; set; }

    //存放在订单日志
    ///// <summary>
    ///// 当前拉取用户姓名
    ///// </summary>
    //[Comment("当前拉取用户姓名")]
    //public string? Truename { get; set; }

    /// <summary>
    /// 拉取订单时间戳
    /// </summary>
    [Comment("拉取订单时间戳")]
    public DateTime? Timestamp { get; set; }

    /// <summary>
    /// 订单退款金额(基准货币)
    /// </summary>
    [Comment("订单退款金额(基准货币)")]
    public MoneyRecordFinancialAffairs Refund { get; set; } = MoneyRecordFinancialAffairs.Empty;

    /// <summary>
    /// 退款账单id(舍弃)
    /// </summary>
    [Comment("退款账单id(舍弃)")]
    public int RefundBill { get; set; }

    /// <summary>
    /// 订单损耗CNY(基准货币)
    /// </summary>
    [Comment("订单损耗CNY(基准货币)")]
    public MoneyRecordFinancialAffairs Loss { get; set; } = MoneyRecordFinancialAffairs.Empty;

    /// <summary>
    /// 采购花费(基准货币)
    /// </summary>
    [Comment("采购花费(基准货币)")]
    public MoneyRecordFinancialAffairs PurchaseFee { get; set; } = MoneyRecordFinancialAffairs.Empty;

    /// <summary>
    /// 合并订单id
    /// </summary>
    [Comment("合并订单id")]
    public int MergeId { get; set; } = -1;

    /// <summary>
    /// 订单主副
    /// </summary>
    [Comment("订单主副")]
    public IsMains IsMain { get; set; }

    /// <summary>
    /// 操作用户id
    /// </summary>
    [Comment("操作用户id")]
    public int OperateUserId { get; set; }

    /// <summary>
    /// 采购确认商品数
    /// </summary>
    [Comment("采购确认商品数")]
    public int PurchaseNum { get; set; }

    /// <summary>
    /// 物流发货数
    /// </summary>
    [Comment("物流接口发货 调用成功次数")]
    public int DeliverySuccess { get; set; }

    /// <summary>
    /// 物流发货失败数
    /// </summary>
    [Comment("物流接口发货 调用失败数")]
    public int DeliveryFail { get; set; }

    /// <summary>
    /// 第一个添加运单用户0未添加1内部2上级
    /// </summary>
    [Comment("第一个添加运单用户")]
    public Belongs ProcessWaybill { get; set; }

    /// <summary>
    /// 第一个添加采购用户0未添加1内部2上级
    /// </summary>
    [Comment("第一个添加采购用户")]
    public Belongs ProcessPurchase { get; set; }

    /// <summary>
    /// 采购上报公司id
    /// </summary>
    [Comment("采购上报公司id")]
    public int PurchaseCompanyId { get; set; }

    /// <summary>
    /// 物流上报公司id
    /// </summary>
    [Comment("物流上报公司id")]
    public int WaybillCompanyId { get; set; }


    /// <summary>
    /// 合并订单上报公司id
    /// </summary>
    [Comment("合并订单上报公司id")]
    public int OrderCompanyId { get; set; } = 0;

    /// <summary>
    /// 运单主键ID(逗号连接)
    /// </summary>
    [Comment("运单主键ID")]
    public string? Express { get; set; } = "[]";

    /// <summary>
    /// 采购主键ID(逗号连接)
    /// </summary>
    [Comment("采购主键ID")]
    public string? PurchaseTrackingNumber { get; set; } = "[]";

    //public enum IsBatchShips//1否2是
    //{
    //    [Description("不加入批量待发货订单列表")]
    //    NO = 1,
    //    [Description("加入批量待发货订单列表")]
    //    YES
    //}

    /// <summary>
    /// 是否加入批量待发货订单列表
    /// </summary>
    [Comment("是否加入批量待发货订单列表")]
    public bool IsBatchShip { get; set; }

    /// <summary>
    /// 分销收货地址
    /// </summary>
    [Comment("分销收货地址")]
    public string? DistributionAddress { get; set; }

    /// <summary>
    /// 发货模式1代发货2下级发货
    /// </summary>

    [Comment("发货模式")]
    public SendModes SendMode { get; set; }


    [Comment("采购是否需要审核")]
    public int PurchaseAudit { get; set; }//采购是否需要审核

    [Comment("发票")]

    public string? Invoice { get; set; }//发票

    [Comment("店铺默认发货地址")]
    public string? DefaultShipFromLocationAddress { get; set; }//店铺默认发货地址

    [Comment("平台id")]
    public PlatformInfo Plateform { get; set; } = null!;

    public string MarketPlace { get; set; } = string.Empty;
    
    /// <summary>
    /// 接口拉取状态
    /// </summary>
    public PullOrderState pullOrderState { get; set; } = PullOrderState.Init;

    public int? PullCount { get; set; }

    public static Dictionary<int, string> GetSupportsOrderProcess()
    {
        return SupportsOrderProcess.ToDictionary(@enum => (int)@enum, @enum => @enum.GetDescription());
    }
    private static readonly BITS[] SupportsOrderProcess = { BITS.Progresses_Unprocessed, BITS.Progresses_PrintLabel_OUTOFSTOCK, BITS.Progresses_PrintLabel_INSTOCK, BITS.Progresses_Shipment_Success, BITS.Progresses_Shipment_Error };

    [Comment("process & 状态")]
    public BITS Bits { get; set; }
    //    private bool GetProgress(BITS bits)
    //    {
    //        return bits switch
    //        {
    //        <= BITS.Progresses_Error && >= BITS.) =>,
    //            //BITS.Progresses_Error_Reason1 => throw new NotImplementedException(),
    //            //BITS.Progresses_Error_Unpaid => throw new NotImplementedException(),
    //            //BITS.Progresses_Error_Program => throw new NotImplementedException(),
    //            //BITS.Progresses_Error_Reason3 => throw new NotImplementedException(),
    //            //BITS.Progresses_Error_Reason4 => throw new NotImplementedException(),
    //            //BITS.Progresses_Error_Reason5 => throw new NotImplementedException(),
    //            //BITS.Progresses_Error_UserAction => throw new NotImplementedException(),
    //            //BITS.Progresses_Shipment => throw new NotImplementedException(),
    //            //BITS.Progresses_Shipment_Error => throw new NotImplementedException(),
    //            //BITS.Progresses_PrintLabel => throw new NotImplementedException(),
    //            //BITS.Progresses_PrintLabel_INSTOCK => throw new NotImplementedException(),
    //            //BITS.Progresses_PurchaseORreport => throw new NotImplementedException(),
    //            //BITS.Progresses_Unprocessed => throw new NotImplementedException(),
    //            //BITS.Progresses_Complete => throw new NotImplementedException(),
    //            //BITS.Progresses_BitMask => throw new NotImplementedException(),
    //            //BITS.Waybill_HasUploadInvoice => throw new NotImplementedException(),
    //            //BITS.Waybill_HasSync => throw new NotImplementedException(),
    //            //BITS.Waybill_HasPrintLabel => throw new NotImplementedException(),
    //            //BITS.ShippingType_Mask => throw new NotImplementedException(),
    //            _ => throw new NotImplementedException(),
    //        }
    //}
    //private void GetBit(BITS bits)
    //{
    //    Bits & BITS.Progresses_BitMask
    //    }
    public void SetBits(BITS bits, bool t = true)
    {
        SetProgress(bits);
        //SetBit(bits, t);
    }
    private void SetProgress(BITS bits)
    {
        if (bits < BITS.Progresses_BitMask)
        {
            //Bits &= ~BITS.Progresses_BitMask;
            //Bits |= bits;
            Bits = bits;
        }       
    }
    private void SetBit(BITS bits, bool t)//true 置1 false 置0
    {
        if (bits > BITS.Progresses_BitMask)
        {
            if (t)
            {
                Bits |= bits;
            }
            else
            {
                Bits &= ~bits;
            }
        }
    }

    public ICollection<Waybill> Waybills { get; set; } = new List<Waybill>();
    public ICollection<Purchase.Purchase> Purchases { get; set; } = new List<Purchase.Purchase>();

    /// <summary>
    /// 自定义标记
    /// </summary>
    [Comment("自定义标记")]
    public string CustomMark { get; set; } = string.Empty;
    public CustomMarkJson GetCustomMark()
    {
        return JsonConvert.DeserializeObject<CustomMarkJson>(CustomMark) ?? new();
    }
    public void SetCustomMark(CustomMarkJson lc)
    {
        CustomMark = JsonConvert.SerializeObject(lc);
    }

    /// <summary>
    /// 接口拉取订单的情况
    /// </summary>
    //public PullOrderState? PullOrder { get; set; }   

    /// <summary>
    /// 是否是分销订单
    /// </summary>
    /// <returns></returns>
    public bool IsDistributionOrder()
    {
        return PurchaseCompanyId > 0 || WaybillCompanyId > 0;
    }
}
