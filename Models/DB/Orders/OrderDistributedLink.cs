using ERP.Data;
using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.DB.Orders;

[Index(nameof(SourceType), nameof(SourceId))]
[Index(nameof(DistributedType), nameof(DistributedId))]
[Comment("被分配订单关联表")]
public class OrderDistributedLink : UserModel
{
    /// <summary>
    /// 来源id类型
    /// </summary>
    [Comment("来源id类型")]
    public SourceIdType SourceType { get; set; }

    /// <summary>
    /// 来源id
    /// </summary>
    [Comment("来源id")]
    public int SourceId { get; set; }

    /// <summary>
    /// 来源id类型
    /// </summary>
    [Comment("被分配id类型")]
    public DistributedIdType DistributedType { get; set; }

    /// <summary>
    /// 被分配id
    /// </summary>
    [Comment("被分配id")]
    public int DistributedId { get; set; }

    /// <summary>
    /// 订单分配类型
    /// </summary>
    public enum SourceIdType
    {
        OrderId,

        /// <summary>
        /// 分销公司
        /// </summary>
        DistributionCompanyId,
        DistributionOrderId,
    }

    /// <summary>
    /// 被分配的id表示类型
    /// </summary>
    public enum DistributedIdType
    {
        UserId,
        GroupId,
    }
}

public static class OrderDistributedLinkDbExtensions
{
    /// <summary>
    /// 获取所有可用被分配订单关联(非分销)
    /// </summary>
    /// <param name="dbSet"></param>
    /// <param name="userId"></param>
    /// <returns></returns>
    public static Task<OrderDistributedLink[]> GetAllLinks(this IQueryable<OrderDistributedLink> dbSet, int userId)
    {
        return dbSet.Where(l =>
                l.SourceType != OrderDistributedLink.SourceIdType.DistributionCompanyId &&
                l.DistributedType == OrderDistributedLink.DistributedIdType.UserId && l.DistributedId == userId)
            .ToArrayAsync();
    }

    /// <summary>
    /// 获取所有可用被分配订单关联(分销)
    /// </summary>
    /// <param name="dbSet"></param>
    /// <param name="userId"></param>
    /// <returns></returns>
    public static Task<OrderDistributedLink[]> GetAllDistributionLinks(this IQueryable<OrderDistributedLink> dbSet,
        int userId)
    {
        return dbSet.Where(l =>
                l.SourceType == OrderDistributedLink.SourceIdType.DistributionCompanyId &&
                l.DistributedType == OrderDistributedLink.DistributedIdType.UserId && l.DistributedId == userId)
            .ToArrayAsync();
    }

    /// <summary>
    /// 查找该用户可见的被分配订单
    /// </summary>
    /// <param name="source"></param>
    /// <param name="dbContext"></param>
    /// <param name="userId"></param>
    /// <returns></returns>
    public static IQueryable<Order> WhereDistributed(this IQueryable<Order> source, DBContext dbContext, int userId)
    {
        return source.Join(dbContext.OrderDistributedLinks.Where(l =>
            l.SourceType == OrderDistributedLink.SourceIdType.OrderId &&
            l.DistributedType == OrderDistributedLink.DistributedIdType.UserId &&
            l.DistributedId == userId), o => o.ID, l => l.SourceId, (order, link) => order);
    }

    /// <summary>
    /// 查找该用户可见的被分配订单(分销)
    /// </summary>
    /// <param name="source"></param>
    /// <param name="dbContext"></param>
    /// <param name="userId"></param>
    /// <returns></returns>
    public static IQueryable<Order> WhereDistributedDistribution(this IQueryable<Order> source, DBContext dbContext, int userId)
    {
        // || l.SourceType == OrderDistributedLink.SourceIdType.DistributionCompanyId
        return source.Join(dbContext.OrderDistributedLinks.Where(l =>
            (l.SourceType == OrderDistributedLink.SourceIdType.DistributionOrderId) &&
            l.DistributedType == OrderDistributedLink.DistributedIdType.UserId &&
            l.DistributedId == userId), o => o.ID, l => l.SourceId, (order, link) => order);
    }
}