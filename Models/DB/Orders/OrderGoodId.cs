using static ERP.Models.DB.Orders.Order;

namespace ERP.Models.DB.Orders;

public class OrderGoodId : OrderHash
{
    public OrderGoodId()
    {
    }
    public OrderGoodId(int orderId, Good good) : base(orderId, good.ID ?? string.Empty, good.QuantityOrdered)
    {
    }
}
