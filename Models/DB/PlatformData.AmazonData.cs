//using System.Text;
//namespace ERP.Models.DB;
//using Enums;
//using Extensions;


//public partial class PlatformData
//{
//    public PlatformData(int id,Platforms platform, AmazonData amazonData)
//    {
//        ID = id;       
//        Platform = platform;
//        SetData(amazonData);
//    }
//    public PlatformData(int id, Enums.ID.Nations Nation, Platforms platform, AmazonData amazonData)
//    {
//        ID = id;
//        NationId = Nation.GetNumber();
//        Platform = platform;
//        SetData(amazonData);
//    }
//    public class AmazonData
//    {
//        public AmazonData() { }

//        public AmazonData(string AreaName, string Name, string Short,string Unit, string Mws, string Marketplace, 
//             string Org, Continents Continent, int KeyId, string? SellerCentralURL = default(string), StateEnum State = StateEnum.Open)
//        {
//            this.AreaName = AreaName;
//            this.Name = Name;
//            this.Short = Short;
//            this.Unit = Unit;
//            Host = Mws.Split("//").Last().ToLower();
//            MwsSecurity = !Mws.StartsWith("http:");//不加协议时默认https

//            this.Marketplace = Marketplace;
//            this.State = State;
//            this.Org = Org;
//            this.Continent = Continent;
//            MWSCanUnion = Continent == Continents.Europe || Continent == Continents.NorthAmerica;
//            switch(this.Continent)
//            {
//                case Continents.FarEast:
//                    RegionName = "us-west-2";
//                    break;
//                case Continents.NorthAmerica:
//                    RegionName = "us-east-1";
//                    break;
//                case Continents.Europe:
//                    RegionName = "eu-west-1";
//                    break;
//                    default:
//                    throw new AggregateException("Continent 数据异常！");
//            }
//            this.KeyId = KeyId;
//            this.RegionName = RegionName;
//            this.SellerCentralURL = SellerCentralURL;
//        }        

//        /// <summary>
//        /// 地区名称-中文
//        /// </summary>
//        public string Name { get; set; } = string.Empty;

//        /// <summary>
//        /// 地区名称-英文
//        /// </summary>
//        public string AreaName { get; set; } = string.Empty;

//        /// <summary>
//        /// 地区简称
//        /// </summary>
//        public string Short { get; set; } = string.Empty;

//        /// <summary>
//        /// 货币
//        /// </summary>
//        public string Unit { get; set; } = string.Empty;

//        /// <summary>
//        /// MarketplaceId
//        /// </summary>
//        public string Marketplace { get; set; } = string.Empty;

//        /// <summary>
//        /// 状态1启用2禁用
//        /// </summary>
//        public StateEnum State { get; set; } = StateEnum.Open;


//        /// <summary>
//        /// 可以为联合店铺(多国家)
//        /// </summary>
//        public bool MWSCanUnion { get; set; }


//        private Continents _Continent;

//        /// <summary>
//        /// 所属区域
//        /// </summary>
//        public Continents Continent
//        {
//            get { return _Continent; }
//            set { _Continent = value; }
//        }

//        /// <summary>
//        /// api端点,发起请求使用
//        /// </summary>
//        public string Host { get; set; } = string.Empty;

//        /// <summary>
//        /// 地区域名，sku加上地区域名，可以直接查看商品
//        /// </summary>
//        public string Org { get; set; } = string.Empty;

//        /// <summary>
//        /// 亚马逊开发者资质id-来自表AmazonKey
//        /// </summary>
//        public int KeyId { get; set; }

//        #region mws        

//        public bool MwsSecurity { get; set; }
//        [Newtonsoft.Json.JsonIgnore, System.Text.Json.Serialization.JsonIgnore]
//        public Encoding Encoding { get => Host.EndsWith(".jp") ? Encoding.GetEncoding("shift-jis") : Encoding.UTF8; }

//        #endregion

       
//        #region spapi        
//        /// <summary>
//        /// spapi url
//        /// </summary>
//        public string? SellerCentralURL { get; set; }

//        public string? RegionName { get; set; }
//        #endregion

//    }
//    public class AmazonDataWithID : AmazonData
//    {
//        public AmazonDataWithID() : base() { }
        
//        public AmazonDataWithID(string AreaName, string Name, string Short, string Unit, string Mws, string Marketplace, StateEnum State, string Org, Continents Continent, int KeyId, string? sellerUrl) : base(AreaName, Name, Short, Unit, Mws, Marketplace, Org, Continent, KeyId, sellerUrl, State)
//        {
//        }
//        public int ID { get; set; }
//    }
   
//}
