﻿using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;

namespace ERP.Models.DB.Purchase;

public class Supplier : BaseModel
{
    /// <summary>
    /// 采购商名称
    /// </summary>

    [Comment("采购商名称")]
    public string? Name { get; set; }

    /// <summary>
    /// 采购商电话
    /// </summary>

    [Comment("采购商电话")]
    public string? Phone { get; set; }

    /// <summary>
    /// 采购商地址
    /// </summary>

    [Comment("采购商地址")]
    public string? Address { get; set; }

    /// <summary>
    /// 备注
    /// </summary>

    [Comment("备注")]
    public string? Remark { get; set; }
}

//-- ----------------------------
//-- Table structure for erp3_supplier
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_supplier`;
//CREATE TABLE `erp3_supplier`  (
//  `id` bigint unsigned NOT NULL,
//  `i_user_id` int unsigned NOT NULL COMMENT '创建者id',
//  `i_company_id` int unsigned NOT NULL COMMENT '采购商id',
//  `i_oem_id` int unsigned NOT NULL COMMENT 'oem_id',
//  `i_group_id` int unsigned NOT NULL COMMENT '组id',
//  `d_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '采购商名称',
//  `d_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '采购商电话',
//  `d_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '采购商地址',
//  `d_remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '备注',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;