﻿using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using ERP.Enums.Purchase;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ERP.Models.DB.Users;
using ERP.Models.Finance;
using ERP.Models.DB.Identity;

namespace ERP.Models.DB.Purchase;

[Index(nameof(OperateCompanyId), nameof(OrderId), nameof(OrderNo), nameof(PlatformId), nameof(PurchaseCompanyId), nameof(PurchaseState), nameof(StoreId), nameof(PurchaseAffirm))]
public class Purchase : UserModel
{
    public Purchase()
    {
        _logs = new List<PurchaseLog>();
    }
    
    /// <summary>
    /// 订单id
    /// </summary>
    [Comment("订单id")]
    public int? OrderId { get; set; }

    /// <summary>
    /// 商品名称
    /// </summary>
    [Comment("商品名称")]
    public string? Name { get; set; }

    /// <summary>
    /// asin码（亚马逊唯一编号，访问商品）
    /// </summary>
    [NotMapped]
    [Comment("asin码（亚马逊唯一编号，访问商品）")]
    public string? Asin { get => OrderGoodId; set => OrderGoodId = value; }

    /// <summary>
    /// 订单产品good_id
    /// </summary>
    [Comment("订单产品good_id")]
    public string? OrderGoodId { get; set; }

    /// <summary>
    /// 所属平台id
    /// </summary>
    [Comment("所属平台id")]
    public int? PlatformId { get; set; }

    /// <summary>
    /// 产品id 
    /// </summary>
    [Comment("产品id")]//产品库id
    public int? ProductId { get; set; }

    /// <summary>
    /// 变体hashcode
    /// </summary>
    [Comment("变体hashcode")]
    public int? VariantId { get; set; }

    /// <summary>
    /// 采购数量
    /// </summary>
    [Comment("采购数量")]
    public int? Num { get; set; }

    /// <summary>
    /// 商品价格
    /// </summary>
    [Comment("订单商品单价")]
    public MoneyRecordFinancialAffairs Price { get; set; } = MoneyRecordFinancialAffairs.Empty;

    /// <summary>
    /// 采购商品单价
    /// </summary>
    [Comment("采购商品单价")]
    public MoneyRecordFinancialAffairs PurchasePrice { get; set; } = MoneyRecordFinancialAffairs.Empty;

    /// <summary>
    /// 商品总价
    /// </summary>
    [Comment("采购商品总价")]
    public MoneyRecordFinancialAffairs ProductTotal { get; set; } = MoneyRecordFinancialAffairs.Empty;

    /// <summary>
    /// 采购其他费用
    /// </summary>
    [Comment("采购其他费用")]
    public MoneyRecordFinancialAffairs PurchaseOther { get; set; } = MoneyRecordFinancialAffairs.Empty;

    /// <summary>
    /// 采购总价
    /// </summary>
    [Comment("采购总价")]
    public MoneyRecordFinancialAffairs PurchaseTotal { get; set; } = MoneyRecordFinancialAffairs.Empty;

    /// <summary>
    /// 货币单位
    /// </summary>
    [Comment("采购货币单位")]
    public string? PurchaseUnit { get; set; }

    /// <summary>
    /// 货币单位
    /// </summary>
    [Comment("货币单位")]
    public string? Unit { get; set; }

    /// <summary>
    /// 采购手续费
    /// </summary>
    [Comment("采购手续费")]
    public decimal? PurchaseRate { get; set; }

    /// <summary>
    /// 采购手续费类型1百分比2固定数值
    /// </summary>
    [Comment("采购手续费类型1百分比2固定数值")]
    public PurchaseRateTypes? PurchaseRateType { get; set; }

    /// <summary>
    /// 采购状态2采购中3已采购4无需采购
    /// </summary>
    [Comment("采购状态2采购中3已采购4无需采购")]
    public PurchaseStates PurchaseState { get; set; }

    /// <summary>
    /// 订单产品图片url
    /// </summary>
    [Description("订单产品图片url")]
    public string? Url { get; set; }

    /// <summary>
    /// 订单编号
    /// </summary>
    [Description("订单编号")]
    public string? OrderNo { get; set; }

    /// <summary>
    /// 产品链接
    /// </summary>
    [Comment("产品链接")]
    public string? FromUrl { get; set; }

    /// <summary>
    /// 添加处理公司id
    /// </summary>
    [Comment("添加处理公司id")]
    public int? OperateCompanyId { get; set; }

    /// <summary>
    /// 采购商品来源
    /// </summary>
    [Comment("采购商品来源")]
    public string? PurchaseUrl { get; set; }

    /// <summary>
    /// 采购商品订单号
    /// </summary>
    [Comment("采购商品订单号")]
    public string? PurchaseOrderNo { get; set; }

    /// <summary>
    /// 采购商品物流追踪号
    /// </summary>
    [Comment("采购商品物流追踪号")]
    public string? PurchaseTrackingNumber { get; set; }

    /// <summary>
    /// 备注
    /// </summary>

    [Comment("备注")]
    public string? Remark { get; set; }

    /// <summary>
    /// 确认状态1未确认2已确认
    /// </summary>

    [Comment("确认状态1未确认2已确认3.已取消")]
    public PurchaseAffirms PurchaseAffirm { get; set; }

    //[Comment("1 是 2 否")]
    //public IsAffirms? IsAffirm { get; set; }

    /// <summary>
    /// 处理用户id
    /// </summary>
    [Comment("处理用户id")]
    public int? PurchaseUserId { get; set; }

    /// <summary>
    /// 采购用户姓名
    /// </summary>
    [Comment("采购用户姓名")]
    public string? FindTruename { get; set; }

    /// <summary>
    /// 账单id
    /// </summary>
    [Comment("账单id")]
    public int? BillLogId { get; set; }
    
    public BillLog? BillLog { get; set; }

    /// <summary>
    /// 找货id
    /// </summary>
    [Comment("找货id")]
    public int? PickId { get; set; }

    /// <summary>
    /// 审核用户id
    /// </summary>
    [Comment("审核用户id")]
    public int? AffirmUid { get; set; }

    /// <summary>
    /// 审核用户姓名
    /// </summary>
    [Comment("审核用户姓名")]
    public string? AffirmTruename { get; set; }

    /// <summary>
    /// 卖家店铺id（平台-商店）
    /// </summary>
    [Comment("卖家店铺id（平台-商店）")]
    public int? StoreId { get; set; }

    /// <summary>
    /// 卖家店铺名称
    /// </summary>

    [Comment("卖家店铺名称")]
    public string? StoreName { get; set; }

    /// <summary>
    /// OrderItemId
    /// </summary>
    [Comment("OrderItemId")]
    public int? OrderItemId { get; set; }

    /// <summary>
    /// SellerSku
    /// </summary>
    [Comment("SellerSku")]
    public string? SellerSku { get; set; }

    /// <summary>
    /// 是否需要入库1否2是
    /// </summary>

    [Comment("是否需要入库1否2是")]
    public bool IsStock { get; set; }

    /// <summary>
    /// 入库仓库id
    /// </summary>

    [Comment("入库仓库id")]
    public int? WarehouseId { get; set; }

    /// <summary>
    /// 入库仓库名称
    /// </summary>

    [Comment("入库仓库名称")]
    public string? WarehouseName { get; set; }
    
    [Comment("产品库存id")]
    public int? StockProductId { get; set; }

    /// <summary>
    /// 采购上报公司id
    /// </summary>

    [Description("采购上报公司id")]
    public int? PurchaseCompanyId { get; set; }

    /// <summary>
    /// 运单主键ID(逗号连接)
    /// </summary>
    [Comment("运单主键ID(逗号连接)订单express留档[,,]")]
    public string Express { get; set; } = "[]";

    /// <summary>
    /// 创建公司id
    /// </summary>
    [Comment("创建公司id")]
    public int? CreateCompanyId { get; set; }

    private List<PurchaseLog> _logs;
    public IReadOnlyCollection<PurchaseLog> Logs => _logs;

    /// <summary>
    /// 添加采购日志
    /// </summary>
    public void AddLog(string action, User user)
    {
        var log = new PurchaseLog(action, user, OrderId, PlatformId ?? 1);
        _logs.Add(log);
    }
    
    /// <summary>
    /// 添加采购日志
    /// </summary>
    public void AddLog(string action, int userId, int groupId, string username, string truename)
    {
        var log = new PurchaseLog(action, userId, username, truename, groupId, CompanyID, OEMID, OrderId, PlatformId ?? 1);
        _logs.Add(log);
    }

    /// <summary>
    /// 汇总计数
    /// 按照创建公司进行计费
    /// - 自己、下级创建的，扣除个人钱包
    /// - 上级创建的，扣除对公钱包
    /// - 老数据默认为0，按照个人创建的算，扣个人钱包
    /// </summary>
    /// <param name="company"></param>
    /// <param name="operateUser"></param>
    /// <exception cref="NotImplementedException"></exception>
    public IWallet GetWallet(Company company, User operateUser)
    {
        if (!CreateCompanyId.HasValue || CreateCompanyId == CompanyID)
            return new SelfWallet(company, operateUser);

        return new PublicWallet(company, operateUser);
    }

    /// <summary>
    /// 将采购状态标记为拒绝
    /// </summary>
    public void ChangeStateToRefused()
    {
        PurchaseState = PurchaseStates.REFUSED;
    }
}


//-- ----------------------------
//-- Table structure for erp3_purchase
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_purchase`;
//CREATE TABLE `erp3_purchase`  (
//  `id` bigint unsigned NOT NULL,
//  `i_order_id` int unsigned NOT NULL COMMENT '订单ID',
//  `u_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '商品名称',
//  `d_asin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'asin码（亚马逊唯一编号，访问商品）',
//  `d_variant_id` bigint unsigned NULL COMMENT '变体hashcode',
//  `d_product_id` bigint unsigned NULL COMMENT '产品id',
//  `d_price` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '商品价格',
//  `d_num` int unsigned NOT NULL COMMENT '购买数量',
//  `d_product_total` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '商品总价',
//  `d_unit` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '货币单位',
//  `t_purchase_state` tinyint unsigned NOT NULL COMMENT '采购状态2采购中3已采购4无需采购',
//  `d_url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '订单产品图片url',
//  `d_order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '订单编号',
//  `d_from_url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '产品链接',
//  `i_company_id` int unsigned NOT NULL COMMENT '公司id',
//  `i_operate_company_id` int unsigned NOT NULL COMMENT '添加处理公司id',
//  `i_platform_id` int unsigned NOT NULL COMMENT '所属平台id',
//  `d_purchase_unit` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '货币单位',
//  `d_purchase_total` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '采购总价',
//  `d_purchase_other` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '采购其他费用',
//  `d_purchase_price` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '采购商品单价',
//  `d_purchase_url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '采购商品来源',
//  `d_purchase_order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '采购商品订单号',
//  `d_purchase_tracking_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '采购商品物流追踪号',
//  `d_remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '备注',
//  `t_purchase_affirm` tinyint unsigned NOT NULL COMMENT '确认状态1未确认2已确认',
//  `i_purchase_user_id` int unsigned NOT NULL COMMENT '处理用户id',
//  `u_find_truename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '采购用户姓名',
//  `i_bill_log_id` int unsigned NOT NULL COMMENT '账单id',
//  `i_pick_id` int unsigned NOT NULL COMMENT '找货id',
//  `t_is_affirm` tinyint(0) NOT NULL DEFAULT 1 COMMENT '需要确认1是2否(弃用)',
//  `i_affirm_uid` int unsigned NOT NULL COMMENT '审核用户id',
//  `u_affirm_truename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '审核用户姓名',
//  `i_store_id` int unsigned NOT NULL COMMENT '卖家店铺id（平台-商店）',
//  `u_store_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '卖家店铺名称',
//  `d_order_item_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'OrderItemId',
//  `d_seller_sku` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'SellerSKU',
//  `d_purchase_rate` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '采购手续费',
//  `d_purchase_rate_type` tinyint unsigned NOT NULL COMMENT '采购手续费类型1百分比2固定数值',
//  `i_oem_id` int unsigned NOT NULL COMMENT 'oem_id',
//  `i_group_id` int unsigned NOT NULL COMMENT '组id',
//  `t_is_stock` tinyint(0) NOT NULL DEFAULT 1 COMMENT '是否需要入库1否2是',
//  `i_warehouse_id` int unsigned NOT NULL COMMENT '入库仓库id',
//  `u_warehouse_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '入库仓库名称',
//  `i_purchase_company_id` int unsigned NOT NULL COMMENT '采购上报公司id',
//  `u_express` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '运单主键ID(逗号连接)',
//  `u_order_good_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '订单产品good_id',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  `i_create_company_id` int(0) NOT NULL DEFAULT 0 COMMENT '创建公司id',
//  PRIMARY KEY (`id`) USING BTREE,
//  INDEX `company_id`(`i_company_id`) USING BTREE,
//  INDEX `operate_company_id`(`i_operate_company_id`) USING BTREE,
//  INDEX `order_id`(`i_order_id`) USING BTREE,
//  INDEX `order_no`(`d_order_no`) USING BTREE,
//  INDEX `platform_id`(`i_platform_id`) USING BTREE,
//  INDEX `purchase_company_id`(`i_purchase_company_id`) USING BTREE,
//  INDEX `purchase_state`(`t_purchase_state`) USING BTREE,
//  INDEX `store_id`(`i_store_id`) USING BTREE,
//  INDEX `t_purchase_affirm`(`t_purchase_affirm`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 75126 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '采购管理_采购列表' ROW_FORMAT = Dynamic;