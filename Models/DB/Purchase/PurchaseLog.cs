﻿using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;
using ERP.Models.DB.Identity;

namespace ERP.Models.DB.Purchase;

[Index(nameof(PlatformId), nameof(PurchaseId))]
public sealed class PurchaseLog : UserModel
{
    public PurchaseLog() {}

    public PurchaseLog(string action, int userId, string username, string truename, int groupId, int companyId
        , int oemId, int? orderId = null, int platformId = 1) : this()
    {
        OrderId = orderId;
        Action = action;
        PlatformId = platformId;
        UserName = username;
        TrueName = truename;
        UserID = userId;
        GroupID = groupId;
        OEMID = oemId;
        CompanyID = companyId;
    }
    
    public PurchaseLog(string action, User user, int? orderId = null, int platformId = 1) : this(action, user.Id
        , user.UserName, user.TrueName, user.GroupID, user.CompanyID, user.OEMID, orderId, platformId)
    {
    }
    
    /// <summary>
    /// 采购单id
    /// </summary>

    [Comment("采购单id")]
    public int PurchaseId { get; set; }

    /// <summary>
    /// 动作内容
    /// </summary>

    [Comment("动作内容")]
    public string? Action { get; set; }

    /// <summary>
    /// 操作人
    /// </summary>

    [Comment("操作人")]
    public string UserName { get; set; } = string.Empty;

    /// <summary>
    /// 操作用户姓名
    /// </summary>

    [Comment("操作用户姓名")]
    public string TrueName { get; set; } = string.Empty;

    /// <summary>
    /// 订单id
    /// </summary>

    [Comment("订单id")]
    public int? OrderId { get; set; }

    /// <summary>
    /// 平台id
    /// </summary>

    [Comment("平台id")]
    public int? PlatformId { get; set; }
}

//-- ----------------------------
//-- Table structure for erp3_purchase_log
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_purchase_log`;
//CREATE TABLE `erp3_purchase_log`  (
//  `id` bigint unsigned NOT NULL,
//  `i_purchase_id` int unsigned NOT NULL COMMENT '采购单id',
//  `u_action` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '动作内容',
//  `i_user_id` int unsigned NOT NULL COMMENT '操作人id',
//  `u_username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作人',
//  `u_truename` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作用户姓名',
//  `i_oem_id` int unsigned NOT NULL COMMENT 'oem_id',
//  `i_group_id` int unsigned NOT NULL COMMENT '组id',
//  `i_company_id` int unsigned NOT NULL COMMENT '公司id',
//  `i_order_id` int unsigned NOT NULL COMMENT '订单id',
//  `i_platform_id` int unsigned NOT NULL COMMENT '平台id',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `order_id`(`i_order_id`) USING BTREE,
// INDEX `platform_id`(`i_platform_id`) USING BTREE,
// INDEX `purchase_id`(`i_purchase_id`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 114331 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '采购管理_采购操作日志表' ROW_FORMAT = Dynamic;