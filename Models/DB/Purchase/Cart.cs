﻿using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;

namespace ERP.Models.DB.Purchase;

[Index(nameof(ProductId), nameof(Spu))]
public class Cart : UserModel
{
    /// <summary>
    /// 购买产品名称
    /// </summary>

    [Comment("购买产品名称")]
    public string? Name { get; set; }

    /// <summary>
    /// 产品图片
    /// </summary>

    [Comment("产品图片")]
    public string? img { get; set; }

    /// <summary>
    /// 属性名称
    /// </summary>

    [Comment("属性名称")]
    public string? AttrName { get; set; }

    /// <summary>
    /// 购买数量
    /// </summary>

    [Comment("购买数量")]
    public int Num { get; set; }

    /// <summary>
    /// 产品hashcode
    /// </summary>

    [Comment("产品hashcode")]
    public int Spu { get; set; }

    public string? Sku { get; set; }

    /// <summary>
    /// 产品单价(基准货币)
    /// </summary>

    [Comment("产品单价(基准货币)")]
    public decimal Price { get; set; }

    /// <summary>
    /// 购买总价(基准货币)
    /// </summary>

    [Comment("购买总价(基准货币)")]
    public decimal Total { get; set; }

    /// <summary>
    /// 产品id
    /// </summary>

    [Comment("产品id")]
    public int ProductId { get; set; }
}

//-- ----------------------------
//-- Table structure for erp3_cart
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_cart`;
//CREATE TABLE `erp3_cart`  (
//  `id` bigint unsigned NOT NULL,
//  `d_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '购买产品名称',
//  `d_img` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '产品图片',
//  `d_attr_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '属性名称',
//  `d_num` int unsigned NOT NULL COMMENT '购买数量',
//  `d_spu` bigint unsigned NOT NULL COMMENT '产品hash_code',
//  `d_sku` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '变体sid',
//  `d_price` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '产品单价(基准货币)',
//  `d_total` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '购买总价(基准货币)',
//  `i_product_id` bigint unsigned NOT NULL COMMENT '产品id',
//  `i_user_id` int unsigned NOT NULL COMMENT '用户id',
//  `i_company_id` int unsigned NOT NULL COMMENT '所属公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `product_id`(`i_product_id`) USING BTREE,
// INDEX `spu`(`d_spu`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '采购管理_购物车' ROW_FORMAT = Dynamic;