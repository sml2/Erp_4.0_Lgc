﻿using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using ERP.Enums.Purchase;
using System.ComponentModel.DataAnnotations;

namespace ERP.Models.DB.Purchase;

[Index(nameof(PurchaseId))]
public class PurchasePick : BaseModel
{
    /// <summary>
    /// 代采购id
    /// </summary>

    [Comment("代采购id")]
    public int PurchaseId { get; set; }

    /// <summary>
    /// 创建用户名
    /// </summary>

    [Comment("创建用户名")]
    public string? TrueName { get; set; }

    /// <summary>
    /// 采购来源url
    /// </summary>

    [Comment("采购来源url")]
    public string? Url { get; set; }

    /// <summary>
    /// 采购货币单位
    /// </summary>

    [Comment("采购货币单位")]
    public string? Unit { get; set; }

    /// <summary>
    /// 采购花费
    /// </summary>

    [Comment("采购花费")]
    public decimal Price { get; set; }

    /// <summary>
    /// 状态1待确认2确认3否决
    /// </summary>

    [Comment("状态1待确认2确认3否决")]
    public States State { get; set; }

    /// <summary>
    /// 备注
    /// </summary>

    [Comment("备注")]
    public string? Remark { get; set; }
}


//-- ----------------------------
//-- Table structure for erp3_purchase_pick
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_purchase_pick`;
//CREATE TABLE `erp3_purchase_pick`  (
//  `id` bigint unsigned NOT NULL,
//  `i_purchase_id` int unsigned NOT NULL COMMENT '待采购id',
//  `i_user_id` int unsigned NOT NULL COMMENT '创建用户id',
//  `d_truename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '创建用户名',
//  `d_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '采购来源url',
//  `d_unit` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '采购货币单位',
//  `d_price` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '采购花费',
//  `t_state` tinyint unsigned NOT NULL COMMENT '状态1待确认2确认3否决',
//  `d_remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '备注',
//  `i_company_id` int unsigned NOT NULL COMMENT '公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT 'oem_id',
//  `i_group_id` int unsigned NOT NULL COMMENT '组id',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `i_purchase_id`(`i_purchase_id`) USING BTREE,
// INDEX `i_user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 272 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '采购管理_采购找货表' ROW_FORMAT = Dynamic;