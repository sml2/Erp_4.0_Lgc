﻿using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;

namespace ERP.Models.DB.Purchase;

public class PurchasingChannels : UserModel
{
    /// <summary>
    /// 渠道名称
    /// </summary>

    [Comment("渠道名称")]
    public string? Name { get; set; }

    /// <summary>
    /// 采购商id
    /// </summary>

    [Comment("采购商id")]
    public int SupplierId { get; set; }

    /// <summary>
    /// 采购商名称
    /// </summary>

    [Comment("采购商名称")]
    public string SupplierName { get; set; } = string.Empty;

    /// <summary>
    /// 产品id
    /// </summary>

    [Comment("产品id")]
    public int ProductId { get; set; }

    /// <summary>
    /// 产品名称
    /// </summary>

    [Comment("产品名称")]
    public string? ProductName { get; set; }

    /// <summary>
    /// 仓库id
    /// </summary>

    public int WarehouseId { get; set; }

    /// <summary>
    /// 仓库名称
    /// </summary>

    [Comment("仓库名称")]
    public string? WarehouseName { get; set; }

    /// <summary>
    /// 产品变体，hashcode,采购单价
    /// </summary>

    [Comment("产品变体，hashcode,采购单价")]
    public string? Variants { get; set; }
}

//-- ----------------------------
//-- Table structure for erp3_purchasing_channels
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_purchasing_channels`;
//CREATE TABLE `erp3_purchasing_channels`  (
//  `id` bigint unsigned NOT NULL,
//  `d_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '渠道名称',
//  `i_user_id` int unsigned NOT NULL COMMENT '用户id',
//  `i_group_id` int unsigned NOT NULL COMMENT '组id',
//  `i_company_id` int unsigned NOT NULL COMMENT '公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT 'oem_id',
//  `i_supplier_id` int unsigned NOT NULL COMMENT '采购商id',
//  `supplier_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '采购商名称',
//  `i_product_id` int unsigned NOT NULL COMMENT '产品id',
//  `product_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '产品名称',
//  `warehouse_id` int unsigned NOT NULL COMMENT '仓库id',
//  `warehouse_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '仓库名称',
//  `d_variants` json NOT NULL COMMENT '产品变体，hashcode,采购单价',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '采购管理_采购渠道' ROW_FORMAT = Dynamic;