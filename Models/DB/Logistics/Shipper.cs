//using ERP.Models.Abstract;
//using Microsoft.EntityFrameworkCore;
//using System.ComponentModel;

//namespace ERP.Models.DB.Logistics
//{
//    public class Shipper : UserModel
//    {
//        //public int ID { get; set; }

//        /// <summary>
//        /// 寄件人的姓名
//        /// </summary>

//        [Comment("寄件人姓名")]
//        public string Name { get; set; } = string.Empty;

//        /// <summary>
//        /// 电话
//        /// </summary>

//        [Comment("电话")]
//        public string? Phone { get; set; }

//        /// <summary>
//        /// 邮编
//        /// </summary>

//        [Comment("邮编")]
//        public string? Postcode { get; set; }

//        /// <summary>
//        /// 邮箱
//        /// </summary>

//        [Comment("邮箱")]
//        public string? Email { get; set; }

//        /// <summary>
//        /// 地址
//        /// </summary>

//        [Comment("地址")]
//        public string? Address { get; set; }

//        /// <summary>
//        /// 省
//        /// </summary>

//        [Comment("省")]
//        public string? Province { get; set; }

//        /// <summary>
//        /// 市
//        /// </summary>

//        [Comment("市")]
//        public string? City { get; set; }

//        /// <summary>
//        /// 区
//        /// </summary>

//        [Comment("区")]
//        public string? District { get; set; }

//        /// <summary>
//        /// 公司
//        /// </summary>

//        [Comment("公司")]
//        public new string? Company { get; set; }

//        /// <summary>
//        /// 最后一条运单时间
//        /// </summary>

//        [Comment("最后一条运单时间")]
//        public DateTime? LastTime { get; set; }
//    }
//}


////------------------------------
////--Table structure for erp3_logistics_shipper
////------------------------------
////DROP TABLE IF EXISTS `erp3_logistics_shipper`;
////CREATE TABLE `erp3_logistics_shipper`  (
////  `id` bigint unsigned NOT NULL,
////  `i_company_id` int unsigned NOT NULL COMMENT '公司id)',
////  `i_user_id` int unsigned NOT NULL COMMENT '用户id)',
////  `i_group_id` int unsigned NOT NULL COMMENT '用户组id)',
////  `i_oem_id` int unsigned NOT NULL COMMENT 'oemid)',
////  `d_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '寄件人姓名',
////  `d_phone` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '电话',
////  `d_postcode` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '邮编',
////  `d_email` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '邮箱',
////  `d_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '地址',
////  `d_province` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '省',
////  `d_city` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '市',
////  `d_district` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '区',
////  `d_company` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '公司',
////  `d_last_time` timestamp(0) NULL DEFAULT NULL COMMENT '最后一条运单时间',
////  `created_at` timestamp(0) NULL DEFAULT NULL,
////  `updated_at` timestamp(0) NULL DEFAULT NULL,
//// PRIMARY KEY(`id`) USING BTREE
////) ENGINE = InnoDB AUTO_INCREMENT = 199 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '运单管理_寄件人信息' ROW_FORMAT = Dynamic;
