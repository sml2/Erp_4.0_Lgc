//using ERP.Models.Abstract;
//using Microsoft.EntityFrameworkCore;
//using System.ComponentModel;

//namespace ERP.Models.DB.Logistics
//{
//    public class ShipTemplate:UserModel 
//    {
//        [Comment("发货模版名称")]
//        public string Name {  get; set; } = string.Empty;
//        [Comment("中文申报名称")]
//        public string Declare_zh {  get; set; } = string.Empty;
//        [Comment("英文申报名称")]
//        public string Declare_en {  get; set; } = string.Empty;
//        [Comment("海关编码")]
//        public string CustomsCode {  get; set; } = string.Empty;
//        [Comment("商品描述")]
//        public string Description {  get; set; } = string.Empty;
//        [Comment("自定义标签")]
//        public string Tag {  get; set; } = string.Empty;
//        [Comment("存在电池0否1是")]
//        public bool Battery {  get; set; }
//        [Comment("长(cm)")]
//        public int Length {  get; set; }
//        [Comment("宽(cm)")]
//        public int Width {  get; set; }
//        [Comment("高(cm)")]
//        public int Height {  get; set; }
//        [Comment("重量(g)")]
//        public int Weight {  get; set; }
//        /// <summary>
//        ///  ERP3.0 状态 启用 2禁用 STATE_ENABLE = 1;  STATE_DISABLE = 2;
//        /// </summary>
//        [Comment("状态")]
//        public StateEnum State {  get; set; }
//        [Comment("排序字段")]
//        public int Sort {  get; set; }

//        [Comment("IOSS")]
//        public string? IOSS { get; set; }

//    }
//}


////------------------------------
////--Table structure for erp3_ship_template
////------------------------------
////DROP TABLE IF EXISTS `erp3_ship_template`;
////CREATE TABLE `erp3_ship_template`  (
////  `id` bigint unsigned NOT NULL,
////  `d_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '发货模版名称',
////  `d_declare_zh` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '中文申报名称',
////  `d_declare_en` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '英文申报名称',
////  `d_customs_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '海关编码',
////  `d_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '商品描述',
////  `d_tag` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '自定义标签',
////  `d_battery` tinyint(0) NOT NULL DEFAULT 1 COMMENT '存在电池1否2是',
////  `d_length` int(0) NOT NULL DEFAULT 0 COMMENT '长(cm)',
////  `d_width` int(0) NOT NULL DEFAULT 0 COMMENT '宽(cm)',
////  `d_height` int(0) NOT NULL DEFAULT 0 COMMENT '高(cm)',
////  `d_weight` int(0) NOT NULL DEFAULT 0 COMMENT '重量(g)',
////  `t_state` tinyint(0) NOT NULL DEFAULT 1 COMMENT '状态1启用2禁用',
////  `d_sort` int(0) NOT NULL DEFAULT 0 COMMENT '排序字段',
////  `i_user_id` int(0) NOT NULL DEFAULT 0 COMMENT '用户id',
////  `i_group_id` int(0) NOT NULL DEFAULT 0 COMMENT '创建用户组id',
////  `i_company_id` int(0) NOT NULL DEFAULT 0 COMMENT '所属公司id',
////  `i_oem_id` int(0) NOT NULL DEFAULT 0 COMMENT '当前Oem id',
////  `created_at` timestamp(0) NULL DEFAULT NULL,
////  `updated_at` timestamp(0) NULL DEFAULT NULL,
//// PRIMARY KEY(`id`) USING BTREE,
//// INDEX `company_id`(`i_company_id`) USING BTREE,
//// INDEX `group_id`(`i_group_id`) USING BTREE,
//// INDEX `oem_id`(`i_oem_id`) USING BTREE,
//// INDEX `sort`(`d_sort`) USING BTREE,
//// INDEX `state`(`t_state`) USING BTREE,
//// INDEX `user_id`(`i_user_id`) USING BTREE
////) ENGINE = InnoDB AUTO_INCREMENT = 763 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '物流管理_发货模版' ROW_FORMAT = Dynamic;

