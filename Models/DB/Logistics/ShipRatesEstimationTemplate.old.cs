﻿namespace ERP.Models.DB.Logistics;

//------------------------------
//--Table structure for erp3_ship_rates_estimation_template
//------------------------------
//DROP TABLE IF EXISTS `erp3_ship_rates_estimation_template`;
//CREATE TABLE `erp3_ship_rates_estimation_template`  (
//  `id` bigint unsigned NOT NULL,
//  `d_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '名称',
//  `d_data` json NOT NULL COMMENT '数据',
//  `d_order` int(0) NOT NULL DEFAULT 0 COMMENT '排序',
//  `t_mode` int(0) NOT NULL COMMENT '模式',
//  `i_user_id` int(0) NOT NULL DEFAULT 0 COMMENT '创建用户id',
//  `i_group_id` int(0) NOT NULL DEFAULT 0 COMMENT '创建用户组id',
//  `i_company_id` int(0) NOT NULL DEFAULT 0 COMMENT '公司id',
//  `i_oem_id` int(0) NOT NULL DEFAULT 0 COMMENT 'oem_id',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
// PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 161 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '运费估算模板表' ROW_FORMAT = Dynamic;
