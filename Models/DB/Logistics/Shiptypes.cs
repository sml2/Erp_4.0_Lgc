//using ERP.Models.Abstract;
//using Microsoft.EntityFrameworkCore;
//using System.ComponentModel;


//namespace ERP.Models.DB.Logistics
//{
//    public class ShipTypes: UserModel
//    {
//        public ShipTypes(ulong shipTypeNoHash, Platforms platform, string code, string name, string firstLogisticParameter, DateTime firstPullTime)
//        {
//            ShipTypeNoHash = shipTypeNoHash;
//            Platform = platform;
//            Code = code;
//            Name = name;
//            FirstLogisticParameter = firstLogisticParameter;
//            FirstPullTime = firstPullTime;
//        }

//        [Comment("createHashCode")]
//        public ulong ShipTypeNoHash { get; set; }

//        [Comment("所属平台")]
//        /// <summary>
//        /// 所属平台
//        /// </summary>
//        public Platforms Platform { get; set; }

//        [Comment("货运线路id")]
//        public string Code { get; set; }


//        [Comment("货运线路内容Json")]
//        public string Name { get; set; }

//        [Comment("第一次获取路线的物流账户Json")]
//        public string FirstLogisticParameter { get; set; } = "[]";

//        [Comment("第一次获取路线的拉取时间")]
//        public DateTime FirstPullTime { get; set; }

//        [Comment("更新获取路线的物流账户Json")]
//        public string UpdateLogisticParameter { get; set; } = "[]";

//        [Comment("更新获取路线的拉取时间")]
//        public DateTime UpdatePullTime { get; set; }
//    }


//    public class UserShipType: UserModel
//    {
//        public UserShipType(ulong shipTypeNoHash, Platforms platform, string code, string logisticParameter)
//        {
//            ShipTypeNoHash = shipTypeNoHash;
//            Platform = platform;
//            Code = code;
//            LogisticParameter = logisticParameter;
//        }

//        [Comment("createHashCode")]
//        public ulong ShipTypeNoHash { get; set; }

//        [Comment("所属平台")]
//        /// <summary>
//        /// 所属平台
//        /// </summary>
//        public Platforms Platform { get; set; }

//        [Comment("货运线路id")]
//        public string Code { get; set; }


//        [Comment("物流账户Json")]
//        public string LogisticParameter { get; set; } = "[]";

//    }

//    public class ShipTypesLog: UserModel
//    {
//        public ShipTypesLog(string logisticParameter, Platforms platform, int count, string data, DateTime pullTime)
//        {
//            LogisticParameter = logisticParameter;
//            Platform = platform;
//            Count = count;
//            Data = data;
//            PullTime = pullTime;
//        }

//        [Comment("物流账户Json")]
//        public string LogisticParameter { get; set; } = string.Empty;


//        [Comment("所属平台")]
//        /// <summary>
//        /// 所属平台
//        /// </summary>
//        public Platforms Platform { get; set; }


//        [Comment("线路数量")]
//        public int Count { get; set; }


//        [Comment("线路数据Json")]
//        public string Data { get; set; } = "[]";


//        [Comment("拉取时间")]
//        public DateTime PullTime { get; set; } = DateTime.Now;

//    }
//}
