//using ERP.Models.Abstract;
//using Microsoft.EntityFrameworkCore;
//using System.ComponentModel.DataAnnotations.Schema;
//using Newtonsoft.Json;
//using System.Collections.ObjectModel;

//namespace ERP.Models.DB.Logistics
//{
//    public class JsonData
//    {
//        public JsonData(string request, string response, string result)
//        {
//            Request = request;
//            Response = response;
//            Result = result;
//        }
//        public DateTime CurrentTime { get; set; } = DateTime.Now;
//        public string Request { get; set; }
//        public string Response { get; set; }
//        public string Result { get; set; }
//    }

//    public enum FieldMode
//    {
//        send = 0,
//        track,
//        price,
//        print,
//        cancle,
//    }

//    [Index(nameof(OrderNoHash), nameof(WaybillNoHash))]
//    public class OperationLogging : UserModel
//    {
//        [NotMapped]
//        public List<JsonData> jsonDatas { get; set; } = new();

//        public ReadOnlyCollection<JsonData> LogScope(JsonData jsonData, FieldMode fieldMode)
//        {
//            string? jsons = "";
//            switch (fieldMode)
//            {
//                case FieldMode.send:
//                    jsons = this.Send;
//                    break;
//                case FieldMode.track:
//                    jsons = this.Track;
//                    break;
//                case FieldMode.price:
//                    jsons = this.Price;
//                    break;
//                case FieldMode.print:
//                    jsons = this.Print;
//                    break;
//                case FieldMode.cancle:
//                    jsons = this.Cancle;
//                    break;
//                default: throw new Exception("OperationLogging FieldMode error");
//            }
//            if (jsons.IsNotWhiteSpace())
//            {
//                jsons = "[]";

//            }
//            jsonDatas = JsonConvert.DeserializeObject<List<JsonData>>(jsons ?? "") ?? new();
//            jsonDatas.Add(jsonData);
//            return jsonDatas.AsReadOnly();
//        }

//        public string GetJson(ReadOnlyCollection<JsonData> jsonDatas)
//        {
//            return JsonConvert.SerializeObject(jsonDatas);
//        }

//        [Comment("订单id")]
//        public int OrderId { get; set; }

//        [Comment("合并订单id")]
//        public int? MergeID { get; set; }

//        [Comment("OrderNoHashCode")]
//        public ulong OrderNoHash { get; set; }

//        [Comment("运单id")]
//        public int WaybillId { get; set; }

//        [Comment("WaybillNoHashCode")]
//        public ulong WaybillNoHash { get; set; }

//        [Column(TypeName = "JSON")]
//        public string? Send { get; set; } = "[]";  //JSON

//        [Comment("第一次send时间")]
//        public DateTime SendFirst { get; set; }

//        [Comment("send成功发送时间")]
//        public DateTime SendSuccess { get; set; }


//        [Comment("send发送次数")]
//        public int SendNums { get; set; }


//        [Column(TypeName = "JSON")]
//        public string? Track { get; set; } = "[]";     //JSON

//        [Comment("第一次Track时间")]
//        public DateTime? TrackFirst { get; set; }

//        [Comment("第一次Track成功获取到内容的时间")]
//        public DateTime? TrackSuccess { get; set; }

//        [Comment("最后一次Track时间")]
//        public DateTime? TrackLast { get; set; }

//        [Comment("Track发送次数")]
//        public int TrackNums { get; set; }

//        [Column(TypeName = "JSON")]
//        public string? Price { get; set; } = "[]";   //JSON

//        [Comment("第一次Price时间")]
//        public DateTime? PriceFirst { get; set; }

//        [Comment("第一次Price成功获取到内容的时间")]
//        public DateTime? PriceSuccess { get; set; }

//        [Comment("最后一次Price时间")]
//        public DateTime? PriceLast { get; set; }

//        [Comment("Price发送次数")]
//        public int PriceNums { get; set; }

//        [Column(TypeName = "JSON")]
//        public string? Print { get; set; } = "[]";    //JSON

//        [Comment("第一次Print时间")]
//        public DateTime? PrintFirst { get; set; }

//        [Comment("第一次Print成功获取到内容的时间")]
//        public DateTime? PrintSuccess { get; set; }

//        [Comment("最后一次Print时间")]
//        public DateTime? PrintLast { get; set; }

//        [Comment("Print发送次数")]
//        public int PrintNums { get; set; }

//        //
//        [Column(TypeName = "JSON")]
//        public string? Cancle { get; set; } = "[]";    //JSON

//        [Comment("第一次Cancle时间")]
//        public DateTime? CancleFirst { get; set; }

//        [Comment("第一次Cancle成功获取到内容的时间")]
//        public DateTime? CancleSuccess { get; set; }

//        [Comment("最后一次Cancle时间")]
//        public DateTime? CancleLast { get; set; }

//        [Comment("Cancle发送次数")]
//        public int CancleNums { get; set; }
//    }
//}
