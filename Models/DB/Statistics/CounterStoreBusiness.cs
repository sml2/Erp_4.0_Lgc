﻿using ERP.Models.DB.Identity;
using ERP.Interface;
using ERP.Models.Abstract;
using ERP.Models.DB.Users;
using ERP.Models.Setting;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.Statistics;

[Index(nameof(StoreId))]
public class CounterStoreBusiness : UserModel
{
    public CounterStoreBusiness() : base() { }
    public CounterStoreBusiness(User? user, Group? userGroup, Company? company, OEM? oem) : base(user, userGroup, company, oem) { }
    public CounterStoreBusiness(int UID, int groupID, int companyID, int oemID) : base(UID, groupID, companyID, oemID) { }
    public CounterStoreBusiness(ISessionProvider sessionProvider) : base(sessionProvider) { }
    public CounterStoreBusiness(ISession session) : base(session) { }
    /// <summary>
    /// 店铺id
    /// </summary>

    [Comment("店铺id")]
    public int StoreId { get; set; }

    /// <summary>
    /// 订单总价汇总(基准货币 完结 所属店铺 汇总)
    /// </summary>

    [Comment("订单总价汇总(基准货币 完结 所属店铺 汇总)")]
    public decimal? OrderTotal { get; set; }

    /// <summary>
    /// 订单手续费汇总(基准货币 完结 所属店铺 汇总)
    /// </summary>

    [Comment("订单手续费汇总(基准货币 完结 所属店铺 汇总)")]
    public decimal? OrderTotalFee { get; set; }

    /// <summary>
    /// 订单退款汇总(基准货币 完结 所属店铺 汇总)
    /// </summary>

    [Comment("订单退款汇总(基准货币 完结 所属店铺 汇总)")]
    public decimal? OrderTotalRefund { get; set; }

    /// <summary>
    /// 订单损耗CNY(基准货币 完结 所属公司 汇总)
    /// </summary>

    [Comment("订单损耗CNY(基准货币 完结 所属公司 汇总)")]
    public decimal? OrderTotalLoss { get; set; }

    /// <summary>
    /// 采购总价汇总(基准货币 完结 所属店铺 汇总)
    /// </summary>

    [Comment("采购总价汇总(基准货币 完结 所属店铺 汇总)")]
    public decimal? PurchaseTotal { get; set; }

    /// <summary>
    /// 采购退款汇总(基准货币 完结 所属店铺 汇总)
    /// </summary>

    [Comment("采购退款汇总(基准货币 完结 所属店铺 汇总)")]
    public decimal? PurchaseTotalRefund { get; set; }

    /// <summary>
    /// 运费总价汇总(基准货币 完结 所属店铺 汇总)
    /// </summary>

    [Comment("运费总价汇总(基准货币 完结 所属店铺 汇总)")]
    public decimal? WaybillTotal { get; set; }

    /// <summary>
    /// 运费退款汇总(基准货币 完结 所属店铺 汇总)
    /// </summary>

    [Comment("运费退款汇总(基准货币 完结 所属店铺 汇总)")]
    public decimal? WaybillTotalRefund { get; set; }

    //public function getOrderTotalAttribute($order_total)
    //{
    //    return ToConversion($order_total, $this->getSession()->getUnitConfig());
    //}

    //public function getOrderTotalFeeAttribute($order_total_fee)
    //{
    //    return ToConversion($order_total_fee, $this->getSession()->getUnitConfig());
    //}

    //public function getOrderTotalRefundAttribute($order_total_refund)
    //{
    //    return ToConversion($order_total_refund, $this->getSession()->getUnitConfig());
    //}
    //public function getOrderTotalLossAttribute($order_total_loss)
    //{
    //    return ToConversion($order_total_loss, $this->getSession()->getUnitConfig());
    //}
    //public function getPurchaseTotalAttribute($purchase_total)
    //{
    //    return ToConversion($purchase_total, $this->getSession()->getUnitConfig());
    //}

    //public function getPurchaseTotalRefundAttribute($purchase_total_refund)
    //{
    //    return ToConversion($purchase_total_refund, $this->getSession()->getUnitConfig());
    //}

    //public function getWaybillTotalAttribute($waybill_total)
    //{
    //    return ToConversion($waybill_total, $this->getSession()->getUnitConfig());
    //}

    //public function getWaybillTotalRefundAttribute($waybill_total_refund)
    //{
    //    return ToConversion($waybill_total_refund, $this->getSession()->getUnitConfig());
    //}

    //public function scopeStoreId(Builder $query, $store_id)
    //{
    //    return $query->where('store_id', '=', $store_id);
    //}

    //public function scopeInStoreId(Builder $query, $store_id)
    //{
    //    return $query->whereIn('store_id', $store_id);
    //}

    //public function scopeCompanyId(Builder $query, $company_id)
    //{
    //    if ($company_id) {
    //        return $query->where('company_id', '=', $company_id);
    //    }
    //}

    //public function scopeOemId(Builder $query, $oem_id)
    //{
    //    if ($oem_id) {
    //        return $query->where('oem_id', '=', $oem_id);
    //    }
    //}
}

//-- ----------------------------
//-- Table structure for erp3_counter_store_business
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_counter_store_business`;
//CREATE TABLE `erp3_counter_store_business`  (
//  `id` bigint unsigned NOT NULL,
//  `i_store_id` int unsigned NOT NULL COMMENT '店铺ID',
//  `i_company_id` int unsigned NOT NULL COMMENT '公司ID',
//  `i_oem_id` int unsigned NOT NULL COMMENT 'OEM ID',
//  `c_order_total` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '订单总价汇总(基准货币 完结 所属店铺 汇总)',
//  `c_order_total_fee` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '订单手续费汇总(基准货币 完结 所属店铺 汇总)',
//  `c_order_total_refund` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '订单退款汇总(基准货币 完结 所属店铺 汇总)',
//  `c_order_total_loss` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '订单损耗CNY(基准货币 完结 所属公司 汇总)',
//  `c_purchase_total` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '采购总价汇总(基准货币 完结 所属店铺 汇总)',
//  `c_purchase_total_refund` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '采购退款汇总(基准货币 完结 所属店铺 汇总)',
//  `c_waybill_total` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '运费总价汇总(基准货币 完结 所属店铺 汇总)',
//  `c_waybill_total_refund` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '运费退款汇总(基准货币 完结 所属店铺 汇总)',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `store_id`(`i_store_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 15787 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '数据统计_店铺业务统计' ROW_FORMAT = Dynamic;