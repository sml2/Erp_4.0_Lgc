﻿using ERP.Interface;
using ERP.Models.Abstract;
using ERP.Models.DB.Users;
using ERP.Models.Setting;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.Models.Statistics;

public class CounterGroupBusiness : GroupModel
{
    public CounterGroupBusiness() : base() { }
    public CounterGroupBusiness(Group? userGroup, Company? company, OEM? oem) : base( userGroup, company, oem) { }
    public CounterGroupBusiness(int UID, int groupID, int companyID, int oemID) : base( groupID, companyID, oemID) { }
    public CounterGroupBusiness(ISessionProvider sessionProvider) : base(sessionProvider) { }
    public CounterGroupBusiness(ISession session) : base(session) { }
    /// <summary>
    /// 当前用户组创建开通用户数(添加即汇总)
    /// </summary>

    [Comment("当前用户组创建开通用户数(添加即汇总)")]
    public int UserNum { get; set; }

    /// <summary>
    /// 当前用户组创建产品数(添加即汇总)
    /// </summary>

    [Comment("当前用户组创建产品数(添加即汇总)")]
    public int ProductNum { get; set; }

    /// <summary>
    /// 当前用户组创建订单数(添加即汇总)
    /// </summary>

    [Comment("当前用户组创建订单数(添加即汇总)")]
    public int OrderNum { get; set; }

    /// <summary>
    /// 当前用户组创建采购数(添加即汇总)
    /// </summary>

    [Comment("当前用户组创建采购数(添加即汇总)")]
    public int PurchaseNum { get; set; }

    /// <summary>
    /// 当前用户组创建运单数(添加即汇总)
    /// </summary>

    [Comment("当前用户组创建运单数(添加即汇总)")]
    public int WaybillNum { get; set; }

    /// <summary>
    /// 当前用户组创建店铺数(添加即汇总)
    /// </summary>

    [Comment("当前用户组创建店铺数(添加即汇总)")]
    public int StoreNum { get; set; }

    /// <summary>
    /// 当前用户组处理上报分销订单数(添加即汇总)
    /// </summary>

    [Comment("当前用户组处理上报分销订单数(添加即汇总)")]
    public int DistributionOrderNum { get; set; }

    /// <summary>
    /// 当前用户组处理上报分销采购数(添加即汇总)
    /// </summary>

    [Comment("当前用户组处理上报分销采购数(添加即汇总)")]
    public int DistributionPurchaseNum { get; set; }

    /// <summary>
    /// 当前用户组处理上报分销运单数(添加即汇总)
    /// </summary>

    [Comment("当前用户组处理上报分销运单数(添加即汇总)")]
    public int DistributionWaybillNum { get; set; }

    /// <summary>
    /// 订单总价汇总(基准货币 完结 触发用户用户组 汇总)
    /// </summary>

    [Comment("订单总价汇总(基准货币 完结 触发用户用户组 汇总)")]
    public decimal OrderTotal { get; set; }

    /// <summary>
    /// 订单手续费汇总(基准货币 完结 触发用户用户组 汇总)
    /// </summary>

    [Comment("订单手续费汇总(基准货币 完结 触发用户用户组 汇总)")]
    public decimal OrderTotalFee { get; set; }

    /// <summary>
    /// 订单退款汇总(基准货币 完结 触发用户用户组 汇总)
    /// </summary>

    [Comment("订单退款汇总(基准货币 完结 触发用户用户组 汇总)")]
    public decimal OrderTotalRefund { get; set; }

    /// <summary>
    /// 订单损耗CNY(基准货币 完结 所属公司 汇总)
    /// </summary>

    [Comment("订单损耗CNY(基准货币 完结 所属公司 汇总)")]
    public int OrderTotalLoss { get; set; }

    /// <summary>
    /// 采购总价汇总(基准货币 完结 触发用户用户组 汇总)
    /// </summary>

    [Comment("采购总价汇总(基准货币 完结 触发用户用户组 汇总)")]
    public decimal PurchaseTotal { get; set; }

    /// <summary>
    /// 采购退款汇总(基准货币 完结 触发用户用户组 汇总)
    /// </summary>

    [Comment("采购退款汇总(基准货币 完结 触发用户用户组 汇总)")]
    public decimal PurchaseTotalRefund { get; set; }

    /// <summary>
    /// 运费总价汇总(基准货币 完结 触发用户用户组 汇总)
    /// </summary>

    [Comment("运费总价汇总(基准货币 完结 触发用户用户组 汇总)")]
    public decimal WaybillTotal { get; set; }

    /// <summary>
    /// 运费退款汇总(基准货币 完结 触发用户用户组 汇总)
    /// </summary>

    [Comment("运费退款汇总(基准货币 完结 触发用户用户组 汇总)")]
    public decimal WaybillTotalRefund { get; set; }

    /// <summary>
    /// 自用钱包充值汇总(基准货币 完结 触发用户用户组 汇总)
    /// </summary>

    [Comment("自用钱包充值汇总(基准货币 完结 触发用户用户组 汇总)")]
    public decimal SelfRechargeTotal { get; set; }


    /// <summary>
    /// 自用钱包支出汇总(基准货币 完结 触发用户用户组 汇总)
    /// </summary>

    [Comment("自用钱包支出汇总(基准货币 完结 触发用户用户组 汇总)")]
    public decimal SelfExpenseTotal { get; set; }

    /// <summary>
    /// 钱包充值汇总(基准货币 完结 触发用户用户组 汇总)
    /// </summary>

    [Comment("钱包充值汇总(基准货币 完结 触发用户用户组 汇总)")]
    public decimal RechargeTotal { get; set; }

    /// <summary>
    /// 钱包支出汇总(基准货币 完结 触发用户用户组 汇总)
    /// </summary>

    [Comment("钱包支出汇总(基准货币 完结 触发用户用户组 汇总)")]
    public decimal ExpenseTotal { get; set; }

    //public function getOrderTotalAttribute($order_total)
    //{
    //    return ToConversion($order_total, $this->getSession()->getUnitConfig());
    //}

    //public function getOrderTotalFeeAttribute($order_total_fee)
    //{
    //    return ToConversion($order_total_fee, $this->getSession()->getUnitConfig());
    //}

    //public function getOrderTotalRefundAttribute($order_total_refund)
    //{
    //    return ToConversion($order_total_refund, $this->getSession()->getUnitConfig());
    //}

    //public function getOrderTotalLossAttribute($order_total_loss)
    //{
    //    return ToConversion($order_total_loss, $this->getSession()->getUnitConfig());
    //}

    //public function getPurchaseTotalAttribute($purchase_total)
    //{
    //    return ToConversion($purchase_total, $this->getSession()->getUnitConfig());
    //}

    //public function getPurchaseTotalRefundAttribute($purchase_total_refund)
    //{
    //    return ToConversion($purchase_total_refund, $this->getSession()->getUnitConfig());
    //}

    //public function getWaybillTotalAttribute($waybill_total)
    //{
    //    return ToConversion($waybill_total, $this->getSession()->getUnitConfig());
    //}

    //public function getWaybillTotalRefundAttribute($waybill_total_refund)
    //{
    //    return ToConversion($waybill_total_refund, $this->getSession()->getUnitConfig());
    //}

    //public function getSelfRechargeTotalAttribute($self_recharge_total)
    //{
    //    return ToConversion($self_recharge_total, $this->getSession()->getUnitConfig());
    //}

    //public function getSelfExpenseTotalAttribute($self_expense_total)
    //{
    //    return ToConversion($self_expense_total, $this->getSession()->getUnitConfig());
    //}

    //public function getRechargeTotalAttribute($recharge_total)
    //{
    //    return ToConversion($recharge_total, $this->getSession()->getUnitConfig());
    //}

    //public function getExpenseTotalAttribute($expense_total)
    //{
    //    return ToConversion($expense_total, $this->getSession()->getUnitConfig());
    //}

    //public function scopeGroupId(Builder $query, $group_id)
    //{
    //    return $query->where('group_id', '=', $group_id);
    //}

    //public function scopeInGroupId(Builder $query, $user_id)
    //{
    //    return $query->whereIn('group_id', $user_id);
    //}

    //public function scopeCompanyId(Builder $query, $company_id)
    //{
    //    if ($company_id) {
    //        return $query->where('company_id', '=', $company_id);
    //    }
    //}

    //public function scopeOemId(Builder $query, $oem_id)
    //{
    //    if ($oem_id) {
    //        return $query->where('oem_id', "=", $oem_id);
    //    }
    //}
}

//-- ----------------------------
//-- Table structure for erp3_counter_group_business
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_counter_group_business`;
//CREATE TABLE `erp3_counter_group_business`  (
//  `id` bigint unsigned NOT NULL,
//  `i_group_id` bigint unsigned NOT NULL COMMENT '用户组ID',
//  `i_company_id` int unsigned NOT NULL COMMENT '公司ID',
//  `i_oem_id` int unsigned NOT NULL COMMENT 'OEM ID',
//  `c_user_num` int (0) NOT NULL DEFAULT 0 COMMENT '当前用户组创建开通用户数(添加即汇总)',
//  `c_product_num` bigint(0) NOT NULL DEFAULT 0 COMMENT '当前用户组创建产品数(添加即汇总)',
//  `c_order_num` int (0) NOT NULL DEFAULT 0 COMMENT '当前用户组创建订单数(添加即汇总)',
//  `c_purchase_num` int (0) NOT NULL DEFAULT 0 COMMENT '当前用户组创建采购数(添加即汇总)',
//  `c_waybill_num` int (0) NOT NULL DEFAULT 0 COMMENT '当前用户组创建运单数(添加即汇总)',
//  `c_store_num` int (0) NOT NULL DEFAULT 0 COMMENT '当前用户组创建店铺数(添加即汇总)',
//  `c_distribution_order_num` int (0) NOT NULL DEFAULT 0 COMMENT '当前用户组处理上报分销订单数(添加即汇总)',
//  `c_distribution_purchase_num` int (0) NOT NULL DEFAULT 0 COMMENT '当前用户组处理上报分销采购数(添加即汇总)',
//  `c_distribution_waybill_num` int (0) NOT NULL DEFAULT 0 COMMENT '当前用户组处理上报分销运单数(添加即汇总)',
//  `c_order_total` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '订单总价汇总(基准货币 完结 触发用户用户组 汇总)',
//  `c_order_total_fee` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '订单手续费汇总(基准货币 完结 触发用户用户组 汇总)',
//  `c_order_total_refund` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '订单退款汇总(基准货币 完结 触发用户用户组 汇总)',
//  `c_order_total_loss` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '订单损耗CNY(基准货币 完结 所属公司 汇总)',
//  `c_purchase_total` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '采购总价汇总(基准货币 完结 触发用户用户组 汇总)',
//  `c_purchase_total_refund` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '采购退款汇总(基准货币 完结 触发用户用户组 汇总)',
//  `c_waybill_total` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '运费总价汇总(基准货币 完结 触发用户用户组 汇总)',
//  `c_waybill_total_refund` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '运费退款汇总(基准货币 完结 触发用户用户组 汇总)',
//  `c_self_recharge_total` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '自用钱包充值汇总(基准货币 完结 触发用户用户组 汇总)',
//  `c_self_expense_total` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '自用钱包支出汇总(基准货币 完结 触发用户用户组 汇总)',
//  `c_recharge_total` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '钱包充值汇总(基准货币 完结 触发用户用户组 汇总)',
//  `c_expense_total` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '钱包支出汇总(基准货币 完结 触发用户用户组 汇总)',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 11263 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '数据统计_用户组业务统计' ROW_FORMAT = Dynamic;