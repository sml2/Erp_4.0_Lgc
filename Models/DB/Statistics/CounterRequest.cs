﻿using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;

namespace ERP.Models.Statistics;

public class CounterRequest : UserModel
{

    /// <summary>
    /// 用户名
    /// </summary>

    [Comment("用户名")]
    public string? UserName { get; set; }

    /// <summary>
    /// 用户真实姓名
    /// </summary>

    [Comment("用户真实姓名")]
    public string? TrueName { get; set; }

    /// <summary>
    /// 请求资源(请求次数)
    /// </summary>

    [Comment("请求资源(请求次数)")]
    public int Request { get; set; } = 0;

    /// <summary>
    /// cpu(秒)
    /// </summary>

    [Comment("cpu(秒)")]
    public int Cpu { get; set; } = 0;

    /// <summary>
    /// 内存(字节B)
    /// </summary>

    [Comment("内存(字节B)")]
    public int Memory { get; set; } = 0;

    /// <summary>
    /// 带宽(字节B)
    /// </summary>

    [Comment("带宽(字节B)")]
    public int Network { get; set; } = 0;

    /// <summary>
    /// 硬盘(字节B)
    /// </summary>

    [Comment("硬盘(字节B)")]
    public int Disk { get; set; } = 0;

    /// <summary>
    /// 图片抠图(请求次数)
    /// </summary>

    [Comment("图片抠图(请求次数)")]
    public int PicApi { get; set; } = 0;

    /// <summary>
    /// 翻译(请求次数)
    /// </summary>

    [Comment("翻译(请求次数)")]
    public int TranslateApi { get; set; } = 0;
}

//-- ----------------------------
//-- Table structure for erp3_counter_request
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_counter_request`;
//CREATE TABLE `erp3_counter_request`  (
//  `id` bigint unsigned NOT NULL,
//  `i_user_id` bigint(0) NOT NULL DEFAULT 0 COMMENT '用户userid',
//  `i_group_id` bigint(0) NOT NULL DEFAULT 0 COMMENT '分组id',
//  `i_company_id` bigint(0) NOT NULL DEFAULT 0 COMMENT '公司id',
//  `u_username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户名',
//  `u_truename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户真实姓名',
//  `c_request` bigint unsigned NOT NULL COMMENT '请求资源(请求次数)',
//  `c_cpu` bigint unsigned NOT NULL COMMENT 'cpu(秒)',
//  `c_memory` bigint unsigned NOT NULL COMMENT '内存(字节B)',
//  `c_network` bigint unsigned NOT NULL COMMENT '带宽(字节B)',
//  `c_disk` bigint(0) NOT NULL DEFAULT 0 COMMENT '硬盘(字节B)',
//  `c_pic_api` int unsigned NULL COMMENT '图片抠图(请求次数)',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  `c_translate_api` bigint unsigned NULL COMMENT '翻译(请求次数)',
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 12336 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '数据统计_用户资源' ROW_FORMAT = Dynamic;