using ERP.Models.DB.Identity;
using ERP.Interface;
using ERP.Models.Abstract;
using ERP.Models.DB.Users;
using ERP.Models.Setting;
using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

namespace ERP.Models.DB.Stores;

//[Index(nameof(BrowserNodeId), nameof(Hash), nameof(Level), nameof(NameHash), nameof(NationId), nameof(Sort), nameof(TypeHash))]
//public class CategoryNodeDefault : UserModel
//{
//    public CategoryNodeDefault() : base() { }
//    public CategoryNodeDefault(User? user, Group? userGroup, Company? company, OEM? oem) : base(user, userGroup, company, oem) { }
//    public CategoryNodeDefault(int UID, int groupID, int companyID, int oemID) : base(UID, groupID, companyID, oemID) { }
//    public CategoryNodeDefault(ISessionProvider sessionProvider) : base(sessionProvider) { }
//    public CategoryNodeDefault(ISession session) : base(session) { }
//    /// <summary>
//    /// 模板名称
//    /// </summary>

//    [Comment("模板名称")]
//    public string? TempName { get; set; }

//    /// <summary>
//    /// 国家id
//    /// </summary>

//    [Comment("国家id")]
//    public int NationId { get; set; }

//    /// <summary>
//    /// 语言id
//    /// </summary>

//    [Comment("语言id")]
//    public int LanguageId { get; set; }
//    /// <summary>
//    /// 语言简称
//    /// </summary>

//    [Comment("语言简称")]
//    public int LanguageCode { get; set; }

//    /// <summary>
//    /// 排序字段
//    /// </summary>

//    [Comment("排序字段")]
//    public int Sort { get; set; }

//    public enum Levels
//    {
//        [Description("平台")]
//        WORKBENCH = 0,

//        [Description("公司")]
//        COMPANY = 1,

//        [Description("用户")]
//        GROUP = 2
//    }

//    /// <summary>
//    /// 使用级别 0平台 1公司 2用户组
//    /// </summary>

//    [Comment("使用级别 0平台 1公司 2用户组")]
//    public Levels Level { get; set; }

//    /// <summary>
//    /// 节点id
//    /// </summary>

//    [Comment("节点id")]
//    public int BrowserNodeId { get; set; }

//    /// <summary>
//    /// 节点描述（UI展示）
//    /// </summary>

//    [Comment("节点描述（UI展示）")]
//    public string? BrowserUi { get; set; }

   

//    /// <summary>
//    /// 模版名称hash temp_name createHashCode
//    /// </summary>

//    [Comment("模版名称hash temp_name createHashCode")]
//    public ulong NameHash { get; set; }

//    /// <summary>
//    /// company_id+type_data+language_code+browserid createHashCode
//    /// </summary>

//    [Comment("company_id+type_data+language_code+browserid createHashCode")]
//    public ulong Hash { get; set; }

//    /// <summary>
//    /// type_data  createHashCode
//    /// </summary>

//    [Comment("type_data  createHashCode")]
//    public ulong TypeHash { get; set; }

//    /// <summary>
//    /// 分类
//    /// </summary>

//    [Comment("分类")]
//    public string? TypeData { get; set; }

    
   
//}

//-- ----------------------------
//-- Table structure for erp3_category_node_default
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_category_node_default`;
//CREATE TABLE `erp3_category_node_default`  (
//  `id` bigint unsigned NOT NULL,
//  `d_temp_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '模板名称',
//  `i_company_id` int unsigned NOT NULL COMMENT '公司id)',
//  `i_user_id` int unsigned NOT NULL COMMENT '用户id)',
//  `i_group_id` int unsigned NOT NULL COMMENT '用户组id)',
//  `i_oem_id` int unsigned NOT NULL COMMENT 'oemid',
//  `i_nation_id` int unsigned NOT NULL COMMENT '国家id',
//  `i_language_id` int unsigned NULL COMMENT '语言id',
//  `d_sort` int (0) NOT NULL DEFAULT 0 COMMENT '排序字段',
//  `t_level` tinyint unsigned NOT NULL COMMENT '使用级别 0平台 1公司 2用户组',
//  `i_browser_node_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '节点id',
//  `u_browser_ui` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '节点描述（UI展示）',
//  `d_language_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '语言简称',
//  `d_name_hash` bigint unsigned NOT NULL COMMENT '模版名称hash temp_name createHashCode',
//  `d_hash` bigint unsigned NOT NULL COMMENT 'company_id+type_data+language_code+browserid createHashCode',
//  `d_type_hash` bigint unsigned NOT NULL COMMENT 'type_data  createHashCode',
//  `d_type_data` json NOT NULL COMMENT '分类',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `browser_node_id`(`i_browser_node_id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `hash`(`d_hash`) USING BTREE,
// INDEX `level`(`t_level`) USING BTREE,
// INDEX `name_hash`(`d_name_hash`) USING BTREE,
// INDEX `nation_id`(`i_nation_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `sort`(`d_sort`) USING BTREE,
// INDEX `type_hash`(`d_type_hash`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 1936 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户配置节点选择表' ROW_FORMAT = Dynamic;
