using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;

namespace ERP.Models.DB.Stores;

//[Index(nameof(Spid), nameof(StoreId))]
//public class SyncMws : UserModel
//{
//    /// <summary>
//    /// 店铺id
//    /// </summary>

//    [Comment("店铺id")]
//    public int StoreId { get; set; }

//    /// <summary>
//    /// [镜像]产品id
//    /// </summary>

//    [Comment("[镜像]产品id")]
//    public int Spid { get; set; }

//    /// <summary>
//    /// 产品名称
//    /// </summary>

//    [Comment("产品名称")]
//    public string? ProductName { get; set; }

//    /// <summary>
//    /// 产品冗余
//    /// </summary>

//    [Comment("产品冗余")]
//    public string? product { get; set; }

//    /// <summary>
//    /// 店铺名称
//    /// </summary>

//    [Comment("店铺名称")]
//    public string? StoreName { get; set; }

//    /// <summary>
//    /// 语言
//    /// </summary>

//    [Comment("语言")]
//    public string? Language { get; set; }

//    /// <summary>
//    /// 亚马逊节点编号
//    /// </summary>

//    [Comment("亚马逊节点编号")]
//    public int BrowserNodeId { get; set; }

//    public enum BrowserNodeTypes
//    {
//        [Description("用户输入")]
//        USERINPUT = 0,

//        [Description("选择")]
//        SELECTOR = 1
//    }

//    /// <summary>
//    /// 亚马逊节点编号类型；0=用户输入，1=选择
//    /// </summary>

//    [Comment("亚马逊节点编号类型；0=用户输入，1=选择")]
//    public BrowserNodeTypes BrowserNodeType { get; set; }

//    /// <summary>
//    /// 修改数据
//    /// </summary>

//    [Comment("修改数据")]
//    public string? SkuChangeData { get; set; }

//    public enum States
//    {
//        [Description("等待执行")]
//        WAITING = 1,

//        [Description("执行中")]
//        EXECUTING = 2,

//        [Description("执行")]
//        SUCCESS = 3,

//        [Description("执行失败")]
//        ERROR = 4
//    }

//    /// <summary>
//    /// 默认STATE_WAITING=1等待执行 STATE_EXECUTING=2 执行中  STATE_SUCCESS = 3执行 STATE_ERROR=4 执行失败
//    /// </summary>

//    [Comment("默认STATE_WAITING=1等待执行 STATE_EXECUTING=2 执行中  STATE_SUCCESS = 3执行 STATE_ERROR=4 执行失败")]
//    public States State { get; set; }

//    /// <summary>
//    /// sync_product表id
//    /// </summary>

//    [Comment("sync_product表id")]
//    public int UploadId { get; set; }

//    /// <summary>
//    /// 备货时间（单位：天）
//    /// </summary>

//    [Comment("备货时间（单位：天）")]
//    public DateTime LeadTime { get; set; }

//    public enum EanupcStatuses
//    {
//        [Description("没")]
//        NOTHING = 0,

//        [Description("部分")]
//        PORTION = 1,

//        [Description("全部")]
//        ALL = 2
//    }

//    /// <summary>
//    /// EANupc分配情况 0(没),1(部分),2(全部)
//    /// </summary>

//    [Comment("EANupc分配情况 0(没),1(部分),2(全部)")]
//    public EanupcStatuses EanupcStatus { get; set; }

//    /// <summary>
//    /// 原产国
//    /// </summary>

//    [Comment("原产国")]
//    public int CountryOfOrigin { get; set; }
//}

//-- ----------------------------
//-- Table structure for erp3_sync_mws
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_sync_mws`;
//CREATE TABLE `erp3_sync_mws`  (
//  `id` bigint unsigned NOT NULL,
//  `i_store_id` int unsigned NOT NULL COMMENT '店铺id',
//  `i_spid` int unsigned NOT NULL COMMENT '[镜像]产品id',
//  `i_user_id` int unsigned NOT NULL COMMENT '用户id',
//  `i_group_id` int unsigned NOT NULL COMMENT '当前用户组id',
//  `i_company_id` int unsigned NOT NULL COMMENT '店铺公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `u_product_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '产品名称',
//  `u_product` json NOT NULL COMMENT '产品冗余',
//  `u_store_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '店铺名称',
//  `u_language` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '语言',
//  `d_browser_node_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '亚马逊节点编号',
//  `t_browser_node_type` tinyint(0) NOT NULL DEFAULT 0 COMMENT '亚马逊节点编号类型；0=用户输入，1=选择',
//  `d_sku_change_data` json NULL COMMENT '修改数据',
//  `t_state` tinyint(0) NOT NULL DEFAULT 1 COMMENT '默认STATE_WAITING=1等待执行 STATE_EXECUTING=2 执行中  STATE_SUCCESS = 3执行 STATE_ERROR=4 执行失败 ',
//  `i_upload_id` int unsigned NULL COMMENT 'sync_product表id',
//  `d_lead_time` int unsigned NOT NULL COMMENT '备货时间（单位：天）',
//  `t_eanupc_status` tinyint unsigned NOT NULL COMMENT 'EANupc分配情况 0(没),1(部分),2(全部)',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  `d_country_of_origin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '原产国',
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `pid`(`i_spid`) USING BTREE,
// INDEX `sid`(`i_store_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 225928 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = 'Mws_商品上传亚马逊店铺表' ROW_FORMAT = Dynamic;