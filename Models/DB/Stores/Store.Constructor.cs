//using ERP.Models.Abstract;

//namespace ERP.Models.DB.Stores;

//using Enums;
//using Models.DB.Identity;
//using Extensions;
//using Interface;
//using Models.Abstract;
//using Models.DB.Users;
//using Models.Setting;
//using EID = Enums.ID;

//public partial class StoreRegion: UserModel
//{
//    #pragma warning disable CS8618 // 在退出构造函数时，不可为 null 的字段必须包含非 null 值。请考虑声明为可以为 null。
//    public StoreRegion() : base() { }
//    public StoreRegion(User? user, Group? userGroup, Company? company, OEM? oem) : base(user, userGroup, company, oem) { }

//    public StoreRegion(int UID, int groupID, int companyID, int oemID) : base(UID, groupID, companyID, oemID) { }

//    public StoreRegion(ISessionProvider sessionProvider) : base(sessionProvider) { }
//    public StoreRegion(ISession session) : base(session) { }
//    #pragma warning restore CS8618 // 在退出构造函数时，不可为 null 的字段必须包含非 null 值。请考虑声明为可以为 null。
//    /// <summary>
//    /// Seed
//    /// </summary>
//    /// <param name="id"></param>
//    /// <param name="u"></param>
//    /// <param name="g"></param>
//    /// <param name="c"></param>
//    /// <param name="o"></param>
//    /// <param name="modes"></param>
//    /// <param name="continent"></param>
//    /// <param name="data"></param>
//    /// <param name="flag"></param>
//    /// <param name="hash"></param>
//    /// <param name="name"></param>
//    /// <param name="nid"></param>
//    /// <param name="n"></param>
//    /// <param name="ns"></param>
//    /// <param name="platform"></param>
//    /// <param name="unitid"></param>
//    /// <param name="unitname"></param>
//    /// <param name="unitsign"></param>
//    public StoreRegion(int id, Enums.Identity.User u, EID.Group g, EID.Company c, EID.OEM o, AddModes modes, Continents continent,
//        string data, Flags flag, ulong hash, string name, Platforms platform,
//        int unitid, string unitname, string unitsign) : this(u.GetID(), g.GetID(), c.GetID(), o.GetID())
//    {
//        ID = id;
//        AddMode = modes;
//        Continent = continent;
//        Data = data;
//        Flag = flag;
//        HashCode = hash;
//        Name = name;        
//        State = StateEnum.Open;
//        Platform = platform;
//        UnitID = unitid;
//        UnitName = unitname;
//        UnitSign = unitsign;
//        BelongsGroupID = g.GetID();
//    }
//}

//public partial class StoreMarket : UserModel
//{
//#pragma warning disable CS8618 // 在退出构造函数时，不可为 null 的字段必须包含非 null 值。请考虑声明为可以为 null。
//    public StoreMarket() : base() { }
//    public StoreMarket(User? user, Group? userGroup, Company? company, OEM? oem) : base(user, userGroup, company, oem) { }

//    public StoreMarket(int UID, int groupID, int companyID, int oemID) : base(UID, groupID, companyID, oemID) { }

//    public StoreMarket(ISessionProvider sessionProvider) : base(sessionProvider) { }
//    public StoreMarket(ISession session) : base(session) { }
//#pragma warning restore CS8618 // 在退出构造函数时，不可为 null 的字段必须包含非 null 值。请考虑声明为可以为 null。
//    /// <summary>
//    /// Seed
//    /// </summary>
//    /// <param name="id"></param>
//    /// <param name="u"></param>
//    /// <param name="g"></param>
//    /// <param name="c"></param>
//    /// <param name="o"></param>
//    /// <param name="modes"></param>
//    /// <param name="continent"></param>
//    /// <param name="data"></param>
//    /// <param name="flag"></param>
//    /// <param name="hash"></param>
//    /// <param name="name"></param>
//    /// <param name="nid"></param>
//    /// <param name="n"></param>
//    /// <param name="ns"></param>
//    /// <param name="platform"></param>
//    /// <param name="unitid"></param>
//    /// <param name="unitname"></param>
//    /// <param name="unitsign"></param>
//    public StoreMarket(int id, Enums.Identity.User u, EID.Group g, EID.Company c, EID.OEM o, 
//        int nid, string n, string ns, 
//        int unitid, string unitname, string unitsign) : this(u.GetID(), g.GetID(), c.GetID(), o.GetID())
//    {
//        ID = id;        
//        State = StateEnum.Open;
//        NationId = nid;
//        NationShort = ns;
//        NationName = n;
//        UnitID = unitid;
//        UnitName = unitname;
//        UnitSign = unitsign;        
//    }
//}
