using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;

namespace ERP.Models.DB.Stores;

//[Index(nameof(Pid), nameof(Sid), nameof(SourceId))]
//public class MwsTask : UserModel
//{
//    /// <summary>
//    /// 店铺id
//    /// </summary>

//    [Comment("店铺id")]
//    public int Sid { get; set; }

//    /// <summary>
//    /// [镜像]产品id
//    /// </summary>

//    [Comment("[镜像]产品id")]
//    public int Pid { get; set; }

//    /// <summary>
//    /// [原始]产品id
//    /// </summary>

//    [Comment("[原始]产品id")]
//    public int SourceId { get; set; }

//    /// <summary>
//    /// 产品SKU
//    /// </summary>

//    [Comment("产品SKU")]
//    public string? Sku { get; set; }

//    public enum SkuTypes
//    {
//        [Description("默认")]
//        DEFAULT = 1,

//        [Description("无前缀")]
//        SELF = 2
//    }

//    /// <summary>
//    /// 1 默认（有前缀可以关联产品） 2 无前缀(无法关联产品)
//    /// </summary>

//    [Comment("1 默认（有前缀可以关联产品） 2 无前缀(无法关联产品)")]
//    public SkuTypes SkuType { get; set; }

//    /// <summary>
//    /// 产品名称
//    /// </summary>

//    [Comment("产品名称")]
//    public string? ProductName { get; set; }

//    /// <summary>
//    /// 产品图片
//    /// </summary>

//    [Comment("产品图片")]
//    public string? ProductUrl { get; set; }

//    /// <summary>
//    /// 店铺名称
//    /// </summary>

//    [Comment("店铺名称")]
//    public string? StoreName { get; set; }

//    /// <summary>
//    /// 语言
//    /// </summary>

//    [Comment("语言")]
//    public string? ProductLanguage { get; set; }

//    /// <summary>
//    /// browser_node_id
//    /// </summary>

//    [Comment("browser_node_id")]
//    public int BrowserNodeId { get; set; }

//    /// <summary>
//    /// TypeData
//    /// </summary>

//    [Comment("TypeData")]
//    public string? TypeData { get; set; }

//    public enum States
//    {
//        [Description("待执行")]
//        WAITING = 1,

//        [Description("进行中")]
//        EXECUTING = 4,

//        [Description("已完成")]
//        SUCCESS = 2,

//        [Description("失败")]
//        ERROR = 3
//    }

//    /// <summary>
//    /// 默认STATE_WAITING=1等待执行 STATE_EXECUTING=4 执行中  STATE_SUCCESS = 2执行 STATE_ERROR=3 执行失败
//    /// </summary>

//    [Comment("默认STATE_WAITING=1等待执行 STATE_EXECUTING=4 执行中  STATE_SUCCESS = 2执行 STATE_ERROR=3 执行失败")]
//    public States State { get; set; }

//    /// <summary>
//    /// ASIN
//    /// </summary>

//    [Description("ASIN")]
//    public string? Asin { get; set; }

//    /// <summary>
//    /// 产品错误原因
//    /// </summary>

//    [Comment("产品错误原因")]
//    public string? ErrProduct { get; set; }

//    /// <summary>
//    /// 变型错误原因
//    /// </summary>

//    [Comment("变型错误原因")]
//    public string? ErrVariant { get; set; }

//    /// <summary>
//    /// 定价错误原因
//    /// </summary>

//    [Comment("定价错误原因")]
//    public string? ErrPricing { get; set; }

//    /// <summary>
//    /// 库存错误原因
//    /// </summary>

//    [Comment("库存错误原因")]
//    public string? ErrInventory { get; set; }

//    /// <summary>
//    /// 图像错误原因
//    /// </summary>

//    [Comment("图像错误原因'")]
//    public string? ErrImage { get; set; }

//    /// <summary>
//    /// 关系错误原因
//    /// </summary>

//    [Comment("关系错误原因")]
//    public string? ErrRelationship { get; set; }

//    /// <summary>
//    /// mws_tasks表id
//    /// </summary>

//    [Comment("mws_tasks表id")]
//    public int TasksId { get; set; }

//    /// <summary>
//    /// 备货时间（单位：天）
//    /// </summary>

//    [Comment("备货时间（单位：天）")]
//    public DateTime LeadTime { get; set; }
//}

//-- ----------------------------
//-- Table structure for erp3_mws_task
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_mws_task`;
//CREATE TABLE `erp3_mws_task`  (
//  `id` bigint unsigned NOT NULL,
//  `i_sid` int unsigned NOT NULL COMMENT '店铺id',
//  `i_pid` int unsigned NOT NULL COMMENT '[镜像]产品id',
//  `i_source_id` int unsigned NOT NULL COMMENT '[原始]产品id',
//  `i_user_id` int unsigned NOT NULL COMMENT '用户id',
//  `i_group_id` int unsigned NOT NULL COMMENT '当前用户组id',
//  `i_company_id` int unsigned NOT NULL COMMENT '店铺公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `d_sku` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '产品SKU',
//  `t_sku_type` int unsigned NOT NULL COMMENT '1 默认（有前缀可以关联产品） 2 无前缀(无法关联产品)',
//  `u_product_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '产品名称',
//  `u_product_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '产品图片',
//  `u_store_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '店铺名称',
//  `u_product_language` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '语言',
//  `d_browser_node_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'browser_node_id',
//  `d_type_data` json NULL COMMENT '{\"parent\":\"{‘cn_name’:\'\'中文名\'\',\'\'\'\'}\",\"son\":\"{‘cn_name’:\'\'中文名\'\',\'\'\'\'}\"}',
//  `t_state` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '默认STATE_WAITING=1等待执行 STATE_EXECUTING=4 执行中  STATE_SUCCESS = 2执行 STATE_ERROR=3 执行失败 ',
//  `d_asin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'ASIN',
//  `d_err_product` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '产品错误原因',
//  `d_err_variant` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '变型错误原因',
//  `d_err_pricing` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '定价错误原因',
//  `d_err_inventory` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '库存错误原因',
//  `d_err_image` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '图像错误原因',
//  `d_err_relationship` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '关系错误原因',
//  `i_tasks_id` int unsigned NULL COMMENT 'mws_tasks表id',
//  `d_lead_time` int unsigned NOT NULL COMMENT '备货时间（单位：天）',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `pid`(`i_pid`) USING BTREE,
// INDEX `sid`(`i_sid`) USING BTREE,
// INDEX `source_id`(`i_source_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 7769 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = 'Mws_商品上传亚马逊任务表' ROW_FORMAT = Dynamic;