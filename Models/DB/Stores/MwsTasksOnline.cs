using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;

namespace ERP.Models.DB.Stores;

//[Index(nameof(TasksId))]
//public class MwsTasksOnline : UserModel
//{
//    /// <summary>
//    /// Mws的Tasks表id
//    /// </summary>

//    [Comment("Mws的Tasks表id")]
//    public int TasksId { get; set; }
//}

//-- ----------------------------
//-- Table structure for erp3_mws_tasks_online
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_mws_tasks_online`;
//CREATE TABLE `erp3_mws_tasks_online`  (
//  `id` bigint unsigned NOT NULL,
//  `i_tasks_id` int unsigned NOT NULL COMMENT 'Mws的Tasks表id',
//  `i_user_id` int unsigned NOT NULL COMMENT '用户id',
//  `i_group_id` int unsigned NOT NULL COMMENT '当前用户组id',
//  `i_company_id` int unsigned NOT NULL COMMENT '公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `tasks_id`(`i_tasks_id`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 2812 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = 'Mws_商品上传亚马逊相同店铺打包任务在线操作用户记录表' ROW_FORMAT = Dynamic;