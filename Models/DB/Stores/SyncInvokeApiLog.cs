using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;

namespace ERP.Models.DB.Stores;

//[Index(nameof(UploadId))]
//public class SyncInvokeApiLog : UserModel
//{
//    / <summary>
//    / 任务集id
//    / </summary>

//    [Comment("任务集id")]
//    public int UploadId { get; set; }

//    / <summary>
//    / 任务拥有者id
//    / </summary>

//    [Comment("任务拥有者id")]
//    public int UploadOperateId { get; set; }

//    / <summary>
//    / 请求信息快照
//    / </summary>

//    [Comment("请求信息快照")]
//    public string? RequestData { get; set; }

//    public enum Apis
//    {
//        [Description("")]
//        GETSTORE = 1,

//        [Description("")]
//        GET = 2,

//        [Description("")]
//        ADDSESSION = 3,

//        [Description("")]
//        EDITSESSION = 4,

//        [Description("")]
//        RESULTERROR = 5,

//        [Description("")]
//        SUCCESS = 6,

//        [Description("")]
//        STATEFINISH = 7
//    }

//    / <summary>
//    / 1:API_GET_STORE 2:API_GET 3:API_ADD_SESSION 4:API_EDIT_SESSION 5:API_RESULT_ERROR 6:API_SUCCESS 7:API_STATE_FINISH
//    / </summary>

//    [Comment("1:API_GET_STORE 2:API_GET 3:API_ADD_SESSION 4:API_EDIT_SESSION 5:API_RESULT_ERROR 6:API_SUCCESS 7:API_STATE_FINISH")]
//    public Apis Api { get; set; }
//}

//-- ----------------------------
//-- Table structure for erp3_sync_invoke_api_log
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_sync_invoke_api_log`;
//CREATE TABLE `erp3_sync_invoke_api_log`  (
//  `id` bigint unsigned NOT NULL,
//  `i_user_id` bigint unsigned NOT NULL COMMENT '当前操作人',
//  `i_group_id` bigint unsigned NOT NULL COMMENT '当前用户组id',
//  `i_company_id` bigint unsigned NOT NULL COMMENT '公司id',
//  `i_oem_id` bigint unsigned NOT NULL COMMENT '当前Oem id',
//  `i_upload_id` bigint unsigned NOT NULL COMMENT '任务集id',
//  `i_upload_operate_id` bigint unsigned NULL COMMENT '任务拥有者id',
//  `d_request_data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '请求信息快照',
//  `t_api` int unsigned NOT NULL COMMENT '1:API_GET_STORE 2:API_GET 3:API_ADD_SESSION 4:API_EDIT_SESSION 5:API_RESULT_ERROR 6:API_SUCCESS 7:API_STATE_FINISH',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `upload_id`(`i_upload_id`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 8882365 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '上传任务Api调用请求快照日志' ROW_FORMAT = Dynamic;