using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;

namespace ERP.Models.DB.Stores;

//[Index(nameof(UploadId))]
//public class SyncUser : UserModel
//{
//    /// <summary>
//    /// 任务集id
//    /// </summary>

//    [Comment("任务集id")]
//    public int UploadId { get; set; }

//    /// <summary>
//    /// 任务拥有者id
//    /// </summary>

//    [Comment("任务拥有者id")]
//    public int OldUserId { get; set; }

//    /// <summary>
//    /// 当前操作人
//    /// </summary>

//    [Comment("当前操作人")]
//    public int NewUserId { get; set; }

//    /// <summary>
//    /// upload表上次更新时间
//    /// </summary>

//    [Comment("upload表上次更新时间")]
//    public int LastUpdated { get; set; }

//    /// <summary>
//    /// 
//    /// </summary>

//    //[Comment("")]
//    public bool IsChanged { get; set; }

//    /// <summary>
//    /// 
//    /// </summary>

//    //[Comment("")]
//    public bool IsIn5Min { get; set; }

//    public enum Types
//    {
//        [Description("创建任务")]
//        START = 1,

//        [Description("修改任务")]
//        EDIT = 2,

//        [Description("结束任务")]
//        END = 3
//    }

//    /// <summary>
//    /// 1:创建任务 2:修改任务 3:结束任务
//    /// </summary>

//    [Comment("1:创建任务 2:修改任务 3:结束任务")]
//    public Types Type { get; set; }
//}

//-- ----------------------------
//-- Table structure for erp3_sync_user
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_sync_user`;
//CREATE TABLE `erp3_sync_user`  (
//  `id` bigint unsigned NOT NULL,
//  `i_upload_id` bigint unsigned NOT NULL COMMENT '任务集id',
//  `i_old_user_id` bigint unsigned NULL COMMENT '任务拥有者id',
//  `i_new_user_id` bigint unsigned NOT NULL COMMENT '当前操作人',
//  `d_last_updated` datetime(0) NULL DEFAULT NULL COMMENT 'upload表上次更新时间',
//  `i_group_id` int unsigned NOT NULL COMMENT '当前用户组id',
//  `i_company_id` int unsigned NOT NULL COMMENT '公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `t_is_changed` int unsigned NULL COMMENT '1:是 2:否',
//  `t_is_in_5_min` int unsigned NULL COMMENT '1:是 2:否',
//  `t_type` int unsigned NOT NULL COMMENT '1:创建任务 2:修改任务 3:结束任务',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `upload_id`(`i_upload_id`) USING BTREE,
// INDEX `user_id`(`i_new_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 3285584 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '任务生命周期表（任务拿取用户记录）' ROW_FORMAT = Dynamic;