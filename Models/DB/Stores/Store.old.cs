﻿//-- ----------------------------
//-- Table structure for erp3_store
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_store`;
//CREATE TABLE `erp3_store`  (
//  `id` bigint unsigned NOT NULL,
//  `d_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '店铺名称',
//  `d_org` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '店铺域名',
//  `i_nation_id` int (0) NOT NULL DEFAULT 0 COMMENT '国家id',
//  `u_nation_short` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '国家简码',
//  `u_nation` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '国家名称',
//  `i_user_id` int (0) NOT NULL DEFAULT 0 COMMENT '创建用户id',
//  `i_group_id` int (0) NOT NULL DEFAULT 0 COMMENT '创建用户组id',
//  `t_state` tinyint(0) NOT NULL DEFAULT 1 COMMENT '状态1启用2禁用',
//  `t_is_verify` tinyint(0) NOT NULL DEFAULT 1 COMMENT '店铺验证1已认证2未认证',
//  `t_type` tinyint(0) NOT NULL DEFAULT 1 COMMENT '店铺平台1亚马逊2其他',
//  `d_seller_id` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'seller_id',
//  `d_data` json NOT NULL COMMENT 'json参数',
//  `d_sort` int (0) NOT NULL DEFAULT 0 COMMENT '排序',
//  `t_flag` tinyint(0) NOT NULL DEFAULT 1 COMMENT '逻辑删除，跟随国家禁用状态1有效2失效',
//  `d_unit` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '货币名称',
//  `d_unit_sign` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '货币名称',
//  `i_company_id` int (0) NOT NULL DEFAULT 0 COMMENT '公司id',
//  `i_continents_id` int (0) NOT NULL DEFAULT 0 COMMENT '所属大洲',
//  `d_hash_code` bigint(0) NOT NULL DEFAULT 0 COMMENT 'hashcode',
//  `i_oem_id` int (0) NOT NULL DEFAULT 0 COMMENT 'oem_id',
//  `i_key_id` int (0) NOT NULL DEFAULT 0 COMMENT 'key_id',
//  `d_last_order_time` timestamp(0) NULL DEFAULT NULL COMMENT '最后一条订单时间(上报即更新)',
//  `d_last_ship_time` timestamp(0) NULL DEFAULT NULL COMMENT '最后一个有效订单时间(上报最终状态即更新)',
//  `d_last_auto_ship_time` timestamp(0) NULL DEFAULT NULL COMMENT '自动拉取最后一个有效订单时间(自动拉取上报最终状态即更新)',
//  `d_last_allot_time` int (0) NOT NULL DEFAULT 0 COMMENT '最后被分配拉取订单时间(自动拉取获取店铺更新)',
//  `d_last_auto_update_time` int (0) NOT NULL DEFAULT 0 COMMENT '最后更新订单状态时间',
//  `d_last_auto_time` timestamp(0) NULL DEFAULT NULL COMMENT '最后自动拉取订单时间(finish更新)',
//  `t_is_del` tinyint(0) NULL DEFAULT 2 COMMENT '是否删除 1是 2否',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `continents_id`(`i_continents_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `hash_code`(`d_hash_code`) USING BTREE,
// INDEX `is_verify`(`t_is_verify`) USING BTREE,
// INDEX `key_id`(`i_key_id`) USING BTREE,
// INDEX `last_allot_time`(`d_last_allot_time`) USING BTREE,
// INDEX `last_auto_update_time`(`d_last_auto_update_time`) USING BTREE,
// INDEX `nation_id`(`i_nation_id`) USING BTREE,
//  INDEX `nation_short`(`u_nation_short`) USING BTREE,
//  INDEX `oem_id`(`i_oem_id`) USING BTREE,
//  INDEX `seller_id`(`d_seller_id`) USING BTREE,
//  INDEX `sort`(`d_sort`) USING BTREE,
//  INDEX `state`(`t_state`) USING BTREE,
//  INDEX `type`(`t_type`) USING BTREE,
//  INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 15787 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '店铺管理_店铺信息' ROW_FORMAT = Dynamic;