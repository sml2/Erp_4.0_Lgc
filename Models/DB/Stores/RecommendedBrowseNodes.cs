using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;

namespace ERP.Models.DB.Stores;

//[Index(nameof(Nation))]
//public class RecommendedBrowseNodes : UserModel
//{
//    /// <summary>
//    /// 
//    /// </summary>

//    //[Comment("")]
//    public int Nation { get; set; }

//    /// <summary>
//    /// 
//    /// </summary>

//    //[Comment("")]
//    public string NationShort { get; set; } = string.Empty;

//    /// <summary>
//    /// 
//    /// </summary>

//    //[Comment("")]
//    public int NodeId { get; set; }

//    /// <summary>
//    /// 
//    /// </summary>

//    //[Comment("")]
//    public string EnPath { get; set; } = string.Empty;

//    /// <summary>
//    /// 
//    /// </summary>

//    //[Comment("")]
//    public string CnPath { get; set; } = string.Empty;

//    /// <summary>
//    /// 
//    /// </summary>

//    //[Comment("")]
//    public string? FeedProductType { get; set; }
//}

//-- ----------------------------
//-- Table structure for erp3_recommended_browse_nodes
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_recommended_browse_nodes`;
//CREATE TABLE `erp3_recommended_browse_nodes`  (
//  `id` int (0) NOT NULL,
//  `i_nation` int (0) NULL DEFAULT NULL,
//  `u_nation_short` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
//  `i_parent` bigint(0) NULL DEFAULT NULL,
//  `i_node_id` bigint(0) NULL DEFAULT NULL,
//  `d_en_path` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
//  `d_cn_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
//  `d_feed_product_type` varchar(320) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `idx`(`i_nation`, `i_parent`) USING BTREE
//) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;