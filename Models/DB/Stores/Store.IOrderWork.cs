//using Microsoft.EntityFrameworkCore;

//using System.ComponentModel.DataAnnotations.Schema;
//using ERP.Services.Host;

//namespace ERP.Models.DB.Stores;
//public partial class StoreRegion : IOrderWork
//{
//    [Comment("最后一条订单时间")]
//    public DateTime LastOrderTime { get; set; } = DateTime.Now.AddDays(-3);
//    [Comment("拉取订单范围开始/结束时间")]
//    public DateTime QueryEndPayload { get; set; } = DateTime.Now.AddDays(-3);// DateTime.Now.AddMonths(-1);
//    [Comment("拉取订单范围结束时间")]
//    public DateTime QueryEnd { get; set; } = DateTime.Now.AddDays(-3);

//    [Comment("下次拉取参数")]
//    public string? NextCycleInfo { get; set; }
//    [Comment("最后自动拉取订单时间")]
//    public DateTime LastPullStart { get; set; } = DateTime.MinValue;
//    [Comment("订单拉取间隔秒数")]
//    public int IntervalSeconds { get; set; }
//    [Comment("下次拉取订单时间")]
//    public DateTime NextPull { get; set; } = DateTime.MinValue;
//    [Comment("下次评估时间")]
//    public DateTime NextEvaluate { get; set; } = DateTime.MinValue;
//    [Comment("当前拉取状态")]
//    public PullProgresses PullProgress { get; set; }
//    [Comment("完整周期数")]
//    public int FullCycle { get; set; }
//    [Comment("拉取次数")]
//    public int PullCount { get; set; }
//    [Comment("最后自动拉取耗时")]
//    public int LastDuration { get; set; }    
//}
