using Common.Enums.ResourcePackage;
using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.DB.Packages;

public class PackagesModel : BaseModel
{
    [Comment("资源包名称")]
    public string Name { get; set; }  
    
    [Comment("描述")]
    public string? Desc { get; set; }
    
    public PackageTypeEnum Type { get; set; }
}