using Common.Enums.ResourcePackage;
using ERP.Extensions;
using ERP.Models.Abstract;
using ERP.Models.DB.Users;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using NPOI.SS.Formula.Functions;

namespace ERP.Models.DB.Packages;

public class CompanyResourcePackagesModel : CompanyModel
{
    public CompanyResourcePackagesModel()
    {
    }
    
    public CompanyResourcePackagesModel(PackagesModel pack, PackageItemsModel item, ISession session)
    {
        
        Name = pack.Name + "_" + item.Num;
        if (item.Type == PackageTypeEnum.Translation)
        {
            Name += "字符";
        }
        else
        {
            Name += "次";
        }
        Type = pack.Type;
        TypeName = pack.Name;
        TypeName = pack.Type.GetDescription();
        RemainingQuantity = item.Num;
        TotalQuantity = item.Num;
        Snapshot = JsonConvert.SerializeObject(item);
        CompanyID = session.GetCompanyID();
        OEMID = session.GetOEMID();
        var nowDate = DateTime.Now.Date;
        Validity = nowDate.AddMonths(item.Validity).AddDays(1);
    }

   

    public CompanyResourcePackagesModel(PackagesModel pack, PackageItemsModel item, Company company)
    {
        Name = pack.Name + "_" + item.Num;
        if (item.Type == PackageTypeEnum.Translation)
        {
            Name += "字符";
        }
        else
        {
            Name += "次";
        }
        Type = pack.Type;
        TypeName = pack.Name;
        TypeName = pack.Type.GetDescription();
        RemainingQuantity = item.Num;
        TotalQuantity = item.Num;
        Snapshot = JsonConvert.SerializeObject(item);
        CompanyID = company.ID;
        OEMID = company.OEMID;
        Mold = PackageMoldEnum.Present;
        var nowDate = DateTime.Now.Date;
        Validity = nowDate.AddMonths(item.Validity).AddDays(1);
    }

    [Comment("资源包名称")]
    public string Name { get; set; }

    [Comment("资源包类型")]
    public PackageTypeEnum Type { get; set; }

    [Comment("资源包类型名称")]
    public string TypeName { get; set; }

    [Comment("剩余量")]
    public int RemainingQuantity { get; set; }

    [Comment("总量")]
    public int TotalQuantity { get; set; }

    [Comment("状态")]
    public PackageUseStateEnum State { get; set; } = PackageUseStateEnum.Effective;
    
    [Comment("有效期")]
    public DateTime?  Validity { get; set; }

    [Comment("快照")]
    public string Snapshot { get; set; }

    [Comment("类型")]
    public PackageMoldEnum Mold { get; set; } = PackageMoldEnum.Self;
}