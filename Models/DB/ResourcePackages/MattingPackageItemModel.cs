using Common.Enums.ResourcePackage;
using ERP.Models.Abstract;
using ERP.Models.View.Setting;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.DB.Packages;

public class MattingPackageItemModel : BaseModel
{
    public MattingPackageItemModel()
    {
        
    }
    
    public MattingPackageItemModel(EditMattingDto req)
    {
        var now = DateTime.Now;
        Times = req.Times;
        Price = req.Price;
        State = req.State;
        CreatedAt = now;
        UpdatedAt = now;
    }

    [Comment("次数")]
    public int Times { get; set; }

    [Comment("金额")]
    public decimal Price { get; set; }

    [Comment("状态")]
    public PackageSellStateEnum State { get; set; } = PackageSellStateEnum.PutOnTheShelf;
}
