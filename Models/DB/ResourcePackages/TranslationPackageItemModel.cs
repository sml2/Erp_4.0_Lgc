using Common.Enums.ResourcePackage;
using ERP.Models.Abstract;
using ERP.Models.View.Setting;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.DB.Packages;

public class TranslationPackageItemModel : BaseModel
{
    public TranslationPackageItemModel()
    {
    }

    public TranslationPackageItemModel(EditTranslationDto req)
    {
        var now = DateTime.Now;
        Num = req.Num;
        Price = req.Price;
        State = req.State;
        CreatedAt = now;
        UpdatedAt = now;
    }

    [Comment("字符数")]
    public int Num { get; set; }

    [Comment("金额")]
    public decimal Price { get; set; }

    [Comment("状态")]
    public PackageSellStateEnum State { get; set; } = PackageSellStateEnum.PutOnTheShelf;
}