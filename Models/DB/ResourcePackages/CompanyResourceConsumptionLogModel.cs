using System.ComponentModel.DataAnnotations.Schema;
using Common.Enums.ResourcePackage;
using ERP.Extensions;
using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.DB.Packages;

public class CompanyResourceConsumptionLogModel : CompanyModel
{
    public CompanyResourceConsumptionLogModel()
    {
        
    }
    public CompanyResourceConsumptionLogModel(ISession session)
    {
        CompanyID = session.GetCompanyID();
        OEMID = session.GetOEMID();
    }

    [Comment("资源类型")]
    public PackageTypeEnum Type { get; set; }

    [Comment("用量 字符数/次数")]
    public int Measure { get; set; }

    [Comment("资源包ID")]
    public int PackageId { get; set; }

    [ForeignKey(nameof(PackageId))]
    public CompanyResourcePackagesModel CompanyResourcePackagesModel { get; set; }

    public int UseUserId { get; set; }

    public string UseTruename { get; set; }
}