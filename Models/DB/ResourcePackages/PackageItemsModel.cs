using Common.Enums.ResourcePackage;
using ERP.Models.Abstract;
using ERP.Models.View.Setting;
using Microsoft.EntityFrameworkCore;
using NPOI.SS.Formula.Functions;

namespace ERP.Models.DB.Packages;

public class PackageItemsModel : BaseModel
{
    public PackageItemsModel()
    {
    }
    public PackageItemsModel(PackageItemDto req)
    {
        Type = req.Type;
        Num = req.Num;
        Price = req.Price;
        Validity = req.Validity;
        State = PackageSellStateEnum.PutOnTheShelf;
    }

    [Comment("资源包类型")]
    public PackageTypeEnum Type { get; set; }
    
    [Comment("数量")]
    public int Num { get; set; }

    [Comment("金额")]
    public decimal Price { get; set; }

    [Comment("有效期（月）")]
    public int Validity { get; set; }

    [Comment("状态")]
    public PackageSellStateEnum State { get; set; } = PackageSellStateEnum.PutOnTheShelf;
}