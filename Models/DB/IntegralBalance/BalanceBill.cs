﻿using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ERP.Models.IntegralBalance;

[Index(nameof(OperateCompanyId), nameof(OperateUserId), nameof(Type))]
public class BalanceBill : CompanyModel
{
    public enum Types
    {
        [Description("充值")] RECHARGE = 1,

        [Description("扣除")] EXPENSE = 2
    }

    public enum PriceConfig
    {
        // [Description("请求次数")] request = 3,
        // [Description("CPU消耗")] cpu,
        // [Description("内存消耗")] memory,
        // [Description("流量空间(GB)")] network,
        // [Description("翻译模块(次)")] translate,
        // [Description("抠图模块(次)")] picapi,
        // [Description("用户数量费用(个/天)")] user,
        // [Description("产品数量费用(个/天)")] product,
        // [Description("订单数量费用(个/天)")] order,
        // [Description("采购数量费用(个/天)")] purchase,
        // [Description("运单数量费用(个/天)")] waybill,
        // [Description("店铺数量费用(个/天)")] store,
//        disk,
    }

    /// <summary>
    /// 1充值2扣除
    /// </summary>

    [Comment("1充值2扣除")]
    public Types Type { get; set; }

    /// <summary>
    /// 变动余额(基准货币)
    /// </summary>

    [Comment("变动余额(基准货币)")]
    public decimal Balance { get; set; }

    /// <summary>
    /// 剩余余额(基准货币)
    /// </summary>

    [Comment("剩余余额(基准货币)")]
    public decimal Surplus { get; set; }

    /// <summary>
    /// 账单原因
    /// </summary>

    [Comment("账单原因")]
    public string? Reason { get; set; }

    /// <summary>
    /// 备注
    /// </summary>

    [Comment("备注")]
    public string? Remark { get; set; }

    /// <summary>
    /// 账单时间
    /// </summary>

    [Comment("账单时间")]
    public DateTime Datetime { get; set; }

    /// <summary>
    /// 触发用户名
    /// </summary>

    [Comment("触发用户名")]
    public string? UserName { get; set; }

    /// <summary>
    /// 触发用户姓名
    /// </summary>

    [Comment("触发用户姓名")]
    public string? TrueName { get; set; }

    /// <summary>
    /// 触发用户ID
    /// </summary>

    [Comment("触发用户ID")]
    public int OperateUserId { get; set; }

    /// <summary>
    /// 触发公司ID
    /// </summary>

    [Comment("触发公司ID")]
    public int OperateCompanyId { get; set; }

    /// <summary>
    /// 被充值用户名
    /// </summary>

    [Comment("被充值用户名")]
    public string? ByuserName { get; set; }

    /// <summary>
    /// 被充值用户姓名
    /// </summary>

    [Comment("被充值用户姓名")]
    public string? BytrueName { get; set; }

    public string? Ext { get; set; }
    public int ByUserId { get; set; }
    public int ByCompanyId { get; set; }
}

//-- ----------------------------
//-- Table structure for erp3_balance_bill
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_balance_bill`;
//CREATE TABLE `erp3_balance_bill`  (
//  `id` bigint unsigned NOT NULL,
//  `i_company_id` int unsigned NOT NULL COMMENT '公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `t_type` tinyint unsigned NOT NULL COMMENT '1充值2扣除',
//  `d_balance` decimal (22, 6) UNSIGNED NOT NULL DEFAULT 0.000000 COMMENT '变动余额(基准货币)',
//  `u_surplus` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '剩余余额(基准货币)',
//  `d_reason` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '账单原因',
//  `d_remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '备注',
//  `d_datetime` date NULL DEFAULT NULL COMMENT '账单时间',
//  `u_username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '触发用户名',
//  `u_truename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '触发用户姓名',
//  `i_operate_user_id` int unsigned NOT NULL COMMENT '触发用户id',
//  `i_operate_company_id` int unsigned NOT NULL COMMENT '触发公司id',
//  `u_byusername` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '被充值用户名',
//  `u_bytruename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '被充值用户姓名',
//  `i_by_user_id` int unsigned NOT NULL COMMENT '被充值用户id',
//  `i_by_company_id` int unsigned NOT NULL COMMENT '被充值公司id',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  `d_ext` json NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `by_company_id`(`i_by_company_id`) USING BTREE,
// INDEX `by_user_id`(`i_by_user_id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `operate_company_id`(`i_operate_company_id`) USING BTREE,
// INDEX `operate_user_id`(`i_operate_user_id`) USING BTREE,
// INDEX `type`(`t_type`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 1061 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统管理_余额账单' ROW_FORMAT = Dynamic;