﻿using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;

namespace ERP.Models.IntegralBalance;

public class CompanyReduceRequest : UserModel
{
    /// <summary>
    /// 请求资源(请求次数)
    /// </summary>

    [Comment("请求资源(请求次数)")]
    public int Request { get; set; }

    /// <summary>
    /// cpu(秒)
    /// </summary>

    [Comment("cpu秒")]
    public string? Cpu { get; set; }

    /// <summary>
    /// 内存(字节B)
    /// </summary>

    [Comment("内存(字节B)")]
    public string? Memory { get; set; }

    /// <summary>
    /// 带宽(字节B)
    /// </summary>

    [Comment("带宽字节B")]
    public string? Network { get; set; }

    /// <summary>
    /// 硬盘(字节B)
    /// </summary>

    [Comment("硬盘字节B")]
    public string? Disk { get; set; }

    /// <summary>
    /// 图片抠图(请求次数)
    /// </summary>

    [Comment("图片抠图(请求次数)")]
    public string? PicApi { get; set; }

    /// <summary>
    /// 翻译(请求次数)
    /// </summary>

    [Comment("翻译(请求次数)")]
    public string? TranslateApi { get; set; }
}

//-- ----------------------------
//-- Table structure for erp3_company_reduce_request
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_company_reduce_request`;
//CREATE TABLE `erp3_company_reduce_request`  (
//  `id` bigint unsigned NOT NULL,
//  `i_company_id` bigint unsigned NOT NULL COMMENT '公司id',
//  `i_oem_id` bigint unsigned NOT NULL COMMENT 'OemId',
//  `c_request` bigint unsigned NOT NULL COMMENT '请求资源(请求次数)',
//  `c_cpu` bigint unsigned NOT NULL COMMENT 'cpu(秒)',
//  `c_memory` bigint unsigned NOT NULL COMMENT '内存(字节B)',
//  `c_network` bigint unsigned NOT NULL COMMENT '带宽(字节B)',
//  `c_disk` bigint(0) NOT NULL DEFAULT 0 COMMENT '硬盘(字节B)',
//  `c_pic_api` bigint(0) NOT NULL DEFAULT 0 COMMENT '图片抠图(请求次数)',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  `c_translate_api` bigint unsigned NULL COMMENT '翻译(请求次数)',
//  PRIMARY KEY(`id`) USING BTREE,
// UNIQUE INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 590 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '数据统计_公司已处理资源' ROW_FORMAT = Dynamic;
