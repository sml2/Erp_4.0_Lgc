﻿using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;

namespace ERP.Models.IntegralBalance;

[Index(nameof(OperateCompanyId), nameof(OperateUserId), nameof(Type))]
public class IntegralBill : UserModel
{
    public enum Types
    {
        [Description("充值")]
        RECHARGE = 1,

        [Description("扣除")]
        EXPENSE = 2
    }

    /// <summary>
    /// 1充值2扣除
    /// </summary>

    [Comment("1充值2扣除")]
    public Types Type { get; set; }

    /// <summary>
    /// 消耗积分
    /// </summary>

    [Comment("消耗积分")]
    public int Integral { get; set; }

    /// <summary>
    /// 剩余积分
    /// </summary>

    [Comment("剩余积分")]
    public decimal surplus { get; set; }

    /// <summary>
    /// 账单原因
    /// </summary>

    [Comment("账单原因")]
    public string? Reason { get; set; }

    /// <summary>
    /// 备注
    /// </summary>

    [Comment("备注")]
    public string? Remark { get; set; }

    /// <summary>
    /// 账单时间
    /// </summary>

    [Comment("账单时间")]
    public DateTime Datetime { get; set; }

    /// <summary>
    /// 触发用户名
    /// </summary>

    [Comment("触发用户名")]
    public string? UserName { get; set; }

    /// <summary>
    /// 触发用户姓名
    /// </summary>

    [Comment("触发用户姓名")]
    public string? TrueName { get; set; }

    /// <summary>
    /// 触发用户ID
    /// </summary>

    [Comment("触发用户ID")]
    public int OperateUserId { get; set; }

    /// <summary>
    /// 触发公司ID
    /// </summary>

    [Comment("触发公司ID")]
    public int OperateCompanyId { get; set; }
}

//-- ----------------------------
//-- Table structure for erp3_integral_bill
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_integral_bill`;
//CREATE TABLE `erp3_integral_bill`  (
//  `id` bigint unsigned NOT NULL,
//  `i_company_id` int unsigned NOT NULL COMMENT '公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `t_type` tinyint unsigned NOT NULL COMMENT '1充值2扣除',
//  `d_integral` bigint unsigned NOT NULL COMMENT '消耗积分',
//  `u_surplus` bigint unsigned NOT NULL COMMENT '剩余积分',
//  `d_reason` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '账单原因',
//  `d_remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '备注',
//  `d_datetime` date NULL DEFAULT NULL COMMENT '账单时间',
//  `u_username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '触发用户名',
//  `u_truename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '触发用户姓名',
//  `i_operate_user_id` int unsigned NOT NULL COMMENT '触发用户id',
//  `i_operate_company_id` int unsigned NOT NULL COMMENT '触发公司id',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  `d_ext` json NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `operate_company_id`(`i_operate_company_id`) USING BTREE,
// INDEX `operate_user_id`(`i_operate_user_id`) USING BTREE,
// INDEX `type`(`t_type`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 2410 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统管理_积分账单' ROW_FORMAT = Dynamic;