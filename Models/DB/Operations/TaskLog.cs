﻿using ERP.Models.Abstract;
using System.ComponentModel;

namespace ERP.Models.DB.Operations;

using ERP.Enums.Operations;
using ERP.Models.DB.Identity;
using Extensions;

public class TaskLog : BaseModel /* : CompanyModel*/
{
    public TaskLog() { }
    public TaskLog(TypeModuels Type, Results result, DateTime start, DateTime endtime,  string ResDetail/*,int CId,int OId*/)
    {
        TypeModuel = Type;
        results = result;
        EndTime = endtime;
        Sort = -DateTime.Now.GetTimeStamp();
        Data = ResDetail ?? result.GetDescription();
        Flag = true;
        //CompanyID = CId;
        //OEMID = OId;
    }
    //public TaskLog(TypeModuels Type, Results result, DateTime endtime, string ResDetail, User user)
    //    : this(Type, result, DateTime.Now, endtime, ResDetail, user.CompanyID, user.OEMID)
    //{
    //}
    public TypeModuels TypeModuel { get; set; }
    public DateTime BeginTime { get; set; }
    public DateTime EndTime { get; set; }
    public Results results { get; set; }
    public int Sort { get; set; }
    public string Data { get; set; } = string.Empty;
    public bool Flag { get; set; }


}
