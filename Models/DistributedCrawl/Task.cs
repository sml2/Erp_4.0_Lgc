﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.Models;

using Models.Abstract;
using Extensions;

[Comment("任务")]
public class Task : UserModel
{
    //1超管2管理员3公司管理员4员工(无用户管理)
    [Comment("名称"), Required]
    public string Name { get; set; } = string.Empty;

    [Comment("权限组ID"), Required]
    public int RuleId { get; set; } = 0;
}