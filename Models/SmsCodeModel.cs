﻿using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;
using ERP.Extensions;

namespace ERP.Models;

[Index(nameof(Type))]
[Index(nameof(IP))]
[Index(nameof(Mobile),nameof(Used))]
public class SmsCodeModel : OEMModel
{
    public SmsCodeModel()
    {

    }
    public SmsCodeModel(int oemId)
    {
        OEMID = oemId;
    }

    /// <summary>
    /// 手机号码
    /// </summary>

    [Comment("手机号码")]
    public string Mobile { get; set; }

    /// <summary>
    /// 验证码
    /// </summary>

    [Comment("验证码")]
    public string? Code { get; set; }

    /// <summary>
    /// 验证码发送时间戳
    /// </summary>

    [Comment("验证码发送时间戳")]
    public DateTime SendTime { get; set; }
    
    [Comment("使用类型")]
    public SmsCodeTypeEnum Type { get; set; }
    
    [Comment("是否使用")]
    public bool Used { get; set; }
    
    public string? IP { get; set; }
}

public enum SmsCodeTypeEnum
{
    Register = 1,
    ForgotPassword = 2,
    BindOrChange = 3,
}

//-- ----------------------------
//-- Table structure for erp3_sms_code
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_sms_code`;
//CREATE TABLE `erp3_sms_code`  (
//  `id` bigint unsigned NOT NULL,
//  `d_mobile` bigint unsigned NOT NULL COMMENT '手机号码',
//  `d_code` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '验证码',
//  `d_send_time` int unsigned NOT NULL COMMENT '验证码发送时间戳',
//  `t_type` tinyint unsigned NOT NULL COMMENT '使用类型1注册2忘记密码3绑定、换绑',
//  `t_is_use` tinyint unsigned NOT NULL COMMENT '使用状态 1未使用2已使用',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `is_use`(`t_is_use`) USING BTREE,
// INDEX `mobile`(`d_mobile`) USING BTREE,
// INDEX `type`(`t_type`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 2848 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统管理_手机验证码' ROW_FORMAT = Dynamic;