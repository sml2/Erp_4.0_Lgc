﻿using Microsoft.EntityFrameworkCore;

namespace ERP.Models.Reporter
{
    /// <summary>
    /// 统计管理_请求耗时统计_归档备份
    /// </summary>
    [Comment("统计管理_请求耗时统计_归档备份")]
    public class ReporterRequestBakup: ReporterRequest
    {
    }
//    -- ----------------------------
//-- Table structure for erp3_reporter_request_bak
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_reporter_request_bak`;
//CREATE TABLE `erp3_reporter_request_bak`  (
//  `i_user_id` int(0) NOT NULL DEFAULT 0 COMMENT '用户id',
//  `i_group_id` int(0) NOT NULL DEFAULT 0 COMMENT '组id',
//  `i_company_id` int(0) NOT NULL DEFAULT 0 COMMENT '公司id',
//  `i_oem_id` int(0) NOT NULL DEFAULT 0 COMMENT 'oem_id',
//  `d_request_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '请求id',
//  `d_module` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '模块名称',
//  `d_uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '链接',
//  `d_namespace` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '命名空间',
//  `d_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '名称',
//  `d_action` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作名称',
//  `d_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '请求方式',
//  `d_net_time` int(0) NOT NULL DEFAULT 0 COMMENT '减掉cpu时间的请求时间(毫秒)',
//  `d_request_time` int(0) NOT NULL DEFAULT 0 COMMENT 'js请求耗费时间',
//  `d_cpu_time` int(0) NOT NULL DEFAULT 0 COMMENT 'cpu耗费时间',
//  `created_at` datetime(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL
//) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '统计管理_请求耗时统计' ROW_FORMAT = Dynamic PARTITION BY RANGE (to_days(`created_at`))
//PARTITIONS 15
//(PARTITION `p0` VALUES LESS THAN (737972) ENGINE = InnoDB MAX_ROWS = 0 MIN_ROWS = 0 ,
//PARTITION `p1` VALUES LESS THAN (738003) ENGINE = InnoDB MAX_ROWS = 0 MIN_ROWS = 0 ,
//PARTITION `p2` VALUES LESS THAN (738034) ENGINE = InnoDB MAX_ROWS = 0 MIN_ROWS = 0 ,
//PARTITION `p3` VALUES LESS THAN (738064) ENGINE = InnoDB MAX_ROWS = 0 MIN_ROWS = 0 ,
//PARTITION `p4` VALUES LESS THAN (738095) ENGINE = InnoDB MAX_ROWS = 0 MIN_ROWS = 0 ,
//PARTITION `p5` VALUES LESS THAN (738125) ENGINE = InnoDB MAX_ROWS = 0 MIN_ROWS = 0 ,
//PARTITION `p6` VALUES LESS THAN (738156) ENGINE = InnoDB MAX_ROWS = 0 MIN_ROWS = 0 ,
//PARTITION `p7` VALUES LESS THAN (738187) ENGINE = InnoDB MAX_ROWS = 0 MIN_ROWS = 0 ,
//PARTITION `p8` VALUES LESS THAN (738215) ENGINE = InnoDB MAX_ROWS = 0 MIN_ROWS = 0 ,
//PARTITION `p9` VALUES LESS THAN (738246) ENGINE = InnoDB MAX_ROWS = 0 MIN_ROWS = 0 ,
//PARTITION `p10` VALUES LESS THAN (738276) ENGINE = InnoDB MAX_ROWS = 0 MIN_ROWS = 0 ,
//PARTITION `p11` VALUES LESS THAN (738307) ENGINE = InnoDB MAX_ROWS = 0 MIN_ROWS = 0 ,
//PARTITION `p12` VALUES LESS THAN (738337) ENGINE = InnoDB MAX_ROWS = 0 MIN_ROWS = 0 ,
//PARTITION `p13` VALUES LESS THAN (738368) ENGINE = InnoDB MAX_ROWS = 0 MIN_ROWS = 0 ,
//PARTITION `p14` VALUES LESS THAN (738399) ENGINE = InnoDB MAX_ROWS = 0 MIN_ROWS = 0 )
//;
}
