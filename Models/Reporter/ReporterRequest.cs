﻿using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;

namespace ERP.Models.Reporter;

public class ReporterRequest
{
    /// <summary>
    /// 请求id
    /// </summary>

    [Comment("请求id")]
    public int RequestId { get; set; }

    /// <summary>
    /// 模块名称
    /// </summary>

    [Comment("模块名称")]
    public string? Module { get; set; }

    /// <summary>
    /// 链接
    /// </summary>

    [Comment("链接")]
    public string? Url { get; set; }

    /// <summary>
    /// 命名空间
    /// </summary>

    [Comment("命名空间")]
    public string? Namespace { get; set; }

    /// <summary>
    /// 名称
    /// </summary>

    [Comment("名称")]
    public string? Name { get; set; }

    /// <summary>
    /// 操作名称
    /// </summary>

    [Comment("操作名称")]
    public string? Action { get; set; }

    /// <summary>
    /// 请求方式
    /// </summary>

    [Comment("请求方式")]
    public string? Method { get; set; }

    /// <summary>
    /// 减掉cpu时间的请求时间(毫秒)
    /// </summary>

    [Comment("减掉cpu时间的请求时间(毫秒)")]
    public int NetTime { get; set; }

    /// <summary>
    /// js请求耗费时间
    /// </summary>

    [Comment("js请求耗费时间")]
    public int RequestTime { get; set; }

    /// <summary>
    /// cpu耗费时间
    /// </summary>

    [Comment("cpu耗费时间")]
    public int CpuTime { get; set; }
}

//-- ----------------------------
//-- Table structure for erp3_reporter_request
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_reporter_request`;
//CREATE TABLE `erp3_reporter_request`  (
//  `id` bigint unsigned NOT NULL,
//  `i_user_id` int unsigned NOT NULL COMMENT '用户id',
//  `i_group_id` int unsigned NOT NULL COMMENT '组id',
//  `i_company_id` int unsigned NOT NULL COMMENT '公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT 'oem_id',
//  `d_request_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '请求id',
//  `d_module` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '模块名称',
//  `d_uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '链接',
//  `d_namespace` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '命名空间',
//  `d_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '名称',
//  `d_action` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作名称',
//  `d_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '请求方式',
//  `d_net_time` int unsigned NOT NULL COMMENT '减掉cpu时间的请求时间(毫秒)',
//  `d_request_time` int unsigned NOT NULL COMMENT 'js请求耗费时间',
//  `d_cpu_time` int unsigned NOT NULL COMMENT 'cpu耗费时间',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 74302022 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '统计管理_请求耗时统计' ROW_FORMAT = Dynamic;