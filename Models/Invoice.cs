﻿namespace ERP.Models
{
    public class Invoice
    {
        public Invoice(bool state, string errorMessage)
        {
            State = state;
            ErrorMessage = errorMessage;
            this.Time = DateTime.Now;
        }

        public bool State { get; set; }

        public string ErrorMessage { get; set; }

        public DateTime Time { get; set; }
    }
}
