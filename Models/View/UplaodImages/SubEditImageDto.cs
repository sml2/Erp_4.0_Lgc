namespace ERP.ViewModels.UplaodImages;

public class SubEditImageDto
{
    [NJP("value")]
    public string Value { get; set; }

    [NJP("oldUrl")]
    public string OldUrl { get; set; }
}