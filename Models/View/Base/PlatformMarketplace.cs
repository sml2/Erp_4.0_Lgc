﻿using Newtonsoft.Json;
namespace ERP.Models.View.Base
{
    public class PlatformMarketplace
    {
        public PlatformMarketplace(string nationName, string marketplace)
        {
            NationName = nationName;
            Marketplace = marketplace;
        }

        [JsonProperty("label")]
        public string NationName { get; set; }

        [JsonProperty("value")]
        public string Marketplace { get; set; }
    }
}
