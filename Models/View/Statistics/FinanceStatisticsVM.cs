using System.ComponentModel.DataAnnotations;
using ERP.Attributes;
using ERP.Services.Jobs;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using NPOI.SS.Formula.Functions;
using Ryu.Money.Json;
using Sy.Net;
using Statistic = ERP.Models.DB.Statistics.Statistic;

namespace ERP.ViewModels.Statistics;

public class FinanceStatisticsVM
{
    public FinanceStatisticsVM(Statistic m, string unit)
    {
        AboutId = m.AboutID;
        OrderTotal = m.OrderTotal;
        OrderTotalFee = m.OrderTotalFee;
        OrderTotalRefund = m.OrderTotalRefund;
        OrderTotalLoss = m.OrderTotalLoss;
        PurchaseTotal = m.PurchaseTotal;
        PurchaseTotalRefund = m.PurchaseTotalRefund;
        WaybillTotal = m.WaybillTotal;
        WaybillTotalRefund = m.WaybillTotalRefund;
        SelfRechargeTotal = m.SelfRechargeTotal;
        SelfExpenseTotal = m.SelfExpenseTotal;
    }


    public FinanceStatisticsVM(IGrouping<int, Statistic> g, string unit)
    {
        AboutId = g.Key;
        OrderTotal = g.Sum(m => m.OrderTotal);
        OrderTotalFee = g.Sum(m => m.OrderTotalFee);
        OrderTotalRefund = g.Sum(m => m.OrderTotalRefund);
        OrderTotalLoss = g.Sum(m => m.OrderTotalLoss);
        PurchaseTotal = g.Sum(m => m.PurchaseTotal);
        PurchaseTotalRefund = g.Sum(m => m.PurchaseTotalRefund);
        WaybillTotal = g.Sum(m => m.WaybillTotal);
        WaybillTotalRefund = g.Sum(m => m.WaybillTotalRefund);
        SelfRechargeTotal = g.Sum(m => m.SelfRechargeTotal);
        SelfExpenseTotal = g.Sum(m => m.SelfExpenseTotal);
    }

    public FinanceStatisticsVM()
    {
    }

    public FinanceStatisticsVM(int aboutId, Money orderTotal, Money orderTotalFee, Money orderTotalRefund,
        Money orderTotalLoss, Money purchaseTotal, Money purchaseTotalRefund, Money waybillTotal,
        Money waybillTotalRefund, Money selfRechargeTotal, Money selfExpenseTotal) : this()
    {
        AboutId = aboutId;
        OrderTotal = orderTotal;
        OrderTotalFee = orderTotalFee;
        OrderTotalRefund = orderTotalRefund;
        OrderTotalLoss = orderTotalLoss;
        PurchaseTotal = purchaseTotal;
        PurchaseTotalRefund = purchaseTotalRefund;
        WaybillTotal = waybillTotal;
        WaybillTotalRefund = waybillTotalRefund;
        SelfRechargeTotal = selfRechargeTotal;
        SelfExpenseTotal = selfExpenseTotal;
    }

    public int AboutId { get; set; }

    [Comment("订单总价汇总(基准货币 完结 所属公司 汇总)"), Display(Name = "订单总价"), JsonProperty("order_total"),
     JsonMoneyForDefaultUnit(false)]
    public Money OrderTotal { get; set; } = 0;

    [Comment("订单手续费汇总(基准货币 完结 所属公司 汇总)"), Display(Name = "订单手续费"), JsonProperty("order_total_fee"),
     JsonMoneyForDefaultUnit(false)]
    public Money OrderTotalFee { get; set; } = 0;

    [Comment("订单退款汇总(基准货币 完结 所属公司 汇总)"), Display(Name = "订单退款"), JsonProperty("order_total_refund"),
     JsonMoneyForDefaultUnit(false)]
    public Money OrderTotalRefund { get; set; } = 0;

    [Comment("订单损耗CNY(基准货币 完结 所属公司 汇总)"), Display(Name = "成本损失"), JsonProperty("order_total_loss"),
     JsonMoneyForDefaultUnit(false)]
    public Money OrderTotalLoss { get; set; } = 0;

    [Comment("采购总价汇总(基准货币 完结 所属公司 汇总)"), Display(Name = "采购总价"), JsonProperty("purchase_total"),
     JsonMoneyForDefaultUnit(false)]
    public Money PurchaseTotal { get; set; } = 0;

    [Comment("采购退款汇总(基准货币 完结 所属公司 汇总)"), Display(Name = "采购退款"), JsonProperty("purchase_total_refund"),
     JsonMoneyForDefaultUnit(false)]
    public Money PurchaseTotalRefund { get; set; } = 0;

    [Comment("运费总价汇总(基准货币 完结 所属公司 汇总)"), Display(Name = "物流总价"), JsonProperty("waybill_total"),
     JsonMoneyForDefaultUnit(false)]
    public Money WaybillTotal { get; set; } = 0;

    [Comment("运费退款汇总(基准货币 完结 所属公司 汇总)"), Display(Name = "物流退款"), JsonProperty("waybill_total_refund"),
     JsonMoneyForDefaultUnit(false)]
    public Money WaybillTotalRefund { get; set; } = 0;

    [Comment("自用钱包充值汇总(基准货币 完结 所属公司 汇总)"), Display(Name = "钱包总充值"), JsonProperty("self_recharge_total"),
     JsonMoneyForDefaultUnit(false)]
    public Money SelfRechargeTotal { get; set; }= 0;


    [Comment("自用钱包支出汇总(基准货币 完结 所属公司 汇总)"), Display(Name = "钱包总支出"), JsonProperty("self_expense_total"),
     JsonMoneyForDefaultUnit(false)]
    public Money SelfExpenseTotal { get; set; }= 0;


    [Display(Name = "利润"), JsonProperty("profit"), JsonMoneyForDefaultUnit(false)]
    public Money Profit => (OrderTotal - OrderTotalFee - OrderTotalLoss - PurchaseTotal - WaybillTotal);
    
    [JsonMoneyForDefaultUnit(false)]
    public Money Balance { get; set; }

}