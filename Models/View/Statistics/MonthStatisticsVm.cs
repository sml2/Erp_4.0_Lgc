using System.ComponentModel.DataAnnotations;
using ERP.Models.DB.Statistics;

namespace ERP.ViewModels.Statistics;

public class MonthStatisticsVm : FinanceStatisticsVM
{
    public MonthStatisticsVm(Statistic m, string unit) : base(m, unit)
    {
        Month = m.CreatedAt.ToString("yyyy-MM");
    }

    [Display(Name = "月份"), DisplayFormat(DataFormatString = "{0:yyyy-MM}")]
    public string? Month { get; set; }
}