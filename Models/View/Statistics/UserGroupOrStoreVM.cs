using ERP.Models.Abstract;

namespace ERP.ViewModels.Statistics;

public class UserGroupOrStoreVM
{
    public int ID { get; set; }

    public string? Name { get; set; }

    public BaseModel.StateEnum State { get; set; }
}