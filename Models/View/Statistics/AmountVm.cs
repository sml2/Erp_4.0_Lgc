namespace ERP.ViewModels.Statistics;

public class AmountVm
{
    public AmountByOrderVm? Order { get; set; }
    public AmountByPurchaseVm? Purchase { get; set; }
    public AmountByWaybillVm? Waybill { get; set; }

    public void SetOrder(AmountByOrderVm vm)
    {
        Order!.OrderTotal = vm.OrderTotal;
        Order.OrderTotalRefund = vm.OrderTotalRefund;
        Order.OrderTotalFee = vm.OrderTotalFee;
        Order.OrderTotalLoss = vm.OrderTotalLoss;
    }

    public void SetPurchase(AmountByPurchaseVm vm)
    {
        Purchase!.PurchaseTotal = vm.PurchaseTotal;
        Purchase.PurchaseTotalRefund = vm.PurchaseTotalRefund;
    }

    public void SetWaybill(AmountByWaybillVm vm)
    {
        Waybill!.WaybillTotal = vm.WaybillTotal;
        Waybill.WaybillTotalRefund = vm.WaybillTotalRefund;
    }
}