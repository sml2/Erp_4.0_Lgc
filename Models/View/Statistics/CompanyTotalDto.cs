namespace ERP.ViewModels.Statistics;

public class CompanyTotalDto : FinanceStatisticsDto
{
    public List<DateTime>? Date { get; set; }
}