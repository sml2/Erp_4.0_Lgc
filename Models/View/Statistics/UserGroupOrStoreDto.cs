namespace ERP.ViewModels.Statistics;

public class UserGroupOrStoreDto : FinanceStatisticsDto
{
    public List<DateTime>? Date { get; set; }
}