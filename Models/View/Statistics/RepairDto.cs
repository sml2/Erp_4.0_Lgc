using ERP.Services.Statistics;

namespace ERP.ViewModels.Statistics;

public class RepairDto
{
    public int Id { get; set; }

    public SourceEnum DataSource { get; set; }

    public DataTypeEnum DataType { get; set; }
}