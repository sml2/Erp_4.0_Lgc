using ERP.Enums.Identity;
using ERP.Extensions;
using ERP.Models.DB.Logistics;
using ERP.Models.DB.Stores;
using ERP.Models.DB.Users;
using ERP.Models.Finance;
using ERP.Models.Product;
using ERP.Models.Stores;

namespace ERP.ViewModels.Statistics;

public class DataBelongDto
{
    public DataBelongDto(Company info)
    {
        CompanyId = info.ID;
        OemId = info.OEMID;
    }

    public DataBelongDto(int userId, int? groupId, int companyId, int oemId, int storeId)
    {
        UserId = userId;
        if (groupId != 1)
        {
            GroupId = groupId;
        }
        CompanyId = companyId;
        OemId = oemId;
        StoreId = storeId;
    }

    public DataBelongDto(Models.DB.Orders.Order m)
    {
        UserId = m.UserID;
        if (m.GroupID != 1)
        {
            GroupId = m.GroupID;
        }
        CompanyId = m.CompanyID;
        OemId = m.OEMID;
        StoreId = m.StoreId;
    }

    public DataBelongDto(StoreRegion m)
    {
        UserId = m.UserID;
        if (m.GroupID != 1 && m.GroupID != 0)
        {
            GroupId = m.GroupID;
        }
        CompanyId = m.CompanyID;
        OemId = m.OEMID;
    }

    public DataBelongDto(Waybill m)
    {
        UserId = m.UserID;
        if (m.GroupID != 1)
        {
            GroupId = m.GroupID;
        }
        CompanyId = m.CompanyID;
        OemId = m.OEMID;
        StoreId = m.StoreId;
    }

    public DataBelongDto(Models.DB.Purchase.Purchase m)
    {
        UserId = m.UserID;
        if (m.GroupID != 1)
        {
            GroupId = m.GroupID;
        }
        CompanyId = m.CompanyID;
        OemId = m.OEMID;
        StoreId = m.StoreId;
    }

    public DataBelongDto(Models.DB.Identity.User m)
    {
        CompanyId = m.CompanyID;
        OemId = m.OEMID;
        UserId = m.Pid;
    }
    
    public DataBelongDto(Models.DB.Identity.User m,bool isNew)
    {
        CompanyId = m.CompanyID;
        OemId = m.OEMID;
        UserId = m.Id;
    }

    public DataBelongDto(ProductModel m)
    {
        UserId = m.UserID;
        if (m.GroupID != 1)
        {
            GroupId = m.GroupID;
        }
        CompanyId = m.CompanyID;
        OemId = m.OEMID;
    }

    public DataBelongDto(BillLog m)
    {
        CompanyId = m.CompanyID;
        OemId = m.OEMID;
        UserId = m.UserID;
        if (m.GroupID != 1)
        {
            GroupId = m.GroupID;
        }
    }

    public int? UserId { get; set; }
    public int? GroupId { get; set; }
    public int? CompanyId { get; set; }
    public int OemId { get; set; }
    public int? StoreId { get; set; }
    public List<int>? ReportIds { get; set; }
}