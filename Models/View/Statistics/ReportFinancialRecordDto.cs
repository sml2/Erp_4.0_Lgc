using ERP.Enums.Finance;

namespace ERP.ViewModels.Statistics;

public class ReportFinancialRecordDto
{
    public MoneyRecord Money { get; set; }
    public TableFieldEnum Field { get; set; }
    public int? IndexId { get; set; }
    public int? SourceId { get; set; }
    public string? Reason { get; set; }
    public string? Remark { get; set; }
}