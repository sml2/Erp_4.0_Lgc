using ERP.Models.Abstract;
using ERP.Models.View.Product;

namespace ERP.ViewModels.Statistics;

public class FinanceStatisticsDto : PageDto
{

    public string? Name { get; set; }

    public BaseModel.StateEnum? State { get; set; }
}

public enum ActionEnum
{
    Total = 1,
    Month,
    UserGroup,
    Store
}