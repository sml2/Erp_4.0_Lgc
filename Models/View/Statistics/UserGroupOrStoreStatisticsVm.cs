using ERP.Models.Abstract;
using ERP.Models.DB.Statistics;

namespace ERP.ViewModels.Statistics;

public class UserGroupOrStoreStatisticsVm : FinanceStatisticsVM
{
    public UserGroupOrStoreStatisticsVm(Statistic m, string unit) : base(m, unit)
    {
    }
    
    public UserGroupOrStoreStatisticsVm(IGrouping<int,Statistic> m, string unit) : base(m, unit)
    {
        
    }

    public UserGroupOrStoreVM? Ext { get; set; }
    
    public bool IsDefault { get; set; } = true;
}