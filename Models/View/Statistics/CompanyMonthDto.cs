namespace ERP.ViewModels.Statistics;

public class CompanyMonthDto : FinanceStatisticsDto
{
    public DateTime? Month { get; set; }
}