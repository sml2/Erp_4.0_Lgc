using ERP.ControllersWithoutToken.Tasks;
using ERP.Enums.Orders;
using ERP.Extensions;
using ERP.Models.Abstract;
using Newtonsoft.Json;

namespace ERP.ViewModels.Statistics;

public class OrderVM
{
    public OrderVM(DbSetExtension.PaginateStruct<OrderStatisticsVm> list, object rules, Dictionary<int, string> sortBy,
        Dictionary<int, string> stateGroup, object? storeCompanys)
    {
        List = list;
        Rules = rules;
        SortBy = sortBy;
        StateGroup = stateGroup;
        StoreCompanys = storeCompanys;
    }

    [JsonProperty("list")]
    public DbSetExtension.PaginateStruct<OrderStatisticsVm> List { get; set; }

    [JsonProperty("rules")]
    public object Rules { get; set; }

    [JsonProperty("sortBy")]
    public Dictionary<int, string> SortBy { get; set; }

    [JsonProperty("stateGroup")]
    public Dictionary<int, string> StateGroup { get; set; }

    [JsonProperty("storeCompanys")]
    public object? StoreCompanys { get; set; }
}