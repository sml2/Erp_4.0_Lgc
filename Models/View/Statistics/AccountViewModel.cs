using ERP.Models.View.Product;
using Newtonsoft.Json;

namespace ERP.ViewModels.Statistics;

public class AccountViewModel
{
    [JsonProperty("role")]
    public string Role { get; set; }

    [JsonProperty("create_user")]
    public string CreateUser { get; set; }

    [JsonProperty("create_username")]
    public string CreateUsername { get; set; }

    [JsonProperty("created_at")]
    public DateTime CratedAt { get; set; }

    [JsonProperty("first_login_web")]
    public string? FirstLoginWeb { get; set; }

    [JsonProperty("id")]
    public int Id { get; set; }

    [JsonProperty("is_distribution")]
    public bool IsDistribution { get; set; }

    [JsonProperty("mobile")]
    public string? Mobile { get; set; }

    [JsonProperty("oem_id")]
    public int OemId { get; set; }

    [JsonProperty("username")]
    public string Username { get; set; }

    [JsonProperty("truename")]
    public string Truename { get; set; }
}