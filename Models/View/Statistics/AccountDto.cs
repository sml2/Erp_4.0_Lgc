using ERP.Models.View.Product;
using Newtonsoft.Json;

namespace ERP.ViewModels.Statistics;

public class AccountDto:PageDto
{
    [JsonProperty("name")]
    public string? Name { get; set; }

    [JsonProperty("mobile")]
    public string? Mobile { get; set; }
}