using ERP.Models.View.Product;
using Newtonsoft.Json;

namespace ERP.ViewModels.Statistics;

public class CountDto:PageDto
{
    [JsonProperty("name")]
    public string? Name { get; set; }
}