using ERP.Enums.Orders;
using ERP.Enums.Statistics;
using ERP.Models.View.Product;
using Newtonsoft.Json;

namespace ERP.ViewModels.Statistics;

public class OrderDto : PageDto
{
    [JsonProperty("order_no")]
    public string? OrderNo { get; set; }

    [JsonProperty("order_state")]
    public Enums.Orders.States? OrderState { get; set; }

    [JsonProperty("sort_by")]
    public SortBy? SortBy { get; set; }

    [JsonProperty("start")]
    public List<DateTime>? Date { get; set; }

    [JsonProperty("store_id")]
    public List<int>? StoreId { get; set; }
}