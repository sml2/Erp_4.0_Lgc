using ERP.Models.View.Product;
using Newtonsoft.Json;

namespace ERP.ViewModels.Statistics;

public class UserCountDto:CountDto
{
    [JsonProperty("companyId")]
    public int CompanyId { get; set; }
}