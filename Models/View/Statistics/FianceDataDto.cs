using ERP.Enums.Finance;
using ERP.Models.Finance;

namespace ERP.ViewModels.Statistics;

public class FianceDataDto
{
    public FianceDataDto(MoneyRecord money, TableFieldEnum field)
    {
        Money = money;
        Field = field;
    }
    
    public FianceDataDto(MoneyRecord money, TableFieldEnum field, int? indexId, int? sourceId)
    {
        Money = money;
        Field = field;
        IndexId = indexId;
        SourceId = sourceId;
    }
    public FianceDataDto(MoneyRecord money, TableFieldEnum field, int? indexId, int? sourceId, string? reason = null, string? remark = null)
    {
        Money = money;
        Field = field;
        IndexId = indexId;
        SourceId = sourceId;
        Reason = reason;
        Remark = remark;
    }

    public MoneyRecord Money { get; set; }
    public TableFieldEnum Field { get; set; }
    public int? IndexId { get; set; }
    public int? SourceId { get; set; }
    public string? Reason { get; set; }
    public string? Remark { get; set; }
    
    public static implicit operator FinancialAffairsModel(FianceDataDto dto)
    {
        return new FinancialAffairsModel(dto.Money, dto.Field, dto.IndexId, dto.SourceId,dto.Reason,dto.Remark);
    }
}