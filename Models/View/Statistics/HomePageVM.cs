using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace ERP.ViewModels.Statistics;

public class HomePageVM
{
    [JsonProperty("user_id")]
    public int? UserId { get; set; }
    
    [JsonProperty("group_id")]
    public int? GroupId { get; set; }
    
    public int? CompanyId { get; set; }

    [Comment("当前公司开通用户数(添加即汇总)"), JsonProperty("user_num")]
    public int UserNum { get; set; }

    [Comment("当前公司产品数(添加即汇总)"), JsonProperty("product_num")]
    public int ProductNum { get; set; }

    [Comment("当前公司订单数(添加即汇总)"), JsonProperty("order_num")]
    public int OrderNum { get; set; }

    [Comment("当前公司采购数(添加即汇总)"), JsonProperty("purchase_num")]
    public int PurchaseNum { get; set; }

    [Comment("当前公司运单数(添加即汇总)"), JsonProperty("waybill_num")]
    public int WaybillNum { get; set; }

    [Comment("当前公司店铺数(添加即汇总)"), JsonProperty("store_num")]
    public int StoreNum { get; set; }

    [Comment("下级上报开通员工数(添加即汇总)"), JsonProperty("report_user_num")]
    public int ReportUserNum { get; set; }


    [Comment("下级上报产品数(添加即汇总)"), JsonProperty("report_product_num")]
    public int ReportProductNum { get; set; }


    [Comment("下级上报订单数(添加即汇总)"), JsonProperty("report_order_num")]
    public int ReportOrderNum { get; set; }

    [Comment("下级上报采购数(添加即汇总)"), JsonProperty("report_purchase_num")]
    public int ReportPurchaseNum { get; set; }


    [Comment("下级上报运单数(添加即汇总)"), JsonProperty("report_waybill_num")]
    public int ReportWaybillNum { get; set; }

    [Comment("下级上报店铺数(添加即汇总)"), JsonProperty("report_store_num")]
    public int ReportStoreNum { get; set; }

    [Comment("下级上报分销订单数(添加即汇总)"), JsonProperty("distribution_order_num")]
    public int? DistributionOrderNum { get; set; }

    [Comment("下级上报分销采购数(添加即汇总)"), JsonProperty("distribution_purchase_num")]
    public int? DistributionPurchaseNum { get; set; }

    [Comment("下级上报分销运单数(添加即汇总)"), JsonProperty("distribution_waybill_num")]
    public int? DistributionWaybillNum { get; set; }
}