using ERP.Enums.Finance;
using ERP.Services.DB.Statistics;

namespace ERP.ViewModels.Statistics;

public class ReportMoneyDto
{
    public int CompanyId { get; set; }
    public int OemId { get; set; }
    public int UserId { get; set; }
    public int? GroupId { get; set; }
    public int StoreId { get; set; }
    public Statistic.MoneyColumnEnum Column { get; set; }
    public MoneyRecord Value { get; set; }
    public TableFieldEnum Field { get; set; }
    public int? IndexId { get; set; }
    public int? SourceId { get; set; }
    public string? Reason { get; set; }
    public string? Remark { get; set; }

    public static implicit operator DataBelongDto(ReportMoneyDto dto)
    {
        return new DataBelongDto(dto.UserId, dto.GroupId, dto.CompanyId, dto.OemId,dto.StoreId);
    }
}