namespace ERP.ViewModels.Statistics;

public class AmountByOrderVm
{
    public MoneyRecord OrderTotal { get; set; }
    public MoneyRecord OrderTotalFee { get; set; }
    public MoneyRecord OrderTotalRefund { get; set; }
    public MoneyRecord OrderTotalLoss { get; set; }
    // public int PurchaseTotal { get; set; }
    // public int PurchaseTotalRefund { get; set; }
    // public int WaybillTotal { get; set; }
    // public int WaybillTotalRefund { get; set; }
}