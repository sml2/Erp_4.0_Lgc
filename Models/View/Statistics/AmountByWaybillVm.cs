namespace ERP.ViewModels.Statistics;

public class AmountByWaybillVm
{
    public MoneyRecord WaybillTotal { get; set; }
    public MoneyRecord WaybillTotalRefund { get; set; }
}