using ERP.Models.View.Product;
using Newtonsoft.Json;

namespace ERP.ViewModels.Statistics;

public class AccountLogDto:PageDto
{
    [JsonProperty("id")]
    public int Id { get; set; }
}