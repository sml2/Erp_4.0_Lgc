using ERP.Attributes;
using ERP.ControllersWithoutToken.Tasks;
using ERP.Enums.Orders;
using ERP.Models.Abstract;
using Newtonsoft.Json;

namespace ERP.ViewModels.Statistics;

public class OrderStatisticsVm
{
    public OrderStatisticsVm(Models.DB.Orders.Order m, string unit)
    {
        OrderId = m.ID;
        OrderImage = m.Url;
        StoreName = m.Store?.Name;
        StoreId = m.StoreId;
        //StoreNationId = m.Store?.NationName;
        OrderNo = m.OrderNo;
        PayMoney = m.OrderTotal.Money;
        Fee = m.Fee.Money;
        Loss = m.Loss.Money;
        Refund = m.Refund.Money;
        PurchaseFee = m.PurchaseFee.Money;
        ShippingMoney = m.ShippingMoney.Money;
        Profit = m.Profit.Money;
        ProfitMargin = PayMoney > 0 ? $"{m.Profit.Money.Value / m.OrderTotal.Money.Value * 100:0.00}%": "0";
        OrderState = m.State;
        OrderTime = m.CreatedAt;
        NationShort = m.Receiver?.NationShort;
        ReceiverNation = m.Receiver?.Nation;
    }

    public string? ReceiverNation { get; set; }

    public string? NationShort { get; set; }


    [JsonProperty("id")]
    public int OrderId { get; set; }

    [JsonProperty("url")]
    public string? OrderImage { get; set; }

    [JsonProperty("store_name")]
    public string? StoreName { get; set; }

    [JsonProperty("store_id")]
    public int? StoreId { get; set; }

    //[JsonProperty("receiver_nation")]
    //public string? StoreNationId { get; set; }

    [JsonProperty("order_no")]
    public string? OrderNo { get; set; }

    [JsonProperty("pay_money"),JsonMoneyForDefaultUnit]
    public Money? PayMoney { get; set; }

    [JsonProperty("fee"),JsonMoneyForDefaultUnit]
    public Money? Fee { get; set; }

    [JsonProperty("loss"),JsonMoneyForDefaultUnit]
    public Money? Loss { get; set; }

    [JsonProperty("refund"),JsonMoneyForDefaultUnit]
    public Money? Refund { get; set; }

    [JsonProperty("purchase_fee"),JsonMoneyForDefaultUnit]
    public Money? PurchaseFee { get; set; }

    [JsonProperty("shipping_money"),JsonMoneyForDefaultUnit]
    public Money? ShippingMoney { get; set; }

    [JsonProperty("profit"),JsonMoneyForDefaultUnit]
    public Money? Profit { get; set; }

    [JsonProperty("profit_margin")]
    public string? ProfitMargin { get; set; }

    [JsonProperty("order_state")]
    public States OrderState { get; set; }

    [JsonProperty("create_time")]
    public DateTime? OrderTime { get; set; }
}