namespace ERP.ViewModels.Statistics;

public class AmountByPurchaseVm
{
    public MoneyRecord PurchaseTotal { get; set; }
    public MoneyRecord PurchaseTotalRefund { get; set; }
    // public int WaybillTotal { get; set; }
    // public int WaybillTotalRefund { get; set; }
}