using ERP.Models.Finance;
using ERP.Models.View.Product;
using Newtonsoft.Json;

namespace ERP.ViewModels.Finance;

public class BillLogIndexDto : PageDto
{
    [JsonProperty("type")]
    public BillLog.Types? Type { get; set; }

    [JsonProperty("audit")]
    public BillLog.Audits? Audit { get; set; }

    [JsonProperty("module")]
    public BillLog.Modules? Module { get; set; }

    [JsonProperty("shop_id")]
    public int? ShopId { get; set; }

    [JsonProperty("start")]
    public List<DateTime>? Date { get; set; }
    
    public string? OrderNo { get; set; }
    public string? PurchaseNo { get; set; }
    public string? WaybillNo { get; set; }
    
    public int? CompanyId { get; set; }
}