using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace ERP.ViewModels.Finance;

public class BillLogDto
{
    [JsonProperty("order_no")]
    public string? OrderNo { get; set; }
    
    [JsonProperty("refund_difference")]
    public decimal RefundDifference { get; set; }
    
    [JsonProperty("company_id")]
    public int CompanyId { get; set; }
    
    [JsonProperty("id")]
    public int Id { get; set; }
    
    [JsonProperty("store_id")]
    public int StoreId { get; set; }
    
    [JsonProperty("info_url"),Required]
    public string InfoUrl { get; set; } = string.Empty;
}