using System.ComponentModel.DataAnnotations.Schema;
using ERP.Extensions;
using ERP.Models.Finance;
using Ryu.Extensions;

namespace ERP.ViewModels.Finance;

public class BillLogVM
{
    public BillLogVM(ISession? session, BillLog info)
    {
        Money = info.Money.To(session!.GetUnitConfig()).ToFixed();
        Balance = info.Balance?.To(session!.GetUnitConfig()).ToFixed();
        Belong = info.Belong;
        Module = info.Module;
        Type = info.Type;
        Remark = info.Remark ?? string.Empty;
        Reason = info.Reason ?? string.Empty;
        CreatedAt = info.CreatedAt;
        UpdatedAt = info.UpdatedAt;
        Id = info.ID;
        Truename = info.Truename ?? string.Empty;
        AuditTruename = info.AuditTruename ?? string.Empty;
        Audit = info.Audit;
        Datetime = info.Datetime;
        ImgAll = info.ImgAll;
    }

    public string? Balance { get; set; }

    [NJP("img_all")]
    public string? ImgAll { get; set; }

    public int Id { get; set; }
    public string Money { get; set; }
    public BillLog.Belongs Belong { get; set; }
    public BillLog.Modules Module { get; set; }
    public BillLog.Types Type { get; set; }
    public string Remark { get; set; }
    public string Reason { get; set; }
    public string Truename { get; set; }

    [NJP("audit_truename")]
    public string AuditTruename { get; set; }

    public BillLog.Audits Audit { get; set; }

    public DateTime? Datetime { get; set; }
    
    [NotMapped]
    public string? DatetimeUi => Datetime?.ToString("yyyy-MM-dd");
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }

    public string? UpdatedAtTicks
    {
        get { return UpdatedAt.Ticks.ToString(); }
        set { }
    }
}