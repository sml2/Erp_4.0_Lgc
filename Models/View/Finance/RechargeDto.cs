using ERP.Models.Finance;
using Newtonsoft.Json;

namespace ERP.ViewModels.Finance;

public class RechargeDto : IdDto
{
    public Info Info { get; set; } = new();
}

public class Info
{
    [JsonProperty("execdate")]
    public DateTime? ExecDate { get; set; }

    [JsonProperty("reason")]
    public string? Reason { get; set; }

    [JsonProperty("remark")]
    public string? Remark { get; set; }

    [JsonProperty("imgs")]
    public List<string>? Images { get; set; }

    [JsonProperty("money")]
    public decimal Money { get; set; }

    public BillLog.Modules? Modules { get; set; } = BillLog.Modules.INTERCHANGEABLE;
}