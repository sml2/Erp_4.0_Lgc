using ERP.Models.Finance;
using Newtonsoft.Json;

namespace ERP.ViewModels.Finance;

public class DoAuditDto
{
    [JsonProperty("id")]
    public int Id { get; set; }
    
    [JsonProperty("audit")]
    public BillLog.Audits Audit { get; set; }
}