using ERP.Extensions;
using ERP.Models.Finance;
using ERP.Models.Stores;
using Newtonsoft.Json;

namespace ERP.ViewModels.Finance;

public class AuditIndexVM
{
    public AuditIndexVM(DbSetExtension.PaginateStruct<BillLog> list, IList<StoreRegion>? store, int companyId, object wallet, object rule, Dictionary<int, string> module)
    {
        List = list;
        Store = store;
        CompanyId = companyId;
        Wallet = wallet;
        Rule = rule;
        Module = module;
    }

    [JsonProperty("list")]
    public DbSetExtension.PaginateStruct<BillLog> List { get; set; }
    
    [JsonProperty("store")]
    public IList<StoreRegion>? Store { get; set; }
    
    [JsonProperty("company_id")]
    public int CompanyId { get; set; }
    
    [JsonProperty("wallet")]
    public object Wallet { get; set; }
    
    [JsonProperty("rule")]
    public object Rule { get; set; }
    
    [JsonProperty("module")]
    public Dictionary<int, string> Module { get; set; }
}