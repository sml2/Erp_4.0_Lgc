using System.ComponentModel.DataAnnotations;
using ERP.Enums.Orders;
using ERP.Models.Api.Amazon.vm;
using ERP.Models.DB.Stores;
using ERP.Models.Stores;

namespace ERP.Models.View.Order.AmazonOrder;

public class DeliveryGoodVM
{
    [Required]
    public int? ID { get; set; }
    [Required]

    //public SyncAmazon syncAmazon { get; set; }
    public string CarrierName { get; set; } = string.Empty;
    public string ShippingMethod { get; set; } = string.Empty;

    public string TrackingNumber { get; set; } = string.Empty;

    public int? OrderStatus { get; set; }

    public bool HandWrite { get; set; } = false;
    
    public string? Country { get; set; }

    public List<ProgressValue> OrderProgress { get; set; } = new List<ProgressValue>();
}

public class OrderInfoVM
{
    public int ID { get; set; }
    [Required]
    public string OrderNo { get; set; } = string.Empty;
}

public class ProgressValue
{
    public int id;//select value-key 
    //public BITS bitMask;//位掩码不需要了
    public BITS bits;//操作位
    public bool state;//置 1或0
}


public class DeliveryGoodsInfo
{
    public DeliveryGoodsInfo(StoreRegion store, List<DeliverProduct> deliverProducts, string marketplaceID, DeliveryGoodVM deliveryGood)
    {
        this.store = store;
        this.deliverProducts = deliverProducts;
        MarketplaceID = marketplaceID;
        this.deliveryGood = deliveryGood;
    }
    public StoreRegion store { get; set; }
    public List<DeliverProduct> deliverProducts { get; set; } = new();
    public string MarketplaceID { get; set; }
    public DeliveryGoodVM deliveryGood { get; set; }
}