﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace ERP.ViewModels.Order.AmazonOrder
{
    /// <summary>
    /// 备注 (订单 物流 采购)
    /// </summary>
    public class RemarkVM
    {
        public int Id { get; set; }

        public string? Remark { get; set; }

        public int? Type { get; set; }
    }
}
