﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace ERP.ViewModels.Order.AmazonOrder
{
    // entrymode = 1 自定义录入 添加 修改 
    public class EditGoodsVM
    {
        //产品 id add 0 edit/del >0
        //[Required(ErrorMessage = "请填写商品编号")]
        public string? Id { get; set; } = string.Empty;
        public string? OldGoodId { get; set; } = string.Empty;
        public string? Sku { get; set; } = string.Empty;
        // 订单ID
        public int OrderId { get; set; }
        //[Required(ErrorMessage = "请填写商品名称")]
        public string Name { get; set; } = string.Empty;
        public int Num { get; set; }
        public decimal Price { get; set; }
        //[Required(ErrorMessage ="Url 图片有误")]
        public string Url { get; set; } = string.Empty;
        public string FromUrl { get; set; } = "";
        //public string FileList { get; set; }
    }


    public class GoodsVM
    {
        public string? Id { get; set; }
        public string SKU { get; set; }
        /// <summary>
        /// 商品名称
        /// </summary>
        [Required(ErrorMessage = "请填写商品名称")]
        public string Name { get; set; } = string.Empty;//name:名称

        /// <summary>
        /// 商品图片
        /// </summary>
        //[Required(ErrorMessage = "商品图片有误")]
        public string ImageURL { set; get; } = string.Empty;//url:商品图片


        /// <summary>
        /// 订购数量
        /// </summary>
        public int QuantityOrdered { get; set; }//num:1


        public string? FromURL { get; set; }  //from_url:商品网址

        /// <summary>
        /// 单价
        /// </summary>
        public MoneyVM UnitPrice { get; set; }= new();

        /// <summary>
        /// 总价
        /// </summary>
        public MoneyVM TotalPrice { get; set; } = new();

        public string toPayload()
        {
            return JsonConvert.SerializeObject(this);
        }
    }


    public class MoneyVM
    {
        public MoneyMeta Mate { get => new(unit, value); }
        public decimal value { get; set; }
        [Required]
        public string unit { get; set; } = string.Empty;
    }

}
