﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace ERP.ViewModels.Order.AmazonOrder
{
    public class ManualAddOrderVM
    {
        [JsonProperty("payload")]
        public string Payload { get; set; } = string.Empty;

        [JsonProperty("payload")]
        public string? OrderNo { get; set; }

        [JsonProperty("payload")]
        public string? Progress { get; set; }
    }
}
