﻿using NuGet.Packaging.Signing;

namespace ERP.ViewModels.Order.AmazonOrder
{
   
    public class DeliverVM
    {
        public DateTime[]? start { get; set; }
        public int? sortBy { get; set; } = -1;
        public string? receiverName { get; set; }
        public int? latestShipTime { get; set; }
        public string? orderNo { get; set; }
    }
}
