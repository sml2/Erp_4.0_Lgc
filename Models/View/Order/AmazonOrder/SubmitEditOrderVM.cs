﻿namespace ERP.Models.View.Order.AmazonOrder;

public class SubmitEditOrderVM
{
    public int Id { get; set; }
    public int StoreId { get; set; }
    public Platforms Plateform { get; set; }
    public string OrderNo { get; set; } = string.Empty;
    public decimal OrderTotal { get; set; }
}
