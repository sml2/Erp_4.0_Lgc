namespace ERP.ViewModels.Order.AmazonOrder;

using Enums;
using Enums.Orders;
using static Models.DB.Orders.Order;

/// <summary>
/// 亚马逊订单index 查询条件
/// </summary>
public class OrderIndexListQueryVM
{
    public int Limit { get; set; }
    public int Page { get; set; }
    public int Total { get; set; }

    //public string Deliver { get; set; }

    public Platforms Plateform { get; set; }
    public List<DateTime>? StartAndEndTime { get; set; }


    public string? LatestShipTimeFilter { get; set; }

    public string OrderNo { get; set; } = string.Empty;

    public States? OrderState { get; set; }


    public ProgressMaskValue Progress { get; set; } = new ProgressMaskValue { Mask = 0, Value = 0 };

    public string? PurchaseOrderNo { get; set; } = string.Empty;
    public string? PurchaseTrackingNumber { get; set; } = string.Empty;
    public string? Express { get; set; } = string.Empty; // 国际运单号 对应 数据库字段为WaybillOrder
    public string? TrackingNumber { get; set; } = string.Empty; // 运单追踪号

    //public string SellerId { get; set; }

    public int? SortBy { get; set; }
    public List<int>? Store { get; set; } = new();
    
    public List<string>? Seller { get; set; } = new();

    public string? Sku { get; set; }
    public string? GoodId { get; set; }
    
    /// <summary>
    /// 只查看被分配订单
    /// </summary>
    public bool? OnlyDistributed { get; set; }

    public bool DistributedLinkFlag { get; set; } = false;
    
    public string? GoodName { get; set; }
}

public class ProgressMaskValue
{
    public static readonly ProgressMaskValue All = new(0, 0);

    public ProgressMaskValue() { }
    public ProgressMaskValue(ulong mask, ulong value) : this()
    {
        Mask = mask;
        Value = value;
    }

    public ulong Mask { get; set; }
    public ulong Value { get; set; }
}

