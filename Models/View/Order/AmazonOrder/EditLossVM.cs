namespace ERP.ViewModels.Order.AmazonOrder
{
    public class EditLossVM
    {
        public int Id { get; set; }
        public decimal Loss { get; set; }

        public string Unit { get; set; }
    }
}
