﻿namespace ERP.ViewModels.Order.AmazonOrder;

using Enums.Orders;
using System.ComponentModel.DataAnnotations;
using static Models.DB.Orders.Order;
public class ChangeStateVM
{
    [Required]
    public List<int> OrderIds { get; set; } = new();
    //public BITS Mask { get; set; } // 不需要了
    public BITS OrderProgress { get; set; }
    public bool State { get; set; }
}
