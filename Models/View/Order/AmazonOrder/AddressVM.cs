﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace ERP.ViewModels.Order.AmazonOrder
{
    /// <summary>
    /// 收货人信息 
    /// </summary>
    public class AddressVM
    {
        public int Id { get; set; }

        public int NationId { get; set; }

        public string Name { get; set; } = string.Empty;

        public string Phone { get; set; } = string.Empty;

        public string? Address1 { get; set; }

        public string? Address2 { get; set; }

        public string? Address3 { get; set; }

        public string? Email { get; set; }

        //public string? Postcode { get; set; }

        public string? Zip { get; set; }

        public string? Province { get; set; }

        public string? City { get; set; }

        public string? District { get; set; }

    }
}
