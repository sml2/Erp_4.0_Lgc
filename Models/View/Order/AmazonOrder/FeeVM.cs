﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace ERP.ViewModels.Order.AmazonOrder
{
    public class FeeVM
    {
        [JsonProperty("id"), Required(ErrorMessage = "")]
        public int Id { get; set; }
    }
}
