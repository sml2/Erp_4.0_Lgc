﻿namespace ERP.ViewModels.Order.AmazonOrder;
using Enums;

public class AddOrderRequestVM
{
    //此处商品类型需要重新适配
    public List<GoodsVM> Goods { get; set; } = new List<GoodsVM>();
    public string OrderNo { get; set; } = string.Empty;
    public decimal OrderTotal { get; set; } // 订单总价
    public Platforms Plateform { get; set; }
    public string ReceiverAddress { get; set; } = string.Empty;
    public string ReceiverCity { get; set; } = string.Empty;
    public string ReceiverDistrict { get; set; } = string.Empty;
    public string ReceiverEmail { get; set; } = string.Empty;
    public string ReceiverName { get; set; } = string.Empty;
    public int ReceiverNationId { get; set; }
    public string ReceiverPhone { get; set; } = string.Empty;
    public string ReceiverProvince { get; set; } = string.Empty;
    public string ReceiverZip { get; set; } = string.Empty;
    public int StoreId { get; set; }
}

public class DoMergeOrderInfo 
{ 
    public int order_id { get; set; } 
    public int is_main { get; set; } 
    public int userid { get; set; } 
    public int companyid { get; set; } 
}



public class OrderChooseFieldParam
{
    /// <summary>
    /// 订单号
    /// </summary>
    public bool ChkOrderNum { get; set; } = false;

    /// <summary>
    /// 订单总额
    /// </summary>
    public bool ChkOrderFinance { get; set; } = false;

    /// <summary>
    /// 订单时间
    /// </summary>
    public bool ChkOrderTime { get; set; } = false;

    /// <summary>
    /// 所属店铺
    /// </summary>
    public bool ChkStore { get; set; } = false;

    /// <summary>
    /// SellerID
    /// </summary>
    public bool ChkSellerID { get; set; } = false;

    /// <summary>
    /// 成本损失费
    /// </summary>
    public bool ChkCostLossFee { get; set; } = false;

    /// <summary>
    /// 手续费
    /// </summary>
    public bool ChkHandlingFee { get; set; } = false;

    /// <summary>
    /// 订单产品信息
    /// </summary>
    public bool ChkOrderProducts { get; set; } = false;

    /// <summary>
    /// 收件人信息
    /// </summary>
    public bool ChkReceiver { get; set; } = false;

    /// <summary>
    /// 采购信息
    /// </summary>
    public bool ChkPurchase { get; set; } = false;

    /// <summary>
    /// 运单信息
    /// </summary>
    public bool ChkWayBill { get; set; } = false;

    /// <summary>
    /// 订单ids
    /// </summary>
    public List<int> OrderIDs { get; set; } = new();

    public int ItemCount { get=>100;}

    public int sleep { get => 1000; }

}