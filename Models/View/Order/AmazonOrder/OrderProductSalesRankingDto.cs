﻿namespace ERP.Models.View.Order.AmazonOrder;

public class OrderProductSalesRankingDto
{
    public int? StoreId { get; set; }
    public string? Sku { get; set; }
    public string? GoodId { get; set; }
    public List<DateTime>? StartAndEndTime { get; set; }
}

