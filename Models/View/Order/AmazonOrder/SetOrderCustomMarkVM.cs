﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using static ERP.Models.DB.Identity.User;
using static ERP.Models.DB.Orders.Order;

namespace ERP.ViewModels.Order.AmazonOrder;

public class SetOrderCustomMarkVM
{
    public List<int> OrderIds { get; set; } = new List<int>();

    public CustomMarkJson CustomMark { get; set; } = new CustomMarkJson();
}
