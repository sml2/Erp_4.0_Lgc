﻿namespace ERP.Models.View.Order
{
    public class WayInfo
    {
        public int  ID { get; set; }
        public string WaybillOrder { get; set; }
        public string Express { get; set; }
        public ERP.Enums.Logistics.States  State { get; set; }
        public string  StateDes { get => Extensions.EnumExtension.GetDescription(State, ""); }
    public int OrderId { get; set; }
}
}
