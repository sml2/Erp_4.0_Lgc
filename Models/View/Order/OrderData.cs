namespace ERP.Models.View.Order
{
    public class OrderData
    {
        public string OrderNum { get; set; }
        public string OrderFinance { get; set; }
        public string OrderUnit { get; set; }
        public string StoreName { get; set; }
        public string StoreCountry { get; set; }
        public string SellerID { get; set; }
        public string OrderTime { get; set; }
        public string HandlingFee { get; set; }
        public string CostLoss { get; set; }
        public List<Product> OrderProducts { get; set; } = new();
        public class Product {
            public string ImageUrl { get; set; }
            public string Name { get; set; }
            public string Asin { get; set; }
            public string Sku { get; set; }
            public int Num { get; set; }
            public string Unit { get; set; }
            public string Price { get; set; }
            public string ShipFee { get; set; }
            public string Total { get; set; }
        }
        public ReceiverItem Receiver { get; set; } 
        public class ReceiverItem {
            public string receiver_name { get; set; }
            public string receiver_phone { get; set; }
            public string receiver_zip { get; set; }
            /// <summary>
            /// 国家
            /// </summary>
            public string receiver_nation { get; set; }
            /// <summary>
            /// 州/省/区域
            /// </summary>
            public string receiver_province { get; set; }
            /// <summary>
            /// 城市
            /// </summary>
            public string receiver_city { get; set; }
            /// <summary>
            ///  区域
            /// </summary>
            public string receiver_district { get; set; }
            public string receiver_email { get; set; }
            public string receiver_address { get; set; }
            public string receiver_address2 { get; set; }
            public string receiver_address3 { get; set; }
        }
        public PurchaseItem Purchase { get; set; } 
        public class PurchaseItem {
            public string TotalPrice { get; set; }
            public string PurchaseTrackNum { get; set; }
        }
        public WayBillItem WayBill { get; set; } 
        public class WayBillItem {
            public string OrderNum { get; set; }
            public string ShippingFee { get; set; }
        }
    }
}
