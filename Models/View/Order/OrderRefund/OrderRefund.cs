﻿using System.ComponentModel.DataAnnotations;
using static ERP.Models.DB.Orders.Refund;

namespace ERP.ViewModels.Order.OrderRefund
{
    public class OrderRefund
    {
        //订单号
        [Required]
        public string OrderNo { get; set; }=string.Empty;
        //订单状态
        public RefundState State { get; set; }
        //店铺名称
        public int ShopID { get; set; }
    }
}
