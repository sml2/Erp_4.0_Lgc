using ERP.Enums.Orders;

namespace ERP.ViewModels.Order.Merge
{
    public class MergeList
    {
        public string? orderType { get; set; }
        public MergeTypes? TypeStatus
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(orderType)){
                    int n=int.Parse(orderType);
                    if(Enum.IsDefined(typeof(MergeTypes), n) && Enum.TryParse<MergeTypes>(n.ToString(), out var taxTypeEnum))
                    {
                        return taxTypeEnum;
                    }
                    else
                    {
                        throw new AggregateException($"MergeList orderType:{orderType} error");
                    }        
                }
                return null;
            }
        }
        public string? OrderNo { get; set; }
    }
    public class MergeOrderInfo
    {
        public int order_id { get; set; }
        public IsMains is_main { get; set; }
    }

    public record PendindList(int id,string nation,string Url,string Store,string OrderNo, int ProductNum,int CompanyID,int? NationId,int StoreId,string orderState,int isMain);
    public record MergeListVm(int id,string? OrderNo,int ProductNum, string State,string StoreName,int MergeId,string? Url,string? Coin,string OrderTotal,string Bit,string FakeProductTime,string TrueProductTime,
            DateTime? CreateTime,string LatestShipTime,int mergeCount, string nation,string MergeType,int OrderCompanyId);



    public class GoodListVM
    {
        public int OrderID { get; set; }
        public string OrderNo { get; set; }
        public Platforms Plateform { get; set; }
        public string OrderUnit { get; set; }
        public string? ID { get; set; }
        public string Name { get; set; }
        public string FromUrl { get; set; }
        public string Sku { get; set; }
        public string UnitPrice { get; set; }
        public string ItemPrice { get; set; }
        public string TotalPrice { get; set; }
        public string ImageURL { get; set; }
        public int QuantityOrdered { get; set; }
        public int QuantityShipped { get; set; }
        public int WaitForSendNum { get; set; }//待发货数
        public string Brand { get; set; }
        public string Size { get; set; }
        public string Color { get; set; }
        public string ShippingPrice { get; set; }
        public string Unit { get; set; }
        public string goodIdText { get; set; }
    }


}
