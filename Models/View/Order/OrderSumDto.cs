using ERP.Attributes;
using Ryu.Extensions;
using Ryu.Money.Json;
using System.Runtime.CompilerServices;

namespace ERP.Models.View.Order;

public class OrderSumDto
{
 

    private Money _OrderTotal;
    public Money OrderTotal { get {return Math.Round(_OrderTotal.Value,2); } set {  _OrderTotal= value; } }

    private Money _Fee; 
    public Money Fee { get { return Math.Round(_Fee.Value, 2); } set { _Fee = value; } }

    private Money _Refund;
    public Money Refund { get { return Math.Round(_Refund.Value, 2); } set { _Refund = value; } }

    private Money _ShippingMoney;
    public Money ShippingMoney { get { return Math.Round(_ShippingMoney.Value, 2); } set { _ShippingMoney = value; } }

    private Money _PurchaseFee;
    public Money PurchaseFee { get { return Math.Round(_PurchaseFee.Value, 2); } set { _PurchaseFee = value; } }

    private Money _Loss;
    public Money Loss { get { return Math.Round(_Loss.Value, 2); } set { _Loss = value; } }

    private Money _Profit;

    // Profit = OrderTotal- Fee- Refund - PurchaseFee- ShippingMoney- Loss,
    public Money Profit { get {
            var p = _OrderTotal - _Fee - _Refund - _ShippingMoney - _PurchaseFee - _Loss;
            return Math.Round(p.Value, 2); } }

    public string ProfitMargin => $"{((OrderTotal.Value == 0 ? 1 : (Profit.Value / OrderTotal.Value)) * 100).ToFixed(0)}%";


    public string Unit { get; set; }

}