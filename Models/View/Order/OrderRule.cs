namespace ERP.Models.View.Order
{
    public class OrderRule
    {
        public bool purchaseBack { get; set; }= false;
        public bool purchaseReport { get; set; } = false;

        public bool waybillBack { get; set; } = false;
        public bool waybillReport { get; set; } = false;

        public bool orderBack { get; set; } = false;
        public bool orderReport { get; set; } = false;
    }
}
