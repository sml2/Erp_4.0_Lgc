using ERP.Enums.Orders;
using ERP.ViewModels.Order.AmazonOrder;

namespace ERP.Models.View.Distribution
{
    public class AmazonOrderListQuery
    {
        public string? OrderNo { get; set; }
        public ProgressMaskValue? progress { get; set; }
        public Enums.Orders.States? OrderState { get; set; }
        public int? CompanyId { get; set; }
        public SortBy? SortBy { get; set; }
        public string? PurchaseTrackingNumber { get; set; }
        public DateTime[]? time { get; set; }
        public string? WaybillOrder { get; set; }
        public int? Type { get; set; }
        public bool? OnlyDistributed { get; set; }
        public int Limit { get; set; }
        public int Page { get; set; }
        public int Total { get; set; }
    }
}