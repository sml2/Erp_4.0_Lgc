﻿namespace ERP.ViewModels.VueDynamicControl
{
    public class SizeField
    {
        public string Label { get; set; } = string.Empty;
        public string LabelWidth { get; set; } = string.Empty;
        public bool ShowLabel { get; set; }
        public bool ChangeTag { get; set; }
        public string Tag { get; set; } = string.Empty;
        public string TagIcon { get; set; } = string.Empty;
        public string DefaultValue { get; set; } = string.Empty;
        public bool Required { get; set; } 
        public string Layout { get; set; } = string.Empty;
        public int Span { get; set; } 
        public string Document { get; set; } = string.Empty;
        public string Pattern { get; set; } = string.Empty;
        public string Message { get; set; } = string.Empty;
        public string Prepend { get; set; } = string.Empty;
        public string Append { get; set; } = string.Empty;
        public string Key { get; set; } = string.Empty;
        public string Value { get; set; } = string.Empty;
        public string Placeholder { get; set; } = string.Empty;
        public string Style { get; set; } = string.Empty;
        public bool Clearable { get; set; }
        public string PrefixIcon { get; set; } = string.Empty;
        public string SuffixIcon { get; set; } = string.Empty;
        public int Maxlength { get; set; }
        public bool ShowWordLimit { get; set; }
        public bool Readonlying { get; set; }
        public bool Disabled { get; set; }
    }
}
