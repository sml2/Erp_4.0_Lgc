﻿using System.ComponentModel.DataAnnotations;
using ERP.Models.View.Product;
using static ERP.Models.Abstract.BaseModel;

namespace ERP.ViewModels;

public class IdDto:PageDto
{
    [Required(ErrorMessage = "缺少参数: ID")]
    public int ID { get; set; }

    [Obsolete("废弃",true)]
    public List<string>? MarketplaceIds { get; set; } = new();
    [Obsolete("废弃",true)]
    public string? Region { get; set; }
}


public class marketDto
{
    [Required(ErrorMessage = "缺少参数: ID")]
    public int ID { get; set; }

    [Required(ErrorMessage = "缺少参数: PlatformID")]
    public int PlatformID { get; set; }

    [Required(ErrorMessage = "缺少参数: MID")]
    public int MID { get; set; }

    public int unitID { get; set; }

    public StateEnum state { get; set; }
}