using ERP.Models.View.Products.Product;

namespace ERP.ViewModels.B2C;

public class ProductsViewModel
{
    public object? B2C { get; set; }
    public int PID { get; set; }
    public string Name { get; set; }
    public string? Source { get; set; }
    public string Sku { get; set; }
    public int? Quantity { get; set; }
    public string ShowImg { get; set; }
    public PricesViewModel Price { get; set; }
    public string? Description { get; set; }
    public List<string>? BulletPoint { get; set; }
    public string? SearchTerms { get; set; }
    public string? Category { get; set; }
    public string? SubCategory { get; set; }
    public Dictionary<string, string[]> Attribute { get; set; }
    public List<ProductViewModel> Products { get; set; }
    public string? ProductID { get; set; }
    public string Language { get; set; }
}