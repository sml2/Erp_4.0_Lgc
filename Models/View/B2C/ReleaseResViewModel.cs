namespace ERP.ViewModels.B2C;

public class ReleaseResViewModel
{
    [NJP("ID")]
    public int? Id { get; set; }

    [NJP("ERR")]
    public string? ERR { get; set; }

    [NJP("code")]
    public int? Code { get; set; }

    [NJP("success")]
    public string? Success { get; set; }

    [NJP("message")]
    public string? Message { get; set; }
    
    public DateTime Time  => DateTime.Now;
}