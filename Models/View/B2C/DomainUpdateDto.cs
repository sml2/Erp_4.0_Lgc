using ERP.Extensions;
using ERP.Models.B2C;

namespace ERP.ViewModels.B2C;

public class DomainUpdateDto
{
    public int? Id { get; set; }

    [NJP("domain")]
    public string Domain { get; set; }

    [NJP("name")]
    public string Name { get; set; }

    [NJP("url")]
    public string Url { get; set; }
    
    public string? UpdatedAtTicks { get; set; }

    public DomainModel To(ISession session)
    {
        var date = DateTime.Now;
        return new DomainModel()
        {
            Name = Name,
            Url = Url,
            Domain = Domain,
            UpdatedAt = date,
            CreatedAt = date,
            UserID = session.GetUserID(),
            GroupID = session.GetGroupID(),
            CompanyID = session.GetCompanyID(),
            OEMID = session.GetOEMID()
        };
    }
}