using ERP.Models.View.Product;

namespace ERP.ViewModels.B2C;

public class DomainListDto : PageDto
{
    [NJP("domain")]
    public string? Domain { get; set; }
}