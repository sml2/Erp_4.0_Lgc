namespace ERP.ViewModels.B2C.SituneShop;

public class ProductsViewModel
{
    public string pic { get; set; }
    public string name { get; set; }
    public string advPic { get; set; }
    public List<ProductViewModel> skuList { get; set; }
    public int productCategoryId => 92;
    public string type => "IT01";
    public int minOrderAmount => 1;
    public int warehouseType => 0;
    public int logisticsWay => 0;
    public int warehouseId => 1;
    public int shippingCode => 2;
    public string? chineseName => null;
    public string sellPointIntroduction { get; set; }
    public string? purchaseLink { get; set; }
}

public class ProductViewModel
{
    public string spData { get; set; }
    public string pic { get; set; }
    public string skuCode { get; set; }
    public decimal priceRmb { get; set; }
    public decimal originalPriceRmb { get; set; }
    public decimal purchasePriceRmb { get; set; }
    public int weight { get; set; }
    public int length { get; set; }
    public int height { get; set; }
    public int width { get; set; }
    public int stock { get; set; }
    public int showStatus { get; set; }
}