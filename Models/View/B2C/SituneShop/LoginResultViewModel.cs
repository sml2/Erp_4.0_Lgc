namespace ERP.ViewModels.B2C.SituneShop;

public class LoginResultViewModel
{
    public int Code { get; set; }
    
    public LoginResultDataViewModel Data { get; set; }
}

public class LoginResultDataViewModel
{
    public string? Token { get; set; }
    public string? TokenHead { get; set; }
}