using ERP.Models.B2C;
using ERP.Models.DB.Product;
using ERP.Models.Product;

namespace ERP.ViewModels.B2C.SituneShop;

public class ReleaseCheckViewModel
{
    public ReleaseCheckViewModel(DomainModel domain, ProductModel product, ProductItemModel item)
    {
        Domain = domain;
        Product = product;
        Item = item;
    }

    public DomainModel Domain { get; set; }
    public ProductModel Product { get; set; }
    public ProductItemModel Item { get; set; }
}