namespace ERP.ViewModels.B2C;

public class ProductViewModel
{
    public string Name { get; set; }
    public string Description { get; set; }
    public List<string>? BulletPoint { get; set; }
    public string? SearchTerms { get; set; }
    public Dictionary<int, int> Values { get; set; }
    public int Sku { get; set; }
    public string? MainImage { get; set; }
    public List<string> Images { get; set; }
    public int Quantity { get; set; }
    public decimal Price { get; set; }
    public decimal Cost { get; set; }
    public string? ProduceCode { get; set; }
    public string? Fields { get; set; }
    public string SID { get; set; }
}