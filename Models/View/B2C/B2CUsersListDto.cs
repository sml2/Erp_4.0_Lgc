using ERP.Models.Abstract;
using ERP.Models.View.Product;

namespace ERP.ViewModels.B2C;

public class B2CUsersListDto : PageDto
{
    public string? Username { get; set; }
    
    public BaseModel.StateEnum? Status { get; set; }
}