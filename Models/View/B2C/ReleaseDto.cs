namespace ERP.ViewModels.B2C;

public class ReleaseDto
{
    [NJP("id")]
    public int Id { get; set; }
    [NJP("platform_id")]
    public int DomainId { get; set; }
}