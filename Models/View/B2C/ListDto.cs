using ERP.Enums.Product;
using ERP.Models.View.Product;
using Newtonsoft.Json;

namespace ERP.ViewModels.B2C;

public class ListDto : PageDto
{
    [NJP("action")]
    public ActionEnum Action { get; set; }
    [NJP("good_id")]
    public int? GoodId { get; set; }
    [NJP("name")]
    public string? Name { get; set; }
    [NJP("pid")]
    public string? Pid { get; set; }
    [NJP("platform_id")]
    public int? PlatformId { get; set; }
    [NJP("user")]
    public int? User { get; set; }
    public List<DateTime>? UpdatedTime { get; set; }
    
    public ReleaseEnum? Release { get; set; }
    
    
    [JsonProperty("category_id")]
    public int? CategoryId { get; set; }

    [JsonProperty("sub_category_id")]
    public int? SubCategoryId { get; set; }
}

public enum ActionEnum
{
    Selection,
    Goods
}

