using ERP.Models.B2C;

namespace ERP.ViewModels.B2C;

public class DomainVM
{

    public DomainVM(DomainModel model)
    {
        Name = model.Name;
        Url = model.Url;
        Value = model.Domain;
        Id = model.ID;
    }
    
    [NJP("name")]
    public string Name { get; set; }
    [NJP("url")]
    public string Url { get; set; }
    [NJP("value")]
    public string Value { get; set; }
    [NJP("id")]
    public int Id { get; set; }
}