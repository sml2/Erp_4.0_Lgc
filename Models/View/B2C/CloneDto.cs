using ERP.Enums;

namespace ERP.ViewModels.B2C;

public class CloneDto
{
    [NJP("id")]
    public int ProductId { get; set; }
    [NJP("language")]
    public int Lang { get; set; }
}