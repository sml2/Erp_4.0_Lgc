using ERP.Models.View.Product;

namespace ERP.Models.View.Setting.Resources;

public class IndexDto : PageDto
{
    public int Type { get; set; }
    public string? Name { get; set; }
}