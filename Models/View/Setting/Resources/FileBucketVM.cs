using ERP.Models.Setting;
using Newtonsoft.Json;

namespace ERP.Models.View.Setting.Resources;

public class FileBucketVM
{
    public FileBucketVM(FileBucket model)
    {
        Name = model.Name;
        Bucket = model.Bucket;
        AccessKey = model.AccessKey;
        SecretKey = model.SecretKey;
        EndpointDomain = model.EndpointDomain;
        BucketDomain = model.BucketDomain;
        Type = model.Type;
        CreatedAt = model.CreatedAt;
        UpdatedAt = model.UpdatedAt;
        Id = model.ID;
    }

    public string Name { get; set; } = string.Empty;

    public string Bucket { get; set; } = string.Empty;

    [JsonProperty("access_key")]
    public string AccessKey { get; set; } = string.Empty;

    [JsonProperty("secret_key")]
    public string SecretKey { get; set; } = string.Empty;

    [JsonProperty("endpoint_domain")]
    public string EndpointDomain { get; set; } = string.Empty;

    [JsonProperty("endpoint_intranet_domain")]
    public string? EndpointIntranetDomain { get; set; }

    [JsonProperty("domain_bucket_default")]
    public string DomainBucketDefault { get; set; } = string.Empty;

    [JsonProperty("bucket_domain")]
    public string BucketDomain { get; set; } = string.Empty;

    public FileBucket.StorageEnum Type { get; set; } = FileBucket.StorageEnum.Obs;

    public int Id { get; set; }

    public DateTime CreatedAt { get; set; } = DateTime.Now;
    public DateTime UpdatedAt { get; set; } = DateTime.Now;

    public string UpdatedAtTicks => UpdatedAt.Ticks.ToString();
}