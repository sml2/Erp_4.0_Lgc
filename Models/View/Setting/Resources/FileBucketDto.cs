using System.ComponentModel.DataAnnotations;

namespace ERP.Models.View.Setting.Resources;

public class FileBucketDto
{
    public int? Id { get; set; }
    [Required]
    public string Name { get; set; } = string.Empty;
    [Required]
    public string Bucket { get; set; }=string.Empty;

    [NJP("access_key"),Required]
    public string AccessKey { get; set; } = string.Empty;

    [NJP("secret_key"),Required]
    public string SecretKey { get; set; } = string.Empty;

    [NJP("endpoint_domain"),Required]
    public string EndpointDomain { get; set; } = string.Empty;

    [NJP("endpoint_intranet_domain"),Required]
    public string? EndpointIntranetDomain { get; set; } = string.Empty;

    [NJP("bucket_domain"),Required]
    public string BucketDomain { get; set; } = string.Empty;

    public string? UpdatedAtTicks { get; set; }
}