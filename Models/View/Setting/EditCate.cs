using ERP.Models.Stores;

namespace ERP.Models.View.Setting
{
    public class EditCate
    {
        public int ID { get; set; }
        public string? Tempname { get; set; }
        public int Nationid { get; set; }
        public string? Languageid { get; set; }
        public string? Sort { get; set; }
        public CategoryNodeDefault.Levels? Level { get; set; }
        public int Browsernodeid { get; set; }
        public string? Browserui { get; set; }
        public string? Languagecode { get; set; }
        public string? Namehash { get; set; }
        public string? Hash { get; set; }
        public string? Typehash { get; set; }
        public string? Createdat { get; set; }
        public string? TypeData { get; set; }
    }
}
