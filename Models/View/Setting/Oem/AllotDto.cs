namespace ERP.Models.View.Setting.Oem;

public class AllotDto
{
    [NJP("oem_id")]
    public int OemId { get; set; }

    [NJP("username")]
    public string Username { get; set; }
}