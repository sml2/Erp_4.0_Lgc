using ERP.Models.Setting;
using System.ComponentModel.DataAnnotations;

namespace ERP.Models.View.Setting.Oem;

public class OemDataDto
{
    public int? Id { get; set; }
    public int? DatabaseId { get; set; }
    public int? OssId { get; set; }
    public int? RedisId { get; set; }

    public OEM.Styles Style { get; set; }

    // public int State { get; set; }
    [Required]
    public string Detail { get; set; } = string.Empty;

    [Required]
    public string Name { get; set; } = string.Empty;

    [Required]
    public string Title { get; set; } = string.Empty;

    [NJP("urlIcon")]
    public string? UrlIcon { get; set; }

    [NJP("urlLogo")]
    public string? UrlLogo { get; set; }

    [Required]
    public List<Domains> Domains { get; set; } = new();

    public List<string> WaitDeleteImages { get; set; } = new();
    
    [NJP("loginPage")]
    public string LoginPage { get; set; }
}