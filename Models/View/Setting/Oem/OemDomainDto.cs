using System.ComponentModel.DataAnnotations;
using System.Windows.Markup;

namespace ERP.Models.View.Setting.Oem;

public class OemDomainDto
{
    public int OemId { get; set; }
    [Required]
    public List<Domains> Domains { get; set; } = new();
}

public class Domains
{
    public string Value { get; set; }=string.Empty; 
}