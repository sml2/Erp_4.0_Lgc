using Common.Enums.ResourcePackage;

namespace ERP.Models.View.Setting;

public class UpdatePackageDto
{
    public int Id { get; set; }

    public string Name { get; set; }

    public string Desc { get; set; }

    public PackageTypeEnum Type { get; set; }
}