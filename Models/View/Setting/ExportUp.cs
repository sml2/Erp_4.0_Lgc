﻿namespace ERP.Models.View.Setting
{
    public class ExportUp
    {
        public int ID { get; set; }
        public Models.Export.ExportProduct.StateEnum state { get; set; }
        public int platformId { get; set; }
    }

}
