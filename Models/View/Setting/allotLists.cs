﻿using ERP.Models.View.Product;

namespace ERP.Models.View.Setting
{
    public class allotLists : PageDto
    {
        public int exportId { get; set; }
        public int platformId { get; set; }
        [NJP("name")]
        public string? Name { get; set; }
    }
}
