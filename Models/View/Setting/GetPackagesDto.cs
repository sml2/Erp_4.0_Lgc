using Common.Enums.ResourcePackage;

namespace ERP.Models.View.Setting;

public class GetPackagesDto
{
    public PackageTypeEnum? Type { get; set; }
    public string? Name { get; set; }
    
    public int? CompanyId { get; set; }
}