﻿using ERP.Models.View.Product;
using ERP.Models.View.User.User;
using Company = ERP.Models.DB.Users.Company;

namespace ERP.Models.View.Information
{
    public class ValidityCompany:PageDto
    {
        public List<int>? CompanyIds { get; set; }
        public string? name { get; set; }
        public string[]? validity { get; set; }
        public DateTime? Start { get => validity is { Length: 2 } ? DateTime.Parse(validity[0]) : DateTime.MinValue; }
        public DateTime? End { get => validity is { Length: 2 } ? DateTime.Parse(validity[1]) : DateTime.MinValue; }
        public IsAgencyEnum? IsAgency { get; set; }

        public Company.Types? Types { get; set; } = null;
    }

}
