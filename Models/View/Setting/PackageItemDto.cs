using Common.Enums.ResourcePackage;

namespace ERP.Models.View.Setting;

public class PackageItemDto
{
    public int? Id { get; set; }

    public int Num { get; set; }
    
    public int Validity { get; set; }

    public decimal Price { get; set; }
    public PackageTypeEnum Type { get; set; }

    public PackageSellStateEnum State { get; set; } = PackageSellStateEnum.PutOnTheShelf;
    
    public string? UpdatedAtTicks { get; set; }
}