﻿using System.ComponentModel.DataAnnotations;

namespace ERP.Models.View.Setting
{
    public class updateAllotDto
    {
        public int id { get; set; }
        public int platformId { get; set; }
        public int companyId { get; set; }
        public int type { get; set; }
        [Required]
        public string username { get; set; } = string.Empty;

    }
}
