using Common.Enums.ResourcePackage;
using ERP.Models.DB.Packages;

namespace ERP.Models.View.Setting;

public class PackagesVm
{
    public PackagesVm()
    {
    }

    public PackagesVm(PackagesModel package)
    {
        Id = package.ID;
        Name = package.Name;
        Desc = package.Desc;
        Type = package.Type;
    }

    public int Id { get; set; }
    public string Name { get; set; }
    public string? Desc { get; set; }
    public object Items { get; set; }
    public PackageTypeEnum Type { get; set; }
}