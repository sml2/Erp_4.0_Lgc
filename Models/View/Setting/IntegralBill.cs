﻿using ERP.Models.IntegralBalance;

namespace ERP.Models.View.Setting;

public class IntegralBillListDto
{
    public int Id { get; }
    public IntegralBill.Types Type { get; }
    public int Integral { get; }
    public decimal Surplus { get; }
    public string Reason { get; }
    public string Remark { get; }
    public DateTime Datetime { get; }
    public DateTime CreatedAt { get; }

    public IntegralBillListDto(int id, IntegralBill.Types type, int integral, decimal surplus, string? reason, string? remark,
        DateTime datetime, DateTime createdAt)
    {
        Id = id;
        Type = type;
        Integral = integral;
        Surplus = surplus;
        Reason = reason ?? string.Empty;
        Remark = remark ?? string.Empty;
        Datetime = datetime;
        CreatedAt = createdAt;
    }
}

public class BalanceBillListDto
{
    public int Id { get; }
    public BalanceBill.Types Type { get; }
    public decimal Balance { get; }
    public decimal Surplus { get; }
    public string Reason { get; }
    public string Remark { get; }
    public DateTime Datetime { get; }
    public DateTime CreatedAt { get; }
    public string Truename { get; }
    public string Username { get; }
    public string Byusername { get; }
    public string Ext { get; }

    public BalanceBillListDto(int id, BalanceBill.Types type, decimal balance, decimal surplus, string? reason, string? remark, DateTime datetime, DateTime createdAt, string? truename, string? username, string? byusername, string? ext)
    {
        Id = id;
        Type = type;
        Balance = new Money(balance).To("CNY");
        Surplus = new Money(surplus).To("CNY");;
        Reason = reason ?? string.Empty;
        Remark = remark ?? string.Empty;
        Datetime = datetime;
        CreatedAt = createdAt;
        Truename = truename ?? string.Empty;
        Username = username ?? string.Empty;
        Byusername = byusername ?? string.Empty;
        Ext = ext ?? string.Empty;
    }
}