using Common.Enums.ResourcePackage;

namespace ERP.Models.View.Setting;

public class SubPresentDto
{
    public PackageTypeEnum Type { get; set; }

    [NJP("id")]
    public int ItemId { get; set; }
    
    public int CompanyId { get; set; }

    public int Num { get; set; }
}