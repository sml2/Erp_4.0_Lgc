using Common.Enums.ResourcePackage;

namespace ERP.Models.View.Setting;

public class EditMattingDto
{
    public int? Id { get; set; }

    public int Times { get; set; }

    public decimal Price { get; set; }

    public PackageSellStateEnum State { get; set; } = PackageSellStateEnum.PutOnTheShelf;
    
    public string? UpdatedAtTicks { get; set; }
}