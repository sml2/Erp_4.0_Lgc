namespace ERP.Models.View.Setting.Balance;

public class AlipayNotifyDto
{
    public decimal total_amount { get; set; }
    public string buyer_id { get; set; }

    
    public string gmt_create { get; set; }
    public string gmt_payment { get; set; }
    public string notify_time { get; set; }
    public string charset { get; set; }
    public string subject { get; set; }
    public string sign { get; set; }
    public decimal invoice_amount { get; set; }
    public string version { get; set; }
    public string notify_id { get; set; }
    public string fund_bill_list { get; set; }
    public string notify_type { get; set; }
    public string out_trade_no { get; set; }
    public string trade_status { get; set; }
    public string trade_no { get; set; }
    public string auth_app_id { get; set; }
    public decimal receipt_amount { get; set; }
    public decimal point_amount { get; set; }
    public decimal buyer_pay_amount { get; set; }
    public string app_id { get; set; }
    public string sign_type { get; set; }
    public string seller_id { get; set; }
}

public class FundBillIst
{
    public decimal amount { get; set; }
    public string fundChannel { get; set; }
}