namespace ERP.Models.View.Setting.Balance;

public class SubPaymentDto
{
    [NJP("out_trade_no")]
    public string OutTradeNo { get; set; }

    [NJP("describe")]
    public string Describe { get; set; }

    [NJP("total_amount")]
    public decimal TotalAmount { get; set; }
}