namespace ERP.Models.View.Setting.Balance;

public class RechargeDto
{
    public string out_trade_no { get; set; }

    public string describe { get; set; }

    public decimal total_amount { get; set; }
}