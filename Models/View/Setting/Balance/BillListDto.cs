using ERP.Extensions;
using ERP.Models.Finance;

namespace ERP.Models.View.Setting.Balance;

public class BillListDto
{
    public BillListDto()
    {
    }

    public string? Name { get; set; }
    public string? OutTradeNo { get; set; }

    public List<DateTime>? Timespan { get; set; }

    public int? TimeType { get; set; }
    public PayBillTypes? Type { get; set; }
    public int? PayConfigId { get; set; }
    public PayBillStates? State { get; set; }
}