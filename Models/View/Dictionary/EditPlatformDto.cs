using ERP.Models.Abstract;
using ERP.Models.DB.Setting;
using ERP.Models.Setting;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace ERP.ViewModels.Dictionary;

public class EditPlatformDto : CommonDto
{
    public int? ID { get; set; }

    [Comment("允许采集0不允许1允许"), Required(ErrorMessage = "请选择是否允许采集")]
    public bool Collect { get; set; } = true;

    [Comment("英文名称"), Required(ErrorMessage = "请输入英文名称")]
    public string English { get; set; } = string.Empty;

    [Comment("允许导出0不允许1允许"), Required(ErrorMessage = "请选择是否允许导出")]
    public bool Export { get; set; } = true;

    [Comment("平台名称"), Required(ErrorMessage = "请输入平台名称")]
    public new string Name { get; set; } = string.Empty;

    public int Sort { get; set; } = 0;

    [Comment("类型1采集专用2通用3导出专用4私人定制（舍弃）")]
    public Platform.Types Type { get; set; } = Platform.Types.Universal;
    
    public new BaseModel.StateEnum State { get; set; }
}