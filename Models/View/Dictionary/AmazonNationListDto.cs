﻿namespace ERP.ViewModels.Dictionary;

public class AmazonNationListDto : CommonDto
{
    public string? Short { get; set; }
}