﻿using ERP.Models.Abstract;
using ERP.Models.View.Product;

namespace ERP.ViewModels.Dictionary;

public class CommonDto : PageDto
{
    public string? Name { get; set; }

    public BaseModel.StateEnum? State { get; set; }
    
    public DateTime? UpdatedAt { get; set; }
}