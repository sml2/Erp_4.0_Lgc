﻿using System.ComponentModel.DataAnnotations;
using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;

namespace ERP.ViewModels.Dictionary;

public class EditEmailConfigDto : CommonDto
{
    public int? ID { get; set; }

    [Comment("邮箱名称"), Required(ErrorMessage = "请输入邮箱名称")]
    public new string Name { get; set; } = string.Empty;

    [Comment("邮箱域名"), Required(ErrorMessage = "请输入邮箱域名")]
    public string Domain { get; set; } = string.Empty;

    [Comment("pop地址"), Required(ErrorMessage = "请输入POP地址")]
    public string Pop { get; set; } = string.Empty;

    [Comment("smtp地址"), Required(ErrorMessage = "请输入SMTP地址")]
    public string Smtp { get; set; }= string.Empty;

    [Comment("pop端口"), Required(ErrorMessage = "请输入POP端口")]
    public int PopPort { get; set; } 
    [Comment("1启用 2关闭"), Required(ErrorMessage = "请选择PopSSL状态")]
    public BaseModel.StateEnum PopSSL { get; set; } = BaseModel.StateEnum.Open;

    [Comment("smtp端口"), Required(ErrorMessage = "请输入Smtp端口")]
    public int SmtpPort { get; set; } 

    [Comment("1启用 2关闭"), Required(ErrorMessage = "请选择SmtpSSL状态")]
    public BaseModel.StateEnum SmtpSSL { get; set; } = BaseModel.StateEnum.Open;

    [Comment("排序"), Required(ErrorMessage = "请输入排序")]
    public int Sort { get; set; } = 0;

    [Comment("1启用 2禁用"), Required(ErrorMessage = "请选择状态")]
    public new BaseModel.StateEnum State { get; set; }
}