using ERP.Models.DB.Setting;
using ERP.Models.Setting;
using Newtonsoft.Json;

namespace ERP.ViewModels.Dictionary;

public class ExportListDto : CommonDto
{
    public ExportModel.QualityEnum? Quality { get; set; }
    [JsonProperty("platform_id")]
    public int? PlatformId { get; set; } = 0;
}