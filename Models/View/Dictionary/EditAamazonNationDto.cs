﻿using System.ComponentModel.DataAnnotations;
using ERP.Enums;
using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace ERP.ViewModels.Dictionary;

public class EditAmazonNationDto : CommonDto
{
    public int? ID { get; set; }

    //[Comment("名称"), Required(ErrorMessage = "请输入名称")]
    //public string Name { get; set; }

    [Comment("简称"), Required(ErrorMessage = "请输入简称")]
    public string Short { get; set; } = string.Empty;

    [Comment("货币"), Required(ErrorMessage = "请选择货币")]
    public string Unit { get; set; } = string.Empty;

    [Comment("亚马逊 MWS 端点"), Required(ErrorMessage = "请输入亚马逊 MWS 端点")]
    public string Mws { get; set; }=string.Empty;

    [Comment("亚马逊地理位置MarketplaceId"), Required(ErrorMessage = "请输入亚马逊地理位置MarketplaceId")]
    public string Marketplace { get; set; } = string.Empty;

    [Comment("状态1启用2禁用"), Required(ErrorMessage = "请选择状态")]
    public new BaseModel.StateEnum State { get; set; }

    [Comment("店铺域名"), Required(ErrorMessage = "请输入店铺域名")]
    public string Org { get; set; } = string.Empty;

    [Comment("所属大洲"), Required(ErrorMessage = "请选择所属大洲"),JsonProperty("continent_id")]
    public Continents ContinentId { get; set; }

    [Comment("亚马逊Key"), Required(ErrorMessage = "请选择亚马逊Key"), JsonProperty("key_id")]
    public int KeyId { get; set; }

    public string? UpdatedAtTicks { get; set; }
    
    [JsonProperty("nation_id")]
    public int NationId { get; set; }
}