using Newtonsoft.Json;

namespace ERP.ViewModels.Dictionary;

public class AddAllotExportProductDto
{
    [JsonProperty("export_id")]
    public int ExportId { get; set; }
    [JsonProperty("platform_id")]
    public int PlatformId { get; set; }
    [JsonProperty("username")]
    public string Username { get; set; } = string.Empty;
}