using System.ComponentModel.DataAnnotations;
using ERP.Models.Abstract;
using ERP.Models.DB.Setting;
using ERP.Models.Setting;
using Microsoft.EntityFrameworkCore;

namespace ERP.ViewModels.Dictionary;

public class EditExportDto : CommonDto
{
    public int? ID { get; set; }

    [Comment("导出excel表 名称"), Required(ErrorMessage = "请输入表格名称")]
    public new string Name { get; set; } = string.Empty;

    [Comment("导出excel表 标识"), Required(ErrorMessage = "请输入表格标识")]
    public int Sign { get; set; }

    [Comment("导出excel表 vue组件名称"), Required(ErrorMessage = "请输入文件名称")]
    public string Filename { get; set; } = string.Empty;

    [Comment("备注")]
    public string? Remark { get; set; }

    [Required(ErrorMessage = "请选择平台")]
    public int PlatformId { get; set; }

    [Comment("导出类型1产品")]
    public ExportModel.TypeEnum Type { get; set; } = ExportModel.TypeEnum.Porduct;

    [Comment("性质1公共2私人")]
    public ExportModel.QualityEnum Quality { get; set; } = ExportModel.QualityEnum.Public;
    
    public BaseModel.StateEnum State { get; set; }
    
    public string? UpdatedAtTicks { get; set; }
}