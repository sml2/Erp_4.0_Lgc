using ERP.Enums;
using ERP.Models.Abstract;
using ERP.Models;
using Newtonsoft.Json;

namespace ERP.ViewModels.Dictionary;

public class AmazonNationListVm
{
    public AmazonNationListVm(PlatformData m)
    {
        var a = m.GetAmazonData()!;
        
        Id = m.ID;
        Name = a.Name;
        Continent = a.Continent;
        Unit = a.Unit;
        Marketplace = a.Marketplace;
        Mws = a.Host;
        KeyId = a.KeyId;
        State = a.State;
        Short = a.Short;
        Org = a.Org;
        CreatedAt = m.CreatedAt;
        UpdatedAt = m.UpdatedAt;
        UpdatedAtTicks = m.UpdatedAt.Ticks.ToString();
    }

    public int Id { get; set; }
    
    public string Org { get; set; }
    public string Name { get; set; }
    public string Short { get; set; }
    public string UpdatedAtTicks { get; set; }

    [JsonProperty("continent_id")]
    public Continents Continent { get; set; }

    public string? Unit { get; set; }
    public string Marketplace { get; set; }
    public string Mws { get; set; }

    [JsonProperty("key_id")]
    public int KeyId { get; set; }

    public BaseModel.StateEnum State { get; set; }

    [JsonProperty("created_at")]
    public DateTime CreatedAt { get; set; }

    [JsonProperty("updated_at")]
    public DateTime UpdatedAt { get; set; }
    [JsonProperty("nation_id")]
    public int NationId { get; set; }
}