﻿using System.ComponentModel.DataAnnotations;
using ERP.Models.Abstract;
using ERP.Models.Setting;
using Microsoft.EntityFrameworkCore;

namespace ERP.ViewModels.Dictionary;

public class EditAmazonKeyDto : CommonDto
{
    public int? ID { get; set; }
    
    [Required(ErrorMessage = "请输入AwsAccessKey")]
    public string AwsAccessKey { get; set; } = string.Empty;

    [Required(ErrorMessage = "请输入SecretKey")]
    public string SecretKey { get; set; } = string.Empty;

    [Comment("编号"),Required(ErrorMessage = "请输入编号")]
    public string CodeNumber { get; set; } = string.Empty;

    [Comment("名称"),Required(ErrorMessage = "请输入名称")]

    public new string Name { get; set; } = string.Empty;

    [Comment("备注")]
    public string? Remark { get; set; }
    
    [Comment("标题"),Required(ErrorMessage = "请输入标题")]
    public string Title { get; set; } = string.Empty;
}