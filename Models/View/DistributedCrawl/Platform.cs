﻿namespace ERP.ViewModels.DistributedCrawl
{
    /// <summary>
    /// 单个平台配置
    /// </summary>
    public struct Platform
    {
        /// <summary>
        /// 平台名称
        /// </summary>
        public string Name { get; set; }
        public IList<Mode> Modes { get; set; }
    }
}
