﻿namespace ERP.ViewModels.DistributedCrawl
{
    //采集模式
    public struct Mode
    {
        /// <summary>
        /// 模式名称
        /// </summary>
        public string Name {  get; set; }
    }
}
