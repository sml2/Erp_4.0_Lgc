﻿namespace ERP.ViewModels.DistributedCrawl
{
    public struct UIConfig
    {
        /// <summary>
        /// 平台配置
        /// </summary>
        public IList<Platform> Platforms {  get; set;}
        /// <summary>
        /// 平台数量
        /// </summary>
        public int Count { get => Platforms.Count; }
    }
}
