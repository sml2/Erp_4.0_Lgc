using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace ERP.ViewModels.Store.Eanupc;

public class ClearDto
{
    public ModeEnum Mode { get; set; }
}

public enum ModeEnum
{
    None,
    Ean,
    Upc,
    All,
}