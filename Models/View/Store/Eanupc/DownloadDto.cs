namespace ERP.ViewModels.Store.Eanupc;

public class DownloadDto
{
    [NJP("file")]
    public string File { get; set; }
}