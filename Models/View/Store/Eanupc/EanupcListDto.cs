using Newtonsoft.Json;
using ERP.Models.DB.Stores;
using ERP.Models.View.Product;
using ERP.Models.Stores;

namespace ERP.ViewModels.Store.Eanupc;

public class EanupcListDto : PageDto
{
    [JsonProperty("value")]
    public string Value { get; set; } = string.Empty;

    [JsonProperty("select")]
    public EanupcModel.SelectModeEnum Select { get; set; }

    [JsonProperty("type")]
    public EanupcModel.TypeEnum? Type { get; set; }
}