using ERP.Extensions;
using Newtonsoft.Json;
using ERP.Models.DB.Stores;
using ERP.Models.View.Product;
using ERP.Models.Stores;

namespace ERP.ViewModels.Store.Eanupc;

public class EanupcListVM
{
    public EanupcListVM(int ean, int upc, int total, DbSetExtension.PaginateStruct<EanupcModel> list)
    {
        Ean = ean;
        Upc = upc;
        Total = total;
        List = list;
    }

    [JsonProperty("ean")]
    public int Ean { get; set; }

    [JsonProperty("upc")]
    public int Upc { get; set; }

    [JsonProperty("total")]
    public int Total { get; set; }

    [JsonProperty("list")]
    public DbSetExtension.PaginateStruct<EanupcModel> List { get; set; }
}