using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace ERP.ViewModels.Store.Eanupc;

public class EAN
{
    [JsonProperty("ean_code"), Required(ErrorMessage = "请填写国家代码")]
    public string EanCode { get; set; } = string.Empty;

    [JsonProperty("ean_manufacturer"), Required(ErrorMessage = "请填写厂商代码")]
    public string EanManufacturer { get; set; } = string.Empty;

    [JsonProperty("ean_goods"), Required(ErrorMessage = "")]
    public int EanGoods { get; set; }
}