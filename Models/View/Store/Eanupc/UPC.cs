using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace ERP.ViewModels.Store.Eanupc;

public class UPC
{
    [JsonProperty("upc_code"), Required(ErrorMessage = "请选择旗码")]
    public string UpcCode { get; set; } = string.Empty;

    [JsonProperty("upc_manufacturer"), Required(ErrorMessage = "请填写厂商代码")]
    public string UpcManufacturer { get; set; } = string.Empty;

    [JsonProperty("upc_goods"), Required(ErrorMessage = "请填写商品代码")]
    public int UpcGoods { get; set; }
}