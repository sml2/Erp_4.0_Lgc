using ERP.Models.DB.Stores;
using ERP.Models.Stores;

namespace ERP.ViewModels.Store.Eanupc;

public class ImportDto
{
    [NJP("type")]
    public EanupcModel.TypeEnum Type { get; set; }

    public IFormFile File { get; set; }
}