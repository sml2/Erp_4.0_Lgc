using Newtonsoft.Json;

namespace ERP.ViewModels.Store.Eanupc;

public class AddVM
{
    public AddVM(object info, Dictionary<string, Dictionary<string, object>>? codeDictionary, List<object> options)
    {
        Info = info;
        CodeDictionary = codeDictionary;
        Options = options;
    }

    [JsonProperty("data")]
    public object Info { get; set; }
    
    [JsonProperty("enum")]
    public Dictionary<string, Dictionary<string, object>>? CodeDictionary { get; set; }
    
    [JsonProperty("options")]
    public List<Object> Options { get; set; }
}