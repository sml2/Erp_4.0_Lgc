using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace ERP.ViewModels.Store.Eanupc;

public class DestroyDto
{
    [JsonProperty("id"), Required]
    public List<int> Ids { get; set; } = Array.Empty<int>().ToList();
}