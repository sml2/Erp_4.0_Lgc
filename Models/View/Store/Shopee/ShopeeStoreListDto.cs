﻿namespace ERP.ViewModels.Store.Remote;

public class ShopeeStoreListDto : StoreActionDto
{
    public string? Name { get; set; }

    public string? ID { get; set; }

    public string[]? lastordertime { get; set; }

}