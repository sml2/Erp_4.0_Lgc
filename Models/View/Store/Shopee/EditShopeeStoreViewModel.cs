﻿using System.ComponentModel.DataAnnotations;
using ERP.Models.Abstract;
using Newtonsoft.Json;

namespace ERP.ViewModels.Store.Remote;

public class ShopeeAndAmazonSpStoreViewModel
{
    public int? ID { get; set; }

    [JsonProperty("name"), Required(ErrorMessage = "店铺名称必须")]
    public string Name { get; set; } = string.Empty;    

    [JsonProperty("remark")]
    public string? Remark { get; set; }
      
    public BaseModel.StateEnum State { get => BaseModel.StateEnum.Open; }   
    
    public Platforms Platform { set; get; }
}

