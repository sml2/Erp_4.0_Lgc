namespace ERP.ViewModels.Store.Remote;

using ERP.Models.Stores;
using Models.DB.Stores;
using model = Models.Stores.StoreRegion;

public class ShopeeStoreList : StoreRegion
{
    public ShopeeStoreList(model m)
    {
        ID = m.ID;
        Name = m.Name;
        UserName = m.User?.UserName??string.Empty;
        State = m.State;
        IsVerify = m.IsVerify;
        LastOrderTime = m.LastOrderTime;
        CreatedAt = m.CreatedAt;
        UpdatedAt = m.UpdatedAt;
        Sort = m.Sort;
        ShopID = m.Data == null ? "" : m.GetShopeeData()!.ShopID.ToString();
        Creator = m.Creator;
    }

    public string UserName { get; set; }

    public string? ShopID { get; set; }
}