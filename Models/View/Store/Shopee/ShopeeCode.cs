﻿namespace ERP.ViewModels.Store.Remote;

public class ShopeeCode
{
    /// <summary>
    /// 店铺id
    /// </summary>
    public int ID { get; set; }

    /// <summary>
    /// code，有效期10min
    /// </summary>
    public string Code { get; set; } = string.Empty;

    /// <summary>
    /// shopee的shopid
    /// </summary>
    public int ShopId { get; set; }

    /// <summary>
    /// shopee的商户id
    /// </summary>
    public int MainAccountId { get; set; }
}

public class AmazonCode
{
    /// <summary>
    /// 店铺id
    /// </summary>
    public int ID { get; set; }

    /// <summary>
    /// code，有效期10min
    /// </summary>
    public string Code { get; set; } = string.Empty;
    
    public string PartnerId { get; set; }    

    public string State { get; set; } = string.Empty;

    public string OriginString { get; set; } = string.Empty;
}