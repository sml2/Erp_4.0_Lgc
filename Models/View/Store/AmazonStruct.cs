using System.Text.Json.Serialization;
using ERP.Models.Stores;


namespace ERP.Models.View.Store;

public class AmazonStruct
{
    public string SellerID { get; set; } = string.Empty;

    public Chooses Choose { get; set; }
  
    #region  MWS开发者账号
    public string? AwsAccessKeyId { get; set; } = string.Empty;

    public string? MWSSecretKey { get; set; } = string.Empty;
    #endregion

    #region "第三方应用程序"
    public string? MwsAuthToken { get; set; } = string.Empty;
    #endregion

    public string Mws { get; set; } = string.Empty;

   // public string Marketplace { get; set; } = string.Empty;

}