using ERP.Models.View.Product;
using Newtonsoft.Json;

namespace ERP.ViewModels.Store.Remote;

public class RemoteListDto : PageDto
{
    [JsonProperty("name")]
    public string? Name { get; set; }
}