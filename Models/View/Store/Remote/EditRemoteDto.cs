using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace ERP.ViewModels.Store.Remote;

public class EditRemoteDto
{
    [JsonProperty("id")]
    public int ID { get; set; } = 0;

    [JsonProperty("name"), Required(ErrorMessage = "远程名称必须")]
    public string Name { get; set; } = string.Empty;

    [JsonProperty("address"), Required(ErrorMessage = "VPS地址必须")]
    public string Address { get; set; } = string.Empty;

    [JsonProperty("password"), Required(ErrorMessage = "VPS密码必须")]
    public string Password { get; set; } = string.Empty;

    [JsonProperty("user"), Required(ErrorMessage = "VPS用户")]
    public string User { get; set; } = string.Empty;
}