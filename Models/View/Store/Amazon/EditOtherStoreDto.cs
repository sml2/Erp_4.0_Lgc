using ERP.Models.Abstract;
using ERP.Models.DB.Stores;
using ERP.Models.View.Store;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace ERP.ViewModels.Store.Remote;
public class EditOtherStoreDto
{
    [JsonProperty("id")]
    public int ID { get; set; } = 0;

    [JsonProperty("name"), Required(ErrorMessage = "店铺名称必须")]
    public string Name { get; set; } = string.Empty;

    [JsonProperty("data")]
    public string Data { get; set; } = string.Empty;

    [JsonProperty("type")]
    public int Type { get; set; }
    [JsonProperty("state")]
    public BaseModel.StateEnum? State { get; set; }
}