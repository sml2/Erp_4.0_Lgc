using System.ComponentModel.DataAnnotations;
using ERP.Enums;
using ERP.Models.Abstract;
using ERP.Models.Stores;
using ERP.Models.View.Store;
using Newtonsoft.Json;
using static ERP.Models.Stores.StoreRegion;

namespace ERP.ViewModels.Store.Remote;



public class MwsStoreDto
{
    public int? ID { get; set; }

    [JsonProperty("name"), Required(ErrorMessage = "店铺名称必须")]
    public string Name { get; set; } = string.Empty;


    [JsonProperty("version"), Required(ErrorMessage = "平台数据不能为空")]
    public int Version { get { return _version; } set{ _version = value; } }

    private int _version;

    public Platforms platform
    {
        get
        {
            if (_version == 0)
                return Enums.Platforms.AMAZON;
            else if (_version == 1)
                return Enums.Platforms.AMAZONSP;
            else if (_version == 2)
                return Enums.Platforms.SHOPEE;
            throw new AggregateException("平台参数异常");
        }
    }


    private AddModes _addMode;

    private int _nationId;

    private List<int?> _nationList;

    /// <summary>
    /// 添加模式
    /// </summary>
    [JsonProperty("addMode"), Required(ErrorMessage = "添加模式")]
    public AddModes addMode { get { return _addMode; } set { _addMode = value; } }

    public int NationId { get { return _nationId; } set { _nationId = value; } }

    /// <summary>
    /// NationId修正为国家数据所在<see cref="Models.DB.PlatformData"/>的ID 而非国家ID
    /// </summary>
    [JsonProperty("checkList"), Required(ErrorMessage = "国家id必须")]
    public List<int?> NationIds
    {
        get
        {
            if (_addMode == AddModes.Single)
                _nationList= new List<int?>() { _nationId };
            else if (_addMode == AddModes.None)
                throw new AggregateException("addmode 参数错误");
            else if(_nationList.IsNull() || _nationList.Count<=0)
                throw new AggregateException("国家id必须");
            return _nationList;
        }
        set { _nationList = value; }
    }

    [JsonProperty("continents"), Required(ErrorMessage = "地区数据必须")]
    public Continents Continents { get; set; }

    [JsonProperty("state")]
    public BaseModel.StateEnum State { get; set; }

    public string? Remark { get; set; }

    [JsonProperty("data")]
    public AmazonStruct Data { get; set; } = new();
}



public class AddSingleStoreDto
{
    public int? ID { get; set; }

    [JsonProperty("name"), Required(ErrorMessage = "店铺名称必须")]
    public string Name { get; set; } = string.Empty;
  
    /// <summary>
    /// 添加模式
    /// </summary>
    public int addMode { get; set; }

    /// <summary>
    /// NationId修正为国家数据所在<see cref="Models.DB.PlatformData"/>的ID 而非国家ID
    /// </summary>
    [JsonProperty("nationId"), Required(ErrorMessage = "国家id必须")]
    public int NationId { get; set; }


    [JsonProperty("checkList"), Required(ErrorMessage = "请选中国家")]
    public int[] checkList { get; set; } = Array.Empty<int>();
    
    /// <summary>
    /// 是否同步修改
    /// </summary>
    //[JsonProperty("fix")]
    //public Models.DB.Stores.StoreRegion.FixEnum? Fix { get; set; }


    [JsonProperty("data"), Required(ErrorMessage = "data数据必须")]
    public AmazonStruct Data { get; set; } = new();

    public string? Remark { get; set; }
    [JsonProperty("continents"), Required(ErrorMessage = "地区数据必须")]
    public Continents continents { get; set; }
    [JsonProperty("state")]
    public BaseModel.StateEnum State { get; set; }

}

public class EditStoreDto
{
    public int? ID { get; set; }
    //public int checkList { get; set; }

    [JsonProperty("name"), Required(ErrorMessage = "店铺名称必须")]
    public string Name { get; set; } = string.Empty;

    [JsonProperty("nationLsts")]
    public List<int> NationLsts = new List<int>();

    /// <summary>
    /// NationId修正为国家数据所在<see cref="Models.DB.PlatformData"/>的ID 而非国家ID
    /// </summary>
    [JsonProperty("nationId"), Required(ErrorMessage = "国家id必须")]
    public int NationId { get; set; }
   
    
    [JsonProperty("nationName"), Required(ErrorMessage = "国家名称必须")]
    public string NationName { get; set; } = string.Empty;

    [JsonProperty("NationShort"), Required(ErrorMessage = "国家简称必须")]
    public string NationShort { get; set; } = string.Empty;

    [Required(ErrorMessage = "货币名称必须")]
    public string unitName { get; set; } = string.Empty;

    [JsonProperty("unitsign"), Required(ErrorMessage = "货币简称必须")]
    public string UnitSign { get; set; } = string.Empty;

    //[JsonProperty("fix")]
    //public Models.DB.Stores.StoreRegion.FixEnum? Fix { get; set; }

    //[JsonProperty("sellerid"), Required(ErrorMessage = "sellerid必须")]
    //public string SellerId { get; set; }     
    public int addMode { get; set; }
    [JsonProperty("data"), Required(ErrorMessage = "data数据必须")]
    public AmazonStruct Data { get; set; } = new();
    public string? Remark { get; set; }
    [JsonProperty("continents"), Required(ErrorMessage = "地区数据必须")]
    public Continents continents { get; set; }
    [JsonProperty("state")]
    public BaseModel.StateEnum State { get; set; }
    //public int Type { get; set; }
}



public class AddMultiStoreDto
{
    public int? ID { get; set; }
    [JsonProperty("checkList"), Required(ErrorMessage = "请选中国家")]
    public int[] checkList { get; set; } = Array.Empty<int>();

    [JsonProperty("name"), Required(ErrorMessage = "店铺名称必须")]
    public string Name { get; set; } = string.Empty;
    public string NationName { get; set; } = string.Empty;
    public string? NationShort { get; set; }
    public string? UnitName { get; set; }
    public string? UnitSign { get; set; }
    //[JsonProperty("fix")]
    //public Models.DB.Stores.StoreRegion.FixEnum? Fix { get; set; }
    [JsonProperty("data"), Required(ErrorMessage = "data数据必须")]
    public AmazonStruct Data { get; set; } = new();
    public int AddMode { get; set; }
    public string? Remark { get; set; }
    public int continents { get; set; }
    //[JsonProperty("state")]
    //public BaseModel.StateEnum State { get; set; }
    public int Type { get; set; }
}