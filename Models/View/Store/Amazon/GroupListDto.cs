﻿
using Newtonsoft.Json;

namespace ERP.ViewModels.Store.Remote;

public class GroupListDto
{
    [JsonProperty("storeId")]
    public int StoreId { get; set; }
}