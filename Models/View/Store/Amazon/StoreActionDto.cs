
using ERP.Models.View.Product;

namespace ERP.ViewModels.Store.Remote;

public class StoreActionDto : PageDto
{
    public ActionEnum Action {get;set;}
    
}

public enum ActionEnum
{
    Store = 1,
    MyStore
}