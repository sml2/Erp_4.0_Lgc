﻿using ERP.Enums;
using ERP.Models.View.Product;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace ERP.ViewModels.Store.Remote;

public class AmazonStoreListDto : StoreActionDto
{
    [JsonProperty("name")]
    public string? Name { get; set; }
    [JsonProperty("SellerID")]
    public string? SellerID { get; set; }
    [JsonProperty("continentsid")]
    public Continents? ContinentsId { get; set; }
    [JsonProperty("nationId")]
    public int? NationId { get; set; }

    [JsonProperty("version")]
    public List<Platforms>? Platforms { get; set; } = new();

    public string[]? lastordertime { get; set; }
    public string? Start {  set => _=lastordertime?.Length == 0 && lastordertime != null ? lastordertime[0] : null; }
    public string? End {  set => _ =lastordertime?.Length == 0 && lastordertime != null ? lastordertime[1] : null; }
}

public class SetSortDto 
{
     public int Id { get; set; }
     public int Type { get; set; } 
}

public class AmzonAssingDto
{
    public int Id { get; set; }
    [Required]
    public int[] checkList { get; set; } = Array.Empty<int>();
}
