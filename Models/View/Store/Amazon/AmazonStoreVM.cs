
using ERP.Extensions;
using ERP.Models;
using ERP.Models.DB;
using ERP.Models.DB.Users;
using ERP.Models.Setting;
using ERP.Models.Stores;
using Newtonsoft.Json;

namespace ERP.ViewModels.Store.Remote;

public class AmazonStoreVM
{
    public AmazonStoreVM(DbSetExtension.PaginateStruct<StoreRegion> stores,
        Dictionary<int, UserInfoSelectStruct> user, IEnumerable<dynamic> continents,
        Dictionary<int, PlatformData.AmazonDataWithID?> amazonNation, IList<Unit>? unit)
    {
        Stores = stores.Transform(x => new {x.ID,x.Name,x.Creator,x.Data,x.Flag,x.Continent,x.AddMode,x.CreatedAt,x.UpdatedAt,
        x.NextCycleInfo,x.BelongsGroup,x.BelongsGroupID,x.FullCycle,x.UpdatedAtTicks,x.IsVerify,x.OEMID,x.Platform,x.Sort,x.State,x.PullProgress,
        x.UnitName,x.UnitID,x.Unit,x.UnitSign,x.Group,x.GroupID,x.Company,x.CompanyID,x.UserID,x.PullCount,
            x.LastOrderTime,x.PlatformInfo

        });
        User = user;
        Continents = continents;
        AmazonNation = amazonNation;
        Unit = unit;
    }

    [JsonProperty("store")]
    public object Stores { get; set; }

    [JsonProperty("user")]
    public Dictionary<int, UserInfoSelectStruct> User { get; set; }

    [JsonProperty("continents")]
    public IEnumerable<dynamic> Continents { get; set; }

    [JsonProperty("amazonNation")]
    public Dictionary<int, PlatformData.AmazonDataWithID?> AmazonNation { get; set; }

    [JsonProperty("unit")]
    public IList<Unit>? Unit { get; set; }
}