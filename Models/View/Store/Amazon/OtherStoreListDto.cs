﻿using System.Text.Json.Serialization;
using ERP.Models.View.Product;
using Newtonsoft.Json;

namespace ERP.ViewModels.Store.Remote;

public class OtherStoreListDto:StoreActionDto
{
    public string? Name { get; set; }
    public int? Type { get; set; }   
}