
using ERP.Attributes;
using ERP.Enums.Purchase;
using ERP.Extensions;
using Ryu.Extensions;

namespace ERP.Models.View.Purchase;

public class PurchaseListVm
{
    public PurchaseListVm(List<DB.Purchase.Purchase> data)
    { 
        var temp = new List<PurchaseItemVm>();
        foreach (var i in data)
        {
            OrderId ??= i.OrderId;
            OrderNo ??= i.OrderNo;
            StoreName ??= i.StoreName;
            temp.Add(new PurchaseItemVm(i));
        }
        Itmes = temp;
    }

    public int? OrderId { get; set; }
    public string? OrderNo { get; set; }
    public string? StoreName { get; set; }
    public List<PurchaseItemVm> Itmes { get; set; }
}

public class PurchaseItemVm
{
    public PurchaseItemVm(DB.Purchase.Purchase purchase)
    {
        ID = purchase.ID;
        OrderId = purchase.OrderId;
        Url = purchase.Url;
        Name = purchase.Name;
        OrderNo = purchase.OrderNo;
        PurchaseOrderNo = purchase.PurchaseOrderNo ?? string.Empty;
        PurchaseTrackingNumber = purchase.PurchaseTrackingNumber ?? string.Empty;
        Num = purchase.Num;
        PurchaseState = purchase.PurchaseState;
        PurchaseAffirm = purchase.PurchaseAffirm;
        PurchaseStateText = Extensions.EnumExtension.GetDescription(purchase.PurchaseState);
        PurchaseAffirmText = Extensions.EnumExtension.GetDescription(purchase.PurchaseAffirm);
        StoreName = purchase.StoreName;
        Remark = purchase.Remark;
        PurchaseCompanyId = purchase.PurchaseCompanyId;
        CompanyID = purchase.CompanyID;
        CreatedAt = purchase.CreatedAt;
        PurchasePrice = purchase.PurchasePrice.Money;
        // IsAffirm = purchase.IsAffirm;
        OperateCompanyId = purchase.OperateCompanyId;
        PurchaseUrl = purchase.PurchaseUrl;
        FromUrl = purchase.FromUrl;
        OrderGoodId = purchase.OrderGoodId;
        SellerSku = purchase.SellerSku;
        PurchaseTotal = purchase.PurchaseTotal.Money;
        ProductTotal = purchase.ProductTotal.Money;
        PurchasePrice = purchase.PurchasePrice.Money;
        FindTruename = purchase.FindTruename;
        Remark = purchase.Remark;
        Price = purchase.Price.Money;
    }

    public string? SellerSku { get; set; }

    [JsonMoneyForDefaultUnit()]
    public Money Price { get; set; }

    public string? FindTruename { get; set; }

    [JsonMoneyForDefaultUnit()]
    public Money ProductTotal { get; set; }

    [JsonMoneyForDefaultUnit()]
    public Money PurchaseTotal { get; set; }

    public string? OrderGoodId { get; set; }

    public string? FromUrl { get; set; }

    public string? PurchaseUrl { get; set; }

    public int ID { get; set; }
    public int? OrderId { get; set; }
    public string? Url { get; set; }
    public string? Name { get; set; }
    public string? OrderNo { get; set; }
    public string PurchaseOrderNo { get; set; }
    public string PurchaseTrackingNumber { get; set; }
    public int? Num { get; set; }
    public PurchaseStates PurchaseState { get; set; }
    public PurchaseAffirms PurchaseAffirm { get; set; }
    public string PurchaseStateText { get; set; }
    public string PurchaseAffirmText { get; set; }
    public string? StoreName { get; set; }
    public string? Remark { get; set; }
    public int? PurchaseCompanyId { get; set; }
    public int CompanyID { get; set; }
    public DateTime CreatedAt { get; set; }
    [JsonMoneyForDefaultUnit()]
    public Money PurchasePrice { get; set; }
    // public int IsAffirm { get; set; }
    public int? OperateCompanyId { get; set; }
}