﻿namespace ERP.ViewModels.Purchase
{
    public class BatchPurchaseVM
    {
        public List<int>? OrderIds { get; set; } 
        public List<int>? PurchaseIds { get; set; }
        public int? Type { get; set; }

    }
}
