﻿using System.ComponentModel.DataAnnotations;
using ERP.Attributes;
using ERP.Enums.Purchase;
using Newtonsoft.Json;

namespace ERP.Models.View.Purchase;

public class DistributionPurchaseListDto
{
    public int Id { get; set; }
    [Required]
    public string Url { get; set; } = string.Empty;
    [Required]
    public string Name { get; set; } = string.Empty;
    [Required]
    public string FromUrl { get; set; } = string.Empty;
    [Required]
    public string OrderNo { get; set; } = string.Empty;
    public int Num { get; set; }
    [Required]
    public string Unit { get; set; } = string.Empty;
    public int CompanyId { get; set; }
    public DateTime CreatedAt { get; set; }
    public int PurchaseState { get; set; }
    public int OrderId { get; set; }
    [Required]
    public string Price { get; set; } = string.Empty;
    [Required]
    public string PurchasePrice { get; set; } = string.Empty;
    [Required]
    public string PurchaseTotal { get; set; }=string.Empty;
    public int OperateCompanyId { get; set; }
}

public class DistributionPurchaseJuniorListDto
{
    public int Id { get; }

    public string Url { get; }

    public string Name { get; }

    [JsonProperty("from_url")]public string FromUrl { get; }
    [JsonProperty("order_no")]public string OrderNo { get; }

    public int Num { get; }

    [JsonMoneyForDefaultUnit]public Money Price { get; }

    public string Unit { get; }

    [JsonProperty("store_id")]public int? StoreId { get; }

    [JsonProperty("created_at")]public DateTime CreatedAt { get; }

    [JsonProperty("purchase_state")]public PurchaseStates PurchaseState { get; }

    [JsonProperty("order_id")]public int? OrderId { get; }

    [JsonProperty("operate_company_id")]public int? OperateCompanyId { get; }

    public DistributionPurchaseJuniorListDto(int id, string? url, string? name, string? fromUrl, string? orderNo, int? num, MoneyRecordFinancialAffairs price, string? unit, int? storeId, DateTime createdAt, PurchaseStates purchaseState, int? orderId, int? operateCompanyId)
    {
        Id = id;
        Url = url ?? string.Empty;
        Name = name ?? string.Empty;
        FromUrl = fromUrl ?? string.Empty;
        OrderNo = orderNo ?? string.Empty;
        Num = num ?? 0;
        Price = price.Money;
        Unit = unit ?? string.Empty;
        StoreId = storeId;
        CreatedAt = createdAt;
        PurchaseState = purchaseState;
        OrderId = orderId;
        OperateCompanyId = operateCompanyId;
    }
}


public class DistributionPurchaseForOrderInfo
{
    public int Id { get; }

    public string? Url { get; }

    public string? Name { get; }

    [JsonProperty("from_url")] string? FromUrl { get; }

    [JsonProperty("order_no")]public string? OrderNo { get; }

    public int? Num { get; }

    [JsonMoneyForDefaultUnit]public Money Price { get; }

    public string? Unit { get; }

    [JsonProperty("company_id")]public int CompanyId { get; }

    [JsonProperty("created_at")]public DateTime CreatedAt { get; }

    [JsonProperty("purchase_state")]public PurchaseStates PurchaseState { get; }

    [JsonProperty("order_id")]public int? OrderId { get; }

    [JsonProperty("purchase_price"), JsonMoneyForDefaultUnit]public Money PurchasePrice { get; }

    [JsonProperty("purchase_total"), JsonMoneyForDefaultUnit]public Money PurchaseTotal { get; }

    [JsonProperty("purchase_tracking_number")]public string? PurchaseTrackingNumber { get; }

    [JsonProperty("seller_sku")]public string? SellerSku { get; }

    [JsonProperty("purchase_order_no")]public string? PurchaseOrderNo { get; }

    [JsonProperty("operate_company_id")]public int? OperateCompanyId { get; }

    [JsonProperty("purchase_other"), JsonMoneyForDefaultUnit]public Money PurchaseOther { get; }

    public DistributionPurchaseForOrderInfo(int id, string? url, string? name, string? fromUrl, string? orderNo, int? num, MoneyRecordFinancialAffairs price, string? unit, int companyId, DateTime createdAt, PurchaseStates purchaseState, int? orderId, MoneyRecordFinancialAffairs purchasePrice, MoneyRecordFinancialAffairs purchaseTotal, string? purchaseTrackingNumber, string? sellerSku, string? purchaseOrderNo, int? operateCompanyId, MoneyRecordFinancialAffairs purchaseOther)
    {
        Id = id;
        Url = url;
        Name = name;
        FromUrl = fromUrl;
        OrderNo = orderNo;
        Num = num;
        Price = price.Money;
        Unit = unit;
        CompanyId = companyId;
        CreatedAt = createdAt;
        PurchaseState = purchaseState;
        OrderId = orderId;
        PurchasePrice = purchasePrice.Money;
        PurchaseTotal = purchaseTotal.Money;
        PurchaseTrackingNumber = purchaseTrackingNumber;
        SellerSku = sellerSku;
        PurchaseOrderNo = purchaseOrderNo;
        OperateCompanyId = operateCompanyId;
        PurchaseOther = purchaseOther.Money;
    }
}