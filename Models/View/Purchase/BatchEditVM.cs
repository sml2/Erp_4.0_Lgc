﻿using System.ComponentModel.DataAnnotations;

namespace ERP.ViewModels.Purchase
{
    public class BatchEditVM
    {
        public int Id { get; set; }
        [Required]
        public string PurchaseOrderNo { get; set; } = string.Empty;
        [Required]
        public string PurchaseTrackingNumber { get; set; } = string.Empty;
    }
}
