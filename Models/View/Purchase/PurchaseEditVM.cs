﻿using ERP.Enums.Orders;
using ERP.Enums.Purchase;

namespace ERP.ViewModels.Purchase
{
    public class PurchaseEditVM
    {
        public int ID { get; set; }
        public int? OrderId { get; set; }

        public string? GoodId { get; set; }
        //public string? OrderGoodId { get; set; }
        public string? Asin { get; set; }

        public string? FromUrl { get; set; }
        public bool? IsStock { get; set; }
        public string? Name { get; set; }
        public int Num { get; set; }
        public string? OrderNo { get; set; }

        public string? PlatForm { get; set; }
        public int? PlatFormId { get; set; }

        public decimal Price { get; set; }
        public decimal PurchasePrice { get; set; }
        public int? ProductId { get; set; }
        public PurchaseAffirms PurchaseAffirm { get; set; } = PurchaseAffirms.CONFIRMED;
        public decimal PurchaseOther { get; set; }
        public PurchaseStates PurchaseState { get; set; }
        public decimal PurchaseTotal { get; set; }
        public decimal ProductTotal { get; set; }
        public string PurchaseUnit { get; set; } = "CNY";
        public string? PurchaseUrl { get; set; }
        public string Remark { get; set; } = string.Empty;
        public string? SellerSku { get; set; }
        public int? StoreId { get; set; }
        public List<int>? Tags { get; set; }
        public string? Unit { get; set; }
        public string? Url { get; set; }
        public int? VariantId { get; set; }
        public int? WarehouseId { get; set; }
        public string? WarehouseName { get; set; }

        private string? _purchaseTrackingNumber;
        public string? PurchaseTrackingNumber
        {
            get { return _purchaseTrackingNumber; }
            set
            {
                if (value is not null) { _purchaseTrackingNumber = value.Trim(); } else { _purchaseTrackingNumber = ""; }
            }
        }
        public decimal PurchaseFee { get; set; }

        private string? _purchaseOrderNo;
        public string? PurchaseOrderNo
        {
            get { return _purchaseOrderNo; }
            set
            {               
                if (value is not null) { _purchaseOrderNo = value.Trim(); }
                else { _purchaseOrderNo = ""; }
            }
        }
        public int? OrderItemId { get; set; }

        public PurchaseRateTypes? PurchaseRateType { get; set; }
    }
}
