﻿using ERP.Enums.Purchase;

namespace ERP.Models.View.Purchase;

public class IndexVM
{
    public int? WarehouseId { get; set; }
    public PurchaseStates? PurchaseState { get; set; }
    public PurchaseAffirms? PurchaseAffirm { get; set; }
    public string? OrderNo { get; set; }
    public string? PurchaseOrderNo { get; set; }
    public string? PurchaseTrackingNumber { get; set; }
    public string? Express { get; set; }
    //"卖家店铺id（平台-商店）"
    public int StoreId { get; set; }
}