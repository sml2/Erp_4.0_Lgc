using System.ComponentModel.DataAnnotations;
using ERP.Enums;

namespace ERP.Models.View.Products.Export;

public class CheckExportProductLanguageDto
{
    [NJP("eid")]
    public int Eid { get; set; }

    [NJP("language")]
    public Languages Language { get; set; }

    [NJP("language_id")]
    public int LanguageId { get; set; }

    [NJP("sid")]
    public int Sid { get; set; }
}

public record UpdateExportProductLanguageDto([Required]int Eid, [Required]Languages Language, [Required]int LanguageId, [Required]int Sid);