﻿using ERP.Models.View.Product;

namespace ERP.Models.View.Products.Export;
public record Temp(bool Code, ERP.Models.Export.SyncExport Model, List<int> Ids, Models.Export.ExcleRole Role, string CodeInfo, string FileName);
public class GetExports:PageDto
{
    public string Action { get; set; }
    public string audit_id { get; set; }
    public string category_id { get; set; }
    public string isRepeat { get; set; }
    public string name { get; set; }
    public string pid { get; set; }
    public string platform_id { get; set; }
    public string state { get; set; }
    public string task_id { get; set; }
    public string type { get; set; }
}
public class ExportQuery :PageDto
{
    [NJP("name")]
    public string? Name { get; set; }
    [NJP("export_sign")]
    public int? ExportSign { get; set; }
    [NJP("language")]
    public int? Language { get; set; }
    public int? Page { get; set; }
    public int? Limit { get; set; }
}

