using ERP.Models.DB.Product;
using ERP.Models.Product;

namespace ERP.Models.View.Products.Export;

public class CopyProductViewModel
{
    public int SourceId { get; set; }
    public int SourceItemId { get; set; }
    public ProductModel Product { get; set; }

    public int NewProductId
    {
        get => Product.ID;
    }

    public ProductItemModel ProductItem { get; set; }

    public int NewProductItem
    {
        get => ProductItem.ID;
    }
}