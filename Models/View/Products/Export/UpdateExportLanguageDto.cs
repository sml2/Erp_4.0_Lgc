using ERP.Enums;

namespace ERP.Models.View.Products.Export;

public class UpdateExportLanguageDto
{
    public int Id { get; set; }
    
    public Languages Language { get; set; }

    [NJP("language_id")]
    public int LanguageId { get; set; }
    
    public string? UpdateUnit { get; set; }
}