using NUnit.Framework;

namespace ERP.Models.View.Products.Export;

public class AllotExportProductDto : EidDto
{
    [NJP("cover")]
    public bool Cover { get; set; }

    [NJP("sid")]
    public int Sid { get; set; }

    [NJP("mode")]
    public ModeEnum Mode { get; set; }

    public enum ModeEnum
    {
        EAN = 1,
        UPC = 2
    }
}