namespace ERP.Models.View.Products.Export;

public class EditExportDto 
{
    public int Id { get; set; }
    
    public string Name { get; set; }
    public bool RemoveDescriptionHtml { get; set; }
}