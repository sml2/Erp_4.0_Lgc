﻿using ERP.Models.DB.Identity;

namespace ERP.Models.View.Products.Export;

public class JoinExportDto
{
    public string spid { get; set; }
    public int source_id { get; set; }
    public int eid { get; set; }
    public ProductConfig ProductConfig { get; set; }
    
}