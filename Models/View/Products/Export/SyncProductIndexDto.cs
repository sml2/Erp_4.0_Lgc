using ERP.Models.DB.Stores;
using ERP.Models.Stores;
using ERP.Models.View.Product;

namespace ERP.Models.View.Products.Export;

public class SyncProductIndexDto : PageDto
{
    [NJP("product_name")]
    public string? ProductName { get; set; }

    [NJP("sku")]
    public string? Sku { get; set; }

    [NJP("type")]
    public SyncProductModel.ProductTypes? Type { get; set; }
    
    public int[]? Pids { get; set; }

    public enum TypeEnum
    {
        /// <summary>
        /// 仅上传
        /// </summary>
        UploadOnly = 1,

        /// <summary>
        /// 仅导出
        /// </summary>
        ExportOnly = 2,

        /// <summary>
        /// 上传导出
        /// </summary>
        UploadExport = 3
    }
}