using ERP.Enums;
using ERP.Export.ExportCommon;
using ERP.Export.Templates;
using ERP.Extensions;
using ERP.Models.DB.Setting;
using ERP.Models.Export;

namespace ERP.Models.View.Products.Export;

public class AddSyncExportDto
{
    [NJP("amount")]
    public int Amount { get; set; }

    [NJP("name")]
    public string Name { get; set; }

    [NJP("export_sign")]
    public TemplateType Format { get; set; }

    [NJP("param")]
    public AddSyncExportParamDto Params { get; set; }

    [NJP("append")]
    public CommonParameter Append { get; set; }

    [NJP("platform_id")]
    public int PlatformId { get; set; }
    public int? RelationId { get; set; }

    public class AddSyncExportParamDto : ExcleRole
    {
        public int? ChangePriceTemplateId { get; set; }
        public bool? RemoveDescriptionHtml { get; set; }
        // public bool DescriptionAppendImage { get; set; }
        // public int DescriptionMode { get; set; }
        // public int KeywordMode { get; set; }
        // public int MainProImgSetting { get; set; }
        // public int SketchMode { get; set; }
        //
        // [NJP("language")]
        // public string Language { get; set; }
    }
}