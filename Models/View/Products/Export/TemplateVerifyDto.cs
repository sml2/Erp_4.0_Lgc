using ERP.Export.Templates;

namespace ERP.Models.View.Products.Export;

public class TemplateVerifyDto
{
    public string Append { get; set; }
    
    public string RoleJson { get; set; }
    public TemplateType Format { get; set; }
}