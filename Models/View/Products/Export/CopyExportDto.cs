using ERP.Export.Templates;
using ERP.Models.DB.Identity;
using ERP.Models.Export;

namespace ERP.Models.View.Products.Export;

public class CopyExportDto
{
    [NJP("export_sign")]
    public TemplateType Format { get; set; }

    [NJP("id")]
    public int ProductId { get; set; }

    [NJP("tasks_id")]
    public int TaskId { get; set; }

    [NJP("platform_id")]
    public int PlatformId { get; set; }

    [NJP("productConfig")]
    public ProductConfig ProductConfig { get; set; }

    [NJP("param")]
    public ExcleRole ExcelRole { get; set; }
}