namespace ERP.Models.View.Products.Export;

public class DeleteExportProductDto
{
    public int Eid { get; set; }
    public int Sid { get; set; }
}