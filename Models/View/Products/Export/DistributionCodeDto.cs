namespace ERP.Models.View.Products.Export;

public class DistributionCodeDto
{
    public int Id { get; set; }
    
    public string Sku { get; set; }

    [NJP("cover")]
    public bool Cover { get; set; }

    [NJP("mode")]
    public AllotExportProductDto.ModeEnum Mode { get; set; }
}