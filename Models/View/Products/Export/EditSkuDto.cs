using System.ComponentModel.DataAnnotations;
using ERP.Models.DB.Stores;
using ERP.Models.Stores;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace ERP.Models.View.Products.Export;

public class EditSkuDto
{
    [Required]
    public int Id { get; set; }
    
    [ValidateNever]
    public string Sku { get; set; }
    
    public RequestType Type { get; set; }

    public enum RequestType
    {
        /// <summary>
        /// 修改单个产品
        /// </summary>
        Single,
        /// <summary>
        /// 批量操作
        /// </summary>
        Batch = 2
    }

    [NJP("sku_suffix")]
    public bool _SkuSuffix { get; set; }

    public SyncProductModel.SkuSuffixs SkuSuffix =>
        _SkuSuffix ? SyncProductModel.SkuSuffixs.TRUE : SyncProductModel.SkuSuffixs.FALSE;


    [NJP("sku_type")]

    public bool _SkuType { get; set; }

    public SyncProductModel.SkuTypes SkuType =>
        _SkuType ? SyncProductModel.SkuTypes.TYPE_TRUE : SyncProductModel.SkuTypes.FALSE;

    [NJP("title_suffix")]
    public bool _TitleSuffix { get; set; }

    public SyncProductModel.TitleSuffixs TitleSuffix =>
        _TitleSuffix ? SyncProductModel.TitleSuffixs.TRUE : SyncProductModel.TitleSuffixs.FALSE;
}