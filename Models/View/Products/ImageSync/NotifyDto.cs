namespace ERP.Models.View.Product.ImageSync;

public class NotifyDto
{
    public int Id { get; set; }
    public bool IsConvert { get; set; } = true;
    
    public List<int>? MainImages { get; set; }
}