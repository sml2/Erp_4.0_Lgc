namespace ERP.Models.View.Product.ImageSync;

public class UploadDto
{
    public int Id { get; set; }
    public string? Base64Data { get; set; }
    public string? Extension { get; set; }
    public bool IsConvert { get; set; } = true;
}