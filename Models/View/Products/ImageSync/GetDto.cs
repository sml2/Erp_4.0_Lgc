using ERP.Enums;

namespace ERP.Models.View.Product.ImageSync;

public class GetDto
{
    public int Id { get; set; }
    public bool IsConvert { get; set; } = true;
}