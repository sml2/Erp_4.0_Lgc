using Common.Html;
using NPOI.SS.Formula.PTG;
using Ryu.String;
using Attribute = ERP.Data.Products.Attribute;

namespace ERP.Models.View.Product.Translation;

public class ClientTranVm : SrcVm
{
    public List<Attribute> Attribute { get; set; }

    public ClientTranVm(ERP.Data.Products.Product itemStruct, int pid)
    {
        ID = pid;
        Name = itemStruct.GetTitle()!;
        Keywords = itemStruct.GetKeyword();
        Sketch = itemStruct.GetSketches();
        var desc = itemStruct.GetDescription();
        Desc = desc is null ? null : HtmlParser.Parse(desc).GetContent(new GetContentSetting(){Unescape = true, RemoveWhiteSpaceEndLines = true, RemoveWhiteSpaceStartLines = true});
        Attribute = itemStruct.Variants.Attributes.items.Values.ToList();
    }

    public ClientTranVm() : base()
    {
    }
}