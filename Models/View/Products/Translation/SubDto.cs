using ERP.Enums;

namespace ERP.Models.View.Product.Translation;

public class SubDto : GetDto
{
    [NJP("pid")]
    public int Pid { get; set; }

    [NJP("form")]
    public Languages Form { get; set; }

    public int FormId
    {
        get => Form == Languages.Default ? 0 : (int)Math.Log2((long)Form) + 1;
    }

    [NJP("to")]
    public Languages To { get; set; }

    public int ToId
    {
        get => To == Languages.Default ? 0 : (int)Math.Log2((long)To) + 1;
    }

    [NJP("attribute")]
    public bool Attribute { get; set; }

    [NJP("variant")]
    public bool Variant { get; set; }
}