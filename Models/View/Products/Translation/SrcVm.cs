using Newtonsoft.Json;
using Attribute = ERP.Data.Products.Attribute;

namespace ERP.Models.View.Product.Translation;

public class SrcVm
{
    public int ID { get; set; }
    public string Name { get; set; }
    public string? Keywords { get; set; }
    public List<string>? Sketch { get; set; }

    public string? SketchStr
    {
        get => Sketch != null ? JsonConvert.SerializeObject(Sketch) : null;
    }

    public string? GetSketchStr(string concat) => Sketch != null ? string.Join(concat, Sketch) : null;

    public string? Desc { get; set; }

    public SrcVm()
    {
    }
}