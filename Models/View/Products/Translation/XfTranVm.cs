using Newtonsoft.Json;

namespace ERP.Models.View.Product.Translation;

public class XfTranVm : SrcVm
{
    public List<string> Attributes { get; set; }


    public string GetAttributesStr(string concat) => string.Join(concat, Attributes);


    public XfTranVm(ERP.Data.Products.Product itemStruct, int pid, bool attribute)
    {
        ID = pid;
        Name = itemStruct.GetTitle()!;
        Keywords = itemStruct.GetKeyword();
        Sketch = itemStruct.GetSketches();
        Desc = itemStruct.GetDescription();
        var attrs = new List<string>();
        // if (attribute)
        // {
        var attr = itemStruct.Variants.Attributes.items.Values.ToList();
        foreach (var i in attr)
        {
            attrs.Add(i.Name);
            foreach (var v in i.Value)
            {
                attrs.Add(v);
            }
        }

        Attributes = attrs;
        // }
    }
}