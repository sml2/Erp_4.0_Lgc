﻿namespace ERP.Models.View.Product.IllegalWord;

public class ListDto : PageDto
{
    public string? Value { get; set; }

    public int? UserId { get; set; }

    public int Select { get; set; } = 1;
}