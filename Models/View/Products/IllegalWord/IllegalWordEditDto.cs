﻿using System.ComponentModel.DataAnnotations;
using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;

namespace ERP.ViewModels.IllegalWord;

public class IllegalWordEditDto
{
    public int? ID { get; set; } = 0;
    
    [Comment("违禁词内容"), Required(ErrorMessage = "请填写违禁词内容")]
    [StringLength(255, ErrorMessage = "违禁词内容最大长度不可超过255")]
    public string Value { get; set; } = string.Empty;
}