using ERP.ViewModels;

namespace ERP.Models.View.Product.Audit;

public class SetDto : IdDto
{
    [NJP("audit")]
    public int? Audit { get; set; }

    [NJP("category")]
    public string? Category { get; set; }

    [NJP("mode")]
    public int Mode { get; set; }

    [NJP("reason")]
    public string? Reason { get; set; }
}