namespace ERP.Models.View.Product;

public abstract class PageDto
{
    public  int Page { get; set; } = 1;

    public  int Limit { get; set; } = 20;
}