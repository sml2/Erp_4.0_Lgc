﻿using Aop.Api.Domain;
using ERP.Models.DB.Product;
using ERP.Models.Product;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;


namespace ERP.Models.View.Products.SmartHijacking
{

    public class AsinCls
    {
        [JsonProperty("asins")]
        public List<string> asins { get; set; }

        [JsonProperty("store_id")]
        public int storeID { get; set; }

        [JsonProperty("marketplace")]
        public string marketPlaceID { get; set; }
    }

    public class CreateUploadAsinsVM: AsinCls
    {
              
    }

    public class UpdateAsinsVM 
    {        
        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("stock")]
        public int Stock { get; set; }

        [JsonProperty("stockdays")]
        public int Stockdays { get; set; }

        [JsonProperty("product_state")]
        public string productState { get; set; }

        [JsonProperty("ids")]
        public List<int> ids { get; set; }
        
    }   

    public class BatchAsinsVM : ParamIDs
    {
        [JsonProperty("type")]
        public  string actionType { get; set; }
    }

    public enum ActionType
    {
        Node=0,        
        Delete,
        PUtOn,
        TakeOff,
    }

    public class ParamIDs
    {
        public List<int> Ids { get; set; }
    }

    public class HijackingVM
    {

        [JsonProperty("asin")]
        public string? asins { get; set; }

        [JsonProperty("store_id")]
        public int? storeID { get; set; }

        [JsonProperty("marketplace")]
        public string? marketPlaceID { get; set; }

        [JsonProperty("productName")]
        public string? ProductName { get; set; }      

        [JsonProperty("state")]
        public int? State { get; set; }

        [JsonProperty("Limit")]
        public int Limit { get; set; }

        [JsonProperty("Page")]
        public int Page { get; set; }

        [JsonProperty("Total")]
        public int Total { get; set; }

        public HijackingResults? productResult { get; set; }
    }
}



