using ERP.Attributes;

namespace ERP.Models.View.Products.Product;

public class PriceViewModel
{
    [NJP("max")]
    public string Max { get; set; }
    [NJP("min")]
    public string Min { get; set; }
}