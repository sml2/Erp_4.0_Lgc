using ERP.Models.DB.Product;
using ERP.Models.Product;

namespace ERP.Models.View.Products.Product;

public class ProductHistoryViewModel : ProductViewModel
{
    public ProductHistoryViewModel()
    {
    }

    public ProductHistoryViewModel(ProductModel productModel, ProductItemModel itemModel,
        Data.Products.Product productStruct, string unit) : base(productModel, itemModel, productStruct,unit)
    {
    }

    [NJP("languagePacks")]
    public new Dictionary<string, Packs>? LanguagePacks { get; set; }
}