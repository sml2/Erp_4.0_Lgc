using ERP.ControllersWithoutToken.Export;
using ERP.Data.Products.Expand;
using ERP.Enums;
using Newtonsoft.Json;
using ThirdParty.Json.LitJson;

namespace ERP.Models.View.Products.Product;

public class HistoryViewDto
{
    [NJP("id")]
    public int Id { get; set; }
    
    [NJP("item_id")]
    public int ItemId { get; set; }
    
    [NJP("version")]
    public long Version { get; set; }
}
