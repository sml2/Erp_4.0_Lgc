using ERP.Data.Products;
using NPOI.SS.Formula.Functions;
using Ryu.Extensions;

namespace ERP.Models.View.Products.Product;

public class PricesViewModel
{
    public PricesViewModel(Prices m,string unit)
    {
        Cost.Max = m.Cost.GetMax(unit).ToFixed();
        Cost.Min = m.Cost.GetMin(unit).ToFixed();
        Sale.Min = m.Sale.GetMin(unit).ToFixed();
        Sale.Max = m.Sale.GetMax(unit).ToFixed();
    }

    [NJP("cost")]
    public PriceViewModel Cost { get; set; } = new PriceViewModel();
    [NJP("sale")]
    public PriceViewModel Sale { get; set; } = new PriceViewModel();
}