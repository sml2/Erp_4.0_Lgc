using ERP.ControllersWithoutToken.Export;
using ERP.Data.Products.Expand;
using ERP.Enums;
using Newtonsoft.Json;
using ThirdParty.Json.LitJson;

namespace ERP.Models.View.Products.Product;

public class SyncNetworkImageDto
{
    [NJP("id")]
    public int Id { get; set; }

    [NJP("img_id")]
    public int ImgId { get; set; }
}