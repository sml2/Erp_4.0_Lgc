namespace ERP.Models.View.Products.Product.ImageTran;

public class ImageTranslateViewModel
{
    public string Message { get; set; }
    public string RequestId { get; set; }
    public int Code { get; set; }
    public TranData? Data { get; set; }
}

public class TranData
{
    public string Url { get; set; }
    public string SslUrl { get; set; }
}