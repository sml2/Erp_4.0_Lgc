using Common.Enums.Products.ImageTran;

namespace ERP.Models.View.Products.Product.ImageTran;

public class GoTranDto : BaseTranDto
{
    public string Path { get; set; }
}