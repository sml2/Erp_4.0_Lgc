using Common.Enums.Products.ImageTran;

namespace ERP.Models.View.Products.Product.ImageTran;

public class BaseTranDto
{
    public SourceLanguageEnum SourceLanguage { get; set; }
    
    public TargetLanguageEnum TargetLanguage { get; set; }
}