namespace ERP.Models.View.Products.Product.ImageTran;

public class TranNotifyDto
{
    public int Pid { get; set; }
    public string Title { get; set; }
    public List<ImageTranDto> Images { get; set; }
}