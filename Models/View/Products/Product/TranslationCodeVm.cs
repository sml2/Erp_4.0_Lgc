using ERP.Enums;
using ERP.Extensions;

namespace ERP.Models.View.Products.Product;

public class TranslationCodeVm
{
    public TranslationCodeVm()
    {
    }

    public TranslationCodeVm(Languages m)
    {
        Name = m.GetDescriptionByKey("Name");
        Code = m.GetDescriptionByKey("Code");
        TranslationCode = m.GetTranslationCode(Code);
    }

    public string Name { get; set; }
    public string Code { get; set; }
    public string TranslationCode { get; set; }
}