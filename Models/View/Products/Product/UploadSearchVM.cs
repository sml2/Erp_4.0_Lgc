using ERP.Enums;
using ERP.Models.View.Product;
using Newtonsoft.Json;

namespace ERP.Models.View.Products.Product
{
    public class UploadSearchVM : PageDto
    {
        [JsonProperty("productName")]
        public string ProductName { get; set; }

        [JsonProperty("storeID")]
        public int StoreID { get; set; }

        [JsonProperty("languageSign")]
        public string? LanguageSign { get; set; }

        public Languages? lang
        {
            get
            {

                if (!string.IsNullOrWhiteSpace(LanguageSign))
                { return Enum.Parse<Languages>(LanguageSign, true); }
                else
                {
                    return null;
                }
            }
        }

        [JsonProperty("state")]
        public int StateValue { get; set; }


        [JsonProperty("marketplace")]
        public string Marketplace { get; set; }


        [JsonProperty("taskID")]
        public int TaskID { get; set; }

    }

    public class SearchJsonVM  //: PageDto
    {
        [JsonProperty("pid")]
        public int Pid { get; set; }
    }   
    

    public class VMPids
    {
        [JsonProperty("ids")]
        public  List<int> Pids { get; set; }
    }
}
