using ERP.Data.Products;
using ERP.Enums.Product;

namespace ERP.Models.View.Products.Product;

public class SkuUploadInfoVm
{
    public SkuUploadInfoVm(Variant variant)
    {
        Sku = variant.Sku!;
        Code = variant.Code;
        Asin = variant.Asin;
        Sid = variant.Sid;
        UpdateState = variant.UploadState ?? ProductUploadEnum.NotUploaded;
    }

    public SkuUploadInfoVm(Data.Products.Product productStruct)
    {
        Sku = productStruct.Sku!;
        Code = productStruct.Code;
        Asin = productStruct.Asin;
        Sid = productStruct.Sid;
        UpdateState = productStruct.UploadState ?? ProductUploadEnum.NotUploaded;
    }

    public SkuUploadInfoVm(string sku, ProductUploadEnum productUploadEnum)
    {
        Sku = sku;     
        UpdateState = productUploadEnum;
    }

    public string Sku { get; set; }
    public string? Code { get; set; }
    public string? Asin { get; set; }
    public ProductUploadEnum UpdateState { get; set; }

    public string? Sid { get; set; }
}