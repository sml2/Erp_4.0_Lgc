namespace ERP.Models.View.Products.Product;

public class EditWithShowDto : EditProductItemDto
{
    public int Id { get; set; }
    public int ItemId { get; set; }
}