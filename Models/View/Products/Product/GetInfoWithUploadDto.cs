using ERP.Enums;
using ERP.Models.DB.Stores;
using ERP.Models.Product;
using ERP.Models.Stores;

namespace ERP.Models.View.Products.Product;

public class GetInfoWithUploadDto
{
    [NJP("product_id")]
    public int Pid { get; set; }

    [NJP("language")]
    public Languages Language { get; set; }

    public int LanguageId => Language == Languages.Default ? 0 : (int)Math.Log2((long)Language) + 1;

    [NJP("code_type")]
    public EanupcModel.TypeEnum  CodeType { get; set; }

    public string Unit { get; set; } = "CNY";

    /// <summary>
    /// true:生成，false：不生成，取已有的
    /// </summary>
    public bool IsPaddingCode { get; set; } = true;

    /// <summary>
    /// true:生成，false：不生成，取已有的
    /// </summary>
    public bool IsGenerateSku { get; set; } = true;

    //public bool NoReview { get; set; } = true;
}