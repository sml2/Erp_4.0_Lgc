using Newtonsoft.Json;
using ThirdParty.Json.LitJson;

namespace ERP.Models.View.Products.Product;

public class SetProductKeywordDto : BatchSetProductInfoDto
{
    [NJP("keywordId")]
    public override int TemplateId { get; set; }

    [NJP("content")]
    public string Content { get; set; }
}