using ERP.Data.Products;
using ERP.Enums;
using ERP.Extensions;
using ERP.Models.Product;
using Attribute = ERP.Data.Products.Attribute;

namespace ERP.Models.View.Products.Product;

public class ViewModel : EditDto
{
    public ViewModel()
    {
    }

    public ViewModel(ProductModel model)
    {
        Id = model.ID;
        UserId = model.UserID;
        GroupId = model.GroupID;
        CompanyId = model.CompanyID;
        OemId = model.OEMID;
        IsEdited = model.IsEdited();
        IsShare = model.IsShare();
        IsLock = model.IsLock();
        UiLanguage = model.UiLanguage.GetName().ToLower();
    }

    public bool IsLock { get; set; }

    public bool IsShare { get; set; }

    public bool IsEdited { get; set; }

    public int Id { get; set; }

    public int UserId { get; set; }

    public int? GroupId { get; set; }

    public int CompanyId { get; set; }

    public int OemId { get; set; }
    [NJP("ui_language")]
    public string UiLanguage { get; set; }

    [NJP("attribute")]
    public Dictionary<int, AttributeViewMode>? Attributes { get; set; }

    [NJP("languagePacks")]
    public Dictionary<string,string> LanguagePacks { get; set; }
    
    public Dictionary<string, LanguageInfo> Languages { get; set; }
    public string CurrentLanguage { get; set; }
    [NJP("current_version")]
    public long? CurrentVersion { get; set; }
    public string UpdatedAtTicks { get; set; }
    
    public new List<VariantViewModel>? Variants { get; set; }
}

public class AttributeViewMode
{
    public AttributeViewMode(Attribute item,int tag)
    {
        Name = item.Name;
        Value = item.Value;
        State = item.State;
        Tag = tag;
    }

    public string Name { get; set; }
    public string[] Value { get; set; }
    public AttributeStateEnum State { get; set; }
    public int Tag { get; set; }

    [NJP("tempValue")]
    public string TempValue { get; set; }
}