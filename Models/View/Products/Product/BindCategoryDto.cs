using ThirdParty.Json.LitJson;

namespace ERP.Models.View.Products.Product;

public class BindCategoryDto
{
    public List<int> Ids { get; set; }
    
    [NJP("category_id")]
    public int? CategroyId { get; set; }
    
    [NJP("sub_category_id")]
    public int? SubCategoryId { get; set; }
}