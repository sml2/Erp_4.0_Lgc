using Common.Enums.Products.ImageTran;
using ERP.ControllersWithoutToken.WayBill;
using ERP.Models.View.Products.Product.ImageTran;

namespace ERP.Models.View.Products.Product;

public class ImageTranDto : BaseTranDto
{
    public int Pid { get; set; }
    public int Id { get; set; }
    public bool IsNetwork { get; set; }
}