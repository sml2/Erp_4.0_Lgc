namespace ERP.Models.View.Products.Product;

public class GoMattingDto
{
    public string Base64Data { get; set; }

    public string Type { get; set; }
}