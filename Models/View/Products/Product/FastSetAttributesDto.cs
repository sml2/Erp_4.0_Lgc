using ERP.ControllersWithoutToken.Export;
using ERP.Data.Products.Expand;
using ERP.Enums;
using Newtonsoft.Json;
using ThirdParty.Json.LitJson;

namespace ERP.Models.View.Products.Product;

public class FastSetAttributesDto
{
    [NJP("id")]
    public int Id { get; set; }
    
    [NJP("itemId")]
    public int ItemId { get; set; }

    [NJP("variants")]
    public List<VariantViewModel>? Variants { get; set; }
    
    [NJP("attributes")]
    public Dictionary<int, ERP.Data.Products.Attribute>? Attributes { get; set; }
    
    [NJP("updatedAtTicks")]
    public string UpdatedAtTicks { get; set; }
    
    public string Coin { get; set; }
}