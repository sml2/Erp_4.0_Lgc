using Common.Enums.Products;
using ERP.Data.Products;
using ERP.Data.Products.Feature;
using ERP.Enums.Product;
using Attribute = ERP.Data.Products.Attribute;

namespace ERP.Models.View.Products.Product;

public class EditProductItemDto
{
    [NJP("common")]
    public CommonDto Common { get; set; }

    [NJP("info")]
    public InformationDto Info { get; set; }

    public bool IsUseMatting { get; set; }
    public bool IsUseImgTran { get; set; }

    public class CommonDto
    {
        [NJP("category_id")]
        public int? CategoryId { get; set; }

        [NJP("sub_category_id")]
        public int? SubCategoryId { get; set; }

        [NJP("coin")]
        public string Coin { get; set; }

        public int? Id { get; set; }

        // [NJP("images")]
        // public Images? Images { get; set; }

        [NJP("images")]
        public ImagesViewModel? Images { get; set; }
        
        [NJP("editorImages")]
        public Dictionary<int, Image>? EditorImages { get; set; }

        [NJP("isImageSync")]
        public bool IsImageSync { get; set; }

        [NJP("newCreateImages")]
        public List<int>? NewCreateImagesIds { get; set; }

        public string? Pid { get; set; }
        public string? Sku { get; set; }

        public ProductUploadEnum UploadState { get; set; }
        public string? Asin { get; set; }
        public string? Code { get; set; }

        [NJP("source")]
        public string? Source { get; set; }

        [NJP("showLanguage")]
        public Enums.Languages? ShowLanguage { get; set; }

        [NJP("isClone")]
        public bool IsClone { get; set; } = false;

        [NJP("codeCompare")]
        public Dictionary<string, string?>? CodeCompare { get; set; }
    }

    public class InformationDto
    {
        public int? Id { get; set; }

        [NJP("attribute")]
        public Dictionary<int, Attribute>? Attributes { get; set; }

        [NJP("attributeTag")]
        public int AttributeTag { get; set; }


        public Enums.Languages CurrentLanguageEnum => Enum.Parse<Enums.Languages>(CurrentLanguageStr, true);

        [NJP("currentLanguage")]
        public string CurrentLanguageStr { get; set; }

        [NJP("currentLanguageId")]
        public int CurrentLanguageId { get; set; }

        [NJP("currentVersion")]
        public long? CurrentVersion { get; set; }

        public string? Desc { get; set; }
        
        public DescriptionTypeEnum DescType { get; set; }

        [NJP("features")]
        public Features? Features { get; set; }

        [NJP("isSingle")]
        public bool IsSingle { get; set; }

        public string? Keyword { get; set; }

        public List<Dictionary<string, string>>? Sketch { get; set; }

        public string? Title { get; set; }

        [NJP("variants")]
        public List<VariantDto>? Variants { get; set; }

        [NJP("quantity")]
        public int Quantity { get; set; }

        [NJP("coin")]
        public string Coin { get; set; }

        [NJP("price")]
        public Prices Prices
        {
            get => _prices;
            set => SetPrices(value);
        }

        protected Prices _prices { get; set; }


        protected virtual void SetPrices(Prices value)
        {
            if (Coin.IsNull())
            {
                throw new InvalidCastException($"coin is null");
            }

            _prices = value;
            _prices.Cost.SetMax(value.Cost.Max, Coin);
            _prices.Cost.SetMin(value.Cost.Min, Coin);

            _prices.Sale.SetMax(value.Sale.Max, Coin);
            _prices.Sale.SetMin(value.Sale.Min, Coin);
        }
    }

    /// <summary>
    /// 图库图片删除，导致其他语言中变体缺少图片。记录缺少的图片id值，再次提交保存时去除删除图片id
    /// </summary>
    [NJP("variantImageRmIds")]
    public List<int> VariantImageRmIds { get; set; } = new List<int>();
}