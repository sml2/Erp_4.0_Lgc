namespace ERP.Models.View.Products.Product;

public class UploadProductVm
{
    public UploadProductVm()
    {
        
    }
    public UploadProductVm(Data.Products.Product itemStruct)
    {
        Title = itemStruct.GetTitle()!;
        Keyword = itemStruct.GetKeyword();
        Sketches = itemStruct.GetSketches();
        Desc = itemStruct.GetDescription();
        Quantity = itemStruct.Quantity;
        Code = itemStruct.GetCode();
    }
    

    /// <summary>
    /// 标题
    /// </summary>
    public string Title { get; set; }

    /// <summary>
    /// 关键字
    /// </summary>
    public string? Keyword { get; set; }

    /// <summary>
    /// 卖点
    /// </summary>
    public List<string>? Sketches { get; set; }

    /// <summary>
    /// 描述
    /// </summary>
    public string? Desc { get; set; }

    /// <summary>
    /// SKU
    /// </summary>
    public string Sku { get; set; }
    
    /// <summary>
    /// EAN/UPC
    /// </summary>
    public string? Code { get; set; }

    /// <summary>
    /// 主图
    /// </summary>
    public string Main { get; set; }
    
    
    /// <summary>
    /// 附图
    /// </summary>
    public List<string> Affiliate { get; set; } = new List<string>();
    
    /// <summary>
    /// 库存
    /// </summary>
    public int? Quantity { get; set; }
    
    /// <summary>
    /// 成本价
    /// </summary>
    public Money Cost { get; set; }

    /// <summary>
    /// 销售价
    /// </summary>
    public Money Sale { get; set; }

    public virtual Dictionary<string, string>? _Attribute { get; set; }

    public List<UploadProductItemVm> Products { get; set; }
}