﻿using ERP.Models.Api.Amazon.vm;
using ERP.Models.DB.Product;
using Newtonsoft.Json.Linq;

namespace ERP.Models.View.Products.Product
{
    public class productUploadView
    {
       public  Dictionary<int, List<ListParam>> datas { get; set; }
        public ProductParam param { get; set; }
   
    }

    public class ListParam
    {
        public ListParam(string sku, JObject data)
        {
            Sku = sku;
            Data = data;
        }

        public string Sku { get; set; }

        public JObject Data { get; set; }
    }
}
