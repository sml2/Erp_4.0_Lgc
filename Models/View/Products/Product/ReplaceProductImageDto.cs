using ERP.Data.Products;

namespace ERP.Models.View.Products.Product;

public class ReplaceProductImageDto
{
    [NJP("id")]
    public int ProductId { get; set; }

    [NJP("imgId")]
    public int ImgId { get; set; }

    [NJP("path")]
    public string OldPath { get; set; }

    [NJP("data")]
    public Image ImgObj { get; set; }
    
    public Boolean IsEditor { get; set; }
}