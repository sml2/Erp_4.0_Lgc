using ERP.Enums;
using ERP.Models.DB.Product;

namespace ERP.Models.View.Products.Product;

public class LanguagePackViewModel
{
    public LanguagePackViewModel(ProductItemModel productItemModel)
    {
        ItemId = productItemModel.ID;
        LanguageCode = productItemModel.LanguageName;
        Name = Enum.Parse<Languages>(LanguageCode,true).GetDescriptionByKey("Name");
        LanguageId = productItemModel.LanguageId;
    }

    public int ItemId { get; set; }

    public string LanguageCode { get; set; }
    public string Name { get; set; }

    public int? LanguageId { get; set; }
}