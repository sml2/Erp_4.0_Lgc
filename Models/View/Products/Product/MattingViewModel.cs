namespace ERP.Models.View.Products.Product;

public class MattingViewModel
{
    public int Code { get; set; }   
    public MattingData? Data { get; set; }   
    public string Msg { get; set; }   
    public long Time { get; set; }   
}

public class MattingData
{
    public string ImageBase64 { get; set; }
}