namespace ERP.Models.View.Products.Product;

public class BatchSetProductInfoDto
{
    [NJP("id")]
    public int Id { get; set; }

    [NJP("Language")]
    public LanguageOptionEnum LanguageOption { get; set; }

    [NJP("mode")]
    public ModeEnum Mode { get; set; }

    [NJP("position")]
    public PositionEnum Position { get; set; }
    
    public virtual int TemplateId { get; set; }
}

public enum PositionEnum
{
    Title = 1,
    Desc = 2,
    Keyword = 3
}

public enum ModeEnum
{
    //覆盖
    Cover = 1,

    //头部追加
    HeadAddition = 2,

    //尾部追加
    TailAppend = 3
}

public enum LanguageOptionEnum
{
    //产品默认语言
    Default = 0,

    //当前产品翻译语种
    CurrentProductLang,

    //当前模板语种
    CurrentTemplateLang,
}