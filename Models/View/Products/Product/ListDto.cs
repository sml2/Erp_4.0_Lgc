using Common.Enums.Products;
using ERP.Enums.Product;
using ERP.Models.View.Product;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;

namespace ERP.Models.View.Products.Product;

public class ListDto : PageDto
{
    public ActionEnum Action { get; set; } = ActionEnum.Collect;

    [JsonProperty("category_id")]
    public int? CategoryId { get; set; }

    [JsonProperty("sub_category_id")]
    public int? SubCategoryId { get; set; }

    [JsonProperty("isRepeat")]
    public bool IsRepeat { get; set; }

    [JsonProperty("multi")]
    public MultiEnum Multi { get; set; }

    [JsonProperty("name")]
    public string? Title { get; set; }

    [JsonProperty("pid")]
    public string? Pid { get; set; }


    [JsonProperty("platform_id")]
    public int? PlatformId { get; set; }

    [JsonProperty("user_id")]
    public int? UserId { get; set; }

    [JsonProperty("task_id")]
    public int? TaskId { get; set; }

    [JsonProperty("audit_id")]
    public AuditStateEnum? AuditState { get; set; }
    
    [JsonProperty("updated_time")]
    public List<DateTime>? UpdatedTime { get; set; }
    
    public int? Share { get; set; }
    
    public ReleaseEnum? Release { get; set; }
}


public enum ActionEnum
{
    Collect = 1,
    Manual,
    Audit,
    Share,
    Recycle
}

public enum MultiEnum
{
    //精确搜索
    Precise,

    //模糊搜索
    Fuzzy
}