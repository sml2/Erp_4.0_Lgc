namespace ERP.Models.View.Products.Product;

public class ShowDto
{
    public int Id { get; set; }
    public int? ItemId { get; set; }
}