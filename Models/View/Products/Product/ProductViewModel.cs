using ERP.Data.Products;
using ERP.Data.Products.Feature;
using ERP.Enums.Product;
using ERP.Extensions;
using ERP.Models.DB.Product;
using ERP.Models.Product;

namespace ERP.Models.View.Products.Product;

public class ProductViewModel : EditProductItemDto
{
    public ProductViewModel()
    {
    }

    [NJP("languagePacks")]
    public Dictionary<string, Packs> LanguagePacks { get; set; }

    public class Packs
    {
        public int ItemId { get; set; }
        public int Value { get; set; }
        public string Label { get; set; }
    }

    public class CommonViewModel : CommonDto
    {
        public new string ShowLanguage { get; set; }
    }

    public class InformationViewModel : InformationDto
    {
        public new Dictionary<int, AttributeViewMode> Attributes { get; set; }
        public new List<VariantViewModel> Variants { get; set; }

        public string UpdatedAtTicks { get; set; }
        
        public new PricesViewModel Prices { get; set; }
    }

    public ProductViewModel(ProductModel productModel, ProductItemModel itemModel, Data.Products.Product productStruct,
        string unit, bool productShowUnit) : this(productModel, itemModel, productStruct,
        productShowUnit ? itemModel.Coin.IsNotEmpty() ? itemModel.Coin : unit : unit)
    {
    }

    public ProductViewModel(ProductModel productModel, ProductItemModel itemModel, Data.Products.Product productStruct,
        string unit)
    {
        Common = new CommonViewModel()
        {
            CategoryId = productModel.CategoryId,
            SubCategoryId = productModel.SubCategoryId,
            Id = productModel.ID,
            Images = productModel.Gallery?.ToImagesVm(),
            EditorImages = productModel.Gallery?.Where(m => m.IsEditor).ToDictionary(m => m.ID,m => m),
            IsImageSync = productModel.IsImageAsync,
            Pid = productModel.Pid,
            Sku = productModel.Sku,
            Code = productStruct.Code,
            Asin = productStruct.Asin,
            UploadState = productStruct.UploadState ?? ProductUploadEnum.NotUploaded,
            Source = productModel.Source,
            ShowLanguage = productModel.UiLanguage.GetName().ToLower(),
            Coin = unit,
            CodeCompare = productModel.CodeCompareDic,
        };

        //Attributes
        var attributes = productStruct.Variants.Attributes;
        var attributesViewModel = new Dictionary<int, AttributeViewMode>();
        foreach (var item in attributes.items)
        {
            attributesViewModel.Add(item.Key, new AttributeViewMode(item.Value, item.Key));
        }

        //Variants
        var variants = productStruct.Variants;
        List<VariantViewModel>? variantsViewModel = new List<VariantViewModel>();
        var variantIndex = 0;
        foreach (var item in variants)
        {
            if (productStruct.Version != productStruct.CurrentVersion)
            {
                item.Sid = string.Empty;
            }
            // variantsViewModel.Add(new VariantViewModel(item, itemModel.Coin.IsNotEmpty() ? itemModel.Coin : unit));
            variantsViewModel.Add(new VariantViewModel(item,unit,variantIndex));
            variantIndex++;
        }

        //Features
        var features = new Features();
        foreach (var item in Enum<FeatureEnum>.ToDictionaryForUI())
        {
            productStruct.Properties.TryGetValue(item.Value, out IProperty property);
            if (property.IsNotNull())
            {
                features.Add(property.Key, property.Value);
            }
        }

        //Sketches
        var sketches = productStruct.GetSketches();
        List<Dictionary<string, string>> tempSketches = new();

        foreach (var i in sketches)
        {
            tempSketches.Add(new Dictionary<string, string>()
            {
                { "value", i }
            });
        }


        Info = new InformationViewModel()
        {
            Id = itemModel.ID,
            Attributes = attributesViewModel,
            Variants = variantsViewModel,
            AttributeTag = productStruct.Variants.Attributes.CurCreateNum,
            CurrentLanguageStr = itemModel.LanguageName,
            CurrentLanguageId = Convert.ToInt32(itemModel.LanguageId),
            CurrentVersion = itemModel.CurrentVersion,
            Features = features,
            IsSingle = productStruct.Variants.Attributes.IsSingle,
            Keyword = productStruct.GetKeyword(),
            Sketch = tempSketches,
            Title = productStruct.GetTitle(),
            Quantity = itemModel.Quantity,
            Coin = unit,
            UpdatedAtTicks = itemModel.UpdatedAtTicks,
            Prices = new (productStruct.GetPrices()!,unit),
            DescType = productStruct.GetDescriptionType()
        };
    }
}