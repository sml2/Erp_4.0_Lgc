using ERP.Data.Products.Expand;
using ERP.Enums;
using Newtonsoft.Json;
using ThirdParty.Json.LitJson;

namespace ERP.Models.View.Products.Product;

public class SetProductRandomDto
{
    [NJP("id")]
    public int Id { get; set; }

    [NJP("keywords")]
    public List<int> Keywords { get; set; }

    [NJP("Sketch")]
    public List<int> Sketch { get; set; }

    public ProductRandomModeEnum Mode { get; set; }

    public Languages Language { get; set; }
    public int LanguageId => Language == Languages.Default ? 0 : (int)Math.Log2((long)Language) + 1;
}

public enum ProductRandomModeEnum
{
    //主产品
    Main = 1,

    //子变体
    Variant = 2,

    //全部(包括主产品/子变体)
    All = 3
}