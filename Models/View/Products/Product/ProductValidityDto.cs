using ERP.Extensions;
using ERP.Interface.Rule;
using ERP.Models.Product;
using OrderSDK.Modles.Amazon.Finances;

namespace ERP.Models.View.Products.Product;

public class ProductValidityDto
{
    public int? Id { get; set; }
    public int UserId { get; set; }
    public int CompanyId { get; set; }
    public decimal? Cost { get; set; }
    public DateTime Validity { get; set; }
    public string? Remark { get; set; }

    public ProductValidityModel To(ISession? session)
    {
        return new ProductValidityModel()
        {
            UserId = UserId,
            CompanyId = CompanyId,
            Cost = Cost,
            Validity = Validity,
            Remark = Remark,
            AddUserId = session!.GetUserID(),
            AddTrueName = session!.GetTrueName()
        };
    }
}