using ERP.Data.Products.Expand;
using ERP.Enums;
using Newtonsoft.Json;
using ThirdParty.Json.LitJson;

namespace ERP.Models.View.Products.Product;

public class SetDefaultLanguageDto
{
    [NJP("id")]
    public int Id { get; set; }

    [NJP("language")]
    public Languages Language { get; set; }
    
    public int LanguageId => Language == Languages.Default ? 0 : (int)Math.Log2((long)Language) + 1;
}
