using ERP.Data.Products.Expand;
using ERP.Data.Products.Feature;
using Newtonsoft.Json;
using ThirdParty.Json.LitJson;

namespace ERP.Models.View.Products.Product;

public class SetProductExtDto
{
    [NJP("id")]
    public int Id { get; set; }
    
    public ExpandModeEnum Mode { get; set; }

    [NJP("content")]
    public ExpandsContent Content { get; set; }
}

public class ExpandsContent 
{
    public int Id { get; set; }
    public string Name { get; set; }
    public Features Content { get; set; }
}

public enum ExpandModeEnum
{
    //覆盖
    Cover = 1,
    //移除重复
    RemoveDuplicates = 2
}