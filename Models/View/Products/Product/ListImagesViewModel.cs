using ERP.Data.Products;
using ERP.Models.Product;

namespace ERP.Models.View.Products.Product;

public class ListImagesViewModel
{
    public ListImagesViewModel()
    {
    }

    public ListImagesViewModel(ProductModel model)
    {
        Id = model.ID;
        Title = model.Title;
        Gallery = model.Gallery?.ToImagesVm();
    }

    public int Id { get; set; }

    public string Title { get; set; }

    public ImagesViewModel? Gallery { get; set; }
}