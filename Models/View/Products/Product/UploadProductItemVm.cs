using ERP.ControllersWithoutToken.Tasks;
using ERP.Data.Products;

namespace ERP.Models.View.Products.Product;


public class UploadProductItemVm : UploadProductVm
{

    public UploadProductItemVm()
    {
    }

    public UploadProductItemVm(Variant variant, ERP.Data.Products.Product product)
    {
        Title = variant.GetTitle() ?? product.GetTitle()!;
        Keyword = variant.GetKeyword() ?? product.GetKeyword();
        Sketches = variant.GetSketches() ?? product.GetSketches();
        Desc = variant.GetDescription() ?? product.GetDescription();
        //Attribute
        Quantity = variant.Quantity;
        Code = variant.Code;
    }

    

    /// <summary>
    /// 属性
    /// </summary>
    public override Dictionary<string,string>? _Attribute { get; set; }
    public string Attribute
    {
        get => string.Join(", ", _Attribute.Select(m => m.Key + ":" + m.Value).ToArray());
    }







   
}