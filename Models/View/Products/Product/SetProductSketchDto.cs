using Newtonsoft.Json;
using ThirdParty.Json.LitJson;

namespace ERP.Models.View.Products.Product;

public class SetProductSketchDto : BatchSetProductInfoDto
{
    [NJP("tempId")]
    public override int TemplateId { get; set; }

    [NJP("content")]
    public List<Dictionary<string, string>> Content { get; set; }
}