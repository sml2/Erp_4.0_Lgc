using ERP.Data.Products;
using ERP.Enums;
using Ryu.Extensions;
using Index = Microsoft.EntityFrameworkCore.Metadata.Internal.Index;

namespace ERP.Models.View.Products.Product;

public class VariantViewModel : VariantDto
{
    public VariantViewModel()
    {
    }

    public VariantViewModel(Variant item,string unit,int index):base(item)
    {
        Index = index;
        Cost = item.Cost.To(unit).ToFixed();
        Sale = item.Sale.To(unit).ToFixed();

        Basic = new LanguageInfo();
        Basic.Title = item.GetTitle();
        Basic.Keyword = item.GetKeyword();
        Basic.Desc = item.GetDescription();
        Basic.Title = item.GetTitle();
        var sketches = item.GetSketches();
        List<Dictionary<string, string>> tempSketches = new();
        if (sketches is not null)
        {
            foreach (var v in sketches)
            {
                tempSketches.Add(new Dictionary<string, string>()
                {
                    { "value", v }
                });
            }
            Basic.Sketch = tempSketches;
        }
    }

    [NJP("cost")]
    public new string Cost { get; set; }

    [NJP("sale")]
    public new string Sale { get; set; }
    public new LanguageInfo? Basic { get; set; }
    
    public int? Index { get; set; }

}