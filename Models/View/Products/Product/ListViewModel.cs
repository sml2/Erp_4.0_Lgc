using ERP.Data.Products;
using ERP.Enums;
using ERP.Enums.Product;
using ERP.Extensions;
using ERP.Models.DB.Product;
using ERP.Models.Product;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using Newtonsoft.Json;
using NPOI.SS.Formula.Functions;
using NPOI.Util;
using OrderSDK.Modles;
using Ryu.Extensions;
using System.Collections.Generic;
using PlatformDataCache = ERP.Services.Caches.PlatformData;
namespace ERP.Models.View.Products.Product;

public class ListViewModel
{
    private readonly Prices _tempPrice;
    private readonly string _coin;

    public ListViewModel(ProductModel model)
    {
        Id = model.ID;
        Title = model.Title;
        Source = model.Source;
        Quantity = model.Quantity;

        var mainImage = model.MainImage != null ? JsonConvert.DeserializeObject<Image>(model.MainImage) : null;
        if (mainImage != null)
        {
            Image = mainImage.Path.IsNotNull() ? mainImage.Path : mainImage.Url;
        }

        IsEdited = model.IsEdited();
        IsShare = model.IsShare();
        Coin = model.Coin;
        CurrentLanguage = model.UiLanguage.GetName().ToLower();
        DownloadImages = new List<string>();


        Language = model.Language;
        _tempPrice = model.Prices is not null
            ? JsonConvert.DeserializeObject<Prices>(model.Prices)!
            : new Prices();
        _coin = model.Coin;

        Type = model.Type;
        Audit = model.Audit;
        Reason = model.Reason;
        UserLevelRepeatNum = model.UserLevelRepeatNum;
        CategoryName = model.CategoryModel?.Name ?? "暂无分类";
        SubCategoryName = model.SubCategoryModel?.Name ?? "暂无二级分类";
        UserId = model.UserID;
        GroupId = model.GroupID;
        CompanyId = model.CompanyID;
        OemId = model.OEMID;
        CreatedAt = model.CreatedAt;
        UpdatedAt = model.UpdatedAt;
        // HistoryVersion = JsonConvert.DeserializeObject<List<string>>(model.HistoryVersion);
        HistoryVersion = new List<string>();
        Platform = model.PlatformId;
        // CurrentVersion = model.CurrentVersion;
        IsImageAsync =  model.IsImageAsync;
        IsReleaseB2C =  model.IsB2CRelease();
        UiLanguage = model.UiLanguage.GetDescriptionByKey("Code");
    }

    public bool IsImageAsync { get; set; }
    public bool IsReleaseB2C { get; set; }
    public string UiLanguage { get; set; }

    public int? Platform { get; set; }
    
    [JsonIgnore]
    public long Language { get; set; }

    [JsonProperty("current_version")]
    public long? CurrentVersion { get; set; }

    [JsonProperty("history_version")]
    public List<string> HistoryVersion { get; set; }

    [JsonProperty("oem_id")]
    public int OemId { get; set; }

    [JsonProperty("company_id")]
    public int CompanyId { get; set; }

    [JsonProperty("group_id")]
    public int GroupId { get; set; }

    [JsonProperty("user_id")]
    public int UserId { get; set; }

    public bool IsShare { get; set; }

    public int UserLevelRepeatNum { get; set; }

    public string? Reason { get; set; }

    public long? Audit { get; set; }
    public long? Type { get; set; }
    public Dictionary<string, string> Translate { get; set; }

    public PricesViewModel? Price { get; set; }

    [JsonProperty("updated_at")]
    public DateTime UpdatedAt { get; set; }

    [JsonProperty("created_at")]
    public DateTime CreatedAt { get; set; }

    public int Id { get; set; }

    public List<string> DownloadImages { get; set; }

    public string CurrentLanguage { get; set; }

    public string Coin { get; set; }

    public bool IsEdited { get; set; }

    public string Title { get; set; }
    public string? CategoryName { get; set; }
    public string? SubCategoryName { get; set; }
    public string? Source { get; set; }
    public int? Quantity { get; set; }
    public string? Image { get; set; }

    public void SetLanguagePack(Dictionary<Languages,string> packs)
    {
        
        var translate = new Dictionary<string, string>();
        foreach (var item in packs)
        {
            translate.Add(item.Key.ToString().ToLower(), item.Value);
        }

        Translate = translate;
    }

    public void SetPrices(string unit)
    {
        Price = new PricesViewModel(_tempPrice, _coin.IsNotEmpty() ? _coin : unit);
    }

    public void SetPrices(string unit, bool productShowUnit)
    {
        //采集货币 
        if (productShowUnit)
        {
            SetPrices(_coin.IsNotEmpty() ? _coin : unit);
        }
        else
        {
            //通用货币
            Price = new PricesViewModel(_tempPrice, unit);
        }
    }
}

public class MarketPlaceInfo
{
    public string MarketPlace { get; set; }

    public string Name { get; set; }
}

public class ListViewUploadModel
{
    public ListViewUploadModel(ProductUpload model,string sku, Dictionary<Languages, string> packs,List<MarketPlaceInfo>? marketPlaceInfos)
    {
        var p = model.Product;
        Id = model.ID;
        var mainImage = p.MainImage != null ? JsonConvert.DeserializeObject<Image>(p.MainImage) : null;
        if (mainImage != null)
        {
            Image = mainImage.Path.IsNotNull() ? mainImage.Path : mainImage.Url;
        }        
        CurrentLanguage = model.LanguageSign;
        Title = p.Title;
        //价格
        var tempPrice = p.Prices is not null
            ? JsonConvert.DeserializeObject<Prices>(p.Prices)!
            : new Prices();
        Price = new PricesViewModel(tempPrice, model.Unit);
        Unit = model.Unit;
        Quantity = p.Quantity;

        var translate = new Dictionary<string, string>();
        foreach (var item in packs)
        {
            translate.Add(item.Key.ToString().ToLower(), item.Value);
        }

        Translate = translate;
        PathNodeNames = model.PathName;
        PathNodeIDs = model.PathNodes;
        NodeID =  JsonConvert.DeserializeObject<List<long>>(model.PathNodes).Last().ToString();
        StoreID = model.Store.ID;
        StoreName = model.Store.Name;
        //刊登状态
        StateValue = model.Result.GetID();
        StateName = model.Result.GetDescription();
        UserName = model.User.UserName;
        UserId = model.UserID;
        GroupId = model.GroupID;
        CompanyId = model.CompanyID;
        OemId = model.OEMID;
        CreatedAt = model.CreatedAt;
        UpdatedAt = model.UpdatedAt;
        Platform = model?.Platform?.GetNumber();
        ProductType = model.ProductType;
        MarketPlace = model.MarketPlace;
        MarketPlaceName = marketPlaceInfos.FirstOrDefault(x => x.MarketPlace == model.MarketPlace).Name;
        ProductID = model.Product.ID;
        Sku = sku;
        Message = string.IsNullOrWhiteSpace(model.Message)? null: JsonConvert.DeserializeObject<List<pushProductResult>>(model.Message);
    }

    public int Id { get; set; }

    public int ProductID { get; set; }

    /// <summary>
    /// 产品图
    /// </summary>
    public string? Image { get; set; }

    /// <summary>
    /// 语种
    /// </summary>
    public string CurrentLanguage { get; set; }

    /// <summary>
    /// 产品名称
    /// </summary>
    public string Title { get; set; }

    //价格
    public PricesViewModel Price { get; set; }

    public string Unit { get; set; }

    //库存
    public int? Quantity { get; set; }

    /// <summary>
    /// 刊登路径名称
    /// </summary>
    public string? PathNodeNames { get; set; }

    /// <summary>
    /// 刊登路径节点
    /// </summary>
    public string? PathNodeIDs { get; set; }
    public string? NodeID { get; set; }

    /// <summary>
    /// 刊登状态值
    /// </summary>
    public int  StateValue { get; set; }

    /// <summary>
    /// 刊登状态名称
    /// </summary>
    public string StateName { get; set; }   

    /// <summary>
    /// 店铺ID
    /// </summary>
    public int StoreID { get; set; }

    /// <summary>
    /// 店铺名称
    /// </summary>
    public string StoreName { get; set; }

    /// <summary>
    /// 用户id
    /// </summary>
    public int UserId { get; set; }

    public int GroupId { get; set; }

    public int CompanyId { get; set; }

    public int OemId { get; set; }

    /// <summary>
    /// 用户名称
    /// </summary>
    public string UserName { get; set; }

    public int? Platform { get; set; }

    public string ProductType { get; set; }

    public string MarketPlace { get; set; }

    public string MarketPlaceName { get; set; }

    public Dictionary<string, string> Translate { get; set; }

    /// <summary>
    /// 上传时间，刊登时间
    /// </summary>
    public DateTime UpdatedAt { get; set; }

    public DateTime CreatedAt { get; set; }

    public string Sku { get; set; }

    public List<pushProductResult> Message { get; set; }
}



//public class ListViewUploadJsonModel
//{
//    public ListViewUploadJsonModel(ProductUploadJson model)
//    {
//        Sku = model.Sku;
//        StateName = model.Result.GetDescription();
//        Message = model.Message;
//        IsMain = model.IsMain;
//        ProductID = model.ProductID ;
//        Id = model.ID;
//        Json = model.ProductJson;
//    }

//    public string  Sku { get; set; }
//    public string StateName { get; set; }
//    public string Message { get; set; }
//    public bool  IsMain { get; set; }
//    public int ProductID { get; set; }
//     public int Id { get; set; }
//    public string Json { get; set; }

//}