using ERP.ViewModels;

namespace ERP.Models.View.Products.Product;

public class DestroyDto : IdDto
{
    public bool isClone { get; set; } = false;
}