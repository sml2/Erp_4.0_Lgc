using ERP.Enums;
using ERP.ViewModels.Template;
using System.ComponentModel.DataAnnotations;
using ERP.Enums.ID;

namespace ERP.Models.View.Products.Template;

public class ChangePriceDto
{
    public int ID { get; set; } = 0;
    public string Name { get; set; }

    public string Value { get; set; } = string.Empty;
    public string NationName { get; set; }

    public string NationShort { get; set; }
    public string? UpdatedAtTicks { get; set; }
}