﻿namespace ERP.ViewModels.Template;

public class ImportKeywordDto
{
    public string Value { get; set; } = string.Empty;
    public string Name { get; set; } = string.Empty;
}