﻿using System.ComponentModel.DataAnnotations;
using ERP.Enums;
using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;

namespace ERP.ViewModels.Template;

public class LanguageViewModel
{
    [Comment("语言"), Required(ErrorMessage = "请选择语言!")]
    public string Language { get; set; }

    [Comment("语言简码"), Required(ErrorMessage = "请选择语言!")]
    public Languages LanguageSign { get; set; }
}