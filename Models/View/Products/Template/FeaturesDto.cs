using ERP.Enums.Product;
using ERP.Extensions;

namespace ERP.Models.View.Products.Template;

public class FeaturesDto
{
    [NJP("key")]
    public FeatureEnum EnumKey { get; set; }

    public string PropertyKey => EnumKey.GetName();

    [NJP("name")]
    public string Name { get; set; }

    [NJP("value")]
    public object Value { get; set; }
}