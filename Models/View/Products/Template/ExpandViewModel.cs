using ERP.Data.Products.Expand;
using ERP.Data.Products.Feature;
using ERP.Models.Product;
using ERP.Models.Template;
using Newtonsoft.Json;

namespace ERP.Models.View.Products.Template;

public class ExpandViewModel
{
    public ExpandViewModel(ExtTemplateModel model)
    {
        Id = model.ID;
        Name = model.Name;
        var arr = JsonConvert.DeserializeObject<List<FeaturesDto>>(model.Content);

        var features = new Features();

        foreach (var item in arr)
        {
            features.Add(item.EnumKey,item.Value);
        }

        Content = features;
    }

    public int Id { get; set; }
    public string Name { get; set; }
    public Features? Content { get; set; }
}