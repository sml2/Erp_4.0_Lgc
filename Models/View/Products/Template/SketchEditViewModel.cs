﻿using System.ComponentModel.DataAnnotations;
using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;

namespace ERP.ViewModels.Template;

public class SketchEditViewModel : LanguageViewModel
{
    public int ID { get; set; } = 0;

    [Comment("卖点模板名称"), Required(ErrorMessage = "请填写模板名称")]
    [StringLength(255, ErrorMessage = "模板名称最大长度不可超过255")]
    public string Name { get; set; } = string.Empty;

    [Comment("卖点模板内容"), Required(ErrorMessage = "请填写卖点内容")]
    public object Content { get; set; } = string.Empty;

    public string? UpdatedAtTicks { get; set; }
}