﻿using System.ComponentModel.DataAnnotations;
using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;

namespace ERP.ViewModels.Template;

public class ExpandEditViewModel
{
    public int ID { get; set; } = 0;

    [Comment("模板名称"), Required(ErrorMessage = "请填写模板名称")]
    [StringLength(255, ErrorMessage = "模板名称最大长度不可超过255")]
    public string Name { get; set; }=string.Empty;

    [Comment("属性信息"), Required(ErrorMessage = "请填写属性信息")]
    public object Content { get; set; } = new();
    public DateTime? UpdatedAt { get; set; }
}