﻿using ERP.Models.View.Product;

namespace ERP.Models.View.Product.Template;

public class ListDto : PageDto
{
    public string? Name { get; set; }

    public string? LanguageSign { get; set; }
    
    public string? NationShort { get; set; }
}