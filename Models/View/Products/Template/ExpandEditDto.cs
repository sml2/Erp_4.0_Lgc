using ERP.Data.Products.Expand;
using ERP.Data.Products.Feature;
using ERP.Extensions;
using ERP.Models.Template;
using Newtonsoft.Json;

namespace ERP.Models.View.Products.Template;

public class ExpandEditDto
{
    public int? Id { get; set; }
    public string Name { get; set; }
    [NJP("content")]
    public List<FeaturesDto> Content { get; set; }

    public ExtTemplateModel To(ISession session)
    {
        return new ExtTemplateModel()
        {
            Name = Name,
            Content = JsonConvert.SerializeObject(Content),
            UserID = session.GetUserID(),
            GroupID = session.GetGroupID(),
            CompanyID = session.GetCompanyID(),
            OEMID = session.GetOEMID(),
        };
    }
}