using ERP.Enums;
using ERP.Models.Api.Logistics.EMS;
using ERP.Models.Product;
using ERP.Models.Template;
using Newtonsoft.Json;

namespace ERP.Models.View.Products.Template;

public class SketchViewModel
{
    public SketchViewModel(SketchTemplateModel model)
    {
        Id = model.ID;
        Name = model.Name;

        var values = model.Content;

        var arr = JsonConvert.DeserializeObject<List<string>>(values);

        List<Dictionary<string, string>> temp = new();
        if (arr != null)
        {
            foreach (var item in arr)
            {
                temp.Add(new()
                {
                    { "value", item }
                });
            }
        }

        Content = temp;
        Language = model.Language;
        LanguageSign = model.LanguageSign;
    }

    public string Language { get; set; }

    public string LanguageSign { get; set; }

    public List<Dictionary<string, string>> Content { get; set; }

    public string Name { get; set; }

    public int Id { get; set; }
}