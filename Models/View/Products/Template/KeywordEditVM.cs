﻿using System.ComponentModel.DataAnnotations;
using ERP.Enums;
using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;

namespace ERP.ViewModels.Template;

public class KeywordEditVM : LanguageViewModel
{
    public int ID { get; set; } = 0;

    [Comment("卖点模板名称"), Required(ErrorMessage = "请填写模板名称")]
    [StringLength(255, ErrorMessage = "模板名称最大长度不可超过255")]
    public string Name { get; set; } = string.Empty;

    [Comment("关键词模板内容"), Required(ErrorMessage = "请填写关键词内容")]
    [StringLength(255, ErrorMessage = "关键词最大长度不可超过255")]
    public string Value { get; set; } = string.Empty;

    [Required(ErrorMessage = "请选择语言")]
    public new Languages LanguageSign { get; set; } = Languages.Default;
    public string? UpdatedAtTicks { get; set; }
}