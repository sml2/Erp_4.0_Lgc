using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.View.Product.Category;

public class EditVM
{
    public int ID { get; set; } = 0;

    [Comment("卖点模板名称"), Required(ErrorMessage = "请填写分类名称")]
    [StringLength(255, ErrorMessage = "分类名称最大长度不可超过255")]
    public string Name { get; set; } = string.Empty;

    public int Pid { get; set; }

    public string? UpdatedAtTicks { get; set; }
    
    public decimal Money { get; set; }
}