﻿using System.ComponentModel.DataAnnotations;
using Common;

namespace ERP.ViewModels.Task;

public class DeleteDto
{
    [Required(ErrorMessage = "缺少参数: ID")]
    public int ID { get; set; }
    
    public TaskDeleteMode Mode { get; set; }
}


public enum TaskDeleteMode {
    /// <summary>
    /// 包含产品
    /// </summary>
    Product = 1,
    /// <summary>
    /// 只任务
    /// </summary>
    Task = 2
}