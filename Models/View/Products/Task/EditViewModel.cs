﻿using System.ComponentModel.DataAnnotations;
using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;

namespace ERP.ViewModels.Task;

public class EditViewModel
{
    public int ID { get; set; } = 0;

    [Comment("采集任务名称"), Required(ErrorMessage = "请填写模板名称")]
    [StringLength(255, ErrorMessage = "模板名称最大长度不可超过255")]
    public string Name { get; set; } = string.Empty;

    [Comment("采集平台"), Required(ErrorMessage = "缺少采集平台")]
    public int PlatformId { get; set; } 

    [Comment("采集平台名称"), Required(ErrorMessage = "缺少采集平台名称")]
    [StringLength(255, ErrorMessage = "采集平台名称名称最大长度不可超过255")]
    public string PlatformName { get; set; } = string.Empty;

    public int? FirstCategoryId { get; set; } = 0;
    public int? SecondCategoryId { get; set; } = 0;

    public DateTime? UpdatedAt { get; set; }
}