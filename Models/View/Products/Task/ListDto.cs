﻿namespace ERP.Models.View.Product.Task;

public class ListDto : PageDto
{
    public string? Name { get; set; }

    public int PlatformId { get; set; } = 0;
}