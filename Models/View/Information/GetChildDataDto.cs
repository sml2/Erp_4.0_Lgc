using ERP.Models.View.Information;
using ERP.Models.View.Product;

namespace ERP.ViewModels.Information;

public class GetChildDataDto : ValidityCompany
{
    public int? CompanyId { get; set; }
    public int? OemId { get; set; }
}