﻿namespace ERP.ViewModels.User;

using Interface.Rule;
using Models.DB.Setting;
using Models.Rule;
using Models;

public class AuthItemData
{
    [NJI,SJI]
    public int IntID;
    
    public string id;
    public string label;
    public string Module;
    public long PermissionFlag;

    public AuthItemData(IHas hasRule, string label)
    {
        this.label = label;
        Module = hasRule.Key;
        PermissionFlag = hasRule.Value;
        Adapte();
    }
    public AuthItemData(IHas hasRule, int intId, string label):this(hasRule, label)
    {
        IntID = intId;
        Adapte();
    }
    public AuthItemData(Menu menu) : this(menu, menu.ID, menu.Name)
    {
    }
    public IEnumerable<AuthItemData> children = Enumerable.Empty<AuthItemData>();
    public void Add(AuthItemData authItemData)
    {
        var Children = children.Any() ? (List<AuthItemData>)children : (List<AuthItemData>)(children = new List<AuthItemData>());
        Children.Add(authItemData);
    }

    private void Adapte()=> id = IntID == 0 ? $"{Module}.{PermissionFlag}" : IntID.ToString();
}

