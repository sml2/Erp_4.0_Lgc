﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.ViewModels.User;
public class LoginViewModel
{
    [Required, Display(Name = "账号")]
    public string UserName { get; set; }=string.Empty;
    [Required, Display(Name = "密码")]
    public string PassWord { get; set; } = string.Empty;

    public bool RememberMe { get; set; } = true;
}


