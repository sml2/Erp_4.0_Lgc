namespace ERP.Models.DB.Users;

public class UserInfoSelectStruct
{
    public UserInfoSelectStruct(int id, string username)
    {
        Id = id;
        Username = username;
    }

    public int Id { get; set; }
    public string Username { get; set; }
}