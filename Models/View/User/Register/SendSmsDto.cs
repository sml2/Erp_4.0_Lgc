namespace ERP.Models.View.User.Register;

public class SendSmsDto
{
    [NJP("code")]
    public string? Code { get; set; }

    [NJP("key")]
    public string? Key { get; set; }

    [NJP("mobile")]
    public string Mobile { get; set; }
}