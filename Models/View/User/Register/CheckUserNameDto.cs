namespace ERP.Models.View.User.Register;

public class CheckUserNameDto
{
 [NJP("username")]
 public string Username { get; set; }   
}