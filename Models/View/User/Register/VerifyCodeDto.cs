namespace ERP.Models.View.User.Register;

public class VerifyCodeDto
{
    public string Code { get; set; }
    public string Key { get; set; }
}