namespace ERP.Models.View.User.Register;

public class GetCodeDto
{
    [NJP("mobile")]
    public string Mobile { get; set; }
}