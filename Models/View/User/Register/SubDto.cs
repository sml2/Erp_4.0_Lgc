namespace ERP.Models.View.User.Register;

public class SubDto
{
    public string Code { get; set; }
    public string Key { get; set; }

    public RegisterForm Register { get; set; }
}

public class RegisterForm
{
    [NJP("username")]
    public string Username { get; set; }

    [NJP("company_name")]
    public string? CompanyName { get; set; }

    [NJP("mobile")]
    public string Mobile { get; set; }

    [NJP("yzm")]
    public string Yzm { get; set; }

    [NJP("password")]
    public string Password { get; set; }

    [NJP("check_password")]
    public string CheckPassword { get; set; }

    [NJP("InviteCode")]
    public string? InviteCode { get; set; }

    [NJP("KwCode")]
    public string? kwCode { get; set; }
}