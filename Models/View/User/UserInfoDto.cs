using ERP.Extensions;

namespace ERP.Models.View.User;

public class UserInfoDto
{
    public UserInfoDto(DB.Identity.User user)
    {
        Username = user.UserName;
        Truename = user.TrueName;
        UserId = user.Id;
        GroupId = user.GroupID;
        CompanyId = user.CompanyID;
        OemId = user.OEMID;
    }

    public UserInfoDto(ISession session)
    {
        Username = session.GetUserName();
        Truename = session.GetTrueName();
        UserId = session.GetUserID();
        GroupId = session.GetGroupID();
        CompanyId = session.GetCompanyID();
        OemId = session.GetOEMID();
    }

    public int? UserId { get; set; }
    public int? GroupId { get; set; }
    public int? CompanyId { get; set; }
    public int? OemId { get; set; }
    public string? Username { get; set; }
    public string? Truename { get; set; }
}