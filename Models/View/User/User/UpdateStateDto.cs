using ERP.Models.Abstract;

namespace ERP.Models.View.User.User;

public class UpdateStateDto
{
    public int CompanyId { get; set; }

    public BaseModel.StateEnum State { get; set; }
}