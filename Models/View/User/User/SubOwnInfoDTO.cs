﻿using ERP.Models.View.Product;
using ERP.ViewModels;
using System.ComponentModel.DataAnnotations;
using static ERP.Models.Abstract.BaseModel;

namespace ERP.Models.View.User.User
{
    public class User : PageDto
    {
        public bool IsDistribution { get; set; }//分销账号
        public int OemId { get; set; }//商标

        public int Id { get; set; }
        public int RuleId { get; set; }//权限组 改为了 角色 对应 ruleTemplate表.id 
        public int GroupId { get; set; } = 0;//用户组
        public string? Mobile { get; set; }//手机
        [Required]
        public string UserName { get; set; } = string.Empty;//用户名
        [Required]
        public string TrueName { get; set; } = string.Empty;//用户姓名
        public string? Password { get; set; }//输入密码  添加
        public string? Newpassword { get; set; }//输入新密码  修改
        public string? CheckPassword { get; set; }//输入密码
        public string? Email { get; set; } = string.Empty;//邮箱
        public StateEnum State { get; set; }//状态
    }

    public class Company : User
    {
        
        /// <summary>
        /// 账户有效期
        /// </summary>
        public DateTime Duration { get; set; }
        public string CompanyName { get; set; } = string.Empty;//公司名称
        public int Dredge { get; set; }//开通时长 int 月
        public int IsNegative { get; set; }//对公钱包
        public int IsOpenStore { get; set; }//店铺限制
        public int OpenStoreNum { get; set; }//店铺数量
        public int IsOpenUser { get; set; }//内部员工数量
        public int OpenUserNum { get; set; }//可开通员工数

        //前端 注释了
        //public string? Mobile { get; set; }//手机
        public int Source { get; set; }//积分账号
        public int Recharge { get; set; }//充值金额
        public decimal Balance { get; set; }//账户余额

        //不知道干啥的
        public int Allow { get; set; }//???

        public int Type { get; set; }//应该没有用 2

    }
}
