﻿using ERP.Models.DB.Users;
using ERP.Models.View.Product;
using ERP.ViewModels;

using static ERP.Models.Abstract.BaseModel;

namespace ERP.Models.View.User.User;

public class GetOwnListDTO : PageDto
{
    public StateEnum? State { get; set; }
    public int? RuleId { get; set; }
    public bool? IsDistribution { get; set; } 
    public string? Name { get; set; } 
    public string? Mobile { get; set; } 
    public IsAgencyEnum? IsAgency { get; set; } 
    public int? OemId { get; set; } 
    public string[]? Validity { get; set; } 
    public DB.Users.Company.Types Type { get; set; } 
}

public enum IsAgencyEnum
{
    None,
    No,
    Yes
}

