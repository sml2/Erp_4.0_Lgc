namespace ERP.Models.View.User.User;

public class BindMobileCodeVM
{
    [NJP("mobile")]
    public string Mobile { get; set; }
    [NJP("txyzm")]
    public string Captcha { get; set; }
    [NJP("yzm")]
    public string? Yzm { get; set; }
}