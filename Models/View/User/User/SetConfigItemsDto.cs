using ERP.Enums.Rule;

namespace ERP.Models.View.Users;

public class SetConfigItemsDto
{
    public int Id { get; set; }
    public Dictionary<string, object> Config { get; set; }
    
    public Modes Mode { get; set; }
}