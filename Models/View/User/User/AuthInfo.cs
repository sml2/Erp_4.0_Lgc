﻿using System.ComponentModel.DataAnnotations;
using ERP.Enums.Rule;

namespace ERP.Models.View.Users;

public class InfoItem
{
    [Required, Display(Name = "模块名称")]
    public string Module { get; set; } = string.Empty;
    [Required, Display(Name = "权限位")]
    public long PermissionFlag { get; set; }
}
public class AuthInfo
{
    [Required, Display(Name = "模板ID")]
    public int ID { set; get; }
    [Required, Display(Name = "权限数据")]
    public List<InfoItem> Data { set; get; } = new();
    
    public Modes Mode { get; set; }
}

