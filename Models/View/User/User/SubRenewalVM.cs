﻿using static ERP.Models.DB.Users.Company;

namespace ERP.Models.View.User.User;

public class SubRenewalVM
{
    public int Id { get; set; }
    public int CompanyId { get; set; }
    // public int Month { get; set; }
    
    public DateTime Duration { get; set; }
}


