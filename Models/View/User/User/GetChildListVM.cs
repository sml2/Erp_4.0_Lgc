﻿using ERP.Models.View.Product;
using static ERP.Models.Abstract.BaseModel;

namespace ERP.Models.View.User.User
{
    public class GetChildListVM : PageDto
    {
        public StateEnum? State { get; set; }
        public int? GroupId { get; set; }
        public int? RuleId { get; set; }
        public int? OemId { get; set; }
        public string? Name { get; set; }
        
    }
}
