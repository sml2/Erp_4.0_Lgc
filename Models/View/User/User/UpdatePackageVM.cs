﻿namespace ERP.Models.View.User.User;

public class PackageToBusinessVM
{
    public int Id { get; set; }
    public int RuleId { get; set; }
    public int CompanyId { get; set; }
}


