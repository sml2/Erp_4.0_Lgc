﻿using System.ComponentModel.DataAnnotations;
using static ERP.Models.Abstract.BaseModel;

namespace ERP.Models.View.User.User
{
    public class EditOwnInfoVM
    {
        public int Id { get; set; }
        public int RuleId { get; set; }//权限组
        [Required]
        public string UserName { get; set; } = string.Empty;//用户名
        [Required]
        public string TrueName { get; set; } = string.Empty;//用户姓名
        [Required]
        public string? Newpassword { get; set; } = string.Empty;//输入密码
        [Required]
        public string? CheckPassword { get; set; } = string.Empty;//输入密码
        [Required]
        public string? Email { get; set; }=string.Empty;
        public StateEnum State { get; set; }//状态
    }
}
