namespace ERP.Models.View.User.User;

public class CopyRuleDto
{
    public int Id { get; set; }
    public string Name { get; set; }
}