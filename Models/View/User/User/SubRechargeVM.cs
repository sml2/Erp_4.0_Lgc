﻿using System.ComponentModel.DataAnnotations;

namespace ERP.Models.View.User.User
{
    public class SubRechargeVM
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public int OemId { get; set; }
        public decimal Recharge { get; set; }
        public string? Remark { get; set; } = string.Empty;
    }
}
