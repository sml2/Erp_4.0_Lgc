namespace ERP.Models.View.User.User;

public class UpdateAgencyDto
{
    public int CompanyId { get; set; }
    public IsAgencyEnum IsAgency { get; set; }
}