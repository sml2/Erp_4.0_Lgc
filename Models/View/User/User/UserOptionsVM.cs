namespace ERP.Models.View.User.User;

public class UserOptionsVM
{
    public UserOptionsVM(DB.Identity.User model)
    {
        TrueName = model.TrueName;
        Id = model.ID;
    }

    [NJP("truename")]
    public string TrueName { get; set; }

    [NJP("id")]
    public int Id { get; set; }
}