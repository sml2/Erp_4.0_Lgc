using ERP.Enums.View;
using ERP.Models.Abstract;

namespace ERP.Models.View.User.User;

public class OwnListVM
{
    public string? Validity { get; set; }
    public DB.Users.Company.Types Type { get; set; }
    public DB.Users.Company.Sources Source { get; set; }
    public int CompanyId { get; set; }
    public DateTime CreatedAt { get; set; }
    public int Id { get; set; }
    public bool IsDistribution { get; set; }
    public int OemId { get; set; }
    public int RuleId { get; set; }
    public BaseModel.StateEnum State { get; set; }
    public string Truename { get; set; }
    public string Username { get; set; }
    
    public string OemName { get; set; }
}