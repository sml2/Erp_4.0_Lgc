﻿using static ERP.Models.DB.Users.Company;

namespace ERP.Models.View.User.User;

public class UpdatePackageVM
{
    public int Id { get; set; }
    public int CompanyId { get; set; }
    public Types PackageType { get; set; }
}


