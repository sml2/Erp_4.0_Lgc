﻿using System.ComponentModel.DataAnnotations;

namespace ERP.Models.View.User.User
{
    public class SubConfigVM
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; } = string.Empty;
        public bool IsChangeCompany { get; set; }
        public bool IsOpenStore { get; set; }
        public int OpenStoreNum { get; set; }
        public bool IsOpenUser { get; set; }
        public int OpenUserNum { get; set; }
        public int ShowOemId { get; set; }
        public bool IsDistribution { get; set; }
        public bool IsNegative { get; set; }
        public bool IsShowRate { get; set; }
        public int? Discount { get; set; }

        public int PurchaseRate { get; set; }
        public int PurchaseRateType { get; set; }
        public int PurchaseRateNum { get; set; }
        public int PurchaseRateUnit { get; set; }

        public int WaybillRate { get; set; }
        public int WaybillRateType { get; set; }
        public int WaybillRateNum { get; set; }
        public int WaybillRateUnit { get; set; }

        // 暂时忽略
        //public string? PriceConfig { get; set; }


    }
}
