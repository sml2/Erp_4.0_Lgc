using ERP.Enums.Rule;
using ERP.Enums.View.Rule;

namespace ERP.Models.View.Users;

public class GetConfigItemsVM
{
    public int rid;
    public int mid;
    public ConfigRanges range;
    public Modes mode;
}