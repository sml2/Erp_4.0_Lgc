using ERP.Models.Abstract;

namespace ERP.Models.View.User.User;

public class SetUserStateDto
{
    public int UserId { get; set; }

    public BaseModel.StateEnum State { get; set; }
}