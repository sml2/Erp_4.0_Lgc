namespace ERP.Models.View.User.User;

public class SpecialOwnListVm : OwnListVM
{
    public decimal Balance { get; set; }
    public decimal Balance2 { get; set; }
    public int Integral { get; set; }
    public string TypeName { get; set; }
    public string Discount { get; set; }
    public string Mobile { get; set; }
    public bool PhoneNumberConfirmed { get; set; }
    public object? FirstLoginWeb { get; set; }
    public object? FirstLoginExe { get; set; }
    public bool Agency { get; set; }
    public string? FirstRechargeDate { get; set; }
    public string? FeferrerKwSign { get; set; }
}