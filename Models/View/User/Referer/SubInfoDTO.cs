﻿using System.ComponentModel.DataAnnotations;

namespace ERP.Models.View.User.Referer
{
    public class SubInfoDTO
    {
        [Required]
        public string UserName { get; set;} = string.Empty;
        public string? CompanyName { get; set;}
        public int OemId { get; set;}
        [Required]
        public string Password { get; set;} = string.Empty;
        public string? CheckPassword { get; set;}
        public string Mobile { get; set;} = String .Empty;
        public string KwCode { get; set;} = String .Empty;
    }
}
