﻿using ERP.Models.View.Product;
using ERP.ViewModels;

namespace ERP.Models.View.User.Referer
{
    public class GetListDTO : PageDto
    {
        public string? Name { get; set; }
        public bool? IsAgency { get; set; }
        public string? UserName { get; set; }
        public string? Mobile { get; set; }
    }
}
