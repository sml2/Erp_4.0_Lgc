using ERP.Models.Abstract;

namespace ERP.Models.View.User.Group;

public class GetGroupListVm
{
    public GetGroupListVm(int id, string name, BaseModel.StateEnum state)
    {
        ID = id;
        Name = name;
        State = state;
    }

    public int ID { get; set; }
    public string Name { get; set; }
    public BaseModel.StateEnum  State { get; set; }
}