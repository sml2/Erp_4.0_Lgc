﻿using ERP.Models.View.Product;
using ERP.ViewModels;
using static ERP.Models.Abstract.BaseModel;

namespace ERP.Models.View.User.Group
{
    public class GetListDTO : PageDto
    {
        public string? Name { get; set; }
        public StateEnum? State { get; set; }
    }
}
