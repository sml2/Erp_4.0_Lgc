﻿using System.Collections.ObjectModel;

namespace ERP.ViewModels.Logistics
{
    public class LogisticsInit
    {
        public LogisticsRule Rule { get; set; } = new();
        public IList<UsedLogisticsSimple> Logistics { get; set; } = new List<UsedLogisticsSimple>();
    }
}
