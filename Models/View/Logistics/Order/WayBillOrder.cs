﻿namespace ERP.ViewModels.Logistics.Order
{
    public class WayBillOrder
    {
        public string? ReceiverNation { get; set; }
        /// <summary>
        /// 收货人所在州/省/地区
        /// </summary>
        public string? ReceiverProvince { get; set; }
        /// <summary>
        /// 收货人所在城市
        /// </summary>
        public string? ReceiverCity { get; set; }
        /// <summary>
        /// 收货人所在区
        /// </summary>
        public string? ReceiverDistrict { get; set; }
        /// <summary>
        /// 收货人详细地址
        /// </summary>
        public string? ReceiverAddress { get; set; }
        /// <summary>
        /// 收货人邮编
        /// </summary>
        public string? ReceiverZip { get; set; }
        /// <summary>
        /// 收货人姓名
        /// </summary>
        public string? ReceiverPhone { get; set; }
        /// <summary>
        /// 收货人姓名
        /// </summary>
        public string? ReceiverName { get; set; }
        /// <summary>
        /// 收货人邮箱
        /// </summary>
        public string? ReceiverEmail { get; set; }
    }
}
