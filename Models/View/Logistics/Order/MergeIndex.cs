﻿namespace ERP.Models.View.Logistics.Order;
using Enums.Orders;
using Extensions;

public class MergeIndex
{
    public MergeIndex(int ID,string? Nation,string? Url, string? OrderNo, int ProductNum, string? Store, Enums.Orders.BITS Bits, IsMains IsMain)
    {
        id = ID;
        nation = Nation?? "暂无";
        url = Url ?? "暂无";
        orderNo = OrderNo ?? "暂无";
        productNum = ProductNum;
        store = Store ?? "暂无";
        orderState = Bits.GetDescription();
        isMain = IsMain;
    }
    public int id { get; set; }
    public string nation { get; set; }
    public string url { get; set; }
    public string orderNo { get; set; }
    public int productNum { get; set; }
    public string store { get; set; }
    public string orderState { get; set; }
    public IsMains isMain { get; set; }
}
