﻿namespace ERP.ViewModels.Logistics
{
    public class LogisticsRule
    {
        public bool LogisticAdd { get; set; }
        public bool LogisticEdit { get; set; }
        public bool LogisticDelete { get; set; }
        public int IsDistribution { get; set; }
    }
}
