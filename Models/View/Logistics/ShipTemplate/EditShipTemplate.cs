﻿using Microsoft.EntityFrameworkCore;

namespace ERP.ViewModels.Logistics
{
    public class EditShipTemplate
    {
        public int Id { get; set; }=0;
        public bool? Battery { get; set; }
        public string CustomsCode { get; set; } = string.Empty;
        public string Declare_en { get; set; } = string.Empty;
        public string Declare_zh { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public int Height { get; set; }
        public int Length { get; set; }
        public string Name { get; set; }=string.Empty;
        public Models.Abstract.BaseModel.StateEnum State { get; set; }
        public string? Tag { get; set; }
        public int Weight { get; set; }
        public int Width { get; set; }
        public string? IOSS { get; set; }
    }
}
