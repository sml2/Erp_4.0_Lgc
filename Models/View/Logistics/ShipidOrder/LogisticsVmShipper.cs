﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace ERP.Models.View.Logistics.ShipidOrder
{
    public class LogisticsVmShipper
    { 
        public int ID { get; set; }
        /// <summary>
        /// 寄件人的姓名
        /// </summary>
        [Required,Comment("寄件人姓名")]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// 电话
        /// </summary>

        [Required,Comment("电话")]
        public string Phone { get; set; } = string.Empty;

        /// <summary>
        /// 邮编
        /// </summary>

        [Required,Comment("邮编")]
        public string Postcode { get; set; } = string.Empty;

        /// <summary>
        /// 邮箱
        /// </summary>

        [Required,Comment("邮箱")]
        public string Email { get; set; } = string.Empty;

        /// <summary>
        /// 地址
        /// </summary>

        [Required,Comment("地址")]
        public string Address { get; set; }=string.Empty;

        /// <summary>
        /// 省
        /// </summary>

        [Required,Comment("省")]
        public string Province { get; set; } = string.Empty;

        /// <summary>
        /// 市
        /// </summary>

        [Required,Comment("市")]
        public string City { get; set; } = string.Empty;

        /// <summary>
        /// 区
        /// </summary>

        [Required,Comment("区")]
        public string District { get; set; } = string.Empty;

        /// <summary>
        /// 公司
        /// </summary>

        [Comment("公司")]
        public string? Company { get; set; }

        /// <summary>
        /// 最后一条运单时间
        /// </summary>

        [Required,Comment("最后一条运单时间")]
        public DateTime LastTime { get; set; }
    }
}
