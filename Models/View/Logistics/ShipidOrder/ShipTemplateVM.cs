﻿namespace ERP.ViewModels.Logistics.ShipidOrder
{
    public class ShipTemplateVM
    {
        public int[] Ids { get; set; }= Array.Empty<int>();
        public decimal Declare { get; set; }
        public string DeclareUnit { get; set; }=string.Empty;
    }
}
