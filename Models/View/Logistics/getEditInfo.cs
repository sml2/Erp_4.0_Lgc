using ERP.Attributes.Logistics;
using ERP.Enums.Logistics;
using ERP.Models.DB.Logistics;
using ERP.Models.Logistics.ViewModels.Parameter;
using System.ComponentModel.DataAnnotations;

namespace ERP.ViewModels.Logistics
{
    public class EditInfo
    {
        public int ID { get;set; }
        public Sources Source { get; set; }
        [Required]
        public string CustomizeName { get; set; } = string.Empty;
        public string? Remark { get; set; }
        public bool State { get; set; }
        [Required]
        public string Param { get; set; } = string.Empty;
        public Models.PlatformData.LogisticsData? Logistics{ get; set; }

    }

    public class GetEditInfo:EditInfo {
        public IEnumerable<DataConfigVm>? Config { get; set; }
    }  
}
