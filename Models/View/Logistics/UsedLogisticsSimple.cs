﻿namespace ERP.ViewModels.Logistics
{
    public class UsedLogisticsSimple
    {
        public int id { get;set;}
        public string name {  get;set;} = string.Empty;
    }

    public class CarrierNameOfCountry
    {
        public string? Country { get; set; }
    }
}
