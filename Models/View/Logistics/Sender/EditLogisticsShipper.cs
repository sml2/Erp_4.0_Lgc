﻿using Microsoft.Build.Framework;

namespace ERP.ViewModels.Logistics
{
    public class EditLogisticsShipper
    {
        public int ID { get; set; }
        public int GroupId { get; set; }
        public int UserId { get; set; }
        public int OemId { get; set; }
        [Required]
        public string Name { get; set; } = string.Empty;
        [Required]
        public string Phone { get; set; } = string.Empty;
        [Required]
        public string Postcode { get; set; } = string.Empty;
        [Required]
        public string Email { get; set; } = string.Empty;
        [Required]
        public string Address { get; set; } = string.Empty;
        [Required]
        public string Province { get; set; } = string.Empty;
        [Required]
        public string City { get; set; } = string.Empty;
        [Required]
        public string District { get; set; } = string.Empty;
        [Required]
        public string Company { get; set; } = string.Empty;
        public DateTime LastTime {  get; set; }
    }
}
