using ERP.Enums.Logistics;
using Microsoft.EntityFrameworkCore;

namespace ERP.ViewModels.Logistics
{
    public class WayBills
    {
        public States? WaybillState { get; set; }

        /// <summary>
        /// 来源公司
        /// </summary>
        public int? companyId { get; set; }
        public string? OrderNo { get; set; }
        public string? WaybillOrder { get; set; }
        public string? Tracking { get; set; }
        public string? Express { get; set; }
        public int? LogisticId { get; set; }
        public int? NationId { get; set; }
        public int? StoreId { get; set; }
        public int WarehouseId { get; set; } = 0;
        public string? PurchaseTrackingNumber { get; set; }
        public string? PurchaseOrderNo { get; set; }

        public int? MergeID { get; set; }
    }

}


