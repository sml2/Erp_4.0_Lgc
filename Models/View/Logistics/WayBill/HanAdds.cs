using ERP.Models.Api.Logistics;
using ERP.Models.View.Order.AmazonOrder;
using System.ComponentModel.DataAnnotations;

namespace ERP.Models.View.Logistics.WayBill;

public enum SendType
{
    None = 0,
    Auto,
    Report,
    Hand,
}

public class HanAddParam
{
    /// <summary>
    /// 订单信息
    /// </summary>
    public OrderSimpleInfo OrderInfo { get; set; } = new();

    public List<ProgressValue> OrderProgress { get; set; } = new List<ProgressValue>();

    public SyncToAmazon SyncAmazon { get; set; } = new();

    public int? MergeID { get; set; }

    public SendType Type { get; set; }

}

public class OrderSimpleInfo
{   
    /// <summary>
    /// 订单主键
    /// </summary>
    public int id { get; set; }    
      
    public List<GoodInfo> GoodInfos { get; set; } = new();

    public decimal CarriageNum { get; set; }//运费

    public string CarriageUnit { get; set; } = string.Empty;

    public string? Remark { get; set; }

    [Required]
    public string LogisticName { get; set; } = string.Empty;

    /// <summary>
    /// 物流的订单号
    /// </summary>
    public string WaybillOrder { get; set; } = string.Empty;

    /// <summary>
    /// 物流运单号
    /// </summary>
    public string Express { get; set; } = string.Empty;

    /// <summary>
    /// 物流跟踪号
    /// </summary>
    public string Tracking { get; set; } = string.Empty;

    /// <summary>
    /// 系统运单号
    /// </summary>
    public string? OrderNo { get; set; } = string.Empty;

    /// <summary>
    /// 缩略图
    /// </summary>
    public string? Url { get; set; }
   
}

public class HanAddsInfo
{
    

    public List<int> WarehouseIds { get; set; } = new();      
}
public class GoodsData
{
    public string ID { get; set; } = string.Empty;
    //[Required]
    public string FromURL { get; set; } = string.Empty;
    public string? ImageUrl { get; set; }
    //[Required]
    public string Name { get; set; } = string.Empty;
    public int QuantityOrdered { get; set; }
    public int SendNum { get; set; }//发货数
    //[Required]
    public string Sku { get; set; } = string.Empty;

    public int StockOutNum { get; set; } //出库数
}
