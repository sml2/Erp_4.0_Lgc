﻿using ERP.Enums.Logistics;
using ERP.Models.DB.Logistics;
using ERP.Models.Setting;
using Newtonsoft.Json;

namespace ERP.Models.View.Logistics.WayBill;

public class ShowInfoVm
{
    public ShowInfoVm(InfoItem info, List<WaybillProblems>? problem, IList<Unit>? units, Dictionary<int, string> dictionary, object waybillLog, object rate)
    {
        Info = info;
        Problem = problem??new();
        Units = units;
        WaybillState = dictionary;
        WaybillLog = waybillLog;
        Rate = rate;
    }
    public InfoItem Info { get; }
    public List<WaybillProblems> Problem { get; }
    public IList<Unit>? Units { get; }
    public Dictionary<int, string> WaybillState { get; }
    public object WaybillLog { get; }
    public object Rate { get; }
}
public class InfoItem
{
    public int Id { get; set; }
    public States WaybillState { get; }

    public int platformsID { get; }
    public string Logistic { get; }
    public string CarriageUnit { get; }
    public decimal CarriageNum { get; }
    public decimal CarriageBase { get; }
    public decimal CarriageOther { get; }
    public string? OrderNo { get; }
    public string Express { get; }
    public string WaybillOrder { get; }
    public string Tracking { get; }
    public string? Remark { get; }
    public VerifyStates IsVerify { get; }
    public List<Models.Api.Logistics.GoodInfo>? ProductInfo { get; }

    public InfoItem(Models.DB.Logistics.Waybill info, string? CurrencyCode)
    {
        Id = info.ID;
        WaybillState = info.State;
        Logistic = info.Logistic.Name;
        platformsID = info.Logistic.GetID();
        CarriageUnit = CurrencyCode ?? info.CarriageNum.Raw.CurrencyCode;
        CarriageNum = info.CarriageNum.Money.To(CarriageUnit);
        CarriageBase = info.CarriageBase.Money.To(CarriageUnit);
        CarriageOther = info.CarriageOther.Money.To(CarriageUnit);
        OrderNo = info.OrderNo;
        Express = info.Express;
        WaybillOrder = info.WaybillOrder??"暂无";
        Tracking = info.Tracking ?? "暂无";
        Remark = info.Remark;
        IsVerify = info.IsVerify;
        //WaybillRate = info.WaybillRate;
        ProductInfo = JsonConvert.DeserializeObject<List<Api.Logistics.GoodInfo>>(info.ProductInfo??string.Empty);
    }
}