﻿using ERP.Models.DB.Orders;

namespace ERP.ViewModels.Logistics
{
    public class WayBillAddLogNew
    {
        public int WayBillId { get; set; }
        public string Action { get; set; } = string.Empty;
        public Refund OrederId { get; set; } = new();
        public int PlatFormId { get; set; }
        public string truename { get; set; } = string.Empty;
        public string UserName { get; set; } = string.Empty;
        public int UserId { get; set; }
        public int CompanyId { get; set; }
        public int GroupId { get; set; }
        public int OemId { get; set; }
    }
}
