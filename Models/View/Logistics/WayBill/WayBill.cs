

using ERP.Models.Api.Logistics;
using ERP.Models.View.Logistics.WayBill;
using System.ComponentModel.DataAnnotations;

namespace ERP.ViewModels.Logistics;

public class WayBillVM
{
    public int ID { get; set; }
    public string? Problem { get; set; }
    public DateTime? Time { get; set; }
    public string? UserName { get; set; }
    public int? WaybillCompanyId { get; set; }
}

public class CancleWayBillParam
{
    [Required]
    public int ID { get; set; }    
}

public class WayBilEditVM
{
    [Required]
    public int Id { get; set; }
    [Required]
    public decimal CarriageBase { get; set; }
    [Required]
    public decimal CarriageOther { get; set; }
    [Required]
    public decimal CarriageNum { get; set; }
    [Required]
    public string CarriageUnit { get; set; } = string.Empty;
    [Required]
    public string Express { get; set; } = string.Empty;
    [Required]
    public decimal WaybillRate { get; set; }
    public bool IsVerify { get; set; }
    [Required]
    public string WaybillOrder { get; set; } = string.Empty;
    public string? Tracking { get; set; }
    [Required]
    public double Rate { get; set; }
    public string? Remark { get; set; }
    [Required]
    public string Logistic { get; set; } = string.Empty;
    [Required]
    public string OrderNo { get; set; } = string.Empty;
    public Enums.Logistics.States WaybillState { get; set; }
    [Required]
    public List<GoodInfo> ProductInfo { get; set; } = new();
}

public class SendProblemVM
{
    [Required]
    public int Id { get; set; }
    [Required]
    public string Problem { get; set; } = string.Empty;
}

