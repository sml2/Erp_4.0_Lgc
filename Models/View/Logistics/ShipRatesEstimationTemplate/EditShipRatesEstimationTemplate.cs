using ERP.Models.Logistics;
using System.ComponentModel.DataAnnotations;

namespace ERP.ViewModels.Logistics.ShipRatesEstimationTemplate
{
    public class EditShipRatesEstimationTemplate
    {
        public int? ID { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        [Required]
        public string Name { get; set; } = string.Empty;
        /// <summary>
        /// 计算方式
        /// </summary>
        public int Mode { get; set; }

    }
    public class data
    {
        public int ID { get; set; }
        [Required]
        public List<TempData> @list { get; set; } = default!;
        [Required]
        public List<ShipNation> nation { get; set; } = default!;
    }
    public class ShipList
    {
        public decimal continuedUnitCost { get; set; }
        public decimal continuedUnitWeight { get; set; }
        public decimal endWeight { get; set; }
        public decimal minCost { get; set; }
        public decimal minWeight { get; set; }
        public decimal registrationCost { get; set; }
        public decimal startWeight { get; set; }
    }

    public class ShipNation
    {
        public string? createdAt { get; set; }
        public int id { get; set; }
        public string name { get; set; } = string.Empty;
        public string sfc { get; set; } = string.Empty;
        public string @short { get; set; } = string.Empty;
        public string? updatedAt { get; set; }
        public string? updatedAtTicks { get; set; }
    }
}
