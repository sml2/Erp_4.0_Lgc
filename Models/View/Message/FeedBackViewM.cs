﻿using ERP.Models.Message;
using ERP.Models.View.Product;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace ERP.Models.View.Message
{
    public class FeedBackViewM
    {
        public class IndexQuery:PageDto
        {
            public string? title { get; set; } = string.Empty;
            public FeedBack.Types? types { get; set; }
            public FeedBack.States? state { get; set; } 
        }

        public class FeedbackVm
        {
            [Required]
            public string title { get; set; } = string.Empty;
            public FeedBack.Types types { get; set; }
            [Required]
            public string content { get; set; } = string.Empty;
            public List<string>? imgs { get; set; }
            [Required]
            public string captcha { get; set; } = string.Empty;
        }

        public record ProInfoVm(string reply, bool isComplete, int userID, int id);

    }
}
