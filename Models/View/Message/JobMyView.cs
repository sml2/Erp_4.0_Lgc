﻿using ERP.Enums.Message;
using ERP.Models.Message;
using ERP.Models.View.Product;

namespace ERP.Models.View.Message
{
    public class WorkOrderView
    {
        public class WorkOrderListQuery: PageDto
        {
            public string? Title { get; set; } 
            public int? category_id { get; set; }
            public int? progress_id { get; set; }
            public WorkOrder.States? State { get; set; }
            public Commits? Commit { get; set; }
        }
        public class WorkOrderListDto
        {
            public int Id { get; }
            public string Title { get; }
            public string Category { get; }
            public string Progress { get; }
            public int SendNum { get; }
            public Commits Commit { get; }
            public Models.Message.WorkOrder.States State { get; }
            public DateTime DeadLine { get; }
            public DateTime CreatedAt { get; }

            public WorkOrderListDto(int id, string? title, string? category, string? progress, int sendNum, Commits commit, Models.Message.WorkOrder.States state, DateTime deadLine, DateTime createdAt)
            {
                Id = id;
                Title = title ?? string.Empty;
                Category = category ?? string.Empty;
                Progress = progress ?? string.Empty;
                SendNum = sendNum;
                Commit = commit;
                State = state;
                DeadLine = deadLine;
                CreatedAt = createdAt;
            }
        }
        public record WorkOrderAddVm(Commits Commit, DateTime Deadline, string Title, int category_id, int progress_id, string Content);

    }
}
