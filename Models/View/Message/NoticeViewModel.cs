﻿using ERP.Models.View.Product;

namespace ERP.Models.View.Message;
using Enums.Message;
public class NoticeViewModel
{
    public class IndexQuery:PageDto
    {
        public string? Title { get; set; }
        public Enums.Message.States? State { get; set; }
    }
    public class AddVm
    {
        public int? id { get; set; }
        public AddInfos Info { get; set; } = default!;
    }
    public record AddInfos(string[]? datetimerange, string title, string content, Enums.Message.States state, Enums.Message.Sizes size,
   Enums.Message.Types type);
    public class PreviewEdit
    {
        public int Id { get; set; }
        public string title { get; set; } = string.Empty;
        public string content { get; set; } = string.Empty;
        public Types type { get; set; }
        public States state { get; set; }
        public Sizes size { get; set; }
        public DateTime? noticeBegin { get; set; }
        public DateTime? noticeEnd { get; set; }
    }
}
