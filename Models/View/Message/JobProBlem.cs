﻿using ERP.Models.Message;

namespace ERP.Models.View.Message;
public class JobProBlem
{
    public record CategoryEditVm(string Name, WorkOrderCategory.States State);
}
