﻿using ERP.Models.DB.Users;
using System.ComponentModel.DataAnnotations;

namespace ERP.Models.View.Message
{
    public class EmailListViewModel
    {
        [Flags]
        public enum ResetAction
        {
            /// <summary>
            /// 清空邮件
            /// </summary>
            ClearEmail = 1,

            /// <summary>
            /// 重置标记
            /// </summary>
            ResetFlag,
            ClearAndReset,
        }
        public record EmailListQuery(UserEmail.States? State, int? TypeId, string? Name);
        public class AddEmailVm
        {
            [EmailAddress(ErrorMessage = "请输入正确的邮箱地址")]
            public string Address { get; set; } = string.Empty;

            public string? Name { get; set; }
            public string? Nick { get; set; }
            public DB.Users.UserEmail.States State { get; set; }
            public string? Token { get; set; }
            public int TypeId { get; set; }
            public int? Id { get; set; }
        }
        public class EmailListVm
        {
            public int ID { get; set; }
            public string? Name { get; set; }

            public string? Token { get; set; }

            public int TypeId { get; set; }

            public string? Tag { get; set; }

            public DB.Users.UserEmail.States State { get; set; }

            public int Sort { get; set; }

            public string? Address { get; set; }

            public string? Nick { get; set; }

            public int Time { get; set; }

            public string? msg { get; set; }
            public string email { get; set; } = string.Empty;
            public string pop { get; set; } = string.Empty;
            public int? pop_port { get; set; }
            public bool pop_ssl { get; set; }
            public DateTime? updatedAt { get; set; }

        }
    }
}
