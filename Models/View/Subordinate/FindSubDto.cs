namespace ERP.ViewModels.Subordinate;

public class FindSubDto
{
    public int OemId { get; set; }
    public string Username { get; set; }
}