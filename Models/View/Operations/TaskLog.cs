﻿using ERP.Enums.Operations;
using ERP.Models.View.Product;
using Results = ERP.Enums.Operations.Results;

namespace ERP.Models.View.Operations
{
    public class TaskLog
    {
        public record ChangeFlag(int Id, bool Flag);
        public class TaskLogVm:PageDto
        {
            //[NJP("state")]
            //public bool state { get; set; }
            [NJP("currentState")]
            public TypeModuels Type { get; set; }
        }

        public class AddTaskLog
        {
             public TypeModuels Type { get; set; }
              public Results results { get; set; }
             public DateTime endtime { get; set; }
              public string Data { get; set; }=string.Empty;
              public string ResDetail { get; set; } = string.Empty;
        }
    }
}
