# docker部署

## 构建镜像

```bash
# 在项目根目录运行
# latest版本表示为最新, 0.0.1代表具体版本,用于回滚
docker build . -t swr.cn-east-3.myhuaweicloud.com/erp/erp:latest -t swr.cn-east-3.myhuaweicloud.com/erp/erp:v0.0.1

# 将镜像推送至线上
docker push swr.cn-east-3.myhuaweicloud.com/erp/erp:latest swr.cn-east-3.myhuaweicloud.com/erp/erp:v0.0.1
```

## 线上部署

### 初次部署

将项目根目录下的`docker-compose.yml`文件复制一份到服务器任意目录

```bash
# 执行以下命令运行docker
docker compose up -d
```

### 版本更新

```bash
# 首先下载新的镜像
docker pull swr.cn-east-3.myhuaweicloud.com/erp/erp:latest
# 只运行此命令即可更新, 但是该命令会停止服务,下载新镜像,会出现服务空挡, 应首先手动执行下载镜像命令
docker compose up -d
```

### 版本回滚
新版本出现异常时,可以执行以下操作
1. 修改`docker-compose.yml`文件中`image`的版本为正常版本
2. 然后执行`docker compose up -d` 应用更新即可

当代码问题修复后，应将`docker-compose.yml`中的`image`改为`latest`版本