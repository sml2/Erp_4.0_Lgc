﻿START TRANSACTION;

INSERT INTO `Menu` (`ID`, `AlwaysShow`, `Component`, `CreatedAt`, `Desc`, `Hidden`, `MetaJson`, `Module`, `Name`, `ParentId`, `Path`, `PermissionFlag`, `Redirect`, `Sort`, `UpdatedAt`)
VALUES (241, FALSE, 'store/uploadShop/index', TIMESTAMP '2021-11-19 13:38:00', NULL, FALSE, '{"Title":"UploadShop","Icon":"arrow","Roles":[],"AlwaysShow":null,"Hidden":false,"NoCache":null,"KeepAlive":null}', 'StoreMenu', 'UploadShop', 200, 'UploadShop', 128, NULL, 4, TIMESTAMP '2021-11-19 13:38:00');






INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20230220065539_t41', '6.0.5');

COMMIT;

START TRANSACTION;

ALTER TABLE `Resource` MODIFY COLUMN `ID` int NOT NULL;




INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20230227055128_t42', '6.0.5');

COMMIT;

START TRANSACTION;

INSERT INTO `Menu` (`ID`, `AlwaysShow`, `Component`, `CreatedAt`, `Desc`, `Hidden`, `MetaJson`, `Module`, `Name`, `ParentId`, `Path`, `PermissionFlag`, `Redirect`, `Sort`, `UpdatedAt`)
VALUES (161, FALSE, 'setting/resourcePackage/index', TIMESTAMP '2021-11-19 13:38:00', NULL, FALSE, '{"Title":"ResourcePackage","Icon":"arrow","Roles":[],"AlwaysShow":null,"Hidden":false,"NoCache":null,"KeepAlive":null}', 'SettingMenu', 'ResourcePackage', 100, 'resourcePackage', 32768, NULL, 10, TIMESTAMP '2021-11-19 13:38:00');




INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20230227072629_t43', '6.0.5');

COMMIT;

START TRANSACTION;

CREATE TABLE `CompanyResourcePackages` (
    `ID` int NOT NULL AUTO_INCREMENT COMMENT '表索引',
    `Name` longtext CHARACTER SET utf8mb4 NOT NULL COMMENT '资源包名称',
    `Type` int NOT NULL COMMENT '资源包类型',
    `TypeName` longtext CHARACTER SET utf8mb4 NOT NULL COMMENT '资源包类型名称',
    `RemainingQuantity` int NOT NULL COMMENT '剩余量',
    `TotalQuantity` int NOT NULL COMMENT '总量',
    `State` int NOT NULL COMMENT '状态',
    `CreatedAt` Timestamp NOT NULL COMMENT '创建时间',
    `UpdatedAt` Timestamp NOT NULL COMMENT '更新时间',
    `OEMID` int NOT NULL,
    `CompanyID` int NOT NULL,
    CONSTRAINT `PK_CompanyResourcePackages` PRIMARY KEY (`ID`),
    CONSTRAINT `FK_CompanyResourcePackages_Company_CompanyID` FOREIGN KEY (`CompanyID`) REFERENCES `Company` (`ID`) ON DELETE CASCADE,
    CONSTRAINT `FK_CompanyResourcePackages_OEM_OEMID` FOREIGN KEY (`OEMID`) REFERENCES `OEM` (`ID`) ON DELETE CASCADE
) CHARACTER SET=utf8mb4;

CREATE TABLE `MattingPackage` (
    `ID` int NOT NULL AUTO_INCREMENT COMMENT '表索引',
    `Name` longtext CHARACTER SET utf8mb4 NOT NULL COMMENT '资源包名称',
    `Desc` longtext CHARACTER SET utf8mb4 NULL COMMENT '描述',
    `CreatedAt` Timestamp NOT NULL COMMENT '创建时间',
    `UpdatedAt` Timestamp NOT NULL COMMENT '更新时间',
    CONSTRAINT `PK_MattingPackage` PRIMARY KEY (`ID`)
) CHARACTER SET=utf8mb4;

CREATE TABLE `MattingPackageItem` (
    `ID` int NOT NULL AUTO_INCREMENT COMMENT '表索引',
    `Times` int NOT NULL COMMENT '次数',
    `Price` decimal(65,30) NOT NULL COMMENT '金额',
    `State` int NOT NULL COMMENT '状态',
    `CreatedAt` Timestamp NOT NULL COMMENT '创建时间',
    `UpdatedAt` Timestamp NOT NULL COMMENT '更新时间',
    CONSTRAINT `PK_MattingPackageItem` PRIMARY KEY (`ID`)
) CHARACTER SET=utf8mb4;




CREATE INDEX `IX_CompanyResourcePackages_CompanyID` ON `CompanyResourcePackages` (`CompanyID`);

CREATE INDEX `IX_CompanyResourcePackages_OEMID` ON `CompanyResourcePackages` (`OEMID`);

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20230227085242_t44', '6.0.5');

COMMIT;

START TRANSACTION;

CREATE TABLE `ProductUpload` (
    `ID` int NOT NULL AUTO_INCREMENT,
    `ProductJson` longtext CHARACTER SET utf8mb4 NOT NULL COMMENT '上传产品内容',
    `Result` int NOT NULL,
    `Message` longtext CHARACTER SET utf8mb4 NOT NULL COMMENT '具体错误信息',
    `Pid` int NOT NULL,
    `Platform` int NULL COMMENT '平台',
    `CreatedAt` Timestamp NOT NULL COMMENT '创建时间',
    `UpdatedAt` Timestamp NOT NULL COMMENT '更新时间',
    `UserID` int NOT NULL,
    `GroupID` int NOT NULL,
    `CompanyID` int NOT NULL,
    `OEMID` int NOT NULL,
    CONSTRAINT `PK_ProductUpload` PRIMARY KEY (`ID`),
    CONSTRAINT `FK_ProductUpload_Company_CompanyID` FOREIGN KEY (`CompanyID`) REFERENCES `Company` (`ID`) ON DELETE CASCADE,
    CONSTRAINT `FK_ProductUpload_OEM_OEMID` FOREIGN KEY (`OEMID`) REFERENCES `OEM` (`ID`) ON DELETE CASCADE,
    CONSTRAINT `FK_ProductUpload_User_UserID` FOREIGN KEY (`UserID`) REFERENCES `User` (`Id`) ON DELETE CASCADE,
    CONSTRAINT `FK_ProductUpload_UserGroup_GroupID` FOREIGN KEY (`GroupID`) REFERENCES `UserGroup` (`ID`) ON DELETE CASCADE
) CHARACTER SET=utf8mb4 COMMENT='产品上传记录';

CREATE TABLE `ProductUploadLog` (
    `ID` int NOT NULL AUTO_INCREMENT,
    `productUploadID` int NOT NULL,
    `Action` longtext CHARACTER SET utf8mb4 NOT NULL,
    `UserName` longtext CHARACTER SET utf8mb4 NOT NULL,
    `CreatedAt` Timestamp NOT NULL COMMENT '创建时间',
    `UpdatedAt` Timestamp NOT NULL COMMENT '更新时间',
    `UserID` int NOT NULL,
    `GroupID` int NOT NULL,
    `CompanyID` int NOT NULL,
    `OEMID` int NOT NULL,
    CONSTRAINT `PK_ProductUploadLog` PRIMARY KEY (`ID`),
    CONSTRAINT `FK_ProductUploadLog_Company_CompanyID` FOREIGN KEY (`CompanyID`) REFERENCES `Company` (`ID`) ON DELETE CASCADE,
    CONSTRAINT `FK_ProductUploadLog_OEM_OEMID` FOREIGN KEY (`OEMID`) REFERENCES `OEM` (`ID`) ON DELETE CASCADE,
    CONSTRAINT `FK_ProductUploadLog_User_UserID` FOREIGN KEY (`UserID`) REFERENCES `User` (`Id`) ON DELETE CASCADE,
    CONSTRAINT `FK_ProductUploadLog_UserGroup_GroupID` FOREIGN KEY (`GroupID`) REFERENCES `UserGroup` (`ID`) ON DELETE CASCADE
) CHARACTER SET=utf8mb4 COMMENT='产品上传日志';




CREATE INDEX `IX_ProductUpload_CompanyID` ON `ProductUpload` (`CompanyID`);

CREATE INDEX `IX_ProductUpload_GroupID` ON `ProductUpload` (`GroupID`);

CREATE INDEX `IX_ProductUpload_OEMID` ON `ProductUpload` (`OEMID`);

CREATE INDEX `IX_ProductUpload_UserID` ON `ProductUpload` (`UserID`);

CREATE INDEX `IX_ProductUploadLog_CompanyID` ON `ProductUploadLog` (`CompanyID`);

CREATE INDEX `IX_ProductUploadLog_GroupID` ON `ProductUploadLog` (`GroupID`);

CREATE INDEX `IX_ProductUploadLog_OEMID` ON `ProductUploadLog` (`OEMID`);

CREATE INDEX `IX_ProductUploadLog_UserID` ON `ProductUploadLog` (`UserID`);

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20230228025308_t45', '6.0.5');

COMMIT;

START TRANSACTION;

DROP TABLE `MattingPackage`;

ALTER TABLE `ProductUpload` ADD `PathName` longtext CHARACTER SET utf8mb4 NOT NULL COMMENT '上传平台节点路径名称';

ALTER TABLE `ProductUpload` ADD `PathNodes` longtext CHARACTER SET utf8mb4 NOT NULL COMMENT '上传平台节点路径';

ALTER TABLE `ProductUpload` ADD `StoreID` int NOT NULL DEFAULT 0 COMMENT '店铺ID';

CREATE TABLE `Packages` (
    `ID` int NOT NULL AUTO_INCREMENT COMMENT '表索引',
    `Name` longtext CHARACTER SET utf8mb4 NOT NULL COMMENT '资源包名称',
    `Desc` longtext CHARACTER SET utf8mb4 NULL COMMENT '描述',
    `Type` int NULL,
    `CreatedAt` Timestamp NOT NULL COMMENT '创建时间',
    `UpdatedAt` Timestamp NOT NULL COMMENT '更新时间',
    CONSTRAINT `PK_Packages` PRIMARY KEY (`ID`)
) CHARACTER SET=utf8mb4;

INSERT INTO `Packages` (`ID`, `CreatedAt`, `Desc`, `Name`, `Type`, `UpdatedAt`)
VALUES (1, TIMESTAMP '2021-11-19 13:38:00', '', '智能抠图', NULL, TIMESTAMP '2021-11-19 13:38:00');
INSERT INTO `Packages` (`ID`, `CreatedAt`, `Desc`, `Name`, `Type`, `UpdatedAt`)
VALUES (2, TIMESTAMP '2021-11-19 13:38:00', '', '文本翻译', NULL, TIMESTAMP '2021-11-19 13:38:00');




INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20230301064243_t46', '6.0.5');

COMMIT;

START TRANSACTION;

CREATE TABLE `TranslationPackageItem` (
    `ID` int NOT NULL AUTO_INCREMENT COMMENT '表索引',
    `Num` int NOT NULL COMMENT '字符数',
    `Price` decimal(65,30) NOT NULL COMMENT '金额',
    `State` int NOT NULL COMMENT '状态',
    `CreatedAt` Timestamp NOT NULL COMMENT '创建时间',
    `UpdatedAt` Timestamp NOT NULL COMMENT '更新时间',
    CONSTRAINT `PK_TranslationPackageItem` PRIMARY KEY (`ID`)
) CHARACTER SET=utf8mb4;




INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20230301070559_t47', '6.0.5');

COMMIT;

START TRANSACTION;

ALTER TABLE `ProductUpload` DROP COLUMN `Message`;

ALTER TABLE `ProductUpload` DROP COLUMN `ProductJson`;

ALTER TABLE `ProductUploadLog` RENAME COLUMN `productUploadID` TO `Pid`;

ALTER TABLE `Packages` MODIFY COLUMN `Type` int NOT NULL DEFAULT 0;

ALTER TABLE `CompanyResourcePackages` ADD `Snapshot` longtext CHARACTER SET utf8mb4 NOT NULL COMMENT '快照';

CREATE TABLE `ProductUploadJson` (
    `ID` int NOT NULL AUTO_INCREMENT,
    `ProductJson` longtext CHARACTER SET utf8mb4 NOT NULL COMMENT '上传产品内容',
    `Result` int NOT NULL,
    `Message` longtext CHARACTER SET utf8mb4 NOT NULL COMMENT '具体错误信息',
    `IsMain` tinyint(1) NOT NULL,
    `Pid` int NOT NULL,
    `Sku` longtext CHARACTER SET utf8mb4 NOT NULL,
    CONSTRAINT `PK_ProductUploadJson` PRIMARY KEY (`ID`)
) CHARACTER SET=utf8mb4 COMMENT='产品上传json结果';

UPDATE `Packages` SET `Type` = 1
WHERE `ID` = 1;
SELECT ROW_COUNT();


UPDATE `Packages` SET `Type` = 2
WHERE `ID` = 2;
SELECT ROW_COUNT();





INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20230301111019_t48', '6.0.5');

COMMIT;

