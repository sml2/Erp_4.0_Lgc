using ERP.Controllers;
using ERP.Interface;
using ERP.Services.Caches;
using Microsoft.DotNet.PlatformAbstractions;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using ERP.Services.Stores;
using Org.BouncyCastle.Ocsp;
using ERP.ViewModels.Store.Remote;
using static NPOI.HSSF.Util.HSSFColor;
using ERP.Services.Api;
using ERP.Services.DB.Users;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Web;
using Ryu.Money.Json;

namespace ERP.Middleware;

public class AdapteEnvironmentMiddleware
{
    readonly ILogger logger;
    private readonly RequestDelegate _next;
    private readonly IHostEnvironment _environment;

    public AdapteEnvironmentMiddleware(RequestDelegate next, IHostEnvironment environment,
        ILogger<AdapteEnvironmentMiddleware> _logger)
    {
        _next = next;
        _environment = environment;
        logger = _logger;
    }

    public async Task InvokeAsync(HttpContext context, IOemProvider oemProvider)
    {
        var filterPath = new List<string>()
        {
            "/api/setting/integralbalance/recharge",
            "/api/setting/integralbalance/notify",
            "/api/user/login",
            "/api/user/logout",
            "/notify/alipayreturnurl",
            "/metrics"
        };
        var path = context.Request.Path.ToString().ToLower();
        if ( !filterPath.Contains(path.ToLower())  && !path.Contains("/vue") && !path.Contains("register"))
        {
            var onlineService = context.RequestServices.GetService<UserOnlineService>();
            var (state, msg) = await onlineService.Check();
            if (!state)
            {
                // throw new Exception(msg);
                context.Response.StatusCode = 200;
                context.Response.ContentType = "application/json;charset=UTF-8";
                await context.Response.WriteAsync(JsonConvert.SerializeObject(new ResultStruct()
                {
                    Code = Codes.Login6,
                    Message = msg
                }, new JsonSerializerSettings()
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                }));
                await Task.CompletedTask;
                return;
            }
        }

        if (path.ToLower() == "/metrics")
        {
            await _next(context);
        }
        //生产
        else if (_environment.IsProduction())
        {
            var headers = JsonConvert.SerializeObject(context.Request.Headers);
            logger.LogInformation($"Request:{context.Request.Path}");
            if (context.Request.Headers.ContainsKey("X-Forwarded-For"))
            {
                logger.LogInformation($"X-Forwarded-For:{context.Request.Headers["X-Forwarded-For"].ToString()}");
            }
            logger.LogInformation($"Headers:{headers}");

            try
            {
                var Oem = oemProvider.OEM;
                if (Oem is not null)
                {
                    //var indexPage = new ERP.Pages.VUE.IndexModel();
                    //return Dir;
                    //return;
                    if (context.Request.Path.Equals("/"))
                        context.Request.Path = "/VUE/";
                    await _next(context);
                }
                else
                {
                    throw new Exception("oem null #45");
                }
            }
            catch (Exception e)
            {
                var Request = context.Request;
                context.Response.ContentType = "text/plain; charset=utf-8";
                var SubDomain = Request.Host.Host.Split('.').First();
                // "AUTH".Equals(SubDomain, StringComparison.CurrentCultureIgnoreCase) || "AUTH2".Equals(SubDomain, StringComparison.CurrentCultureIgnoreCase)
                if (SubDomain.Contains("AUTH") || SubDomain.Contains("auth"))
                {
                    var storeService = context.RequestServices.GetService<StoreService>();
                    string authInfo = "";
                    var Query = Request.Query;
                    logger.LogInformation($"Request Path:{Request.Path},QueryString:{Request.QueryString}");
                    //?spapi_oauth_code=ANBwYEwXjqmjDHJKeOEw&state=962|535920230626T092749Z&selling_partner_id=A2PMJMJF144QMA
                    const string AmazonQ1 = "spapi_oauth_code";
                    const string AmazonQ2 = "selling_partner_id";
                    const string AmazonQ3 = "state";
                    
                    const string ShopeeQ1 = "id";
                    const string ShopeeQ2 = "code";
                    const string ShopeeQ3 = "shop_id";
                    const string ShopeeQ4 = "main_account_id";
                    if (!string.IsNullOrEmpty(Query[AmazonQ1]) && !string.IsNullOrEmpty(Query[AmazonQ2])) //亚马逊授权页面
                    {                        
                        //请求授权方法
                        try
                        {
                            var state = HttpUtility.UrlDecode(Query[AmazonQ3].ToString());
                            logger.LogInformation($"state:{state}");
                            int.TryParse(state.Split('|')[1], out int id);
                            AmazonCode req = new AmazonCode()
                            {
                                ID = id,
                                Code = Query[AmazonQ1].ToString(),
                                PartnerId = Query[AmazonQ2].ToString(),
                                OriginString= Query[AmazonQ3].ToString(),
                            };
                            var res = await storeService.SaveAmazonAccessToken(req);
                            if (res.State)
                            {
                                authInfo = $"已绑定亚马逊{req.PartnerId}成功！请进入ERP处理相关业务，可关闭此页！";
                            }
                            else
                            {
                                authInfo = res.Msg;
                            }
                        }
                        catch (Exception ex)
                        {
                            authInfo = ex.Message;
                        }

                        await context.Response.WriteAsync(authInfo);
                        await Task.CompletedTask;
                    }
                    else if ((!string.IsNullOrEmpty(Query[ShopeeQ1]) &&
                              !string.IsNullOrEmpty(Query[ShopeeQ2]))) //虾皮授权页面
                    {
                        //context.Response.Redirect($"/#/SVerify{Request.QueryString}");
                        try
                        {
                            int.TryParse(Query[ShopeeQ1], out int id);
                            string code = Query[ShopeeQ2];
                            int shopid = string.IsNullOrWhiteSpace(Query[ShopeeQ3]) ? 0 : int.Parse(Query[ShopeeQ3]);
                            int mainaccountId = string.IsNullOrWhiteSpace(Query[ShopeeQ4])
                                ? 0
                                : int.Parse(Query[ShopeeQ4]);
                            ShopeeCode req = new ShopeeCode()
                            {
                                ID = id,
                                Code = code,
                                ShopId = shopid,
                                MainAccountId = mainaccountId,
                            };
                            //请求授权方法
                            var res = await storeService.SaveShopeeAccessToken(req);
                            if (res.State)
                            {
                                authInfo = $"已绑定虾皮店铺{req.ShopId}成功！请进入ERP处理相关业务，可关闭此页！";
                            }
                            else
                            {
                                authInfo = res.Msg;
                            }
                        }
                        catch (Exception ex)
                        {
                            authInfo = ex.Message;
                        }

                        await context.Response.WriteAsync(authInfo);
                        await Task.CompletedTask;
                    }
                    else
                    {
                        var authError = $"Error Auth Query:{Request.QueryString};";
                        await context.Response.WriteAsync(authError);
                    }
                }
                else
                {
                    var settings = JsonConvert.DefaultSettings?.Invoke();
                    if (settings is not null)
                        settings.ContractResolver = new MoneyContractResolver()
                            { NamingStrategy = new CamelCaseNamingStrategy() };
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(BaseController.Error(e), settings));
                    logger.LogError("have no AUTH,please check");
                    logger.LogError(e, e.Message);
                }
            }
        }
        //运维
        else if (_environment.IsEnvironment("Maintenance"))
        {
            await context.Response.WriteAsync("正在维护中...<br/>Maintenance...");
        }
        //测试
        else if (_environment.IsStaging())
        {
            await _next(context);
        }
        //开发
        else if (_environment.EnvironmentName.StartsWith(Environments.Development) || _environment.IsDevelopment())
        {
            await _next(context);
        }
        //未知
        else
        {
            throw new Exception($"Unknown Environment[{_environment.EnvironmentName}]");
        }
    }
}