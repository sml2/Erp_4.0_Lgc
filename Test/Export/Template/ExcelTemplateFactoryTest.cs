using ERP.Export.Templates;
using ERP.Export.V4_Export.ExcelTemplates;
using ERP.Interface;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace Test.Export.Template;

public class ExcelTemplateFactoryTest
{
    [Test]
    public void TestExcelTemplateFactory_DependencyInjection()
    {
        // setup
        var host = WebApplication.CreateBuilder();
        var service = host.Services;
        service.AddSingleton<ERP.Export.ExportCommon.ProductExportHelper>()
            .AddHttpContextAccessor()
            .AddTransient<IOemProvider, ERP.Services.Provider.OemProvider>();
        
        //service.AddExcelTemplate()
        //    .RegisterTemplate<ERP.Export.V4_Export.ExcelTemplates.Amazon>()
        //    .RegisterTemplate<DianXiaoMi>();
        var sp = host.Build().Services;
        
        // action
        var factory = sp.GetRequiredService<ExcelTemplateFactory>();
        
        // assert
        Assert.IsNotNull(factory[TemplateType.Amazon]);
        Assert.IsNotNull(factory[TemplateType.Dianxiaomi]);
    }
}