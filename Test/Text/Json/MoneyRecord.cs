﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using ERP.Text.Json.Serialization;
using NUnit.Framework;
using Ryu.Data;
using Ryu.Data.Abstraction;
using MR = ERP.Data.MoneyRecord;

namespace Text.Json;
public class MoneyRecord : InjectTest
{
    public MoneyRecord()
    {
        MoneyFactory.Instance.SetRateProvider(Resolve<IRateProvider>());
    }
    const string sample_false = "{\"Money\":2619501.3500073,\"Raw\":{\"CurrencyCode\":\"\",\"Amount\":0.0,\"Raw\":\"\",\"Result\":0.0,\"Rate\":0.0,\"Success\":false,\"FromType\":0}}";
    const string sample_true = "{\"Money\":2222486.8986573,\"Raw\":{\"CurrencyCode\":\"EUR\",\"Amount\":27.99,\"Raw\":\"27.99\",\"Result\":2222486.8986573,\"Rate\":79402.89027,\"Success\":true,\"FromType\":1}}";
    [Test]
    public void Test()
    {
        MR mR = new MR(66);
        var json = System.Text.Json.JsonSerializer.Serialize(mR);
        Console.WriteLine(json);
        var Mr = System.Text.Json.JsonSerializer.Deserialize<MR>(json);
        Console.WriteLine(Mr.Money.Value);
        var result = System.Text.Json.JsonSerializer.Deserialize<MR>(sample_false);
        Console.WriteLine(result.Money);
        result = System.Text.Json.JsonSerializer.Deserialize<MR>(sample_true);
        Console.WriteLine(result.Money);
        result = Newtonsoft.Json.JsonConvert.DeserializeObject<MR>(sample_false);
        Console.WriteLine(result.Money);
        result = Newtonsoft.Json.JsonConvert.DeserializeObject<MR>(sample_true);
        Console.WriteLine(result.Money);
    }

}

