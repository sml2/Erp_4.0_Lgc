﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using ERP.Text.Json.Serialization;
using Newtonsoft.Json;
using NUnit.Framework;
using Ryu.Data;
using Ryu.Data.Abstraction;
using StructMoney = Ryu.Data.Money;

namespace Text.Json;
public class Money : InjectTest
{
    public Money()
    {
        MoneyFactory.Instance.SetRateProvider(Resolve<IRateProvider>());
    }
   
    [Test]
    public void Test()
    {
        // throw new ERP.Exceptions.Developers.Bugs.MoneyDeserializeBeZero();
        StructMoney SM = new(2619501.3500073m);
        var json = System.Text.Json.JsonSerializer.Serialize(SM);
        Console.WriteLine(json);
        var Mr = System.Text.Json.JsonSerializer.Deserialize<StructMoney>(json);
        Console.WriteLine(Mr.Value);
    }
    
    [Test]
    public void TestSerializeJson()
    {
        var jsonStr = @"{
        ""Cost"":{
        ""Max"":""8156.00"",
        ""Min"":""3332.00""
    },
    ""Sale"":{
        ""Max"":""8156.00"",
        ""Min"":""3332.00""
    }
}";
        var prices = JsonConvert.DeserializeObject<ERP.Data.Products.Prices>(jsonStr);

        var pricesStr = JsonConvert.SerializeObject(prices);
        
    }

}

