﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using ERP.Text.Json.Serialization;
using NUnit.Framework;

namespace Text.Json;
public class DateTime : InjectTest
{
    public DateTime()
    {
    }
    private class Product
    {
        public string Name { get; set; } = string.Empty;
        [JsonConverter(typeof(DateTimeConverterUsingDateTimeParse))]
        public System.DateTime ExpiryDate { get; set; }
    }
    const string sample = @"{""Name"":""Banana"",""ExpiryDate"":""2019-07-26 00:00:00""}";
    [Test]
    public void SYSTEM()
    {
        var d = System.DateTime.Parse("2022-02-16 16:44:33");
        var str = System.Text.Json.JsonSerializer.Serialize(d);
        Console.WriteLine(str);
        var result = System.Text.Json.JsonSerializer.Deserialize<Product>(sample);
        Console.WriteLine(result.ToStringFormField());
    }
    [Test]
    public void NEWTONSOFT()
    {
        var result = Newtonsoft.Json.JsonConvert.DeserializeObject<Product>(sample);
        Console.WriteLine(result.ToStringFormField());
    }
}

