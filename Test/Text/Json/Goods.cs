﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using static ERP.Models.DB.Orders.Order;

namespace Text.Json;
public class Goods : InjectTest
{
    public Goods()
    {
    }
    const string sample = "[{\"ID\":\"B08Q8KMVHS\",\"Name\":\"kijighg Attack On Titan Anime Figure Eren Mikasa Levi Ackerman Figma Action PVC Figure Collection Model Toy Collection Miglior Regalo\",\"Sku\":\"IT-SP-28\",\"ImageURL\":null,\"ItemPrice\":{\"Money\":2222486.8986573,\"Raw\":{\"CurrencyCode\":\"EUR\",\"Amount\":27.99,\"Raw\":\"27.99\",\"Result\":2222486.8986573,\"Rate\":79402.89027,\"Success\":true,\"FromType\":1}},\"ShippingPrice\":{\"Money\":397014.4513500,\"Raw\":{\"CurrencyCode\":\"EUR\",\"Amount\":5.00,\"Raw\":\"5.00\",\"Result\":397014.4513500,\"Rate\":79402.89027,\"Success\":true,\"FromType\":1}},\"promotionDiscount\":{\"Money\":0.0,\"Raw\":{\"CurrencyCode\":\"EUR\",\"Amount\":0.90,\"Raw\":\"0.00\",\"Result\":0.0,\"Rate\":79402.89027,\"Success\":true,\"FromType\":1}},\"ItemTax\":{\"Money\":400984.5958635,\"Raw\":{\"CurrencyCode\":\"EUR\",\"Amount\":5.05,\"Raw\":\"5.05\",\"Result\":400984.5958635,\"Rate\":79402.89027,\"Success\":true,\"FromType\":1}},\"ShippingTax\":{\"Money\":71462.6012430,\"Raw\":{\"CurrencyCode\":\"EUR\",\"Amount\":0.90,\"Raw\":\"0.90\",\"Result\":71462.6012430,\"Rate\":79402.89027,\"Success\":true,\"FromType\":1}},\"ShippingDiscountTax\":{\"Money\":0.0,\"Raw\":{\"CurrencyCode\":\"EUR\",\"Amount\":0.90,\"Raw\":\"0.00\",\"Result\":0.0,\"Rate\":79402.89027,\"Success\":true,\"FromType\":1}},\"PromotionDiscountTax\":{\"Money\":0.0,\"Raw\":{\"CurrencyCode\":\"EUR\",\"Amount\":0.90,\"Raw\":\"0.00\",\"Result\":0.0,\"Rate\":79402.89027,\"Success\":true,\"FromType\":1}},\"QuantityOrdered\":1,\"QuantityShipped\":1,\"WaitForSendNum\":0,\"FromURL\":\"(True, mws.amazonservices.it, System.Text.UTF8Encoding+UTF8EncodingSealed)/dp/B08Q8KMVHS\",\"UnitPrice\":{\"Money\":2222486.8986573,\"Raw\":{\"CurrencyCode\":\"\",\"Amount\":0.0,\"Raw\":\"\",\"Result\":0.0,\"Rate\":0.0,\"Success\":false,\"FromType\":0}},\"TotalPrice\":{\"Money\":2619501.3500073,\"Raw\":{\"CurrencyCode\":\"\",\"Amount\":0.0,\"Raw\":\"\",\"Result\":0.0,\"Rate\":0.0,\"Success\":false,\"FromType\":0}}}]";
    [Test]
    public void SYSTEM()
    {
        var result = System.Text.Json.JsonSerializer.Deserialize<List<Good>>(sample);
        Console.WriteLine(result);
        Assert.AreNotEqual(0, result[0].ShippingTax.Money);
        Assert.AreNotEqual(0, result[0].ShippingTax.Raw.Amount);
    }
    [Test]
    public void NEWTONSOFT()
    {
        _ = Ryu.Data.Money.Zero;
        var result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Good>>(sample);
        Console.WriteLine(result);
        Assert.AreNotEqual(0, result[0].ShippingTax.Money);
        Assert.AreNotEqual(0, result[0].ShippingTax.Raw.Amount);
    }
}

