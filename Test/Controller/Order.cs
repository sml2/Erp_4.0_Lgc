﻿using Coravel.Events.Interfaces;
using Coravel.Queuing.Broadcast;
using Coravel.Queuing.Interfaces;
using ERP.Interface;
using ERP.Services.Distribution;
using ERP.Services.Stores;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ERP.Controllers.Orders.OrderController;

namespace Test.Controller
{
    public  class Order: InjectTest
    {
        private readonly ERP.Controllers.Orders.OrderController order;//=new ERP.Controllers.Orders.Order();// .Stores.StoreService storeService;

        private readonly ILogger<ERP.Controllers.Orders.OrderController> logger;
        private readonly IQueue queue;
        private readonly IListener<QueueTaskCompleted> ilistener;


        //OrderHostService orderHostService, OrderService orderService,
        //PurchaseService purchaseService,
        ///*UploadService uploadService,*/
        //DBContext dbContext,
        //NationCache nationCache,
        //ERP.Services.DB.Statistics.Statistic statisticService,
        //UserService userService,
        //StoreService storeService,
        //IExcelFactory excelFactory,
        //UserManager<Models.DB.Identity.User> userManager,
        //MergeService mergeService, ISessionProvider sessionProvider, IMediator mediator
        public Order()
        {
            logger= Resolve<ILogger<ERP.Controllers.Orders.OrderController>>();
            queue= Resolve<IQueue>();
            ilistener= Resolve<IListener<QueueTaskCompleted>>();
            //order = Resolve<ERP.Controllers.Orders.Order>();
        }






        //ERP.Controllers.Orders.Order 
        [Test]
        public async Task ManualPullOrder()
        {
            ManualPullOrderVM manualPullOrderVM = new ManualPullOrderVM();
            DateTime begin = Convert.ToDateTime("2022-11-13");
            DateTime end = Convert.ToDateTime("2022-11-19"); 
            manualPullOrderVM.DateRange=new List<DateTime>() { begin, end };
            manualPullOrderVM.CurrentTime = true;
            manualPullOrderVM.LastOrderPullTime = true;
            var reautl=await order.ManualPullOrder(manualPullOrderVM, logger, queue, ilistener);
        }
        
    }
}
