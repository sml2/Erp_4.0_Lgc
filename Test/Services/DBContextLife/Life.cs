using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.Data;
using NUnit.Framework;
using ERP.Services.DB.Statistics;
using Microsoft.EntityFrameworkCore;
using System.Transactions;
using System.IO.Compression;
using System.IO;
using System.Security.Policy;
using ERP.Models.DB.Product;
using ERP;
using ERP.Models.Product;

namespace Services.Lifes;

public class DBContextLife : InjectTest
{
    private readonly ERP.Services.DB.Statistics.Statistic _statisticService;
    private readonly DBContext _dbContext;

    public DBContextLife()
    {
        _dbContext = Resolve<DBContext>();
    }

    [Test]
    public async Task TestInNormalScope()
    {
        object obj = _dbContext;
        var o1Hash = GetHashCode(obj);
        var o2Hash = SubFun();
        Assert.AreEqual(o1Hash, o2Hash);
    }

    [Test]
    public async Task TestInTransactionScope()
    {
        object obj = _dbContext;
        var o1Hash = GetHashCode(obj);
        using (var scope = new TransactionScope())
        {
            var o2Hash = SubFun();
            Assert.AreEqual(o1Hash, o2Hash);
            scope.Complete();
        }
    }
    [Test]
    public async Task TestInTwoScope()
    {
        object obj = _dbContext;
        var o1Hash = GetHashCode(obj);
        Console.WriteLine(System.Threading.Thread.CurrentThread.ManagedThreadId);
        var o2Hash = await Task.Factory.StartNew(() =>
        {
            Console.WriteLine(System.Threading.Thread.CurrentThread.ManagedThreadId);
            return SubFun();
        });
        var thread = new System.Threading.Thread(() =>
        {
            Console.WriteLine($"ThreadID:{System.Threading.Thread.CurrentThread.ManagedThreadId} Hash:{SubFun()}");
        });
        thread.Start();
        Console.WriteLine(o1Hash);
        Assert.AreEqual(o1Hash, o2Hash);

    }

    [Test]
    public void Tosell()
    {
        var result = _dbContext.ToSell.Where(x => x.HashCode == 7092578560261149144).FirstOrDefault();
        result.ProductResult.GetDescription();
        Assert.IsNull(result);
    }

    [Test]
    public void InsertToSell()
    {
        
        var store = _dbContext.StoreRegion.Where(x => x.ID == 253).First();
        Console.WriteLine(store.UserID);
        var user = _dbContext.User.Where(x => x.Id == 444450).First();

        ToSellModel model = new ToSellModel();       
              model.HashCode = Helpers.CreateHashCode("Asin" + 253);
        model.Title = "Title";
        model.Asin = "Asin";
        model.ImageUrl = "ImageUrl";
        model.AsinMarketplace = "AsinMarketplace";
        model.Brand = "Brand";
        //model.Plateform = Platforms.AMAZONSP;
        model.storeRegion = store;
        model.CompanyID = user.CompanyID;
        model.GroupID = user.GroupID;
        model.UserID = user.ID;
        model.OEMID = user.OEMID;
        model.ProductResult = HijackingResults.WaitSynchronize;
        model.ProductType = "";
        model.Msg = "";
        _dbContext.ToSell.Add(model);
        _dbContext.SaveChanges();
    }


    public int SubFun()
    {
        var NewObj = Resolve<DBContext>(); ;
        return GetHashCode(NewObj);

    }

    private int GetHashCode(object o)
    {
        return o.GetHashCode();
    }

    [Test]
    public async Task TestStore()
    {
        var StoreRegion = _dbContext.StoreRegion.First();
        StoreRegion.Name = "sp-西班牙123";
        var state = _dbContext.StoreRegion.Update(StoreRegion);
        state.Property(o => o.Name).IsModified = false;
        Assert.AreEqual(1, _dbContext.SaveChanges());

    }


    [Test]
    public void GZIPDemo()
    {
        string CompressedFileName = @"amzn1.tortuga.3.0dc6de79-207b-4c33-95c5-a4558e0a47ea.gz";
        string DecompressedFileName = @"decompressed.xls";

        using FileStream compressedFileStream = File.Open(CompressedFileName, FileMode.Open);
        using FileStream outputFileStream = File.Create(DecompressedFileName);
        using var decompressor = new GZipStream(compressedFileStream, CompressionMode.Decompress);
        decompressor.CopyTo(outputFileStream);
            
    
    }

    [Test]
    public void HashSet()
    {
        HashSet<string> hashSet = new HashSet<string>();
        hashSet.Add("123");
        hashSet.Add("689");
        hashSet.Add("456");
        hashSet.Add("12435");
        hashSet.Add("12435");
        hashSet.Add("12435");
        foreach (var item in hashSet)
        {
            Console.WriteLine(item);
        }
        Console.WriteLine(hashSet.Count);
        Console.WriteLine(hashSet.Contains("12345"));
    }


    [Test]
    public void m()
    {
        var n = (ulong)512 & 32;
        Console.WriteLine(n);
        var b = 1 << 9;
        Console.WriteLine(b);
    }
}