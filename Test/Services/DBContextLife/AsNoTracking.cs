using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ERP.Data;
using ERP.Enums.Orders;
using ERP.ViewModels.Distribution;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace Test.Services.DBContextLife
{
  
    internal class AsNoTracking: InjectTest
    {
        private readonly DBContext _dbContext;
        public AsNoTracking()
        {
            _dbContext = Resolve<DBContext>();
        }

        [Test]
        public  void Track()
        {
            var store = _dbContext.StoreRegion.AsNoTracking().FirstOrDefault(x => x.ID == 149);
            if(store != null)
            {
                Console.WriteLine(store.Name);
                var newStore = store;
                newStore.Name = "苏夫君01";
                _dbContext.StoreRegion.Update(store);
                 _dbContext.SaveChanges();
                var s = _dbContext.StoreRegion.FirstOrDefault(x => x.ID == 149);
                if (s != null)
                {
                    Console.WriteLine(s.Name);
                }
                Assert.AreEqual(store.Name, s.Name);
            }
            else
            {
                Assert.Fail("store id error");
            }
        }

        //[Test]
        //public async Task Purchse()
        //{
        //    var root = _dbContext.Order.Where(x => x.OrderNo == "114-4706486-6786654").AsNoTracking();
        //    if (root != null)
        //    {
        //        var orders = await root.Select(o => new OrderListDto
        //    (o.ID, o.OrderNo, o.StoreId, o.Receiver.Nation, o.Url, o.ProductNum,
        //        o.OrderTotal,
        //        o.Coin, o.Fee, o.Loss, o.Refund, o.PurchaseFee, o.ShippingMoney, o.Profit, (int)o.State, o.CompanyID,
        //        o.CreateTime, o.LatestShipTime,
        //        o.OrderTotal.Raw.Rate, o.PurchaseAudit, o.ShippingType, o.Store!.Name, o.Plateform,
        //        o.PurchaseCompanyId, o.WaybillCompanyId,
        //        o.Purchases.Where(p => !string.IsNullOrEmpty(p.PurchaseTrackingNumber)).Select(p => new { id = p.ID, purchaseTrackingNumber = p.PurchaseTrackingNumber! }).ToArray(),
        //        o.Waybills.Where(w => !string.IsNullOrEmpty(w.Express)).Select(w => new { id = w.ID, express = w.Express, stateDes = w.StateDes }).ToArray(),
        //        o.CustomMark, o.Receiver.NationShort, o.Delivery)
        //        {
        //            Username = o.User.UserName
        //        }
        //    ).ToListAsync();
        //        Assert.Pass();
        //    }
        //    else
        //    {
        //        Assert.Fail("store id error");
        //    }
        //}


        [Test]
        public async Task Fee()
        {
            var feeAmount = _dbContext.Order.Sum(x => x.Fee.Money);

            var root = _dbContext.Order.Where(x=> x.pullOrderState== PullOrderState.OutOfProcess).AsNoTracking().ToList();
            if (root != null)
            {
                var feeList=root.Where(f=>f.Fee.Money>0).ToList();
               foreach(var item in root)
                {
                    var fee = item.Fee;
                    var feeMoney = item.Fee.Money;
                }
                Assert.Pass();
            }
            else
            {
                Assert.Fail("store id error");
            }
        }

    }
}