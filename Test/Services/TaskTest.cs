
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using ERP.Attributes;
using ERP.ControllersWithoutToken.Product;
using ERP.Data;
using ERP.Enums;
using ERP.Extensions;
using ERP.Models.Stores;
using ERP.Models.Product;
using ERP.Models.View.Products.Product;
using ERP.Services.Product;
using Ryu.Extensions;
using Ryu.Linq;
using Sy.Threading;
using ERP.Services.Api;
using ERP.Models.Stores;

namespace Test.Services
{
    internal class TaskTest : InjectTest
    {
        private readonly ERP.Services.Stores.StoreService storeService;
        private readonly IOrderFactory _ApiOrderFactory;
        private readonly DBContext dBContext;
        private readonly ERP.Services.Caches.AmazonSPKey CacheAmazonSpKey;
        private readonly ERP.Services.Caches.PlatformData CachePlatformData;
        private readonly ERP.Services.DB.Orders.Order orderService;
        private readonly ProductService _productService;
        private readonly TranslationService _translationService;

        public TaskTest()
        {
            _productService = Resolve<ProductService>();
            storeService = Resolve<ERP.Services.Stores.StoreService>();
            _ApiOrderFactory = Resolve<IOrderFactory>();
            dBContext = Resolve<DBContext>();
            CacheAmazonSpKey = Resolve<ERP.Services.Caches.AmazonSPKey>();
            CachePlatformData = Resolve<ERP.Services.Caches.PlatformData>();
            orderService = Resolve<ERP.Services.DB.Orders.Order>();
            _translationService = Resolve<TranslationService>();
        }

        [Test]
        public async Task<string> TimeTest()
        {
            int a = 0, b = 0, c = 0;
            var taskArray = new Task[2];
            TaskFactory taskFactory = new TaskFactory();
            taskArray[0] = taskFactory.StartNew(() => { a = Method1(); });
            taskArray[1] = taskFactory.StartNew(() => { b = Method2(); });
            taskArray[2] = taskFactory.StartNew(() => { c = Method3(); });

            try
            {
                await Task.WhenAll(taskArray);
                int d = a + b + c;
                Assert.Equals(d, 10);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception:{e}");
            }

            return "";
        }

        public int Method1()
        {
            Thread.Sleep(1000);
            return 3;
        }

        public int Method2()
        {
            Thread.Sleep(2000);
            return 6;
        }

        public int Method3()
        {
            Thread.Sleep(3000);
            return 1;
        }


        [Test]
        public void TestStringToChar()
        {
            string s = "+8";
            char[] chars = s.ToCharArray();
            foreach (var a in chars)
            {
                Console.WriteLine(a);
            }
        }

        [Test]
        public async Task Test()
        {

            // await _translationService.Send();


            var a = await _productService.GetInfoWithUpload(new GetInfoWithUploadDto()
            { Pid = 61, Language = Languages.Default, CodeType = EanupcModel.TypeEnum.EAN,Unit = "CNY"});

            // var languages = Enum.GetValues<Languages>()
            //     .Where(l => typeof(Languages).GetMember(l.ToString())[0].GetCustomAttribute<TranslationAttribute>() != null)
            //     .Select(m => new CodeVm(m))
            //     .ToArray();
            // await _translationService.Send();
        }
    }

    public class CodeVm
    {
        public CodeVm()
        {
            
        }
        public CodeVm(Languages m)
        {
            Name = m.GetDescriptionByKey("Name");
            Code = m.GetDescriptionByKey("Code");
            TranslationCode = m.GetTranslationCode(Code);
        }
        
        public string Name { get; set; }
        public string Code { get; set; }
        public string TranslationCode { get; set; }
    }
}