﻿using System;
using System.Threading.Tasks;
using ERP.Services.Api.ExchangeRate;
using Microsoft.Extensions.Logging;
using NUnit.Framework;

namespace Services
{
    public class ExchangeRate: InjectTest
    {
        private Aliyun? aliyun;


        [SetUp]
        public void Setup() {
            //var i = new Mock<IHttpClientFactory>();
            //aliyun = new Aliyun(i.Object);

            //var h = new HttpClientFactory();

            aliyun = Resolve<Aliyun>();
        }



        [Test]
        public async Task Get()
        {
            var r =  await aliyun!.Get(Resolve<ILogger<ExchangeRate>>());
            Console.WriteLine(r.ToStringFormField());
        }
    }
}

