using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.Data;
using NUnit.Framework;
using ERP.Services.DB.Statistics;
using Microsoft.EntityFrameworkCore;

namespace Services.Api.Statistic;

public class Count : InjectTest
{
    private readonly ERP.Services.DB.Statistics.Statistic _statisticService;
    private readonly DBContext _dbContext;

    public Count()
    {
        _dbContext = Resolve<ERP.Data.DBContext>();
    }

    [Test]
    public async Task Test()
    {

        var transaction = await _dbContext.Database.BeginTransactionAsync();

        try
        {
            var info = await _dbContext.UserGroup.Where(m => m.ID == 1).FirstAsync();

            info.Name = "111";

            await _dbContext.SaveChangesAsync();
            
            info.Name = "222";

            await transaction.CommitAsync();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
}