﻿using ERP.Data;
using ERP.Enums;
using ERP.Services.Api;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Services.Api.AmazonSpApi
{
    internal class VaildMetaSchema : InjectTest
    {
        private readonly ERP.Services.Stores.StoreService storeService;
        private readonly IOrderFactory _ApiOrderFactory;
        private readonly DBContext dBContext;
        private readonly ERP.Services.Caches.AmazonSPKey CacheAmazonSpKey;
        private readonly ERP.Services.Caches.PlatformData CachePlatformData;
        private readonly ERP.Services.DB.Orders.Order orderService;
        public VaildMetaSchema()
        {
            storeService = Resolve<ERP.Services.Stores.StoreService>();
            _ApiOrderFactory = Resolve<IOrderFactory>();
            dBContext = Resolve<DBContext>();
            CacheAmazonSpKey = Resolve<ERP.Services.Caches.AmazonSPKey>();
            CachePlatformData = Resolve<ERP.Services.Caches.PlatformData>();
            orderService = Resolve<ERP.Services.DB.Orders.Order>();
        }


        [Test]
        public void Test()
        {
            //亚马逊产品类型定义元架构的 $id。
            // $id of the Amazon Product Type Definition Meta-Schema.
            var schemaId = "https://schemas.amazon.com/selling-partners/definitions/product-types/meta-schema/v1";

            //亚马逊产品类型定义元架构的本地副本。
            // Local copy of the Amazon Product Type Definition Meta-Schema.
            var metaSchemaPath = "C:\\Users\\Administrator\\Desktop\\metaSchema.json";

            //亚马逊产品类型定义元架构实例的本地副本
            // Local copy of an instance of the Amazon Product Type Definition Meta-Schema.
            var luggageSchemaPath = "C:\\Users\\Administrator\\Desktop\\watchAttributes.json";

            //使用模式的本地副本而不是从 Web 检索它们的模式解析器。
            // Schema resolver that uses local copies of schemas rather than retrieving them from the web.
            var resolver = new JSchemaPreloadedResolver();

            //将元模式添加到解析器。
            // Add the meta-schema to the resolver.
            resolver.Add(new Uri(schemaId), File.ReadAllText(metaSchemaPath));

            //使用解析器和关键字验证器配置读取器设置以在解析元模式实例时使用。
            // Configure reader settings with resolver and keyword validators to use when parsing instances of the meta-schema.
            var readerSettings = new JSchemaReaderSettings
            {
                Resolver = resolver,
                Validators = new List<JsonValidator>
                {
                    new MaxUniqueItemsKeywordValidator(),
                    new MaxUtf8ByteLengthKeywordValidator(),
                    new MinUtf8ByteLengthKeywordValidator()
                }
            };

            var luggageSchema = JSchema.Parse(File.ReadAllText(luggageSchemaPath), readerSettings);

            var item = luggageSchema.Properties["externally_assigned_product_identifier"];
            // var itemName =JsonConvert.DeserializeObject<AttributeModel>();
            //为有效负载创建一个 JObject（这可以在代码中构建，从文件中读取等）。
            // Create a JObject for the payload (this can be constructed in code, read from a file, etc.).

          

            var payload = JObject.Parse(File.ReadAllText("./payload.json"));
            //验证有效负载并获取任何生成的验证消息。
            // Validate the payload and get any resulting validation messages.
            var valid = payload.IsValid(luggageSchema, out IList<string> errorMessages);
        }
    }

    public class MaxUniqueItemsKeywordValidator : JsonValidator
    {
        public override void Validate(JToken value, JsonValidatorContext context)
        {
            var maxUniqueItems = GetMaxUniqueItems(context.Schema);

            // Get the selector properties configured on the scheme element, if they exist. Otherwise, this validator
            // defaults to using all properties.
            var selectors = GetSelectors(context.Schema);

            // Only process if the value is an array with values.
            if (value.Type != JTokenType.Array) return;

            // Create a property-value dictionary of each items properties (selectors) and count the number of
            // occurrences for each combination.
            var uniqueItemCounts = new Dictionary<IDictionary<string, string>, int>(new UniqueKeyComparer());
            foreach (var instance in value)
            {
                // Only process instances in the array that are objects.
                if (instance.Type != JTokenType.Object) continue;

                var instanceObject = JObject.FromObject(instance);
                var uniqueKeys = instanceObject.Properties()
                    .Where(property => selectors.Count == 0 || selectors.Contains(property.Name))
                    .ToDictionary(property => property.Name, property => property.Value.ToString());

                var count = uniqueItemCounts.GetValueOrDefault(uniqueKeys, 0) + 1;
                uniqueItemCounts[uniqueKeys] = count;
            }

            // Find first selector combination with too many instances.
            var (uniqueKey, itemCount) = uniqueItemCounts.FirstOrDefault(entry => entry.Value > maxUniqueItems);
            if (itemCount > 0)
            {
                var selectorValues = string.Join(", ", uniqueKey.Select(keyValuePair => $"{keyValuePair.Key}={keyValuePair.Value}").ToList());
                context.RaiseError($"Each combination of selector values may only occur {maxUniqueItems} times. " +
                                   $"The following selector value combination occurs too many times: {{{selectorValues}}}");
            }
        }




        public override bool CanValidate(JSchema schema)

        {
            return GetMaxUniqueItems(schema) >= 0;
        }

        private static IList<string> GetSelectors(JSchema schema)
        {
            var selectors = new List<string>();

            var schemaObject = JObject.FromObject(schema);
            var selectorsProperty = schemaObject["selectors"];

            if (selectorsProperty.HasValues)
                selectors.AddRange(selectorsProperty.Select(selector => selector.ToString()));

            return selectors;
        }

        private static int GetMaxUniqueItems(JSchema schema)
        {
            var schemaObject = JObject.FromObject(schema);
            var maxUniqueItemsProperty = schemaObject["maxUniqueItems"];

            if (maxUniqueItemsProperty != null && int.TryParse(maxUniqueItemsProperty.ToString(), out var maxUniqueItems))
                return maxUniqueItems;

            return -1;
        }

        /**
         * "Deep" comparator for unique keys dictionary to enable use as a dictionary key.
         */
        private class UniqueKeyComparer : IEqualityComparer<IDictionary<string, string>>
        {
            public bool Equals(IDictionary<string, string> x, IDictionary<string, string> y)
            {
                return x.Count == y.Count
                       && x.Aggregate(true, (current, keyValuePair) => current && keyValuePair.Value == y[keyValuePair.Key]);
            }

            public int GetHashCode(IDictionary<string, string> obj)
            {
                return ("Keys_" + string.Join(",", obj.Select(o => o.Key))
                        + "_Values_" + string.Join(",", obj.Select(o => o.Value))).GetHashCode();
            }
        }
    }

    public class MaxUtf8ByteLengthKeywordValidator : JsonValidator
    {
        public override void Validate(JToken value, JsonValidatorContext context)
        {
            var maxUtf8ByteLength = GetMaxUtf8ByteLength(context.Schema);
            if (Encoding.UTF8.GetBytes(value.ToString()).Length > maxUtf8ByteLength)
                context.RaiseError($"Value must be less than or equal {maxUtf8ByteLength} bytes in length.");
        }

        public override bool CanValidate(JSchema schema)
        {
            return GetMaxUtf8ByteLength(schema) >= 0;
        }

        private static int GetMaxUtf8ByteLength(JSchema schema)
        {
            var schemaObject = JObject.FromObject(schema);
            var byteLengthProperty = schemaObject["maxUtf8ByteLength"];

            if (byteLengthProperty != null && int.TryParse(byteLengthProperty.ToString(), out var maxUtf8ByteLength))
                return maxUtf8ByteLength;

            return -1;
        }
    }


    /**
    * Example validator for the "minUtf8ByteLength" keyword.
    */
    public class MinUtf8ByteLengthKeywordValidator : JsonValidator
    {
        public override void Validate(JToken value, JsonValidatorContext context)
        {
            var minUtf8ByteLength = GetMinUtf8ByteLength(context.Schema);
            if (Encoding.UTF8.GetBytes(value.ToString()).Length < minUtf8ByteLength)
                context.RaiseError($"Value must be greater than or equal {minUtf8ByteLength} bytes in length.");
        }

        public override bool CanValidate(JSchema schema)
        {
            return GetMinUtf8ByteLength(schema) >= 0;
        }

        private static int GetMinUtf8ByteLength(JSchema schema)
        {
            var schemaObject = JObject.FromObject(schema);
            var byteLengthProperty = schemaObject["minUtf8ByteLength"];

            if (byteLengthProperty != null && int.TryParse(byteLengthProperty.ToString(), out var minUtf8ByteLength))
                return minUtf8ByteLength;

            return -1;
        }
    }

}
