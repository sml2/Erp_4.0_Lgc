
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ERP.Enums;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json;
using NUnit.Framework;
using ERP.Data;
using ERP.Models.Stores;
using System.IO;

using ERP.Services.DB.Product;
using ERP.Services.DB.ProductType;
using ERP;
using System.Security.Cryptography;

using ERP.Services.Api;
using OrderSDK.Modles.Amazon.Feeds;
using OrderSDK.Modles.Amazon.ListingsItems;
using static OrderSDK.Modles.Amazon.ListingsItems.ListingsItemPutRequest;
using ERP.Models.Stores;

namespace Test.Services.Api.AmazonSpApi
{
    internal class feed : InjectTest
    {

        private readonly ERP.Services.Stores.StoreService storeService;
        private readonly IOrderFactory _ApiOrderFactory;
        private readonly DBContext dBContext;
        private readonly ERP.Services.Caches.AmazonSPKey CacheAmazonSpKey;
        private readonly ERP.Services.Caches.Store storeCache;
        private readonly ERP.Services.Caches.PlatformData CachePlatformData;
        private readonly ERP.Services.DB.Orders.Order orderService;
        private readonly ERP.Services.DB.ProductType.ProductHost productHost;
        private readonly ERP.Services.DB.Stores.AsinProduct asinProduct;
        private readonly ProductUploadService productUploadService;
        private readonly ProductTypeService productTypeService;

        public feed()
        {
            storeService = Resolve<ERP.Services.Stores.StoreService>();
            _ApiOrderFactory = Resolve<IOrderFactory>();
            dBContext = Resolve<DBContext>();

            orderService = Resolve<ERP.Services.DB.Orders.Order>();
            productHost = Resolve<ERP.Services.DB.ProductType.ProductHost>();
            asinProduct = Resolve<ERP.Services.DB.Stores.AsinProduct>();

            productTypeService = Resolve<ProductTypeService>();
        }

        [Test]
        public void TestMethod()
        {
            var f = 8 % 1;
            Console.WriteLine(f);
        }

        private string productAllPath = @"E:\product-copy.json";

        //[Test]
        //public async Task GetProductInfoBySku()
        //{
        //    StoreRegion store = await storeService.GetInfoById(711);
          
        //    var marketLst = await storeService.GetMarketInfoByHashNoTrack(store.UniqueHashCode, store.UserID);
        //    if (marketLst.IsNotNull() && marketLst.Count > 0)
        //    {
        //        store.MarketPlaces = new List<string> { "A2EUQ1WTGCTBG2" };
        //    }
        //    var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
        //    var asin = await asinProduct.Get(2367218996242548196, 711);
        //    var result = _ApiOrderServices.getListingsItem("A2EUQ1WTGCTBG2", store, asin);
        //}


        [Test]
        public async Task SubmitProducts()
        {
            var json = File.ReadAllText(@"D:\item.json");
            var store = await GetStore();
            //构造完整json结构
            string sellerID = store.GetSPAmazon().SellerID;// key.SellerID;
            Dictionary<int, List<IndexInfo>> lts = new();
            ListingFeed<JObject> listingFeed = new();
            listingFeed._Header = new();
            listingFeed._Header.SellerId = sellerID;
            listingFeed._Header.IssueLocale = "en_CA";
            listingFeed._Header.Version = "2.0";
            listingFeed.Messages = new List<MessageInfo<JObject>>();
            int i = 0;
            List<IndexInfo> strings = new List<IndexInfo>();
            var payload = JObject.Parse(json);
            //if (payload.GetValue("recommended_browse_nodes") != null)
            //{
            //    var value = (string)payload["recommended_browse_nodes"][0]["value"];
            //    payload["recommended_browse_nodes"][0]["value"] = value.Trim();
            //}
            //if (payload.GetValue("merchant_shipping_group") != null)
            //{
            //    //var s = (string)payload["merchant_shipping_group"][0]["value"];
            //    //if (string.IsNullOrWhiteSpace(s))
            //    //{
            //    JObject channel = (JObject)payload["merchant_shipping_group"][0];
            //    channel.Remove();
            //    //}
            //}
            var messageInfo = new MessageInfo<JObject>();
            messageInfo.MessageId = 1;
            messageInfo.Sku = "2xA37PmHuTT";
            messageInfo.ProductType = "WATCH";
            messageInfo.Attributes = payload;
            messageInfo.OperationType = "PARTIAL_UPDATE";   //PARTIAL_UPDATE  部分更新，Requirements参数不需要
                                                            // messageInfo.Requirements = "LISTING";
            listingFeed.Messages.Add(messageInfo);

            var jsonStr = JsonConvert.SerializeObject(listingFeed);
            var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
            var result = await _ApiOrderServices.SubmitProduicts(store, jsonStr, "A2EUQ1WTGCTBG2");
        }


        [Test]
        public async Task SubmitProductsAll()
        {
            var jsonStr = File.ReadAllText(@"D:\items.json");
            var store = await GetStore();
            var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
            var result = await _ApiOrderServices.SubmitProduicts(store, jsonStr, "A2EUQ1WTGCTBG2");
        }

        [Test]
        public async Task SubmitProductTest()
        {
            var payload = File.ReadAllText(productAllPath);
            var store = await GetStore();
            var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
            await _ApiOrderServices.SubmitProduicts(store, payload, "A2EUQ1WTGCTBG2");
        }


        private string productPath = @"E:\product.json";

        [Test]
        public async Task SubmitProductByApi()
        {
            var attributeJson = File.ReadAllText(productPath);

            var store = await GetStore();
            var sellerID = store.GetSPAmazon()!.SellerID;
            ParameterPutListingItem request = new ParameterPutListingItem();
            request.SellerId = sellerID;
            request.MarketplaceIds = store.MarketPlaces;
            request.Sku = "My-SKU-C";
            request.IssueLocale = "en_CA";
            request.BodyRequest = true;
            request._ListingsItemPutRequest = new ListingsItemPutRequest();
            request._ListingsItemPutRequest.ProductType = "WATCH";
            request._ListingsItemPutRequest.Requirements = RequirementsEnum.LISTINGPRODUCTONLY;
            request._ListingsItemPutRequest.Attributes = attributeJson;
            request.Check();

            var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
            var s = JsonConvert.SerializeObject(request);
            await _ApiOrderServices.SubmitProduicts(store, JsonConvert.SerializeObject(request), "A2EUQ1WTGCTBG2");
        }

        [Test]
        public void GetSku()
        {
            var sku = Helpers.SkuCode();
            Assert.IsNotEmpty(sku);
        }

        //[Test]
        //public async Task PutListingsItem()
        //{
        //    var jsonStr = File.ReadAllText(@"D:\item.json");
        //    var store = await GetStore();
        //    var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
        //    _ApiOrderServices.PutListingsItemAsync(store, jsonStr, new List<string>() { "ATVPDKIKX0DER" });
        //}


        //保留

        //[Test]
        //public async Task SubmitFeedAddProductMessage()
        //{
        //    var store = await GetStore();
        //    var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
        //    _ApiOrderServices.AddProductMessage(store, "A2EUQ1WTGCTBG2");

        //}


        public async Task<StoreRegion?> GetStore(int storeid = 711)
        {
            StoreRegion? store = await storeService.GetInfoById(storeid);
            if (store == null)
            {
                Assert.Fail("store is null");
            }
            var marketLst = await storeService.GetAllMarketInfoByHash(store!.UniqueHashCode);
            if (marketLst.IsNull() || marketLst!.Count <= 0)
            {
                Assert.Fail("store marketLst is null");
            }
            store.MarketPlaces = marketLst!.Select(x => x.Marketplace).ToList();
            return store;
        }



        //feedid:


        [Test]
        public async Task UploadExcel()
        {
            try
            {
                string text = "D:\\6_美国.xlsx";
                var store = await GetStore();
                var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
                await _ApiOrderServices.UploadExcelUrl(store, "ATVPDKIKX0DER", text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }           
        }

        [Test]
        public async Task GetDetail()
        {
            var store = await GetStore();
            var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
            var res = await _ApiOrderServices.GetFeedDetail("54832019579", store);

        }


        //[Test]

        //public async Task getListingsItem()
        //{
        //    var store = await GetStore();
        //    var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
        //    AsinProduct asin = new AsinProduct();
        //    asin.Sku = "1KDJtbwvqdC";// "2xA37PmHuTS";
        //    var result = _ApiOrderServices.getListingsItem("ATVPDKIKX0DER", store, asin);

        //}

        #region 跟卖

        [Test]
        public async Task SearchCatalogItems()
        {
            var store = await GetStore();
            var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
            var asin = await asinProduct.Get(2367218996242548196, 711);          
            var result = _ApiOrderServices.SearchCatalogItems(store, "ATVPDKIKX0DER", new List<string> { "B006DWKIG0" });

        }

        //B0C14ZHD9M  墨西哥成功的那个例子

        //B0BJ5TX2MN

        //[Test]
        //public async Task getCatalogItems()
        //{
        //    var store = await GetStore();
        //    var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);

        //    var asin = await asinProduct.Get(2367218996242548196, 711);           
        //    var result = _ApiOrderServices.GetCatalogItems("ATVPDKIKX0DER", store, "B006DWKIG0");
        //}

        //B0C14ZHD9M 墨西哥成功的那个例子
            //B006DWKIG0  
            //B006DWKJM8

            [Test]
        public async Task HijackProducts()
        {
            var json = File.ReadAllText(@"D:\item.json");
            var store = await GetStore();
            //构造完整json结构
            string sellerID = store.GetSPAmazon().SellerID;
            Dictionary<int, List<IndexInfo>> lts = new();
            ListingFeed<JObject> listingFeed = new();
            listingFeed._Header = new();
            listingFeed._Header.SellerId = sellerID;
            listingFeed._Header.IssueLocale = "en_US";
            listingFeed._Header.Version = "2.0";
            listingFeed.Messages = new List<MessageInfo<JObject>>();
            int i = 0;
            List<IndexInfo> strings = new List<IndexInfo>();
            var payload = JObject.Parse(json);
            var messageInfo = new MessageInfo<JObject>();
            messageInfo.MessageId = 1;
            messageInfo.Sku = "1KDJtbwvqdC";
            messageInfo.ProductType = "CLOCK";
            messageInfo.Attributes = payload;
            messageInfo.OperationType = "PARTIAL_UPDATE";   //PARTIAL_UPDATE  部分更新，Requirements参数不需要            
            listingFeed.Messages.Add(messageInfo);
            var jsonStr = JsonConvert.SerializeObject(listingFeed);
            var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
            var result = await _ApiOrderServices.SubmitProduicts(store, jsonStr, "ATVPDKIKX0DER");
        }

        [Test]
        public async Task HijackProductsByjson()
        {
            var json = File.ReadAllText(@"D:\items.json");
            var store = await GetStore();
            var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
            var result = await _ApiOrderServices.SubmitProduicts(store, json, "ATVPDKIKX0DER");
        }
        #endregion


        #region 各种语言编码

        //西班牙语
        //private string s = "En los Juegos Asiáticos de Hangzhou soplaba el fuerte viento de Asia, mostrando el estilo asiático de respirar, sentir y soñar juntos.";

        private string s = "Ai Giochi Asiatici di Hangzhou soffiava il potente vento dell'Asia, mostrando lo stile asiatico di respirare, sentire e sognare insieme.";  //法语
        [Test]
        public void TestLanguage()
        {           
            // convert string to stream
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream,Encoding.GetEncoding("ISO-8859-1"));
            writer.Write(s);
            writer.Flush();

            

            // convert stream to string
            stream.Position = 0;
            StreamReader reader = new StreamReader(stream, Encoding.GetEncoding("ISO-8859-1"));
            string text = reader.ReadToEnd();
            Console.WriteLine(text);
        }
       


        #endregion


    }
}



