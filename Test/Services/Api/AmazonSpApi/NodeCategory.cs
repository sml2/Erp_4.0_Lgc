using System;
using System.Collections.Generic;
using System.Linq;
using ERP.Data;
using ERP.Models.Stores;
using NUnit.Framework;
using Task = System.Threading.Tasks.Task;
using categoryModel = ERP.Models.ProductNodeType.Amazon.Category;
using System.IO;
using System.Xml.Serialization;
using System.Diagnostics;
using Newtonsoft.Json;
using ERP.Enums;
using ERP;
using ERP.Services.Api;
using OrderSDK.Modles.Amazon;
using System.Threading.Tasks;
using ERP.Models.Stores;

namespace Test.Services.Api.AmazonSpApi
{
    public class NodeCategory : InjectTest
    {
        private readonly ERP.Services.Stores.StoreService storeService;
        private readonly IOrderFactory _ApiOrderFactory;
        private readonly DBContext dBContext;       
        private readonly ERP.Services.DB.ProductType.CategoryService categoryService;
        public NodeCategory()
        {
            storeService = Resolve<ERP.Services.Stores.StoreService>();
            _ApiOrderFactory = Resolve<IOrderFactory>();
            dBContext = Resolve<DBContext>();           
            categoryService = Resolve<ERP.Services.DB.ProductType.CategoryService>();
        }


        public async Task<StoreRegion?> GetStore(int storeid = 711)
        {
            StoreRegion? store = await storeService.GetInfoById(storeid);
            if (store == null)
            {
                Assert.Fail("store is null");
            }
            var marketLst = await storeService.GetAllMarketInfoByHash(store!.UniqueHashCode);
            if (marketLst.IsNull() || marketLst!.Count <= 0)
            {
                Assert.Fail("store marketLst is null");
            }
            store.MarketPlaces = marketLst!.Select(x => x.Marketplace).ToList();
            return store;
        }


        public CategoiesData GetCategoryNodes(string path)
        {
            //西班牙 1 :A1RKKUPIHCS9HS   37301  path: D:\\09e91189-9299-4381-949c-8ef20578580e-A1RKKUPIHCS9HS
            //英国 2 ：A1F83G8C2ARO7P    47297  path: D:\\22e46b6b-130c-4b8e-836d-701357f9a876-A1F83G8C2ARO7P
            //法国 3 ：A13V1IB3VIYZZH    39572  path = "D:\\9d81d8de-821d-4e0b-a7f8-489fd110f49e-A13V1IB3VIYZZH";
            //德国 4 ：A1PA6795UKMFR9   47810   path:D:\\53bd8bc9-b1c7-4575-9504-93e0ca351c8b-A1PA6795UKMFR9
            //意大利 5 ：APJ6JRA9NG5V4    37286 path: D:\\0128e3c8-0d86-4613-93ba-ca05cbf11afe-APJ6JRA9NG5V4
            //加拿大  C:\Users\Administrator\AppData\Local\Temp\4ecbc444-aa29-4517-a250-5c92d2801325-A2EUQ1WTGCTBG2
            //美国：C:\Users\Administrator\AppData\Local\Temp\420eeeb9-b471-4cc8-8b08-b47616acaee7-ATVPDKIKX0DER

            CategoiesData Data = new CategoiesData();
            var xml = File.ReadAllText(path);
            var serializer = new XmlSerializer(typeof(CategoiesData));
            try
            {
                using (StringReader reader = new StringReader(xml))
                {
                    Data = (CategoiesData)serializer.Deserialize(reader);

                    //保存入库
                    return Data;
                }
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        public int Count = 500;

        [Test]
        public async Task GetReportTest()
        {
            StoreRegion? store = await GetStore();
            int allCount = 0;
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            var transaction = dBContext.Database.BeginTransaction();
            var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
            foreach (var Marketplace in store.MarketPlaces)
            {               
                var Datas = await _ApiOrderServices.GetReportBowerIDs(store, Marketplace);
                int i = 0;
                var listNodes = Datas.Node;
                allCount += listNodes.Count;
                int n = listNodes.Count / Count;
                List<categoryModel> categoryList = new List<categoryModel>();

                while (listNodes.Skip(i * Count).Take(Count).ToList().Any())
                {
                    var itemListNodes = listNodes.Skip(i * Count).Take(Count).ToList();
                    foreach (var data in itemListNodes)
                    {
                        long categoryId = 0, parentCategoryId = 0;
                        int root = 0;
                        if (!long.TryParse(data.BrowseNodeId, out categoryId))
                        {
                            Console.WriteLine($"{data.BrowseNodeId} 转int失败");
                        }

                        var pathID = data.BrowsePathById;
                        if (!pathID.IsNullOrWhiteSpace())
                        {
                            var pId = pathID.Split(',').Reverse().ToArray();
                            root = pId.Length;
                            if (!long.TryParse(pId[1], out parentCategoryId))
                            {
                                Console.WriteLine($"path:{pathID},pid:{pId} 转int失败");
                            }
                        }
                        var productType = data.ProductTypeDefinitions;

                        categoryModel category = new categoryModel(data.BrowseNodeName, data.BrowseNodeStoreContextName, ERP.Enums.Platforms.AMAZONSP, categoryId, parentCategoryId, productType, Marketplace, data.HasChildren, data.BrowsePathById, root == 2 ? true : false);
                        categoryList.Add(category);
                    }
                    await dBContext.NodeCategory.AddRangeAsync(categoryList);
                    i++;
                }
                try
                {
                    await dBContext.SaveChangesAsync();
                    await transaction.CommitAsync();
                }
                catch (Exception e)
                {
                    await transaction.RollbackAsync();
                    Console.WriteLine(e);
                    Assert.Fail();
                }
            }
            stopwatch.Stop();
            Console.WriteLine($"all count:{allCount},spand time:{stopwatch.ElapsedMilliseconds} Milliseconds");
            Assert.IsTrue(allCount > 0);
        }
       
          
     

        [Test]
        public async Task GetReportBowerIdTest()
        {
            StoreRegion? store = await GetStore();           
            var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
            var Datas = await _ApiOrderServices.GetReportBowerIDs(store, "A2EUQ1WTGCTBG2");
            Assert.IsNotNull(Datas);
        }

        [Test]
        public void Test()
        {
            Platforms enumPlatform = Platforms.AMAZONSP;
            var s = enumPlatform.ToString();
            var ss = enumPlatform.GetDescription();
            var sss = enumPlatform.GetDescriptionByKey(s);
        }

        //地区，市场，店铺
        // 同一店铺下 同一地区，不同市场的node节点
        //不同店铺，  同一地区  同一市场的node节点
        //同店铺，  不同地区   同一市场的node节点   不存在

        //78 欧洲地区
        //西班牙 :A1RKKUPIHCS9HS   37301
        //英国 ：A1F83G8C2ARO7P    47297
        //法国 ：A13V1IB3VIYZZH    39572
        //德国 ：A1PA6795UKMFR9   47810
        //意大利 ：APJ6JRA9NG5V4    37286

        //荷兰： A1805IZSGTT6HS    37312
        //瑞典 ： A2NODRKZP88ZB9
        //波兰 ：A1C3SOZRARQ6R3
        //埃及 ：ARBP9OOSHTCHU
        //土耳其：A33AVAJ2PDY3EV
        //沙特阿拉伯 ： A17E79C6D8DWNP
        //阿联酋 ： A2VIGQ35RCS4UG

        //72
        //西班牙 :A1RKKUPIHCS9HS   37301
        //英国 ：A1F83G8C2ARO7P    47297 
        //法国 ：A13V1IB3VIYZZH
        //德国 ：A1PA6795UKMFR9
        //意大利 ：APJ6JRA9NG5V4
        //荷兰： A1805IZSGTT6HS    
        //瑞典 ： A2NODRKZP88ZB9
        //波兰 ：A1C3SOZRARQ6R3
        //埃及 ：ARBP9OOSHTCHU
        //土耳其：A33AVAJ2PDY3EV
        //沙特阿拉伯 ： A17E79C6D8DWNP
        //阿联酋 ： A2VIGQ35RCS4UG

        //美国   ATVPDKIKX0DER     61222
        //只要是同一市场，取到的是全部节点，并且都是一样的
    }

}



