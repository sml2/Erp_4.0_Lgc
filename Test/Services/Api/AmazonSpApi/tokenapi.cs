using ERP.Data;
using ERP.Models.Stores;
using ERP.Services.Api;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Services.Api.AmazonSpApi
{
    public class testApi : InjectTest
    {
        //

        private readonly ERP.Services.Stores.StoreService storeService;
        private readonly IOrderFactory _ApiOrderFactory;
        private readonly DBContext dBContext;
        private readonly ERP.Services.Caches.AmazonSPKey CacheAmazonSpKey;
        private readonly ERP.Services.Caches.PlatformData CachePlatformData;
        private readonly ERP.Services.DB.Orders.Order orderService;
        private readonly ERP.Services.Caches.Store CacheStore;
        public testApi()
        {
            storeService = Resolve<ERP.Services.Stores.StoreService>();
            _ApiOrderFactory = Resolve<IOrderFactory>();
            dBContext = Resolve<DBContext>();
            CacheAmazonSpKey = Resolve<ERP.Services.Caches.AmazonSPKey>();
            CachePlatformData = Resolve<ERP.Services.Caches.PlatformData>();
            orderService = Resolve<ERP.Services.DB.Orders.Order>();
        }       
    }
}
