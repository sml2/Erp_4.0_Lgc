using System;
using System.Collections.Generic;
using System.Linq;
using ERP.Data;
using ERP.Models.Stores;
using NUnit.Framework;
using System.IO;
using System.Diagnostics;
using System.Net;
using Newtonsoft.Json.Linq;
using ERP.Enums;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json;
using ERP;
using ERP.Models.ProductNodeType.Amazon;

using ProductTypeService = ERP.Services.DB.ProductType.ProductTypeService;
using Task = System.Threading.Tasks.Task;

using NuGet.Protocol;
using ERP.Services.Api;
using Coravel.Tasks;
using System.Threading.Tasks;
using ERP.Models.Stores;

namespace Test.Services.Api.AmazonSpApi
{
    public class ProductType : InjectTest
    {
        private readonly ERP.Services.Stores.StoreService storeService;
        private readonly IOrderFactory _ApiOrderFactory;
        private readonly DBContext dBContext;      
        private readonly ERP.Services.DB.ProductType.CategoryService categoryService;
        private readonly ERP.Services.DB.Orders.Order orderService;
        private readonly ProductTypeService productTypeService;

        public ProductType()
        {
            storeService = Resolve<ERP.Services.Stores.StoreService>();
            _ApiOrderFactory = Resolve<IOrderFactory>();
            dBContext = Resolve<DBContext>();
            categoryService = Resolve<ERP.Services.DB.ProductType.CategoryService>();
            orderService = Resolve<ERP.Services.DB.Orders.Order>();
            productTypeService = Resolve<ProductTypeService>();
        }


        public async Task<StoreRegion?> GetStore(int storeid = 711)
        {
            StoreRegion? store = await storeService.GetInfoById(storeid);
            if (store == null)
            {
                Assert.Fail("store is null");
            }
            var marketLst = await storeService.GetAllMarketInfoByHash(store!.UniqueHashCode);
            if (marketLst.IsNull() || marketLst!.Count <= 0)
            {
                Assert.Fail("store marketLst is null");
            }
            store.MarketPlaces = marketLst!.Select(x => x.Marketplace).ToList();
            return store;
        }


        [Test]
        public async Task GetProductType()
        {
            StoreRegion? store = await GetStore();
            var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
            foreach (var Marketplace in store.MarketPlaces)
            {
                if (!Marketplace.Equals("A2EUQ1WTGCTBG2"))
                {
                    continue;
                }
                try
                {                   
                    store.MarketPlaces = new List<string>() { Marketplace };
                    var list = await _ApiOrderServices.SearchDefinitionsProductTypes(store); //取出该地区所有的产品分类
                    if (list != null)
                    {
                        foreach (var data in list.ProductTypes)
                        {
                            var attributeList = await _ApiOrderServices.GetProductTypeModels(store, Marketplace, data.Name);
                            using (var transaction = await dBContext.Database.BeginTransactionAsync())
                            {
                                try
                                {
                                    if (await productTypeService.AddRangeAsync(attributeList, categoryService))
                                    {
                                        await transaction.CommitAsync();
                                    }
                                    else
                                    {
                                        await transaction.RollbackAsync();
                                    }
                                }
                                catch (Exception e)
                                {
                                    await transaction.RollbackAsync();
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {

                }
            }
        }





        [Test]
        public async Task SearchDefinitionsProductTypesAsync()
        {
            StoreRegion? store = await GetStore();
            var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
            var listOrder = _ApiOrderServices.SearchDefinitionsProductTypes(store);
            Assert.IsNotNull(listOrder);
        }

        private string productPath = @"E:\product.json";


        [Test]
        public async Task CheckJson()
        {
            StoreRegion? store = await GetStore();
            var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
            var DefinitionsProductTypes = await _ApiOrderServices.GetDefinitionsProductTypes(store, "A2EUQ1WTGCTBG2", "PERSONAL_CARE_APPLIANCE");
            if (DefinitionsProductTypes is not null && DefinitionsProductTypes.MetaSchema is not null)
            {
                //分析属性组
                var metaSchema = DefinitionsProductTypes.MetaSchema;
                var metaSchemaLink = (JToken)metaSchema.Link;
                var metaSchemaUrl = metaSchemaLink.First.First.ToString();
                var metaSchemaMethod = metaSchemaLink.Last.First.ToString();
                var schema = DefinitionsProductTypes.Schema;
                var SchemaLink = (JToken)schema.Link;
                var SchemaUrl = SchemaLink.First.First.ToString();
                var SchemaMethod = SchemaLink.Last.First.ToString();
                var SchemaJson = GetProperties(metaSchemaUrl, SchemaUrl);
                if (SchemaJson is not null)
                {
                    var version = DefinitionsProductTypes.ProductTypeVersion;//可以依据这个来判断是否需要更新属性
                    var locale = DefinitionsProductTypes.Locale;

                    var json = SchemaJson.ToJson();
                    ////验证产品json
                    var payload = JObject.Parse(JObject.Parse(File.ReadAllText(productPath)).ToString(Formatting.Indented));
                    ////验证有效负载并获取任何生成的验证消息。
                    //// Validate the payload and get any resulting validation messages.
                    var valid = payload.IsValid(SchemaJson, out IList<string> errorMessages);
                }
            }
        }

        public JSchema? GetProperties(string metaSchemaUrl, string SchemaUrl)
        {
            try
            {
                //使用模式的本地副本而不是从 Web 检索它们的模式解析器。
                // Schema resolver that uses local copies of schemas rather than retrieving them from the web.
                var resolver = new JSchemaPreloadedResolver();
                var metaSchemajson = GetJsonFormUrl(metaSchemaUrl);
                var Schemajson = GetJsonFormUrl(SchemaUrl);
                var payload = JObject.Parse(metaSchemajson);
                if (payload is not null)
                {
                    //将元模式添加到解析器。
                    //Add the meta-schema to the resolver.
                    foreach (var v in payload)
                    {
                        if ("$id".Equals(v.Key))
                        {
                            //亚马逊产品类型定义元架构的 $id。
                            // $id of the Amazon Product Type Definition Meta-Schema.
                            var schemaId = v.Value.ToString();
                            resolver.Add(new Uri(schemaId), metaSchemajson);
                            //使用解析器和关键字验证器配置读取器设置以在解析元模式实例时使用。
                            //Configure reader settings with resolver and keyword validators to use when parsing instances of the meta-schema.
                            var readerSettings = new JSchemaReaderSettings
                            {
                                Resolver = resolver,
                                Validators = new List<JsonValidator>
                                {
                                    new MaxUniqueItemsKeywordValidator(),
                                    new MaxUtf8ByteLengthKeywordValidator(),
                                    new MinUtf8ByteLengthKeywordValidator()
                                }
                            };
                            return JSchema.Parse(Schemajson, readerSettings);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"GetProperties error,SchemaUrl:{SchemaUrl}");
            }
            return null;
        }


        public string GetJsonFormUrl(string url)
        {
            using (var client = new WebClient())
            {
                var stream = client.OpenReadTaskAsync(url).Result;

                using (var Reader = new StreamReader(stream))
                {
                    return Reader.ReadToEnd();
                }
            }
        }



        [Test]
        public void TranscationTime()
        {
            //value.CreateTime.ToString("yyyy-HH-MM hh:mm:ss"),

            var currentTime = DateTime.Now;
            var time = currentTime.ToString("yyyy-MM-dd HH:mm:ss");
            Console.WriteLine(time);
        }


        [Test]
        public async void UpdateJsonSchema()
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            stopwatch.Start();
            StoreRegion? store = await GetStore();
            var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform); //WATCH
            var DefinitionsProductTypes = await _ApiOrderServices.GetDefinitionsProductTypes(store, "A2EUQ1WTGCTBG2", "HEALTH_PERSONAL_CARE");
            if (DefinitionsProductTypes is not null && DefinitionsProductTypes.MetaSchema is not null)
            {
                //分析属性组
                var metaSchema = DefinitionsProductTypes.MetaSchema;
                var metaSchemaLink = (JToken)metaSchema.Link;
                var metaSchemaUrl = metaSchemaLink.First.First.ToString();
                var metaSchemaMethod = metaSchemaLink.Last.First.ToString();
                var schema = DefinitionsProductTypes.Schema;
                var SchemaLink = (JToken)schema.Link;
                var SchemaUrl = SchemaLink.First.First.ToString();
                var SchemaMethod = SchemaLink.Last.First.ToString();
                var SchemaJson = GetProperties(metaSchemaUrl, SchemaUrl);
                if (SchemaJson is not null)
                {
                    //获取propertyGroup
                    var type = DefinitionsProductTypes.ProductType;
                    var version = DefinitionsProductTypes.ProductTypeVersion;//可以依据这个来判断是否需要更新属性                      
                    var locale = DefinitionsProductTypes.Locale;
                    foreach (var p in SchemaJson.Properties)  //几大分组
                    {
                        var name = p.Key;
                        var item = SchemaJson.Properties[name];
                        //解析节点属性
                        if (item is not null)
                        {
                            UpdateDefault(item, "");
                        }
                    }
                    var json = SchemaJson.ToString();
                }
                ////验证产品json
                var payload = JObject.Parse(File.ReadAllText("./payload.json"));
                ////验证有效负载并获取任何生成的验证消息。
                //// Validate the payload and get any resulting validation messages.
                var valid = payload.IsValid(SchemaJson, out IList<string> errorMessages);
            }
            stopwatch.Stop();
            Console.WriteLine($"总耗时：{stopwatch.ElapsedMilliseconds}毫秒");
            Assert.IsNotNull(DefinitionsProductTypes);
        }

        public void UpdateDefault(JSchema jSchema, string name)
        {
            if (jSchema.Items.Count > 0)
            {
                UpdateDefault(jSchema.Items[0], "");
            }
            else if (jSchema.Items.Count <= 0 && jSchema.Properties.Count > 0)
            {
                foreach (var properity in jSchema.Properties.Keys)
                {
                    var itemP = jSchema.Properties[properity];
                    UpdateDefault(itemP, properity);
                }
            }
            else if (jSchema.Items.Count <= 0 && jSchema.Properties.Count <= 0)
            {
                if (name == "marketplace_id" || name == "language_tag")
                {
                    jSchema.Title = name;
                }
                else
                {

                    if (jSchema.Type == JSchemaType.String)
                    {
                        if (jSchema.Enum is not null && jSchema.Enum.Count > 0)
                        {
                            if (jSchema.Default is null) jSchema.Default = jSchema.Enum.First();
                        }
                        else if (jSchema.Default is null)
                        {
                            jSchema.Default = "";
                        }
                        else
                        {

                        }
                    }
                    else if (jSchema.Type == JSchemaType.Integer || jSchema.Type == JSchemaType.Number)
                    {
                        if (jSchema.Default is null) { jSchema.Default = 0; }
                        else
                        {

                        }
                    }
                    else if (jSchema.Type == JSchemaType.Boolean)
                    {
                        if (jSchema.Default is null) jSchema.Default = false;
                    }
                }
            }
            else
            {
                throw new Exception($"出现特殊情况:{jSchema.Title}");
            }
        }

    }
}



