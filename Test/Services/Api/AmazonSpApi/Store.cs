using System;
using System.Linq;
using System.Threading.Tasks;
using ERP.Models;
using ERP.Models.Stores;
using ERP.Services.Api;
using NUnit.Framework;
using Task = System.Threading.Tasks.Task;
using TokenRes = OrderSDK.Modles.Amazon.Authorization.TokenResponse;

using System.Text;
using System.Web;
using ERP.ViewModels;
using ERP.Models.Stores;

namespace Test.Services.Api.AmazonSpApi
{
    public class Store : InjectTest
    {
        private readonly ERP.Services.Stores.StoreService storeService;        
        private readonly IOrderFactory _ApiOrderFactory;
       
        private readonly ERP.Services.Caches.AmazonSPKey CacheAmazonSpKey ;
        private readonly ERP.Services.Caches.PlatformData CachePlatformData ;
      
        private readonly ERP.Services.Caches.Store StoreCache;
        public Store()
        {           
            storeService = Resolve<ERP.Services.Stores.StoreService>();
            _ApiOrderFactory = Resolve<IOrderFactory>();          
            CacheAmazonSpKey = Resolve<ERP.Services.Caches.AmazonSPKey>();
            CachePlatformData= Resolve<ERP.Services.Caches.PlatformData>();                     
            StoreCache = Resolve<ERP.Services.Caches.Store>();
        }

        [Test]
        public void GetStoreFromCache()
        {
            var store1 = StoreCache.Get(253);
            var h = store1.GetHashCode();
            var store2 = StoreCache.Get(253);
            var h2 = store2.GetHashCode();
            Assert.Equals(h, h2);
        }

        [Test]
        public void testUrl()
        {
            string url = "spapi_oauth_code=ANhdAHwHwSxapXufKxRf&state=962%7C131520230626T094348Z&selling_partner_id=A2PMJMJF144QMA";

          var s=  HttpUtility.UrlDecode(url);


        }


        [Test]
        public async Task GetAuthUrl()
        {
            StoreRegion store = await storeService.GetInfoById(711);
            if(store ==null)
            {
                Assert.Fail("store is null");
            }            
            var marketLst = await storeService.GetAllMarketInfoByHash(store.UniqueHashCode);
            if (marketLst.IsNotNull() && marketLst.Count > 0)
            {
                store.MarketPlaces = marketLst!.Select(x => x.Marketplace).ToList();
            }
            var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
            var result=  _ApiOrderServices.GetAuthURL(store, "124");
            Assert.True(result!="","获取url成功");
        }

        [Test]
        public async Task VerifyStore()
        {
            StoreRegion store = await storeService.GetInfoById(711);
            if (store == null)
            {
                Assert.Fail("store is null");
            }           
            var marketLst = await storeService.GetAllMarketInfoByHash(store.UniqueHashCode);
            if (marketLst.IsNotNull() && marketLst.Count > 0)
            {
                store.MarketPlaces = marketLst!.Select(x => x.Marketplace).ToList();
            }
            var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
            var result =await _ApiOrderServices.VerifyStore<TokenRes>(store, "124");
            Assert.IsNotNull(result);
        }


        [Test]
        public async Task RefreshNormalAccessToken()
        {
            StoreRegion store = await storeService.GetInfoById(711);
            if (store == null)
            {
                Assert.Fail("store is null");
            }

            var marketLst = await storeService.GetAllMarketInfoByHash(store.UniqueHashCode);
            if (marketLst.IsNotNull() && marketLst.Count > 0)
            {
                store.MarketPlaces = marketLst!.Select(x => x.Marketplace).ToList();
            }
            var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
            var result = await _ApiOrderServices.RefreshNormalAccessToken(store);
            Assert.True(result != null, "Refresh token 成功");
        }

        [Test]
        public async Task GetMarketIds()
        {
            IdDto idDto = new IdDto();
            idDto.ID = 1117;

            var result= await storeService.CheckMarketIDs(idDto);

            //StoreRegion store = await storeService.GetInfoById(662);
            //if (store == null)
            //{
            //    Assert.Fail("store is null");
            //}

            //var marketLst = await storeService.GetMarketInfoByHashNoTrack(store.UniqueHashCode, store.UserID);
            //if (marketLst.IsNotNull() && marketLst.Count > 0)
            //{
            //    store.MarketPlaces = marketLst!.Select(x => x.Marketplace).ToList();
            //}
            //var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
            //var result = await _ApiOrderServices.GetMarketIds(store);
            Assert.IsNotNull(result);
        }
    }
}



