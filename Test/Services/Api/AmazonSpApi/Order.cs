using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using ERP.Data;
using ERP.Models;

using ERP.Models.Stores;
using ERP.Services.Api.ElectronicBusiness;

using ERP.Services.DB.ProductType;
using ERP.Services.Stores;
using ERP.ViewModels;
using ERP.ViewModels.Store.Remote;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NPOI.HSSF.Record;
using NPOI.SS.Formula.Functions;
using NUnit.Framework;
using Task = System.Threading.Tasks.Task;
using ERP.Models.ProductNodeType;
using Amazon.Runtime.Internal.Util;
using ERP.Services.Caches;
using System.IO;
using System.Xml.Serialization;
using System.Diagnostics;
using System.Globalization;
using System.Security.Cryptography;

using ERP.Enums;
using ERP.Models.DB.Logistics;
using ERP.Models.View.Order.AmazonOrder;
using Newtonsoft.Json;
using static ERP.Services.BaseService;
using ERP.Interface.Rule;
using ERP.Services.Api;

namespace Test.Services.Api.AmazonSpApi
{
    //public class Order : InjectTest
    //{
    //    private readonly ERP.Services.Stores.StoreService storeService;
    //    private readonly IOrderFactoryNew _ApiOrderFactory;
    //    private readonly DBContext dBContext;
    //    private readonly ERP.Services.DB.Orders.Order orderService;
    //    public Order()
    //    {            
    //        _ApiOrderFactory = Resolve<IOrderFactoryNew>();
    //        dBContext = Resolve<DBContext>();
    //        orderService = Resolve<ERP.Services.DB.Orders.Order>();
    //        storeService = Resolve<ERP.Services.Stores.StoreService>();
    //    }

    //    [Test]
    //    public async Task GetAllAsinByStore()
    //    {
    //        StoreRegion store = await storeService.GetInfoById(711);
    //        var marketLst = await storeService.GetMarketInfoByHashNoTrack(store.UniqueHashCode, store.UserID);
    //        if (marketLst.IsNotNull() && marketLst.Count > 0)
    //        {
    //            store.MarketPlaces = marketLst!.Select(x => x.Marketplace).ToList();
    //        }
    //        var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);

    //        var listAsin = _ApiOrderServices.GetAllAsin(store);
    //        Assert.IsNotNull(listAsin);
    //    }


    //    [Test]
    //    public async Task TimeTest()
    //    {
    //        DateTime start = Convert.ToDateTime("2022-10-16 00:12:26");
    //        DateTime end = Convert.ToDateTime("2022-10-17 16:00:39");
    //        string worldStart = start.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ", DateTimeFormatInfo.InvariantInfo);
    //        string worldEnd = end.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ", DateTimeFormatInfo.InvariantInfo);
    //        Console.WriteLine(worldStart);
    //        Console.WriteLine(worldEnd);
    //    }

    //    [Test]
    //    public void StoreTest()
    //    {
    //        var t = DateTime.MinValue;
    //        var result = dBContext.StoreRegion.AsNoTracking().ToList();
    //        Assert.AreEqual(0, result.Count);
    //    }

    //    [Test]
    //    public void TestInclude()
    //    {

    //        var result = dBContext.StoreRegion.Include(x => x.Company).ToList();

    //        var s = dBContext.StoreRegion.Include(x => x.Company).ToList().ToString();
    //        var count = result.ToList().Count();
    //        Assert.AreEqual(0, count);
    //    }

    //    [Test]
    //    public void TestExclude()
    //    {
    //        string str = "234234,234231,445645,676845";
    //        var temp = str.Split(",");
    //        var ids = temp.Cast<long>().ToList();

    //    }


    //    [Test]
    //    public async Task TestDatetime()
    //    {
    //        //utc:2023-02-03T12:59:59Z
    //        //数据库：2023-02-03 20:59:59.000000
    //        var order = await dBContext.Order.Where(x => x.OrderNo == "503-0421282-4725434").FirstOrDefaultAsync();
    //        var temp = (DateTime)order.LatestShipTime!;
    //        var time = temp.AddHours(-9).ToString("yyyy-MM-ddTHH:mm:ssZ");
    //    }


    //    [Test]
    //    public async Task Store()
    //    {
    //        Dictionary<int, string> sellerid = new();
    //        //select id,name,QueryEnd,QueryEndPayload,NextPull,OEMID,`Data` from storeregion where IsVerify=1 and IsDel=0 order by name desc
    //        var list = await dBContext.StoreRegion.Where(x => x.IsVerify == StoreRegion.IsVerifyEnum.Certified && x.IsDel == false).ToListAsync();

    //        foreach (var item in list)
    //        {
    //            sellerid.Add(item.ID, item.GetSPAmazon().SellerID);
    //        }

    //        Assert.True(sellerid.Count > 0);
    //    }



    //    [Test]
    //    public async Task GetOrders()
    //    {
    //        //252:100,255:0,256:0,254:0,253:3
    //        StoreRegion store = await storeService.GetInfoById(58);
    //        store.SetAmazonCredential(CacheAmazonSpKey, CachePlatformData, storeCache);
    //        var marketLst = await storeService.GetMarketInfoByHashNoTrack(store.UniqueHashCode, store.UserID);
    //        if (marketLst.IsNotNull() && marketLst.Count > 0)
    //        {
    //            store.MarketPlaces = marketLst!.Select(x => x.Marketplace).ToList();
    //        }
    //        var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
    //        DateTime start = Convert.ToDateTime("2023-03-01 08:00:00");
    //        DateTime end = Convert.ToDateTime("2023-03-28 08:00:00");


    //        ListParameter listParameter = new ListParameter(store, start, end);
    //        var listOrder = _ApiOrderServices.List(listParameter);
    //        Assert.IsNotNull(listOrder);
    //        var orders = listOrder!.waitOrders.OrderBy(x => x.GetAmazonSPParam().LastUpdateDate).ToList();
    //        foreach (var o in orders!)
    //        {
    //            var order = o.GetAmazonSPParam();

    //        }
    //    }

    //    //[Test]
    //    //public async Task GetTest()
    //    //{
    //    //    StoreRegion store = await storeService.GetInfoById(266);
    //    //    store.SetAmazonCredential(CacheAmazonSpKey, CachePlatformData, storeCache);
    //    //    var marketLst = await storeService.GetMarketInfoByHashNoTrack(store.UniqueHashCode, store.UserID);
    //    //    if (marketLst.IsNotNull() && marketLst.Count > 0)
    //    //    {
    //    //       store.MarketPlaces = marketLst!.Select(x => x.Marketplace).ToList();
    //    //    }
    //    //    var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
    //    //    var order =await  orderService.GetOrderById(5013);

    //    //     _ApiOrderServices.GetOrderInfomation(store, order);
    //    //    //Assert.IsNotNull(Order);            
    //    //}

    //    [Test]
    //    public async Task GetOrder()
    //    {
    //        StoreRegion store = await storeService.GetInfoById(55);
    //        store.SetAmazonCredential(CacheAmazonSpKey, CachePlatformData, storeCache);
    //        var marketLst = await storeService.GetMarketInfoByHashNoTrack(store.UniqueHashCode, store.UserID);
    //        if (marketLst.IsNotNull() && marketLst.Count > 0)
    //        {
    //            store.MarketPlaces = marketLst!.Select(x => x.Marketplace).ToList();
    //        }
    //        var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);

    //        var listOrder = _ApiOrderServices.GetOrder(store, "503-0807616-7671806");

    //        Assert.IsNotNull(listOrder);

    //    }

    //    [Test]
    //    public async Task Get()
    //    {
    //        //余露-新加坡
    //        StoreRegion store = await storeService.GetInfoById(55);
    //        store.SetAmazonCredential(CacheAmazonSpKey, CachePlatformData, storeCache);
    //        var marketLst = await storeService.GetMarketInfoByHashNoTrack(store.UniqueHashCode, store.UserID);
    //        if (marketLst.IsNotNull() && marketLst.Count > 0)
    //        {
    //            store.MarketPlaces = marketLst!.Select(x => x.Marketplace).ToList();
    //        }
    //        var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
    //        var orderMdel = _ApiOrderServices.GetTest(store, "503-0807616-7671806");
    //        await orderService.Add(orderMdel);
    //    }




    //    [Test]
    //    public async Task GetTryCount()
    //    {
    //        var result = Test();
    //        Assert.IsNotNull(result);
    //    }

    //    public int Test()
    //    {
    //        int tryCount = 0;
    //        while (true)
    //        {
    //            try
    //            {
    //                var result = 0;
    //                if (result == 0)
    //                {
    //                    throw new Exception("");
    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                tryCount++;
    //                if (tryCount == 3)
    //                {
    //                    return 3;
    //                }
    //            }
    //        }
    //    }

    //    [Test]
    //    public async Task updateStore()
    //    {
    //        StoreRegion store = await storeService.GetInfoById(126);
    //        store.Name = "111";
    //        dBContext.StoreRegion.Update(store);
    //        dBContext.SaveChanges();
    //        store.Name = "箭店北美站";
    //        dBContext.StoreRegion.Update(store);
    //        var res = dBContext.SaveChanges();
    //        Assert.IsNotNull(res);
    //    }


    //    [Test]
    //    public async Task GetTask()
    //    {
    //        var a = T();
    //        var b = T();
    //        var flag = a.Equals(b);
    //    }

    //    public async Task T()
    //    {
    //        await Task.Delay(1000);
    //    }

    //    //[Test]
    //    //public async Task ManualPullOrders()
    //    //{
    //    //    // 96,127
    //    //    StoreRegion store = await storeService.GetInfoById(91);
    //    //    store.SetAmazonCredential(CacheAmazonSpKey, CachePlatformData, storeCache);
    //    //    var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
    //    //    var tokenStatus = await _ApiOrderServices.RefreshNormalAccessToken(store);
    //    //    var result = _ApiOrderServices.GetMarketIds(store);
    //    //    Assert.IsNotNull(result);
    //    //}

    //    [Test]
    //    public async Task TestSaveToken()
    //    {
    //        IdDto amazonCode = new IdDto();
    //        amazonCode.ID = 127;
    //        var result = await storeService.CheckMarketIDs(amazonCode);

    //        Assert.IsNotNull(result);
    //    }

    //    [Test]
    //    public void WEIys()
    //    {
    //        var bits = ERP.Enums.Orders.BITS.Progresses_Unprocessed;
    //        var n = (ulong)bits;

    //        var bits2 = bits |= ERP.Enums.Orders.BITS.Progresses_Complete;
    //        var n2 = (ulong)bits2;

    //        var bits3 = (ulong)bits2 & 512;
    //        var n3 = (ulong)bits3;
    //        Console.WriteLine(n);
    //        Console.WriteLine(n2);
    //        Console.WriteLine(n3);


    //        var bitinit = ERP.Enums.Orders.BITS.Null;
    //        var none = (ulong)bitinit;

    //        var Cancel = bitinit |= ERP.Enums.Orders.BITS.Progresses_Error_Cancel;

    //        var cancel = (ulong)Cancel;
    //        bitinit = ERP.Enums.Orders.BITS.Null;

    //        var Complate = bitinit |= ERP.Enums.Orders.BITS.Progresses_Complete;
    //        var complate = (ulong)Complate;
    //        bitinit = ERP.Enums.Orders.BITS.Null;
    //        var Unpaid = bitinit |= ERP.Enums.Orders.BITS.Progresses_Error_Unpaid;
    //        var unpaid = (ulong)Unpaid;
    //        bitinit = ERP.Enums.Orders.BITS.Null;

    //        var Unprocessed = bitinit |= ERP.Enums.Orders.BITS.Progresses_Unprocessed;
    //        var unprocessed = (ulong)Unprocessed;

    //        var b = 512 & 512;
    //        var b2 = 45 & 45;
    //        var b3 = 1024 & 1024;
    //        var b4 = 23 & 23;
    //        Console.WriteLine(n);
    //        //305-3409609-0967525
    //    }


    //    //public void GetBitsEnum()
    //    //{

    //    //    //[Description("异常订单")]
    //    //    //Progresses_Error = ONE << 3,                             //0000000000000000000000000000000000000000000000000000000000001000
    //    //    var b = ERP.Enums.Orders.BITS.Progresses_Error;
    //    //    var n = (ulong)b;

    //    //    //[Description("异常订单-取消")]
    //    //    //Progresses_Error_Cancel = Progresses_Error,                         //0000000000000000000000000000000000000000000000000000000000001000

    //    //    var b2 = ERP.Enums.Orders.BITS.Progresses_Error_Cancel;
    //    //    var n2 = (ulong)b2;

    //    //    //[Description("异常订单-Reason1")]
    //    //    //Progresses_Error_Reason1 = Progresses_Error | ONE,                     //0000000000000000000000000000000000000000000000000000000000001001

    //    //    var b3 = ERP.Enums.Orders.BITS.Progresses_Error_Reason1;
    //    //    var n3 = (ulong)b3;

    //    //    //[Description("异常订单-未付款")]
    //    //    //Progresses_Error_Unpaid = Progresses_Error | TWO,                    //0000000000000000000000000000000000000000000000000000000000001010

    //    //    var b4 = ERP.Enums.Orders.BITS.Progresses_Error_Unpaid;
    //    //    var n4 = (ulong)b4;


    //    //    //[Description("异常订单-Reason2")]
    //    //    //Progresses_Error_Program = Progresses_Error | THREE,                    //0000000000000000000000000000000000000000000000000000000000001011


    //    //    var b5 = ERP.Enums.Orders.BITS.Progresses_Error_Program;
    //    //    var n5 = (ulong)b5;


    //    //    //[Description("异常订单-Reason3")]
    //    //    //Progresses_Error_Reason3 = Progresses_Error | FOUR,                    //0000000000000000000000000000000000000000000000000000000000001100

    //    //    var b6 = ERP.Enums.Orders.BITS.Progresses_Error_Reason3;
    //    //    var n6 = (ulong)b6;

    //    //    //[Description("异常订单-Reason4")]
    //    //    //Progresses_Error_Reason4 = Progresses_Error | FIVE,                    //0000000000000000000000000000000000000000000000000000000000001101


    //    //    var b7 = ERP.Enums.Orders.BITS.Progresses_Error_Reason4;
    //    //    var n7 = (ulong)b7;

    //    //    //[Description("异常订单-Reason5")]
    //    //    //Progresses_Error_Reason5 = Progresses_Error | SIX,                    //0000000000000000000000000000000000000000000000000000000000001110


    //    //    var b8 = ERP.Enums.Orders.BITS.Progresses_Error_Reason5;
    //    //    var n8 = (ulong)b8;


    //    //    //[Description("异常订单-用户自己放入")]
    //    //    //Progresses_Error_UserAction = Progresses_Error | SEVEN,                    //0000000000000000000000000000000000000000000000000000000000001111
    //    //    //Progresses_Error_BitMask = (ONE << 4) - 1, //移位优先级<加减 1111

    //    //    var b9 = ERP.Enums.Orders.BITS.Progresses_Error_UserAction;
    //    //    var n9 = (ulong)b9;


    //    //    var b10 = ERP.Enums.Orders.BITS.Progresses_Error_BitMask;
    //    //    var n10 = (ulong)b10;


    //    //    //4-5
    //    //    //[Description("仓库发货")]
    //    //    //Progresses_Shipment = TWO << 4,                                       //0000000000000000000000000000000000000000000000000000000000100000

    //    //    var a1 = ERP.Enums.Orders.BITS.Progresses_Shipment;
    //    //    var c1 = (ulong)a1;





    //    //    [Description("仓库发货-成功")]
    //    //    Progresses_Shipment_Success = Progresses_Shipment,                  //0000000000000000000000000000000000000000000000000000000000100000

    //    //    var a2 = ERP.Enums.Orders.BITS.Progresses_Shipment_Success;
    //    //    var c2 = (ulong)a2;


    //    //    [Description("仓库发货-异常")]
    //    //    Progresses_Shipment_Error = Progresses_Shipment | ONE << 4,           //0000000000000000000000000000000000000000000000000000000000110000
    //    //    Progresses_Shipment_BitMask = Progresses_Shipment_Error,


    //    //    var a3 = ERP.Enums.Orders.BITS.Progresses_Shipment_BitMask;
    //    //    var c3 = (ulong)a3;

    //    //    //6-7
    //    //    [Description("打印面单")] Progresses_PrintLabel = TWO << 6,                              //0000000000000000000000000000000000000000000000000000000010000000

    //    //    var a4 = ERP.Enums.Orders.BITS.Progresses_Shipment_BitMask;
    //    //    var c4 = (ulong)a4;


    //    //    [Description("打印面单-缺货")] Progresses_PrintLabel_OUTOFSTOCK = Progresses_PrintLabel, //0000000000000000000000000000000000000000000000000000000011000000

    //    //    var a5 = ERP.Enums.Orders.BITS.Progresses_PrintLabel;
    //    //    var c5 = (ulong)a5;



    //    //    [Description("打印面单-有货")] Progresses_PrintLabel_INSTOCK = THREE << 6,               //0000000000000000000000000000000000000000000000000000000011000000

    //    //    var a6 = ERP.Enums.Orders.BITS.Progresses_PrintLabel_INSTOCK;
    //    //    var c6 = (ulong)a6;




    //    //    Progresses_PrintLabel_BitMask = Progresses_PrintLabel_INSTOCK,

    //    //    //8
    //    //    [Description("采购或运单申报")]
    //    //    Progresses_PurchaseORreport = ONE << 8,                               //0000000000000000000000000000000000000000000000000000000100000000

    //    //    //9
    //    //    [Description("未处理")] // 512
    //    //    Progresses_Unprocessed = ONE << 9,                                    //0000000000000000000000000000000000000000000000000000001000000000

    //    //    //10 已完成 20220615 Define 董建方[UI不展示,但是全部订单有]
    //    //    [Description("已完成")] // 1024
    //    //    Progresses_Complete = ONE << 10,                                    //0000000000000000000000000000000000000000000000000000001000000000

    //    //    [Description("订单进度位掩码")]
    //    //    Progresses_BitMask = (ONE << 20) - 1,
    //    //    //20
    //    //    //[Description("已签收")] // 2048 仅和用户操作标记相关 20220615 Define 董建方[暂不提供撤销操作,只能单向标记,不关联Progresses变更]
    //    //    //Complete新 = ONE << 20,                                   
    //    //    //21-24
    //    //    [Description("上传发票")]
    //    //    Waybill_HasUploadInvoice = ONE << 21,
    //    //    [Description("同步运单到店铺(虚拟发货)")]
    //    //    Waybill_HasSync = ONE << 22,
    //    //    [Description("打印面单")]
    //    //    Waybill_HasPrintLabel = ONE << 23,

    //    //    ShippingType_Mask = ONE << 24,
    //    //    [Description("货运方式")]
    //    //    ShippingType_FBA = ShippingType_Mask,
    //    //    ShippingType_MFN = ZERO,

    //    //}

    //    [Test]
    //    public void TestOrderBits()
    //    {

    //        //ERP.ViewModels.Order.AmazonOrder.ProgressMaskValue p = new ERP.ViewModels.Order.AmazonOrder.ProgressMaskValue(512, 512);
    //        //(ulong)a.Bits & p!.Mask)

    //        var Bits = ERP.Enums.Orders.BITS.Null;
    //        var States = ERP.Enums.Orders.States.SENT;
    //        switch (States)
    //        {
    //            case ERP.Enums.Orders.States.CANCELED:
    //                Bits |= ERP.Enums.Orders.BITS.Progresses_Error_Cancel;
    //                break;
    //            case ERP.Enums.Orders.States.UNPAID:
    //                Bits |= ERP.Enums.Orders.BITS.Progresses_Error_Unpaid;
    //                break;
    //            case ERP.Enums.Orders.States.DELIVERY:
    //            case ERP.Enums.Orders.States.PARTIALLYSHIPPED:
    //            case ERP.Enums.Orders.States.SENT:
    //                if (1 == 1)
    //                    Bits |= ERP.Enums.Orders.BITS.Progresses_Complete;
    //                else
    //                {
    //                    Bits |= ERP.Enums.Orders.BITS.Progresses_Unprocessed;
    //                }
    //                break;

    //            default:
    //                Bits |= ERP.Enums.Orders.BITS.Progresses_Unprocessed;
    //                break;
    //        }
    //        var s = Bits.ToString();
    //        var n = (ulong)Bits;
    //        Console.WriteLine(n);
    //        Console.WriteLine(s);

    //    }
    //}


}



