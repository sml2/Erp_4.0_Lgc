using System;
using System.Linq;
using ERP.Enums.Orders;

using ERP.Models.Stores;

using NUnit.Framework;

using ListParameter = ERP.Models.Api.Amazon.vm.ListParameter;

using AsinProducrService = ERP.Services.DB.Stores.AsinProduct;
using Task = System.Threading.Tasks.Task;

using System.Threading.Tasks;
using ERP.Services.Api;
using ERP;
using Model = ERP.Models.DB.Orders.Order;
using System.Collections.Generic;
using Newtonsoft.Json;
using ERP.Models.Stores;

namespace Test.Services.Api.AmazonSpApi
{
    public class OrderNew : InjectTest
    {
        private readonly ERP.Services.Stores.StoreService storeService;
        private readonly IOrderFactory _ApiOrderFactory;      
        private readonly ERP.Services.DB.Orders.Order orderService;
        private readonly AsinProducrService asinProducrService;
       
        public OrderNew()
        {
            storeService = Resolve<ERP.Services.Stores.StoreService>();
            _ApiOrderFactory = Resolve<IOrderFactory>();           
            orderService = Resolve<ERP.Services.DB.Orders.Order>();
            asinProducrService = Resolve<AsinProducrService>();
        }

        [Test]
        public async Task TestMoney()
        {
            var a = await orderService.GetOrderById(144545);

            var OrderTotal = Helpers.TransMoney(a.OrderTotal, false, a.OrderTotal.Raw.CurrencyCode, "EUR", true); // 订单总价(订单实付金额、成交价、基准货币)

            var m =a.OrderTotal.Money.To("EUR");

            Assert.IsNotNull(OrderTotal);
        }

        [Test]
        public void Create()
        {
            var s = CheckFor();
            Assert.IsEmpty(s);
        }


        [Test]
        public void Check()
        {
            List<int> ids2 = new();
            "123,45".Split(",").ForEach(x=> ids2.Add(Convert.ToInt32(x)));
            var ids = "123,45".Split(",").OfType<int>().ToList();

            Assert.IsTrue(ids.Any());
        }

        public string  CheckFor()
        {
            string s = "3,4,5,6,7,8,wer,etrt,23";
            var sArry = s.Split(",");
            for(int i=0;i<sArry.Length;i++)
            {
                if (sArry[i]=="3")
                {
                    return "-1";
                }
            }
            return "";
        }

        public async Task<StoreRegion?> GetStore(int storeid=711)
        {
            StoreRegion? store = await storeService.GetInfoById(storeid);
            if (store == null)
            {
                Assert.Fail("store is null");
            }
            var marketLst = await storeService.GetAllMarketInfoByHash(store!.UniqueHashCode);
            if (marketLst.IsNull() || marketLst!.Count <= 0)
            {
                Assert.Fail("store marketLst is null");
            }
            store.MarketPlaces = marketLst!.Select(x => x.Marketplace).ToList();
            return store;
        }

        [Test]
        public async Task List()
        {
            var store =await GetStore();            
            var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
            DateTime start = Convert.ToDateTime("2023-05-01 08:00:00");
            DateTime end = Convert.ToDateTime("2023-06-01 08:00:00");
            ListParameter listParameter = new ListParameter(store, start, end);
            var listOrder = await _ApiOrderServices.List(listParameter);
            Assert.IsNotNull(listOrder.waitOrders);
            Assert.IsEmpty(listOrder.Msg);
            var orders = listOrder.waitOrders!.OrderBy(x => x.GetAmazoNewParam().LastUpdateDate).ToList();
            foreach (var o in orders!)
            {
                var order = o.GetAmazoNewParam();
            }
        }


        
        [Test]
        public async Task GetTest()
        {
            var store = await GetStore(593);
            var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);           
            var listOrder = await _ApiOrderServices.GetTest(store, "249-2814543-4911062"); //249-2814543-4911062,249-2250208-1115023, 503-5761569-5040613
            Assert.IsNotNull(listOrder);
        }




        [Test]
        public async Task GetOrderItem()
        {
            var store = await GetStore(344);
            var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);           
            await _ApiOrderServices.GetOrderItem(store, "503-8954308-1931805", "A1VC38T7YXB528");
            
        }


        [Test]
        public async Task GetOrderAddress()
        {
            var store = await GetStore();

            var InPullOrders = orderService.GetInPullOrders(store!.ID, x => x.pullOrderState == PullOrderState.Address, 30);
            if (InPullOrders.IsNull() || InPullOrders.Count <= 0)
            {
                Assert.Fail("store have no order to pull");
            }            

            var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);           

            await _ApiOrderServices.GetOrderAddress(store, InPullOrders);
            foreach(var o in InPullOrders)
            {
                Assert.IsNotEmpty(o.Receiver.Address1);
            }
        }



        [Test]
        public async Task GetOrderBuyerInfo()
        {
            var store = await GetStore();
           
            var InPullOrders = orderService.GetInPullOrders(store!.ID, x => x.pullOrderState == PullOrderState.BuyerInfo, 30);
            if (InPullOrders.IsNull() || InPullOrders.Count <= 0)
            {
                Assert.Fail("store have no order to pull");
            }

            var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
           
            await _ApiOrderServices.GetOrderBuyerInfo(store, InPullOrders);

            foreach (var o in InPullOrders)
            {
                Assert.IsNotEmpty(o.BuyerUserName);
            }

        }




        //503-8954308-1931805
        [Test]
        public async Task GetOrderFinanciall()
        {
            var store = await GetStore(2);           
            var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);         
            await _ApiOrderServices.GetOrderFinanciall(store, "026-5300119-0969149");
        }

        [Test]
        public async Task UpdateOrder()
        {
            var store = await GetStore();
            var order =await orderService.GetOrderById(3);
            var _ApiOrderServices = _ApiOrderFactory.Create(store!.Platform);
            await _ApiOrderServices.UpdateOrder(store, order!);
        }

        [Test]
        public async Task GetOrder()
        {
            var store = await GetStore(973);          
            var _ApiOrderServices = _ApiOrderFactory.Create(store!.Platform);
          var order=  await _ApiOrderServices.GetTest(store, "112-6443459-3495438");
            Assert.IsNotNull(order);
        }

        [Test]
        public async Task ConvertCls()
        {
            try
            {
                string s = "{\"MaterialType\": ['']}";
                var obj = JsonConvert.DeserializeObject<TestCls>(s);
                Assert.IsNotNull(obj);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }            
        }

        public class TestCls
        {
           public List<string?> MaterialType { get; set; }
        }
    }
}



