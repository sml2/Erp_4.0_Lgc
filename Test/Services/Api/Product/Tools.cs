using System.Threading.Tasks;
using Common.Enums.Products;
using ERP.Services.Product;
using NUnit.Framework;

namespace Services.Api;

public class Tools : InjectTest
{
    private readonly ToolsService _service;

    public Tools()
    {
        _service = Resolve<ToolsService>();
    }
    
    [Test]
    public async Task Test()
    {
        await _service.GetUnavailableProducts("dresss", Countries.美国,false);
    }
}