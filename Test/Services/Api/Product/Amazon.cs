﻿using System;
using System.Linq;
using System.Reflection;
using ERP.Extensions;
using ERP.Services.DB.Orders;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Services.Api.Product;

//public class Amazon : InjectTest
//{
//    public Amazon()
//    {
//    }
//    [Test]
//    public void Visit()
//    {
//        var host = "";
//        var path = "";
//        var SecretKey = "";
//        var longger = Resolve<ILogger<ERP.Services.DB.Orders.Order>>();
//        MWS.Request req = new(new(),path, longger, null, "");
//        var r = MWS.Visit<string>(req);
//        Console.WriteLine(r);
//    }

//    [Test]
//    public void Method()
//    {
//        string s = "{'errors':[{'code':'QuotaExceeded','message':'You exceeded your quota for the requested resource.','details':''}]}";

//        var f = JsonConvert.DeserializeObject<AmazonErrors>(s);

//        Assert.IsNotNull(f);
//    }

//    [Test]
//    public void Attribute()
//    {
//        string js = "{'item_name':[{'value':'','language_tag':'en_CA','marketplace_id':'A2EUQ1WTGCTBG2'}],'brand':[{'value':'','language_tag':'en_CA','marketplace_id':'A2EUQ1WTGCTBG2'}],'externally_assigned_product_identifier':[{'type':'ean','value':'','marketplace_id':'A2EUQ1WTGCTBG2'}],'merchant_suggested_asin':[{'value':'','marketplace_id':'A2EUQ1WTGCTBG2'}],'supplier_declared_has_product_identifier_exemption':[{'value':false,'marketplace_id':'A2EUQ1WTGCTBG2'}],'recommended_browse_nodes':[{'value':'','marketplace_id':'A2EUQ1WTGCTBG2'}],'manufacturer':[{'value':'222','language_tag':'en_CA','marketplace_id':'A2EUQ1WTGCTBG2'}],'skip_offer':[{'value':false,'marketplace_id':'A2EUQ1WTGCTBG2'}],'fulfillment_availability':[{'fulfillment_channel_code':'AMAZON_NA','is_inventory_available':false,'lead_time_to_ship_max_days':1,'quantity':5,'restock_date':'2023-03-09'}],'map_policy':[{'value':'policy_1','marketplace_id':'A2EUQ1WTGCTBG2'}],'purchasable_offer':[{'discounted_price':[{'schedule':[{'value_with_tax':5,'start_at':'2023-03-09','end_at':'2024-04-05'}]}],'map_price':[{'schedule':[{'value_with_tax':5}]}],'start_at':{'value':'2023-03-09'},'end_at':{'value':'2023-03-09'},'currency':'CAD','marketplace_id':'A2EUQ1WTGCTBG2','our_price':[{'schedule':[{'value_with_tax':5}]}],'minimum_seller_allowed_price':[{'schedule':[{'value_with_tax':0}]}],'maximum_seller_allowed_price':[{'schedule':[{'value_with_tax':0}]}]}],'condition_type':[{'value':'club_club','marketplace_id':'A2EUQ1WTGCTBG2'}],'condition_note':[{'value':'222','language_tag':'en_CA','marketplace_id':'A2EUQ1WTGCTBG2'}],'list_price':[{'value':0,'currency':'AED','marketplace_id':'A2EUQ1WTGCTBG2'}],'product_tax_code':[{'value':'222','marketplace_id':'A2EUQ1WTGCTBG2'}],'merchant_release_date':[{'marketplace_id':'A2EUQ1WTGCTBG2','value':'2023-03-09'}],'merchant_shipping_group':[{'value':'222','marketplace_id':'A2EUQ1WTGCTBG2'}],'max_order_quantity':[{'value':1,'marketplace_id':'A2EUQ1WTGCTBG2'}],'gift_options':[{'can_be_messaged':false,'can_be_wrapped':false,'marketplace_id':'A2EUQ1WTGCTBG2'}],'main_offer_image_locator':[{'marketplace_id':'A2EUQ1WTGCTBG2','media_location':''}],'other_offer_image_locator_1':[{'marketplace_id':'A2EUQ1WTGCTBG2','media_location':''}],'other_offer_image_locator_2':[{'marketplace_id':'A2EUQ1WTGCTBG2','media_location':''}],'other_offer_image_locator_3':[{'marketplace_id':'A2EUQ1WTGCTBG2','media_location':''}],'other_offer_image_locator_4':[{'marketplace_id':'A2EUQ1WTGCTBG2','media_location':''}],'other_offer_image_locator_5':[{'marketplace_id':'A2EUQ1WTGCTBG2','media_location':''}],'product_description':[{'value':'','language_tag':'en_CA','marketplace_id':'A2EUQ1WTGCTBG2'}],'bullet_point':[{'value':'222','language_tag':'en_CA','marketplace_id':'A2EUQ1WTGCTBG2'}],'generic_keyword':[{'value':'','language_tag':'en_CA','marketplace_id':'A2EUQ1WTGCTBG2'}],'color':[{'value':'','language_tag':'en_CA','marketplace_id':'A2EUQ1WTGCTBG2'}],'part_number':[{'value':'222','marketplace_id':'A2EUQ1WTGCTBG2'}],'street_date':[{'marketplace_id':'A2EUQ1WTGCTBG2','value':'2023-03-09'}],'product_site_launch_date':[{'marketplace_id':'A2EUQ1WTGCTBG2','value':'2023-03-09'}],'manufacturer_contact_information':[{'value':'222','language_tag':'en_CA','marketplace_id':'A2EUQ1WTGCTBG2'}],'parentage_level':[{'value':'child','marketplace_id':'A2EUQ1WTGCTBG2'}],'child_parent_sku_relationship':[{'child_relationship_type':'variation','marketplace_id':'A2EUQ1WTGCTBG2','parent_sku':''}],'variation_theme':[{'name':'COLOR'}],'safety_warning':[{'value':'222','language_tag':'en_CA','marketplace_id':'A2EUQ1WTGCTBG2'}],'country_of_origin':[{'value':'AF','marketplace_id':'A2EUQ1WTGCTBG2'}],'warranty_description':[{'value':'222','language_tag':'en_CA','marketplace_id':'A2EUQ1WTGCTBG2'}],'batteries_required':[{'value':false,'marketplace_id':'A2EUQ1WTGCTBG2'}],'batteries_included':[{'value':false,'marketplace_id':'A2EUQ1WTGCTBG2'}],'battery':[{'cell_composition':[{'value':'alkaline'}],'marketplace_id':'A2EUQ1WTGCTBG2','weight':[{'value':0,'unit':'grams'}]}],'num_batteries':[{'marketplace_id':'A2EUQ1WTGCTBG2','quantity':0,'type':'12v'}],'number_of_lithium_metal_cells':[{'value':0,'marketplace_id':'A2EUQ1WTGCTBG2'}],'number_of_lithium_ion_cells':[{'value':0,'marketplace_id':'A2EUQ1WTGCTBG2'}],'lithium_battery':[{'energy_content':[{'value':0,'unit':'btus'}],'marketplace_id':'A2EUQ1WTGCTBG2','packaging':[{'value':'batteries_contained_in_equipment'}],'weight':[{'value':0,'unit':'grams'}]}],'supplier_declared_dg_hz_regulation':[{'value':'ghs','marketplace_id':'A2EUQ1WTGCTBG2'}],'contains_liquid_contents':[{'value':false,'marketplace_id':'A2EUQ1WTGCTBG2'}],'hazmat':[{'aspect':'united_nations_regulatory_id','value':'','marketplace_id':'A2EUQ1WTGCTBG2'}],'safety_data_sheet_url':[{'value':'','language_tag':'en_CA','marketplace_id':'A2EUQ1WTGCTBG2'}],'item_weight':[{'value':0,'unit':'grams','marketplace_id':'A2EUQ1WTGCTBG2'}],'ghs':[{'classification':[{'class':'amzn_specific_no_label_with_warning'}],'marketplace_id':'A2EUQ1WTGCTBG2'}],'liquid_volume':[{'marketplace_id':'A2EUQ1WTGCTBG2','unit':'centiliters','value':0}],'main_product_image_locator':[{'marketplace_id':'A2EUQ1WTGCTBG2','media_location':''}],'other_product_image_locator_1':[{'marketplace_id':'A2EUQ1WTGCTBG2','media_location':''}],'other_product_image_locator_2':[{'marketplace_id':'A2EUQ1WTGCTBG2','media_location':''}],'other_product_image_locator_3':[{'marketplace_id':'A2EUQ1WTGCTBG2','media_location':''}],'other_product_image_locator_4':[{'marketplace_id':'A2EUQ1WTGCTBG2','media_location':''}],'other_product_image_locator_5':[{'marketplace_id':'A2EUQ1WTGCTBG2','media_location':''}],'other_product_image_locator_6':[{'marketplace_id':'A2EUQ1WTGCTBG2','media_location':''}],'other_product_image_locator_7':[{'marketplace_id':'A2EUQ1WTGCTBG2','media_location':''}],'other_product_image_locator_8':[{'marketplace_id':'A2EUQ1WTGCTBG2','media_location':''}],'swatch_product_image_locator':[{'marketplace_id':'A2EUQ1WTGCTBG2','media_location':''}],'item_dimensions':[{'length':{'value':0,'unit':'angstrom'},'width':{'value':0,'unit':'angstrom'},'height':{'value':0,'unit':'angstrom'},'marketplace_id':'A2EUQ1WTGCTBG2'}],'item_package_dimensions':[{'length':{'value':0,'unit':'angstrom'},'width':{'value':0,'unit':'angstrom'},'height':{'value':0,'unit':'angstrom'},'marketplace_id':'A2EUQ1WTGCTBG2'}],'item_package_weight':[{'value':1,'unit':'grams','marketplace_id':'A2EUQ1WTGCTBG2'}]}";
//        var attributeObj = JsonConvert.DeserializeObject<JObject>(js);

//        Type instence = attributeObj.GetType();


//        if (attributeObj.GetValue("item_name").HasValues)
//        {
//            var v = (string)attributeObj["item_name"][0]["value"];
//            v = "444";
//            attributeObj["item_name"][0]["value"] = v;
//            var rating_comment = (string)attributeObj["item_name"][0]["marketplace_id"];
//            var rating = (string)attributeObj["item_name"][0]["language_tag"];          
//        }
//        else
//        {
//            Console.WriteLine("No feedback rating found");
//        }


//        //foreach (var item in attributeObj.Children())
//        //{
//        //    if (item.Path == "item_name")
//        //    {
//        //        var w = item.Values().First().First();

//        //      w.First().Value<string>("444");

//        //        Console.WriteLine(item);
//        //    }
//        //}



//    }

//}

