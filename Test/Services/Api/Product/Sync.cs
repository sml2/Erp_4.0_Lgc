
//using System;
//using System.Threading.Tasks;
//using ERP.Interface;
//using ERP.Models.Product;
//using ERP.Services.DB.ERP3;
//using Microsoft.EntityFrameworkCore;
//using Microsoft.Extensions.DependencyInjection;
//using Moq;
//using NUnit.Framework;

//namespace Test.Services.Api.Product;

//public class Sync : InjectTest
//{
//    [Test]
//    public async Task Test()
//    {
//        try
//        {
//            var dbContext3 = Resolve<ERP.Data.DBContext3>();
//            var dbContext = Resolve<ERP.Data.DBContext>();
//            var mockOemProvider = new Mock<IOemProvider>();

//            var erp3Product = await dbContext3.erp3_products.FirstAsync(m => m.id == 16113702);
//            var user = await dbContext.User.FirstAsync(m => m.UserName == "lyq");
//            var oem = await dbContext.OEM.FirstAsync(m => m.ID == 7);
//            var task = await dbContext.ProductMigrationTasks.FirstAsync(m => m.ID == 9);
            
//            mockOemProvider.Setup(o => o.OEM).Returns(oem);
//            var productSyncService = ActivatorUtilities.CreateInstance<ProductSyncService>(sp, mockOemProvider.Object);

//            await productSyncService.Handle(erp3Product, task, user);
//        }
//        catch (Exception e)
//        {
//            Console.WriteLine(e);
//            throw;
//        }
//    }
//}