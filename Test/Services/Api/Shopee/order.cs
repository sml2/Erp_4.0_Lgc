using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.Models.Api;
using ERP.Services.Api;
using NUnit.Framework;

using ERP.Models.Stores;
using static ERP.Models.Stores.StoreRegion;

namespace Test.Services.Api.Shopee
{
    //public class order:InjectTest
    //{
    //    private ERP.Services.Api.ShopeeClient client ;
    //    private ShopeeOption option;

    //    private ERP.Services.Stores.StoreService  storeService;

    //    [SetUp]
    //    public void Setup()
    //    {          
    //        option = new ShopeeOption();
    //        client = Resolve<ERP.Services.Api.ShopeeClient>();
    //        storeService = Resolve<ERP.Services.Stores.StoreService>();
    //    }


    //    protected  ShopeeData? shopeeDataModel { get; set; }

    //    public  ShopeeData GetShopeeToken()
    //    {
    //        if(shopeeDataModel==null || shopeeDataModel.AccessToken.IsNullOrWhiteSpace())
    //        {
    //            int id =3;
    //            StoreRegion store =storeService.GetInfoById(id).Result;
    //            var d = store.GetShopeeData()!;
    //            shopeeDataModel = d;
    //        }
    //        return shopeeDataModel;
    //    }



    //    //[TestCase("6c474162537771767a4259586671686f", 33859)]//main_account_id=11164
    //    //public async Task GetOrderDetail(string accesstoken, int shopID)
    //    //{
    //    //    RequestGetOrderDetail request = new RequestGetOrderDetail()
    //    //    {
    //    //       OrderSnList=new string[] { "201214JASXYXY6" },
    //    //       ShopId = shopID,
    //    //       //shopeedata = new     
    //    //       // AccessToken=new  accesstoken,
    //    //    };
    //    //    //调用方法
    //    //    var result =await  client.SendRequestResultAsync<ResponseGetOrderDetail>(request);
    //    //    string msg = result.Message;


    //    //    Assert.IsNotNull(result);
    //    //    Assert.IsTrue(result.Success);
    //    //}

    //    public async Task< ShopeeResponseBase<ResponseUploadImage>> UploadImage<ResponseUploadImage>(RequestUploadImage requestUploadImage)
    //    {                          
    //            var response = await client.SendFormDataRequestAsync<ResponseUploadImage>(requestUploadImage);
    //            return response;          
    //    }

    //    [Test]
    //    public async Task AddItem()
    //    {

    //        GetShopeeToken();

    //        //List<string> images = new List<string>(){
    //        //    "https://ssproxy2.ucloudbiz.olleh.com/v1/AUTH_8556e13c15264137a6a9e78811845b46/AwesomeSwagReal/9976b7cc-125f-454a-b08a-673848e1b08e/d5358081-4285-4363-b471-3bd084ddce63/Original/Thumb/%EB%B2%A0%EB%A3%A8%EC%8A%A4+%EA%B0%A4%EB%9F%AD%EC%8B%9C+S21+%ED%94%8C%EB%9F%AC%EC%8A%A4+%EC%9A%B8%ED%8A%B8%EB%9D%BC+%EB%85%B8%ED%8A%B820+S20+%EB%85%B8%ED%8A%B810+A52+A72+%EB%B2%84%EC%A6%88+%EC%BC%80%EC%9D%B4%EC%8A%A4+%ED%88%AC%EB%AA%85%2c%EB%B2%94%ED%8D%BC%2c%EC%B9%B4%EB%93%9C%EC%88%98%EB%82%A9%2c%EA%B1%B0%EC%B9%98%EB%8C%80_Thumb_1.jpg",
    //        //    "https://ssproxy2.ucloudbiz.olleh.com/v1/AUTH_8556e13c15264137a6a9e78811845b46/AwesomeSwagReal/9976b7cc-125f-454a-b08a-673848e1b08e/d5358081-4285-4363-b471-3bd084ddce63/Original/Thumb/%EB%B2%A0%EB%A3%A8%EC%8A%A4+%EA%B0%A4%EB%9F%AD%EC%8B%9C+S21+%ED%94%8C%EB%9F%AC%EC%8A%A4+%EC%9A%B8%ED%8A%B8%EB%9D%BC+%EB%85%B8%ED%8A%B820+S20+%EB%85%B8%ED%8A%B810+A52+A72+%EB%B2%84%EC%A6%88+%EC%BC%80%EC%9D%B4%EC%8A%A4+%ED%88%AC%EB%AA%85%2c%EB%B2%94%ED%8D%BC%2c%EC%B9%B4%EB%93%9C%EC%88%98%EB%82%A9%2c%EA%B1%B0%EC%B9%98%EB%8C%80_Thumb_3.jpg",
    //        //    "https://ssproxy2.ucloudbiz.olleh.com/v1/AUTH_8556e13c15264137a6a9e78811845b46/AwesomeSwagReal/9976b7cc-125f-454a-b08a-673848e1b08e/d5358081-4285-4363-b471-3bd084ddce63/Original/Thumb/%EB%B2%A0%EB%A3%A8%EC%8A%A4+%EA%B0%A4%EB%9F%AD%EC%8B%9C+S21+%ED%94%8C%EB%9F%AC%EC%8A%A4+%EC%9A%B8%ED%8A%B8%EB%9D%BC+%EB%85%B8%ED%8A%B820+S20+%EB%85%B8%ED%8A%B810+A52+A72+%EB%B2%84%EC%A6%88+%EC%BC%80%EC%9D%B4%EC%8A%A4+%ED%88%AC%EB%AA%85%2c%EB%B2%94%ED%8D%BC%2c%EC%B9%B4%EB%93%9C%EC%88%98%EB%82%A9%2c%EA%B1%B0%EC%B9%98%EB%8C%80_Thumb_2.jpg",
    //        //    "https://ssproxy2.ucloudbiz.olleh.com/v1/AUTH_8556e13c15264137a6a9e78811845b46/AwesomeSwagReal/9976b7cc-125f-454a-b08a-673848e1b08e/d5358081-4285-4363-b471-3bd084ddce63/Original/Thumb/%EB%B2%A0%EB%A3%A8%EC%8A%A4+%EA%B0%A4%EB%9F%AD%EC%8B%9C+S21+%ED%94%8C%EB%9F%AC%EC%8A%A4+%EC%9A%B8%ED%8A%B8%EB%9D%BC+%EB%85%B8%ED%8A%B820+S20+%EB%85%B8%ED%8A%B810+A52+A72+%EB%B2%84%EC%A6%88+%EC%BC%80%EC%9D%B4%EC%8A%A4+%ED%88%AC%EB%AA%85%2c%EB%B2%94%ED%8D%BC%2c%EC%B9%B4%EB%93%9C%EC%88%98%EB%82%A9%2c%EA%B1%B0%EC%B9%98%EB%8C%80_Thumb_4.jpg"
    //        //};

    //        //var upload_tasks = images.Select(e => UploadImage<ResponseUploadImage>(new RequestUploadImage()
    //        //{
    //        //    Image = e,
    //        //    shopeedata= shopeeDataModel,
    //        //}));
    //        //var image_upload_res= await Task.WhenAll(upload_tasks);
    //        ////分类
    //        //var res_category =await GetCategory();
    //        //var target_category = res_category.Where(e => !e.HasChildren).FirstOrDefault();

    //        ////品牌      
    //        //var res_brand =await GetBrandList(target_category.CategoryId);

    //        ////dts
    //        //var res_dts = await GetDtsLimit(target_category.CategoryId);

    //        ////attribute
    //        //var res_attr = await GetAttributes(target_category.CategoryId);

    //        ////Channel api获取所有受支持的物流通道           
    //        //var res_channel = GetChannelList();

    //        ////获取类别支持大小图表
    //        //var res_size_chart = await SupportSizeChart(target_category.CategoryId);


    //        //if (
    //        //    image_upload_res.Where(e => e.Response.ImageInfo.IsNotNull()).Any() &&
    //        //    res_category.IsNotNull() &&
    //        //    res_brand.IsNotNull() &&
    //        //    res_dts.IsNotNull() &&
    //        //    res_attr.IsNotNull() &&
    //        //    res_channel.IsNotNull() &&
    //        //    res_size_chart.IsNotNull()
    //        //    )
    //        //{
    //        //    RequestAddItem shopee_req = new RequestAddItem()
    //        //    {
    //        //        shopeedata = shopeeDataModel,
    //        //        ShopId = shopeeDataModel.ShopID,
    //        //        OriginalPrice = 20,
    //        //        Description = "Test Description method testTest Description method testTest Description method test",
    //        //        Weight = 5,
    //        //        ItemName = "Test Item Name",
    //        //        ItemSku="sku111",
    //        //         Condition= "NEW",
    //        //         Wholesale=new RequestAddItem._Wholesale()
    //        //         {
    //        //             MaxCount=100,
    //        //             MinCount=1,
    //        //             UnitPrice=28.2,
    //        //         },
    //        //        ItemStatus = EShopeeItemStatus.NORMAL,
    //        //        Dimension = new RequestAddItem._Dimension()
    //        //        {
    //        //            PackageWidth = 10,
    //        //            PackageHeight = 10,
    //        //            PackageLength = 10
    //        //        },
    //        //        NormalStock = 200,
    //        //        LogisticInfo = new List<RequestAddItem._LogisticInfo>(){
    //        //        new RequestAddItem._LogisticInfo(){
    //        //            Enabled = true,
    //        //            LogisticId = res_channel.Result.LogisticsChannelList.FirstOrDefault().LogisticsChannelId
    //        //        }
    //        //    },
    //        //        AttributeList = res_attr.AttributeList// .Where(e => e.IsMandatory)
    //        //        .Select(e => new RequestAddItem._Attribute()
    //        //        {
    //        //            AttributeId = e.AttributeId,
    //        //            AttributeValueList = e.AttributeValueList.Select(r => new RequestAddItem._AttributeValue()
    //        //            {
    //        //                ValueId = r.ValueId
    //        //            }).ToList()
    //        //        }).ToList(),
    //        //        CategoryId = target_category.CategoryId,
    //        //        Image = new RequestAddItem._Image()
    //        //        {
    //        //            ImageIdList = image_upload_res.Where(e => e.Response.ImageInfo.IsNotNull()).Select(r => r.Response.ImageInfo.ImageId)
    //        //        },
    //        //        PreOrder = new RequestAddItem._PreOrder()
    //        //        {
    //        //            IsPreOrder = false,
    //        //        },
    //        //        Brand = new RequestAddItem._Brand()
    //        //        {
    //        //            BrandId = res_brand.BrandList.FirstOrDefault().BrandId,
    //        //            OriginalBrandName = res_brand.BrandList.FirstOrDefault().OriginalBrandName,
    //        //        }
    //        //    };

    //        RequestAddItem shopee_req = new RequestAddItem()
    //        {
    //            shopeedata = shopeeDataModel,
    //            ShopId = shopeeDataModel.ShopID,
    //        };

    //        string json = @"{'original_price':123,'description':'itemtest','weight':1.1,'item_name':'Item Name Example','item_status':'UNLIST','dimension':{'package_height':11,'package_length':11,'package_width':11},'normal_stock':33,'logistic_info':[{'size_id':0,'shipping_fee':23.12,'enabled':true,'logistic_id':80101,'is_free':false}],'attribute_list':[{'attribute_id':4990,'attribute_value_list':[{'value_id':32142,'original_value_name':'Brand','value_unit':' kg'}]}],'category_id':14695,'image':{'image_id_list':[]},'pre_order':{'is_pre_order':false,'days_to_ship':3},'item_sku':'','condition':'NEW','wholesale':[{'min_count':1,'max_count':100,'unit_price':28.3}],'brand':{'brand_id':0},'tax_info':{'ncm':'','same_state_cfop':'','diff_state_cfop':'','csosn':'','origin':'','cest':'','measure_unit':'','invoice_option':'','vat_rate':'','hs_code':'','tax_code':''},'complaint_policy':{'warranty_time':'','exclude_entrepreneur_warranty':'','complaint_address_id':0,'additional_information':''},'description_info':{'extended_description':{'field_list':[{'field_type':'','text':'','image_info':{'image_id':''}}]}},'description_type':'normal ','seller_stock':[{'location_id':'','stock':0}]}";
    //        var shopee_res = await AddItem(shopee_req, json);
    //        Assert.IsNotNull(shopee_res);
    //    }

    //    public async Task<ShopeeResponseBase<ResponseAddItem>> AddItem(RequestAddItem requestAddItem,string json)
    //    {            
    //        var response = await client.TestSendRequestAsync<ResponseAddItem>(requestAddItem, json);
    //        return response;
    //    }     

    //    public async Task<IEnumerable<_Category>> GetCategory()
    //    {
    //        RequestGetCategory request = new RequestGetCategory();
    //        request.ShopId= shopeeDataModel.ShopID;
    //        request.shopeedata = shopeeDataModel;
    //        var response =await  client.SendRequestAsync<ResponseGetCategory>(request);
    //        if (response.IsNotNull() && response.Response.IsNotNull() && response.Response.CategoryList.IsNotNull())
    //        {
    //            return response.Response.CategoryList;
    //        }
    //        else
    //        {
    //            return null;
    //        }
    //    }


    //    public async Task<ResponseGetBrandList> GetBrandList(long? CategoryId )
    //    {
    //        RequestGetBrandList request = new RequestGetBrandList();
    //        request.ShopId= shopeeDataModel.ShopID;
    //        request.CategoryId= CategoryId;
    //        request.shopeedata = shopeeDataModel;
    //        var response = await client.SendRequestAsync<ResponseGetBrandList>(request);
    //        return response.Response;
    //    }


    //    public async Task<ResponseGetDTSLimit> GetDtsLimit(long? CategoryId )
    //    {
    //        RequestGetDTSLimit request=new RequestGetDTSLimit();
    //        request.ShopId= shopeeDataModel.ShopID;
    //        request.CategoryId = CategoryId;
    //        request.shopeedata = shopeeDataModel;
    //        var response = await client.SendRequestAsync<ResponseGetDTSLimit>(request);
    //        return response.Response;
    //    }

    //    public async Task<ResponseGetAttributes> GetAttributes(long? CategoryId)
    //    {
    //        RequestGetAttributes request = new RequestGetAttributes();
    //        request.ShopId = shopeeDataModel.ShopID;
    //        request.CategoryId = CategoryId;
    //        request.shopeedata = shopeeDataModel;
    //        var response = await client.SendRequestAsync<ResponseGetAttributes>(request);
    //        return response.Response;
    //    }

    //    public async Task<ResponseGetChannelList?> GetChannelList()
    //    {
    //        RequestGetChannelList request = new RequestGetChannelList();
    //        request.ShopId = shopeeDataModel.ShopID;
    //        request.shopeedata = shopeeDataModel;
    //        var response = await client.SendRequestAsync<ResponseGetChannelList>(request);
    //        return response.Response;

    //    }

    //    public async Task<ResponseSupportSizeChart> SupportSizeChart(long? CategoryId)
    //    {
    //        RequestSupportSizeChart request = new RequestSupportSizeChart();
    //        request.ShopId = shopeeDataModel.ShopID;
    //        request.CategoryId = CategoryId;
    //        request.shopeedata = shopeeDataModel;
    //        var response = await client.SendRequestAsync<ResponseSupportSizeChart>(request);
    //        return response.Response;
    //    }

    //}


}
