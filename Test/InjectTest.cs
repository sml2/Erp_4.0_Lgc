﻿using ERP.Data;
using ERP.Extensions;
using ERP.Services.Product;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestPlatform.ObjectModel;
using Ryu.Data.Abstraction;
using Ryu.Data;

public abstract class InjectTest
{
    protected ServiceProvider sp;
    private class HostingEnvironment : IHostEnvironment
    {
        public string EnvironmentName { get; set; }

        public string ApplicationName { get; set; }

        public string ContentRootPath { get; set; }

        public IFileProvider ContentRootFileProvider { get; set; }
    }
    public InjectTest()
    {
        var serviceCollection = new ServiceCollection();
        var jsonPath = "appsettings.Development.json";
        if (IsSunYongInHome)
            jsonPath = "appsettings.Development.SunYongHome.json";
        if (IsSunYongInCompany)
            jsonPath = "appsettings.Development.SunYongCompany.json";
        if (IsZhuYingyingInCompany)
            jsonPath = "appsettings.Development.ZhuYY.json";

        var configurationBuilder = new ConfigurationBuilder().AddJsonFile(jsonPath);
        serviceCollection.AddSingleton<IHostEnvironment, HostingEnvironment>();
        var configuration = configurationBuilder.Build();
       
        serviceCollection.AddLogging(builder =>
        {
            builder.AddConsole();
        });
       
        // if (IsZhuYingyingInCompany)
        // {
            var connectionStr = configuration.GetConnectionString(configuration["ConnectionStrings:database"]);           
            serviceCollection.AddDbContext<DBContext>(config => config.UseMySql(connectionStr, ServerVersion.AutoDetect(connectionStr)).EnableSensitiveDataLogging().EnableDetailedErrors());
        // }
        // else {
        //     configuration["ConnectionStrings:database"] = "test";
        //     serviceCollection.AddDbContext<DBContext>(config => config.UseInMemoryDatabase("test"));
        // }       
        serviceCollection.AddServices(configuration);
        sp = serviceCollection.BuildServiceProvider();
        MoneyFactory.Instance.SetRateProvider(sp.GetRequiredService<IRateProvider>());
    }
    private bool IsSunYongInHome => System.IO.Directory.Exists("E:\\GIT\\");
    private bool IsSunYongInCompany => System.IO.Directory.Exists("C:\\Users\\Administrator\\Desktop\\ExeLink");
    private bool IsZhuYingyingInCompany => System.IO.Directory.Exists("E:\\ZYY");
    protected T Resolve<T>() where T : class
    {
        return sp.GetRequiredService<T>();
    }
    protected T CreateInstance<T>() where T : class
    {
        return ActivatorUtilities.CreateInstance<T>(sp);
    }
    protected void InitMoneyRateProvider()=> MoneyFactory.Instance.SetRateProvider(Resolve<IRateProvider>());
}
