﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using ERP.Data;
using ERP.Data.Products;
using ERP.Enums;
using ERP.Services.DB.Orders;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NUnit.Framework;
using Ryu.Data;
using Ryu.Data.Abstraction;
using Text;

namespace Data;

public class Product : InjectTest
{

    private ERP.Data.Products.Product GetProduct() {
        var Title = "RV Ceiling Light 7\" Touch on/off Switch Surface Mount Warm White Leisure LED 4PK";
        var Source = "https://www.ebay.com/itm/184609325209";
        var Quantity = 50;
        var Platform = Platforms.EBay;
        var Carrie = "USD";
        var Price = 67159.167226;
        ERP.Data.Products.Product Product = new();
        Product.Title = Title;
        Product.Source = Source;
        Product.Quantity = Quantity;
        Product.Platform = Platform;
        Product.Images.Add(new(234488892, "https://i.ebayimg.com/images/g/VyQAAOSw9BBf4TcT/s-l1600.jpg"));
        Product.Images.Add(234488893, "https://i.ebayimg.com/images/g/6SwAAOSwN21f4REU/s-l1600.jpg");

        //InitMoneyRateProvider();
        //Product.SetPrice(Carrie, Price);
        Product.Variants.Attributes.Add("Size", new[] { "XL", "L", "M", "S" });
        Product.Variants.Attributes.Add("Color", new[] { "Red", "Green", "Blue" });
        Product.Variants.Add(new Variant() { Title = "TTTT XL" });
        Product.Variants.Add(new Variant() { Title = "TTTT L" });
        Product.Variants.Add(new Variant() { Title = "TTTT M" });
        Product.Variants.Add(new Variant() { Title = "TTTT S" });
        Product.Language.Package.Add(Languages.ZH, new() { { "Size", "型号" }, { "L", "大号" }, { "M", "中号" }, { "S", "小号" } });
        return Product;
    }
    [Test]
    public void Serialize()
    {
        var Product = GetProduct();
        Debug.Assert(Product is not null);
        var json = JsonConvert.SerializeObject(Product);
        Debug.Print(json);
    }

    [Test]
    public void Deserialize()
    {
        var json = "{\"Language\":{\"Bits\":0,\"Package\":{\"ZH\":{\"Size\":\"型号\",\"L\":\"大号\",\"M\":\"中号\",\"S\":\"小号\"}}},\"Variants\":[{\"Properties\":{\"Title\":{\"Key\":\"Title\",\"Value\":\"TTTT XL\"}},\"PropertiesForLang\":{},\"Title\":\"TTTT XL\"},{\"Properties\":{\"Title\":{\"Key\":\"Title\",\"Value\":\"TTTT L\"}},\"PropertiesForLang\":{},\"Title\":\"TTTT L\"},{\"Properties\":{\"Title\":{\"Key\":\"Title\",\"Value\":\"TTTT M\"}},\"PropertiesForLang\":{},\"Title\":\"TTTT M\"},{\"Properties\":{\"Title\":{\"Key\":\"Title\",\"Value\":\"TTTT S\"}},\"PropertiesForLang\":{},\"Title\":\"TTTT S\"}],\"Images\":[{\"ID\":234488892,\"Url\":\"https://i.ebayimg.com/images/g/VyQAAOSw9BBf4TcT/s-l1600.jpg\",\"Size\":-1,\"Path\":null,\"Time\":\"0001-01-01T00:00:00\"},{\"ID\":234488893,\"Url\":\"https://i.ebayimg.com/images/g/6SwAAOSwN21f4REU/s-l1600.jpg\",\"Size\":-1,\"Path\":null,\"Time\":\"0001-01-01T00:00:00\"}],\"Properties\":{\"Title\":{\"Key\":\"Title\",\"Value\":\"RV Ceiling Light 7\\\" Touch on/off Switch Surface Mount Warm White Leisure LED 4PK\"},\"Source\":{\"Key\":\"Source\",\"Value\":\"https://www.ebay.com/itm/184609325209\"},\"Quantity\":{\"Key\":\"Quantity\",\"Value\":50},\"Platform\":{\"Key\":\"Platform\",\"Value\":7}},\"Title\":\"RV Ceiling Light 7\\\" Touch on/off Switch Surface Mount Warm White Leisure LED 4PK\",\"Source\":\"https://www.ebay.com/itm/184609325209\",\"Quantity\":50,\"Platform\":7,\"MainImage\":{\"ID\":234488892,\"Url\":\"https://i.ebayimg.com/images/g/VyQAAOSw9BBf4TcT/s-l1600.jpg\",\"Size\":-1,\"Path\":null,\"Time\":\"0001-01-01T00:00:00\"},\"Carrie\":null,\"Price\":null}";
        var Product = JsonConvert.DeserializeObject<ERP.Data.Products.Product>(json);
        Debug.Assert(Product is not null);
        Debug.WriteLineIf(Product.Images is not null, Product.Images.Count);
    }
    [Test]
    public void SerializeAndDeserialize()
    {
        var Product = GetProduct();
        Debug.Assert(Product is not null);
        var JsonSerializerSettings = new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.All };
        var json = JsonConvert.SerializeObject(Product,Formatting.Indented, JsonSerializerSettings);
        Debug.Print(json);
        var ProductDe = JsonConvert.DeserializeObject<ERP.Data.Products.Product>(json, JsonSerializerSettings);
        Debug.Assert(ProductDe is not null);
      //Debug.Assert(Product.Variants.Attributes.Count == ProductDe.Variants.Attributes.Count );
        Debug.WriteLineIf(ProductDe.Images is not null, ProductDe.Images?.Count);
    }

}