﻿using System;
using System.Diagnostics;
using System.Linq;
using ERP.Data;

using ERP.Services.DB.Orders;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NUnit.Framework;
using Ryu.Data;
using Ryu.Data.Abstraction;
using Text;

namespace Data;

public class Bits : InjectTest
{
    [Test]
    public void Test()
    {
        const int length = 2 << 29;
        Stopwatch stopwatch = Stopwatch.StartNew();
        //预热CPU
        stopwatch.Start();
        for (int i = 0; i < length; i++)
        {
            var ii = i;
        }
        stopwatch.Stop();
        Console.WriteLine($"Nop:{stopwatch.ElapsedMilliseconds},{stopwatch.ElapsedTicks}");
        stopwatch.Restart();
        for (int i = 0; i < length; i++)
        {
            var ii = i | length;
        }
        stopwatch.Stop();
        Console.WriteLine($"|:{stopwatch.ElapsedMilliseconds},{stopwatch.ElapsedTicks}");
        stopwatch.Restart();
        for (int i = 0; i < length; i++)
        {
            var ii = i ^ length;
        }
        stopwatch.Stop();
        Console.WriteLine($"^:{stopwatch.ElapsedMilliseconds},{stopwatch.ElapsedTicks}");
    }
  
}

