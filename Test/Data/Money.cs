﻿using System;
using System.Diagnostics;
using System.Linq;
using ERP.Data;

using Newtonsoft.Json;
using NUnit.Framework;
using Ryu.Data;
using Ryu.Data.Abstraction;
using Text;

namespace Data;

public class MoneyTest : InjectTest
{
    [Test]
    public void Test()
    {
        var Amount1 = 1;
        var Amount2 = 0.456;
        var Amount3 = "1";
        var Amount4 = "0.618";
        var Amount5 = "x";
        InitMoneyRateProvider();
        Check(new MoneyMeta("CNY", Amount1), m => m != 0m);
        Check(new MoneyMeta("CNY", Amount2), m => m != 0m);
        Check(new MoneyMeta("CNY", Amount3), m => m != 0m);
        Check(new MoneyMeta("CNY", Amount4), m => m != 0m);
        Check(new MoneyMeta("CNY", Amount5), m => m == 0m);
        Check(new MoneyMeta("QQQ", Amount1), m => m == 0m);
        Check(new MoneyMeta("QQQ", Amount2), m => m == 0m);
        Check(new MoneyMeta("QQQ", Amount3), m => m == 0m);
        Check(new MoneyMeta("QQQ", Amount4), m => m == 0m);
        Check(new MoneyMeta("QQQ", Amount5), m => m == 0m);
    }
    public void Check(MoneyMeta moneyMeta, Func<Money, bool> func)
    {
        var R = moneyMeta.ToRecode();
        Debug.Print(JsonConvert.SerializeObject(R));
        Debug.Assert(func(R.Money));
    }
}

