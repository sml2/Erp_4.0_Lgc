﻿using System;
using System.Diagnostics;
using System.Net;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using NUnit.Framework;
using Http = Sy.Net.HTTP;
namespace Test;
public class Proxy
{
    private Http Factory(string url) => Sy.Net.Factory.Http(url).SetExceptionHandle(e => ExpMsg = ERP.Helpers.ExpHandle(e));
    Http HttpProxy(string Proxy, string u) => Factory(Proxy).SetHeader("ProxyUrl", u);
    Http TinyProxy(WebProxy Proxy, string u) => Factory(u).SetProxy(Proxy);

    Http P10(string u) => Factory(u);
    Http P21(string u) => HttpProxy("http://proxyhw.miwaimao.com/", u);
    Http P22(string u) => TinyProxy(new("proxy.miwaimao.com", 443), u);//
    Http P23(string u) => HttpProxy("http://proxy.miwaimao.com", u);
    Http P31(string u) => HttpProxy("http://httpproxyhw.miwaimao.com/", u);
    Http P32(string u) => TinyProxy(new("hwtinyproxy.miwaimao.com", 80), u);
    Http P33(string u) => HttpProxy("http://hwhttpproxy.miwaimao.com/", u);
    Http P41(string u) => HttpProxy("http://hwproxy.miwaimao.com/", u);
    Http P42(string u) => TinyProxy(new("altinyproxy.miwaimao.com", 80), u);
    Http P43(string u) => HttpProxy("http://httpproxy.miwaimao.com/", u);

    string ExpMsg = string.Empty;


    Stopwatch stopwatch = new();
    [Test]
    public async Task TestAll()
    {
        const string Url_Sml2 = "https://ip.sml2.com/";
        //const string Url_Sml2 = "https://api.sml2.com/";
        const string Mws = "https:mws.";

        Stopwatch stopwatch = new();
        foreach (var ProxyInfo in new System.Collections.Generic.Dictionary<string, Func<string, Http>> {
                                       { nameof(P10), P10 },
                                       { nameof(P21), P21 },{ nameof(P22), P22 },{ nameof(P23), P23 },
                                       { nameof(P31), P31 },{ nameof(P32), P32 },{ nameof(P33), P33 },
                                       { nameof(P41), P41 },{ nameof(P42), P42 },{ nameof(P43), P43 }})
        {
            var Name = ProxyInfo.Key;
            var Proxy = ProxyInfo.Value;

            stopwatch.Restart();
            var html = Proxy(Url_Sml2).FastTry();
            stopwatch.Stop();
            Console.WriteLine($"{Name} Time:{stopwatch.ElapsedMilliseconds}ms IP:{html}");
        }
        await Task.CompletedTask;
    }
    [Test]
    public async Task TestP10()
    {
        const string Url_Sml2 = "https://ip.sml2.com/";
        stopwatch.Restart();
        var html = P10(Url_Sml2).FastTry();
        stopwatch.Stop();
        Console.WriteLine($"{nameof(P10)} Time:{stopwatch.ElapsedMilliseconds}ms IP:{html}");
        await Task.CompletedTask;
    }

}
