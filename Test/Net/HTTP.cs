﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using NUnit.Framework;

namespace Test;
public class HTTP
{
    static ILoggerFactory loggerFactory = NullLoggerFactory.Instance;
    public static ILoggerFactory LoggerFactory { get => loggerFactory; }

    readonly ILogger<HTTP> logger = LoggerFactory.CreateLogger<HTTP>();

    [Test]
    public async Task Get()
    {
        var Url = "https://api.sml2.com/";
        var page = Sy.Net.Factory.Http(Url).Get();
        Assert.IsNotNull(page);
        Console.WriteLine(page);
        await Task.CompletedTask;
    }
    [Test]
    public async Task Json()
    {
        var Url = "https://api.sml2.com/";
        var json = "{'x':6}";
        var page = Sy.Net.Factory.Http(Url).Post(json);
        Assert.IsNotNull(page);
        Console.WriteLine(page);
        await Task.CompletedTask;
    }
    [Test]
    public async Task URLEncode()
    {
        var Url = "https://api.sml2.com/";
        var Data = "a=b";
        var page = Sy.Net.Factory.Http(Url).Post(Data);
        Assert.IsNotNull(page);
        Console.WriteLine(page);
        await Task.CompletedTask;
    }
    [Test]
    public void Header()
    {
        var Url = "https://api.sml2.com/";
        var Data = "a=b";
        var page = Sy.Net.Factory.Http(Url).SetContentMd5("xxxx").Post(Data);
        Assert.IsNotNull(page);
        Console.WriteLine(page);
    }
    [Test]
    public void Authorization()
    {
        var Url = "https://api.sml2.com/";
        var Data = "a=b";
        var page = Sy.Net.Factory.Http(Url).SetAuthorization("xxxx").Post(Data);
        Assert.IsNotNull(page);
        Console.WriteLine(page);
    }
    [Test]
    public void MD5()
    {
        var Url = "https://api.sml2.com/";
        var Data = "a=b";
        var page = Sy.Net.Factory.Http(Url).SetContentMd5("md5555vvvv").Post(Data);
        Assert.IsNotNull(page);
        Console.WriteLine(page);
    }
}
