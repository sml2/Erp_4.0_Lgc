﻿using System.Diagnostics;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Net
{
    public class DateTime: InjectTest
    {
        private System.DateTime Now =System.DateTime.Now;


        [SetUp]
        public void Setup() {
        }



        [Test]
        public async Task LastDay()
        {
            System.DateTime January = new(2022, 1, 31);
            var NextMonth = January.Date.AddMonths(1);
            System.DateTime February = new(January.Year, 2, 28);
            Debug.Assert(NextMonth.Equals(February.Date));

            January = new(2000, 1, 31);
            NextMonth = January.Date.AddMonths(1); 
            February = new(January.Year, 2, 28);
            Debug.Assert(!NextMonth.Equals(February.Date));
        }
    }
}

