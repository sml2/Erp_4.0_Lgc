﻿using System.Linq;
using System.Threading.Tasks;
using ERP.Data;
using ERP.Data.Products;
using ERP.DomainEvents;
using ERP.DomainEvents.Product;
using ERP.Enums.Statistics;
using ERP.Models.DB.Orders;
using ERP.Models.DB.Users;
using ERP.Models.Product;
using ERP.Models.Setting;
using MediatR;
using NUnit.Framework;

namespace Test.DomainEvents;


public class ProductCreatedEventTest : InjectTest
{
    
    [TestCase]
    public async Task TestProductCreated()
    {
        // setup
        var mediator = Resolve<IMediator>();
        var mockProduct = new ProductModel()
        {
            ID = 1,
            OEMID = 7,
            CompanyID = 26,
            GroupID = 2,
            UserID = 4444477,
        };

        var dbContext = Resolve<DBContext>();
        await dbContext.OEM.AddAsync(new OEM(){ID = 7});
        await dbContext.Company.AddRangeAsync(new Company[]
        {
            new (){ID = 2,},
            new (){ID = 26, ReportId = 36, Pids = "[2]"}
        });
        await dbContext.UserGroup.AddAsync(new (){ID = 2});
        await dbContext.User.AddAsync(new (){ID = 4444477});
        
        // action
        await mediator.Publish(new ProductCreatedEvent(mockProduct));
        
        // assert 
        // 个人订单数
        Assert.AreEqual(1,
            dbContext.Statistic
                .First(s => s.AboutType == (Types.User | Types.DAY) && s.AboutID == mockProduct.UserID).ProductCount);
        // 公司订单数
        Assert.AreEqual(1,
            dbContext.Statistic
                .First(s => s.AboutType == (Types.Company | Types.DAY) && s.AboutID == mockProduct.CompanyID).ProductCount);
    }
}