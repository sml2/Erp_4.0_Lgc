﻿using System.Linq;
using System.Threading.Tasks;
using ERP.Data;
using ERP.DomainEvents;
using ERP.Enums.Statistics;
using ERP.Models.DB.Orders;
using ERP.Models.DB.Users;
using ERP.Models.Setting;
using MediatR;
using NUnit.Framework;

namespace Test.DomainEvents;


public class OrderCreatedEventTest : InjectTest
{
    
    [TestCase]
    public async Task TestOrderCreated()
    {
        // setup
        var mediator = Resolve<IMediator>();
        var mockOrder = new Order()
        {
            OEMID = 7,
            CompanyID = 26,
            StoreId = 51,
            GroupID = 1,
            UserID = 4444477,
            WaybillCompanyId = 36
        };

        var dbContext = Resolve<DBContext>();
        await dbContext.OEM.AddAsync(new OEM(){ID = 7});
        await dbContext.Company.AddRangeAsync(new Company[]
        {
            new (){ID = 2,},
            new (){ID = 26, ReportId = 36, Pids = "[2]"},
            new (){ID = 36},
        });
        await dbContext.UserGroup.AddAsync(new (){ID = 1});
        await dbContext.User.AddAsync(new (){ID = 4444477});
        await dbContext.StoreRegion.AddAsync(new (){ID = 51, Name = "测试店铺"});
        
        // action
        await mediator.Publish(new OrderCreatedEvent(mockOrder));
        
        // assert 
        // 个人订单数
        Assert.AreEqual(1,
            dbContext.Statistic
                .First(s => s.AboutType == (Types.User | Types.DAY) && s.AboutID == mockOrder.UserID).OrderCount);
        // 公司订单数
        Assert.AreEqual(1,
            dbContext.Statistic
                .First(s => s.AboutType == (Types.Company | Types.DAY) && s.AboutID == mockOrder.CompanyID).OrderCount);
    }
}