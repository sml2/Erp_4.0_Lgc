﻿using System.Diagnostics;
using System.Threading.Tasks;
using System;
using NUnit.Framework;
using Model = ERP.Models.DB.Statistics.Statistic;
namespace Models.DB
{
    public class Statistic : InjectTest
    {
        private System.DateTime Now =System.DateTime.Now;


        [SetUp]
        public void Setup() {
        }



        [Test]
        public async Task New()
        {
            Model m = new(0, ERP.Enums.Statistics.Types.ZERO, ERP.Enums.Statistics.Types.DAY, Now);
            Debug.Assert(m != null);
            Console.WriteLine(m.Validity);
             m = new(0, ERP.Enums.Statistics.Types.ZERO, ERP.Enums.Statistics.Types.MONTH, Now);
            Debug.Assert(m != null);
            Console.WriteLine(m.Validity);
            m = new(0, ERP.Enums.Statistics.Types.ZERO, ERP.Enums.Statistics.Types.YEAR, Now);
            Debug.Assert(m != null);
            Console.WriteLine(m.Validity);
        }
    }
}

