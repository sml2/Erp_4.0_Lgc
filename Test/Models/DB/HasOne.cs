﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace Models.DB;

public class HasOne
{
    public class Order
    {
        public int Id { get; set; }
        public Address Address { get; set; }
    }
        
    public class Address
    {
        public int Id { get; set; }
            
        public int OrderId { get; set; }
        public Order Order { get; set; }
    }
    public class MyDbContext: DbContext
    {
        public MyDbContext(DbContextOptions<MyDbContext> option) : base(option)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Order>().HasOne(a => a.Address).WithOne(b => b.Order)
                .HasForeignKey<Address>(b => b.OrderId);
            // modelBuilder.Entity<B>().HasOne<A>("A");
        }

        public DbSet<Order> A { get; set; }
        public DbSet<Address> B { get; set; }
    }
        
    [Test]
    public async Task Test()
    {
        var option = new DbContextOptionsBuilder<MyDbContext>().UseSqlite("Data Source=t.db").Options;
        var dbContext = new MyDbContext(option);
        await dbContext.Database.EnsureDeletedAsync();
        await dbContext.Database.EnsureCreatedAsync();

        var a = new Order();
        var b = new Address();
        b.Order = a;
        a.Address = b;

        dbContext.Add(b);
        await dbContext.SaveChangesAsync();
    }
}