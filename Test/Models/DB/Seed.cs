﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace Models.DB;

public class Seed
{
    public class xxx
    {
        public int Id { get; set; }
        public Address Address { get; set; }
    }

    [Owned]
    public class Address
    {
        public int xxxId { get; set; }
        public string Name { get; set; }
        public xxx xxx { get; set; }
    }
    public class MyDbContext : DbContext
    {
        public MyDbContext(DbContextOptions<MyDbContext> option) : base(option)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<xxx>().HasData(new xxx[]
            {
                new xxx(){ Id = 1}
            });
            //匿名对象可行 
            var o = new { Name = "3", xxxId = 1 };

            //Dictionary不可行
            //Dictionary<string, object> o = new() { { "Name", "3" }, { "xxxId", 1 } };
            modelBuilder.Entity<xxx>().OwnsOne(typeof(Address), "Address").HasData(o);
            // modelBuilder.Entity<B>().HasOne<A>("A");
        }

        public DbSet<xxx> A { get; set; }
    }

    [Test]
    public async Task Test()
    {
        var option = new DbContextOptionsBuilder<MyDbContext>()
            .EnableSensitiveDataLogging().UseSqlite("Data Source=seed.db").Options;
        var dbContext = new MyDbContext(option);
        await dbContext.Database.EnsureDeletedAsync();

        await dbContext.Database.EnsureCreatedAsync();

    }
}