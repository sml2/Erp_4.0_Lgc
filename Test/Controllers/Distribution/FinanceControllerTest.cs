using System;
using ERP.Controllers.Distribution;
using ERP.DBContext;
using ERP.Interface;
using ERP.Models;
using ERP.Models.Abstract;
using ERP.Services.Caches;
using ERP.Services.Distribution;
using ERP.Services.Finance;
using ERP.Services.Identity;
using ERP.Services.DB.Logistics;
using ERP.Services.Store;
using ERP.Services.User;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using Task = System.Threading.Tasks.Task;

namespace Controllers.Distribution;

public class FinanceControllerTest
{
    private Mock<CompanyService> _companyService;
    private MyDbContext _dbContext;
    private BillLog _billLogService;
    private FinanceService _financeService;
    private Mock<UserManager> _mockUserManager;
    private Mock<ISessionProvider> _mockSessionProvider;

    [SetUp]
    public void SetUp()
    {
        var option = new DbContextOptionsBuilder<MyDbContext>()
            .UseInMemoryDatabase("order")
            .Options;
        _dbContext = new MyDbContext(option);
        _companyService = new Mock<CompanyService>(_dbContext);
        //_billLogService = new ERP.Services.Finance.BillLog();
        //_financeService = new FinanceService(_billLogService, _dbContext);
        _mockUserManager = Helper.MockUserManager();
        _mockSessionProvider = new Mock<ISessionProvider>();
    }

    

    [Test]
    public async Task SuperiorInit()
    {
        // setup

        var controller = new FinanceController(_financeService, _companyService.Object, _mockUserManager.Object);
        
        // action
        var result = await controller.SuperiorInit();
    }

    [Test]
    public async Task GetWalletSearchList()
    {
        // setup
        var controller = new FinanceController(_financeService, _companyService.Object, _mockUserManager.Object);

        // action
        var result = await controller.GetWalletSearchList();
    }

    [Test]
    public async Task GetWalletList()
    {
        // setup
        var companyId = 1;
        
        var controller = new FinanceController(_financeService, _companyService.Object, _mockUserManager.Object);

        // action
        var result = await controller.GetWalletList(companyId);
    }

    [Test]
    public void GetJuniorCompany()
    {
        // setup
        var controller = new FinanceController(_financeService, _companyService.Object, _mockUserManager.Object);

        // action
        var result = controller.GetJuniorCompany();
    }

    [Test]
    public void GetStatisticsList()
    {
        // setup

        var controller = new FinanceController(_financeService, _companyService.Object, _mockUserManager.Object);

        // action
        var result = controller.GetStatisticsList();
    }

    [Test]
    public async Task GetApprovalSearchList()
    {
        // setup

        var controller = new FinanceController(_financeService, _companyService.Object, _mockUserManager.Object);

        // action
        var result = await controller.GetApprovalSearchList();
    }
    
    [Test]
    public async Task GetApprovalList()
    {
        // setup
        var companyId = 1;
        var type = ERP.Models.Finance.BillLog.Types.Recharge;
        var audit = ERP.Models.Finance.BillLog.Audits.Audited;
        var module = ERP.Models.Finance.BillLog.Modules.PURCHASE;

        var controller = new FinanceController(_financeService, _companyService.Object, _mockUserManager.Object);

        // action
        var result = await controller.GetApprovalList(companyId, type, audit, module);
    }
    
    [Test]
    public async Task Approval()
    {
        // setup
        var id = 1;
        var audited = ERP.Models.Finance.BillLog.Audits.Audited;

        var controller = new FinanceController(_financeService, _companyService.Object, _mockUserManager.Object);

        // action
        var result = await controller.Approval(id, audited);
    }
    
    [Test]
    public async Task JuniorInit()
    {
        // setup

        var controller = new FinanceController(_financeService, _companyService.Object, _mockUserManager.Object);

        // action
        var result = await controller.JuniorInit();
    }
    
    [Test]
    public async Task JuniorStatisticsInit()
    {
        // setup

        var controller = new FinanceController(_financeService, _companyService.Object, _mockUserManager.Object);

        // action
        var result = await controller.JuniorStatisticsInit();
    }
    
    [Test]
    public void GetUserStatisticsList()
    {
        // setup
        var name = "fakeName";
        var state = 1;
        var userService = new UserService(_dbContext, _mockSessionProvider.Object, _mockUserManager.Object);

        var controller = new FinanceController(_financeService, _companyService.Object, _mockUserManager.Object);

        // action
        var result = controller.GetUserStatisticsList(name, state, userService);
    }
    
    [Test]
    public void GetStoreStatisticsList()
    {
        // setup
        var name = "fakeName";
        var storeService = new StoreService(_dbContext, null, null, null,null, null, null, null, null, null, null, null, null);
        BaseModel.StateEnum? stateEnum = null;

        var controller = new FinanceController(_financeService, _companyService.Object, _mockUserManager.Object);

        // action
        var result = controller.GetStoreStatisticsList(name, stateEnum, storeService);
    }
    
    [Test]
    public void GetGroupStatisticsList()
    {
        // setup

        var controller = new FinanceController(_financeService, _companyService.Object, _mockUserManager.Object);

        // action
        var result = controller.GetGroupStatisticsList();
    }
    
    [Test]
    public async Task GetCompanyBillSearchList()
    {
        // setup

        var controller = new FinanceController(_financeService, _companyService.Object, _mockUserManager.Object);

        // action
        var result = await controller.GetCompanyBillSearchList();
    }
    
    [Test]
    public async Task GetCompanyBillList()
    {
        // setup
        var types = (ERP.Models.Finance.BillLog.Types?)null;
        var audit = (ERP.Models.Finance.BillLog.Audits?)null;
        var module = (ERP.Models.Finance.BillLog.Modules?)null;

        var controller = new FinanceController(_financeService, _companyService.Object, _mockUserManager.Object);

        // action
        var result = await controller.GetCompanyBillList(types, audit, module);
    }
    
    [Test]
    public async Task GetApprovalJuniorSearchList()
    {
        // setup
        _companyService.Setup(c => c.GetInfoById(1)).ReturnsAsync(Helper.FakeCompany);
        var controller = new FinanceController(_financeService, _companyService.Object, _mockUserManager.Object);

        // action
        var result = await controller.GetApprovalJuniorSearchList();
    }
    
    [Test]
    public async Task GetApprovalJuniorList()
    {
        // setup
        ERP.Models.Finance.BillLog.Types? type = null;
        ERP.Models.Finance.BillLog.Modules? module = null;

        var controller = new FinanceController(_financeService, _companyService.Object, _mockUserManager.Object);

        // action
        var result = await controller.GetApprovalJuniorList(type, module);
    }
    
    [Test]
    public void Action()
    {
        Assert.Ignore();
        // setup

        var controller = new FinanceController(_financeService, _companyService.Object, _mockUserManager.Object);

        // action
        var result = controller.Action(null);
    }
    
    [Test]
    public async Task BillInfo()
    {
        // setup
        var id = 1;

        var controller = new FinanceController(_financeService, _companyService.Object, _mockUserManager.Object);

        // action
        var result = await controller.BillInfo(id);
    }
    
}