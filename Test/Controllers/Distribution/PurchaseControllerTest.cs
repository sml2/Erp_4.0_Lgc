using ERP.Controllers.Distribution;
using ERP.DBContext;
using ERP.Enums.Purchase;
using ERP.Interface;
using ERP.Services.DB.Logistics;
using ERP.Services.DB.Statistics;
using ERP.Services.Distribution;
using ERP.Services.Finance;
using ERP.Services.Identity;
using ERP.Services.Statistics;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using CompanyService = ERP.Services.DB.User.CompanyService;
using DisCompanyService = ERP.Services.Distribution.CompanyService;
using Task = System.Threading.Tasks.Task;

namespace Controllers.Distribution;

public class PurchaseControllerTest
{
    private AmazonOrderService _amazonOrderService;
    private DisCompanyService _DisCompanyService;
    private PurchaseService _purchaseService;
    private WaybillService _waybillService;
    private MyDbContext _dbContext;
    private CounterGroupBusinessService _CounterGroupBusinessService;
    private CounterUserBusinessService _CounterUserBusinessService;
    private CounterCompanyBusinessService _CounterCompanyBusiness;
    private FinancialAffairsService _FinancialAffairsService;
    private BillLog _BillLogService;
    private CompanyService _CompanyService;
    private Mock<UserManager> _mockUserManager;
    private ERP.Services.DB.Orders.Order _amazonOrder;
    private ERP.Services.DB.Orders.Action _OrderAction;
    private Statistic _statisticService;
    [SetUp]
    public void SetUp()
    {
        var option = new DbContextOptionsBuilder<MyDbContext>()
            .UseInMemoryDatabase("purchase")
            .Options;
        _dbContext = new MyDbContext(option);
        var _mockSessionProvider = new Mock<ISessionProvider>();
        _mockUserManager = Helper.MockUserManager();
        var userService = new ERP.Services.User.UserService(_dbContext, _mockSessionProvider.Object, _mockUserManager.Object);
        _amazonOrderService = new AmazonOrderService(_dbContext, userService);
        _DisCompanyService = new DisCompanyService(_dbContext);
        _purchaseService = new PurchaseService(_dbContext);
        _statisticService = new Statistic(_dbContext, new FinancialAffairsService(_dbContext, _mockSessionProvider.Object));
        _amazonOrder = new ERP.Services.DB.Orders.Order(_dbContext, _mockSessionProvider.Object, _statisticService);
        _waybillService = new WaybillService(new WayBill(_dbContext, _mockSessionProvider.Object, _amazonOrder, _OrderAction, _statisticService, _CounterGroupBusinessService, _CounterUserBusinessService, _CounterCompanyBusiness, _FinancialAffairsService, _BillLogService, _CompanyService), userService, _dbContext);
        
    }
    
    [Test]
    public async Task SuperiorInit()
    {
        // setup
        var controller = new PurchaseController(_purchaseService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.SuperiorInit();
    }

    [Test]
    public async Task GetWaitSearchList()
    {
        // setup

        var controller = new PurchaseController(_purchaseService, _DisCompanyService, _mockUserManager.Object);
        
        // action
        var result = await controller.GetWaitSearchList();
    }

    [Test]
    public async Task GetWaitList()
    {
        // setup
        var orderNo = "";
        var companyId = 1;
        var progress = 1;
        var orderState = ERP.Enums.Orders.States.CANCELED;//CANCEL
        var sortBy = 1;
        
        var controller = new PurchaseController(_purchaseService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.GetWaitList(new PurchaseController.WaitListQuery(orderNo, companyId, progress, orderState, sortBy), _amazonOrderService);
    }

    [Test]
    public async Task GetHasList()
    {
        // setup
        var orderNo = "fakeOrderNo";
        var companyId = 1;
        
        var controller = new PurchaseController(_purchaseService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.GetHasList(orderNo, companyId);
    }
    
    [Test]
    public async Task GetHasSearchList()
    {
        // setup
        var controller = new PurchaseController(_purchaseService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.GetHasSearchList();
    }

    [Test]
    public async Task GetList()
    {
        // seup
        var orderNo = "fakeOrderNo";
        var companyId = 1;
        var state = PurchaseStates.REFUSED;
        
        var controller = new PurchaseController(_purchaseService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.GetList(orderNo, companyId, state);
    }
    
    [Test]
    public async Task GetSearchList()
    {
        // setup
        var controller = new PurchaseController(_purchaseService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.GetSearchList();
    }

    [Test]
    public async Task GetMySearchList()
    {
        // setup

        var controller = new PurchaseController(_purchaseService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.GetMySearchList();
    }

    [Test]
    public async Task GetMyList()
    {
        // setup
        var orderNo = "fakeOrderNo";
        var companyId = 1;
        var state = PurchaseStates.REFUSED;

        var controller = new PurchaseController(_purchaseService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.GetMyList(orderNo, companyId, state);
    }
    
    [Test]
    public async Task Info()
    {
        // setup
        var id = 1;

        var controller = new PurchaseController(_purchaseService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.Info(id);
    }

    [Test]
    public void GetPurchase()
    {
        Assert.Ignore();
        // setup

        var controller = new PurchaseController(_purchaseService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = controller.GetPurchase();
    }
    
    [Test]
    public void SubPurchase()
    {
        Assert.Ignore();
        // setup

        var controller = new PurchaseController(_purchaseService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = controller.SubPurchase();
    }
    
    [Test]
    public async Task JuniorInit()
    {
        // setup

        var controller = new PurchaseController(_purchaseService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.JuniorInit();
    }
    
    [Test]
    public async Task GetJuniorWaitSearchList()
    {
        // setup

        var controller = new PurchaseController(_purchaseService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.GetJuniorWaitSearchList();
    }
    
    [Test]
    public async Task BackJuniorPurchase()
    {
        // setup
        var id = 1;

        var controller = new PurchaseController(_purchaseService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.BackJuniorPurchase(id, _amazonOrderService);
    }
    
    [Test]
    public async Task ReportJuniorPurchase()
    {
        // setup
        var id = 1;

        var controller = new PurchaseController(_purchaseService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.ReportJuniorPurchase(id, _amazonOrderService);
    }
    
    [Test]
    public async Task GetJuniorHasList()
    {
        // setup
        var storeId = 1;
        var orderNo = "fakeOrderNo";

        var controller = new PurchaseController(_purchaseService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.GetJuniorHasList(orderNo, storeId);
    }
    
    [Test]
    public async Task GetJuniorHasSearchList()
    {
        // setup

        var controller = new PurchaseController(_purchaseService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.GetJuniorHasSearchList();
    }
    
    [Test]
    public async Task GetJuniorWaitList()
    {
        // setup
        var orderNo = "fakeOrderNo";
        int? storeId = null;
        var progress = 0;
        ERP.Enums.Orders.States? state = null;
        string sortBy = null;

        var controller = new PurchaseController(_purchaseService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.GetJuniorWaitList(orderNo, storeId, progress, state, sortBy, _amazonOrderService);
    }
    
    [Test]
    public async Task GetJuniorList()
    {
        // setup
        var orderNo = "fakeOrderNo";
        int? storeId = null;
        PurchaseStates? state = null;

        var controller = new PurchaseController(_purchaseService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.GetJuniorList(orderNo, storeId, state);
    }
    
    [Test]
    public async Task GetJuniorSearchList()
    {
        // setup

        var controller = new PurchaseController(_purchaseService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.GetJuniorSearchList();
    }
    
    [Test]
    public async Task Confirm()
    {
        Assert.Ignore();
        // setup
        var ids = new int[] { 1 };

        var controller = new PurchaseController(_purchaseService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.Confirm(ids, _dbContext);
    }
    
    [Test]
    public void Reject()
    {
        Assert.Ignore();
        // setup

        var controller = new PurchaseController(_purchaseService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = controller.Reject();
    }
}