using ERP.Controllers.Distribution;
using ERP.DBContext;
using ERP.Enums.Orders;
using ERP.Interface;
using ERP.Services.Caches;
using ERP.Services.DB.Logistics;
using ERP.Services.DB.Statistics;
using ERP.Services.Distribution;
using ERP.Services.Finance;
using ERP.Services.Identity;
using ERP.Services.Statistics;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;
using CompanyService = ERP.Services.DB.User.CompanyService;
using DisCompanyService = ERP.Services.Distribution.CompanyService;
using Task = System.Threading.Tasks.Task;

namespace Controllers.Distribution;

public class WaybillControllerTest
{
    private AmazonOrderService _amazonOrderService;
    private DisCompanyService _DisCompanyService;
    private CompanyService _CompanyService;
    private PurchaseService _purchaseService;
    private WaybillService _waybillService;
    private CounterGroupBusinessService _CounterGroupBusinessService;
    private CounterUserBusinessService _CounterUserBusinessService;
    private CounterCompanyBusinessService _CounterCompanyBusiness;
    private FinancialAffairsService _FinancialAffairsService;
    private BillLog _BillLogService;
    private MyDbContext _dbContext;
    private Mock<UserManager> _mockUserManager;
    private ERP.Services.DB.Orders.Order _amazonOrder;
    private ERP.Services.DB.Orders.Action _OrderAction;
    private Statistic _statisticService;
    [SetUp]
    public void SetUp()
    {
        var option = new DbContextOptionsBuilder<MyDbContext>()
            .UseInMemoryDatabase("order")
            .Options;
        _dbContext = new MyDbContext(option);
        var _mockSessionProvider = new Mock<ISessionProvider>();
        _mockUserManager = Helper.MockUserManager();
        var userService = new ERP.Services.User.UserService(_dbContext, _mockSessionProvider.Object, _mockUserManager.Object);
        _amazonOrderService = new AmazonOrderService(_dbContext, userService);
        _DisCompanyService = new DisCompanyService(_dbContext);
        _purchaseService = new PurchaseService(_dbContext);
        _statisticService = new Statistic(_dbContext, new FinancialAffairsService(_dbContext, _mockSessionProvider.Object));
        _amazonOrder = new ERP.Services.DB.Orders.Order(_dbContext, _mockSessionProvider.Object, _statisticService);
        _waybillService = new WaybillService(new WayBill(_dbContext, _mockSessionProvider.Object, _amazonOrder,_OrderAction, _statisticService, _CounterGroupBusinessService, _CounterUserBusinessService, _CounterCompanyBusiness, _FinancialAffairsService, _BillLogService, _CompanyService), userService, _dbContext);
    }

    [Test]
    public async Task SuperiorInit()
    {
        // setup

        var controller = new WaybillController(_waybillService, _DisCompanyService, _mockUserManager.Object);
        
        // action
        var result = await controller.SuperiorInit();
    }
    
    [Test]
    public async Task GetWaitSearchList()
    {
        // setup

        var controller = new WaybillController(_waybillService, _DisCompanyService, _mockUserManager.Object);
        
        // action
        var result = await controller.GetWaitSearchList();
    }

    [Test]
    public async Task GetWaitList()
    {
        // setup
        var orderNo = "fakeOrderNo";
        int? companyId = null;
        var progress = 0;
        ERP.Enums.Orders.States? state = null;
        string sortBy = "";
        var controller = new WaybillController(_waybillService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.GetWaitList(orderNo, companyId, progress, state, sortBy, _amazonOrderService);
    }

    [Test]
    public async Task GetSearchList()
    {
        // setup
        var controller = new WaybillController(_waybillService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.GetSearchList();
    }

    [Test]
    public async Task GetList()
    {
        // seup
        string orderNo = "fakeOrderNo";
        string express = null;
        string waybillOrder = null;
        string tracking = null;
        var state = ERP.Enums.Logistics.States.COMMITTED;
        int? companyId = null;
        int? warehouseId = null;
        var controller = new WaybillController(_waybillService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.GetList(orderNo, express, waybillOrder, tracking, state, companyId, warehouseId);
    }

    [Test]
    public async Task GetMySearchList()
    {
        // setup

        var controller = new WaybillController(_waybillService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.GetMySearchList();
    }

    [Test]
    public async Task GetMyList()
    {
        // setup
        string orderNo = "fakeOrderNo";
        string express = null;
        string waybillOrder = null;
        string tracking = null;
        var state = ERP.Enums.Logistics.States.COMMITTED;
        int? companyId = null;
        int? warehouseId = null;
        var controller = new WaybillController(_waybillService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.GetMyList(orderNo, express, waybillOrder, tracking, state, companyId, warehouseId);
    }
    
    [Test]
    public async Task JuniorInit()
    {
        // setup

        var controller = new WaybillController(_waybillService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.JuniorInit();
    }
    
    [Test]
    public async Task BackJuniorWaybill()
    {
        // setup
        var id = 1;

        var controller = new WaybillController(_waybillService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.BackJuniorWaybill(id, _amazonOrderService);
    }
    
    [Test]
    public async Task ReportJuniorWaybill()
    {
        // setup
        var id = 1;

        var controller = new WaybillController(_waybillService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.ReportJuniorWaybill(id, _amazonOrderService);
    }
    
    [Test]
    public async Task GetJuniorOrderSearchList()
    {
        // setup

        var controller = new WaybillController(_waybillService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.GetJuniorOrderSearchList();
    }
    
    [Test]
    public async Task GetJuniorOrderList()
    {
        // setup
        string orderNo = null;
        string sortBy = orderNo;
        var state = States.None;
        var progress = 0;
        int? storeId = null;

        var controller = new WaybillController(_waybillService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.GetJuniorOrderList(orderNo, storeId, progress, state, sortBy, _amazonOrderService);
    }
    
    [Test]
    public async Task GetJuniorSearchList()
    {
        // setup

        var controller = new WaybillController(_waybillService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.GetJuniorSearchList(new Store(Mock.Of<IOptions<MemoryCacheOptions>>(), Mock.Of<ILogger<Store>>(), new ServiceCollection().BuildServiceProvider()));
    }
    
    [Test]
    public async Task GetJuniorList()
    {
        // setup
        string orderNo = null;
        string express = null;
        string waybillOrder = null;
        string tracking = null;
        ERP.Enums.Logistics.States? state = null;
        int? storeId = null;
        int? warehouseId = null;

        var controller = new WaybillController(_waybillService, _DisCompanyService, _mockUserManager.Object);

        // action
        var result = await controller.GetJuniorList(orderNo, express, waybillOrder, tracking, state, storeId, warehouseId);
    }
    
}