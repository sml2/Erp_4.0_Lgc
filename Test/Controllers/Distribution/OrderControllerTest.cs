using System;
using System.Threading.Tasks;
using ERP.Controllers.Distribution;
using ERP.DBContext;
using ERP.Interface;
using ERP.Services.DB.Statistics;
using ERP.Services.Distribution;
using ERP.Services.Identity;
using ERP.Services.DB.Logistics;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using ERP.Services.Statistics;
using ERP.Services.Finance;
using DisCompanyService = ERP.Services.Distribution.CompanyService;
using  CompanyService = ERP.Services.DB.User.CompanyService;

namespace Controllers.Distribution;

public class OrderControllerTest
{
    private AmazonOrderService _amazonOrderService;
    private DisCompanyService _DisCompanyService;
    private PurchaseService _purchaseService;
    private WaybillService _waybillService;
    private MyDbContext _dbContext;
    private CounterGroupBusinessService _CounterGroupBusinessService;
    private CounterUserBusinessService _CounterUserBusinessService;
    private CounterCompanyBusinessService _CounterCompanyBusiness;
    private FinancialAffairsService _FinancialAffairsService;
    private BillLog _BillLogService;
    private  CompanyService _CompanyService;
    private ISessionProvider _SessionProvider;
    private Mock<UserManager> _mockUserManager;
    private ERP.Services.DB.Orders.Action _OrderAction;
    private Statistic _statisticService;
    private ERP.Services.DB.Orders.Order _amazonOrder;

    [SetUp]
    public void SetUp()
    {
        var option = new DbContextOptionsBuilder<MyDbContext>()
            .UseInMemoryDatabase("order")
            .Options;
        _dbContext = new MyDbContext(option);
        var _mockSessionProvider = new Mock<ISessionProvider>();
        _mockUserManager = Helper.MockUserManager();
        var userService = new ERP.Services.User.UserService(_dbContext, _mockSessionProvider.Object,_mockUserManager.Object);
        _amazonOrderService = new AmazonOrderService(_dbContext, userService);
        _DisCompanyService = new DisCompanyService(_dbContext);
        _purchaseService = new PurchaseService(_dbContext);

        _statisticService = new Statistic(_dbContext, new FinancialAffairsService(_dbContext, _mockSessionProvider.Object));
        _amazonOrder = new ERP.Services.DB.Orders.Order(_dbContext, _mockSessionProvider.Object, _statisticService);
      _waybillService = new WaybillService(new WayBill(_dbContext, _mockSessionProvider.Object, _amazonOrder, _OrderAction, _statisticService, _CounterGroupBusinessService, _CounterUserBusinessService, _CounterCompanyBusiness, _FinancialAffairsService, _BillLogService, _CompanyService), userService, _dbContext);
    }

    [Test]
    public async Task GetList()
    {
        // _amazonOrderService.Setup(o =>
        //     o.getListPage("order_no", "test", ERP.Models.Abstract.BaseModel.StateEnum.Enable, 2, Array.Empty<int>()
        //         , "test", 0, null, null) == Task.FromResult(new ERP.Extensions.DbSetExtension.PaginateStruct<ERP.Models.Order.AmazonOrder>())
        // );
        // setup
        var orderNo = "order_no";
        var progress = "test";
        var orderState = ERP.Models.Abstract.BaseModel.StateEnum.Enable;
        var companyId = 2;
        var sortBy = "test";
        var purchaseTrackingNumber = "";
        DateTime[] dateTimes = null;
        var waybillOrder = "";
        var type = 0;
        
        var controller = new OrderController(_amazonOrderService, _DisCompanyService, _purchaseService, _waybillService);
        
        // action
        var query = new OrderController.AmazonOrderListQuery(
            orderNo,
            progress,
            orderState,
            companyId,
            sortBy,
            purchaseTrackingNumber, // purchasing
            dateTimes,
            waybillOrder, // waybill
            type
        );

        var result = await controller.GetList(query);

    }

    [Test]
    public void GetSearchList()
    {
        // setup
        var controller = new OrderController(_amazonOrderService, _DisCompanyService,_purchaseService, _waybillService);

        // action
        var result = controller.GetSearchList();
    }

    [Test]
    public void GetJuniorSearchList()
    {
        // setup
        var controller = new OrderController(_amazonOrderService, _DisCompanyService,
            _purchaseService, _waybillService);

        // action
        var result = controller.GetJuniorSearchList();
    }

    [Test]
    public void GetJuniorList()
    {
        // seup
        var controller = new OrderController(_amazonOrderService, _DisCompanyService,
            _purchaseService, _waybillService);

        // action
        var result = controller.GetJuniorList();
    }

    [Test]
    public void Info()
    {
        // setup
        var id = 1;

        var controller = new OrderController(_amazonOrderService, _DisCompanyService,
            _purchaseService, _waybillService);

        // action
        var result = controller.Info(id, _dbContext);
    }

    [Test]
    public void PurchaseGood()
    {
        // setup
        var id = 1;

        var controller = new OrderController(_amazonOrderService, _DisCompanyService,_purchaseService, _waybillService);

        // action
        var result = controller.PurchaseGood(id);
    }
}