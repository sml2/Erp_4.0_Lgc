using ERP.Controllers.Message;
using ERP.Controllers.Setting;
using ERP.DBContext;
using ERP.Interface;
using ERP.Models.Abstract;
using ERP.Services.DB.Statistics;
using ERP.Services.Distribution;
using ERP.Services.Finance;
using ERP.Services.Identity;
using ERP.Services.DB.Logistics;
using ERP.Services.Message;
using ERP.Services.Statistics;
using ERP.Services.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using CompanyService = ERP.Services.DB.User.CompanyService;
using Task = System.Threading.Tasks.Task;
using ERP.Models.DB.Users;

namespace Controllers.Setting;

public class BusinessControllerTest
{
    private AmazonOrderService _amazonOrderService;
    private Mock<CompanyService> _mockCompanyService;
    private PurchaseService _purchaseService;
    private WaybillService _waybillService;
    private MyDbContext _dbContext;
    private Mock<UserManager> _mockUserManager;
    private UserService _userService;
    private CounterUserBusinessService _counterUserBusinessService;
    private CounterCompanyBusinessService _counterCompanyBusinessService;
    private CounterGroupBusinessService _counterGroupBusinessService;
    private GroupService _groupService;
    private CounterRequestService _counterRequestService;
    private PayBillService _payBillService;
    private ERP.Services.DB.Orders.Order _amazonOrder;
    private ERP.Services.DB.Orders.Action _OrderAction;
    private Statistic _statisticService;
    [SetUp]
    public void SetUp()
    {
        var option = new DbContextOptionsBuilder<MyDbContext>()
            .UseInMemoryDatabase("order")
            .Options;
        _dbContext = new MyDbContext(option);
        var _mockSessionProvider = new Mock<ISessionProvider>();
        _mockUserManager = Helper.MockUserManager();
        _userService = new UserService(_dbContext, _mockSessionProvider.Object, _mockUserManager.Object);
        _counterUserBusinessService = new CounterUserBusinessService(_dbContext, _mockSessionProvider.Object);
        _counterCompanyBusinessService = new CounterCompanyBusinessService(_dbContext, _mockSessionProvider.Object);
        _counterGroupBusinessService = new CounterGroupBusinessService(_dbContext, _mockSessionProvider.Object);
        _counterRequestService = new CounterRequestService(_dbContext);
        _amazonOrderService = new AmazonOrderService(_dbContext, _userService);
        _mockCompanyService = new Mock<CompanyService>(_dbContext, _userService, _mockSessionProvider.Object, null, null, null);
        _groupService = new GroupService(_dbContext, _mockSessionProvider.Object);
        _purchaseService = new PurchaseService(_dbContext);
        _payBillService = new PayBillService(_dbContext);
        _statisticService = new Statistic(_dbContext, new FinancialAffairsService(_dbContext, _mockSessionProvider.Object));
        _amazonOrder = new ERP.Services.DB.Orders.Order(_dbContext, _mockSessionProvider.Object, _statisticService);
        _waybillService = new WaybillService(new WayBill(_dbContext, _mockSessionProvider.Object, _amazonOrder, _OrderAction, _statisticService,_counterGroupBusinessService,_counterUserBusinessService,_counterCompanyBusinessService,null,null, _mockCompanyService.Object), _userService, _dbContext);
    }
 
    [Test]
    public async Task RuleInit()
    {
        // setup

        var controller = new BusinessController(_userService, _mockCompanyService.Object, _mockUserManager.Object);
        
        // action
        var result = await controller.RuleInit();
    }
    
    [Test]
    public async Task Person()
    {
        // setup

        var controller = new BusinessController(_userService, _mockCompanyService.Object, _mockUserManager.Object);
        
        // action
        var result = await controller.Person(_counterUserBusinessService);
    }
    
    [Test]
    public async Task Current()
    {
        // setup

        var controller = new BusinessController(_userService, _mockCompanyService.Object, _mockUserManager.Object);
        
        // action
        var result = await controller.Current(_counterCompanyBusinessService);
    }

    [Test]
    public async Task GetUserList()
    {
        // setup
        string name = null;
        int? state = null;
        var controller = new BusinessController(_userService, _mockCompanyService.Object, _mockUserManager.Object);

        // action
        
        var result = await controller.GetUserList(name, state, _counterUserBusinessService);
    }
    
    [Test]
    public async Task GetGroupList()
    {
        // setup
        string name = null;
        BaseModel.StateEnum? state = null;
        var controller = new BusinessController(_userService, _mockCompanyService.Object, _mockUserManager.Object);

        // action
        
        var result = await controller.GetGroupList(name, state, _groupService, _counterGroupBusinessService);
    }
    
    [Test]
    public async Task GetCompanyList()
    {
        // setup
        string name = null;
        Company.States? state = null;
        var controller = new BusinessController(_userService, _mockCompanyService.Object, _mockUserManager.Object);

        // action
        
        var result = await controller.GetCompanyList(name, state, _counterCompanyBusinessService);
    }
    
    [Test]
    public async Task GetResourceList()
    {
        // setup
        string name = null;
        int? state = null;
        var controller = new BusinessController(_userService, _mockCompanyService.Object, _mockUserManager.Object);

        // action
        
        var result = await controller.GetResourceList(name, state, _counterRequestService);
    }
    
    [Test]
    public async Task GetReferrerList()
    {
        // setup
        string name = null;
        var controller = new BusinessController(_userService, _mockCompanyService.Object, _mockUserManager.Object);

        // action
        
        var result = await controller.GetReferrerList(name);
    }

    [Test]
    public async Task GetExtendList()
    {
        // setup
        int companyId = 2;
        var controller = new BusinessController(_userService, _mockCompanyService.Object, _mockUserManager.Object);

        // action
        var result = await controller.GetExtendList(companyId, _payBillService);
    }

    [Test]
    public async Task Dashboard()
    {
        // setup
        _mockCompanyService.Setup(c => c.GetCurrentConfig(1)).ReturnsAsync(Helper.FakeCompany());
        var controller = new BusinessController(_userService, _mockCompanyService.Object, _mockUserManager.Object);
        
        // action
        var result = await controller.Dashboard(new NoticeService(_dbContext), _counterCompanyBusinessService, _counterUserBusinessService, _dbContext);
    }

}