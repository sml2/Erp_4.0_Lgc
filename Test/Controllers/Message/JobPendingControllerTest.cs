﻿using System.Threading.Tasks;
using ERP.Controllers.Message;
using ERP.DBContext;
using ERP.Models.Message;
using ERP.Services.Identity;
using ERP.Services.Message;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;

namespace Controllers.Message;

public class JobPendingControllerTest
{
    private MyDbContext _dbContext;
    private Mock<UserManager> _mockUserManager;

    private NoticeService _noticeService;
    private WorkOrderService _workOrderService;
    private WorkOrderReplyService _workOrderReplyService;
    private WorkOrderCategoryService _workOrderCategoryService;
    private WorkOrderProgressService _workOrderProgressService;

    [SetUp]
    public void SetUp()
    {
        var option = new DbContextOptionsBuilder<MyDbContext>()
            .UseInMemoryDatabase("order")
            .Options;
        _dbContext = new MyDbContext(option);
        _mockUserManager = Helper.MockUserManager();
        _workOrderService = new WorkOrderService(_dbContext);
        _workOrderReplyService = new WorkOrderReplyService(_dbContext);
        _workOrderCategoryService = new WorkOrderCategoryService(_dbContext);
        _workOrderProgressService = new WorkOrderProgressService(_dbContext);
    }

    [Test]
    public async Task PendingList()
    {
        // setup
        var name = "name";
        var state = WorkOrder.States.Complete;
        var commit = WorkOrder.Commits.HIGHERCOMPANY;

        var controller = new JobPendingController(_workOrderService,_workOrderReplyService,_workOrderCategoryService,_workOrderProgressService, _mockUserManager.Object);
        // action

        var title = "title";
        var result = await controller.PendingList(new JobPendingController.PendingListQuery(title, state, commit));
    }
    
    [Test]
    public async Task MessagePendingList()
    {
        // setup

        var controller = new JobPendingController(_workOrderService,_workOrderReplyService,_workOrderCategoryService,_workOrderProgressService, _mockUserManager.Object);
        // action

        var result = await controller.MessagePendingList();
    }
    
    [Test]
    public async Task WorkOrderProcess()
    {
        // setup
        var id = 1;
        var progressId = 1;
        var isComplete = false;
        var content = "content";
        var userId = 1;

        var controller = new JobPendingController(_workOrderService,_workOrderReplyService,_workOrderCategoryService,_workOrderProgressService, _mockUserManager.Object);
        // action

        var result = await controller.WorkOrderProcess(id, new JobPendingController.WorkOrderProcessVm(progressId, isComplete, content, userId));
    }
    
    [Test]
    public async Task Complete()
    {
        // setup
        var id = 1;

        var controller = new JobPendingController(_workOrderService,_workOrderReplyService,_workOrderCategoryService,_workOrderProgressService, _mockUserManager.Object);
        // action

        var result = await controller.Complete(id);
    }
    
}