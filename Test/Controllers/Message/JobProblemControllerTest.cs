﻿using System.Threading.Tasks;
using ERP.Controllers.Message;
using ERP.DBContext;
using ERP.Models.Message;
using ERP.Services.Identity;
using ERP.Services.Message;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;

namespace Controllers.Message;

public class JobProblemControllerTest
{
    private MyDbContext _dbContext;
    private Mock<UserManager> _mockUserManager;

    private WorkOrderService _workOrderService;
    private WorkOrderCategoryService _workOrderCategoryService;

    [SetUp]
    public void SetUp()
    {
        var option = new DbContextOptionsBuilder<MyDbContext>()
            .UseInMemoryDatabase("order")
            .Options;
        _dbContext = new MyDbContext(option);
        _mockUserManager = Helper.MockUserManager();
        _workOrderService = new WorkOrderService(_dbContext);
        _workOrderCategoryService = new WorkOrderCategoryService(_dbContext);
    }

    [Test]
    public async Task CategoryList()
    {
        // setup
        var name = "title";
        WorkOrderCategory.States? state = null;
        
        var controller = new JobProblemController(_workOrderService,_workOrderCategoryService, _mockUserManager.Object);
        
        // action

        var result = await controller.CategoryList(new JobProblemController.CategoryListQuery(state, name));
    
    }
    
    [Test]
    public async Task CategoryAdd()
    {
        // setup
        var name = "title";
        
        var controller = new JobProblemController(_workOrderService,_workOrderCategoryService, _mockUserManager.Object);
        // action

        var result = await controller.CategoryAdd(new JobProblemController.CategoryAddVm(name));
    
    }
    
    [Test]
    public async Task CategoryEdit()
    {
        // setup
        int id = 1;
        var state = WorkOrderCategory.States.ENABLE;
        var name = "name";
        
        var controller = new JobProblemController(_workOrderService,_workOrderCategoryService, _mockUserManager.Object);
        // action

        var result = await controller.CategoryEdit(id, new JobProblemController.CategoryEditVm(name, state));
    }
    
    [Test]
    public async Task CategoryDel()
    {
        // setup
        var id = 1;

        var controller = new JobProblemController(_workOrderService,_workOrderCategoryService, _mockUserManager.Object);
        // action

        var result = await controller.CategoryDel(id);
    }
}