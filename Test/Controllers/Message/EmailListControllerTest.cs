﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.Controllers.Message;
using ERP.DBContext;
using ERP.Interface;
using ERP.Models.DB.Users;
using ERP.Services.Caches;
using ERP.Services.Distribution;
using ERP.Services.Identity;
using ERP.Services.Message;
using ERP.Services.User;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using EmailConfig = ERP.Models.Setting.EmailConfig;

namespace Controllers.Message;

public class EmailListControllerTest
{
    private WaybillService _waybillService;
    private MyDbContext _dbContext;
    private ISessionProvider _SessionProvider;
    private Mock<UserManager> _mockUserManager;

    private EmailInfoService _emailInfoService;
    private UserEmailService _userEmailService;

    [SetUp]
    public void SetUp()
    {
        var option = new DbContextOptionsBuilder<MyDbContext>()
            .UseInMemoryDatabase("order")
            .Options;
        _dbContext = new MyDbContext(option);
        _mockUserManager = Helper.MockUserManager();
        _emailInfoService = new EmailInfoService(_dbContext);
        _userEmailService = new UserEmailService(_dbContext);
    }

    [Test]
    public async Task EmailList()
    {
        // setup
        UserEmail.States? state = UserEmail.States.ENABLE;
        var typeId = 0;
        var name = "";
        
        var controller = new EmailListController(_emailInfoService, _userEmailService, _mockUserManager.Object);
        var emailConfigCacheMock = new Mock<EmailConfigCache>();
        emailConfigCacheMock.Setup(e => e.List()).Returns(new List<EmailConfig>());
        // action
    
        var result = await controller.EmailList(new ( state, typeId, name), emailConfigCacheMock.Object);
    
    }
    
    [Test]
    public async Task Info()
    {
        // setup
        var id = 0;
        
        var controller = new EmailListController(_emailInfoService, _userEmailService, _mockUserManager.Object);
        var emailConfigCacheMock = new Mock<EmailConfigCache>();
        emailConfigCacheMock.Setup(e => e.List()).Returns(new List<EmailConfig>());
        // action
    
        var result = await controller.Info(id, emailConfigCacheMock.Object);
    }
    
    [Test]
    public async Task AddEmail()
    {
        // setup
        var id = 0;
        var address = "address";
        var name = "name";
        var nick = "nick";
        var state = UserEmail.States.ENABLE;
        var token = "token";
        var typeId = 1;
        
        
        var controller = new EmailListController(_emailInfoService,  _userEmailService, _mockUserManager.Object);
        // action

        var result = await controller.AddEmail(new EmailListController.AddEmailVm(){Address = address, Name = name, Nick = nick, State = state, Token = token, TypeId = typeId});
    }
    
    [Test]
    public async Task DelEmail()
    {
        // setup
        var id = 0;

        var controller = new EmailListController(_emailInfoService, _userEmailService, _mockUserManager.Object);
        // action

        var result = await controller.DelEmail(id);
    }
    
    [Test]
    public async Task ChangeEmailState()
    {
        // setup
        var id = 0;
        var state = UserEmail.States.ENABLE;

        var controller = new EmailListController(_emailInfoService, _userEmailService, _mockUserManager.Object);
        // action

        var result = await controller.ChangeEmailState(id, state);
    }
    
    [Test]
    public async Task ResetEmail()
    {
        // setup
        var id = 0;
        var state = EmailListController.ResetAction.ClearAndReset;

        var controller = new EmailListController(_emailInfoService, _userEmailService, _mockUserManager.Object);
        // action

        var result = await controller.ResetEmail(id, state);
    }
}