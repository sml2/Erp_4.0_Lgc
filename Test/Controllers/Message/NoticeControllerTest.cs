﻿using System.Threading.Tasks;
using ERP.Controllers.Message;
using ERP.DBContext;
using ERP.Models.Message;
using ERP.Services.Identity;
using ERP.Services.Message;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Moq;
using NUnit.Framework;

namespace Controllers.Message;

public class NoticeControllerTest
{
    private MyDbContext _dbContext;
    private Mock<UserManager> _mockUserManager;

    private NoticeService _noticeService;

    [SetUp]
    public void SetUp()
    {
        var option = new DbContextOptionsBuilder<MyDbContext>()
            .UseInMemoryDatabase("order")
            .Options;
        var source = new DbContextFactorySource<MyDbContext>();
        _dbContext = source.Factory(null, option);
        _mockUserManager = Helper.MockUserManager();
        _noticeService = new NoticeService(_dbContext);
    }

    [Test]
    public async Task Index()
    {
        // setup
        var title = "title";
        Notice.States? state = null;
        
        var controller = new NoticeController(_noticeService, _mockUserManager.Object);
        
        // action

        var result = await controller.Index(new NoticeController.IndexQuery(title, state));
    
    }
    
    [Test]
    public async Task Add()
    {
        // setup
        var title = "title";
        Notice.States state = Notice.States.ON;
        Notice.Types type = Notice.Types.ALL;
        Notice.Sizes size = Notice.Sizes.BIG;
        
        var controller = new NoticeController(_noticeService, _mockUserManager.Object);
        // action

        var result = await controller.Add(new NoticeController.AddVm(null, title, "content", state, size, type));
    
    }
    
    [Test]
    public async Task Info()
    {
        // setup
        int id = 1;
        
        var controller = new NoticeController(_noticeService, _mockUserManager.Object);
        // action

        var result = await controller.Info(id);
    }
    
    [Test]
    public async Task NoticeList()
    {
        // setup

        var controller = new NoticeController(_noticeService, _mockUserManager.Object);
        // action

        var result = await controller.NoticeList();
    }
    
    [Test]
    public async Task LoginNoticeList()
    {
        // setup

        var controller = new NoticeController(_noticeService, _mockUserManager.Object);
        // action

        var result = await controller.LoginNoticeList();
    }
    
    [Test]
    public async Task Delete()
    {
        // setup
        var id = 1;

        var controller = new NoticeController(_noticeService, _mockUserManager.Object);
        // action

        var result = await controller.Delete(id);
    }
    
}