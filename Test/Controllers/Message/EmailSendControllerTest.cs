﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.Controllers.Message;
using ERP.DBContext;
using ERP.Interface;
using ERP.Services.Caches;
using ERP.Services.DB.Orders;
using ERP.Services.DB.Statistics;
using ERP.Services.Distribution;
using ERP.Services.Finance;
using ERP.Services.Identity;
using ERP.Services.Message;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using EmailConfig = ERP.Models.Setting.EmailConfig;

namespace Controllers.Message;

public class EmailSendControllerTest
{
    private WaybillService _waybillService;
    private MyDbContext _dbContext;
    private ISessionProvider _SessionProvider;
    private Mock<UserManager> _mockUserManager;

    private EmailInfoService _emailInfoService;
    private EmailMassService _emailMassService;
    private Order _amazonOrderService;
    private Statistic _statisticService;

    [SetUp]
    public void SetUp()
    {
        var option = new DbContextOptionsBuilder<MyDbContext>()
            .UseInMemoryDatabase("order")
            .Options;
        _dbContext = new MyDbContext(option);
        var _mockSessionProvider = new Mock<ISessionProvider>();
        _mockUserManager = Helper.MockUserManager();
        _emailInfoService = new EmailInfoService(_dbContext);
        _emailMassService = new EmailMassService(_dbContext);
        _statisticService = new Statistic(_dbContext, new FinancialAffairsService(_dbContext, _mockSessionProvider.Object));
        _amazonOrderService = new Order(_dbContext, _mockSessionProvider.Object, _statisticService);
    }

    [Test]
    public async Task MassImport()
    {
        // setup
        var id = 1;
        var type = 0;
        
        var controller = new EmailSendController(_emailInfoService, _amazonOrderService, _emailMassService, _mockUserManager.Object);
        
        // action
    
        var result = await controller.MassImport(new EmailSendController.MassImportVM() { Id = id, Type = type });
    
    }

    [Test]
    public async Task EmailSendIndex()
    {
        // setup
        var emailId = 0;
        var emailAddress = "address";

        var controller = new EmailSendController(_emailInfoService, _amazonOrderService, _emailMassService, _mockUserManager.Object);
        var emailConfigCacheMock = new Mock<EmailConfigCache>();
        emailConfigCacheMock.Setup(e => e.List()).Returns(new List<EmailConfig>());
        // action

        var result = await controller.EmailSendIndex(new EmailCollectionController.EmailCollectListQuery(emailId, emailAddress), emailConfigCacheMock.Object);
    }
}