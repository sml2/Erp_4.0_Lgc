﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.Controllers.Message;
using ERP.DBContext;
using ERP.Interface;
using ERP.Services.Caches;
using ERP.Services.Distribution;
using ERP.Services.Identity;
using ERP.Services.Message;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using EmailConfig = ERP.Models.Setting.EmailConfig;

namespace Controllers.Message;

public class EmailCollectionControllerTest
{
    private WaybillService _waybillService;
    private MyDbContext _dbContext;
    private ISessionProvider _SessionProvider;
    private Mock<UserManager> _mockUserManager;

    private EmailInfoService _emailInfoService;

    [SetUp]
    public void SetUp()
    {
        var option = new DbContextOptionsBuilder<MyDbContext>()
            .UseInMemoryDatabase("order")
            .Options;
        _dbContext = new MyDbContext(option);
        _mockUserManager = Helper.MockUserManager();
        _emailInfoService = new EmailInfoService(_dbContext);
    }

    [Test]
    public async Task EmailCollectList()
    {
        // setup
        var emailId = 0;
        var emailAddress = "address";

        var controller = new EmailCollectionController(_emailInfoService, _mockUserManager.Object);
        var emailConfigCacheMock = new Mock<EmailConfigCache>();
        emailConfigCacheMock.Setup(e => e.List()).Returns(new List<EmailConfig>());
        // action

        var result = await controller.EmailCollectList(new EmailCollectionController.EmailCollectListQuery(emailId, emailAddress), emailConfigCacheMock.Object);
    }
    
    [Test]
    public async Task EmailDel()
    {
        // setup
        var id = 0;

        var controller = new EmailCollectionController(_emailInfoService, _mockUserManager.Object);
        // action

        var result = await controller.EmailDel(id);
    }
    
    [Test]
    public async Task EmailInfoPage()
    {
        // setup
        var id = 0;

        var controller = new EmailCollectionController(_emailInfoService, _mockUserManager.Object);
        // action

        var result = await controller.EmailInfoPage(id);
    }
}