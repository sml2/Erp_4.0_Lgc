﻿using System.Threading.Tasks;
using ERP.Controllers.Message;
using ERP.DBContext;
using ERP.Models.Message;
using ERP.Services.Identity;
using ERP.Services.Message;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;

namespace Controllers.Message;

public class JobProgressControllerTest
{
    private MyDbContext _dbContext;
    private Mock<UserManager> _mockUserManager;

    private WorkOrderService _workOrderService;
    private WorkOrderProgressService _workOrderProgressService;

    [SetUp]
    public void SetUp()
    {
        var option = new DbContextOptionsBuilder<MyDbContext>()
            .UseInMemoryDatabase("order")
            .Options;
        _dbContext = new MyDbContext(option);
        _mockUserManager = Helper.MockUserManager();
        _workOrderService = new WorkOrderService(_dbContext);
        _workOrderProgressService = new WorkOrderProgressService(_dbContext);
    }

    [Test]
    public async Task ProgressList()
    {
        // setup
        var name = "name";
        var state = WorkOrderProgress.States.ENABLE;

        var controller = new JobProgressController(_workOrderService,_workOrderProgressService, _mockUserManager.Object);
        // action

        var result = await controller.ProgressList(new JobProgressController.ProgressListQuery(state, name));
    }
    
    [Test]
    public async Task ProgressAdd()
    {
        // setup
        var name = "name";

        var controller = new JobProgressController(_workOrderService,_workOrderProgressService, _mockUserManager.Object);
        // action

        var result = await controller.ProgressAdd(new JobProgressController.ProgressAddVm(name));
    }
    
    [Test]
    public async Task ProgressEdit()
    {
        // setup
        var name = "name";
        var state = WorkOrderProgress.States.ENABLE;
        var id = 1;

        var controller = new JobProgressController(_workOrderService,_workOrderProgressService, _mockUserManager.Object);
        // action

        var result = await controller.ProgressEdit(id, new JobProgressController.ProgressEditVm(name, state));
    }
    
    [Test]
    public async Task ProgressDel()
    {
        // setup
        var id = 1;

        var controller = new JobProgressController(_workOrderService,_workOrderProgressService, _mockUserManager.Object);
        // action

        var result = await controller.ProgressDel(id);
    }

}