﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.Controllers.Message;
using ERP.DBContext;
using ERP.Models.Message;
using ERP.Services.Identity;
using ERP.Services.Message;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Moq;
using NUnit.Framework;

namespace Controllers.Message;

public class FeedBackControllerTest
{
    private MyDbContext _dbContext;
    private Mock<UserManager> _mockUserManager;

    private FeedBackService _feedBackService;
    private FeedBackReplyService _feedBackReplyService;

    [SetUp]
    public void SetUp()
    {
        var option = new DbContextOptionsBuilder<MyDbContext>()
            .UseInMemoryDatabase("order")
            .Options;
        var source = new DbContextFactorySource<MyDbContext>();
        _dbContext = source.Factory(null, option);
        _mockUserManager = Helper.MockUserManager();
        _feedBackService = new FeedBackService(_dbContext);
        _feedBackReplyService = new FeedBackReplyService(_dbContext);
    }

    [Test]
    public async Task Index()
    {
        // setup
        var title = "title";
        FeedBack.States? states = null;
        FeedBack.Types? types = null;
        
        var controller = new FeedBackController(_feedBackService, _feedBackReplyService, _mockUserManager.Object);
        
        // action

        var result = await controller.Index(new FeedBackController.IndexQuery(title, types, states));
    
    }
    
    [Test]
    public async Task ProIndex()
    {
        // setup
        var title = "title";
        FeedBack.States? states = null;
        FeedBack.Types? types = null;
        
        var controller = new FeedBackController(_feedBackService, _feedBackReplyService, _mockUserManager.Object);
        // action
    
        var result = await controller.ProIndex(new FeedBackController.IndexQuery(title, types, states));
    
    }
    
    [Test]
    public async Task Add()
    {
        // setup
        var title = "title";
        var types = FeedBack.Types.WEB;
        var content = "content";
        var imgs = new List<string>();
        
        var controller = new FeedBackController(_feedBackService, _feedBackReplyService, _mockUserManager.Object);
        // action

        var result = await controller.Add(new FeedBackController.FeedbackVm(title, types, content, imgs));
    }
    
    [Test]
    public async Task ProInfo()
    {
        // setup
        var id = 1;
        var reply = "reply";
        var isComplete = false;
        var userId = 1;

        var controller = new FeedBackController(_feedBackService, _feedBackReplyService, _mockUserManager.Object);
        // action

        var result = await controller.Info(new FeedBackController.ProInfoVm(reply, isComplete, userId, id));
    }
    
    [Test]
    public async Task Complete()
    {
        // setup
        var id = 1;

        var controller = new FeedBackController(_feedBackService, _feedBackReplyService, _mockUserManager.Object);
        // action

        var result = await controller.Complete(id);
    }
    
    [Test]
    public async Task DelImage()
    {
        // setup
        var id = 0;

        var controller = new FeedBackController(_feedBackService, _feedBackReplyService, _mockUserManager.Object);
        // action

        var result = await controller.DelImage(id);
    }
    
    [Test]
    public async Task Withdraw()
    {
        // setup
        var id = 0;

        var controller = new FeedBackController(_feedBackService, _feedBackReplyService, _mockUserManager.Object);
        // action

        var result = await controller.Withdraw(id);
    }
    
    [Test]
    public async Task ClearImage()
    {
        // setup
        var imgs = new []{1};

        var controller = new FeedBackController(_feedBackService, _feedBackReplyService, _mockUserManager.Object);
        // action

        var result = await controller.ClearImage(imgs);
    }
    
    [Test]
    public async Task ProDel()
    {
        // setup
        var id = 0;

        var controller = new FeedBackController(_feedBackService, _feedBackReplyService, _mockUserManager.Object);
        // action

        var result = await controller.ProDel(id);
    }
}