﻿using System.Threading.Tasks;
using ERP.Controllers.Message;
using ERP.DBContext;
using ERP.Interface;
using ERP.Services.Distribution;
using ERP.Services.Identity;
using ERP.Services.Message;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;

namespace Controllers.Message;

public class EmailMassControllerTest
{
    private WaybillService _waybillService;
    private MyDbContext _dbContext;
    private ISessionProvider _SessionProvider;
    private Mock<UserManager> _mockUserManager;

    private EmailMassService _emailMassService;

    [SetUp]
    public void SetUp()
    {
        var option = new DbContextOptionsBuilder<MyDbContext>()
            .UseInMemoryDatabase("order")
            .Options;
        _dbContext = new MyDbContext(option);
        _mockUserManager = Helper.MockUserManager();
        _emailMassService = new EmailMassService(_dbContext);
    }


    [Test]
    public async Task EmailMassList()
    {
        // setup
        var postbox = "postbox";

        var controller = new EmailMassController(_emailMassService, _mockUserManager.Object);
        // action

        var result = await controller.EmailMassList(new (postbox));
    }
    
    [Test]
    public async Task MassAdd()
    {
        // setup
        var postbox = "postbox";
        var id = 1;
        var nick = "nick";

        var controller = new EmailMassController(_emailMassService, _mockUserManager.Object);
        // action

        var result = await controller.MassAdd(new EmailMassController.EmailMassVm(postbox, nick, id));
    }
    
    [Test]
    public async Task MassInfo()
    {
        // setup
        var id = 1;

        var controller = new EmailMassController(_emailMassService, _mockUserManager.Object);
        // action

        var result = await controller.MassInfo(id);
    }
    
    [Test]
    public async Task MassDel()
    {
        // setup
        var id = 1;

        var controller = new EmailMassController(_emailMassService, _mockUserManager.Object);
        // action

        var result = await controller.MassDel(id);
    }
}