﻿using System;
using System.Threading.Tasks;
using ERP.Controllers.Message;
using ERP.DBContext;
using ERP.Models.Message;
using ERP.Services.Identity;
using ERP.Services.Message;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;

namespace Controllers.Message;

public class JobMyControllerTest
{
    private MyDbContext _dbContext;
    private Mock<UserManager> _mockUserManager;

    private WorkOrderService _workOrderService;
    private WorkOrderCategoryService _workOrderCategoryService;
    private WorkOrderProgressService _workOrderProgressService;

    [SetUp]
    public void SetUp()
    {
        var option = new DbContextOptionsBuilder<MyDbContext>()
            .UseInMemoryDatabase("order")
            .Options;
        _dbContext = new MyDbContext(option);
        _mockUserManager = Helper.MockUserManager();
        _workOrderService = new WorkOrderService(_dbContext);
        _workOrderCategoryService = new WorkOrderCategoryService(_dbContext);
        _workOrderProgressService = new WorkOrderProgressService(_dbContext);
    }

    [Test]
    public async Task WorkOrderList()
    {
        // setup
        var title = "title";
        var categoryId = 1;
        var progressId = 1;
        var state = WorkOrder.States.ToBeProcessed;
        WorkOrder.Commits? commit = WorkOrder.Commits.HIGHERCOMPANY;

        var controller = new JobMyController(_workOrderService,_workOrderCategoryService,_workOrderProgressService, _mockUserManager.Object);
        // action

        var result = await controller.WorkOrderList(new JobMyController.WorkOrderListQuery(title, categoryId, progressId, state, commit));
    }
    
    [Test]
    public async Task WorkOrderAdd()
    {
        // setup
        var content = "content";
        var progressId = 1;
        var categoryId = 1;
        var title = "title";
        var deadline = DateTime.Now;

        var controller = new JobMyController(_workOrderService,_workOrderCategoryService,_workOrderProgressService, _mockUserManager.Object);
        // action

        var result = await controller.WorkOrderAdd(new JobMyController.WorkOrderAddVm(deadline, title, categoryId, progressId, content));
    }
    
    [Test]
    public async Task WorkOrderWithdrawn()
    {
        // setup
        var id = 1;

        var controller = new JobMyController(_workOrderService,_workOrderCategoryService,_workOrderProgressService, _mockUserManager.Object);
        // action

        var result = await controller.WorkOrderWithdrawn(id);
    }

}