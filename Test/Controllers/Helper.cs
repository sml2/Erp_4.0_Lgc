﻿using ERP.Models.DB.Users;
using ERP.Services.Identity;
using Microsoft.AspNetCore.Identity;
using Moq;

namespace Controllers;

public static class Helper
{
    public static Mock<UserManager> MockUserManager()
    {
        var store = new Mock<IUserStore<User>>();
        var mgr = new Mock<UserManager>(store.Object, null, null, null, null, null, null, null, null);
        mgr.Object.UserValidators.Add(new UserValidator<User>());
        mgr.Object.PasswordValidators.Add(new PasswordValidator<User>());

        mgr.Setup(x => x.GetUserAsync(null)).ReturnsAsync(FakeUser());

        return mgr;
    }

    public static User FakeUser()
    {
        return new User() 
        {
            Id = 1,
            CompanyID = 1,
            OEMID = 1,
            Company = FakeCompany()
        };
    }
    
    public static Company FakeCompany()
    {
        return new () 
        {
            ID = 1,
            ReportId = 1,
            OEMID = 1,
        };
    }
}