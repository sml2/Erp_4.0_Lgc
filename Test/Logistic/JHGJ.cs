using ERP.Models.Api.Logistics;
using ERP.Services.Api.Logistics;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Abstractions;

namespace TestERP4._0
{
    [TestFixture]
    public class K5 : InjectTest
    {
        static ILoggerFactory loggerFactory = NullLoggerFactory.Instance;
        public static ILoggerFactory LoggerFactory { get => loggerFactory; }


        //private ERP.Services.Api.Logistics.Detail.JHGJ _JHGJ;

        private ERP.Services.Api.Logistics.Detail.JHGJ _JHGJ;


        string parameter = "{'Token': 'eXd6Zmdqc2Vfcb1I5J0Uyr47lEbx', 'Domain': 'ywzfgj.kingtrans.cn', 'Clientid': 'YZ-641'}";



        ERP.Enums.Platforms logName = ERP.Enums.Platforms.K5_JHGJ;
        [SetUp]
        public void Setup()
        {
            _JHGJ = Resolve<ERP.Services.Api.Logistics.Detail.JHGJ>();
        }

        [Test]
        public void methodDefineds()
        {
            BaseParameter baseParameter = new BaseParameter()
            {
                LogName = ERP.Enums.Platforms.K5_JHGJ,
                Parameter = parameter,
            };
            var result = _JHGJ.methodDefineds(baseParameter);
            Assert.IsNotNull(result);
        }

        [Test]
        public async Task TestGetShipTypes()
        {
            ShipTypeParameter shipTypeParameter = new ShipTypeParameter()
            {
                LogName = logName,
                Parameter = parameter,
            };
            var results = await _JHGJ.GetShipTypes(shipTypeParameter);
            Assert.IsNotNull(results.CommonResults);
            Assert.IsTrue(results.Success);
        }

        [Test]
        public async Task LoadChannelCode()
        {
            BaseParameter shipTypeParameter = new BaseParameter()
            {
                LogName = logName,
                Parameter = parameter,             
            };
            var results = await _JHGJ.LoadChannelCode(shipTypeParameter);
            Assert.IsNotNull(results.CommonResults);
            string s = Newtonsoft.Json.JsonConvert.SerializeObject(results.CommonResults);
            Assert.IsTrue(results.Success);
        }



        [Test]
        public async Task TestLoadCbFileType()
        {
            FileTypeParameter shipTypeParameter = new FileTypeParameter()
            {
                LogName = logName,
                Parameter = parameter,
                code= "EUD10",
            };
            var results = await _JHGJ.LoadCbFileType(shipTypeParameter);
            Assert.IsNotNull(results.CommonResults);
            string s = Newtonsoft.Json.JsonConvert.SerializeObject(results.CommonResults);
            Assert.IsTrue(results.Success);
        }


        [Test]
        public async Task TestTrack()
        {
            List<TrackOrderInfo> trackOrderInfos = new List<TrackOrderInfo>()
            {
                new TrackOrderInfo(){
                    CustomNumber ="250-6236454-5197451",
                    LogictisNumber= "ZF3021838932",
                    TrackNumber="ZF3021838932",
                    Price=0,
                    Unit="",},
            };
            TrackParameter trackParameter = new TrackParameter()
            {
                LogName = logName,
                Parameter = parameter,
               TrackOrderInfoLst=trackOrderInfos,
            };
            var results = await _JHGJ.Track(trackParameter);
            Assert.IsNotNull(results.TrackResults);
        }


        [Test]
        public async Task TestPrint()
        {
            PrintParam printParam = new PrintParam() { CustomNumber = "", LogictisNumber = "", TrackNumber = "" };
            MultiPrint multiPrint = new MultiPrint()
            {
                LogName = logName,
                Parameter = parameter,
                ContentType = "And1And2",
                FileType = "欧洲泡货P线10*15",
               printParams=new List<PrintParam>() { printParam },
            };
            var results = await _JHGJ.Print(multiPrint);
            Assert.That(results, Is.True);
        }



        [Test]
        public async Task TestSend()
        {
            string ext = (@"{'ProNameCn':'挂饰','ProNameEn':'Ornaments','Weight':0.5,'Length':30,'Width':10,
'Height':20,'HasCode':'23456','Price':10,'taxType':'iossCode','VatNumber':'454545','ShipMethod':'EUD10'}");
            SenderInfo sender = new SenderInfo()
            {
                Name = "米境通",
                Mail = "xiaomiwaimao@qq.com",
                PostalCode = "210012",
                PhoneNumber = "13218051893",
                Region = "雨花台区",
                Province = "江苏省",
                City = "南京市",
                Address = "软件大道华博智慧园4F天联网络",
                Company = null,
            };

            ReceiverInfo receiverAddress = new ReceiverInfo()
            {
                Name = "Janette Bastyr",
                Phone = "",
                NationId = 46,
                NationShort = "DE",
                Nation = "德国",
                Province = "",
                City = "Faßberg",
                District = "aabb",
                Address = "",
                Address2 = "Heideweg 44",
                Address3 = "",
                Zip = "29328",
                Email = "s72zbb0470998yq@marketplace.amazon.de",
            };

            GoodInfo good = new GoodInfo()
            {
                goodId = "B07N7VKGST",
                Name = "zhongjiany 22,9 cm Drache Plüschpuppe Super Fire Dragon Plüsch Tier gefüllte Puppe Spielzeug für Kinder (blau)",
                Asin = "B07N7VKGST",
                Num = 1,
                Unit = "EUR",
                Price = 18,
                Total = 23,
                Delivery = 0,
                SendNum = 1,
                Count = 1,
                Url = "https://m.media-amazon.com/images/I/517Arqs7EhL._SL75_.jpg",
                Sku = "DE-SP-69",
            };
            List<GoodInfo> goodInfos = new List<GoodInfo>();
            goodInfos.Add(good);

            OrderInfo order = new OrderInfo()
            {
                ext = ext,
                id = 312617,
                Url = "https://m.media-amazon.com/images/I/517Arqs7EhL._SL75_.jpg",
                OrderNo = "306-0479350-5531550",
                ReceiverAddress = receiverAddress,
                SendNum = 1,
                FailNum = 0,
                GoodInfos = goodInfos,
            };

            List<OrderInfo> orderInfos = new List<OrderInfo>();
            orderInfos.Add(order);

            SendParameter sendParameter = new SendParameter()
            {
                LogName = logName,
                Parameter = parameter,
                SenderInfo = sender,
                OrderInfos = orderInfos,
            };
            var results = await _JHGJ.Send(sendParameter);
            Assert.IsNotNull(results.ReportResults);

        }
    }
}