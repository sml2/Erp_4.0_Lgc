using ERP.Models.Api.Logistics;
using ERP.Services.Api.Logistics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using Newtonsoft.Json;
using ERP.Models.Api.Logistics.SDGJ;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Abstractions;

using static ERP.Models.Api.Logistics.SDGJ.GetEstimatePriceResponse;

namespace TestERP4._0
{
    public class SDGJ : InjectTest
    {
        static ILoggerFactory loggerFactory = NullLoggerFactory.Instance;
        public static ILoggerFactory LoggerFactory { get => loggerFactory; }

        private ERP.Services.Api.Logistics.Detail.SDGJ _SDGJ;

        string TestParam = "{'appKey': 'ace56aeaee5077d77292b15dcdf3dc8dace56aeaee5077d77292b15dcdf3dc8d', 'appToken': 'b34d68ecc4628f9bdb64ad515ffddaa0'}";


        //Parameter parameter = new Parameter()
        //{
        //    appKey = "952430766530c2b7502d9b6633d2c621",
        //    appToken = "b9371bf7092b43cdb747e175f067cf20b9371bf7092b43cdb747e175f067cf20",
        //};

        //{"appKey": "178b13e3f965c7cf0dbdca75872ac66b178b13e3f965c7cf0dbdca75872ac66b", "OrderUrl": "", "Password": "", "PrintUrl": "", "Username": "XUELEILEI", "appToken": "c7cd68a1959e0deb36bef381f57109d8"}


        Parameter parameter = new Parameter()
        {
            appKey = "178b13e3f965c7cf0dbdca75872ac66b178b13e3f965c7cf0dbdca75872ac66b",
            appToken = "c7cd68a1959e0deb36bef381f57109d8",
        };



        //string logName = "SDGJ";

        ERP.Enums.Platforms logName = ERP.Enums.Platforms.RTB_SDGJ;
        [SetUp]
        public void Setup()
        {
            _SDGJ = Resolve<ERP.Services.Api.Logistics.Detail.SDGJ>();
        }


        [Test]
        public void methodDefineds()
        {
            BaseParameter baseParameter = new BaseParameter()
            {
                LogName = ERP.Enums.Platforms.RTB_SDGJ,
                Parameter = JsonConvert.SerializeObject(parameter),
            };
            var result = _SDGJ.methodDefineds(baseParameter);
            Assert.IsNotNull(result);
        }


        [Test]
        public async Task GetFee()
        {
            int waybillid = 0;
            string CustomNumber = "701-6095759-2165014-3";
            string trackNumber = "4006318298739635";
            var results = await _SDGJ.GetFeeInfo(CustomNumber,"", trackNumber, parameter,"SDGJ", waybillid);
            Assert.IsNotNull(results);
        }

        [Test]
        public async Task Track()
        {
            List<TrackOrderInfo> trackOrderInfos = new List<TrackOrderInfo>()
            {
                new TrackOrderInfo(){
                    CustomNumber ="701-6095759-2165014-3",
                    LogictisNumber= "",
                    TrackNumber="",
                    Price=0,
                    Unit="",},
            };

            List<string> OrderNums = new List<string>();
            OrderNums.Add("");
            TrackParameter trackParameter = new TrackParameter()
            {
                LogName = logName,
                Parameter = JsonConvert.SerializeObject(parameter),
              TrackOrderInfoLst =trackOrderInfos,
            };

            var results = await _SDGJ.Track(trackParameter);
            Assert.IsNotNull(results);
        }

        [Test]
        public async Task GetTrackNumber()
        {
            string CustomOrderID = "701-6095759-2165014-3";
            var results = await _SDGJ.GetTrackNumber(CustomOrderID, parameter,"SDGJ");
            Assert.IsNull(results.Item1);
            Assert.IsNotNull(results.Item2);
        }



        [Test]
        public async Task TestGetShipTypes()
        {
            ShipTypeParameter shipTypeParameter = new ShipTypeParameter()
            {
                LogName = logName,
                Parameter = TestParam,
            };
            var results = await _SDGJ.GetShipTypes(shipTypeParameter);
            Assert.IsNotNull(results.CommonResults);
            Assert.IsTrue(results.Success);
        }

        [Test]
        public async Task TestBill()
        {
            BillParameter shipTypeParameter = new BillParameter()
            {                         
                waybillID=1,
                Parameter = TestParam,                
                LogName= logName,
                CustomNumber = "",
                BeginDate = "2023-06-01 00:00:00",
                EndDate = "2023-06-30 23:59:59",
            };
            await _SDGJ.LogisticBilling(shipTypeParameter);           
        }

        [Test]
        public async Task PrintLabel()
        {
            PrintParam printParam = new PrintParam() { CustomNumber = "206-6532196-4831559", LogictisNumber = "206-6532196-4831559", TrackNumber = "" };            
            MultiPrint multiPrint = new MultiPrint()
            {
                LogName = logName,
                Parameter = JsonConvert.SerializeObject(parameter),
                ContentType = "0",
                FileType = "0",
                printParams=new List<PrintParam>() { printParam },
            };
            string json = JsonConvert.SerializeObject(multiPrint);
            var results = await _SDGJ.Print(multiPrint);
            Assert.That(results, Is.True);
        }


        public string ext = (@"{'ApplicationNameCn':'挂饰','ApplicationName':'Ornaments','Weight':0.5,'Length':30,'Width':10,
'Height':20,'HsCode':'23456','Declare':10,'IsEnableFiling':true,'IOSS':'454545','ShipMethod':'AEZX'}");


        public async Task EstimatePrice()
        {
            PriceParameter priceParameter = new PriceParameter()
            {
                LogName = logName,
                Parameter = JsonConvert.SerializeObject(parameter),
                ext = ext,
            };
            throw new ERP.Exceptions.Developers.Merges.Merge_79efd94dff00cbdcd6225b476ce28630d3d45017();
            var results = await _SDGJ.EstimatePrice(priceParameter);
            Assert.That(results, Is.True);
        }




        [Test]
        public async Task TestSend()
        {

            SenderInfo sender = new SenderInfo()
            {
                Name = "米境通",
                Mail = "xiaomiwaimao@qq.com",
                PostalCode = "210012",
                PhoneNumber = "13218051893",
                Region = "雨花台区",
                Province = "江苏省",
                City = "南京市",
                Address = "软件大道华博智慧园4F天联网络",
                Company = null,
            };

            ReceiverInfo receiverAddress = new ReceiverInfo()
            {
                Name = "Janette Bastyr",
                Phone = "",
                NationId = 46,
                NationShort = "DE",
                Nation = "德国",
                Province = "",
                City = "Faßberg",
                District = "",
                Address = "",
                Address2 = "Heideweg 44",
                Address3 = "",
                Zip = "29328",
                Email = "s72zbb0470998yq@marketplace.amazon.de",
            };


            GoodInfo good = new GoodInfo()
            {
                goodId = "B07N7VKGST",
                Name = "zhongjiany 22,9 cm Drache Plüschpuppe Super Fire Dragon Plüsch Tier gefüllte Puppe Spielzeug für Kinder (blau)",
                Asin = "B07N7VKGST",
                Num = 1,
                Unit = "EUR",
                Price = 18,
                Total = 23,
                Delivery = 0,
                SendNum = 1,
                Count = 1,
                Url = "https://m.media-amazon.com/images/I/517Arqs7EhL._SL75_.jpg",
                Sku = "DE-SP-69",
            };
            List<GoodInfo> goodInfos = new List<GoodInfo>();
            goodInfos.Add(good);


            OrderInfo order = new OrderInfo()
            {
                ext = ext,
                id = 312617,
                Url = "https://m.media-amazon.com/images/I/517Arqs7EhL._SL75_.jpg",
                OrderNo = "306-0479350-5531550",
                ReceiverAddress = receiverAddress,
                SendNum = 1,
                FailNum = 0,
                GoodInfos = goodInfos,
            };

            List<OrderInfo> orderInfos = new List<OrderInfo>();
            orderInfos.Add(order);

            SendParameter sendParameter = new SendParameter()
            {
                LogName = logName,
                Parameter = JsonConvert.SerializeObject(parameter),
                SenderInfo = sender,
                OrderInfos = orderInfos,
            };

            var results = await _SDGJ.Send(sendParameter);
            Assert.IsNotNull(results.ReportResults);
            //Assert.That(results.Success, Is.True);
        }
    }
}