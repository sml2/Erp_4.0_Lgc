using ERP.Models.Api.Logistics;
using ERP.Services.Api.Logistics;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Abstractions;

namespace TestERP4._0
{
    [TestFixture]
    public class Y7F : InjectTest
    {
        static ILoggerFactory loggerFactory = NullLoggerFactory.Instance;
        public static ILoggerFactory LoggerFactory { get => loggerFactory; }

        private ERP.Services.Api.Logistics.Detail.Y7F _Y7F;

        string parameter = "{'apiName': '佐川急变', 'apiToken': '佐川急变'}";  

        ERP.Enums.Platforms logName = ERP.Enums.Platforms.Y7F;
        [SetUp]
        public void Setup()
        {
            _Y7F = Resolve<ERP.Services.Api.Logistics.Detail.Y7F>();
        }


        [Test]
        public void methodDefineds()
        {
             BaseParameter baseParameter = new BaseParameter()
              {
                  LogName = ERP.Enums.Platforms.Y7F,
                  Parameter = parameter,
              };
            var result = _Y7F.methodDefineds(baseParameter);
            Assert.IsNotNull(result);
        }

        [Test]
        public async Task TestGetShipTypes()
        {
            ShipTypeParameter shipTypeParameter = new ShipTypeParameter()
            {
                LogName = logName,
                Parameter = parameter,
            };
            var results = await _Y7F.GetShipTypes(shipTypeParameter);
            Assert.IsNotNull(results.CommonResults);
            Assert.IsTrue(results.Success);
        }


        [Test]
        public async Task TestTrack()
        {
            List<TrackOrderInfo> trackOrderInfos = new List<TrackOrderInfo>()
            {
                new TrackOrderInfo(){
                    CustomNumber ="",
                    LogictisNumber= "",
                    TrackNumber="",
                    Price=0,
                    Unit="",},
            };
            TrackParameter trackParameter = new TrackParameter()
            {
                LogName = logName,
                Parameter = parameter,
               TrackOrderInfoLst=trackOrderInfos,
            };
            var results = await _Y7F.Track(trackParameter);
            Assert.IsNotNull(results.TrackResults);
        }

        [Test]
        public async Task TestPrint()
        {
            PrintParam printParam = new PrintParam() { CustomNumber = "", LogictisNumber = "", TrackNumber = "" };
            MultiPrint multiPrint = new MultiPrint()
            {
                LogName = logName,
                Parameter = parameter,
                ContentType = "label10x10_1",
                FileType = "png",
               printParams=new List<PrintParam>() { printParam},
            };
            string json = JsonConvert.SerializeObject(multiPrint);
            var results = await _Y7F.Print(multiPrint);
            Assert.That(results, Is.True);
        }



        [Test]
        public async Task TestSend()
        {
            string ext = (@"{'name':'挂饰','nameEn':'Ornaments','boxWeight':0.5,'boxLength':30,'boxWidth':10,
'boxHeight':20,'HasCode':'23456','reportPrice':10,'boxCount':'1','ShipMethodName':'日本标准小包-普货','ShipMethod':'JPA11A-SH'}");
            SenderInfo sender = new SenderInfo()
            {
                Name = "米境通",
                Mail = "xiaomiwaimao@qq.com",
                PostalCode = "210012",
                PhoneNumber = "13218051893",
                Region = "雨花台区",
                Province = "江苏省",
                City = "南京市",
                Address = "软件大道华博智慧园4F天联网络",
                Company = "",
            };

            ReceiverInfo receiverAddress = new ReceiverInfo()
            {
                Name = "Janette Bastyr",
                Phone = "",
                NationId = 46,
                NationShort = "DE",
                Nation = "德国",
                Province = "",
                City = "Faßberg",
                District = "aabb",
                Address = "",
                Address2 = "Heideweg 44",
                Address3 = "",
                Zip = "29328",
                Email = "s72zbb0470998yq@marketplace.amazon.de",
            };

            GoodInfo good = new GoodInfo()
            {
                goodId = "B07N7VKGST",
                Name = "zhongjiany 22,9 cm Drache Plüschpuppe Super Fire Dragon Plüsch Tier gefüllte Puppe Spielzeug für Kinder (blau)",
                Asin = "B07N7VKGST",
                Num = 1,
                Unit = "EUR",
                Price = 18,
                Total = 23,
                Delivery = 0,
                SendNum = 1,
                Count = 1,
                Url = "https://m.media-amazon.com/images/I/517Arqs7EhL._SL75_.jpg",
                Sku = "DE-SP-69",
            };
            List<GoodInfo> goodInfos = new List<GoodInfo>();
            goodInfos.Add(good);

            OrderInfo order = new OrderInfo()
            {
                ext = ext,
                id = 312617,
                Url = "https://m.media-amazon.com/images/I/517Arqs7EhL._SL75_.jpg",
                OrderNo = "306-0479350-5531550",
                ReceiverAddress = receiverAddress,
                SendNum = 1,
                FailNum = 0,
                GoodInfos = goodInfos,
            };

            List<OrderInfo> orderInfos = new List<OrderInfo>();
            orderInfos.Add(order);

            SendParameter sendParameter = new SendParameter()
            {
                LogName = logName,
                Parameter = parameter,
                SenderInfo = sender,
                OrderInfos = orderInfos,
            };

            var results = await _Y7F.Send(sendParameter);
            Assert.IsNotNull(results.ReportResults);
            Assert.That(results, Is.True);
        }
    }
}