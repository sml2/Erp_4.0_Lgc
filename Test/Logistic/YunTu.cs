using ERP.Models.Api.Logistics;
using ERP.Services.Api.Logistics;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Abstractions;
using AmazonService = ERP.Services.DB.Orders.Order;
using ERP;
using System.IO;
using System;

namespace TestERP4._0
{
    [TestFixture]
    public class YunTu : InjectTest
    {
        private AmazonService amazonOrderService;
        static ILoggerFactory loggerFactory = NullLoggerFactory.Instance;
        public static ILoggerFactory LoggerFactory { get => loggerFactory; }

        private ERP.Services.Api.Logistics.Detail.YunTu _YunTu;
        string parameter = "{'user': 'CN3622633', 'secret_key': 'B93+/+LVH158I2KYzbrZvg=='}";


        string TestParameter = "{'user': 'C16475', 'secret_key': '9Mha0MXVPKg='}";
        

        ERP.Enums.Platforms logName = ERP.Enums.Platforms.YunTu;

        [SetUp]
        public void Setup()
        {
            _YunTu = Resolve<ERP.Services.Api.Logistics.Detail.YunTu>();
        }


        [Test]
        public void methodDefineds()
        {
            BaseParameter baseParameter = new BaseParameter()
            {
                LogName = ERP.Enums.Platforms.YunTu,
                Parameter = parameter,
            };
            var result = _YunTu.methodDefineds(baseParameter);
            Assert.IsNotNull(result);
        }

        [Test]
        public async Task TestGetShipTypes()
        {
            ShipTypeParameter shipTypeParameter = new ShipTypeParameter()
            {
                LogName = logName,
                Parameter = parameter,
                CountryCode = "EN",
            };
            var results = await _YunTu.GetShipTypes(shipTypeParameter);
            Assert.IsNotNull(results.CommonResults);
            Assert.IsTrue(results.Success);
        }


        [Test]
        public async Task TestTrack()
        {
           
            List<TrackOrderInfo> trackOrderInfos = new List<TrackOrderInfo>()
            {
                new TrackOrderInfo(){
                    CustomNumber ="408-8153438-5641160",
                    LogictisNumber= "YT2316721272041014",
                    TrackNumber="Q1010167872368700",
                    Price=0.0,
                    Unit=null,},
            };


            TrackParameter trackParameter = new TrackParameter()
            {
                LogName = logName,
                Parameter = parameter,
               TrackOrderInfoLst=trackOrderInfos,
            };
            var results = await _YunTu.Track(trackParameter);
            Assert.IsNotNull(results.TrackResults);
        }


        [Test]
        public async Task TestPrint()
        {
            PrintParam printParam = new PrintParam() { CustomNumber = "", LogictisNumber = "", TrackNumber = "" };
            MultiPrint multiPrint = new MultiPrint()
            {
                LogName = logName,
                Parameter = parameter,
                printParams = new List<PrintParam>() { printParam },
            };
            //var results = await _YunTu.Print(multiPrint);           
            var stream = await _YunTu.Print(multiPrint);
            Assert.IsNotNull(stream);
        }



        [Test]
        public async Task TestSend()
        {
            string ext = (@"{'CName':'挂饰','EName':'Ornaments','HsCode':'23456','IossTag':true,'Width':5,'Length':30,'Weight':10,
'Height':20,'Declare':10,'Unit':'USD','IossCode':'454545','RecipientTaxNumber':'232323','ShipMethod':'PK0052'}");
            SenderInfo sender = new SenderInfo()
            {
                Name = "米境通",
                Mail = "xiaomiwaimao@qq.com",
                PostalCode = "210012",
                PhoneNumber = "13218051893",
                Region = "雨花台区",
                Province = "江苏省",
                City = "南京市",
                Address = "软件大道华博智慧园4F天联网络",
                Company = "",
            };
            ReceiverInfo receiverAddress = new ReceiverInfo()
            {
                Name = "Janette Bastyr",
                Phone = "",
                NationId = 46,
                NationShort = "DE",
                Nation = "德国",
                Province = "",
                City = "Faßberg",
                District = "aabb",
                Address = "",
                Address2 = "Heideweg 44",
                Address3 = "",
                Zip = "29328",
                Email = "s72zbb0470998yq@marketplace.amazon.de",
            };
            GoodInfo good = new GoodInfo()
            {
                goodId = "B07N7VKGST",
                Name = "zhongjiany 22,9 cm Drache Plüschpuppe Super Fire Dragon Plüsch Tier gefüllte Puppe Spielzeug für Kinder (blau)",
                Asin = "B07N7VKGST",
                Num = 1,
                Unit = "EUR",
                Price = 18,
                Total = 23,
                Delivery = 0,
                SendNum = 1,
                Count = 1,
                Url = "https://m.media-amazon.com/images/I/517Arqs7EhL._SL75_.jpg",
                Sku = "DE-SP-69",
            };
            List<GoodInfo> goodInfos = new List<GoodInfo>();
            goodInfos.Add(good);
            OrderInfo order = new OrderInfo()
            {
                ext = ext,
                id = 312617,
                Url = "https://m.media-amazon.com/images/I/517Arqs7EhL._SL75_.jpg",
                OrderNo = "406-0479350-5531554",  //306-0479350-5531550-1
                ReceiverAddress = receiverAddress,
                SendNum = 1,
                FailNum = 0,
                GoodInfos = goodInfos,
            };
            List<OrderInfo> orderInfos = new List<OrderInfo>();
            orderInfos.Add(order);
            SendParameter sendParameter = new SendParameter()
            {
                LogName = logName,
                Parameter = parameter,
                SenderInfo = sender,
                OrderInfos = orderInfos,
            };
            var results = await _YunTu.Send(sendParameter);
            Assert.IsNotNull(results.ReportResults);
            Assert.That(results, Is.True);
        }
    }
}