using ERP.Models.Api.Logistics;
using ERP.Services.Api.Logistics;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Abstractions;
using ERP.Models.Api.Logistics.HL;

namespace TestERP4._0
{
    [TestFixture]
    public class HL : InjectTest
    {
        static ILoggerFactory loggerFactory = NullLoggerFactory.Instance;
        public static ILoggerFactory LoggerFactory { get => loggerFactory; }

        private ERP.Services.Api.Logistics.Detail.HuaLei_YH _HuaLei;

        //沙箱测试账户
        Parameter parameter = new Parameter()
        {
            Username = "soko",
            Password = "Zl123321",
            OrderUrl = "http://172.81.251.240:8082",
            PrintUrl= "http://119.91.133.111:8089"
        };

        string param = "{'username':'LS188','password':'123456','OrderUrl':'http://sz56t.com'}";
        ERP.Enums.Platforms logName = ERP.Enums.Platforms.HuaLei_YH;
        [SetUp]
        public void Setup()
        {
            _HuaLei = Resolve<ERP.Services.Api.Logistics.Detail.HuaLei_YH>();
        }


        [Test]
        public void methodDefineds()
        {
            BaseParameter baseParameter = new BaseParameter()
            {
                LogName = ERP.Enums.Platforms.HuaLei_YH,
                Parameter = param,
            };
            var result = _HuaLei.methodDefineds(baseParameter);
            Assert.IsNotNull(result);
        }


        [Test]
        public async Task TestGetShipTypes()
        {
            ShipTypeParameter shipTypeParameter = new ShipTypeParameter()
            {
                LogName = logName,
                Parameter =JsonConvert.SerializeObject(parameter),
            };
            var results = await _HuaLei.GetShipTypes(shipTypeParameter);
            Assert.IsNotNull(results.CommonResults);
            Assert.IsTrue(results.Success);
        }

        [Test]
        public async Task TestLoadCbFileType()
        {
            FileTypeParameter shipTypeParameter = new FileTypeParameter()
            {
                LogName = logName,
                Parameter = param,
            };
            var results = await _HuaLei.LoadCbContentType(shipTypeParameter);
            Assert.IsNotNull(results.CommonResults);
            string s = Newtonsoft.Json.JsonConvert.SerializeObject(results.CommonResults);
            Assert.IsTrue(results.Success);
        }


        //k5 logNumber:MAXCN2032967793YQ

        [Test]
        public async Task TestSend()
        {
            string ext = (@"{'ProNameCn':'挂饰','ProNameEn':'Ornaments','weight':0.5,'length':30,'width':10,
'height':20,'HasCode':'23456','invoice_amount':10,'BoxCount':1,'shipper_taxnotype':'IOSS','shipper_taxno':'454545','ShipType':2,'ShipMethod':'FEDEX'}");
            SenderInfo sender = new SenderInfo()
            {
                Name = "米境通",
                Mail = "xiaomiwaimao@qq.com",
                PostalCode = "210012",
                PhoneNumber = "13218051893",
                Region = "雨花台区",
                Province = "江苏省",
                City = "南京市",
                Address = "软件大道华博智慧园4F天联网络",
                Company = "",
            };

            ReceiverInfo receiverAddress = new ReceiverInfo()
            {
                Name = "Janette Bastyr",
                Phone = "",
                NationId = 46,
                NationShort = "DE",
                Nation = "德国",
                Province = "",
                City = "Faßberg",
                District = "aabb",
                Address = "",
                Address2 = "Heideweg 44",
                Address3 = "",
                Zip = "29328",
                Email = "s72zbb0470998yq@marketplace.amazon.de",
            };

            GoodInfo good = new GoodInfo()
            {
                goodId = "B07N7VKGST",
                Name = "zhongjiany 22,9 cm Drache Plüschpuppe Super Fire Dragon Plüsch Tier gefüllte Puppe Spielzeug für Kinder (blau)",
                Asin = "B07N7VKGST",
                Num = 1,
                Unit = "EUR",
                Price = 18,
                Total = 23,
                Delivery = 0,
                SendNum = 1,
                Count = 1,
                Url = "https://m.media-amazon.com/images/I/517Arqs7EhL._SL75_.jpg",
                Sku = "DE-SP-69",
            };
            List<GoodInfo> goodInfos = new List<GoodInfo>();
            goodInfos.Add(good);

            OrderInfo order = new OrderInfo()
            {
                ext = ext,
                id = 312617,
                Url = "https://m.media-amazon.com/images/I/517Arqs7EhL._SL75_.jpg",
                OrderNo = "306-0479350-5531550",
                ReceiverAddress = receiverAddress,
                SendNum = 1,
                FailNum = 0,
                GoodInfos = goodInfos,
            };

            List<OrderInfo> orderInfos = new List<OrderInfo>();
            orderInfos.Add(order);

            SendParameter sendParameter = new SendParameter()
            {
                LogName = logName,
                Parameter = JsonConvert.SerializeObject(parameter),
                SenderInfo = sender,
                OrderInfos = orderInfos,
            };
            string js = JsonConvert.SerializeObject(sendParameter);
            var results = await _HuaLei.Send(sendParameter);
            Assert.IsNotNull(results.ReportResults);
        }


        [Test]
        public async Task Track()
        {
            List<TrackOrderInfo> trackOrderInfos = new List<TrackOrderInfo>()
            {
                new TrackOrderInfo(){
                    CustomNumber ="",
                    LogictisNumber= "",
                    TrackNumber="",
                    Price=0,
                    Unit="",},
            };
            TrackParameter trackParameter = new TrackParameter()
            {
                LogName = logName,
                Parameter = JsonConvert.SerializeObject(parameter),
               TrackOrderInfoLst=trackOrderInfos,
            };
            var results = await _HuaLei.Track(trackParameter);
            Assert.IsNotNull(results.TrackResults);
        }


        [Test]
        public async Task TestPrint()
        {
            PrintParam printParam = new PrintParam() { CustomNumber = "", LogictisNumber = "", TrackNumber = "" };
            MultiPrint multiPrint = new MultiPrint()
            {
                LogName = logName,
                Parameter = JsonConvert.SerializeObject(parameter),
                ContentType = "0",
                FileType = "0",
                printParams = new List<PrintParam>() { printParam },
            };
            var results = await _HuaLei.Print(multiPrint);
            Assert.That(results, Is.True);
        }
    }
}