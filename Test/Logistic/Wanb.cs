using ERP.Models.Api.Logistics;
using ERP.Services.Api.Logistics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using Newtonsoft.Json;
using ERP.Models.Api.Logistics.WANB;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Abstractions;
using ERP.Services.Api.Logistics.Detail;
using NPOI.SS.Formula.Functions;
using Sy.Security;
using System;

namespace TestERP4._0
{
    public class Wanb : InjectTest
    {
        static ILoggerFactory loggerFactory = NullLoggerFactory.Instance;
        public static ILoggerFactory LoggerFactory { get => loggerFactory; }

        private WANB wANB;
        string parameter = "{'Token':'DK49AjVbKj23bdk','AccountNo':'TEST'}";
        ERP.Enums.Platforms logName = ERP.Enums.Platforms.WANB;
        [SetUp]
        public void Setup()
        {
            wANB = Resolve<WANB>();
        }

        [Test]
        public void methodDefineds()
        {
            BaseParameter baseParameter = new BaseParameter()
            {
                LogName = ERP.Enums.Platforms.WANB,
                Parameter = parameter,
            };
            var result = wANB.methodDefineds(baseParameter);
            Assert.IsNotNull(result);
        }

        [Test]
        public async Task TestGetShipTypes()
        {
            ShipTypeParameter shipTypeParameter = new ShipTypeParameter()
            {
                LogName = logName,
                Parameter = parameter,
            };
            var results = await wANB.GetShipTypes(shipTypeParameter);
            Assert.IsNotNull(results.CommonResults);
            Assert.IsTrue(results.Success);
        }

        [Test]
        public async Task TestGetWarehouseCode()
        {
            ERP.Models.Api.Logistics.BaseParameter baseParameter = new ERP.Models.Api.Logistics.BaseParameter()
            {
                LogName = logName,
                Parameter = parameter,
            };
            var results = await wANB.GetWarehouseCode(baseParameter);
            Assert.IsNotNull(results);
            
        }

        [Test]
        public void CheckJson()
        {
            string content = "{'Data':{'ShippingMethods':[{'Code':'3HPA','Name':'3HPA: Test Product - 3HPA','IsTracking':true,'IsVolumeWeight':true,'MaxVolumeWeightInCm':0.0,'MaxWeightInKg':null,'Region':'','Description':null},{'Code':'3H','Name':'3H: Test Product - 3H','IsTracking':true,'IsVolumeWeight':true,'MaxVolumeWeightInCm':0.0,'MaxWeightInKg':null,'Region':'','Description':null},{'Code':'TEST_PRODUCT','Name':'TEST_PRODUCT: test','IsTracking':true,'IsVolumeWeight':true,'MaxVolumeWeightInCm':0.0,'MaxWeightInKg':null,'Region':'','Description':null}]},'Succeeded':true,'Error':null}";

            try
            {
                var desc = JsonConvert.DeserializeObject<ResponseBaseData<Cls_Warehouses>>(content) ?? default!;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }           
        }

        [Test]
        public async Task TestTrack()
        {
            List<TrackOrderInfo> trackOrderInfos = new List<TrackOrderInfo>()
            {
                new TrackOrderInfo(){
                    CustomNumber ="",
                    LogictisNumber= "",
                    TrackNumber="",
                    Price=0,
                    Unit="",},
            };
            TrackParameter trackParameter = new TrackParameter()
            {
                LogName = logName,
                Parameter = parameter,
                TrackOrderInfoLst = trackOrderInfos,
            };
            var results = await wANB.Track(trackParameter);
            Assert.IsNotNull(results.TrackResults);
        }



        [Test]
        public async Task TestSend()
        {
            string ext = (@"{'CName':'挂饰','EName':'Ornaments','HsCode':'23456','Weight':0.5,'Length':30,'Width':10,
'Height':20,'IsSensitive':true,'WithBatteryType':'NOBattery','ItemType':'SPX','GoodsValue':10,'TaxType':'IOSS','TaxNumber':'454545','GoodsValueCurrency':'USD','ShipMethod':'3HPA',
'WarehouseCode':'SZ','WithBatteryType':'NOBattery','itemType':'DOC','RecipientTaxNumber':'23456'}");
            SenderInfo sender = new SenderInfo()
            {
                Name = "米境通",
                Mail = "xiaomiwaimao@qq.com",
                PostalCode = "210012",
                PhoneNumber = "13218051893",
                Region = "雨花台区",
                Province = "江苏省",
                City = "南京市",
                Address = "软件大道华博智慧园4F天联网络",
                Company = null,
            };

            ReceiverInfo receiverAddress = new ReceiverInfo()
            {
                Name = "Janette Bastyr",
                Phone = "",
                NationId = 46,
                NationShort = "DE",
                Nation = "德国",
                Province = "",
                City = "Faßberg",
                District = "",
                Address = "",
                Address2 = "Heideweg 44",
                Address3 = "",
                Zip = "29328",
                Email = "s72zbb0470998yq@marketplace.amazon.de",
            };
            GoodInfo good = new GoodInfo()
            {
                goodId = "B07N7VKGST",
                Name = "zhongjiany 22,9 cm Drache Plüschpuppe Super Fire Dragon Plüsch Tier gefüllte Puppe Spielzeug für Kinder (blau)",
                Asin = "B07N7VKGST",
                Num = 1,
                Unit = "EUR",
                Price = 18,
                Total = 23,
                Delivery = 0,
                SendNum = 1,
                Count = 1,
                Url = "https://m.media-amazon.com/images/I/517Arqs7EhL._SL75_.jpg",
                Sku = "DE-SP-69",
            };
            List<GoodInfo> goodInfos = new List<GoodInfo>();
            goodInfos.Add(good);
            OrderInfo order = new OrderInfo()
            {
                ext = ext,
                id = 312617,
                Url = "https://m.media-amazon.com/images/I/517Arqs7EhL._SL75_.jpg",
                OrderNo = "306-0479350-5531550",
                ReceiverAddress = receiverAddress,
                SendNum = 1,
                FailNum = 0,
                GoodInfos = goodInfos,
            };
            List<OrderInfo> orderInfos = new List<OrderInfo>();
            orderInfos.Add(order);

            SendParameter sendParameter = new SendParameter()
            {
                LogName = logName,
                Parameter = parameter,
                SenderInfo = sender,
                OrderInfos = orderInfos,
            };
            var results = await wANB.Send(sendParameter);
            //Assert.IsNotNull(results.Result);
            //Assert.That(results.Success, Is.True);
        }


      

        [Test]
        public async Task TestPrint()
        {
            PrintParam printParam = new PrintParam() { CustomNumber = "3A5V594328018", LogictisNumber = "", TrackNumber = "" };
            PrintParam printParam2 = new PrintParam() { CustomNumber = "3A5V594243300", LogictisNumber = "", TrackNumber = "" };            
            MultiPrint multiPrint = new MultiPrint()
            {
                LogName = logName,
                Parameter = parameter,
                printParams=new List<PrintParam>() { printParam,printParam2},
            };
            var results = await wANB.Print(multiPrint);
            Assert.That(results, Is.True);
        }

    }
}