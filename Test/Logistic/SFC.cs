using ERP.Models.Api.Logistics;
using ERP.Services.Api.Logistics;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Abstractions;

namespace TestERP4._0
{
    [TestFixture]
    public class SFC : InjectTest
    {
        static ILoggerFactory loggerFactory = NullLoggerFactory.Instance;
        public static ILoggerFactory LoggerFactory { get => loggerFactory; }


        private ERP.Services.Api.Logistics.Detail.SFC _SFC;
       

        string Parameter = "{'appKey': 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDaQfud671dnXVGHkUObhpzd9NV5O/DS07x/r56XE+SEsDt7KzcQuWgCjYehqbbjkTTWUIoaf4nh3SKpkWxFxNcnmM9sYnY37+PxrWlDdf+z0a16Nc5ITYk29MUZC+BpMZR1PpMGHdAe6y4D/beNMSoSqf7/V/Kh7yqGlFtDX3arwIDAQAB', 'token': 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC0yWyIzqinHXtrUwKplJJXtdd7pWS57dn+J0sAH8OGKJJ/I6NX4HnXR0wMhs0QyVa3lxRV/BiiX+YmmqVTLLOK9T54lE495X2g2dj6gdwOV9TZTCzmdPSm5rYZlIuvtZ4VK29QuHbnQ9v5ahvcrnIBfvBnjpMjpOWEBAaAHq7OhQIDAQAB','userId':'X8307'}";


        //MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDaQfud671dnXVGHkUObhpzd9NV5O/DS07x/r56XE+SEsDt7KzcQuWgCjYehqbbjkTTWUIoaf4nh3SKpkWxFxNcnmM9sYnY37+PxrWlDdf+z0a16Nc5ITYk29MUZC+BpMZR1PpMGHdAe6y4D/beNMSoSqf7/V/Kh7yqGlFtDX3arwIDAQAB


        //MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC0yWyIzqinHXtrUwKplJJXtdd7pWS57dn+J0sAH8OGKJJ/I6NX4HnXR0wMhs0QyVa3lxRV/BiiX+YmmqVTLLOK9T54lE495X2g2dj6gdwOV9TZTCzmdPSm5rYZlIuvtZ4VK29QuHbnQ9v5ahvcrnIBfvBnjpMjpOWEBAaAHq7OhQIDAQAB


        ERP.Enums.Platforms logName = ERP.Enums.Platforms.SFC;

        [SetUp]
        public void Setup()
        {
            _SFC = Resolve<ERP.Services.Api.Logistics.Detail.SFC>();
        }


        [Test]
        public void methodDefineds()
        {
            BaseParameter baseParameter = new BaseParameter()
            {
                LogName = ERP.Enums.Platforms.SFC,
                Parameter = Parameter,
            };
            var result = _SFC.methodDefineds(baseParameter);
            Assert.IsNotNull(result);
        }

        [Test]
        public async Task TestGetShipTypes()
        {
            ShipTypeParameter shipTypeParameter = new ShipTypeParameter()
            {
                LogName = logName,
                Parameter = Parameter,
            };
            var results = await _SFC.GetShipTypes(shipTypeParameter);
            Assert.IsNotNull(results.CommonResults);
            Assert.IsTrue(results.Success);
        }

        //SFCAA0060873597YQ

        [Test]
        public async Task TestTrack()
        {
            List<TrackOrderInfo> trackOrderInfos = new List<TrackOrderInfo>()
            {
                new TrackOrderInfo(){
                    CustomNumber ="028-4477076-0009123",
                    LogictisNumber= "SFCAA0060310732YQ",
                    TrackNumber="SFCAA0060310732YQ",
                    Price=0,
                    Unit=null},
            };
            TrackParameter trackParameter = new TrackParameter()
            {
                LogName = logName,
                Parameter = Parameter,
                TrackOrderInfoLst = trackOrderInfos,
            };
            var results = await _SFC.Track(trackParameter);
            Assert.IsNotNull(results.TrackResults);
        }


        [Test]
        public async Task TestPrint()
        {
            // setup
            var trackParam = Parameter;
            var express = "";

            PrintParam printParam = new PrintParam() { CustomNumber = "", LogictisNumber = "", TrackNumber = "", Express = express };
            MultiPrint multiPrint = new MultiPrint()
            {
                LogName = logName,
                Parameter = trackParam,
                FileType = "PDF",
                printParams = new List<PrintParam>() { printParam },
            };

            // action
            var results = await _SFC.Print(multiPrint);

            // assert
            Assert.IsTrue(results.Success);
            // var stream = new MemoryStream();
            // results.PrintResults[0].Stream.CopyTo(stream);
            // File.WriteAllBytes("./test.pdf", stream.ToArray());
        }



        [Test]
        public async Task TestSend()
        {
            string ext = (@"{'goodsDescription':'','detailCustomLabel':'','detailDescriptionCN':'挂饰',
'detailDescription':'Ornaments','Weight':0.5,'Length':30,'Width':10,
'Height':20,'HsCode':'23456','MaxPrice':10,'IOSS':'454545','ShipMethod':'ACS2'}");
            SenderInfo sender = new SenderInfo()
            {
                Name = "米境通",
                Mail = "xiaomiwaimao@qq.com",
                PostalCode = "210012",
                PhoneNumber = "13218051893",
                Region = "雨花台区",
                Province = "江苏省",
                City = "南京市",
                Address = "软件大道华博智慧园4F天联网络",
                Company = "",
            };

            ReceiverInfo receiverAddress = new ReceiverInfo()
            {
                Name = "Janette Bastyr",
                Phone = "",
                NationId = 46,
                NationShort = "DE",
                Nation = "德国",
                Province = "",
                City = "Faßberg",
                District = "aabb",
                Address = "",
                Address2 = "Heideweg 44",
                Address3 = "",
                Zip = "29328",
                Email = "s72zbb0470998yq@marketplace.amazon.de",
            };

            GoodInfo good = new GoodInfo()
            {
                goodId = "B07N7VKGST",
                Name = "zhongjiany 22,9 cm Drache Plüschpuppe Super Fire Dragon Plüsch Tier gefüllte Puppe Spielzeug für Kinder (blau)",
                Asin = "B07N7VKGST",
                Num = 1,
                Unit = "EUR",
                Price = 18,
                Total = 23,
                Delivery = 0,
                SendNum = 1,
                Count = 1,
                Url = "https://m.media-amazon.com/images/I/517Arqs7EhL._SL75_.jpg",
                Sku = "DE-SP-69",
            };
            List<GoodInfo> goodInfos = new List<GoodInfo>();
            goodInfos.Add(good);

            OrderInfo order = new OrderInfo()
            {
                ext = ext,
                id = 312617,
                Url = "https://m.media-amazon.com/images/I/517Arqs7EhL._SL75_.jpg",
                OrderNo = "306-0479350-5531550",
                ReceiverAddress = receiverAddress,
                SendNum = 1,
                FailNum = 0,
                GoodInfos = goodInfos,
            };

            List<OrderInfo> orderInfos = new List<OrderInfo>();
            orderInfos.Add(order);

            SendParameter sendParameter = new SendParameter()
            {
                LogName = logName,
                Parameter = Parameter,
                SenderInfo = sender,
                OrderInfos = orderInfos,
            };
            var results = await _SFC.Send(sendParameter);
            Assert.IsNotNull(results.ReportResults);
            Assert.That(results, Is.True);
        }
    }
}