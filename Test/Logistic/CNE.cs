using ERP.Models.Api.Logistics;
using ERP.Services.Api.Logistics;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;

namespace TestERP4._0
{
    [TestFixture]
    public class CNE : InjectTest
    {
        static ILoggerFactory loggerFactory = NullLoggerFactory.Instance;
        public static ILoggerFactory LoggerFactory { get => loggerFactory; }


        private ERP.Services.Api.Logistics.Detail.DYGJ _CNE;
       
        string TestParameter = "{'icID': '313124190', 'SecretKey': 'b98aae34fb0650129668e0f5a519d407','Platform':'CNE'}";

        
        ERP.Enums.Platforms logName = ERP.Enums.Platforms.CNE_DYGJ;
        [SetUp]
        public void Setup()
        {
            _CNE = Resolve<ERP.Services.Api.Logistics.Detail.DYGJ>();
        }

        [Test]
        public void methodDefineds()
        {
            BaseParameter baseParameter  = new BaseParameter()
            {
                LogName = ERP.Enums.Platforms.CNE_HZGJ,
                Parameter = TestParameter,
            };
            var result = _CNE.methodDefineds(baseParameter);
            Assert.IsNotNull(result);
        }

        [Test]
        public async Task TestGetShipTypes()
        {
            ShipTypeParameter shipTypeParameter = new ShipTypeParameter()
            {
                LogName = logName,
                Parameter = TestParameter,
            };
            var results = await _CNE.GetShipTypes(shipTypeParameter);
            Assert.IsNotNull(results.CommonResults);
            Assert.IsTrue(results.CommonResults.Count > 0);
        }


        [Test]
        public async Task TestTrack()
        {
            List<TrackOrderInfo> trackOrderInfos = new List<TrackOrderInfo>()
            {
                new TrackOrderInfo(){
                    CustomNumber ="",
                    LogictisNumber= "112-8428635-4939412",
                    TrackNumber="",
                    Price=0,
                    Unit="",},
            };
            //cne:List<string> lst = new List<string>() { "3A5V594328018", "3A5V594243300" };
         
            TrackParameter trackParameter = new TrackParameter()
            {
                LogName = logName,
                Parameter = TestParameter,
                TrackOrderInfoLst = trackOrderInfos,
            };
            var results = await _CNE.Track(trackParameter);
            Assert.IsNotNull(results.TrackResults);
        }


        [Test]
        public async Task TestPrint()
        {
            PrintParam printParam = new PrintParam() { CustomNumber = "", LogictisNumber = "112-8428635-4939412",Express= "3A5V622265022", TrackNumber = "" };
            //cne:List<string> lst = new List<string>() { "3A5V594328018", "3A5V594243300" };          
            //hzgj: 112-8428635-4939412
            MultiPrint multiPrint = new MultiPrint()
            {
                LogName = ERP.Enums.Platforms.CNE_HZGJ,
                Parameter = TestParameter,
                PaperType = "label10x10_1",             
                printParams= new List<PrintParam>() { printParam },               
            };
            var result = await _CNE.Print(multiPrint);
            if (result.Success && result.PrintResults[0].Success && result.PrintResults[0].Stream is not null)
            {
                var print = result.PrintResults[0];
                var stream = print.Stream;
                var contentType = result.PrintResults[0].ContentType;
                var ext = result.PrintResults[0].Ext;
                var data = new
                {
                    print = multiPrint,
                };
                string request = JsonConvert.SerializeObject(data);
                string resultStr = "";
                //await operateService.UpdatePrintInfo(multiPrint.waybillId, request, result.Response, resultStr, result.Success);
               
                using (StreamReader reader = new StreamReader(stream, System.Text.Encoding.UTF8))
                {
                    var s= reader.ReadToEnd();
                }
                //return new FileStreamResult(stream, contentType)
                //{
                //    FileDownloadName = Random.Shared.NextInt64(100000, 999999).ToString() + ext,
                //};
            }
            else
            {
                //if (result.exception.GetType() == typeof(Exceptions.Logistics.UnKnownError))
                //{
                //    resultError = "下载失败";
                //}
                //else
                //{
                //    resultError = result.PrintResults[0].exception.Message;
                //}

                var resultError = result.PrintResults[0].exception.Message;
            }


            Assert.That(result, Is.True);
        }

        [Test]
        public async Task GetPrint()
        {
            
            FileTypeParameter multiPrint = new FileTypeParameter()
            {
                LogName = ERP.Enums.Platforms.CNE_HZGJ,
                Parameter = TestParameter,               
            };
            var results = await _CNE.GetPrint(multiPrint);
            Assert.IsNotNull(results);
        }
        


        [Test]
        public async Task TestSend()
        {
            string ext = (@"{'ProNameCn':'挂饰','ProNameEn':'Ornaments','fWeight':0.5,'iLong':30,'iWidth':10,
'iHeight':20,'HasCode':'23456','fxPrice':0,'taxType':'iossCode','taxNumber':'454545','ShipMethod':'CNE全球快捷'}");
            SenderInfo sender = new SenderInfo()
            {
                Name = "米境通",
                Mail = "xiaomiwaimao@qq.com",
                PostalCode = "210012",
                PhoneNumber = "13218051893",
                Region = "雨花台区",
                Province = "江苏省",
                City = "南京市",
                Address = "软件大道华博智慧园4F天联网络",
                Company = "",
            };

            ReceiverInfo receiverAddress = new ReceiverInfo()
            {
                Name = "Janette Bastyr",
                Phone = "",
                NationId = 46,
                NationShort = "DE",
                Nation = "德国",
                Province = "",
                City = "Faßberg",
                District = "aabb",
                Address = "",
                Address2 = "Heideweg 44",
                Address3 = "",
                Zip = "29328",
                Email = "s72zbb0470998yq@marketplace.amazon.de",
            };

            GoodInfo good = new GoodInfo()
            {
                goodId = "B07N7VKGST",
                Name = "zhongjiany 22,9 cm Drache Plüschpuppe Super Fire Dragon Plüsch Tier gefüllte Puppe Spielzeug für Kinder (blau)",
                Asin = "B07N7VKGST",
                Num = 1,
                Unit = "EUR",
                Price = 18,
                Total = 23,
                Delivery = 0,
                SendNum = 1,
                Count = 1,
                Url = "https://m.media-amazon.com/images/I/517Arqs7EhL._SL75_.jpg",
                Sku = "DE-SP-69",
            };
            List<GoodInfo> goodInfos = new List<GoodInfo>();
            goodInfos.Add(good);

            OrderInfo order = new OrderInfo()
            {
                ext = ext,
                id = 312617,
                Url = "https://m.media-amazon.com/images/I/517Arqs7EhL._SL75_.jpg",
                OrderNo = "306-0479350-5531550",
                ReceiverAddress = receiverAddress,
                SendNum = 1,
                FailNum = 0,
                GoodInfos = goodInfos,
            };

            List<OrderInfo> orderInfos = new List<OrderInfo>();
            orderInfos.Add(order);

            SendParameter sendParameter = new SendParameter()
            {
                LogName = logName,
                Parameter = TestParameter,
                SenderInfo = sender,
                OrderInfos = orderInfos,
            };
            var results = await _CNE.Send(sendParameter);
            Assert.IsNotNull(results.ReportResults);
            Assert.That(results, Is.True);
        }
    }
}