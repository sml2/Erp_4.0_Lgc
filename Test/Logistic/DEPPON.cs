using ERP.Models.Api.Logistics;
using ERP.Services.Api.Logistics;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Abstractions;
using ERP.Models.Api.Logistics.DEPPON;
using AmazonService = ERP.Services.DB.Orders.Order;

namespace TestERP4._0
{
    [TestFixture]
    public class DEPPON : InjectTest
    {      
        private ERP.Services.Api.Logistics.Detail.DEPPON _DEPPON;

        //沙箱测试账户
        Parameter parameter = new Parameter()
        {
            AppKey = "61ef3c73122b4a8b54ef810af58474596d7db442f06296dd75136925021d9037",
            AppToken = "61ef3c73122b4a8b54ef810af5847459",
        };

        string TestParam = "{'AppKey': '61ef3c73122b4a8b54ef810af58474596d7db442f06296dd75136925021d9037', 'AppToken': '61ef3c73122b4a8b54ef810af5847459'}";
       
                                
        
        ERP.Enums.Platforms logName = ERP.Enums.Platforms.DEPPON;

        [SetUp]
        public void Setup()
        {
            _DEPPON = Resolve<ERP.Services.Api.Logistics.Detail.DEPPON>();
        }

        [Test]
        public void methodDefineds()
        {
            BaseParameter baseParameter = new BaseParameter()
            {
                LogName = ERP.Enums.Platforms.DEPPON,
                Parameter = TestParam,
            };
            var result = _DEPPON.methodDefineds(baseParameter);
            Assert.IsNotNull(result);
        }


        [Test]
        public async Task TestGetShipTypes()
        {
            ShipTypeParameter shipTypeParameter = new ShipTypeParameter()
            {
                LogName = logName,
                Parameter = TestParam,
            };
            var results = await _DEPPON.GetShipTypes(shipTypeParameter);
            Assert.IsTrue(results.Success);
            Assert.IsNotNull(results.CommonResults);           
        }

        //lognumber:DP201231000084901
        //trackNUmber:00340434318033292072
        [Test]
        public async Task Track()
        {
            List<TrackOrderInfo> trackOrderInfos = new List<TrackOrderInfo>()
            {
                new TrackOrderInfo(){
                    CustomNumber ="",
                    LogictisNumber= "DP201231000084901",
                    TrackNumber="",
                    Price=0,
                    Unit="",},
            };

            TrackParameter trackParameter = new TrackParameter()
            {
                LogName = ERP.Enums.Platforms.DEPPON,
                Parameter = JsonConvert.SerializeObject(parameter),
                TrackOrderInfoLst = trackOrderInfos,
            };
            var results = await _DEPPON.Track(trackParameter);
            Assert.IsNotNull(results.TrackResults);
        }


        [Test]
        public async Task TestPrint()
        {
            PrintParam printParam = new PrintParam() { CustomNumber = "", LogictisNumber = "DP201231000084901", TrackNumber = "" };          
            MultiPrint multiPrint = new MultiPrint()
            {
                LogName = ERP.Enums.Platforms.DEPPON,
                Parameter = JsonConvert.SerializeObject(parameter),
                ContentType = "label10x10_1",
                FileType = "Label",
                printParams=new List<PrintParam>() { printParam },                
            };
            var results = await _DEPPON.Print(multiPrint);
            Assert.IsNotNull(results.PrintResults);
        }



        [Test]
        public async Task TestSend()
        {
            string ext = (@"{'cnname':'挂饰','enname':'Ornaments','weight':0.5,'length':30,'width':10,
'height':20,'HsCode':'23456','Price':10,'is_return':true,'ShipMethod':'DP-US-MGH(SZ)'}");
            SenderInfo sender = new SenderInfo()
            {
                Name = "米境通",
                Mail = "xiaomiwaimao@qq.com",
                PostalCode = "210012",
                PhoneNumber = "13218051893",
                Region = "雨花台区",
                Province = "江苏省",
                City = "南京市",
                Address = "软件大道华博智慧园4F天联网络",
                Company = "",
            };

            ReceiverInfo receiverAddress = new ReceiverInfo()
            {
                Name = "Janette Bastyr",
                Phone = "",
                NationId = 46,
                NationShort = "DE",
                Nation = "德国",
                Province = "",
                City = "Faßberg",
                District = "aabb",
                Address = "",
                Address2 = "Heideweg 44",
                Address3 = "",
                Zip = "29328",
                Email = "s72zbb0470998yq@marketplace.amazon.de",
            };

            GoodInfo good = new GoodInfo()
            {
                goodId = "B07N7VKGST",
                Name = "zhongjiany 22,9 cm Drache Plüschpuppe Super Fire Dragon Plüsch Tier gefüllte Puppe Spielzeug für Kinder (blau)",
                Asin = "B07N7VKGST",
                Num = 1,
                Unit = "EUR",
                Price = 18,
                Total = 23,
                Delivery = 0,
                SendNum = 1,
                Count = 1,
                Url = "https://m.media-amazon.com/images/I/517Arqs7EhL._SL75_.jpg",
                Sku = "DE-SP-69",
            };
            List<GoodInfo> goodInfos = new List<GoodInfo>();
            goodInfos.Add(good);

            OrderInfo order = new OrderInfo()
            {
                ext = ext,
                id = 312617,
                Url = "https://m.media-amazon.com/images/I/517Arqs7EhL._SL75_.jpg",
                OrderNo = "306-0479350-5531550",
                ReceiverAddress = receiverAddress,
                SendNum = 1,
                FailNum = 0,
                GoodInfos = goodInfos,
            };
            List<OrderInfo> orderInfos = new List<OrderInfo>();
            orderInfos.Add(order);
            SendParameter sendParameter = new SendParameter()
            {
                LogName = logName,
                Parameter = JsonConvert.SerializeObject(parameter),
                SenderInfo = sender,
                OrderInfos = orderInfos,
            };
            var results = await _DEPPON.Send(sendParameter);
            Assert.IsNotNull(results.ReportResults);
            Assert.That(results, Is.True);
        }
    }
}