# 亚马逊产品上传

## 流程图

```mermaid
flowchart TD
start[选择产品/多选] --> input_params[填写上传参数]
input_params --> create_upload_task[创建上传任务]
create_upload_task --> append_product[追加上传产品]
append_product --> is_auto_upload{是否自动上传}
is_auto_upload --> |是| auto_upload[自动同步]
is_auto_upload --> |否| click_upload[手动点击同步]
auto_upload --> push_queue
click_upload --> push_queue[将上传任务加入后台队列]
push_queue --> build_excel[构建表格]
build_excel --> upload_to_amazon[使用spapi上传至亚马逊]
upload_to_amazon --> monitor_upload_progress[监听上传进度]
```
