# 亚马逊跟卖模块

## 跟卖产品改价流程图

```mermaid
flowchart TD
start[轮询跟卖产品] --> is_timed{是否开启定时上下架}
is_timed --> |是| is_onsell{是否是上架状态}
is_timed --> |否| is_onsell{是否是上架状态}
is_onsell --> |否| start
is_onsell --> |是| change_price[应用改价规则]
change_price --> completed[结束]
completed -.-> start
```
