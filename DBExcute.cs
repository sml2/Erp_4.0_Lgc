﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using ERP.Models.ProductNodeType.Enum;

namespace ERP
{
    public class DBExcute
    {
        public static string GetFileName(string fileName)
        {
            string name = "";
            if (fileName.Length > 0 && fileName.Split('.').Count() > 0)
            {
                string[] arrayName = fileName.Split('.');
                name = string.Join('.', arrayName.Take(arrayName.Length - 1).ToArray());
            }
            return name.ToUpper();
        }
        public static EnumIsMandatory getIsMandatory(bool IsMandatory)
        {
            return IsMandatory == true ? EnumIsMandatory.REQUIRED : EnumIsMandatory.OPTIONAL;
        }

        public static EnumIsMandatory getRequireType(string Required)
        {
            switch (Required)
            {
                case "REQUIRED":
                    return EnumIsMandatory.REQUIRED;
                case "OPTIONAL":
                    return EnumIsMandatory.OPTIONAL;
                case "DESIRED":
                    return EnumIsMandatory.DESIRED;
                case "DISABLED":
                    return EnumIsMandatory.DISABLED;
                default:
                    return EnumIsMandatory.NONE;
            }
        }

        public static EnumInputType getInputType(string str)
        {
            switch (str)
            {
                case "STRING_TYPE":
                    return EnumInputType.STRING_TYPE;
                    //break;
                case "DATE_TYPE":
                    return EnumInputType.DATE_TYPE;
                    //break;
                default:
                    return EnumInputType.None;
            }

        }

        public static EnumType getType(string str)
        {
            switch (str)
            {
                case "TEXT_FILED":
                    return EnumType.TEXT_FILED;
                    //break;
                case "COMBO_BOX":
                    return EnumType.SELECT_COMBO_BOX;
                    //break;
                case "MULTIPLE_SELECT_COMBO_BOX":
                    return EnumType.MULTIPLE_SELECT_COMBO_BOX;
                    //break;
                default: return EnumType.None;
            }
        }

        public static EnumFormatType getFormatType(string str)
        {
            switch (str)
            {
                case "NORMAL":
                    return EnumFormatType.NORMAL;
                    //break;
                default: return EnumFormatType.None;
            }
        }

        public static EnumDateFormatType getDateFormatType(string str)
        {
            switch (str)
            {
                case "YEAR_MONTH_DATE":
                    return EnumDateFormatType.YEAR_MONTH_DATE;
                    //break;
                default: return EnumDateFormatType.None;
            }
        }

        public static string getStringByArray(object? o)
        {
            if (o == null)
            {
                return "";
            }
            return JsonConvert.SerializeObject(o);
        }

        public static int parseIntByCode(string value)
        {
            Regex e = new Regex("[a-zA-Z]+");
            if (value == null || !e.IsMatch(value))
            {
                throw new Exception();
            }
            char[] chars = value.ToLower().ToCharArray();
            int index = 0;
            for (int i = 0; i < chars.Length; i++)
            {
                index += ((int)chars[i] - (int)'a' + 1) * (int)Math.Pow(26, chars.Length - i - 1);
            }
            return index;
        }
    }
}

