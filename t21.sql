﻿START TRANSACTION;

ALTER TABLE `SyncExport` DROP INDEX `IX_SyncExport_LanguageId_PlatformId_Sort`;

ALTER TABLE `AttributeModels` DROP COLUMN `Platform_Name`;

ALTER TABLE `AttributeModels` RENAME COLUMN `Platform_Platform` TO `Platform`;

ALTER TABLE `SyncExport` COMMENT '产品导出_导出任务集';

ALTER TABLE `AttributeModels` MODIFY COLUMN `State` int NOT NULL COMMENT '属性是否在用';

ALTER TABLE `AttributeModels` MODIFY COLUMN `PropertyHash` bigint unsigned NOT NULL;

UPDATE `AttributeModels` SET `Datas` = ''
WHERE `Datas` IS NULL;
SELECT ROW_COUNT();


ALTER TABLE `AttributeModels` MODIFY COLUMN `Datas` longtext CHARACTER SET utf8mb4 NOT NULL COMMENT 'Property具体属性细节';

ALTER TABLE `AttributeModels` MODIFY COLUMN `Platform` int NOT NULL COMMENT '平台id';

ALTER TABLE `AttributeModels` ADD `RowMd5` longtext CHARACTER SET utf8mb4 NOT NULL;

ALTER TABLE `AsinProduct` MODIFY COLUMN `Size` longtext CHARACTER SET utf8mb4 NULL;

ALTER TABLE `AsinProduct` MODIFY COLUMN `ImageUrl` longtext CHARACTER SET utf8mb4 NULL;

ALTER TABLE `AsinProduct` MODIFY COLUMN `Color` longtext CHARACTER SET utf8mb4 NULL;

ALTER TABLE `AsinProduct` MODIFY COLUMN `Brand` longtext CHARACTER SET utf8mb4 NULL;

ALTER TABLE `AsinProduct` ADD `LanguageTag` longtext CHARACTER SET utf8mb4 NULL;

UPDATE `User` SET `CreatedAt` = TIMESTAMP '2022-12-08 19:42:43', `PasswordHash` = 'AQAAAAEAACcQAAAAEOF078BsFcTrdk8OnAFyp9SiJtHVPcgmc4fx/cE8QgTJL+ma0cWdWy3HW7cNZzIapg==', `UpdatedAt` = TIMESTAMP '2022-12-08 19:42:43'
WHERE `Id` = 444445;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2022-12-08 19:42:43', `PasswordHash` = 'AQAAAAEAACcQAAAAEJE4rbd7KaeQJE/gOE/UCDOkPYnprGJeutTmxb/ZUU6htqG53ntoxusCsGbqvWyuOg==', `UpdatedAt` = TIMESTAMP '2022-12-08 19:42:43'
WHERE `Id` = 444446;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2022-12-08 19:42:43', `PasswordHash` = 'AQAAAAEAACcQAAAAEAhCg4rafMS2xun1GZLVUAADAHlNdXIT3nQz7MlqqgfsLfznfL6J9IvZiPQR6PUqnw==', `UpdatedAt` = TIMESTAMP '2022-12-08 19:42:43'
WHERE `Id` = 444447;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2022-12-08 19:42:43', `PasswordHash` = 'AQAAAAEAACcQAAAAEIkgbbzKYZRSN9wlvRLY9fSzbpi4X3S752v6HDvoR9ZGY8GlplWdYouzY9vTs1f2Ig==', `UpdatedAt` = TIMESTAMP '2022-12-08 19:42:43'
WHERE `Id` = 444448;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2022-12-08 19:42:43', `PasswordHash` = 'AQAAAAEAACcQAAAAEOe8KGCifSdieNtvpUzlIM/8EhLjiOQ3bBQm8o6tcYHkKuG7ZkfufAydDtPBImSe1g==', `UpdatedAt` = TIMESTAMP '2022-12-08 19:42:43'
WHERE `Id` = 444449;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2022-12-08 19:42:43', `PasswordHash` = 'AQAAAAEAACcQAAAAEHZHL4N+44EsH6kBxGGvVP1bYV51EEy8zIYnxRyLuEy8lvNULrghCm144Xt2gYOe/A==', `UpdatedAt` = TIMESTAMP '2022-12-08 19:42:43'
WHERE `Id` = 444450;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2022-12-08 19:42:43', `PasswordHash` = 'AQAAAAEAACcQAAAAEN45Chyyxw8Dn/wCRDvqbQufJxL+KO+e7ITF0nT3UUhElLOh57HMSTb1pngOn+xL1A==', `UpdatedAt` = TIMESTAMP '2022-12-08 19:42:43'
WHERE `Id` = 444451;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2022-12-08 19:42:43', `PasswordHash` = 'AQAAAAEAACcQAAAAELuuUcNtfKmbe/iwAecoRin4vbbamXA+Yf8mYtE5kuJ3lgriI0/Uz8bZZloEEgPiiw==', `UpdatedAt` = TIMESTAMP '2022-12-08 19:42:43'
WHERE `Id` = 444452;
SELECT ROW_COUNT();


UPDATE `UserGroup` SET `Sort` = -1670499763
WHERE `ID` = 1;
SELECT ROW_COUNT();


UPDATE `UserValidities` SET `CreatedAt` = TIMESTAMP '2022-12-08 19:42:43', `UpdatedAt` = TIMESTAMP '2022-12-08 19:42:43'
WHERE `ID` = 1;
SELECT ROW_COUNT();


UPDATE `UserValidities` SET `CreatedAt` = TIMESTAMP '2022-12-08 19:42:43', `UpdatedAt` = TIMESTAMP '2022-12-08 19:42:43'
WHERE `ID` = 2;
SELECT ROW_COUNT();


UPDATE `UserValidities` SET `CreatedAt` = TIMESTAMP '2022-12-08 19:42:43', `UpdatedAt` = TIMESTAMP '2022-12-08 19:42:43'
WHERE `ID` = 3;
SELECT ROW_COUNT();


CREATE INDEX `IX_SyncExport_ExportSign` ON `SyncExport` (`ExportSign`);

CREATE INDEX `IX_SyncExport_LanguageId` ON `SyncExport` (`LanguageId`);

CREATE INDEX `IX_SyncExport_PlatformId` ON `SyncExport` (`PlatformId`);

CREATE INDEX `IX_SyncExport_Sort` ON `SyncExport` (`Sort`);

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20221208114248_t25', '6.0.5');

COMMIT;

