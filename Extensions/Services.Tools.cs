﻿using ERP.Services.Tools;

namespace ERP.Extensions;


public static partial class IServiceCollectionExtensions
{
    public static IServiceCollection AddTools(this IServiceCollection Services)
    {
        Services.AddSingleton<Captcha>();
        return Services;
    }
 
}
