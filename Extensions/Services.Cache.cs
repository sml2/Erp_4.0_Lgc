using ERP.Services.Caches;

namespace ERP.Extensions;


public static partial class IServiceCollectionExtensions
{
    public static IServiceCollection AddCacheServices(this IServiceCollection Services, IConfiguration configuration)
    {
        Services.AddMemoryCache();//ProductType UserOnLine
        Services.AddSingleton<TaskList>();
        Services.AddSingleton<FileBucketCache>();
        Services.AddSingleton<PlatformCache>();
        Services.AddSingleton<AmazonKey>();
        Services.AddSingleton<AmazonSPKey>();
        Services.AddSingleton<ShopeeKey>();        
        Services.AddSingleton<Company>();
        Services.AddSingleton<Menu>();
        Services.AddSingleton<Nation>();
        Services.AddSingleton<OEM>();
        Services.AddSingleton<PlatformData>();
        Services.AddSingleton<Store>();      
        Services.AddSingleton<Unit>();
        Services.AddSingleton<UserGroup>();
        Services.AddSingleton<WaitOrders>();
        Services.AddSingleton<Services.Caches.Logistics>();
        Services.AddSingleton<EmailConfigCache>();      
        Services.AddSingleton<Services.Caches.StoreMarket>();
        Services.AddSingleton<LanguagesCache>();
        Services.AddSingleton<B2CUsersCache>();
    
        Services.AddDistributedMemoryCache();//Redis
        var redisConnectionString = configuration.GetConnectionString("redis");
        if (!string.IsNullOrEmpty(redisConnectionString))
            Services.AddStackExchangeRedisCache(option =>
            {
                option.Configuration = redisConnectionString;
            });
        return Services;
    }
 
}
