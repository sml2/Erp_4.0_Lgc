﻿using ERP.Services.Caches;
using ERP.Services.DistributedCrawl;

namespace ERP.Extensions;


public static partial class IServiceCollectionExtensions
{
    static IServiceCollection AddDistributedCrawlEngine(this IServiceCollection services)
    {
        return services.AddTransient<Engine>();
    }
}
