﻿using ERP.Data;
using Microsoft.EntityFrameworkCore;

namespace ERP.Extensions;

public static partial class IServiceCollectionExtensions
{
    /// <summary>
    /// 数据库
    /// </summary>
    /// <param name="Services"></param>
    /// <param name="Builder"></param>
    /// <returns></returns>
    /// <exception cref="ArgumentException"></exception>
    public static IServiceCollection AddDBContext3(this IServiceCollection Services, IConfiguration Configuration)
    {
        var connectionStr = Configuration.GetConnectionString("mysql3");
        if (!string.IsNullOrEmpty(connectionStr))
            Services.AddDbContext<DBContext3>(options =>
                options.UseMySql(connectionStr, ServerVersion.AutoDetect(connectionStr)).EnableSensitiveDataLogging()
                    .EnableDetailedErrors());
        return Services;
    }
}