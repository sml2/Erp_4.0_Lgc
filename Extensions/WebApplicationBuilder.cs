﻿using ERP.Models;
using ERP.Services.FileStorage;
using ERP.Services.FileStorage.Models;

namespace ERP.Extensions;
using Microsoft.Extensions.DependencyInjection;


public static class WebApplicationBuilderExtension
{
    public static IServiceCollection AddServices(this WebApplicationBuilder Builder)
    {
        var Services = Builder.Services;

        // Add services to the container.
        Services.AddServices(Builder.Configuration);

        return Services;
    }
}
