using ERP.Services.Export;
using ERP.Services.Host.Waybill;
using ERP.Services.Jobs.Coravel.Schedule;
using ERP.Services.Product;

namespace ERP.Extensions;

using Coravel;
using Services.Host;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.DependencyInjection;
using Coravel.Events.Interfaces;
using Coravel.Queuing.Broadcast;
using amazon=ERP.Services.Host.OrderNew;
using product = ERP.Services.Host.ProductNew;

public static partial class IServiceCollectionExtensions
{
    public static IServiceCollection AddHostServices(this IServiceCollection Services)
    {
        #region "Coravel"
        //Services.AddTransient<OrderList>();
        Services.AddTransient<ExchangeRate>();
        Services.AddTransient<StatisticsPlatformByDay>();
        Services.AddTransient<StatisticsPlatformByMonth>();
        Services.AddQueue();
        Services.AddEvents();
        Services.AddSingleton<IListener<QueueTaskCompleted>, TaskCompletedListener>();
        Services.AddScheduler();
        #endregion
        //
        Services.AddHostedService<amazon.OrderBasc>();  //增量拉取
        Services.AddHostedService<amazon.OrderItem>(); //增量拉取
        Services.AddHostedService<amazon.OrderAddress>();
        Services.AddHostedService<amazon.OrderBuyerInfo>();
        Services.AddHostedService<amazon.OrderFinancial>();
        Services.AddHostedService<Statistic>();
        //Services.AddHostedService<product.AsinAll>();
        //Services.AddHostedService<product.Asin>();

        Services.AddHostedService<product.NodeCategory>();
        Services.AddHostedService<product.ProductType>();
        Services.AddHostedService<RemoveExportedFilesBackground>();
        //
        Services.AddTaskQueue<ExportQueueItem, ExportBackground>();
        Services.AddTaskQueue<ProductUploadQueueItem, ProductUploadBackground>();
        Services.AddHostedService<PullShippingFeeHost>();//自动拉取运费

        Services.Configure<KestrelServerOptions>(o => o.AllowSynchronousIO = true);
        return Services;
    }
}





