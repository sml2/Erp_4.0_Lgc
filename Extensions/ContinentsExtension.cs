﻿using ERP.Enums;
using ERP.Models;
using System.Security.Claims;
namespace ERP.Extensions;
public static class ContinentsExtension
{
    public static IEnumerable<dynamic> GetIEnumerable(this Continents  continents)
    {
        return Enum.GetValues<Continents>().Where(@enum => @enum != continents).Select(e => new { ID = (int)e, Name = e.GetDescription(), English = $"{e}" });
    }
}
