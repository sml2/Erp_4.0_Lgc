﻿namespace ERP.Extensions;
public static class Enumerable
{
    public static MoneyRecord Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, MoneyRecord> selector)
    {
        var temp = MoneyRecord.Empty;
        source.ForEach(item => temp = temp + selector(item));
        return temp;
    }
}
