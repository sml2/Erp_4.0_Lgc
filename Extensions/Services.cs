using ERP.DomainEvents;
using ERP.DomainEvents.Product;
using ERP.Services.DB.Sms;
using ERP.Storage.Api.Requests;
using MediatR;

namespace ERP.Extensions;

using Data;
using Models;
using Services.FileStorage;
using Services.FileStorage.Models;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.DependencyInjection;
using Services.DistributedCrawl;
using OrderSDK.Modles.Shopee;
using OrderSDK.Modles.Amazon;

public static partial class IServiceCollectionExtensions
{
    public static IServiceCollection AddServices(this IServiceCollection Services, IConfiguration configuration)
    {
        configuration.GetSection("Disk").Bind(FileSystemFactory.diskConfig);
        var diskManager = configuration.GetSection("Disk").Get<DiskManager>();

        configuration.GetSection("Shopee").Bind(BaseOption.ShopeeOption);
        var shopee = configuration.GetSection("Shopee").Get<ShopeeOption>();

        configuration.GetSection("AmazonProxy").Bind(BaseAmazonOption.amazonProxy);
        var amazonProxy = configuration.GetSection("AmazonProxy").Get<AmazonProxyOption>();
        
        configuration.GetSection("Sms").GetSection("YunTongXun").Bind(SendService.smsConfig);
        var YtxSms = configuration.GetSection("Sms").GetSection("YunTongXun").Get<ERP.Services.DB.Sms.Ytx.Config>();

        Services.AddLoggingServices();
        Services.AddTools();
        Services.AddDBContext(configuration);
        Services.AddDBContext3(configuration);
        Services.AddModelServices();
        Services.AddIdentity();
        Services.AddMoney<MoneySeeder>(_ => { });
        Services.AddCacheServices(configuration);
        Services.AddProviderServices();
        Services.AddHTTPServices();
        Services.AddApiServices();
        Services.AddExcelServices(configuration);
        Services.AddDistributedCrawlEngine();
        Services.AddHostServices();
        Services.AddMediatR(typeof(OrderCreatedEvent).Assembly, typeof(ProductCreatedEvent).Assembly);

        Services.AddControllersWithViews();

        Services.AddSpaStaticFiles(config =>
        {
            config.RootPath = "wwwroot";
        });
        return Services;
    }

}
