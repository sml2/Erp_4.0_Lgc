﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace EntityFrameworkCore.Cacheable
{
    public class CacheableOptionsExtension : IDbContextOptionsExtension
    {
        ICacheProvider _cacheProvider;

        internal CacheableOptionsExtension(ICacheProvider cacheProvider)
        {
            _cacheProvider = cacheProvider;
        }


        public void ApplyServices(IServiceCollection services)
        {
            services.AddSingleton<ICacheProvider>(_cacheProvider);
        }


        public void Validate(IDbContextOptions options)
        {            
        }

        /// <summary>
        ///     The option set from the <see cref="DbContextOptionsBuilder.UseSecondLevelMemoryCache" /> method.
        /// </summary>
        public virtual ICacheProvider CacheProvider => _cacheProvider;

        public DbContextOptionsExtensionInfo Info => new CacheableOptionsExtensionInfo(this);
    }

    public class CacheableOptionsExtensionInfo : DbContextOptionsExtensionInfo
    {
        private readonly CacheableOptionsExtension _extension;

        public CacheableOptionsExtensionInfo(CacheableOptionsExtension extension) : base(extension)
        {
            this._extension = extension;
        }

        public override bool IsDatabaseProvider => false;

        public override string LogFragment => $"Using {_extension.CacheProvider.GetType().Name}";

        public override int GetServiceProviderHashCode() => 0;

        public override void PopulateDebugInfo(IDictionary<string, string> debugInfo)
        {
            debugInfo.Add(nameof(_extension.CacheProvider), $"Using {_extension.CacheProvider.GetType().Name}");
            debugInfo.Add(nameof(CacheableOptionsExtensionInfo), nameof(CacheableOptionsExtensionInfo));

        }

        public override bool ShouldUseSameServiceProvider(DbContextOptionsExtensionInfo other)
        {
            return true;
        }
    }
}
