﻿using ERP.Services.Excel;


namespace ERP.Extensions;
using Export.V4_Export.ExcelTemplates;

public static partial class IServiceCollectionExtensions
{
    public static IServiceCollection AddExcelServices(this IServiceCollection Services, IConfiguration configuration)
    {
        Services.AddSingleton<IExcelFactory, ExcelFactory>();

        Services.AddScoped<ERP.Services.Export.SyncExportService>();
        Services.AddScoped<ERP.Services.Stores.SyncProductService>();
        Services.AddSingleton<Export.ExportCommon.ProductExportHelper>();
        Services.AddExcelTemplate(configuration)
            .RegisterTemplate<Amazon>()
            .RegisterTemplate<DianXiaoMi>()
            .RegisterTemplate<BasicData>()
            
            // 董哥暂时不要lazada模板, 说是模板内容变更, 后期可能重新做
            // .RegisterTemplate<LazadaSingleCountry>()
            // .RegisterTemplate<LazadaSingleCountryOLD>()
            // .RegisterTemplate<LazadaSmallAppliances>()
            // .RegisterTemplate<LazadaFurnitureOrganization>()
            // .RegisterTemplate<LazadaMotherBaby>()
            // .RegisterTemplate<LazadaMobilesTablets>()
            // .RegisterTemplate<LazadaToysGames>()
            // .RegisterTemplate<LazadaWatchesSunglassesJewellery>()
            // .RegisterTemplate<LazadaSportsOutdoors>()
            // .RegisterTemplate<LazadaSmartDevices>()
            // .RegisterTemplate<LazadaSpecialDigitalProducts>()
            // .RegisterTemplate<LazadaStationeryCraft>()
            // .RegisterTemplate<LazadaTelevisionsVideos>()
            // .RegisterTemplate<LazadaToolsHomeImprovement>()
            // .RegisterTemplate<LazadaWomensShoesandClothing>()
            // .RegisterTemplate<LazadaAudio>()
            // .RegisterTemplate<LazadaBeauty>()
            // .RegisterTemplate<LazadaBeddingBath>()
            // .RegisterTemplate<LazadaCamerasDrones>()
            // .RegisterTemplate<LazadaComputersLaptops>()
            // .RegisterTemplate<LazadaDataStorage>()
            // .RegisterTemplate<LazadaDigitalGoods>()
            // .RegisterTemplate<LazadaGroceries>()
            // .RegisterTemplate<LazadaHealth>()
            // .RegisterTemplate<LazadaHouseholdSupplies>()
            // .RegisterTemplate<LazadaKitchenDining>()
            // .RegisterTemplate<LazadaLaundryCleaningEquipment>()
            // .RegisterTemplate<LazadaLighting>()
            // .RegisterTemplate<LazadaMensShoesandClothing>()
            // .RegisterTemplate<LazadaMonitorsPrinters>()
            // .RegisterTemplate<LazadaMotors>()
            // .RegisterTemplate<LazadaOutdoorGarden>()
            // .RegisterTemplate<LazadaPetSupplies>()
            .RegisterTemplate<JapanYahoo>()
            .RegisterTemplate<ZYingExcel>()
            .RegisterTemplate<Gmarket>()
            .RegisterTemplate<JapanNetsea>()
            .RegisterTemplate<Fecshop>()
            .RegisterTemplate<ShopifyCSV>()
            .RegisterTemplate<ERPebay>()
            .RegisterTemplate<HanDingYun>()
            .RegisterTemplate<TempData>()
            .RegisterTemplate<Cdiscount>()
            .RegisterTemplate<Shopee>()
            .RegisterTemplate<Korea>()
            .RegisterTemplate<LINIO>()
            .RegisterTemplate<QuTian>()
            .RegisterTemplate<NOON>()
            .RegisterTemplate<EbayTemplate>()
            .RegisterTemplate<NopCommerce>()
            .RegisterTemplate<OnBuy>()
            .RegisterTemplate<UEESHOP>()
            .RegisterTemplate<Coupang>()
            .RegisterTemplate<Ymmbh>()
            .RegisterTemplate<XingChuang>()
            .RegisterTemplate<ZYingThress>()
            .RegisterTemplate<StandAloneStation>()
            .RegisterTemplate<Zood>()
            .RegisterTemplate<Walmart>()
            .RegisterTemplate<Allegro>()
            .RegisterTemplate<QuickMaxvic>()
            .RegisterTemplate<YunHe>()
            .RegisterTemplate<Fruugo>()
            .RegisterTemplate<Rakuten_Fr>()
            .RegisterTemplate<Hktvmall>()
            .RegisterTemplate<Bol>()
            .RegisterTemplate<GlobalSources>()
            .RegisterTemplate<OriginEbay>()
            .RegisterTemplate<Ddpt>()
            .RegisterTemplate<Catch>();
        //Services.AddScoped<Cdiscount>();

        return Services;
    }




}
