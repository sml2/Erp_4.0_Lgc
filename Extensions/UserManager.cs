﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;
using ERP.Models.DB.Identity;
using ERP.Models.Finance;
using ERP.Services.Identity;
using Microsoft.AspNetCore.Identity;
using Role = ERP.Enums.Identity.Role;

namespace ERP.Extensions;
public static class UserManagerExtension
{
    public static Task<IdentityResult>  AddToRoleAsync(this UserManager um,User user,Role role) => um.AddToRoleAsync(user,role.GetName());
}
