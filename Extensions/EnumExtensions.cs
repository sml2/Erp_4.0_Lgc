//using System.Reflection;

//namespace ERP.Extensions;

//public static class EnumExtensions
//{
//    public static Models.PlatformData.LogisticsData GetLogisticsData(this Enum @enum)
//    {
//        var enumType = @enum.GetType();
//        if (enumType.Equals(typeof(Enums.ID.PlatformData)))
//        {
//            var value = @enum.GetNumber();
//            var fieldInfo = enumType.GetField(Enum.GetName(enumType, value)!);
//            if (fieldInfo!.GetCustomAttribute(typeof(Attributes.Logistics.Data), false) is Models.PlatformData.LogisticsData logdata)
//            {
//                return logdata;
//            }
//            else
//            {
//                throw new InvalidOperationException($"not found {nameof(Attributes.Logistics.Data)} in {@enum}");
//            }
//        }
//        else
//        {
//            throw new ArgumentOutOfRangeException($"{@enum} type is not {nameof(Enums.ID.PlatformData)}");
//        }
//    }

//    public static string GetGoodIdDescription(this Platforms p) => p switch
//    {
//        //Platforms.None => throw new NotImplementedException(),
//        Platforms.AMAZON => "ASIN",
//        //Platforms.OTHERS => throw new NotImplementedException(),
//        //Platforms.ALIEXPRESS => throw new NotImplementedException(),
//        //Platforms.WISH => throw new NotImplementedException(),
//        Platforms.SHOPEE => "唯一识别码",
//        _ => "唯一识别码"
//    };

//}