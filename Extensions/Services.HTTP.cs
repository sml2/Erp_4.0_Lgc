﻿using ERP.Controllers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace ERP.Extensions;


public static partial class IServiceCollectionExtensions
{
    public static IServiceCollection AddHTTPServices(this IServiceCollection Services)
    {
        //注入httpclientFactory，默认单例模式
        Services.AddHttpClient();
        Services.AddHttpClient(nameof(ERP.Services.Api.Logistics)).ConfigurePrimaryHttpMessageHandler(() =>
        {
            return new HttpClientHandler()
            {
                AllowAutoRedirect = true,
            };
        });
        Services.AddHttpContextAccessor();
        Services.AddRazorPages();

        Services.AddHealthChecks();
        Services.AddCors(option =>
        {
            option.AddDefaultPolicy(builder => builder.WithOrigins("https://eat.bananacakepop.com")
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());
        });
        // 使用此配置每次会重新创建对象
        JsonConvert.DefaultSettings = () => new JsonSerializerSettings()
        {
            ObjectCreationHandling = ObjectCreationHandling.Replace,
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        };

        Services.AddControllers().AddNewtonsoftJson(options =>
        {
            // Newtonsoft.Json.JsonSerializationException: Self referencing loop detected for property 
            options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
            options.SerializerSettings.ObjectCreationHandling = ObjectCreationHandling.Replace;
        }).ConfigureApiBehaviorOptions(options =>
        {
            var emptyArray = Array.Empty<string>();
            options.InvalidModelStateResponseFactory = context =>
            {
                var errorDic = context.ModelState.ToDictionary(m => m.Key,
                    m => m.Value?.Errors.Select(e => e.ErrorMessage).ToArray() ?? emptyArray);
                var errors = errorDic.Select(d => d.Value.Select(message => $"{d.Key}: {message}")).SelectMany(v => v).ToArray();
                var result = new ResultStruct()
                {
                    Code = Codes.V1,
                    Message = errors.First(),
                    Data = errors,
                };

                return new JsonResult(result);
            };
        })
        .AddMvcOptions(options =>
        {
            // api接口默认不使用浏览器缓存, 可以在控制器单独设置
            options.Filters.Add(new ResponseCacheAttribute()
            {
                NoStore = true,
                Location = ResponseCacheLocation.None,
            });
        });

        Services.AddResponseCaching();
        return Services;
    }
}
