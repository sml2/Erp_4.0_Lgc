using Coravel;
using ERP.Config;
using ERP.Middleware;
using ERP.Services.Identity;
using ERP.Services.Jobs.Coravel.Schedule;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Primitives;
using Prometheus;
using Ryu.Data.Abstraction;
using Exception = ERP.Exceptions.Exception;

namespace ERP.Extensions;

public static class WebApplicationExtension
{
    /// <summary>
    /// Configure the HTTP request pipeline.
    /// 
    /// </summary>
    /// <param name="App"></param>
    /// <returns><see cref="WebApplication"/></returns>
    public static WebApplication Configure(this WebApplication App)
    {
        // App.UseHttpLogging();
        // 未启用https时只会在首次弹出警告, 只使用http无影响
        // App.UseHttpsRedirection();
        // 只将域名erp.dmkjdzsw.com进行https跳转
        App.UseRewriter(new RewriteOptions().Add(ctx =>
        {
            if (ctx.HttpContext.Request.IsHttps)
                return;
            HostString host = ctx.HttpContext.Request.Host;
            if (!host.Host.Equals("erp.dmkjdzsw.com", StringComparison.OrdinalIgnoreCase))
                return;
            host = new HostString(host.Host, 443);
            HttpRequest request = ctx.HttpContext.Request;
            string str = UriHelper.BuildAbsolute("https", host, request.PathBase, request.Path, request.QueryString);
            HttpResponse response = ctx.HttpContext.Response;
            response.StatusCode = 301;
            response.Headers.Location = (StringValues) str;
            ctx.Result = RuleResult.EndResponse;
            ctx.Logger.LogInformation("Request redirected to HTTPS");
        }));
        App.Use(async (context, next) =>
        {
            if (context.Request.Path.Equals("/"))
                context.Request.Path = "/VUE/";
            await next(context);
        });
        App.UsePaginator();
        App.UseHttpMetrics();
        App.UseResponseCaching();
        App.UseDefaultFiles();

        App.UseStaticFiles(new StaticFileOptions()
        {
            OnPrepareResponse = ResponseCache,
        });
        if (App.Environment.IsProduction())
            App.UseSpaStaticFiles(new StaticFileOptions()
            {
                OnPrepareResponse = ResponseCache,
            });

        App.UseMoney();
        MoneyFactory.Instance.SetRateProvider(App.Services.GetRequiredService<IRateProvider>());
        if (!App.Environment.IsDevelopment())
        {
            App.UseDeveloperExceptionPage();
        }

        App.UseRouting();
        App.UseCors();
        App.UseSession();

        App.UseAuthentication();

        App.UseAuthorization();
        App.UseHealthChecks("/health");
        App.UseMiddleware<AdapteEnvironmentMiddleware>();


        App.Use(async (context, next) =>
        {
            if (context.User.Identity is { IsAuthenticated: true })
            {
                try
                {
                    context.Session.GetUserID();
                }
                catch (Exception e)
                {
                    var signInManager = context.RequestServices.GetService<SignInManager>();
                    var loggerFactory = context.RequestServices.GetService<ILoggerFactory>();
                    var logger = loggerFactory.CreateLogger($"{nameof(WebApplicationExtension)}.{nameof(Configure)}");
                    if (signInManager is not null)
                    {
                        var user = await signInManager.UserManager.GetUserAsync(context.User);
                        if (user is not null)
                        {
                            context.Session.SetUser(user);
                            context.Session.SetCompany(user.Company);
                        }
                        else
                        {
                            await signInManager.SignOutAsync();
                            await next(context);
                            return;
                        }
                    }
                    logger.LogError(e, e.Message);
                }
                // var dbContext = context.RequestServices.GetService<DBContext>();
                // var ruleInfo = await dbContext.RuleTemplate.Where(m => m.ID == context.Session.GetFormRuleTemplateID())
                //     .FirstOrDefaultAsync();
                // if (ruleInfo == null)
                // {
                //     // return Error(message: "角色不存在");
                // }
                // context.User.AddIdentity(new ClaimsIdentity(ruleInfo.GetClaims(context.Session.GetOEMID())));
            }
            await next(context);
        });

        App.MapGraphQLHttp();
        App.MapControllers();
        App.MapRazorPages();
        App.MapMetrics();
        #region "Coravel"
        //var QueueConfig = App.Services.ConfigureQueue();
        //var QueueLog = App.Services.GetService<ILogger<IQueue>>();
        //QueueConfig.LogQueuedTaskProgress(QueueLog);
        //QueueConfig.OnError((e)=> QueueLog.LogError($"OnError:{e}"));
        //var IQueue = App.Services.GetService<IQueue>();
        //IQueue.QueueInvocableWithPayload<OrderList,int>(0);

        //var registration = App.Services.ConfigureEvents();
        //registration
        //.Register<QueueTaskCompleted>()
        //.Subscribe<TaskCompletedListener>();

        App.Services.UseScheduler(schedule =>
        {
            var config = App.Configuration.GetSection("Schedule").Get<ScheduleConfig>();
            // schedule.Schedule<ExchangeRate>().EveryMinute();
            schedule.Schedule<ExchangeRate>().Cron(config.ExchangeRate);
            schedule.Schedule<StatisticsPlatformByDay>().Cron(config.Statistics);
            schedule.Schedule<StatisticsPlatformByMonth>().Cron(config.Statistics);
            // schedule.Schedule<StatisticsPlatformByDay>().EveryMinute();
        });

        #endregion
        return App;
    }

    /// <summary>
    /// 设置静态资源浏览器缓存时间, 可被cdn代理
    /// </summary>
    /// <param name="ctx"></param>
    private static void ResponseCache(StaticFileResponseContext ctx)
    {
        if (ctx.Context.Request.Path.HasValue)
        {
            var path = ctx.Context.Request.Path.Value;
            // 单位为秒
            var age = path switch
            {
                // 缓存js,css
                var x when x.EndsWith(".js") || x.EndsWith(".css") => 10,
                _ => 0,
            };
            if (age > 0)
            {
                ctx.Context.Response.Headers.Append("Cache-Control", $"public, max-age={age}");
            }
        }
    }
}
