﻿using ERP.Interface;
using ERP.Services.Provider;

namespace ERP.Extensions;


public static partial class IServiceCollectionExtensions
{
    public static IServiceCollection AddProviderServices(this IServiceCollection Services)
    {
        Services.AddScoped<ISessionProvider, Session>();
        Services.AddScoped<IOemProvider,OemProvider>();
        Services.AddScoped<OemSetter>();
        return Services;
    }
}
