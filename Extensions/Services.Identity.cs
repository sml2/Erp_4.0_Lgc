﻿using ERP.Data;
using Microsoft.AspNetCore.Identity;
using ERP.Services.Identity;
using Microsoft.AspNetCore.Authorization;
using ERP.Authorization.Rule;
using ERP.Interface.Rule;
using ERP.Services.Rule;
using ERP.Models.DB.Identity;
using ERP.Enums.Identity;

namespace ERP.Extensions;

using Role = Models.DB.Identity.Role;
using RoleHandler = Authorization.Role.Handler;
using RoleRequirement = Authorization.Role.Requirement;
using RuleHandler = Authorization.Rule.Handler;
using RuleRequirement = Authorization.Rule.Requirement;
using User = Models.DB.Identity.User;

public static partial class IServiceCollectionExtensions
{
    /// <summary>
    /// 用户认证
    /// </summary>
    /// <param name="Services"></param>
    /// <returns></returns>
    public static IServiceCollection AddIdentity(this IServiceCollection Services)
    {

        Services.AddIdentity<User, Role>(options =>
        {
            // Password settings.
            options.Password = new PasswordOptions
            {
                RequiredLength = 5, //要求密码最小长度，默认是 6 个字符
                RequireDigit = false, //要求有数字
                RequiredUniqueChars = 0, //要求至少要出现的字母数
                RequireLowercase = false, //要求小写字母
                RequireNonAlphanumeric = false, //要求特殊字符
                RequireUppercase = false //要求大写字母
            };

            // Lockout settings.
            options.Lockout = new LockoutOptions
            {
                AllowedForNewUsers = true, // 新用户锁定账户
                DefaultLockoutTimeSpan = TimeSpan.FromHours(30), //锁定时长，默认是 5 分钟
                MaxFailedAccessAttempts = 5 //登录错误最大尝试次数，默认 5 次
            };
            //options.Stores = new StoreOptions
            //{
            //    MaxLengthForKeys = 128, // 主键的最大长度
            //    ProtectPersonalData = true //保护用户数据，要求实现 IProtectedUserStore 接口
            //};
            //options.Tokens = new TokenOptions
            //{
            //    AuthenticatorIssuer = "Identity", //认证的消费者
            //    AuthenticatorTokenProvider = "MyAuthenticatorTokenProvider", //认证令牌的提供者
            //    ChangeEmailTokenProvider = "MyChangeEmailTokenProvider", //更换邮箱的令牌提供者
            //    ChangePhoneNumberTokenProvider = "MyChangePhoneNumberTokenProvider", //更换手机号的令牌提供者
            //    EmailConfirmationTokenProvider = "MyEmailConfirmationTokenProvider", //验证邮箱的令牌提供者
            //    PasswordResetTokenProvider = "MyPasswordResetTokenProvider", //重置密码的令牌提供者
            //    ProviderMap = new Dictionary<string, TokenProviderDescriptor>()
            //};
            // User settings.
            options.User = new UserOptions
            {
                RequireUniqueEmail = false, //要求Email唯一
                AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+" //允许的用户名字符，默认是 
            };

            options.ClaimsIdentity = new ClaimsIdentityOptions
            {
                // RoleClaimType = "IdentityRole",
                UserIdClaimType = nameof(UserClaim.UserId),
                // SecurityStampClaimType = "SecurityStamp",
                // UserNameClaimType = "IdentityName"
            };
            //options.SignIn = new SignInOptions
            //{
            //    RequireConfirmedEmail = true, //要求激活邮箱
            //    RequireConfirmedPhoneNumber = true //要求激活手机号
            //};
        })
            .AddEntityFrameworkStores<DBContext>()
            .AddDefaultTokenProviders()//重置密码 No IUserTokenProvider named 'Default' is registered 配置信息
            .AddUserManager<UserManager>()
            .AddRoleManager<RoleManager>()
            .AddSignInManager<SignInManager>()
            .AddUserStore<UserStore>()
            /*.AddClaimsPrincipalFactory<RuleTemplateUserClaimsFactory>()*/;
        //Services.AddScoped<SignInManager>();
        //Services.AddScoped<UserManager>();
        Services.ConfigureApplicationCookie(config =>
        {
            config.Cookie.Name = "erp_token";
            config.Cookie.HttpOnly = false;
            config.Events.OnRedirectToLogin = async (o) =>
            {
                //Console.WriteLine("OnRedirectToLogin");
                //未找到注入CookieAuthenticationHandler的方法,只好重写重定向到登录页的事件

                //var i = o.HttpContext.RequestServices.GetRequiredService<IActionContextAccessor>();//i.ActionContext为空,在非MVC的上下文中无法使用
                //await (new JsonResult(new BaseController.ResultStruct() { Message = "未授权", Code = 401 }) { StatusCode = 200 }).ExecuteResultAsync(i.ActionContext);

                //var res = JsonConvert.SerializeObject(new BaseController.ResultStruct() {Code = 401,Message = "未授权"});
                var res = "{\"message\":\"未授权\",\"data\":null,\"code\":50001}";
                o.Response.StatusCode = 200;
                o.Response.ContentType = "application/json;charset=UTF-8";
                await o.Response.WriteAsync(res);
            };
        });
        //Services.AddAuthentication().AddCookie(o => { o.Cookie.Name = "crm_auth"; o.LoginPath = "xxx"; });//AddIdentity 下会自动调用

        //Services.AddAuthentication(x =>
        //{
        //    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        //    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        //}).AddJwtBearer(x =>
        //{
        //    // x.RequireHttpsMetadata = true;
        //    x.SaveToken = true;
        //    x.TokenValidationParameters = new TokenValidationParameters
        //    {
        //        ValidateIssuer = false,
        //        // ValidIssuer = jwtTokenConfig.Issuer,
        //        ValidateIssuerSigningKey = true,
        //        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("123456123456123456")),
        //        // ValidAudience = jwtTokenConfig.Audience,
        //        ValidateAudience = false,
        //        ValidateLifetime = true,
        //        LifetimeValidator = ((_, expires, _, _) => DateTime.UtcNow < expires)
        //        // ClockSkew = TimeSpan.FromMinutes(1)
        //    };
        //});
        Services.AddSession(o => o.Cookie = new() { Name = "erp_session", HttpOnly = false });
        //鉴权
        Services.AddSingleton<IAuthorizationHandler, RoleHandler>();
        Services.AddSingleton<IAuthorizationHandler, RuleHandler>();
        Services.AddSingleton<IManager, Manager>();
        Services.AddAuthorization(options =>
        {
            options.AddPolicy("admin",
                policy => { policy.AddRequirements(new RoleRequirement(ERP.Enums.Identity.Role.SU.GetName(), ERP.Enums.Identity.Role.DEV.GetName(), ERP.Enums.Identity.Role.ADMIN.GetName())); });
            options.AddPolicy(nameof(PermissionAttribute),
                policy => policy.AddRequirements(new RuleRequirement())
            );
        });

        return Services;
    }
}
