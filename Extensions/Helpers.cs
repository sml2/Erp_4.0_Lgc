//<?php

//use App\Cache\Cache;
//use App\Models\User;
//use App\Models\Company;

///**
// * Created by PhpStorm.
// * User: ZY
// * Date: 2020/1/19
// * Time: 15:57
// * 项目全局方法
// */

//function p($arr = [])
//{
//    echo "<pre>";
//    print_r($arr);
//    echo "<pre>";
//}

//function d($arr = [])
//{
//    p($arr);
//    die;
//}

////检测数组,是否为不为空的数组 是 true 否 false
//function checkArr($arr)
//{
//    $res = is_array($arr) ? count($arr) > 0 ? true : false : false;
//    return $res;
//}

////检测是否非空字符串 是 true 否 false
//function checkStr($str = '')
//{
//    if (!is_string($str)) {
//        return false;
//    }
//    $str = trim($str);
//    if ($str == '' || $str == null) {
//        return false;
//    }
//    return empty($str) ? false : true;
//}

////截取指定字符串长度
//function subString($str, $length = 2, $suffix = '...')
//{
//    $str = trim($str);
//    if ($str == '' || $str == null) {
//        return '';
//    }
//    if (mb_strlen($str) <= $length) {
//        return $str;
//    }
//    $str = substr($str, 0, $length);
//    $str = trim($str);
//    return $str . $suffix;
//}


////判断是否URL网址（PHP代码/函数）
//function is_url($str)
//{
//    if (!checkStr($str)) {
//        return false;
//    }
//    $pattern = "#(http|https)://(.*\.)?.*\..*#i";
//    if (preg_match($pattern, $str)) {
//        return true;
//    } else {
//        return false;
//    }
//}


///**
// * 二位数组元素替换键值
// * @param array  $arr
// * @param string $id
// * @return array|false
// */
//function arrayCombine($arr = [], $id = '')
//{
//    $temp_key = array_column($arr, $id);  //键值 php5.5+
//    $mobile_arr = array_combine($temp_key, $arr);
//    return $mobile_arr;
//}

///**
// * 格式化字节大小
// * @param number $size      字节数
// * @param string $delimiter 数字和单位分隔符
// * @param string $precision 保留小数位
// * @return string 格式化后的带单位的大小
// */
//function diskTransform($size, $delimiter = '', $precision = 2)
//{
//    if ($size == 0) {
//        return 0;
//    }
//    $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
//    return round($size / pow(1024, ($i = floor(log($size, 1024)))), $precision) . $delimiter . $units[$i];
//}

///**
// * 硬盘使用百分比
// * @param int $total GB
// * @param int $use   B
// * @return float|int
// */
//function getDiskBar($total = 0, $use = 0)
//{
//    if ($total == 0 || $use == 0) {
//        return 0;
//    }
//    $bar = $use / $total / 1024 / 1024 / 1024 * 100;
//    return round($bar, 2) . '%';
//}

///* GB 转换 B */
//function gbToB($total = 0)
//{
//    if ($total == 0) {
//        return 0;
//    }
//    return $total * 1024 * 1024 * 1024;
//}

///* B 转换 GB */
//function bToGb($total = 0)
//{
//    if ($total == 0) {
//        return 0;
//    }
//    return round($total / 1024 / 1024 / 1024, 2);
//}

///* B 转换 MB */
//function bToMb($total = 0)
//{
//    if ($total == 0) {
//        return 0;
//    }
//    return round($total / 1024 / 1024, 2);
//}

////使用剩余容量
//function residueDisk($use = 0, $total = 0)
//{
//    if ($total == 0 || $use == 0) {
//        return $total;
//    }
//    $residue = gbToB($total) - $use;
//    return bToGb($residue);
//}

///**
// * 生成guid
// * GUID的生成源码
// * 代码中//后面标示了字符的转码如chr(45)代表“-”，chr(123)代表“{”等等，可以根据自己需要改写生成GUID的位数以及样式
// * @param int $type
// * @return string 26E69265-078C-C7D9-A0CA-33348CEEE712
// */
//function guid($type = 1)
//{
//    if (function_exists('com_create_guid')) {
//        return com_create_guid();
//    } else {
//        mt_srand((double)microtime() * 10000);//optional for php 4.2.0 and up.
//        $charid = strtoupper(md5(uniqid(rand(), true)));
//        $hyphen = $type == 1 ? '' : chr(45);// "-"
//        $uuid = substr($charid, 0, 8) . $hyphen
//            . substr($charid, 8, 4) . $hyphen
//            . substr($charid, 12, 4) . $hyphen
//            . substr($charid, 16, 4) . $hyphen
//            . substr($charid, 20, 12);
//        return strtolower($uuid);
//    }
//}

////字符串转bigint
//function createHashCode($str)
//{
//    //32 8-24 16Char->8Byte->64Bit
//    $md = md5($str);
//    $LowBase = base_convert(substr($md, 10, 14), 16, 10); //10-24 -> 56Bit
//    $HighBase = base_convert(substr($md, 8, 2), 16, 10) >> 1 << 56; //8-9 -> 7Bit
//    //dump(decbin($HighBase),decbin($LowBase),decbin( $LowBase | $HighBase));
//    return $LowBase | $HighBase;//OR
//}

////生成产品skucode
//function skucode()
//{
//    return from10to62(createHashCode(guid()));
//}

///**
// * 62进制转10进制
// * @param string $str 62进制
// * @return int 十进制
// */
//function from62to10($str)
//{
//    $dict = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
//    $len = strlen($str);
//    $dec = 0;
//    for ($i = 0; $i < $len; $i++) {
//        // 找到对应字典的下标.
//        $pos = strpos($dict, $str[$i]);
//        $dec += ($pos * pow(62, ($len - $i - 1)));
//    }

//    return $dec;
//}//end from62to10()


///**
// * 10进制转62进制
// * @param int $dec 十进制
// * @return string 62进制
// */
//function from10to62($dec)
//{
//    $dict = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
//    $result = '';
//    try {
//        do {
//            $result = $dict[($dec % 62)] . $result;
//            $dec = intval($dec / 62);
//        } while ($dec != 0);
//    } catch (Exception $e) {
//        $result = false;
//    }
//    return $result;
//}//end from10to62()


///**
// * 获取加密sku前缀
// * @param int    $id       产品id
// * @param string $skuInput 用户输入
// * @return string
// */
//function encodeSku($id, $skuInput = '')
//{
//    $key = 'P' . from10to62($id);
//    return $key . substr(md5($key), 0, 3) . '-' . $skuInput;
//}//end encodeSku()


///**
// * 验证sku前缀并返回产品id
// * @param string $sku 产品sku
// * @return int|bool
// */
//function decodeSku($sku)
//{
//    if (empty($sku)) {
//        return false;
//    }

//    // 存在.
//    $str = explode('-', $sku)[0];
//    if (substr($str, 0, 1) != 'P') {
//        return false;
//    }

//    if (strlen($str) < 5) {
//        return false;
//    }

//    $verify = substr($str, -3);
//    $key = substr($str, 0, (strlen($str) - 3));
//    $id = substr($key, 1);
//    if (substr(md5($key), 0, 3) != $verify) {
//        return false;
//    } else {
//        return from62to10($id);
//    }
//}//end decodeSku()


////解析url中参数信息，返回参数数组
//function convertUrlQuery($query)
//{
//    $queryParts = explode('&', $query);

//    $params = array();
//    foreach ($queryParts as $param) {
//        $item = explode('=', $param);
//        $params[$item[0]] = $item[1];
//    }

//    return $params;
//}

//function getIp()
//{
//    if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown")) {
//        $ip = getenv("HTTP_CLIENT_IP");
//    } elseif (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown")) {
//        $ip = getenv("HTTP_X_FORWARDED_FOR");
//    } elseif (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown")) {
//        $ip = getenv("REMOTE_ADDR");
//    } elseif (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp(
//            $_SERVER['REMOTE_ADDR'],
//            "unknown"
//        )) {
//        $ip = $_SERVER['REMOTE_ADDR'];
//    } else {
//        $ip = "unknown";
//    }
//    return $ip;
//}

///**
// * 接口通用返回方式
// * User: ZY
// * Date: 2020/3/5 16:09
// * @param int    $code
// * @param string $data
// * @param string $message
// * @return false|string
// */
//function message($code = 9999, $data = '', $message = '')
//{
//    if (!$message) {
//        $message = config('code')[$code];
//    }
//    $result = [
//        'code'    => $code,
//        'data'    => $data,
//        'message' => $message
//    ];

//    return $result;
//}

//function success($msg = '提交成功', $data = [], $code = 20000)
//{
//    $res = [
//        'code'    => $code,
//        'data'    => $data,
//        'message' => $msg
//    ];
//    return response()->json($res);
//}

//function warning($msg = '提交成功', $data = [], $code = 500)
//{
//    $res = [
//        'code'    => $code,
//        'data'    => $data,
//        'message' => $msg
//    ];
//    return response()->json($res);
//}

//function error($msg = '提交失败', $data = [], $code = 100)
//{
//    $res = [
//        'code'    => $code,
//        'data'    => $data,
//        'message' => $msg
//    ];
//    return response()->json($res);
//}

//function returnArr($state = true, $msg = '', $data = [])
//{
//    return [
//        'state' => $state,
//        'data'  => $data,
//        'msg'   => $msg
//    ];
//}

///**
// * 模拟get进行url请求
// * @param string $url
// * @param array  $header
// * @return bool|string
// */
//function curlGet($url = '', $header = [])
//{
//    $ch = curl_init();
//    $header = count($header) > 0 ? $header : array("Content-type: text/xml");
//    curl_setopt($ch, CURLOPT_URL, $url);
//    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//    curl_setopt($ch, CURLOPT_TIMEOUT, 15);
//    curl_setopt($ch, CURLOPT_POST, 0);
//    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
//    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
//    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
//    curl_setopt($ch, CURLOPT_HEADER, 0);
//    $response = curl_exec($ch);
//    return $response;
//}

//function downloadImgUrl($url, $path)
//{
//    $headers = [
//        'user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
//        'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
//    ];
//    $ch = curl_init();
//    curl_setopt($ch, CURLOPT_URL, $url);
//    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
//    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
//    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//    //执行命令
//    $file = curl_exec($ch);
//    curl_close($ch);


//    $resource = @fopen($path, 'a');
//    if ($resource == false) {
//        return false;
//    }
//    fwrite($resource, $file);
//    fclose($resource);

//    return filesize($path);
//}

//function writelog($data, $url = '')
//{
//    $url == '' && $url = "../storage/logs/" . date('Ymd') . '_log.txt';

//    $dir_name = dirname($url);
//    //目录不存在就创建
//    if (!file_exists($dir_name)) {
//        //iconv防止中文名乱码
//        $res = mkdir(iconv("UTF-8", "GBK", $dir_name), 0777, true);
//    }

//    $fp = fopen($url, "a");//打开文件资源通道 不存在则自动创建
//    fwrite(
//        $fp,
//        "********************** " . date(
//            'Y-m-d H:i:s',
//            time()
//        ) . " **********************************\r\n" . var_export($data, true) . "\r\n"
//    );//写入文件
//    fclose($fp);//关闭资源通道
//    return;
//}

//function writeImageLog($path, $data)
//{
//    $path = $path . date('Ymd') . '_log.txt';

//    $dir_name = dirname($path);
//    //目录不存在就创建
//    if (!file_exists($dir_name)) {
//        //iconv防止中文名乱码
//        $res = mkdir(iconv("UTF-8", "GBK", $dir_name), 0777, true);
//    }

//    $fp = fopen($path, "a");//打开文件资源通道 不存在则自动创建
//    fwrite(
//        $fp,
//        "********************** " . date(
//            'Y-m-d H:i:s',
//            time()
//        ) . " **********************************\r\n" . var_export($data, true) . "\r\n"
//    );//写入文件
//    fclose($fp);//关闭资源通道
//    return;
//}

//function collection($resource)
//{
//    return new \App\Resource\BaseCollection($resource);
//}

///**
// * csv导入,单列数据，此格式每次最多可以处理10000条数据
// * User: CCY
// * Date: 2019/7/4 17:20
// * @param $csv_file
// * @return array
// */
//function inputOnceCsv($csv_file)
//{
//    $out = array();
//    $n = 0;
//    while ($data = fgetcsv($csv_file, 10000)) {
//        isset($data[0]) && $out[$n] = $data[0];
//        $n++;
//    }
//    return $out;
//}

///**
// *  毫秒级时间戳
// * @author: Saber
// * Date: 2019/7/16 15:20
// * @return bool|string
// */
//function getMillisecond()
//{
//    [$msec, $sec] = explode(' ', microtime());
//    $msectime = (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);
//    return $msectimes = substr($msectime, 0, 13);
//}

///**
// * 更新所有换行符为指定符号
// * @param string $str
// * @param string $spliter
// * @return mixed|string|string[]|null
// */
//function transitionbr($str = '', $spliter = "\r\n")
//{

//    $str = trim($str);
//    if ($str == '') {
//        return '';
//    }
//    $br = '<br/>';
//    if (strpos($str, "\r\n") !== false) {//存在\r\n
//        $str = str_replace("\r\n", $br, $str);
//    }
//    if (strpos($str, "\n") !== false) {//存在\n
//        $str = str_replace("\n", $br, $str);
//    }
//    if (strpos($str, "\r") !== false) {//存在\r
//        $str = str_replace("\r", $br, $str);
//    }
//    //$str = preg_replace('/(<br[ \/]{0,3}>\s*)+/i', $spliter, $str); //替换多个<br/><br /><br  />为$str
//    $str = preg_replace('/<br[ \/]{0,3}>/i', $spliter, $str);
////    if (strpos($str, '<br/>') !== false) {//存在<br/>
////        $str = preg_replace('/<br\/>*/', $br, $str);
////        $str = preg_replace('/(<br\/>)+/i', $spliter, $str);
////    }
////    dd($str,1);
//    return $str;
//}

///**
// * User: ZY
// * Date: 2020/6/9 10:23
// * @param         $arr
// * @param         $field
// * @param boolean $type true max false min
// * @return mixed
// */
//function getArrayMaxOrMin(array $arr, string $field, bool $type = false)
//{
//    $temp = [];
//    foreach ($arr as $k => $v) {
//        $temp[] = $v[$field];
//    }
//    return $type ? max($temp) : min($temp);
//}

//function unicodeDecode($unicode_str)
//{
//    $json = '{"str":"' . $unicode_str . '"}';
//    $arr = json_decode($json, true);
//    if (empty($arr)) {
//        return '';
//    }
//    return $arr['str'];
//}

///**
// * 判断字符串是否base64编码
// */

//function isBase64($str)
//{
//    return $str == base64_encode(base64_decode($str)) ? true : false;
//}

///**
// * 将金额转换后存储数据库
// * @param float  $money 当前需要转换的金额
// * @param string $unit  当前币种
// * @param null   $rate
// * @return float|int 转换后存储进入数据库的数据
// */
//function FromConversion($money, $unit, $rate = null)
//{
//    if ($money == 0) {
//        return $money;
//    }
//    if ($rate === null || $rate == 0) {
//        $rate = $unit == 'CNY' ? 10000 : Cache::instance()->unitSingle()->get($unit)['rate'];
//    }
//    // FIXME:  bcmul(): bcmath function argument is not well-formed
//    return number_format(bcmul($money, $rate, 6), 6, '.', '');
//}

///**
// * 将数据库存储的金额转换在UI显示
// * @param float  $money 数据库中存储的金额数据
// * @param string $unit  需要转换成的币种
// * @param null   $rate
// * @return float|int 对应币种UI展示用的金额数据
// */
//function ToConversion($money, $unit, $rate = null)
//{
//    if ($money == 0) {
//        return (float)$money;
//    }

//    if (empty($rate) || $rate == 0) {
//        $rate = $unit === 'CNY' ? 10000 : Cache::instance()->unitSingle()->get($unit)['rate'];
//    }
//    // FIXME 分销添加的订单汇率可能为0
//    return (float)number_format(bcdiv($money, $rate, 6), 2, '.', '');
//}

///**
// * 从一种币种转换成另一种币种
// * @param string $money 需要转换的金额数量
// * @param string $from  当前币种
// * @param string $to    转换后的币种
// * @return float|int
// */
//function FromToConversion($money, $from, $to)
//{
//    $money = FromConversion($money, $from);
//    return ToConversion($money, $to);
//}

///**
// * 微秒组装字符串
// * @return string
// */
//if (!function_exists('getNumString')) {
//    function getNumString()
//    {
//        $time = request()->input('microtime', microtime(true));
//        $times = explode('.', $time);
//        return $times[1] . $times[0];
//    }
//}

///**
// *  远程图片缩放
// * User: ZY
// * Date: 2020/7/3 11:40
// * @param $image
// * @return string|string[]|null
// */
//function imageZoom($image)
//{
//    $zoom = [
//        'amazon'        => ['reg' => '/_\w{2}\d{3,4}_/', 'zoom' => '_SY120_'], //亚马逊
//        'alicdn'        => ['reg' => ' /_\d{3,4}x\d{3,4}/', 'zoom' => '_120x120'], //速卖通
//        '360buyimg'     => ['reg' => '/s\d{3,4}x\d{3,4}/', 'zoom' => 's120x120'], //京东
//        'ebayimg'       => ['reg' => '/s-l\d{3,4}/', 'zoom' => 's-l120'], //爱贝
//        'shopee'        => ['reg' => '', 'zoom' => '_tn'], // 虾皮
//        'jumia'         => ['reg' => '/\d{3,4}x\d{3,4}/', 'zoom' => '120x120'], //Jumia
//        'rightinthebox' => ['reg' => '/\d{3,4}x\d{3,4}/', 'zoom' => '120x120'], //rightinthebox
//        'flixcart'      => ['reg' => '/\d{3,4}\/\d{3,4}/', 'zoom' => '120/120'], //flixcart
//    ];

//    $zoomKeys = array_keys($zoom);

//    for ($i = 0, $iMax = count($zoomKeys); $i < $iMax; $i++) {
//        $sign = $zoomKeys[$i];
//        if (strpos($image, $sign)) {
//            $reg = $zoom[$sign];
//            if ($sign == 'shopee') {
//                $image = $image . $reg['zoom'];
//            } else {
//                $image = preg_replace($reg['reg'], $reg['zoom'], $image);
//            }
//            break;
//        }
//    }
//    return $image;
//}

//function getlang($str, $arr = [])
//{
//    return trans($str, $arr);
//}


///**
// * 需要积分转余额数值 1CNY = 10积分 / 1000基准货币数值=1积分
// * User: CCY
// * Date: 2020/11/16
// * @param $integral
// * @return mixed
// */
//function getBalanceFromIntegral($integral)
//{
//    return intval($integral * 1000);
//}

///**
// * 资源转换消耗积分
// * 请求次数/200+CPU占用时长/10000+内存使用量/(1024*1024*1024)+带宽使用量/(4*1024*1024)
// * User: CCY
// * Date: 2020/11/16
// * @param $request
// * @param $priceCf
// * @return int
// */
//function getIntegralFromRequest($request,$priceCf)
//{
//    $ext = [];
//    $msg = '【';
//    $total_request = 0;
//    if($request['total_request'] > 0 && $priceCf['request'] > 0){
//        $total_request = bcdiv($request['total_request'], $priceCf['request'], 6);
//        if($total_request > 0){
//            $formula = "请求次数:({$request['total_request']}*0.005)";
//            $msg .= "{$formula}+";
//            $ext['request'] = [
//                'formula' => $formula,
//                'amount' => $total_request
//            ];
//        }
//    }

//    $total_cpu = 0;
//    if($request['total_cpu'] > 0 && $priceCf['cpu'] > 0){
//        $total_cpu = bcdiv($request['total_cpu'], $priceCf['cpu'], 6);
//        if($total_cpu > 0){
//            $formula = "CPU消耗:({$request['total_cpu']}*0.0001)";
//            $msg .= "{$formula}+";
//            $ext['cpu'] = [
//                'formula' => $formula,
//                'amount' => $total_cpu
//            ];
//        }
//    }

//    $total_memory = 0;
//    if($request['total_memory'] > 0  && $priceCf['memory'] > 0){
//        $total_memory = bcdiv($request['total_memory'], $priceCf['memory'], 6);
//        if($total_memory > 0){
//            $formula = "内存消耗:(".getFilesize($request['total_memory'])."*0.0009765625M)";
//            $msg .= "{$formula}+";
//            $ext['memory'] = [
//                'formula' => $formula,
//                'amount' => $total_memory
//            ];
//        }
//    }
//    $total_network = 0;
//    if($request['total_network'] > 0 && $priceCf['network'] > 0){
//        $total_network = bcdiv($request['total_network'], $priceCf['network'], 6);
//        if($total_network > 0){
//            $formula = "带宽消耗:(".getFilesize($request['total_network'])."*0.25M)";
//            $msg .= "{$formula}+";
//            $ext['network'] = [
//                'formula' => $formula,
//                'amount' => $total_network
//            ];
//        }
//    }
//    $msg = trim($msg, '+').'】';
//    $cost =  (int)($total_request + $total_cpu + $total_memory + $total_network);
//    return compact(['cost','msg','ext']);
//}

///**
//20210720之前
// * function getIntegralFromRequestOld($request,$company)
//{
//    $total_request = bcdiv($request['total_request'], 200, 6);
//    $total_cpu = bcdiv($request['total_cpu'], 10000, 6);
//    $total_memory = bcdiv($request['total_memory'], (1024 * 1024 * 1024), 6);
//    $total_network = bcdiv($request['total_network'], (4 * 1024 * 1024), 6);
//return (int)($total_request + $total_cpu + $total_memory + $total_network);
//}
//function getIntegralFromPicApiOld($useNum = 0,$priceCf, $type = 1)
//{
//return ($useNum == 0 || $type == 1) ? $useNum : $useNum * 1000;
//}
// */

///**
// *  产品抠图接口使用次数转换对应扣除积分、金额
// * 1CNY = 10积分 =10000基准货币数值  一次0.1CNY1积分1000基准货币数值
// * User: CCY
// * Date: 2021/3/31
// * @param int $useNum 使用次数
// * @param int $type   1扣除积分 2扣除金额
// * @return float|int|mixed
// */
//function getIntegralFromPicApi($useNum = 0, $type = 1)
//{
//    return ($useNum == 0 || $type == 1) ? $useNum : $useNum * 1000;
//}

///**
// * Date: 2021/7/20 18:48
// * @param $priceCf
// * @param $type  1扣除积分 2扣除金额
// * @param $company  公司
// * 1CNY = 10积分 =10000基准货币数值  一次0.1CNY1积分1000基准货币数值
// */
//function getSoftwareConsume($statistics, $priceCf, $lastLoginTime, $type,$company)
//{
//    $currentTime = strtotime(date('Y-m-d', time()));
//    $lastLoginTime = strtotime(date('Y-m-d', strtotime($lastLoginTime)));
//    if ($currentTime == $lastLoginTime) {
//        $day = 0;
//    } else {
//        $day = (int) (($currentTime - $lastLoginTime) / 86400);
//    }
//    //基准货币数值 处理
//    if ($type == 2) {
//        foreach ($priceCf as $k => $v) {
//            $priceCf[$k] = $v * 1000;
//        }
//    }
//    $base = $type == 1 ? 1 : 10000;
//    $dayBase = bcmul($day,$base,6);

////    $picApi = $statistics['pic_api'] * $priceCf['picapi'] * ($type == 1 ? 1 : 1000);
////    $user = $day * $statistics['user_num'] * $priceCf['user'] * ($type == 1 ? 1 : 1000);
////    $product = $day * $statistics['product_num'] * $priceCf['product'] * ($type == 1 ? 1 : 1000);
////    $order = $day * $statistics['order_num'] * $priceCf['order'] * ($type == 1 ? 1 : 1000);
////    $purchase = $day * $statistics['purchase_num'] * $priceCf['purchase'] * ($type == 1 ? 1 : 1000);
////    $waybill = $day * $statistics['waybill_num'] * $priceCf['waybill'] * ($type == 1 ? 1 : 1000);
////    $store = $day * $statistics['store_num'] * $priceCf['store'] * ($type == 1 ? 1 : 1000);

//    $picApi = bcmul(bcmul($statistics['pic_api'],$priceCf['picapi'],6),$base,6);
//    $translateApi = 0;
//    if(isset($priceCf['translate'])){
//        $translateApi = bcmul(bcmul($statistics['translate_api'],$priceCf['translate'],6),$base,6);
//    }
//    $user = bcmul(bcmul($statistics['user_num'],$priceCf['user'],6),$dayBase,6);
//    $product = bcmul(bcmul($statistics['product_num'],$priceCf['product'],6),$dayBase,6);
//    $order = bcmul(bcmul($statistics['order_num'],$priceCf['order'],6),$dayBase,6);
//    $purchase = bcmul(bcmul($statistics['purchase_num'],$priceCf['purchase'],6),$dayBase,6);
//    $waybill = bcmul(bcmul($statistics['waybill_num'] , $priceCf['waybill'],6),$dayBase,6);
//    $store = bcmul(bcmul($statistics['store_num'] , $priceCf['store'],6),$dayBase,6);

//    $network = 0;
//    if(isset($statistics['network'])){
//        $network = bcmul(bcmul(bcdiv($statistics['network'],1073741824,6),$priceCf['network'],6),$base,6);
//    }

//    $ext = [];

//    $msg = '【';
//    $currentOem = Cache::instance()->currentOem()->get();
//    if( is_array($currentOem) && strpos($currentOem['title'],'财付通') !== false && $network > 0){
//        $val = bcdiv($statistics['network'],1073741824,6);
//        $formula = "流量空间:({$val}*{$priceCf['network']})";
//        $msg .= "{$formula}+";
//        $ext['network'] = [
//            'formula' => $formula,
//            'amount' => $network
//        ];
//    }
//    if ($picApi > 0) {
//        $formula = "抠图模块:({$statistics['pic_api']}*{$priceCf['picapi']})";
//        $msg .= "{$formula}+";
//        $ext['picapi'] = [
//            'formula' => $formula,
//            'amount' => $picApi
//        ];
//    }

//    if ($translateApi > 0) {
//        $formula = "翻译模块:({$statistics['translate_api']}*{$priceCf['translate']})";
//        $msg .= "{$formula}+";
//        $ext['translate'] = [
//            'formula' => $formula,
//            'amount' => $translateApi
//        ];
//    }

//    if ($user > 0) {
//        $formula = "用户数量费用(天):({$day}*({$statistics['user_num']}*{$priceCf['user']}))";
//        $msg .= "{$formula}+";
//        $ext['user'] = [
//            'formula' => $formula,
//            'amount' => $user
//        ];
//    }

//    if ($product > 0) {
//        $formula = "产品数量费用(天):({$day}*({$statistics['product_num']}*{$priceCf['product']}))";
//        $msg .= "{$formula}+";
//        $ext['product'] = [
//            'formula' => $formula,
//            'amount' => $product
//        ];
//    }

//    if ($store > 0) {
//        $formula = "店铺数量费用(天):({$day}*({$statistics['store_num']}*{$priceCf['store']}))";
//        $msg .= "{$formula}+";
//        $ext['store'] = [
//            'formula' => $formula,
//            'amount' => $store
//        ];
//    }

//    if ($order > 0) {
//        $formula = "订单数量费用(天):({$day}*({$statistics['order_num']}*{$priceCf['order']}))";
//        $msg .= "{$formula}+";
//        $ext['order'] = [
//            'formula' => $formula,
//            'amount' => $order
//        ];
//    }

//    if ($purchase > 0) {
//        $formula = "采购数量费用(天):({$day}*({$statistics['purchase_num']}*{$priceCf['purchase']}))";
//        $msg .= "{$formula}+";
//        $ext['purchase'] = [
//            'formula' => $formula,
//            'amount' => $purchase
//        ];
//    }

//    if ($waybill > 0) {
//        $formula = "运单数量费用(天):({$day}*({$statistics['waybill_num']}*{$priceCf['waybill']}))+";
//        $msg .= "{$formula}+";
//        $ext['waybill'] = [
//            'formula' => $formula,
//            'amount' => $waybill
//        ];
//    }

//    $msg = trim($msg, '+').'】';
//    $costArr =compact(['picApi','translateApi','user','product','store','order','purchase','waybill','network']);
//    return compact(['costArr','msg','ext']);
//}

///**
// * Notes:curl抓取数据(POST方式)
// * User: wanghousheng
// * Date: 2019/4/27
// * @param       $url
// * @param       $data
// * @param array $headers
// * @return mixed
// */
//function urlPost($url, $data, $headers = [])
//{
//    $curl = curl_init(); // 启动一个CURL会话
//    curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
//    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
//    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2); // 从证书中检查SSL加密算法是否存在
//    curl_setopt($curl, CURLOPT_SSLVERSION, 4);
//    curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36'); // 模拟用户使用的浏览器
//    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
//    curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
//    curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
//    curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
//    curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
//    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
//    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
//    $tmpInfo = curl_exec($curl); // 执行操作
////    if(curl_exec($curl) === false)
////    {
////        curl_error($curl);
////    }
//    curl_close($curl); // 关键CURL会话
//    return $tmpInfo; // 返回数据
//}

///**
// * 判断需要花费余额操作
// * User: CCY
// * Date: 2021/3/29
// * @param $company companyservice->getCurrentConfig()
// * @param $level   $this->sessionData->getLevel()
// * @return int 1 通过 2弹窗跳转充值页面 3弹窗提醒上级划分余额
// */
//function getBlanceType($company, $level)
//{
//    //盘他无限抠图
//    if($company['show_oem_id'] == 11){
//        return 1;
//    }

//    if ($company['type'] == Company::TYPE_TWO || $company['balance'] > 0) {
//        return 1;
//    }
//    if ($company['oem_id'] == User::OEM_ID || ($company['oem_id'] != User::OEM_ID && $company['second_package_oem'] == Company::SECOND_PACKAGE_OEM_FALSE && $level < 3)) {
//        return 2;
//    }
//    return 3;
//}


//function getFilesize($num)
//{
//    $p = 0;
//    $format = 'bytes';
//    if ($num > 0 && $num < 1024) {
//        $p = 0;
//        return number_format($num) . ' ' . $format;
//    }
//    if ($num >= 1024 && $num < pow(1024, 2)) {
//        $p = 1;
//        $format = 'KB';
//    }
//    if ($num >= pow(1024, 2) && $num < pow(1024, 3)) {
//        $p = 2;
//        $format = 'MB';
//    }
//    if ($num >= pow(1024, 3) && $num < pow(1024, 4)) {
//        $p = 3;
//        $format = 'GB';
//    }
//    if ($num >= pow(1024, 4) && $num < pow(1024, 5)) {
//        $p = 3;
//        $format = 'TB';
//    }
//    $num /= pow(1024, $p);

//    return number_format($num, 3) . ' ' . $format;
//}

//function perm(array $names, $count,array &$out, $l = 0)
//{
//    if ($l === $count - 1) {
//        $out[] = $names;
//    } else {
//        for($i = $l; $i < $count; $i++) {
//            swap($names[$i], $names[$l]);
//            perm($names, $count, $out, $l + 1);
//            swap($names[$i], $names[$l]);
//        }
//    }
//}

//function swap(&$a, &$b)
//{
//    $c = $a;
//    $a = $b;
//    $b = $c;
//}
