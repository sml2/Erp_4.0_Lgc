﻿
using ERP.Services.Api.ExchangeRate;
using ERP.Services.Api.Logistics;
using ERP.Services.Api;
using ERP.Services.Api.Logistics.Detail;
using ERP.Services.DB.Logistics;
using OrderSDK.Services;

namespace ERP.Extensions;


public static partial class IServiceCollectionExtensions
{
    public static IServiceCollection AddApiServices(this IServiceCollection Services)
    {
        Services.AddSingleton<Aliyun>();
        //Services.AddSingleton<OrderAmazonMWS>();
        //Services.AddSingleton<OrderShopee>();
        //Services.AddSingleton<IOrderFactory, Orders>();

        #region Amazon
        Services.AddSingleton<IOrderFactory, Orders>();

        Services.AddOrderApi();
       // Services.AddScoped<AmazonSpClient>();
   
        //Services.AddScoped<OrderService>();
        //Services.AddScoped<ShippingService>();
        //Services.AddScoped<LWAClient>();
        //Services.AddScoped<LWAAuthorizationCredentials>();
        #endregion

        #region shopee
        Services.AddTransient<ShopeeClient>();
        //Services.AddSingleton<shopeeAuth>();
        //Services.AddSingleton<GlobalProduct>();
        //Services.AddSingleton<Logistic>();
        //Services.AddSingleton<MediaSpace>();
        //Services.AddSingleton<shopeeOrder>();
        //Services.AddSingleton<Payment>();
        //Services.AddSingleton<shopeeProduct>();
        //Services.AddSingleton<shopeePublic>();
        //Services.AddSingleton<shopeeShop>();
        #endregion

        #region Logistic
        Services.AddSingleton<LogisticClient>();
        Services.AddTransient<ShipType>();
        Services.AddSingleton<YH>();
        Services.AddSingleton<DYGJ>().AddSingleton<HZGJ>();//CNE
        Services.AddSingleton<DEPPON>();
        Services.AddSingleton<EMS>();
        Services.AddSingleton<EQuick>();
        Services.AddSingleton<FPX>();
        Services.AddSingleton<HuaLei_YH>(); Services.AddSingleton<HuaLei_OWN>();
        Services.AddSingleton<BZY>().AddSingleton<HL>().AddSingleton<JHGJ>();//K5
        Services.AddSingleton<KJHYXB>();
        Services.AddSingleton<JFCC>().AddSingleton<SDGJ>().AddSingleton<YDH>().AddSingleton<ZHXT>();//RTB
        Services.AddSingleton<SFGJ>();
        Services.AddSingleton<SFC>();
        Services.AddSingleton<UBI>();
        Services.AddSingleton<WANB>();
        Services.AddSingleton<Y7F>();
        Services.AddSingleton<YanWen>();
        Services.AddSingleton<YunTu>();        
        Services.AddSingleton<Factory>();
        #endregion
        return Services;
    }
}
