﻿using Prometheus.SystemMetrics;

namespace ERP.Extensions;


public static partial class IServiceCollectionExtensions
{
    public static IServiceCollection AddLoggingServices(this IServiceCollection Services)
    {
        Services.AddSystemMetrics();
        Services.AddLogging(log =>
        {
            log.AddLog4Net("Config/log4net.Config");
        });
        return Services;
    }
}
