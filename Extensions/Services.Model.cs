﻿using ERP.Data;
using ERP.Data.Products;
using ERP.Distribution.Abstraction.Services;
using ERP.Models.View.Purchase;
using ERP.Services;
using ERP.Services.DB.Setting;
using ERP.Services.Export;
using ERP.Services.Distribution;
using ERP.Services.Finance;
using ERP.Services.DB.Logistics;
using ERP.Services.Message;
using ERP.Services.DB.Orders;
using ERP.Services.IntegralBalance;
using ERP.Services.Product;
using ERP.Services.Purchase;
using ERP.Services.Setting;
using ERP.Services.Statistics;
using ERP.Services.Stores;
using ERP.Services.DB.Users;
using CompanyService = ERP.Services.DB.Users.CompanyService;
using ERP.Interface.Services;
using ERP.Services.DataStatistics;
using ERP.Services.DB.B2C;
using ERP.Services.DB.CompanyResource;
using ERP.Services.DB.Pay;
using ERP.Services.Images;
using ERP.Services.Upload;
using BaseService = ERP.Services.Upload.BaseService;
using ERP.Services.DB.Stores;

using ProductService = ERP.Services.DB.Product.Product;
using ERP.Services.DB.ProductType;
using ERP.Services.DB.Sms;
using ERP.Services.DB.Sms.Ytx;
using categoryItemService= ERP.Services.DB.ProductType.CategoryService;
using productTypeItemService = ERP.Services.DB.ProductType.ProductTypeService;
using CategoryService = ERP.Services.Product.CategoryService;
using ERP.Services.DB.Product;
using ERP.Services.Excel.ExcelTemplate;
using AmazonExcel = ERP.Services.Excel.ExcelTemplate.Amazon;
using ExcelFactory= ERP.Services.Excel.ExcelFactory;

namespace ERP.Extensions;
public static partial class IServiceCollectionExtensions
{
    public static IServiceCollection AddModelServices(this IServiceCollection Services)
    {
        #region User
        Services.AddScoped<RuleTemplate>();
        Services.AddScoped<CompanyService>();
        Services.AddScoped<ICompanyService, CompanyService>();
        Services.AddScoped<UserService>();
        Services.AddScoped<GroupService>();
        Services.AddScoped<CompanyOem>();
        Services.AddScoped<UserValidity>();
        Services.AddScoped<UserOperateLogService>();
        Services.AddScoped<UserOnlineService>();
        Services.AddScoped<LoginLogService>();
        Services.AddScoped<PackagesResourceService>();
        Services.AddScoped<RegisterService>();
        #endregion

        #region Setting
        Services.AddScoped<AmazonKey>();
        Services.AddScoped<AmazonSPKey>();
        Services.AddScoped<ShopeeKey>();
        Services.AddScoped<DictionaryService>();
        Services.AddScoped<LanguageUnitService>();
        Services.AddScoped<Nation>();
        Services.AddScoped<FileBucketService>();
        Services.AddScoped<OemService>();
        Services.AddScoped<RdsService>();
        Services.AddScoped<RedisService>();
        Services.AddScoped<Unit>();
        Services.AddScoped<StatisticsPlatformByDayService>();
        Services.AddScoped<StatisticsPlatformByMonthService>();
        Services.AddScoped<Menu>();
        Services.AddScoped<TrendService>();
        Services.AddScoped<ChartService>();
        Services.AddScoped<IntegralBalanceService>();
        Services.AddScoped<LanguagesService>();
        Services.AddScoped<PackagesService>();
        #endregion

        #region Export
        Services.AddScoped<OrderBackgroundExporter>();
        Services.AddScoped<ExportProductService>();
        Services.AddScoped<ExportService>();
        Services.AddScoped<SyncExportProductService>();
        #endregion

        #region Purchase
        Services.AddScoped<AffirmPurchase>();
        Services.AddScoped<MyPurchase>();
        Services.AddScoped<Purchase>();
        Services.AddScoped<PurchasingChannels>();
        Services.AddScoped<Supplier>();
        #endregion

        #region Logistics             
        Services.AddScoped<OperationLogging>();
        Services.AddScoped<LogisticsParam>();
        Services.AddScoped<Shipper>();
        Services.AddScoped<ShipOrder>();
        Services.AddScoped<ShipRatesEstimationTemplate>();
        Services.AddScoped<ShipTemplate>();      
        Services.AddScoped<WayBill>();
        Services.AddScoped<WayBillLog>();
        Services.AddSingleton<WaybillFeeHost>();
        #endregion

        #region Store
        Services.AddScoped<StoreService>();
        Services.AddScoped<RemoteService>();
        Services.AddScoped<EanupcService>();
        Services.AddScoped<CategoryNodeDefault>();
        Services.AddScoped<StoreGroupService>();
        Services.AddScoped<AsinProduct>();
        #endregion

        #region Finance
        Services.AddScoped<BillLog>();
        Services.AddScoped<BillingRecordsService>();
        Services.AddScoped<FinancialAffairsService>();
        Services.AddScoped<StatisticsService>();
        Services.AddScoped<BillLogService>();
        Services.AddScoped<PayBillService>();
        #endregion

        #region Statistics
        Services.AddScoped<CounterCompanyBusinessService>();
        Services.AddScoped<CounterRealFinanceService>();
        Services.AddScoped<CounterUserBusinessService>();
        Services.AddScoped<CounterGroupBusinessService>();
        Services.AddScoped<CounterStoreBusinessService>();
        Services.AddScoped<CounterRequestService>();
        Services.AddScoped<Services.DB.Statistics.Statistic>();
        Services.AddScoped<StatisticMoney>();
        Services.AddScoped<RepairService>();
        #endregion

        #region Order
        Services.AddScoped<Order>();
        Services.AddSingleton<OrderHost>();
        Services.AddScoped<Services.DB.Orders.Action>();
        Services.AddScoped<Merge>();
        Services.AddScoped<Refund>();

        Services.AddSingleton<ExcelFactory>();        
        Services.AddSingleton<NormalOrder>();
        Services.AddSingleton<AmazonExcel>();
        #endregion

        #region
        Services.AddScoped<ERP.Services.DB.ProductType.CategoryService>();        
        #endregion

        #region Product
        Services.AddScoped<ExpandService>();
        Services.AddScoped<IllegalWordService>();
        Services.AddScoped<KeywordService>();
        Services.AddScoped<SketchService>();
        Services.AddScoped<ChangePriceService>();
        Services.AddScoped<CategoryService>();
        Services.AddScoped<ProductService>();
        Services.AddScoped<ERP.Services.Product.ProductService>();
        Services.AddScoped<UploadAmazonService>();
        Services.AddScoped<ProductUploadService>();
        Services.AddScoped<AuditService>();
        Services.AddScoped<ConvertProduct>();
        Services.AddScoped<ToSellService>();
        Services.AddScoped<TaskService>();
        Services.AddScoped<ToolsService>();
        Services.AddScoped<TempImagesService>();
        Services.AddScoped<ImagesResourceService>();
        Services.AddScoped<TranslationService>();
        Services.AddScoped<ImagesService>();
        #endregion

        #region ProductType
        Services.AddScoped<categoryItemService>();
        Services.AddScoped<productTypeItemService>();
        Services.AddSingleton<ProductHost>();
        #endregion

        #region Dictionary
        Services.AddScoped<DictionaryService>();
        #endregion

        #region Message

        Services.AddScoped<NoticeService>();
        Services.AddScoped<WorkOrderService>();
        Services.AddScoped<WorkOrderCategoryService>();
        Services.AddScoped<WorkOrderProgressService>();
        Services.AddScoped<WorkOrderReplyService>();
        
        Services.AddScoped<UserEmailService>();
        Services.AddScoped<EmailInfoService>();
        Services.AddScoped<EmailMassService>();
        
        Services.AddScoped<FeedBackService>();
        Services.AddScoped<FeedBackReplyService>();

        #endregion

        #region Distribution

        Services.AddScoped<IDistributionFinanceService, FinanceService>();
        Services.AddScoped<PurchaseService>();
        Services.AddScoped<CartService>();
        Services.AddScoped<AmazonOrderService>();

        #endregion

        #region Operations
        Services.AddScoped<Services.DB.Operations.OperationService>();
        #endregion

        Services.AddStorage<DBContext>();
        
        Services.AddDistribution<DBContext>();
        
        Services.AddScoped<UserService>();
        Services.AddScoped<Order>();

        Services.AddScoped<Services.DB.PlatformData>();
        Services.AddScoped<Services.DB.Setting.Unit>();
        Services.AddScoped<Services.DB.Setting.Nation>();
        Services.AddScoped<Services.DB.Setting.AmazonKey>();
        #region mapper
        Services.AddAutoMapper(config =>
        {
            config.CreateMap<ViewModels.Storage.WarehouseViewModel, Models.DB.Storage.Warehouse>();
            config.CreateMap<Models.DB.Purchase.Purchase, DistributionPurchaseListDto>()
                .ForMember(p => p.Price, c => c.MapFrom(p => p.Price.ToString("CNY")))
                .ForMember(p => p.PurchasePrice, c => c.MapFrom(p => p.PurchasePrice.ToString("CNY")))
                .ForMember(p => p.PurchaseTotal, c => c.MapFrom(p => p.PurchaseTotal.ToString("CNY")));
        });
        #endregion

        #region Upload
        Services.AddScoped<UploadService>();      
        #endregion
        
        #region Sms
        Services.AddScoped<Rest>();      
        Services.AddScoped<SendService>();      
        #endregion

        #region B2C
        Services.AddScoped<DomainService>();
        Services.AddScoped<SelectionService>();
        Services.AddScoped<ReleaseService>();
        Services.AddScoped<SituneShopService>();
        Services.AddScoped<B2CUsersService>();
        #endregion
        
        Services.AddScoped<AlipayService>();
        Services.AddScoped<ProductValidityService>();
        
        #region NoSessionService
        Services.AddScoped<OrderNoSessionService>();
        Services.AddScoped<CompanyNoSessionService>();
        Services.AddScoped<LogisticsNoSessionService>();
        Services.AddScoped<WaybillNoSessionService>();
        Services.AddScoped<UserNotSessionService>();
        Services.AddScoped<ShippingFeeService>();

        #endregion
        
        return Services;
    }
}
