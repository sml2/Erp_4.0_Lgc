using ERP.Data;
using ERP.Graphql;
using HotChocolate.Data.Sorting;
using HotChocolate.Language;
using HotChocolate.Types.Pagination;
using Microsoft.EntityFrameworkCore;
using DateTimeType = ERP.Graphql.Scalars.DateTimeType;

namespace ERP.Extensions;


public static partial class IServiceCollectionExtensions
{
    /// <summary>
    /// 数据库
    /// </summary>
    /// <param name="Services"></param>
    /// <param name="Builder"></param>
    /// <returns></returns>
    /// <exception cref="ArgumentException"></exception>z
    public static IServiceCollection AddDBContext(this IServiceCollection Services, IConfiguration Configuration)
    {
        var database = Configuration.GetConnectionString("database") ?? "mysql";
        if (database == "test")
            return Services;
        var connectionStr = Configuration.GetConnectionString(database);

        Services.AddDbContext<DBContext>(options =>
        {
            _ = database.ToUpper() switch
            {
                // "MYSQL" => options.UseMySql(connectionStr, ServerVersion.AutoDetect(connectionStr)).EnableSensitiveDataLogging().EnableDetailedErrors().LogTo(s=>Console.WriteLine(s)),
                "MYSQL" => options.UseMySql(connectionStr, ServerVersion.AutoDetect(connectionStr)).EnableSensitiveDataLogging().EnableDetailedErrors(),
                "SQLITE" => options.UseSqlite(connectionStr),
                //"SQLSERVER" => options.UseSqlServer(connectionStr),
                _ => throw new ArgumentException($"Unsupported DatabaseDriver:{database}")
            };
        });
        // Services.AddScoped<type>(sp => sp.GetRequiredService<DBContext>());
        Services.AddSha256DocumentHashProvider(HashFormat.Hex).AddGraphQLServer()
            .AddAuthorization()
            .AddFiltering()
            .AddSorting()
            .UseAutomaticPersistedQueryPipeline()
            .AddInMemoryQueryStorage()
            .SetPagingOptions(new PagingOptions()
            {
                MaxPageSize = 100,
                DefaultPageSize = 10,
                IncludeTotalCount = true,
            })
            .AddQueryType<Query>();
        return Services;
    }
}
