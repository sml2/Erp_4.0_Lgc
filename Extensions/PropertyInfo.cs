﻿namespace ERP.Extensions;
using Attributes.Logistics;
using System.Reflection;
public static class PropertyInfoExtension
{
    public static ViewConfigParameter? GetViewConfig(this PropertyInfo propertyInfo) => propertyInfo.GetCustomAttribute(typeof(ViewConfigParameter), false) as ViewConfigParameter;
}
