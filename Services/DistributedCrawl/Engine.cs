﻿namespace ERP.Services.DistributedCrawl;

using ERP.Data;
using UIConfigModel = ViewModels.DistributedCrawl.UIConfig; 
/// <summary>
/// 分布式采集引擎
/// </summary>
public class Engine
{
    readonly DBContext DbContext;
    public Engine(DBContext myDbContext) {
        DbContext = myDbContext;
    }
    /// <summary>
    /// 构建用户任务配置信息
    /// </summary>
    /// <returns></returns>
    public async ValueTask<UIConfigModel> BuildUIConfig() { 
        await DbContext.SaveChangesAsync();
        return new();
    }
}

