﻿using ERP.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Model = ERP.Models.DB.Users.Company;
using Service = ERP.Services.DB.Users.CompanyService;
namespace ERP.Services.Caches;

/// <summary>
/// 国家缓存
/// </summary>
public class Company : BaseCache<Model>
{
    public Company(IOptions<MemoryCacheOptions> optionsAccessor, ILogger<Company> logger, IServiceProvider serviceProvider)
        : base(optionsAccessor, logger, serviceProvider)
        => Init(ServiceProvider.GetRequiredService<DBContext>().Company.AsNoTracking().ToListAsync().Result);
    protected override int AddInternal(Model m) => AddInternal(m, m => m.ID);
    protected override bool Predicate(Model model, Model other) => model.ID == other.ID;
}

