﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Model = ERP.Models.Setting.Nation;
using Service = ERP.Services.DB.Setting.Nation;
namespace ERP.Services.Caches;

/// <summary>
/// 国家缓存
/// </summary>
public class Nation : BaseCache<Model>
{
    public Nation(IOptions<MemoryCacheOptions> optionsAccessor, ILogger<Nation> logger, IServiceProvider serviceProvider)
        : base(optionsAccessor, logger, serviceProvider)
        => Init(ServiceProvider.GetRequiredService<Service>().Load().Result);
    public Model? Get(string Key)
    {
        if (Key.Length > 1 && TryGetValue(Key, out var obj))
            return (Model)obj;
        logger.LogInformation("Not Found Key:[{Key}]", Key);
        return null;
    }


    public Model? Get(int Id)
    {
      return List()!.Where(x => x.ID == Id).FirstOrDefault();
    }

    public Model? GetByName(string Name)
    {
        return List()!.Where(x => x.Name==Name).FirstOrDefault();
    }

    protected override int AddInternal(Model m) => AddInternal(m, m => m.ID, m => m.Short);
    protected override bool Predicate(Model model, Model other) => model.ID == other.ID;

}

