﻿ using ERP.Data;
using ERP.Models.Setting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
namespace ERP.Services.Caches;

/// <summary>
/// 邮箱配置缓存
/// </summary>
public class EmailConfigCache : BaseCache<EmailConfig>
{
    public EmailConfigCache(IOptions<MemoryCacheOptions> optionsAccessor, ILogger<EmailConfigCache> logger, IServiceProvider serviceProvider)
        : base(optionsAccessor, logger, serviceProvider)
        => Init(ServiceProvider.GetRequiredService<DBContext>().EmailConfig.ToListAsync().Result);
    
    public EmailConfig? Get(string Key)
    {
        if (Key.Length > 1 && TryGetValue(Key, out var obj))
            return (EmailConfig)obj;
        logger.LogInformation("Not Found Key:[{Key}]", Key);
        return null;
    }

    protected override int AddInternal(EmailConfig m) => AddInternal(m, m => m.ID);
    protected override bool Predicate(EmailConfig model, EmailConfig other) => model.ID == other.ID;

}

