﻿using System;
using System.Collections.Concurrent;
using ERP.Services.Api.Amazon;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;

namespace ERP.Services.Caches;

/// <summary>
/// 用于队列间通讯（List与Get）
/// </summary>
public class WaitOrders : MemoryCache
{
    public WaitOrders(IOptions<MemoryCacheOptions> optionsAccessor) : base(optionsAccessor)
    {
    }
    public ConcurrentQueue<WaitOrder> Get(int ID) {
        return this.GetOrCreate(ID, o => new ConcurrentQueue<WaitOrder>());
    }
}

