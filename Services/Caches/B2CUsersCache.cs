using ERP.Data;
using ERP.Models.Abstract;
using ERP.Services.DB.B2C;
using ERP.Services.Setting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;

using Model = ERP.Models.B2C.B2CUserModel;

namespace ERP.Services.Caches;

public class B2CUsersCache  : BaseCache<Model>
{
    public B2CUsersCache(IOptions<MemoryCacheOptions> optionsAccessor, ILogger<AmazonKey> logger,
        IServiceProvider serviceProvider)
        : base(optionsAccessor, logger, serviceProvider)
        => Init(ServiceProvider.GetRequiredService<DBContext>().B2CUser.Where(m => m.Status == BaseModel.StateEnum.Open).AsNoTracking().ToListAsync().Result);

    protected override int AddInternal(Model m) => AddInternal(m, m => m.ID);
    protected override bool Predicate(Model model, Model other) => model.ID == other.ID;
}