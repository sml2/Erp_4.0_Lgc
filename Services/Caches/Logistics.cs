using ERP.Interface;
using ERP.Models.Logistics.ViewModels.Parameter;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
namespace ERP.Services.Caches;

using Attributes.Logistics;
using Extensions;
using Services.Api.Logistics;
using Model = Models.PlatformData.LogisticsData;
public class Logistics : BaseCache<Model>
{
    public Logistics(IOptions<MemoryCacheOptions> optionsAccessor, ILogger<Logistics> logger, IServiceProvider serviceProvider)
        : base(optionsAccessor, logger, serviceProvider) {
        var PlatformData = ServiceProvider.GetRequiredService<PlatformData>();
        var Lst = PlatformData.ListMask(Platforms.LogisticMask);
        var Factory = ServiceProvider.GetRequiredService<Factory>();
        var WithCaches = Lst.Select(o => {
            var data = o.GetLogisticsData()!;
            var LogLgc = Factory.Get(o.Platform)!;
            List<ViewConfigParameter> ps = new();
            foreach (var item in LogLgc.GetParameters())
            {
                var vcp = item.GetViewConfig();
                if (vcp is not null) {
                    ps.Add(vcp);
                }
            }
            data.SetParameterConfigCache(ps.OrderBy(p => p.Sort).Select(p => new DataConfigVm(p)));
            return data;
        });
        Init(WithCaches);
    }
    protected override int AddInternal(Model m) => AddInternal(m, m =>(int)m.Platform, m => m.Platform);

    protected override bool Predicate(Model model, Model other) => model.Platform == other.Platform;
    
    
    public override IList<Model>? List()
    {
        var oemProvider = ServiceProvider.CreateScope().ServiceProvider.GetRequiredService<IOemProvider>();

        
        if (TryGetValue(LoadAllKey, out var obj))
        {
            IList<Model> data = new List<Model>();
            var list = (IList<Model>)obj;

            if (oemProvider.OEM!.ID == (int)Enums.ID.OEM.YongHe)
            {
                return list.Where(m => m.Name == Platforms.HuaLei_YH.GetDescription()).ToList();
            }

            return list;

        }
        logger.LogInformation("Not Found Key:[{Key}]", LoadAllKey);
        return null;
    }
}

