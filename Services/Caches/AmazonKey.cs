using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Model = ERP.Models.Setting.AmazonKey;
using ModelSP = ERP.Models.Setting.AmazonSPKey;
using ModelShopee = ERP.Models.Setting.ShopeeKey;

using Service = ERP.Services.DB.Setting.AmazonKey;
using ServiceSP = ERP.Services.DB.Setting.AmazonSPKey;
using ServiceShopee = ERP.Services.DB.Setting.ShopeeKey;

namespace ERP.Services.Caches;

/// <summary>
/// 亚马逊开发者密钥
/// </summary>
public class AmazonKey : BaseCache<Model>
{
    public AmazonKey(IOptions<MemoryCacheOptions> optionsAccessor, ILogger<AmazonKey> logger, IServiceProvider serviceProvider) 
        : base(optionsAccessor, logger, serviceProvider) 
        =>Init(ServiceProvider.GetRequiredService<Service>().Load().Result);
    protected override int AddInternal(Model m) => AddInternal(m, m => m.ID);
    protected override bool Predicate(Model model, Model other) => model.ID == other.ID;
}


public class AmazonSPKey : BaseCache<ModelSP>
{
    public AmazonSPKey(IOptions<MemoryCacheOptions> optionsAccessor, ILogger<AmazonSPKey> logger, IServiceProvider serviceProvider)
        : base(optionsAccessor, logger, serviceProvider)
        => Init(ServiceProvider.GetRequiredService<ServiceSP>().Load().Result);
    protected override int AddInternal(ModelSP m) => AddInternal(m, m => m.ID);
    protected override bool Predicate(ModelSP model, ModelSP other) => model.ID == other.ID;
}



public class ShopeeKey : BaseCache<ModelShopee>
{
    public ShopeeKey(IOptions<MemoryCacheOptions> optionsAccessor, ILogger<ShopeeKey> logger, IServiceProvider serviceProvider)
        : base(optionsAccessor, logger, serviceProvider)
        => Init(ServiceProvider.GetRequiredService<ServiceShopee>().Load().Result);
    protected override int AddInternal(ModelShopee m) => AddInternal(m, m => m.ID);
    protected override bool Predicate(ModelShopee model, ModelShopee other) => model.ID == other.ID;
}

