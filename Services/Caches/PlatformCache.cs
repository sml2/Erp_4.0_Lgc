using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Model = ERP.Models.Setting.Platform;
using Service = ERP.Services.Setting.DictionaryService;

namespace ERP.Services.Caches;

public class PlatformCache : BaseCache<Model>
{
    public PlatformCache(IOptions<MemoryCacheOptions> optionsAccessor, ILogger<AmazonKey> logger,
        IServiceProvider serviceProvider)
        : base(optionsAccessor, logger, serviceProvider)
        => Init(ServiceProvider.GetRequiredService<Service>().Load().Result);

    protected override int AddInternal(Model m) => AddInternal(m, m => m.ID);
    protected override bool Predicate(Model model, Model other) => model.ID == other.ID;
}