﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Model = ERP.Models.Setting.Unit;
using Service = ERP.Services.DB.Setting.Unit;
using ERP.Extensions;
namespace ERP.Services.Caches;

/// <summary>
/// 货币汇率
/// </summary>
public class Unit : BaseCache<Model>
{
    public Unit(IOptions<MemoryCacheOptions> optionsAccessor, ILogger<Unit> logger, IServiceProvider serviceProvider) 
        : base(optionsAccessor, logger, serviceProvider)
         => Init(ServiceProvider.GetRequiredService<Service>().Load().Result);

    public Model? Get(string Key)
    {
        if (Key.Length > 1 && TryGetValue(Key, out var obj))
            return (Model)obj;
        logger.LogInformation("Not Found Key:[{Key}]", Key);
        return null;
    }

    protected override int AddInternal(Model m) => AddInternal(m, m => m.ID, m => m.Sign);
    protected override bool Predicate(Model model, Model other) => model.ID == other.ID;
}

