﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Model = ERP.Models.Setting.OEM;
using Service = ERP.Services.Setting.OemService;
using ERP.Data;
using Microsoft.EntityFrameworkCore;

namespace ERP.Services.Caches;

/// <summary>
/// oem缓存
/// </summary>
public class OEM : BaseCache<Model>
{
    public OEM(IOptions<MemoryCacheOptions> optionsAccessor, ILogger<OEM> logger, IServiceProvider serviceProvider)
        : base(optionsAccessor, logger, serviceProvider)
        => Init(ServiceProvider.GetRequiredService<DBContext>().OEM.Include(m => m.Oss).AsNoTracking().ToListAsync().Result);
    public Model? Get(string Key)
    {
        if (Key.Length > 1 && TryGetValue(Key, out var obj))
            return (Model)obj;
        logger.LogInformation("Not Found Key:[{Key}]", Key);
        return default;
    }
    protected override int AddInternal(Model m) => AddInternal(m, m => m.ID, m => m.HashCodes, m => m.Name);
    protected override bool Predicate(Model model, Model other) => model.ID == other.ID;
}

