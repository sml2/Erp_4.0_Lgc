using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Model = ERP.Models.Stores.StoreRegion;
using ModelMarket = ERP.Models.Stores.StoreMarket;
using UserModel = ERP.Models.DB.Identity.User;
using CompanyModel = ERP.Models.DB.Users.Company;
using ERP.Data;
using Microsoft.EntityFrameworkCore;
using static ERP.Models.Abstract.BaseModel;

namespace ERP.Services.Caches;

/// <summary>
/// 店铺缓存
/// </summary>
public class Store : BaseCache<Model>
{
    public Store(IOptions<MemoryCacheOptions> optionsAccessor, ILogger<Store> logger, IServiceProvider serviceProvider)
        : base(optionsAccessor, logger, serviceProvider)
        =>Init(ServiceProvider.GetRequiredService<DBContext>().StoreRegion.AsNoTracking().ToListAsync().Result);
    internal IList<Model> List(UserModel user)
    {
        return List(Types.User, user.ID);
    }
    internal IList<Model> List(CompanyModel company)
    {
        return List(Types.Company, company.ID);
    }
    public enum Types { User, Company }
    internal IList<Model> List(Types type, int id)
    {
        if (type == Types.Company)
        {
            return List()!.Where(s => s.CompanyID == id).ToList();
        }
        
        return List()!.Where(s => s.UserID == id).ToList();
    }
    protected override int AddInternal(Model m) => AddInternal(m, m => m.ID/*,m=> m.ID.ToString()*/);
    //public Model? Get(string Key)
    //{
    //    if (Key.Length > 1 && TryGetValue(Key, out var obj))
    //        return (Model)obj;
    //    logger.LogInformation("Not Found Key:[{Key}]", Key);
    //    return null;
    //}
    protected override bool Predicate(Model model, Model other) => model.ID == other.ID;
}



public class StoreMarket : BaseCache<ModelMarket>
{
    public StoreMarket(IOptions<MemoryCacheOptions> optionsAccessor, ILogger<StoreMarket> logger, IServiceProvider serviceProvider)
        : base(optionsAccessor, logger, serviceProvider)
        => Init(ServiceProvider.GetRequiredService<DBContext>().StoreMarket.AsNoTracking().ToListAsync().Result);

    internal IList<ModelMarket> List(ulong hash)
    {
        return List()!.Where(s => s.UniqueHashCode == hash && s.State == StateEnum.Open).ToList();
    }

    protected override int AddInternal(ModelMarket m) => AddInternal(m, m => m.ID/*,m=> m.ID.ToString()*/);
    //public Model? Get(string Key)
    //{
    //    if (Key.Length > 1 && TryGetValue(Key, out var obj))
    //        return (Model)obj;
    //    logger.LogInformation("Not Found Key:[{Key}]", Key);
    //    return null;
    //}
    protected override bool Predicate(ModelMarket model, ModelMarket other) => model.ID == other.ID;


    internal void AddList(List<ModelMarket> markets)
    {        
        if (markets != null && markets.Count() > 0)
        {
            markets.ForEach(x => Add(x));
        }
    }

    internal void AddList(ulong hash, List<ModelMarket> markets)
    {
        RemoveList(hash);
        if (markets != null && markets.Count() > 0)
        {
            markets.ForEach(x => Add(x));
        }
    }

    internal void RemoveList(ulong hash)
    {
        var lst = List(hash);
        if (lst != null && lst.Count() > 0)
        {
            lst.ForEach(x => Remove(x));
        }
    }
}
