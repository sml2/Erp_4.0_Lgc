﻿using ERP.Data.Task;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
namespace ERP.Services.Caches
{
    /// <summary>
    /// 缓存
    /// </summary>
    public  class TaskList : MemoryCache // MemoryDistributedCache
    {
        protected readonly ILogger logger;
        //private const string AllListKey = nameof(AllListKey);
        public  static  List<State> AllList = new();
        //MemoryDistributedCacheOptions
        public TaskList(IOptions<MemoryCacheOptions> optionsAccessor, ILogger<TaskList> logger) : base(optionsAccessor)
        {
            this.logger = logger;            
            //CacheExtensions.Set(this, AllListKey, new Dictionary<string,List<State>>(), NeverRemoveOptions);
        }


        private static MemoryCacheEntryOptions NeverRemoveOptions = new() { Priority = CacheItemPriority.NeverRemove };
        public State Set(State  value)
        {
             var key =  value.GetHashCode();
            //List<State> Lst;
            //if (AllList.TryGetValue(value.ClassName, out var list))
            //{
            //    Lst = list;
            //}
            //else
            //{
            //    Lst = new();
            //    AllList.Add(value.ClassName, Lst);
            //}
            //Lst.Add(value);

            AllList.Add(value);

            logger.LogInformation($"Add Key[{key}] - [{value.ClassName}]");
            return CacheExtensions.Set(this, value.GetHashCode(), value, NeverRemoveOptions);
        }


        public virtual void Rremove()
        {
            Compact(1);
        }

    }
}

