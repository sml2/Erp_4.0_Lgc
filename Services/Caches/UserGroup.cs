using ERP.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
namespace ERP.Services.Caches;
using Model = Models.DB.Users.Group;
using Service = DB.Users.GroupService;
public class UserGroup : BaseCache<Model>
{
    public UserGroup(IOptions<MemoryCacheOptions> optionsAccessor, ILogger<Company> logger, IServiceProvider serviceProvider)
        : base(optionsAccessor, logger, serviceProvider)
        => Init(ServiceProvider.GetRequiredService<DBContext>().UserGroup.AsNoTracking().ToListAsync().Result);
    protected override int AddInternal(Model m) => AddInternal(m, m => m.ID);
    protected override bool Predicate(Model model, Model other) => model.ID == other.ID;
}