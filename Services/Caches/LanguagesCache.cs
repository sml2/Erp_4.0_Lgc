﻿using ERP.Services.Setting;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Model = ERP.Models.Setting.Language;


namespace ERP.Services.Caches;

/// <summary>
/// 亚马逊开发者密钥
/// </summary>
public class LanguagesCache : BaseCache<Model>
{
    public LanguagesCache(IOptions<MemoryCacheOptions> optionsAccessor, ILogger<AmazonKey> logger,
        IServiceProvider serviceProvider)
        : base(optionsAccessor, logger, serviceProvider)
        => Init(ServiceProvider.GetRequiredService<LanguagesService>().Load().Result);

    protected override int AddInternal(Model m) => AddInternal(m, m => m.ID);
    protected override bool Predicate(Model model, Model other) => model.ID == other.ID;
}