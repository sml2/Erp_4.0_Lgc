using ERP.Services.Setting;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;

using Model = ERP.Models.Setting.FileBucket;

namespace ERP.Services.Caches;

public class FileBucketCache  : BaseCache<Model>
{
    public FileBucketCache(IOptions<MemoryCacheOptions> optionsAccessor, ILogger<AmazonKey> logger,
        IServiceProvider serviceProvider)
        : base(optionsAccessor, logger, serviceProvider)
        => Init(ServiceProvider.GetRequiredService<FileBucketService>().Load().Result);

    protected override int AddInternal(Model m) => AddInternal(m, m => m.ID);
    protected override bool Predicate(Model model, Model other) => model.ID == other.ID;
}