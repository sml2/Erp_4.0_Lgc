﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Model = ERP.Models.DB.Setting.Menu;
using Service = ERP.Services.DB.Setting.Menu;
namespace ERP.Services.Caches;

/// <summary>
/// Menu缓存
/// </summary>
public class Menu : BaseCache<Model>
{
    public Menu(IOptions<MemoryCacheOptions> optionsAccessor, ILogger<Menu> logger, IServiceProvider serviceProvider)
        : base(optionsAccessor, logger, serviceProvider)
        => Init(ServiceProvider.GetRequiredService<Service>().Load().Result);
    public Model? Get(string Key)
    {
        if (Key.Length > 1 && TryGetValue(Key, out var obj))
            return (Model)obj;
        logger.LogInformation("Not Found Key:[{Key}]", Key);
        return default;
    }
    protected override int AddInternal(Model m) => AddInternal(m, m => m.ID, m => m.Name);
    protected override bool Predicate(Model model, Model other) => model.ID == other.ID;

    internal IEnumerable<Model>? ListClone()=>List()?.Select(m=>m.Clone());
    internal IEnumerable<Model> Menus(bool hasModule = false) => List()!.SelectMany(m => hasModule ? m.Children.Concat(new[] {m}) : m.Children);
}

