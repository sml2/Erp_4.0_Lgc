﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
namespace ERP.Services.Caches
{
    /// <summary>
    /// 缓存
    /// </summary>
    public abstract class BaseCache<TModel> : MemoryCache 
    {
        protected readonly ILogger logger;
        protected readonly IServiceProvider ServiceProvider;
        protected const string LoadAllKey = "_";
        protected BaseCache(IOptions<MemoryCacheOptions> optionsAccessor, ILogger logger, IServiceProvider serviceProvider) : base(optionsAccessor)
        {
            this.logger = logger;
            ServiceProvider = serviceProvider.CreateScope().ServiceProvider;
        }
        /// <summary>
        /// 设置缓存对象
        /// </summary>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        protected TItem Set<TItem>(object key, TItem value, MemoryCacheEntryOptions options)
        {
            return CacheExtensions.Set(this, key, value, options);
        } 
        private static MemoryCacheEntryOptions NeverRemoveOptions = new() { Priority = CacheItemPriority.NeverRemove };
        /// <summary>
        /// 设置常驻缓存对象
        /// </summary>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected TItem Set<TItem>(object key, TItem value)
        {
            return CacheExtensions.Set(this, key, value, NeverRemoveOptions);
        }
        public void Init(IEnumerable<TModel> enumerable) => Init(enumerable.ToList());
        public void Init(IList<TModel> Datas)
        {
            logger.LogDebug("Load Begin");
            Set(LoadAllKey, Datas);
            logger.LogDebug("Load List[{units.Count}]", Datas.Count);
            Datas.ForEach(data => {
                var c = AddInternal(data);
                logger.LogDebug("Load {data} * {num}", data, c);
            });
            logger.LogDebug("Load Complete,Current Cache Count Was [{Count}]", Count);
        }
        protected abstract bool Predicate(TModel model, TModel other);
        public void Add(TModel model)
        {
            logger.LogDebug("Add Begin");
            var lst = List()!;
            lock (lst) {
                var arr = lst.Where((m)=> Predicate(m, model)).ToArray();
                var num = arr.Length;
                if (num == 0)
                {
                    lst.Add(model);
                }
                else if (num == 1)
                {
                    lst[lst.IndexOf(arr[0])] = model;
                }
                else
                {
                    throw new InvalidOperationException("predicate match more");
                }
            }
            AddInternal(model);
            logger.LogDebug("Add Complete,Current Count Was [{Count}]", Count);
        }
        protected abstract int AddInternal(TModel model);
        protected int AddInternal(TModel model, Func<TModel, int> IDFactory, params Func<TModel, object?>[] KeyFactories) {
            Set(IDFactory(model), model);
            KeyFactories.ForEach(KeyFactory => {
                var key = KeyFactory(model);
                if (key is not null)
                {
                    //当Key为Array的时候,要循环设置key，方便domainHashes->OEM
                    if (key is IEnumerable<ulong> items)
                    {
                        foreach (var item in items)
                        {
                            Set(item, model);
                        }
                    }
                    else
                    {
                        Set(key, model);
                    }
                }
            });
            return KeyFactories.Length;
        }
        public TModel? Get(int ID)
        {//??? >1 => >0
            if (ID > 0 && TryGetValue(ID, out var obj))
                return (TModel)obj;
            logger.LogInformation("Not Found ID:[{ID}]", ID);
            return default;
        }
        public TModel? Get<T>(T Key)
        {
            if (TryGetValue(Key, out var obj))
                return (TModel)obj;
            logger.LogInformation("Not Found Key:[{Key}]", Key);
            return default;
        }
        public List<TModel?> Gets(List<int?> keys)
        {
            var results = new List<TModel?>();
            foreach(var key in keys)
            {
                if (TryGetValue(key, out var obj))
                {
                    results.Add((TModel)obj);
                }
                else
                {
                    logger.LogInformation(typeof(TModel)+"Not Found Key:[{Key}]", key);
                }                                
            }
            return results;
        }

        public List<TModel?> Gets(List<int> keys)
        {
            var results = new List<TModel?>();
            foreach (var key in keys)
            {
                if (TryGetValue(key, out var obj))
                {
                    results.Add((TModel)obj);
                }
                else
                {
                    logger.LogInformation(typeof(TModel) + "Not Found Key:[{Key}]", key);
                }
            }
            return results;
        }

        public virtual IList<TModel>? List()
        {
            if (TryGetValue(LoadAllKey, out var obj))
                return (IList<TModel>)obj;
            logger.LogInformation("Not Found Key:[{Key}]", LoadAllKey);
            return null;
        }

        public virtual void Rremove()
        {
            Compact(1);
        }
    }
}

