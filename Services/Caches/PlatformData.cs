using ERP.Enums;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Model = ERP.Models.PlatformData;

using ERP.Enums;
using Service = ERP.Services.DB.PlatformData;
namespace ERP.Services.Caches;

/// <summary>
/// 平台所在国家的配置数据
/// </summary>
public class PlatformData : BaseCache<Model>
{
    public PlatformData(IOptions<MemoryCacheOptions> optionsAccessor, ILogger<PlatformData> logger, IServiceProvider serviceProvider)
        : base(optionsAccessor, logger, serviceProvider)
        => Init(ServiceProvider.GetRequiredService<Service>().Load().Result);
    public IList<Model> List(Platforms platform)
    {
        return List()?.Where(a => a.Platform == platform).ToList()!;
    }   

    public IList<Model> List(List<Platforms> platforms)
    {
        return List()?.Where(a => platforms.Contains(a.Platform)).ToList()!;
    }   

    public IList<Model> ListMask(Platforms platforms)//
    {
        return List()?.Where(a => (a.Platform & platforms) != Platforms.None).ToList()!;
    }   
    
    public int? GetAmazonNationId(Platforms platform, string marketPlaceId)
    {       
        var lst = List()?.Where(a => a.Platform == platform).ToList()!;
        foreach (var l in lst)
        {
            if (l.GetAmazonData()!.Marketplace == marketPlaceId)
            {
                return l.NationId;
            }
        }
        return 0;
    }

    public string? GetHost(Platforms platform, string marketPlaceId)
    {
        var lst = List()?.Where(a => a.Platform == platform).ToList()!;
        foreach (var l in lst)
        {
            if (l.GetAmazonData()!.Marketplace == marketPlaceId)
            {
                return l.GetAmazonData()!.Host;
            }
        }
        return "";
    }

    public string? GetOrg(Platforms platform, string marketPlaceId)
    {
        var lst = List()?.Where(a => a.Platform == platform).ToList()!;
        foreach (var l in lst)
        {
            if (l.GetAmazonData()!.Marketplace == marketPlaceId)
            {
                return l.GetAmazonData()!.Org;
            }
        }
        return "";
    }

    public List<string> GetMarketIds(List<int?> platformIds)
    {
       return Gets(platformIds)!.Select(x => x.GetAmazonData()!.Marketplace).ToList();
    }


    public Model GetByNationId(Platforms platform, int? nationId)
    {
        return List()?.Where(a => a.Platform == platform && a.NationId== nationId).FirstOrDefault();
    }

    public IList<Model> GetByNationIds(Platforms platform, List<int?> nationIds)
    {
        return List()?.Where(a => a.Platform == platform && nationIds.Contains(a.NationId)).ToList()!;
    }

    protected override int AddInternal(Model m) => AddInternal(m, m => m.ID, m => m.Nation?.Short);
    protected override bool Predicate(Model model, Model other) => model.ID == other.ID;
}