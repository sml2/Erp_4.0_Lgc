﻿using ERP.Data;
using ERP.Enums.Orders;
using ERP.Extensions;
using ERP.Interface;
using ERP.Services.Stores;
using ERP.ViewModels.Finance;
using Microsoft.EntityFrameworkCore;
using NPOI.SS.Formula.PTG;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ERP.Models.DB.Orders.Order;
using NationCache = ERP.Services.Caches.Nation;
using OrderService = ERP.Services.DB.Orders.Order;

namespace ERP.Services.Excel.ExcelTemplate;

public class Amazon : ExcelBase
{    

    public Amazon(ILogger<Amazon> _logger, IServiceProvider ServiceProvider) : base(ExcelTemplateTypes.Amazon, _logger, ServiceProvider)
    {       
    }
           
    public override async Task<string> ImportExcel(ISheet sheet, int userid, int companyid, int groupid, int oemid)
    {
        //    var _storeService = ServiceProvider.GetRequiredService<StoreService>();
        //    var _dbContext = ServiceProvider.GetRequiredService<ERP.Data.DBContext>();
        //    var _nationCache = ServiceProvider.GetRequiredService<NationCache>();
        //    var _orderService = ServiceProvider.GetRequiredService<OrderService>();

        //    StringBuilder errMsg = new();
        //    List<Models.DB.Orders.Order> orders = new();
        //    Models.DB.Orders.Order orderRaw = new();

        //    // 该用户能操作的所有店铺
        //    var stores = await _storeService.CurrentUserGetStores(true);

        //    //获取Excel表格中除标题以外的所有数据源，转化为dataTable中的表格数据源
        //    for (var i = sheet.FirstRowNum + 1; i <= sheet.LastRowNum; i++)
        //    {
        //        var row = sheet.GetRow(i);
        //        if (row == null) continue; //没有数据的行默认是null　

        //        //订单号0	订单总价1 	币种2	订单时间3 	所属店铺4 	SellerID5	手续费6	   成本损失费7	产品图8	产品名称9 	asin10	
        //        //Sku11 	购买数量12	单价13	亚马逊运费14	总价15   	买家姓名16	电话号码17	邮编18	    国家19	州/省/区域20	城市21
        //        //区域22 	详细地址23 	详细地址24	详细地址25 	采购总价26	 国内物流追踪号27  	国际物流运单号28

        //        //订单号
        //        var orderNo = row.GetCell(0).ParseCell();
        //        int n = i + 1;
        //        if (string.IsNullOrWhiteSpace(orderRaw.OrderNo))  //新增订单 
        //        {                
        //            if (orderNo.IsNullOrWhiteSpace())
        //            {
        //                errMsg.AppendLine($"第{n}行,订单号为空;");
        //                continue;
        //            }
        //            else
        //            {
        //                if(orders.Any(x=>x.OrderNo== orderNo))
        //                {
        //                    errMsg.AppendLine($"第{n}行,订单号重复;");
        //                    continue;
        //                }
        //                else
        //                {                       
        //                    orderRaw = null;
        //                    orderRaw.OrderNo = orderNo;
        //                    orderRaw.OrderNoHash = Helpers.CreateHashCode(orderNo);
        //                    orderRaw.OperateUserId = userid;
        //                    orderRaw.CompanyID = companyid;
        //                    orderRaw.GroupID= groupid;
        //                    orderRaw.OEMID = oemid;
        //                    AddField(n, row, stores, orderRaw, _nationCache, errMsg);
        //                    AddProductField(n, row, orderRaw, errMsg);
        //                    orders.Add(orderRaw);                        
        //                }
        //            }                
        //        }
        //        else 
        //        {
        //            var order = orders.FirstOrDefault(x => x.OrderNo == orderNo);
        //            if (order !=null)
        //            {
        //                //更新产品信息
        //                AddProductField(n, row, order, errMsg);
        //            }
        //            else
        //            {
        //                throw new AggregateException($"{orderNo} 数据获取异常！");
        //            }
        //        }           
        //    }

        //    if (errMsg.Length > 0)
        //    {
        //        return errMsg.ToString();
        //    }
        //    if (orders.Count > 0)
        //    {
        //        var models = await _orderService.GetOrdersByHashs(orders.Select(x => x.OrderNoHash).ToList());
        //        if (models.Count > 0)
        //        {
        //            models.ForEach(x => errMsg.AppendLine($"订单编号：{x.OrderNo} 已经存在！"));
        //            return errMsg.ToString();
        //        }
        //        else
        //        {
        //            await _dbContext.Order.AddRangeAsync(orders);
        //            await _dbContext.SaveChangesAsync();
        //            return "";
        //        }
        //    }
        //    else
        //    {
        //        return "没有符合的数据！";
        //    }

        return "";
    }

    //public void AddField(int i, IRow? row, List<Models.DB.Stores.StoreRegion>? stores, Models.DB.Orders.Order orderRaw, NationCache _nationCache, StringBuilder errMsg)
    //{
    //    //币种
    //    var unit = row.GetCell(2).ParseCell();
    //    unit = unit.IsNullOrWhiteSpace() ? "CNY" : unit.ToUpper();

    //    //订单总价
    //    var orderTotalPrice = row.GetCell(1).ParseCell();
    //    if (!string.IsNullOrWhiteSpace(orderTotalPrice))
    //    {
    //        orderTotalPrice=orderTotalPrice!.Replace(unit, "");
    //        if (!decimal.TryParse(orderTotalPrice, out decimal totalPrice))
    //        {
    //            errMsg.AppendLine($"第{i}行,订单总价必须是正整数;");
    //            return;
    //        }
    //        else if (totalPrice <= 0)
    //        {
    //            errMsg.AppendLine($"第{i}行,订单总价必须是正整数;");
    //            return;
    //        }
    //        else
    //        {
    //            orderRaw.OrderTotal = new MoneyMeta(unit, totalPrice);
    //        }
    //    }

    //    //订单时间
    //    var datetimeStr = row.GetCell(3).ParseCell();
    //    if (!string.IsNullOrWhiteSpace(datetimeStr))
    //    {
    //        if (DateTime.TryParse(datetimeStr, out DateTime time))
    //        {
    //            orderRaw.CreateTime = time;
    //        }
    //        else
    //        {
    //            errMsg.AppendLine($"第{i}行,订单时间格式不对;");
    //            return;
    //        }
    //    }
    //    else
    //    {
    //        errMsg.AppendLine($"第{i}行,订单时间为空;");
    //        return;
    //    }

    //    //所属店铺	SellerID
    //    bool flag = false;
    //    string storeName = "";
    //    int storeID = 0;
    //    Platforms storePlatform = Platforms.OTHERS;
    //    var SellerID = row.GetCell(5).ParseCell().Trim();
    //    if (!string.IsNullOrWhiteSpace(SellerID))
    //    {
    //        foreach (var s in stores)
    //        {
    //            var amazon = s.GetAmazonData();
    //            if (amazon != null && amazon.SellerID == SellerID)
    //            {
    //                flag = true;
    //                storeName = s.Name;
    //                storeID = s.ID;
    //                storePlatform = s.Platform;
    //                break;
    //            }
    //        }
    //        if (!flag)
    //        {
    //            errMsg.AppendLine($"第{i}行,店铺[{SellerID}]不存在,请验证店铺信息再导入订单;");
    //            return;
    //        }
    //        else
    //        {
    //            orderRaw.StoreId = storeID;
    //            orderRaw.Plateform = storePlatform;
    //        }
    //    }
    //    else
    //    {
    //        storeName = row.GetCell(4).ParseCell();
    //        if (!string.IsNullOrWhiteSpace(storeName))
    //        {
    //            var s = stores.FirstOrDefault(s => s.Name == storeName);
    //            if (s == null)
    //            {
    //                errMsg.AppendLine($"第{i}行,店铺[{storeName}]不存在,请先添加店铺再导入订单;");
    //                return;
    //            }
    //            else
    //            {
    //                orderRaw.StoreId = s.ID;
    //                orderRaw.Plateform = s.Platform;
    //            }
    //        }
    //        else
    //        {
    //            errMsg.AppendLine($"第{i}行,店铺[{storeName}]为空;");
    //            return;
    //        }
    //    }

    //    //手续费	
    //    var feeStr = row.GetCell(6).ParseCell();
    //    if (!string.IsNullOrWhiteSpace(feeStr))
    //    {
    //        feeStr = feeStr!.Replace(unit, "");
    //        if (!decimal.TryParse(feeStr, out decimal fee))
    //        {
    //            errMsg.AppendLine($"第{i}行,订单总价必须是正整数;");
    //            return;
    //        }
    //        else if (fee <= 0)
    //        {
    //            errMsg.AppendLine($"第{i}行,订单总价必须是正整数;");
    //            return;
    //        }
    //        else
    //        {
    //            orderRaw.Fee = new MoneyMeta(unit, fee);
    //        }
    //    }

    //    //成本损失费
    //    var costFeeStr = row.GetCell(7).ParseCell();
    //    if (!string.IsNullOrWhiteSpace(costFeeStr))
    //    {
    //        costFeeStr = costFeeStr!.Replace(unit, "");
    //        if (!decimal.TryParse(costFeeStr, out decimal costFee))
    //        {
    //            errMsg.AppendLine($"第{i}行,订单总价必须是正整数;");
    //            return;
    //        }
    //        else if (costFee <= 0)
    //        {
    //            errMsg.AppendLine($"第{i}行,订单总价必须是正整数;");
    //            return;
    //        }
    //        else
    //        {
    //            orderRaw.Loss = new MoneyMeta(unit, costFee);
    //        }
    //    }

    //    //买家姓名
    //    var orderName = row.GetCell(5).ParseCell();
    //    if (!string.IsNullOrWhiteSpace(orderName))
    //    {
    //        errMsg.AppendLine($"第{i}行,请填写买家姓名;");
    //        return;
    //    }

    //    //电话号码
    //    var phone = row.GetCell(6).ParseCell();
    //    if (!string.IsNullOrWhiteSpace(phone))
    //    {
    //        errMsg.AppendLine($"第{i}行,请填写电话号码;");
    //        return;
    //    }
    //    //国家
    //    var orderNation = row.GetCell(8).ParseCell();
    //    if (!string.IsNullOrWhiteSpace(orderNation))
    //    {
    //        errMsg.AppendLine($"第{i}行,请填写国家简码;");
    //        return;
    //    }
    //    var nation = _nationCache.List().FirstOrDefault(a => a.Short == orderNation.Trim().ToUpper());
    //    if (nation == null)
    //    {
    //        errMsg.AppendLine($"第{i}行,未找到此简码代表的国家;");
    //        return;
    //    }

    //    //买家姓名  电话号码    邮编  国家  州/省/区域    城市  区域  详细地址
    //    orderRaw.Receiver = new Models.DB.Orders.Address()
    //    {
    //        Name = orderName,                        //收件人姓名
    //        Phone = phone,                           //电话号码
    //        Zip = row.GetCell(7).ParseCell(),        //邮编
    //        Nation = nation.Name,
    //        NationShort = nation.Short,
    //        NationId = nation.ID,
    //        Province = row.GetCell(9).ParseCell(),     //州/省/区域
    //        City = row.GetCell(10).ParseCell(),        //城市
    //        District = row.GetCell(11).ParseCell(),    //区域
    //        Address1 = row.GetCell(12).ParseCell(),    //详细地址1
    //        Address2 = row.GetCell(13).ParseCell(),    //详细地址2
    //        Address3 = row.GetCell(14).ParseCell(),    //详细地址3
    //    };

    //    //采购总价  国内物流追踪号    国际物流运单号
    //    orderRaw.Profit = orderRaw.Profit.Next(orderRaw.OrderTotal.Money);
    //    orderRaw.CreateTime = DateTime.Now;
    //    orderRaw.EntryMode = EntryModes.Excel;
    //    orderRaw.BuyerUserName = orderRaw.Receiver.Name; //买家名称
    //    orderRaw.Bits = BITS.Progresses_Unprocessed; //未处理
    //    orderRaw.Coin = unit;
    //    orderRaw.IsMain = IsMains.THELORD;
    //}


    //public void AddProductField(int i, IRow? row,  Models.DB.Orders.Order orderRaw, StringBuilder errMsg)
    //{       
    //    string unit = orderRaw.Coin;
    //    List<ExcelGoodInfo> goodInfos = new();
    //    var goods = orderRaw.GoodsScope();
    //    foreach(var g in goods) {
    //        goodInfos.Add(new ExcelGoodInfo(g.ID, g.ImageURL, g.Name, g.ID, g.Sku, g.QuantityOrdered, g.UnitPrice, g.ShippingPrice));
    //    }        
       
    //    ExcelGoodInfo info = new();
    //    //产品图
    //    var imageUrl = row.GetCell(8).ParseCell();
    //    if (!string.IsNullOrWhiteSpace(imageUrl))
    //    {
    //        info.ImageURL = imageUrl;
    //    }

    //    //产品名称
    //    var nameStr = row.GetCell(9).ParseCell();
    //    if (!string.IsNullOrWhiteSpace(nameStr))
    //    {
    //        info.Name = nameStr;
    //    }

    //    //asin
    //    var asinStr = row.GetCell(10).ParseCell();
    //    if (!string.IsNullOrWhiteSpace(asinStr))
    //    {
    //        info.Asin = asinStr;
    //        info.ID = asinStr;
    //    }
    //    else
    //    {
    //        info.ID = Guid.NewGuid().ToString("N");
    //    }

    //    //Sku
    //    var SkuStr = row.GetCell(11).ParseCell();
    //    if (!string.IsNullOrWhiteSpace(SkuStr))
    //    {
    //        info.Sku = SkuStr;
    //    }

    //    //购买数量12
    //    var countStr = row.GetCell(12).ParseCell();
    //    if (!string.IsNullOrWhiteSpace(countStr))
    //    {
    //        if (!int.TryParse(countStr, out int count))
    //        {
    //            errMsg.AppendLine($"第{i}行,产品数量必须是正整数;");
    //            return;
    //        }
    //        else if (count <= 0)
    //        {
    //            errMsg.AppendLine($"第{i}行,产品数量必须是正整数;");
    //            return;
    //        }
    //        else
    //        {
    //            info.Count = count;
    //        }
    //    }
    //    else
    //    {
    //        errMsg.AppendLine($"第{i}行,产品数量为空;");
    //        return;
    //    }

    //    //单价13
    //    var priceStr = row.GetCell(13).ParseCell();
    //    if (!decimal.TryParse(priceStr, out decimal price))
    //    {
    //        errMsg.AppendLine($"第{i}行,产品单价必须是正数;");
    //        return;
    //    }
    //    else if (price <= 0)
    //    {
    //        errMsg.AppendLine($"第{i}行,产品单价必须是正数;");
    //        return;
    //    }

    //    info.Price = new MoneyMeta(unit, price);

    //    //亚马逊运费14
    //    var shipFeeStr = row.GetCell(14).ParseCell();
    //    if(!string.IsNullOrWhiteSpace(shipFeeStr))
    //    {
    //        if (!decimal.TryParse(shipFeeStr, out decimal shipFee))
    //        {
    //            errMsg.AppendLine($"第{i}行,运费必须是正数;");
    //            return;
    //        }
    //        else if (price <= 0)
    //        {
    //            errMsg.AppendLine($"第{i}行,运费必须是正数;");
    //            return;
    //        }
    //        info.Fee = new MoneyMeta(unit, shipFee);
    //    }              
    //    goodInfos.Add(info);


    //    var NumTemp = 0;
    //    orderRaw.GoodsScope(GoodList =>
    //    {
           
    //        foreach (var item in goodInfos)
    //        {               
    //            Good good = new();
    //            good.ID = item.ID;//asin
    //            //good.FromURL = $"{cache.GetOrg(store.Platform, orderBasic.MarketplaceId)}/dp/{item.ASIN}";//from_url
    //            good.Name = item.Name;//name
    //            good.Sku = item.Sku;//seller_sku                
    //            good.ImageURL = item.ImageURL;               
    //            NumTemp += item.Count;
    //            good.QuantityOrdered = item.Count;//num
    //            good.QuantityShipped = 0;//num-send_num? 已发货数量
    //                                                        //产品价格
    //            good.ItemPrice = item.ItemPrice;// var product_money
    //            good.ShippingPrice = item.Fee;// var shipping_money,amazon_shipping_money
    //            good.UnitPrice = item.Price; //price
    //            good.TotalPrice = item.Fee+item.ItemPrice;// with { Money = good.ItemPrice.Money + good.ShippingPrice.Money };//total
    //                                              //折后价格
    //                                              //good.promotionDiscount = item.PromotionDiscount.Mate;
    //            //good.ShippingTax = item.Fee;//.Mate;//shipping_tax
    //            //good.ShippingDiscountTax = item.ShippingDiscountTax.Mate;
    //            //good.PromotionDiscountTax = item.PromotionDiscountTax.Mate;
    //            //good.ItemTax = item.ItemTax.Mate;//item_tax
    //            GoodList.Add(good);
    //        }           
    //    }, Modes.Save);
    //    orderRaw.ProductNum = NumTemp;
    //    //总价
        
    //}


}


