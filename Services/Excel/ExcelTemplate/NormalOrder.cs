using ERP.Data;
using ERP.Enums.Orders;
using ERP.Extensions;
using ERP.Interface;
using ERP.Services.Distribution;
using ERP.Services.Stores;
using Microsoft.EntityFrameworkCore;
using NPOI.SS.UserModel;
using System.Data;
using System.Text;
using Z.EntityFramework.Plus;
using static ERP.Models.DB.Orders.Order;
using NationCache= ERP.Services.Caches.Nation;
using UnitCache = ERP.Services.Caches.Unit;
using OrderService = ERP.Services.DB.Orders.Order;

using ERP.Services.DB.Orders;

namespace ERP.Services.Excel.ExcelTemplate;

public class NormalOrder : ExcelBase
{   
    public NormalOrder(ILogger<NormalOrder> _logger, IServiceProvider ServiceProvider) : base(ExcelTemplateTypes.NormalOrder,_logger, ServiceProvider)
    {  
        
    }

    //private ISession Session { get => _sessionProvider.Session!; }
    //private int UserID { get => Session.GetUserID(); }
    //private int GroupID { get => Session.GetGroupID(); }
    //private int CompanyID { get => Session.GetCompanyID(); }
    //private int OEMID { get => Session.GetOEMID(); }
    //public override async Task<string> ImportExcel(ISheet sheet,int userid,int companyid,int groupid,int oemid)
    //{
    //    var _storeService = ServiceProvider.GetRequiredService<StoreService>();
    //    var _dbContext = ServiceProvider.GetRequiredService<ERP.Data.DBContext>();
    //    var _nationCache = ServiceProvider.GetRequiredService<NationCache>();
    //    var _orderService = ServiceProvider.GetRequiredService <OrderService>();
  

    //    StringBuilder errMsg = new();
    //    List<Models.DB.Orders.Order> orders = new();

    //    // 该用户能操作的所有店铺
    //    var stores = await _storeService.CurrentUserGetStores(true);

    //    //获取Excel表格中除标题以外的所有数据源，转化为dataTable中的表格数据源
    //    for (var i = sheet.FirstRowNum + 1; i <= sheet.LastRowNum; i++)
    //    {
    //        var row = sheet.GetRow(i);
    //        if (row == null) continue; //没有数据的行默认是null　

    //        //通用模板，根据店铺决定平台

    //        //订单号	 订单总价	 币种 所属店铺  收件人姓名(买家姓名)  电话号码6  邮编	国家  州/省/区域	城市 	区域	 详细地址           

    //        //订单号
    //        var orderNo = row.GetCell(0).ParseCell();
    //        if (orderNo.IsNullOrWhiteSpace())
    //        {
    //            errMsg.AppendLine($"第{i + 1}行,订单号为空;");
    //            continue;
    //        }

    //        //所属店铺
    //        var storeName = row.GetCell(3).ParseCell();
    //        var s = stores.FirstOrDefault(s => s.Name == storeName);
    //        if (s == null)
    //        {
    //            errMsg.AppendLine($"第{i + 1}行,店铺[{storeName}]不存在,请先添加店铺再导入订单;");
    //            continue;
    //        }

    //        //币种
    //        var unit = row.GetCell(2).ParseCell();
    //        unit = unit.IsNullOrWhiteSpace() ? "CNY" : unit;

    //        //订单总价
    //        var orderTotalPrice = row.GetCell(1).ParseCell();
    //        if (!string.IsNullOrWhiteSpace(orderTotalPrice))
    //        {
    //            orderTotalPrice!.Replace(unit, "");
    //        }
    //        decimal price = Convert.ToDecimal(orderTotalPrice);

    //        //买家姓名
    //        var orderName = row.GetCell(4).ParseCell();
    //        if (string.IsNullOrWhiteSpace(orderName))
    //        {
    //            errMsg.AppendLine($"第{i + 1}行,请填写买家姓名;");
    //            continue;
    //        }
    //        //电话号码
    //        var phone = row.GetCell(5).ParseCell();
    //        if (string.IsNullOrWhiteSpace(phone))
    //        {
    //            errMsg.AppendLine($"第{i + 1}行,请填写电话号码;");
    //            continue;
    //        }
    //        //国家
    //        var orderNation = row.GetCell(7).ParseCell();
    //        if (string.IsNullOrWhiteSpace(orderNation))
    //        {
    //            errMsg.AppendLine($"第{i + 1}行,请填写国家简码;");
    //            continue;
    //        }
    //        var nation = _nationCache.List().FirstOrDefault(a => a.Short == orderNation.Trim().ToUpper());
    //        if (nation == null)
    //        {
    //            errMsg.AppendLine($"第{i + 1}行,未找到此简码代表的国家;");
    //            continue;
    //        }

    //        var order = orders.FirstOrDefault(o => o.OrderNo == orderNo);
    //        if (order == null)  //添加;
    //        {
    //            order = new();
    //            order.OrderNo = orderNo;
    //            order.OrderNoHash = Helpers.CreateHashCode(orderNo);
    //            order.Plateform = s.PlatformInfo;
    //            order.OrderTotal = order.OrderTotal.Next(unit!, price);
    //            order.StoreId = s.ID;
    //            order.Receiver = new Models.DB.Orders.Address()
    //            {
    //                Name = orderName,  //收件人姓名
    //                Phone = phone,   //电话号码
    //                Zip = row.GetCell(6).ParseCell(),     //邮编
    //                Nation = nation.Name,
    //                NationShort = nation.Short,
    //                NationId = nation.ID,
    //                Province = row.GetCell(8).ParseCell(),   //州/省/区域
    //                City = row.GetCell(9).ParseCell(),      //城市
    //                District = row.GetCell(10).ParseCell(),   //区域
    //                Address1 = row.GetCell(11).ParseCell(),    //详细地址                                                                                              
    //            };
    //            order.Profit = order.Profit.Next(order.OrderTotal.Money);
    //            order.CreateTime = DateTime.Now;
    //            order.EntryMode = EntryModes.WebForm;
    //            order.BuyerUserName = order.Receiver.Name;//买家名称
    //            order.OperateUserId = userid;
    //            order.Bits = BITS.Progresses_Unprocessed;//未处理
    //            order.Coin = unit;
    //            order.pullOrderState = PullOrderState.OutOfProcess;
    //            order.IsMain = IsMains.THELORD;
    //            order.UserID = userid;
    //            order.GroupID = groupid;
    //            order.CompanyID = companyid;
    //            order.OEMID = oemid;

    //            orders.Add(order);
    //        }
    //    }

    //    if(errMsg.Length>0)
    //    {
    //        return errMsg.ToString();
    //    }       
    //    if (orders.Count > 0)
    //    {
    //        var models = await _orderService.GetOrdersByHashs(orders.Select(x => x.OrderNoHash).ToList());
    //        if (models.Count > 0)
    //        {
    //            models.ForEach(x => errMsg.AppendLine($"订单编号：{x.OrderNo} 已经存在！"));
    //            return errMsg.ToString();
    //        }
    //        else
    //        {
    //            await _dbContext.Order.AddRangeAsync(orders);
    //            await _dbContext.SaveChangesAsync();
    //            return "";
    //        }
    //    }
    //    else
    //    {
    //        return "没有符合的数据！";
    //    }
    //}


    public override async Task<string> ImportExcel(ISheet sheet, int userid, int companyid, int groupid, int oemid)
    {
        var _storeService = ServiceProvider.GetRequiredService<StoreService>();
        var _dbContext = ServiceProvider.GetRequiredService<ERP.Data.DBContext>();
        var _nationCache = ServiceProvider.GetRequiredService<NationCache>();
        var _unitCache = ServiceProvider.GetRequiredService<UnitCache>();
        var _orderService = ServiceProvider.GetRequiredService<OrderService>();

        StringBuilder errMsg = new();
        List<Models.DB.Orders.Order> orders = new();
        Models.DB.Orders.Order orderRaw = new();

        // 该用户能操作的所有店铺
        var stores = await _storeService.CurrentUserGetStores(true);

        //获取Excel表格中除标题以外的所有数据源，转化为dataTable中的表格数据源
        for (var i = sheet.FirstRowNum + 1; i <= sheet.LastRowNum; i++)
        {
            var row = sheet.GetRow(i);
            if (row == null) continue; //没有数据的行默认是null　

            //订单号0	订单总价1 	币种2	订单时间3 	所属店铺4 	SellerID5	手续费6	   成本损失费7	产品图8	产品名称9 	asin10	
            //Sku11 	购买数量12	单价13	亚马逊运费14	总价15   	买家姓名16	电话号码17	邮编18	    国家19	州/省/区域20	城市21
            //区域22 	详细地址23 	详细地址24	详细地址25 	采购总价26	 国内物流追踪号27  	国际物流运单号28

            //订单号
            var orderNo = row.GetCell(0).ParseCell();
            int n = i + 1;

            if (orderNo.IsNullOrWhiteSpace())
            {
                if (string.IsNullOrWhiteSpace(orderRaw.OrderNo))  //新增订单 
                {
                    errMsg.AppendLine($"第{n}行,订单号为空;");
                    continue;
                }
                else
                {
                    var order = orders.FirstOrDefault(x => x.OrderNo == orderRaw.OrderNo);
                    if (order != null)
                    {
                        //更新产品信息
                        AddProductField(n, row, order, errMsg);
                    }
                    else
                    {
                        throw new AggregateException($"{orderNo} 数据获取异常！");
                    }
                }               
            }
            else
            {
                if (orders.Any(x => x.OrderNo == orderNo))
                {
                    errMsg.AppendLine($"第{n}行,订单号重复;");
                    continue;
                }
                else
                {
                    orderRaw = new();
                    orderRaw.OrderNo = orderNo;
                    orderRaw.OrderNoHash = Helpers.CreateHashCode(orderNo);
                    orderRaw.OperateUserId = userid;
                    orderRaw.pullOrderState = PullOrderState.OutOfProcess;
                    orderRaw.UserID = userid;
                    orderRaw.CompanyID = companyid;
                    orderRaw.GroupID = groupid;
                    orderRaw.OEMID = oemid;
                    AddField(n, row, stores, orderRaw, _nationCache, _unitCache, errMsg);
                    if(errMsg.Length!=0)
                    {
                        return errMsg.ToString();
                    }
                    AddProductField(n, row, orderRaw, errMsg);
                    orders.Add(orderRaw);
                }
            }          
        }

        if (errMsg.Length > 0)
        {
            return errMsg.ToString();
        }
        if (orders.Count > 0)
        {
            var models = await _orderService.GetOrdersByHashs(orders.Select(x => x.OrderNoHash).ToList());
            if (models.Count > 0)
            {
                models.ForEach(x => errMsg.AppendLine($"订单编号：{x.OrderNo} 已经存在！"));
                return errMsg.ToString();
            }
            else
            {
                await _dbContext.Order.AddRangeAsync(orders);
                await _dbContext.SaveChangesAsync();
                return "";
            } 
        }
        else
        {
            return "没有符合的数据！";
        }
    }

    public void AddField(int i, IRow? row, List<Models.Stores.StoreRegion>? stores, Models.DB.Orders.Order orderRaw, NationCache _nationCache, UnitCache _unitCache, StringBuilder errMsg)
    {
        //币种
        string unit = "";
        var unitStr = row.GetCell(2).ParseCell();
        if(!string.IsNullOrWhiteSpace(unitStr))
        {           
            var units = _unitCache.List();
            var u = units.FirstOrDefault(x => x.Sign == unitStr.ToUpper());
            if (u != null)
            {
                unit = u.Sign;
            }
            else
            {
                errMsg.AppendLine($"第{i}行,币种不正确;");
                return;
            }
        }
        else
        {
            unit = "CNY";
        }
        orderRaw.Coin = unit;
        //订单总价
        var orderTotalPrice = row.GetCell(1).ParseCell();
        if (!string.IsNullOrWhiteSpace(orderTotalPrice))
        {
            orderTotalPrice = orderTotalPrice!.Replace(unit, "");
            if (!decimal.TryParse(orderTotalPrice, out decimal totalPrice))
            {
                errMsg.AppendLine($"第{i}行,订单总价必须是正整数;");
                return;
            }
            else if (totalPrice <= 0)
            {
                errMsg.AppendLine($"第{i}行,订单总价必须是正整数;");
                return;
            }
            else
            {
                orderRaw.OrderTotal = new MoneyMeta(unit, totalPrice);
            }
        }

        //订单时间
        var datetimeStr = row.GetCell(3).ParseCell();
        if (!string.IsNullOrWhiteSpace(datetimeStr))
        {
            if (DateTime.TryParse(datetimeStr, out DateTime time))
            {
                orderRaw.CreateTime = time;
            }
            else
            {
                errMsg.AppendLine($"第{i}行,订单时间格式不对，正确格式yyyy-MM-dd或yyyy-MM-dd HH:mm:ss;");
                return;
            }
        }
        else
        {
            errMsg.AppendLine($"第{i}行,订单时间为空;");
            return;
        }

        //所属店铺	SellerID
        bool flag = false;
        string storeName = "";
        int storeID = 0;
        Platforms storePlatform = Platforms.OTHERS;
        var SellerID = row.GetCell(5).ParseCell().Trim();
        if (!string.IsNullOrWhiteSpace(SellerID))
        {
            foreach (var s in stores)
            {
                var amazon = s.GetSPAmazon();
                if (amazon != null && amazon.SellerID == SellerID)
                {
                    flag = true;
                    storeName = s.Name;
                    storeID = s.ID;
                    storePlatform = s.Platform;
                    break;
                }
            }
            if (!flag)
            {
                errMsg.AppendLine($"第{i}行,店铺[{SellerID}]不存在,请验证店铺信息再导入订单;");
                return;
            }
            else
            {
                orderRaw.StoreId = storeID;
                orderRaw.Plateform = storePlatform;
            }
        }
        else
        {
            storeName = row.GetCell(4).ParseCell();
            if (!string.IsNullOrWhiteSpace(storeName))
            {
                var s = stores.FirstOrDefault(s => s.Name == storeName);
                if (s == null)
                {
                    errMsg.AppendLine($"第{i}行,店铺[{storeName}]不存在,请先添加店铺再导入订单;");
                    return;
                }
                else
                {
                    orderRaw.StoreId = s.ID;
                    orderRaw.Plateform = s.Platform;
                }
            }
            else
            {
                errMsg.AppendLine($"第{i}行,店铺[{storeName}]为空;");
                return;
            }
        }

        //手续费	
        var feeStr = row.GetCell(6).ParseCell();
        if (!string.IsNullOrWhiteSpace(feeStr))
        {
            feeStr = feeStr!.Replace(unit, "");
            if (!decimal.TryParse(feeStr, out decimal fee))
            {
                errMsg.AppendLine($"第{i}行,订单总价必须是正整数;");
                return;
            }
            else if (fee < 0)
            {
                errMsg.AppendLine($"第{i}行,订单总价必须是正整数;");
                return;
            }
            else
            {
                orderRaw.Fee = new MoneyMeta(unit, fee);
            }
        }

        //成本损失费
        var costFeeStr = row.GetCell(7).ParseCell();
        if (!string.IsNullOrWhiteSpace(costFeeStr))
        {
            costFeeStr = costFeeStr!.Replace(unit, "");
            if (!decimal.TryParse(costFeeStr, out decimal costFee))
            {
                errMsg.AppendLine($"第{i}行,订单总价必须是正整数;");
                return;
            }
            else if (costFee < 0)
            {
                errMsg.AppendLine($"第{i}行,订单总价必须是正整数;");
                return;
            }
            else
            {
                orderRaw.Loss = new MoneyMeta(unit, costFee);
            }
        }

        //买家姓名
        var orderName = row.GetCell(16).ParseCell();
        if (string.IsNullOrWhiteSpace(orderName))
        {
            errMsg.AppendLine($"第{i}行,请填写买家姓名;");
            return;
        }        

        //电话号码
        var phone = row.GetCell(17).ParseCell();
        if (string.IsNullOrWhiteSpace(phone))
        {
            errMsg.AppendLine($"第{i}行,请填写电话号码;");
            return;
        }
        //国家
        var orderNation = row.GetCell(19).ParseCell();
        if (string.IsNullOrWhiteSpace(orderNation))
        {
            errMsg.AppendLine($"第{i}行,请填写中文国家名称;");
            return;
        }
        var nation = _nationCache.List().FirstOrDefault(a => a.Name == orderNation.Trim());
        if (nation == null)
        {
            errMsg.AppendLine($"第{i}行,请填写中文国家名称;");
            return;
        }       

        var address=new Models.DB.Orders.Address()
        {
            Name = orderName,                        //收件人姓名
            Phone = phone,                           //电话号码
            Zip = row.GetCell(18).ParseCell(),        //邮编
            Nation = nation.Name,
            NationShort = nation.Short,
            NationId = nation.ID,
            Province = row.GetCell(20).ParseCell(),     //州/省/区域
            City = row.GetCell(21).ParseCell(),        //城市
            District = row.GetCell(22).ParseCell(),    //区域
            Address1 = row.GetCell(23).ParseCell(),    //详细地址1
            Address2 = row.GetCell(24).ParseCell(),    //详细地址2
            Address3 = row.GetCell(25).ParseCell(),    //详细地址3
        };

        //买家姓名  电话号码    邮编  国家  州/省/区域    城市  区域  详细地址
        orderRaw.Receiver = address;

        //采购总价  国内物流追踪号    国际物流运单号
        orderRaw.Profit = orderRaw.Profit.Next(orderRaw.OrderTotal.Money);
        orderRaw.CreateTime = DateTime.Now;
        orderRaw.EntryMode = EntryModes.WebForm;
        orderRaw.BuyerUserName = orderRaw.Receiver.Name; //买家名称
        orderRaw.Bits = BITS.Progresses_Unprocessed; //未处理
        orderRaw.State = States.UNSHIPPED;
       
        orderRaw.IsMain = IsMains.THELORD;
    }


    public void AddProductField(int i, IRow? row, Models.DB.Orders.Order orderRaw, StringBuilder errMsg)
    {
        string unit = orderRaw.Coin;
        List<ExcelGoodInfo> goodInfos = new();
        var goods = orderRaw.GoodsScope();
        foreach (var g in goods)
        {
            goodInfos.Add(new ExcelGoodInfo(g.ID, g.ImageURL, g.Name, g.ID, g.Sku, g.QuantityOrdered, g.UnitPrice, g.ShippingPrice));
        }

        ExcelGoodInfo info = new();
        //产品图
        var imageUrl = row.GetCell(8).ParseCell();
        if (!string.IsNullOrWhiteSpace(imageUrl))
        {
            info.ImageURL = imageUrl;
        }

        //产品名称
        var nameStr = row.GetCell(9).ParseCell();
        if (!string.IsNullOrWhiteSpace(nameStr))
        {
            info.Name = nameStr;
        }

        //asin
        var asinStr = row.GetCell(10).ParseCell();
        if (!string.IsNullOrWhiteSpace(asinStr))
        {
            info.Asin = asinStr;
            info.ID = asinStr;
        }
        else
        {
            info.ID = Guid.NewGuid().ToString("N");
        }

        //Sku
        var SkuStr = row.GetCell(11).ParseCell();
        if (!string.IsNullOrWhiteSpace(SkuStr))
        {
            info.Sku = SkuStr;
        }

        //购买数量12
        var countStr = row.GetCell(12).ParseCell();
        if (!string.IsNullOrWhiteSpace(countStr))
        {
            if (!int.TryParse(countStr, out int count))
            {
                errMsg.AppendLine($"第{i}行,产品数量必须是正整数;");
                return;
            }
            else if (count <= 0)
            {
                errMsg.AppendLine($"第{i}行,产品数量必须是正整数;");
                return;
            }
            else
            {
                info.Count = count;
            }
        }
        else
        {
            errMsg.AppendLine($"第{i}行,产品数量为空;");
            return;
        }

        //单价13
        var priceStr = row.GetCell(13).ParseCell();
        if (!string.IsNullOrWhiteSpace(priceStr))
        {
            priceStr = priceStr!.Replace(unit, "");
            if (!decimal.TryParse(priceStr, out decimal price))
            {
                errMsg.AppendLine($"第{i}行,产品单价必须是正数;");
                return;
            }
            else if (price < 0)
            {
                errMsg.AppendLine($"第{i}行,产品单价必须是正数;");
                return;
            }
            info.Price = new MoneyMeta(unit, price);
        }
        else
        {
            errMsg.AppendLine($"第{i}行,产品单价不能为空;");
            return;
        }

        //亚马逊运费14
        var shipFeeStr = row.GetCell(14).ParseCell();       
        if (!string.IsNullOrWhiteSpace(shipFeeStr))
        {
            shipFeeStr = shipFeeStr!.Replace(unit, "");
            if (!decimal.TryParse(shipFeeStr, out decimal shipFee))
            {
                errMsg.AppendLine($"第{i}行,运费必须是正数;");
                return;
            }
            else if (shipFee < 0)
            {
                errMsg.AppendLine($"第{i}行,运费必须是正数;");
                return;
            }
            info.Fee = new MoneyMeta(unit, shipFee);
        }
        else
        {
            info.Fee = new MoneyMeta(unit, 0);
        }
        goodInfos.Add(info);

        var NumTemp = 0;
        orderRaw.GoodsScope(GoodList =>
        {

            foreach (var item in goodInfos)
            {
                Good good = new();
                good.ID = item.ID;//asin
                //good.FromURL = $"{cache.GetOrg(store.Platform, orderBasic.MarketplaceId)}/dp/{item.ASIN}";//from_url
                good.Name = item.Name;//name
                good.Sku = item.Sku;//seller_sku                
                good.ImageURL = item.ImageURL;
                NumTemp += item.Count;
                good.QuantityOrdered = item.Count;//num
                good.QuantityShipped = 0;//num-send_num? 已发货数量
                                         //产品价格
                good.UnitPrice = item.Price; //price
                good.ItemPrice = item.Price with { Money = item.Price.Money * item.Count };// var product_money
                good.ShippingPrice = item.Fee;// var shipping_money,amazon_shipping_money
               
                good.TotalPrice = item.Fee + good.ItemPrice;// with { Money = good.ItemPrice.Money + good.ShippingPrice.Money };//total
                                                            //折后价格
                                                            //good.promotionDiscount = item.PromotionDiscount.Mate;
                                                            //good.ShippingTax = item.Fee;//.Mate;//shipping_tax
                                                            //good.ShippingDiscountTax = item.ShippingDiscountTax.Mate;
                                                            //good.PromotionDiscountTax = item.PromotionDiscountTax.Mate;
                                                            //good.ItemTax = item.ItemTax.Mate;//item_tax
                GoodList.Add(good);
            }
        }, Modes.Save);
        orderRaw.ProductNum = NumTemp;
        //总价

    }




}



public class ExcelGoodInfo
{
    public ExcelGoodInfo()
    {

    }

    public ExcelGoodInfo(string iD, string imageURL, string name, string asin, string sku, int count, MoneyRecord price, MoneyRecord fee)
    {
        ID = iD;
        ImageURL = imageURL;
        Name = name;
        Asin = asin;
        Sku = sku;
        Count = count;
        Price = price;
        Fee = fee;
        ItemPrice = price with { Money = price.Money * count };
    }

    public string ID { get; set; }

    /// <summary>
    /// 产品图
    /// </summary>
    public string ImageURL { get; set; }

    /// <summary>
    /// 产品名称
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// asin
    /// </summary>
    public string? Asin { get; set; }

    /// <summary>
    /// Sku
    /// </summary>
    public string? Sku { get; set; }

    /// <summary>
    /// 购买数量
    /// </summary>
    public int Count { get; set; }

    /// <summary>
    /// 单价
    /// </summary>
    public MoneyRecord Price { get; set; }

    /// <summary>
    /// 运费
    /// </summary>
    public MoneyRecord Fee { get; set; }

    public MoneyRecord ItemPrice { get; set; }
}
