﻿using ERP.Models.Api.Logistics.OCS;
using ERP.Services.Api;
using Microsoft.Extensions.DependencyInjection;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Text;

namespace ERP.Services.Excel;

public abstract class ExcelBase : IExcel
{
    protected readonly ILogger logger;
    protected readonly LogisticClient Request;
    protected readonly IServiceProvider ServiceProvider;
    protected ExcelBase(ExcelTemplateTypes excelTemplateType, ILogger _logger, IServiceProvider _ServiceProvider)
    {
        ExcelTemplateType = excelTemplateType;
        logger = _logger;
        ServiceProvider = _ServiceProvider.CreateScope().ServiceProvider;

    }

    public ExcelTemplateTypes ExcelTemplateType { get; }

    public virtual IExcel CheckAndCreateSheet(IFormFile formFile, out ISheet sheet)
    {
        var stream = formFile.OpenReadStream();
        //Workbook对象代表一个工作簿,首先定义一个Excel工作薄
        //XSSFWorkbook 适用XLSX格式，HSSFWorkbook 适用XLS格式
        #region 判断Excel版本
        IWorkbook workbook = Path.GetExtension(formFile.FileName) switch
        {
            ".xlsx" => new XSSFWorkbook(stream),//.XLSX是07版(或者07以上的)的Office Excel
            ".xls" => new HSSFWorkbook(stream),//.XLS是03版的Office Excel
            _ => throw new Exception("Excel文档格式有误"),
        };
        #endregion

        sheet = workbook.GetSheetAt(0);
        if (sheet == null || sheet.LastRowNum < 1) throw new Exception("Excel文档为空");

        return this;
    }

    public abstract Task<string> ImportExcel(ISheet sheet, int userid, int companyid, int groupid, int oemid);
}
