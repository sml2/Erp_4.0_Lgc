﻿using NPOI.SS.UserModel;

namespace ERP.Services.Excel;

public static class ExcelHelper
{
    public static string? ParseCell(this ICell cell)
    {
        if (cell != null)
        {
            if (!string.IsNullOrEmpty(cell.ToString()) && !string.IsNullOrWhiteSpace(cell.ToString()))
            {
                return cell.ToString().Trim();
            }
        }
        return null;
    }
}