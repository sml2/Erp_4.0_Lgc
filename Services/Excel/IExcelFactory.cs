﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Services.Excel;

public interface IExcelFactory
{
    void Add(IExcel excel);
    IExcel Create(ExcelTemplateTypes excelTemplateType);
}
