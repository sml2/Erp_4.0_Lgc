﻿using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Services.Excel;

public enum ExcelTemplateTypes
{
    None,
    [Description("通用订单")]
    NormalOrder,
    Amazon,
    Shopee,
}

public interface IExcel
{
    ExcelTemplateTypes ExcelTemplateType { get; }
    IExcel CheckAndCreateSheet(IFormFile formFile, out ISheet sheet);
    Task<string> ImportExcel(ISheet sheet, int userid, int companyid, int groupid, int oemid);
}
