﻿using ERP.Services.Excel.ExcelTemplate;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace ERP.Services.Excel;

public class ExcelFactory : IExcelFactory
{
    private readonly Dictionary<ExcelTemplateTypes, IExcel> _excelTemplate = new();

    //public ExcelFactory(){}
    public ExcelFactory(
        NormalOrder normalOrder,
        ExcelTemplate.Amazon amazon)
    {
        Add(normalOrder);
        Add(amazon);
    }

    public void Add(IExcel excel) => _excelTemplate.TryAdd(excel.ExcelTemplateType, excel);

    public IExcel Create(ExcelTemplateTypes excelTemplateType)
    {
        return _excelTemplate[excelTemplateType];
    }
}
