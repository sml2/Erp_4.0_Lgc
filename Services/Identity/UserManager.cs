﻿using System.Security.Claims;
using ERP.Authorization.Rule.Component;
using ERP.ControllersWithoutToken.Sender;
using ERP.Enums.ID;
using ERP.Enums.Rule.Config.Internal;
using ERP.Extensions;
using ERP.Interface.Rule;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Role = ERP.Enums.Identity.Role;

namespace ERP.Services.Identity;
using Model = Models.DB.Identity.User;
public class UserManager : UserManager<Model>
{
    public readonly IManager Config;
    public UserManager(IUserStore<Model> store,
        IOptions<IdentityOptions> optionsAccessor,
        IPasswordHasher<Model> passwordHasher,
        IEnumerable<IUserValidator<Model>> userValidators,
        IEnumerable<IPasswordValidator<Model>> passwordValidators,
        ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors,
        IServiceProvider services, 
        ILogger<UserManager> logger, IManager config) :
        base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
    {
        Config = config;
    }
    public virtual Task<Model> FindByIdAsync(int userId) => FindByIdAsync(userId.ToString());

    public async Task<DataRange_2b> GetProductModuleRange(ClaimsPrincipal principal,Enum config)
    {
        // var userClaims = principal.Claims.ToList();
        // Func<IConfigVisitor, long> val = userClaims.Has;
        // Func<IHas, bool> has = principal.Has;
        // var configItems = Config.Get(menu.GetNumber());
        // if (configItems is null)
        // {
        //     return null;
        // }
        // var data = configItems.RenderConfigItems(has, val);
        // IComponentView? item = data.FirstOrDefault(m => m is Select);
        //
        // var select = (Select?)item;
        var user = await GetUserAsync(principal);
        return principal.GetProductModuleRange(config, user);
    }
}
