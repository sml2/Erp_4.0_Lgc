﻿using ERP.Models.DB.Identity;
using ERP.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ERP.Services.Identity;

public class UserStore : UserStore<User, Role, DBContext, int, UserClaim, UserRole, UserLogin, UserToken, RoleClaim>
{
    public UserStore(DBContext context, IdentityErrorDescriber describer) : base(context, describer)
    {
    }

    private static readonly Func<DBContext, IQueryable<User>> IncludeCompanyOem = context => context.User.Include(u => u.Company)
            .Include(u => u.OEM);

    public override Task<User> FindByIdAsync(string userId, CancellationToken cancellationToken = default(CancellationToken))
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();
        var id = ConvertIdFromString(userId);
#pragma warning disable CS8619
        return IncludeCompanyOem(Context).FirstOrDefaultAsync(u => u.Id == id, cancellationToken);
#pragma warning restore CS8619
    }
    
    public override Task<User> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken = default(CancellationToken))
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();

#pragma warning disable CS8619
        return IncludeCompanyOem(Context).FirstOrDefaultAsync(u => u.NormalizedUserName == normalizedUserName, cancellationToken);
#pragma warning restore CS8619
    }
    
    protected override Task<User> FindUserAsync(int userId, CancellationToken cancellationToken)
    {
#pragma warning disable CS8619
        return IncludeCompanyOem(Context).FirstOrDefaultAsync(u => u.Id.Equals(userId), cancellationToken);
#pragma warning restore CS8619
    }
    
    public override Task<User> FindByEmailAsync(string normalizedEmail, CancellationToken cancellationToken = default(CancellationToken))
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();

#pragma warning disable CS8619
        return IncludeCompanyOem(Context).FirstOrDefaultAsync(u => u.NormalizedEmail == normalizedEmail, cancellationToken);
#pragma warning restore CS8619
    }
}