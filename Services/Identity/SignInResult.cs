﻿using AspNetCoreSignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace ERP.Services.Identity;

public class SignInResult : AspNetCoreSignInResult
{
    public bool IsValidityExpired { get; set; }

    public static SignInResult ValidityExpired { get; } = new SignInResult() { IsValidityExpired = true };

    public override string ToString()
    {
        return IsValidityExpired ? "用户授权过期" : base.ToString();
    }
}

public static class SignInResultExtension
{
    public static bool IsValidityExpired(this SignInResult signInResult)
    {
        if (signInResult == null)
            throw new ArgumentNullException(nameof(signInResult));
        return signInResult is SignInResult { IsValidityExpired: true };
    }
}