﻿using Microsoft.AspNetCore.Identity;

namespace ERP.Services.Identity;
using Model = Models.DB.Identity.Role;

public class RoleManager : RoleManager<Model>
{
    public RoleManager(IRoleStore<Model> store, IEnumerable<IRoleValidator<Model>> roleValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, ILogger<RoleManager> logger) : base(store, roleValidators, keyNormalizer, errors, logger)
    {
    }
    public virtual Task<Model> FindByIdAsync(int roleId) => FindByIdAsync(roleId.ToString());
}
