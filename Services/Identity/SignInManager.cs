﻿using ERP.Data;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Security.Claims;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace ERP.Services.Identity;
using ENUM = Enums.ID.Company;

/// <summary>
/// 自定义登录管理器
/// </summary>
public class SignInManager : SignInManager<Models.DB.Identity.User>
{
    private readonly IHttpContextAccessor _contextAccessor;

    public SignInManager(UserManager<Models.DB.Identity.User> userManager,
        IHttpContextAccessor contextAccessor,
        IUserClaimsPrincipalFactory<Models.DB.Identity.User> claimsFactory,
        IOptions<IdentityOptions> optionsAccessor,
        ILogger<SignInManager> logger,
        IAuthenticationSchemeProvider schemes,
        IUserConfirmation<Models.DB.Identity.User> confirmation)
        : base(userManager,
            contextAccessor,
            claimsFactory,
            optionsAccessor,
            logger,
            schemes,
            confirmation)
    {
        _contextAccessor = contextAccessor;
    }

    /// <summary>
    /// 将sessionId加入用户令牌
    /// </summary>
    /// <param name="user"></param>
    /// <param name="isPersistent"></param>
    /// <param name="additionalClaims"></param>
    /// <returns></returns>
    public override Task SignInWithClaimsAsync(Models.DB.Identity.User user, bool isPersistent, IEnumerable<Claim> additionalClaims)
    {
        var tempClaims = new Claim[] { new Claim("SessionId", Guid.NewGuid().ToString()) };
        // 将sessionId加入当前用户临时令牌, 以供单点登陆操作储存
        Context.User.AddIdentity(new ClaimsIdentity(tempClaims));

        return base.SignInWithClaimsAsync(user, isPersistent, additionalClaims.Concat(tempClaims));
    }

    protected override async Task<Microsoft.AspNetCore.Identity.SignInResult?> PreSignInCheck(
        Models.DB.Identity.User user)
    {
        var result = await base.PreSignInCheck(user);
        if (result != null)
            return result;

        if (!await CheckUserValidity(user))
        {
            Logger.LogWarning("User #{Id} [{Name}]'s validity is expired", user.Id, user.UserName);
            return SignInResult.ValidityExpired;
        }

        return null;
    }

    /// <summary>
    /// 验证用户授权是否过期
    /// <remarks>TODO 可以尝试将方法放在UserStore中 </remarks>
    /// </summary>
    /// <param name="user"></param>
    /// <returns></returns>
    private async Task<bool> CheckUserValidity(Models.DB.Identity.User user)
    {
        var dbContext = Context.RequestServices.GetRequiredService<DBContext>();
        var company = await dbContext.Company.Where(m => m.ID == user.CompanyID).FirstAsync();

        var pids = JsonConvert.DeserializeObject<int[]>(company.Pids)?? Array.Empty<int>();

        if (pids.Length <= 0)
        {
            return company.Validity >= DateTime.Now;
        }
        
        var allCompany = await dbContext.Company
            .Where(m => pids.Contains(m.ID) && m.ID != (int) ENUM.XiaoMi)
            .OrderByDescending(m => m.ID)
            .ToListAsync();
        
        foreach (var item in allCompany)
        {
            if(item.Validity < DateTime.Now)
            {
                return false;
            }
        }
        return true;

        // var userValidity = await dbContext.UserValidities.FirstOrDefaultAsync(uv => uv.CompanyID == user.CompanyID);
        // return userValidity != null && userValidity.Validity >= DateTime.Now;
    }
}