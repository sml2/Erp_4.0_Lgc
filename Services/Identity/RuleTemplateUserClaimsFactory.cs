﻿using System.Security.Claims;
using ERP.Authorization;
using ERP.Data;
using ERP.Models.DB.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Claim = System.Security.Claims.Claim;

namespace ERP.Services.Identity;

public class RuleTemplateUserClaimsFactory : UserClaimsPrincipalFactory<User, Role>
{
    private readonly DBContext _dbContext;

    public RuleTemplateUserClaimsFactory(UserManager<User> userManager, RoleManager<Role> roleManager, IOptions<IdentityOptions> options, DBContext dbContext) : base(userManager, roleManager, options)
    {
        _dbContext = dbContext;
    }

    protected override async Task<ClaimsIdentity> GenerateClaimsAsync(User user)
    {
        var id = await base.GenerateClaimsAsync(user);
        var ruleInfo = await _dbContext.RuleTemplate.Where(m => m.ID == user.FormRuleTemplateID)
            .FirstOrDefaultAsync();
        if (ruleInfo is not null)
        {
            id.AddClaims(ruleInfo.GetClaims(user.OEMID));
        }

        return id;
    }
}