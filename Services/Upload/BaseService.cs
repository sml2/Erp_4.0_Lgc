using ERP.Interface;
using ERP.Models.Setting;
using ERP.Services.FileStorage;
using ERP.Services.FileStorage.Models;
using Newtonsoft.Json;


namespace ERP.Services.Upload;

public  class BaseService : Services.BaseService
{
    private readonly AdapterInterface _storage;

    public enum FileEnum
    {
        IMAGE,
        TXT
    }

    private readonly ILogger logger;

    public BaseService(IOemProvider oemProvider, IHostEnvironment environment, ILogger _logger)
    {
        logger = _logger;
        logger.LogInformation("BaseService ctor", oemProvider);
        logger.LogInformation($"environment {environment.IsProduction().ToString()}");
        logger.LogInformation($"OEM {JsonConvert.SerializeObject(oemProvider?.OEM)}");        
        _storage = !environment.IsProduction()
            ? FileSystemFactory.Storage("obs")
            : FileSystemFactory.Storage(new FileSystems(oemProvider?.OEM?.Oss ?? default!));
    }
    public BaseService(FileBucket model, IHostEnvironment environment)
    {       
        _storage = !environment.IsProduction()
            ? FileSystemFactory.Storage("obs")
            : FileSystemFactory.Storage(new FileSystems(model));
    }
    //public async Task GetStore()
    //{
    //    if ($this->sessionData->getConcierge() == UserModel::TYPE_OWN) {
    //        $stores = $this->getStoreCompanyCache();
    //    } else
    //    {
    //        $stores = $this->getStorePersonalCache();
    //    }

    //    if (checkArr($stores))
    //    {
    //        return array_filter($stores, fn($item) => $item['type'] === 1);
    //    }
    //    return [];

    //}

    //public async Task<ReturnStruct> GetIndex(Controllers.Store.UploadProgress.UploadShow upload)
    //{
    //    List<Models.Store.StoreModel> stores = new();
    //    var FindStore = _MyDbContext.Stores.Where(x => x.User.Concierge == Models.User.ConciergeEnum.Own).FirstOrDefaultAsync(x => x.ID == upload.Id);
    //    //if(FindStore != null)
    //    //{
    //    //    stores=
    //    //} else{
    //    //    stores=
    //    //}
    //    if (FindStore!=null)
    //    {
    //        _MyDbContext.SyncMws.WhenWhere(FindStore.Id > 0, x => x.StoreId == FindStore.Id)
    //            .Select(x => new { x });
    //    }
    //}


    public async Task<string> GetUrl(string path)
    {
        return await _storage.GetUrl(path);
    }

    public async Task<bool> PutAsync(string path, string contents, object? config = null)
    {
        //todo 文字上传
        return await _storage.Put(path, contents, config);
    }

    public async Task<bool> PutStream(string path, Stream contents, object? config = null)
    {
        return await _storage.PutStream(path, contents, config);
    }


    public async Task<long> GetSize(string path)
    {
        return await _storage.GetSize(path);
    }

    public async Task<bool> Has(string path)
    {
        return await _storage.Has(path);
    }
    
    public Task<Stream> Read(string path)
    {
        return _storage.Read(path);
    }

    public async Task<bool> Delete(List<string> strings)
    {
        try
        {
            foreach (var item in strings)
            {
                await _storage.Delete(item);
            }

            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    public async Task<bool> Delete(string path)
    {
        return await _storage.Delete(path);
    }
    
    public async Task<bool> BatchDeleteAsync(List<string> paths,bool quiet)
    {
        return await _storage.BatchDeleteAsync(paths,quiet);
    }

    public async Task<bool> Copy(string path, string newPath)
    {
        return await _storage.Copy(path, newPath);
    }
    
    public async Task<bool> CopyObject(string path, string newPath,string sourceBucketName)
    {
        return await _storage.CopyObject(path, newPath,sourceBucketName);
        
    }
    public async Task<string> FilePath(int partition, int userId, string ext, FileEnum fileType = FileEnum.IMAGE)
    {
        System.Text.StringBuilder rnd = new();
        foreach (var b in BitConverter.GetBytes(Random.Shared.NextInt64()))
        {
            rnd.Append(b.ToString("X"));
        }

        var t = DateTime.Now;
        var timestamp = (new DateTimeOffset(t)).ToUnixTimeSeconds();
        var name = $"ERP_{timestamp}_{rnd}.{ext.ToLower().Trim('.')}";

        var dir = fileType == FileEnum.IMAGE ? "images" : "content";

        return $"{partition}/{t.Year:0000}/{t.Month:00}/{t.Day:00}/{dir}/{userId}/{name}";
    }
}