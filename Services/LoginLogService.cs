using ERP.Data;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.DB.Identity;
using ERP.Models.DB.Setting;
using ERP.Models.DB.Users;
using ERP.Models.Setting;
using ERP.Models.Users;

namespace ERP.Services;

public class LoginLogService : BaseWithUserService
{
    protected readonly IHttpContextAccessor _httpContext;

    public LoginLogService(DBContext dbContext, ISessionProvider sessionProvider, IHttpContextAccessor httpContext) :
        base(dbContext, sessionProvider)
    {
        _httpContext = httpContext;
    }

    // public void AddLogWithLogin(User user, bool isTimeout, DateTime loginTime, UserOnlineModel? userOnline)
    // {
    //     var model = new LoginLogModel()
    //     {
    //         OemId = user.OemId,
    //         UserId = user.Id,
    //         Username = user.UserName,
    //         LoginIp = _httpContext.HttpContext!.GetIPv4()!,
    //         LastLogId = userOnline?.ID,
    //         CreatedAt = loginTime,
    //         UpdatedAt = loginTime,
    //     };
    //     //类型
    //     if (userOnline != null)
    //     {
    //         model.LastLogId = userOnline.ID;
    //         model.LastTime = userOnline.LastTime;
    //         if (isTimeout)
    //         {
    //             model.Type = LoginLogModel.Types.TIMEOUTLOGOUT;
    //         }
    //         else
    //         {
    //             model.Type = LoginLogModel.Types.TIMEOUTLOGOUT;
    //         }
    //     }
    //     else
    //     {
    //         model.Type = LoginLogModel.Types.LOGIN;
    //         model.Remark = $"{model.Username}于{loginTime}登录系统";
    //     }
    // }

    /// <summary>
    /// 被动写入日志模型初始化
    /// </summary>
    /// <param name="userOnline"></param>
    /// <param name="loginTime"></param>
    /// <returns></returns>
    private LoginLogModel Passive(ISession session, DateTime? loginTime = null)
    {
        var now = DateTime.Now;
        return new LoginLogModel(session, loginTime.HasValue ? Convert.ToDateTime(loginTime) : now);
    }
    
    /// <summary>
    /// 被动写入日志模型初始化
    /// </summary>
    /// <param name="userOnline"></param>
    /// <param name="loginTime"></param>
    /// <returns></returns>
    private LoginLogModel Passive(UserOnlineModel userOnline, DateTime? loginTime = null)
    {
        var now = DateTime.Now;
        return new LoginLogModel(userOnline, loginTime.HasValue ? Convert.ToDateTime(loginTime) : now);
    }

    /// <summary>
    /// 超时登出
    /// </summary>
    /// <param name="userOnline"></param>
    /// <param name="user"></param>
    /// <returns></returns>
    public async Task<bool> TimeOutLogout(UserOnlineModel userOnline, User user)
    {
        var m = Passive(userOnline);
        m.Type = LoginLogModel.Types.TimeoutLogout;
        m.Remark = $"{m.Username}超时登出登录系统，最后操作时间{userOnline.LastTime}，触发该检测的用户是{user.UserName}";

        await _dbContext.LoginLog.AddAsync(m);
        return await _dbContext.SaveChangesAsync() > 0;
    }

    /// <summary>
    /// 超时登出
    /// </summary>
    /// <param name="userOnline"></param>
    /// <param name="user"></param>
    /// <returns></returns>
    public async Task<bool> TimeOutLogout(List<UserOnlineModel> userOnline, User user)
    {
        var logs = new List<LoginLogModel>();
        foreach (var item in userOnline)
        {
            var m = Passive(item);
            m.Type = LoginLogModel.Types.TimeoutLogout;
            m.Remark = $"{m.Username}超时登出登录系统，最后操作时间{item.LastTime}，触发该检测的用户是{user.UserName}";
            logs.Add(m);
        }
        await _dbContext.LoginLog.AddRangeAsync(logs);
        return await _dbContext.SaveChangesAsync() > 0;
    }

    public async Task<bool> SqueezedOffLine(UserOnlineModel userOnline)
    {
        var m = Passive(userOnline);
        m.Type = LoginLogModel.Types.SqueezedOffline;
        m.Remark = $"{m.Username}异处登录被挤下线，原账户最后操作时间{userOnline.LastTime}";

        await _dbContext.LoginLog.AddAsync(m);
        return await _dbContext.SaveChangesAsync() > 0;
    }

    public async Task<bool> Loginout(UserOnlineModel userOnline)
    {
        var m = Passive(userOnline);
        m.Type = LoginLogModel.Types.NormalExit;
        m.Remark = $"{m.Username}主动登出下线，最后操作时间{userOnline.LastTime}";

        await _dbContext.LoginLog.AddAsync(m);
        return await _dbContext.SaveChangesAsync() > 0;
    }

    public async Task<bool> Login(UserOnlineModel userOnline, DateTime loginTime)
    {
        var m = Passive(userOnline, loginTime);
        m.Type = LoginLogModel.Types.Login;
        m.Remark = $"{m.Username}于{loginTime}登录系统";

        await _dbContext.LoginLog.AddAsync(m);
        return await _dbContext.SaveChangesAsync() > 0;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="session"></param>
    /// <returns></returns>
    public async Task<bool> Loginout(ISession session)
    {
        var m = Passive(session);
        m.Type = LoginLogModel.Types.ErrorNormalExit;
        m.Remark = $"{m.Username}异常登出下线，最后操作时间{m.CreatedAt}";

        await _dbContext.LoginLog.AddAsync(m);
        return await _dbContext.SaveChangesAsync() > 0;
    }
}