using ERP.Data;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.Export;
using Microsoft.EntityFrameworkCore;

namespace ERP.Services.Export;

public class SyncExportProductService : BaseWithUserService
{
    public SyncExportProductService(DBContext dbContext, ISessionProvider sessionProvider) : base(dbContext, sessionProvider)
    {
    }
    
    public async Task<bool> DeleteExportProductById(int id)
    {
        var info = await _dbContext.SyncExportProduct
            .Where(m => m.ID == id)
            .FirstOrDefaultAsync();

        if (info == null)
        {
            throw new Exceptions.SyncExportProduct.NotFoundException("找不到相关数据#1");
        }

        _dbContext.SyncExportProduct.Remove(info);
        return  await _dbContext.SaveChangesAsync() > 0;
    }
    
    /// <summary>
    /// 获取导出产品关联关系
    /// </summary>
    /// <param name="eid"></param>
    /// <param name="sid"></param>
    /// <returns></returns>
    public async Task<SyncExportProduct?> GetExportProductInfo(int eid, int sid)
    {
        return await _dbContext.SyncExportProduct
            .WhenWhere(eid > 0, m => m.Eid == eid)
            .WhenWhere(sid > 0, m => m.Sid == sid)
            .FirstOrDefaultAsync();
    }
}