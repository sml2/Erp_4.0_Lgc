﻿using ERP.Enums;
using ERP.Models.Export;
using ERP.Models.View.Products.Product;

namespace ERP.Services.Export;

public class AssembleContext
{
    public ExportTask Task { get; set; }
    public Languages Language { get; set; }
    public List<ProductPriceParams>? Params { get; set; } = null;
    public string SuffixParam => Task.User is { ProductConfig.ExportImgUrl: true } ? $"?t={DateTime.Now.GetTimeStampMilliseconds()}" : "";
    public List<int> Ids { get; set; }
}