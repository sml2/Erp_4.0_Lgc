using System.IO.Compression;
using System.Security.Claims;
using ERP.Data;
using ERP.Data.Products;
using ERP.Enums;
using ERP.Exceptions.Export;
using ERP.Export.ExportCommon;
using ERP.Export.Models;
using ERP.Export.Queue;
using ERP.Export.Templates;
using ERP.Export.Templates.Adapter;
using ERP.Extensions;
using ERP.Models.Export;
using ERP.Models.View.Products.Product;
using ERP.Services.Product;
using ERP.Services.Provider;
using ERP.Services.Stores;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using File = Sy.IO.File;
using Unit = MediatR.Unit;
using UnitCache = ERP.Services.Caches.Unit;

namespace ERP.Services.Export;

public class ExportBackground : IQueueHandler<ExportQueueItem>
{
    private readonly ILogger<ExportBackground> _logger;
    private readonly IServiceProvider _serviceProvider;
    private readonly ExcelTemplateFactory _templateFactory;
    private readonly UnitCache _unitCache;
    private ConvertProduct _convertProduct;

    static ExportBackground()
    {
        Directory.CreateDirectory("temp");
    }

    public ExportBackground(ILogger<ExportBackground> logger, IServiceProvider serviceProvider,
        ExcelTemplateFactory templateFactory, UnitCache unitCache)
    {
        _logger = logger;
        _serviceProvider = serviceProvider.CreateScope().ServiceProvider;
        _templateFactory = templateFactory;
        _unitCache = unitCache;
    }

    public async Task OnStart(ITaskQueue<ExportQueueItem> queue, CancellationToken cancellationToken)
    {
        var dbContext = _serviceProvider.CreateScope().ServiceProvider.GetRequiredService<DBContext>();
        var exportTasks = await LoadAsync(dbContext, cancellationToken);

        foreach (var task in exportTasks)
        {
            queue.Enqueue(new ExportQueueItem(task));
        }

        var lockedTasks = await LoadAsync(dbContext, cancellationToken, true);
        var ids = lockedTasks.Select(t => t.ID).ToArray();
        if (lockedTasks.Any())
        {
            do
            {
                await Task.Delay(3000, cancellationToken);
                lockedTasks = await LoadAsync(dbContext, cancellationToken, true, ids);
            } while (lockedTasks.Any() && !cancellationToken.IsCancellationRequested);

            exportTasks = await LoadAsync(dbContext, cancellationToken, false, ids);

            foreach (var task in exportTasks)
            {
                queue.Enqueue(new ExportQueueItem(task));
            }
        }

        static Task<ExportTask[]> LoadAsync(DBContext context, CancellationToken cancellationToken, bool locked = false,
            int[]? ids = null)
        {
            return context.ExportTasks
                .WhenWhere(ids is not null, t => ids!.Contains(t.ID))
                .Where(t => t.State <= ExportTask.TaskState.Running && t.Locked == locked)
                .OrderBy(t => t.ID)
                .ToArrayAsync(cancellationToken);
        }
    }

    public Task<Unit> Handle(ExportQueueItem request, CancellationToken stoppingToken)
    {
        if (request.Task.Type == ExportTask.TaskType.Products)
            return ExportProducts(request, stoppingToken);

        if (request.Task.Type == ExportTask.TaskType.Orders)
        {
            var oemCache = _serviceProvider.GetRequiredService<Caches.OEM>();
            var oem = oemCache.Get(request.Task.OEMID)!;
            return _serviceProvider.CreateScopeWithOem(oem).ServiceProvider
                .GetRequiredService<OrderBackgroundExporter>().ExportOrders(request, stoppingToken);
        }

        return Task.FromResult(Unit.Value);
    }

    public async Task<Unit> ExportProducts(ExportQueueItem request, CancellationToken stoppingToken)
    {
        var oemCache = _serviceProvider.GetRequiredService<Caches.OEM>();
        var oem = oemCache.Get(request.Task.OEMID)!;
        var sp = _serviceProvider.CreateScopeWithOem(oem).ServiceProvider;
        var dbContext = sp.GetRequiredService<DBContext>();
        _convertProduct = sp.GetRequiredService<ConvertProduct>();
        var entry = dbContext.Entry(request.Task);
        await entry.ReloadAsync(stoppingToken);

        if (entry.Entity.Locked)
            return Unit.Value;
        await entry.Reference(t => t.SyncExport).LoadAsync(stoppingToken);
        await entry.Reference(t => t.User).LoadAsync(stoppingToken);
        await entry.Reference(t => t.OEM).LoadAsync(stoppingToken);
        await entry.Reference(t => t.OEM).TargetEntry!.Reference(t => t.Oss).LoadAsync(stoppingToken);
        var exportTask = request.Task;

        var productService = sp.GetRequiredService<ProductService>();
        var syncProductService = sp.GetRequiredService<SyncProductService>();


        exportTask.Locked = true;
        await dbContext.SaveChangesAsync(stoppingToken);

        var user = exportTask.User!;


        _logger.LogInformation("开始{Msg}执行任务{ExportTask}",
            exportTask.State == ExportTask.TaskState.Waiting ? "首次" : "继续", exportTask);

        if (!exportTask.SyncExport!.CanExport(out var error))
        {
            exportTask.MarkFailed(error);
            await dbContext.SaveChangesAsync(default);
            return Unit.Value;
        }

        var claimsPrincipal = new ClaimsPrincipal(new List<ClaimsIdentity>()
        {
            new ClaimsIdentity((await dbContext.UserClaims.Where(c => c.UserId == user.Id).ToListAsync(stoppingToken))
                .Select(c => c.ToClaim()))
        });
        var range = claimsPrincipal.GetProductModuleRange(Enums.Rule.Config.Store.UploadProgressRange, user);
        var ids = await dbContext.SyncExportProduct
            .WhereRange(range, user.CompanyID, user.GroupID, user.Id)
            .Where(m => m.Eid == exportTask.SyncExportId)
            .Select(m => m.Sid)
            .ToArrayAsync(stoppingToken);
        if (!ids.Any())
        {
            exportTask.MarkFailed("导出产品为空");
            await dbContext.SaveChangesAsync(default);
            return Unit.Value;
        }

        if (exportTask.TotalCount > 0 && exportTask.TotalCount != ids.Length)
            _logger.LogWarning("导出任务{Task}产品数量发生变更: {Old}->{New}", exportTask, exportTask.TotalCount,
                ids.Length);

        exportTask.TotalCount = ids.Length;
        try
        {
            var result = await ExportExcel(dbContext, syncProductService, productService, exportTask, ids,
                stoppingToken);
            if (result.IsFailed)
                exportTask.MarkFailed(result.Errors.First().Message);
        }
        catch (System.Exception e)
        {
            exportTask.MarkFailed(e.ToString());
            _logger.LogError(e, "导出表格异常");
        }

        exportTask.Locked = false;
        await dbContext.SaveChangesAsync(default);
        return Unit.Value;
    }


    private async Task<Result> ExportExcel(DBContext dbContext, SyncProductService syncProductService,
        ProductService productService, ExportTask task, int[] ids, CancellationToken cancellationToken)
    {
        var role = task.SyncExport!.ParamObj;
        var parameter = task.SyncExport.Append;
        var exportSign = task.SyncExport.ExportSign;
        if (role is null || parameter is null || !ids.Any() || !Enum.IsDefined(exportSign))
            return Result.Fail("数据为空,请重试");

        var factory = _templateFactory[exportSign];
        if (factory is null)
            return Result.Fail("无法找到导出的数据类型");

        List<ProductPriceParams>? changePriceTemplate = null;
        var changePriceTemplateId = role["ChangePriceTemplateId"]?.Value<int?>();
        if (changePriceTemplateId != null)
        {
            var template =
                await dbContext.ChangePriceTemplate.FirstOrDefaultAsync(c =>
                    c.ID == changePriceTemplateId.Value, cancellationToken);
            if (template is not null)
                changePriceTemplate = JsonConvert.DeserializeObject<List<ProductPriceParams>>(template.Value);
        }

        IExportAdapter adapter;
        if (task.State == ExportTask.TaskState.Waiting)
        {
            var headers = await factory.GetHead(parameter);
            if (headers is null) return Result.Fail("读取表头出错");
            
            adapter = ProductExportHelper.CreateExportAdapter(task.SyncExport.ExportSign, _serviceProvider);
            adapter.AddHeaders(headers);

            task.TempRowIndex = adapter.GetState();
            task.State = ExportTask.TaskState.Running;
            await dbContext.SaveChangesAsync(default);
        }
        else
        {
            var result = ProductExportHelper.RestoreExportAdapter(task.SyncExport.ExportSign, _serviceProvider,
                task.TempPath, task.TempRowIndex);

            if (result.IsFailed)
                return result.ToResult();
            
            adapter = result.Value;
        }

        var unit = new List<string>();
        if (factory.Name.Contains("Lazada"))
        {
            unit.AddRange(new[] { "CNY", "SGD", "MYR", "IDR", "PHP", "THB", "VND" });
        }
        else
        {
            if (factory.TemplateType == TemplateType.ZYingExcel)
            {
                Sy.String tmpParameter = parameter.Replace(" ", "").ToUpper();
                var checkCny = "\"is_trans\":1,".ToUpper();
                if (tmpParameter.Contains(checkCny))
                {
                    unit.Add("CNY");
                }
            }
            else
            {
                var unitMidor = new Midor("\"unit\":\"".ToUpper(), "\",".ToUpper());
                Sy.String tmpParameter = parameter.Replace(" ", "").ToUpper();
                if (tmpParameter.CanMid(unitMidor))
                {
                    var unitStr = tmpParameter.Mid(unitMidor);
                    if (unitStr.IsNotEmpty())
                        unit.Add(unitStr);
                }
            }
        }

        // 获取汇率及一次性获取的产品数量\间隔\ 
        var number = 3;
        var unitCache = _unitCache.List()!;

        var rate = unit.Count > 0
            ? unitCache.Where(x => unit.Contains(x.Sign)).ToList()
            : unitCache.Where(x => x.Sign == task.User!.UnitConfig).ToList();
        if (rate.Count == 0)
        {
            return Result.Fail("汇率不可为空");
        }

        var productErrors =
            JsonConvert.DeserializeObject<Dictionary<int, List<string>>>(task.ProductErrors ?? string.Empty) ??
            new Dictionary<int, List<string>>();
        do
        {
            // 正在获取产品数据...
            var proId = ids.Skip(task.CompletedCount).Take(number).ToList();

            var cellDataInfo = await Export_Get_Products(dbContext, syncProductService, productService, task, proId,
                role.Language, changePriceTemplate);

            if (cellDataInfo.Any())
            {
                foreach (var cellData in cellDataInfo)
                {
                    try
                    {
                        if (cellData is { State: true, Data: not null })
                        {
                            role.TaskId = task.ID;
                            var data = await factory.GetData(cellData.Data, role, parameter,
                                rate.Select(r => new ERP.Export.Models.Unit(r.Name, r.Sign, r.Rate, r.RefreceTime))
                                    .ToList());
                            if (data is not null)
                            {
                                adapter.AddProduct(data);
                            }
                        }
                    }
                    catch (System.Exception e)
                    {
                        cellData.State = false;
                        cellData.CodeInfo = e is ExportException ? e.Message : e.ToString();
                        _logger.LogError(e, "任务{TaskId}导出产品{Pid}出错", task.ID, cellData.ID);
                    }

                    if (cellData.State is false)
                    {
                        if (productErrors.TryGetValue(cellData.ID, out var errors))
                            errors.Add(cellData.CodeInfo);
                        else
                            productErrors.Add(cellData.ID, new List<string>() { cellData.CodeInfo });

                        task.ProductErrors = JsonConvert.SerializeObject(productErrors);
                    }

                    _logger.LogInformation("导出任务{Task}构建产品{Id}: {Message}", task, cellData.ID, cellData.CodeInfo);
                }
            }

            task.TempRowIndex = adapter.GetState();
            task.CompletedCount += proId.Count;
            await dbContext.SaveChangesAsync(default);
        } while (ids.Length > task.CompletedCount && !cancellationToken.IsCancellationRequested);

        adapter.SaveToPath(task.TempPath);
        adapter.Dispose();
        if (factory.TemplateType == TemplateType.Hktvmall)
        {
            using var ms = new MemoryStream();
            using (var archive = new ZipArchive(ms, ZipArchiveMode.Create))
            {
                archive.CreateEntryFromFile(task.TempPath, "product_upload_template.xlsx", CompressionLevel.Optimal);
                if (Directory.Exists(task.TempExtPath))
                    foreach (var img in Directory.GetFiles(task.TempExtPath))
                    {
                        archive.CreateEntryFromFile(img, Path.GetFileName(img), CompressionLevel.Optimal);
                    }
            }

            File.WriteAllBytes(task.TempPath, ms.ToArray());
            if (Directory.Exists(task.TempExtPath))
                Directory.Delete(task.TempExtPath, true);
        }
        
        if (factory.TemplateType == TemplateType.GlobalSources)
        {
            using var ms = new MemoryStream();
            using (var archive = new ZipArchive(ms, ZipArchiveMode.Create))
            {
                archive.CreateEntryFromFile(task.TempPath, "1.xlsx", CompressionLevel.Optimal);
                if (Directory.Exists(task.TempExtPath))
                {
                    foreach (var dir in Directory.GetDirectories(task.TempExtPath))
                    {
                        foreach (var img in Directory.GetFiles(dir))
                        {
                            archive.CreateEntryFromFile(img, Path.GetRelativePath(task.TempExtPath, img), CompressionLevel.Optimal);
                        }
                    }
                    foreach (var img in Directory.GetFiles(task.TempExtPath))
                    {
                        archive.CreateEntryFromFile(img, Path.GetFileName(img), CompressionLevel.Optimal);
                    }
                }
            }

            File.WriteAllBytes(task.TempPath, ms.ToArray());
            if (Directory.Exists(task.TempExtPath))
                Directory.Delete(task.TempExtPath, true);
        }

        if (task.CompletedCount >= task.TotalCount)
            task.State = ExportTask.TaskState.Exported;
        await dbContext.SaveChangesAsync(default);

        return Result.Ok().WithSuccess("导出完成");
    }

    private async Task<List<CellData>> Export_Get_Products(DBContext dbContext, SyncProductService syncProductService,
        ProductService productService, ExportTask task, List<int> ids, Languages language,
        List<ProductPriceParams>? @params = null)
    {
        var context = new AssembleContext()
        {
            Ids = ids,
            Language = language,
            Params = @params,
            Task = task,
        };

        var loggerFactory = _serviceProvider.GetRequiredService<ILoggerFactory>();
        var assembler = task.SyncExport!.ExportSign switch
        {
            TemplateType.Hktvmall => new HktvmallDataAssembler(productService, dbContext, syncProductService,
                _convertProduct, loggerFactory.CreateLogger<HktvmallDataAssembler>()),
            _ => new DataAssembler(productService, dbContext, syncProductService, _convertProduct,
                loggerFactory.CreateLogger<DataAssembler>()),
        };

        return await assembler.Assemble(context);
    }

}