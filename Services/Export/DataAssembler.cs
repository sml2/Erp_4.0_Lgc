﻿using System.Text;
using ERP.Data;
using ERP.Data.Products;
using ERP.Enums;
using ERP.Exceptions.Export;
using ERP.Export;
using ERP.Export.Models;
using ERP.Export.Templates;
using ERP.Models.Export;
using ERP.Models.Product;
using ERP.Models.Stores;
using ERP.Models.View.Products.Product;
using ERP.Services.Product;
using ERP.Services.Stores;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ModelProduct = ERP.Data.Products.Product;

namespace ERP.Services.Export;

public class DataAssembler
{
    private readonly ProductService _productService;
    private readonly DBContext _dbContext;
    private readonly SyncProductService _syncProductService;
    private readonly ConvertProduct _convertProduct;
    private readonly ILogger<DataAssembler> _logger;

    public DataAssembler(ProductService productService, DBContext dbContext, SyncProductService syncProductService,
        ConvertProduct convertProduct, ILogger<DataAssembler> logger)
    {
        _productService = productService;
        _dbContext = dbContext;
        _syncProductService = syncProductService;
        _convertProduct = convertProduct;
        _logger = logger;
    }

    public async Task<List<CellData>> Assemble(AssembleContext context)
    {
        //获取SyncProduct 产品信息
        var tempSyncProduct = await _syncProductService.GetSyncProduct(context.Ids);
        //产品ids
        var pids = tempSyncProduct.Select(a => a.Pid);
        var syncProduct = tempSyncProduct.ToDictionary(a => a.Pid, a => a);
        //查询产品
        var products = await _dbContext.Product.Where(a => a.CompanyID == context.Task.CompanyID)
            .Where(a => pids.Contains(a.ID))
            .ToListAsync();

        var listProductInfo = new List<CellData>();
        //循环获取产品
        foreach (var product in products)
        {
            try
            {
                var result = await AssembleSingleProduct(context, product, syncProduct[product.ID]);
                if (result.IsSuccess)
                {
                    listProductInfo.Add(result.Value);
                    _logger.LogDebug("导出产品{Id}, json: {Json}", product.ID, JsonConvert.SerializeObject(result.Value));
                }
                else
                    listProductInfo.Add(new CellData()
                        { ID = product.ID, State = false, CodeInfo = result.Errors.First().Message });
            }
            catch (System.Exception e)
            {
                listProductInfo.Add(new CellData()
                    { ID = product.ID, State = false, CodeInfo = $"未知错误, 请联系客服. error: {e}" });
                _logger.LogError(e, "导出任务{Task}构建产品{Id}出错", context.Task, product.ID);
            }
        }

        return listProductInfo;
    }

    private async Task<Result<CellData>> AssembleSingleProduct(AssembleContext context, ProductModel productModel,
        SyncProductModel syncProductModel)
    {
        var suffixParam = context.SuffixParam;
        var task = context.Task;
        var supportedStructsResult = await GetSupportedProductStructs(productModel, context.Language);

        if (supportedStructsResult.IsFailed)
            return Result.Fail(supportedStructsResult.Errors);

        var supportedStructs = supportedStructsResult.Value;
        var proStruct = supportedStructs.Values.First();

        // 金额
        if (context.Params is not null)
        {
            var unit = productModel.Coin.IsNotEmpty() ? productModel.Coin : context.Task.User!.UnitConfig;
            productModel.ChangePrices(proStruct, context.Params, unit);
        }

        //判断是否有无变体
        var autoGenerate = AutoGenerate(proStruct.Variants.Attributes);
        var skuGenerator = new SkuGenerator(syncProductModel.SkuType, syncProductModel.SourceId,
            syncProductModel.SkuSuffix, autoGenerate);
        skuGenerator.GenerateMain(syncProductModel.Sku);

        var messy = new MessyData(productModel, syncProductModel, task, supportedStructs, skuGenerator);
        //变体
        var products = AssembleVariants(messy, context);

        var data = AssembleMain(messy, suffixParam);
        data.Products = products;

        return new CellData()
        {
            ID = productModel.ID,
            Data = data,
            CodeInfo = "Success",
            State = true
        };
    }

    /// <summary>
    /// 获取构建表格所需要的多语言产品数据
    /// </summary>
    /// <returns></returns>
    protected virtual async Task<Result<Dictionary<Languages, ModelProduct>>> GetSupportedProductStructs(
        ProductModel productModel, Languages language)
    {
        var pid = productModel.ID;
        var langId = language == Languages.Default ? 0 : (int)Math.Log2((long)language) + 1;

        var usedLanguage = language;
        var item = await _productService.GetItemInfoByLangId(pid, langId);
        if (item is null)
        {
            usedLanguage = Languages.Default;
            item = await _productService.GetItemInfoByLangId(pid, 0);
        }

        if (item is null)
            return Result.Fail($"Pid[{pid}]数据为空#1");

        var proStruct = await _productService.GetProductStruct(item.FilePath, productModel);
        return new Dictionary<Languages, ModelProduct>()
        {
            { usedLanguage, proStruct }
        };
    }

    /// <summary>
    /// 构建主产品及变体的杂糅数据
    /// </summary>
    public class MessyData
    {
        public MessyData(ProductModel product, SyncProductModel syncProductModel, ExportTask task,
            Dictionary<Languages, ModelProduct> supportedProductStructs, SkuGenerator skuGenerator)
        {
            Product = product;
            SyncProductModel = syncProductModel;
            Task = task;
            SupportedProductStructs = supportedProductStructs;
            SkuGenerator = skuGenerator;
            ProductImages = product.Gallery;
            ProStruct = SupportedProductStructs.Values.First();
            Variants = ProStruct.Variants;

            RemoveDescriptionHtml = task.SyncExport?.ParamObj?["RemoveDescriptionHtml"]?.Value<bool?>() is true;
        }

        public ProductModel Product { get; }
        public SyncProductModel SyncProductModel { get; }
        public ModelProduct ProStruct { get; }
        public Variants Variants { get; }
        public ExportTask Task { get; }
        public Dictionary<Languages, ModelProduct> SupportedProductStructs { get; }
        public SkuGenerator SkuGenerator { get; }
        public bool RemoveDescriptionHtml { get; }
        public Data.Products.Images? ProductImages { get; }
    }

    protected virtual DataItem AssembleMain(MessyData data, string suffixParam)
    {
        var proStruct = data.ProStruct;
        var task = data.Task;
        var productModel = data.Product;
        var skuGenerator = data.SkuGenerator;
        var syncProductModel = data.SyncProductModel;

        var mainBulletPoint = proStruct.GetSketches() ?? new List<string>(0);
        var prices = proStruct.GetPrices()!;
        string? showImage = null;
        if (proStruct.MainImage is not null)
        {
            showImage = GetImageUrl(task, proStruct.MainImage.Path ?? proStruct.MainImage.Url) + suffixParam;
        }

        return new DataItem()
        {
            PID = productModel.ID,
            RawPid = productModel.Pid,
            Name = proStruct.GetTitle() ?? string.Empty,
            Sku = skuGenerator.GenerateMain(syncProductModel.Sku),
            Source = productModel.Source ?? string.Empty,
            Quantity = proStruct.Quantity,
            ShowImage = showImage,
            ProductImages = data.ProductImages?.Select(p => GetImageUrl(task, p.Path ?? p.Url) + suffixParam).ToList() ?? new List<string>(),
            Price = new()
            {
                Cost = new() { Max = prices.Cost.Max, Min = prices.Cost.Min },
                Sale = new() { Max = prices.Sale.Max, Min = prices.Sale.Min }
            },
            Description = ExportUtils.RemoveHtmlTag(_convertProduct.GetProductDesc(proStruct.GetDescription()!, task.OEM!),
                data.RemoveDescriptionHtml, task.SyncExport!.ExportSign),
            BulletPoint = mainBulletPoint,
            SearchTerms = proStruct.GetKeyword(),
            Category = productModel.CategoryModel?.Name,
            Attribute = GetProAttribute(proStruct.Variants.Attributes),
            ProductID = productModel.Pid,
        };
    }

    protected virtual List<ProductsItem> AssembleVariants(MessyData data, AssembleContext assembleContext)
    {
        var syncProductModel = data.SyncProductModel;
        var variants = data.Variants;
        var proStruct = data.ProStruct;
        var task = data.Task;
        var skuGenerator = data.SkuGenerator;
        var removeDescriptionHtml = data.RemoveDescriptionHtml;
        var productImages = data.ProductImages;

        var products = new List<ProductsItem>();
        for (var index = 0; index < variants.Count; index++)
        {
            var item = variants[index];
            if (item.State == VariantStateEnum.Disable)
                continue;

            var name = item.GetTitle() ?? proStruct.GetTitle()!;
            var description = _convertProduct.GetProductDesc(item.GetDescription() ?? proStruct.GetDescription()!,
                task.OEM!);
            description = ExportUtils.RemoveHtmlTag(description, removeDescriptionHtml, task.SyncExport!.ExportSign);
            var sketch = item.GetSketches() is { Count: > 0 }
                ? item.GetSketches()!
                : (proStruct.GetSketches() is { Count: > 0 } ? proStruct.GetSketches()! : new List<string>(0));
            var bulletPoint = sketch;

            var keyword = item.GetKeyword() ?? proStruct.GetKeyword();

            // 字典的顺序可能是乱的, 此处重新排序
            var ids = item.Ids.OrderBy(m => m.Key).Select(m => m.Value).ToList();

            var variantDto = new VariantDto(item);

            var attributesName =
                MakeAttribute(syncProductModel, proStruct.Variants.Attributes, variantDto.Ids);

            var sku = skuGenerator.GenerateVariant(item.Sku, index, attributesName);

            // 暂时没有使用描述图片的需求
            // var descImages = GetImageUrls(description).Select(img => GetImageUrl(task, img) + assembleContext.SuffixParam).ToList();
            var descImages = new List<string>();
            var mainImage = item.Images.Main.HasValue
                ? GetFullUrl(productImages, item.Images.Main, task) + assembleContext.SuffixParam
                : null;
            var images = GetFullUrl(productImages, item.Images.Affiliate, task).Select(i => i + assembleContext.SuffixParam).ToList();

            products.Add(new ProductsItem()
            {
                Name = MakeVariantTitle(syncProductModel, name, attributesName),
                Description = description,
                BulletPoint = bulletPoint,
                SearchTerms = keyword,
                Values = ids,
                Sku = sku,
                MainImage = mainImage,
                Images = images,
                DescriptionImages = descImages,
                Quantity = item.Quantity,
                Price = item.Sale.Value.ToString(),
                Cost = item.Cost.Value.ToString(),
                ProduceCode = item.Code ?? string.Empty,
                //todo Fields
                SID = item.Sid
            });
        }

        return products;
    }


    private string? GetFullUrl(Data.Products.Images? images, int? id, ExportTask task)
    {
        if (images is null)
            return null;

        if (!id.HasValue)
            return null;

        var image = images.FirstOrDefault(m => m.ID == id);
        if (image is null)
            return null;

        return GetImageUrl(task, image.Path ?? image.Url);
    }

    private List<string> GetFullUrl(Data.Products.Images? images, List<int>? ids, ExportTask task)
    {
        if (images is not { Count: > 0 } || ids is null)
            return new List<string>(0);

        var result = ids.Join(images.Distinct(i => i.ID).ToDictionary(i => i.ID, i => i), id => id, kv => kv.Key,
                (id, kv) => kv.Value)
            .Select(i => GetImageUrl(task, i.Path ?? i.Url))
            .ToList();

        return result;
    }

    /// <summary>
    /// 判断产品是否为无变体产品, 单变体产品当作无变体产品处理
    /// </summary>
    /// <param name="attributes"></param>
    /// <returns></returns>
    private static bool AutoGenerate(Data.Products.Attributes attributes) =>
        attributes.items is { Count: 1 } && attributes.items.First().Value.Value.Length == 1;

    public string MakeAttribute(SyncProductModel syncProductModel, Data.Products.Attributes attributes,
        List<Ids>? ids)
    {
        if (syncProductModel.SkuSuffix == SyncProductModel.SkuSuffixs.FALSE &&
            syncProductModel.TitleSuffix == SyncProductModel.TitleSuffixs.FALSE)
        {
            return string.Empty;
        }

        if (ids is null)
            return string.Empty;

        var str = new StringBuilder();
        foreach (var item in ids)
        {
            if (attributes.items.TryGetValue(item.Tag, out var attr) && attr.Value.Length > item.Val)
            {
                str.Append($"{attr.Value[item.Val]}-");
            }
        }

        if (str.Length > 0)
            str.Remove(str.Length - 1, 1);

        return str.ToString();
    }

    public string MakeVariantTitle(SyncProductModel syncProductModel, string title, string attributeName)
    {
        if (syncProductModel.TitleSuffix == SyncProductModel.TitleSuffixs.FALSE)
            return title;
        return $"{title}-{attributeName}";
    }

    protected virtual string GetImageUrl(ExportTask task, string str)
    {
        var src = str.Trim('/');
        if (src.StartsWith("http"))
            return src;

        var domain = task.OEM!.Oss!.BucketDomain;
        // if (task.OEM.ID != 8)
        // {
            // domain = "overseasimg4.liulergou.com";
            // domain = "img4.miwaimao.com";
        // }
        return $"http://{domain}/{src}";
    }

    public Dictionary<string, string[]> GetProAttribute(Data.Products.Attributes attributes)
    {
        var data = new Dictionary<string, string[]>();
        var items = attributes.items;

        foreach (var item in items)
        {
            if (!data.TryAdd(item.Value.Name, item.Value.Value))
                throw new AssembleException($"产品属性名称重复[{item.Value.Name}]");
        }

        return data;
    }


    private List<string> GetImageUrls(string html)
    {
        var chunks = html.Split("<img ");
        var result = new List<string>(chunks.Length - 1);
        foreach (Sy.String chunk in chunks.Skip(1))
        {
            var url = chunk.Mid("src=\"", "\"");
            if (!string.IsNullOrEmpty(url))
                result.Add(url);
        }

        return result;
    }
}