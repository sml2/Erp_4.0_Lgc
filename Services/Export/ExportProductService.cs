using Microsoft.EntityFrameworkCore;
using System.Text;
using ERP.Enums.Identity;
using ERP.Enums.Rule.Config.Internal;
using ERP.Exceptions.Export;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using ERP.Models.Export;
//using ERP.Export.Templates;
using ERP.Interface;
using ERP.Models.Abstract;
using ERP.Models.DB.Setting;
using ERP.Models.View.Setting;
using ERP.Services.Stores;


namespace ERP.Services.Export;

using Data;
using Enums;
using Exceptions;
using Extensions;
using Aspose.Cells;
using Data.Products;
using Models.Product;
using Models.DB.Stores;
using UnitCache = Caches.Unit;
using ExpProductModel = Models.Export.ExportProduct;
using ERP.Services.Product;
using ERP.Export.ExportCommon;
using Newtonsoft.Json;
using ERP.Models.View.Products.Product;
using System.Text.RegularExpressions;
using ERP.Models.View.Products.Export;
using ERP.Export.Templates;
using ERP.Services.Caches;
using ERP.Export.Models;
using Newtonsoft.Json.Linq;
using ERP.Models.Setting;

public class ExportProductService : BaseService
{
    private readonly DBContext _dbContext;
    private readonly ISessionProvider _serverOptions;
    private readonly UnitCache _unitCache;
    private readonly SyncProductService _syncProductService;
    private readonly ILogger<ExportProductService> _logger;
    private readonly SyncExportService _syncExportServiceService;
    private readonly LanguagesCache _languagesCache;
    private readonly ProductExportHelper _exportCommon;
    private readonly ProductService _productService;
    private readonly IOemProvider _oemProvider;


    public ExportProductService(DBContext dbContext, ISessionProvider ServerOptions,
        SyncProductService syncProductService,
        ILogger<ExportProductService> logger, UnitCache unitCache,
        SyncExportService syncExportServiceService, LanguagesCache LanguagesCache, ProductService productService,
        ProductExportHelper ExportCommon, IOemProvider oemProvider)
    {
        _dbContext = dbContext;
        _serverOptions = ServerOptions;
        _unitCache = unitCache;
        _syncProductService = syncProductService;
        _logger = logger;
        _languagesCache = LanguagesCache;
        _productService = productService;
        _syncExportServiceService = syncExportServiceService;
        _productService = productService;
        _exportCommon = ExportCommon;
        _oemProvider = oemProvider;
    }

    private ISession Session
    {
        get => _serverOptions.Session!;
    }

    private int UserID => Session.GetUserID();

    private string UserName => Session.GetUserName();

    private int CompanyID => Session.GetCompanyID();

    private int OEMID => Session.GetOEMID();

    private int GroupID => Session.GetGroupID();

    private string suffix_param { get; set; } = "";


    /// <summary>
    /// 获取当前用户可导出模版
    /// </summary>
    /// <returns></returns>
    public async Task<List<ExportModel>> GetExportProductExcel()
    {
        if (Session.GetRole() == Role.DEV)
        {
            return await _dbContext.Export
                // .Where(m => m.State == BaseModel.StateEnum.Open)
                .ToListAsync();
        }
        
        var companyId = CompanyID;
        var oemId = OEMID;
        var validExportIds = await _dbContext.ExportProduct
            .WhenWhere(companyId > 0, m => m.CompanyID == companyId)
            .Where(m => m.State == BaseModel.StateEnum.Open)
            .Select(m => m.ExportId)
            .ToListAsync();


        var invalidExportIds = await _dbContext.ExportProduct
            .WhenWhere(companyId > 0, m => m.CompanyID == companyId)
            .Where(m => m.State == BaseModel.StateEnum.Close)
            .Select(m => m.ExportId)
            .ToListAsync();
        

        var model = _dbContext.Export
            .Where(m => m.State == BaseModel.StateEnum.Open)
            .WhenWhere(invalidExportIds.Any(), m => !invalidExportIds.Contains(m.ID));

        if (validExportIds.Any())
        {
            model = model.Where(m =>
                (validExportIds.Contains(m.ID) && m.Quality == ExportModel.QualityEnum.Private) ||
                m.Quality == ExportModel.QualityEnum.Public);
        }
        else
        {
            model = model.Where(m => m.Quality == ExportModel.QualityEnum.Public);
        }
     

        return await model.OrderBy(m => m.Quality).ToListAsync();
    }

    public async Task<object> ExportList()
    {
        var original = await GetExportProductExcel();

        var user = await _dbContext.User.Where(x => x.Id == UserID).FirstOrDefaultAsync();
        if (user == null)
        {
            throw new Exceptions.Users.NotFoundException("无法找到个人信息,刷新页面");
        }

        return new
        {
            exportOriginal = original,
            list = original.ToDictionary(key => key.Sign, value => value),
            language = _languagesCache.List()?.ToDictionary(m => m.ID, m => m),
            unit = _unitCache.List()?.ToDictionary(m => m.Sign, m => m),
            user.ProductConfig
        };
    }


    /// <summary>
    /// 获取公司分配导出模版信息
    /// </summary>
    /// <returns></returns>
    public async Task<List<ExpProductModel>> GetExportProduct()
    {
        return await _dbContext.ExportProduct
            .Where(x => x.CompanyID == CompanyID)
            .Where(x => x.OEMID == OEMID)
            .ToListAsync();
    }

    /// <summary>
    /// 获取公司分配导出模版信息
    /// </summary>
    /// <returns></returns>
    public async Task<List<ExpProductModel>> GetExportProduct(int Id)
    {
        return await _dbContext.ExportProduct
            .Where(x => x.CompanyID == CompanyID)
            .Where(x => x.OEMID == OEMID)
            .Where(x => x.ID == Id)
            .ToListAsync();
    }

    /// <summary>
    /// 获取模版信息
    /// </summary>
    /// <param name="exportIds"></param>
    /// <param name="Name"></param>
    /// <returns></returns>
    /// (string,List<Model>)
    public async Task<List<ExportModel>> getExportTemplate(List<int> exportIds, string? Name)
    {
        return await _dbContext.Export.Where(x => x.State == ExportModel.StateEnum.Open)
            .Where(m => exportIds.Contains(m.ID) || m.Quality == ExportModel.QualityEnum.Public)
            .WhenWhere(!string.IsNullOrEmpty(Name), x => x.Name.Contains(Name!))
            .OrderBy(x => x.Sort)
            .OrderByDescending(x => x.Quality)
            .Include(x => x.Platforms)
            .ToListAsync();
    }

    /// <summary>
    /// 获取导出产品默认详情
    /// </summary>
    /// <param name="exportId"></param>
    /// <returns></returns>
    public async Task<ExpProductModel?> GetExportProductInfo(int exportId)
    {
        var info = await _dbContext.ExportProduct
            .Where(x => x.CompanyID == CompanyID)
            // .Where(x => x.OEMID == OEMID)
            .FirstOrDefaultAsync(m => m.ExportId == exportId);
        if (info == null) return null;

        return info;
    }

    /// <summary>
    /// 更新关联信息状态
    /// </summary>
    /// <param name="id"></param>
    /// <param name="state"></param>
    /// <returns></returns>
    public async Task<bool> UpdateState(int id, ExportModel.StateEnum state)
    {
        var info = await _dbContext.ExportProduct.FirstOrDefaultAsync(m => m.ID == id);
        if (info == null)
        {
            return false;
        }

        info.State = state;
        _dbContext.ExportProduct.Update(info);
        return await _dbContext.SaveChangesAsync() > 0;
    }

    /// <summary>
    ///  添加关联信息   
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public async Task<bool> AddExportProduct(ExportUp exportUp)
    {
        ExpProductModel Data = new()
        {
            UserID = UserID,
            GroupID = GroupID,
            CompanyID = CompanyID,
            OEMID = OEMID,
            Username = UserName,
            ExportId = exportUp.ID,
            PlatformId = exportUp.platformId,
            State = exportUp.state,
            CreatedAt = DateTime.Now,
        };
        await _dbContext.ExportProduct.AddAsync(Data);
        return await _dbContext.SaveChangesAsync() > 0;
    }

    /// <summary>
    /// 获取已分配用户公司id
    /// </summary>
    /// <param name="exportId"></param>
    /// 
    /// <param name="platformId"></param>
    /// <param name="companyIds"></param>
    /// <returns></returns>
    public async Task<IEnumerable<int>> GetAllot(int exportId, int platformId, IEnumerable<int> companyIds)
    {
        return await _dbContext.ExportProduct
            .Where(m => m.ExportId == exportId && m.PlatformId == platformId && companyIds.Contains(m.CompanyID))
            .WhenWhere(exportId > 0, m => m.ExportId == exportId)
            .WhenWhere(platformId > 0, m => m.PlatformId == platformId)
            .WhenWhere(companyIds.Any(), m => companyIds.Contains(m.CompanyID))
            .Select(m => m.CompanyID).ToListAsync();
    }

    /// <summary>
    /// 移除分配信息
    /// </summary>
    /// <param name="exportId"></param>
    /// <param name="platformId"></param>
    /// <param name="companyId"></param>
    /// <param name="oemId"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DelAllot(int exportId, int platformId, int companyId, int oemId)
    {
        var info = await _dbContext.ExportProduct
            .WhenWhere(exportId > 0, m => m.ExportId == exportId)
            .WhenWhere(platformId > 0, m => m.PlatformId == platformId)
            .WhenWhere(companyId > 0, m => m.CompanyID == companyId)
            .WhenWhere(oemId > 0, m => m.OEMID == oemId)
            .FirstOrDefaultAsync();

        if (info == null)
        {
            return ReturnArr(false, "找不到相关数据");
        }

        _dbContext.ExportProduct.Remove(info);
        await _dbContext.SaveChangesAsync();
        return ReturnArr(true);
    }

    /// <summary>
    /// 添加分配
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> AddAllot(Models.View.Setting.updateAllotDto updateAllotDto, int oemId)
    {
        var data = new Models.Export.ExportProduct()
        {
            ExportId = updateAllotDto.id,
            Username = updateAllotDto.username,
            PlatformId = updateAllotDto.platformId,
            CompanyID = updateAllotDto.companyId,
            OEMID = oemId,
            UserID = UserID,
            GroupID = GroupID,
        };

        await _dbContext.ExportProduct.AddAsync(data);

        await _dbContext.SaveChangesAsync();

        return ReturnArr(true);
    }

    public async Task<List<ProductModel>> GetExportIds(ListDto req)
    {
        var model = _productService.ProductModelQuery(req);
        return model.ToList();
    }


    private async Task<Dictionary<string, string[]>> SetLanguageAttribute(
        Dictionary<int, AttributeViewMode>? Attributes, Dictionary<string, LanguageInfo> DicLanInfo, Languages Language)
    {
        var Data = new Dictionary<string, string[]>();
        string lan = Language.ToString().ToLower();
        var DicSketch = DicLanInfo[lan].Sketch; //Enum

        if (Attributes is not null)
        {
            var DataItem = Array.Empty<string>().ToList();
            foreach (var Attribute in Attributes)
            {
                if (DicLanInfo[lan] is not null && DicSketch is not null && DicSketch.Count() > 0)
                {
                    foreach (var item in Attribute.Value.Value)
                    {
                        foreach (var Sketch in DicSketch)
                        {
                            if (Sketch.ContainsKey(item))
                            {
                                DataItem.Add(item);
                            }
                            else
                            {
                                DataItem.Add(item);
                            }
                        }

                        Data.Add(Attribute.Value.Name, DataItem.Distinct().ToArray());
                    }
                }
                else
                {
                    Data.Add(Attribute.Value.Name, Attribute.Value.Value);
                }
            }
        }

        await Task.CompletedTask;
        return Data;
    }

    private string GetImgMain(ERP.Data.Products.VariantImages variantImages, ERP.Data.Products.Images Images)
    {
        if (Images is not null && variantImages is not null && variantImages.Main is not null)
            return Images.Where(x => x.ID == variantImages.Main)
                       .Select(x => GetImageUrl(x.Path ?? string.Empty) + suffix_param).ToString() ??
                   string.Empty;

        return string.Empty;
    }


    private int FullNoTemplateData(Worksheet Sheet, int StartRow, List<List<string>> Data)
    {
        var RowNum = Data.Count;
        var ExportNum = 0;
        for (var i = 0; i <= RowNum - 1; i++)
        {
            var RowData = Data[i];
            var ColNum = RowData.Count;
            Sheet.Cells.CreateRange(StartRow + i, 0, 1, ColNum);
            var Cells = Sheet.Cells;
            var CanExport = true;
            // 检测当前数据中是否有超过最大字符的数据,存在则跳过导出
            RowData.ForEach(item =>
            {
                if (item.IsNotNull() && item.Length > 32767)
                {
                    CanExport = false;
                    //Log(I18N.Instance.Frm_Export3_Log30(RowData[1], item.Substring(0, 50)));
                    return;
                }
            });
            // 可导出
            if (CanExport)
            {
                ExportNum += 1;
                for (var ColIndex = 0; ColIndex <= ColNum - 1; ColIndex++)
                {
                    var ExportData = RowData[ColIndex];
                    Cells[StartRow + i, ColIndex].PutValue(ExportData, false, false);
                }
            }
        }

        return ExportNum;
    }

    public async Task<object> GetExportProductIds(int eid, DataRange_2b range)
    {
        // 获取导出产品ids
        var syncProduct = await _syncExportServiceService.GetExportProductIdsByTask(eid,range);


        return _dbContext.SyncProduct
            .Where(m => syncProduct.Contains(m.ID))
            .AsEnumerable()
            .GroupBy(m => m.Sku)
            .Select(m => new
            {
                Sid = m.First().ID
            }).ToList();
    }

    public async Task<string> AllotExportProduct(AllotExportProductDto req)
    {
        return await _syncProductService.DistributionCodeToExportProducts(req.Sid, req.Mode, req.Cover);
    }


    private string GetImageUrl(string str)
    {
        var src = str.Trim('/');
        if (src.StartsWith("http"))
        {
            return src;
        }

        var domain = _oemProvider.OEM.Oss.BucketDomain;
        return $"https://{domain}/{src}";
    }
}