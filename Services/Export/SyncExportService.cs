using System.Diagnostics;
using ERP.Data;
using ERP.Export.Templates;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.Export;
using ERP.Models.View.Products.Export;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using ERP.Enums;
using ERP.Enums.Rule.Config.Internal;
using ERP.Exceptions.Export;
using ERP.Models.Biter;
using ERP.Models.DB.Identity;
using ERP.Models.DB.Stores;
using ERP.Models.Product;
using ERP.Services.Caches;
using ERP.Services.Product;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ERP.Models.Stores;

namespace ERP.Services.Export
{
    using SyncExportModel = Models.Export.SyncExport;

    public class SyncExportService : BaseService
    {
        public readonly DBContext _dbContext;
        private readonly ISessionProvider _sessionProvider;
        private readonly ILogger<SyncExportService> _logger;
        private readonly LanguagesCache _languagesCache;
        private readonly ProductService _productService;
        private ISession _session => _sessionProvider.Session!;

        private int _userId => _session.GetUserID();

        private int _groupId => _session.GetGroupID();

        private int _companyId => _session.GetCompanyID();

        private int _oemId => _session.GetOEMID();

        public SyncExportService(DBContext dbContext, LanguagesCache languagesCache,
            ProductService productService, ISessionProvider sessionProvider, ILogger<SyncExportService> logger)
        {
            _dbContext = dbContext;
            _languagesCache = languagesCache;
            _productService = productService;
            _sessionProvider = sessionProvider;
            _logger = logger;
        }


        /// <summary>
        /// 添加任务集信息
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<bool> AddSyncExport(Models.Export.SyncExport data)
        {
            await _dbContext.SyncExport.AddAsync(data);

            return await _dbContext.SaveChangesAsync() > 0;
        }


        /// <summary>
        /// 任务集信息
        /// </summary>
        /// <param name="exportQuery"></param>
        /// <returns></returns>
        public async Task<object> GetSyncExportPage(ExportQuery exportQuery)
        {
            var companyId = _companyId;
            var oemId = _oemId;
            var groupId = _groupId;
            return await _dbContext.SyncExport
                .Where(m => m.OEMID == oemId && m.CompanyID == companyId)
                .WhenWhere(groupId > 0, m => m.GroupID == groupId)
                .WhenWhere(exportQuery.Name.IsNotNull(), m => m.Name!.Contains(exportQuery!.Name))
                .WhenWhere(exportQuery.Language > 0 && exportQuery.Language != null,
                    m => m.LanguageId == exportQuery!.Language)
                .WhenWhere(exportQuery.ExportSign > 0 && exportQuery.ExportSign != null,
                    m => m.ExportSign == (TemplateType)exportQuery!.ExportSign.Value)
                .OrderByDescending(m => m.ID)
                .ThenBy(m => m.Sort)
                .Select(m => new
                    { m.ID, m.Name, m.Amount, m.ExportSign, m.LanguageId, m.CreatedAt, m.UpdatedAt, m.Append })
                .ToPaginateAsync(exportQuery.Page ?? -1, exportQuery.Limit ?? -1);
        }

        public async Task<object> GetSyncExportPage(ExportQuery exportQuery, DataRange_2b range)
        {
            return await _dbContext.SyncExport.WhereRange(range, _companyId, _groupId, _userId)
                .WhenWhere(exportQuery.Name.IsNotNull(), m => m.Name!.Contains(exportQuery!.Name))
                .WhenWhere(exportQuery.Language > -1 && exportQuery.Language != null,
                    m => m.LanguageId == exportQuery!.Language)
                .WhenWhere(exportQuery.ExportSign > 0 && exportQuery.ExportSign != null,
                    m => m.ExportSign == (TemplateType)exportQuery!.ExportSign.Value)
                .OrderByDescending(m => m.ID)
                .ThenBy(m => m.Sort)
                .Select(m => new
                    { m.ID, m.Name, m.Amount, m.ExportSign, m.LanguageId, m.CreatedAt, m.UpdatedAt, m.Append })
                .ToPaginateAsync(exportQuery.Page ?? -1, exportQuery.Limit ?? -1);
        }

        /// <summary>
        /// 更新导出任务名
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<Result> EditExport(EditExportDto req)
        {
            var info = await _dbContext.SyncExport.Where(m => m.ID == req.Id).FirstOrDefaultAsync();

            if (info == null)
            {
                return Result.Fail("找不到相关数据");
            }

            info.Name = req.Name;
            var param = JObject.Parse(info.Param);
            param["RemoveDescriptionHtml"] = req.RemoveDescriptionHtml;
            info.Param = param.ToString(Formatting.None);
            
            await _dbContext.SaveChangesAsync();

            return Result.Ok();
        }

        /// <summary>
        /// 根据id更新导出任务
        /// </summary>
        /// <param name="id"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<Result> UpdateExport(int id, SyncExportModel data)
        {
            var syncExport = await _dbContext.SyncExport.Where(m => m.ID == id).FirstOrDefaultAsync();

            if (syncExport is null)
            {
                return Result.Fail("找不到相关数据");
            }

            if (data.Param != null) syncExport.Param = data.Param;

            if (data.LanguageId > -1) syncExport.LanguageId = data.LanguageId;

            if (data.Append != null) syncExport.Append = data.Append;

            await _dbContext.SaveChangesAsync();

            return Result.Ok();
        }

        /// <summary>
        /// 主键删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ReturnStruct> DeleteExport(List<int> ids)
        {
            var list = await _dbContext.SyncExport
                .Where(m => ids.Contains(m.ID))
                .ToListAsync();

            if (list.Count <= 0)
            {
                return ReturnArr(false, "找不到相关数据");
            }

            _dbContext.SyncExport.RemoveRange(list);
            await _dbContext.SaveChangesAsync();
            return ReturnArr(true);
        }

        public async Task<bool> DeleteExport(int id)
        {
            var info = await _dbContext.SyncExport
                .Where(m => m.ID == id)
                .FirstOrDefaultAsync();
            
            if (info == null)
            {
                throw new NotFoundException("找不到相关数据#1");
            }

            var tasks = await _dbContext.ExportTasks.Where(t => t.SyncExportId == id).ToArrayAsync();

            _dbContext.SyncExport.Remove(info);
            _dbContext.ExportTasks.RemoveRange(tasks);
            return await _dbContext.SaveChangesAsync() > 0;
        }

        /// <summary>
        /// 条件删除指定导出任务
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public async Task<ReturnStruct> DelExport(object where)
        {
            //return SyncExportModel::query()->where($where)->delete();
            return await Task.FromResult(ReturnArr(true));
        }

        /// <summary>
        /// 删除导出任务下关联产品信息
        /// </summary>
        /// <param name="eid"></param>
        /// <returns></returns>
        public async Task<ReturnStruct> DeleteExportProduct(List<int> LisEid)
        {
            //return SyncExportProductModel::query()
            //->eid($eid)
            //->companyId($this->getSession()->getCompanyId())
            //->oemId($this->getSession()->getOemId())
            //->groupId($this->getSession()->getGroupId())
            ////            ->userId($this->getSession()->getUserId())
            //->delete();
            var companyId = _companyId;
            var oemId = _oemId;
            var groupId = _groupId;
            var info = await _dbContext.SyncExportProduct
                .WhenWhere(oemId > 0, m => m.OEMID == oemId)
                .WhenWhere(companyId > 0, m => m.CompanyID == companyId)
                .WhenWhere(groupId > 0, m => m.GroupID == groupId)
                .WhenWhere(LisEid.Count() > 0, m => LisEid.Contains(m.Eid))
                .ToListAsync();

            if (info == null)
            {
                return ReturnArr(false, "找不到相关数据");
            }

            _dbContext.SyncExportProduct.RemoveRange(info);
            return await _dbContext.SaveChangesAsync() > 0 ? ReturnArr(true) : ReturnArr(false);
        }

        /// <summary>
        /// 获取任务产品详情
        /// </summary>
        /// <param name="id"></param>
        /// <param name="range"></param>
        /// <returns></returns>
        public async Task<SyncExport?> GetInfoById(int id, DataRange_2b range = DataRange_2b.User)
        {
            return await _dbContext.SyncExport.WhereRange(range, _companyId, _groupId, _userId)
                .Where(m => m.ID == id)
                .FirstOrDefaultAsync();
        }

        public async Task<SyncProductModel?> GetSyncProductInfoById(int id)
            => await _dbContext.SyncProduct.FirstOrDefaultAsync(m => m.ID == id);

        /// <summary>
        /// 获取任务产品详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<List<SyncExportModel>?> GetInfoByIds(int[] Ids)
        {
            var companyId = _companyId;
            var oemId = _oemId;
            var groupId = _groupId;
            return await _dbContext.SyncExport
                .WhenWhere(oemId > 0, m => m.OEMID == oemId)
                .WhenWhere(companyId > 0, m => m.CompanyID == companyId)
                .WhenWhere(groupId > 0, m => m.GroupID == groupId)
                .Where(m => Ids.Contains(m.ID))
                .ToListAsync();
        }


        /// <summary>
        /// 获取导出任务下产品同步信息ids
        /// </summary>
        /// <param name="eid"></param>
        /// <returns></returns>
        public async Task<List<int>> GetExportProductIdsByTask(int eid, DataRange_2b? range = null)
        {
            return await _dbContext.SyncExportProduct.WhereRange(range, _companyId, _groupId, _userId)
                .WhenWhere(eid > 0, m => m.Eid == eid)
                .Select(m => m.Sid)
                .ToListAsync();
        }

        /// <summary>
        /// 获取导出任务下产品同步信息                                                                    
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<ReturnStruct> GetExportProductIds(List<int> ids)
        {
            //return SyncExportProductModel::query()
            //        ->InId($id)
            //        ->companyId($this->getSession()->getCompanyId())
            //        ->oemId($this->getSession()->getOemId())
            //        ->groupId($this->getSession()->getGroupId())
            //        //            ->userId($this->getSession()->getUserId())
            //        ->get()
            //        ->toArray();

            var list = await _dbContext.SyncExportProduct
                .Where(m => m.OEMID == _oemId)
                .Where(m => m.CompanyID == _companyId)
                .Where(m => m.GroupID == _groupId)
                .WhenWhere(ids.Count > 0, m => ids.Contains(m.ID))
                .ToListAsync();

            return list is null || list.Count <= 0 ? ReturnArr(false, msg: "没有找到数据") : ReturnArr(true, "", list);
        }


        /// <summary>
        /// 更新导出任务产品
        /// </summary>
        /// <param name="where"></param>
        /// <param name="updateData"></param>
        /// <returns></returns>
        public async Task<Result> UpdateExportProduct(Expression<Func<SyncExportProduct, bool>> where,
            SyncExportProduct data)
        {
            var list = await _dbContext.SyncExportProduct.Where(where).ToListAsync();

            if (list.Count <= 0)
            {
                return Result.Fail("找不到相关数据");
            }

            foreach (var item in list)
            {
                if (data.LanguageId < 0)
                {
                    item.LanguageId = data.LanguageId;
                }

                if (Enum.IsDefined(data.Language))
                {
                    item.Language = data.Language;
                }
            }

            await _dbContext.SaveChangesAsync();
            return Result.Ok();
        }


        /// <summary>
        /// 导出任务下产品数自减
        /// </summary>
        /// <param name="id"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public async Task<bool> ExportReduce(int id, int count = 1)
        {
            var info = await _dbContext.SyncExport.Where(m =>
                m.OEMID == _oemId &&
                m.CompanyID == _companyId &&
                m.ID == id
            ).FirstOrDefaultAsync();

            if (info == null)
            {
                throw new Exceptions.SyncExport.NotFoundException("找不到导出相关数据");
            }

            info.Amount = info.Amount - count;

            if (info.Amount < 0)
            {
                info.Amount = 0;
            }

            await _dbContext.SyncExport.SingleUpdateAsync(info);

            return await _dbContext.SaveChangesAsync() > 0;
        }


        public async Task<int?> AddExportTask(AddSyncExportDto req)
        {
            var tempLang = _languagesCache.List();
            if (tempLang is null)
            {
                throw new SavedException("language cache is null");
            }

            var language = tempLang.ToDictionary(m => Enum.Parse<Languages>(m.Code, true), m => m);

            var languageId = 0;

            if (req.Params.Language != Languages.Default)
            {
                if (!language.ContainsKey(req.Params.Language))
                {
                    throw new SavedException($"找不到 {req.Params.Language.GetDescriptionByKey("Name")}语种");
                }

                languageId = language[req.Params.Language].ID;
            }

            var model = new SyncExportModel(req)
            {
                UserID = _userId,
                GroupID = _groupId,
                CompanyID = _companyId,
                OEMID = _oemId,
                LanguageId = languageId,
                RelationId = req.RelationId,
            };
            var result = await AddSyncExport(model);
            if (result)
            {
                return model.ID;
            }

            return null;
        }

        public async Task<Result> CopyExport(CopyExportDto req)
        {
            var languageCache = _languagesCache.List()!.ToDictionary(m => Enum.Parse<Languages>(m.Code, true), m => m);
            var message = "克隆成功";
            // 只有初次创建导出任务会调用这个方法, 多语言导出时需要保证只复制一份产品
            var se = await _dbContext.SyncExport.Where(se => se.ID == req.TaskId).FirstOrDefaultAsync();
            if (se is null)
                return Result.Fail("导出任务不存在");
            
            var transaction = await _dbContext.Database.BeginTransactionAsync();
            try
            {
                SyncProductModel? sp = null;
                ProductModel? product = null;
                // 当存在多语言关联id时, 查询已存在的spid
                if (se.RelationId is not null)
                {
                    var sids = await _dbContext.SyncExportProduct.Where(sep => sep.Eid == se.RelationId).Select(sep => sep.Sid)
                        .ToArrayAsync();
                    sp = await _dbContext.SyncProduct.Where(p => sids.Contains(p.ID) && p.SourceId == req.ProductId)
                        .FirstOrDefaultAsync();
                    if (sp is not null)
                        product = await _dbContext.Product.Where(p => p.ID == sp.Pid).FirstOrDefaultAsync();
                } 
                else
                {
                    var cloneViewModel = await _productService.ImportClone(req.ProductId, req.ExcelRole.Language);
                    product = cloneViewModel.Product;
                    sp = await InsertSyncProduct(cloneViewModel, req.ProductConfig);
                }
                
                Debug.Assert(sp is not null);
                Debug.Assert(product is not null);

                var syncExportProductModel = new SyncExportProduct(_session)
                {
                    Eid = req.TaskId,
                    Sid = sp.ID,
                    Language = req.ExcelRole.Language,
                    LanguageId = req.ExcelRole.Language == Languages.Default
                        ? null
                        : languageCache[req.ExcelRole.Language].ID,
                    ExportSign = req.Format,
                    PlatformId = req.PlatformId
                };

                await AddSyncExportProduct(syncExportProductModel);
                await ExportIncrement(req.TaskId);
                await transaction.CommitAsync();

                // 多语言导出并且存在关联id时, ean只需在第一次复制产品时分配
                if (se.RelationId is null && req.ProductConfig.AutoAllotProduceCode)
                {
                    await _productService.DistributeEanUpc(new() { { product, se.LanguageId } },
                        Enum.Parse<AllotExportProductDto.ModeEnum>(req.ProductConfig.ProduceCodeType, true),
                        false);
                    sp.EanupcStatus = SyncProductModel.EanupcStatuss.ALL;
                    await _dbContext.SaveChangesAsync();
                }
            }
            catch (ERP.Exceptions.Product.EanUpcNotEnoughException e)
            {
                message += ", " + e.Message;
            }
            catch (Exception e)
            {
                await transaction.RollbackAsync();
                _logger.LogError(e, e.Message);
                throw;
            }

            return Result.Ok().WithSuccess(message);
        }

        /// <summary>
        /// 导出任务下产品数自增
        /// </summary>
        /// <param name="id"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public async Task ExportIncrement(int id, int count = 1)
        {
            var info = await _dbContext.SyncExport.Where(m => m.ID == id).FirstOrDefaultAsync();

            if (info is null)
            {
                throw new NotFoundException("SyncExport is null");
            }

            info.Amount += count;
            await _dbContext.SyncExport.SingleUpdateAsync(info);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<int> AddSyncExportProduct(SyncExportProduct model)
        {
            await _dbContext.SyncExportProduct.AddAsync(model);
            await _dbContext.SaveChangesAsync();
            return model.ID;
        }

        public async Task<SyncProductModel> InsertSyncProduct(CopyProductViewModel vm, ProductConfig productConfig)
        {
            var insertModel = new SyncProductModel(_session)
            {
                ProductName = vm.Product.Title,
                ImgUrl = vm.Product.MainImage,
                Pid = vm.Product.ID,
                SourceId = vm.SourceId,
                SourceItemId = vm.SourceItemId,
                Sku = !string.IsNullOrEmpty(vm.Product.Sku) ? vm.Product.Sku: Helpers.SkuCode(),
                SkuType = productConfig.SkuPrefix
                    ? SyncProductModel.SkuTypes.TYPE_TRUE
                    : SyncProductModel.SkuTypes.FALSE,
                SkuSuffix = productConfig.SkuSuffix
                    ? SyncProductModel.SkuSuffixs.TRUE
                    : SyncProductModel.SkuSuffixs.FALSE,
                TitleSuffix = productConfig.TitleSuffix
                    ? SyncProductModel.TitleSuffixs.TRUE
                    : SyncProductModel.TitleSuffixs.FALSE,
                ProductType = SyncProductModel.ProductTypes.Export,
                CategoryId = vm.Product.CategoryId,
                SubCategoryId = vm.Product.SubCategoryId,
                AlreadyUpload = true,
                Uploading = true,
                TypeData = null,
                TypeDataId = null,
            };
            await _dbContext.SyncProduct.AddAsync(insertModel);
            await _dbContext.SaveChangesAsync();
            return insertModel;
        }
    }
}