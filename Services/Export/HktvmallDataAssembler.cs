﻿using ERP.Data;
using ERP.Data.Products;
using ERP.Enums;
using ERP.Export;
using ERP.Export.ExportCommon;
using ERP.Export.Models;
using ERP.Models.Export;
using ERP.Models.Product;
using ERP.Services.Product;
using ERP.Services.Stores;
using Sy.Security;
using ModelProduct = ERP.Data.Products.Product;

namespace ERP.Services.Export;

/// <summary>
/// 适用于Hktvmall的数据组装器
/// </summary>
public class HktvmallDataAssembler : DataAssembler
{
    private readonly ProductService _productService;
    private readonly ConvertProduct _convertProduct;
    private readonly ILogger<HktvmallDataAssembler> _logger;

    public HktvmallDataAssembler(ProductService productService, DBContext dbContext, SyncProductService syncProductService, ConvertProduct convertProduct, ILogger<HktvmallDataAssembler> logger) : base(productService, dbContext, syncProductService, convertProduct, logger)
    {
        _productService = productService;
        _convertProduct = convertProduct;
        _logger = logger;
    }

    /// <summary>
    /// hktvmall需要两种语言, 默认语言为繁体中文
    /// </summary>
    /// <param name="productModel"></param>
    /// <param name="language"></param>
    /// <returns></returns>
    protected override async Task<Result<Dictionary<Languages, Data.Products.Product>>> GetSupportedProductStructs(ProductModel productModel, Languages language)
    {
        var pid = productModel.ID;
        var zhLangId = (int)Math.Log2((long)Languages.ZH_TW) + 1;
        var enLangId = (int)Math.Log2((long)Languages.EN) + 1;
        var zhItem = await _productService.GetItemInfoByLangId(pid, zhLangId);
        var enItem = await _productService.GetItemInfoByLangId(pid, enLangId);
        
        // 用默认语言填充缺少项
        if (zhItem is null)
            zhItem = await _productService.GetItemInfoByLangId(pid, 0);
        else if (enItem is null)
            enItem = await _productService.GetItemInfoByLangId(pid, 0);
        
        if (zhItem is null || enItem is null)
            return Result.Fail($"Pid[{pid}]数据缺少繁体中文或英文语言包#1");

        var zhProStruct = await _productService.GetProductStruct(zhItem.FilePath, productModel);
        var enProStruct = await _productService.GetProductStruct(enItem.FilePath, productModel);
        
        return new Dictionary<Languages, ModelProduct>()
        {
            { Languages.ZH_TW, zhProStruct },
            { Languages.EN, enProStruct },
        };
    }

    protected override List<ProductsItem> AssembleVariants(MessyData data, AssembleContext assembleContext)
    {
        var items = base.AssembleVariants(data, assembleContext);

        var proStruct = data.SupportedProductStructs[Languages.EN];
        var result = new List<ProductsItem>(items.Count);

        foreach (var old in items)
        {
            var item = new HktvmallProductItem(old);
            item.NameEn = proStruct.Title ??  string.Empty;
            item.BulletPointEn = proStruct.GetSketches() ?? new List<string>();
            
            var description = _convertProduct.GetProductDesc(proStruct.GetDescription()!, data.Task.OEM!);
            description = ExportUtils.RemoveTag(description, new[]
            {
                "script",
                "style"
            }, new string[] { }).TrimEnd(Environment.NewLine.ToArray());
            item.DescriptionEn = description;
            
            item.Description = ExportUtils.RemoveTag(item.Description ?? string.Empty, new[]
            {
                "script",
                "style"
            }, new string[] { }).TrimEnd(Environment.NewLine.ToArray());
            result.Add(item);
        }

        return result;
    }

    protected override string GetImageUrl(ExportTask task, string str)
    {
        var url = base.GetImageUrl(task, str);

        return DownloadImage(task.TempExtPath, url);
    }
    
    private static string DownloadImage(string path, string? url)
    {
        if (string.IsNullOrEmpty(url))
            return string.Empty;

        var hash = MD5.Encrypt(url);
        var filename = $"{hash}.jpg";
        var tempPath = $"{path}/{filename}";
        if (!File.Exists(tempPath))
        {
            ProductExportHelper.DownloadAsync(url, tempPath).GetAwaiter().GetResult();
        }

        return filename;
    }
}