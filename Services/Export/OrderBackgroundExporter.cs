﻿using ERP.Data;
using ERP.Models.DB.Identity;
using ERP.Models.Export;
using ERP.Models.View.Order;
using ERP.Services.DB.Logistics;
using ERP.Services.DB.Orders;
using ERP.Services.Stores;
using ERP.ViewModels.Order.AmazonOrder;
using MediatR;
using Newtonsoft.Json;
using NPOI.OpenXml4Net.OPC;
using NPOI.SS.UserModel;
using NPOI.XSSF.Streaming;
using NPOI.XSSF.UserModel;

namespace ERP.Services.Export;

public class OrderBackgroundExporter
{
    private readonly IServiceProvider _serviceProvider;
    private readonly ILogger<OrderBackgroundExporter> _logger;
    private readonly DBContext _dbContext;

    public OrderBackgroundExporter(IServiceProvider serviceProvider, ILogger<OrderBackgroundExporter> logger, DBContext dbContext)
    {
        _serviceProvider = serviceProvider;
        _logger = logger;
        _dbContext = dbContext;
    }

    public async Task<Unit> ExportOrders(ExportQueueItem request, CancellationToken stoppingToken)
    {
        var entry = _dbContext.Entry(request.Task);
        var task = entry.Entity;
        await entry.ReloadAsync(stoppingToken);
        task.Locked = true;
        await _dbContext.SaveChangesAsync(stoppingToken);
        
        await entry.Reference(t => t.User).LoadAsync(stoppingToken);
        await entry.Reference(t => t.User).TargetEntry!.Reference(u => u.OrderUnitConfig).LoadAsync(stoppingToken);

        var unitInfo = entry.Entity.User!.OrderUnitConfig;
        var orderChoose = JsonConvert.DeserializeObject<OrderChooseFieldParam>(entry.Entity.Params)!;
        
        task.TotalCount = orderChoose.OrderIDs.Count;

        try
        {
            var result = await DownExcel(entry.Entity, orderChoose, unitInfo);
            if (result.IsFailed)
                task.MarkFailed(result.Errors.First().Message);
        }
        catch (Exception e)
        {
            task.MarkFailed(e.ToString());
        }

        task.Locked = false;
        await _dbContext.SaveChangesAsync(default);
        return Unit.Value;
    }


    public async Task<Result> DownExcel(ExportTask task, OrderChooseFieldParam orderChoose, OrderUnitConfig orderUnit)
    {
        #region "获取表头"

        int EmptyNum = 0; //记录订单导出产品之前的选中项个数
        Dictionary<int, List<string>> HeaderLst = new();
        List<string> Checked = new List<string>();

        if (orderChoose.ChkOrderNum) Checked.Add("订单号");
        if (orderChoose.ChkOrderFinance)
        {
            Checked.Add("订单总价");
            Checked.Add("币种");
        }

        if (orderChoose.ChkOrderTime) Checked.Add("订单时间");
        if (orderChoose.ChkStore) Checked.Add("所属店铺");
        if (orderChoose.ChkSellerID) Checked.Add("SellerID");
        if (orderChoose.ChkHandlingFee) Checked.Add("手续费");
        if (orderChoose.ChkCostLossFee) Checked.Add("成本损失费");
        EmptyNum = Checked.Count;
        //订单产品信息
        if (orderChoose.ChkOrderProducts)
        {
            Checked.Add("产品图");
            Checked.Add("产品名称");
            Checked.Add("asin");
            Checked.Add("Sku");
            Checked.Add("购买数量");
            Checked.Add("单价");
            Checked.Add("亚马逊运费");
            Checked.Add("总价");
        }

        //收件人信息
        if (orderChoose.ChkReceiver)
        {
            Checked.Add("买家姓名");
            Checked.Add("电话号码");
            Checked.Add("邮编");
            Checked.Add("国家");
            Checked.Add("州/省/区域");
            Checked.Add("城市");
            Checked.Add("区域");
            Checked.Add("详细地址1");
            Checked.Add("详细地址2");
            Checked.Add("详细地址3");
        }

        //采购信息
        if (orderChoose.ChkPurchase)
        {
            Checked.Add("采购总价");
            Checked.Add("国内物流追踪号");
        }

        //运单信息
        if (orderChoose.ChkWayBill)
        {
            Checked.Add("国际物流运单号");
            Checked.Add("运费");
        }

        HeaderLst.Add(0, Checked);

        #endregion

        int RowIndex = 0;
        string SheetName = "Order";
        IWorkbook workbook = new SXSSFWorkbook(new XSSFWorkbook());
        var Sheet = workbook.CreateSheet(SheetName);

        if (task.State == ExportTask.TaskState.Waiting)
        {
            var result = FullData(Sheet, RowIndex, HeaderLst);
            if (!result.Item1.IsNullOrWhiteSpace())
            {
                return Result.Fail("创建表头失败");
            }
            RowIndex += result.Item2;
            task.TempRowIndex = JsonConvert.SerializeObject(RowIndex);
            task.State = ExportTask.TaskState.Running;
            await _dbContext.SaveChangesAsync(default);
        }
        else
        {
            if (!File.Exists(task.TempPath))
            {
                _logger.LogWarning("任务{Task}临时文件丢失, 尝试重置导出任务", task);
                task.State = ExportTask.TaskState.Waiting;
                task.CompletedCount = 0;
                task.TotalCount = 0;
                await _dbContext.SaveChangesAsync(default);
                return Result.Ok();
            }

            var openStream = File.Open(task.TempPath, FileMode.Open, FileAccess.Read, FileShare.Read);
            workbook = new SXSSFWorkbook(new XSSFWorkbook(OPCPackage.Open(openStream)));

            var rowIndex = JsonConvert.DeserializeObject<int?>(task.TempRowIndex!);
            if (rowIndex is null)
                return Result.Fail("读取临时表格异常");
            RowIndex = rowIndex.Value;
        }

        //获取订单数据
        //"正在获取订单数据..."
        List<int> TempOrders = new();
        TempOrders = orderChoose.OrderIDs;
        do
        {
            var TempInvoke = orderChoose.OrderIDs.Take(orderChoose.ItemCount).ToList(); //???
            List<OrderData>? ExcelOrders;
            try
            {
                ExcelOrders = await getExportOrder(task, TempInvoke, orderChoose.ChkPurchase, orderChoose.ChkWayBill,
                    orderUnit);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "构建订单数据失败");
                return Result.Fail(e.ToString());
            }
            if (ExcelOrders.IsNotNull() && ExcelOrders!.Count > 0)
            {
                foreach (var Order in ExcelOrders)
                {
                    Dictionary<int, List<string>> OData = new Dictionary<int, List<string>>();
                    var products = Order.OrderProducts.ToArray();
                    for (int i = 0; i <= products.Length - 1; i++)
                    {
                        var product = products[i];
                        var oneLine = BuildOneLine(orderChoose, i == 0, Order, EmptyNum, product);

                        OData.Add(i, oneLine);
                        if (!orderChoose.ChkOrderProducts) break;
                    }

                    var res = FullData(Sheet, RowIndex, OData);
                    if (res.Item1.IsNullOrWhiteSpace())
                    {
                        RowIndex += res.Item2;
                    }
                    else
                    {
                        return Result.Fail(res.Item1);
                    }
                }
            }
            else
            {
                return Result.Fail("获取订单数据失败, 数量为0");
            }

            TempOrders.RemoveRange(0, TempInvoke.Count);
            task.TempRowIndex = JsonConvert.SerializeObject(RowIndex);
            task.CompletedCount += TempInvoke.Count;
            await _dbContext.SaveChangesAsync(default);
        } while (TempOrders.Count > 0);

        if (task.CompletedCount >= task.TotalCount)
            task.State = ExportTask.TaskState.Exported;
        await _dbContext.SaveChangesAsync(default);

        await using var stream = File.Open(task.TempPath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Write);
        workbook.Write(stream);
        stream.Close();
        workbook.Close();
        ((SXSSFWorkbook)workbook).Dispose();
        return Result.Ok().WithSuccess("导出完成");
    }

    private List<string> BuildOneLine(OrderChooseFieldParam orderChoose, bool isMain, OrderData Order, int EmptyNum, OrderData.Product TempProduct)
    {
        List<string> oneLine = new();
        //产品第一行添加订单及收件人信息
        if (isMain)
        {
            if (orderChoose.ChkOrderNum) oneLine.Add(Order.OrderNum);
            if (orderChoose.ChkOrderFinance)
            {
                oneLine.Add(Order.OrderFinance);
                oneLine.Add(Order.OrderUnit);
            }

            if (orderChoose.ChkOrderTime) oneLine.Add(Order.OrderTime);
            if (orderChoose.ChkStore) oneLine.Add(Order.StoreName);
            if (orderChoose.ChkSellerID) oneLine.Add(Order.SellerID);
            if (orderChoose.ChkHandlingFee) oneLine.Add($"{Order.HandlingFee}"); //手续费
            if (orderChoose.ChkCostLossFee) oneLine.Add(Order.CostLoss);
        }
        else
        {
            AddData(oneLine, "", EmptyNum);
        }

        //订单产品信息
        if (orderChoose.ChkOrderProducts)
        {
            oneLine.Add(TempProduct.ImageUrl);
            oneLine.Add(TempProduct.Name);
            oneLine.Add(TempProduct.Asin);
            oneLine.Add(TempProduct.Sku);
            oneLine.Add(TempProduct.Num.ToString());
            oneLine.Add($"{TempProduct.Price}");
            oneLine.Add($"{TempProduct.ShipFee}");
            oneLine.Add($"{TempProduct.Total}");
        }

        //收件人信息
        if (isMain && orderChoose.ChkReceiver)
        {
            oneLine.Add(Order.Receiver.receiver_name);
            oneLine.Add(Order.Receiver.receiver_phone);
            oneLine.Add(Order.Receiver.receiver_zip);
            oneLine.Add(Order.Receiver.receiver_nation);
            oneLine.Add(Order.Receiver.receiver_province);
            oneLine.Add(Order.Receiver.receiver_city);
            oneLine.Add(Order.Receiver.receiver_district);
            oneLine.Add(Order.Receiver.receiver_address);
            oneLine.Add(Order.Receiver.receiver_address2);
            oneLine.Add(Order.Receiver.receiver_address3);
        }

        //采购信息
        if (isMain && orderChoose.ChkPurchase && Order.Purchase.IsNotNull())
        {
            oneLine.Add(Order.Purchase.TotalPrice);
            oneLine.Add(Order.Purchase.PurchaseTrackNum);
        }

        //运单信息
        if (isMain && orderChoose.ChkWayBill && Order.WayBill.IsNotNull())
        {
            oneLine.Add(Order.WayBill.OrderNum);
            oneLine.Add(Order.WayBill.ShippingFee);
        }

        return oneLine;
    }


    public List<string> AddData(List<string> Row, string Data, int Num)
    {
        for (int i = 1; i <= Num; i++)
        {
            Row.Add(Data);
        }

        return Row;
    }

    public Tuple<string, int> FullData(ISheet Sheet, int StartRow, Dictionary<int, List<string>> Data)
    {
        string msg = "";
        var RowNum = Data.Count;
        var ExportNum = 0;
        for (int i = 0; i <= RowNum - 1; i++)
        {
            var Row = Sheet.CreateRow(StartRow + i);
            var RowData = Data[i];
            var ColNum = RowData.Count;
            var CanExport = true;
            var arrayData = RowData.ToArray();
            //检测当前数据中是否有超过最大字符的数据,存在则跳过导出
            foreach (var item in arrayData)
            {
                if (item.IsNotNull() && item.Length > 32767)
                {
                    CanExport = false;
                    msg = $"[跳过导出{arrayData[0]}]该数据: 无法存入Excel,存在有超过最大字符限制(32767)的数据[{item.Substring(0, 50)}]....";
                    return new Tuple<string, int>(msg, 0);
                }
            }

            //可导出
            if (CanExport)
            {
                ExportNum += 1;
                for (int ColIndex = 0; ColIndex <= ColNum - 1; ColIndex++)
                {
                    var Cell = Row.CreateCell(ColIndex);
                    var ExportData = arrayData[ColIndex];
                    Cell.SetCellValue(ExportData);
                }
            }
        }

        return new Tuple<string, int>("", ExportNum);
    }


    public async Task<List<OrderData>?> getExportOrder(ExportTask task,List<int> orderIds, bool isPurchase, bool isWaybill,
        OrderUnitConfig orderUnit)
    {
        var orderService = _serviceProvider.GetRequiredService<Order>();
        var purchaseService = _serviceProvider.GetRequiredService<Purchase.Purchase>();
        var storeService = _serviceProvider.GetRequiredService<StoreService>();
        var waybillService = _serviceProvider.GetRequiredService<WayBill>();
        var userUnitConfig = task.User!.UnitConfig;
        if (orderIds.IsNull() || orderIds.Count <= 0)
        {
            return null;
        }

        var orders = await orderService.GetAllOrderByIds(orderIds, null);
        if (orders.IsNull() || orders.Count <= 0)
        {
            return null;
        }


        List<Models.DB.Purchase.Purchase> purchaseLst = new();
        List<ERP.Models.DB.Logistics.Waybill> waybillLst = new();
        if (isPurchase)
        {
            purchaseLst = await purchaseService.GetPurchaseByOrderIds(orderIds);
        }

        //公司下全部店铺        
        // var storesLst = await _storeService.getStoreByCompanyId(CompanyID);
        List<OrderData> data = new List<OrderData>();
        foreach (var value in orders)
        {
            OrderData.PurchaseItem purchase = new OrderData.PurchaseItem();
            OrderData.WayBillItem waybill = new OrderData.WayBillItem();
            var store = await storeService.GetInfoById(value.StoreId);
            if (store.IsNull())
            {
                return null;
            }

            string unit = orderUnit.Total && !string.IsNullOrWhiteSpace(value.OrderTotal.Raw.CurrencyCode) ? value.OrderTotal.Raw.CurrencyCode : userUnitConfig;
            if (purchaseLst.IsNotNull() && purchaseLst.Count > 0)
            {
                decimal amount = 0;
                foreach (var item in purchaseLst)
                {
                    amount += item.PurchaseTotal.Money.To(unit);
                }

                MoneyRecordFinancialAffairs totalprice = new MoneyMeta(unit, amount);
                string TotalPrice = Helpers.TransMoney(totalprice, orderUnit.Total, userUnitConfig);

                string PurchaseTrackNum = string.Join(',',
                    purchaseLst.Where(x => x.OrderId == value.ID).Select(s => s.PurchaseTrackingNumber));
                purchase.TotalPrice = TotalPrice;
                purchase.PurchaseTrackNum = PurchaseTrackNum;
            }

            if (isWaybill)
            {
                var wids = await waybillService.GetWaybillIDsByOrder(value.ID, value.MergeId);
                if (wids is not null && wids.Count > 0)
                {
                    waybillLst = await waybillService.GetWaybill(wids);
                    var waybills = waybillLst.Where(a => a.State != Enums.Logistics.States.Cancle).ToList();
                    if (waybills.IsNotNull() && waybills.Count > 0)
                    {
                        string carriageNum = Helpers.TransMoney(value.ShippingMoney, orderUnit.Deliver,
                            userUnitConfig);
                        string OrderNum =
                            string.Join(',',
                                waybills.Select(s =>
                                    s.WaybillOrder)); //  waybillLst.Where( implode(',', $orderWaybill[$value['id']]['OrderNum']);
                        waybill.ShippingFee = carriageNum;
                        waybill.OrderNum = OrderNum;
                    }
                }
            }

            var productLst = new List<OrderData.Product>();
            if (value.GoodsScope().Count() > 0)
            {
                foreach (var s in value.GoodsScope())
                {
                    productLst.Add(new OrderData.Product()
                    {
                        Name = s.Name,
                        Num = s.QuantityOrdered,
                        Sku = s.Sku,
                        Price = Helpers.TransMoney(s.UnitPrice, orderUnit.ProductPrice,
                            userUnitConfig), //  s.UnitPrice.ToString(userUnitConfig),
                        ShipFee = Helpers.TransMoney(s.ShippingPrice, orderUnit.Deliver,
                            userUnitConfig), // s.ShippingPrice.ToString(userUnitConfig),
                        Total = Helpers.TransMoney(s.TotalPrice, orderUnit.ProductPrice,
                            userUnitConfig), // s.TotalPrice.ToString(userUnitConfig),
                        Unit = Helpers.TransMoney(s.UnitPrice, orderUnit.ProductPrice,
                            userUnitConfig), // s.UnitPrice.ToString(userUnitConfig),
                        ImageUrl = s.ImageURL,
                        Asin = s.ID,
                    });
                }
            }

            var receiver = new OrderData.ReceiverItem();
            if (value.Receiver.IsNotNull())
            {
                receiver.receiver_name = value.Receiver.Name;
                receiver.receiver_phone = value.Receiver.Phone;
                receiver.receiver_email = value.Receiver.Email;
                receiver.receiver_zip = value.Receiver.Zip;
                receiver.receiver_nation = value.Receiver.Nation;
                receiver.receiver_province = value.Receiver.Province;
                receiver.receiver_city = value.Receiver.City;
                receiver.receiver_district = value.Receiver.District;
                receiver.receiver_address = value.Receiver.Address1;
                receiver.receiver_address2 = value.Receiver.Address2;
                receiver.receiver_address3 = value.Receiver.Address3;
            }


            data.Add(new OrderData()
            {
                OrderNum = value.OrderNo,
                OrderFinance =
                    Helpers.TransMoney(value.OrderTotal, orderUnit.Total,
                        userUnitConfig), // value.OrderTotal.ToString(userUnitConfig),
                HandlingFee = Helpers.TransMoney(value.Fee, orderUnit.Fee, userUnitConfig),
                //value.Fee.ToString(userUnitConfig),
                CostLoss = Helpers.TransMoney(value.Loss, orderUnit.Total, userUnitConfig),
                //value.Loss.ToString(userUnitConfig),
                OrderUnit = orderUnit.Total ? value.OrderTotal.Raw.CurrencyCode : userUnitConfig,
                // Session.GetUnitConfig() ?? value.Coin,
                StoreName = store.Name,
                //StoreCountry = store.NationName,
                SellerID = value.ShopID,
                OrderTime = value.CreateTime.ToString("yyyy-MM-dd HH:mm:ss"),
                OrderProducts = productLst,
                Receiver = receiver,
                Purchase = purchase,
                WayBill = waybill,
            });
        }

        return data;
    }
}