﻿using System.Text;
using System.Text.RegularExpressions;
using ERP.Models.Stores;

namespace ERP.Services.Export;

public class SkuGenerator
{
    private readonly SyncProductModel.SkuTypes _skuType;
    private readonly int _sourceId;
    private readonly SyncProductModel.SkuSuffixs _skuSuffix;
    private readonly bool _hasVariant;
    private string? _mainSku;

    public SkuGenerator(SyncProductModel.SkuTypes skuType, int sourceId, SyncProductModel.SkuSuffixs skuSuffix,
        bool hasVariant)
    {
        _skuType = skuType;
        _sourceId = sourceId;
        _skuSuffix = skuSuffix;
        _hasVariant = hasVariant;
    }

    /// <summary>
    /// 生成主产品sku
    /// </summary>
    /// <param name="sku"></param>
    /// <returns></returns>
    public string GenerateMain(string sku)
    {
        return _mainSku ??= MakeSku(_skuType, _sourceId, sku);
    }

    /// <summary>
    /// 生成变体sku
    /// </summary>
    /// <param name="sku"></param>
    /// <param name="index"></param>
    /// <param name="attributeName"></param>
    /// <returns></returns>
    public string GenerateVariant(string? sku, int index, string attributeName)
    {
        // 变体单独设置sku | 追加属性 | 结果
        // 否            | 否      | 主产品sku + `-` + index
        // 否            | 是      | 主产品sku + `-` + 属性值
        // 是            | 否      | 变体sku
        // 是            | 是      | 变体sku + `-` + 属性值
        if (_mainSku is null)
            throw new Exception("需要先生成主产品sku");
        if (_hasVariant)
            return _mainSku;

        var useMainSku = string.IsNullOrEmpty(sku);
        var sb = new StringBuilder(useMainSku
            ? $"{_mainSku}"
            : MakeSku(_skuType, _sourceId, sku!));


        if (_skuSuffix == SyncProductModel.SkuSuffixs.TRUE && !string.IsNullOrWhiteSpace(attributeName))
        {
            var matches = Regex.Matches(attributeName, "[a-zA-Z0-9\\-]");
            sb.Append($"-{string.Join("", matches.Select(m => m.Value))}");
        }
        else if (useMainSku)
            sb.Append($"-{index}");

        return sb.ToString();
    }

    private static string MakeSku(SyncProductModel.SkuTypes skuType, int sourceId, string sku)
    {
        return skuType == SyncProductModel.SkuTypes.TYPE_TRUE
            ? Helpers.EnCodeSku(sourceId, sku)
            : sku;
    }
}