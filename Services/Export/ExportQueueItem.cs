﻿using ERP.Export.Queue;
using ERP.Models.Export;

namespace ERP.Services.Export;

public record ExportQueueItem(ExportTask Task) : IQueueItem;