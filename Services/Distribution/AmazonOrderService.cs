using ERP.Data;
using ERP.Enums.Orders;
using ERP.Extensions;
using ERP.Models.DB.Identity;
using ERP.Models.View.Order;
using ERP.Services.DB.Orders;
using ERP.Services.DB.Statistics;
using ERP.Services.DB.Users;
using ERP.Services.Finance;
using ERP.ViewModels.Distribution;
using ERP.ViewModels.Order.AmazonOrder;
using Microsoft.EntityFrameworkCore;

namespace ERP.Services.Distribution;

using ERP.Distribution.Abstraction.Services;
using ERP.Models.DB.Logistics;
using ERP.Models.View.Distribution;
using Models.DB.Orders;
using Ryu.Linq;
using static ERP.Extensions.DbSetExtension;

public class AmazonOrderService : BaseService
{
    private readonly DBContext _dbContext;
    private readonly UserService _userService;
    private readonly Statistic _statisticService;
    private readonly DB.Orders.Order _orderService;
    private readonly DB.Logistics.WayBill wayBillService;
    private readonly PurchaseService _purchaseService;
    private readonly IDistributionWaybillService _DiswaybillService;

    public AmazonOrderService(DBContext dbContext, UserService userService, Statistic statisticService,
        Services.DB.Orders.Order orderService, DB.Logistics.WayBill _wayBillService, PurchaseService purchaseService, IDistributionWaybillService DiswaybillService)
    {
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        _userService = userService ?? throw new ArgumentNullException(nameof(userService));
        _statisticService = statisticService;
        _orderService = orderService;
        wayBillService = _wayBillService;
        _purchaseService= purchaseService;
        _DiswaybillService = DiswaybillService;
    }

    /// <summary>
    /// 上级获取上报订单分页列表
    /// </summary>
    /// <param name="order_no"></param>
    /// <param name="progress"></param>
    /// <param name="order_state"></param>
    /// <param name="company_id"></param>
    /// <param name="oids"></param>
    /// <param name="sort_by"></param>
    /// <param name="type">1分销上级订单列表 2分销上级订单汇总</param>
    /// <param name="createTimeStart"></param>
    /// <param name="createTimeEnd"></param>
    /// <param name="onlyDistributed"></param>
    /// <returns></returns>
    public async Task<PaginateStruct<OrderListDto, OrderSumDto>> GetListPage(AmazonOrderListQuery query,  User user, string unit, int? type = 1)
    {

        DateTime? createTimeStart = null;
        DateTime? createTimeEnd = null;
        // $type vue 1分销亚马逊订单 2下级亚马逊订单汇总
        if (query.Type == 2 && query.time is { Length: 2 })
        {
            createTimeStart = query.time[0];
            createTimeEnd = query.time[1];
        }
        
        var oids = Array.Empty<int>();
        var pids = Array.Empty<int>();
        var wids = Array.Empty<int>();
        if (!string.IsNullOrEmpty(query.PurchaseTrackingNumber))
        {           
            if (query.PurchaseTrackingNumber is { Length: > 0 })
            {
                var purchase = await _purchaseService.getPurchase("", query.PurchaseTrackingNumber, 2);
                if (purchase.Count <= 0)
                {
                    return new PaginateStruct<OrderListDto, OrderSumDto>();
                }
                pids = purchase.Pluck(p => p.OrderId).Cast<int>().ToArray();
            }
        }
        if (!string.IsNullOrEmpty(query.WaybillOrder))
        {           
            if (query.WaybillOrder is { Length: > 0 })
            {
                wids= wayBillService.GetWayIds(query.WaybillOrder).ToArray();

                //var waybill = await _DiswaybillService.GetWaybill(user.CompanyID, query.WaybillOrder);
                //if (waybill.IsNullOrEmpty())
                //{
                //    return new PaginateStruct<OrderListDto, OrderSumDto>();
                //}
                //wids = waybill.Pluck(w => w.OrderId).Cast<int>().ToArray();                
            }          
        }
      
        if(pids.Count()>0 || wids.Count()>0)
        {
            oids = (pids, wids) switch
            {
                ({ Length: > 0 }, { Length: > 0 }) => pids.Intersect(wids).ToArray(),
                ({ Length: 0 }, { Length: > 0 }) => wids,
                ({ Length: > 0 }, { Length: 0 }) => pids,
                _ => oids
            };

            if (oids.IsNullOrEmpty())
            {
                return new PaginateStruct<OrderListDto, OrderSumDto>();
            }
        }

        var progress = query.progress;
        var unitConfig = user.OrderUnitConfig;
        var defaultProgress = progress ?? ProgressMaskValue.All;
       
        var root = _dbContext.Order.AsNoTracking().Include(o => o.Receiver)
            .Include(o => o.Store)
            .Include(u => u.User).Where(x=>x.pullOrderState==PullOrderState.OutOfProcess)
            .Where(o => o.ShippingType == ShippingTypes.MFN &&
                     ((ulong)o.Bits & defaultProgress.Mask) == defaultProgress.Value &&
            (o.PurchaseCompanyId == user.CompanyID || o.WaybillCompanyId == user.CompanyID || o.OrderCompanyId == user.CompanyID))
            //.Where(x=>x.OrderCompanyId==0)  //不是下级上报的合并订单
            .WhenWhere(query.OnlyDistributed is true, db => db.WhereDistributedDistribution(_dbContext, user.Id))
            .WhenWhere(query.OrderState.HasValue, o => o.State == query.OrderState)
            .WhenWhere(query.CompanyId.HasValue, o => o.CompanyID == query.CompanyId)
            .WhenWhere(createTimeStart.HasValue, o => o.CreateTime >= createTimeStart)
            .WhenWhere(createTimeEnd.HasValue, o => o.CreateTime <= createTimeEnd)
            .WhenWhere(oids.Any(), o => oids.Contains(o.ID))
            .WhenWhere(!string.IsNullOrEmpty(query.OrderNo), o => o.OrderNo == query.OrderNo)
            .GetSortBy(query.SortBy);
        // TODO: group by sum 无法使用多个owned字段，目前这个方式会影响性能
        var sum = new OrderSumDto
        {
            OrderTotal = Helpers.TransMoney(await root.SumAsync(o => o.OrderTotal.Money), unit),           
            Fee = Helpers.TransMoney(await root.SumAsync(o => o.Fee.Money), unit),
            Refund = Helpers.TransMoney(await root.SumAsync(o => o.Refund.Money), unit),
            ShippingMoney  = Helpers.TransMoney(await root.SumAsync(o => o.ShippingMoney.Money), unit),
            PurchaseFee = Helpers.TransMoney(await root.SumAsync(o => o.PurchaseFee.Money), unit),
            Loss = Helpers.TransMoney(await root.SumAsync(o => o.Loss.Money), unit),
            //Profit = Helpers.TransMoney(await root.SumAsync(o => o.Profit.Money), unit),
            Unit=unit,
        };
        var orders = await root.Select(o => new OrderListDto
            (
            unitConfig, unit, o.ID, o.OrderNo, o.StoreId, o.Receiver.Nation, o.Url, o.ProductNum,
                o.OrderTotal,
                o.Coin, 
                o.Fee,   
                o.Loss, 
                o.Refund, 
                o.PurchaseFee,
                o.ShippingMoney, 
                o.Profit, (int)o.State, o.CompanyID,
                o.CreateTime, o.LatestShipTime,
                o.OrderTotal.Raw.Rate, o.PurchaseAudit, o.ShippingType, o.Store!.Name, o.Plateform,
                o.PurchaseCompanyId, o.WaybillCompanyId,o.OrderCompanyId,
                o.Purchases.Where(p => !string.IsNullOrEmpty(p.PurchaseTrackingNumber)).Select(p =>new {id = p.ID, purchaseTrackingNumber = p.PurchaseTrackingNumber!}).ToArray(),             
                o.CustomMark, o.Receiver.NationShort, o.Delivery)
                {
                    Username = o.User.UserName
                }                
            )
            .ToPaginateAsync();

        var result = new DbSetExtension.PaginateStruct<OrderListDto, OrderSumDto>()
        {
            CurrentPage = orders.CurrentPage,
            Total = orders.Total,
            Items = orders.Items,
            PageSize = orders.PageSize,
            Group = sum
        };
        return result;
    }




    //public async Task<Waybill[]> GetWaybillIDs( string express, int orderID,int? mergID)
    //{
    //    if (!string.IsNullOrEmpty(express))
    //    {
    //        var wids =await wayBillService.GetWaybillIDsByOrder(orderID, mergID);
    //        if (wids != null && wids.Count > 0)
    //        {
    //            var info =await wayBillService.GetWaybill(wids);
    //            if (info != null && info.Count > 0)
    //            {
    //                return info;
    //            }
    //        }            
    //    }
    //    return null;
    //}

    /// <summary>
    /// 下级分销用户获取当前公司上报订单分页列表
    /// </summary>
    /// <returns></returns>
    public async Task<DbSetExtension.PaginateStruct<OrderListDto>> GetJuniorListPage(OrderUnitConfig unitConfig,string unit,
        int companyId, string? orderNo, ProgressMaskValue progress, States? orderState, int[] oids, SortBy? sortBy)
    {
        //        $field = [ 'truename', 'store_id', 'fake_product_time', 'true_product_time', 'process_waybill', 'process_purchase',];
        //            ->PurchaseOrWaybillCompanyId($this->getSession()->getReportId())
        var defaultProgress = progress ?? ProgressMaskValue.All;
        return await _dbContext.Order.AsNoTracking().Include(o => o.Receiver)
            .Include(o => o.Store)
            .Where(o => o.ShippingType == ShippingTypes.MFN &&
                        ((ulong)o.Bits & defaultProgress.Mask) == defaultProgress.Value && o.CompanyID == companyId)
            .WhenWhere(orderState.HasValue, o => o.State == orderState)
            .WhenWhere(oids.Any(), o => oids.Contains(o.ID))
            .WhenWhere(!string.IsNullOrEmpty(orderNo), o => o.OrderNo == orderNo)
            .GetSortBy(sortBy)
            .Select(o => new OrderListDto(unitConfig,unit,o.ID, o.OrderNo, o.StoreId, o.Receiver.Nation, o.Url, o.ProductNum,
                o.OrderTotal,
                o.Coin, o.Fee, o.Loss, o.Refund, o.PurchaseFee, o.ShippingMoney, o.Profit, (int)o.State, o.CompanyID,
                o.CreateTime, o.LatestShipTime,
                o.OrderTotal.Raw.Rate, o.PurchaseAudit, o.ShippingType, o.Store!.Name, o.Plateform,
                o.PurchaseCompanyId, o.WaybillCompanyId,o.OrderCompanyId,
                o.Purchases.Where(p => !string.IsNullOrEmpty(p.PurchaseTrackingNumber)).Select(p => new {id = p.ID, purchaseTrackingNumber = p.PurchaseTrackingNumber!}).ToArray(),
                //o.Waybills.Where(w => !string.IsNullOrEmpty(w.Express)).Select(w => new {id = w.ID,express = w.Express,stateDes = w.StateDes}).ToArray(),
                o.CustomMark, o.Receiver.NationShort, o.Delivery))
            .ToPaginateAsync();
    }

    /// <summary>
    /// 上级获取下级待采购、待发货订单
    /// </summary>
    /// <param name="currentCompanyId">当前（上级）公司id</param>
    /// <param name="orderNo"></param>
    /// <param name="progress"></param>
    /// <param name="orderState"></param>
    /// <param name="companyId"></param>
    /// <param name="type"></param>
    /// <param name="sortBy"></param>
    /// <returns></returns>
    public Task<DbSetExtension.PaginateStruct<OrderWaitListDto>> GetWaitListPage(int currentCompanyId, string? orderNo,
         Enums.Orders.States? orderState, int? companyId, int type = 1,
        object? sortBy = null)
    {
        //        $field = ['pay_money', 'progress'];
        int? purchaseCompanyId = type == 1 ? currentCompanyId : null;
        int? waybillCompanyId = type != 1 ? currentCompanyId : null;
       
        return _dbContext.Order.Include(o => o.Receiver)
            .Where(a => a.ShippingType == ShippingTypes.MFN && a.pullOrderState==PullOrderState.OutOfProcess)
            .WhenWhere(orderState.HasValue, a => a.State == orderState)
            .WhenWhere(companyId.HasValue, a => a.CompanyID == companyId)
            .WhenWhere(purchaseCompanyId.HasValue, a => a.PurchaseCompanyId == purchaseCompanyId)
            .WhenWhere(waybillCompanyId.HasValue, a => a.WaybillCompanyId == waybillCompanyId)
            .WhenWhere(!string.IsNullOrEmpty(orderNo), a => a.OrderNo == orderNo)
            .OrderByDescending(a => a.ID)
            .Select(o => new OrderWaitListDto(o.ID, o.OrderNo, o.Receiver.Nation, o.Receiver.NationShort, o.Url,
                o.ProductNum, o.Coin, (int)o.State, o.CompanyID, o.CreateTime, o.LatestShipTime, o.IsBatchShip,
                o.OrderTotal.Raw.Rate))
            .ToPaginateAsync();
    }

    /// <summary>
    /// 下级获取待采购、待发货订单
    /// </summary>
    /// <param name="storeIds"></param>
    /// <param name="type">1 待采购 2待发货</param>
    /// <param name="currentCompanyId"></param>
    /// <param name="reportCompanyId"></param>
    /// <param name="orderNo"></param>
    /// <param name="orderState"></param>
    /// <param name="progress"></param>
    /// <param name="storeId"></param>
    /// <param name="sortBy"></param>
    /// <returns></returns>
    public Task<DbSetExtension.PaginateStruct<Order>> GetJuniorWaitListPage(int currentCompanyId, int reportCompanyId,
        string? orderNo,
        Enums.Orders.States? orderState, ProgressMaskValue? progress, int? storeId, int[] storeIds, int type = 1,
        object? sortBy = null)
    {
        //        $field = ['id', 'order_no', 'receiver_nation', 'url', 'product_num', 'pay_money', 'coin', 'order_state', 'progress', 'company_id', 'store_id', 'truename', 'create_time', 'latest_ship_time', 'process_purchase', 'process_waybill', 'purchase_company_id', 'waybill_company_id', 'rate'];
        int? purchaseCompanyId = type == 1 ? reportCompanyId : null;
        int? waybillCompanyId = type != 1 ? reportCompanyId : null;
        var defaultProgress = progress ?? ProgressMaskValue.All;
        return _dbContext.Order.Where(a =>
                a.CompanyID == currentCompanyId && a.ShippingType == Enums.Orders.ShippingTypes.MFN
                                                && ((ulong)a.Bits & defaultProgress.Mask) == defaultProgress.Value)
            .WhenWhere(orderState.HasValue, a => a.State == orderState)
            .WhenWhere(storeId.HasValue, a => a.StoreId == storeId)
            .WhenWhere(storeIds is { Length: > 0 }, a => storeIds.Contains(a.StoreId))
            .WhenWhere(purchaseCompanyId.HasValue, a => a.PurchaseCompanyId == purchaseCompanyId)
            .WhenWhere(waybillCompanyId.HasValue, a => a.WaybillCompanyId == waybillCompanyId)
            .WhenWhere(!string.IsNullOrEmpty(orderNo), a => a.OrderNo == orderNo)
            .OrderByDescending(a => a.ID)
            .ToPaginateAsync();
    }

    /// <summary>
    /// Model
    /// </summary>
    /// <param name="id"></param>
    /// <param name="reportCompanyId"></param>
    /// <param name="type">1 待采购 2待发货</param>
    /// <param name="currentCompanyId"></param>
    /// <returns></returns>
    public async Task<int> ToBack(int id, int currentCompanyId, int reportCompanyId, int type = 1)
    {
        int? purchaseCompanyId = null;
        int? waybillCompanyId = null;
        Dictionary<string, object> updateData;
        var isReportOrderCount = false;
        var order = await _dbContext.Order.Where(a => a.ID == id && a.CompanyID == currentCompanyId)
            .FirstOrDefaultAsync();
        
        if (type == 1)
        {
            purchaseCompanyId = reportCompanyId;
            updateData = new Dictionary<string, object>() { { nameof(Order.PurchaseCompanyId), 0 } };
            await _statisticService.DistributionCount(Statistic.NumColumnEnum.DistributionPurchaseNum, reportCompanyId,
                -1);
            if (order is { WaybillCompanyId: <= 0 })
            {
                isReportOrderCount = true;
            }
        }
        else
        {
            waybillCompanyId = reportCompanyId;
            updateData = new Dictionary<string, object>() { { nameof(Order.WaybillCompanyId), 0 } };
            await _statisticService.DistributionCount(Statistic.NumColumnEnum.DistributionWaybillNum, reportCompanyId,
                -1);
            if (order is { PurchaseCompanyId: <= 0 })
            {
                isReportOrderCount = true;
            }
        }
        if(isReportOrderCount)
            await _statisticService.DistributionCount(Statistic.NumColumnEnum.DistributionOrderNum, reportCompanyId, -1);
        
        return await _dbContext.Order.Where(a => a.ID == id && a.CompanyID == currentCompanyId)
            .WhenWhere(purchaseCompanyId.HasValue, a => a.PurchaseCompanyId == purchaseCompanyId)
            .WhenWhere(waybillCompanyId.HasValue, a => a.WaybillCompanyId == waybillCompanyId)
            .UpdateFromQueryAsync(updateData);
    }

    /// <summary>
    /// 下级上报内部待采购、待发货订单
    /// </summary>
    /// <param name="id"></param>
    /// <param name="currentCompanyId"></param>
    /// <param name="reportCompanyId"></param>
    /// <param name="type">1 待采购 2待发货</param>
    /// <returns></returns>
    public async Task<int> ToReport(int id, int currentCompanyId, int reportCompanyId, int type = 1)
    {
        Dictionary<string, object> updateData;
        var isReportOrderCount = false;
        var order = await _dbContext.Order.Where(a => a.ID == id && a.CompanyID == currentCompanyId)
            .FirstOrDefaultAsync();
        if (type == 1)
        {
            updateData = new Dictionary<string, object>() { { nameof(Order.PurchaseCompanyId), reportCompanyId } };
            await _statisticService.DistributionCount(Statistic.NumColumnEnum.DistributionPurchaseNum, reportCompanyId);
            if (order is { WaybillCompanyId: <= 0 })
            {
                isReportOrderCount = true;
            }
        }
        else
        {
            updateData = new Dictionary<string, object>() { { nameof(Order.WaybillCompanyId), reportCompanyId } };
            await _statisticService.DistributionCount(Statistic.NumColumnEnum.DistributionWaybillNum, reportCompanyId);
            if (order is { PurchaseCompanyId: <= 0 })
            {
                isReportOrderCount = true;
            }
        }
        

        if (isReportOrderCount)
            await _statisticService.DistributionCount(Statistic.NumColumnEnum.DistributionOrderNum, reportCompanyId);
        
        return await _dbContext.Order.Where(a => a.ID == id && a.CompanyID == currentCompanyId)
            .UpdateFromQueryAsync(updateData);
    }

    /// <summary>
    /// 上报合并订单
    /// </summary>
    /// <param name="mid"></param>
    /// <param name="currentCompanyId"></param>
    /// <param name="reportCompanyId"></param>
    /// <param name="num"></param>
    /// <returns></returns>
    public async Task<int> ToReportMergeOrder(int mid, int currentCompanyId, int reportCompanyId)
    {
        Dictionary<string, object> updateData;
        updateData = new Dictionary<string, object>() { { nameof(Order.OrderCompanyId), reportCompanyId } };

        Dictionary<string, object> mupdateData;
        mupdateData = new Dictionary<string, object>() { { nameof(Merge.OrderCompanyID), reportCompanyId } };

        int num = GetCountByMergeID(mid);
        if(num>0)
        {
            await _statisticService.DistributionCount(Statistic.NumColumnEnum.DistributionOrderNum, reportCompanyId, num);
        }
        await _dbContext.OrderMerge.Where(a => a.ID == mid)
            .UpdateFromQueryAsync(mupdateData);

        return await _dbContext.Order.Where(a => a.MergeId == mid)
            .UpdateFromQueryAsync(updateData);
    }

    /// <summary>
    /// 撤回上报合并订单
    /// </summary>
    /// <param name="mid"></param>
    /// <param name="currentCompanyId"></param>
    /// <param name="reportCompanyId"></param>
    /// <param name="num"></param>
    /// <returns></returns>
    public async Task<int> ToBackMerge(int mid, int currentCompanyId, int reportCompanyId)
    {
        int orderCompanyId = reportCompanyId;
        Dictionary<string, object> updateData;
        updateData = new Dictionary<string, object>() { { nameof(Order.OrderCompanyId), 0 } };

        Dictionary<string, object> mupdateData;
        mupdateData = new Dictionary<string, object>() { { nameof(Merge.OrderCompanyID), 0 } };

        int num = GetCountByMergeID(mid);
        if(num>0)
        {
            await _statisticService.DistributionCount(Statistic.NumColumnEnum.DistributionOrderNum, reportCompanyId, -num);
        }
        await _dbContext.OrderMerge.Where(a => a.ID == mid)
            .UpdateFromQueryAsync(mupdateData);

        return await _dbContext.Order.Where(a => a.MergeId == mid && a.CompanyID == currentCompanyId)
            .Where(a => a.OrderCompanyId == orderCompanyId)
            .UpdateFromQueryAsync(updateData);
    }


    //        /**
    //         * 订单运费汇总
    //         * User: CCY
    //         * Date: 2020/6/2 16:28
    //         * @param     $id
    //         * @param int $money
    //         * @return bool|int
    //         */
    public ReturnStruct updateWaybill( /*$id, $money = 0*/)
    {
        //        $updateData = [
        //            'shipping_money' => DB::raw($this->amazonOrderModel->transformKey('shipping_money'). '+'. $money),
        //            'profit'         => DB::raw($this->amazonOrderModel->transformKey('profit') . '-' . $money),
        //        ];
        //        return AmazonOrderModel::id($id)
        //            ->update($updateData);
        return ReturnArr();
    }

    /// <summary>
    /// 获取订单详细信息
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public Task<Order?> GetInfoById(int id) => _orderService.GetOrderById(id);

    /// <summary>
    /// 订单操作日志
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public Task<List<OrderActionListDto>> GetOrderAction(int id) => _orderService.GetOrderAction(new[] { id });

    /// <summary>
    /// 订单备注信息
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public Task<List<OrderRemarkListDto>> GetOrderRemarks(int id) => _orderService.GetOrderRemarks(new[] { id });

    public Task<object> GetPurchaseWaybill(List<int> ids) => _orderService.GetPurchaseWaybill(ids);

    //    /**
    //     * 上级新增采购更新订单信息
    //     * User: CCY
    //     * Date: 2020/6/17 13:37
    //     * @param $orderId
    //     * @param $purchase_state
    //     * @param $money
    //     * @param $num
    //     */
    public ReturnStruct updateOrderByPurchaseAdd(
 /*$order, $purchase_state, $money, $num, $tracking_number = '', $updateData = []*/)
    {
        //        $biter = new Progress($order['progress_original']);
        //        $biter->setPurchase(true);
        //        $biter->setProgress(AmazonOrderModel::PROGRESS_PROCESSING);
        //        $updateOrder['progress'] = $biter->getFlags();
        //        $tracking_number && $updateOrder['purchase_tracking_number'] = $tracking_number;

        //        if ($purchase_state == PurchaseModel::STATE_PURCHASED) {
        //            //采购完成
        //            $updateOrder = [
        //                'purchase_fee' => DB::raw($this->amazonOrderModel->transformKey('purchase_fee') . '+' . $money),
        //                'profit'       => Db::raw($this->amazonOrderModel->transformKey('profit') . '-' . $money),
        //            ];
        //        }
        //        $updateOrder['purchase_num'] = Db::raw($this->amazonOrderModel->transformKey('purchase_num') . '+' . $num);
        //        $order['process_purchase'] == AmazonOrderModel::PROCESSPURCHASE_NONE && $updateOrder['process_purchase'] = AmazonOrderModel::PROCESSPURCHASE_SUPERIOR;
        //        $updateOrder = array_merge($updateOrder, $updateData);
        //        AmazonOrderModel::id($order['id'])->update($updateOrder);
        return ReturnArr();
    }

    //    /**
    //     * 分销上级采购完成更新订单信息
    //     * User: CCY
    //     * Date: 2020/6/17 16:24
    //     * @param $orderId
    //     * @param int $money
    //     * @param string $tracking_number
    //     * @return bool
    //     */
    public ReturnStruct updateOrderByPurchaseUpdate( /*$orderId, $money = 0, $tracking_number = ''*/)
    {
        //        $updateOrder = [];
        //        $money != 0 && $updateOrder = [
        //            'purchase_fee' => DB::raw($this->amazonOrderModel->transformKey('purchase_fee') . '+' . $money),
        //            'profit'       => Db::raw($this->amazonOrderModel->transformKey('profit') . '-' . $money),
        //        ];
        //        $tracking_number && $updateOrder['purchase_tracking_number'] = $tracking_number;
        //        if (!checkArr($updateOrder)) {
        //            return false;
        //        }
        //        AmazonOrderModel::query()->id($orderId)->update($updateOrder);
        //        return true;
        return ReturnArr();
    }

    public int GetCountByMergeID(int mid)
    {
        return  _dbContext.Order.Where(x=>x.WaybillCompanyId==0 && x.PurchaseCompanyId==0).Count(a => a.MergeId == mid);
    }

    /// <summary>
    /// 获取指定订单信息
    /// </summary>
    /// <param name="orderNo"></param>
    /// <returns></returns>
    public async Task<Order?> GetInfoByOrderNo(string orderNo)
    {
        var user = await _userService.GetCurrentUser();

        return user is not null
            ? await _dbContext.Order.Where(a => a.CompanyID == user.CompanyID && a.OrderNo == orderNo)
                .FirstOrDefaultAsync()
            : null;
    }

    //    /**
    //     * 购物车添加无订单信息
    //     * User: CCY
    //     * Date: 2020/6/20 13:23
    //     * @param $order_no
    //     * @param $info
    //     * @param $nation
    //     * @param $good
    //     * @param $num
    //     * @param $total
    //     * @return int
    //     */
    public ReturnStruct OrderManualAdd( /*$order_no, $info, $nation, $good, $num, $total*/)
    {
        //        $session = $this->getSession();
        //        $insertData = [
        //            "user_id"               => $session->getUserId(),
        //            'group_id'              => $session->getGroupId(),
        //            "company_id"            => $session->getCompanyId(),
        //            'oem_id'                => $session->getOemId(),
        //            'url'                   => $good[0]['url'] ?? '',
        //            'goods'                 => json_encode($good),
        //            "entry_mode"            => AmazonOrderModel::ENTRY_MODE_CUSTOM,
        //            "order_no"              => $order_no,
        //            "user_name"             => $session->getUsername(),
        //            'pay_time'              => $this->datetime,
        //            "receiver_nation_id"    => $nation['id'],
        //            "receiver_nation"       => $nation['name'],
        //            "receiver_nation_short" => $nation['short'],
        //            "receiver_address"      => $info['receiver_address'],
        //            'product_money'         => $total,
        //            'profit'                => 0,
        //            'order_money'           => $total,
        //            "shipping_money"        => FromConversion($info['shipping_money'], $session->getUnitConfig()),
        //            'pay_money'             => $total,
        //            'order_state'           => AmazonOrderModel::ORDER_STATE_PENDING,
        //            "create_time"           => $this->datetime,
        //            "coin"                  => $session->getUnitConfig(),
        //            'product_num'           => $num,
        //            "truename"              => $session->getTruename(),
        //            'process_purchase'      => AmazonOrderModel::PROCESSPURCHASE_INSIDE,
        //            'purchase_fee'          => $total,
        //            "progress"              => $info['progress'],
        //            "operate_user_id"       => $session->getUserId(),
        //            'purchase_num'          => $num,
        //            'purchase_company_id'   => $session->report_id,
        //            'waybill_company_id'    => $info['send_mode'] == AmazonOrderModel::SEND_MODE_SUBSTITUTE ? $session->report_id : 0,
        //            'send_mode'             => $info['send_mode'],
        //            'purchase_audit'        => AmazonOrderModel::PURCHASE_AUDIT_TRUE,
        //            'distribution_address'  => json_encode($info['distribution_address']),
        //            'created_at'            => $this->datetime
        //        ];
        //        return AmazonOrderModel::query()->insertGetId($insertData);
        return ReturnArr();
    }

    /// <summary>
    /// 查看下级公司订单信息
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="oemId"></param>
    /// <returns></returns>
    public async Task<object> GetJuniorOrders(int companyId, int oemId)
    {
        return await _dbContext.Order.Where(x => x.CompanyID == companyId)
            .Where(x => x.OEMID == oemId)
            .OrderByDescending(x => x.ID)
            .Select(x => new
            {
                x.ID, x.OrderNo, ReceiverNation = x.Receiver.Nation, x.Url, x.ProductNum, x.CreatedAt, x.LatestShipTime
            })
            .ToPaginateAsync();
    }

    //    /**
    //     * 获取下级订单
    //     * User: CCY
    //     * Date: 2020/7/10 10:24
    //     * @param $ids
    //     * @return AmazonOrderModel[]|Builder[]|Collection
    //     */
    public ReturnStruct GetJuniorOrderByIds( /*$ids*/)
    {
        //        $field = ['id', 'order_no', 'receiver_nation', 'url', 'product_num', 'pay_money', 'coin', 'order_state', 'progress', 'company_id', 'create_time', 'latest_ship_time', 'rate'];
        //        return AmazonOrderModel::query()
        //            ->shippingType(AmazonOrderModel::SHIPPINGTYPE_MFN)
        ////            ->PurchaseCompanyId($this->getSession()->getCompanyId())
        //            ->IdIn($ids)
        //            ->sort()
        //            ->select($field)
        //            ->get();
        return ReturnArr();
    }
}