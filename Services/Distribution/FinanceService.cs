﻿using ERP.Data;
using ERP.Distribution.Abstraction;
using ERP.Distribution.Abstraction.Services;
using ERP.Enums.Statistics;
using ERP.Extensions;
using ERP.Models.DB.Identity;
using ERP.Models.DB.Statistics;
using ERP.Models.DB.Users;
using ERP.Services.Finance;
using ERP.ViewModels.Finance;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Ryu.Extensions;
using BillLog = ERP.Models.Finance.BillLog;
using BillLogDto = ERP.Distribution.Abstraction.BillLogDto;

namespace ERP.Services.Distribution
{
    public class FinanceService : BaseService, IDistributionFinanceService
    {
        private BillLogService _billLogService;
        private readonly DBContext _dbContext;

        private IQueryable<Statistic> StatisticsNt => _dbContext.Set<Statistic>().AsNoTracking();
        // private IQueryable<BillLog> BillLogsNt => _dbContext.BillLog.AsNoTracking();

        public FinanceService(BillLogService billLogService, DBContext dbContext)
        {
            _billLogService = billLogService;
            _dbContext = dbContext;
        }

        
        public async Task<DbSetExtension.PaginateStruct<BillLogDto>> GetBillPage(int currentCompanyId, Models.Finance.BillLog.Types? type, BillLog.Modules? module,
            BillLog.Audits? audit, int? companyId)
        {
            var data = await _dbContext.BillLog.Where(b => b.Belong == BillLog.Belongs.RIGHT && b.OperateCompanyId == currentCompanyId &&
                                                     b.ReportCompanyId == currentCompanyId)
                .WhenWhere(type.HasValue, b => b.Type == type)
                .WhenWhere(audit.HasValue, b => b.Audit == audit)
                .WhenWhere(module.HasValue, b => b.Module == module)
                .WhenWhere(companyId.HasValue, b => b.CompanyID == companyId)
                .OrderBy(b => b.Audit)
                .ThenByDescending(b => b.ID)
                .ToPaginateAsync();
            var list = data.Transform(b => new BillLogDto(b.ID, b.Money, b.Type, b.Datetime, b.Reason, b.Remark, b.Audit,
                b.CompanyID, b.Module, b.CreatedAt, null, null));
            return list;
        }

        /// <summary>
        /// 下级公司账单
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="type"></param>
        /// <param name="audit"></param>
        /// <param name="module"></param>
        /// <returns></returns>
        public Task<DbSetExtension.PaginateStruct<BillLogDto>> GetCurrentBillPage(int companyId,
            BillLog.Types? type, BillLog.Audits? audit,
            BillLog.Modules? module)
        {
            return _dbContext.BillLog.Where(w => w.Belong == BillLog.Belongs.RIGHT && w.CompanyID == companyId)
                .WhenWhere(type.HasValue, b => b.Type == type)
                .WhenWhere(audit.HasValue, b => b.Audit == audit)
                .WhenWhere(module.HasValue, b => b.Module == module)
                .OrderByDescending(b => b.ID)
                .Select(b => new BillLogDto(b.ID, b.Money, b.Type, b.Datetime, b.Reason, b.Remark,b.Audit, b.CompanyID, b.Module, b.CreatedAt,null,null))
                .ToPaginateAsync();
        }

        public async Task<DbSetExtension.PaginateStruct<BillLogDto>> GetCurrentBillPageMoreSearch(int companyId, BillLog.Types? type, BillLog.Audits? audit, BillLog.Modules? module, string? unit, List<DateTime>? date,
            string? orderNo, string? purchaseNo, string? waybillNo, int? storeId)
        {
            
            var data = await _dbContext.BillLog
                .Where(w => w.Belong == BillLog.Belongs.RIGHT && w.CompanyID == companyId)
                .WhenWhere(storeId.HasValue, m => m.StoreId == storeId.Value)
                .WhenWhere(type.HasValue, b => b.Type == type)
                .WhenWhere(audit.HasValue, b => b.Audit == audit)
                .WhenWhere(module.HasValue, b => b.Module == module)
                .WhenWhere(date?.Count >= 2, m => m.CreatedAt >= date![0] && m.CreatedAt <= date![1].LastDayTick())
                .WhenWhere(!string.IsNullOrEmpty(orderNo), m => m.Remark != null  && m.Remark.Contains(orderNo))
                .WhenWhere(!string.IsNullOrEmpty(purchaseNo), m => m.Remark != null  && m.Remark.Contains(purchaseNo))
                .WhenWhere(!string.IsNullOrEmpty(waybillNo), m => m.Remark != null  && m.Remark.Contains(waybillNo))
                .OrderByDescending(b => b.ID)
                .ThenBy(b => b.Balance)
                .ToPaginateAsync();
            return data.Transform(b => new BillLogDto(b.ID, b.Money, b.Type, b.Datetime, b.Reason, b.Remark, b.Audit,
                b.CompanyID, b.Module, b.CreatedAt, b.Balance, unit));
        }

        public async Task<DbSetExtension.PaginateStruct<BillLogDto>> GetBillPage(int currentCompanyId, Models.Finance.BillLog.Types? type, BillLog.Modules? module,
            BillLog.Audits? audit, int? companyId,string? unit,List<DateTime>? date,
            string? orderNo, string? purchaseNo, string? waybillNo)
        {
            //&& b.OperateCompanyId == currentCompanyId
            var data = await _dbContext.BillLog.Where(b => b.Belong == BillLog.Belongs.RIGHT  &&
                                             b.ReportCompanyId == currentCompanyId)
                .WhenWhere(type.HasValue, b => b.Type == type)
                .WhenWhere(audit.HasValue, b => b.Audit == audit)
                .WhenWhere(module.HasValue, b => b.Module == module)
                .WhenWhere(companyId.HasValue, b => b.CompanyID == companyId)
                .WhenWhere(date?.Count >= 2, m => m.CreatedAt >= date![0] && m.CreatedAt <= date![1].LastDayTick())
                .WhenWhere(!string.IsNullOrEmpty(orderNo), m => m.Remark != null  && m.Remark.Contains(orderNo))
                .WhenWhere(!string.IsNullOrEmpty(purchaseNo), m => m.Remark != null  && m.Remark.Contains(purchaseNo))
                .WhenWhere(!string.IsNullOrEmpty(waybillNo), m => m.Remark != null  && m.Remark.Contains(waybillNo))
                .OrderByDescending(b => b.ID)
                .ThenBy(b => b.Balance)
                .ThenByDescending(b => b.Audit)
                .ToPaginateAsync();
            return data.Transform(b => new BillLogDto(b.ID, b.Money, b.Type, b.Datetime, b.Reason, b.Remark, b.Audit,
                b.CompanyID, b.Module, b.CreatedAt, b.Balance, unit));
        }

        /// <summary>
        /// 获取账单详细信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ValueTask<BillLog?> GetBillLogInfo(int id)
        {
            return _dbContext.BillLog.FindAsync(id);
        }

        /// <summary>
        /// 更新账单信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="audit"></param>
        /// <param name="operateUser"></param>
        /// <param name="balance"></param>
        /// <returns></returns>
        public Task<int> UpdateBillLog(int id, BillLog.Audits audit, User operateUser,Money? balance)
        {
            return _dbContext.BillLog.Where(b => b.ID == id).UpdateFromQueryAsync(b => new BillLog
            {
                Audit = audit,
                AuditUserId = operateUser.Id,
                AuditTruename = operateUser.UserName,
                Balance = balance,
            });
        }

        public Task<int> UpdateBillLogPayNotify(int id, BillLog.Audits audit)
        {
            return _dbContext.BillLog.Where(b => b.ID == id).UpdateFromQueryAsync(b => new BillLog
            {
                Audit = audit,
            });
        }
        
        public async Task AddWalletBill(Money money, BillLog.Types type, DateTime datetime, string reason, string remark, BillLog.Audits audit,
            string[] imgAll, Company juniorCompany,Company superiorCompany, User user,User? auditUser)
        {
            var billLog = new BillLog(money, type, datetime, remark, audit, user.Id, user.TrueName, auditUser?.Id, auditUser?.TrueName, 
                juniorCompany.ID, user.GroupID, juniorCompany.OEMID, user.CompanyID,
                superiorCompany.ID, superiorCompany.Name, BillLog.Modules.INTERCHANGEABLE, 0, null)
            {
                Belong = BillLog.Belongs.RIGHT,
                Reason = reason,
                ReportCompanyId = juniorCompany.ReportId,
                ImgAll = JsonConvert.SerializeObject(imgAll),
               
            };
            if (audit == BillLog.Audits.Audited)
            {
                billLog.Balance = juniorCompany.PublicWallet.Money;
            }
            _dbContext.BillLog.Add(billLog);
            await _dbContext.SaveChangesAsync();
        }

        public Task<object?> GetBillPage()
        {
            throw new NotImplementedException();
        }

        //    /**
        //     * 分销财务下级钱包在线充值账单
        //     * Date: 2021/9/24 14:50
        //     * @param $money
        //     * @param $companyCurrent
        //     * @return int
        //     */
        public ReturnStruct addWalletBillOnlinePay( /*$money,$company,$companyCurrent,$module_id*/)
        {
            //    $this->billLogModel->money = $money;
            //    $this->billLogModel->belong = $this->billLogModel::BELONG_RIGHT;
            //    $this->billLogModel->type = $this->billLogModel::TYPE_RECHARGE;
            //    $this->billLogModel->datetime = date('Y-m-d');
            //    $this->billLogModel->audit = $this->billLogModel::AUDIT_DIDNOTPASS;
            //    $this->billLogModel->user_id = $this->getSession()->getUserId();
            //    $this->billLogModel->truename = $this->getSession()->getTruename();
            //    $this->billLogModel->group_id = $this->getSession()->getGroupId();
            //    $this->billLogModel->oem_id = $this->getSession()->getOemId();
            //    $this->billLogModel->company_id = $company['id'];
            //    $this->billLogModel->report_company_id = $company['report_id'];
            //    $this->billLogModel->creat_company_id = $companyCurrent['id'];
            //    $this->billLogModel->operate_company_id = $companyCurrent['id'];
            //    $this->billLogModel->operate_company_name = $companyCurrent['company_name'];
            //    $this->billLogModel->module = $this->billLogModel::MODULE_ONLINEPAY;
            //    $this->billLogModel->module_id = $module_id;
            //    $this->billLogModel->save();
            //        return $this->billLogModel->id;
            return ReturnArr();
        }

        //    /**
        //     * 业务扣款账单
        //     * User: CCY
        //     * Date: 2020/6/17 14:30
        //     * @deprecated
        //     * @param $money
        //     * @param $reason
        //     * @param $module
        //     * @param $company
        //     * @param $companyCurrent
        //     * @param int $group_id
        //     * @param string $remark
        //     * @return int
        //     */
        public ReturnStruct addBill(
 /*$money, $reason, $module, $company, $companyCurrent, $group_id = 0, $remark = ''*/)
        {
            //    $this->billLogModel->money = $money;
            //    $this->billLogModel->belong = BillLogModel::BELONG_RIGHT;
            //    $this->billLogModel->type = BillLogModel::TYPE_DEDUCTION;
            //    $this->billLogModel->datetime = $this->datetime;
            //    $this->billLogModel->reason = $reason;
            //    $this->billLogModel->remark = $remark;
            //    $this->billLogModel->audit = BillLogModel::AUDIT_NOREVIEWREQUIRED;
            //    $this->billLogModel->user_id = $this->getSession()->user_id;
            //    $this->billLogModel->truename = $this->getSession()->getTruename();
            //    $this->billLogModel->company_id = $company['id'];
            //    $this->billLogModel->report_company_id = $company['report_id'];
            //    $this->billLogModel->creat_company_id = $companyCurrent['id'];
            //    $this->billLogModel->operate_company_id = $companyCurrent['id'];
            //    $this->billLogModel->operate_company_name = $companyCurrent['company_name'];
            //    $this->billLogModel->module = $module;
            //    $this->billLogModel->img_all = json_encode([]);
            //    $this->billLogModel->group_id = $group_id;
            //    $this->billLogModel->oem_id = $company['oem_id'];
            //    $this->billLogModel->save();
            //        return $this->billLogModel->id;
            return ReturnArr();
        }

        //    /**
        //     * 业务扣款账单(new)
        //     * @param $money
        //     * @param $reason
        //     * @param $module
        //     * @param $company
        //     * @param $companyCurrent
        //     * @param SessionData $currentUser
        //     * @param int $group_id
        //     * @param string $remark
        //     * @return BillLogModel
        //     */
        public static object addBillNew(
 /*$money, $reason, $module, $company, $companyCurrent, $currentUser, $group_id = 0, $remark = ''*/) //: BillLogModel
        {
            //    $billLog = new BillLogModel();
            //    $billLog->money = $money;
            //    $billLog->belong = BillLogModel::BELONG_RIGHT;
            //    $billLog->type = BillLogModel::TYPE_DEDUCTION;
            //    $billLog->datetime = Carbon::now()->format(self::SIMPLE_DATE);
            //    $billLog->reason = $reason;
            //    $billLog->remark = $remark;
            //    $billLog->audit = BillLogModel::AUDIT_NOREVIEWREQUIRED;
            //    $billLog->user_id = $currentUser->user_id;
            //    $billLog->truename = $currentUser->getTruename();
            //    $billLog->group_id = $group_id;
            //    $billLog->company_id = $company['id'];
            //    $billLog->report_company_id = $company['report_id'];

            //    $billLog->creat_company_id = $companyCurrent['id'];
            //    $billLog->oem_id = $company['oem_id'];
            //    $billLog->operate_company_id = $companyCurrent['id'];
            //    $billLog->operate_company_name = $companyCurrent['company_name'];

            //    $billLog->module = $module;
            //    $billLog->img_all = '[]';
            //    $billLog->save();
            //    return $billLog;
            return new object();
        }

        ///**
        // * 分销换绑、解绑清空余额账单
        // * User: CCY
        // * Date: 2020/6/23 17:23
        // * @param $money
        // * @param $reason
        // * @param $company
        // * @return mixed
        // */
        public ReturnStruct addEmptyWalletBill( /*$money, $reason, $company*/)
        {
            //    $this->billLogModel->money = $money;
            //    $this->billLogModel->belong = $this->billLogModel::BELONG_RIGHT;
            //    $this->billLogModel->type = $this->billLogModel::TYPE_DEDUCTION;
            //    $this->billLogModel->datetime = $this->datetime;
            //    $this->billLogModel->reason = $reason;
            //    $this->billLogModel->remark = '';
            //    $this->billLogModel->audit = $this->billLogModel::AUDIT_NOREVIEWREQUIRED;
            //    $this->billLogModel->user_id = $this->getSession()->getUserId();
            //    $this->billLogModel->truename = $this->getSession()->getTruename();
            //    $this->billLogModel->company_id = $company['id'];
            //    $this->billLogModel->report_company_id = $company['report_id'];
            //    $this->billLogModel->creat_company_id = $company['id'];
            //    $this->billLogModel->operate_company_id = $company['id'];
            //    $this->billLogModel->operate_company_name = $company['company_name'];
            //    $this->billLogModel->module = $this->billLogModel::MODULE_INTERCHANGEABLE;
            //    $this->billLogModel->img_all = json_encode([]);
            //    $this->billLogModel->group_id = $this->getSession()->getGroupId();
            //    $this->billLogModel->oem_id = $company['oem_id'];
            //    $this->billLogModel->save();
            //    return $this->billLogModel->id;
            return ReturnArr();
        }

        public async Task<DbSetExtension.PaginateStruct<MonthFinanceDto>> GetMonthFinance(int[] companyIds, DateOnly? month)
        {
            var monthStart = month?.AddDays(-month.Value.Day).ToDateTime(TimeOnly.MinValue) ?? DateTime.Now;
            var monthEnd = monthStart.AddMonths(1);
            var data = await _dbContext.Statistic
                .Where(s => (s.AboutType & Types.Role) == Types.Company && companyIds.Contains(s.AboutID))
                .WhenWhere(month.HasValue,
                    s => (s.AboutType & Types.Effective) == Types.MONTH && s.CreatedAt >= monthStart &&
                         s.CreatedAt < monthEnd)
                .WhenWhere(!month.HasValue, s => (s.AboutType & Types.Effective) == Types.TOTAL)
                .ToPaginateAsync();
            return data.Transform(s => new MonthFinanceDto(s.AboutID, s.OrderTotal, s.OrderTotalFee, s.OrderTotalRefund,
                s.PurchaseTotal, s.PurchaseTotalRefund,
                s.WaybillTotal, s.WaybillTotalRefund, s.RechargeTotal, s.ExpenseTotal));

        }
    }
}