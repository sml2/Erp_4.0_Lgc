﻿using ERP.Data;
using ERP.Models.DB.Purchase;
using ERP.Services.DB.Users;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace ERP.Services.Distribution
{
    public class CartService : BaseService
    {
        private readonly DBContext _dbContext;
        private readonly UserService _userService;

        public CartService(DBContext dbContext, UserService userService)
        {
            this._dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            this._userService = userService ?? throw new ArgumentNullException(nameof(userService));
        }

        ////原ERP3.0注释
        #region ERP3.0 PHP
        /**
     * 产品添加购物车
     * @param $id
     * @param $requestData
     * @return bool
     * @author CCY
     * @createdAt 2020/6/19 17:27
     */
        public ReturnStruct cartAdd(/*$id, $requestData*/)//: bool
        {
            //    // TODO: 价格不能信任前台字段
            //    $cart = new CartModel();
            //    $price = FromConversion($requestData['price'], $requestData['coin'] ?? $this->getSession()->getUnitConfig());
            //    $cart->name = $requestData['name'];
            //    $cart->product_id = $id;
            //    $cart->img = $requestData['img'];
            //    $cart->attr_name = $requestData['attr_name'];
            //    $cart->num = $requestData['num'];
            //    $cart->spu = $requestData['spu'];
            //    $cart->sku = $requestData['sku'] ?? '';
            //    $cart->price = $price;
            //    $cart->total = bcmul($price, $requestData['num'], 6);
            //    $cart->user_id = $this->getSession()->getUserId();
            //    $cart->company_id = $this->getSession()->getCompanyId();
            //    $cart->oem_id = $this->getSession()->getOemId();
            //    return $cart->save();
            return ReturnArr();
        }

        ///**
        // * 根据产品spu查找是否存在
        // * @param $spu
        // * @return CartModel|EloquentBuilder|Model|object|null
        // * @author CCY
        // * @createdAt 2020/6/19 18:26
        // */
        public ReturnStruct getInfo(/*$spu*/)
        {
            //    return CartModel::spu($spu)->companyId($this->getSession()->company_id)->first();
            return ReturnArr();
        }

        ///**
        // * 添加同样商品更新数据
        // * @param $info
        // * @param $requestData
        // * @return bool|int
        // * @author CCY
        // * @createdAt 2020/6/19 18:08
        // */
        public ReturnStruct updateCart(/*$info, $requestData*/)
        {
            //    $num = $info['num'] + $requestData['num'];
            //    $price = FromConversion($requestData['price'], $requestData['coin'] ?? $this->getSession()->getUnitConfig());
            //    $total = bcmul($price, $num, 6);

            //    $updateData = [
            //        'num'   => $num,
            //        'price' => $price,
            //        'total' => $total
            //    ];
            //    return CartModel::id($info['id'])->update($updateData);
            return ReturnArr();
        }

        /// <summary>
        /// 获取指定用户购物车信息
        /// </summary>
        /// <returns></returns>
        public async Task<List<Cart>> getList()
        {
            //    return CartModel::UserId($this->getSession()->getUserId())
            //        ->CompanyId($this->getSession()->getCompanyId())
            //        ->OemId($this->getSession()->getOemId())
            //        ->select(['id', 'name', 'img', 'attr_name', 'num', 'price', 'total', 'product_id', 'spu', 'sku'])
            //        ->Sort()
            //        ->get();
            var user = await _userService.GetCurrentUser();
            if (user is not null)
             return await _dbContext.Carts.Where(c => c.UserID == user.Id && c.CompanyID == user.CompanyID
                && c.OEMID == user.OEMID)
                .OrderByDescending(c => c.CreatedAt)
                .ToListAsync();

            return new();
        }

        /// <summary>
        /// 主键删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<int> deleteInfoById(int id)
        {
            _dbContext.Carts.Remove(new Cart() { ID = id });
            return await _dbContext.SaveChangesAsync();
        }

        ///**
        // * 删除购物车指定产品
        // * @author CCY
        // * @createdAt 2020/6/20 17:57
        // * @param $ids
        // * @return mixed
        // */
        public ReturnStruct deleteInfoByIds(/*$ids*/)
        {
            //    return CartModel::query()
            //        ->UserId($this->getSession()->getUserId())
            //        ->CompanyId($this->getSession()->getCompanyId())
            //        ->OemId($this->getSession()->getOemId())
            //        ->ids($ids)
            //        ->delete();
            return ReturnArr();
        }


        /// <summary>
        /// 清除购物车
        /// </summary>
        /// <returns></returns>
        public async Task<int> clearCart()
        {
            var user = await _userService.GetCurrentUser();
            if (user is not null)
             return await _dbContext.Carts.Where(c => c.UserID == user.Id && c.CompanyID == user.CompanyID
                    && c.OEMID == user.OEMID)
                    .DeleteFromQueryAsync();

            return -1;
        }

        #endregion
    }
}
