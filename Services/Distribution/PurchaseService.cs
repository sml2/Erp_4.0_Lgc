﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using ERP.Data;
using ERP.Enums.Purchase;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.DB.Purchase;
using ERP.Models.View.Purchase;
using Microsoft.EntityFrameworkCore;

namespace ERP.Services.Distribution;
public class PurchaseService : BaseService
{
    private readonly DBContext _dbContext;
    private readonly IMapper _mapper;
    private readonly ISessionProvider _iSessionProvider;


    public PurchaseService(DBContext dbContext, IMapper mapper, ISessionProvider iSessionProvider)
    {
        this._dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        _mapper = mapper;
        _iSessionProvider = iSessionProvider;
    }

    ////原ERP3.0注释
    #region ERP3.0 PHP
    /// <summary>
    /// 上级获取上报采购分页列表
    /// </summary>
    /// <param name="currentCompanyId"></param>
    /// <param name="orderNo"></param>
    /// <param name="purchaseState"></param>
    /// <param name="companyId"></param>
    /// <param name="purchaseUserId"></param>
    /// <returns></returns>
    public Task<DbSetExtension.PaginateStruct<DistributionPurchaseListDto>> GetListPage(int currentCompanyId, string? orderNo, PurchaseStates? purchaseState, int? companyId, int? purchaseUserId = null)
    {
        return _dbContext.Purchase.Where(p => p.PurchaseCompanyId == currentCompanyId)
            .AsNoTracking()
            .WhenWhere(!string.IsNullOrEmpty(orderNo), p => p.OrderNo == orderNo)
            .WhenWhere(purchaseState.HasValue, p => p.PurchaseState == purchaseState)
            .WhenWhere(companyId.HasValue, p => p.CompanyID == companyId)
            .WhenWhere(purchaseUserId.HasValue, p => p.PurchaseUserId == purchaseUserId)
            .OrderBy(p => p.PurchaseState)
            .ThenByDescending(p => p.ID)
            .ProjectTo<DistributionPurchaseListDto>(_mapper.ConfigurationProvider)
            .ToPaginateAsync();;
    }
    
    /// <summary>
    /// 下级获取上报采购分页列表
    /// </summary>
    /// <param name="currentCompanyId"></param>
    /// <param name="reportCompanyId"></param>
    /// <param name="orderNo"></param>
    /// <param name="purchaseState"></param>
    /// <param name="storeId"></param>
    /// <param name="storeIds"></param>
    /// <returns></returns>
    public Task<DbSetExtension.PaginateStruct<DistributionPurchaseJuniorListDto>> GetJuniorListPage(int currentCompanyId,int reportCompanyId,string? orderNo, PurchaseStates? purchaseState, int? storeId, int[] storeIds)
    {
        var ids = storeIds.Cast<int?>().ToArray();
        return _dbContext.Purchase.Where(p => p.CompanyID == currentCompanyId && p.PurchaseCompanyId == reportCompanyId)
                .WhenWhere(purchaseState.HasValue, p => p.PurchaseState == purchaseState)
                .WhenWhere(storeId.HasValue, p => p.StoreId == storeId)
                .WhenWhere(!string.IsNullOrEmpty(orderNo), p => p.OrderNo == orderNo)
                .WhenWhere(storeIds is { Length: > 0 }, p => ids.Contains(p.StoreId))
                .OrderByDescending(p => p.ID)
                .Select(p => new DistributionPurchaseJuniorListDto(p.ID, p.Url, p.Name, p.FromUrl, p.OrderNo, p.Num, p.Price, p.Unit, p.StoreId, p.CreatedAt, p.PurchaseState, p.OrderId, p.OperateCompanyId))
                .ToPaginateAsync();
    }

    /// <summary>
    /// 获取采购详细信息
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public ValueTask<Models.DB.Purchase.Purchase?> GetInfoById(int id)
    {
        return _dbContext.Purchase.FindAsync(id);
    }

    /// <summary>
    /// 获取订单详情展示采购信息
    /// </summary>
    /// <param name="orderId"></param>
    /// <returns></returns>
    public Task<List<DistributionPurchaseForOrderInfo>> GetPurchaseByOrderId(int orderId)
    {

        return _dbContext.Purchase.AsNoTracking().Where(p => p.OrderId == orderId)
            .OrderByDescending(p => p.CreatedAt)
            .Select(p => new DistributionPurchaseForOrderInfo(p.ID, p.Url, p.Name, p.FromUrl, p.OrderNo, p.Num, p.Price, p.Unit, p.CompanyID, p.CreatedAt, p.PurchaseState, p.OrderId, p.PurchasePrice, p.PurchaseTotal, p.PurchaseTrackingNumber, p.SellerSku, p.PurchaseOrderNo, p.OperateCompanyId, p.PurchaseOther))
            .ToListAsync();
    }

    ///**
    // *上级添加采购信息
    // * User: CCY
    // * Date: 2020/6/17 11:33
    // * @param $requestData
    // * @param $order
    // * @param $good
    // * @return int
    // */
    public ReturnStruct insertPurchase(/*$requestData, $order, $good, $bill_log_id = 0, $oem_id = 0*/)
    {
        //        $oem_id = $oem_id ?: $this->getSession()->getOemId();
        //        $stores = $this->getCompanyStore($order['company_id']);
        //        $insertData = [
        //            'order_id'                 => $order['id'],
        //            'name'                     => $good['name'],
        //            'asin'                     => $good['asin'],
        //            'price'                    => $good['price'],
        //            'num'                      => $requestData['num'],
        //            'product_total'            => $good['total'],
        //            'unit'                     => $good['unit'],
        //            'url'                      => $good['url'],
        //            'from_url'                 => $good['from_url'],
        //            'purchase_state'           => $requestData['purchase_state'],
        //            'order_no'                 => $order['order_no'],
        //            'oem_id'                   => $oem_id,
        //            'company_id'               => $order['company_id'],
        //            'platform_id'              => 1,
        //            'purchase_unit'            => $requestData['purchase_unit'],
        //            'purchase_total'           => FromConversion($requestData['purchase_total'], $requestData['purchase_unit']),
        //            'purchase_other'           => FromConversion($requestData['purchase_other'], $requestData['purchase_unit']),
        //            'purchase_price'           => FromConversion($requestData['purchase_price'], $requestData['purchase_unit']),
        //            'purchase_url'             => $requestData['purchase_url'],
        //            'purchase_order_no'        => $requestData['purchase_order_no'],
        //            'purchase_tracking_number' => $requestData['purchase_tracking_number'],
        //            'remark'                   => $requestData['remark'],
        //            'purchase_affirm'          => PurchaseModel::PURCHASEAFFIRM_CONFIRMED,
        //            'purchase_user_id'         => $this->getSession()->getUserId(),
        //            'find_truename'            => $this->getSession()->getTruename(),
        //            'affirm_truename'          => $this->getSession()->getTruename(),
        //            'is_affirm'                => PurchaseModel::ISAFFIRM_NO,
        //            'bill_log_id'              => $bill_log_id,
        //            'store_id'                 => $order['store_id'],
        //            'store_name'               => isset($stores[$order['store_id']]) ? $stores[$order['store_id']]['name'] : '',
        //            'seller_sku'               => $good['seller_sku'],
        //            'purchase_rate'            => FromConversion($requestData['purchase_fee'], $requestData['purchase_unit']),
        //            'purchase_rate_type'       => $requestData['purchase_rate_type'],
        //            'purchase_company_id'      => $this->getSession()->getCompanyId(),
        //            'operate_company_id'       => $this->getSession()->getCompanyId(),
        //            'is_stock'                 => PurchaseModel::IS_STOCK_FALSE,
        //            'created_at'               => $this->datetime
        //        ];
        //return PurchaseModel::query()->insertGetId($insertData);
        return ReturnArr();
    }

    //    /**
    //     *分销上级更新采购信息
    //     * User: CCY
    //     * Date: 2020/6/17 16:17
    //     * @param $requestData
    //     * @return int
    //     */
    public ReturnStruct updatePurchaseModel(/*$requestData*/)
    {
        //        $updateData = [
        //            'purchase_state'           => $requestData['purchase_state'],
        //            'purchase_unit'            => $requestData['purchase_unit'],
        //            'purchase_total'           => FromConversion($requestData['purchase_total'], $requestData['purchase_unit']),
        //            'purchase_other'           => FromConversion($requestData['purchase_other'], $requestData['purchase_unit']),
        //            'purchase_price'           => FromConversion($requestData['purchase_price'], $requestData['purchase_unit']),
        //            'purchase_url'             => $requestData['purchase_url'],
        //            'purchase_order_no'        => $requestData['purchase_order_no'],
        //            'purchase_tracking_number' => $requestData['purchase_tracking_number'],
        //            'remark'                   => $requestData['remark'],
        //            'purchase_rate'            => FromConversion($requestData['purchase_fee'], $requestData['purchase_unit']),
        //            'purchase_rate_type'       => $requestData['purchase_rate_type']
        //        ];
        //    return PurchaseModel::query()->where('id', $requestData['id'])->update($updateData);
        return ReturnArr();
    }


    ///**
    // * 增加采购单条日志
    // * User: CCY
    // * Date: 2020/6/24 14:40
    // * @param string $orderId
    // * @param string $purchaseId
    // * @param string $action
    // * @param int $platformId
    // * @param string $oem_id
    // * @param string $company_id
    // * @param string $group_id
    // * @return PurchaseLog|Model
    // */
    public ReturnStruct addPurchaseLog(/*$orderId = '', $purchaseId = '', $action = '', $platformId = 1, $oem_id = 0, $company_id = 0, $group_id = 0*/)
    {
        //        $oem_id === 0 && $oem_id = $this->getSession()->getOemId();
        //        $company_id === 0 && $company_id = $this->getSession()->getCompanyId();
        //        $data = [
        //            'purchase_id' => $purchaseId,
        //            'action'      => $action,
        //            'user_id'     => $this->getSession()->getUserId(),
        //            'username'    => $this->getSession()->getUsername(),
        //            'truename'    => $this->getSession()->getTruename(),
        //            'oem_id'      => $oem_id,
        //            'group_id'    => $group_id,
        //            'company_id'  => $company_id,
        //            'order_id'    => $orderId,
        //            'platform_id' => $platformId,
        //        ];
        //    return PurchaseLog::create($data);
        return ReturnArr();
    }

    ///**
    // *购物车绑定空订单 批量增加采购信息
    // * User: CCY
    // * Date: 2020/6/20 15:09
    // * @param $order_id
    // * @param $order_no
    // * @param $good
    // * @return int
    // */
    public ReturnStruct puechaseAddAll(/*$order_id, $order_no, $good, $bill_log_id = 0*/)
    {
        //        $session = $this->getSession();
        //    foreach ($good as $value) {
        //            $insertData[] = [
        //                'order_id'                 => $order_id,
        //                'name'                     => $value['name'],
        //                'asin'                     => '',
        //                'price'                    => $value['price'],
        //                'num'                      => $value['num'],
        //                'product_total'            => $value['total'],
        //                'unit'                     => $session->getUnitConfig(),
        //                'url'                      => $value['url'],
        //                'from_url'                 => '',
        //                'purchase_state'           => PurchaseModel::STATE_INPROCUREMENT,
        //                'order_no'                 => $order_no,
        //                'company_id'               => $session->getCompanyId(),
        //                'platform_id'              => 1,
        //                'purchase_unit'            => $session->getUnitConfig(),
        //                'purchase_total'           => $value['total'],
        //                'purchase_other'           => 0,
        //                'purchase_price'           => $value['price'],
        //                'purchase_url'             => '',
        //                'purchase_order_no'        => '',
        //                'purchase_tracking_number' => '',
        //                'remark'                   => '购物车购买',
        //                'purchase_affirm'          => PurchaseModel::PURCHASEAFFIRM_CONFIRMED,
        //                'purchase_user_id'         => $session->getUserId(),
        //                'find_truename'            => $session->getTruename(),
        //                'affirm_truename'          => $session->getTruename(),
        //                'is_affirm'                => PurchaseModel::ISAFFIRM_NO,
        //                'store_id'                 => 0,
        //                'store_name'               => '',
        //                'bill_log_id'              => $bill_log_id,
        //                'seller_sku'               => $value['seller_sku'],
        //                'purchase_rate'            => 0,
        //                'purchase_company_id'      => $session->report_id,
        //                'operate_company_id'       => $session->report_id,
        //                'create_company_id'        => $session->getCompanyId(),
        //                'oem_id'                   => $session->getOemId(),
        //                'group_id'                 => $session->getGroupId(),
        //                'is_stock'                 => PurchaseModel::IS_STOCK_FALSE,
        //                'created_at'               => $this->datetime
        //            ];
        //}

        //return PurchaseModel::query()->insert($insertData);
        return ReturnArr();
    }

    //    /**
    //     *订单选择购物车
    //     * User: CCY
    //     * Date: 2020/6/20 18:17
    //     * @param $order
    //     * @param $goods
    //     * @param int $bill_log_id
    //     * @return bool
    //     */
    public ReturnStruct puechaseAdd(/*$order, $goods*/)
    {
        //        $orderGood = arrayCombine(json_decode($order['goods'], true), 'good_id');

        //    foreach ($goods as $value) {
        //            $good = $orderGood[$value['good_id']];
        //            $insertData[] = [
        //                'order_id'                 => $order['id'],
        //                'name'                     => $value['name'],
        //                'asin'                     => $good['asin'],
        //                'price'                    => $good['price'],
        //                'num'                      => $value['num'],
        //                'product_total'            => $good['total'],
        //                'unit'                     => $good['unit'],
        //                'url'                      => $good['url'],
        //                'from_url'                 => $good['from_url'],
        //                'purchase_state'           => PurchaseModel::STATE_INPROCUREMENT,
        //                'order_no'                 => $order['order_no'],
        //                'company_id'               => $this->getSession()->getCompanyId(),
        //                'platform_id'              => 1,
        //                'purchase_unit'            => $this->getSession()->getUnitConfig(),
        //                'purchase_total'           => FromConversion($value['total'], $this->getSession()->getUnitConfig()),
        //                'purchase_other'           => 0,
        //                'purchase_price'           => FromConversion($value['price'], $this->getSession()->getUnitConfig()),
        //                'purchase_url'             => '',
        //                'purchase_order_no'        => '',
        //                'purchase_tracking_number' => '',
        //                'remark'                   => '购物车购买',
        //                'purchase_affirm'          => PurchaseModel::PURCHASEAFFIRM_CONFIRMED,
        //                'purchase_user_id'         => $this->getSession()->getUserId(),
        //                'find_truename'            => $this->getSession()->getTruename(),
        //                'affirm_truename'          => $this->getSession()->getTruename(),
        //                'is_affirm'                => PurchaseModel::ISAFFIRM_NO,
        //                'store_id'                 => 0,
        //                'store_name'               => '',
        //                'bill_log_id'              => 0,
        //                'seller_sku'               => $good['seller_sku'],
        //                'purchase_rate'            => 0,
        //                'purchase_company_id'      => $this->getSession()->getReportId(),
        //                'operate_company_id'       => $this->getSession()->getReportId(),
        //                'oem_id'                   => $this->getSession()->getOemId(),
        //                'group_id'                 => $this->getSession()->getGroupId(),
        //                'is_stock'                 => PurchaseModel::IS_STOCK_FALSE,
        //                'created_at'               => $this->datetime
        //            ];
        //        }

        //        return PurchaseModel::query()->insert($insertData);
        return ReturnArr();
    }

    //    /**
    //     * 上级采购确认获取采购信息
    //     * User: CCY
    //     * Date: 2020/7/2 17:22
    //     * @param $purchase_order_no
    //     * @param $purchase_tracking_number
    //     * @param int $viewRange 1 分销 2 上级
    //     * @return PurchaseModel[]|Builder[]|Collection
    //     */
    public async Task<List<Models.DB.Purchase.Purchase>> getPurchase(string purchaseOrderNo = "", string purchaseTrackingNumber = "", int viewRange = 1)
    {
        //        $field = ['id', 'url', 'name', 'num', 'price', 'created_at', 'purchase_state', 'order_id', 'purchase_total', 'purchase_order_no', 'purchase_tracking_number', 'purchase_affirm', 'purchase_unit', 'store_id', 'company_id','is_stock',
        //            'warehouse_id'];

        //        $where[] = ['company_id', '=', $this->getSession()->getCompanyId()];
        //        if ($viewRange === 2) {
        //            $where = function ($query) use ($where) {
        //                $query->where($where)->orWhere('purchase_company_id',$this->getSession()->getCompanyId());
        //            };
        //        }
        //        return PurchaseModel::query()
        ////            ->PurchaseCompanyId($purchaseCompanyId)
        ////            ->PurchaseAffirm(PurchaseModel::PURCHASEAFFIRM_CONFIRMED)
        //            ->PurchaseOrderNo($purchase_order_no, false)
        //            ->PurchaseTrackingNumber($purchase_tracking_number, false)
        //            ->where($where)
        //            ->sortState()
        //            ->sort()
        //            ->select($field)
        //            ->get();
        var model = _dbContext.Purchase
            .WhenWhere(!purchaseOrderNo.IsNullOrEmpty(),m => m.PurchaseOrderNo == purchaseOrderNo)
            .WhenWhere(!purchaseTrackingNumber.IsNullOrEmpty(),m => m.PurchaseTrackingNumber == purchaseTrackingNumber);

        if (viewRange == 2)
        {
            model = model.Where(m => m.CompanyID == _iSessionProvider.Session.GetCompanyID() || m.PurchaseCompanyId == _iSessionProvider.Session.GetCompanyID());
        }
        else
        {
            model = model.Where(m => m.CompanyID == _iSessionProvider.Session.GetCompanyID());
        }

        return await model.ToListAsync();
    }

    //    /**
    //     * 拒签
    //     * User: CCY
    //     * Date: 2020/7/3 10:27
    //     * @param $id
    //     * @return bool|int
    //     */
    public static object rejection(/*$id*/)
    {
        //        $update = [
        //            'purchase_state'  => PurchaseModel::STATE_INPROCUREMENT,
        //            'purchase_affirm' => PurchaseModel::PURCHASEAFFIRM_UNCONFIRMED
        //        ];
        //        return PurchaseModel::query()->IdIn($id)->update($update);
        return new object();
    }

    //    /**
    //     * 签收
    //     * User: CCY
    //     * Date: 2020/7/3 10:57
    //     * @param $id
    //     * @return bool|int
    //     */
    public static object confirmPurchase(/*$id*/)
    {
        //        $update = [
        //            'purchase_state'  => PurchaseModel::STATE_PURCHASED,
        //            'purchase_affirm' => PurchaseModel::PURCHASEAFFIRM_CONFIRMED
        //        ];
        //        return PurchaseModel::query()->IdIn($id)->update($update);
        return new object();
    }

    /// <summary>
    /// 获取采购日志
    /// </summary>
    /// <param name="purchaseId"></param>
    /// <returns></returns>
    public Task<List<PurchaseLog>> GetAllLog(int purchaseId)
    {
        //            ->select(['action','truename','created_at'])
        return _dbContext.PurchaseLog.Where(l => l.PurchaseId == purchaseId)
            .OrderByDescending(l => l.ID)
            .ToListAsync();
    }


    #endregion
}

