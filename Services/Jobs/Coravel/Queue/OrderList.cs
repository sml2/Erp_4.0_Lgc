using Coravel.Invocable;
using Coravel.Queuing.Interfaces;
using ERP.Data;
using Microsoft.EntityFrameworkCore;

namespace ERP.Jobs.Queue;

public class OrderList : IInvocable, IInvocableWithPayload<int>
{
    private ILogger<OrderList> _logger;
    private IQueue _queue;
    private DBContext _dbContext;
    private Once once = Once.Create();
    public OrderList(ILogger<OrderList> logger, IQueue queue, DBContext dbContext) {
        _logger = logger;
        _queue = queue;
        _dbContext = dbContext;
        _logger.LogTrace("Instance New");
    }
    public int Payload { get; set; }

    /* Constructor, etc. */

    public async Task Invoke()
    {
        Console.WriteLine($"{DateTime.Now} {Payload}");
        Thread.Sleep(Payload);

        _logger.LogTrace($"Start {nameof(Invoke)}");

        if (once) {
            _queue.QueueInvocableWithPayload<OrderList, int>(500);
            _queue.QueueInvocableWithPayload<OrderList, int>(500);
            _queue.QueueInvocableWithPayload<OrderList, int>(500);
            _queue.QueueInvocableWithPayload<OrderList, int>(1500);
            _queue.QueueInvocableWithPayload<OrderList, int>(5000);
        }
        _queue.QueueInvocableWithPayload<OrderList, int>(4000);
        _logger.LogTrace($"Ended {nameof(Invoke)}");
         await Task.CompletedTask;
    }
}