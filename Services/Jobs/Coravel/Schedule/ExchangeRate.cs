﻿using Coravel.Invocable;
using Ryu.Data.Abstraction;
using Service = ERP.Services.DB.Setting.Unit;
using Cache = ERP.Services.Caches.Unit;
using Aliyun = ERP.Services.Api.ExchangeRate.Aliyun;
namespace ERP.Services.Jobs.Coravel.Schedule;


public class ExchangeRate : IInvocable
{
    private readonly Service _service;
    private readonly IRateProvider _provider;
    private readonly Cache _cache;
    private readonly Aliyun _api;
    private readonly ILogger<ExchangeRate> _logger;

    public ExchangeRate(Service service,IRateProvider provider, Cache cache, Aliyun api, ILogger<ExchangeRate> logger)
    {
        _service = service;
        _provider = provider;
        _cache = cache;
        _api = api;
        _logger = logger;
    }

    public async Task Invoke()
    {
        try
        {
            var dic = await _api.Get(_logger);
            if (dic is not null)
            {
                _logger.LogInformation("Api Return [{Count}]", dic.Count);
                foreach (var item in dic)
                {
                    var m = _cache.Get(item.Key);
                    if (m is not null)
                    {
                        var Info = item.Value;
                        if (Info.Rate != 0)
                        {
                            var @old = m.Rate;
                            var @new = 10000 / Info.Rate;
                            m.Rate = @new;
                            m.RefreceTime = Info.RefreceTime;
                            await _service.Update(m);
                            //_service.Update(m.ID, m.Rate, Info.RefreceTime)
                            _logger.LogInformation("{info}'s rate [{@old}]->[{@new}] ", Info, @old, @new);
                        }
                        else
                        {
                            _logger.LogWarning("{item}'s rate be zero", Info);
                        }
                    }
                }
                _provider.Update();
                _logger.LogWarning("{Rate} Already Deploy",nameof(Provider));
            }
            else
            {
                _logger.LogWarning("Api Return Null");
            }
        }
        catch (Exception ex)
        {
            _logger.LogError("Message:{m}{newLine}Info:{ex}",ex.Message,Environment.NewLine,ex);
        }
    }
}
