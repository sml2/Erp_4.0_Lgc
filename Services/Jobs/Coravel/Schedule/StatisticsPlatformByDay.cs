using Coravel.Invocable;
using ERP.Services.DataStatistics;
using ERP.Services.Setting;

namespace ERP.Services.Jobs.Coravel.Schedule;

public class StatisticsPlatformByDay : IInvocable
{
    private readonly ILogger<StatisticsPlatformByDay> _logger;
    private readonly StatisticsPlatformByDayService _service;

    public StatisticsPlatformByDay(ILogger<StatisticsPlatformByDay> logger, StatisticsPlatformByDayService service)
    {
        _logger = logger;
        _service = service;
    }

    public async Task Invoke()
    {
        try
        {
           await _service.Handle();
            _logger.LogInformation("统计平台日增数据量 Complete");
        }
        catch (Exception ex)
        {
            _logger.LogError("Message:{m}{newLine}Info:{ex}",ex.Message,Environment.NewLine,ex);
        }
    }
}
