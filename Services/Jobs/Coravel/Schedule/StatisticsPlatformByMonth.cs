using Coravel.Invocable;
using ERP.Extensions;
using ERP.Services.DataStatistics;
using ERP.Services.Setting;

namespace ERP.Services.Jobs.Coravel.Schedule;

public class StatisticsPlatformByMonth : IInvocable
{
    private readonly ILogger<StatisticsPlatformByMonth> _logger;
    private readonly StatisticsPlatformByMonthService _service;

    public StatisticsPlatformByMonth(ILogger<StatisticsPlatformByMonth> logger,
        StatisticsPlatformByMonthService service)
    {
        _logger = logger;
        _service = service;
    }

    public async Task Invoke()
    {
        try
        {
            var now = DateTime.Now;
            var cDate = now.Date;
            var lastMonthDate = now.LastMonthTick().Date;

            if (cDate == lastMonthDate)
            {
                await _service.Handle();
                _logger.LogInformation("统计平台月活 Complete"); 
            }
            else
            {
                _logger.LogInformation("未到月底无需统计月活 Complete");  
            }

           
        }
        catch (Exception ex)
        {
            _logger.LogError("Message:{m}{newLine}Info:{ex}", ex.Message, Environment.NewLine, ex);
        }
    }
}