# Jobs

## 目录结构

``` bash

|- Queue     # 队列(单次)任务
|- Schedule  # 定时任务

```

## 队列简单用例

[文档链接](https://docs.coravel.net/Queuing/)

定义一个队列任务类

``` c#
                                        // 必须继承 👇      // 需要传递参数时继承 👇
public class SendWelcomeUserEmailInvocable : IInvocable, IInvocableWithPayload<UserModel>
{
  // This is the implementation of the interface 👇
  public UserModel Payload { get; set; }

  /* Constructor, etc. */

  public async Task Invoke()
  {
    // `this.Payload` will be available to use now.
  }
}
```

业务代码添加队列,控制器或服务类注入`IQueue queue`

``` c#
var userModel = await _userService.Get(userId);
queue.QueueInvocableWithPayload<SendWelcomeUserEmailInvocable, UserModel>(userModel);
```
