﻿using ERP.Extensions;
using ERP.Interface;
using Microsoft.EntityFrameworkCore;
using UserModel = ERP.Models.Abstract.UserModel;
using ERP.Data;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using ERP.ViewModels.User;
using ERP.Enums.Rule;
using ERP.Interface.Rule;
using ERP.Models.DB.Identity;
using ERP.Models.DB.Rule;
using ERP.Models.View.Users;
using ERP.Services.Caches;
using Microsoft.ApplicationInsights.Metrics.Extensibility;
using Z.EntityFramework.Plus;

namespace ERP.Services.DB.Users;

using RuleTemplateModel = Models.DB.Users.RuleTemplate;
using Model = Models.DB.Users.RuleTemplate;
using RULE = Attributes.Rule.RuleAttribute;

public class RuleTemplate : BaseService
{
    private readonly DBContext _dbContext;
    private readonly ISessionProvider _sessionProvider;
    private readonly Services.Caches.Menu _menuCache;

    public RuleTemplate(DBContext dbContext, ISessionProvider session, Menu menuCache)
    {
        _dbContext = dbContext;
        _sessionProvider = session;
        _menuCache = menuCache;
    }

    private ISession Session
    {
        get => _sessionProvider.Session!;
    }

    //    /**
    //     * 权限模版列表分页
    //     * User: CCY
    //     * Date: 2020/4/28 11:57
    //     * @param $name
    //     * @param $state
    //     * @param $type
    //     * @param $company_id
    //     * @param $oem_id
    //     * @return mixed
    //     */
    public Task<DbSetExtension.PaginateStruct<RuleTemplateModel>> GetRuleListPage(string? name,
        Models.Abstract.BaseModel.StateEnum? state, int companyId, int oemId)
    {
        //        $field = ['id', 'name', 'state', 'rule_id', 'remark', 'created_at'];
        //            ->sort()
        return _dbContext.RuleTemplate.WhenWhere(!string.IsNullOrEmpty(name), r => r.Name == name)
            .WhenWhere(state.HasValue, r => r.State == state)
            .Where(r => r.CompanyID == companyId && r.OEMID == oemId)
            .OrderByDescending(r => r.ID)
            .ToPaginateAsync();
    }

    /// <summary>
    /// 获取权限模版详细信息
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    internal async Task<RuleTemplateModel> FindByIdAsync(int id)
    {
        var ruleTemplate = await _dbContext.RuleTemplate.FirstOrDefaultAsync(m => m.ID == id);
        if (ruleTemplate == null) throw new Exception($"未找到[ID:{id}]的角色模板");
        return ruleTemplate;
    }

    /// <summary>
    /// 修改存储模块
    /// </summary>
    /// <param name="id"></param>
    /// <param name="ruleId"></param>
    /// <param name="requestData"></param>
    /// <param name="companyId"></param>
    /// <param name="oemId"></param>
    /// <returns></returns>
    public async Task SaveInfo(int id, int ruleId, RuleTemplateModel requestData, int companyId = 0, int oemId = 0)
    {
        RuleTemplateModel? ruleTemplate;
        if (id > 0) //修改
        {
            ruleTemplate = await _dbContext.RuleTemplate.FirstOrDefaultAsync(a => a.ID == id);
            if (ruleTemplate == null)
            {
                throw new Exception("修改存储模块错误");
            }

            ruleTemplate.Name = requestData.Name;
            ruleTemplate.Remark = requestData.Remark;
            ruleTemplate.State = requestData.State;
            _dbContext.RuleTemplate.Update(ruleTemplate);
        }
        else //添加
        {
            await _dbContext.RuleTemplate.AddAsync(requestData);
        }

        await _dbContext.SaveChangesAsync();
    }

    //    /**
    //     * 主键删除
    //     * User: CCY
    //     * Date: 2020/3/20 14:08
    //     * @param $id
    //     * @return int
    //     */
    public async Task<bool> Delete(int id)
    {
        //        return ruleTemplateModel::destroy($id);
        var row = await _dbContext.RuleTemplate.FirstOrDefaultAsync(a => a.ID == id);
        if (row == null)
            return false;

        _dbContext.RuleTemplate.Remove(row);
        return await _dbContext.SaveChangesAsync() > 0;
    }

    public record RuleListTemplate(int ID, string? Name, int RuleId, Models.Abstract.BaseModel.StateEnum State,
        int CompanyID);

    /// <summary>
    /// 获取当前公司全部权限模版信息
    /// </summary>
    /// <param name="state"></param>
    /// <param name="ComPanyId"></param>
    /// <param name="OemId"></param>
    /// <returns></returns>
    public async Task<List<RuleListTemplate>> GetRuleList(int ComPanyId, int OemId,
        Models.Abstract.BaseModel.StateEnum? state = null)
    {
        return await _dbContext.RuleTemplate
            .WhenWhere(state != null, x => x.State == state)
            .Where(x => x.CompanyID == ComPanyId)
            .WhenWhere(OemId > 0, x => x.OEMID == OemId)
            .OrderByDescending(x => x.Sort)
            .Select(x => new RuleListTemplate(x.ID, x.Name, x.ID, x.State, x.CompanyID))
            .ToListAsync();
    }

    //    /**
    //     * 更新权限组信息
    //     * User: CCY
    //     * Date: 2021/1/22
    //     * @param $where
    //     * @param $updateData
    //     * @return int
    //     */
    public ReturnStruct updateRuleTemplate( /*$where, $updateData*/)
    {
        //        return ruleTemplateModel::query()->where($where)->update($updateData);
        return ReturnArr();
    }

    public async Task SetRuleTemplateAuthData(AuthInfo authInfo)
    {
        var id = authInfo.ID;
        Model ruleTemplate = await FindByIdAsync(id);
        var ul = Convert.ToInt64(Flag.BASE);
        

        var orderModuleMenus = _menuCache.ListClone()?.OrderBy(i => i.ParentId).ThenBy(i => i.Sort);
        if (orderModuleMenus is null)
        {
            throw new Exception("菜单缓存异常");
        }

        foreach (var propertie in ruleTemplate.GetType().GetProperties())
        {
            if (propertie is not null)
            {
                var attribute = propertie.GetCustomAttributes(typeof(RULE), false).FirstOrDefault() as RULE;
                if (attribute is not null && attribute.Type == Types.CONFIG)
                {
                    var propName = propertie.Name;
                    var visible = $"{propName}.{nameof(IConfig.Visible)}";
                    
                    ruleTemplate.SetValue(visible, ul);
                }
            }
        }

        
        foreach (var item in orderModuleMenus)
        {
            ruleTemplate.SetValue(item.Module, ul);
        }

        //重置权限字段
        authInfo.Data.ForEach(i => ruleTemplate.SetValue(i.Module, ruleTemplate.GetValue(i.Module) | i.PermissionFlag));
        await _dbContext.SaveChangesAsync();
    }

    private async Task SetConfig(Model rule, Dictionary<string, object> config)
    {
        config.ForEach(kv =>
            {
                var Key = kv.Key;
                if (Key.Contains('.'))
                {
                    var s = Key.Split('.');
                    var PropName = s[0];
                    if (long.TryParse(s[1], out var Mask))
                    {
                        var V = kv.Value;
                        long value;
                        if (V is bool b)
                            value = b ? 1 : 0;
                        else
                            value = long.Parse(V.ToString());

                        var PropVisitor = $"{PropName}.{nameof(IConfig.Editable)}";
                        var old = rule.GetValue(PropVisitor);
                        old &= ~Mask;
                        old ^= value << Helpers.GetLastBit((ulong) Mask);
                        rule.SetValue(PropVisitor, old);
                    }
                    else
                    {
                        throw new ArgumentException(nameof(s));
                    }
                }
                else
                {
                    throw new ArgumentException(nameof(Key));
                }
            }
        );
    }
    
    public async Task<bool> SetUserConfigItems(SetConfigItemsDto vm)
    {
        var claims = await _dbContext.UserClaims.Where(m => m.UserId == vm.Id).ToListAsync();
        Model rule = new RuleTemplateModel();
        foreach (var item in claims)
        {
            rule.SetValue(item.ClaimType,Convert.ToInt64(item.ClaimValue));
        }

        await SetConfig(rule, vm.Config);
        var newclaims = rule.GetClaims(_sessionProvider.Session.GetOEMID());

        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            await _dbContext.UserClaims.Where(m => m.UserId == vm.Id).DeleteAsync();
            _dbContext.UserClaims.AddRange(newclaims.Select(m => new UserClaim(vm.Id, m)));
            await transaction.CommitAsync();
            return await _dbContext.SaveChangesAsync() > 0;
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            Console.WriteLine(e.Message);
            throw new Exception(e.Message);
        }
    }

    public async Task SetConfigItems(int id, Dictionary<string, object> config)
    {
        Model ruleTemplate = await FindByIdAsync(id);
        //重置权限字段
        config.ForEach(kv =>
            {
                var Key = kv.Key;
                if (Key.Contains('.'))
                {
                    var s = Key.Split('.');
                    var PropName = s[0];
                    if (long.TryParse(s[1], out var Mask))
                    {
                        var V = kv.Value;
                        long value;
                        if (V is bool b)
                            value = b ? 1 : 0;
                        else
                            value = long.Parse(V.ToString());

                        var PropVisitor = $"{PropName}.{nameof(IConfig.Editable)}";
                        var old = ruleTemplate.GetValue(PropVisitor);
                        old &= ~Mask;
                        old ^= value << Helpers.GetLastBit((ulong) Mask);
                        ruleTemplate.SetValue(PropVisitor, old);
                    }
                    else
                    {
                        throw new ArgumentException(nameof(s));
                    }
                }
                else
                {
                    throw new ArgumentException(nameof(Key));
                }
            }
        );
        await _dbContext.SaveChangesAsync();
    }

    //    /**
    //     * 更新公司下全部权限组信息
    //     * User: CCY
    //     * Date: 2021/1/22
    //     * @param $company_id
    //     * @param $updateData
    //     * @return mixed
    //     */
    public ReturnStruct updateDataByCompanyId( /*$company_id, $updateData*/)
    {
        //        return ruleTemplateModel::query()->CompanyId($company_id)->update($updateData);
        return ReturnArr();
    }

    /// <summary>
    /// 复制权限数据
    /// </summary>
    /// <param name="id"></param>
    /// <param name="oemId"></param>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public async Task<Model> CopyRule(int id, int oemId, int companyId)
    {
        var rule = await _dbContext.RuleTemplate.AsNoTracking().FirstOrDefaultAsync(a => a.ID == id);
        if (rule == null)
        {
            throw new Exception("复制权限数据是没有查询到权限信息");
        }

        var newRule = rule;
        newRule.ID = 0;
        newRule.OEMID = oemId;
        newRule.CompanyID = companyId;
        _dbContext.Add(newRule);
        if (await _dbContext.SaveChangesAsync() == 0)
        {
            throw new Exception("复制权限数据失败");
        }


        return rule;
    }

    public async Task<bool> SetUserAuthData(AuthInfo vm)
    {
        

        var orderModuleMenus = _menuCache.ListClone()?.OrderBy(i => i.ParentId).ThenBy(i => i.Sort);
        if (orderModuleMenus is null)
        {
            throw new Exception("菜单缓存异常");
        }
        // var rule = new RuleTemplateModel(orderModuleMenus);
        var claims = await _dbContext.UserClaims.Where(m => m.UserId == vm.ID).ToListAsync();
        
        Model rule = new RuleTemplateModel();
        foreach (var item in claims)
        {
            rule.SetValue(item.ClaimType,Convert.ToInt64(item.ClaimValue));
        }

        var ul = Convert.ToInt64(Flag.BASE);
        foreach (var item in orderModuleMenus)
        {
            rule.SetValue(item.Module,ul);
        }
        
        foreach (var propertie in rule.GetType().GetProperties())
        {
            if (propertie is not null)
            {
                var attribute = propertie.GetCustomAttributes(typeof(RULE), false).FirstOrDefault() as RULE;
                if (attribute is not null && attribute.Type == Types.CONFIG)
                {
                    var propName = propertie.Name;
                    var visible = $"{propName}.{nameof(IConfig.Visible)}";
                    
                    rule.SetValue(visible, ul);
                }
            }
        }

        //重置权限字段
        vm.Data.ForEach(i => rule.SetValue(i.Module, rule.GetValue(i.Module) | i.PermissionFlag));

        var newClaims = rule.GetClaims(_sessionProvider.Session.GetOEMID());

        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            await _dbContext.UserClaims.Where(m => m.UserId == vm.ID).DeleteAsync();
            _dbContext.UserClaims.AddRange(newClaims.Select(m => new UserClaim(vm.ID, m)));
            await transaction.CommitAsync();
            return await _dbContext.SaveChangesAsync() > 0;
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            Console.WriteLine(e.Message);
            throw new Exception(e.Message);
        }
    }
}