using ERP.Data;
using Microsoft.EntityFrameworkCore;

namespace ERP.Services.DB.Users;
using Model = Models.DB.Users.Company;

public class CompanyNoSessionService : BaseService
{
    protected readonly DBContext _dbContext;
    
    public CompanyNoSessionService(DBContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task<Model> GetInfoById(int id)
    {
        var company = await _dbContext.Company.SingleOrDefaultAsync(m => m.ID == id);
        if (company == null) throw new Exception($"未找到[id:{id}]的公司");
        return company;
    }
}