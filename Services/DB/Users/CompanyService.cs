using Common.Enums.Products;
using ERP.DomainEvents;
using ERP.DomainEvents.Company;
using ERP.Exceptions;
using ERP.Exceptions.Users;
using ERP.Export.V4_Export.ExcelTemplates;
using ERP.Services.Statistics;
using ERP.ViewModels.Statistics;
using MediatR;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;
using Exception = System.Exception;

namespace ERP.Services.DB.Users;

using Extensions;
using Interface;
using Models.DB.Users;
using Models.View.Information;
using Models.View.User.Referer;
using Models.View.User.User;
using Services.DB.Statistics;
using Services.DB.Users;
using CompanyCache = Caches.Company;
using Model = Models.DB.Users.Company;
using OemCache = Caches.OEM;
using PackageModel = Models.DB.Users.Package;
using UserValidityModel = Models.DB.Users.UserValidity;
using static Models.DB.Users.Company;
using Interface.Services;
using ERP.Data;

public class CompanyService : CompanyNoSessionService, ICompanyService
{
    // private readonly DBContext _dbContext;
    private readonly UserService _userService;
    private readonly ISessionProvider _sessionProvider;
    private readonly CompanyCache _companyCache;
    private readonly OemCache _oemCache;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly Statistic _statisticService;
    private readonly StatisticMoney _statisticMoney;
    private readonly IMediator _mediator;

    public CompanyService(
        DBContext myDbContext,
        UserService userService,
        ISessionProvider session,
        CompanyCache companycache,
        OemCache oemCache,
        IHttpContextAccessor httpContextAccessor, Statistic statisticService, IMediator mediator) : base(myDbContext)
    {
        // _dbContext = myDbContext;
        _userService = userService;
        _sessionProvider = session;
        _companyCache = companycache;
        _oemCache = oemCache;
        _httpContextAccessor = httpContextAccessor;
        _statisticService = statisticService;
        _mediator = mediator;
        _statisticMoney = new StatisticMoney(_statisticService);
    }

    private ISession Session
    {
        get => _sessionProvider.Session!;
    }

    private int CompanyID
    {
        get => Session.GetCompanyID();
    }

    private int Level
    {
        get => Session.GetLevel();
    }

    private int UserID
    {
        get => Session.GetUserID();
    }

    private int OEMID
    {
        get => Session.GetOEMID();
    }

    private int GroupID
    {
        get => Session.GetGroupID();
    }

    //public async Task<List<Model>> Load()
    //{
    //    return await _dbContext.Company.AsNoTracking().ToListAsync();
    //}


    /// <summary>
    /// 公司自用钱包支出扣款(调用此方法需要事务)
    /// </summary>
    /// <param name="Money"></param>
    /// <returns></returns>
    
    [Obsolete("弃用",true)]
    public async Task<bool> ExpenseWallet(decimal Money,Model company)
    {
        // var Company = _dbContext.Company.SingleOrDefault(x => x.ID == CompanyID);
        // if (Company != null)
        // {
            company.PrivateWallet -= Money;

            #region 自用钱包跟踪

            await _statisticMoney.SelfWalletReport(company);

            #endregion

            _dbContext.Update(company);
        // }

        return await _dbContext.SaveChangesAsync() > 0;
    }

    [Obsolete("弃用",true)]
    public async Task<bool> ExpensePublicWallet(decimal Money, int CompanyId)
    {
        var Company = _dbContext.Company.SingleOrDefault(x => x.ID == CompanyID);
        if (Company != null)
        {
            Company.PublicWallet -= Money;
            _dbContext.Update(Company);
        }

        return await _dbContext.SaveChangesAsync() > 0;
    }

    /// <summary>
    /// 余额转换积分
    /// </summary>
    /// <param name="integral"></param>
    /// <param name="balance"></param>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public Task<int> RechargeIntegral(int integral, decimal balance, int companyId)
    {
        return _dbContext.Company.Where(c => c.ID == companyId)
            .UpdateFromQueryAsync(c => new Model()
            {
                Integral = c.Integral + integral,
                Balance = c.Balance - balance,
            });
    }

    public async Task<bool> ChangeOpenStoreNum(Model model)
    {
        _dbContext.Company.Update(model);
        return await _dbContext.SaveChangesAsync() > 0;
    }

    /// <summary>
    /// 积分扣除
    /// </summary>
    /// <param name="integral"></param>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public Task<int> ExpenseIntegral(int integral, int companyId)
    {
        return _dbContext.Company.Where(c => c.ID == companyId)
            .UpdateFromQueryAsync(c => new Model()
            {
                Integral = c.Integral - integral,
            });
        ;
    }

    public async Task<Model> InitCompany(Models.View.User.User.Company dto, DateTime showValidity)
    {
        var companyName = dto.CompanyName.IsNotWhiteSpace() ? dto.CompanyName : dto.TrueName;
        var openStoreNum = dto.IsOpenStore == 1 ? 0 : dto.OpenStoreNum;
        var openUserNum = dto.IsOpenUser == 1 ? 0 : dto.OpenUserNum;

        // 获取上级公司扣费配置
        var parentCompanyId = CompanyID;
        string? priceConfig = null;
        var parentCompanyInfo = await GetInfoById(parentCompanyId);
        if (parentCompanyInfo is null)
        {
            return new();
        }

        var currentOem = _oemCache.Get(OEMID);
        if (currentOem != null && currentOem.Title.Contains("财付通") && parentCompanyInfo?.ShowOemId != 1)
        {
            if (parentCompanyInfo?.DefaultPriceConfig is not null)
            {
                priceConfig = parentCompanyInfo.DefaultPriceConfig;
            }
        }

        var ip = _httpContextAccessor.HttpContext!.GetIPv4();
        var insertData = new Model
        {
            Balance = dto.Balance,
            ParentCompanyId = CompanyID,
            Name = companyName,
            IsOpenStore = dto.IsOpenStore == 1,
            OpenStoreNum = openStoreNum,
            IsOpenUser = dto.IsOpenUser == 1,
            OpenUserNum = openUserNum,
            CreatedAt = DateTime.Now,
            IsDistribution = dto.IsDistribution,
            OEMID = dto.OemId,
            ShowOemId = dto.OemId,
            Validity = showValidity,
            IsNegative = dto.IsNegative == 1,
            State = States.ENABLE,
            CreatIp = ip,
            CreatIpHashCode = Helpers.CreateHashCode(ip),
            PriceConfig = priceConfig,
        };
        // 积分性质
        // BIT #12 of 积分余额 权限
        //$IntegralBill = $this->getSession()->getRule()->getSetting()->getBiter()->hasIntegralBalance();
        if (dto.Source > 0 && Session.GetSource() == Sources.ONE /*&& IntegralBill*/)
        {
            insertData.Source = (Sources)dto.Source;
        }
        else
        {
            insertData.Source = Session.GetSource();
        }

        // 账户类型 1业务用户 2注册免费用户 3个人版用户 4团队版用户 5企业版用户
        insertData.Type = insertData.Source == Sources.ONE ? Types.ONE : Types.TWO;
        int reportId = 0;
        string reportPids = "[]";
        if (dto.IsDistribution)
        {
            insertData.DistributionPurchase = DistributionPurchases.TRUE;
            insertData.DistributionWaybill = DistributionWaybills.TRUE;
            if (Session.GetIsDistribution())
            {
                reportId = Session.GetReportId();
                reportPids = Session.GetReportPids();
                //同步费率设置
                //查找当前用户公司费率信息
                var company = await GetInfoById(CompanyID);
                if (company != null)
                {
                    insertData.PurchaseRate = company.PurchaseRate;
                    insertData.PurchaseRateType = company.PurchaseRateType;
                    insertData.PurchaseRateNum = company.PurchaseRate;
                    insertData.PurchaseRateUnit = company.PurchaseRateUnit;
                    insertData.WaybillRate = company.WaybillRate;
                    insertData.WaybillRateType = company.WaybillRateType;
                    insertData.WaybillRateNum = company.WaybillRateNum;
                    insertData.WaybillRateUnit = company.WaybillRateUnit;
                }
                else
                {
                    throw new Exception("没有找到该公司");
                }
            }
            else
            {
                reportId = CompanyID;
                var pids = JsonConvert.DeserializeObject<List<int>>(Session.GetReportPids());
                pids?.Add(reportId);
                reportPids = JsonConvert.SerializeObject(pids);
            }
        }
        else
        {
            insertData.DistributionPurchase = DistributionPurchases.FALSE;
            insertData.DistributionWaybill = DistributionWaybills.FALSE;
            if (!Session.GetIsDistribution())
            {
                var pids = JsonConvert.DeserializeObject<List<int>>(Session.GetReportPids());
                pids?.Add(CompanyID);
                reportPids = JsonConvert.SerializeObject(pids);
            }
        }

        insertData.ReportId = reportId;
        insertData.ReportPids = reportPids;

        //公司创建过程
        var companyPids = JsonConvert.DeserializeObject<List<int>>(Session.GetCompanyPids());
        companyPids?.Add(CompanyID);
        insertData.Pids = JsonConvert.SerializeObject(companyPids);

        //var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            await _dbContext.Company.AddAsync(insertData);
            if (await _dbContext.SaveChangesAsync() == 0)
            {
                throw new Exception("添加失败");
            }

            _companyCache.Add(insertData);
            //await transaction.CommitAsync();


            #region 统计-当前公司数
            var companyCreatedEvent = new CompanyCreatedEvent(insertData,await _userService.GetInfoById(Session.GetUserID()));
            await _mediator.Publish(companyCreatedEvent);
            #endregion

            if (dto.IsDistribution == false)
            {
                insertData.ReportId = insertData.ID;
                _dbContext.Company.Update(insertData);
                if (await _dbContext.SaveChangesAsync() == 0)
                {
                    throw new Exception("保存数据库失败");
                }

                _companyCache.Add(insertData);
            }

            //await transaction.CommitAsync();
        }
        catch (Exception e)
        {
            //await transaction.RollbackAsync();
            Console.WriteLine(e.Message);
            throw new Exception("添加失败");
        }


        // if (await _dbContext.SaveChangesAsync() == 0)
        // {
        //     throw new Exception("添加失败");
        // };
        // if (dto.IsDistribution == false)
        // {
        //     insertData.ReportId = insertData.ID;
        //     _dbContext.Company.Update(insertData);
        //     if (await _dbContext.SaveChangesAsync() == 0)
        //     {
        //         throw new Exception("保存数据库失败");
        //     };
        // }
        return insertData;
    }

    /// <summary>
    /// 查找以当前公司为上报id的数量
    /// </summary>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public async Task<int> GetReportIdCompany(int companyId = 0)
    {
        if (companyId == 0) companyId = CompanyID;
        return await _dbContext.Company
            .Where(a => a.State == States.ENABLE)
            .Where(a => a.ReportId == companyId)
            .Where(a => a.ID != companyId)
            .CountAsync();
    }


    public async Task<bool> UpdateValidity(DateTime validity, int companyId)
    {
        var company = await _dbContext.Company.FirstOrDefaultAsync(a => a.ID == companyId);
        if (company == null) throw new Exception($"未找到[id:{companyId}]的公司");
        company.Validity = validity;
        _dbContext.Company.Update(company);
        _companyCache.Add(company);
        await _dbContext.SaveChangesAsync();
        return true;
    }


    public async Task<bool> UpdateCompanyConfig(SubConfigVM vm)
    {
        var company = await _dbContext.Company.FirstOrDefaultAsync(a => a.ID == vm.ID);
        if (company == null)
        {
            throw new Exception("未查询到此公司");
        }

        company.Name = vm.Name;
        company.IsChangeCompany = vm.IsChangeCompany;
        company.IsOpenStore = vm.IsOpenStore;
        company.OpenStoreNum = vm.OpenStoreNum;
        company.IsOpenUser = vm.IsOpenUser;
        company.OpenUserNum = vm.OpenUserNum;
        company.ShowOemId = vm.ShowOemId;
        company.UpdatedAt = DateTime.Now;
        if (CompanyID == 2 && vm.Discount != null)
        {
            company.Discount = (int)vm.Discount;
        }

        if (vm.IsDistribution == true)
        {
            company.IsNegative = vm.IsNegative;
            company.IsShowRate = vm.IsShowRate;
            company.PurchaseRate = vm.PurchaseRate;
            company.PurchaseRateType = vm.PurchaseRateType;
            company.PurchaseRateNum = vm.PurchaseRateNum;
            company.PurchaseRateUnit = vm.PurchaseRateUnit;
            company.WaybillRate = vm.WaybillRate;
            company.WaybillRateType = vm.WaybillRateType;
            company.WaybillRateNum = vm.WaybillRateNum;
            company.WaybillRateUnit = vm.WaybillRateUnit;
        }

        ///??? 暂时忽略
        //if ($this->getSession()->getOemId() != 1){
        //    foreach ($requestData['price_config'] as $k => $v){
        //        if ($v > 0){
        //            $requestData['price_config'][$k] = bcdiv($v, 1000, 6);
        //        }
        //    }

        //    $updateData['price_config'] = ($requestData['price_config']);
        //}

        //ERP3.0 注释掉了
        //积分性质
        //        $IntegralBill = $this->getSession()->getRule()->getSetting()->getBiter()->hasIntegralBalance();
        //        if (isset($requestData['source']) && $IntegralBill && $this->getSession()->getSource() == companyModel::SOURCE_ONE) {
        //            $updateData['source'] = $requestData['source'];
        //            //账户类型 1业务用户 2注册免费用户 3个人版用户 4团队版用户 5企业版用户
        //            if ($updateData['source'] == companyModel::SOURCE_ONE) {
        //                $updateData['type'] = companyModel::TYPE_ONE;
        //            } else {
        //                $company = $this->getCompany($id);
        //                if ($company['source'] == companyModel::SOURCE_ONE) {
        //                    $updateData['type'] = companyModel::TYPE_TWO;
        //                }
        //            }
        //        }
        _dbContext.Company.Update(company);
        if (await _dbContext.SaveChangesAsync() == 0)
        {
            throw new Exception("公司配置保存数据库失败");
        }

        _companyCache.Add(company);
        return true;
    }

    /************************************* 公司配置 ****************************************************/

    public async Task<string> GetrefererCode()
    {
        var company = await GetCurrentConfig(CompanyID);

        var key = "C" +Helpers.From10to62(company.ID);

        //推广码，推广链接
        var refererCode = "";
        if (Level == 1)
        {
            refererCode = string.Concat(key, Sy.Security.MD5.Encrypt(key).AsSpan(8, 4));
        }
        return refererCode;
    }

    /// <summary>
    /// 获取当前公司信息
    /// </summary>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public virtual async Task<Model> GetCurrentConfig(int companyId = 0)
    {
        //$company_id == '' && $company_id = $this->getSession()->getCompanyId();
        if (companyId == 0)
        {
            companyId = CompanyID;
        }

        var company = await _dbContext.Company.SingleOrDefaultAsync(x => x.ID == companyId);
        if (company == null) throw new Exception($"未找到[id:{companyId}]的公司");
        //产品翻译
        if (company.ProductTranslation == 0)
            company.ProductTranslation = ProductTranslationEnum.Free;
        
        //产品描述编辑器
        if (company.ProductEditor == 0)
            company.ProductEditor = DescriptionTypeEnum.PlainText;
        
        return company;
    }

    /// <summary>
    /// 更新当前公司配置信息
    /// </summary>
    /// <param name="c"></param>
    /// <returns></returns>
    public async Task<bool> UpdateCurrentConfig(Model requestData)
    {
        var company = await _dbContext.Company.FirstOrDefaultAsync(a => a.ID == requestData.ID);
        if (company is null) throw new NullReferenceException($"未查询到ID为[{requestData.ID}]的公司");
        if (requestData.IsChangeCompany == true)
        {
            company.Name = requestData.Name;
        }

        if (requestData.IsDistribution == true)
        {
            company.DistributionPurchase = requestData.DistributionPurchase;
            company.DistributionWaybill = requestData.DistributionWaybill;
            company.DistributionOrder= requestData.DistributionOrder;
        }
        //产品翻译
        company.ProductTranslation= requestData.ProductTranslation;
        //产品描述编辑器
        company.ProductEditor= requestData.ProductEditor;
        company.ProductAudit = requestData.ProductAudit;
        company.RefundAudit = requestData.RefundAudit;
        company.PurchaseAudit = requestData.PurchaseAudit;
        company.BillAudit = requestData.BillAudit;
        company.AllowPhone = requestData.AllowPhone;
        company.WaybillConfirm = requestData.WaybillConfirm;

        //$currentOem = Cache::instance()->currentOem()->get();
        //var currentOem = Caches.OEM.Get();//???
        //if (is_array($currentOem) && strpos($currentOem['title'], '财付通') !== false && $this->getSession()->getOemId() != 1){
        //    foreach ($requestData['default_price_config'] as $k => $v){
        //        if ($v > 0){
        //            $requestData['default_price_config'][$k] = bcdiv($v, 1000, 6);
        //        }
        //    }

        //    $updateData['default_price_config'] = ($requestData['default_price_config']);
        //}else
        //{
        //    $updateData['default_price_config'] = null;
        //}
        _dbContext.Company.Update(company);
        var count = await _dbContext.SaveChangesAsync();
        if (count > 0)
        {
            if (company.ID == _sessionProvider.Session.GetCompanyID())
            {
                //当前公司操作
                _httpContextAccessor.HttpContext.Session.SetCompanyFuncItem(company);
            }
            _companyCache.Add(company);
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// 余额自减
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="recharge"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    public async Task<bool> Charge(int companyId, decimal recharge)
    {
        var company = await _dbContext.Company.FirstOrDefaultAsync(a => a.ID == companyId);
        if (company == null) throw new Exception("未查询到公司");
        company.Balance -= recharge;
        _dbContext.Update(company);
        if (await _dbContext.SaveChangesAsync() == 0)
        {
            throw new Exception("余额自减失败");
        }

        ;
        return true;
    }

    /// <summary>
    /// 充值自增
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="recharge"></param>
    /// <returns></returns>
    public async Task<bool> ChargeAdd(int companyId, decimal recharge)
    {
        var company = await _dbContext.Company.FirstOrDefaultAsync(a => a.ID == companyId);
        if (company == null) throw new Exception("未查询到公司");
        company.Balance += recharge;
        _dbContext.Update(company);
        if (await _dbContext.SaveChangesAsync() == 0)
        {
            throw new Exception("充值自增失败");
        }

        ;
        return true;
    }
    
    public async Task<bool> ChargeSubtract(int companyId, decimal recharge)
    {
        var company = await _dbContext.Company.FirstOrDefaultAsync(a => a.ID == companyId);
        if (company == null) throw new Exception("未查询到公司");
        company.Balance -= recharge;
        _dbContext.Update(company);
        if (await _dbContext.SaveChangesAsync() == 0)
        {
            throw new Exception("扣费失败");
        }

        ;
        return true;
    }

    /// <summary>
    /// 获取下级公司业务统计用户信息
    /// </summary>
    /// <param name="currentCompanyId"></param>
    /// <param name="state"></param>
    /// <param name="company_name"></param>
    /// <returns></returns>
    public async Task<DbSetExtension.PaginateStruct<Model>> GetCompanyBusiness(int currentCompanyId, States? state,
        string? company_name)
    {
        // ->select(['id', 'company_name', 'state'])

        return await _dbContext.Company.Where(c => c.ParentCompanyId == currentCompanyId)
            .WhenWhere(state.HasValue, c => c.State == state)
            .WhenWhere(!string.IsNullOrEmpty(company_name), c => c.Name.StartsWith(company_name!))
            .OrderByDescending(c => c.ID)
            .ToPaginateAsync();
    }

    /// <summary>
    /// 绑定、解绑公司信息更新
    /// </summary>
    /// <param name="report_id"></param>
    /// <param name="report_pids"></param>
    /// <param name="is_distribution"></param>
    /// <param name="public_wallet"></param>
    /// <returns></returns>
    public async Task<int> InviteCode(int report_id, string report_pids, bool is_distribution,
        decimal public_wallet = 0)
    {
        var company = await _dbContext.Company.FirstOrDefaultAsync(a => a.ID == CompanyID);
        if (company != null)
        {
            company.ReportId = report_id;
            company.ReportPids = report_pids;
            company.IsDistribution = is_distribution;
            if (is_distribution == true)
            {
                //2020.7.27 15:20 董哥：绑定用户后，公司设置里的状态就变成了全部上报
                company.DistributionPurchase = DistributionPurchases.TRUE;
                company.DistributionWaybill = DistributionWaybills.TRUE;
            }
            else
            {
                company.DistributionPurchase = DistributionPurchases.FALSE;
                company.DistributionWaybill = DistributionWaybills.FALSE;
            }

            company.PublicWallet = public_wallet;
            await _dbContext.User.Where(u => u.CompanyID == CompanyID).UpdateFromQueryAsync(u =>
                new Models.DB.Identity.User() { IsDistribution = is_distribution });

            return await _dbContext.SaveChangesAsync();
        }

        return -1;
    }


    public async Task<Model?> GetLimitInfoById(int ID)
    {
        return await _dbContext.Company.Where(x => x.ID == ID).SingleOrDefaultAsync();
    }

    // public async Task<Model> GetInfoById(int id)
    // {
    //     var company = await _dbContext.Company.SingleOrDefaultAsync(m => m.ID == id);
    //     if (company == null) throw new Exception($"未找到[id:{id}]的公司");
    //     return company;
    // }

    public async Task<Model?> GetCompanyByName(string? name)
    {
        if (string.IsNullOrEmpty(name))
            return null;
        var company = await _dbContext.Company.SingleOrDefaultAsync(m => m.Name == name);
        return company;
    }

    public record CompanyValidityDto(int ID, DateTime ShowValidity, string ShowValidityInfo, Types Types, int Discount,
        Sources Source, DateTime? FirstRechargeDate,
        decimal Balance, int Integral, bool IsAgency, string? ReferrerKwSign
    );

    /// <summary>
    /// 获取公司对外有效期
    /// </summary>
    /// <param name="Validity"></param>
    /// <returns></returns>
    public async Task<List<Model>> GetCompanyValidity(ValidityCompany Validity) //,List<int> companyIds,bool IsAgency=false
    {
        //.Select(x => new CompanyValidityDto(x.ID, x.ShowValidity, x.ShowValidityInfo, x.Type, x.Discount,
        //       x.Source, x.FirstRechargeDate, x.Balance, x.Integral, x.IsAgency, x.ReferrerKwSign))
        //.WhenWhere(CompanyID != 0, c => c.ParentCompanyId == CompanyID)
        return await _dbContext.Company
            .WhenWhere(Validity.IsAgency.IsNotNull(),
                c => c.IsAgency == (Validity.IsAgency == IsAgencyEnum.No ? false : true))
            .WhenWhere(Validity.CompanyIds != null && Validity.CompanyIds.Count > 0,
                c => Validity.CompanyIds!.Contains(c.ID))
            .WhenWhere(Validity.Start != DateTime.MinValue, c => c.Validity >= Validity.Start)
            .WhenWhere(Validity.End != DateTime.MinValue, c => c.Validity <= Validity.End)
            .WhenWhere(Validity.Types != null,c => c.Type == Validity.Types)
            .OrderBy(x => x.State)
            .ToListAsync();
    }

    /// <summary>
    /// 获取正常使用的下级公司
    /// </summary>
    /// <returns></returns>
    public async Task<List<CompanyDto>> GetValidDistributionList(int companyId)
    {
        return await _dbContext.Company.Where(c => c.State == States.ENABLE
                                                   && c.IsDistribution
                                                   && c.ReportId == companyId
                                                   && c.ID != companyId)
                .Select(c => new CompanyDto(c.ID, c.Name))
                .ToListAsync()
            ;
    }

    public IQueryable<Model> GetValidDistributionListQuery(int companyId)
    {
        return _dbContext.Company.Where(c => c.State == States.ENABLE
                                             && c.IsDistribution
                                             && c.ReportId == companyId
                                             && c.ID != companyId);
    }

    /****************************** 开放注册 **********************************************/
    public async Task<Model> InitRegisterCompany(
        SubInfoDTO vm,
        PackageModel registerInfo,
        int showValidityId,
        DateTime validity,
        int referrerId)
    {
        var ip = _httpContextAccessor.HttpContext!.GetIPv4();
        var insertData = new Model
        {
            ParentCompanyId = registerInfo.CompanyID,
            Name = vm.CompanyName.IsNotWhiteSpace() ? vm.CompanyName! : vm.UserName,
            ReportPids = registerInfo.Pids,
            State = States.ENABLE,
            IsChangeCompany = true,
            IsOpenStore = true,
            OpenStoreNum = 0,
            IsOpenUser = false,
            OpenUserNum = registerInfo.OpenUserNum,
            IsDistribution = false,
            Type = Types.TWO,
            Source = Sources.TWO,
            SecondPackageOem = registerInfo.SecondPackageOem,
            Integral = registerInfo.GiveIntegral,
            ReferrerKwSign = vm.KwCode.IsNotWhiteSpace()
                ? System.Web.HttpUtility.UrlDecode(vm.KwCode, System.Text.Encoding.UTF8)
                : string.Empty,
            OEMID = registerInfo.OEMID,
            ShowOemId = registerInfo.OEMID,
            Validity = validity,
            ReferrerId = referrerId,
            CreatIp = ip,
            CreatIpHashCode = Helpers.CreateHashCode(ip),
            CreatedAt = DateTime.UtcNow,
        };
        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            await _dbContext.Company.AddAsync(insertData);
            if (await _dbContext.SaveChangesAsync() == 0)
            {
                throw new Exception("初始化公司失败");
            }

            _companyCache.Add(insertData);

            #region 统计-当前公司数

            await _statisticService.IncCompanyCount(_sessionProvider.Session, insertData);

            #endregion

            await transaction.CommitAsync();
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            Console.WriteLine(e.Message);
            throw new Exception(e.Message);
        }

        return insertData;
    }

    /// <summary>
    /// 获取推广注册用户
    /// </summary>
    /// <returns></returns>
    public async Task<object?> GetRefererRegister()
    {
        return await _dbContext.Company
            .Where(m => m.ReferrerId == CompanyID)
            .Where(a => a.State == States.ENABLE)
            .Select(a => new
            {
                name = a.Name
            }).ToListAsync();
    }

    /// <summary>
    /// 获取存在注册完用户推荐人公司id
    /// </summary>
    /// <returns></returns>
    public Task<DbSetExtension.PaginateStruct<int>> GetReferrer()
    {
        return _dbContext.Company.Where(c => c.ReferrerId != 0 && c.State == States.ENABLE)
            .GroupBy(c => c.ReferrerId)
            .Select(c => c.Key)
            .ToPaginateAsync();
    }

    public async Task<int> UpdateCompanyById(Model company)
    {
        _dbContext.Company.Update(company);
        await _dbContext.SaveChangesAsync();
        _companyCache.Add(company);
        return 1;
    }

    /// <summary>
    /// 获取指定公司信息
    /// </summary>
    /// <param name="companyIds"></param>
    /// <param name="state"></param>
    /// <param name="isAgency"></param>
    /// <returns></returns>
    public async Task<List<Model>> GetList(IEnumerable<int> companyIds, States state = States.ENABLE,
        bool isAgency = false)
    {
        return await _dbContext.Company
            .Where(c => companyIds.Contains(c.ID) && c.State == state && c.IsAgency == isAgency)
            .ToListAsync();
    }
    
    public async Task<List<Model>> GetListAll(IEnumerable<int> companyIds)
    {
        return await _dbContext.Company
            .Where(c => companyIds.Contains(c.ID))
            .ToListAsync();
    }

    /// <summary>
    /// 获取推荐公司信息
    /// </summary>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public async Task<DbSetExtension.PaginateStruct<Model>> GetRefererAllList(int companyId)
    {
        return await _dbContext.Company.Where(c => c.ReferrerId == companyId && c.State == States.ENABLE)
            .ToPaginateAsync();
    }

    /// <summary>
    /// 获取推荐公司信息
    /// </summary>
    /// <param name="companyName"></param>
    /// <param name="companyIds"></param>
    /// <param name="companyId"></param>
    /// <param name="isAgency"></param>
    /// <returns></returns>
    public async Task<DbSetExtension.PaginateStruct<Model>> GetRefererList(string? companyName, List<int>? companyIds,
        int companyId, bool? isAgency)
    {
        return await _dbContext.Company
            .WhenWhere(companyId > 0, c => c.ReferrerId == companyId)
            .WhenWhere(companyIds is not null && companyIds.Any(), c => companyIds!.Contains(c.ID))
            .WhenWhere(isAgency is not null, c => c.IsAgency == isAgency)
            .WhenWhere(companyName.IsNotWhiteSpace(), c => c.Name == companyName)
            .Where(c => c.State == States.ENABLE)
            .Where(c => c.Source == Sources.TWO)
            .OrderByDescending(c => c.ID)
            //.Select(c => new
            //{
            //    c.ID,
            //    c.CompanyName,
            //    c.Type,
            //    c.ShowValidity,
            //    c.CreatedAt,
            //    c.OEMID,
            //    balance_cny = c.Balance,
            //    c.Discount,
            //    c.ReferrerKwSign,
            //    c.FirstRechargeDate,
            //    c.IsOpenUser,
            //    c.OpenUserNum,
            //    c.IsAgency
            //})
            .AsNoTracking().ToPaginateAsync();
    }

    /// <summary>
    /// 公司自用钱包充值(调用此方法需要开启事务)
    /// </summary>
    /// <param name="money"></param>
    /// <param name="companyId"></param>
    /// <returns></returns>
    
    [Obsolete("弃用",true)]
    public async Task<bool> RechargeWallet(decimal money,Model company)
    {
        // if (companyId == null)
        // {
        //     companyId = _sessionProvider.Session!.GetCompanyID();
        // }

        // var info = await _dbContext.Company.Where(m => m.ID == companyId).FirstOrDefaultAsync();
        //
        // if (info == null)
        // {
        //     return false;
        // }

        company.PrivateWallet += money;

        #region 自用钱包跟踪

        await _statisticMoney.SelfWalletReport(company);

        #endregion

        _dbContext.Company.Update(company);
        _companyCache.Add(company);
        return await _dbContext.SaveChangesAsync() > 0;
    }

    /// <summary>
    /// 更新独立用户用户信息同步更新公司信息
    /// </summary>
    /// <param name="id"></param>
    /// <param name="ruleId"></param>
    /// <param name="state"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    public async Task<int> UpdateSync(int id, States state)
    {
        //
        // $updateData = [
        // 'rule_id' => $rule_id,
        // 'state'   => $state
        //     ];
        // return $this->companyModel::query()
        //     ->where('id', $id)
        //     ->update($updateData);
        var info = await _dbContext.Company.Where(m => m.ID == id).FirstOrDefaultAsync();

        if (info is null)
        {
            throw new Exception("找不到相关数据");
        }

        if (info.State != state)
        {
            await _statisticService.Reject(new DataBelongDto(info), Statistic.NumColumnEnum.CompanyReject,
                state == States.DISABLE);
        }
        

        info.State = state;
        

        _dbContext.Update(info);

        return await _dbContext.SaveChangesAsync();
    }

    public async Task<List<int>> GetSubCompanyIds(List<int> pids,List<int> subCompanyIds)
    {
        if (_sessionProvider?.Session?.GetCompanyID() != Enums.ID.Company.XiaoMi.GetNumber())
        {
            throw new PermissionException("无权操作#1");
        }
        
        if (pids.Contains(Enums.ID.Company.XiaoMi.GetNumber()))
        {
            throw new Exceptions.Exception("因米境通公司下级数量过多，无法查询#2");
        }
        var tempCompanyIds = await _dbContext.Company
            .Where(m => pids.Contains(m.ParentCompanyId))
            .Select(m => m.ID)
            .ToListAsync();

        if (tempCompanyIds.Count > 0)
        {
            foreach (var tempCompanyId in tempCompanyIds)
            {
                subCompanyIds.Add(tempCompanyId);
            }
            await GetSubCompanyIds(tempCompanyIds, subCompanyIds);
        }

        return subCompanyIds;
    }
}