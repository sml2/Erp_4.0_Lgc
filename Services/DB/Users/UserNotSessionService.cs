using ERP.Data;
using ERP.Models.DB.Identity;
using ERP.Models.DB.Users;
using ERP.Services.DB.Statistics;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace ERP.Services.DB.Users;

public class UserNotSessionService
{
    private readonly DBContext _dbContext;
    private readonly Statistic _statisticsService;

    public UserNotSessionService(DBContext dbContext, Statistic statisticsService)
    {
        _dbContext = dbContext;
        _statisticsService = statisticsService;
    }


    /// <summary>
    /// Save User Claims
    /// </summary>
    /// <param name="id"></param>
    /// <param name="userId"></param>
    /// <param name="oemId"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    public async Task<bool> SaveUserClaims(int id, int userId,int oemId)
    {
        var ruleTemplate = await _dbContext.RuleTemplate.AsNoTracking().FirstOrDefaultAsync(a => a.ID == id);
        if (ruleTemplate == null)
        {
            throw new Exception("没有查询到权限信息");
        }

        await _dbContext.UserClaims.Where(m => m.UserId == userId).DeleteAsync();

        var claims = ruleTemplate.GetClaims(oemId);

        _dbContext.UserClaims.AddRange(claims.Select(m => new UserClaim(userId, m)));

        return await _dbContext.SaveChangesAsync() > 0;
    }
    
    /// <summary>
    /// 更新独立账户用户公司id
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public async Task<User> UpdateUserCompanyId(User user, int companyId)
    {
        //var user = await _dbContext.User.FirstOrDefaultAsync(a => a.ID == userId);
        if (user == null)
        {
            throw new Exception("更新用户公司失败!");
        }

        //todo 待定
        user.CompanyID = companyId;
        _dbContext.User.Update(user);
        if (await _dbContext.SaveChangesAsync() == 0)
        {
            throw new Exception("更新用户公司数据库失败!");
        }
        return user;
    }
    
    /// <summary>
    /// 根据用户id获取用户信息
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    public async Task<User> GetInfoById(int id)
    {
        var user = await _dbContext.User.FirstOrDefaultAsync(a => a.Id == id);
        if (user == null)
        {
            throw new Exception("未找到此用户");
        }

        return user;
    }
    
    public async Task<Company> GetCompanyInfoById(int id)
    {
        var company = await _dbContext.Company.SingleOrDefaultAsync(m => m.ID == id);
        if (company == null) throw new Exception($"未找到[id:{id}]的公司");
        return company;
    }
    
    
    /// <summary>
    /// 获取权限模版详细信息
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<Models.DB.Users.RuleTemplate> GetRuleTemplateInfoById(int id)
    {
        var ruleTemplate = await _dbContext.RuleTemplate.FirstOrDefaultAsync(m => m.ID == id);
        if (ruleTemplate == null) throw new Exception($"未找到[ID:{id}]的角色模板");
        return ruleTemplate;
    }

    
    /// <summary>
    /// 复制权限数据
    /// </summary>
    /// <param name="id"></param>
    /// <param name="oemId"></param>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public async Task<Models.DB.Users.RuleTemplate> CopyRule(int id, int oemId, int companyId)
    {
        var rule = await _dbContext.RuleTemplate.AsNoTracking().FirstOrDefaultAsync(a => a.ID == id);
        if (rule == null)
        {
            throw new Exception("复制权限数据是没有查询到权限信息");
        }

        var newRule = rule;
        newRule.ID = 0;
        newRule.OEMID = oemId;
        newRule.CompanyID = companyId;
        _dbContext.Add(newRule);
        if (await _dbContext.SaveChangesAsync() == 0)
        {
            throw new Exception("复制权限数据失败");
        }


        return rule;
    }
    
    /// <summary>
    /// 修改存储模块
    /// </summary>
    /// <param name="id"></param>
    /// <param name="ruleId"></param>
    /// <param name="requestData"></param>
    /// <param name="companyId"></param>
    /// <param name="oemId"></param>
    /// <returns></returns>
    public async Task SaveRuleTemplate(int id, int ruleId, Models.DB.Users.RuleTemplate requestData, int companyId = 0, int oemId = 0)
    {
        Models.DB.Users.RuleTemplate? ruleTemplate;
        if (id > 0) //修改
        {
            ruleTemplate = await _dbContext.RuleTemplate.FirstOrDefaultAsync(a => a.ID == id);
            if (ruleTemplate == null)
            {
                throw new Exception("修改存储模块错误");
            }

            ruleTemplate.Name = requestData.Name;
            ruleTemplate.Remark = requestData.Remark;
            ruleTemplate.State = requestData.State;
            _dbContext.RuleTemplate.Update(ruleTemplate);
        }
        else //添加
        {
            await _dbContext.RuleTemplate.AddAsync(requestData);
        }

        await _dbContext.SaveChangesAsync();
    }
    
    /// <summary>
    /// 修改用户组信息
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public async Task<Group> SaveGroupInfo(Models.DB.Users.Group data)
    {
        if (data.ID > 0)//修改
        {
            //var group = await _dbContext.UserGroup.FirstOrDefaultAsync(a => a.ID == data.ID);
            //if (group.IsNull() || data.UpdatedAt.Date != group!.UpdatedAt.Date)
            //{
            //    return ReturnArr(false, "用户组信息已变更", 0);
            //}

            _dbContext.UserGroup.Update(data);
            if (await _dbContext.SaveChangesAsync() == 0)
            {
                throw new Exception("用户组信息错误");
            }
        }
        else//添加
        {
            // var transaction = await _dbContext.Database.BeginTransactionAsync();
            try
            {
                data.Sort = - DateTime.Now.GetTimeStamp();
                await _dbContext.UserGroup.AddAsync(data);
                await _dbContext.SaveChangesAsync();
                //group
                await _statisticsService.CheckOrAdd(data);
                // await transaction.CommitAsync();
            }
            catch (Exception e)
            {
                // await transaction.RollbackAsync();
                Console.WriteLine(e.Message);
                throw new Exception(e.Message);
            }
            
        }

        return data;
    }
    
    public async Task<bool> AddBalanceBillWithRecharge(decimal balance, int companyId, int oemId,
        Models.DB.Identity.User user, decimal surplus = 0,
        Models.IntegralBalance.BalanceBill.Types type = Models.IntegralBalance.BalanceBill.Types.EXPENSE,
        string reason = "", string remark = "")
    {
        var model = new Models.IntegralBalance.BalanceBill()
        {
            CompanyID = companyId,
            OEMID = oemId,
            Type = type,
            Balance = balance,
            Surplus = surplus,
            Reason = reason,
            Remark = !string.IsNullOrEmpty(remark) ? remark : reason,
            Datetime = DateTime.Now,
            UserName = user.UserName,
            TrueName = user.TrueName,
            OperateUserId = user.Id,
            OperateCompanyId = companyId,

            BytrueName = String.Empty,
            ByuserName = String.Empty,
            ByUserId = 0,
            ByCompanyId = user.CompanyID,
            CreatedAt = DateTime.Now,
        };
        await  _dbContext.BalanceBills.AddAsync(model);
        return await _dbContext.SaveChangesAsync() > 0;
    }
}