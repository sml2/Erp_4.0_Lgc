using ERP.Interface;
using UserOperateLogModel = ERP.Models.DB.Users.UserOperateLog;
using ERP.Extensions;
using static ERP.Models.DB.Users.UserOperateLog;
using Microsoft.EntityFrameworkCore;
using PackageModel = ERP.Models.DB.Users.Package;
using ERP.Data;

namespace ERP.Services.DB.Users;
public class UserOperateLogService : BaseService
{
    private readonly DBContext _dbContext;
    private readonly ISessionProvider _SessionProvider;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private ISession Session { get => _SessionProvider.Session!; }
    public UserOperateLogService(DBContext dbContext, ISessionProvider SessionProvider, IHttpContextAccessor httpContextAccessor)
    {
        _dbContext = dbContext;
        _SessionProvider = SessionProvider;
        _httpContextAccessor = httpContextAccessor;
    }

    //    private $userOperateLogModel;

    //   public function  __construct()
    //    {
    //        parent::__construct();
    //        $this->userOperateLogModel = new UserOperateLogModel();
    //    }

    //    /**
    //     * 获取用户信息日志
    //     * User: CCY
    //     * Date: 2020/4/9 15:16
    //     * @param $user_id
    //     * @return mixed
    //     */
    public async Task<DbSetExtension.PaginateStruct<UserOperateLogModel>> GetLogPage(int userId)
    {
        return await _dbContext.UserOperateLog.Where(a => a.UserID == userId).ToPaginateAsync();
    }

    /// <summary>
    /// 添加用户信息日志
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="remark"></param>
    /// <param name="ext"></param>
    /// <returns></returns>
    public async Task<UserOperateLogModel> AddUserLog(int userId, string remark = "", string ext = "")
    {
        var log = new UserOperateLogModel
        {
            UserID = userId,
            OEMID = Session.GetOEMID(),
            CompanyID = Session.GetCompanyID(),
            GroupID = Session.GetGroupID(),
            OperateUserId = Session.GetUserID(),
            OperateTrueName = Session.GetTrueName(),
            OperateIp = _httpContextAccessor.HttpContext!.GetIPv4(),
            Type = Types.USER,
            Remark = remark,
            Ext = ext,
            CreatedAt = DateTime.Now,
        };
        await _dbContext.UserOperateLog.AddAsync(log);
        var count = await _dbContext.SaveChangesAsync();
        if (count > 0)
        {
            return log;
        }
        else
        {
            throw new Exception("用户信息日志添加失败");
        }
    }

    //    /**
    //     *添加公司日志
    //     * User: CCY
    //     * Date: 2020/4/9 16:13
    //     * @param $user_id
    //     * @param string $remark
    //     * @param string $ext
    //     * @return mixed
    //     */
    public ReturnStruct addCompanyLog(/*$user_id, $remark = '', $ext = ''*/)
    {
        //        $insertData = [
        //            'user_id'          => $user_id,
        //            'operate_user_id'  => $this->getSession()->getUserId(),
        //            'operate_truename' => $this->getSession()->getTruename(),
        //            'operate_ip'       => getIp(),
        //            'type'             => $this->userOperateLogModel::STYLE_COMPANY,
        //            'remark'           => $remark,
        //            'ext'              => $ext,
        //            'created_at'       => $this->datetime
        //        ];
        //        return $this->userOperateLogModel->insertGetId($insertData);
        return ReturnArr();
    }


    /****************************** 开放注册 **********************************************/

    //    /**
    //     *注册日志
    //     * User: CCY
    //     * Date: 2020/11/18
    //     * @param $user_id
    //     * @param $registerInfo
    //     * @return mixed
    //     */
    public async Task<bool> AddRegisterUserLog(int userId, PackageModel registerInfo)
    {
        //        $insertData = [
        //            'user_id'          => $user_id,
        //            'operate_user_id'  => $registerInfo['uid'],
        //            'operate_truename' => $registerInfo['truename'],
        //            'operate_ip'       => getIp(),
        //            'type'             => $this->userOperateLogModel::STYLE_USER,
        //            'remark'           => '用户注册成功',
        //            'ext'              => '',
        //            'created_at'       => $this->datetime
        //        ];
        //        return $this->userOperateLogModel->insertGetId($insertData);
        var insertData = new UserOperateLogModel
        {
            UserID = userId,
            CompanyID = Session.GetCompanyID(),
            OEMID = Session.GetOEMID(),
            GroupID = Session.GetGroupID(),
            OperateUserId = registerInfo.Uid,
            OperateTrueName = registerInfo.TrueName,
            OperateIp = _httpContextAccessor.HttpContext!.GetIPv4(),
            Type = Types.USER,
            Remark= "用户注册成功",
            Ext = "",
            CreatedAt = DateTime.Now,
        };
        await _dbContext.UserOperateLog.AddAsync(insertData);
        if(await _dbContext.SaveChangesAsync() == 0)
        {
            throw new Exception("注册日志出错");
        };
        return true;
    }
}
