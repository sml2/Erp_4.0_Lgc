using ERP.Data;
using ERP.DomainEvents.Company;
using ERP.DomainEvents.Users;
using ERP.Enums.ID;
using ERP.Extensions;
using ERP.Models;
using ERP.Models.Abstract;
using ERP.Models.DB.Identity;
using ERP.Models.View.User.Register;
using ERP.Models.View.User.User;
using ERP.Services.Identity;
using MediatR;
using Newtonsoft.Json;
using Company = ERP.Models.View.User.User.Company;
using Role = ERP.Enums.Identity.Role;
using RuleTemplateModel = ERP.Models.DB.Users.RuleTemplate;
using UserGroupModel = ERP.Models.DB.Users.Group;

namespace ERP.Services.DB.Users;

using Models.DB.Users;
using UserModel = Models.DB.Identity.User;
using CompanyModel = Models.DB.Users.Company;

public class RegisterService : BaseService
{
    private readonly DBContext _dbContext;
    private readonly UserNotSessionService _userNotSessionService;
    private readonly Caches.OEM _oemCache;
    private readonly Caches.Company _companyCache;
    private readonly UserManager _userManager;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly IMediator _mediator;

    private const int RegisterRuleId = 248;
    private const int GroupId = 1;

    private static readonly int UserPid = Enums.Identity.User.XIAOMI.GetNumber();

    private readonly List<int> UserPids = new List<int>()
    {
        Enums.Identity.User.SU.GetNumber(),
        UserPid
    };

    private static readonly int CompanyPid = Enums.ID.Company.XiaoMi.GetNumber();

    private readonly List<int> CompanyPids = new List<int>()
    {
        Enums.ID.Company.Empty.GetNumber(),
        CompanyPid
    };

    public RegisterService(DBContext dbContext, Caches.OEM oemCache, UserManager userManager,
        IHttpContextAccessor httpContextAccessor, Caches.Company companyCache, IMediator mediator,
        UserNotSessionService userNotSessionService)
    {
        _dbContext = dbContext;
        _oemCache = oemCache;
        _userManager = userManager;
        _httpContextAccessor = httpContextAccessor;
        _companyCache = companyCache;
        _mediator = mediator;
        _userNotSessionService = userNotSessionService;
    }

    public async Task<bool> SubRegister(SubDto req, SmsCodeModel smsCodeModel)
    {
        var oemId = OEM.MiJingTong.GetNumber();
        var rule = await _userNotSessionService.GetRuleTemplateInfoById(RegisterRuleId);

        var ruleTemplate = new RuleTemplateModel
        {
            Name = "初始权限组",
            State = BaseModel.StateEnum.Open,
            OEMID = OEM.MiJingTong.GetNumber(),
            Remark = ""
        };
        var groupInsert = new UserGroupModel
        {
            Name = "初始用户组",
            State = BaseModel.StateEnum.Open,
            OEMID = oemId
        };


        // 启动事务
        using var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            //绑定推荐人公司id
            var referrerId = 0;
            if (!string.IsNullOrEmpty(req.Register.InviteCode))
            {
                var temp = req.Register.InviteCode.Substring(1, req.Register.InviteCode.Length - 1);
                referrerId = Helpers.From62to10(temp.Substring(0, 1));
                //检测有效推荐人
                await _userNotSessionService.GetCompanyInfoById(referrerId);
            }

            //添加独立账号
            var newUser = await InitRegisterUser(req.Register, oemId);
            //添加公司
            var newCompany = await InitRegisterCompany(req.Register, oemId, DateTime.Now.AddMonths(1));
            var newCompanyId = newCompany.ID;
            //更新用户公司id
            await _userNotSessionService.UpdateUserCompanyId(newUser, newCompanyId);
            //初始化权限模版 复制权限模板
            var newRule = await _userNotSessionService.CopyRule(rule.ID, oemId, newCompanyId);
            ruleTemplate.CompanyID = newCompanyId;
            newRule.Name = ruleTemplate.Name;
            newRule.State = ruleTemplate.State;
            await _userNotSessionService.SaveRuleTemplate(newRule.ID, newRule.ID, newRule, newCompanyId, oemId);

            //初始化用户组
            groupInsert.CompanyID = newCompanyId;
            var group = await _userNotSessionService.SaveGroupInfo(groupInsert);

            //SaveUserClaims
            await _userNotSessionService.SaveUserClaims(newRule.ID, newUser.ID, oemId);
            //用户统计 添加独立用户创建者公司计数
            var userCreatedEvent = new UserCreatedEvent(newUser,
                await _userNotSessionService.GetInfoById(Enums.Identity.User.XIAOMI.GetNumber()));

            await _mediator.Publish(userCreatedEvent);

            //使用code
            //使用短信验证码
            // smsCodeModel.Used = true;
            await _dbContext.SaveChangesAsync();
            await transaction.CommitAsync();
            return true;
        }
        catch (Exception e)
        {
            // 回滚事务
            await transaction.RollbackAsync();
            throw;
        }
    }

    private async Task<UserModel> InitRegisterUser(RegisterForm vm, int oemId)
    {
        var userInsertData = new UserModel()
        {
            UserName = vm.Username,
            TrueName = vm.Username,
            Password = vm.CheckPassword,
            Mobile = vm.Mobile,
            PhoneNumberConfirmed = true,
            FormRuleTemplateID = RegisterRuleId,
            GroupID = GroupId,
            Pid = UserPid,
            Pids = JsonConvert.SerializeObject(UserPids),
            State = BaseModel.StateEnum.AdminToDisable,
            Role = Role.ADMIN,
            IsDistribution = false,
            Sort = -DateTime.Now.GetTimeStamp(),
            Level = 1,
            OEMID = oemId,
            //公司初始化，稍后更改
            CompanyID = Enums.ID.Company.XiaoMi.GetNumber(),
            CreateCompanyId = Enums.ID.Company.XiaoMi.GetNumber(),
            ThemeId = _oemCache.Get(oemId)!.ThemeId.GetValueOrDefault(),
            OrderRateConfig = UserModel.OrderRateConfigs.REALTIME,
        };
        userInsertData.PasswordHash = _userManager.PasswordHasher.HashPassword(userInsertData, userInsertData.Password);
        var res = await _userManager.CreateAsync(userInsertData);

        if (res.Succeeded)
        {
            var ret = await _userManager.AddToRoleAsync(userInsertData, userInsertData.Role);
            if (!ret.Succeeded)
                throw new Exception(string.Join(",", ret.Errors.Select(e => e.Description)));
        }
        else
        {
            throw new Exception(string.Join(",", res.Errors.Select(e => e.Description)));
        }

        return userInsertData;
    }

    private async Task<Company> InitRegisterCompany(RegisterForm vm, int oemId, DateTime validity)
    {
        var companyName = string.IsNullOrEmpty(vm.CompanyName) ? vm.Username : vm.CompanyName;
        var parentCompanyId = CompanyPid;
        string? priceConfig = null;
        var parentCompanyInfo = await _userNotSessionService.GetCompanyInfoById(parentCompanyId);
        if (parentCompanyInfo is null)
        {
            throw new Exception("找不到上级公司信息");
        }

        var currentOem = _oemCache.Get(oemId);
        if (currentOem != null)
        {
            if (parentCompanyInfo?.DefaultPriceConfig is not null)
            {
                priceConfig = parentCompanyInfo.DefaultPriceConfig;
            }
        }

        var ip = _httpContextAccessor.HttpContext!.GetIPv4();

        var companyInsertData = new CompanyModel()
        {
            Balance = 0,
            ParentCompanyId = parentCompanyId,
            Name = companyName,
            IsOpenStore = false,
            OpenStoreNum = 10,
            IsOpenUser = false,
            OpenUserNum = 0,
            CreatedAt = DateTime.Now,
            IsDistribution = false,
            OEMID = oemId,
            ShowOemId = oemId,
            Validity = validity,
            IsNegative = false,
            State = Company.States.AdminToDisable,
            CreatIp = ip,
            CreatIpHashCode = Helpers.CreateHashCode(ip),
            PriceConfig = priceConfig,
            Source = Company.Sources.ONE,
            Type = Company.Types.TWO,
            DistributionPurchase = Company.DistributionPurchases.FALSE,
            DistributionWaybill = Company.DistributionWaybills.FALSE,
            Pids = JsonConvert.SerializeObject(CompanyPids),
            ReportPids = JsonConvert.SerializeObject(CompanyPids),
            ReferrerKwSign = vm.kwCode,
            ReferrerKwName = 0,
        };
        await _dbContext.Company.AddAsync(companyInsertData);
        if (await _dbContext.SaveChangesAsync() == 0)
            throw new Exception("注册公司初始化失败");
        _companyCache.Add(companyInsertData);

        #region 统计-当前公司数

        var companyCreatedEvent = new CompanyCreatedEvent(companyInsertData,
            await _userNotSessionService.GetInfoById(Enums.Identity.User.XIAOMI.GetNumber()));
        await _mediator.Publish(companyCreatedEvent);

        #endregion

        return companyInsertData;
    }
}