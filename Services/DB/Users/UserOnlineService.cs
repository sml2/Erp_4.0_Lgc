using ERP.Data;
using ERP.Exceptions;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.DB.Identity;
using ERP.Models.DB.Users;
using ERP.Models.Users;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using Z.EntityFramework.Plus;
using Exception = System.Exception;

namespace ERP.Services.DB.Users;

public class UserOnlineService : BaseWithUserService
{
    protected readonly IHttpContextAccessor _httpContext;
    protected readonly LoginLogService _loginLogService;
    private readonly UserManager<User> _userManager;
    private IDistributedCache _distributedCache;

    //Minutes
    private int _timeOut = 30;
    private string _onlineCacheKey = "UserOnlineInfo_";

    public UserOnlineService(DBContext dbContext, ISessionProvider sessionProvider, IHttpContextAccessor httpContext,
        LoginLogService loginLogService, UserManager<User> userManager, IDistributedCache distributedCache) : base(dbContext, sessionProvider)
    {
        _httpContext = httpContext;
        _loginLogService = loginLogService;
        _userManager = userManager;
        _distributedCache = distributedCache;
    }

    public async Task<UserOnlineModel?> GetInfoByUserId(int userId) =>
        await _dbContext.UserOnline.FirstOrDefaultAsync(m => m.UserId == userId);

    public async Task<UserOnlineModel?> GetInfoById(int id) =>
        await _dbContext.UserOnline.FirstOrDefaultAsync(m => m.ID == id);

    public async Task<UserOnlineModel?> AddOnline(User user, DateTime loginTime)
    {
        var model = new UserOnlineModel()
        {
            OemId = user.OEMID,
            UserId = user.Id,
            Username = user.UserName,
            SessionId = _httpContext.HttpContext!.User.GetSessionId()!,
            LoginIP = _httpContext.HttpContext.GetIPv4(),
            LoginTime = loginTime,
            LastTime = loginTime,
            CreatedAt = loginTime,
            UpdatedAt = loginTime,
        };

        await _dbContext.UserOnline.AddAsync(model);
        if (await _dbContext.SaveChangesAsync() > 0)
        {
            return model;
        }

        return null;
    }

    public async Task<int> DeleteOnline(UserOnlineModel m)
    {
        //删除缓存
        var cacheKey = $"{_onlineCacheKey}{m.UserId}";
        
        var userOnlineCache = await _distributedCache.GetStringAsync(cacheKey);
        var cacheData = string.IsNullOrEmpty(userOnlineCache) ? null : JsonConvert.DeserializeObject<UserOnlineModel>(userOnlineCache);
        if (cacheData != null)
        {
            await _distributedCache.RemoveAsync(cacheKey); 
        }

        _dbContext.UserOnline.Remove(m);
        return await _dbContext.SaveChangesAsync();
    }

    public async Task<int> DeleteOnline(List<int> ids)
    {
        return await _dbContext.UserOnline.Where(m => ids.Contains(m.ID)).DeleteAsync();
    }

    public async Task HandleOnline(User user, DateTime time)
    {
        // var onlines = _dbContext.UserOnline.AsEnumerable().Where(m => (time - m.LastTime).TotalMinutes > _timeOut).ToList();
        // 检测数据库是否存在已超时用户，且更新用户最后操作时间
        var needUpdateLastTimeModels = new List<UserOnlineModel>();
        // 不包含当前用户的全部在线数据
        var onlines = await _dbContext.UserOnline.Where(m => m.UserId != user.ID).ToListAsync();
        if (onlines.Count > 0)
        {
            foreach (var item in onlines)
            {
                var cacheKey = $"{_onlineCacheKey}{item.UserId}";
                var userOnlineCache = await _distributedCache.GetStringAsync(cacheKey);
                var cacheData = string.IsNullOrEmpty(userOnlineCache) ? null : JsonConvert.DeserializeObject<UserOnlineModel>(userOnlineCache);
                if (cacheData != null)
                {
                    var dbLiveTime = (time - item.LastTime).TotalMinutes;
                    var cacheLiveTime = (time - cacheData.LastTime).TotalMinutes;

                    if (dbLiveTime > _timeOut && cacheLiveTime > _timeOut)
                    {
                        await _distributedCache.RemoveAsync(cacheKey);
                        //长时间未操作 超时
                        await _loginLogService.TimeOutLogout(item, user);
                        await DeleteOnline(item);
                    }
                    else
                    {
                        item.LastTime = cacheData.LastTime;
                        needUpdateLastTimeModels.Add(item);
                    }
                }
                else
                {
                    //已经超时
                    await _loginLogService.TimeOutLogout(item, user);
                    await DeleteOnline(item);
                }
            }
        }

        _dbContext.UserOnline.UpdateRange(needUpdateLastTimeModels);
        await _dbContext.SaveChangesAsync();
        
        var currentUserOldOnline = await _dbContext.UserOnline.Where(m => m.UserId == user.ID).FirstOrDefaultAsync();
        if (currentUserOldOnline != null)
        {
            //挤掉
            await DeleteOnline(currentUserOldOnline); 
            //挤掉日志
            await _loginLogService.SqueezedOffLine(currentUserOldOnline);
        }
        //添加在线信息
        var m = await AddOnline(user, time);
        if (m == null)
        {
            throw new Exception("用户在线信息创建失败");
        }
        await _distributedCache.SetStringAsync($"{_onlineCacheKey}{user.ID}", JsonConvert.SerializeObject(m));
        //登陆日志
        await _loginLogService.Login(m, time);
        

        // await _loginLogService.TimeOutLogout(onlines, user);
        // await DeleteOnline(onlines.Select(m => m.ID).ToList());

        //是否存在在线数据
        // var oldOnlineInfo = await GetInfoByUserId(user.ID);
        // if (oldOnlineInfo != null)
        // {
        //     //挤掉
        //     await DeleteOnline(oldOnlineInfo);
        //     //挤掉日志
        //     await _loginLogService.SqueezedOffLine(oldOnlineInfo);
        // }
        //
        // //添加在线信息
        // var m = await AddOnline(user, time);
        // if (m == null)
        // {
        //     throw new Exception("用户在线信息创建失败");
        // }
        //
        // //登陆日志
        // await _loginLogService.Login(m, time);
    }

    public async Task<(bool, string)> Check()
    {
        var now = DateTime.Now;
        var user = _httpContext.HttpContext?.User;
        var sessionId = user!.GetSessionId()!;
        if (!string.IsNullOrEmpty(sessionId) && user != null)
        {
            var userInfo = await _userManager.GetUserAsync(user);
            if (userInfo == null)
            {
                return (true, "");
            }

            var online = await GetInfoByUserId(userInfo.ID);
            if (online == null)
            {
                return (false, "长时间未操作，超时登出#1");
            }

            if (online.SessionId != sessionId)
            {
                return (false, "异地登陆#1");
            }
            
            var userOnlineCache = await _distributedCache.GetStringAsync($"{_onlineCacheKey}{online.UserId}");
            var cacheData = string.IsNullOrEmpty(userOnlineCache) ? null : JsonConvert.DeserializeObject<UserOnlineModel>(userOnlineCache);

            if (cacheData == null)
            {
                online.LastTime = now;
                await _distributedCache.SetStringAsync($"{_onlineCacheKey}{online.UserId}", JsonConvert.SerializeObject(online));
            }
            else
            {
                if (cacheData.SessionId != sessionId)
                {
                    return (false, "异地登陆#2");
                }
                
                if ((now - cacheData.LastTime).TotalMinutes > _timeOut)
                {
                    return (false, "长时间未操作，超时登出#2");
                }

                cacheData.LastTime = now;
                await _distributedCache.SetStringAsync($"{_onlineCacheKey}{online.UserId}", JsonConvert.SerializeObject(cacheData));
            }
           

            //引发死锁
            //var now = DateTime.Now;
            //await _dbContext.UserOnline.Where(m => m.UserId == userInfo.ID && m.SessionId == sessionId)
            //    .UpdateAsync(m => new { LastTime = now,UpdatedAt = now });
        }

        return (true, "");
    }

    public async Task UpdateOnline(User user)
    {
        var now = DateTime.Now;
        var httpUser = _httpContext.HttpContext?.User;
        var sessionId = httpUser!.GetSessionId()!;
        await _dbContext.UserOnline.Where(m => m.UserId == user.ID && m.SessionId == sessionId)
            .UpdateAsync(m => new { LastTime = now, UpdatedAt = now });
    }
}