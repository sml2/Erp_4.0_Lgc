using System.Linq.Expressions;
using ERP.Extensions;
using ERP.Models.DB.Users;
using Microsoft.EntityFrameworkCore;
using ERP.Models.View.User.Group;
using ERP.Services.DB.Statistics;

namespace ERP.Services.DB.Users;

using Models.Abstract;
using Model = Models.DB.Users.Group;
using static Models.Abstract.BaseModel;
using static Extensions.DbSetExtension;
using Interface;
using Models.DB.Users;

using Data;
public class GroupService : BaseService
{
    private readonly DBContext _dbContext;
    private readonly ISessionProvider _SessionProvider;
    private readonly Statistic _statisticsService;

    public GroupService(DBContext myDbContext, ISessionProvider SessionProvider, Statistic statisticsService)
    {
        _dbContext = myDbContext;
        _SessionProvider = SessionProvider;
        _statisticsService = statisticsService;
    }
    private ISession Session { get => _SessionProvider.Session!; }
    private int UserID { get => Session.GetUserID(); }
    private int GroupID { get => Session.GetGroupID(); }
    private int CompanyID { get => Session.GetCompanyID(); }
    private int OEMID { get => Session.GetOEMID(); }
    
    ////原ERP3.0注释

    #region ERP3.0 PHP

    /// <summary>
    /// 获取用户组列表-分页
    /// </summary>
    /// <param name="dto"></param>
    /// <param name="companyId"></param>
    /// <param name="oemId"></param>
    /// <returns></returns>
    public async Task<PaginateStruct<Model>> GetGroupListPage(GetListDTO dto, int companyId, int oemId)
    {
        return await _dbContext.UserGroup
            .AsNoTracking()
            .WhenWhere(!dto.Name.IsNullOrWhiteSpace(), a => a.Name.Contains(dto.Name!))
            .WhenWhere(dto.State is not null && dto.State != StateEnum.None, a => a.State == dto.State)
            .WhenWhere(companyId > 0, a => a.CompanyID == companyId)
            .WhenWhere(oemId > 0, a => a.OEMID == oemId)
            .OrderBy(o => o.State)
            .OrderBy(o => o.Sort)
            .ToPaginateAsync();
    }

    //    /**
    //     * 获取用户组详细信息
    //     * User: CCY
    //     * Date: 2020/3/20 11:25
    //     * @param $id
    //     * @return mixed
    //     */
    public async Task<Model?> GetInfoById(int id)
    {
        return await _dbContext.UserGroup.FirstOrDefaultAsync(a => a.ID == id);
    }

    /// <summary>
    /// 修改用户组信息
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public async Task<Model> SaveInfo(Model data)
    {
        if (data.ID > 0)//修改
        {
            //var group = await _dbContext.UserGroup.FirstOrDefaultAsync(a => a.ID == data.ID);
            //if (group.IsNull() || data.UpdatedAt.Date != group!.UpdatedAt.Date)
            //{
            //    return ReturnArr(false, "用户组信息已变更", 0);
            //}

            var info = await _dbContext.UserGroup.Where(m => m.ID == data.ID).FirstOrDefaultAsync();
            if (info == null)
            {
                throw new Exception("用户组信息错误#1");
            }

            info.Name = data.Name;
            info.State = data.State;
            info.UpdatedAt = DateTime.Now;
            await _dbContext.UserGroup.SingleUpdateAsync(info);
            if (await _dbContext.SaveChangesAsync() == 0)
            {
                throw new Exception("用户组信息错误#2");
            }
        }
        else//添加
        {
            // var transaction = await _dbContext.Database.BeginTransactionAsync();
            try
            {
                data.Sort = - DateTime.Now.GetTimeStamp();
                await _dbContext.UserGroup.AddAsync(data);
                await _dbContext.SaveChangesAsync();
                //group
                await _statisticsService.CheckOrAdd(data);
                // await transaction.CommitAsync();
            }
            catch (Exception e)
            {
                // await transaction.RollbackAsync();
                Console.WriteLine(e.Message);
                throw new Exception(e.Message);
            }
            
        }

        return data;
    }

    /// <summary>
    /// 主键删除
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DeleteInfoById(int id)
    {
        var userGroup = await _dbContext.UserGroup.FirstOrDefaultAsync(a => a.ID == id);
        if (userGroup.IsNull())
            return ReturnArr(false, "没有找到此用户组");
        _dbContext.Remove(userGroup!);
        var count = await _dbContext.SaveChangesAsync();
        if (count > 0)
            return ReturnArr();
        else
            return ReturnArr(false, "删除失败");
    }


    //    /**
    //     * 获取当前公司全部可选用户组信息
    //     * User: CCY
    //     * Date: 2020/3/9 13:40
    //     * @param array $where
    //     * @param $userInfo
    //     * @return array
    //     */
    public async Task<List<GetGroupListVm>> GetGroupList/*<TSource>*/(
        //Expression<Func<UserGroupModel, TSource>> map,
        BaseModel.StateEnum? state = null /*$state, $company_id, $oem_id*/)
    {
        //        return groupModel::query()
        //            ->state($state)
        //            ->companyId($company_id)
        //            ->oemId($oem_id)
        //            ->sort()
        //            ->select(['id', 'name', 'state'])
        //            ->get()
        //            ->toArray();

        return
            await _dbContext.UserGroup
                .WhenWhere(state != null, m => m.State == state)
                .Where(m => m.CompanyID == CompanyID)
                .Where(m => m.OEMID == OEMID)
                .Select(x => new GetGroupListVm(x.ID, x.Name, x.State) )
                .ToListAsync();

    }


    //    /**
    //     * 获取权限组下用户组数量
    //     * User: CCY
    //     * Date: 2020/4/27 11:53
    //     * @param $id
    //     * @return mixed
    //     */
    public ReturnStruct checkGroupNumByRuleId(int ID /*$id*/)
    {
        //        return groupModel::query()->ruleId($id)->count();

        return ReturnArr();
    }

    public record GroupWithNameDto(int Id, string Name, StateEnum State);
    
    /// <summary>
    /// 获取用户组业务统计用户信息
    /// </summary>
    /// <param name="currentCompanyId"></param>
    /// <param name="name"></param>
    /// <param name="state"></param>
    /// <returns></returns>
    public Task<PaginateStruct<GroupWithNameDto>> GetGroupBusiness(int currentCompanyId, string? name, StateEnum? state)
    {
        return _dbContext.UserGroup.Where(m => m.CompanyID == currentCompanyId)
            .WhenWhere(!string.IsNullOrEmpty(name), g => g.Name.StartsWith(name!))
            .WhenWhere(state.HasValue, g => g.State == state)
            .OrderByDescending(g => g.ID)
            .Select(m => new GroupWithNameDto(m.ID, m.Name, m.State))
            .ToPaginateAsync();
    }

    #endregion

    /// <summary>
    /// 根据公司id获取所有用户组
    /// </summary>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public async Task<List<Group>> GetGroupsByCompanyId(int companyId)
    {
        return await _dbContext.UserGroup.Where(m => m.CompanyID == companyId).ToListAsync();
    }

}