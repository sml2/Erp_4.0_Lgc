using ERP.Data;
using ERP.Extensions;
using ERP.Interface;
using Microsoft.EntityFrameworkCore;
using UserValidityModel = ERP.Models.DB.Users.UserValidity;
namespace ERP.Services.DB.Users;
public class UserValidity : BaseService
{
    private readonly DBContext _dbContext;
    private readonly ISessionProvider _SessionProvider;

    public UserValidity(DBContext dbContext, ISessionProvider SessionProvider)
    {
        _dbContext = dbContext;
        _SessionProvider = SessionProvider;

    }

    private ISession Session { get => _SessionProvider.Session!; }
    private int UserID { get => Session.GetUserID(); }
    private int GroupID { get => Session.GetGroupID(); }
    private int CompanyID { get => Session.GetCompanyID(); }
    private int OEMID { get => Session.GetOEMID(); }
    //    /**
    //     * 
    //     * User: CCY
    //     * Date: 2020/3/25 17:07
    //     * @param $validity
    //     * @return int
    //     */
    public async Task<UserValidityModel> AddValidity(DateTime validity)
    {
        var u = new UserValidityModel
        {
            Validity = validity,
            CompanyID = CompanyID,
            OEMID = OEMID
        };
        await _dbContext.UserValidities.AddAsync(u);
        var count = await _dbContext.SaveChangesAsync();
        if (count > 0)
        {
            return u;
        }
        else
        {
            throw new Exception("添加有效期失败");
        }
    }

    /// <summary>
    /// 获取有效期时间戳
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    public async Task<List<UserValidityModel>> GetValidityList(List<int> ids)
    {
        return await _dbContext.UserValidities.Where(a => ids.Contains(a.ID)).ToListAsync();
    }

    /// <summary>
    /// 获取有效期时间戳
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    public async Task<UserValidityModel?> GetValidity(int id)
    {
        var v = await _dbContext.UserValidities.FirstOrDefaultAsync(a => a.ID == id);
        //if (v == null)
        //{
        //    throw new Exception("没有查询到时间戳");
        //}
        return v;
    }

    public async Task<List<UserValidityModel>> GetValidity(List<int> ids)
    {
        return await _dbContext.UserValidities.Where(a => ids.Contains(a.ID)).ToListAsync();
    }

    /// <summary>
    /// 更新用户展示有效期
    /// </summary>
    /// <param name="id"></param>
    /// <param name="showValidity"></param>
    /// <returns></returns>
    public async Task<bool> UpdateValidity(int id, DateTime showValidity)
    {
        // return UserValidityModel::query()->where('id', $id)->update(['validity' => $validity]);
        var v = await _dbContext.UserValidities.FirstOrDefaultAsync(a => a.ID == id);
        if (v != null)
        {
            v.Validity = showValidity;
            _dbContext.UserValidities.Update(v);
            if (await _dbContext.SaveChangesAsync() > 0)
            {
                return true;
            };
        }
        throw new Exception("更新用户展示有效期失败");
    }
}
