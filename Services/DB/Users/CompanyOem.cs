using Ryu.Linq;

namespace ERP.Services.DB.Users;

using Data;
using Extensions;
using Interface;
using Microsoft.EntityFrameworkCore;
using static Extensions.DbSetExtension;
using CompanyOemModel = Models.DB.Users.CompanyOem;
using UserModel = Models.DB.Identity.User;
public class CompanyOem : BaseService
{
    private readonly DBContext _dbContext;
    private readonly ISessionProvider _session;
    public CompanyOem(DBContext dbContext, ISessionProvider session)
    {
        _dbContext = dbContext;
        _session = session;
    }
    ////原ERP3.0注释
    #region ERP3.0 PHP
    //    /**
    //     * 获取OEM下分配信息
    //     * User: CCY
    //     * Date: 2020/6/28 10:17
    //     * @param $oem_id
    //     * @param $username
    //     * @return mixed
    //     */
    public async Task<PaginateStruct<CompanyOemModel>> GetList(int oem_id, string? username)
    {
        //        return companyOemModel::query()
        //            ->oemId($oem_id)
        //            ->username($username)
        //            ->paginate($this->limit);
        return await _dbContext.CompanyOem
            .Where(a => a.OEMID == oem_id)
            .WhenWhere(username != null,a => a.UserName == username)
            .ToPaginateAsync();
    }

    //    /**
    //     * 主键删除
    //     * User: CCY
    //     * Date: 2020/6/28 10:44
    //     * @param $id
    //     * @return int
    //     */
    public async Task<ReturnStruct> DeleteInfoById(int id)
    {
        var c = await _dbContext.CompanyOem.FirstOrDefaultAsync(a => a.ID == id);
        if(c != null)
        {
            _dbContext.CompanyOem.Remove(c);
            await _dbContext.SaveChangesAsync();
            return ReturnArr(true);
        }
        return ReturnArr(false);
    }

    /// <summary>
    /// 检测存在关联性
    /// </summary>
    /// <param name="oemId"></param>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public async Task<CompanyOemModel?> CheckExist(int oemId, int companyId)
    {
        return await _dbContext.CompanyOem
            .Where(c => c.OEMID == oemId)
            .Where(c => c.CompanyID == companyId)
            .FirstOrDefaultAsync();
    }
    
    /// <summary>
    /// 添加关联
    /// </summary>
    /// <param name="oemId"></param>
    /// <param name="companyId"></param>
    /// <param name="username"></param>
    /// <returns></returns>
    public async Task<int> AddAllot(int oemId, int companyId, string username)
    {

        CompanyOemModel companyOem = new()
        {
            OEMID = oemId,
            CompanyID = companyId,
            UserName = username,
            AllotUserId = _session.Session!.GetUserID(),
            AllotTrueName = _session.Session!.GetTrueName(),
            CreatedAt = DateTime.Now,
        };
        await _dbContext.CompanyOem.AddAsync(companyOem);
        await _dbContext.SaveChangesAsync();
        return companyOem.ID;
    }

    /// <summary>
    /// 添加关联
    /// </summary>
    /// <param name="oemId"></param>
    /// <param name="user"></param>
    /// <returns></returns>
    public async Task<int> AddAllot(int oemId, UserModel user)
    {

        CompanyOemModel companyOem = new()
        {
            OEMID = oemId,
            CompanyID = user.CompanyID,
            UserName = user.UserName,
            AllotUserId = _session.Session!.GetUserID(),
            AllotTrueName = _session.Session!.GetTrueName(),
            CreatedAt = DateTime.Now,
        };
        await _dbContext.CompanyOem.AddAsync(companyOem);
        await _dbContext.SaveChangesAsync();
        return companyOem.ID;
    }


    //    /**
    //     * 获取其余oem信息id
    //     * User: CCY
    //     * Date: 2020/4/11 14:57
    //     * @param $company_id
    //     * @return mixed
    //     */
    public async Task<List<int>> GetOemIds(int companyId)
    {
        return await _dbContext.CompanyOem.Where(a => a.CompanyID == companyId).Select(a => a.OEMID).ToListAsync();
    }
    #endregion
}
