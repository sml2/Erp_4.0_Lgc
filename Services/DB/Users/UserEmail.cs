using ERP.Data;
using ERP.Extensions;
using ERP.Models.DB.Users;
using Microsoft.EntityFrameworkCore;

namespace ERP.Services.DB.Users;
public class UserEmailService : BaseService
{
    private readonly DBContext _dbContext;
      public UserEmailService(DBContext dbContext)
      {
          _dbContext = dbContext;
      }

    public IQueryable<UserEmail> GetUserEmailInfo(int? id = null, UserEmail.States? state = null,int userId= 0,int? typeId= null,string name = "")
    {
        return _dbContext.UserEmails.WhenWhere(id is not null, ue => ue.ID == id)
            .WhenWhere(state is not null, ue => ue.State == state)
            .WhenWhere(userId != 0, ue => ue.UserID == userId)
            .WhenWhere(typeId.HasValue, ue => ue.TypeId == typeId)
            .WhenWhere(!string.IsNullOrEmpty(name), ue => ue.Name == name);
    }

    public ReturnStruct getPage(/*$result*/)
    {
        //        return $result->paginate($this->limit);
        return ReturnArr();
    }

    public async Task Insert(UserEmail userEmail)
    {
         _dbContext.UserEmails.Add(userEmail);
         await _dbContext.SaveChangesAsync();
    }

    public Task<UserEmail?> Info(int id)
    {
        return _dbContext.UserEmails.FirstOrDefaultAsync(u => u.ID == id);
    }

    /// <summary>
    /// 修改状态
    /// </summary>
    /// <param name="id"></param>
    /// <param name="state"></param>
    public async Task ChangeStateAsync(int id, UserEmail.States state)
    {
        var row = await (from ue in _dbContext.UserEmails
            where ue.ID == id
            select ue
            // new UserEmail() { ID = ue.ID, State = ue.State }
            ).FirstOrDefaultAsync();
        
        if (row == null) return;
        
        row.State = state;
        _dbContext.SaveChanges();
    }
    
    public async Task Update(int id, UserEmail updateData)
    {
        _dbContext.UserEmails.Update(updateData);
        await _dbContext.SaveChangesAsync();
    }

    public async Task<bool> Delete(int id)
    {
        _dbContext.UserEmails.Remove(new UserEmail() { ID = id });
        await _dbContext.SaveChangesAsync();
        return true;
    }
    
    public async Task<bool> Delete(IEnumerable<int> ids)
    {
        _dbContext.UserEmails.RemoveRange(ids.Select(id => new UserEmail(){ ID = id}));
        await _dbContext.SaveChangesAsync();
        return true;
    }

    /// <summary>
    /// 重置标记
    /// </summary>
    /// <param name="id"></param>
    /// <param name="userId"></param>
    public async Task<bool> ResetFlag(int id, int userId)
    {
        var model = new UserEmail()
        {
            ID = id,
            UserID = userId,
            Tag = "",
            SyncMsg = "已重置",
            SyncTime = null
        };
        _dbContext.UserEmails.Update(model);
        var affected = await _dbContext.SaveChangesAsync();
        return affected > 0;
    }
}
