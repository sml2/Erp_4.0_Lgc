using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Linq.Expressions;
using ERP.Enums.Statistics;
using ERP.Exceptions.Company;
using ERP.Exceptions.Users;
using ERP.Models;
using ERP.Models.Abstract;
using ERP.Models.DB.Identity;
using ERP.Services.Statistics;
using ERP.Services.Stores;
using ERP.Storage.Services.Internal;
using ERP.ViewModels.Subordinate;
using Z.EntityFramework.Plus;
using NotFoundException = ERP.Exceptions.Company.NotFoundException;
using Role = ERP.Enums.Identity.Role;
using Task = System.Threading.Tasks.Task;

namespace ERP.Services.DB.Users;

using Enums.View;
using Interface;
using Extensions;
using Models.DB.Users;
using Models.View.User.Referer;
using Models.View.User.User;
using Services.DB.Statistics;
using Services.Identity;
using static Models.Abstract.BaseModel;
using Model = Models.DB.Identity.User;
using PackageModel = Models.DB.Users.Package;
using Role = Role;
using StoreModel = Models.Stores.StoreRegion;
using UserStoreModel = Models.DB.Users.UserStore;
using ERP.Data;
using StoreCache = Caches.Store;

public class UserService : BaseService
{
    private readonly DBContext _dbContext;
    private readonly ISessionProvider _SessionProvider;
    private readonly UserManager _userManager;
    private readonly Statistic _statisticService;
    private readonly StoreCache _storeCache;
    private readonly UserOperateLogService _userOperateLogService;
    private readonly StatisticMoney _statisticMoney;
    private readonly GroupService _groupService;
    

    private ISession Session
    {
        get => _SessionProvider.Session!;
    }

    //public UserService(DBContext.MyDbContext myDbContext, ISessionProvider SessionProvider, Statistic statisticService)
    //{
    //    _dbContext = myDbContext;
    //    _SessionProvider = SessionProvider;
    //    _statisticService = statisticService;
    //}
    public UserService(DBContext myDbContext, StoreCache storeCache, ISessionProvider SessionProvider,
        UserManager userManager,
        Statistic statisticService, UserOperateLogService userOperateLogService, GroupService groupService)
    {
        _dbContext = myDbContext;
        _SessionProvider = SessionProvider;
        _userManager = userManager;
        _statisticService = statisticService;
        _userOperateLogService = userOperateLogService;
        _groupService = groupService;
        _storeCache = storeCache;
        _statisticMoney = new StatisticMoney(_statisticService);
    }

    private int UserID
    {
        get => Session.GetUserID();
    }

    private int GroupID
    {
        get => Session.GetGroupID();
    }

    private int CompanyID
    {
        get => Session.GetCompanyID();
    }

    private int OEMID
    {
        get => Session.GetOEMID();
    }

    private int Level
    {
        get => Session.GetLevel();
    }

    private string Pids
    {
        get => Session.GetUserPids();
    }
    ////原ERP3.0注释

    #region ERP3.0 PHP

    //    private $userModel;

    //   public ReturnStruct __construct()
    //    {
    //        parent::__construct();
    //        $this->userModel = new userModel();
    //    }

    //    /**
    //     * 获取用户列表分页
    //     * User: CCY
    //     * Date: 2020/3/16 16:51
    //     * @param array $where
    //     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
    //     */
    public async Task<DbSetExtension.PaginateStruct<UserListPage>> GetUserListPage(
        int page, int limit,
        string? Name = null, int? RuleId = null, StateEnum? State = null, bool? IsDistribution = null,
        int? oemId = null, List<int>? companyIds = null, string? mobile = null, bool IsPage = true)
    {
        return await _dbContext.User
            // .AsNoTracking()
            .Include(m => m.OEM)
            .WhenWhere(Name.IsNotWhiteSpace(), u => u.UserName.Contains(Name!) || u.TrueName.Contains(Name!))
            .WhenWhere(RuleId.HasValue, u => u.FormRuleTemplateID == RuleId)
            .WhenWhere(State.HasValue, u => u.State == State)
            .Where(u => u.Role == Role.ADMIN)
            .Where(u => u.CreateCompanyId == CompanyID)
            .WhenWhere(IsDistribution.HasValue, u => u.IsDistribution == IsDistribution)
            .WhenWhere(oemId.HasValue, u => u.OEMID == oemId)
            .WhenWhere(companyIds != null && companyIds.Count > 0, u => companyIds!.Contains(u.CompanyID))
            .WhenWhere(!mobile.IsNullOrWhiteSpace(), u => u.PhoneNumber == mobile)
            .OrderByDescending(u => u.Id)
            .Select(x => new UserListPage(x.ID, x.UserName, x.PhoneNumberConfirmed, x.TrueName, x.Mobile,
                x.FormRuleTemplateID, x.State, x.Role == Role.Employee ? Concierges.Child : Concierges.Own,
                x.IsDistribution, x.Avatar, x.OEMID, x.CompanyID, x.CreatedAt, x.FirstLoginExe, x.FirstLoginWeb,
                x.OEM.Name
            )).ToPaginateAsync(page: page, limit: limit); //vmodel应该在controller编写       
        //嘉杰科技定制
        //        if((int)$this->getSession()->getOemId() === 10003){
        //            $model->State(userModel::STATE_ENABLE);
        //        }
    }

    public record UserListPage(int ID, string Username, bool PhoneNumberConfirmed, string Truename, string Mobile,
        int RuleId, StateEnum State, Concierges Concierge,
        bool IsDistribution, string Avatar, int OemId, int CompanyId, DateTime CreatedAt, DateTime? FirstLoginExe,
        DateTime? FirstLoginWeb, string OemName
    );

    /// <summary>
    /// 获取员工用户列表分页
    /// </summary>
    /// <param name="vm"></param>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public async Task<DbSetExtension.PaginateStruct<Model>> GetChildUserListPage(GetChildListVM vm, int companyId)
    {
        var list = await _dbContext.User
            .WhenWhere(vm.Name.IsNotWhiteSpace(), a => a.UserName.Contains(vm.Name!) || a.TrueName.Contains(vm.Name!))
            .WhenWhere(vm.RuleId != null, a => a.FormRuleTemplateID == vm.RuleId)
            .WhenWhere(vm.State != null, a => a.State == vm.State)
            .WhenWhere(vm.GroupId != null, a => a.GroupID == vm.GroupId)
            .WhenWhere(vm.OemId != null, a => a.OEMID == vm.OemId)
            .Where(a => a.CompanyID == companyId)
            .Where(a => a.Role == Role.Employee)
            .OrderByDescending(a => a.Id)
            .ThenBy(a => a.Sort)
            .ToPaginateAsync(page: vm.Page, limit: vm.Limit);
        //        //嘉杰科技定制
        ////        if((int)$this->getSession()->getOemId() === 10003){
        ////            $list->State(userModel::STATE_ENABLE);
        ////        }
        //        return $list->paginate($this->limit);
        return list;
    }

    /// <summary>
    /// /检测用户名是否存在
    /// </summary>
    /// <param name="userName"></param>
    /// <returns></returns>
    public async Task<Model?> CheckUserName(string userName)
    {
        // return await _dbContext.User.FirstOrDefaultAsync(a => a.UserName == userName);
        return await _dbContext.User.FirstOrDefaultAsync(a => a.UserName == userName);
    }

    /// <summary>
    /// 检测手机号码是否存在
    /// </summary>
    /// <param name="phoneNumber"></param>
    /// <returns></returns>
    public async Task<Model?> CheckUserMobile(string phoneNumber)
    {
        return await _dbContext.User.FirstOrDefaultAsync(a => a.PhoneNumber == phoneNumber);
    }

    //    /**
    //     * 查找指定公司下用户数
    //     * User: CCY
    //     * Date: 2020/6/10 14:42
    //     * @param $company_id
    //     * @return mixed
    //     */
    public async Task<int> GetUserNumByCompanyId(int companyId)
    {
        //        return $this->userModel::query()->companyId($company_id)->count();
        return await _dbContext.User.Where(a => a.CompanyID == companyId).CountAsync();
    }

    //    /**
    //     * 查找指定用户组下用户数
    //     * User: CCY
    //     * Date: 2020/4/8 10:09
    //     * @param $group_id
    //     * @return mixed
    //     */
    public async Task<int> CheckUserNumByGroupId(int groupId)
    {
        //        return $this->userModel::query()->groupId($group_id)->count();
        return await _dbContext.User.Where(a => a.GroupID == groupId).CountAsync();
    }


    public async Task<int> CheckStoreNumByGroupId(int groupId)
    {
        return await _dbContext.StoreRegion.Where(a => a.BelongsGroupID == groupId).CountAsync();
    }

    //    /**
    //     * 查找指定权限组下用户数
    //     * User: CCY
    //     * Date: 2020/5/9 15:05
    //     * @param $rule_id
    //     * @return int
    //     */
    public ReturnStruct checkUserNumByRuleId( /*$rule_id*/)
    {
        //        return $this->userModel::query()->ruleId($rule_id)->count();
        return ReturnArr();
    }


    /// <summary>
    /// 初始化信息(独立账户/子账户)
    /// </summary>
    /// <param name="requestData"></param>
    /// <param name="themeId"></param>
    /// <returns></returns>
    public async Task<Model> InitUser(Models.View.User.User.User vm, Role role, int themeId)
    {
        var pids = JsonConvert.DeserializeObject<List<int>>(Pids);
        pids?.Add(UserID);
        var insertData = new Model
        {
            UserName = vm.UserName,
            TrueName = vm.TrueName,
            Password = vm.CheckPassword ?? "", // dto.Password
            Mobile = vm.Mobile ?? "",
            Email = vm.Email ?? "",
            FormRuleTemplateID = vm.RuleId,
            GroupID = Role.ADMIN == role ? GroupID : vm.GroupId,
            Pid = UserID,
            State = vm.State,
            Pids = JsonConvert.SerializeObject(pids),
            Role = role,
            IsDistribution = Role.ADMIN == role ? vm.IsDistribution : Session.GetIsDistribution(),
            Sort = -DateTime.Now.GetTimeStamp(),
            Level = Level + Role.ADMIN == role ? 1 : 0,
            OEMID = Role.ADMIN == role ? vm.OemId : OEMID,
            CompanyID = CompanyID,
            CreateCompanyId = CompanyID,
            ThemeId = themeId,
            OrderRateConfig = Model.OrderRateConfigs.REALTIME,
        };
        insertData.PasswordHash = _userManager.PasswordHasher.HashPassword(insertData, insertData.Password);
        var res = await _userManager.CreateAsync(insertData);
        if (res.Succeeded)
        {
            var ret = await _userManager.AddToRoleAsync(insertData, insertData.Role);
            if (ret.Succeeded)
            {
                return insertData;
            }
            else
            {
                throw new Exception(string.Join(",", ret.Errors.Select(e => e.Description)));
            }
        }
        else
        {
            throw new Exception(string.Join(",", res.Errors.Select(e => e.Description)));
        }
    }

    public async Task<Model> GetCompanyAdmin(Model user)
    {
        if (user == null)
            throw new ArgumentNullException(nameof(user));
        var CompanyAdmin =
            await _dbContext.User.FirstOrDefaultAsync(u => u.CompanyID == user.CompanyID && u.Role == Role.ADMIN);
        if (CompanyAdmin == null)
            throw new NullReferenceException(nameof(CompanyAdmin));
        return CompanyAdmin;
    }

    public async Task<Model> GetAdminUser(FindSubDto vm)
    {
        var user = await _dbContext.User
            .FirstOrDefaultAsync(u => u.OEMID == vm.OemId && u.UserName == vm.Username && u.Role == Role.ADMIN);

        if (user == null)
            throw new Exceptions.Exception("请输入管理员账户用户名");

        return user;
    }
    public async Task<List<JuniorUser>> GetAdminUser(List<int> companyIds,int oemId)
    {
        if (_SessionProvider.Session.GetCompanyID() != Enums.ID.Company.XiaoMi.GetNumber())
        {
            throw new PermissionException("无权操作#1");
        }
        
        return await _dbContext.User
            .Where(x => x.Role == Role.ADMIN)
            .Where(x => x.OEMID == oemId)
            .Where(x => companyIds.Contains(x.CompanyID))
            .OrderBy(x => x.CompanyID)
            .Select(x => new JuniorUser(x.ID, x.UserName, x.TrueName, x.FormRuleTemplateID, x.State, x.CreatedAt,
                x.IsDistribution, x.CompanyID,x.OEMID,x.PhoneNumberConfirmed)) //is_distribution
            .ToListAsync();
    }

    /// <summary>
    /// 更新独立账户用户公司id
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public async Task<Model> UpdateUserCompanyId(Model user, int companyId)
    {
        //var user = await _dbContext.User.FirstOrDefaultAsync(a => a.ID == userId);
        if (user == null)
        {
            throw new Exception("更新用户公司失败!");
        }

        //todo 待定
        user.CompanyID = companyId;
        _dbContext.User.Update(user);
        if (await _dbContext.SaveChangesAsync() == 0)
        {
            throw new Exception("更新用户公司数据库失败!");
        }
        return user;
    }

    /// <summary>
    /// 根据用户id获取用户信息
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    public async Task<Model> GetInfoById(int id)
    {
        var user = await _dbContext.User.FirstOrDefaultAsync(a => a.Id == id);
        if (user == null)
        {
            throw new Exception("未找到此用户");
        }

        return user;
    }
    
    public async Task<Model> GetUserWithCompanyAdmin(int companyId)
    {
        var user = await _dbContext.User.FirstOrDefaultAsync(a => a.CompanyID == companyId && a.Role == Role.ADMIN);
        if (user == null)
        {
            throw new Exception("未找到此用户");
        }

        return user;
    }

    /// <summary>
    /// 更新用户信息
    /// </summary>
    /// <param name="vm"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> EditUser(Models.View.User.User.User vm)
    {
        var user = await _userManager.FindByIdAsync(vm.Id.ToString());
        
        if (user.State == StateEnum.AdminToDisable && Session.GetCompanyID() != Enums.ID.Company.XiaoMi.GetNumber())
        {
            throw new Exceptions.Exception("无权操作");
        }
        
        user.UserName = vm.UserName;
        user.TrueName = vm.TrueName;
        user.FormRuleTemplateID = vm.RuleId;
        user.Mobile = vm.Mobile ?? "";
        user.Email = vm.Email ?? "";
        user.State = vm.State;
        if (vm.GroupId > 0)
        {
            user.GroupID = vm.GroupId;
        }

        if (vm.Newpassword.IsNotWhiteSpace())
        {
            user.PasswordHash = _userManager.PasswordHasher.HashPassword(user, vm.Newpassword);
            user.Password = vm.Newpassword ?? "";
        }

        await _userManager.UpdateAsync(user);

        return ReturnArr();
    }

    /// <summary>
    /// 禁用独立账户下员工账户
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="state"></param>
    /// <returns></returns>
    public async Task<int> UserStateDisable(int companyId, StateEnum state)
    {
        //        return userModel::query()
        //            ->State(userModel::STATE_ENABLE)
        //            ->companyId($company_id)
        //            ->update(['state' => userModel::STATE_DISABLE]);
        return await _dbContext.User.Where(m => m.CompanyID == companyId && m.State == StateEnum.Open)
            .UpdateFromQueryAsync(m => new { State = state });
    }

    //    /**
    //     * 更新公司下用户状态
    //     * User: CCY
    //     * Date: 2021/4/14
    //     * @param     $companyId
    //     * @param int $state
    //     * @return mixed
    //     */
    public ReturnStruct updateUserState( /*$companyId, $state = userModel::STATE_DISABLE*/)
    {
        //        return userModel::query()
        //            ->companyId($companyId)
        //            ->update(['state' => $state]);
        return ReturnArr();
    }

    public record UserWithNameDto(int Id, string UserName, string TrueName, StateEnum State);

    /// <summary>
    /// 获取用户业务统计用户信息
    /// </summary>
    /// <param name="name"></param>
    /// <param name="state"></param>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public Task<DbSetExtension.PaginateStruct<UserWithNameDto>> GetUserBusiness(string? name, StateEnum? state,
        int? companyId = null)
    {
        //小米搜索用户过滤公司信息
        if (!string.IsNullOrEmpty(name) && companyId == (int)Enums.ID.Company.XiaoMi)
        {
            companyId = null;
        }

        return _dbContext.User
            .WhenWhere(!string.IsNullOrEmpty(name), u => name != null && u.UserName.StartsWith(name))
            .WhenWhere(state != null, user => user.State == state)
            .WhenWhere(companyId.HasValue, u => u.CompanyID == companyId)
            .OrderByDescending(u => u.CreatedAt)
            .Select(m => new UserWithNameDto(m.Id, m.UserName, m.TrueName, m.State))
            .ToPaginateAsync();
    }

    /// <summary>
    /// 重置密码
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ResetPassword(int id)
    {
        //var user = await _dbContext.User.FirstOrDefaultAsync(a => a.Id == id);

        return await Task.FromResult(ReturnArr(true));
    }

    public ReturnStruct addOrUpdateTheme( /*$data*/)
    {
        //        if (isset($data['id']) && !empty($data['id'])) {
        //            $id = $data['id'];
        //            $theme = Theme::query()->find($id);
        //        } else {
        //            $theme = new Theme();
        //        }
        //        DB::beginTransaction();
        //        try {
        //            $theme->menu_text_color = $data['menu_text_color'];
        //            $theme->menu_active_text_color = $data['menu_active_text_color'];
        //            $theme->sub_menu_active_text_color = $data['sub_menu_active_text_color'];
        //            $theme->menu_bg_url = $data['menu_text_color'];
        //            $theme->menu_bg = $data['menu_bg'];
        //            $theme->menu_hover_color = $data['menu_hover_color'];
        //            $theme->sub_menu_bg = $data['sub_menu_bg'];
        //            $theme->sub_menu_hover_color = $data['sub_menu_hover_color'];
        //            $theme->title_color = $data['title_color'];
        //            $theme->oem_id = $this->getSession()->getOemId();
        //            $theme->user_id = $this->getSession()->getUserId();
        //            $theme->save();
        //            $this->userModel = $this->userModel::query()->find($this->getSession()->getUserId());
        //            $this->userModel->theme_id = $theme->id;
        //            $this->userModel->save();
        //            DB::commit();
        //        } catch (\Exception $e) {
        //            DB::rollBack();
        //            return returnArr(false, $e->getMessage());
        //        }
        //        return returnArr(true);
        return ReturnArr();
    }

    public ReturnStruct setUserInfo( /*$data*/)
    {
        //        // 删除未使用的图片
        //        $imageArr = $data['avatarList'];

        //        $disk = app('oem')->getOss();
        //        if (count($imageArr)) {
        //            foreach ($imageArr as $v) {
        //                if (!$disk->delete($v)) {
        //                    writeImageLog(storage_path('/logs/avatar/'), $v);
        //                }
        //            }
        //        }
        //        $userInfo = $data['userInfo'];
        //        $userId = $this->getSession()->getUserId();

        //        $users = $this->userModel->find($userId);

        //        if ($userInfo['password']) {
        //            $users->password = encrypt($userInfo['password']);
        //        }
        //        $users->truename = $userInfo['truename'];
        ////        $users->mobile = $userInfo['mobile'];
        //        $users->email = $userInfo['email'];
        //        $users->avatar = $userInfo['avatar'];
        //        $users->unit_config = $userInfo['unit_config'];
        //        $users->order_rate_config = $userInfo['order_rate_config'];
        //        $users->product_img_config = $userInfo['product_img_config'];
        //        $OrderUnitConfigModel = new OrderUnitConfigModel($users->order_unit_config);
        //        $bitArr = $userInfo['order_unit_config'];
        //        if (isset($userInfo['product_show_unit']) && $userInfo['product_show_unit']) {
        //            $bitArr[] = 'ProductShowUnit';
        //        }
        //        if (isset($userInfo['product_prefix_config']) && $userInfo['product_prefix_config']) {
        //            $bitArr[] = 'ProductPrefixConfig';
        //        }
        //        if (isset($userInfo['product_suffix_config']) && $userInfo['product_suffix_config']) {
        //            $bitArr[] = 'ProductSuffixConfig';
        //        }
        //        if (isset($userInfo['product_title_config']) && $userInfo['product_title_config']) {
        //            $bitArr[] = 'ProductTitleConfig';
        //        }
        //        $users->order_unit_config = self::getOrderUnitConfigSave($OrderUnitConfigModel, $bitArr);
        //        $rs = $users->save();
        //        if ($rs) {
        //            $this->getSession()->setAvatar($users->avatar);
        ////            $this->getSession()->setMobile($users->mobile);
        //            $this->getSession()->setEmail($users->email);
        //            $this->getSession()->setUnitConfig($users->unit_config);
        //            $this->getSession()->setOrderRateConfig($users->order_rate_config);
        //            $this->getSession()->setProductImgConfig($users->product_img_config);
        //            $this->getSession()->setOrderUnitConfigTotal($OrderUnitConfigModel->hasOrderUnitConfigTotal());
        //            $this->getSession()->setOrderUnitConfigGoods($OrderUnitConfigModel->hasOrderUnitConfigGoods());
        //            $this->getSession()->setOrderUnitConfigFee($OrderUnitConfigModel->hasOrderUnitConfigFee());
        //            $this->getSession()->setOrderUnitConfigRefund($OrderUnitConfigModel->hasOrderUnitConfigRefund());
        //            $this->getSession()->setOrderUnitConfigDeliver($OrderUnitConfigModel->hasOrderUnitConfigDeliver());
        //            $this->getSession()->setOrderUnitConfigPurchase($OrderUnitConfigModel->hasOrderUnitConfigPurchase());
        //            $this->getSession()->setOrderUnitConfigProfit($OrderUnitConfigModel->hasOrderUnitConfigProfit());
        //            //产品展示货币
        //            $this->getSession()->setProductShowUnit($OrderUnitConfigModel->hasProductShowUnit());
        //            //SKU前缀配置
        //            $this->getSession()->setProductPrefixConfig($OrderUnitConfigModel->hasProductPrefixConfig());
        //            //SKU追加配置
        //            $this->getSession()->setProductSuffixConfig($OrderUnitConfigModel->hasProductSuffixConfig());
        //            //标题追加配置
        //            $this->getSession()->setProductTitleConfig($OrderUnitConfigModel->hasProductTitleConfig());
        //        }
        //        if ($userInfo['truename'] != $users['truename']) {
        //            //删除公司下员工redis
        //            Cache::instance()->CompanyUsers()->del();
        return ReturnArr();
    }

    //        return $rs;
    //    }

    //    /**
    //     * 更新当前公司下用户分销属性
    //     * User: CCY
    //     * Date: 2020/6/23 19:20
    //     * @param string $is_distribution
    //     * @return mixed
    //     */
    public ReturnStruct updateDistribution( /*$is_distribution = ''*/)
    {
        //        $is_distribution == '' && $is_distribution = $this->userModel::DISTRIBUTION_TRUE;
        //        return $this->userModel::query()
        //            ->companyId($this->getSession()->getCompanyId())
        //            ->update(['is_distribution' => $is_distribution]);
        return ReturnArr();
    }

    /// <summary>
    /// 获取指定公司下用户id
    /// </summary>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public async Task<List<int>> getUidsByCompanyId(int companyId)
    {
        return await _dbContext.User.Where(m => m.CompanyID == companyId && m.State == StateEnum.Open)
            .Select(m => m.ID).ToListAsync();
    }

    /// <summary>
    /// 查看下级用户信息
    /// </summary>
    /// <param name="CompanyId"></param>
    /// <param name="concierge"></param>
    /// <param name="userName"></param>
    /// <param name="companyIds"></param>
    /// <returns></returns>
    public async Task<DbSetExtension.PaginateStruct<JuniorUser>> GetJuniorUser(int CompanyId, Role role,
        string? userName, int[] companyIds,int? oemId)
    {
        return await _dbContext.User
            .Where(x => x.CreateCompanyId == CompanyId)
            .Where(x => x.Role == role)
            .WhenWhere(oemId.HasValue,x => x.OEMID == oemId)
            .WhenWhere(!userName.IsNullOrWhiteSpace(), x => x.UserName.Contains(userName!))
            .WhenWhere(companyIds.Count() > 0, x => companyIds.Contains(x.CompanyID))
            .OrderByDescending(x => x.Sort)
            .Select(x => new JuniorUser(x.ID, x.UserName, x.TrueName, x.FormRuleTemplateID, x.State, x.CreatedAt,
                x.IsDistribution, x.CompanyID,x.OEMID,x.PhoneNumberConfirmed)) //is_distribution
            .ToPaginateAsync();
    }
    
    public async Task<DbSetExtension.PaginateStruct<JuniorUser>> GetJuniorUserAll(Role role, string userName, int? oemId)
    {
        return await _dbContext.User
            .Where(x => x.Role == role)
            .WhenWhere(oemId.HasValue,x => x.OEMID == oemId)
            .WhenWhere(!userName.IsNullOrWhiteSpace(), x => x.UserName.Contains(userName!))
            .OrderByDescending(x => x.Sort)
            .Select(x => new JuniorUser(x.ID, x.UserName, x.TrueName, x.FormRuleTemplateID, x.State, x.CreatedAt,
                x.IsDistribution, x.CompanyID,x.OEMID,x.PhoneNumberConfirmed)) //is_distribution
            .ToPaginateAsync();
    }
    
    

    public record JuniorUser(int ID, string UserName, string TrueName, int RuleId, StateEnum State, DateTime CreatedAt,
        bool IsDistribution, int CompanyId, int OemId, bool PhoneNumberConfirmed);

    public ReturnStruct userStateChange( /*$userId, $action*/)
    {
        //        if (!$userId) {
        //            return returnArr(false, '缺少用户ID');
        //        }
        //        // 3权限变更 2用户修改密码 1用户禁止登录
        //        $redisUserInfo = $this->getUserInfoCache($userId);
        //        if ($redisUserInfo) {
        //            $state = isset($redisUserInfo['state']) ? $redisUserInfo['state'] : 0;
        //            $newState = 0;
        //            if ($state == 0 && ($action == 2 || $action == 3 || $action == 1)) {
        //                $newState = (int)$action;
        //            } elseif ($state == 1) {
        //                $newState = 1;
        //            } elseif ($state == 2 && $action == 1) {
        //                $newState = 1;
        //            } elseif ($state == 2 && ($action == 2 || $action == 3)) {
        //                $newState = 2;
        //            } elseif ($state == 3 && ($action == 2 || $action == 3 || $action == 1)) {
        //                $newState = (int)$action;
        //            }
        //            $redisUserInfo = array_merge($redisUserInfo, ['state' => $newState]);
        //            return $this->setUserInfoCache($userId, $redisUserInfo);
        //        }
        //        return false;
        return ReturnArr();
    }

    //    /**
    //     * 获取用户全部信息
    //     * User: CCY
    //     * Date: 2020/7/20 19:24
    //     * @param $id
    //     * @return null|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
    //     */
    public ReturnStruct getInfo( /*$id*/)
    {
        //        return $this->userModel::query()->find($id);
        return ReturnArr();
    }

    //    /**
    //     * 获取用户名
    //     * User: CCY
    //     * Date: 2020/7/23 14:29
    //     * @param $id
    //     * @return mixed
    //     */
    public async Task<List<UserInfoSelectStruct>> GetUsers(List<int> ids) =>
        await GetUsers(ids, m => new UserInfoSelectStruct(m.ID, m.UserName));

    public async Task<List<TSource>> GetUsers<TSource>(List<int> ids, Expression<Func<Model, TSource>> map)
    {
        return await _dbContext.User
            .Where(m => m.CompanyID == CompanyID)
            .Where(m => ids.Contains(m.Id))
            .Select(map)
            .ToListAsync();
    }

    //    /**
    //     * 设置订单显示货币配置项
    //     * @param $OrderUnitConfigModel
    //     * @param $order_unit_config
    //     * @return bool
    //     */
    public ReturnStruct getOrderUnitConfigSave( /*$OrderUnitConfigModel, $order_unit_config*/)
    {
        //        $res = 0;
        //        foreach ($order_unit_config as $key => $value) {
        //            if (array_key_exists($value, userModel::USER_CONFIG)) {
        ////                $fn = 'set'.$value;
        //                $res ^= userModel::USER_CONFIG[$value];
        //            }
        //        }
        //        $OrderUnitConfigModel->setOrderUnitConfig($res);
        ////        dd($OrderUnitConfigModel->getFlags(), $res);
        //        return $OrderUnitConfigModel->getFlags();
        return ReturnArr();
    }

    //    /**
    //     * 更新用户信息
    //     * User: CCY
    //     * Date: 2021/2/4
    //     * @param $id
    //     * @param $updateData
    //     * @return int
    //     */
    public ReturnStruct updateUser( /*$id, $updateData*/)
    {
        //        return userModel::query()->where('id', $id)->update($updateData);
        return ReturnArr();
    }

    //    /****************************** 开放注册 **********************************************/

    /// <summary>
    /// 注册用户
    /// </summary>
    /// <param name="dto"></param>
    /// <param name="registerInfo"></param>
    /// <returns></returns>
    public async Task<Model> InitRegisterUser(SubInfoDTO m, PackageModel registerInfo)
    {
        var insertData = new Model
        {
            UserName = m.UserName,
            TrueName = m.UserName,
            Password = m.Password,
            Mobile = m.Mobile,
            Email = "",
            PhoneNumberConfirmed = m.Mobile.IsNotWhiteSpace(),
            OrderUnitConfig = OrderUnitConfig.Default, /*4611686018427388295*/
            FormRuleTemplateID = registerInfo.RuleTemplateId,
            Pid = registerInfo.Uid,
            State = StateEnum.Open,
            Pids = registerInfo.Pids,
            CreateCompanyId = registerInfo.CompanyID,
            Role = Role.ADMIN,
            IsDistribution = false,
            Sort = -DateTime.Now.GetTimeStamp(),
            Level = 2,
            ThemeId = 0,
            OEMID = registerInfo.OEMID,
            CompanyID = Session.GetCompanyID(),
            GroupID = Session.GetGroupID(),
            CreatedAt = DateTime.Now,
            OrderRateConfig = Model.OrderRateConfigs.REALTIME,
        };
        insertData.PasswordHash = _userManager.PasswordHasher.HashPassword(insertData, insertData.Password);
        var res = await _userManager.CreateAsync(insertData);
        if (res.Succeeded)
        {
            var ret = await _userManager.AddToRoleAsync(insertData, insertData.Role);
            if (ret.Succeeded)
            {
                return insertData;
            }
            else
            {
                throw new Exception(string.Join(",", ret.Errors.Select(e => e.Description)));
            }
        }
        else
        {
            throw new Exception(string.Join(",", res.Errors.Select(e => e.Description)));
        }
    }


    //    /**
    //     * 更新公司管理员绑定权限
    //     * User: CCY
    //     * Date: 2020/12/17
    //     * @param $rule_id
    //     * @param $company_id
    //     * @return int
    //     */
    public ReturnStruct updateRule( /*$rule_id, $company_id*/)
    {
        //        $where = [
        //            ['company_id', '=', $company_id],
        //            ['concierge', '=', userModel::TYPE_OWN]
        //        ];
        //        return userModel::query()->where($where)->update(['rule_id' => $rule_id]);
        return ReturnArr();
    }

    //    /**
    //     * 更新用户信息
    //     * User: CCY
    //     * Date: 2021/1/22
    //     * @param $id
    //     * @param $updateData
    //     * @return int
    //     */
    public async Task<ReturnStruct> UpdateDataById(Model user)
    {
        //        return userModel::query()->where('id', $id)->update($updateData);
        return await Task.FromResult(ReturnArr());
    }

    //    /**
    //     * 更换免费版、个人版 禁用员工 更新用户信息
    //     * User: CCY
    //     * Date: 2021/1/22
    //     * @param $where
    //     * @param $updateData
    //     * @return int
    //     */
    public async Task<ReturnStruct> UpdateUserData(int companyId)
    {
        var list = await _dbContext.User.Where(a => a.CompanyID == companyId)
            .Where(a => a.Role == Role.Employee)
            .ToListAsync();

        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            foreach (var item in list)
            {
                item.State = StateEnum.Close;
            }

            _dbContext.User.UpdateRange(list);
            await _dbContext.SaveChangesAsync();

            #region 统计-禁用员工数

            await _statisticService.Reject(_SessionProvider.Session, Statistic.NumColumnEnum.EmployeeReject,
                true, list.Count);

            #endregion

            await transaction.CommitAsync();
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            Console.WriteLine(e.Message);
            throw new Exception(e.Message);
        }


        return ReturnArr();
    }

    /// <summary>
    /// 获取指定公司下的独立账号
    /// </summary>
    /// <param name="companyIds"></param>
    /// <param name="username"></param>
    /// <param name="mobile"></param>
    /// <returns></returns>
    public Task<List<Model>> GetOwnUser(IEnumerable<int>? companyIds, string? username = "", string? mobile = "")
    {
        return _dbContext.User
            .WhenWhere(companyIds is not null && companyIds.Any(),
                u => companyIds!.Contains(u.CompanyID) && u.Role == Role.ADMIN)
            .WhenWhere(!string.IsNullOrEmpty(username), u => u.UserName == username)
            .WhenWhere(!string.IsNullOrEmpty(mobile), u => u.Mobile == mobile)
            .ToListAsync();
    }

    #endregion

    //    public function rule()
    //    {
    //        return $this->hasOne(Rule::class, 'id', 'rule_id');
    //    }

    //public function company()
    //{
    //    return $this->hasOne(Company::class, 'id', 'company_id');
    //    }

    //    public function scopeName(Builder $query, $name)
    //{
    //    if ($name) {
    //    return $query->where('username', 'like', "%{$name}%")->orWhere('truename', 'like', "%{$name}%");
    //}
    //    }

    //    public function scopeId(Builder $query, $id)
    //{
    //    if ($id) {
    //    return $query->where('id', '=', $id);
    //}
    //    }

    //    public function scopeNoId(Builder $query, $id)
    //{
    //    if ($id) {
    //    return $query->where('id', '!=', $id);
    //}
    //    }

    //    public function scopeInIds(Builder $query, $id)
    //{
    //    if (checkArr($id))
    //    {
    //        return $query->whereIn('id', $id);
    //    }
    //}

    //public function scopeSort(Builder $query)
    //{
    //    return $query->orderBy('sort', 'asc');
    //}

    //public function scopeStateSort(Builder $query)
    //{
    //    return $query->orderBy('state', 'asc');
    //}

    //public function theme()
    //{
    //    return $this->hasOne('App\Models\Setting\Theme', 'id', 'theme_id');
    //}

    /// <summary>
    /// 获取当前登录用户
    /// </summary>
    /// <returns></returns>
    public Task<Model?> GetCurrentUser()
    {
        var userId = Session.GetUserID();
        return _dbContext.User.SingleOrDefaultAsync(u => u.Id == userId);
    }

    public async Task<Model?> GetCurrentUser(int id)
    {
        var users = await _dbContext.User.Where(u => u.Id == id).AsNoTracking().ToListAsync();
        if (users.Any())
        {
            return users.First();
        }
        else
        {
            return null;
        }
    }

    /************************************************************组配置******************************************************************/
    /// <summary>
    /// 获取当前公司下用户信息
    /// </summary>
    /// <returns></returns>
    public async Task<List<Model>> GetUserByCompanyId()
    {
        return await _dbContext.User.Where(a => a.CompanyID == CompanyID).ToListAsync();
    }

    /// <summary>
    /// 获取当前公司下店铺信息
    /// </summary>
    /// <returns></returns>
    public async Task<List<StoreModel>> GetStoreByCompanyId()
    {
        return await _dbContext.StoreRegion
            .Where(s => !s.IsDel)
            .Where(s => s.State == StateEnum.Open)
            .Where(s => s.CompanyID == CompanyID).ToListAsync();
    }

    /// <summary>
    /// 获取组下用户
    /// </summary>
    /// <param name="groupId"></param>
    /// <returns></returns>
    public async Task<List<Model>> GetGroupUserList(int groupId)
    {
        return await _dbContext.User.Where(a => a.GroupID == groupId).ToListAsync();
    }

    /// <summary>
    /// 获取组下店铺
    /// </summary>
    /// <param name="groupId"></param>
    /// <returns></returns>
    public async Task<List<StoreModel>> GetGroupStoreList(int groupId)
    {
        return await _dbContext.StoreRegion
            .Where(s => !s.IsDel)
            .Where(a => a.BelongsGroupID == groupId)
            .Where(s => s.State == StateEnum.Open)
            .ToListAsync();
    }

    /// <summary>
    /// 编辑组下用户
    /// </summary>
    /// <param name="groupId"></param>
    /// <param name="userIds"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    public async Task EditGroupUserList(int groupId, List<int> userIds)
    {
        if (await _dbContext.UserGroup.SingleOrDefaultAsync(g => g.ID == groupId) == null)
            throw new Exception($"未查询到[ID:{groupId}]的组");
        var list = await GetUserByCompanyId();
        foreach (var item in list)
        {
            // item.OpenUserGroupID = null;
            if (userIds.Contains(item.ID))
            {
                item.GroupID = groupId;
            }
            else
            {
                item.GroupID = GroupID;
            }
        }

        _dbContext.User.UpdateRange(list);
        await _dbContext.SaveChangesAsync();
    }

    /// <summary>
    /// 编辑组下店铺
    /// </summary>
    /// <param name="groupId"></param>
    /// <param name="storeIds"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    public async Task EditGroupStoreList(int groupId, List<int> storeIds)
    {
        if (await _dbContext.UserGroup.SingleOrDefaultAsync(g => g.ID == groupId) == null)
            throw new Exception($"未查询到[ID:{groupId}]的组");
        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            //需要删除的用户组
            var reduceGroupIds = new Dictionary<int, List<int>>();
            var plusGroupIds = new Dictionary<int, List<int>>();
            var list = await GetStoreByCompanyId();
            foreach (var item in list)
            {
                // if (item.BelongsGroupID.HasValue &&  item.BelongsGroupID.Value != groupId)
                // {
                //     reduceGroupIds.Add(item.BelongsGroupID.Value);
                // }
                // //更换绑定用户组
                // if (item.BelongsGroupID.HasValue && item.BelongsGroupID != groupId)
                // {
                //     var amount = await _statisticService.AmountByTheStoreWithGroup(item.ID, item.BelongsGroupID.Value);
                //
                //     await _statisticMoney.StoreFinancialWithGroup(item.BelongsGroupID.Value, amount);
                // }
                
                
                //将所有店铺的绑定的用户组id设置为null
                // item.BelongsGroupID = null;
                //根据选中的店铺设置用户组
                if (storeIds.Contains(item.ID))
                {
                    //原店铺绑定用户组发生变化需要减财务
                    if (item.BelongsGroupID.HasValue && item.BelongsGroupID.Value != groupId)
                    {
                        var belongsGroupId = item.BelongsGroupID.Value;
                        if (reduceGroupIds.Keys.Contains(belongsGroupId))
                        {
                            reduceGroupIds[belongsGroupId].Add(item.ID);
                        }
                        else
                        {
                            reduceGroupIds.Add(belongsGroupId,new List<int>(){item.ID});
                        }
                    }
                    //原店铺绑定用户组发生变化需要加财务
                    if (item.BelongsGroupID == null || item.BelongsGroupID.Value != groupId)
                    {
                        var belongsGroupId = groupId;
                        if (plusGroupIds.Keys.Contains(belongsGroupId))
                        {
                            plusGroupIds[belongsGroupId].Add(item.ID);
                        }
                        else
                        {
                            plusGroupIds.Add(belongsGroupId,new List<int>(){item.ID});
                        }
                    }
                    
                    item.BelongsGroupID = groupId;
                }
            }
            // //将所有用户组的订单相关财务全部置为0
            // var groups = await _groupService.GetGroupsByCompanyId(CompanyID);
            // var groupIds = groups.Select(m => m.ID).ToList();
            // await _statisticService.RestOrderFiance(groupIds, Types.UserGroup);
            // //新绑定的店铺增加财务
            // var belongStore = list.Where(m => m.BelongsGroupID != null).ToList();
            // foreach (var store in belongStore)
            // {
            //     var groupIdList = new List<int>() { groupId };
            //     var groupStoreAmountToPlus = _statisticService.GroupAmountByTheStore(groupIdList, store);
            //     if(groupStoreAmountToPlus.Count > 0)
            //         await _statisticMoney.PlusStoreOrderFianceToGroup(groupIdList, groupStoreAmountToPlus);
            // }
            
            //店铺被移除绑定处理财务
            foreach (var item in reduceGroupIds)
            {
                var groupIdList = new List<int>() { item.Key };
                var stores = list.Where(m => item.Value.Contains(m.ID)).ToList();
                foreach (var store in stores)
                {
                    var groupStoreAmount = _statisticService.GroupAmountByTheStore(groupIdList, store);
                    if(groupStoreAmount.Count > 0)
                        await _statisticMoney.ReduceStoreOrderFianceToGroup(groupIdList, groupStoreAmount);
                }
            }
            
            //店铺新增绑定处理财务
            foreach (var item in plusGroupIds)
            {
                var groupIdList = new List<int>() { item.Key };
                var stores = list.Where(m => item.Value.Contains(m.ID)).ToList();
                foreach (var store in stores)
                {
                    var groupStoreAmount = _statisticService.GroupAmountByTheStore(groupIdList, store);
                    if(groupStoreAmount.Count > 0)
                        await _statisticMoney.PlusStoreOrderFianceToGroup(groupIdList, groupStoreAmount);
                }
            }
            
            _dbContext.StoreRegion.UpdateRange(list);
            await _dbContext.SaveChangesAsync();
            await transaction.CommitAsync();
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            Console.WriteLine(e);
            throw;
        }
        
    }

    /// <summary>
    /// 获取员工绑定的店铺
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    public async Task<List<UserStoreModel>> GetUserBindStoreList(int userId)
    {
        return await _dbContext.UserStore.Where(a => a.CorrelationUserID == userId).ToListAsync();
    }

    /// <summary>
    /// 获取店铺绑定的员工
    /// </summary>
    /// <param name="uerId"></param>
    /// <returns></returns>
    public async Task<List<UserStoreModel>> GetStoreBindUserList(int storeId)
    {
        return await _dbContext.UserStore.Where(a => a.CorrelationStoreID == storeId).ToListAsync();
    }

    /// <summary>
    /// 获取组-店铺tree
    /// </summary>
    /// <returns></returns>
    public async Task<object> GetGroupStoreTree()
    {
        var stores = await _dbContext.StoreRegion
            .Where(s => !s.IsDel)
            .Where(s => s.State == StateEnum.Open)
            .Where(a => a.CompanyID == CompanyID).Include(s => s.BelongsGroup)
            .ToListAsync();
        return stores.GroupBy(s => new { s.BelongsGroupID, s.BelongsGroup })
            .Select(g => new
            {
                id = g.Key.BelongsGroupID is not null ? $"group:{g.Key.BelongsGroupID}" : "null",
                label = g.Key.BelongsGroupID is not null ? g.Key.BelongsGroup!.Name : "暂无分组",
                children = g.Select(s => new
                {
                    id = s.ID,
                    label = s.Name
                })
            });
    }

    /// <summary>
    /// 获取组-员工tree
    /// </summary>
    /// <returns></returns>
    public async Task<object> GetGroupUserTree()
    {
        var users = await _dbContext.User.Where(a => a.CompanyID == CompanyID).Include(s => s.BelongsGroup)
            .ToListAsync();
        return users.GroupBy(s => new { BelongsGroupID = s.GroupID, s.BelongsGroup })
            .Select(g => new
            {
                id = $"group:{g.Key}",
                label = g.Key.BelongsGroup!.Name,
                children = g.Select(s => new
                {
                    id = s.ID,
                    label = s.TrueName
                })
            });
    }

    /// <summary>
    /// 员工绑定店铺 edit
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="storeIds"></param>
    /// <returns></returns>
    public async Task UserBindStore(int userId, List<int> storeIds)
    {
        
        
        
        var stores = await _dbContext.StoreRegion
            .Where(m => !m.IsDel)
            .Where(s => s.State == StateEnum.Open)
            .Where(s => storeIds.Contains(s.ID)).ToListAsync();
        var userStores = await GetUserBindStoreList(userId);

        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            //需要移除的用户的ID
            var removeStoreIds = userStores.Select(m => m.CorrelationStoreID).ToList();
            //现有绑定对即将绑定的差集这是需要删除财务的用户 解除绑定的用户的ID 
            var opReduceStoreIds = removeStoreIds.Except(storeIds).ToList();

            foreach (var storeId in opReduceStoreIds)
            {
                var store = await _dbContext.StoreRegion.Where(m => m.ID == storeId).FirstAsync();
                var data = new List<int>(){userId};
                //店铺解绑清理与用户相关的订单财务，不处理用户的采购物流金额
                var userStoreAmountToReduce = _statisticService.UserAmountByTheStore(data, store);
                if(userStoreAmountToReduce.Count > 0)
                    await _statisticMoney.ReduceStoreOrderFianceToUser(data, userStoreAmountToReduce);
                
                //店铺解绑清理与用户组相关的订单财务，不处理用户组的采购物流金额
                var reduceGroupIds = await GetUserGroupIds(data);
                var groupStoreAmountToReduce = _statisticService.GroupAmountByTheStore(reduceGroupIds, store);
                if(groupStoreAmountToReduce.Count > 0)
                    await _statisticMoney.ReduceStoreOrderFianceToGroup(reduceGroupIds, groupStoreAmountToReduce);
            }
            
            //即将绑定对现有绑定的差集这是需要增加财务的用户 新增绑定的用户的ID 
            var opPlusStoreIds = storeIds.Except(removeStoreIds).ToList();
            
            foreach (var storeId in opPlusStoreIds)
            {
                var store = await _dbContext.StoreRegion.Where(m => m.ID == storeId).FirstAsync();
                var data = new List<int>(){userId};
                //店铺绑定加回之前与用户相关的订单财务，不处理用户的采购物流金额
                var userStoreAmountToPlus = _statisticService.UserAmountByTheStore(data, store);
                if(userStoreAmountToPlus.Count > 0)
                    await _statisticMoney.PlusStoreOrderFianceToUser(data, userStoreAmountToPlus);
                
                //店铺绑定加回之前与用户组相关的订单财务，不处理用户组的采购物流金额
                var plusGroupIds = await GetUserGroupIds(data);
                var groupStoreAmountToPlus = _statisticService.GroupAmountByTheStore(plusGroupIds, store);
                if(groupStoreAmountToPlus.Count > 0)
                    await _statisticMoney.PlusStoreOrderFianceToGroup(plusGroupIds, groupStoreAmountToPlus);
            }
        
            
            _dbContext.UserStore.RemoveRange(userStores);
            var list = new List<UserStoreModel>();
            foreach (var storeId in storeIds)
            {
                var item = stores.FirstOrDefault(a => a.ID == storeId);
                if (item != null)
                {
                    list.Add(new UserStoreModel(Session)
                    {
                        CorrelationStoreID = storeId,
                        CorrelationUserID = userId,
                        Platform = item.Platform,
                    }); 
                }
            
            }
            await _dbContext.UserStore.AddRangeAsync(list);
            await _dbContext.SaveChangesAsync();
            await transaction.CommitAsync();
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            throw new Exception(e.Message);
        }
    }

    /// <summary>
    /// 店铺绑定员工 edit
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="storeIds"></param>
    /// <returns></returns>
    public async Task StoreBindUser(int storeId, List<int> userIds)
    {
        var store = await _dbContext.StoreRegion.SingleOrDefaultAsync(s => s.ID == storeId);
        if (store == null) throw new Exception($"未找到ID为[{storeId}]的店铺");
        var userStores = await GetStoreBindUserList(storeId);
        
        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {

            //需要移除的用户的ID
            var removeUserIds = userStores.Select(m => m.CorrelationUserID).ToList();
            
            //现有绑定对即将绑定的差集这是需要删除财务的用户 解除绑定的用户的ID 
            var opReduceUserIds = removeUserIds.Except(userIds).ToList();
            //店铺解绑清理与用户相关的订单财务，不处理用户的采购物流金额
            var userStoreAmountToReduce = _statisticService.UserAmountByTheStore(opReduceUserIds, store);
            if(userStoreAmountToReduce.Count > 0)
                await _statisticMoney.ReduceStoreOrderFianceToUser(opReduceUserIds, userStoreAmountToReduce);
            //店铺解绑清理与用户组相关的订单财务，不处理用户组的采购物流金额
            var reduceGroupIds = await GetUserGroupIds(opReduceUserIds);
            var groupStoreAmountToReduce = _statisticService.GroupAmountByTheStore(reduceGroupIds, store);
            if(groupStoreAmountToReduce.Count > 0)
                await _statisticMoney.ReduceStoreOrderFianceToGroup(reduceGroupIds, groupStoreAmountToReduce);
           
            //即将绑定对现有绑定的差集这是需要增加财务的用户 新增绑定的用户的ID 
            var opPlusUserIds = userIds.Except(removeUserIds).ToList();
            //店铺绑定加回之前与用户相关的订单财务，不处理用户的采购物流金额
            var userStoreAmountToPlus = _statisticService.UserAmountByTheStore(opPlusUserIds, store);
            if(userStoreAmountToPlus.Count > 0)
                await _statisticMoney.PlusStoreOrderFianceToUser(opPlusUserIds, userStoreAmountToPlus);
            
            //店铺绑定加回之前与用户组相关的订单财务，不处理用户组的采购物流金额
            var plusGroupIds = await GetUserGroupIds(opPlusUserIds);
            var groupStoreAmountToPlus = _statisticService.GroupAmountByTheStore(plusGroupIds, store);
            if(groupStoreAmountToPlus.Count > 0)
                await _statisticMoney.PlusStoreOrderFianceToGroup(plusGroupIds, groupStoreAmountToPlus);
            
            
            

            _dbContext.UserStore.RemoveRange(userStores);
            var list = new List<UserStoreModel>();
            foreach (var userId in userIds)
            {
                list.Add(new UserStoreModel(Session)
                {
                    CorrelationStoreID = storeId,
                    CorrelationUserID = userId,
                    Platform = store.Platform,
                });
            }

            await _dbContext.AddRangeAsync(list);
            await _dbContext.SaveChangesAsync();
            await transaction.CommitAsync();
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            Console.WriteLine(e);
            throw;
        }
        
       
        
    }


    /// <summary>
    /// Save User Claims
    /// </summary>
    /// <param name="id"></param>
    /// <param name="userId"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    public async Task<bool> SaveUserClaims(int id, int userId)
    {
        var ruleTemplate = await _dbContext.RuleTemplate.AsNoTracking().FirstOrDefaultAsync(a => a.ID == id);
        if (ruleTemplate == null)
        {
            throw new Exception("没有查询到权限信息");
        }

        await _dbContext.UserClaims.Where(m => m.UserId == userId).DeleteAsync();

        var claims = ruleTemplate.GetClaims(OEMID);

        _dbContext.UserClaims.AddRange(claims.Select(m => new UserClaim(userId, m)));

        return await _dbContext.SaveChangesAsync() > 0;
    }

    public async Task<bool> UpdateAgency(UpdateAgencyDto req)
    {
        if (_SessionProvider.Session.GetCompanyID() != 2)
        {
            throw new PermissionException("无权操作#1");
        }

        var info = await GetCompanyById(req.CompanyId);

        if (info is null)
        {
            throw new NotFoundException("找不到相关数据#2");
        }

        info.IsAgency = req.IsAgency != IsAgencyEnum.No;

        await _dbContext.Company.SingleUpdateAsync(info);

        return await _dbContext.SaveChangesAsync() > 0;
    }

    private async Task<Models.DB.Users.Company?> GetCompanyById(int companyId)
    {
        return await _dbContext.Company.Where(a => a.ID == companyId).FirstOrDefaultAsync();
    }

    public async Task<bool> UpdateState(UpdateStateDto req)
    {
        //无权操作
        if (_SessionProvider.Session.GetCompanyID() != Enums.ID.Company.XiaoMi.GetNumber())
        {
            return false;
        }
        
        string text;
        StateEnum updateState;
        if (req.State == StateEnum.Open)
        {
            //启用->禁用
            //独立账户，公司下员工
            var userInfo = await getUidsByCompanyId(req.CompanyId);
            // updateState = StateEnum.Close;
            updateState = StateEnum.AdminToDisable;
            // text = "禁用用户信息";
            text = "管理员强制禁用用户，下级账号无权开启";
        }
        else
        {
            //禁用->启用
            updateState = StateEnum.Open;
            text = "启用用户信息";
        }

        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
           await _dbContext.User.Where(m => m.CompanyID == req.CompanyId).UpdateAsync(m => new
            {
                State = updateState
            });
           
           await _dbContext.Company.Where(m => m.ID == req.CompanyId).UpdateAsync(m => new
           {
               State = Enum.Parse<Models.DB.Users.Company.States>(updateState.GetNumber().ToString())
           });

            await  _userOperateLogService.AddUserLog(UserID, text);
           
            await transaction.CommitAsync();
            return true;
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            Console.WriteLine(e.Message);
            throw new Exception(e.Message);
            throw;
        }
    }

    public async Task<bool> BindMobile(ERP.Models.DB.Identity.User user,SmsCodeModel sms, string mobile)
    {
        
        var transaction = _dbContext.Database.BeginTransaction();
        try
        {
            //绑定手机号
            user.PhoneNumber = mobile;
            user.PhoneNumberConfirmed = true;
            
            //使用短信验证码
            //sms.Used = true;
            await _dbContext.SaveChangesAsync();
            await transaction.CommitAsync();
            return true;
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            throw new Exceptions.Exception(e.Message);
        }
        
    }

    public async Task<List<UserOptionsVM>> GetUserOptions(int? companyId = null)
    {
        if (!companyId.HasValue)
            companyId = _SessionProvider.Session.GetCompanyID();
        
        return await _dbContext.User
            .Where(m => m.CompanyID == companyId).Select(m => new UserOptionsVM(m))
            .ToListAsync();
    }


    public async Task<bool> SetUserFirstLoginWeb(int userId, DateTime now)
    {
        await _dbContext.User.Where(m => m.Id == userId).UpdateAsync(m => new { FirstLoginWeb = now });
        return await _dbContext.SaveChangesAsync() > 1;
    }

    public async Task<bool> SetUserState(SetUserStateDto req)
    {
        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            var user = await _userManager.FindByIdAsync(req.UserId.ToString());
            if (user.State == StateEnum.AdminToDisable && Session.GetCompanyID() != Enums.ID.Company.XiaoMi.GetNumber())
            {
                throw new StorageDomainException("无权操作");
            }

            if (user.Role == Role.ADMIN)
            {
                await _dbContext.Company.Where(m => m.ID == user.CompanyID).UpdateAsync(m => new
                {
                    State = Enum.Parse<Models.DB.Users.Company.States>(req.State.GetNumber().ToString())
                });
            }
            
            user.State = req.State;
            await _userManager.UpdateAsync(user);
            await _userOperateLogService.AddUserLog(req.UserId, $"{req.State.GetDescription()}账户");
            await transaction.CommitAsync();
            return true;
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            Console.WriteLine(e);
            throw;
        }
    }

    public async Task<List<int>> GetUserGroupIds(List<int> userIds)
    {
        return await _dbContext.User
            .Where(m => userIds.Contains(m.Id) && GroupID != 0)
            .Select(m => m.GroupID)
            .ToListAsync();
    }
}