﻿using System.Collections;
using ERP.Extensions;
using ERP.Models.Product;
using ERP.Models.View.Product.Category;
using ERP.Services.Product;
using Microsoft.EntityFrameworkCore;
using ERP.Data;
namespace ERP.Services.DB.Product;

public class CategoryServiceBak : BaseService
{
    public readonly IHttpContextAccessor _httpContextAccessor;

    public readonly DBContext _dbContext;

    //public readonly HttpRequest _request;

    public CategoryServiceBak(IHttpContextAccessor httpContextAccessor, DBContext dbContext)
    {
        _httpContextAccessor = httpContextAccessor;
        _dbContext = dbContext;
        // 空引用 ?
        //_request = _httpContextAccessor.HttpContext.Request;
    }

    public async Task<ReturnStruct> GetCategoryList(int type = (int) CategoryModel.Types.TYPE_FALSE)
    {
        var list = await _dbContext.Category.Where(a => a.Type == (CategoryModel.Types) type).ToListAsync();

        //return ReturnArr(true, "", ArrayLevel(list));
        return ReturnArr(data: list);
    }

    public async Task<ReturnStruct> CategoryInfo(int id, CategoryModel.Types type = CategoryModel.Types.TYPE_FALSE)
    {
        var rs = await _dbContext.Category.FirstOrDefaultAsync(a => a.ID == id && a.Type == type);

        return ReturnArr(true, "", rs);
    }

    /// <summary>
    /// 删除分类
    /// </summary>
    /// <param name="id"></param>
    /// <param name="type"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DelCategory(int id, CategoryModel.Types type = CategoryModel.Types.TYPE_FALSE)
    {
        // 判断是否可以删除
        var info = await _dbContext.Category.FirstOrDefaultAsync(a => a.ID == id && a.Type == type);
        if (info == null)
        {
            return ReturnArr(false, "该分类不存在");
        }

        if (info.Level == 1)
        {
            //是否存在子类
            var subCount = _dbContext.Category.Where(a => a.Pid == id).Count();
            if (subCount > 0)
            {
                return ReturnArr(false, "改分类下面存在子类，请先删除子类");
            }
        }

        return ReturnArr(true, "删除成功", _dbContext.Category.Remove(info));
    }

    public async Task<ReturnStruct> AddOrUpdateCategory(EditVM req)
    {
        var id = req.ID;
        //$whereRepeat = $this->productWhereDataRange(self::RANGE);
        //if (!count($whereRepeat))
        //{
        //    return returnArr(false, '缺少可见范围');
        //}
        //$userId = $this->getSession()->getUserId();
        //$groupId = $this->getSession()->getGroupId();
        //$companyId = $this->getSession()->getCompanyId();
        //$oemId = $this->getSession()->getOemId();

        var level = (req.Pid == 0) ? 2 : 1;

        // hashCode 去重
        //$hash_code = createHashCode($data['name']);
        //var hash_code = data.Name;
        var hash_code = 1;
        var count = await _dbContext.Category
            //.Where(a => a.HashCode == hash_code)
            .Where(a => a.Level == level)
            //.Where(a => a.Company.ID == companyID )
            .WhenWhere(id != 0, a => a.ID != id).CountAsync();
        if (count > 0)
        {
            return ReturnArr(false, "分类名称重复");
        }


        if (id > 0)
        {
            var categoryModel = await _dbContext.Category.FirstOrDefaultAsync(a => a.ID == id);
            if (categoryModel == null)
            {
                return ReturnArr(false, "分类不存在");
            }

            // if (categoryModel.UpdatedAt != req.UpdatedAtTicks)
            // {
            //     return ReturnArr(false, "数据不同步");
            // }

            // categoryModel.Type = type;
            // categoryModel.Name = data.Name;
            categoryModel.HashCode = hash_code;
            _dbContext.Category.Update(categoryModel);
            return ReturnArr(true, "修改成功");
        }
        else
        {
            CategoryModel categoryModelModel = new();
            //$categoryModel = new CategoryModel();
            //$categoryModel->user_id = $userId;
            //$categoryModel->group_id = $groupId;
            //$categoryModel->company_id = $companyId;
            //$categoryModel->oem_id = $oemId;
            // categoryModelModel.Pid = data.Pid;
            categoryModelModel.Level = level;
            // _dbContext.Category.Add(categoryModel);
            // categoryModelModel.Type = type;
            // categoryModelModel.Name = data.Name;
            categoryModelModel.HashCode = hash_code;
            await _dbContext.SaveChangesAsync();
            return ReturnArr(true, "添加成功");
        }
    }

    /// <summary>
    /// 数组层级缩进转换
    /// </summary>
    /// <param name="array">源数组</param>
    /// <param name="pid"></param>
    /// <param name="level"></param>
    /// <returns></returns>
    private ArrayList ArrayLevel(List<CategoryModel> array, int pid = 0, int level = 1)
    {
        ArrayList list = new();
        foreach (CategoryModel item in array)
        {
            if (item.Pid == pid)
            {
                item.Level = level;
                list.Add(item);
                ArrayLevel(array, item.ID, level + 1);
            }
        }

        return list;
    }

    /// <summary>
    /// 获取一级分类
    /// </summary>
    /// <returns></returns>
    public async Task<ReturnStruct> FirstCategory(CategoryModel.Types Type = CategoryModel.Types.TYPE_FALSE)
    {
        //$where[] = ['type', '=', $type];
        //$where[] = ['level', '=', CategoryModel::FIRST];
        var parentCategory = await _dbContext.Category.Where(a => a.Type == Type)
            .Where(a => a.Level == 1)
            .Select(a => new {a.ID, a.Name})
            .ToListAsync();

        parentCategory.Insert(0, new {ID = 0, Name = "一级分类"});
        return ReturnArr(true, "", parentCategory);
    }

    public async Task<ReturnStruct> ParentCategory(CategoryModel.Types Type = CategoryModel.Types.TYPE_FALSE)
    {
        //$where[] = ['type', '=', $type];
        //$where[] = ['level', '=', CategoryModel::FIRST];
        var parentCategory = await _dbContext.Category.Where(a => a.Type == Type)
            .Where(a => a.Level == 1)
            .Select(a => new {a.ID, a.Name})
            .ToListAsync();

        return ReturnArr(true, "", parentCategory);
    }

    public  Task<ReturnStruct> Info(int id)
    {
        throw new NotImplementedException();
    }

    public  Task<ReturnStruct> Destroy(int id)
    {
        throw new NotImplementedException();
    }
}