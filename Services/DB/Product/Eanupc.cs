using ERP.Extensions;
using ERP.Models.Product;
using Microsoft.EntityFrameworkCore;
using System.Collections;
using ERP.Models.DB.Stores;
using ERP.Data;
using ERP.Models.Stores;

namespace ERP.Services.Product;


public class Eanupc : BaseService
{
    public readonly IHttpContextAccessor _httpContextAccessor;

    public readonly DBContext _dbContext;

    public readonly HttpRequest _request;

    public Eanupc(IHttpContextAccessor httpContextAccessor, DBContext dbContext)
    {
        _httpContextAccessor = httpContextAccessor;
        _dbContext = dbContext;
        // 空引用 ?
        _request = _httpContextAccessor.HttpContext.Request;
    }

    public async Task<ReturnStruct> List()
    {
        _request.TryGetValue("value", out var value);
        _request.TryGetValue("type", out var type);
        _request.TryGetValue("selectMode", out var selectMode);

        var list = await _dbContext.Eanupc.OrderByDescending(a => a.ID).ToPaginateAsync();

        return list.Items.Count > 0 ? ReturnArr(true, "", list) : ReturnArr(false);
    }

    //没做
    public async Task<ReturnStruct> Add()
    {
        //检测用户EAN/UPC数量
        return await System.Threading.Tasks.Task.FromResult(ReturnArr(true, ""));
    }

    public async Task<ReturnStruct> Del(int id)
    {
        var eanupc = await GetInfo(id);
        if (eanupc != null)
        {
            _dbContext.Eanupc.Remove(eanupc);
            var count = await _dbContext.SaveChangesAsync();
        }

        return ReturnArr(true, "");
    }

    public async Task<ReturnStruct> Clear(int mode)
    {
        //if ($mode == 3) {
        //    EanupcModel::query()->where($where)->delete();
        //} else
        //{
        //    EanupcModel::query()->where($where)->type($mode)->delete();
        //}
        if (mode == 3)
        {
            await _dbContext.Eanupc.FindAsync();
        }
        else
        {
            //_dbContext.
        }

        return ReturnArr(true, "");
    }

    public async Task<ReturnStruct> Import(int mode)
    {
        //导入没弄....计算ean/upc
        return await System.Threading.Tasks.Task.FromResult(ReturnArr(true, ""));
    }

    /// <summary>
    /// 获取详情
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<EanupcModel?> GetInfo(int id)
    {
        return await _dbContext.Eanupc.FirstOrDefaultAsync(a => a.ID == id);
    }
}