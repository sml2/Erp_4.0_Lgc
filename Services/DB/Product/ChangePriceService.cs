using ERP.Data;
using ERP.Enums.Rule.Config.Internal;
using ERP.Exceptions.DataTemplate;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.DB.Product;
using ERP.Models.Template;
using ERP.Models.View.Product.Template;
using ERP.Models.View.Products.Template;
using Microsoft.EntityFrameworkCore;
using Enumerable = System.Linq.Enumerable;

namespace ERP.Services.Product;

public class ChangePriceService : BaseWithUserService
{
    public ChangePriceService(DBContext dbContext, ISessionProvider sessionProvider) : base(dbContext, sessionProvider)
    {
    }

    public IQueryable<ChangePriceTemplateModel>? DataWatchRange(DataRange_2b range)
    {
        if (range == DataRange_2b.None)
            return null;

        IQueryable<ChangePriceTemplateModel>? model =
            _dbContext.ChangePriceTemplate.Where(m => m.OEMID == _oemId && m.CompanyID == _companyId);

        if (range == DataRange_2b.Group)
            model = model.Where(m => m.GroupID == _groupId);

        if (range == DataRange_2b.User)
            model = model.Where(m => m.UserID == (_userId));

        return model;
    }

    public async Task<DbSetExtension.PaginateStruct<ChangePriceTemplateModel>> List(ListDto req, DataRange_2b range)
    {
        var model = DataWatchRange(range);

        if (model is null)
            return new DbSetExtension.PaginateStruct<ChangePriceTemplateModel>();

        return await model
            .WhenWhere(!string.IsNullOrEmpty(req.NationShort), a => a.NationShort.Equals(req.NationShort))
            .WhenWhere(!string.IsNullOrEmpty(req.Name), a => a.Name.Contains(req.Name))
            .OrderByDescending(a => a.UpdatedAt)
            .ToPaginateAsync(page: req.Page, limit: req.Limit);
    }

    public async Task<object> Get(DataRange_2b range)
    {
        var nations = new Dictionary<string, string>();
        var templates = new Dictionary<string, List<ChangePriceTemplateModel>>();

        var model = DataWatchRange(range);
        if (model is null)
        {
            return new
            {
                nations,
                templates,
                TemplatesArray = Enumerable.Empty<ChangePriceTemplateModel>()
            };
        }

        var data = await model.OrderByDescending(a => a.UpdatedAt).ToListAsync();
        foreach (var item in data)
        {
            if (!nations.Keys.Contains(item.NationShort))
            {
                nations.Add(item.NationShort, item.NationName);
            }
        }

        foreach (var item in data)
        {
            if (!templates.Keys.Contains(item.NationShort))
            {
                templates.Add(item.NationShort, new List<ChangePriceTemplateModel>() { item });
            }
            else
            {
                templates[item.NationShort].Add(item); 
            }
        }

        return new
        {
            nations,
            templates,
            TemplatesArray = data,
        };
    }

    private async Task<ChangePriceTemplateModel?> GetInfo(int id, DataRange_2b range)
    {
        var model = DataWatchRange(range);
        if (model is null)
        {
            return null;
        }

        return await model.FirstOrDefaultAsync(a => a.ID == id);
    }

    public async Task<ReturnStruct> AddOrUpdate(ChangePriceDto vm, DataRange_2b range)
    {
        if (vm.ID > 0)
        {
            var info = await GetInfo(vm.ID, range);
            if (info == null)
            {
                return ReturnArr(false, "该条改价模板不存在#1");
            }

            if (info.UpdatedAtTicks != vm.UpdatedAtTicks)
            {
                return ReturnArr(false, "数据不同步#2");
            }


            info.Value = vm.Value;
            info.Name = vm.Name;
            info.NationName = vm.NationName;
            info.NationShort = vm.NationShort;
            info.UpdatedAt = DateTime.Now;
            _dbContext.ChangePriceTemplate.Update(info);
        }
        else
        {
            var data = new ChangePriceTemplateModel(_session)
            {
                Value = vm.Value,
                Name = vm.Name,
                NationName = vm.NationName,
                NationShort = vm.NationShort,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
            };
            await _dbContext.ChangePriceTemplate.AddAsync(data);
        }

        await _dbContext.SaveChangesAsync();
        return ReturnArr(true, "");
    }

    public async Task<bool> Destroy(int id, DataRange_2b range)
    {
        var info = await GetInfo(id, range);
        if (info == null)
        {
            throw new NotFoundException("找不到相关数据#1");
        }

        _dbContext.ChangePriceTemplate.Remove(info);
        return await _dbContext.SaveChangesAsync() > 0;
    }

    public async Task<ChangePriceTemplateModel?> Info(int id, DataRange_2b range) => await GetInfo(id, range);


    // public async Task<List<string>> GetAllNation()
    // {
    //     return await _dbContext.ChangePriceTemplate
    //         .Where(m => m.OEMID == _oemId && m.CompanyID == _companyId)
    //         .Select(m => m.NationShort)
    //         .ToListAsync();
    // }
}