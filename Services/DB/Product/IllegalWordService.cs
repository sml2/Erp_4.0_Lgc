﻿using ERP.Authorization.Rule.Component;
using ERP.Extensions;
using ERP.Models.Product;
using Microsoft.EntityFrameworkCore;
using ERP.Data;
using ERP.Enums.ID;
using ERP.Enums.Rule.Config.Internal;
using ERP.Exceptions.IllegalWords;
using ERP.Interface;
using ERP.Interface.Rule;
using ERP.Models.View.Product.IllegalWord;
using ERP.Services.Identity;
using ERP.ViewModels.IllegalWord;

namespace ERP.Services.Product;

public class IllegalWordService : BaseWithUserService
{
    public IllegalWordService(DBContext dbContext, ISessionProvider sessionProvider) : base(dbContext, sessionProvider)
    {
    }

    private ISession Session
    {
        get => _sessionProvider.Session!;
    }

    private IQueryable<IllegalWordModel>? DataWatchRange(DataRange_2b range)
    {
       
        if (range == DataRange_2b.None)
            return null;

        var model = _dbContext.IllegalWord.Where(m => m.OEMID == _oemId && m.CompanyID == _companyId);

        if (range == DataRange_2b.Group)
            model = model.Where(m => m.GroupID == _groupId);

        if (range == DataRange_2b.User)
            model = model.Where(m => m.UserID == (_userId));

        return model;
    }

    public async Task<DbSetExtension.PaginateStruct<IllegalWordModel>> List(ListDto req, DataRange_2b range)
    {
        var model = DataWatchRange(range);

        if (model is null)
            return new DbSetExtension.PaginateStruct<IllegalWordModel>();

        return await model
            .WhenWhere(req.Select == 1 && req.Value != null, o => o.Value.Contains(req.Value))
            .WhenWhere(req.Select == 2 && req.Value != null, o => o.Value.Equals(req.Value))
            .ToPaginateAsync(page: req.Page, limit: req.Limit);
    }

    /// <summary>
    /// 删除违禁词
    /// </summary>
    /// <param name="id"></param>
    /// <param name="range"></param>
    /// <returns></returns>
    public async Task<bool> Del(List<int> id, DataRange_2b range)
    {

        var model = DataWatchRange(range);

        if (model is null)
        {
            return false;
        }
        
        var list = model.Where(a => id.Contains(a.ID));
        _dbContext.IllegalWord.RemoveRange(list);
        await _dbContext.SaveChangesAsync();
        return true;
    }

    public async Task<ReturnStruct> Add(IllegalWordEditDto req)
    {
        var userId = _userId;
        var groupId = _groupId;
        var companyId = _companyId;
        var oemId = _oemId;

        var date = DateTime.Now;
        if (req.Value.Contains(','))
        {
            var value = req.Value.Replace("\n", "");
            var values = value.Trim(',').Split(",");
            if (values.Count() >= 255)
            {
                return ReturnArr(false, "批量违禁词重复时，不可同时添加超过255个");
            }

            List<IllegalWordModel> insertArr = new();

            var isRepeat = false;
            var isTooLength = false;

            for (var index = 0; index < values.Length; index++)
            {
                var val = values[index];

                if (!string.IsNullOrEmpty(val))
                {
                    if (val.Length >= 255)
                    {
                        isTooLength = true;
                        break;
                    }

                    //var code = createHashCode(val);
                    var code = Helpers.CreateHashCode(val).ToString();
                    if (!await IllegalWordCheck(code))
                    {
                        isRepeat = true;
                        break;
                    }

                    var item = new IllegalWordModel()
                    {
                        Value = val,
                        OEMID = oemId,
                        UserID = userId,
                        GroupID = groupId,
                        CompanyID = companyId,
                        HashCode = code,
                        //独立账号/子账号
                        Level = 1,
                        CreatedAt = date,
                        UpdatedAt = date,
                    };
                    insertArr.Add(item);
                }
            }

            foreach (var val in values)
            {
            }

            if (isRepeat)
            {
                return ReturnArr(false, "违禁词重复");
            }

            if (isTooLength)
            {
                return ReturnArr(false, "单个违禁长度不可超过255");
            }

            // 判断批量新增是否存在重复
            var arrc = insertArr.Select(a => a.HashCode).Distinct().Count();
            if (arrc < insertArr.Count())
            {
                return ReturnArr(false, "违禁词重复");
            }

            if (insertArr.Count() > 0)
            {
                await _dbContext.IllegalWord.AddRangeAsync(insertArr);
            }
        }
        else
        {
            //$code = createHashCode($data['value']);
            var code = "";
            if (!await IllegalWordCheck(code))
            {
                return ReturnArr(false, "违禁词重复");
            }

            IllegalWordModel insertData = new();
            insertData.Value = req.Value;
            insertData.UserID = userId;
            insertData.OEMID = oemId;
            insertData.GroupID = groupId;
            insertData.CompanyID = companyId;
            insertData.HashCode = insertData.Value.GetHashCode().ToString();
            insertData.Level = 1;
            _dbContext.IllegalWord.Add(insertData);
        }

        var res = await _dbContext.SaveChangesAsync();
        // IllegalWordService Add Cache
        //if ($res) {
        //    $this->delWordPersonalCache(
        //        $this->getSessionObj()->getConcierge() == User::TYPE_OWN,
        //        $this->getSessionObj()->getCompanyId(),
        //        $this->getSessionObj()->getGroupId()
        //    );
        //}
        return ReturnArr(true, "");
    }

    //没弄
    public async Task<ReturnStruct> Edit(Models.Product.IllegalWordModel vm,DataRange_2b range)
    {
        var info = await GetInfo(vm.ID,range);

        if (info == null)
        {
            return ReturnArr(false, "该条违禁词不存在");
        }

        if (info.UpdatedAt != vm.UpdatedAt)
        {
            return ReturnArr(false, "数据不同步");
        }

        var code = vm.Value.GetHashCode().ToString();
        if (!await IllegalWordCheck(vm.Value))
        {
            return ReturnArr(false, "违禁词重复");
        }

        //todo  $userId = $this->getSession()->getUserId();
        info.Value = vm.Value;
        _dbContext.IllegalWord.Update(info);

        var res = await _dbContext.SaveChangesAsync();
        return ReturnArr();
    }

    //没弄
    public async Task<ReturnStruct> Clear()
    {
        //todo IllegalWordService Clear
        //$where = $this->productWhereDataRange(self::RANGE);
        //if (!count($where))
        //{
        //    return returnArr(false, '缺少可见范围');
        //}
        //$res = IllegalWordModel::query()->where($where)->delete();
        //if ($res) {
        //    $this->delWordPersonalCache(
        //        $this->getSessionObj()->getConcierge() == User::TYPE_OWN,
        //        $this->getSessionObj()->getCompanyId(),
        //        $this->getSessionObj()->getGroupId()
        //    );
        //}
        //return $res;
        return await System.Threading.Tasks.Task.FromResult(ReturnArr());
    }

    public async Task<IllegalWordModel?> Info(int id,DataRange_2b range)
    {
        return await GetInfo(id,range);
    }

    /// <summary>
    /// 删除违禁词
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<bool> Destroy(int id,DataRange_2b range)
    {
        var info = await GetInfo(id,range);
        if (info == null)
        {
            throw new NotFoundException("找不到相关数据#1");
        }

        _dbContext.IllegalWord.Remove(info);
        return await _dbContext.SaveChangesAsync() > 0;
    }

    //没弄
    public async Task<ReturnStruct> Init()
    {
        return await System.Threading.Tasks.Task.FromResult(ReturnArr(true, "成功"));
    }

    public async Task<bool> IllegalWordCheck(string code)
    {
        var total = await _dbContext.IllegalWord.Where(a => a.HashCode == code).CountAsync();
        return total > 0 ? false : true;
    }

    /// <summary>
    /// 获取详情
    /// </summary>
    /// <param name="id"></param>
    /// <param name="range"></param>
    /// <returns></returns>
    private async Task<IllegalWordModel?> GetInfo(int id,DataRange_2b range)
    {
        var model = DataWatchRange(range);
        if (model is null)
            return null;
        
        return await model.FirstOrDefaultAsync(a => a.ID == id);
    }
}