﻿using ERP.Authorization.Rule.Component;
using ERP.Extensions;
using ERP.Models.Product;
using ERP.Models.View.Product.Template;
using ERP.ViewModels.Template;
using Microsoft.EntityFrameworkCore;
using ERP.Data;
using ERP.Enums.Rule.Config.Internal;
using ERP.Exceptions.Expands;
using ERP.Interface;
using ERP.Models.Template;
using ERP.Models.View.Products.Template;
using Newtonsoft.Json;

namespace ERP.Services.Product;

public class ExpandService : BaseWithUserService
{
    public ExpandService(DBContext dbContext, ISessionProvider sessionProvider) : base(dbContext, sessionProvider)
    {
    }

    /// <summary>
    /// 获取卖点模板数据
    /// </summary>
    /// <returns></returns>
    public async Task<ReturnStruct> GetList(ListDto req)
    {
        //todo 权限可见范围
        //$where = $this->productWhereDataRange(self::RANGE);
        //if (!count($where))
        //{
        //    return returnArr(false, '缺少可见范围');
        //}
        var list = await _dbContext.ExtTemplate
            .WhenWhere(req.Name != null, m => m.Name.Contains(req.Name!))
            .OrderByDescending(a => a.ID)
            .ToPaginateAsync(page: req.Page, limit: req.Limit);
        return list.Items.Count > 0 ? ReturnArr(true, "", list) : ReturnArr(false);
    }

    public async Task<ReturnStruct> Edit(ExpandEditViewModel vm, DataRange_2b range)
    {
        if (vm.ID == 0)
        {
            // $extTemplate->oem_id = $this->getSession()->getOemId();
            // $extTemplate->user_id = $this->getSession()->getUserId();
            // $extTemplate->company_id = $this->getSession()->getCompanyId();
            // $extTemplate->group_id = $this->getSession()->getGroupId();
            var data = new ExtTemplateModel()
            {
                Name = vm.Name,
                Content = vm.Content.ToString()!,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                OEMID = 1,
                CompanyID = 1,
                UserID = 1,
                GroupID = 1,
            };
            await _dbContext.ExtTemplate.AddAsync(data);
            await _dbContext.SaveChangesAsync();
            return ReturnArr(true);
        }

        if (vm.ID > 0)
        {
            //edit
            var info = await GetInfo(vm.ID, range);
            if (info == null)
            {
                return ReturnArr(false, "该条属性模板不存在");
            }

            if (info.UpdatedAt != vm.UpdatedAt)
            {
                return ReturnArr(false, "数据不同步");
            }

            info.Name = vm.Name;
            info.Content = vm.Content.ToString()!;
            _dbContext.ExtTemplate.Update(info);
        }

        await _dbContext.SaveChangesAsync();
        return ReturnArr();
    }


    //public function get()
    //{
    //    $where = $this->productWhereDataRange(self::RANGE);
    //    if (!count($where))
    //    {
    //        return returnArr(false, '缺少可见范围');
    //    }
    //    return returnArr(true, '', ExtTemplate::query()->where($where)->get());
    //}

    private IQueryable<ExtTemplateModel>? DataWatchRange(DataRange_2b range)
    {
        if (range == DataRange_2b.None)
            return null;

        var model = _dbContext.ExtTemplate.Where(m => m.OEMID == _oemId && m.CompanyID == _companyId);

        if (range == DataRange_2b.Group)
            model = model.Where(m => m.GroupID == _groupId);

        if (range == DataRange_2b.User)
            model = model.Where(m => m.UserID == (_userId));

        return model;
    }

    public async Task<DbSetExtension.PaginateStruct<ExtTemplateModel>> List(ListDto req, DataRange_2b range)
    {
        var model = DataWatchRange(range);

        if (model is null)
            return new DbSetExtension.PaginateStruct<ExtTemplateModel>();

        return await model
            .Where(m => m.OEMID == _oemId && m.CompanyID == _companyId && m.UserID == _userId)
            .WhenWhere(req.Name.IsNotEmpty(), m => m.Name == req.Name)
            .ToPaginateAsync(page: req.Page, limit: req.Limit);
    }

    public async Task<string> Set(int id, string content, string mode)
    {
        // $whereArr = $this->productWhereDataRange('Product');
        // if (!count($whereArr)) {
        //     return returnArr('缺少查看范围');
        // }
        // $productInfo = ProductModel::query()->where($whereArr)->where('id', $id)->first();
        // if (!$productInfo) {
        //     return returnArr('产品不存在');
        // }
        // $ext = $productInfo->ext;
        // if($mode == 1 || empty($ext) ){
        //     $productInfo->ext =  $content;
        // }else{
        //     //已存在的属性
        //     $exitsExt = [];
        //     foreach ($ext as $k => $v){
        //         $exitsExt[] = strtolower($v['key']);
        //     }
        //     foreach ($content as $k => $v){
        //         if(!in_array(strtolower($v['key']),$exitsExt)){
        //             $ext[] = $v;
        //         }
        //     }
        //     $productInfo->ext =  $ext;
        // }
        // return $productInfo->save();
        return await System.Threading.Tasks.Task.FromResult("");
    }

    /// <summary>
    /// 获取详情
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<ExtTemplateModel?> GetInfo(int? id, DataRange_2b range)
    {
        var model = DataWatchRange(range);
        if (model is null)
            return null;

        return await model
            .FirstOrDefaultAsync(a => a.ID == id);
    }

    public async Task<bool> SaveOrUpdate(ExpandEditDto req, DataRange_2b range)
    {
        ExtTemplateModel? model = null;

        if (!req.Id.HasValue)
        {
            model = req.To(_sessionProvider.Session);
        }
        else
        {
            model = await GetInfo(req.Id, range);
            if (model is null)
            {
                throw new NotFoundException("找不到相关数据#1");
            }

            model.Name = req.Name;
            model.Content = JsonConvert.SerializeObject(req.Content);
        }

        model.UpdatedAt = DateTime.Now;
        if (model is null)
        {
            throw new NotFoundException("找不到相关数据#2");
        }

        if (model.ID <= 0)
        {
            await _dbContext.ExtTemplate.AddAsync(model);
        }
        else
        {
            await _dbContext.ExtTemplate.SingleUpdateAsync(model);
        }

        return await _dbContext.SaveChangesAsync() > 0;
    }

    public async Task<ExpandViewModel> Show(int id, DataRange_2b range)
    {
        var info = await GetInfo(id, range);

        if (info is null)
        {
            throw new NotFoundException("找不到相关数据#1");
        }

        return new ExpandViewModel(info);
    }

    public Task<ReturnStruct> Info(int id)
    {
        throw new NotImplementedException();
    }

    public async Task<bool> Destroy(int id, DataRange_2b range)
    {
        var info = await GetInfo(id, range);

        if (info is null)
        {
            throw new NotFoundException("找不到相关数据#1");
        }

        _dbContext.Remove(info);

        return await _dbContext.SaveChangesAsync() > 0;
    }

    public async Task<List<ExpandViewModel>?> Get(DataRange_2b range)
    {
        var model = DataWatchRange(range);
        if (model is null)
            return null;
        
        return await model
            .Select(m => new ExpandViewModel(m))
            .ToListAsync();
    }
}