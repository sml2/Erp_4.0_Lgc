using ERP.Data;
using ERP.Data.Products;
using ERP.Exceptions.Upload;
using ERP.Interface;
using ERP.Models.DB.Product;
using ERP.Models.Product;
using ERP.Services.Upload;
using Newtonsoft.Json;

namespace ERP.Services.Product;

public class ProductCloneService : BaseWithUserService
{
    private readonly ILogger<ProductCloneService> _logger;
    private readonly IOemProvider _oemProvider;
    private readonly UploadService _uploadService;

    public ProductCloneService(ILogger<ProductCloneService> logger, IOemProvider oemProvider, DBContext dbContext,
        ISessionProvider sessionProvider, UploadService uploadService) : base(dbContext, sessionProvider)
    {
        _logger = logger;
        _oemProvider = oemProvider;
        _uploadService = uploadService;
    }

    private async void HandleClone(ProductModel sproduct,ProductItemModel sitem,ProductModel nproduct)
    {
        try
        {
            //复制图片
            var (imageComparison, copySuccessImages, allTempImages) =
                await BeforeCloneProWithImages(sproduct.Gallery, sproduct);

            //新产品图库
            nproduct.Images = JsonConvert.SerializeObject(copySuccessImages);
            await _dbContext.Product.AddAsync(nproduct);
            await _dbContext.SaveChangesAsync();
            
            var updatedAt = DateTime.Now;
            var version = updatedAt.GetTimeStampMilliseconds();
            
        }
        catch (Exception e)
        {
            _logger.LogError(e, e.Message);
            throw new Exceptions.Exception(e.Message);
        }
    }

    protected async Task<(Dictionary<int, int> imageComparison, Data.Products.Images copySuccessImages,
        List<TempImagesModel>
        allTempImages)> BeforeCloneProWithImages(Data.Products.Images? images, ProductModel product)
    {
        //复制成功的图片
        var copySuccessImages = new Data.Products.Images();
        //图片id对应表
        var imageComparison = new Dictionary<int, int>();
        //复制过程中的全部临时图片
        var allTempImages = new List<TempImagesModel>();

        if (images != null && images.Count > 0)
        {
            foreach (var item in images)
            {
                var tempImageModel = new TempImagesModel();
                if (item.IsNetwork)
                {
                    //远程图
                    tempImageModel = new TempImagesModel(item.Url, item, _oemProvider.OEM!.OssId,
                        ResourceModel.Types.Network, _session);
                }
                else
                {
                    //云存储图
                    var path = await _uploadService.GetImagePath(item.Path!, product);
                    var copyState = await _uploadService.Copy(item.Path!, path);
                    if (!copyState)
                        throw new UploadException($"资源复制失败{item.Path!}");

                    tempImageModel = new TempImagesModel(path, item, _oemProvider.OEM!.OssId,
                        ResourceModel.Types.Storage, _session);
                }

                //添加
                await _dbContext.TempImages.AddAsync(tempImageModel);
                await _dbContext.SaveChangesAsync();
                imageComparison.Add(item.ID, tempImageModel.ID);
                copySuccessImages.Add(new Image(tempImageModel));
                allTempImages.Add(tempImageModel);
            }
        }

        return (imageComparison, copySuccessImages, allTempImages);
    }
    
    
}