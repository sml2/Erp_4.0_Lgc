using Common.Enums.Products;
using ERP.Data;
using ERP.Data.Products;
using ERP.Enums.Product;
using ERP.Enums.Rule.Config.Internal;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.DB.Product;
using ERP.Models.Product;
using ERP.Models.View.Products.Product;
using ERP.Models.View.Products.Product.ImageTran;
using ERP.Services.DB.CompanyResource;
using ERP.Services.Upload;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace ERP.Services.Product;

public class ImagesService : BaseWithUserService
{
    private readonly PackagesResourceService _packagesResourceService;
    private readonly ILogger<ImagesService> _logger;
    private readonly UploadService _uploadService;
    private readonly IOemProvider _oemProvider;
    private readonly ProductService _productService;
    private readonly ConvertProduct _convertProduct;

    public ImagesService(DBContext dbContext, ISessionProvider sessionProvider,
        PackagesResourceService packagesResourceService, ILogger<ImagesService> logger, UploadService uploadService,
        IOemProvider oemProvider, ProductService productService, ConvertProduct convertProduct) : base(dbContext, sessionProvider)
    {
        _packagesResourceService = packagesResourceService;
        _logger = logger;
        _uploadService = uploadService;
        _oemProvider = oemProvider;
        _productService = productService;
        _convertProduct = convertProduct;
    }

    public async Task<DbSetExtension.PaginateStruct<ListImagesViewModel>> GetList(ListDto req, DataRange_2b range)
    {
        var model = _dbContext.Product.WhereRange(range, _companyId, _groupId, _userId);
        model = model.IsOnGoing(false).IsExport(false).IsB2C(false).IsUpload(false).IsRecycle(false);


        model = (IQueryable<ProductModel>)model
            .Where(m => !string.IsNullOrEmpty(m.Images) && m.Images != "[]")
            .WhenWhere(!req.Pid.IsNullOrEmpty(), m => m.Pid == req.Pid)
            .WhenWhere(req.UserId.HasValue, m => m.UserID == req.UserId)
            .WhenWhere(req.TaskId.HasValue, m => m.TaskId == req.TaskId)
            .WhenWhere(req.CategoryId.HasValue, m => m.CategoryId == req.CategoryId)
            .WhenWhere(req.SubCategoryId.HasValue, m => m.SubCategoryId == req.SubCategoryId)
            .WhenWhere(req.PlatformId.HasValue, m => m.PlatformId == req.PlatformId)
            .WhenWhere(req.Title.Length > 0 && req.Multi == MultiEnum.Fuzzy, m => m.Title.Contains(req.Title))
            .WhenWhere(req.Title.Length > 0 && req.Multi == MultiEnum.Precise, m => m.Title == req.Title)
            .WhenWhere(req.UpdatedTime != null && req.UpdatedTime.Count == 2,
                a => a.UpdatedAt >= req.UpdatedTime![0] && a.UpdatedAt < req.UpdatedTime[1].LastDayTick())            .AsEnumerable();
        if (req.AuditState.IsNotNull())
        {
            //audit
            var audited = (long)ProductModel.AUDIT_AUDITED << (int)Types.Audit;
            var typeAudit = Types.Audit;
            var auditMask = Types.MASK_AUDIT;

            switch (req.AuditState)
            {
                case AuditStateEnum.NotReviewed:
                    var a =
                        model = model.Where(m => ((m.Type & (long)auditMask)) < audited);
                    break;
                case AuditStateEnum.Audited:
                    model = model.Where(m => ((m.Type & (long)auditMask)) >= audited);
                    break;
                case AuditStateEnum.DidNotPass:
                    model = model.Where(m => ((m.Type & (long)auditMask)) > audited);
                    break;
            }
        }

        if (req.IsRepeat)
        {
            model = model.Where(m => m.UserLevelRepeatNum > 0)
                .OrderByDescending(m => m.UserLevelRepeatNum)
                .ThenByDescending(m => m.UpdatedAt);
        }

        return await model.OrderByDescending(m => m.ID)
            .Select(m => new ListImagesViewModel(m))
            .ToPaginateAsync(page: req.Page, limit: req.Limit);
    }
    public async Task<(bool State, String Message)> Tran(GoTranDto req)
    {
        var transation = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            var consumptionResult = await _packagesResourceService.ConsumptionImgTran(1);
            if (!consumptionResult.State)
            {
                throw new Exception(consumptionResult.Msg);
            }

            var sendResult = await SendTran(req, req.Path);

            if (!sendResult.State)
            {
                throw new Exception(sendResult.Message);
            }

            await _dbContext.SaveChangesAsync();

            await transation.CommitAsync();
            return (true, sendResult.Message);
        }
        catch (Exception e)
        {
            await transation.RollbackAsync();
            _logger.LogError(e, e.Message);
            return (false, e.Message);
        }
    }


    public async Task<(bool State, String Message)> Tran(ImageTranDto req)
    {
        var info = await _dbContext.Resource.FirstOrDefaultAsync(m => m.ID == req.Id && m.ProductId == req.Pid);
        if (info == null)
        {
            return (false, "找不到相关数据#1");
        }

        var transation = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            var consumptionResult = await _packagesResourceService.ConsumptionImgTran(1);
            if (!consumptionResult.State)
            {
                throw new Exception(consumptionResult.Msg);
            }

            var sendResult = await SendTran(req, info.Path);

            if (!sendResult.State)
            {
                throw new Exception(sendResult.Message);
            }

            var path = await _uploadService.UploadImageByNetwork(sendResult.Message,_oemId,_userId);

            info.Path = path;
            info.Size = await _uploadService.GetSize(path);
            info.Type = ResourceModel.Types.Storage;
            info.StorageId = _oemProvider.OEM.OssId;

            await _dbContext.Resource.SingleUpdateAsync(info);
            await _dbContext.SaveChangesAsync();

            await transation.CommitAsync();
            return (true, "图片翻译成功");
        }
        catch (Exception e)
        {
            await transation.RollbackAsync();
            _logger.LogError(e, e.Message);
            return (false, e.Message);
        }
    }

    private async Task<(bool State, string Message)> SendTran(BaseTranDto dto,string  imgUrl)
    {
        var userKey = "9964996788";
        var imgTransKey = "4336835733";
        var time = Helpers.GetTimeStamp(DateTime.Now);
        var sign = GetSign(time, userKey, imgTransKey).ToLower();
        var req = new HttpRequestMessage(HttpMethod.Get, "http://api.tosoiot.com");
        req.Content = new FormUrlEncodedContent(new Dictionary<string, string>()
        {
            { "Action", "GetImageTranslate" },
            { "SourceLanguage", dto.SourceLanguage.GetDescriptionByKey("Code") },
            { "TargetLanguage", dto.TargetLanguage.GetDescriptionByKey("Code") },
            { "Url", _convertProduct.GetImageUrl(imgUrl,_oemProvider) },
            { "ImgTransKey", imgTransKey },
            { "CommitTime", time.ToString() },
            { "Sign", sign },
        });
        var client = new HttpClient();
        var response = await client.SendAsync(req);
        var content = await response.Content.ReadAsStringAsync();
        var res = JsonConvert.DeserializeObject<ImageTranslateViewModel>(content);
        if (res == null)
        {
            return (false, "请求异常");
        }

        if (res.Code == 200 && res.Data.IsNotNull() && !string.IsNullOrEmpty(res.Data!.Url))
        {
            //http url 
            //https SslUrl
            return (true, res.Data!.Url);
        }

        return (false, res.Message);
    }

    private string GetSign(long time, string userKey, string key)
    {
        return Sy.Security.MD5.Encrypt($"{time}_{userKey}_{key}");
    }

    public async Task<(bool State, string Message)> TranNotify(TranNotifyDto req)
    {
        var reqImages = req.Images;

        if (reqImages.Count <= 0)
        {
            return (false, "无图片进行翻译#1");
        }

        var productInfo = await _dbContext.Product.FirstOrDefaultAsync(m => m.ID == req.Pid);
        if (productInfo == null)
        {
            return (false, "找不到相关产品#2");
        }

        var gallery = productInfo.Gallery;
        if (gallery == null || gallery.Count <= 0)
        {
            return (false, "产品图库为空，请核实产品是否正确#3");
        }

        var itemList = await _productService.GetItemInfoList(productInfo.ID);
        if (itemList.Count <= 0)
        {
            return (false, "找不到相关产品数据#4");
        }

        var syncImageIds = reqImages.Select(m => m.Id).ToList();

        var dicImages = await _dbContext.Resource.Where(m => syncImageIds.Contains(m.ID))
            .ToDictionaryAsync(m => m.ID, m => m);

        //图库
        var dicGallery = gallery!.ToDictionary(m => m.ID, m => m);
        foreach (var item in dicGallery)
        {
            if (dicImages.Keys.Contains(item.Key))
            {
                var newImg = dicImages[item.Key];
                var m = dicGallery[item.Key];
                m.Path = newImg.Path;
                m.Size = newImg.Size;
                m.Time = newImg.UpdatedAt;
            }

            productInfo.Images = JsonConvert.SerializeObject(gallery.ToImages());
        }
        
        //主图
        var main = productInfo.MainImageObj;
        if (main != null)
        {
            if (dicImages.Keys.Contains(main.ID))
            {
                main.Path = dicImages[main.ID].Path;
                main.Size = dicImages[main.ID].Size;
                main.Time = dicImages[main.ID].UpdatedAt;
            }

            productInfo.MainImage = JsonConvert.SerializeObject(main);
        }

        var newGallery = dicGallery.Select(m => m.Value).ToList();
        var updatedAt = DateTime.Now;
        productInfo.FilesSize = 0;
        var transation = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            foreach (var proItem in itemList)
            {
                var currentVersionJsonFilepath = proItem.FilePath;
                var productStruct = await _productService.GetProductStruct(currentVersionJsonFilepath, productInfo);

                var imagesStruct = productStruct.Images;
                var dicImagesStruct = imagesStruct?.ToDictionary(m => m.ID, m => m);

                if (dicImagesStruct != null)
                {
                    var newImagesStruct = new ERP.Data.Products.Images();
                    foreach (var item in dicImagesStruct)
                    {
                        var m = dicImagesStruct[item.Key];
                        if (dicImages.Keys.Contains(item.Key))
                        {
                            var newImg = dicImages[item.Key];
                            m.Path = newImg.Path;
                            m.Size = newImg.Size;
                            m.Time = newImg.UpdatedAt;
                        }

                        newImagesStruct.Add(m);
                    }

                    productStruct.Images = newImagesStruct;
                }

                var version = updatedAt.GetTimeStampMilliseconds();
                proItem.CurrentVersion = version;
                proItem.HistoryVersionList.Add(proItem.CurrentVersion);
                proItem.HistoryVersion = JsonConvert.SerializeObject(proItem.HistoryVersionList);
                
                var jsonProduct = JsonConvert.SerializeObject(productStruct, new JsonSerializerSettings()
                {
                    TypeNameHandling = TypeNameHandling.All
                });
                var jsonFilePath = await _productService.UploadStructFile(jsonProduct, version, productInfo.ID);
                proItem.FilesSize = jsonProduct.Length;
                productInfo.FilesSize += proItem.FilesSize;
                await _dbContext.ProductItem.SingleUpdateAsync(proItem);
            }
            
            await _dbContext.Product.SingleUpdateAsync(productInfo);
            await _dbContext.SaveChangesAsync();

            await transation.CommitAsync();
            return (true, "产品处理成功");
        }
        catch (Exception e)
        {
            await transation.RollbackAsync();
            _logger.LogError(e, e.Message);
            return (false, e.Message);
        }
    }


}