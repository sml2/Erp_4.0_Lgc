﻿using Aspose.Cells.Charts;
using ERP.Data;
using ERP.Extensions;
using ERP.Models.DB.Product;
using ERP.Models.Product;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Globalization;


namespace ERP.Services.DB.Product;
public class Product : BaseService
{
    public readonly IHttpContextAccessor _httpContextAccessor;

    public readonly DBContext _dbContext;

    protected readonly ILogger<Product> logger;

    public Product(IHttpContextAccessor httpContextAccessor, DBContext dbContext, ILogger<Product> _logger)
    {
        _httpContextAccessor = httpContextAccessor;
        _dbContext = dbContext;
        logger = _logger;
    }

    public async Task<bool> Add(List<ProductInfo> models) => await AddInternal(models);

    public async Task<bool> Add(ProductInfo model) => await AddInternal(new List<ProductInfo>() { model });

    private async Task<bool> AddInternal(List<ProductInfo> models)
    {      
        try
        {
            foreach (var model in models)
            {
                if (await AddInternal(model))
                    continue;
                else
                    return false;
            }
            return true;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);           
            return false;
        }
    }

    private async Task<bool> AddInternal(ProductInfo productInfo)
    {
        var model = productInfo.ProductModel;
        var modelItem = productInfo.ProductItemModel;
        try
        {           
            var Source = await _dbContext.Product.FirstOrDefaultAsync(a => a.PidHashCode == model.PidHashCode);
            if (Source is null)
            {
                await _dbContext.Product.AddAsync(model);
                //add productItem
                await  _dbContext.ProductItem.AddAsync(modelItem);
            }
            else
            {
                //更新数据库
                //todo ProductItemModel HistoryVersion
                var lstHistoryVersion = JsonConvert.DeserializeObject<List<long>>(Source.HistoryVersion);
                // lstHistoryVersion.Add(model.CurrentVersion);
                //todo ProductItemModel FilePath
               
                Source.Quantity = model.Quantity;
                Source.Title = model.Title!;
                Source.HashCode = Helpers.CreateHashCode(model.Title!);
                Source.MainImage = model.MainImage;               
                 _dbContext.Product.Update(Source);

                var SourceModel = await _dbContext.ProductItem.FirstOrDefaultAsync(a => a.Pid == model.ID && a.LanguageId==0);
                if(SourceModel is null)
                {
                    await _dbContext.ProductItem.AddAsync(modelItem);
                }
                else
                {
                    SourceModel.FilePath = modelItem.FilePath;
                    SourceModel.FilePathHashCode = modelItem.FilePathHashCode;
                    SourceModel.HistoryVersion = JsonConvert.SerializeObject(lstHistoryVersion);
                    SourceModel.CurrentVersion = modelItem.CurrentVersion;
                    _dbContext.ProductItem.Update(SourceModel);
                }
            }
            await _dbContext.SaveChangesAsync();
            return true;
        }
        catch (Exception e)
        {
            string modelJson = "";
            if (productInfo.IsNotNull())
            {
                modelJson = JsonConvert.SerializeObject(productInfo);
            }            
            logger.LogError($"product AddInternal asin:{model.Pid},json:{modelJson} error:"+e.Message);
        }       
        return false;
    }





    //    /// <summary>
    //    /// 初始化产品列表
    //    /// </summary>
    //    /// <returns></returns>
    //    public async Task<ReturnStruct> Init()
    //    {
    //        //$where = $this->productWhereDataRange('Task');

    //        //$audit = [
    //        //    ['label' => '未审核', 'value' => ProductModel::AUDIT_UNREVIEWED_SELECT],
    //        //    ['label' => '已审核', 'value' => ProductModel::AUDIT_AUDITED],
    //        //    ['label' => '未通过', 'value' => ProductModel::AUDIT_INFRINGEMENT],
    //        //];
    //        //$type = false;
    //        //if ($this->getSession()->getIsDistribution() == Company::DISTRIBUTION_FALSE) {
    //        //    $res = (new CompanyService())->getReportIdCompany();
    //        //    if ($res > 0) {
    //        //        $type = true;
    //        //    }
    //        //}
    //        //$isCompanyAdmin = $this->getSession()->getConcierge() == User::TYPE_OWN;
    //        //$companyUsers = [];
    //        //if ($isCompanyAdmin) {
    //        //    $companyUsers = $this->getCompanyUsersCache();
    //        //}
    //        //$oemId = $this->getSessionObj()->getOemId();
    //        //$menuName = null;
    //        //// 4 为致领国际OEM ID
    //        //if ($oemId == 4) {
    //        //    $menuName = '致领国际';
    //        //}
    //        //if ($this->getSessionObj()->getConcierge() == User::TYPE_OWN) {
    //        //    $item = [
    //        //        'search'       => true,
    //        //        'image'        => true,
    //        //        'price'        => true,
    //        //        'quantity'     => true,
    //        //        'edit'         => true,
    //        //        'recycle'      => true,
    //        //        'audit'        => true,
    //        //        'repeat'       => true,
    //        //        'translate'    => true,
    //        //        'upload'       => true,
    //        //        'share'        => true,
    //        //        'delete'       => true,
    //        //        'export'       => true,
    //        //        'distribution' => true,
    //        //        'sketch'       => true,
    //        //        'keyword'      => true,
    //        //        'download'     => true,
    //        //        'exeImport'    => true,
    //        //        'bindCategory' => true,
    //        //        'ext'          => true,
    //        //        'imageSync'    => true,
    //        //        'randomEdit'   => true,
    //        //        //                'imageSync'    => false
    //        //    ];
    //        //} else
    //        //{
    //        //    $item = [
    //        //        'search'       => $this->ruleInfo()->hasProductSearch(),
    //        //        'image'        => $this->ruleInfo()->hasProductShowImg(),
    //        //        'price'        => $this->ruleInfo()->hasProductPrice(),
    //        //        'quantity'     => $this->ruleInfo()->hasProductQuantity(),
    //        //        'edit'         => $this->ruleInfo()->hasProductRepeat(),
    //        //        'recycle'      => $this->ruleInfo()->hasRecycleProduct(),
    //        //        'audit'        => $this->ruleInfo()->hasAuditProduct(),
    //        //        'repeat'       => $this->ruleInfo()->hasProductRepeat(),
    //        //        'translate'    => $this->ruleInfo()->hasProductTranslate(),
    //        //        'upload'       => $this->getSessionObj()->getRule()->getStore()->getBiter()->hasUploadProgress(),
    //        //        'share'        => $this->ruleInfo()->hasShareProduct(),
    //        //        'delete'       => $this->ruleInfo()->hasProductDelete(),
    //        //        'export'       => $this->ruleInfo()->hasProductExport(),
    //        //        'distribution' => $this->ruleInfo()->hasProductDistribution(),
    //        //        'sketch'       => $this->ruleInfo()->hasSketchTemplate(),
    //        //        'keyword'      => $this->ruleInfo()->hasKeywordTemplate(),
    //        //        'ext'          => $this->ruleInfo()->hasExtTemplate(),
    //        //        'download'     => $this->ruleInfo()->hasProductDownloadImages(),
    //        //        'exeImport'    => $this->ruleInfo()->hasProductExeImport(),
    //        //        'bindCategory' => $this->ruleInfo()->hasBindCategory(),
    //        //        'imageSync'    => $this->getSessionObj()->getRule()->getProduct()->getBiter2()->hasProductImageSync(),
    //        //        'randomEdit'    => $this->getSessionObj()->getRule()->getProduct()->getBiter2()->hasProductRandomEdit(),
    //        //        //                'imageSync'    => false,
    //        //    ];
    //        //}

    //        //$rule = [
    //        //    'menu' => [
    //        //        'collect' => ['isShow' => $this->ruleInfo()->hasCollectProduct(), 'label' => ''],
    //        //        'manual'  => ['isShow' => $this->ruleInfo()->hasManualProduct(), 'label' => ''],
    //        //        'audit'   => ['isShow' => $this->ruleInfo()->hasAuditProduct(), 'label' => ''],
    //        //        'share'   => ['isShow' => $this->ruleInfo()->hasShareProduct(), 'label' => $menuName],
    //        //        'recycle' => ['isShow' => $this->ruleInfo()->hasRecycleProduct(), 'label' => ''],
    //        //    ],
    //        //    'item' => $item
    //        //];
    //        //$data = [
    //        //    'platform'       => $this->getCollectPlatformCache(),
    //        //    'task'           => Task::query()->select(['id', 'name'])->latest()->where($where)->get()->toArray(),
    //        //    'audit'          => $audit,
    //        //    'concierge'      => $isCompanyAdmin,
    //        //    'companyUsers'   => $companyUsers,
    //        //    'isDistribution' => $type,
    //        //    'rule'           => $rule,
    //        //    'category'       => $this->firstCategory()['data']
    //        //];
    //        //return returnArr(true, '', $data);
    //    }

    //    public async Task<ReturnStruct> Info(int id, string companyId = "", string groupId = "", string userId = "", string oem_id = "")
    //    {
    //        //$where = [];
    //        //$companyId == '' && $companyId = $this->getSession()->getCompanyId();
    //        //if (!$companyId) {
    //        //    return returnArr(false, '服务器异常: 缺少公司');
    //        //}
    //        //$where[] = ['company_id', '=', $companyId];
    //        //$groupId != '' && $where[] = ['group_id', '=', $groupId];

    //        //$where[] = ['id', '=', $id];
    //        //$where[] = ['oem_id', '=', $oem_id ? $oem_id: $this->getSession()->getOemId()];
    //        //$info = ProductModel::query()->where($where)->select(ProductModel::COLLECT_SELECT)->first();
    //        //if (!$info) {
    //        //    return returnArr(false, '该产品不存在');
    //        //}
    //        //$info = $info->toArray();
    //        //$typeBiter = new Type($info['type']);
    //        //$info['recycle'] = $typeBiter->isRecycle();
    //        //$info['share'] = $typeBiter->isShare();

    //        //$languages = &$info['languages'];
    //        //$lp = ['s'];
    //        //foreach ($lp as $val) {
    //        //    foreach ($languages as $k => $v) {
    //        //        $languages[$k][$val] = collect($v[$val])->map(
    //        //            function($item) {
    //        //            return [
    //        //                'value' => $item
    //        //            ];
    //        //        }
    //        //        );
    //        //    }
    //        //}
    //        //$languageBiter = new Language($info['language']);

    //        //$languageDefault = ['default' => '默认'];
    //        //$uiLanguage = 'default';
    //        ////        if ($isClone) {
    //        ////            $uiLanguage = $languageBiter::LanguageCode[$languageBiter->getUILanguage() - 1] ?? '';
    //        ////            if ($uiLanguage) {
    //        ////                $languageDefault = [$uiLanguage => $this->getLanguageCache()[$uiLanguage]['name']];
    //        ////            }
    //        ////        }
    //        //$languagePack = $languageDefault + $languageBiter->getTranslated();
    //        //$uiLanguage = $uiLanguage ?: 'default';
    //        //$info['languages'][$uiLanguage] = [
    //        //    'n' => $info['lp_n'],
    //        //    's' => collect($info['lp_s'])->map(
    //        //        function($item) {
    //        //    return [
    //        //        'value' => $item
    //        //    ];
    //        //}
    //        //    ),
    //        //    'k' => $info['lp_k'],
    //        //    'd' => $info['lp_d'],
    //        //];


    //        //$unsetArr = ['lp_k', 'lp_s', 'lp_d'];
    //        //for ($i = 0; $i < count($unsetArr); $i++) {
    //        //    unset($info[$unsetArr[$i]]);
    //        //}
    //        //// 是否单变体
    //        //if (count($info['variants']) == 1)
    //        //{
    //        //    //去重变体附图
    //        //    $info['variants'][0]['img']['affiliate'] = array_unique($info['variants'][0]['img']['affiliate']);
    //        //}
    //        //foreach ($info['variants'] as $k => $v) {
    //        //    $info['variants'][$k]['quantity'] = (int)$v['quantity'];
    //        //}
    //        //$data = [
    //        //    'info'         => $info,
    //        //    'languagePack' => $languagePack,
    //        //];
    //        ////权限
    //        //if ($this->getSessionObj()->getConcierge() == User::TYPE_OWN) {
    //        //    $rule = [
    //        //        'matting'      => true,
    //        //        'addTranslate' => true,
    //        //        'cloneTranslate' => true,
    //        //    ];
    //        //} else
    //        //{
    //        //    $rule = [
    //        //        'matting'      => $this->getSessionObj()->getRule()->getProduct()->getBiter2()->hasProductMatting(),
    //        //        'addTranslate' => $this->getSessionObj()->getRule()->getProduct()->getBiter2()->hasProductManualTranslation(),
    //        //        'cloneTranslate' => $this->getSessionObj()->getRule()->getProduct()->getBiter2()->hasProductCloneTranslate()
    //        //    ];
    //        //}
    //        return ReturnArr(true, "");
    //    }

    //    public async Task<Category?> GetCategoryById(int id)
    //    {
    //        return await _dbContext.Category.FirstOrDefaultAsync(a => a.ID == id);
    //    }

    //    public async Task<ReturnStruct> List(
    //        //$pid,
    //        //$taskId,
    //        //$platformId,
    //        //$categoryId,
    //        //$name,
    //        //$action,
    //        //$time = '',
    //        //$auditId = -1,
    //        //$userId = '',
    //        //$share = -1,
    //        //$isRepeat = false,
    //        //$multi = false
    //        )
    //    {
    //        //$ProductModel = new ProductModel();
    //        //$onGoingBiter = Type::FLAG_IS_ONGOING;
    //        //$uploadBiter = Type::FLAG_IS_UPLOAD;
    //        //$recycleBiter = Type::FLAG_RECYCLE;
    //        //$auditState = ProductModel::AUDIT_AUDITED;
    //        //$typeBiter = Type::MASK_AUDIT;
    //        //$bitAudit = Type::BIT_AUDIT;
    //        //$bitExport = Type::FLAG_IS_EXPORT;
    //        //$whereArr = $this->productWhereDataRange('Product');
    //        //$whereSql = '!('. $ProductModel->transformKey('type'). '&'. $onGoingBiter. ')';
    //        //$whereSql.= ' AND !('. $ProductModel->transformKey('type'). '&'. $bitExport. ')';
    //        //if ($auditId == 1) {
    //        //    // 未审核
    //        //    $whereSql.= ' AND ('. $ProductModel->transformKey(
    //        //          'type'
    //        //      ). '&'. $typeBiter. ')>>'. $bitAudit. '<'. $auditState;
    //        //} else
    //        //{
    //        //    if ($auditId == 2) {
    //        //        // 已审核
    //        //        $whereSql.= ' AND ('. $ProductModel->transformKey(
    //        //              'type'
    //        //          ). '&'. $typeBiter. ')>>'. $bitAudit. '>='. $auditState;
    //        //    } else
    //        //    {
    //        //        if ($auditId == 3) {
    //        //            // 为未通过
    //        //            $whereSql.= ' AND ('. $ProductModel->transformKey(
    //        //                  'type'
    //        //              ). '&'. $typeBiter. ')>>'. $bitAudit. '>'. $auditState;
    //        //        }
    //        //    }
    //        //}
    //        //$whereSql.= ' AND !('. $ProductModel->transformKey('type'). '&'. $uploadBiter. ')';
    //        //switch ($action) {
    //        //    case 'purchase':
    //        //        $whereArr[] = ['company_id', '=', $this->getSessionObj()->getCompanyId()];
    //        //        $whereArr[] = ['oem_id', '=', $this->getSessionObj()->getOemId()];
    //        //        $whereSql.= ' AND ('. $ProductModel->transformKey(
    //        //              'type'
    //        //          ). '&'. $typeBiter. ')>>'. $bitAudit. '='.ProductModel::AUDIT_AUDITED;
    //        //        $whereSql.= ' AND  !('. $ProductModel->transformKey('type'). '&'. $recycleBiter. ')';
    //        //    break;

    //        //    case 'collect':
    //        //        $collectBiter = Type::FLAG_COLLECT;
    //        //        $operator = '=';
    //        //        $whereSql.= ' AND '. $ProductModel->transformKey(
    //        //              'type'
    //        //          ). '&'. $collectBiter. $operator . $collectBiter;
    //        //        $whereSql.= ' AND  !('. $ProductModel->transformKey('type'). '&'. $recycleBiter. ')';
    //        //    break;

    //        //    case 'manual':
    //        //        $collectBiter = Type::FLAG_COLLECT;
    //        //        $operator = '!=';
    //        //        $whereSql.= ' AND '. $ProductModel->transformKey(
    //        //              'type'
    //        //          ). '&'. $collectBiter. $operator . $collectBiter;
    //        //        $whereSql.= ' AND  !('. $ProductModel->transformKey('type'). '&'. $recycleBiter. ')';
    //        //    break;

    //        //    case 'audit':
    //        //        $whereSql.= ' AND ('. $ProductModel->transformKey(
    //        //              'type'
    //        //          ). '&'. $typeBiter. ')>>'. $bitAudit. '<'. $auditState;
    //        //        $whereSql.= ' AND  !('. $ProductModel->transformKey('type'). '&'. $recycleBiter. ')';
    //        //    break;

    //        //    case 'share':
    //        //        $whereArr = [];
    //        //    if ($this->ruleInfo()->hasShareProductRange()) {
    //        //            $companyId = $this->getSession()->getCompanyId();
    //        //            $whereArr[] = ['company_id', '=', $companyId];
    //        //    } else
    //        //    {
    //        //            $group_id = $this->getSession()->getGroupId();
    //        //            $whereArr[] = ['group_id', '=', $group_id];
    //        //    }
    //        //    if ($share != null && $share != -1) {
    //        //            $whereArr[] = ['user_id', '=', $this->getSessionObj()->getUserId()];
    //        //    } else
    //        //    {
    //        //        if ($share == -1) {
    //        //                $whereArr[] = ['user_id', '!=', $this->getSessionObj()->getUserId()];
    //        //        }
    //        //    }
    //        //        $whereArr[] = ['oem_id', '=', $this->getSessionObj()->getOemId()];
    //        //        $shareBiter = Type::FLAG_SHARE;
    //        //        $whereSql.= ' AND '. $ProductModel->transformKey('type'). '&'. $shareBiter;
    //        //        $whereSql.= ' AND  !('. $ProductModel->transformKey('type'). '&'. $recycleBiter. ')';
    //        //    break;

    //        //    case 'recycle':
    //        //        $recycleBiter = Type::FLAG_RECYCLE;
    //        //        $whereSql.= ' AND '. $ProductModel->transformKey('type'). '&'. $recycleBiter;
    //        //    break;
    //        //}

    //        //// 是否查询重复产品
    //        //if ($isRepeat) {
    //        //    $repeatBiter = Type::FLAG_TITLE_REPEAT;
    //        //    $whereSql.= ' AND '. $ProductModel->transformKey(
    //        //          'type'
    //        //      ). '&'. $repeatBiter. '='. $repeatBiter;
    //        //}

    //        //if (!count($whereArr))
    //        //{
    //        //    return returnArr(false, '缺少可见范围');
    //        //}
    //        //$whereUser = [];
    //        //if ($this->getSession()->getConcierge() == User::TYPE_OWN && $userId) {
    //        //    $whereUser[] = ['user_id', '=', $userId];
    //        //}
    //        //$data = $ProductModel->query()->whereRaw($whereSql)
    //        //    ->with(['category', 'task'])->latest('id')->pageList(
    //        //        $whereArr,
    //        //        $pid,
    //        //        $taskId,
    //        //        $platformId,
    //        //        $categoryId,
    //        //        $name,
    //        //        $time
    //        //    )->nameLike($name, $multi)->where($whereUser)->select(ProductModel::LIST_SELECT)->paginate(
    //        //        $this->limit
    //        //    );
    //        //return returnArr(true, '', $data);
    //    }

    //    /// <summary>
    //    /// 产品新增/编辑
    //    /// </summary>
    //    /// <param name="data"></param>
    //    /// <param name="isClone"></param>
    //    /// <returns></returns>
    //    public async Task<ReturnStruct> Edit(Models.Product.Product data, bool isClone = true)
    //    {
    //        //$resourceData = [];
    //        //$delResourceData = [];
    //        //$addImagesIds = [];
    //        //$delImagesIds = [];

    //        //$id = '';
    //        //if (isset($data['id']) && !empty($data['id']))
    //        //{
    //        //    $id = $data['id'];
    //        //    $productModel = ProductModel::query()->where('id', $id)->first();
    //        //    if (!$productModel) {
    //        //        return returnArr(false, '该条产品数据不存在');
    //        //    }
    //        //    if (strtotime($productModel->updated_at) != strtotime($data['updated_at'] ?? null))
    //        //    {
    //        //        return returnArr(false, '数据异常');
    //        //    }
    //        //    $languageBiter = new Language($productModel->language);
    //        //    $typeBiter = new Type($productModel->type);
    //        //    // 设置为二次修改
    //        //    $typeBiter->setEdited(true);
    //        //    // 资源图片
    //        //    $originalImages = $productModel->images;
    //        //    $originalImageIds = array_keys(arrayCombine($originalImages, 'id'));
    //        //    $images = $data['images'];
    //        //    $currentImagesIds = array_keys(arrayCombine($images, 'id'));
    //        //    $addImagesIds = array_diff($currentImagesIds, $originalImageIds);
    //        //    $delImagesIds = array_diff($originalImageIds, $currentImagesIds);
    //        //    if (count($addImagesIds))
    //        //    {
    //        //        $resourceData = TempImages::query()->select(
    //        //            [
    //        //                'id',
    //        //                'oss_path AS path',
    //        //                'size',
    //        //                'user_id',
    //        //                'oss_id',
    //        //                'oem_id',
    //        //                'type',
    //        //                'created_at',
    //        //                'updated_at'
    //        //            ]
    //        //        )
    //        //            ->whereIn('id', $addImagesIds)
    //        //            ->get()
    //        //            ->toArray();
    //        //    }
    //        //    if (count($delImagesIds))
    //        //    {
    //        //        $delResourceData = Resource::query()->select(
    //        //            [
    //        //                'id',
    //        //                'path AS oss_path',
    //        //                'size',
    //        //                'user_id',
    //        //                'oss_id',
    //        //                'oem_id',
    //        //                'type',
    //        //                'created_at',
    //        //                'updated_at'
    //        //            ]
    //        //        )
    //        //            ->whereIn('id', $delImagesIds)
    //        //            ->get()
    //        //            ->toArray();
    //        //    }
    //        //}
    //        //else
    //        //{
    //        //    $productModel = new ProductModel();
    //        //    $productModel->user_id = $this->getSession()->getUserId();
    //        //    $productModel->group_id = $this->getSession()->getGroupId();
    //        //    $productModel->oem_id = $this->getSession()->getOemId();
    //        //    $productModel->company_id = $this->getSession()->getCompanyId();
    //        //    // 类型处理
    //        //    $typeBiter = new Type(BitwiseFlag::FLAG_BASE);
    //        //    $typeBiter->setCollct(false);
    //        //    $typeBiterValue = $typeBiter->getFlags();
    //        //    $productModel->type = $typeBiterValue;
    //        //    $languageBiter = new Language(BitwiseFlag::FLAG_BASE);
    //        // 资源图片
    //        var images = data.Images;
    //        if (images.Length > 0)
    //        {
    //            //$addImagesIds = array_keys(arrayCombine($images, 'id'));
    //            int[] addImagesIds = { 1, 2 };
    //            var resourceData = (from t1 in _dbContext.TempImages
    //                                select new
    //                                {
    //                                    t1.ID,
    //                                    t1.OssPath,
    //                                    t1.Size,
    //                                    t1.UserID,
    //                                    t1.OssID,
    //                                    t1.OEMID,
    //                                    t1.Type,
    //                                    t1.CreatedAt,
    //                                    t1.UpdatedAt,
    //                                }).Where(a => addImagesIds.Contains(a.ID)).ToList();
    //        }
    //        // 获取当前公司配置 判断是否审核
    //        //$company = Cache::instance()->company()->get();
    //        //$typeBiter = new Type(Type::FLAG_BASE);
    //        //$typeBiter->setAudit($company['product_audit']);
    //        //}
    //        //$typeBiter->setLock(true);

    //        //// 语言处理
    //        var languages = data.Languages;
    //        if (isClone)
    //        {
    //            if (languages.default == null)
    //            {
    //                return ReturnArr(false, "无默认语言数据");
    //            }
    //            var default = languages.default;
    //            //$languageKeys = array_keys($languages);
    //            //foreach ($languageKeys as $v) {
    //            //    $fn = 'set'. $v;
    //            //    $languageBiter->$fn(true);
    //            //}
    //            //$languageBiterValue = $languageBiter->getFlags();
    //            string[] lp = { "s" };

    //            //foreach ($lp as $val) {
    //            //    foreach ($languages as $k => $v) {
    //            //        $languages[$k][$val] = array_column($v[$val], 'value');
    //            //    }
    //            //}
    //            foreach (var val in lp)
    //            {
    //                foreach (var item in languages)
    //                {

    //                }
    //            }

    //        }
    //        else
    //        {
    //            //$default = $languages[$productModel->ui_language ?: 'default'];
    //            //$languages = [];
    //            //$languageBiterValue = $productModel->language;
    //        }
    //        // 设置默认变体
    //        var attribute = data.Attribute;
    //        var variants = data.Variants;
    //        if (attribute == null)
    //        {
    //            // 设置默认图片
    //            ArrayList variantsImages;
    //            if (images.Count() > 0)
    //            {
    //                //$variantsImages = arrayCombine($images, 'id');
    //            }
    //            //$attribute[] = [
    //            //    'tag'   => 0,
    //            //    'name'  => '默认变体',
    //            //    'value' => ['default'],
    //            //];
    //            attribute.Insert();
    //            ArrayList affiliate;
    //            string main = "";
    //            //if (count($variantsImages))
    //            //{
    //            //    $main = key($variantsImages);
    //            //    unset($variantsImages[$main]);
    //            //    $affiliate = array_keys($variantsImages);
    //            //}
    //            //$variants[] = [
    //            //    'img'      => [
    //            //        'main'      => $main,
    //            //        'affiliate' => $affiliate,
    //            //    ],
    //            //    'state'    => 1,
    //            //    'ids'      => [['tag' => 0, 'val' => 0]],
    //            //    'cost'     => 0,
    //            //    'sale'     => 0,
    //            //    'quantity' => 50,
    //            //    'code'     => 0,
    //            //    'sid'      => '',
    //            //];

    //        }
    //        else
    //        {
    //            if (count($variants) == 1)
    //            {
    //                // 如果只存在一个变体 去除lp 和 ext
    //                // 20200806 lp->lps
    //                unset($variants[0]['lps']);
    //                unset($variants[0]['ext']);
    //                //20200923 17:00 zyy 只剩一个变体的时候默认是启用
    //                $variants[0]['state'] = 1;
    //            }
    //        }
    //        // 变体hash_code
    //        //$attributeTag = arrayCombine($attribute, 'tag');
    //        //foreach ($variants as $k => &$v) {
    //        //    $v['img']['main'] = (string)$v['img']['main'];
    //        //    array_walk(
    //        //        $v['img']['affiliate'],
    //        //        function(&$value) {
    //        //            $value = (string)$value;
    //        //    }
    //        //    );
    //        //    $v['quantity'] = (int)$v['quantity'];
    //        //    $v['hash_code'] = createHashCode($this->variantName($attributeTag, $v['ids']));
    //        //}
    //        await _dbContext.Product.FirstOrDefaultAsync(a => a.ID == data.ID);
    //        return ReturnArr();

    //    }

    //    /// <summary>
    //    /// 编辑产品时删除图片
    //    /// </summary>
    //    /// <param name="id"></param>
    //    /// <returns></returns>
    //    public async Task<ReturnStruct> DelImage(int id)
    //    {

    //        var res = await _dbContext.TempImages.FirstOrDefaultAsync(a => a.ID == id);
    //        if (res != null)
    //        {
    //            //if (Upload::deleteOssImage($res->oss_path))
    //            //{
    //            //    TempImages::query()->where('id', $id)->delete();
    //            //    request()->merge(['disk' => -$res->size]);
    //            //}
    //        }
    //        return ReturnArr(true, "");
    //    }

    //    public async Task<ReturnStruct> DistributionInfo($id, $companyId = 0, $groupId = 0, $userId = '', $oem_id = '')
    //    {
    //        //$where = [];
    //        //$where[] = ['company_id', '=', $companyId ?: $this->getSession()->getCompanyId()];
    //        //$groupId && $where[] = ['group_id', '=', $groupId];
    //        //$where[] = ['id', '=', $id];
    //        //$where[] = ['oem_id', '=', $oem_id ?: $this->getSession()->getOemId()];

    //        //$info = DistributionProduct::query()->where($where)
    //        //    ->select(ProductModel::COLLECT_SELECT)->first();

    //        //if (!$info) {
    //        //    return null;
    //        //}

    //        //return $info->append(['recycle', 'share', 'uiLanguages'])
    //        //        ->makeHidden(['lp_k', 'lp_s', 'lp_d']);
    //    }

    //    /// <summary>
    //    /// 移入回收站
    //    /// </summary>
    //    /// <param name="id"></param>
    //    /// <returns></returns>
    //    public async Task<ReturnStruct> MoveRecycleBin(int id)
    //    {

    //        var productModel = _dbContext.Product.FirstOrDefaultAsync(a => a.ID == id);
    //        if (productModel == null)
    //        {
    //            return ReturnArr(false, "该产品不存在");
    //        }
    //        //$typeBiter = new Type($productModel->type);
    //        //$typeBiter->setRecycle(true);
    //        //$productModel->type = $typeBiter->getFlags();
    //        await _dbContext.SaveChangesAsync();
    //        return ReturnArr(true, "");
    //    }

    //    /// <summary>
    //    /// 检测标题
    //    /// </summary>
    //    /// <param name="id"></param>
    //    /// <returns></returns>
    //    public async Task<ReturnStruct> CheckTitle(int id)
    //    {

    //        var productInfo = _dbContext.Product.FirstOrDefaultAsync(a => a.ID == id);
    //        if (productInfo == null)
    //        {
    //            return ReturnArr(false, "该产品不存在");
    //        }
    //        //$isRepeat = $this->checkProductTitle($productInfo->hash_code, $id);

    //        //$typeBiter = new Type($productInfo->type);
    //        //$typeBiter->setRepeat($isRepeat);
    //        //$productInfo->type = $typeBiter->getFlags()
    //        await _dbContext.SaveChangesAsync();
    //        return ReturnArr(true, "");
    //    }

    //    /// <summary>
    //    /// 分配EAN/UPC
    //    /// </summary>
    //    /// <param name="sku"></param>
    //    /// <param name="mode">ean/upc</param>
    //    /// <param name="cover">是否强制覆盖</param>
    //    /// <param name="changeToInit"></param>
    //    /// <returns></returns>
    //    public async Task<ReturnStruct> Distribution(int sku, int mode, bool cover = false, bool changeToInit = false)
    //    {
    //        //$oem_id = $this->getSession()->getOemId();
    //        //$company_id = $this->getSession()->getCompanyId();
    //        //$syncProduct = SyncProductModel::query()->where('oem_id',$oem_id)->where('company_id',$company_id)->where('sku',$sku)->get()->toArray();//i_pid
    //        //$pids = [];//Product
    //        //if ($syncProduct){
    //        //    $pids = array_column($syncProduct, 'pid');
    //        //}
    //        var productInfo = _dbContext.Product.Where(a => a.OEMID == oemid).whereIn(id, pids);
    //        if (productInfo == null)
    //        {
    //            return ReturnArr(false, "产品不存在");
    //        }
    //        var title = mode == (int)EanUpc.Types.EAN ? "EAN" : "UPC";

    //        //$dic = [];
    //        //foreach ($productInfo as $productInfoKey => $productInfoValue) {
    //        //    //$typeBiter = new Type($productInfo['type']);
    //        //    $variants = $productInfoValue->variants;
    //        //    if (is_array($variants))
    //        //    {
    //        //        foreach ($variants as $k => $v){
    //        //            if (isset($v['hash_code']))
    //        //            {//防止老数据不存在hashcode
    //        //                $dic[$v['hash_code']] = "";
    //        //            }
    //        //        }
    //        //        //已分配产品过滤
    //        //        $codeCollect = array_column($variants, 'code');
    //        //        if ($cover == false && !array_sum($codeCollect) == 0) {
    //        //            return returnArr(false, "当前产品{$title}已分配");
    //        //        }
    //        //    }
    //        //    else
    //        //    {
    //        //        return returnArr(false, '当前产品变体数据存在异常');
    //        //    }
    //        //}
    //        //$dicNum = count($dic);

    //        //$where = (new \App\Service\Store\Eanupc())->EANWhereDataRange('Eanupc');


    //        //$code = Eanupc::query()
    //        //    ->select(['id', 'value'])
    //        //    ->where($where)
    //        //    ->where('type', $mode)
    //        //    ->limit($dicNum)
    //        //    ->get()
    //        //    ->toArray();
    //        if (code.Count() < dicNum)
    //        {
    //            return ReturnArr(false, $"当前{title}数量不足");
    //        }
    //        else
    //        {
    //            //foreach ($dic as $key => $val){
    //            //    $dic[$key] = current($code);//获取第一个码值
    //            //    array_shift($code);//删除第一个码值
    //            //}
    //        }

    //        var ids = new[] { 1, 2 };
    //        foreach (var item in productInfo)
    //        {
    //            //$variants = $productInfoValue->variants;
    //            //foreach ($variants as $key => &$value) {
    //            //    if ($cover !== true && !empty($value['code'])) {
    //            //        continue;
    //            //    }
    //            //    if ($value['state'] == 1) {
    //            //        $t = $dic[$value['hash_code']];
    //            //        $value['code'] = $t['value'];
    //            //        $id = $t['id'];
    //            //        if (!in_array($id,$ids))
    //            //        {
    //            //            $ids[] = $id;
    //            //        }
    //            //    }
    //            //}
    //            //$productInfoValue->variants = $variants;
    //        }


    //        using var Transaction = _dbContext.Database.BeginTransaction();
    //        try
    //        {
    //            if (ids.Count() > 0)
    //            {
    //                var eanupc = _dbContext.Eanupc.Where(a => ids.Contains(a.ID));
    //                _dbContext.Eanupc.RemoveRange(eanupc);
    //            }
    //            foreach (var item in productInfo)
    //            {
    //                //$item->save();
    //                //(new SyncProduct())->updateEanupcStatus($item->id, SyncProductModel::CODE_ALL, $changeToInit);
    //            }
    //            // 提交事务
    //            Transaction.Commit();
    //        }
    //        catch (Exception e)
    //        {
    //            // 回滚事务
    //            Transaction.Rollback();
    //            return ReturnArr(false, e.Message);
    //        }

    //        return ReturnArr(true, "", productInfo[0]["lp_n"]);
    //    }

    //    /// <summary>
    //    /// 分享
    //    /// </summary>
    //    /// <param name="id"></param>
    //    /// <returns></returns>
    //    public async Task<ReturnStruct> Share(int id)
    //    {
    //        var productInfo = await _dbContext.Product.FirstOrDefaultAsync(a => a.ID == id);
    //        if (productInfo is null)
    //        {
    //            return ReturnArr(false, "该产品不存在");
    //        }
    //        //$typeBiter = new Type($productInfo->type);

    //        //if ($typeBiter->getAudit() != ProductModel::AUDIT_AUDITED) {
    //        //    return returnArr(false, '当前产品未审核通过');
    //        //}
    //        //$typeBiter->setShare(true);
    //        //$productInfo->type = $typeBiter->getFlags();
    //        await _dbContext.SaveChangesAsync();
    //        return ReturnArr(true, "");
    //    }

    //    /// <summary>
    //    /// 商品真删除
    //    /// </summary>
    //    /// <param name="id"></param>
    //    /// <param name="type">1 无需减去汇总计数 2减去汇总计数</param>
    //    /// <param name="redisReduce">1 减去redis硬盘使用 2无需减去redis硬盘使用</param>
    //    /// <returns></returns>
    //    public async Task<ReturnStruct> Delete(int id, int type = 1, int redisReduce = 1)
    //    {
    //        var productInfo = await _dbContext.Product.FirstOrDefaultAsync(a => a.ID == id);
    //        if (productInfo is null)
    //        {
    //            return ReturnArr(false, "该产品不存在");
    //        }
    //        //$productInfo = $productInfo->toArray();
    //        //$imagesIds = array_keys(arrayCombine($productInfo['images'], 'id'));

    //        // 解除库存关联
    //        var variants = productInfo.Variants;
    //        //foreach ($variants as $k => $v) {
    //        //    if (isset($v->sid))
    //        //    {
    //        //        $storageIds[] = $v->sid;
    //        //    }
    //        //}


    //        //$quickIndexProductControl = new QuickIndexProduct();
    //        //DB::beginTransaction();
    //        //try
    //        //{
    //        //    ProductModel::query()->where('id', $id)->delete();

    //        //    if (count($imagesIds))
    //        //    {
    //        //        //FIXME 资源计数-1
    //        //        Resource::query()->whereIn('id', $imagesIds)->decrement('reuse', 1);
    //        //        //获取真删除产品图片数据
    //        //        $this->deleteImage($id, $imagesIds, $redisReduce);
    //        //    }
    //        //    //FIXME 解除库存
    //        //    if (count($storageIds))
    //        //    {
    //        //    }
    //        //    //标记删除
    //        //    $quickIndexProductControl->proSet($id);

    //        //    if ($type == 2) {
    //        //        // 公司计数 / 上报
    //        //        $company = $this->getCompany($productInfo['company_id']);
    //        //        $CompanyBusinessService = new CounterCompanyBusiness();
    //        //        $CompanyBusinessService->productNumReduce($company['id']);
    //        //        //过滤超管计数
    //        //        $report_pids = json_decode($company['report_pids'], true);
    //        //        $report_pids = array_merge(array_diff($report_pids, [$this->super_id]));
    //        //        $CompanyBusinessService->reportProductNumReduce($report_pids);

    //        //        // 个人计数
    //        //        $CounterUserBusiness = new CounterUserBusiness();
    //        //        $CounterUserBusiness->productNumReduce($productInfo['user_id']);

    //        //        // 分组计数
    //        //        if ($productInfo['group_id'] != 0) {
    //        //            $CounterGroupBusinessService = new CounterGroupBusiness();
    //        //            $CounterGroupBusinessService->productNumReduce($productInfo['group_id']);
    //        //        }
    //        //    }

    //        //    if ($redisReduce == 2) {
    //        //        $CounterRequest = new CounterRequest();
    //        //        $CounterRequest->productNumReduce($productInfo['size'], $productInfo['user_id']);
    //        //    }

    //        //    // 提交事务
    //        //    Db::commit();
    //        //}
    //        //catch (\Exception $e) {
    //        //    // 回滚事务
    //        //    Db::rollback();
    //        //    return returnArr(false, $e->getMessage());
    //        //}
    //        //$redisReduce == 1 && request()->merge(['disk' => -$productInfo['size']]);
    //        return ReturnArr(true, "");
    //    }

    //    //真删除oss 调用此方法必须开启事务
    //    //redisReduce 1 oss日志 2跳过oss日志
    //    private async Task DeleteImage($pid, $imagesIds, $redisReduce)
    //    {
    //        //$images = Resource::query()->where('reuse', 0)->whereIn('id', $imagesIds)->get()->toArray();
    //        //$ossArr = [];
    //        //$networkArr = [];
    //        //if (!count($images))
    //        //{
    //        //    return true;
    //        //}
    //        //foreach ($images as $k => $v) {
    //        //    if (Str::startsWith($v['path'], 'http'))
    //        //    {
    //        //        $networkArr[$v['id']] = $v['path'];
    //        //    }
    //        //    else
    //        //    {
    //        //        $ossArr[$v['id']] = $v['path'];
    //        //    }
    //        //}
    //        //if (count($networkArr))
    //        //{
    //        //    Resource::query()->where('reuse', 0)->whereIn('id', array_keys($networkArr))->delete();
    //        //}
    //        //if (count($ossArr))
    //        //{
    //        //    $ossDisk = app('oem')->getOss();
    //        //    $res = $ossDisk->delete(array_values($ossArr));
    //        //    if ($res) {
    //        //        Resource::query()->where('reuse', 0)->whereIn('id', array_keys($ossArr))->delete();
    //        //    } else
    //        //    {
    //        //        $domain = app('oem')->getCurrentOssDomain();
    //        //        Log::channel('imageDeleteFail')->error(PHP_EOL. $domain. "\r\n".var_export($ossArr, true));
    //        //        $ossArr = [];
    //        //    }
    //        //}
    //        //if ((count($networkArr) || count($ossArr)) && $redisReduce == 1) {
    //        //    $data = [
    //        //        'Pid'      => $pid,
    //        //        'UserID'   => $this->getSessionObj()->getUserId(),
    //        //        'Username' => $this->getSessionObj()->getUsername(),
    //        //        'Oss'      => $ossArr,
    //        //        'Network'  => $networkArr,
    //        //    ];
    //        //    $this->ossDeleteLog($data);
    //        //}
    //    }

    //    protected async Task OssDeleteLog($data, $route = null)
    //    {
    //        //$logData = [
    //        //    'route' => empty($route) ? request()->getRequestUri() : $route,
    //        //    'data'  => var_export($data, true),
    //        //];
    //        //Log::channel('ossDelete')->info(var_export($logData, true));
    //    }

    //    /// <summary>
    //    /// 从回收站恢复
    //    /// </summary>
    //    /// <param name="id"></param>
    //    /// <returns></returns>
    //    public async Task<ReturnStruct> Recovery(int id)
    //    {
    //        var productInfo = await _dbContext.Product.FirstOrDefaultAsync(a => a.ID == id);
    //        if (productInfo is null)
    //        {
    //            return ReturnArr(false, "该产品不存在");
    //        }
    //        //$typeBiter = new Type($productInfo->type);

    //        //$typeBiter->setRecycle(false);
    //        //$productInfo->type = $typeBiter->getFlags();
    //        await _dbContext.SaveChangesAsync();
    //        return ReturnArr(true, "");
    //    }

    //    /// <summary>
    //    /// 分享产品导入
    //    /// </summary>
    //    /// <param name="id"></param>
    //    /// <returns></returns>
    //    public async Task<ReturnStruct> ImportShare(int id)
    //    {
    //        //$date = date('Y-m-d H:i:s', request()->input('time'));
    //        //if ($this->ruleInfo()->hasShareProductRange()) {
    //        //    $companyId = $this->getSession()->getCompanyId();
    //        //    $where[] = ['company_id', '=', $companyId];
    //        //} else
    //        //{
    //        //    $group_id = $this->getSession()->getGroupId();
    //        //    $where[] = ['group_id', '=', $group_id];
    //        //}
    //        //$productInfo = ProductModel::query()->where($where)->where('id', $id)->first();

    //        //if (!$productInfo) {
    //        //    return returnArr(false, '该产品不存在');
    //        //}
    //        //// 标题重复检测
    //        //$name = $productInfo->lp_n;
    //        //$hashCode = createHashCode($name);
    //        //$isRepeat = $this->checkProductTitle($hashCode);

    //        //// 类型
    //        //$typeBiter = new Type($productInfo->type);

    //        //// 是否重复
    //        //$typeBiter->setRepeat($isRepeat);
    //        //// 设置来源分享
    //        //$typeBiter->setFromShare(true);
    //        //// 设置为自增
    //        //$typeBiter->setCollct(false);
    //        //// 取消分享
    //        //$typeBiter->setShare(false);
    //        //// 设置非删除
    //        //$typeBiter->setRecycle(false);
    //        //// 审核
    //        //$company = $this->getCompany();
    //        //$typeBiter->setAudit($company['product_audit']);

    //        //$userId = $this->getSession()->getUserId();
    //        //$oemId = $this->getSession()->getOemId();
    //        //$insertData = [
    //        //    'oem_id'      => $oemId,
    //        //    'company_id'  => $this->getSession()->getCompanyId(),
    //        //    'user_id'     => $userId,
    //        //    'group_id'    => $this->getSession()->getGroupId(),
    //        //    'source'      => $productInfo->source,
    //        //    'lp_n'        => $name,
    //        //    'lp_k'        => $productInfo->lp_k,
    //        //    'lp_s'        => json_encode($productInfo->lp_s),
    //        //    'lp_d'        => $productInfo->lp_d,
    //        //    'hash_code'   => $hashCode,
    //        //    'languages'   => json_encode($productInfo->languages),
    //        //    'language'    => $productInfo->language,
    //        //    'price'       => $productInfo->price_original,
    //        //    'platform_id' => $productInfo->platform_id,
    //        //    'platform'    => $productInfo->platform,
    //        //    'quantity'    => json_encode($productInfo->quantity),
    //        //    'ext'         => json_encode($productInfo->ext),
    //        //    'attribute'   => json_encode($productInfo->attribute),
    //        //    'size'        => $productInfo->size,
    //        //    'pid'         => $productInfo->pid,
    //        //    'created_at'  => $date,
    //        //    'updated_at'  => $date,
    //        //];

    //        //$isImageSync = $typeBiter->hasImageSync();
    //        //$imagesIds = array_keys(arrayCombine($productInfo->images, 'id'));
    //        //if ($isImageSync) {
    //        //    // 占坑
    //        //    $typeBiter->setOngoing(true);

    //        //    $followData = [
    //        //        'type'     => $typeBiter->getFlags(),
    //        //        'image'    => $productInfo->getOriginal('image_original'),
    //        //        'variants' => json_encode([]),
    //        //    ];
    //        //    $insertData = array_merge($followData, $insertData);

    //        //    // 插入数据
    //        //    $pid = ProductModel::query()->insertGetId($insertData);
    //        //    if (!$pid) {
    //        //        return returnArr(false, '导入失败');
    //        //    }

    //        //    // 处理图片
    //        //    if ($imagesIds) {
    //        //        $imagesResource = Resource::query()->whereIn('id', $imagesIds)
    //        //            ->select(Resource::COPY_SELECT)
    //        //            ->get()
    //        //            ->toArray();
    //        //        $size = 0;
    //        //        $insertImages = collect($imagesResource)->map(
    //        //            function($item) use($userId, $oemId, $pid, $date, &$size) {
    //        //                $size += $item['size'];
    //        //                $newItem = [
    //        //                    'oss_path'   => $item['path'],
    //        //                    'size'       => $item['size'],
    //        //                    'user_id'    => $userId,
    //        //                    'oem_id'     => $oemId,
    //        //                    'oss_id'     => $item['oss_id'],
    //        //                    'created_at' => $date,
    //        //                    'product_id' => $pid,
    //        //                    'updated_at' => $date,
    //        //                    'type'       => 2,
    //        //                ];
    //        //            return $newItem;
    //        //        }
    //        //        );
    //        //        $res = TempImages::query()->insert($insertImages->toArray());
    //        //        if (!$res) {
    //        //            return $this->fail(self::TempImagesInsertFailed);
    //        //        }
    //        //        request()->merge(['disk' => $size]);

    //        //        $resourceData = TempImages::query()->select(
    //        //            [
    //        //                'id',
    //        //                'oss_path AS path',
    //        //                'size',
    //        //                'user_id',
    //        //                'oss_id',
    //        //                'oem_id',
    //        //                'type',
    //        //                'created_at',
    //        //                'updated_at'
    //        //            ]
    //        //        )
    //        //            ->where('product_id', $pid)
    //        //            ->get()
    //        //            ->toArray();
    //        //        $pathResourceData = arrayCombine($resourceData, 'path');

    //        //        $productImages = json_decode($productInfo->getOriginal('images_original'), true);


    //        //        $newImages = [];
    //        //        foreach ($resourceData as $k => $v) {
    //        //            $newImages[] = [
    //        //                'id'   => $v['id'],
    //        //                'path' => $v['path'],
    //        //                'size' => $v['size'],
    //        //            ];
    //        //        }
    //        //        //处理产品图片id
    //        //        $contrastId = [];
    //        //        foreach ($productImages as $k => $v) {
    //        //            if (!isset($pathResourceData[$v['path']]))
    //        //            {
    //        //                break;
    //        //            }
    //        //            $contrastId[$v['id']] = $pathResourceData[$v['path']]['id'];
    //        //        }
    //        //        if (count($productImages) !== count($contrastId))
    //        //        {
    //        //            return returnArr(false, '导入失败,产品图片存在差异');
    //        //        }
    //        //        $variants = json_decode($productInfo->variants_original, true);
    //        //        if (count($variants))
    //        //        {
    //        //            foreach ($variants as $key => &$value) {
    //        //                if ($value['img']['main']) {
    //        //                    $value['img']['main'] = (string)$contrastId[$value['img']['main']];
    //        //                }
    //        //                if (count($value['img']['affiliate']))
    //        //                {
    //        //                    foreach ($value['img']['affiliate'] as &$v) {
    //        //                        $v = (string)$contrastId[$v];
    //        //                    }
    //        //                }
    //        //            }
    //        //        }
    //        //        $typeBiter->setOngoing(false);
    //        //        $updateData = [
    //        //            'type'     => $typeBiter->getFlags(),
    //        //            'images'   => json_encode($newImages),
    //        //            'variants' => json_encode($variants),
    //        //        ];
    //        //        DB::beginTransaction();
    //        //        try
    //        //        {
    //        //            if (count($resourceData))
    //        //            {
    //        //                Resource::query()->insert($resourceData);
    //        //            }
    //        //            TempImages::query()->where('product_id', $pid)->delete();

    //        //            // 更新数据
    //        //            ProductModel::query()->where('id', $pid)->update($updateData);

    //        //            // 公司计数 / 上报
    //        //            $CompanyBusinessService = new CounterCompanyBusiness();
    //        //            $CompanyBusinessService->productNumAdd();
    //        //            $CompanyBusinessService->reportProductNumAdd();

    //        //            // 个人计数
    //        //            $CounterUserBusiness = new CounterUserBusiness();
    //        //            $CounterUserBusiness->productNumAdd();

    //        //            // 分组计数
    //        //            if ($this->getSessionObj()->getConcierge() != User::TYPE_OWN) {
    //        //                $CounterGroupBusinessService = new CounterGroupBusiness();
    //        //                $CounterGroupBusinessService->productNumAdd();
    //        //            }
    //        //            // 提交事务
    //        //            DB::commit();
    //        //            return returnArr(true);
    //        //        }
    //        //        catch (\Exception $e) {
    //        //            // 回滚事务
    //        //            DB::rollback();
    //        //            return returnArr(false, $e->getMessage());
    //        //        }
    //        //        }
    //        //    } else
    //        //    {
    //        //    $followData = [
    //        //        'type'     => $typeBiter->getFlags(),
    //        //        'variants' => $productInfo->variants_original,
    //        //        'images'   => $productInfo->getOriginal('images_original'),
    //        //        'image'    => $productInfo->getOriginal('image_original'),
    //        //    ];
    //        //    $insertData = array_merge($followData, $insertData);

    //        //    DB::beginTransaction();
    //        //    try {
    //        //        // 插入数据
    //        //        ProductModel::query()->insert($insertData);

    //        //        //FIXME 资源计数+1
    //        //        if (count($imagesIds)) {
    //        //            Resource::query()->whereIn('id', $imagesIds)->increment('reuse', 1);
    //        //        }

    //        //        // 公司计数 / 上报
    //        //        $CompanyBusinessService = new CounterCompanyBusiness();
    //        //        $CompanyBusinessService->productNumAdd();
    //        //        $CompanyBusinessService->reportProductNumAdd();

    //        //        // 个人计数
    //        //        $CounterUserBusiness = new CounterUserBusiness();
    //        //        $CounterUserBusiness->productNumAdd();

    //        //        // 分组计数
    //        //        if ($this->getSession()->getConcierge() != User::TYPE_OWN) {
    //        //            $CounterGroupBusinessService = new CounterGroupBusiness();
    //        //            $CounterGroupBusinessService->productNumAdd();
    //        //        }
    //        //        // 提交事务
    //        //        Db::commit();
    //        //        request()->merge(['disk' => $productInfo->size]);
    //        //        return returnArr(true);
    //        //    } catch (\Exception $e) {
    //        //        // 回滚事务
    //        //        Db::rollback();
    //        //        return returnArr(false, $e->getMessage());
    //        //    }
    //        //}
    //        //return returnArr(false, '导入失败');
    //    }

    //    /// <summary>
    //    /// 取消分享
    //    /// </summary>
    //    /// <param name="id"></param>
    //    /// <returns></returns>
    //    public async Task<ReturnStruct> CancelShare(int id)
    //    {
    //        var productInfo = await _dbContext.Product.FirstOrDefaultAsync(a => a.ID == id);
    //        if (productInfo is null)
    //        {
    //            return ReturnArr(false, "该产品不存在");
    //        }
    //        //$typeBiter = new Type($productInfo->type);

    //        //$typeBiter->setRecycle(false);
    //        //$productInfo->type = $typeBiter->getFlags();
    //        await _dbContext.SaveChangesAsync();
    //        return ReturnArr(true, "");
    //    }

    //    /// <summary>
    //    /// 关键词追加使用次数
    //    /// </summary>
    //    /// <param name="id"></param>
    //    /// <returns></returns>
    //    public async Task<ReturnStruct> IncrementKeyword(int id)
    //    {
    //        var res = await _dbContext.KeywordTemplate.FirstOrDefaultAsync(a => a.ID == id);
    //        if (res is null)
    //        {
    //            return ReturnArr(false, "没有数据");
    //        }
    //        ++res.Count;
    //        await _dbContext.SaveChangesAsync();
    //        return ReturnArr(true, "");
    //    }

    //    //@:FIXME可优化在前端处理
    //    protected async Task<ReturnStruct> variantName($attribute, $ids)
    //    {
    //        //$name = '';
    //        //foreach ($ids as $k => $v) {
    //        //    $name.= $attribute[$v['tag']]['name']. ':'. $attribute[$v['tag']]['value'][$v['val']]. ',';
    //        //}
    //        //return trim($name, ',');
    //    }

    //    //根据id获取单条产品指定字段
    //    public async Task<ReturnStruct> GetProductWhereFirst($field, $id = '')
    //    {
    //        //empty($field) && $field = ["*"];
    //        //return ProductModel::query()
    //        //    ->select($field)
    //        //    ->Id($id)
    //        //    ->firstOr(
    //        //        function() {
    //        //    return false;
    //        //}
    //        //    );
    //    }

    //    public async Task<ReturnStruct> apiInfo($field, $id = '', $user_id = '', $group_id = '', $company_id = '')
    //    {
    //        //empty($field) && $field = ["*"];
    //        //return ProductModel::query()
    //        //    ->select($field)
    //        //    ->id($id)
    //        //    ->userId($user_id)
    //        //    ->groupId($group_id)
    //        //    ->companyId($company_id);
    //    }

    //    /// <summary>
    //    /// 设置默认语言包
    //    /// </summary>
    //    /// <param name="id"></param>
    //    /// <param name="language"></param>
    //    /// <returns></returns>
    //    public async Task<ReturnStruct> SetDefaultLanguage(int id, string language)
    //    {
    //        var productInfo = await _dbContext.Product.FirstOrDefaultAsync(a => a.ID == id);
    //        if (productInfo is null)
    //        {
    //            return ReturnArr(false, "该产品不存在");
    //        }
    //        //$languageBiter = new Language($productInfo->language);

    //        //$languageCode = $languageBiter::LanguageCode;
    //        language = language.ToLower();
    //        if (language == "default")
    //        {
    //            //$biter = -1;
    //        }
    //        else
    //        {
    //            //if (!in_array($language, $languageCode, true))
    //            //{
    //            //    return returnArr(false, '非法语言code'. $language);
    //            //}
    //            //$biter = array_search($language, $languageCode, true);
    //        }
    //        //$languageBiter->setUILanguage($biter + 1);
    //        //$productInfo->language = $languageBiter->getFlags();

    //        var res = await _dbContext.SaveChangesAsync();
    //        if (res > 0)
    //        {
    //            return ReturnArr(false, "设置失败");
    //        }

    //        //return returnArr(
    //        //    true,
    //        //    '',
    //        //    [
    //        //        'uiLanguage' => $language,
    //        //        'updated_at' => $productInfo->updated_at
    //        //    ]
    //        //);
    //        return ReturnArr(true, "");
    //    }

    //    /// <summary>
    //    /// 产品设置为分销
    //    /// </summary>
    //    /// <param name="id"></param>
    //    /// <param name="type"></param>
    //    /// <param name=""></param>
    //    /// <returns></returns>
    //    public async Task<ReturnStruct> SetDistribution(int id, int type, int category_id, int sub_category_id)
    //    {
    //        //$where = [];
    //        //if ($this->getSession()->getConcierge() == User::TYPE_OWN) {
    //        //    $companyId = $this->getSession()->getCompanyId();
    //        //    if (!$companyId) {
    //        //        return returnArr(false, '服务器异常: 缺少公司');
    //        //    }
    //        //    $where[] = ['company_id', '=', $companyId];
    //        //} else
    //        //{
    //        //    $groupId = $this->getSession()->getGroupId();
    //        //    if (!$groupId) {
    //        //        return returnArr(false, '服务器异常: 缺少用户分组');
    //        //    }
    //        //    $where[] = ['group_id', '=', $groupId];
    //        //}

    //        //$where[] = ['id', '=', $id];
    //        var productInfo = _dbContext.Product.FirstOrDefaultAsync(a => a.ID == id);
    //        if (productInfo is null)
    //        {
    //            return ReturnArr(false, "该产品不存在");
    //        }
    //        //$typeBiter = new Type($productInfo->type);

    //        //$typeBiter->setDistribution((bool)$type);
    //        //$productInfo->type = $typeBiter->getFlags();

    //        //$productInfo->distribution_category_id = $category_id;
    //        //$productInfo->distribution_sub_category_id = $sub_category_id;
    //        return ReturnArr(true, "");
    //    }

    //    /// <summary>
    //    /// 获取一级分类
    //    /// </summary>
    //    /// <param name="type"></param>
    //    /// <returns></returns>
    //    public async Task<ReturnStruct> FirstCategory(Category.Types type = Category.Types.TYPE_FALSE)
    //    {
    //        var parentCategory = await _dbContext.Category.Where(a => a.Type == type)
    //            .Where(a => a.Level == 1)
    //            .ToArrayAsync();

    //        return ReturnArr(true, "", parentCategory);
    //    }

    //    public async Task<ReturnStruct> SubCategory(int pid, Category.Types type = Category.Types.TYPE_FALSE)
    //    {
    //        var data = await _dbContext.Category.Where(a => a.Type == type)
    //            .Where(a => a.Level == 1)
    //            .Where(a => a.Pid == pid)
    //            .ToArrayAsync();

    //        return ReturnArr(true, "", data);
    //    }

    //    /// <summary>
    //    /// 获取分销一级分类
    //    /// </summary>
    //    /// <param name=""></param>
    //    /// <returns></returns>
    //    public async Task<ReturnStruct> firstCategoryDistribution(company_id)
    //    {
    //        //return CategoryModel::query()
    //        //    ->type(CategoryModel::TYPE_TRUE)
    //        //    ->level(CategoryModel::FIRST)
    //        //    ->companyId($company_id)
    //        //    ->select(['id', 'name'])
    //        //    ->get()
    //        //    ->toArray();
    //    }

    //    /// <summary>
    //    /// 获取分销二级分类
    //    /// </summary>
    //    /// <param name=""></param>
    //    /// <param name=""></param>
    //    /// <param name=""></param>
    //    /// <returns></returns>
    //    public async Task<ReturnStruct> subCategoryDistribution($pid, $company_id)
    //    {
    //        //return CategoryModel::query()
    //        //    ->pid($pid)
    //        //    ->type(CategoryModel::TYPE_TRUE)
    //        //    ->level(CategoryModel::SECOND)
    //        //    ->companyId($company_id)
    //        //    ->select(['id', 'name'])
    //        //    ->get()
    //        //    ->toArray();
    //    }

    //    //绑定分销分类
    //    public async Task<ReturnStruct> binding($id, $category_id, $sub_category_id)
    //    {
    //        //$updateData = [
    //        //    'distribution_category_id'     => $category_id,
    //        //    'distribution_sub_category_id' => $sub_category_id
    //        //];
    //        //return ProductModel::query()->id($id)->update($updateData);
    //    }

    //查看下级公司产品信息
    public async Task<List<ProductModel>> getJuniorProduct(int CompanyId,int OEMID)
    {
        //$ProductModel = new ProductModel();
        //$typeBiter = Type::MASK_AUDIT;
        //$recycleBiter = Type::FLAG_RECYCLE;
        //$bit_audit = Type::BIT_AUDIT;

    return  await  _dbContext.Product.Where( x => x.CompanyID == CompanyId)
            .Where(x => x.OEMID == OEMID)
            .ToListAsync();

        //$ProductModel = new ProductModel();
        //$typeBiter = Type::MASK_AUDIT;
        //$recycleBiter = Type::FLAG_RECYCLE;
        //$bit_audit = Type::BIT_AUDIT;

        //$whereSql = '(('. $ProductModel->transformKey(
        //       'type'
        //   ). '&'. $typeBiter. ')>>'. $bit_audit. '='.ProductModel::AUDIT_AUDITED. ')';
        //$whereSql.= ' AND !('. $ProductModel->transformKey('type'). '&'. $recycleBiter. ')';

        //$whereArr[] = ['company_id', '=', $company_id];
        //$whereArr[] = ['oem_id', '=', $oem_id];
        //$data = $ProductModel
        //    ->where($whereArr)
        //    ->whereRaw($whereSql)
        //    ->select(['id', 'image', 'lp_n', 'price', 'created_at', 'updated_at'])
        //    ->paginate($this->limit);

        //return returnArr(true, '', $data);
    }

//    //分销复制产品
//    public async Task<ReturnStruct> copyDistribution($id, $companyId = '', $oem_id = '')
//    {
//        /*
//         $where[] = ['id', '=', $id];
//        if ($companyId) {
//            $where[] = ['company_id', '=', $companyId];
//        }
//        if ($oem_id) {
//            $where[] = ['oem_id', '=', $oem_id];
//        }
//        //        $where[] = ['id', '=', $id];
//        $oldProductInfo = ProductModel::query()->where($where)->find($id);
//        if (!$oldProductInfo) {
//            return returnArr(false, '产品数据不存在');
//        }
//        $productInfo = $oldProductInfo->replicate();

//        if (!$productInfo) {
//            return returnArr(false, '产品不存在');
//        }
//        $company = $this->getCompanyCache();
//        if (!$company) {
//            return returnArr(false, '无公司相关信息');
//        }
//        $rawTypeBiter = new Type($productInfo->type);
//        // 标题重复检测
//        $hashCode = createHashCode($productInfo->lp_n);
//        $isRepeat = $this->checkProductTitle($hashCode);
//        $typeBiter = new Type(BitwiseFlag::FLAG_BASE);
//        $typeBiter->setCollct(false);
//        $typeBiter->setRecycle(false);
//        $typeBiter->setAudit($company['product_audit']);
//        $typeBiter->setRepeat($isRepeat);

//        $userId = $this->getSession()->getUserId();
//        $groupId = $this->getSession()->getGroupId();
//        $currentCompanyId = $this->getSession()->getCompanyId();
//        $oemId = $this->getSession()->getOemId();

//        $ext = $productInfo->ext;
//        $rawProductExt = [
//            'key'   => 'RawProductId',
//            'name'  => '原产品ID',
//            'type'  => 5,
//            'value' => $id,
//        ];
//        $ext[] = $rawProductExt;
//        $imagesIds = array_keys(arrayCombine(json_decode($productInfo->images_original, true), 'id'));
//        if (!$rawTypeBiter->hasImageSync()) {
//            $productInfo->type = $typeBiter->getFlags();
//            $productInfo->ext = $ext;
//            $productInfo->user_id = $userId;
//            $productInfo->group_id = $groupId;
//            $productInfo->company_id = $currentCompanyId;
//            $productInfo->oem_id = $oemId;

//            DB::beginTransaction();
//            try {
//                if (count($imagesIds)) {
//                    // FIXME 资源计数+1.
//                    Resource::query()->whereIn('id', $imagesIds)->increment('reuse', 1);
//                }
//                request()->merge(['disk' => $productInfo->size]);
//                $productInfo->save();
//                $CompanyBusinessService = new CounterCompanyBusiness();
//                $CompanyBusinessService->productNumAdd();
//                $CompanyBusinessService->reportProductNumAdd();

//                // 个人计数
//                $CounterUserBusiness = new CounterUserBusiness();
//                $CounterUserBusiness->productNumAdd();

//                // 分组计数
//                if ($this->getSession()->getConcierge() != 1) {
//                    $CounterGroupBusinessService = new CounterGroupBusiness();
//                    $CounterGroupBusinessService->productNumAdd();
//                }
//                // 提交事务
//                Db::commit();
//                return returnArr(true);
//            } catch (\Exception $e) {
//                // 回滚事务
//                Db::rollback();
//                return returnArr(false, $e->getMessage());
//            }
//        } else {
//            $date = date('Y-m-d H:i:s', request()->input('time'));
//            $insertData = [
//                'oem_id'      => $oemId,
//                'company_id'  => $currentCompanyId,
//                'user_id'     => $userId,
//                'group_id'    => $this->getSession()->getGroupId(),
//                'source'      => $productInfo->source,
//                'lp_n'        => $productInfo->lp_n,
//                'lp_k'        => $productInfo->lp_k,
//                'lp_s'        => json_encode($productInfo->lp_s),
//                'lp_d'        => $productInfo->lp_d,
//                'hash_code'   => $hashCode,
//                'languages'   => json_encode($productInfo->languages),
//                'language'    => $productInfo->language,
//                'price'       => $productInfo->price_original,
//                'platform_id' => $productInfo->platform_id,
//                'platform'    => $productInfo->platform,
//                'quantity'    => json_encode($productInfo->quantity),
//                'ext'         => json_encode($ext),
//                'attribute'   => json_encode($productInfo->attribute),
//                'size'        => $productInfo->size,
//                'created_at'  => $date,
//                'updated_at'  => $date,
//            ];

//            // 占坑
//            $typeBiter->setOngoing(true);

//            $followData = [
//                'type'     => $typeBiter->getFlags(),
//                'image'    => $oldProductInfo->getOriginal('image_original'),
//                'variants' => json_encode([]),
//            ];
//            $insertData = array_merge($followData, $insertData);

//            // 插入数据
//            $pid = ProductModel::query()->insertGetId($insertData);
//            if (!$pid) {
//                return returnArr(false, '导入失败');
//            }

//            // 处理图片
//            if ($imagesIds) {
//                $imagesResource = Resource::query()->whereIn('id', $imagesIds)
//                    ->select(Resource::COPY_SELECT)
//                    ->get()
//                    ->toArray();
//                $size = 0;
//                $insertImages = collect($imagesResource)->map(
//                    function ($item) use ($userId, $oemId, $pid, $date, &$size) {
//                        $size += $item['size'];
//                        $newItem = [
//                            'oss_path'   => $item['path'],
//                            'size'       => $item['size'],
//                            'user_id'    => $userId,
//                            'oem_id'     => $oemId,
//                            'oss_id'     => $item['oss_id'],
//                            'created_at' => $date,
//                            'product_id' => $pid,
//                            'updated_at' => $date,
//                            'type'       => 2,
//                        ];
//                        return $newItem;
//                    }
//                );
//                $res = TempImages::query()->insert($insertImages->toArray());
//                if (!$res) {
//                    return $this->fail(self::TempImagesInsertFailed);
//                }
//                request()->merge(['disk' => $size]);

//                $resourceData = TempImages::query()->select(
//                    [
//                        'id',
//                        'oss_path AS path',
//                        'size',
//                        'user_id',
//                        'oss_id',
//                        'oem_id',
//                        'type',
//                        'created_at',
//                        'updated_at'
//                    ]
//                )
//                    ->where('product_id', $pid)
//                    ->get()
//                    ->toArray();
//                $pathResourceData = arrayCombine($resourceData, 'path');

//                $productImages = json_decode($oldProductInfo->getOriginal('images_original'), true);
//                $newImages = [];
//                foreach ($resourceData as $k => $v) {
//                    $newImages[] = [
//                        'id'   => $v['id'],
//                        'path' => $v['path'],
//                        'size' => $v['size'],
//                    ];
//                }
//                //处理产品图片id
//                $contrastId = [];
//                foreach ($productImages as $k => $v) {
//                    if (!isset($pathResourceData[$v['path']])) {
//                        break;
//                    }
//                    $contrastId[$v['id']] = $pathResourceData[$v['path']]['id'];
//                }
//                if (count($productImages) !== count($contrastId)) {
//                    return returnArr(false, '导入失败,产品图片存在差异');
//                }
//                $variants = json_decode($productInfo->variants_original, true);
//                if (count($variants)) {
//                    foreach ($variants as $key => &$value) {
//                        if ($value['img']['main']) {
//                            $value['img']['main'] = (string)$contrastId[$value['img']['main']];
//                        }
//                        if (count($value['img']['affiliate'])) {
//                            foreach ($value['img']['affiliate'] as &$v) {
//                                $v = (string)$contrastId[$v];
//                            }
//                        }
//                    }
//                }
//                $typeBiter->setOngoing(false);
//                $updateData = [
//                    'type'     => $typeBiter->getFlags(),
//                    'images'   => json_encode($newImages),
//                    'variants' => json_encode($variants),
//                ];
//                DB::beginTransaction();
//                try {
//                    if (count($resourceData)) {
//                        Resource::query()->insert($resourceData);
//                    }
//                    TempImages::query()->where('product_id', $pid)->delete();

//                    // 更新数据
//                    ProductModel::query()->where('id', $pid)->update($updateData);

//                    // 公司计数 / 上报
//                    $CompanyBusinessService = new CounterCompanyBusiness();
//                    $CompanyBusinessService->productNumAdd();
//                    $CompanyBusinessService->reportProductNumAdd();

//                    // 个人计数
//                    $CounterUserBusiness = new CounterUserBusiness();
//                    $CounterUserBusiness->productNumAdd();

//                    // 分组计数
//                    if ($this->getSessionObj()->getConcierge() != User::TYPE_OWN) {
//                        $CounterGroupBusinessService = new CounterGroupBusiness();
//                        $CounterGroupBusinessService->productNumAdd();
//                    }
//                    // 提交事务
//                    DB::commit();
//                    return returnArr(true);
//                } catch (\Exception $e) {
//                    // 回滚事务
//                    DB::rollback();
//                    return returnArr(false, $e->getMessage());
//                }
//            }
//        }

//         */
//    }

//    public async Task<ReturnStruct> CopyDistribution(int id, int companyId = 0, int oem_id = 0)
//    {
//        var oldProductInfo = await _dbContext.Product.FirstOrDefaultAsync(a => a.ID == id);
//        if (oldProductInfo is null)
//        {
//            return ReturnArr(false, "产品数据不存在");
//        }
//        //$productInfo = $oldProductInfo->replicate();
//        var productInfo = oldProductInfo;
//        //if (!$productInfo) {
//        //    return returnArr(false, '产品不存在');
//        //}
//        //$company = $this->getCompanyCache();
//        //if (!$company) {
//        //    return returnArr(false, '无公司相关信息');
//        //}
//        //$rawTypeBiter = new Type($productInfo->type);

//        // 标题重复检测
//        //$hashCode = createHashCode($productInfo->lp_n);
//        //$isRepeat = $this->checkProductTitle($hashCode);
//        //$typeBiter = new Type(BitwiseFlag::FLAG_BASE);
//        //$typeBiter->setCollct(false);
//        //$typeBiter->setRecycle(false);
//        //$typeBiter->setAudit($company['product_audit']);
//        //$typeBiter->setRepeat($isRepeat);

//        //$userId = $this->getSession()->getUserId();
//        //$groupId = $this->getSession()->getGroupId();
//        //$currentCompanyId = $this->getSession()->getCompanyId();
//        //$oemId = $this->getSession()->getOemId();

//        var ext = productInfo.Ext;
//        var rawProductExt = new
//        {
//            key = "RawProductId",
//            name = "原产品ID",
//            type = "5",
//            value = id
//        };

//        #region 没弄
//        //$ext[] = $rawProductExt;
//        //$imagesIds = array_keys(arrayCombine(json_decode($productInfo->images_original, true), 'id'));
//        //if (!$rawTypeBiter->hasImageSync()) {
//        //    $productInfo->type = $typeBiter->getFlags();
//        //    $productInfo->ext = $ext;
//        //    $productInfo->user_id = $userId;
//        //    $productInfo->group_id = $groupId;
//        //    $productInfo->company_id = $currentCompanyId;
//        //    $productInfo->oem_id = $oemId;

//        //    DB::beginTransaction();
//        //    try
//        //    {
//        //        if (count($imagesIds))
//        //        {
//        //            // FIXME 资源计数+1.
//        //            Resource::query()->whereIn('id', $imagesIds)->increment('reuse', 1);
//        //        }
//        //        request()->merge(['disk' => $productInfo->size]);
//        //        $productInfo->save();
//        //        $CompanyBusinessService = new CounterCompanyBusiness();
//        //        $CompanyBusinessService->productNumAdd();
//        //        $CompanyBusinessService->reportProductNumAdd();

//        //        // 个人计数
//        //        $CounterUserBusiness = new CounterUserBusiness();
//        //        $CounterUserBusiness->productNumAdd();

//        //        // 分组计数
//        //        if ($this->getSession()->getConcierge() != 1) {
//        //            $CounterGroupBusinessService = new CounterGroupBusiness();
//        //            $CounterGroupBusinessService->productNumAdd();
//        //        }
//        //        // 提交事务
//        //        Db::commit();
//        //        return returnArr(true);
//        //    }
//        //    catch (\Exception $e) {
//        //        // 回滚事务
//        //        Db::rollback();
//        //        return returnArr(false, $e->getMessage());
//        //    }
//        //    } else
//        //    {
//        //    $date = date('Y-m-d H:i:s', request()->input('time'));
//        //    $insertData = [
//        //        'oem_id'      => $oemId,
//        //        'company_id'  => $currentCompanyId,
//        //        'user_id'     => $userId,
//        //        'group_id'    => $this->getSession()->getGroupId(),
//        //        'source'      => $productInfo->source,
//        //        'lp_n'        => $productInfo->lp_n,
//        //        'lp_k'        => $productInfo->lp_k,
//        //        'lp_s'        => json_encode($productInfo->lp_s),
//        //        'lp_d'        => $productInfo->lp_d,
//        //        'hash_code'   => $hashCode,
//        //        'languages'   => json_encode($productInfo->languages),
//        //        'language'    => $productInfo->language,
//        //        'price'       => $productInfo->price_original,
//        //        'platform_id' => $productInfo->platform_id,
//        //        'platform'    => $productInfo->platform,
//        //        'quantity'    => json_encode($productInfo->quantity),
//        //        'ext'         => json_encode($ext),
//        //        'attribute'   => json_encode($productInfo->attribute),
//        //        'size'        => $productInfo->size,
//        //        'created_at'  => $date,
//        //        'updated_at'  => $date,
//        //    ];

//        //    // 占坑
//        //    $typeBiter->setOngoing(true);

//        //    $followData = [
//        //        'type'     => $typeBiter->getFlags(),
//        //        'image'    => $oldProductInfo->getOriginal('image_original'),
//        //        'variants' => json_encode([]),
//        //    ];
//        //    $insertData = array_merge($followData, $insertData);

//        //    // 插入数据
//        //    $pid = ProductModel::query()->insertGetId($insertData);
//        //        if (!$pid) {
//        //            return returnArr(false, '导入失败');
//        //        }

//        //        // 处理图片
//        //        if ($imagesIds) {
//        //        $imagesResource = Resource::query()->whereIn('id', $imagesIds)
//        //            ->select(Resource::COPY_SELECT)
//        //            ->get()
//        //            ->toArray();
//        //        $size = 0;
//        //        $insertImages = collect($imagesResource)->map(
//        //            function($item) use($userId, $oemId, $pid, $date, &$size) {
//        //                $size += $item['size'];
//        //                $newItem = [
//        //                    'oss_path'   => $item['path'],
//        //                    'size'       => $item['size'],
//        //                    'user_id'    => $userId,
//        //                    'oem_id'     => $oemId,
//        //                    'oss_id'     => $item['oss_id'],
//        //                    'created_at' => $date,
//        //                    'product_id' => $pid,
//        //                    'updated_at' => $date,
//        //                    'type'       => 2,
//        //                ];
//        //                return $newItem;
//        //            }
//        //        );
//        //        $res = TempImages::query()->insert($insertImages->toArray());
//        //            if (!$res) {
//        //                return $this->fail(self::TempImagesInsertFailed);
//        //            }
//        //            request()->merge(['disk' => $size]);

//        //        $resourceData = TempImages::query()->select(
//        //            [
//        //                'id',
//        //                'oss_path AS path',
//        //                'size',
//        //                'user_id',
//        //                'oss_id',
//        //                'oem_id',
//        //                'type',
//        //                'created_at',
//        //                'updated_at'
//        //            ]
//        //            )
//        //            ->where('product_id', $pid)
//        //            ->get()
//        //            ->toArray();
//        //        $pathResourceData = arrayCombine($resourceData, 'path');

//        //        $productImages = json_decode($oldProductInfo->getOriginal('images_original'), true);
//        //        $newImages = [];
//        //            foreach ($resourceData as $k => $v) {
//        //            $newImages[] = [
//        //                'id'   => $v['id'],
//        //                'path' => $v['path'],
//        //                'size' => $v['size'],
//        //            ];
//        //            }
//        //        //处理产品图片id
//        //        $contrastId = [];
//        //            foreach ($productImages as $k => $v) {
//        //                if (!isset($pathResourceData[$v['path']]))
//        //                {
//        //                    break;
//        //                }
//        //            $contrastId[$v['id']] = $pathResourceData[$v['path']]['id'];
//        //            }
//        //            if (count($productImages) !== count($contrastId))
//        //            {
//        //                return returnArr(false, '导入失败,产品图片存在差异');
//        //            }
//        //        $variants = json_decode($productInfo->variants_original, true);
//        //            if (count($variants))
//        //            {
//        //                foreach ($variants as $key => &$value) {
//        //                    if ($value['img']['main']) {
//        //                    $value['img']['main'] = (string)$contrastId[$value['img']['main']];
//        //                    }
//        //                    if (count($value['img']['affiliate']))
//        //                    {
//        //                        foreach ($value['img']['affiliate'] as &$v) {
//        //                        $v = (string)$contrastId[$v];
//        //                        }
//        //                    }
//        //                }
//        //            }
//        //        $typeBiter->setOngoing(false);
//        //        $updateData = [
//        //            'type'     => $typeBiter->getFlags(),
//        //            'images'   => json_encode($newImages),
//        //            'variants' => json_encode($variants),
//        //        ];
//        //            DB::beginTransaction();
//        //            try
//        //            {
//        //                if (count($resourceData))
//        //                {
//        //                    Resource::query()->insert($resourceData);
//        //                }
//        //                TempImages::query()->where('product_id', $pid)->delete();

//        //                // 更新数据
//        //                ProductModel::query()->where('id', $pid)->update($updateData);

//        //            // 公司计数 / 上报
//        //            $CompanyBusinessService = new CounterCompanyBusiness();
//        //            $CompanyBusinessService->productNumAdd();
//        //            $CompanyBusinessService->reportProductNumAdd();

//        //            // 个人计数
//        //            $CounterUserBusiness = new CounterUserBusiness();
//        //            $CounterUserBusiness->productNumAdd();

//        //                // 分组计数
//        //                if ($this->getSessionObj()->getConcierge() != User::TYPE_OWN) {
//        //                $CounterGroupBusinessService = new CounterGroupBusiness();
//        //                $CounterGroupBusinessService->productNumAdd();
//        //                }
//        //                // 提交事务
//        //                DB::commit();
//        //                return returnArr(true);
//        //            }
//        //            catch (\Exception $e) {
//        //                // 回滚事务
//        //                DB::rollback();
//        //                return returnArr(false, $e->getMessage());
//        //            }
//        //            }
//        //        }
//        #endregion
//        return ReturnArr(true, "", data);
//    }

//    /// <summary>
//    /// 获取导出ids
//    /// </summary>
//    /// <param name="id"></param>
//    /// <returns></returns>
//    public async Task<ReturnStruct> GetIds(int id)
//    {
//        //$ProductModel = new ProductModel();
//        //$onGoingBiter = Type::FLAG_IS_ONGOING;
//        //$uploadBiter = Type::FLAG_IS_UPLOAD;
//        //$recycleBiter = Type::FLAG_RECYCLE;
//        //$auditState = ProductModel::AUDIT_AUDITED;
//        //$typeBiter = Type::MASK_AUDIT;
//        //$bitAudit = Type::BIT_AUDIT;
//        //$whereArr = [];
//        //$whereSql = '!('. $ProductModel->transformKey('type'). '&'. $onGoingBiter. ')';

//        //// 审核通过
//        //$whereSql.= ' AND ('. $ProductModel->transformKey(
//        //      'type'
//        //  ). '&'. $typeBiter. ')>>'. $bitAudit. '='. $auditState;

//        //$whereSql.= ' AND !('. $ProductModel->transformKey('type'). '&'. $uploadBiter. ')';
//        //$whereArr = $this->productWhereDataRange('Product');
//        //switch ($action) {
//        //    case 'collect':
//        //        $collectBiter = Type::FLAG_COLLECT;
//        //        $operator = '=';
//        //        $whereSql.= ' AND '. $ProductModel->transformKey(
//        //              'type'
//        //          ). '&'. $collectBiter. $operator . $collectBiter;
//        //        $whereSql.= ' AND  !('. $ProductModel->transformKey('type'). '&'. $recycleBiter. ')';
//        //    break;

//        //    case 'manual':
//        //        $collectBiter = Type::FLAG_COLLECT;
//        //        $operator = '!=';
//        //        $whereSql.= ' AND '. $ProductModel->transformKey(
//        //              'type'
//        //          ). '&'. $collectBiter. $operator . $collectBiter;
//        //        $whereSql.= ' AND  !('. $ProductModel->transformKey('type'). '&'. $recycleBiter. ')';
//        //    break;
//        //}

//        //if (!count($whereArr))
//        //{
//        //    return returnArr(false, '缺少可见范围');
//        //}
//        //$whereUser = [];
//        //if ($this->getSession()->getConcierge() == User::TYPE_CHILD && $userId) {
//        //    $whereUser[] = ['user_id', '=', $userId];
//        //}
//        var data = _dbContext.Product.Select(a => new { a.ID, a.Lp_Name });
//        if (data.Count() > 300)
//        {
//            return ReturnArr(false, "产品数量过多请选择条件导出");
//        }
//        var ids = data.Select(a => new { a.ID });
//        return ReturnArr(true, "", ids);
//    }

//    public async Task<ReturnStruct> SetProductSketch(int id, string content, string mode, int isDefault = 0, string tempId = "")
//    {
//        var productInfo = await _dbContext.Product.FirstOrDefaultAsync(a => a.ID == id);
//        if (productInfo is null)
//        {
//            return ReturnArr(false, "产品不存在");
//        }
//        var language = "default";
//        if (isDefault > 0)
//        {
//            if (productInfo.ui_language is not null && isDefault == 1)
//            {
//                language = productInfo.ui_language
//            }
//            else
//            {
//                if (isDefault == 2)
//                {
//                    // 获取模板info
//                    var template = await _dbContext.SketchTemplate.FirstOrDefaultAsync(a => a.ID == Convert.ToInt32(tempId));
//                    if (template is null)
//                    {
//                        return ReturnArr(false, "该卖点模板不存在");
//                    }
//                    language = template.Language_sign;
//                }
//            }
//        }
//        var sketch = productInfo.Lp_Sketch;
//        productInfo = FillProductInfo(4, language, productInfo, SketchMode(mode, sketch, content));
//        if (productInfo is null)
//        {
//            return ReturnArr(true, $"当前产品不存在【' . {language} . '】语种,跳过");
//        }
//        //变体处理
//        var variants = productInfo.Variants;
//        //foreach ($variants as $k => &$v) {
//        //    if (isset($v['lps']) && isset($v['lps'][$language]))
//        //    {
//        //        $variantSketch = $v['lps'][$language]['s'];
//        //        $v['lps'][$language]['s'] = $this->sketchMode($mode, $variantSketch->toArray(), $content, true);
//        //    }
//        //}
//        //$productInfo->variants = $variants;
//        await _dbContext.SaveChangesAsync();
//        return ReturnArr(true, "");
//    }

//    protected async Task keywordMode($mode, $original, $content)
//    {
//        ////mode 1覆盖 2头部追加 3 尾部追加
//        //switch ($mode) {
//        //    case 1:
//        //        $original = $content;
//        //    break;
//        //    case 2:
//        //        $original = ($content. $original);
//        //    break;
//        //    case 3:
//        //        $original.= $content;
//        //    break;
//        //}
//        //return $original;
//    }

//    protected async Task selectPosition($position, $productInfo, $array, $default = 'lp_k')
//    {
//        //if ($productInfo) {
//        //    return $productInfo[$array[$position] ?? $default];
//        //}
//        //return false;
//    }

//    protected async Task<ReturnStruct> SketchMode(string mode, string sketch, string content, bool isVariants = false)
//    {
//        //if (!$isVariants) {
//        //    $content = collect($content)->map(
//        //        function($item) {
//        //        return $item['value'];
//        //    }
//        //    )->toArray();
//        //}
//        ////mode 1覆盖 2头部追加 3 尾部追加
//        //switch ($mode) {
//        //    case 1:
//        //        $sketch = $content;
//        //    break;    
//        //    case 2:
//        //        for ($i = (count($content) - 1); $i >= 0; $i--) {
//        //        array_unshift($sketch, $content[$i]);
//        //    }
//        //    break;
//        //    case 3:
//        //        for ($i = 0; $i < count($content); $i++) {
//        //        array_push($sketch, $content[$i]);
//        //    }
//        //    break;
//        //}
//        return ReturnArr(true, "", sketch);
//    }

//    protected async Task<ReturnStruct> FillProductInfo(position, language, productInfo, newContent)
//    {
//        //$array = [
//        //    'default' => [
//        //        1 => 'lp_n',
//        //        2 => 'lp_d',
//        //        3 => 'lp_k',
//        //        4 => 'lp_s',
//        //    ],
//        //    'other'   => [
//        //        1 => 'n',
//        //        2 => 'd',
//        //        3 => 'k',
//        //        4 => 's',
//        //    ]
//        //];
//        //if ($language === 'default') {
//        //    $column = $array[$language][$position];
//        //    $productInfo->$column = $newContent;
//        //} else
//        //{
//        //    $column = $array['other'][$position];
//        //    $languages = $productInfo->languages;
//        //    if (!$languages) {
//        //        return false;
//        //    }
//        //    $languages[$language][$column] = $newContent;
//        //    $productInfo->languages = $languages;
//        //}
//        //return $productInfo;
//    }

//    /// <summary>
//    /// 
//    /// </summary>
//    /// <param name="id"></param>
//    /// <param name="keywordId"></param>
//    /// <param name="content"></param>
//    /// <param name="mode">1覆盖 2头部追加 3尾部追加</param>
//    /// <param name="position">1标题 2描述 3关键词</param>
//    /// <param name="isDefault">true 默认语言 false 默认翻译语言</param>
//    /// <returns></returns>
//    public async Task<ReturnStruct> SetProductKeyword(int id, int keywordId, string content, int mode, int position, int isDefault = 1)
//    {
//        var productInfo = await _dbContext.Product.FirstOrDefaultAsync(a => a.ID == id);
//        if (productInfo is null)
//        {
//            return ReturnArr(false, "产品不存在");
//        }
//        // 主产品
//        var language = "default";

//        if (isDefault > 0)
//        {
//            if (isDefault == 1 && productInfo.ui_language is not null)
//            {
//                var language = productInfo.ui_language;
//                if (productInfo.Languages["language"] is null)
//                {
//                    return ReturnArr(false, $"当前语言【' . {language} . '】不存在");
//                }
//            }
//            else
//            {
//                if (isDefault == 2)
//                {
//                    // 获取模板info
//                    var template = await _dbContext.KeywordTemplate.FirstOrDefaultAsync(a => a.ID == keywordId);
//                    if (template is null)
//                    {
//                        return ReturnArr(false, "该关键词模板不存在");
//                    }
//                    language = template.LanguageSign;
//                }
//            }
//        }
//        //var array = language == "default" ? new { 1 = "lp_n", 2 = "lp_d", 3 = "lp_k" } : [1 => 'n', 2 => 'd', 3 => 'k'];

//        var original = SelectPosition(
//            position,
//            language == "default" ? productInfo : (productInfo.Languages["language"]) ? productInfo.Languages["language"] : ""),
//            array,
//            language== "default" ? "lp_n" : "n"
//            );
//        if (!$original && is_bool($original)) {
//            return returnArr(true, '当前产品不存在【'. $language. '】语种,跳过');
//        }
//        await _dbContext.SaveChangesAsync();
//        return ReturnArr(true, "");
//    }

//    public async Task<ReturnStruct> SetProductPrice(int id, string paramss)
//    {
//        //$whereArr = $this->productWhereDataRange('Product');
//        //if (!count($whereArr))
//        //{
//        //    return returnArr('缺少查看范围');
//        //}
//        //$productInfo = ProductModel::query()->where($whereArr)->where('id', $id)->first();
//        //if (!$productInfo) {
//        //    return returnArr('产品不存在');
//        //}
//        //$variants = $productInfo->variants;
//        //foreach ($params as $key => $val) {
//        //    foreach ($variants as $k => &$v) {
//        //        //1成本价格 2销售价格 3变体库存
//        //        switch ($val['type']) {
//        //            case 1:
//        //                isset($v['cost']) && $v['cost'] = $this->detection($val, $v['cost'], 1);
//        //            break;
//        //            case 2:
//        //                isset($v['sale']) && $v['sale'] = $this->detection($val, $v['sale'], 2);
//        //            break;
//        //            case 3:
//        //                isset($v['quantity']) && $v['quantity'] = $this->detection($val, $v['quantity'], 3);
//        //            break;
//        //        }
//        //    }
//        //}
//        //// 金额
//        //$price = [
//        //    'cost' => $this->getArrayMaxAndMin($variants, 'cost'),
//        //    'sale' => $this->getArrayMaxAndMin($variants, 'sale'),
//        //];
//        //$productInfo->price = $price;
//        //$productInfo->variants = $variants;
//        //$res = $productInfo->save();
//        //return returnArr($res);
//    }

//    /// <summary>
//    /// 获取产品单条原始数据
//    /// </summary>
//    /// <param name="id"></param>
//    /// <returns></returns>
//    public async Task<ReturnStruct> GetInfoById(int id)
//    {
//        //return ProductModel::query()->find($id);
//    }

//    /// <summary>
//    /// 获取单条分类信息
//    /// </summary>
//    /// <param name="id"></param>
//    /// <returns></returns>
//    public async Task<ReturnStruct> GetCategoryById(int id)
//    {
//        //return ProductModel::query()->find($id);
//    }

//    /// <summary>
//    /// 添加资源表信息
//    /// </summary>
//    /// <param name=""></param>
//    /// <returns></returns>
//    public async Task<ReturnStruct> AddResource(insertData)
//    {
//        //return Resource::query()->insertGetId($insertData);
//    }

//    //更新产品信息
//    public async Task<ReturnStruct> UpdateProduct(int id, updateData)
//    {
//        //return ProductModel::query()->id($id)->update($updateData);
//    }

//    //临时文件主键删除
//    public async Task<ReturnStruct> DelTempImage(int id)
//    {
//        //return TempImages::destroy($id);
//    }

//    //资源计数减一
//    public async Task<ReturnStruct> ResourceReuseDecrement(int id)
//    {
//        //return Resource::query()->where('id', $id)->decrement('reuse');
//    }

//    //上传、导出产品克隆
//    //source 来源：1=当前公司，2=下级获取上级产品
//    //type   1=上传，2=导出
//    public async Task<ReturnStruct> CopyProduct(int id, language, int source = 1, int type = 1)
//    {
//        //if (empty($language))
//        //{
//        //    return returnArr('没有选择语言包');
//        //}

//        //if ($this->getSession()->getIsDistribution() === Company::DISTRIBUTION_TRUE && $source == 2) {
//        //    $companyId = $this->getSession()->getReportId();
//        //    $company = $this->getCompany($companyId);
//        //    $oemId = $company['oem_id'];
//        //} else
//        //{
//        //    $companyId = $this->getSession()->getCompanyId();
//        //    $oemId = $this->getSession()->getOemId();
//        //}


//        //$productInfo = ProductModel::query()
//        //    ->id($id)
//        //    ->CompanyId($companyId)
//        //    ->OemId($oemId)
//        //    ->first();
//        //if (!$productInfo) {
//        //    return returnArr(false, '产品数据不存在');
//        //}

//        //// 判断产品的源产品id.
//        //$sourceId = $id;
//        //if ($this->getSession()->getIsDistribution() === Company::DISTRIBUTION_TRUE && $source == 2) {
//        //    $ext = $productInfo->ext;
//        //    $ext = arrayCombine($ext, 'key');
//        //    if (isset($ext['RawProductId']))
//        //    {
//        //        $sourceProduct = ProductModel::query()->where('company_id', $companyId)
//        //            ->where('id', $ext['RawProductId']['value'])
//        //            ->first();
//        //        if ($sourceProduct) {
//        //            $sourceId = $sourceProduct->id;
//        //        }
//        //    }
//        //}
//        //$languagePack = $productInfo->languages;

//        //if ($language && $language != 'default' && !isset($languagePack[$language])) {
//        //    return returnArr(false, '产品语言包不存在');
//        //}

//        //$typeBiter = new Type(Type::FLAG_BASE);
//        //if ($type == 1) {
//        //    $typeBiter->setUpload(true);
//        //} else
//        //{
//        //    $typeBiter->setExport(true);
//        //}

//        //$insertProductData = [
//        //    'oem_id'          => $this->getSession()->getOemId(),
//        //    'company_id'      => $this->getSession()->getCompanyId(),
//        //    'user_id'         => $this->getSession()->getUserId(),
//        //    'group_id'        => $this->getSession()->getGroupId(),
//        //    'category_id'     => $productInfo->category_id,
//        //    'sub_category_id' => $productInfo->sub_category_id,
//        //    'lp_n'            => $productInfo->lp_n,
//        //    'lp_s'            => json_encode($productInfo->lp_s),
//        //    'lp_k'            => $productInfo->lp_k,
//        //    'lp_d'            => $productInfo->lp_d,
//        //    'source'          => $productInfo->source,
//        //    'hash_code'       => createHashCode($productInfo->lp_n),
//        //    'languages'       => json_encode($productInfo->languages),
//        //    'language'        => $productInfo->language,
//        //    'type'            => $typeBiter->getFlags(),
//        //    'price'           => $productInfo->price_original,
//        //    'platform_id'     => $productInfo->platform_id,
//        //    'platform'        => $productInfo->platform,
//        //    'attribute'       => json_encode($productInfo->attribute),
//        //    'variants'        => $productInfo->variants_original,
//        //    'ext'             => json_encode($productInfo->ext),
//        //    'images'          => $productInfo->getOriginal('images_original'),
//        //    'image'           => $productInfo->getOriginal('image_original'),
//        //    'size'            => $productInfo->size,
//        //    'pid'             => $productInfo->pid,
//        //    'created_at'      => $this->datetime,
//        //    'updated_at'      => $this->datetime,
//        //];
//        //$pid = ProductModel::query()->insertGetId($insertProductData);

//        //if (count($productInfo->images))
//        //{
//        //    // FIXME 资源计数+1.
//        //    $imagesIds = array_keys(arrayCombine($productInfo->images, 'id'));
//        //    Resource::query()->whereIn('id', $imagesIds)->increment('reuse', 1);
//        //}

//        //request()->merge(['disk' => $productInfo->size]);
//        //$insertProductData['id'] = $pid;
//        //return returnArr(true, '', ['pid' => $pid, 'source_id' => $sourceId, 'info' => $insertProductData]);
//    }

//    //获取导出ids
//    public async Task<ReturnStruct> GetExportIds(
//        //$name,
//        //$pid,
//        //$taskId,
//        //$platformId,
//        //$categoryId,
//        //$time,
//        //$userId,
//        //$isAll,
//        //$action
//        )
//    {
//        //$ProductModel = new ProductModel();
//        //$onGoingBiter = Type::FLAG_IS_ONGOING;
//        //$uploadBiter = Type::FLAG_IS_UPLOAD;
//        //$recycleBiter = Type::FLAG_RECYCLE;
//        //$auditState = ProductModel::AUDIT_AUDITED;
//        //$typeBiter = Type::MASK_AUDIT;
//        //$bitAudit = Type::BIT_AUDIT;
//        //$whereSql = '!('. $ProductModel->transformKey('type'). '&'. $onGoingBiter. ')';

//        //// 审核通过
//        //$whereSql.= ' AND ('. $ProductModel->transformKey(
//        //      'type'
//        //  ). '&'. $typeBiter. ')>>'. $bitAudit. '='. $auditState;

//        //$whereSql.= ' AND !('. $ProductModel->transformKey('type'). '&'. $uploadBiter. ')';
//        //$whereArr = $this->productWhereDataRange('Product');
//        //switch ($action) {
//        //    case 'collect':
//        //        $collectBiter = Type::FLAG_COLLECT;
//        //        $operator = '=';
//        //        $whereSql.= ' AND '. $ProductModel->transformKey(
//        //              'type'
//        //          ). '&'. $collectBiter. $operator . $collectBiter;
//        //        $whereSql.= ' AND  !('. $ProductModel->transformKey('type'). '&'. $recycleBiter. ')';
//        //    break;

//        //    case 'manual':
//        //        $collectBiter = Type::FLAG_COLLECT;
//        //        $operator = '!=';
//        //        $whereSql.= ' AND '. $ProductModel->transformKey(
//        //              'type'
//        //          ). '&'. $collectBiter. $operator . $collectBiter;
//        //        $whereSql.= ' AND  !('. $ProductModel->transformKey('type'). '&'. $recycleBiter. ')';
//        //    break;
//        //}

//        //if (!count($whereArr))
//        //{
//        //    return returnArr(false, '缺少可见范围');
//        //}
//        //$whereUser = [];
//        //if ($this->getSession()->getConcierge() == User::TYPE_CHILD && $userId) {
//        //    $whereUser[] = ['user_id', '=', $userId];
//        //}
//        //$data = ProductModel::query()->whereRaw($whereSql)
//        //    ->latest('id')
//        //    ->exportList(
//        //        $whereArr,
//        //        $pid,
//        //        $taskId,
//        //        $platformId,
//        //        $categoryId,
//        //        $name,
//        //        $time,
//        //        $isAll
//        //    )->where($whereUser)->select(['id', 'lp_n'])->get();
//        //$num = count($data);
//        //$isOpen = !Str::contains($_SERVER['HTTP_HOST'], 'lxkj');

//        //if ($num > 300 && $isOpen) {
//        //    return returnArr(false, '当前条件下产品数量为'. $num. '，超过300，请重新选择条件导出');
//        //}
//        //$ids = collect($data)->map(
//        //    function($item) {
//        //    return ['id' => $item->id, 'lp_n' => $item->lp_n];
//        //}
//        //);
//        //return returnArr(true, '', $ids);
//    }

//    //克隆产品保留单一关系
//    //int $product_type 保留关系 1上传 2导出
//    public async Task<ReturnStruct> RemoveProductType(int id, int product_type = 1)
//    {
//        //$typeBiter = new Type(Type::FLAG_BASE);
//        //if ($product_type == 1) {
//        //    $typeBiter->setUpload(true);
//        //} else
//        //{
//        //    $typeBiter->setExport(true);
//        //}

//        //return ProductModel::query()->id($id)->update(['type' => $typeBiter->getFlags()]);
//    }

//    //更新克隆产品使用关系
//    public async Task<ReturnStruct> UpdateProductType(info, int product_type = 1)
//    {
//        //$typeBiter = new Type($info['type']);
//        //if ($product_type == 1) {
//        //    $typeBiter->setUpload(true);
//        //} else
//        //{
//        //    $typeBiter->setExport(true);
//        //}

//        //return ProductModel::query()->id($info['id'])->update(['type' => $typeBiter->getFlags()]);
//    }

//    //绑定分类
//    public async Task<ReturnStruct> BindCategory(string category_id = "", string sub_category_id = "", ids = [])
//    {
//        //$updateData = [
//        //    'category_id'     => $category_id ?: 0,
//        //    'sub_category_id' => $sub_category_id ?: 0
//        //];
//        //$whereArr = $this->productWhereDataRange('Product');
//        //return ProductModel::query()->where($whereArr)->whereIn('id', $ids)->update($updateData);
//    }

//    //是否是无变体产品
//    public async Task<ReturnStruct> AutoGenerate(array attribute)
//    {
//        //$autoGenerate = false;
//        // $productAttribute = $attribute;
//        // if (count($productAttribute) === 1 && $productAttribute[0]['name'] === '默认变体' && count(
//        //         $productAttribute[0]['value']
//        //     ) === 1 && $productAttribute[0]['value'][0] === 'default') {
//        //     $autoGenerate = true;
//        // }
//        // return $autoGenerate;
//    }

//    //一键清空回收站 limit 200
//    public async Task<ReturnStruct> GetWaitDelete()
//    {
//        //$ProductModel = new ProductModel();
//        //$onGoingBiter = Type::FLAG_IS_ONGOING;
//        //$uploadBiter = Type::FLAG_IS_UPLOAD;
//        //$bitExport = Type::FLAG_IS_EXPORT;
//        //$whereArr = $this->productWhereDataRange('Product');
//        //$whereSql = '!('. $ProductModel->transformKey('type'). '&'. $onGoingBiter. ')';
//        //$whereSql.= ' AND !('. $ProductModel->transformKey('type'). '&'. $bitExport. ')';
//        //$whereSql.= ' AND !('. $ProductModel->transformKey('type'). '&'. $uploadBiter. ')';
//        //$recycleBiter = Type::FLAG_RECYCLE;
//        //$whereSql.= ' AND '. $ProductModel->transformKey('type'). '&'. $recycleBiter;
//        //$data = $ProductModel
//        //    ->query()
//        //    ->whereRaw($whereSql)
//        //    ->latest('id')
//        //    ->where($whereArr)
//        //    ->select(['id', 'lp_n', 'languages', 'language'])
//        //    ->limit(200)
//        //    ->get();
//        //return returnArr(true, '', $data);
//    }
//    //检测产品语言包是否存在
//    public async Task<ReturnStruct> CheckProductLanguage(id, language)
//    {
//        //$info = ProductModel::query()->where('id', $id)->first();
//        //if (!$info) {
//        //    return returnArr(false, '产品不存在');
//        //}
//        //if (!isset($info->languages[$language]))
//        //{
//        //    return returnArr(false, $info->lp_n);
//        //}
//        //return returnArr(true, $info->lp_n);

//    }

//    public async Task<ReturnStruct> GetDownloadImages(id)
//    {
//        //$model = ProductModel::query()->where('oem_id', $this->getSessionObj()->getOemId());
//        //if (is_array($id))
//        //{
//        //    $model->whereIn('id', $id);
//        //}
//        //else
//        //{
//        //    $model->where('id', $id);
//        //}
//        //$info = $model->select(['id', 'lp_n', 'images'])->get()->toArray();

//        //if (!$info) {
//        //    return returnArr(false, '产品不存在');
//        //}
//        //return returnArr(true, '获取成功', $info);

   }
//}
