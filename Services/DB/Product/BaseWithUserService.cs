using System.Security.Claims;
using ERP.Interface;
using ERP.Data;
using ERP.Enums.Identity;
using ERP.Enums.Rule.Config.Internal;
using ERP.Extensions;
using ERP.Interface.Rule;
using ERP.Services.Identity;
using Microsoft.ApplicationInsights;
using Microsoft.EntityFrameworkCore;

namespace ERP.Services;

public abstract class BaseWithUserService : BaseService
{
    protected readonly IHttpContextAccessor _httpContextAccessor;

    protected readonly DBContext _dbContext;


    protected readonly ISessionProvider _sessionProvider;

    public BaseWithUserService(DBContext dbContext, ISessionProvider sessionProvider)
    {
        _dbContext = dbContext;
        _sessionProvider = sessionProvider;
    }
    


    protected ISession _session => _sessionProvider.Session!;

    protected int _companyId => _sessionProvider.Session!.GetCompanyID();

    protected int _userId => _sessionProvider.Session!.GetUserID();

    protected int _groupId => _sessionProvider.Session!.GetGroupID();

    protected int _oemId => _sessionProvider.Session!.GetOEMID();

    protected ClaimsPrincipal? userClaimsPrincipal = null;

    protected async Task<DataRange_2b> GetUserRange(Enum config)
    {
        if (userClaimsPrincipal is null)
        {
            var tempUserClaims =  await _dbContext.UserClaims
                .Where(m => m.UserId == _userId)
                .ToListAsync();
            userClaimsPrincipal = new ClaimsPrincipal(new List<ClaimsIdentity>
                { new ClaimsIdentity(tempUserClaims.Select(m => m.ToClaim())) });
        }
       
        
        var range = (DataRange_2b)userClaimsPrincipal.GetEditableValue(config);
        if (_session.GetRole() == Role.ADMIN || _session.GetRole() == Role.DEV)
        {
            range = DataRange_2b.Company;
        }
        return range;
    }
}