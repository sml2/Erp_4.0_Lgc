﻿using ERP.Authorization.Rule.Component;
using ERP.Extensions;
using ERP.Models.Product;
using Microsoft.EntityFrameworkCore;
using ERP.Data;
using ERP.Enums.Rule.Config.Internal;
using ERP.Exceptions.Sketches;
using ERP.Interface;
using ERP.Models.Template;
using ERP.Models.View.Product.Template;
using ERP.Models.View.Products.Template;
using ERP.Services.Identity;
using ERP.ViewModels.Template;

namespace ERP.Services.Product;

public class SketchService : BaseWithUserService
{
    public SketchService(DBContext dbContext, ISessionProvider sessionProvider) : base(dbContext, sessionProvider)
    {
    }

    private ISession Session
    {
        get => base._sessionProvider.Session!;
    }

    private IQueryable<SketchTemplateModel>? DataWatchRange(DataRange_2b range)
    {
        if (range == DataRange_2b.None)
            return null;


        var model = _dbContext.SketchTemplate.Where(m => m.OEMID == _oemId && m.CompanyID == _companyId);

        if (range == DataRange_2b.Group)
            model = model.Where(m => m.GroupID == _groupId);

        if (range == DataRange_2b.User)
            model = model.Where(m => m.UserID == (_userId));

        return model;
    }

    /// <summary>
    /// 获取卖点模板数据
    /// </summary>
    /// <param name="vm"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> List(ListDto vm, DataRange_2b range)
    {
        var model = DataWatchRange(range);

        if (model is null)
            return ReturnArr(true);

        var data = await model
            .WhenWhere(!string.IsNullOrEmpty(vm.Name), a => a.Name.Contains(vm.Name!))
            .WhenWhere(!string.IsNullOrEmpty(vm.LanguageSign), a => a.LanguageSign.Equals(vm.LanguageSign))
            .OrderByDescending(a => a.ID)
            .ToPaginateAsync(page: vm.Page, limit: vm.Limit);

        return ReturnArr(true, "", data);
    }

    /// <summary>
    /// 新增修改卖点模板
    /// </summary>
    /// <param name="vm"></param>
    /// <param name="range"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> AddOrUpdateSketch(SketchEditViewModel vm, DataRange_2b range)
    {
        if (vm.ID > 0)
        {
            var info = await GetInfo(vm.ID, range);
            if (info == null)
            {
                return ReturnArr(false, "该条卖点模板不存在");
            }

            if (info.UpdatedAtTicks != vm.UpdatedAtTicks)
            {
                return ReturnArr(false, "数据不同步");
            }

            info.UpdatedAt = DateTime.Now;
            ;
            info.Name = vm.Name;
            info.Content = vm.Content.ToString()!;
            info.Language = vm.Language;
            info.LanguageSign = vm.LanguageSign.ToString().ToLower();
            _dbContext.SketchTemplate.Update(info);
        }
        else
        {
            var oemId = Session.GetOEMID();
            var userId = Session.GetUserID();
            var groupId = Session.GetGroupID();
            var companyId = Session.GetCompanyID();
            var data = new SketchTemplateModel()
            {
                Name = vm.Name,
                Language = vm.Language,
                LanguageSign = vm.LanguageSign.ToString().ToLower(),
                Content = vm.Content + string.Empty ?? string.Empty,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                OEMID = oemId,
                UserID = userId,
                GroupID = groupId,
                CompanyID = companyId
            };
            await _dbContext.SketchTemplate.AddAsync(data);
        }

        await _dbContext.SaveChangesAsync();
        return ReturnArr();
    }

    public async Task<bool> Destroy(int id, DataRange_2b range)
    {
        var sketchTemplate = await GetInfo(id, range);
        if (sketchTemplate == null)
        {
            throw new NotFoundException("找不到相关数据#1");
        }

        _dbContext.SketchTemplate.Remove(sketchTemplate);
        return await _dbContext.SaveChangesAsync() > 0;
    }

    public async Task<ReturnStruct> Info(int id, DataRange_2b range)
    {
        var sketchTemplate = await GetInfo(id, range);
        if (sketchTemplate == null)
        {
            return ReturnArr(false, "没有数据");
        }

        return ReturnArr(true, "", sketchTemplate);
    }

    /// <summary>
    /// 获取详情
    /// </summary>
    /// <param name="id"></param>
    /// <param name="range"></param>
    /// <returns></returns>
    private async Task<SketchTemplateModel?> GetInfo(int id, DataRange_2b range)
    {
        var model = DataWatchRange(range);
        if (model is null)
            return null;

        return await model.FirstOrDefaultAsync(a => a.ID == id);
    }

    public async Task<List<SketchViewModel>?> GetSketch(DataRange_2b range)
    {
        var model = DataWatchRange(range);

        if (model is null)
            return null;

        return await model
            .Select(m => new SketchViewModel(m))
            .ToListAsync();
    }
}