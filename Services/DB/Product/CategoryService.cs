using ERP.Authorization.Rule.Component;
using ERP.Data;
using ERP.Enums.Rule.Config.Internal;
using ERP.Exceptions.Category;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.Product;
using ERP.Models.View.Product.Category;
using ERP.Services.DB.Statistics;
using ERP.Services.Finance;
using Microsoft.EntityFrameworkCore;

namespace ERP.Services.Product;

public class CategoryService : BaseWithUserService
{

    public CategoryService(DBContext dbContext, ISessionProvider sessionProvider) : base(dbContext, sessionProvider)
    {
    }



    private IQueryable<CategoryModel>? DataWatchRange(DataRange_2b range)
    {
        if (range == DataRange_2b.None)
            return null;

        var model = _dbContext.Category.Where(m => m.OEMID == _oemId && m.CompanyID == _companyId);

        if (range == DataRange_2b.Group)
            model = model.Where(m => m.GroupID == _groupId);

        if (range == DataRange_2b.User)
            model = model.Where(m => m.UserID == (_userId));

        return model;
    }

    public async Task<CategoryModel?> GetInfoById(int id, DataRange_2b range)
    {
        var model = DataWatchRange(range);
        if (model is null)
            return null;

        return await model.FirstOrDefaultAsync(a => a.ID == id);
    }


    public async Task<ReturnStruct> List(DataRange_2b range)
    {
        var model = DataWatchRange(range);

        if (model is null)
            return ReturnArr(data: ArrayLevel(new List<CategoryModel>(), new List<CategoryModel>()));

        //暂定获取个人
        var list = await model.Where(m => m.UserID == _session.GetUserID()).ToListAsync();

        return ReturnArr(data: ArrayLevel(list, new List<CategoryModel>()));
    }


    /// <summary>
    /// 获取单条分类
    /// </summary>
    /// <param name="id"></param>
    /// <param name="range"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> Info(int id, DataRange_2b range)
    {
        var info = await GetInfoById(id, range);

        return info is not null ? ReturnArr(true, "", info) : ReturnArr(false);
    }

    /// <summary>
    /// 删除分类
    /// </summary>
    /// <param name="id"></param>
    /// <param name="range"></param>
    /// <returns></returns>
    public async Task<bool> Destroy(int id, DataRange_2b range)
    {
        var info = await GetInfoById(id, range);
        if (info == null)
        {
            throw new NotFoundException("找不到相关数据#1");
        }

        if (info.Level == 1)
        {
            //是否存在子类
            var subCount = await _dbContext.Category.Where(a => a.Pid == id).CountAsync();
            if (subCount > 0)
            {
                throw new DestroyException("该分类下面存在子类，请先删除子类#2");
            }
            //是否存在商品
            var productCount = await _dbContext.Product.Where(a => a.CategoryId == id).CountAsync();
            if (productCount > 0)
            {
                throw new DestroyException("该分类下面存在产品，请先删除产品#3");
            }
        }

        if (info.Level == 2)
        {
            //是否存在产品
            var productCount = await _dbContext.Product.Where(a => a.SubCategoryId == id).CountAsync();
            if (productCount > 0)
            {
                throw new DestroyException("该分类下面存在产品，请先删除产品#4");
            }
        }
        

        _dbContext.Category.Remove(info);
        return await _dbContext.SaveChangesAsync() > 0;
    }

    public async Task<ReturnStruct> AddOrUpdateCategory(EditVM req)
    {
        var testMoney = new MoneyFinancialAffairs(MoneyFactory.Instance.Create(req.Money, "CNY"));

        var statisticService = new Statistic(_dbContext, new FinancialAffairsService(_dbContext, _sessionProvider));


        var level = (req.Pid == 0) ? 1 : 2;

        //是否重复 暂定公司范围内
        var hashCode = req.Name.GetHashCode();

        var repeat = await _dbContext.Category
            .Where(m => m.Level == level && m.CompanyID == _session.GetCompanyID() &&
                        m.HashCode == hashCode)
            .WhenWhere(req.ID != 0, m => m.ID != req.ID)
            .CountAsync();

        if (repeat > 0)
        {
            return ReturnArr(false, "分类名称重复");
        }

        var now = DateTime.Now;

        if (req.ID > 0)
        {
            var info = await _dbContext.Category.Where(m => m.ID == req.ID).FirstOrDefaultAsync();

            if (info == null)
            {
                return ReturnArr(false, "找不到相关数据");
            }

            if (info.UpdatedAtTicks != req.UpdatedAtTicks)
            {
                return ReturnArr(false, "数据不同步");
            }


            var transaction1 = _dbContext.Database.BeginTransaction();

            try
            {
                info.Name = req.Name;
                info.HashCode = hashCode;
                info.UpdatedAt = now;
                _dbContext.Category.Update(info);
                await _dbContext.SaveChangesAsync();


                transaction1.Commit();
                return ReturnArr(true, "修改成功");
            }
            catch (Exception e)
            {
                transaction1.Rollback();
                return ReturnArr(false, msg: e.Message);
            }
        }


        var insertData = new CategoryModel()
        {
            Pid = req.Pid,
            Level = level,
            Name = req.Name,
            HashCode = hashCode,
            Type = CategoryModel.Types.TYPE_FALSE,
            UserID = _session.GetUserID(),
            GroupID = _session.GetGroupID(),
            CompanyID = _session.GetCompanyID(),
            OEMID = _session.GetOEMID(),
            CreatedAt = now,
            UpdatedAt = now
        };

        var transaction = _dbContext.Database.BeginTransaction();

        try
        {
            //新增统计但财务表中并无产生金额的数据主键id


            await _dbContext.Category.AddAsync(insertData);
            await _dbContext.SaveChangesAsync();


            transaction.Commit();

            return ReturnArr(true, "添加成功");
        }
        catch (Exception e)
        {
            transaction.Rollback();
            return ReturnArr(false, msg: e.Message);
        }
    }

    /// <summary>
    /// 获取一级分类
    /// </summary>
    /// <returns></returns>
    public async Task<object> FirstCategory(DataRange_2b range,bool isProduct)
    {
        var model = DataWatchRange(range);
        if (model is null)
            return new();
        //暂定获取个人
        var firstCategory = await model
            .Where(m => m.Level == 1)
            .Select(m => new { m.ID, m.Name })
            .ToListAsync();

        if (!isProduct)
        {
            firstCategory.Insert(0, new { ID = 0, Name = "一级分类" });
        }

        return firstCategory;
    }

    /// <summary>
    /// 获取二级分类
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<object> SubCategory(int id)
    {
        //暂定获取个人
        return await _dbContext.Category
            .Where(m => m.Level == 2 && m.CompanyID == _session.GetCompanyID() && m.Pid == id)
            .Select(m => new { m.ID, m.Name })
            .ToListAsync();
    }

    /// <summary>
    /// 分类递归处理
    /// </summary>
    /// <param name="lists"></param>
    /// <param name="arrays"></param>
    /// 
    /// <param name="pid"></param>
    /// <returns></returns>
    public List<CategoryModel> ArrayLevel(List<CategoryModel> lists, List<CategoryModel> arrays, int pid = 0)
    {
        //找当前层级下级(如果parentId==null那就是第一级)
        List<CategoryModel> result = lists.Where(a => a.Pid == pid).ToList();
        foreach (var item in result)
        {
            arrays.Add(item);
            ArrayLevel(lists, arrays, item.ID);
        }

        return arrays;
    }

    /// <summary>
    /// 分销产品获取上级公司产品分类
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="oemId"></param>
    public async Task<List<CategoryModel>> DistributionCategory(int companyId, int oemId)
    {
        var list = await _dbContext.Category.Where(c =>
                c.CompanyID == companyId && c.OEMID == oemId && c.Type == CategoryModel.Types.TYPE_TRUE)
            .ToListAsync();
        // ->select(['id', 'pid', 'level', 'name'])
        return ArrayLevel(list, new List<CategoryModel>());
    }

    public object ParentCategory()
    {
        throw new NotImplementedException();
    }
}