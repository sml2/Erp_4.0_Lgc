using ERP.Data;
using ERP.Extensions;
using ERP.Models.Product;
using ERP.Models.View.Products.Product;
using Microsoft.EntityFrameworkCore;

namespace ERP.Services.Product;

public class ProductValidityService
{
    protected readonly DBContext _dbContext;

    public ProductValidityService(DBContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<DbSetExtension.PaginateStruct<ProductValidityModel>> List(int userId)
    {
        return await _dbContext.ProductValidity.Where(m => m.UserId == userId).OrderByDescending(m => m.ID)
            .ToPaginateAsync();
    }

    public async Task<ProductValidityModel?> GetInfoById(int Id) =>
        await _dbContext.ProductValidity.Where(m => m.ID == Id).FirstOrDefaultAsync();

    public async Task<bool> AddOrEdit(ProductValidityDto req, ISession? session)
    {
        ProductValidityModel item = new();
        if (req.Id.HasValue)
        {
            var info = await GetInfoById(req.Id.Value);
            if (info == null)
                throw new Exception("找不到相关数据");
            item = info;
            item.Remark = req.Remark;
            item.Cost = req.Cost;
            await _dbContext.ProductValidity.SingleUpdateAsync(item);
        }
        else
        {
            item = req.To(session);
            await _dbContext.ProductValidity.AddAsync(item);
        }


        return await _dbContext.SaveChangesAsync() > 0;
    }

    public async Task<bool> Delete(int id)
    {
        var info = await GetInfoById(id);
        if (info == null)
            // throw new Exception("找不到相关数据");
            return false;

        _dbContext.ProductValidity.Remove(info);
        return await _dbContext.SaveChangesAsync() > 0;
    }

    private async Task<ProductValidityModel?> GetInfoByUserId(int userId) =>
        await _dbContext.ProductValidity.Where(m => m.UserId == userId).FirstOrDefaultAsync();

    public async Task<ProductValidityModel?> GetUserProductValidityModel(int userId) => await GetInfoByUserId(userId);

    public async Task<DateTime> GetUserProductValidity(int userId)
    {
        var item = await GetInfoByUserId(userId);
        if (item == null)
            return DateTime.Now.AddMonths(6);

        return item.Validity;
    }

    public async Task<(DateTime showDt, DateTime? trueDt)> GetUserProductDelayed(int userId)
    {
        var now = DateTime.Now;
        var dt = Convert.ToDateTime(now.ToString("yyyy-MM-dd 23:59:59"));
        var systemSet = dt.AddMonths(-6).AddDays(-1).LastDayTick();
        var item = await _dbContext.ProductValidity
            .Where(m => m.UserId == userId)
            .OrderByDescending(m => m.Validity)
            .FirstOrDefaultAsync();
        if (item == null)
            return (systemSet, dt.AddMonths(6).AddDays(1).LastDayTick());

        return (item.Validity, item.Validity);
    }
}