using System.Text;
using Common.Enums.Products;
using ERP.Extensions;
using Ryu.Extensions;
using Sy.Net;
using Sy.Text;

namespace ERP.Services.Product;

public class ToolsService : BaseService
{
    public ToolsService()
    {
    }

    public string expMsg = string.Empty;

    void ExpHandle(Exception exp)
    {
        StringBuilder sb = new(exp.Message);
        sb.AppendLine();
        while (exp.InnerException is not null)
        {
            exp = exp.InnerException;
            sb.AppendLine(exp.Message);
        }

        expMsg = sb.ToString();
    }

    public async Task<string> GetKeyword(string asin, MarketplaceIds markerplaceIds)
    {
        var domain = markerplaceIds.GetDescriptionByKey("Domain")!;
        string url =
            $"{domain}/abis/ajax/reconciledDetailsV2?asin={asin.Trim()}&marketplaceId={markerplaceIds.GetDescription()}";
        return await Task.FromResult(Factory.Http("http://httpproxy.miwaimao.com/").SetHeader("ProxyUrl", url)
            .SetHeader("AmazonToken", "True")
            .SetExceptionHandle(ExpHandle).Get());
    }

    public async Task<object> GetUnavailableProducts(string keyword, Countries country, bool? accurate)
    {
        // throw new Exceptions.Exception("无效解析规则#46");

        var searchRaw = country.GetDescriptionByKey("SearchRaw");
        var search = String.Format(searchRaw,
            accurate is not null && accurate == true ? $"""{ keyword}  """ : keyword);
        var q = Sy.Text.Encode.Url_Encode(search);
        var cookie = "MUID=2FD254470BCF64AE1760459D0AA9656B; SRCHD=AF=NOFORM; SRCHHPGUSR=SRCHLANG=en; SRCHUID=V=2&GUID=3F1AF045D4DD4660B975D38A45355174&dmnchg=1; SRCHUSR=DOB=20220707; SUID=M; _EDGE_S=F=1&SID=29EF6B7394796A8D39BB7AA9951F6B3C; _EDGE_V=1; _SS=SID=29EF6B7394796A8D39BB7AA9951F6B3C; MUIDB=2FD254470BCF64AE1760459D0AA9656B";

        // 'Cookie = "MUID=295E31956F996C2624BD21676EFF6D67; MUIDB=295E31956F996C2624BD21676EFF6D67; _EDGE_V=1; SRCHD=AF=BEHPTB; SRCHUID=V=2&GUID=30EFB44563514185993551102580DA29&dmnchg=1; SRCHUSR=DOB=20211117&T=1637153049000&TPC=1637153112000; ABDEF=V=13&ABDV=13&MRNB=1637153597275&MRB=0; _HPVN=CS=eyJQbiI6eyJDbiI6MSwiU3QiOjAsIlFzIjowLCJQcm9kIjoiUCJ9LCJTYyI6eyJDbiI6MSwiU3QiOjAsIlFzIjowLCJQcm9kIjoiSCJ9LCJReiI6eyJDbiI6MSwiU3QiOjAsIlFzIjowLCJQcm9kIjoiVCJ9LCJBcCI6dHJ1ZSwiTXV0ZSI6dHJ1ZSwiTGFkIjoiMjAyMS0xMS0xN1QwMDowMDowMCIsIklvdGQiOjAsIkd3YiI6MCwiRGZ0IjpudWxsLCJNdnMiOjAsIkZsdCI6MCwiSW1wIjoyNn0=; SRCHHPGUSR=SRCHLANG=en&BRW=HTP&BRH=M&CW=895&CH=929&SW=1920&SH=1080&DPR=1&UTC=480&DM=0&HV=1637156143&WTS=63772749923&BZA=0&NEWWND=1&NRSLT=50c&LSL=0&AS=1&POFF=0&NNT=1&HAP=0&VSRO=1"
        // 'https://www.bing.com/search?q=site%3Awww.amazon.com%20%2Fdp%2F%20%22currently%20unavailable%22%20address POST会302到本地化，有结果
        // 'Dim URL = $"https://bing.com/search?q={q}&ensearch=1" ‘无结果
        // 'Dim URL = $"https://cn.bing.com/search?q={q}"

        var url = $"https://cn.bing.com/search?q={q}&ensearch=1";//无结果
        string html = Factory.Http(url).SetCookies(cookie).SetExceptionHandle(ExpHandle).Get();
        if (html.Length == 0)
        {
            throw new Exceptions.Exception("返回为空,未知原因#41");
        }

        var spliter = "class=\"b_algo\"";

        var rm = country.GetDescriptionByKey("SearchReplace");
        rm = rm.Substring(0, 1).ToUpper() + rm.Substring(1);
        if (html.Contains(spliter))
        {
            var list = html.Split(spliter);
            foreach (var page in list)
            {
                string itemURL = "NotFound";
                string itemDesc = "NotFound";
                string itemTitle = "NotFound";
                var startH2 = page.IndexOf("<h2>", StringComparison.Ordinal);
                var endH2 = page.IndexOf("</h2>", StringComparison.Ordinal);

                if (startH2 > -1 && endH2 > -1)
                {
                    var h2 = page.Substring(startH2, page.Length - endH2);
                    var startA = h2.IndexOf("<a>", StringComparison.Ordinal);
                    var endA = h2.IndexOf("<a>", StringComparison.Ordinal);
                    if (startA > -1 && endA > -1)
                    {
                        var link = h2.Substring(startA, h2.Length - endA);
                        
                    }
                }

                List<string> H2Midor = new List<string>() {"<h2>", "</h2>"};
            }
        }
        else
        {
            throw new Exceptions.Exception("无效解析规则#46");
        }

        return new { };
    }
}