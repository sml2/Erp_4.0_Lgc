using ERP.Controllers.Product;
using ERP.Data;
using Microsoft.EntityFrameworkCore;
//using ModelView = ERP.Models.View.Products.Product.productUploadView;
//using ModelLog = ERP.Models.DB.Product.ProductUploadLog;
using Model = ERP.Models.DB.Product.ProductUpload;
//using ModelJson = ERP.Models.DB.Product.ProductUploadJson;
using ERP.Models.DB.Product;
using ERP.Models.View.Products.Product;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.DB.Stores;
using ERP.Services.Product;
using ERP.Models.Product;
using PlatformDataCache = ERP.Services.Caches.PlatformData;
using Newtonsoft.Json;
using ERP.Services.Api;
using Sy.Text;
using ERP.Models.ProductNodeType.Amazon;
using System.Linq;
using ERP.Enums.Rule;
using ERP.Models.Stores;
using ERP.Models.View.Products.Export;
using ERP.Services.Stores;
using Z.EntityFramework.Plus;
using Enumerable = System.Linq.Enumerable;

namespace ERP.Services.DB.Product;

public class ProductUploadService : BaseWithUserService
{
    protected readonly ILogger<ProductUploadService> _logger;
    private readonly Caches.Store _storeCache;
    private readonly SyncProductService _syncProductService;
    private readonly DBContext _dbContext;

    private readonly ProductService _productService;

    private readonly PlatformDataCache _platformDataCache;

    private readonly IOrderFactory ApiOrderFactory;

    public ProductUploadService(PlatformDataCache platformDataCache, ProductService productService,
        ISessionProvider sessionProvider, DBContext dbContext, IOrderFactory _ApiOrderFactory,
        ILogger<ProductUploadService> logger, Caches.Store storeCache, SyncProductService syncProductService) : base(
        dbContext,
        sessionProvider)
    {
        _dbContext = dbContext;
        _logger = logger;
        _storeCache = storeCache;
        _syncProductService = syncProductService;
        _productService = productService;
        _platformDataCache = platformDataCache;
        ApiOrderFactory = _ApiOrderFactory;
    }

    public async Task<bool> AddInternalAsync(Model model) => await AddInternalsAsync(new List<Model>() { model });

    /// <summary>
    /// 
    /// </summary>
    /// <param name="models"></param>
    /// <returns></returns>
    public async Task<bool> AddInternalsAsync(List<Model> productUploads)
    {
        List<Model> addmodelList = new();
        List<Model> updatemodelList = new();
        try
        {
            foreach (var model in productUploads)
            {
                var m = await GetByPid(model.Product.ID);
                if (m == null)
                {
                    addmodelList.Add(model);
                }
                else
                {
                    updatemodelList.Add(model);
                }

                if (addmodelList.Count > 0)
                {
                    await _dbContext.productUpload.AddRangeAsync(addmodelList);
                }

                if (updatemodelList.Count > 0)
                {
                    _dbContext.productUpload.UpdateRange(addmodelList);
                }
            }

            return true;
        }
        catch (Exception e)
        {
            _logger.LogError($" ProductUploadService  error:{e.Message}");
            _logger.LogError(e.StackTrace);
            return false;
        }
    }

    public async Task<bool> UpdateAsync(List<Model> productUploads)
    {
        try
        {
            _dbContext.productUpload.UpdateRange(productUploads);

            return true;
        }
        catch (Exception e)
        {
            _logger.LogError($" ProductUploadService  error:{e.Message}");
            _logger.LogError(e.StackTrace);
            return false;
        }
    }


    public async Task<Model?> GetByPid(int pid)
    {
        return await _dbContext.productUpload.FirstOrDefaultAsync(x => x.Product.ID == pid);
    }

    public List<Model> GetByPids(List<int> pids)
    {
        return _dbContext.productUpload.Where(x => pids.Contains(x.Product.ID)).ToList();
    }


    public async Task<List<ProductModel>> GetListById(List<int> pids, int oemId)
    {
        return await _dbContext.Product.Where(m => m.OEMID == oemId && pids.Contains(m.ID)).ToListAsync();
    }

    //public async Task<List<ModelJson>?> GetModelJsonAsync(int pid)
    //{
    //    return await _dbContext.productUploadJson.Where(x => x.ProductID == pid).ToListAsync();
    //}


    #region 查询

    public async Task<DbSetExtension.PaginateStruct<ListViewUploadModel>> List(UploadSearchVM vm)
    {
        var amazonNation = _platformDataCache.List(new List<Platforms>() { Platforms.AMAZONSP })
            .Where(x => x.GetAmazonData()!.Unit.IsNotWhiteSpace()).Select(x => new MarketPlaceInfo
                { MarketPlace = x.GetAmazonData().Marketplace, Name = x.GetAmazonData().Name }).ToList();

        var data = _dbContext.productUpload.Include(x => x.Product).Include(x => x.Store).Include(x => x.User)
            .Where(m => m.OEMID == _oemId && m.CompanyID == _companyId)
            .WhenWhere(!string.IsNullOrEmpty(vm.ProductName), x => x.Product.Title.Contains(vm.ProductName))
            .WhenWhere(vm.StoreID != 0, x => x.Store.ID == vm.StoreID)
            .WhenWhere(!string.IsNullOrWhiteSpace(vm.LanguageSign), x => x.Product.Language == (long)vm.lang)
            .WhenWhere(!string.IsNullOrWhiteSpace(vm.Marketplace), x => x.MarketPlace == vm.Marketplace)
            .WhenWhere(vm.StateValue == 1, x => x.Result <= UploadResult.NeedToUpload)
            .WhenWhere(vm.StateValue == 2, x => x.Result == UploadResult.Uploading)
            .WhenWhere(vm.StateValue == 3, x => x.Result == UploadResult.UploadFail)
            .WhenWhere(vm.StateValue == 4, x => x.Result == UploadResult.UploadSuccess)
            .WhenWhere(vm.TaskID > 0, x => x.SyncUploadTaskId == vm.TaskID)
            .OrderByDescending(c => c.ID)
            .Select(m =>
                new ListViewUploadModel(m, m.Product.Sku, _productService.GetTranslated(m.Product, false),
                    amazonNation));

        return await Task.FromResult(data.ToPaginate(page: vm.Page, limit: vm.Limit));
    }

    #endregion


    /// <summary>
    /// 不管有没有上传成功，全部逻辑删除
    /// </summary>
    /// <param name="pid"></param>
    /// <returns></returns>
    public async Task<bool> Delete(VMPids vm)
    {
        using (var trans = await _dbContext.Database.BeginTransactionAsync())
        {
            try
            {
                var listUpload = _dbContext.productUpload.Where(x => vm.Pids.Contains(x.Product.ID)).ToList();
                if (listUpload.Count > 0)
                {
                    _dbContext.productUpload.RemoveRange(listUpload);
                }

                await _dbContext.SaveChangesAsync();
                await trans.CommitAsync();
                return true;
            }
            catch (Exception e)
            {
                await trans.RollbackAsync();
                return false;
            }
        }
    }


    //public List<ListViewUploadJsonModel> GetProductInfo(SearchJsonVM vm)
    //{
    //    return _dbContext.productUploadJson.Where(x => x.ProductID == vm.Pid).Select(m => new ListViewUploadJsonModel(m)).ToList();
    //}

    //public ModelJson? GetJsonByid(int id)
    //{
    //    return _dbContext.productUploadJson.FirstOrDefault(x => x.ID == id);
    //}

    //public List<ModelJson> GetJsonByPid(int pid)
    //{
    //    return _dbContext.productUploadJson.Where(x => x.product.ID == pid).ToList();
    //}

    //public Newtonsoft.Json.Linq.JObject InsertData(string storeName, string attributejson, long nodeID, bool isParent, string ParentSku, UploadProductVm item,string unit)
    //{
    //    var Obj = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JObject>(attributejson);

    //    string date = DateTime.UtcNow.ToString("yyyy-MM-dd");
    //    string endDate = DateTime.UtcNow.AddYears(2).ToString("yyyy-MM-dd");
    //    ////////////必填、必传的
    //    //externally_assigned_product_identifier
    //    if (Obj.GetValue("externally_assigned_product_identifier") != null)
    //    {
    //        Obj["externally_assigned_product_identifier"][0]["value"] = item.Code;
    //    }
    //    //item_name
    //    if (Obj.GetValue("item_name") != null)
    //    {
    //        Obj["item_name"][0]["value"] = item.Title;
    //    }
    //    //model_name
    //    if (Obj.GetValue("model_name") != null)
    //    {
    //        Obj["model_name"][0]["value"] = item.Title;
    //    }
    //    //bullet_point 卖点
    //    if (Obj.GetValue("bullet_point") != null)
    //    {
    //        Obj["bullet_point"][0]["value"] = string.Join(',', item.Sketches);
    //    }
    //    //product_description  产品描述
    //    if (Obj.GetValue("product_description") != null)
    //    {
    //        Obj["product_description"][0]["value"] = item.Desc;
    //    }
    //    //generic_keyword  关键字
    //    if (Obj.GetValue("generic_keyword") != null)
    //    {
    //        Obj["generic_keyword"][0]["value"] = item.Keyword;
    //    }
    //    //recommended_browse_nodes
    //    if (Obj.GetValue("recommended_browse_nodes") != null)
    //    {
    //        Obj["recommended_browse_nodes"][0]["value"] = nodeID + " ";
    //    }
    //    //////'purchasable_offer',
    //    //if (Obj.GetValue("purchasable_offer") != null)
    //    //{
    //    //    JObject channel = (JObject)Obj["purchasable_offer"][0];
    //    //    channel.Remove();
    //    //}
    //    //'list_price',
    //    if (Obj.GetValue("list_price") != null)
    //    {
    //        Obj["list_price"][0]["value"] = item.Sale.Value.ToFixed();
    //        Obj["list_price"][0]["currency"] = unit;                
    //    }
    //    //'merchant_shipping_group', //default
    //    if (Obj.GetValue("merchant_shipping_group") != null)
    //    {
    //        var s = (string)Obj["merchant_shipping_group"][0]["value"];
    //        if (string.IsNullOrWhiteSpace(s))
    //        {
    //            Obj["merchant_shipping_group"][0]["value"] = "DEFAULT";
    //        }
    //    }
    //    //'main_product_image_locator',            
    //    if (Obj.GetValue("main_product_image_locator") != null)
    //    {
    //        Obj["main_product_image_locator"][0]["media_location"] = item.Main;
    //    }
    //    //other_product_image_locator_
    //    if (Obj.GetValue("other_product_image_locator_1") != null)
    //    {
    //        int count = item.Affiliate.Count;
    //        for (int i = 1; i <= 8; i++)
    //        {
    //            Obj["other_product_image_locator_" + i][0]["media_location"] = count > i ? item.Affiliate[i - 1] : item.Main;
    //        }
    //    }
    //    //main_offer_image_locator
    //    if (Obj.GetValue("main_offer_image_locator") != null)
    //    {
    //        Obj["main_offer_image_locator"][0]["media_location"] = item.Main;
    //    }
    //    //other_offer_image_locator_
    //    if (Obj.GetValue("other_offer_image_locator_1") != null)
    //    {
    //        int count = item.Affiliate.Count;
    //        for (int i = 1; i <= 5; i++)
    //        {
    //            Obj["other_offer_image_locator_" + i][0]["media_location"] = count > i ? item.Affiliate[i - 1] : item.Main;
    //        }
    //    }
    //    //'swatch_product_image_locator',           
    //    if (Obj.GetValue("swatch_product_image_locator") != null)
    //    {
    //        Obj["swatch_product_image_locator"][0]["media_location"] = item.Main;
    //    }
    //    //merchant_suggested_asin
    //    if (Obj.GetValue("merchant_suggested_asin") != null)
    //    {
    //        // Obj["merchant_suggested_asin"][0]["value"] = "          ";// 10个空格填充
    //        JObject channel = (JObject)Obj["merchant_suggested_asin"][0];
    //        channel.Remove();
    //    }

    //    //'parentage_level',
    //    //'child_parent_sku_relationship',           
    //    if (Obj.GetValue("parentage_level") != null)
    //    {
    //        Obj["parentage_level"][0]["value"] = isParent ? "parent" : "child";
    //        if (!isParent)
    //        {
    //            Obj["child_parent_sku_relationship"][0]["parent_sku"] = ParentSku;
    //        }
    //        else
    //        {
    //            JObject channel = (JObject)Obj["child_parent_sku_relationship"][0];
    //            channel.Property("parent_sku").Remove();
    //        }
    //    }
    //    //'color',            
    //    if (item._Attribute != null && item._Attribute.ContainsKey("color"))
    //    {
    //        Obj.TryGetValue("color", out var color);
    //        if (color != null && Obj.GetValue("color") != null)
    //        {
    //            Obj["color"][0]["value"] = item._Attribute["color"];
    //        }
    //    }
    //    //'size',
    //    if (item._Attribute != null && item._Attribute.ContainsKey("size"))
    //    {
    //        Obj.TryGetValue("size", out var size);
    //        if (size != null && Obj.GetValue("size") != null)
    //        {
    //            Obj["size"][0]["value"] = item._Attribute["size"];
    //        }
    //    }

    //    if (Obj.GetValue("color") != null)
    //    {
    //        Obj.TryGetValue("color", out var color);
    //        try
    //        {                    
    //            if(Obj["color"][0]["standardized_values"]!=null)
    //            {
    //                var s = (string)Obj["color"][0]["standardized_values"][0];
    //                if (string.IsNullOrWhiteSpace(s))
    //                {
    //                    Obj["color"][0]["standardized_values"][0] = "Beige";
    //                }
    //            }

    //        }
    //        catch(Exception e)
    //        {
    //            //啥也不做，继续走
    //        }               
    //    }

    //    CheckValue(Obj, "merchant_release_date", date);
    //    CheckValue(Obj, "product_site_launch_date", date);
    //    CheckValue(Obj, "street_date", date);
    //    CheckValue(Obj, "water_resistance_level", "not_water_resistant");
    //    CheckValue(Obj, "country_of_origin", "CN");
    //    CheckValue(Obj, "manufacturer", storeName);
    //    CheckValue(Obj, "supplier_declared_dg_hz_regulation", "other");
    //    CheckValue(Obj, "condition_type", "new_new");

    //    if (Obj.GetValue("ghs") != null)
    //    {
    //        var s = (string)Obj["ghs"][0]["class"];
    //        if (string.IsNullOrWhiteSpace(s))
    //        {
    //            Obj["ghs"][0]["classification"][0]["class"] = "amzn_specific_no_label_with_warning";
    //        }               
    //    }

    //    //'fulfillment_availability',           
    //    if (Obj.GetValue("fulfillment_availability") != null)
    //    {
    //        Obj["fulfillment_availability"][0]["quantity"] = item.Quantity;

    //        Obj["fulfillment_availability"][0]["fulfillment_channel_code"] = "DEFAULT";

    //        JObject channel = (JObject)Obj["fulfillment_availability"][0];
    //        channel.Property("is_inventory_available").Remove(); 

    //        var s = (string)Obj["fulfillment_availability"][0]["restock_date"];
    //        if (string.IsNullOrWhiteSpace(s))
    //        {
    //            Obj["fulfillment_availability"][0]["restock_date"] = date;
    //        }

    //        if (Obj["fulfillment_availability"][0]["lead_time_to_ship_max_days"] != null)
    //        {
    //            Obj["fulfillment_availability"][0]["lead_time_to_ship_max_days"] = 3;
    //        }
    //    }

    //    if (Obj.GetValue("max_order_quantity") != null)
    //    {
    //        Obj["max_order_quantity"][0]["value"] = 1;
    //    }
    //    return Obj;
    //}

    //public void CheckValue(Newtonsoft.Json.Linq.JObject Obj,string fieldName,string value)
    //{
    //    if (Obj.GetValue(fieldName) != null)
    //    {
    //        var s = (string)Obj[fieldName][0]["value"];
    //        if (string.IsNullOrWhiteSpace(s))
    //        {
    //            Obj[fieldName][0]["value"] = value;
    //        }
    //    }
    //}

    //public Newtonsoft.Json.Linq.JObject UpdateData(Newtonsoft.Json.Linq.JObject Obj, UploadProductVm item, bool IsParent, SkuUploadInfoVm skuUpload)
    //{
    //    //'parentage_level',
    //    if (Obj.GetValue("parentage_level") != null)
    //    {
    //        if (IsParent)  //主产品不需要parent_sku字段
    //        {
    //            JObject channel = (JObject)Obj["child_parent_sku_relationship"][0];
    //            channel.Property("parent_sku").Remove();
    //        }               
    //    }
    //    //不管产品有没有上传成功，sku都不能修改；ean/upc,asin 根据上传情况而定
    //    if (skuUpload.UpdateState==ProductUploadEnum.NotUploaded)
    //    {
    //        try
    //        {
    //            if (Obj.GetValue("merchant_suggested_asin") != null)
    //            {
    //                var channel = Obj["merchant_suggested_asin"][0];
    //                channel.Remove();
    //            }                      
    //        }
    //        catch(Exception e)
    //        {

    //        }

    //    }
    //    else
    //    {
    //        if(string.IsNullOrWhiteSpace(skuUpload.Asin))
    //        {
    //            throw new Exception("上传过的产品，asin还没有取到");
    //        }
    //        Obj["merchant_suggested_asin"][0]["value"] = skuUpload.Asin;

    //        if (Obj.GetValue("externally_assigned_product_identifier") != null)
    //        {
    //            Obj["externally_assigned_product_identifier"][0]["value"] = skuUpload.Code;
    //        }
    //    }

    //    if (Obj.GetValue("fulfillment_availability") != null)
    //    {
    //        var s = (string)Obj["fulfillment_availability"][0]["fulfillment_channel_code"];
    //        if (s.ToUpper().Equals("DEFAULT"))
    //        {
    //            JObject channel = (JObject)Obj["fulfillment_availability"][0];
    //            channel.Property("is_inventory_available").Remove();
    //        }

    //        if (Obj["fulfillment_availability"][0]["lead_time_to_ship_max_days"] != null)
    //        {
    //            var sdays = (string)Obj["fulfillment_availability"][0]["lead_time_to_ship_max_days"];
    //            if (string.IsNullOrWhiteSpace(sdays))
    //            {
    //                Obj["fulfillment_availability"][0]["lead_time_to_ship_max_days"] = 3;
    //            }
    //        }
    //    }

    //    if (Obj.GetValue("max_order_quantity") != null)
    //    {               
    //        var sorders = (string)Obj["max_order_quantity"][0]["value"];
    //        if (string.IsNullOrWhiteSpace(sorders))
    //        {
    //            Obj["max_order_quantity"][0]["value"] = 1;
    //        }
    //    }

    //    //'merchant_shipping_group', //default
    //    if (Obj.GetValue("merchant_shipping_group") != null)
    //    {
    //        var s = (string)Obj["merchant_shipping_group"][0]["value"];
    //        if (string.IsNullOrWhiteSpace(s))
    //        {
    //            Obj["merchant_shipping_group"][0]["value"] = "DEFAULT";
    //        }
    //    }
    //    return Obj;
    //}

    //public string GetErrorMessage(IList<ValidationError> errorMessages)
    //{
    //    StringBuilder str = new StringBuilder();
    //    foreach (var errorMessage in errorMessages)
    //    {
    //        str.Append($"{errorMessage.Path}:{errorMessage.Message}");
    //        foreach (var error in errorMessage.ChildErrors)
    //        {
    //            str.AppendLine(error.Path + ":" + error.Message + error.Schema.ToString() + "\n");
    //        }
    //    }
    //    return str.ToString();
    //}

    //public async Task<FeedParams?> SaveUpload(bool flag, StoreRegion? store, string productType, string marketPlace, string schemaJson, productUploadView view,Dictionary<int,UploadProductVm> uploadProducts,string unit,string Amazonlang)
    //{
    //    Dictionary<int, List<ListParam>> datas = view.datas;
    //    ProductParam param = view.param;
    //    FeedParams feeParam = new FeedParams();
    //    var jsChema = JSchema.Parse(schemaJson);
    //    List<Model> models = new List<Model>();
    //    List<ModelJson> alljsonList = new();
    //    List<ModelLog> logs = new();
    //    using (var transaction = await _dbContext.Database.BeginTransactionAsync())
    //    {
    //        try
    //        {
    //            bool f = true;
    //            foreach (var data in datas)
    //            {
    //                bool flagResult = true;
    //                var feedParamLists = new List<FeedParam>();
    //                uploadResult resultJson = uploadResult.None;
    //                List<ModelJson> jsonList = new();
    //                FeedInfo feed = new FeedInfo();
    //                ProductModel productModel = new ProductModel();
    //                int NewPid = 0;
    //                if (flag)  //新增
    //                {
    //                    //1.克隆
    //                    var pinfo = await productService.UploadClone(data.Key, param.lang, uploadProducts[data.Key]);
    //                    productModel = pinfo.Product;
    //                    NewPid = pinfo.NewProductId;
    //                }
    //                else
    //                {
    //                    //修改
    //                    productModel = await productService.GetInfoById(data.Key);
    //                    NewPid = productModel.ID;
    //                }
    //                var jsonnew = data.Value[0].Data;                        
    //                //2.校验
    //                foreach (var d in data.Value)
    //                {
    //                    var obj = d.Data;
    //                    uploadResult result = uploadResult.None;
    //                    string message = "";
    //                    string sku = d.Sku;
    //                    string level = (string)obj["parentage_level"][0]["value"];
    //                    bool isMain = level == "parent" ? true : false;
    //                    var valid = true;// obj.IsValid(jsChema, out IList<ValidationError> errorMessages);
    //                    if (!valid)
    //                    {
    //                        flagResult = false;
    //                        result = uploadResult.VerityJsonFail;
    //                        message = "";// GetErrorMessage(errorMessages);
    //                    }
    //                    else
    //                    {
    //                        FeedParam feedParam = new FeedParam(sku, data.ToString(), productType);
    //                        feedParamLists.Add(feedParam);
    //                        result = uploadResult.VerityJsonSuccess;
    //                    }
    //                    ModelJson json = new ModelJson(obj.ToString(), result, message, isMain, NewPid, sku);
    //                    jsonList.Add(json);
    //                }
    //                alljsonList.AddRange(jsonList);
    //                if (!flagResult)
    //                {
    //                    f = false;
    //                    resultJson = uploadResult.VerityJsonFail;
    //                }
    //                else
    //                {
    //                    resultJson = uploadResult.VerityJsonSuccess;
    //                    feed.feedParams = feedParamLists;
    //                    feed.Pid = data.Key;
    //                    feeParam.feedInfos.Add(feed);
    //                    feeParam.IssueLocale = Amazonlang;
    //                }

    //                Model productUpload = new Model(productModel, store, resultJson, param.PathNodes, param.PathName, Platforms.AMAZONSP, marketPlace, param.lang, productType, unit, _userId, _groupId, _companyId, _oemId);
    //                models.Add(productUpload);

    //                ModelLog productUploadLog = new ModelLog(NewPid, "刊登产品信息", _session.GetUserName(), _userId, _groupId, _companyId, _oemId);
    //                logs.Add(productUploadLog);
    //                await Clear(data.Key);
    //                await AddAll(productUpload, productUploadLog, jsonList);
    //            }
    //            await _dbContext.SaveChangesAsync();
    //            await transaction.CommitAsync();
    //            return feeParam;
    //        }
    //        catch (Exception e)
    //        {
    //            await transaction.RollbackAsync();
    //            _logger.LogError($"amazon SaveUpload fail,{e.Message}");
    //            _logger.LogError($"amazon SaveUpload:{e.StackTrace}");
    //        }
    //    }
    //    return null;
    //}

    //public  FeedParams ConstructParams(List<int> Pids,string LanguageSign)
    //{
    //    FeedParams feeParam = new FeedParams();
    //    List<FeedInfo> feedInfos = new();
    //    foreach (var pid in Pids)
    //    {
    //        List<FeedParam> feedParams = new();
    //        var upload = GetByPid(pid);
    //        FeedInfo feed = new FeedInfo();
    //        if (upload != null)
    //        {
    //            var datas = GetJsonByPid(pid);
    //            if (datas != null && datas.Count > 0)
    //            {
    //                foreach (var data in datas)
    //                {
    //                    feedParams.Add(new FeedParam(data.Sku, upload.ProductType, data.ProductJson));
    //                }
    //                feed.Pid = pid;
    //                feed.feedParams = feedParams;
    //                feedInfos.Add(feed);
    //            }                   
    //        }                
    //    }
    //    feeParam.feedInfos = feedInfos;
    //    feeParam.IssueLocale = LanguageSign;
    //    return feeParam;
    //}

    //public async Task<bool> UpdateUploadJson(ModelJson model,string action= "修改刊登产品信息")
    //{
    //    using (var transaction = await _dbContext.Database.BeginTransactionAsync())
    //    {
    //        try
    //        {
    //            _dbContext.productUploadJson.Update(model);
    //            ModelLog productUploadLog = new ModelLog(model.ProductID, action, _session.GetUserName(), _userId, _groupId, _companyId, _oemId);
    //            _dbContext.productUploadLog.Add(productUploadLog);
    //            await _dbContext.SaveChangesAsync();
    //            await transaction.CommitAsync();
    //            return true;
    //        }
    //        catch (Exception e)
    //        {
    //            await transaction.RollbackAsync();
    //            _logger.LogError($"amazon UpdateUploadJson fail,{e.Message}");
    //            _logger.LogError($"amazon UpdateUploadJson:{e.StackTrace}");
    //            return false;
    //        }
    //    }
    //}

    //public bool? CheckJsonResult(int pid)
    //{
    //   var result= _dbContext.productUploadJson.Where(x => x.product.ID == pid).ToList();
    //    if(result.Count > 0)
    //    {
    //       var r= result.Any(x => x.Result == uploadResult.VerityJsonFail || x.Result == uploadResult.UploadFail || x.Result == uploadResult.UploadError);
    //        return r;
    //    }
    //    return null;
    //}

    //public async Task<bool> UpdateUploadJson(int pid)
    //{
    //    using (var transaction = await _dbContext.Database.BeginTransactionAsync())
    //    {
    //        try
    //        {
    //           var result= _dbContext.productUpload.FirstOrDefault(x=>x.Product.ID==pid);
    //            if(result!=null)
    //            {
    //                result.Result = uploadResult.VerityJsonSuccess;
    //                _dbContext.productUpload.Update(result);
    //                await _dbContext.SaveChangesAsync();
    //                await transaction.CommitAsync();
    //                return true;
    //            }
    //            else
    //            {
    //                return false;
    //            }
    //        }
    //        catch (Exception e)
    //        {
    //            await transaction.RollbackAsync();
    //            _logger.LogError($"amazon UpdateUploadJson fail,{e.Message}");
    //            _logger.LogError($"amazon UpdateUploadJson:{e.StackTrace}");
    //            return false;
    //        }
    //    }
    //}

    /// <summary>
    /// 添加
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public async Task<Result> Add(ProductUploadController.ProductUploadAddRequest input)
    {
        var stores = GetStores();
        if (stores.All(s => s.ID != input.StoreId))
            return Result.Fail("店铺不存在");

        //查询sync_product表是否有相同数据
        var skuSuffix = input.ProductConfig.SkuSuffix
            ? SyncProductModel.SkuSuffixs.TRUE
            : SyncProductModel.SkuSuffixs.FALSE;
        var titleSuffix = input.ProductConfig.TitleSuffix
            ? SyncProductModel.TitleSuffixs.TRUE
            : SyncProductModel.TitleSuffixs.FALSE;
        var syncProduct = await _syncProductService.CheckData(input.Spid);
        ProductModel product;
        if (syncProduct is null) //不存在->添加
        {
            var cloneView = await _productService.ImportClone(input.ProductId, input.Language);
            syncProduct =
                await _syncProductService.InsertSyncProduct(cloneView, input.ProductConfig, input.TypeData,
                    input.TypeDataId);
            product = cloneView.Product;
        }
        else
        {
            syncProduct.TypeData = JsonConvert.SerializeObject(input.TypeData);
            syncProduct.TypeDataId = JsonConvert.SerializeObject(input.TypeDataId);
            syncProduct.SkuSuffix = skuSuffix;
            syncProduct.SkuType = input.SkuType;
            syncProduct.TitleSuffix = titleSuffix;
            product = (await _productService.GetInfoById(syncProduct.Pid))!;
        }

        var productUpload = await InsertProductUpload(input, syncProduct, product!);
        // 多语言导出并且存在关联id时, ean只需在第一次复制产品时分配
        if (input.ProductConfig.AutoAllotProduceCode)
        {
            await _productService.DistributeEanUpc(new() { { product, (int)productUpload.Languages } },
                Enum.Parse<AllotExportProductDto.ModeEnum>(input.ProductConfig.ProduceCodeType, true),
                false);
            syncProduct.EanupcStatus = SyncProductModel.EanupcStatuss.ALL;
            await _dbContext.SaveChangesAsync();
        }

        return Result.Ok();
    }

    //
    private async Task<ProductUpload> InsertProductUpload(ProductUploadController.ProductUploadAddRequest input,
        SyncProductModel syncProduct,
        ProductModel product)
    {
        input.Ext["feed_product_type"] = !string.IsNullOrWhiteSpace(input.Ext["feed_product_type"]?.ToString())
            ? input.Ext["feed_product_type"]
            : "home";
        input.Ext["EnableApparelSize"] = false;
        input.Ext["MaxOrderQuantity"] = "10";
        var model = new ProductUpload()
        {
            StoreId = input.StoreId,
            SyncUploadTaskId = input.SyncUploadTaskId,
            Spid = syncProduct.ID,
            ProductId = syncProduct.Pid,
            Languages = input.Language,
            ProductName = product.Title,
            MarketPlace = input.Marketplace,
            Platform = input.Platform,
            CompanyID = syncProduct.CompanyID,
            OEMID = syncProduct.OEMID,
            GroupID = syncProduct.GroupID,
            UserID = syncProduct.UserID,
            Result = UploadResult.None,
            PathNodes = JsonConvert.SerializeObject(input.TypeDataId),
            PathName = JsonConvert.SerializeObject(input.TypeData),
            ProductType = input.ProductType,
            // CountryOfOrigin = input.CountryOfOrigin,
            Unit = !string.IsNullOrWhiteSpace(input.Unit) ? input.Unit : _session.GetUnitConfig(),
            ChangePriceId = input.ChangePriceId,
            Params = JsonConvert.SerializeObject(input.Params),
            Ext = JsonConvert.SerializeObject(input.Ext),
        };
        _dbContext.Add(model);
        await _dbContext.SyncUploadTasks.Where(t => t.ID == input.SyncUploadTaskId)
            .UpdateAsync(t => new { Amount = t.Amount + 1 });
        await _dbContext.SaveChangesAsync();
        return model;
    }


    public async Task<Model?> GetModelAsync(int id)
    {
        return await _dbContext.productUpload.FirstOrDefaultAsync(x => x.ID == id);
    }

    //获取店铺
    public StoreRegion[] GetStores()
    {
        return _storeCache.List()?.Where(s => s.CompanyID == ISessionExtension.GetCompanyID(_session)).ToArray() ??
               Enumerable.Empty<StoreRegion>().ToArray();
    }

    /// <summary>
    /// 添加上传任务
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public async Task<Result<int>> AddTask(ProductUploadController.ProductUploadAddTaskRequest input)
    {
        try
        {
            var stores = GetStores();
            if (stores.All(s => s.ID != input.StoreId))
                return Result.Fail("店铺不存在");
            var platformData = input.Platform switch
            {
                Platforms.AMAZONSP => new PlatformExtData()
                    { BrowserNodeNames = JsonConvert.SerializeObject(input.TypeData), MarketPlace = input.Marketplace },
                _ => new PlatformExtData(),
            };
            var model = new SyncUploadTask()
            {
                Name = $"{input.Platform}_{DateTime.Now.ToString("yyyyMMddHHmmss")}",
                StoreId = input.StoreId,
                Language = input.Language,
                State = UploadResult.None,
                Platform = input.Platform,
                PlatformData = JsonConvert.SerializeObject(platformData),
                UserID = _session.GetUserID(),
                GroupID = _session.GetGroupID(),
                CompanyID = _session.GetCompanyID(),
                OEMID = _session.GetOEMID(),
            };
            _dbContext.SyncUploadTasks.Add(model);
            await _dbContext.SaveChangesAsync();
            return Result.Ok(model.ID);
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    public async Task<Result> EditTask(int id, ProductUploadController.ProductUploadEditTaskRequest input)
    {
        var model = await _dbContext.SyncUploadTasks.FindAsync(id);
        if (model is null)
            return Result.Fail("任务不存在");

        model.Name = input.Name;
        await _dbContext.SaveChangesAsync();
        return Result.Ok();
    }

    public async Task<Result> DeleteTask(int id)
    {
        var model = await _dbContext.SyncUploadTasks.FindAsync(id);
        if (model is null)
            return Result.Ok();

        var isUsed = await _dbContext.productUpload.Where(p => p.SyncUploadTaskId == model.ID).AnyAsync();
        if (isUsed)
            return Result.Fail("任务下存在产品, 无法删除");

        _dbContext.Remove(model);
        await _dbContext.SaveChangesAsync();
        return Result.Ok();
    }
}