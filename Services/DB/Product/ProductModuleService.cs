using System.Security.Claims;
using ERP.Authorization.Rule.Component;
using ERP.Data;
using ERP.Enums.Identity;
using ERP.Extensions;
using ERP.Interface;
using ERP.Interface.Rule;
using ERP.Services.Identity;

namespace ERP.Services.Product;

public class ProductModuleService : BaseWithUserService
{
    protected readonly UserManager _userManager;
    public readonly IManager _config;
    public ProductModuleService(DBContext dbContext, ISessionProvider sessionProvider, UserManager userManager, IManager config) : base(dbContext, sessionProvider)
    {
        _userManager = userManager;
        _config = config;
    }


    // protected async Task<Select?> GetRange(int menuId)
    // {
    //     var user = await _userManager.FindByIdAsync(_userId);
    //     var userClaims = await _userManager.GetClaimsAsync(user);
    //     Func<IConfigVisitor, long> val = userClaims.Has;
    //     Func<IHas, bool> has = new ClaimsPrincipal().Has;
    //     var configItems = _config.Get(menuId);
    //     if (configItems is null)
    //     {
    //         return null;
    //     }
    //     var data = configItems.RenderConfigItems(has, val);
    //     IComponentView? item = data.FirstOrDefault(m => m is Select);
    //     return (Select?)item;
    // }
}