using ERP.Data;
using ERP.Data.Products;
using ERP.DomainEvents.Product;
using ERP.Enums;
using ERP.Enums.Identity;
using ERP.Enums.Product;
using ERP.Exceptions.Product;
using ERP.Exceptions.Upload;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.DB.Product;
using ERP.Models.Product;
using ERP.Models.View.Product.Translation;
using ERP.Models.View.Products.Product;
using ERP.Services.Caches;
using ERP.Services.DB.Statistics;
using ERP.Services.Images;
using ERP.Services.Upload;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Z.EntityFramework.Plus;
using ProductDBService = ERP.Services.DB.Product.Product;
using ERP.Services.Stores;
using ERP.Services.DB.Users;
using ERP.ViewModels;
using ERP.Models.View.Products.Export;
using MediatR;
using AsinProduct = ERP.Services.DB.Stores.AsinProduct;
using ModelProduct = ERP.Data.Products.Product;
using ImagesStruct = ERP.Data.Products.Images;
using System.Reflection;
using System.Text.RegularExpressions;
using Common.Enums.Products;
using Common.Enums.Products.ImageTran;
using Common.Enums.ResourcePackage;
using ERP.Attributes;
using ERP.Data.Products.Feature;
using ERP.Enums.Rule.Config;
using ERP.Enums.Rule.Config.Internal;
using ERP.Models.DB.Stores;
using ERP.Services.DB.CompanyResource;
using Image = ERP.Data.Products.Image;
using ProductHostService = ERP.Services.DB.ProductType.ProductHost;
using ERP.Models.Stores;
using ERP.Services.DB.B2C;
using Enumtxtention = ERP.Extensions.EnumExtension;

namespace ERP.Services.Product;

public class ProductService : BaseWithUserService
{
    private readonly FeatureEnum _b2cOriginalPidKey = FeatureEnum.B2cOriginalPid;
    private readonly UploadService _uploadService;
    private readonly TempImagesService _tempImagesService;
    private readonly ImagesResourceService _imagesResourceService;
    private readonly ConvertProduct _convertProduct;

    private readonly TaskService _taskService;
    private readonly CategoryService _categoryService;

    private readonly StoreService _storeService;
    private readonly CompanyService _companyService;

    private readonly AsinProduct _asinProduct;
    private readonly LanguagesCache _languagesCache;
    private readonly PlatformCache _platformCache;
    private readonly IOemProvider _oemProvider;
    private readonly Statistic _statisticService;
    private readonly EanupcService _eanupcService;
    private readonly IMediator _mediator;

    private readonly ProductHostService _ProductHostService;
    private readonly PackagesResourceService _packagesResourceService;
    private readonly TranslationService _translationService;
    private readonly ProductValidityService _productValidityService;
    private readonly DomainService _domainService;
    private readonly ILogger<ProductService> _logger;


    public ProductService(IHttpContextAccessor httpContextAccessor, DBContext dbContext,
        ISessionProvider sessionProvider, UploadService uploadService, TempImagesService tempImagesService,
        ConvertProduct convertProduct, StoreService storeService,
        AsinProduct asinProduct, LanguagesCache languagesCache, CompanyService companyService,
        IOemProvider oemProvider, Statistic statisticService, ILogger<ProductService> _logger,
        PlatformCache platformCache, TaskService taskService, CategoryService categoryService,
        IMediator mediator, ImagesResourceService imagesResourceService, ProductHostService productHostService,
        PackagesResourceService packagesResourceService, TranslationService translationService,
        EanupcService eanupcService, ProductValidityService productValidityService, DomainService domainService) : base(dbContext,
        sessionProvider)
    {
        _uploadService = uploadService;
        _tempImagesService = tempImagesService;
        _convertProduct = convertProduct;
        _storeService = storeService;
        _asinProduct = asinProduct;
        _languagesCache = languagesCache;
        _companyService = companyService;
        _oemProvider = oemProvider;
        _statisticService = statisticService;
        this._logger = _logger;
        _platformCache = platformCache;
        _taskService = taskService;
        _categoryService = categoryService;
        _mediator = mediator;
        _imagesResourceService = imagesResourceService;
        _ProductHostService = productHostService;
        _packagesResourceService = packagesResourceService;
        _translationService = translationService;
        _eanupcService = eanupcService;
        _productValidityService = productValidityService;
        _domainService = domainService;
    }

    /// <summary>
    /// 模块初始化
    /// </summary>
    /// <returns></returns>
    public async Task<object> Init()
    {
        var task = await _taskService.GetSelectOptions(await GetUserRange(ProductAbout.TaskRange));
        var category = await _categoryService.FirstCategory(await GetUserRange(ProductAbout.CategoryRange), true);

        var productValidity = await _productValidityService.GetUserProductDelayed(_userId);

        return new
        {
            audit = new List<object>()
            {
                new { label = "未审核", value = AuditStateEnum.NotReviewed },
                new { label = "已审核", value = AuditStateEnum.Audited },
                new { label = "未通过", value = AuditStateEnum.DidNotPass },
            },
            category = category,
            companyUsers = await _dbContext.User.Where(m => m.CompanyID == _companyId)
                .Select(m => new { m.ID, m.TrueName }).ToDictionaryAsync(m => m.ID, m => m),
            concierge = _session.GetRole() == Role.ADMIN,
            isDistribution = _session.GetIsDistribution(),
            platform = _platformCache.List().Where(m => m.Collect).ToDictionary(m => m.ID, m => m),
            task = task.Select(m => new { Id = m.ID, Name = m.Name }).ToList(),
            languages = _languagesCache.List()!.ToDictionary(m => m.Code, m => m),
            SourceLanguage = Enum.GetValues<SourceLanguageEnum>().Cast<SourceLanguageEnum>().ToDictionary(x => (int)x,x =>x.GetDescriptionByKey("Name")),
            TargetLanguage = Enum.GetValues<TargetLanguageEnum>().Cast<TargetLanguageEnum>().ToDictionary(x => (int)x,x =>x.GetDescriptionByKey("Name")),
            IsUseTranslation = await _packagesResourceService.CheckIsHaveAvailableResource(PackageTypeEnum.Translation),
            IsUseImgTran = await _packagesResourceService.CheckIsHaveAvailableResource(PackageTypeEnum.ImgTranslation),
            ProductTrueExpirationDate = productValidity.trueDt,
            ProductValidity = productValidity.showDt,
            B2cDomain = await _domainService.GetDomainList()
        };
    }


    /// <summary>
    /// 单品详情
    /// </summary>
    /// <param name="req"></param>
    /// <param name="range"></param>
    /// <param name="isB2C"></param>
    /// <returns></returns>
    /// <exception cref="NotFoundException"></exception>
    public async Task<EditProductItemDto> Show(ShowDto req, DataRange_2b range, bool isB2C = false)
    {
        ProductModel? productModel = null;
        if (isB2C)
        {
            productModel = await GetInfoById(req.Id);
        }
        else
        {
            productModel = await GetInfoById(req.Id, range);
        }

        if (productModel is null)
        {
            throw new NotFoundException("找不到产品相关数据#1");
        }

        ProductItemModel? itemModel = null;

        if (req.ItemId.HasValue)
        {
            //获取item
            itemModel = await GetItemInfoById(Convert.ToInt32(req.ItemId), productModel.ID);
        }
        else
        {
            //获取语言 CurrentLanguage
            var currentLanguage = "default";
            var languageIndex = productModel.GetUILanguage();
            var index = (int)languageIndex - 1;
            if (index != -1)
            {
                currentLanguage = ProductModel.LanguageCode[index];
            }

            var langEnum = Enum.Parse<Languages>(currentLanguage, true);

            var langId = langEnum == Languages.Default ? 0 : (int)Math.Log2((long)langEnum) + 1;


            //获取item
            itemModel = await GetItemInfoByLangId(productModel.ID, langId);
        }


        if (itemModel is null)
        {
            throw new NotFoundException("找不到产品相关数据#2");
        }

        //获取ListItem

        var listItem = await _dbContext.ProductItem.Where(m => m.Pid == productModel.ID).ToListAsync();

        return await GetProductInfoToUi(productModel, itemModel, listItem);
    }

    public Task<ReturnStruct> Info(int id) => throw new NotImplementedException();


    /// <summary>
    /// 删除产品调用此方法需要开启事务
    /// </summary>
    /// <param name="id"></param>
    /// <param name="isDecCount"></param>
    /// <exception cref="NotFoundException"></exception>
    public async Task DestroyNoTransaction(int id, bool isDecCount)
    {
        var product = await GetInfoById(id);

        if (product is null)
        {
            //因删除脚本清理商品时并没有删除导出模板导致产品无法清楚 所以停止抛异常，商品不存在则直接返回
            return;
           /* throw new NotFoundException("找不到相关数据#1");*/
        }

        var itemList = await GetItemInfoList(product.ID);

        var deleteItemIds = await DeleteProductItemStorage(itemList, product);

        var imageIds = await _tempImagesService.Transfer(product.ID);

        //删除正式资源数据
        await _imagesResourceService.Delete(imageIds);

        //删除产品数据
        await DeleteProductById(product.ID);

        await DeleteProductItemByIdAndPid(product.ID, deleteItemIds);

        // if (isDecCount)
        // {
        //     #region 删除产品数
        //
        //     var productDeleteEvent = new ProductDeleteEvent(product);
        //     await _mediator.Publish(productDeleteEvent);
        //
        //     #endregion
        // }
    }

    /// <summary>
    /// 删除产品
    /// </summary>
    /// <param name="req"></param>
    /// <param name="dataRange2B"></param>
    /// <returns></returns>
    /// <exception cref="NotFoundException"></exception>
    public new async Task<bool> Destroy(DestroyDto req, DataRange_2b range)
    {
        var product = await GetInfoById(req.ID, range);

        if (product is null)
        {
            throw new NotFoundException("找不到相关数据#1");
        }

        var itemList = await GetItemInfoList(product.ID);

        var transaction = await _dbContext.Database.BeginTransactionAsync();

        try
        {
            await DestroyNoTransaction(req.ID, true);

            await transaction.CommitAsync();

            #region 删除产品数

            var productDeleteEvent = new ProductDeleteEvent(product);
            await _mediator.Publish(productDeleteEvent);

            #endregion
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            _logger.LogError(e, e.Message);
        }

        return true;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="isNeedCate"></param>
    /// <param name="isShare"></param>
    /// <returns></returns>
    protected internal IEnumerable<ProductModel>? ProductDataWatchRange(bool isNeedCate = true, bool isShare = false)
    {
        // if (range == DataRange_2b.None)
        //     return null;

        var model = _dbContext.Product.Where(m => m.OEMID == _oemId && m.CompanyID == _companyId);

        // if (range == DataRange_2b.Group)
        // {
        //     model = model.Where(m => m.GroupID == _groupId);
        // }
        //
        // if (range == DataRange_2b.User)
        // {
        //     model = model.Where(m => m.UserID == (_userId));
        // }

        if (isNeedCate)
        {
            model = (IQueryable<ProductModel>)model
                .Include(m => m.CategoryModel)
                .Include(m => m.SubCategoryModel)
                .AsEnumerable();
        }
        else
        {
            model = (IQueryable<ProductModel>)model.AsEnumerable();
        }

        if (isShare)
        {
            model = model.Where(m =>
                m.OEMID == _oemId
                && m.CompanyID == _companyId);
        }
        else
        {
            model = model.Where(m =>
                m.OEMID == _oemId
                && m.CompanyID == _companyId
                && m.UserID == _userId);
        }

        return model;
    }

    public IEnumerable<ProductModel> ProductModelQuery(ListDto req)
    {
        IEnumerable<ProductModel> model;

        model = ProductDataWatchRange(req.Action != ActionEnum.Share);


        //排除上传中/导出/B2C状态的数据
        model = model.Where(m => (m.Type & (long)Types.OnGoing) != (long)Types.OnGoing);
        model = model.Where(m => (m.Type & (long)Types.Export) != (long)Types.Export);
        model = model.Where(m => (m.Type & (long)Types.B2C) != (long)Types.B2C);
        model = model.Where(m => (m.Type & (long)Types.Uplaod) != (long)Types.Uplaod);


        //audit
        var audited = ProductModel.AUDIT_AUDITED;
        var typeAudit = Types.Audit;
        var auditMask = Types.MASK_AUDIT;

        switch (req.AuditState)
        {
            case AuditStateEnum.NotReviewed:
                model = model.Where(m => m.Audit < audited);
                break;
            case AuditStateEnum.Audited:
                model = model.Where(m => m.Audit >= audited);
                break;
            case AuditStateEnum.DidNotPass:
                model = model.Where(m => m.Audit > audited);
                break;
        }

        //tabs
        //非回收站时应排除回收站
        if (req.Action != ActionEnum.Recycle)
            model = model.Where(m => (m.Type & (long)Types.Recycle) != (long)Types.Recycle);

        switch (req.Action)
        {
            case ActionEnum.Collect:
                model = model.Where(m => (m.Type & (long)Types.Collect) == (long)Types.Collect);
                break;
            case ActionEnum.Manual:
                model = model.Where(m => (m.Type & (long)Types.Collect) != (long)Types.Collect);
                break;
            case ActionEnum.Audit:
                model = model.Where(m => m.Audit < audited);
                break;
            case ActionEnum.Share:
                model = model.Where(m => (m.Type & (long)Types.Share) == (long)Types.Share);
                break;
            case ActionEnum.Recycle:
                model = model.Where(m => (m.Type & (long)Types.Recycle) == (long)Types.Recycle);
                break;
        }

        model = model
            .WhenWhere(!req.Pid.IsNullOrEmpty(), m => m.Pid == req.Pid)
            .WhenWhere(req.UserId.HasValue, m => m.UserID == req.UserId)
            .WhenWhere(req.TaskId.HasValue, m => m.TaskId == req.TaskId)
            .WhenWhere(req.CategoryId.HasValue, m => m.CategoryId == req.CategoryId)
            .WhenWhere(req.SubCategoryId.HasValue, m => m.SubCategoryId == req.SubCategoryId)
            .WhenWhere(req.PlatformId.HasValue, m => m.PlatformId == req.PlatformId)
            .WhenWhere(req.Title.Length > 0 && req.Multi == MultiEnum.Fuzzy, m => m.Title.Contains(req.Title))
            .WhenWhere(req.Title.Length > 0 && req.Multi == MultiEnum.Precise, m => m.Title == req.Title)
            .WhenWhere(req.UpdatedTime != null && req.UpdatedTime.Count == 2,
                a => a.UpdatedAt >= req.UpdatedTime![0] && a.UpdatedAt < req.UpdatedTime[1].LastDayTick());

        if (req.IsRepeat)
        {
            model = model.Where(m => m.UserLevelRepeatNum > 0)
                .OrderByDescending(m => m.UserLevelRepeatNum)
                .ThenByDescending(m => m.UpdatedAt);
        }

        return model;
    }

    /// <summary>
    /// 获取产品重复次数
    /// </summary>
    /// <param name="hashcode"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<int> UserLevelRepeatNum(ulong hashcode, int? id = null)
    {
        return await _dbContext.Product
            .WhenWhere(id.HasValue, m => m.ID != id)
            .Where(m => m.HashCode == hashcode && m.UserID == _userId)
            .CountAsync();
    }

    public async Task SetUserLevelRepeatNum(ulong hashcode)
    {
        var count = await _dbContext.Product
            .Where(m => m.HashCode == hashcode && m.UserID == _userId)
            .Where(m => (m.Type & (long)Types.OnGoing) != (long)Types.OnGoing)
            .CountAsync();

        if (count == 1)
        {
            count = 0;
        }

//, UpdatedAt = DateTime.Now
        await _dbContext.Product.Where(m => m.HashCode == hashcode && m.UserID == _userId)
            .UpdateAsync(m => new { UserLevelRepeatNum = count });
    }

    public async Task<bool> CheckProductTitle(ulong hashcode)
    {
        var count = await _dbContext.Product
            .Where(m => m.HashCode == hashcode && m.UserID == _userId)
            .Where(m => (m.Type & (long)Types.OnGoing) != (long)Types.OnGoing)
            .CountAsync();

        if (count == 1)
        {
            count = 0;
        }

        return count > 1;
    }

    public async Task<ProductModel?> GetInfoByIdWithUser(int id)
    {
        return await _dbContext.Product.Where(m => m.ID == id && m.CompanyID == _companyId && m.OEMID == _oemId)
            .FirstOrDefaultAsync();
    }


    public static async Task<ProductModel?> GetInfoById(DBContext dbContext, int? id)
    {
        return await dbContext.Product.Where(m => m.ID == id).FirstOrDefaultAsync();
    }

    public async Task<ProductModel?> GetInfoById(int? id)
    {
        return await _dbContext.Product.Where(m => m.ID == id).FirstOrDefaultAsync();
    }

    private async Task<ProductModel?> GetInfoById(int? id, DataRange_2b range)
    {
        var model = ProductDataWatchRange(range, true, false);
        if (model == null)
            return null;
        return await model.Where(m => m.ID == id).FirstOrDefaultAsync();
    }

    public async Task<ProductItemModel?> GetItemInfoById(int id, int pid)
    {
        return await _dbContext.ProductItem.Where(m => m.ID == id && m.Pid == pid).FirstOrDefaultAsync();
    }

    public async Task<List<ProductItemModel>> GetItemInfoList(int pid)
    {
        return await _dbContext.ProductItem.Where(m => m.Pid == pid).ToListAsync();
    }

    public async Task<ProductItemModel?> GetItemInfoByLangId(int pid, int langId)
    {
        return await _dbContext.ProductItem.Where(m => m.Pid == pid && m.LanguageId == langId)
            .FirstOrDefaultAsync();
    }


    // protected internal async Task<ProductModel?> GetProductInfoById(int id)
    // {
    // var model = ProductDataWatchRange(false);
    //     // return model.FirstOrDefault(m => m.ID == id);
    // }


    //todo 功能待定
    public async Task<Image> SyncImagesToYun(int id, int pid)
    {
        var product = await GetInfoById(pid);

        if (product == null)
        {
            throw new NotFoundException("找不到相关数据#1");
        }

        var image = await _dbContext.TempImages.Where(m => m.ID == id).FirstOrDefaultAsync();

        if (image == null)
        {
            throw new Exceptions.TempImages.NotFoundException("找不到相关数据#1");
        }

        if (image.Type == ResourceModel.Types.Storage)
        {
            throw new Exceptions.TempImages.NotFoundException("无需同步#2");
        }

        var filePath = await _uploadService.UploadImageByNetwork(image.Path, image.OEMID, image.UserID);

        image.Path = filePath;
        image.Size = await _uploadService.GetSize(filePath);

        if (await _dbContext.SaveChangesAsync() >= 1)
        {
            return new Image(image);
        }

        throw new Exceptions.TempImages.NotFoundException("图片更新失败#3");
    }

    /// <summary>
    /// 获取产品拥有的语种
    /// </summary>
    /// <param name="model"></param>
    /// <param name="isNeedDefault"></param>
    /// <returns></returns>
    public Dictionary<Languages, string> GetTranslated(ProductModel model, bool? isNeedDefault = null)
        => GetTranslated(model.Language, isNeedDefault);

    public Dictionary<Languages, string> GetTranslated(long modelLanguage, bool? isNeedDefault = null)
    {
        var languages = _languagesCache.List();

        var hasLanguage = new Dictionary<Languages, string>();

        foreach (var item in languages)
        {
            if ((modelLanguage & (long)item.Flag) == (long)item.Flag)
            {
                hasLanguage.Add(item.Flag, item.Name);
            }
        }

        if (isNeedDefault.HasValue && isNeedDefault.Value)
        {
            hasLanguage.Add(Languages.Default, "默认");
        }

        return hasLanguage;
    }

    private async Task<ProductItemModel> EditProductItem(ProductModel productModel, ProductItemModel model,
        ModelProduct product,
        EditProductItemDto req)
    {
        long version;
        var historyVersion = model.ID <= 0
            ? new List<long>()
            : JsonConvert.DeserializeObject<List<long>>(model.HistoryVersion)!;
        var createdAt = DateTime.Now;

        if (productModel.IsExport() || productModel.IsUpload())
        {
            version = model.CurrentVersion;
        }
        else
        {
            version = createdAt.GetTimeStampMilliseconds();
            historyVersion.Add(version);
        }


        model.LanguageName = req.Info.CurrentLanguageEnum.GetDescriptionByKey("Code");
        model.LanguageId = req.Info.CurrentLanguageId;
        model.Coin = req.Info.Coin;
        model.Quantity = req.Info.Quantity;
        model.CurrentVersion = version;
        model.HistoryVersion = Newtonsoft.Json.JsonConvert.SerializeObject(historyVersion);

        if (model.ID <= 0)
        {
            model.CreatedAt = createdAt;
        }

        //处理code对照
        var codeCompareDic = productModel.CodeCompareDic;
        if (productModel.CodeCompareDic == null)
        {
            codeCompareDic = new Dictionary<string, string?>();
        }

        foreach (var variant in product.Variants)
        {
            if (codeCompareDic.Keys.Contains(variant.Sid!))
            {
                codeCompareDic[variant.Sid!] = variant.Code;
            }
            else
            {
                codeCompareDic.Add(variant.Sid!, variant.Code);
            }
        }

        model.CodeCompare = codeCompareDic;

        model.UpdatedAt = createdAt;
        return model;
    }

    private async Task<ProductModel> EditProduct(ProductModel model, Data.Products.Product product,
        EditProductItemDto req)
    {
        var createdAt = DateTime.Now;

        if (model.UiLanguage == req.Info.CurrentLanguageEnum)
        {
            model.Title = product.GetTitle();
            model.HashCode = Helpers.CreateHashCode(model.Title);
            model.Prices = JsonConvert.SerializeObject(product.GetPrices());
            model.Quantity = product.GetQuantity();
        }

        model.Coin = req.Info.Coin;
        model.Sku = req.Common.Sku;
        model.Source = product.GetSource();
        model.CategoryId = req.Common.CategoryId;
        model.SubCategoryId = req.Common.SubCategoryId;
        model.IsImageAsync = req.Common.IsImageSync;
        model.Pid = product.GetPid();
        model.PidHashCode = product.GetPidHashCode().IsNotNull() ? product.GetPidHashCode().ToString() : string.Empty;
        model.Images = JsonConvert.SerializeObject(req.Common.Images?.ToImages());
        // model.MainImage = product.MainImage != null ? JsonConvert.SerializeObject(product.MainImage) : null;
        if (model.ID <= 0)
        {
            model.Type = (long)Types.Default;
            model.Language = (long)Types.Default;
            //新增默认为设置为自增产品
            model.SetCollect(false);
            model.CreatedAt = createdAt;
        }
        else
        {
            model.SetEdited(true);
        }

        model.UpdatedAt = createdAt;
        model.CodeCompare = JsonConvert.SerializeObject(req.Common.CodeCompare);

        //设置语言
        if (req.Info.CurrentLanguageEnum != Languages.Default)
        {
            model.SetLanguage(req.Info.CurrentLanguageEnum, true);
        }

        return model;
    }

    public async Task<bool> SaveOrUpdate(EditProductItemDto req, DataRange_2b range)
    {
        //前端产品表结构转为为后端结构
        var structProduct = _convertProduct.ToBeServiceStruct(req);

        if (structProduct is null)
        {
            return false;
        }

        ProductModel? modelParams = null;
        ProductItemModel? itemModelParams = null;
        if (!req.Common.Id.HasValue)
        {
            modelParams = new ProductModel(_sessionProvider.Session!);
        }
        else
        {
            modelParams = await GetInfoById(req.Common.Id, range);
        }

        if (modelParams is null)
        {
            throw new NotFoundException("数据丢失#1");
        }

        if (!req.Info.Id.HasValue)
        {
            itemModelParams = new ProductItemModel(_sessionProvider.Session!);
        }
        else
        {
            itemModelParams = await GetItemInfoById(Convert.ToInt32(req.Info.Id), Convert.ToInt32(req.Common.Id));
        }


        if (itemModelParams is null)
        {
            throw new NotFoundException("数据丢失#2");
        }


        var model = await EditProduct(modelParams, structProduct, req);
        var itemModel = await EditProductItem(model, itemModelParams, structProduct, req);
        //列表主体图选择第一个变体的主体
        var firstVariant = structProduct.Variants.First();
        structProduct.MainImage = structProduct.Images != null && firstVariant.Images.Main.HasValue && firstVariant.Images.Main.Value > 0
            ? structProduct.Images.FirstOrDefault(m => m.ID == firstVariant.Images.Main.Value)
            : null;
        model.MainImage = structProduct.Images != null && firstVariant.Images.Main.HasValue && firstVariant.Images.Main.Value > 0
            ? JsonConvert.SerializeObject(structProduct.Images.First(m => m.ID == firstVariant.Images.Main.Value))
            : null;
        model.CodeCompare = JsonConvert.SerializeObject(itemModel.CodeCompare);

        // structProduct.SetTypesFlag(model.Type);
        var jsonProduct = JsonConvert.SerializeObject(structProduct, new JsonSerializerSettings()
        {
            TypeNameHandling = TypeNameHandling.All
        });


        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            if (!req.Common.Id.HasValue)
            {
                // #region 新增产品数/当前产品数
                //
                // var productCreatedEvent = new ProductCreatedEvent(model);
                // await _mediator.Publish(productCreatedEvent);
                //
                // #endregion

                model.FilesSize += jsonProduct.Length;
                await _dbContext.Product.AddAsync(model);
                await _dbContext.SaveChangesAsync();
            }

            //获取产品json路径
            var jsonFilePath = GetProductJsonPath(itemModel.CurrentVersion, model.ID);

            //临时图片迁移
            var newCreateImagesIds = req.Common.NewCreateImagesIds;
            if (newCreateImagesIds is { Count: > 0 })
            {
                var newCreateImages = await _dbContext.TempImages
                    .Where(m => newCreateImagesIds.Contains(m.ID))
                    .Select(m => new ResourceModel(m, model.ID)).ToListAsync();

                await _dbContext.Resource.AddRangeAsync(newCreateImages);
                await _dbContext.TempImages.Where(m => newCreateImagesIds.Contains(m.ID)).DeleteAsync();

                var newCreateImagesSize = newCreateImages.Sum(m => m.Size);
                var savedImagesSize = await _dbContext.Resource
                    .Where(m => m.ProductId == model.ID)
                    .SumAsync(m => m.Size);
                //计算产品图片大小
                model.ImagesSize = newCreateImagesSize + savedImagesSize;
            }


            //Pid
            itemModel.Pid = model.ID;
            //写入文件路径
            itemModel.FilePath = jsonFilePath;
            itemModel.FilePathHashCode = Helpers.CreateHashCode(jsonFilePath);
            //文件大小
            itemModel.FilesSize += jsonProduct.Length;
            if (itemModel.ID <= 0)
            {
                await _dbContext.ProductItem.AddAsync(itemModel);
            }
            else
            {
                await _dbContext.ProductItem.SingleUpdateAsync(itemModel);
            }


            await _dbContext.SaveChangesAsync();

            //设置重复次数
            await SetUserLevelRepeatNum(model.HashCode);


            var uploadState = await _uploadService.PutAsync(jsonFilePath, jsonProduct);

            if (!uploadState)
            {
                throw new SavedException("数据包存储失败");
            }

            //重置上传
            if (model.IsUpload())
            {
                await UpdateModel(model.ID);
            }

            await transaction.CommitAsync();
            if (!req.Common.Id.HasValue)
            {
                #region 新增产品数/当前产品数

                var productCreatedEvent = new ProductCreatedEvent(model);
                await _mediator.Publish(productCreatedEvent);

                #endregion
            }

            return true;
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            _logger.LogError(e, e.Message);
            throw new SavedException("数据保存失败");
        }
    }


    private string? GetHistoryProductJsonPath()
    {
        return string.Empty;
    }

    public string GetProductJsonPath(long version, int pid)
    {
        //userId作为路径文件名的组成可用于产品编辑日志所属人；
        var t = DateTimeOffset.FromUnixTimeMilliseconds(version);
        return $"{_oemId}/{_companyId}/products/{t.Year:0000}/{t.Month:00}/{t.Day:00}/{pid}/v_{_userId}_{version}.json";

        //userId作为路径会存在问题，A用户修改B用户的产品信息新增一个版本的json文件，在回滚历史版本的时候会导致文件找不到
        // return $"{_oemId}/{t.Year:0000}/{t.Month:00}/{t.Day:00}/{_companyId}/{_userId}/products/{pid}/v_{version}.json";
    }

    public string GetProductJsonPath(long version, ProductModel model)
    {
        var t = DateTimeOffset.FromUnixTimeMilliseconds(version);
        return
            $"{model.OEMID}/{model.CompanyID}/products/{t.Year:0000}/{t.Month:00}/{t.Day:00}/{model.ID}/v_{model.UserID}_{version}.json";
    }


    public async Task<bool> Recovery(int id, DataRange_2b range, bool isInRecovery)
    {
        var info = await GetInfoById(id, range);

        if (info is null)
        {
            throw new NotFoundException("找不到相关数据#1");
        }

        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            info.SetRecycle(isInRecovery);
            await _dbContext.Product.SingleUpdateAsync(info);


            await _dbContext.SaveChangesAsync();
            await transaction.CommitAsync();

            #region 回收站产品数

            var productRecoveryEvent = new ProductRecoveryEvent(info, isInRecovery);
            await _mediator.Publish(productRecoveryEvent);

            #endregion
            var str = isInRecovery ? "移入" : "移出";
            _logger.LogInformation($"【用户】[{_userId}],将产品【{id}】 {str} 回收站");
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            _logger.LogError(e, e.Message);
            throw new SavedException(e.Message);
        }

        return true;
    }

    public async Task<bool> Share(int id, DataRange_2b dataRange2B)
    {
        var info = await GetInfoById(id);

        if (info is null)
        {
            throw new NotFoundException("找不到相关数据#1");
        }

        info.SetShare(true);

        await _dbContext.Product.SingleUpdateAsync(info);

        return await _dbContext.SaveChangesAsync() > 0;
    }

    public async Task<bool> CancelShare(int id, DataRange_2b range)
    {
        var info = await GetInfoById(id, range);

        if (info is null)
        {
            throw new NotFoundException("找不到相关数据#1");
        }

        info.SetShare(false);

        await _dbContext.Product.SingleUpdateAsync(info);

        return await _dbContext.SaveChangesAsync() > 0;
    }

    public async Task<bool> BindCategory(BindCategoryDto req, DataRange_2b range)
    {
        var model = ProductDataWatchRange(range, false, false);

        if (model == null)
            return false;

        var result = await model.Where(m => req.Ids.Contains(m.ID))
            .UpdateAsync(m => new ProductModel() { CategoryId = req.CategroyId, SubCategoryId = req.SubCategoryId });

        return result > 0;
    }

    public async Task<ProductModel> LinkChangeProductStruct(ProductModel model)
    {
        //todo 重构
        return new ProductModel();
        // var productJsonPath = model.FilePath;
        //
        // if (productJsonPath is null)
        // {
        //     throw new NotFoundException("数据丢失#1");
        // }
        //
        // var isExists = await _uploadService.Has(model.FilePath!);
        //
        // if (!isExists)
        // {
        //     throw new NotFoundException("数据丢失#2");
        // }
        //
        // var productStruct = await GetProductStruct(productJsonPath);
        //
        // productStruct!.SetTypesFlag(model.Type);
        // productStruct!.SetLanguageFlag(model.Language);
        //
        // var jsonProduct = JsonConvert.SerializeObject(productStruct, new JsonSerializerSettings()
        // {
        //     TypeNameHandling = TypeNameHandling.All
        // });
        //
        // var updatedAt = DateTime.Now;
        // // var version = updatedAt.GetTimeStampMilliseconds();
        // // var historyVersion = JsonConvert.DeserializeObject<List<long>>(model.HistoryVersion)!;
        // // historyVersion.Add(version);
        // // model.HistoryVersion = JsonConvert.SerializeObject(historyVersion);
        // //
        // // var jsonFilePath = await UploadStructFile(jsonProduct, version, model.ID);
        // //
        // // model.CurrentVersion = version;
        // // model.FilePath = jsonFilePath;
        // // model.FilePathHashCode = Helpers.CreateHashCode(jsonFilePath);
        // // model.FilesSize += jsonProduct.Length;
        // model.UpdatedAt = updatedAt;
        // return model;
    }

    private async Task<string> UploadStructFile(string jsonProduct, string jsonFilePath)
    {
        var uploadState = await _uploadService.PutAsync(jsonFilePath, jsonProduct);

        if (!uploadState)
        {
            throw new SavedException("数据包存储失败");
        }

        return jsonFilePath;
    }

    public async Task<string> UploadStructFile(string jsonProduct, long version, int id)
    {
        var jsonFilePath = GetProductJsonPath(version, id);

        var uploadState = await _uploadService.PutAsync(jsonFilePath, jsonProduct);

        if (!uploadState)
        {
            throw new SavedException("数据包存储失败");
        }

        return jsonFilePath;
    }

    protected internal async Task<ModelProduct> GetProductStruct(string productJsonPath, ProductModel productModel)
    {
        var fileContent = await _uploadService.Read(productJsonPath);
        var sr = new StreamReader(fileContent);
        var json = await sr.ReadToEndAsync();

        // 将语言枚举移到Common后, 会使原本的json字符串找不到类型而无法反序列化, 在此处手动替换`Language.Package.$type`中的类型,
        // 当后续不再使用老的json字符串时,可以将以下代码删除.
        json = json.Replace("ERP.Enums.Languages, ERP", "ERP.Enums.Languages, Common");
        var regex = new Regex("(ERP\\.Data\\.Products\\.[\\w\\[\\]\\.]+, )ERP");
        json = regex.Replace(json, "$1Product.Abstraction");
        var productStruct = JsonConvert.DeserializeObject<ERP.Data.Products.Product>(json,
            new JsonSerializerSettings()
            {
                TypeNameHandling = TypeNameHandling.All,
            })!;

        if (productStruct.Version == productStruct.CurrentVersion && productModel.CodeCompareDic != null)
        {
            var codeCompare = productModel.CodeCompareDic!;
            foreach (var item in productStruct.Variants)
            {
                item.Code = codeCompare.Keys.Contains(item.Sid!)
                    ? codeCompare[item.Sid!]
                    : string.Empty;
            }
        }

        return productStruct;
    }

    public async Task<(bool State, string Message)> SetProductSketch(SetProductSketchDto req, DataRange_2b range)
    {
        var template = await _dbContext.SketchTemplate.Where(m => m.ID == req.TemplateId).FirstOrDefaultAsync();

        if (template is null)
        {
            throw new Exceptions.Keywords.NotFoundException("找不到模板数据#2");
        }

        var (product, itemModel) = await BeforeBatchFillProductItem(req, template.LanguageId, range);

        if (itemModel is null && req.LanguageOption == LanguageOptionEnum.CurrentTemplateLang)
        {
            return (true, $"当前产品不存在【{template.LanguageSign}】语种,跳过");
        }

        var productStruct = await GetProductStruct(itemModel.FilePath, product);

        var sketch = productStruct.GetSketches();


        //todo  变体处理

        var newSketch = await SketchMode(req.Mode, sketch, req.Content);

        productStruct.SetSketches(newSketch);
        return await AfterBatchFillProduct(productStruct, product, itemModel);
    }

    private async Task<List<string>> SketchMode(ModeEnum mode, List<string>? sketch,
        List<Dictionary<string, string>> temp)
    {
        if (sketch is null)
        {
            sketch = new List<string>();
        }

        List<string> content = new List<string>();
        foreach (var item in temp)
        {
            content.Add(item["value"]);
        }

        switch (mode)
        {
            case ModeEnum.Cover:
                sketch = content;
                break;

            case ModeEnum.HeadAddition:
                for (int i = (content.Count - 1); i >= 0; i--)
                {
                    sketch.Insert(0, content[i]);
                }


                break;
            case ModeEnum.TailAppend:
                foreach (var item in content)
                {
                    sketch.Add(item);
                }

                break;
        }

        return sketch;
    }

    private async Task<ProductModel> UpdateProductStructWithNewVersion(ERP.Data.Products.Product productStruct,
        ProductModel model,
        DateTime updatedAt)
    {
        var jsonProduct = JsonConvert.SerializeObject(productStruct, new JsonSerializerSettings()
        {
            TypeNameHandling = TypeNameHandling.All
        });

        // var version = updatedAt.GetTimeStampMilliseconds();
        // var jsonFilePath = await UploadStructFile(jsonProduct, version, model.ID);
        // model.FilePath = jsonFilePath;
        // model.FilePathHashCode = Helpers.CreateHashCode(jsonFilePath);
        // model.FilesSize += jsonProduct.Length;
        model.UpdatedAt = updatedAt;
        return model;
    }

    public async Task InitItemVersion(ERP.Data.Products.Product productStruct,
        ProductModel model, ProductItemModel itemModel, long version)
    {
        productStruct.Images = model.Gallery;
        var jsonProduct = JsonConvert.SerializeObject(productStruct, new JsonSerializerSettings()
        {
            TypeNameHandling = TypeNameHandling.All
        });

        var jsonFilePath = await UploadStructFile(jsonProduct, version, model.ID);
        itemModel.CurrentVersion = version;
        itemModel.HistoryVersion = JsonConvert.SerializeObject(new List<long>() { version });
        itemModel.FilePath = jsonFilePath;
        itemModel.FilePathHashCode = Helpers.CreateHashCode(jsonFilePath);
        itemModel.FilesSize = jsonProduct.Length;
    }

    private async Task UpdateProductStructWithNewVersion(ERP.Data.Products.Product productStruct,
        ProductModel model, ProductItemModel itemModel)
    {
        //设置为二次修改
        model.SetEdited(true);
        productStruct.SetTypesFlag(model.Type);

        var jsonProduct = JsonConvert.SerializeObject(productStruct, new JsonSerializerSettings()
        {
            TypeNameHandling = TypeNameHandling.All
        });

        var updatedAt = DateTime.Now;
        var version = updatedAt.GetTimeStampMilliseconds();
        var historyVersion = JsonConvert.DeserializeObject<List<long>>(itemModel.HistoryVersion)!;
        historyVersion.Add(version);
        itemModel.HistoryVersion = JsonConvert.SerializeObject(historyVersion);

        var jsonFilePath = await UploadStructFile(jsonProduct, version, model.ID);

        itemModel.CurrentVersion = version;
        itemModel.FilePath = jsonFilePath;
        itemModel.FilePathHashCode = Helpers.CreateHashCode(jsonFilePath);
        itemModel.FilesSize += jsonProduct.Length;
        itemModel.UpdatedAt = updatedAt;

        model.FilesSize += jsonProduct.Length;
        model.UpdatedAt = updatedAt;
    }

    private async Task<ProductModel> UpdateProductStructWithNewVersion(ERP.Data.Products.Product productStruct,
        ProductModel model)
    {
        //设置为二次修改
        model.SetEdited(true);
        productStruct.SetTypesFlag(model.Type);

        var jsonProduct = JsonConvert.SerializeObject(productStruct, new JsonSerializerSettings()
        {
            TypeNameHandling = TypeNameHandling.All
        });

        var updatedAt = DateTime.Now;
        // var version = updatedAt.GetTimeStampMilliseconds();
        // var historyVersion = JsonConvert.DeserializeObject<List<long>>(model.HistoryVersion)!;
        // historyVersion.Add(version);
        // model.HistoryVersion = JsonConvert.SerializeObject(historyVersion);
        //
        // var jsonFilePath = await UploadStructFile(jsonProduct, version, model.ID);
        //
        // model.CurrentVersion = version;
        // model.FilePath = jsonFilePath; 
        // model.FilePathHashCode = Helpers.CreateHashCode(jsonFilePath);
        // model.FilesSize += jsonProduct.Length;
        model.UpdatedAt = updatedAt;
        return model;
    }

    public async Task<(bool State, string Message)> SetProductKeyword(SetProductKeywordDto req,
        DataRange_2b range)
    {
        var template = await _dbContext.KeywordTemplate.Where(m => m.ID == req.TemplateId).FirstOrDefaultAsync();

        if (template is null)
        {
            throw new Exceptions.Keywords.NotFoundException("找不到模板数据#2");
        }

        var (product, itemModel) = await BeforeBatchFillProductItem(req, template.LanguageId, range);
        if (itemModel is null && req.LanguageOption == LanguageOptionEnum.CurrentTemplateLang)
        {
            return (true, $"当前产品不存在【{template.LanguageSign}】语种,跳过");
        }

        var productStruct =
            KeywordMode(req.Mode, req.Position, await GetProductStruct(itemModel.FilePath, product), req.Content);

        if (req.Position == PositionEnum.Title)
        {
            product.Title = productStruct.GetTitle()!;
        }

        //todo 变体处理

        return await AfterBatchFillProduct(productStruct, product, itemModel);
    }

    protected ModelProduct KeywordMode(ModeEnum mode, PositionEnum position, ERP.Data.Products.Product productStruct,
        string content)
    {
        string? original = null;

        switch (position)
        {
            case PositionEnum.Title:
                original = productStruct.GetTitle();
                break;
            case PositionEnum.Desc:
                original = productStruct.GetDescription();
                break;
            case PositionEnum.Keyword:
                original = productStruct.GetKeyword();
                break;
        }


        //mode 1覆盖 2头部追加 3 尾部追加
        switch (mode)
        {
            case ModeEnum.Cover:
                original = content;
                break;
            case ModeEnum.HeadAddition:
                original = $"{content}{original}";
                break;
            case ModeEnum.TailAppend:
                original = $"{original}{content}";
                break;
        }

        switch (position)
        {
            case PositionEnum.Title:
                productStruct.SetTitle(original);
                break;
            case PositionEnum.Desc:
                productStruct.SetDescription(original);
                break;
                ;
            case PositionEnum.Keyword:
                productStruct.SetKeyword(original);
                break;
                ;
        }

        return productStruct;
    }

    private async Task<(bool State, string Message)> AfterBatchFillProduct(ModelProduct productStruct,
        ProductModel product, ProductItemModel itemModel)
    {
        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            await UpdateProductStructWithNewVersion(productStruct, product, itemModel);
            await _dbContext.Product.SingleUpdateAsync(product);
            await _dbContext.ProductItem.SingleUpdateAsync(itemModel);
            await _dbContext.SaveChangesAsync();
            await transaction.CommitAsync();
            return (true, "填充成功");
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            _logger.LogError(e, e.Message);
            return (false, e.Message);
        }
    }


    private async Task<(ProductModel product, ProductItemModel? itemModel)> BeforeBatchFillProductItem(
        BatchSetProductInfoDto req,
        int langId, DataRange_2b range)
    {
        var product = await GetInfoById(req.Id, range);

        if (product is null)
        {
            throw new NotFoundException("找不到产品相关数据#1");
        }

        ProductItemModel? itemModel = null;

        //产品默认语言
        if (req.LanguageOption == LanguageOptionEnum.Default)
        {
            itemModel = await GetItemInfoByLangId(product.ID, 0);
        }

        if (req.LanguageOption == LanguageOptionEnum.CurrentProductLang)
        {
            itemModel = await GetItemInfoByLangId(product.ID, product.UiLanguageId);
        }

        if (req.LanguageOption == LanguageOptionEnum.CurrentTemplateLang)
        {
            itemModel = await GetItemInfoByLangId(product.ID, langId);
        }
        
        if (itemModel is null)
        {
            throw new NotFoundException("找不到产品相关数据#2");
        }

        return (product, itemModel);
    }


    public async Task<(bool State, string Message)> SetProductExpand(SetProductExtDto req, DataRange_2b range)
    {
        var product = await GetInfoById(req.Id, range);

        if (product is null)
        {
            throw new NotFoundException("找不到相关数据#1");
        }

        var itemModel = await GetItemInfoByLangId(product.ID, product.UiLanguageId);

        if (itemModel is null)
        {
            throw new NotFoundException("找不到相关数据#2");
        }

        var productStruct = await GetProductStruct(itemModel.FilePath, product);


        if (req.Mode == ExpandModeEnum.Cover)
        {
            // productStruct.Expands = req.Content.;
        }
        else if (req.Mode == ExpandModeEnum.RemoveDuplicates)
        {
            //特性
            foreach (var item in req.Content.Content)
            {
                productStruct.Properties[item.PropertyKey].Value = item.Value;
            }
        }

        return await AfterBatchFillProduct(productStruct, product, itemModel);
    }

    public async Task<(Dictionary<int, int> imageComparison, ImagesStruct copySuccessImages, List<TempImagesModel>
            allTempImages)>
        BeforeCloneProWithImages(ImagesStruct? images, ProductModel product)
    {
        //处理图片
        var copySuccessImages = new ImagesStruct();
        var imageComparison = new Dictionary<int, int>();
        var allTempImages = new List<TempImagesModel>();

        if (images != null && images.Count > 0)
        {
            foreach (var item in images)
            {
                var tempImageModel = new TempImagesModel();
                if (item.IsNetwork)
                {
                    tempImageModel = new TempImagesModel(item.Url, item, _oemProvider.OEM.OssId,
                        ResourceModel.Types.Network, _session);
                }
                else
                {
                    var path = await GetImagePath(item.Path, product);
                    var copyState = await _uploadService.Copy(item.Path!, path);
                    if (!copyState)
                    {
                        throw new UploadException("资源复制失败#3");
                    }

                    tempImageModel = new TempImagesModel(path, item, _oemProvider.OEM.OssId,
                        ResourceModel.Types.Storage, _session);
                }

                await _dbContext.TempImages.AddAsync(tempImageModel);
                await _dbContext.SaveChangesAsync();
                imageComparison.Add(item.ID, tempImageModel.ID);
                copySuccessImages.Add(new Image(tempImageModel));
                allTempImages.Add(tempImageModel);
            }
        }

        return (imageComparison, copySuccessImages, allTempImages);
    }

    public async Task AfterCloneMoveImage(List<TempImagesModel> allTempImages, ProductModel model)
    {
        //移动图片
        await _dbContext.Resource
            .AddRangeAsync(allTempImages.Select(m => new ResourceModel(m, model.ID)).ToList());
        var tempImagesIds = allTempImages.Select(m => m.ID).ToList();
        await _dbContext.TempImages
            .Where(m => tempImagesIds.Contains(m.ID))
            .DeleteAsync();
    }

    public async Task<ProductItemModel> BeforeCloneWithItem(ProductItemModel oldItemModel, ProductModel oldProduct,
        ProductModel newProduct,
        Dictionary<int, int> imageComparison, long version, bool isDefault)
    {
        var newItemModel = new ProductItemModel(oldItemModel, _sessionProvider.Session, isDefault);

        // var currentVersionJsonFilepath = GetProductJsonPath(oldItemModel.CurrentVersion, oldProduct);
        var currentVersionJsonFilepath = oldItemModel.FilePath;

        var productStruct = await GetProductStruct(currentVersionJsonFilepath, oldProduct);

        var variants = productStruct.Variants;
        var newVariants = new Variants();
        newVariants.Attributes = productStruct.Variants.Attributes;
        if (variants.Count > 0)
        {
            foreach (var item in variants)
            {
                //处理主图
                if (item.Images.Main.HasValue)
                {
                    item.Images.Main = imageComparison.ContainsKey((int)item.Images.Main)
                        ? imageComparison[(int)item.Images.Main]
                        : null;
                }

                if (item.Images.Affiliate != null && item.Images.Affiliate.Count > 0)
                {
                    var affiliate = new List<int>();
                    var oldAffiliate = item.Images.Affiliate;
                    foreach (int i in oldAffiliate)
                    {
                        if (imageComparison.ContainsKey(i))
                        {
                            affiliate.Add(imageComparison[i]);
                        }
                    }

                    item.Images.Affiliate = affiliate;
                }

                newVariants.Add(item);
            }
        }

        productStruct.Properties[PropertyKey.SourceId].Value = oldProduct.ID;
        productStruct.Properties[PropertyKey.SourceItemId].Value = oldItemModel.ID;
        productStruct.Variants = newVariants;

        newItemModel.CurrentVersion = version;
        newItemModel.HistoryVersion = JsonConvert.SerializeObject(new List<long>() { oldItemModel.CurrentVersion });
        newItemModel.Pid = newProduct.ID;
        newItemModel.Title = productStruct.GetTitle()!;
        await InitItemVersion(productStruct, newProduct, newItemModel, ++version);

        return newItemModel;
    }

    private async Task<CopyProductViewModel> HandleClone(ProductItemModel itemModel, ProductModel model,
        ProductModel product, bool isDefault = false)
    {
        model.Language = (long)Types.Default;

        try
        {
            //处理图片
            var (imageComparison, copySuccessImages, allTempImages) =
                await BeforeCloneProWithImages(product.Gallery, product);

            model.Images = JsonConvert.SerializeObject(copySuccessImages);
            await _dbContext.Product.AddAsync(model);
            await _dbContext.SaveChangesAsync();

            var updatedAt = DateTime.Now;
            var version = updatedAt.GetTimeStampMilliseconds();

            var newItemModel =
                await BeforeCloneWithItem(itemModel, product, model, imageComparison, version, isDefault);
            model.FilesSize = newItemModel.FilesSize;
            model.UpdatedAt = updatedAt;
            model.MainImage = copySuccessImages.Count > 0
                ? JsonConvert.SerializeObject(copySuccessImages.First())
                : null;
            model.Title = newItemModel.Title;

            await _dbContext.ProductItem.AddAsync(newItemModel);
            await _dbContext.Product.SingleUpdateAsync(model);
            await AfterCloneMoveImage(allTempImages, model);
            await _dbContext.SaveChangesAsync();
            // await transaction.CommitAsync();
            return new CopyProductViewModel()
            {
                SourceId = product.ID,
                SourceItemId = itemModel.ID,
                Product = model,
                ProductItem = newItemModel
            };
        }
        catch (Exception e)
        {
            // await transaction.RollbackAsync();
            _logger.LogError(e, e.Message);
            throw new SavedException(e.Message);
        }
    }

    /// <summary>
    /// 导出克隆 (调用此方法需要开启事务)
    /// </summary>
    /// <param name="proId"></param>
    /// <returns></returns>
    // public async Task<CopyProductViewModel> ImportClone(int proId, Languages lang)
    // {
    //     var (product, itemModel) = await CloneInt(proId, lang);
    //     var model = new ProductModel(_sessionProvider.Session, product);
    //     model.SetExport(true);
    //     return await HandleClone(itemModel, model, product);
    // }

    /// <summary>
    /// 设置默认展示语言
    /// </summary>
    /// <param name="product"></param>
    /// <param name="lang"></param>
    /// <returns></returns>
    /// <exception cref="NotFoundException"></exception>
    private Task SetUiLanguage(ProductModel product, Languages lang)
    {
        int biter = -1;
        if (lang == Languages.Default)
        {
            biter = -1;
        }
        else
        {
            var language = lang.GetDescriptionByKey("Code");
            if (ProductModel.LanguageCode.Contains(language))
            {
                biter = ProductModel.LanguageCode.FindIndex(m => m == language);
            }
            else
            {
                throw new NotFoundException("非法语言code");
            }
        }

        product.SetUILanguage(biter + 1);
        return Task.CompletedTask;
    }

    /// <summary>
    /// 导出克隆 (调用此方法需要开启事务)
    /// </summary>
    /// <param name="proId"></param>
    /// <param name="lang"></param>
    /// <returns></returns>
    /// <exception cref="NotFoundException"></exception>
    public async Task<CopyProductViewModel> ImportClone(int proId, Languages lang)
    {
        var product = await GetInfoById(proId);
        if (product == null)
            throw new NotFoundException("找不到相关产品数据#1");

        var translate = GetTranslated(product, true);

        if (!translate.Keys.Contains(lang))
            throw new NotFoundException("产品中未找到需要克隆的语言#2");

        var model = new ProductModel(_sessionProvider.Session, product);
        model.SetExport(true);
        await SetUiLanguage(model, lang);
        return await CloneWithAllItem(model, product, lang);
    }

    private async Task<CopyProductViewModel> CloneWithAllItem(ProductModel model, ProductModel product, Languages lang)
    {
        var itemList = await GetItemInfoList(product.ID);

        if (itemList.Count <= 0)
        {
            throw new NotFoundException("找不到相关产品数据#3");
        }

        try
        {
            //处理图片
            var (imageComparison, copySuccessImages, allTempImages) =
                await BeforeCloneProWithImages(product.Gallery, product);
            model.Images = JsonConvert.SerializeObject(copySuccessImages);
            await _dbContext.Product.AddAsync(model);
            //先保存 Pid
            await _dbContext.SaveChangesAsync();

            //items
            List<ProductItemModel> itemModels = new List<ProductItemModel>();
            var updatedAt = DateTime.Now;
            var version = updatedAt.GetTimeStampMilliseconds();
            foreach (var proItem in itemList)
            {
                var newItemModel =
                    await BeforeCloneWithItem(proItem, product, model, imageComparison, ++version, false);
                model.FilesSize = newItemModel.FilesSize;
                model.UpdatedAt = updatedAt;
                model.MainImage = copySuccessImages.Count > 0
                    ? JsonConvert.SerializeObject(copySuccessImages.First())
                    : null;
                itemModels.Add(newItemModel);
                if (proItem.Language == lang)
                {
                    model.Title = newItemModel.Title;
                }

                model.FilesSize += newItemModel.FilesSize;
            }

            model.UpdatedAt = updatedAt;
            model.MainImage = copySuccessImages.Count > 0
                ? JsonConvert.SerializeObject(copySuccessImages.First())
                : null;
            await _dbContext.ProductItem.AddRangeAsync(itemModels);
            await _dbContext.Product.SingleUpdateAsync(model);
            await AfterCloneMoveImage(allTempImages, model);
            await _dbContext.SaveChangesAsync();
            return new CopyProductViewModel()
            {
                SourceId = product.ID,
                Product = model,
            };
        }
        catch (Exception e)
        {
            // await transaction.RollbackAsync();
            _logger.LogError(e, e.Message);
            throw new SavedException(e.Message);
        }
    }


    private async Task<(ProductModel product, ProductItemModel itemModel)> CloneInt(int proId, int lang)
    {
        var product = await GetInfoById(proId);
        if (product is null)
        {
            throw new NotFoundException("产品相关数据不存在#1");
        }

        // var langId = language != Languages.Default ? (int)Math.Log2((long)language) + 1 : 0;

        var itemModel = await GetItemInfoByLangId(proId, lang);

        if (itemModel is null)
        {
            throw new NotFoundException("产品中未找到需要克隆的语言#2");
        }

        return (product, itemModel);
    }

    public async Task<CopyProductViewModel> B2cClone(int proId, int lang)
    {
        var (product, itemModel) = await CloneInt(proId, lang);
        var model = new ProductModel(_sessionProvider.Session, product,
            await GetProductStruct(itemModel.FilePath, product));
        model.SetB2C(true);
        
        //设置B2C克隆的原产品ID
        var features = new Features { { _b2cOriginalPidKey, product.ID } };
        model.Feature = JsonConvert.SerializeObject(features);
        
        return await HandleClone(itemModel, model, product, true);
    }

    private async Task<TempImagesModel> ProcessImage(Image item, ProductModel model)
    {
        if (!item.IsNetwork)
        {
            var path = await GetImagePath(item.Path!, model);
            var copyState = await _uploadService.Copy(item.Path!, path);
            if (!copyState)
            {
                throw new UploadException("资源复制失败#3");
            }

            return new TempImagesModel(path, item, _oemProvider.OEM.OssId,
                ResourceModel.Types.Storage, _session);
        }

        return new TempImagesModel(item.ID, item.Url, item, _oemProvider.OEM.OssId,
            ResourceModel.Types.Network, _session);
    }


    public async Task<(bool State, string Message)> Clone(IdDto req, bool isShare)
    {
        var product = await GetInfoById(req.ID);
        if (product is null)
        {
            throw new NotFoundException("找不到相关产品数据#1");
        }

        var itemList = await GetItemInfoList(product.ID);

        if (itemList.Count <= 0)
        {
            throw new NotFoundException("找不到相关产品数据#2");
        }

        var company = await _companyService.GetInfoById(_companyId);

        if (company is null)
        {
            throw new Exceptions.Company.NotFoundException("找不到公司相关数据#2");
        }

        var model = new ProductModel(_sessionProvider.Session, product);

        if (isShare)
        {
            //设置来源分享
            model.SetFromShare(true);
        }

        model.SetCollect(false);

        //设置审核状态
        model.SetAudit((int)company.ProductAudit);


        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            //处理图片
            var copySuccessImages = new ImagesStruct();
            var imageComparison = new Dictionary<int, int>();
            var images = product.Gallery;
            var allTempImages = new List<TempImagesModel>();
            if (images != null && images.Count > 0)
            {
                foreach (var item in images)
                {
                    var tempImageModel = new TempImagesModel();
                    if (item.IsNetwork)
                    {
                        tempImageModel = new TempImagesModel(item.Url, item, _oemProvider.OEM.OssId,
                            ResourceModel.Types.Network, _session);
                    }
                    else
                    {
                        var path = await GetImagePath(item.Path, model);
                        var copyState = await _uploadService.Copy(item.Path!, path);
                        if (!copyState)
                        {
                            throw new UploadException("资源复制失败#3");
                        }

                        tempImageModel = new TempImagesModel(path, item, _oemProvider.OEM.OssId,
                            ResourceModel.Types.Storage, _session);
                    }

                    await _dbContext.TempImages.AddAsync(tempImageModel);
                    await _dbContext.SaveChangesAsync();
                    imageComparison.Add(item.ID, tempImageModel.ID);
                    copySuccessImages.Add(new Image(tempImageModel));
                    allTempImages.Add(tempImageModel);
                }
            }

            model.Images = JsonConvert.SerializeObject(copySuccessImages);
            await _dbContext.Product.AddAsync(model);
            await _dbContext.SaveChangesAsync();

            List<ProductItemModel> itemModels = new List<ProductItemModel>();
            var updatedAt = DateTime.Now;
            var version = updatedAt.GetTimeStampMilliseconds();
            foreach (var proItem in itemList)
            {
                var newItemModel = new ProductItemModel(proItem, _sessionProvider.Session);

                // var currentVersionJsonFilepath = GetProductJsonPath(proItem.CurrentVersion, product);
                var currentVersionJsonFilepath = proItem.FilePath;
                var productStruct = await GetProductStruct(currentVersionJsonFilepath, model);
                var variants = productStruct.Variants;
                var newVariants = new Variants();
                newVariants.Attributes = productStruct.Variants.Attributes;
                if (variants.Count > 0)
                {
                    foreach (var item in variants)
                    {
                        //处理主图
                        if (item.Images.Main.HasValue)
                        {
                            item.Images.Main = imageComparison.ContainsKey((int)item.Images.Main)
                                ? imageComparison[(int)item.Images.Main]
                                : null;
                        }

                        if (item.Images.Affiliate != null && item.Images.Affiliate.Count > 0)
                        {
                            var affiliate = new List<int>();
                            var oldAffiliate = item.Images.Affiliate;
                            foreach (int i in oldAffiliate)
                            {
                                if (imageComparison.ContainsKey(i))
                                {
                                    affiliate.Add(imageComparison[i]);
                                }
                            }

                            item.Images.Affiliate = affiliate;
                        }

                        newVariants.Add(item);
                    }
                }

                productStruct.Variants = newVariants;
                newItemModel.CurrentVersion = updatedAt.GetTimeStampMilliseconds();
                newItemModel.HistoryVersion = JsonConvert.SerializeObject(new List<long>() { proItem.CurrentVersion });
                newItemModel.Pid = model.ID;
                await InitItemVersion(productStruct, model, newItemModel, ++version);
                itemModels.Add(newItemModel);
                model.FilesSize += newItemModel.FilesSize;
            }

            model.UpdatedAt = updatedAt;
            model.MainImage = copySuccessImages.Count > 0
                ? JsonConvert.SerializeObject(copySuccessImages.First())
                : null;


            await _dbContext.ProductItem.AddRangeAsync(itemModels);
            await _dbContext.Product.SingleUpdateAsync(model);

            //移动图片
            await _dbContext.Resource
                .AddRangeAsync(allTempImages.Select(m => new ResourceModel(m, model.ID)).ToList());
            var tempImagesIds = allTempImages.Select(m => m.ID).ToList();
            await _dbContext.TempImages
                .Where(m => tempImagesIds.Contains(m.ID))
                .DeleteAsync();

            //添加产品计数
            await _statisticService.InsertOrCount(_session, Statistic.NumColumnEnum.ProductInsert,
                Statistic.NumColumnEnum.PurchaseCount);


            await _dbContext.SaveChangesAsync();

            //设置重复次数
            await SetUserLevelRepeatNum(product.HashCode);

            await transaction.CommitAsync();
            return (true, "导入成功");
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            _logger.LogError(e, e.Message);
            return (false, e.Message);
            throw;
        }


        // var now = DateTime.Now;
        // var history = now.GetTimeStampMilliseconds();
        // var jsonFilepath = GetProductJsonPath(history, model);
        //
        // var state = await _uploadService.Copy(product.FilePath!, jsonFilepath);
        //
        //
        // model.CurrentVersion = history;
        // model.HistoryVersion = JsonConvert.SerializeObject(new List<long>() { history });
        // model.FilePath = jsonFilepath;
        // model.FilePathHashCode = Helpers.CreateHashCode(jsonFilepath);
        // model.FilesSize = await _uploadService.GetSize(jsonFilepath);
        // model.Language = product.Language;
        //
        // return true;
    }

    public async Task<bool> CheckTitle(int id)
    {
        var product = await GetInfoById(id);

        if (product is null)
        {
            throw new NotFoundException("找不到相关产品数据#1");
        }

        // product.UserLevelRepeatNum = await UserLevelRepeatNum(product.HashCode, product.ID);
        // await _dbContext.Product.SingleUpdateAsync(product);
        // await _dbContext.SaveChangesAsync();
        await SetUserLevelRepeatNum(product.HashCode);
        return true;
    }

    public async Task<string> GetImagePath(string path, ProductModel model)
    {
        var pathArr = path.Split('.');

        return await _uploadService.FilePath(model.OEMID, model.UserID, pathArr[pathArr.Length - 1],
            Upload.BaseService.FileEnum.TXT);
    }

    public async Task<object> SetDefaultLanguage(SetDefaultLanguageDto req, DataRange_2b range)
    {
        var product = await GetInfoById(req.Id, range);

        if (product is null)
        {
            throw new NotFoundException("找不到相关产品数据#1");
        }

        var itemModel = await GetItemInfoByLangId(product.ID, req.LanguageId);

        if (itemModel is null)
        {
            throw new NotFoundException("找不到相关产品数据或该产品无此语种#2");
        }

        var proStruct = await GetProductStruct(itemModel.FilePath, product);
        product.Title = proStruct.GetTitle()!;
        product.HashCode = Helpers.CreateHashCode(product.Title);
        product.Prices = JsonConvert.SerializeObject(proStruct.GetPrices());

        var language = req.Language.GetName().ToLower();
        int biter = -1;
        if (language == "default")
        {
            biter = -1;
        }
        else
        {
            if (ProductModel.LanguageCode.Contains(language))
            {
                biter = ProductModel.LanguageCode.FindIndex(m => m == language);
            }
            else
            {
                throw new NotFoundException("非法语言code");
            }
        }

        product.SetUILanguage(biter + 1);

        var state = 0;
        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            //设置重复次数
            await SetUserLevelRepeatNum(product.HashCode);

            await _dbContext.Product.SingleUpdateAsync(product);
            await _dbContext.SaveChangesAsync();
            await transaction.CommitAsync();
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            _logger.LogError(e, e.Message);
            throw;
        }

        return new
        {
            updated_at = product.UpdatedAt,
            updated_at_ticks = product.UpdatedAtTicks,
            ui_language = product.UiLanguage.GetName().ToLower(),
        };
    }

    public async Task<(bool State, string Message)> SetProductRandom(SetProductRandomDto req, DataRange_2b range)
    {
        var product = await GetInfoById(req.Id, range);

        if (product is null)
        {
            throw new NotFoundException("找不到相关产品数据#1");
        }

        var itemModel = await GetItemInfoByLangId(product.ID, req.LanguageId);


        if (itemModel is null)
        {
            if (req.Language == Languages.Default)
            {
                throw new NotFoundException("找不到相关产品数据#2");
            }

            return (true, $"当前产品不存在【{req.Language.GetDescriptionByKey("Name")}】语种,跳过");
        }


        var productStruct = await GetProductStruct(itemModel.FilePath!, product);

        //获取关键词模板内容
        var keywordTemplateCollect = await _dbContext.KeywordTemplate
            .Where(m => req.Keywords.Contains(m.ID))
            .ToDictionaryAsync(m => m.ID, m => m);

        if (req.Keywords.Count != keywordTemplateCollect.Count)
        {
            throw new NotFoundException("找不到关键词模板#2");
        }

        //获取卖点模板内容
        var sketchTemplateCollect = await _dbContext.SketchTemplate
            .Where(m => req.Sketch.Contains(m.ID))
            .ToDictionaryAsync(m => m.ID, m => m);

        if (req.Sketch.Count != sketchTemplateCollect.Count)
        {
            throw new NotFoundException("找不到卖点模板#3");
        }


        var useKeywordIds = new List<int>();
        if (req.Mode == ProductRandomModeEnum.Main || req.Mode == ProductRandomModeEnum.All)
        {
            var keyword = keywordTemplateCollect[RandomArray(req.Keywords)];
            var sketch = sketchTemplateCollect[RandomArray(req.Sketch)];

            productStruct.SetKeyword(keyword.Value);
            productStruct.SetSketches(JsonConvert.DeserializeObject<List<string>>(sketch.Content));
            useKeywordIds.Add(keyword.ID);
        }

        if (req.Mode == ProductRandomModeEnum.Variant || req.Mode == ProductRandomModeEnum.All)
        {
            var variants = productStruct.Variants;

            foreach (var item in variants)
            {
                var keyword = keywordTemplateCollect[RandomArray(req.Keywords)];
                var sketch = sketchTemplateCollect[RandomArray(req.Sketch)];
                item.SetKeyword(keyword.Value);
                item.SetSketches(JsonConvert.DeserializeObject<List<string>>(sketch.Content));
                useKeywordIds.Add(keyword.ID);
            }

            productStruct.Variants = variants;
        }

        return await AfterBatchFillProduct(productStruct, product, itemModel);
    }

    private int RandomArray(List<int> arr)
    {
        Random rnd = new Random();
        int index = rnd.Next(arr.Count);
        return arr[index];
    }

    public async Task<(bool State, string Message)> SetProductPrice(SetProductPriceDto req)
    {
        var product = await GetInfoById(req.Id);

        if (product is null)
        {
            throw new NotFoundException("找不到相关产品数据#1");
        }

        var itemModel = await GetItemInfoByLangId(product.ID, product.UiLanguageId);
        if (itemModel is null)
        {
            throw new NotFoundException("找不到相关产品数据#2");
        }

        var productStruct = await GetProductStruct(itemModel.FilePath, product);

        // var unit = product.Coin.IsNotEmpty() ? product.Coin : _session.GetUnitConfig();

        var unit = _session.GetProductShowUnit()
            ? product.Coin.IsNotEmpty() ? product.Coin : _session.GetUnitConfig()
            : _session.GetUnitConfig();

        // 金额
        product.ChangePrices(productStruct, req.Params, unit);

        return await AfterBatchFillProduct(productStruct, product, itemModel);
    }


    private async Task<EditProductItemDto> GetProductInfoToUi(ProductModel product, ProductItemModel itemModel,
        long version)
    {
        // var productJsonPath = GetProductJsonPath(version, product);
        var productJsonPath = itemModel.FilePath;
        var isExists = await _uploadService.Has(productJsonPath);

        if (!isExists)
        {
            throw new NotFoundException("数据包丢失#1");
        }

        var productStruct = await GetProductStruct(productJsonPath, product);

        var viewModel = _convertProduct.ToBeProViewModelWithHistory(productStruct, product, itemModel,
            _session.GetUnitConfig(), _oemProvider);
        return viewModel;
    }

    private async Task<EditProductItemDto> GetProductInfoToUi(ProductModel product, ProductItemModel item,
        List<ProductItemModel> listItem)
    {
        // var productJsonPath = GetProductJsonPath(item.CurrentVersion, product);
        var productJsonPath = item.FilePath;
        var isExists = await _uploadService.Has(productJsonPath);

        if (!isExists)
        {
            throw new NotFoundException("数据包丢失#1");
        }

        var productStruct = await GetProductStruct(productJsonPath, product);

        var viewModel = _convertProduct.ToBeProViewModel(productStruct, product, item, listItem,
            _session.GetUnitConfig(), _session.GetProductShowUnit(), _oemProvider);
        viewModel.IsUseMatting = await _packagesResourceService.CheckIsHaveAvailableResource(PackageTypeEnum.Matting);
        viewModel.IsUseImgTran = await _packagesResourceService.CheckIsHaveAvailableResource(PackageTypeEnum.ImgTranslation);
        return viewModel;
    }


    public async Task<object?> HistoryView(HistoryViewDto req)
    {
        var product = await GetInfoById(req.Id);

        if (product is null)
        {
            throw new NotFoundException("找不到相关产品数据#1");
        }

        var itemModel = await GetItemInfoById(req.ItemId, req.Id);

        if (itemModel is null)
        {
            throw new NotFoundException("找不到相关产品数据#2");
        }

        return await GetProductInfoToUi(product, itemModel, req.Version);
    }

    public async Task<bool> RollbackVersion(HistoryViewDto req)
    {
        //todo 重构
        return false;
        // var product = await GetInfoById(_dbContext, req.Id);
        //
        // if (product is null)
        // {
        //     throw new NotFoundException("找不到相关产品数据#1");
        // }
        //
        // var productJsonPath = GetProductJsonPath(req.Version, product);
        //
        // var isExists = await _uploadService.Has(productJsonPath);
        //
        // if (!isExists)
        // {
        //     throw new NotFoundException("数据丢失#1");
        // }
        //
        // var productStruct = await GetProductStruct(productJsonPath);
        //
        //
        // var images = productStruct.Images;
        //
        // if (images is not null && images.Count > 0)
        // {
        //     var networkImages = images.Where(m => m.IsNetwork)
        //         .Select((m, v) => (m.ID, v))
        //         .ToDictionary(x => x.ID, x => x.v);
        //
        //     if (networkImages.Count > 0)
        //     {
        //         var networkImagesIds = networkImages.Select(m => m.Key).ToList();
        //         var resources = await _dbContext.Resource
        //             .Where(m => networkImagesIds.Contains(m.ID) && m.Type == ResourceModel.Types.Storage)
        //             .Select(m => new Image(m))
        //             .ToDictionaryAsync(m => m.ID, m => m);
        //
        //         if (resources.Count > 0)
        //         {
        //             foreach (var item in resources)
        //             {
        //                 images[networkImages[item.Key]] = item.Value;
        //             }
        //         }
        //     }
        //
        //     productStruct.Images = images;
        //
        //     var networkImagesCount = images.Count(m => m.IsNetwork);
        //     if (networkImagesCount == 0)
        //     {
        //         product.IsImageAsync = false;
        //     }
        // }
        //
        // await UpdateProductStructNoChangeVersion(productStruct, product);
        // product.CurrentVersion = req.Version;
        // product.FilePath = productJsonPath;
        // product.FilePathHashCode = Helpers.CreateHashCode(productJsonPath);
        //
        // await _dbContext.Product.SingleUpdateAsync(product);
        //
        // return await _dbContext.SaveChangesAsync() > 0;
    }

    public async Task<bool> SyncNetworkImage(SyncNetworkImageDto req)
    {
        return true;
        // var product = await GetInfoById(_dbContext, req.Id);
        //
        // if (product is null)
        // {
        //     throw new NotFoundException("找不到相关产品数据#1");
        // }
        //
        // var resource = await _dbContext.Resource.Where(m => m.ID == req.ImgId).FirstOrDefaultAsync();
        //
        // if (resource is null)
        // {
        //     throw new Exceptions.ImageResource.NotFoundException("找不到相关图片数据#2");
        // }
        //
        // if (resource.Type != ResourceModel.Types.Network)
        // {
        //     var result = await _uploadService.DownloadImage(resource.Path);
        //     var s = result.GetResponseStream();
        //
        //     string filePath = await _uploadService.FilePath(_oemId, _userId, result.ContentType.ToLower());
        //     //保存图片
        //     var state = await _uploadService.PutStream(filePath, s);
        //     if (!state)
        //     {
        //         throw new UploadException("上传图片发生错误#3");
        //     }
        //
        //     resource.Path = filePath;
        //     resource.Size = s.Length;
        //     resource.Type = ResourceModel.Types.Storage;
        //     resource.UpdatedAt = DateTime.Now;
        // }
        //
        // var productStruct = await GetProductStruct(product.FilePath!);
        //
        // var images = productStruct.Images;
        //
        // if (images is null || images.Count <= 0)
        // {
        //     throw new NotFoundException("该产品暂无图片#4");
        // }
        //
        // var index = images.Where(m => m.ID == req.ImgId).Select((v, i) => i).First();
        //
        // images[index] = new Image(resource);
        //
        // productStruct.Images = images;
        //
        // var networkImagesCount = images.Count(m => m.IsNetwork);
        // if (networkImagesCount == 0)
        // {
        //     product.IsImageAsync = false;
        // }
        //
        // var transaction = await _dbContext.Database.BeginTransactionAsync();
        // try
        // {
        //     await UpdateProductStructNoChangeVersion(productStruct, product);
        //     await _dbContext.Product.SingleUpdateAsync(product);
        //
        //     await _dbContext.Resource.SingleUpdateAsync(resource);
        //
        //     await _dbContext.SaveChangesAsync();
        //
        //     await transaction.CommitAsync();
        // }
        // catch (Exception e)
        // {
        //     await transaction.RollbackAsync();
        //     Console.WriteLine(e.Message);
        //     return false;
        // }
        //
        // return true;
    }


    public async Task<bool> FastSetAttributes(FastSetAttributesDto req)
    {
        var product = await GetInfoById(req.Id);

        if (product is null)
        {
            throw new NotFoundException("找不到相关产品数据#1");
        }

        var item = await GetItemInfoById(req.ItemId, req.Id);

        if (item is null)
        {
            throw new NotFoundException("找不到相关产品数据#2");
        }

        if (item.UpdatedAtTicks != req.UpdatedAtTicks)
        {
            throw new NotFoundException("数据不同步#2");
        }

        var productStruct = await GetProductStruct(item.FilePath, product);


        //Variants
        Variants productVariants = new Variants();
        var variants = req.Variants;

        if (variants != null)
        {
            foreach (var i in variants)
            {
                productVariants.Add(i, req.Coin);
            }

            productStruct.Variants = productVariants;
        }

        //Attribute
        var attributes = req.Attributes;
        Data.Products.Attributes productAttributes = new Data.Products.Attributes();

        if (attributes != null)
        {
            foreach (var i in attributes)
            {
                productAttributes.Add(i.Key, attributes[i.Key].Name, attributes[i.Key].Value, attributes[i.Key].State);
            }
        }

        productStruct.Variants.Attributes = productAttributes;

        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            await UpdateProductStructWithNewVersion(productStruct, product, item);
            await _dbContext.Product.SingleUpdateAsync(product);
            await _dbContext.ProductItem.SingleUpdateAsync(item);
            await _dbContext.SaveChangesAsync();
            await transaction.CommitAsync();
            return true;
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            _logger.LogError(e, e.Message);
            throw new SavedException("保存失败#3");
        }
    }

    public async Task<ProductModel?> GetOnGoingProduct(int id)
    {
        return await _dbContext.Product
            .Where(m =>
                (m.Type & (long)Types.OnGoing) == (long)Types.OnGoing
                && m.ID == id
            ).FirstOrDefaultAsync();
    }

    public async Task<EditProductItemDto?> EditWithShow(EditWithShowDto req, DataRange_2b range)
    {
        EditProductItemDto editParams = new EditProductItemDto()
        {
            Common = req.Common,
            Info = req.Info,
        };
        try
        {
            await SaveOrUpdate(editParams, range);
            return await Show(new ShowDto() { Id = req.Id, ItemId = req.ItemId }, range);
        }
        catch (Exception e)
        {
            throw new SavedException(e.Message);
        }

        return null;
    }

    public async Task<object> GetWaitDelete()
    {
        IEnumerable<ProductModel> model;

        model = ProductDataWatchRange(false);

        //排除上传中/导出/B2C状态的数据
        model = model.Where(m => (m.Type & (long)Types.OnGoing) != (long)Types.OnGoing);
        model = model.Where(m => (m.Type & (long)Types.Export) != (long)Types.Export);
        model = model.Where(m => (m.Type & (long)Types.B2C) != (long)Types.B2C);
        model = model.Where(m => (m.Type & (long)Types.Recycle) == (long)Types.Recycle);

        return model.OrderBy(m => m.CreatedAt).Select(m => new
        {
            Id = m.ID,
            Title = m.Title
        }).Take(200).ToList();
    }




    /// <summary>
    /// 删除产品数据
    /// </summary>
    /// <param name="id"></param>
    private async Task DeleteProductById(int id)
    {
        await _dbContext.Product.Where(m => m.ID == id).DeleteAsync();
    }

    /// <summary>
    /// 删除产品数据
    /// </summary>
    /// <param name="ids"></param>
    private async Task DeleteProductById(List<int> ids)
    {
        await _dbContext.Product.Where(m => ids.Contains(m.ID)).DeleteAsync();
    }

    /// <summary>
    /// 删除产品Item数据
    /// </summary>
    /// <param name="pid"></param>
    /// <param name="id"></param>
    private async Task DeleteProductItemByIdAndPid(int pid, int id)
    {
        await _dbContext.ProductItem
            .Where(m => m.Pid == pid && m.ID == id)
            .DeleteAsync();
    }

    /// <summary>
    /// 删除产品Item数据
    /// </summary>
    /// <param name="pid"></param>
    /// <param name="ids"></param>
    private async Task DeleteProductItemByIdAndPid(int pid, List<int> ids)
    {
        await _dbContext.ProductItem
            .Where(m => m.Pid == pid && ids.Contains(m.ID))
            .DeleteAsync();
    }

    //todo 路径待修改
    public async Task<List<int>> DeleteProductItemStorage(List<ProductItemModel> itemList, ProductModel product)
    {
        var deleteItemIds = new List<int>();
        foreach (var item in itemList)
        {
            var historyVersions = JsonConvert.DeserializeObject<List<long>>(item.HistoryVersion);
            if (historyVersions is null)
            {
                throw new NotFoundException("历史版本丢失#2");
            }

            var historyVersionsFilePath = historyVersions.Select(m => GetProductJsonPath(m, product)).ToList();
            await _uploadService.BatchDeleteAsync(historyVersionsFilePath, false);
            deleteItemIds.Add(item.ID);
        }

        return deleteItemIds;
    }

    /// <summary>
    /// 克隆产品保留单一关系
    /// </summary>
    /// <param name="pid"></param>
    /// <param name="type">保留关系 1上传 2导出</param>
    /// <exception cref="NotFoundException"></exception>
    public async Task UpdateProductType(int pid, int type = 1)
    {
        var info = await _dbContext.Product.Where(m => m.ID == pid).FirstOrDefaultAsync();

        if (info == null)
        {
            throw new NotFoundException("找不到相关产品数据");
        }

        if (type == 1)
        {
            info.SetUpload(true);
            info.SetExport(false);
        }
        else
        {
            info.SetUpload(false);
            info.SetExport(true);
        }

        await _dbContext.Product.SingleUpdateAsync(info);

        await _dbContext.SaveChangesAsync();
    }


    public async Task<DbSetExtension.PaginateStruct<ListViewModel>> GetJuniorProduct(int oemId, int companyId, int page,
        int limit)
    {
        //audit
        var audited = ProductModel.AUDIT_AUDITED;

        var paginate = await _dbContext.Product
            .Where(m => m.OEMID == oemId && m.CompanyID == companyId)
            .Where(m => ((m.Type & (long)Types.MASK_AUDIT)) >= audited)
            .Select(m => new ListViewModel(m))
            .ToPaginateAsync(page, limit);

        foreach (var item in paginate.Items)
        {
            item.SetLanguagePack(GetTranslated(item.Language));
            item.SetPrices(_session.GetUnitConfig());
        }

        return paginate;
    }

    public async Task<List<LanguagePackViewModel>> GetLanguagePack(ProductModel info)
    {
        return await _dbContext.ProductItem
            .Where(m => m.Pid == info.ID)
            .Select(m => new LanguagePackViewModel(m))
            .ToListAsync();
    }


    /// <summary>
    /// 检测产品语言包是否存在
    /// </summary>
    /// <param name="id">产品id</param>
    /// <param name="language">产品语言</param>
    /// <returns></returns>
    public async Task<Result> CheckProductLanguageRT(int id, Languages language)
    {
        var info = await GetInfoById(id);
        if (info is null)
            return Result.Fail("找不到相关数据#2503");

        var allLanguage = GetTranslated(info, true);
        return allLanguage.ContainsKey(language) ? Result.Ok().WithSuccess(info.Title) : Result.Fail(info.Title);
    }

    public async Task<(bool State, Image ImgObj)> ReplaceProductImage(ReplaceProductImageDto req)
    {
        var product = await GetInfoById(req.ProductId);
        if (product == null)
        {
            throw new NotFoundException("找不到产品相关数据#1");
        }

        var resourceInfo = await _imagesResourceService.GetInfoById(req.ImgId);

        if (resourceInfo == null)
        {
            throw new NotFoundException("找不到图片相关数据#1");
        }


        //修改产品图库信息
        var images = product.Gallery;
        foreach (var item in images!)
        {
            if (item.ID == req.ImgId)
            {
                item.Path = req.ImgObj.Path;
                item.Size = req.ImgObj.Size;
                item.Time = req.ImgObj.Time;
                // item.IsEditor = req.IsEditor;
            }
        }

        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            //修改产品itemSize
            var sizeDiff = req.ImgObj.Size - resourceInfo.Size;
            product.ImagesSize += sizeDiff;
            //修改产品
            product.Images = JsonConvert.SerializeObject(images);
            product.MainImage = images.Count > 0 ? JsonConvert.SerializeObject(images.First()) : null;
            product.UpdatedAt = DateTime.Now;
            await _dbContext.Product.SingleUpdateAsync(product);

            //修改原正式资源数据
            resourceInfo.Path = req.ImgObj.Path!;
            resourceInfo.Size = req.ImgObj.Size;
            resourceInfo.UpdatedAt = (DateTime)req.ImgObj.Time!;
            await _dbContext.Resource.SingleUpdateAsync(resourceInfo);
            req.ImgObj.Size = resourceInfo.ID;
            //删除临时资源
            await _dbContext.TempImages.Where(m => m.ID == req.ImgObj.ID).DeleteAsync();

            await _dbContext.SaveChangesAsync();
            await transaction.CommitAsync();
            return (true, req.ImgObj);
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            _logger.LogError(e, e.Message);
            throw;
        }
    }

    public async Task<(bool State, string Result)> GoMatting(GoMattingDto req)
    {
        //嘉杰独立抠图 a5d9203cfb83471f9040f9206386b9d9
        var apiKey = "db379440fda943e895f4af2a50a55ebf";
        var requestUrl = "http://www.picup.shop/api/v1/matting2?mattingType=6";

        ByteArrayContent imageByte = new ByteArrayContent(Convert.FromBase64String(req.Base64Data));

        var client = new HttpClient();
        var formData = new MultipartFormDataContent();
        formData.Headers.Add("APIKEY", apiKey);
        formData.Add(imageByte, "file", $"{Helpers.GetRandStr(20)}.jpeg");
        formData.Add(new StringContent(req.Type), "bgcolor");
        var response = await client.PostAsync(requestUrl, formData);
        var vm = JsonConvert.DeserializeObject<MattingViewModel>(await response.Content.ReadAsStringAsync());
        if (vm == null)
        {
            return (false, "接口异常");
        }


        if (vm.Data == null)
        {
            return (false, vm.Msg);
        }

        var transation = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            var result = await _packagesResourceService.ConsumptionMatting(1);
            if (!result.State)
            {
                throw new Exception(result.Msg);
            }

            await transation.CommitAsync();
            return (true, vm.Data.ImageBase64);
        }
        catch (Exception e)
        {
            await transation.RollbackAsync();
            _logger.LogError(e, e.Message);
            return (false, e.Message);
        }
    }

    public async Task<(bool State, string Msg)> PaddingCode(ModelProduct itemStruct, UploadProductVm products,
        EanupcModel.TypeEnum type)
    {
        var variantNum = itemStruct.Variants.Count();
        // var codes = await _dbContext.Eanupc
        //     // .Where(m => m.CompanyID == _companyId).Skip(variantNum)
        //     .Where(m => m.Type == type)
        //     .Where(m => m.CompanyID == _companyId).Skip(0).Take(variantNum + 1)
        //     .ToListAsync();
        var codes = await _eanupcService.GetFixedQuantityCodes(type, variantNum + 1,
            await GetUserRange(Enums.Rule.Config.Store.EanUpcRange));
        if (codes.Count != variantNum + 1)
        {
            return (false, $"产品码数量不足");
        }

        //填充Code
        var variants = itemStruct.Variants;
        for (int i = 0; i <= variants.Count; i++)
        {
            if (i == variants.Count)
            {
                //主产品
                products.Code = codes[i].Value;
            }
            else
            {
                variants[i].Code = codes[i].Value;
            }
        }

        return (true, "");
    }

    public async Task<(string Msg, UploadProductVm? Products)> GetInfoWithUpload(GetInfoWithUploadDto req)
    {
        //获取产品信息
        var productInfo = await GetInfoById(req.Pid);

        if (productInfo == null)
        {
            return ("找不到产品相关数据", null);
        }

        //审核
        if (productInfo.Audit != ProductModel.AUDIT_AUDITED && (req.IsGenerateSku || req.IsPaddingCode))
        {
            return ("产品未审核通过", null);
        }

        //语言信息
        var itemInfo = await GetItemInfoByLangId(req.Pid, req.LanguageId);
        if (itemInfo == null)
        {
            return ($"当前产品不存在【{req.Language.GetDescription()}】语种,跳过", null);
        }

        var itemStruct = await GetProductStruct(itemInfo.FilePath, productInfo);
        // var variantNum = itemStruct.Variants.Count();


        //code +1主产品
        // var codes = await _dbContext.Eanupc
        //     // .Where(m => m.CompanyID == _companyId).Skip(variantNum)
        //     .Where(m => m.CompanyID == _companyId).Skip(0).Take(variantNum + 1)
        //     .ToListAsync();
        // if (codes.Count != variantNum + 1)
        // {
        //     return ($"产品码数量不足", null);
        // }

        var products = new UploadProductVm(itemStruct);
        products.Main = itemStruct.MainImage != null
            ? _convertProduct.GetImageUrl(itemStruct.MainImage, _oemProvider)
            : string.Empty;
        products.Affiliate = productInfo.Gallery != null
            ? _convertProduct.GetImageUrl(productInfo.Gallery, _oemProvider)
            : new List<string>();

        if (req.IsPaddingCode)
        {
            //填充Code
            var paddingCodeRes = await PaddingCode(itemStruct, products, req.CodeType);
            if (!paddingCodeRes.State)
            {
                return (paddingCodeRes.Msg, null);
            }
        }

        var variants = itemStruct.Variants;
        //填充Code
        // for (int i = 0; i <= variants.Count; i++)
        // {
        //     if (i == variants.Count)
        //     {
        //         //主产品
        //         products.Code = codes[i].Value;
        //     }
        //     else
        //     {
        //         variants[i].Code = codes[i].Value;
        //     }
        // }

        //sku
        if (req.IsGenerateSku)
        {
            products.Sku = !string.IsNullOrEmpty(productInfo.Sku) ? productInfo.Sku! : Helpers.SkuCode();
        }
        else
        {
            products.Sku = !string.IsNullOrEmpty(productInfo.Sku) ? productInfo.Sku! : string.Empty;
        }

        //price
        Money? cost = null;
        Money? sale = null;
        //数据拼接
        var data = new List<UploadProductItemVm>();
        for (int i = 0; i < variants.Count; i++)
        {
            var item = variants[i];
            var v = new UploadProductItemVm(item, itemStruct);
            v.Cost = item.Cost.To(req.Unit);
            v.Sale = item.Cost.To(req.Unit);

            //主产品价格获取第一个变体的价格
            if (cost == null)
            {
                cost = v.Cost;
            }

            if (sale == null)
            {
                sale = v.Cost;
            }

            v._Attribute = _convertProduct.GetAttributeNameWithUpload(itemStruct.Variants.Attributes.items, item.Ids)
                .OrderByDescending(p => p.Key.ToLower() == "color")
                .ThenByDescending(p => p.Key.ToLower() == "size")
                .ToDictionary(m => m.Key, m => m.Value);

            if (productInfo.Gallery != null)
            {
                v.Main = item.Images.Main.HasValue
                    ? _convertProduct.GetImageUrl(item.Images.Main.Value, productInfo.Gallery, _oemProvider)
                    : string.Empty;
                v.Affiliate = item.Images.Affiliate != null
                    ? _convertProduct.GetImageUrl(item.Images.Affiliate, productInfo.Gallery, _oemProvider)
                    : new List<string>();
            }

            if (req.IsGenerateSku)
            {
                v.Sku = !string.IsNullOrEmpty(item.Sku) ? item.Sku! : $"{products.Sku}-{i}";
            }
            else
            {
                v.Sku = !string.IsNullOrEmpty(item.Sku) ? item.Sku! : string.Empty;
            }

            data.Add(v);
        }

        products.Cost = cost!.Value;
        products.Sale = sale!.Value;
        products.Products = data;
        return ("", products);
    }


    public async Task<CopyProductViewModel> UploadClone(int proId, Languages lang, UploadProductVm uploadPro)
    {
        var langId = lang != Languages.Default ? (int)Math.Log2((long)lang) + 1 : 0;
        var (product, itemModel) = await CloneInt(proId, langId);
        var model = new ProductModel(_sessionProvider.Session, product);
        model.SetUpload(true);
        return await HandleCloneWithUpload(itemModel, model, product, uploadPro);
    }

    private async Task<CopyProductViewModel> HandleCloneWithUpload(ProductItemModel itemModel, ProductModel model,
        ProductModel product, UploadProductVm uploadPro)
    {
        model.Language = (long)Types.Default;
        try
        {
            //处理图片
            var (imageComparison, copySuccessImages, allTempImages) =
                await BeforeCloneProWithImages(product.Gallery, product);

            model.Images = JsonConvert.SerializeObject(copySuccessImages);
            await _dbContext.Product.AddAsync(model);
            await _dbContext.SaveChangesAsync();

            var updatedAt = DateTime.Now;
            var version = updatedAt.GetTimeStampMilliseconds();

            var newItemModel =
                await BeforeUploadCloneWithItem(itemModel, product, model, imageComparison, version, uploadPro);
            model.Sku = uploadPro.Sku;
            model.FilesSize = newItemModel.FilesSize;
            model.UpdatedAt = updatedAt;
            model.MainImage = copySuccessImages.Count > 0
                ? JsonConvert.SerializeObject(copySuccessImages.First())
                : null;
            model.Title = newItemModel.Title;
            model.Quantity = newItemModel.Quantity;

            await _dbContext.ProductItem.AddAsync(newItemModel);
            await _dbContext.Product.SingleUpdateAsync(model);
            await AfterCloneMoveImage(allTempImages, model);
            await _dbContext.SaveChangesAsync();
            return new CopyProductViewModel()
            {
                SourceId = product.ID,
                SourceItemId = itemModel.ID,
                Product = model,
                ProductItem = newItemModel
            };
        }
        catch (Exception e)
        {
            _logger.LogError(e, e.Message);
            throw new SavedException(e.Message);
        }
    }

    public async Task<ProductItemModel> BeforeUploadCloneWithItem(ProductItemModel oldItemModel,
        ProductModel oldProduct,
        ProductModel newProduct,
        Dictionary<int, int> imageComparison,
        long version,
        UploadProductVm uploadProductVm)
    {
        var newItemModel = new ProductItemModel(oldItemModel, _sessionProvider.Session, true);

        var currentVersionJsonFilepath = oldItemModel.FilePath;

        var productStruct = await GetProductStruct(currentVersionJsonFilepath, oldProduct);

        //basic
        productStruct.SetSku(uploadProductVm.Sku);
        productStruct.SetCode(uploadProductVm.Code);

        var variants = productStruct.Variants;
        var newVariants = new Variants();
        newVariants.Attributes = productStruct.Variants.Attributes;

        if (variants.Count != uploadProductVm.Products.Count)
        {
            throw new Exception("变体数据异常#");
        }

        for (int i = 0; i < variants.Count; i++)
        {
            var item = variants[i];
            //处理主图
            if (item.Images.Main.HasValue)
            {
                item.Images.Main = imageComparison.ContainsKey((int)item.Images.Main)
                    ? imageComparison[(int)item.Images.Main]
                    : null;
            }

            if (item.Images.Affiliate != null && item.Images.Affiliate.Count > 0)
            {
                var affiliate = new List<int>();
                var oldAffiliate = item.Images.Affiliate;
                foreach (int k in oldAffiliate)
                {
                    if (imageComparison.ContainsKey(k))
                    {
                        affiliate.Add(imageComparison[k]);
                    }
                }

                item.Images.Affiliate = affiliate;
            }

            item.Code = uploadProductVm.Products[i].Code!;
            item.Sku = uploadProductVm.Products[i].Sku!;
            newVariants.Add(item);
        }

        productStruct.Properties[PropertyKey.SourceId].Value = oldProduct.ID;
        productStruct.Properties[PropertyKey.SourceItemId].Value = oldItemModel.ID;
        productStruct.Variants = newVariants;
        productStruct.SetCode(uploadProductVm.Code);
        newItemModel.CurrentVersion = version;
        newItemModel.HistoryVersion = JsonConvert.SerializeObject(new List<long>() { oldItemModel.CurrentVersion });
        newItemModel.Pid = newProduct.ID;
        newItemModel.Title = productStruct.GetTitle()!;
        await InitItemVersion(productStruct, newProduct, newItemModel, ++version);

        return newItemModel;
    }

    public async Task<ModelProduct> GetTranslateSource(int pid, int? langId)
    {
        var product = await _dbContext.Product
            .FirstOrDefaultAsync(m => m.ID == pid && m.CompanyID == _companyId && m.OEMID == _oemId);

        if (product is null)
        {
            throw new NotFoundException("找不到产品相关数据#1");
        }

        if (product.Audit != ProductModel.AUDIT_AUDITED)
        {
            throw new GeneralException("产品未审核通过#2");
        }

        var languageId = langId;
        if (!langId.HasValue)
        {
            languageId = product.UiLanguageId;
        }


        var itemInfo = await _dbContext.ProductItem
            .FirstOrDefaultAsync(m => m.LanguageId == languageId && m.Pid == product.ID);

        if (itemInfo is null)
        {
            throw new NotFoundException("找不到产品相关数据#3");
        }

        return await GetProductStruct(itemInfo.FilePath, product);
    }

    public async Task<(bool State, string Message)> NewTranslate(SubDto req)
    {
        var pid = req.ID;
        var product = await GetInfoByIdWithUser(pid);

        if (product is null)
        {
            throw new NotFoundException("找不到产品相关数据#1");
        }

        if (product.Audit != ProductModel.AUDIT_AUDITED)
        {
            throw new GeneralException("产品未审核通过#2");
        }

        var itemInfo = await GetItemInfoByLangId(product.ID, product.UiLanguageId);

        if (itemInfo is null)
        {
            throw new NotFoundException("找不到产品相关数据#3");
        }

        var itemStruct = await GetProductStruct(itemInfo.FilePath, product);
        var vm = new XfTranVm(itemStruct, product.ID, req.Attribute);

        var jsononnectStr = "|";

        //是否执行翻译
        var isTran = false;
        //是否全部翻译完成
        var isAllTran = false;
        //错误信息
        var successMsg = "";

        var tranLength = 0;
        try
        {
            var form = req.Form.GetTranslationCode(req.Form.GetDescriptionByKey("Code"));
            var to = req.To.GetTranslationCode(req.To.GetDescriptionByKey("Code"));
            //标题
            var tranVm = await _translationService.Send(form, to, vm.Name);
            itemStruct.SetTitle(tranVm.TransResult.Dst);
            tranLength += vm.Name.Length;
            isTran = true;
            successMsg += "标题";
            //关键词
            if (!string.IsNullOrEmpty(vm.Keywords))
            {
                tranVm = await _translationService.Send(form, to, vm.Keywords);
                itemStruct.SetKeyword(tranVm.TransResult.Dst);
                tranLength += vm.Keywords.Length;
                successMsg += "，关键词";
            }

            if (vm.Sketch != null && !string.IsNullOrEmpty(vm.Sketch[0]))
            {
                var sketchStr = vm.GetSketchStr(jsononnectStr);
                tranVm = await _translationService.Send(form, to, sketchStr!);
                itemStruct.SetSketches(tranVm.TransResult.Dst
                    .Split(jsononnectStr, StringSplitOptions.RemoveEmptyEntries).ToList());
                tranLength += sketchStr!.Length;
                successMsg += "，卖点";
            }

            if (!string.IsNullOrEmpty(vm.Desc))
            {
                tranVm = await _translationService.Send(form, to, vm.Desc!);
                itemStruct.SetDescription(tranVm.TransResult.Dst);
                tranLength += vm.Desc.Length;
                successMsg += "，描述";
            }

            if (req.Variant)
            {
                var variants = itemStruct.Variants;
                foreach (var item in variants)
                {
                    if (item.GetTitle() != null)
                    {
                        item.SetTitle(itemStruct.GetTitle()!);
                        item.SetSketches(itemStruct.GetSketches());
                        item.SetKeyword(itemStruct.GetKeyword());
                        item.SetDescription(itemStruct.GetDescription());
                    }
                }
            }

            if (req.Attribute)
            {
                var attributeStr = vm.GetAttributesStr(jsononnectStr);
                var attrCount = vm.Attributes.Count;
                tranVm = await _translationService.Send(form, to, attributeStr!);
                var attrDst = tranVm.TransResult.Dst.Split(jsononnectStr, StringSplitOptions.RemoveEmptyEntries)
                    .ToList();

                if (attrDst.Count == vm.Attributes.Count)
                {
                    var map = new Dictionary<string, string>();
                    for (int i = 0; i < attrDst.Count; i++)
                    {
                        var oldVal = vm.Attributes[i];
                        var newVal = attrDst[i];
                        if (oldVal != newVal)
                        {
                            map.Add(oldVal, newVal);
                        }
                    }

                    itemStruct.Variants.Attributes.items =
                        _convertProduct.ReplaceAttributes(itemStruct.Variants.Attributes.items, map);
                    tranLength += attributeStr.Length;
                    successMsg += "，属性";
                }
                else
                {
                    throw new GeneralException("产品属性翻译出错#7");
                }
            }
        }
        catch (Exception e)
        {
            _logger.LogError(e, e.Message);
            if (!isTran)
            {
                return (false, "翻译失败: " + e.Message);
            }
        }

        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            var now = DateTime.Now;
            ProductItemModel? newItemModel;
            newItemModel = await GetItemInfoByLangId(product.ID, req.ToId);
            if (newItemModel == null)
            {
                newItemModel = new ProductItemModel(itemInfo, _sessionProvider.Session, true);
                //设置语言
                newItemModel.CurrentVersion = now.GetTimeStampMilliseconds();
                newItemModel.HistoryVersion =
                    JsonConvert.SerializeObject(new List<long>() { newItemModel.CurrentVersion });
                newItemModel.Pid = product.ID;
                newItemModel.LanguageId = req.ToId;
                newItemModel.LanguageName = req.To.GetDescriptionByKey("Code");
                await InitItemVersion(itemStruct, product, newItemModel, newItemModel.CurrentVersion);
                product.SetLanguage(req.To, true);
                await _dbContext.ProductItem.AddAsync(newItemModel);
                product.FilesSize += newItemModel.FilesSize;
                product.UpdatedAt = now;
            }
            else
            {
                await UpdateProductStructWithNewVersion(itemStruct, product, newItemModel);
                await _dbContext.ProductItem.SingleUpdateAsync(newItemModel);
            }

            // 
            await _dbContext.Product.SingleUpdateAsync(product);
            //消费字符数
            var result = await _packagesResourceService.ConsumptionTranslation(tranLength);
            if (!result.State)
            {
                throw new Exception(result.Msg);
            }

            await _dbContext.SaveChangesAsync();
            await transaction.CommitAsync();
            if (isAllTran)
            {
                return (true, "翻译成功");
            }
            else
            {
                return (false, $"部分内容翻译成功，其中包括{successMsg}");
            }
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            _logger.LogError(e, e.Message);
            return (false, e.Message);
        }
    }

    public async Task<(bool State, string Message)> Translate(SubDto req)
    {
        var pid = req.ID;
        var product = await GetInfoByIdWithUser(pid);

        if (product is null)
        {
            throw new NotFoundException("找不到产品相关数据#1");
        }

        if (product.Audit != ProductModel.AUDIT_AUDITED)
        {
            throw new GeneralException("产品未审核通过#2");
        }

        var itemInfo = await GetItemInfoByLangId(product.ID, product.UiLanguageId);

        if (itemInfo is null)
        {
            throw new NotFoundException("找不到产品相关数据#3");
        }

        var itemStruct = await GetProductStruct(itemInfo.FilePath, product);

        var vm = new XfTranVm(itemStruct, product.ID, req.Attribute);
        var connectStr = "258"; //,
        var jsononnectStr = "852"; // /
        var pTagStr = "147"; //<p>
        var pCloseTagStr = "741"; //</p>
        var brTagStr = "369"; //<br>
        var brCloseTagStr = "963"; //<br/>
        var tNum = vm.Name.Split(connectStr, StringSplitOptions.RemoveEmptyEntries).Length;
        var sketchStr = vm.GetSketchStr(jsononnectStr);
        var sNum = sketchStr?.Split(connectStr, StringSplitOptions.RemoveEmptyEntries).Length;
        var kNum = vm.Keywords?.Split(connectStr, StringSplitOptions.RemoveEmptyEntries).Length;
        var dNum = vm.Desc?.Split(connectStr, StringSplitOptions.RemoveEmptyEntries).Length;
        var attributeStr = vm.GetAttributesStr(jsononnectStr);
        var aNum = attributeStr.Split(connectStr, StringSplitOptions.RemoveEmptyEntries).Length;
        var text = $"{vm.Name}{connectStr}";
        if (sketchStr != null)
            text += $"{sketchStr}{connectStr}";
        if (vm.Keywords != null)
            text += $"{vm.Keywords}{connectStr}";
        if (vm.Desc != null)
        {
            var desc = vm.Desc
                .Replace("<br>", brTagStr)
                .Replace("<br/>", brCloseTagStr)
                .Replace("<br />", brCloseTagStr)
                .Replace("<p>", pTagStr)
                .Replace("</p>", pCloseTagStr);
            text += $"{desc}{connectStr}";
        }

        if (req.Attribute)
            text += $"{attributeStr}{connectStr}";

        if (text.Length > 5000)
        {
            return (false, "文字过长#4");
        }

        var transaction = await _dbContext.Database.BeginTransactionAsync();

        try
        {
            var form = req.Form.GetTranslationCode(req.Form.GetDescriptionByKey("Code"));
            var to = req.To.GetTranslationCode(req.To.GetDescriptionByKey("Code"));
            var date = DateTime.UtcNow.GetDateTimeFormats('r')[0];
            var tranVm = await _translationService.Send(form, to, text);
            var dstArr = tranVm.TransResult.Dst
                .Replace("”", "\"")
                .Replace("“", "\"")
                .Replace("，", ",")
                .Replace("& nbsp;", "&nbsp;")
                .Replace("；", ";")
                .Split(connectStr, StringSplitOptions.RemoveEmptyEntries);

            //判断翻译返回长度
            var srcArr = text.Split(connectStr, StringSplitOptions.RemoveEmptyEntries);
            if (srcArr.Length != dstArr.Length)
            {
                return (false, "翻译文本缺失#6");
            }

            var starIndex = 0;
            itemStruct.SetTitle(string.Join(connectStr, dstArr.Skip(0).Take(tNum)));
            starIndex += tNum;
            if (vm.SketchStr != null)
            {
                var sketch = dstArr.Skip(starIndex).Take(sNum!.Value).ToList();
                var str = string.Join("", sketch);
                itemStruct.SetSketches(str.Split(jsononnectStr, StringSplitOptions.RemoveEmptyEntries).ToList());
                starIndex += sNum.Value;
            }

            if (vm.Keywords != null)
            {
                itemStruct.SetKeyword(string.Join(connectStr, dstArr.Skip(starIndex).Take(kNum!.Value)));
                starIndex += kNum.Value;
            }

            if (vm.Desc != null)
            {
                var tempDesc = string.Join(connectStr, dstArr.Skip(starIndex).Take(dNum!.Value));
                var descStr = tempDesc.Replace(brTagStr, "<br>")
                    .Replace(brCloseTagStr, "<br />")
                    .Replace(pTagStr, "<p>")
                    .Replace(pCloseTagStr, "</p>");
                itemStruct.SetDescription(descStr);
                starIndex += dNum.Value;
            }

            //填充变体
            if (req.Variant)
            {
                var variants = itemStruct.Variants;
                foreach (var item in variants)
                {
                    if (item.GetTitle() != null)
                    {
                        item.SetTitle(itemStruct.GetTitle()!);
                        item.SetSketches(itemStruct.GetSketches());
                        item.SetKeyword(itemStruct.GetKeyword());
                        item.SetDescription(itemStruct.GetDescription());
                    }
                }
            }

            //翻译属性
            if (req.Attribute)
            {
                var attrs = dstArr.Skip(starIndex).Take(aNum).ToList();
                var str = string.Join("", attrs);
                var attributes = str.Split(jsononnectStr, StringSplitOptions.RemoveEmptyEntries).ToList();
                if (attributes != null && attributes.Count == vm.Attributes.Count)
                {
                    var map = new Dictionary<string, string>();
                    for (int i = 0; i < attributes.Count; i++)
                    {
                        var oldVal = vm.Attributes[i];
                        var newVal = attributes[i];
                        if (oldVal != newVal)
                        {
                            map.Add(oldVal, newVal);
                        }
                    }

                    itemStruct.Variants.Attributes.items =
                        _convertProduct.ReplaceAttributes(itemStruct.Variants.Attributes.items, map);
                }
                else
                {
                    throw new GeneralException("产品属性翻译出错#7");
                }
            }

            var now = DateTime.Now;
            ProductItemModel? newItemModel;
            newItemModel = await GetItemInfoByLangId(product.ID, req.ToId);
            if (newItemModel == null)
            {
                newItemModel = new ProductItemModel(itemInfo, _sessionProvider.Session, true);
                //设置语言
                newItemModel.CurrentVersion = now.GetTimeStampMilliseconds();
                newItemModel.HistoryVersion =
                    JsonConvert.SerializeObject(new List<long>() { newItemModel.CurrentVersion });
                newItemModel.Pid = product.ID;
                newItemModel.LanguageId = req.ToId;
                newItemModel.LanguageName = req.To.GetDescriptionByKey("Code");
                await InitItemVersion(itemStruct, product, newItemModel, newItemModel.CurrentVersion);
                product.SetLanguage(req.To, true);
                await _dbContext.ProductItem.AddAsync(newItemModel);
                product.FilesSize += newItemModel.FilesSize;
                product.UpdatedAt = now;
            }
            else
            {
                await UpdateProductStructWithNewVersion(itemStruct, product, newItemModel);
                await _dbContext.ProductItem.SingleUpdateAsync(newItemModel);
            }

            // 
            await _dbContext.Product.SingleUpdateAsync(product);
            //消费字符数
            var result = await _packagesResourceService.ConsumptionTranslation(text.Length);
            if (!result.State)
            {
                throw new Exception(result.Msg);
            }

            await _dbContext.SaveChangesAsync();
            await transaction.CommitAsync();
            return (true, "翻译成功");
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            _logger.LogError(e, e.Message);
            return (false, e.Message);
        }
    }

    public async Task<object> TranslateInit()
    {
        var languages = _languagesCache.List().ToDictionary(m => m.Code, m => m);
        var tranLang = Enum.GetValues<Languages>().Where(l =>
                typeof(Languages).GetMember(l.ToString())[0].GetCustomAttribute<TranslationAttribute>() != null)
            .Select(m => new TranslationCodeVm(m))
            .ToList();
        return new Dictionary<string, object>()
        {
            { "languages", languages },
            { "tranLang", tranLang },
        };
    }

    /// <summary>
    /// 修改上传数据
    /// </summary>
    /// <param name="pid"></param>
    /// <param name="result"></param>
    /// <returns></returns>
    public async Task<bool> UpdateModel(int pid, UploadResult result = UploadResult.NeedToUpload)
    {
        var model = _dbContext.productUpload.FirstOrDefault(x => x.Product.ID == pid);
        if (model != null)
        {
            model.Result = result;
            await _dbContext.productUpload.SingleUpdateAsync(model);
            return true;
        }

        return false;
    }

    public async Task UpdateSkuInfo(ProductModel model, Dictionary<string, SkuUploadInfoVm> vm)
    {
        var itemModel = await GetItemInfoByLangId(model.ID, model.UiLanguageId);
        if (itemModel is null)
            throw new NotFoundException("找不到产品相关数据#2");

        await OpUpdateSkuInfo(model, itemModel, vm);
    }

    public async Task UpdateSkuInfo(int id, Dictionary<string, SkuUploadInfoVm> vm)
    {
        var model = await GetInfoById(id);
        if (model == null)
            throw new NotFoundException("找不到产品相关数据#1");

        var itemModel = await GetItemInfoByLangId(model.ID, model.UiLanguageId);
        if (itemModel is null)
            throw new NotFoundException("找不到产品相关数据#2");

        await OpUpdateSkuInfo(model, itemModel, vm);
    }

    private async Task OpUpdateSkuInfo(ProductModel model, ProductItemModel itemModel,
        Dictionary<string, SkuUploadInfoVm> vm)
    {
        var productStruct = await GetProductStruct(itemModel.FilePath, model);

        if (model.Sku != productStruct.GetSku())
            throw new Exception("主产品Sku异常");

        if (String.IsNullOrEmpty(productStruct.GetSku()) || !vm.Keys.Contains(productStruct.GetSku()))
            throw new Exception("缺少主产品Sku");

        productStruct.UploadState = vm[productStruct.GetSku()!].UpdateState;

        var variants = productStruct.Variants;

        if (vm.Count != (variants.Count + 1))
        {
            throw new Exception("数量不符");
        }


        foreach (var item in variants)
        {
            if (String.IsNullOrEmpty(item.GetSku()) || !vm.Keys.Contains(item.GetSku()))
                throw new Exception("缺少变体Sku");
            item.UploadState = vm[item.GetSku()!].UpdateState;
        }

        productStruct.Variants = variants;

        var jsonProduct = JsonConvert.SerializeObject(productStruct, new JsonSerializerSettings()
        {
            TypeNameHandling = TypeNameHandling.All
        });
        var uploadState = await _uploadService.PutAsync(itemModel.FilePath, jsonProduct);

        if (!uploadState)
            throw new SavedException("数据包存储失败");
        var now = DateTime.Now;
        itemModel.FilesSize = itemModel.FilesSize + (jsonProduct.Length - itemModel.FilesSize);
        itemModel.UpdatedAt = now;
        model.UpdatedAt = now;

        await _dbContext.Product.SingleUpdateAsync(model);
        await _dbContext.ProductItem.SingleUpdateAsync(itemModel);
    }


    #region 获取SkuInfo

    /// <summary>
    /// 获取SkuInfo
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    /// <exception cref="NotFoundException"></exception>
    public async Task<Dictionary<string, SkuUploadInfoVm>> GetSkuWithUpload(int id)
    {
        var model = await GetInfoById(id);
        if (model == null)
            throw new NotFoundException("找不到产品相关数据#1");

        var itemModel = await GetItemInfoByLangId(model.ID, model.UiLanguageId);
        if (itemModel is null)
            throw new NotFoundException("找不到产品相关数据#2");

        return await SkuWithUpload(model, itemModel);
    }

    /// <summary>
    /// 获取SkuInfo
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    /// <exception cref="NotFoundException"></exception>
    public async Task<Dictionary<string, SkuUploadInfoVm>> GetSkuWithUpload(ProductModel model)
    {
        var itemModel = await GetItemInfoByLangId(model.ID, model.UiLanguageId);
        if (itemModel is null)
            throw new NotFoundException("找不到产品相关数据#2");

        return await SkuWithUpload(model, itemModel);
    }

    /// <summary>
    /// 获取SkuInfo
    /// </summary>
    /// <param name="model"></param>
    /// <param name="itemModel"></param>
    /// <returns></returns>
    public async Task<Dictionary<string, SkuUploadInfoVm>> GetSkuWithUpload(ProductModel model,
        ProductItemModel itemModel) => await SkuWithUpload(model, itemModel);


    /// <summary>
    /// 获取SkuInfo
    /// </summary>
    /// <param name="model"></param>
    /// <param name="itemModel"></param>
    /// <returns></returns>
    /// <exception cref="NotFoundException"></exception>
    private async Task<Dictionary<string, SkuUploadInfoVm>> SkuWithUpload(ProductModel model,
        ProductItemModel itemModel)
    {
        var result = new Dictionary<string, SkuUploadInfoVm>();

        var productStruct = await GetProductStruct(itemModel.FilePath, model);

        if (string.IsNullOrEmpty(productStruct.Sku))
            throw new NotFoundException("主产品Sku为空#3");

        result.Add(productStruct.Sku, new SkuUploadInfoVm(productStruct));

        var variants = productStruct.Variants;

        foreach (var item in variants)
        {
            if (string.IsNullOrEmpty(item.Sku))
                throw new NotFoundException("变体Sku为空#3");

            result.Add(item.Sku, new SkuUploadInfoVm(item));
        }

        return result;
    }

    #endregion

    public async Task<DbSetExtension.PaginateStruct<ListViewModel>?> List(ListDto req, DataRange_2b range)
    {
        var model = ProductModelQuery(req, range);
        if (model == null)
            return null;

        var paginate = await model
            // .AsEnumerable()
            // .Select(m => new ListViewModel(m, GetTranslated(m), _session.GetUnitConfig()))
            .OrderByDescending(m => m.ID)
            .Select(m => new ListViewModel(m))
            .ToPaginateAsync(page: req.Page, limit: req.Limit);

        foreach (var item in paginate.Items)
        {
            item.SetLanguagePack(GetTranslated(item.Language));
            item.SetPrices(_session.GetUnitConfig(), _session.GetProductShowUnit());
        }

        return paginate;
    }
    

    public IQueryable<ProductModel>? ProductModelQuery(ListDto req, DataRange_2b range)
    {
        var model = ProductDataWatchRange(range, req.Action != ActionEnum.Share, req.Action == ActionEnum.Share);

        if (model == null)
            return null;


        //排除上传中/导出/B2C状态的数据
        model = model.Where(m => (m.Type & (long)Types.OnGoing) != (long)Types.OnGoing);
        model = model.Where(m => (m.Type & (long)Types.Export) != (long)Types.Export);
        model = model.Where(m => (m.Type & (long)Types.B2C) != (long)Types.B2C);
        model = model.Where(m => (m.Type & (long)Types.Uplaod) != (long)Types.Uplaod);


        //audit
        var audited = (long)ProductModel.AUDIT_AUDITED << (int)Types.Audit;
        var typeAudit = Types.Audit;
        var auditMask = Types.MASK_AUDIT;

        switch (req.AuditState)
        {
            case AuditStateEnum.NotReviewed:
                var a =
                    model = model.Where(m => ((m.Type & (long)auditMask)) < audited);
                break;
            case AuditStateEnum.Audited:
                model = model.Where(m => ((m.Type & (long)auditMask)) >= audited);
                break;
            case AuditStateEnum.DidNotPass:
                model = model.Where(m => ((m.Type & (long)auditMask)) > audited);
                break;
        }

        //tabs
        //非回收站时应排除回收站
        if (req.Action != ActionEnum.Recycle)
            model = model.Where(m => (m.Type & (long)Types.Recycle) != (long)Types.Recycle);

        switch (req.Action)
        {
            case ActionEnum.Collect:
                model = model.Where(m => (m.Type & (long)Types.Collect) == (long)Types.Collect);
                break;
            case ActionEnum.Manual:
                model = model.Where(m => (m.Type & (long)Types.Collect) != (long)Types.Collect);
                break;
            case ActionEnum.Audit:
                model = model.Where(m => ((m.Type & (long)auditMask)) < audited);
                break;
            case ActionEnum.Share:
                model = model.Where(m => (m.Type & (long)Types.Share) == (long)Types.Share);
                break;
            case ActionEnum.Recycle:
                model = model.Where(m => (m.Type & (long)Types.Recycle) == (long)Types.Recycle);
                break;
        }

        if (req.Action == ActionEnum.Share)
        {
            model = model.Where(m => m.CompanyID == _companyId);
            if (req.Share.HasValue)
            {
                if (req.Share.Value == -1)
                {
                    model = model.Where(m => m.UserID != _userId);
                }
                else
                {
                    model = model.Where(m => m.UserID == _userId);
                }
            }
        }
        
        if (req.Release.HasValue)
        {
            if (req.Release.Value == ReleaseEnum.Release)
            {
                model = model.IsB2cRelease(true);
            }
            else
            {
                model = model.IsB2cRelease(false);
            }
        }
        
        


        model = model
            .WhenWhere(!req.Pid.IsNullOrEmpty(), m => m.Pid == req.Pid)
            .WhenWhere(req.UserId.HasValue, m => m.UserID == req.UserId)
            .WhenWhere(req.TaskId.HasValue, m => m.TaskId == req.TaskId)
            .WhenWhere(req.CategoryId.HasValue, m => m.CategoryId == req.CategoryId)
            .WhenWhere(req.SubCategoryId.HasValue, m => m.SubCategoryId == req.SubCategoryId)
            .WhenWhere(req.PlatformId.HasValue, m => m.PlatformId == req.PlatformId)
            .WhenWhere(req.Title?.Length > 0 && req.Multi == MultiEnum.Fuzzy, m => m.Title.Contains(req.Title))
            .WhenWhere(req.Title?.Length > 0 && req.Multi == MultiEnum.Precise, m => m.Title == req.Title)
            .WhenWhere(req.UpdatedTime != null && req.UpdatedTime.Count == 2,
                a => a.UpdatedAt >= req.UpdatedTime![0] && a.UpdatedAt < req.UpdatedTime[1].LastDayTick());

        if (req.IsRepeat)
        {
            model = model.Where(m => m.UserLevelRepeatNum > 0)
                .OrderByDescending(m => m.UserLevelRepeatNum)
                .ThenByDescending(m => m.UpdatedAt);
        }

        return model;
    }

    protected internal IQueryable<ProductModel>? ProductDataWatchRange(DataRange_2b range, bool isNeedCate,
        bool isShare)
    {
        if (range == DataRange_2b.None)
            return null;

        var model = _dbContext.Product.Where(m => m.OEMID == _oemId && m.CompanyID == _companyId);

        if (isNeedCate)
            model = (IQueryable<ProductModel>)model
                .Include(m => m.CategoryModel)
                .Include(m => m.SubCategoryModel);

        if (!isShare)
        {
            if (range == DataRange_2b.Group)
                model = model.Where(m => m.GroupID == _groupId);

            if (range == DataRange_2b.User)
                model = model.Where(m => m.UserID == (_userId));
        }

        // else
        // {
        //     model = model.Where(m =>
        //         m.OEMID == _oemId
        //         && m.CompanyID == _companyId
        //         && m.UserID == _userId);
        // }

        return model;
    }

    /// <summary>
    /// 分配ean/upc
    /// </summary>
    /// <param name="productList">待分配的产品列表</param>
    /// <param name="mode">类型</param>
    /// <param name="cover">是否强制覆盖</param>
    /// <returns></returns>
    /// <exception cref="NotFoundException"></exception>
    /// <exception cref="SavedException"></exception>
    /// <exception cref="EanUpcNotEnoughException"></exception>
    public async Task DistributeEanUpc(Dictionary<ProductModel, int> productList, AllotExportProductDto.ModeEnum mode,
        bool cover)
    {
        if (productList.Count <= 0)
        {
            throw new NotFoundException("找不到产品相关数据#1");
        }

        var isNeedCode = 0;
        var itemModelList = new List<ProductItemModel>();
        var itemStructList = new Dictionary<ProductItemModel, ModelProduct>();
        var dicProductList = productList.ToDictionary(m => m.Key.ID, m => m.Key);
        foreach (var (product, langId) in productList)
        {
            var itemModel = await GetItemInfoByLangId(product.ID, langId) ?? await GetItemInfoByLangId(product.ID, 0);
            if (itemModel is null)
            {
                throw new NotFoundException("找不到产品相关数据#2");
            }

            var itemStruct = await GetProductStruct(itemModel.FilePath, product);

            var variants = itemStruct.Variants;

            if (variants.Count <= 0)
            {
                throw new NotFoundException($"当前产品变体数据存在异常ProId[{product.ID}],ItemId[{itemModel.ID}]");
            }

            //sid 老版本兼容
            foreach (var variant in variants)
            {
                // if (!string.IsNullOrEmpty(variant.Sid))
                // if(DateTime.Compare(itemModel.UpdatedAt,Convert.ToDateTime("23/08/02")) > 0)
                if (itemStruct.Version != itemStruct.CurrentVersion)
                {
                    variant.Sid = Helpers
                        .CreateHashCode(_convertProduct.GetVariantName(itemStruct.Variants.Attributes, variant.Ids))
                        .ToString();
                }
            }


            var need = variants.Count(m => string.IsNullOrWhiteSpace(m.Code) || m.Code == "0");
            // //默认变体数量，然后对比对照表中和实际变体
            // var need = variants.Count;
            // //对照表
            // var codecompare = itemStruct.CodeCompare;
            // if (codecompare!= null)
            // {
            //     need = variants.Count(m =>
            //         !codecompare.Keys.Contains(m.Sid!)
            //         || (codecompare.Keys.Contains(m.Sid!) && !string.IsNullOrEmpty(codecompare[m.Sid!]))
            //     );
            // }
            if (!cover && need == 0)
            {
                // throw new NotFoundException($"当前产品{mode.GetName()}已分配");
                throw new NotFoundException($"当前产品已分配");
            }

            if (cover)
            {
                need = variants.Count;
            }

            isNeedCode += need;

            itemModelList.Add(itemModel);
            itemStructList.Add(itemModel, itemStruct);
        }

        // var codes = await _dbContext.Eanupc
        //     .Where(m => m.OEMID == _oemId && m.CompanyID == _companyId)
        //     .Where(m => m.Type == Enum.Parse<EanupcModel.TypeEnum>(mode.GetName(), true))
        //     .Take(isNeedCode)
        //     .ToListAsync();

        var codes = await _eanupcService.GetFixedQuantityCodes(Enum.Parse<EanupcModel.TypeEnum>(mode.GetName(), true),
            isNeedCode, await GetUserRange(Enums.Rule.Config.Store.EanUpcRange));
        if (codes.Count < isNeedCode)
        {
            throw new EanUpcNotEnoughException(mode.GetName());
        }

        var codeIds = new List<int>();
        try
        {
            foreach (var item in itemModelList)
            {
                var itemStruct = itemStructList[item];
                var product = dicProductList[item.Pid];
                //对照表
                var codecompare = product.CodeCompareDic;

                var variants = itemStruct.Variants;
                foreach (var variant in variants)
                {
                    if (!(cover || string.IsNullOrWhiteSpace(variant.Code) || variant.Code == "0"))
                        continue;

                    if (variant.State == VariantStateEnum.Enable)
                    {
                        var code = codes.First();
                        variant.Code = code.Value;
                        //初始化
                        if (codecompare == null)
                        {
                            codecompare = new Dictionary<string, string?>();
                        }

                        if (codecompare.Keys.Contains(variant.Sid!))
                        {
                            codecompare[variant.Sid!] = code.Value;
                        }
                        else
                        {
                            codecompare.Add(variant.Sid!, code.Value);
                        }

                        codeIds.Add(code.ID);
                        codes.RemoveAt(0);
                    }

                    // itemStruct.CodeCompare = codecompare;
                }

                product.CodeCompare = JsonConvert.SerializeObject(codecompare);


                var jsonProduct = JsonConvert.SerializeObject(itemStruct, new JsonSerializerSettings()
                {
                    TypeNameHandling = TypeNameHandling.All
                });
                item.FilesSize = jsonProduct.Length;
                // it双引号em.CodeCompare = itemStruct.CodeCompare!;
                var uploadState = await _uploadService.PutAsync(item.FilePath, jsonProduct);

                if (!uploadState)
                {
                    throw new SavedException("数据包存储失败");
                }
            }
        }
        catch (Exception e)
        {
            _logger.LogError(e, e.Message);
            throw new SavedException(e.Message);
        }

        var transaction = await _dbContext.Database.BeginTransactionAsync();

        try
        {
            //删除code
            await _dbContext.Eanupc.Where(m => codeIds.Contains(m.ID)).DeleteAsync();


            var dicItemList = itemModelList.ToDictionary(m => m.Pid, m => m);

            foreach (var (product, _) in productList)
            {
                product.FilesSize = dicItemList[product.ID].FilesSize;
                product.CodeCompare = dicProductList[product.ID].CodeCompare;
                await _dbContext.Product.SingleUpdateAsync(product);
            }

            foreach (var item in itemModelList)
            {
                await _dbContext.ProductItem.SingleUpdateAsync(item);
            }

            await _dbContext.SaveChangesAsync();
            await transaction.CommitAsync();
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            _logger.LogError(e, e.Message);
            throw new SavedException(e.Message);
        }
    }
}