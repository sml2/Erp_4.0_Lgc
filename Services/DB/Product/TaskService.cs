﻿using ERP.Extensions;
using ERP.Models.View.Product.Task;
using Microsoft.EntityFrameworkCore;
using ERP.ViewModels.Task;
using ERP.Data;
using ERP.Enums.Identity;
using ERP.Enums.Rule.Config;
using ERP.Enums.Rule.Config.Internal;
using ERP.Interface;
using ERP.Models.Product;
using ERP.Services.Caches;
using Z.EntityFramework.Plus;

namespace ERP.Services.Product;

public class TaskService : BaseWithUserService
{
    public readonly PlatformCache _platformcache;
    public readonly CategoryService _categoryService;
    public readonly ILogger _Logger;

    public TaskService(DBContext dbContext, ISessionProvider sessionProvider, PlatformCache platformcache,
        CategoryService categoryService, ILogger<TaskService> logger) : base(
        dbContext, sessionProvider)
    {
        _platformcache = platformcache;
        _categoryService = categoryService;
        _Logger = logger;
    }

    //public readonly HttpRequest _request;

    private IQueryable<TaskModel>? DataWatchRange(DataRange_2b range)
    {
        if (range == DataRange_2b.None)
            return null;

        var model = _dbContext.Task.Where(m => m.OEMID == _oemId && m.CompanyID == _companyId && !m.IsDel);

        if (range == DataRange_2b.Group)
            model = model.Where(m => m.GroupID == _groupId);

        if (range == DataRange_2b.User)
            model = model.Where(m => m.UserID == (_userId));

        return model;
    }

    private IQueryable<TaskModel> DataWatchRange()
    {
        if (_session.GetRole() == Role.ADMIN)
        {
            return _dbContext.Task.Where(m => m.CompanyID == _session.GetCompanyID() && !m.IsDel);
        }
        //todo 暂定以个人维度查看数据
        return _dbContext.Task.Where(m => m.UserID == _session.GetUserID() && !m.IsDel);
    }


    public async Task<List<TaskModel>> GetSelectOptions(DataRange_2b range)
    {
        var model = DataWatchRange(range);

        if (model is null)
            return new List<TaskModel>();

        return await model.OrderByDescending(m => m.ID).ToListAsync();
    }

    public async Task<DbSetExtension.PaginateStruct<TaskModel>?> GetPaginateList(ListDto req, DataRange_2b range)
    {
        var model = DataWatchRange(range);

        if (model != null)
            return await model
                .WhenWhere(!string.IsNullOrEmpty(req.Name), a => a.Name.Contains(req.Name!))
                .WhenWhere(req.PlatformId > 0, a => a.PlatformId.Equals(req.PlatformId))
                .OrderByDescending(a => a.ID)
                .ToPaginateAsync(page: req.Page, limit: req.Limit);
        return null;
    }

    /// <summary>
    /// 初始化
    /// </summary>
    /// <returns></returns>
    public async Task<object?> Init()
    {
        return _platformcache.List()!.ToDictionary(m => m.ID);
    }

    /// <summary>
    /// 详情
    /// </summary>
    /// <param name="id"></param>
    /// <param name="range"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> Detail(int id, DataRange_2b range)
    {
        var info = await GetInfo(id, range);
        if (info == null)
            return ReturnArr(false, "没有数据");
        var parentCategory = await _categoryService.FirstCategory(await GetUserRange(ProductAbout.CategoryRange),true);
        return ReturnArr(true, "", new
        {
            info = info,
            parentCategory = parentCategory
        });
    }

    /// <summary>
    /// 二级分类
    /// </summary>
    /// <param name="pid"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> SecondCategory(int pid)
    {
        //todo TaskService SecondCategory 可见范围
        // $where = $this->productWhereDataRange('Category');
        // if (!count($where)) {
        //     return returnArr(false, '缺少可见范围');
        // }
        var secondCategory = await _dbContext.Category.Where(a => a.Level == 2)
            .Where(a => a.Pid == pid)
            // .Where(a => a.Type == ERP.Models.Product.Category.Types.TYPE_FALSE)
            .ToListAsync();
        return ReturnArr(true, "", secondCategory);
    }

    public async Task Edit(EditViewModel vm, DataRange_2b range)
    {
        var transatcion = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            var info = await GetInfo(vm.ID,range);
            if (info == null)
                throw new Exception("该采集任务不存在");
            

            if (info.UpdatedAt != vm.UpdatedAt)
                throw new Exception("数据不同步");
            
            // hashCode 去重
            var hashCode = vm.Name.GetHashCode();
            var count = _dbContext.Task.Count(a => a.ID != vm.ID && a.HashCode == hashCode.ToString() && !a.IsDel);
            if (count > 0)
                throw new Exception("任务名称重复");
            
            info.Name = vm.Name;
            info.FirstCategoryId = vm.FirstCategoryId;
            info.SecondCategoryId = vm.SecondCategoryId;
            info.PlatformId = vm.PlatformId;
            info.PlatformName = vm.PlatformName;
            info.HashCode = hashCode.ToString();
            await _dbContext.Task.SingleUpdateAsync(info);

            await _dbContext.Product.Where(m => m.CompanyID == _companyId && m.TaskId == info.ID).UpdateAsync(m => new
            {
                CategoryId = vm.FirstCategoryId,
                SubCategoryId = vm.SecondCategoryId
            });
            
            await _dbContext.SaveChangesAsync();
            await transatcion.CommitAsync();
        }
        catch (Exception e)
        {
            await transatcion.RollbackAsync();
            Console.WriteLine(e.Message);
            throw new Exception("更新失败");
        }
    }

    public async Task<bool> Delete(DeleteDto req,DataRange_2b range)
    {

        var item = await GetInfo(req.ID,range);
        if (item == null)
        {
            throw new Exception("找不到相关数据#1");
        }

        var transaction = await _dbContext.Database.BeginTransactionAsync();

        try
        {
            if (req.Mode == TaskDeleteMode.Product)
            {
                var pm = new ProductModel();
                pm.SetRecycle(true);
                await _dbContext.Product.Where(m => m.TaskId == item.ID).UpdateAsync(m => new { Type = pm.Type   } );
            }
            item.IsDel = true;
            await _dbContext.Task.SingleUpdateAsync(item);
            await transaction.CommitAsync();
        }
        catch (Exception e)
        {
            _Logger.LogInformation(e.Message,e);
            await transaction.RollbackAsync();
            throw;
        }



        return true;
    }

    /// <summary>
    /// 获取详情
    /// </summary>
    /// <param name="id"></param>
    /// <param name="range"></param>
    /// <returns></returns>
    public async Task<TaskModel?> GetInfo(int id, DataRange_2b range)
    {
        var model = DataWatchRange(range);
        if (model == null) return null;
        return await model.FirstOrDefaultAsync(a => a.ID == id && !a.IsDel);
    }
}