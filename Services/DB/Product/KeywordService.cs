﻿using ERP.Extensions;
using ERP.Models.Product;
using Microsoft.EntityFrameworkCore;
using System.Collections;
using System.Globalization;
using ERP.Authorization.Rule.Component;
using ERP.ControllersWithoutToken.Tasks;
using ERP.Data;
using ERP.Enums.Rule.Config.Internal;
using ERP.Exceptions.Keywords;
using ERP.Interface;
using ERP.Models.Template;
using ERP.Models.View.Product.Template;
using ERP.ViewModels.Logistics.ShipRatesEstimationTemplate;
using ERP.ViewModels.Template;

namespace ERP.Services.Product;

public class KeywordService : BaseWithUserService
{
    public KeywordService(DBContext dbContext, ISessionProvider sessionProvider) : base(dbContext, sessionProvider)
    {
    }

    private ISession _session => _sessionProvider.Session!;

    private IQueryable<KeywordTemplateModel>? DataWatchRange(DataRange_2b range)
    {
        if (range == DataRange_2b.None)
            return null;

        var model = _dbContext.KeywordTemplate.Where(m => m.OEMID == _oemId && m.CompanyID == _companyId);

        if (range == DataRange_2b.Group)
            model = model.Where(m => m.GroupID == _groupId);

        if (range == DataRange_2b.User)
            model = model.Where(m => m.UserID == (_userId));

        return model;
    }

    /// <summary>
    /// 关键词列表
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    public async Task<DbSetExtension.PaginateStruct<KeywordTemplateModel>> List(ListDto req, DataRange_2b range)
    {
        var model = DataWatchRange(range);

        if (model is null)
            return new DbSetExtension.PaginateStruct<KeywordTemplateModel>();

        return await model
            .WhenWhere(!string.IsNullOrEmpty(req.Name), a => a.Name.Contains(req.Name!))
            .WhenWhere(!string.IsNullOrEmpty(req.LanguageSign), a => a.LanguageSign.Equals(req.LanguageSign))
            .OrderByDescending(a => a.ID)
            .ThenByDescending(a => a.Count)
            .ToPaginateAsync(page: req.Page, limit: req.Limit);
    }

    /// <summary>
    /// 获取关键词
    /// </summary>
    /// <returns></returns>
    public async Task<List<KeywordTemplateModel>> GetKeyword(DataRange_2b range)
    {
        var model = DataWatchRange(range);
        if (model is null)
        {
            return new List<KeywordTemplateModel>();
        }

        return await model.OrderByDescending(a => a.Count).ToListAsync();
    }

    public async Task<ReturnStruct> AddOrUpdateKeyword(KeywordEditVM vm, DataRange_2b range)
    {
        if (vm.ID > 0)
        {
            //todo KeywordService  AddOrUpdateKeyword 可见范围
            // $where = $this->productWhereDataRange(self::RANGE);
            // if (!count($where)) {
            //     return returnArr(false, '缺少可见范围');
            // }
            var info = await GetInfo(vm.ID, range);
            if (info == null)
            {
                return ReturnArr(false, "该条关键词模板不存在");
            }

            if (info.UpdatedAtTicks != vm.UpdatedAtTicks)
            {
                return ReturnArr(false, "数据不同步");
            }

            info.Name = vm.Name;
            info.Value = vm.Value;
            info.Language = vm.Language;
            info.LanguageSign = vm.LanguageSign.ToString().ToLower();
            info.UpdatedAt = DateTime.Now;
            _dbContext.KeywordTemplate.Update(info);
        }
        else
        {
            var data = new KeywordTemplateModel(_session)
            {
                Name = vm.Name,
                Language = vm.Language,
                LanguageSign = vm.LanguageSign.ToString().ToLower(),
                Value = vm.Value,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
            };
            await _dbContext.KeywordTemplate.AddAsync(data);
        }

        await _dbContext.SaveChangesAsync();
        return ReturnArr(true, "");
    }


    public async Task<bool> Destroy(int id, DataRange_2b range)
    {
        //todo KeywordService  DelKeyword 可见范围
        // $where = $this->productWhereDataRange(self::RANGE);
        // if (!count($where)) {
        //     return returnArr(false, '缺少可见范围');
        // }
        var keywordTemplate = await GetInfo(id, range);
        if (keywordTemplate == null)
        {
            throw new NotFoundException("找不到相关数据#1");
        }

        _dbContext.KeywordTemplate.Remove(keywordTemplate);


        return await _dbContext.SaveChangesAsync() > 0;
    }

    /// <summary>
    /// 获取关键词详情
    /// </summary>
    /// <param name="id"></param>
    /// <param name="range"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> Info(int id, DataRange_2b range)
    {
        // todo KeywordService  KeywordInfo 可见范围
        // $where = $this->productWhereDataRange(self::RANGE);
        // if (!count($where)) {
        //     return returnArr(false, '缺少可见范围');
        // }
        var keywordTemplate = await GetInfo(id, range);
        if (keywordTemplate == null)
        {
            return ReturnArr(false, "没有数据");
        }

        return ReturnArr(true, "", keywordTemplate);
    }

    /// <summary>
    /// 产品关键词工具导入关键词
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    public async Task<bool> ImportKeyword(KeywordEditVM req)
    {
        KeywordTemplateModel keywordTemplateModel = new(_session, req.Name, req.Value, req.LanguageSign.ToString());
        _dbContext.KeywordTemplate.Add(keywordTemplateModel);
        return await _dbContext.SaveChangesAsync() > 0;
    }

    /// <summary>
    /// 获取详情
    /// </summary>
    /// <param name="id"></param>
    /// <param name="range"></param>
    /// <returns></returns>
    private async Task<KeywordTemplateModel?> GetInfo(int id, DataRange_2b range)
    {
        var model = DataWatchRange(range);
        if (model is null)
        {
            return null;
        }

        return await model.FirstOrDefaultAsync(a => a.ID == id);
    }

    /// <summary>
    /// 关键词使用次数追加
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    /// <exception cref="NotFoundException"></exception>
    public async Task<bool> Increment(int id, DataRange_2b range)
    {
        var info = await GetInfo(id, range);

        if (info is null)
        {
            throw new NotFoundException("找不到相关数据#1");
        }

        info.Count += 1;
        await _dbContext.KeywordTemplate.SingleUpdateAsync(info);

        return await _dbContext.SaveChangesAsync() > 0;
    }
}