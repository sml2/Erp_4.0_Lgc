using ERP.Enums;
using ERP.Models.DB.Product;
using ERP.Models.Product;
using ERP.Services.Upload;
using Newtonsoft.Json;
using ProductDBService = ERP.Services.DB.Product.Product;
using AsinModel = ERP.Models.Stores.AsinProduct;
using ERP.Services.Stores;
using AsinProduct = ERP.Services.DB.Stores.AsinProduct;
using ILogger = Microsoft.Extensions.Logging.ILogger;
using ModelProduct = ERP.Data.Products.Product;


namespace ERP.Services.Product;

public class UploadAmazonService
{
    private readonly StoreService _storeService;

    private readonly AsinProduct _asinProduct;

    private readonly ProductDBService _productDbService;

    public UploadAmazonService(StoreService storeService, AsinProduct asinProduct, ProductDBService productDbService)
    {
        _storeService = storeService;
        _asinProduct = asinProduct;
        _productDbService = productDbService;
    }

    private string GetProductJsonPath(long version, string pid, int companyId, int userId, int oemID)
    {
        var t = DateTime.Now;

        return $"{oemID}/{t.Year:0000}/{t.Month:00}/{t.Day:00}/{companyId}/products/{pid}/v_{userId}_{version}.json";
    }

    public async Task<bool> UploadProduct(List<ModelProduct> models, string nationShort, IHostEnvironment environment,
        ILogger logger)
    {
        try
        {
            var ProductModels = new List<ProductInfo>();
            var AsinModels = new List<AsinModel>();
            //var language = GetLanguage(nationShort.ToLower(), logger);
            foreach (var model in models)
            {
                var store = await _storeService.GetInfoById(model.storeId);
                if (store.IsNotNull())
                {
                    var oem = await _storeService.GetFileBultById(model.storeId);
                    if (oem is not null)
                    {
                        logger.LogInformation($"UploadProduct oem is not null");
                        logger.LogInformation($"UploadProduct oem:{JsonConvert.SerializeObject(oem)}");
                        var uploadServiceTask = new UploadService(oem, environment);
                        string sellerID = store.GetSPAmazon()!.SellerID;
                        var createdAt = DateTime.Now;
                        var version = createdAt.GetTimeStampMilliseconds();
                        //获取产品json路径
                        var jsonFilePath = GetProductJsonPath(version, model.Pid, store.CompanyID, store.UserID,
                            store.OEMID);
                        var jsonProduct = JsonConvert.SerializeObject(model);
                        var uploadState = await uploadServiceTask.PutAsync(jsonFilePath, jsonProduct);
                        //var res=await  _uploadService.PutStream(model, _oemId, _userId);
                        if (uploadState)
                        {
                            var historyVersion = new List<long>();
                            historyVersion.Add(version);
                            var product = new ProductModel(store.UserID, store.GroupID, store.CompanyID, store.OEMID)
                            {
                                Pid = model.productID,
                                PidHashCode = model.HashCode.ToString(),
                                PlatformId = 1,
                                Quantity = model.Quantity!,
                                Title = model.Title.IsNullOrEmpty() ? "" : model.Title,
                                HashCode = model.Title.IsNullOrEmpty() ? 0 : Helpers.CreateHashCode(model.Title!),
                                MainImage = model.MainImage.IsNull()
                                    ? null
                                    : JsonConvert.SerializeObject(model.MainImage),
                                Language = 0,
                            };

                            ProductItemModel productItem = new ProductItemModel();
                            productItem.Pid = product.ID;
                            productItem.FilePath = jsonFilePath;
                            productItem.FilePathHashCode = Helpers.CreateHashCode(jsonFilePath);
                            productItem.HistoryVersion = JsonConvert.SerializeObject(historyVersion);
                            productItem.CurrentVersion = version;
                            productItem.UserID = store.UserID;
                            productItem.GroupID = store.GroupID;
                            productItem.CompanyID = store.CompanyID;
                            productItem.OEMID = store.OEMID;
                            productItem.LanguageId = 0;
                            productItem.LanguageName = Languages.Default.GetDescription();
                            product.SetAmazonProductFlag(true); //amazon product flag
                            var ProductInfo = new ProductInfo(product, productItem);
                            ProductModels.Add(ProductInfo);
                            AsinModel asinModel = new AsinModel(model.HashCode, store.ID, model.Brand, model.Size,
                                model.Color, model.ImageUrl, model.LanguageTag);
                            AsinModels.Add(asinModel);
                        }
                    }
                    else
                    {
                        logger.LogError($"UploadProduct oem not found :storeid {model.storeId}");
                    }
                }
                else
                {
                    logger.LogError($"UploadProduct store not found :storeid {model.storeId}");
                }
            }

            if (ProductModels.Count > 0)
            {
                await _productDbService.Add(ProductModels);
            }

            if (AsinModels.Count > 0)
            {
                await _asinProduct.Update(AsinModels);
            }

            return true;
        }
        catch (Exception e)
        {
            logger.LogError($"UploadProduct exception:{e.Message},StackTrace:{e.StackTrace}");
        }

        return false;
    }

    /// <summary>
    /// Amazon spapi中找不到关于languageTag的说明，只能先在asinproduct表中存下来，后续观察
    /// 暂时不用，以后 to do
    /// </summary>
    /// <param name="nationShort"></param>
    /// <param name="logger"></param>
    /// <returns></returns>
    public Languages GetLanguage(string nationShort, ILogger logger)
    {
        switch (nationShort)
        {
            case "mx": //墨西哥 西班牙语
                return Languages.ES;
            case "br": //巴西  葡萄牙语
                return Languages.PT;
            case "es": //西班牙   西班牙语
                return Languages.ES;
            case "fr":
                return Languages.FR;
            case "be": //比利时
                return Languages.FR;

            default: //us,ca,uk
                return Languages.Default;
        }
    }



}