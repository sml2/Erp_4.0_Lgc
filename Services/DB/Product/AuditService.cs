﻿using ERP.Extensions;
using ERP.Models.Product;
using Microsoft.EntityFrameworkCore;
using ERP.Data;
using ERP.Exceptions.Product;
using ERP.Models.View.Product.Audit;
using ERP.ViewModels;
using static ERP.Enums.Product.Types;

namespace ERP.Services.Product;

public class AuditService
{
    public readonly DBContext _dbContext;
    public readonly ProductService _productService;

    public AuditService(DBContext dbContext, ProductService productService)
    {
        _dbContext = dbContext;
        _productService = productService;
    }

    public async Task<bool> SetAudit(SetDto req)
    {
        var model = await ProductService.GetInfoById(_dbContext, req.ID);
        if (model == null)
        {
            throw new NotFoundException("找不到相关数据#1");
        }

        var audit = req.Audit;

        if (req.Mode == ProductModel.AUDIT_AUDITED)
        {
            audit = req.Mode;
        }

        // model.Type = (model.Type & (~(long)MASK_AUDIT)) ^ ((long)audit << (int)Audit);
        model.SetAudit(Convert.ToInt32(audit));
        model.Reason = req.Reason;
        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            await _dbContext.Product.SingleUpdateAsync(model);
            await _dbContext.SaveChangesAsync();
            await transaction.CommitAsync();
            return true;
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            Console.WriteLine(e);
            throw;
        }
    }


    public async Task<object?> Init(IdDto req)
    {
        var auditSelection = new Dictionary<string, Dictionary<object, string>>();

        auditSelection.Add("checkKey", new Dictionary<object, string>()
        {
            { "product", "产品本身" },
            { "image", "产品图片" },
            { "info", "产品详情" },
            { "other", "其他原因" },
        });

        auditSelection.Add("product", new Dictionary<object, string>()
        {
            // {1,"当前产品未审核"},
            // {2,"审核通过"},
            { 3, "侵权" },
            { 4, "重复产品" },
            { 5, "违禁产品" },
            { 6, "款式陈旧" },
            { 7, "进货困难" },
        });

        auditSelection.Add("image", new Dictionary<object, string>()
        {
            { 30, "带有水印图片" },
            { 31, "图片破损或不符" },
            { 32, "图片重复" },
            { 40, "描述为空" },
            { 41, "描述不符" },
            { 50, "卖点为空" },
            { 51, "卖点不符" },
            { 60, "关键词为空" },
            { 61, "关键词不符" },
            { 70, "分类为空" },
            { 71, "分类不符" },
            { 80, "标题为空" },
            { 81, "标题不符" },
        });

        auditSelection.Add("info", new Dictionary<object, string>()
        {
            { 20, "价格不合理" },
            { 21, "价格偏高" },
            { 22, "价格偏低" },
        });

        auditSelection.Add("other", new Dictionary<object, string>()
        {
            { 19, "自定义" },
            { 18, "其他原因" },
        });

        ProductModel? product = null;
        if (req.ID > 0)
        {
            product = await ProductService.GetInfoById(_dbContext, req.ID);

            if (product.IsNotNull())
            {
                if (product.Audit != ProductModel.AUDIT_UNREVIEWED)
                {
                    foreach (var i in auditSelection["checkKey"])
                    {
                        foreach (var item in auditSelection[i.Key.ToString()])
                        {
                            if (Convert.ToInt64(item.Key) == product.Audit)
                            {
                                product.AuditKey = item.Key;
                                product.AuditType = i.Key;
                            }
                        }
                    }
                }
            }
            else
            {
                throw new NotFoundException("找不到相关数据#1");
            }
        }

        return new
        {
            Info = product,
            Audit = auditSelection,
        };
    }
}