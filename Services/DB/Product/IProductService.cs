
namespace ERP.Services.Product;

interface IProductService
{
    public Task<BaseService.ReturnStruct> Info(int id);
    public Task<bool> Destroy(int id);
}