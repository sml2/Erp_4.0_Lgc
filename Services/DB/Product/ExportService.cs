using ERP.Data;
using ERP.Enums;
using ERP.Exceptions.Export;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.DB.Identity;
using ERP.Models.DB.Stores;
using ERP.Models.Export;
using ERP.Models.Stores;
using ERP.Models.View.Products.Export;
using ERP.Services.Export;
using ERP.Services.Stores;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using NotFoundException = ERP.Exceptions.SyncExport.NotFoundException;

namespace ERP.Services.Product;

public class ExportService : BaseWithUserService
{
    private readonly SyncExportService _syncExportService;
    private readonly SyncExportProductService _syncExportProductService;
    private readonly ILogger<ExportService> _logger;
    private readonly SyncProductService _syncProductService;
    private readonly ProductService _productService;

    public ExportService(ISessionProvider sessionProvider, DBContext dbContext,
        SyncExportService syncExportService, SyncProductService syncProductService,
        ProductService productService, SyncExportProductService syncExportProductService, ILogger<ExportService> logger) : base(dbContext, sessionProvider)
    {
        _syncExportService = syncExportService;
        _syncProductService = syncProductService;
        _productService = productService;
        _syncExportProductService = syncExportProductService;
        _logger = logger;
    }

    public async Task<bool> DeleteExportProduct(DeleteExportProductDto req)
    {
        var exportProduct = await _syncExportProductService.GetExportProductInfo(req.Eid, req.Sid);

        if (exportProduct is null)
        {
            throw new NotFoundException("产品不存在#1");
        }

        var transatcion = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            //删除导出关联关系
            await _syncExportProductService.DeleteExportProductById(exportProduct.ID);
            //导出任务产品数减一
            //3.0 FIXME 数量计算不对，导致数据库自减失败
            await _syncExportService.ExportReduce(req.Eid);

            // 多语言导出时, 需要判断还有没有其他任务在使用此产品
            var isUsing = await _dbContext.SyncExportProduct
                .Where(sep => sep.ID != exportProduct.ID && sep.Sid == req.Sid).AnyAsync();
            if (!isUsing)
            {
                await _syncProductService.DeleteSyncProduct(req.Sid, SyncProductModel.ProductTypes.Export); //syncProduct只被用户导出使用  物理删除复制产品信息
            }

            await transatcion.CommitAsync();
        }
        catch (Exception e)
        {
            await transatcion.RollbackAsync();
            _logger.LogError(e, "删除导出产品出错");
            throw new ExportException(e.Message);
        }

        return true;
    }
    
    /// <summary>
    /// 产品列表原始产品加入导出任务
    /// </summary>
    /// <returns></returns>
    public async Task<Result> JoinExportProduct(int sourceId, ProductConfig productConfig, SyncExport syncExport)
    {
        var param = JObject.Parse(syncExport.Param);
        var language = Enum.Parse<Languages>(param.GetValue("Language", StringComparison.OrdinalIgnoreCase).ToString(), true);
        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try {
            var res = await _productService.ImportClone(sourceId, language);
            var sp = await _syncExportService.InsertSyncProduct(res, productConfig);
            
            // 多语言导出时, 追加产品将产品同步给其他语言
            var relationId = syncExport.RelationId ?? syncExport.ID;
            var syncExports = await _dbContext.SyncExport
                .Where(se => se.ID == relationId || se.RelationId == relationId)
                .ToArrayAsync();

            foreach (var export in syncExports)
            {
                await _syncExportService.AddSyncExportProduct(new SyncExportProduct()
                {
                    Eid = export.ID,
                    Sid = sp.ID,
                    Language = language,
                    LanguageId = export.LanguageId == 0 ? null : export.LanguageId,
                    ExportSign = export.ExportSign,
                    PlatformId = export.PlatformId,
                    UserID = _userId,
                    GroupID = _groupId,
                    CompanyID = _companyId,
                    OEMID = _oemId,
                });
            
                //任务集产品数自增
                await _syncExportService.ExportIncrement(export.ID);
            }
            
            await transaction.CommitAsync();
        } catch (Exception e)
        {
            await transaction.RollbackAsync();
            return Result.Fail(e.Message);
        }
        
        return Result.Ok();
    }
}