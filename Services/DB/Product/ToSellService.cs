﻿using ERP.Data;
using ERP.Extensions;

using Microsoft.EntityFrameworkCore;
using ERP.Services.Stores;
using Model = ERP.Models.Product.ToSellModel;
using Z.EntityFramework.Plus;
using System;
using static ERP.Extensions.DbSetExtension;
using ERP.Models.DB.Product;
using ERP.Models.View.Products.SmartHijacking;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using ERP.Models.DB.Stores;
using ERP.Models.Product;
using OrderSDK.Modles.Amazon.Feeds;

namespace ERP.Services.Product;

public class ToSellService
{
    public readonly DBContext _dbContext;
    private readonly StoreService storeService;
    protected readonly ILogger<ToSellService> _logger;

    public ToSellService(StoreService _storeService, ILogger<ToSellService> logger, DBContext dbContext)
    {
        storeService = _storeService;
        _logger = logger;
        _dbContext = dbContext;
    }

    public async Task<bool> Adds(List<Model> models) => await AddInternal(models);

    private async Task<bool> AddInternal(List<Model> models)
    {
        List<Model> addList = new();       
        try
        {
            if (models.Count() > 0)
            {
                foreach (var model in models)
                {
                    var m = GetModel(model.HashCode);
                    if (m == null)
                    {
                        addList.Add(model);
                    }                   
                }

                if (addList.Count > 0)
                {
                    await _dbContext.ToSell.AddRangeAsync(addList);
                }               
            }
        }
        catch (Exception e)
        {
            _logger.LogError("ToSellService AddInternal error:" + e.Message);
            _logger.LogError("ToSellService AddInternal " + e.StackTrace);
            return false;
        }
        return true;
    }

    public List<Model> CheckModels(UpdateAsinsVM vm)
    {
        List<Model> updateList = new List<Model>();
        List<string> errors = new List<string>();

        var models = GetModels(vm.ids);
        if (models != null)
        {
            foreach (var model in models)
            {
                var p = new MoneyMeta(model.Currency, vm.Price);
                if (model.Quantity != vm.Stock || model.UnitPrice != p || model.StockDays != vm.Stockdays || model.ProductState != vm.productState)
                {
                    model.ProductResult = HijackingResults.WaitSynchronize;
                }
                //更新修改值
                model.Quantity = vm.Stock;
                model.UnitPrice = p;
                model.StockDays = vm.Stockdays;
                model.ProductState = vm.productState;
                updateList.Add(model);
            }
        }
        return models;
    }  

    public async Task<bool> Deletes(List<Model> models) => await DeleteInternal(models);

    private async Task<bool> DeleteInternal(List<Model> models)
    {
        using (var tr = await _dbContext.Database.BeginTransactionAsync())
        {
            try
            {               
                _dbContext.ToSell.RemoveRange(models);
                await _dbContext.SaveChangesAsync();
                await tr.CommitAsync();
                return true;
            }
            catch (Exception e)
            {
                await tr.RollbackAsync();
                _logger.LogError("ToSellService DeleteInternal error:" + e.Message);
                _logger.LogError("ToSellService DeleteInternal " + e.StackTrace);
                return false;
            }
        }
    }



    public async Task<bool> Updates(List<Model> models) => await UpdatesInternal(models);

    private async Task<bool> UpdatesInternal(List<Model> models)
    {
        using(var tr=await _dbContext.Database.BeginTransactionAsync())
        {
            try
            {
                if (models.Count <= 0)
                {
                    return false;
                }
                _dbContext.ToSell.UpdateRange(models);
                await _dbContext.SaveChangesAsync();
                await tr.CommitAsync();
                return true;
            }
            catch (Exception e)
            {
                await tr.RollbackAsync();
                _logger.LogError("ToSellService UpdatesInternal error:" + e.Message);
                _logger.LogError("ToSellService UpdatesInternal " + e.StackTrace);
                return false;
            }           
        }        
    }

    public async Task<bool> UpdateSkus(List<Model> models)
    {
        using (var tr = await _dbContext.Database.BeginTransactionAsync())
        {
            try
            {
                foreach(var item in models)
                {
                    if(item.Sync!=1)
                    {
                        item.Sku = Helpers.SkuCode();
                    }                    
                }              
                _dbContext.ToSell.UpdateRange(models);
                await _dbContext.SaveChangesAsync();
                await tr.CommitAsync();
                return true;
            }
            catch (Exception e)
            {
                await tr.RollbackAsync();
                _logger.LogError("ToSellService UpdatesInternal error:" + e.Message);
                _logger.LogError("ToSellService UpdatesInternal " + e.StackTrace);
                return false;
            }
        }
    }



    public Model? GetModel(ulong hashCode)
    {
       return _dbContext.ToSell.Where(x=>x.HashCode== hashCode).FirstOrDefault();      
    }

    public Model? GetModelById(int id)
    {
        return _dbContext.ToSell.Where(x => x.ID==id).FirstOrDefault();
    }
    public List<Model>? GetModels(List<int> ids)
    {
        return _dbContext.ToSell.Include(x=>x.storeRegion).Include(x=>x.User).Where(x => ids.Contains(x.ID)).ToList();
    }

    /// <summary>
    /// 同一店铺，同一市场
    /// </summary>
    /// <param name="models"></param>
    public string JsonCtor(List<Model> models, ListingFeed<UpdateHijackingJson> listingFeed, ActionType action)
    {
        UpdateHijackingJsons jsons = new();
        foreach (var model in models)
        {
            UpdateHijackingJson hijacking = new();           
            hijacking.ProductType = model.ProductType;
            hijacking.Sku = model.Sku;
            hijacking.fulfillmentAvailability = new List<fulfillment_availability>();
            fulfillment_availability fulfillment = new();
            fulfillment.Quantity = action == ActionType.TakeOff ? 0 : model.Quantity;
            fulfillment.leadTimeToShipMaxDays = model.StockDays == null ? 0 : (int)model.StockDays!;
            hijacking.fulfillmentAvailability.Add(fulfillment);
            hijacking.listPrices = new List<list_price>();
            list_price list_Price = new list_price();
            list_Price.value = model.UnitPrice == null ? 0 :Convert.ToDecimal(model.UnitPrice.ToString(model.Currency, false));
            list_Price.currency = model.Currency;
            list_Price.marketplaceId = model.AsinMarketplace;
            hijacking.listPrices.Add(list_Price);
            hijacking.merchantSuggestedAsin = new List<merchant_suggested_asinC>();
            merchant_suggested_asinC suggested_asin = new merchant_suggested_asinC();
            suggested_asin.Value = model.Asin;
            suggested_asin.MarketplaceId = model.AsinMarketplace;
            Condition_type condition_Type = new Condition_type();
            condition_Type.Value = model.ProductState;
            condition_Type.MarketplaceId = model.AsinMarketplace;
            hijacking.conditionType = new List<Condition_type>();
            hijacking.conditionType.Add(condition_Type);
            hijacking.merchantSuggestedAsin.Add(suggested_asin);
            jsons.Add(hijacking);            
        }
        int i = 0;
        foreach (var item in jsons)
        {
            i++;
            MessageInfo<UpdateHijackingJson> messageInfo = new();
            messageInfo.MessageId = i;
            messageInfo.Sku = item.Sku;
            messageInfo.ProductType = item.ProductType;
            messageInfo.Attributes = item;
            messageInfo.OperationType = "PARTIAL_UPDATE";
            listingFeed.Messages.Add(messageInfo);
        }

        //JsonSerializerSettings jss = new JsonSerializerSettings();
        //jss.NullValueHandling = NullValueHandling.Ignore;
        //return  Newtonsoft.Json.JsonConvert.SerializeObject(listingFeed, Formatting.Indented, jss);

        return JsonConvert.SerializeObject(listingFeed);
    }

    /// <summary>
    /// 同一店铺，同一市场
    /// </summary>
    /// <param name="models"></param>
    public string JsonCtor(List<Model> models, ListingFeed<DeleteHijackingJson> listingFeed, ActionType action)
    {
        DeleteHijackingJsons jsons = new();
        foreach (var model in models)
        {
            DeleteHijackingJson hijacking = new();
            hijacking.Sku = model.Sku!;
            hijacking.ProductType = model.ProductType;
            hijacking.merchantSuggestedAsin = new List<DeleteHijackingJson.merchant_suggested_asinC>();
            DeleteHijackingJson.merchant_suggested_asinC suggested_asin = new DeleteHijackingJson.merchant_suggested_asinC();
            suggested_asin.Value = model.Asin;
            suggested_asin.MarketplaceId = model.AsinMarketplace;
            hijacking.merchantSuggestedAsin.Add(suggested_asin);
            jsons.Add(hijacking);
        }        
        int i = 0;
        foreach (var item in jsons)
        {
            i++;
            MessageInfo<DeleteHijackingJson> messageInfo = new();
            messageInfo.MessageId = i;
            messageInfo.Sku = item.Sku;
            messageInfo.ProductType = item.ProductType;
            messageInfo.Attributes = item;
            messageInfo.OperationType = "DELETE";
            listingFeed.Messages.Add(messageInfo);
        }
        return JsonConvert.SerializeObject(listingFeed);
        //JsonSerializerSettings jss = new JsonSerializerSettings();
        //jss.NullValueHandling = NullValueHandling.Ignore;
        //return Newtonsoft.Json.JsonConvert.SerializeObject(listingFeed, Formatting.Indented, jss);
    }
    public string JsonCtor(List<Model> models, ListingFeed<AddHijackingJson> listingFeed, ActionType action)
    {
        AddHijackingJsons jsons = new();
        foreach (var model in models)
        {
            AddHijackingJson hijacking = new();
            hijacking.Sku = model.Sku!;
            hijacking.ProductType = model.ProductType;
            hijacking.merchantSuggestedAsin = new List<AddHijackingJson.merchant_suggested_asinC>();
            AddHijackingJson.merchant_suggested_asinC suggested_asin = new AddHijackingJson.merchant_suggested_asinC();
            suggested_asin.Value = model.Asin;
            suggested_asin.MarketplaceId = model.AsinMarketplace;
            hijacking.merchantSuggestedAsin.Add(suggested_asin);
            jsons.Add(hijacking);
        }
        int i = 0;
        foreach (var item in jsons)
        {
            i++;
            MessageInfo<AddHijackingJson> messageInfo = new();
            messageInfo.MessageId = i;
            messageInfo.Sku = item.Sku;
            messageInfo.ProductType = item.ProductType;
            messageInfo.Attributes = item;
            messageInfo.OperationType = "PARTIAL_UPDATE";
            listingFeed.Messages.Add(messageInfo);
        }
        //JsonSerializerSettings jss = new JsonSerializerSettings();
        //jss.NullValueHandling = NullValueHandling.Ignore;
        //return Newtonsoft.Json.JsonConvert.SerializeObject(listingFeed, Formatting.Indented, jss);
        //
        return JsonConvert.SerializeObject(listingFeed);
    }


    //    /**
    //     * 查询产品存在性
    //     * User: CCY
    //     * Date: 2020/12/3
    //     * @param $marketplace
    //     * @param $hash_code
    //     * @param $asin
    //     * @return mixed
    //     */
    //    public function checkExists($marketplace, $hash_code, $asin)
    //    {
    //        return ToSellModel::query()
    //            ->CompanyId($this->getSession()->getCompanyId())
    //            ->marketplace($marketplace)
    //            ->HashCode($hash_code)
    //            ->asin($asin)
    //            ->exists();
    //    }

    /// <summary>
    /// 查询产品存在性
    /// </summary>
    /// <param name="marketplace"></param>
    /// <param name="hashCode"></param>
    /// <param name="asin"></param>
    /// <returns></returns>
    public async Task<bool> CheckExists(string marketplace, ulong hashCode, string asin)
    {
        // var user = await _userManager.GetUserAsync(_httpContextAccessor.HttpContext?.User);
        // if (user == null)
        // {
        //     return false;
        // }

        var companyId = 0;// Session.GetCompanyID();

        var info = await _dbContext.ToSell
            .WhenWhere(companyId != 0, m => m.CompanyID == companyId)
            .WhenWhere(!string.IsNullOrEmpty(marketplace), m => m.AsinMarketplace == marketplace)
            .WhenWhere(hashCode != 0, m => m.HashCode == hashCode)
            .WhenWhere(!string.IsNullOrEmpty(asin), m => m.Asin == asin)
            .FirstOrDefaultAsync();

        return info!=null;
    }

    //    /**
    //     * 添加跟卖产品
    //     * User: CCY
    //     * Date: 2020/12/3
    //     * @param $insertData
    //     * @return bool
    //     */
    //    public function insertData($insertData)
    //    {
    //        return ToSellModel::query()->insert($insertData);
    //    }

    //    /**
    //     * 获取列表数据
    //     * User: CCY
    //     * Date: 2020/12/3
    //     * @param $asin
    //     * @param $store_id
    //     * @param $store_ids
    //     * @return mixed
    //     */
    //    public function getList($asin, $store_id, $store_ids)
    //    {
    //        $field = ['id', 'marketplace', 'title', 'asin', 'lowestprice', 'shipping', 'brand', 'manufacturer', 'imageurl', 'unit', 'store_id', 'truename', 'created_at', 'rate'];
    //        return ToSellModel::query()
    //            ->companyId($this->getSession()->getCompanyId())
    //            ->StoreId($store_id)
    //            ->asin($asin)
    //            ->StoreIds($store_ids)
    //            ->oemId($this->getSession()->getOemId())
    //            ->sort()
    //            ->select($field)
    //            ->paginate($this->limit);
    //    }

    public async Task<PaginateStruct<Model>?> List(HijackingVM vM)
    {
        var storeList = await storeService.CurrentUserGetStores();
        var storeids= storeList.Select(x=>x.ID).ToList();
        var info = await _dbContext.ToSell.Include(x=>x.User)
            .Where(x => storeids.Contains(x.storeRegion.ID))
            .WhenWhere(!string.IsNullOrEmpty(vM.ProductName), m => m.Title == vM.ProductName)
            .WhenWhere(!string.IsNullOrEmpty(vM.asins), m => m.Asin == vM.asins)
            .WhenWhere(vM.storeID.IsNotNull(), m => m.storeRegion.ID == vM.storeID)
            .WhenWhere(vM.State.IsNotNull(), m => m.ProductResult == vM.productResult)      
            .WhenWhere(vM.marketPlaceID.IsNotEmpty(),m=>m.AsinMarketplace==vM.marketPlaceID)

            .WhenWhere(vM.State==1 , m => m.ProductResult == HijackingResults.WaitSynchronize)
            .WhenWhere(vM.State == 2, m => m.ProductResult == HijackingResults.Synchronize)
            .WhenWhere(vM.State == 3, m => m.ProductResult == HijackingResults.SynchronizeFail)
            .WhenWhere(vM.State == 4, m => m.ProductResult == HijackingResults.PutOn)
            .WhenWhere(vM.State ==5, m => m.ProductResult == HijackingResults.TakeOff)
            .ToPaginateAsync(); 
        return info;
    }

    //    /**
    //     * 添加待同步产品
    //     * User: CCY
    //     * Date: 2020/12/4
    //     * @param $insertData
    //     * @return bool
    //     */
    //    public function insertTaskProduct($insertData)
    //    {
    //        return ToSellTaskProduct::query()->insert($insertData);
    //    }

    //    /**
    //     * 获取同步产品
    //     * User: CCY
    //     * Date: 2020/12/7
    //     * @param int    $type 1全部 2待同步 3同步中 4完成 5失败
    //     * @param        $asin
    //     * @param string $pid
    //     * @param        $store_id
    //     * @param        $store_ids
    //     * @return mixed
    //     */
    //    public function getListPage($type, $asin, $pid, $store_id, $store_ids)
    //    {
    //        $field = ['id', 'sku', 'asin', 'price', 'unit', 'stock', 'stock_up', 'product_state', 'store_id', 'truename', 'created_at', 'product_json', 'product_result', 'product_reason', 'price_result', 'price_reason', 'stock_result', 'stock_reason', 'end_result', 'other_info', 'upload_state', 'type', 'data_state'];

    //        $endResult = '';
    //        switch ($type) {
    //            case 2:
    //                $UploadState = ToSellTaskProduct::UPLOAD_ONE;
    //                break;
    //            case 3:
    //                $UploadState = ToSellTaskProduct::UPLOAD_TWO;
    //                break;
    //            case 4:
    //                $UploadState = ToSellTaskProduct::UPLOAD_THREE;
    //                break;
    //            case 5:
    //                $UploadState = ToSellTaskProduct::UPLOAD_THREE;
    //                $endResult = ToSellTaskProduct::END_FALSE;
    //                break;
    //            default:
    //                $UploadState = '';
    //                break;
    //        }
    //        return ToSellTaskProduct::query()
    //            ->Pid($pid)
    //            ->companyId($this->getSession()->getCompanyId())
    //            ->where(function ($query) use ($type, $UploadState) {
    //                if ($type == 4) {
    //                    $where = [
    //                        ['product_result', '=', ToSellTaskProduct::RESULT_THREE],
    //                        ['price_result', '=', ToSellTaskProduct::RESULT_THREE],
    //                        ['stock_result', '=', ToSellTaskProduct::RESULT_THREE],
    //                        ['end_result', '=', ToSellTaskProduct::END_TRUE],
    //                        ['upload_state', '=', $UploadState]
    //                    ];
    //                    $query->where($where);
    //                } elseif ($type == 5) {
    //                    $query->where('upload_state', '=', $UploadState)
    //                        ->where(function ($query) use ($type, $UploadState) {
    //                            $query->orWhere('product_result', '=', ToSellTaskProduct::RESULT_FOUR)
    //                                ->orWhere('price_result', '=', ToSellTaskProduct::RESULT_FOUR)
    //                                ->orWhere('stock_result', '=', ToSellTaskProduct::RESULT_FOUR)
    //                                ->orWhere('end_result', '=', ToSellTaskProduct::RESULT_FOUR);
    //                        });
    //                } else {
    //                    $UploadState && $query->where('upload_state', $UploadState);
    //                }
    //            })
    ////            ->UploadState($UploadState)
    ////            ->EndResult($endResult)
    //            ->StoreId($store_id)
    //            ->asin($asin)
    //            ->StoreIds($store_ids)
    //            ->oemId($this->getSession()->getOemId())
    //            ->sort()
    //            ->select($field)
    //            ->paginate($this->limit);
    //    }

    //    /**
    //     * 获取待上传产品
    //     * User: CCY
    //     * Date: 2020/12/4
    //     * @param $ids
    //     * @param $store_id
    //     * @return mixed
    //     */
    //    public function getWaitList($ids, $store_id)
    //    {
    //        return ToSellTaskProduct::query()
    //            ->companyId($this->getSession()->getCompanyId())
    //            ->UploadState(ToSellTaskProduct::UPLOAD_ONE)
    //            ->StoreId($store_id)
    //            ->Ids($ids)
    //            ->oemId($this->getSession()->getOemId())
    //            ->get();
    //    }

    //    /**
    //     * 添加同步任务集
    //     * User: CCY
    //     * Date: 2020/12/4
    //     * @param $storeId
    //     * @return int
    //     */
    //    public function addTask($storeId)
    //    {
    //        $insertData = [
    //            'is_finish'   => ToSellTask::FINISH_FALSE,
    //            'store_id'    => $storeId,
    //            'end_result'  => json_encode([]),
    //            'platform_id' => 1,
    //            'user_id'     => $this->getSession()->getUserId(),
    //            'group_id'    => $this->getSession()->getGroupId(),
    //            'company_id'  => $this->getSession()->getCompanyId(),
    //            'oem_id'      => $this->getSession()->getOemId(),
    //            'created_at'  => $this->datetime
    //        ];
    //        return ToSellTask::query()->insertGetId($insertData);
    //    }

    //    /**
    //     * 添加关联关系
    //     * User: CCY
    //     * Date: 2020/12/4
    //     * @param $insertRelation
    //     * @return bool
    //     */
    //    public function addTaskRelation($insertRelation)
    //    {
    //        return ToSellTaskProductRelation::query()->insert($insertRelation);
    //    }

    //    /**
    //     * 更新同步产品
    //     * User: CCY
    //     * Date: 2020/12/4
    //     * @param $id
    //     * @param $updateData
    //     * @return int
    //     */
    //    public function updateTaskProduct($id, $updateData)
    //    {
    //        return ToSellTaskProduct::query()->where('id', $id)->update($updateData);
    //    }

    //    /**
    //     * 更新同步产品
    //     * User: CCY
    //     * Date: 2020/12/4
    //     * @param $id
    //     * @param $updateData
    //     * @return int
    //     */
    //    public function updateTaskProducts($ids, $updateData)
    //    {
    //        return ToSellTaskProduct::query()->Ids($ids)->update($updateData);
    //    }

    //    public function updateTaskProductUpload($ids)
    //    {
    //        return ToSellTaskProduct::query()
    //            ->Ids($ids)
    //            ->where('product_result', ToSellTaskProduct::RESULT_TWO)
    //            ->update(['product_result' => ToSellTaskProduct::RESULT_THREE]);
    //    }

    //    public function updateTaskProductPrice($ids)
    //    {
    //        return ToSellTaskProduct::query()
    //            ->Ids($ids)
    //            ->where('price_result', ToSellTaskProduct::RESULT_TWO)
    //            ->update(['price_result' => ToSellTaskProduct::RESULT_THREE]);
    //    }

    //    public function updateTaskProductStock($ids)
    //    {
    //        return ToSellTaskProduct::query()
    //            ->Ids($ids)
    //            ->where('stock_result', ToSellTaskProduct::RESULT_TWO)
    //            ->update(['stock_result' => ToSellTaskProduct::RESULT_THREE]);
    //    }

    //    /**
    //     * 获取未结束任务下的店铺id
    //     * User: CCY
    //     * Date: 2020/12/4
    //     * @return mixed
    //     */
    //    public function getStoreIdsByTask()
    //    {
    //        return ToSellTask::query()
    //            ->companyId($this->getSession()->getCompanyId())
    //            ->IsFinish(ToSellTask::FINISH_FALSE)
    //            ->oemId($this->getSession()->getOemId())
    //            ->select(['store_id'])
    //            ->get();
    //    }

    //    /**
    //     * 根据店铺获取任务信息
    //     * User: CCY
    //     * Date: 2020/12/4
    //     * @param $storeId
    //     * @return mixed
    //     */
    //    public function getTaskByStoreId($storeId, $beforeTime)
    //    {
    //        $field = ['id', 'upload_session_state', 'upload_session_id', 'price_session_state', 'price_session_id', 'stock_session_state', 'stock_session_id', 'end_result', 'is_finish', 'operate_user_id'];
    //        return ToSellTask::query()
    //            ->companyId($this->getSession()->getCompanyId())
    //            ->StoreId($storeId)
    //            ->IsFinish(ToSellTask::FINISH_FALSE)
    //            ->oemId($this->getSession()->getOemId())
    //            ->where(function ($query) use ($beforeTime) {
    //                $query->where('operate_user_id', '=', $this->getSession()->getUserId())
    //                    ->orWhere(function ($query) use ($beforeTime) {
    //                        $query->where('updated_at', '<', $beforeTime)
    //                            ->orWhere('operate_user_id', '=', 0);
    //                    });
    //            })
    //            ->select($field)
    //            ->get();
    //    }

    //    /**
    //     * 获取任务下对应产品id
    //     * User: CCY
    //     * Date: 2020/12/4
    //     * @param $taskIds
    //     * @return mixed
    //     */
    //    public function getProductIdByTaskIds($taskIds)
    //    {
    //        return ToSellTaskProductRelation::query()
    //            ->TaskIds($taskIds)
    //            ->select(['task_id', 'task_product_id'])
    //            ->get();
    //    }

    //    /**
    //     * 获取任务下对应产品id
    //     * User: CCY
    //     * Date: 2020/12/4
    //     * @param $taskIds
    //     * @return mixed
    //     */
    //    public function getProductIdByTaskId($taskId)
    //    {
    //        return ToSellTaskProductRelation::query()
    //            ->TaskId($taskId)
    //            ->select(['task_id', 'task_product_id'])
    //            ->get();
    //    }

    //    /**
    //     * 获取任务下产品
    //     * User: CCY
    //     * Date: 2020/12/4
    //     * @param $taskProductIds
    //     * @return mixed
    //     */
    //    public function getTaskProductByIds($taskProductIds)
    //    {
    //        $field = ['id', 'sku', 'asin', 'price', 'unit', 'stock', 'stock_up', 'product_state', 'product_json', 'type', 'product_result', 'price_result', 'stock_result', 'upload_state'];
    //        return ToSellTaskProduct::query()
    //            ->companyId($this->getSession()->getCompanyId())
    //            ->UploadState(ToSellTaskProduct::UPLOAD_TWO)
    //            ->Ids($taskProductIds)
    //            ->oemId($this->getSession()->getOemId())
    //            ->select($field)
    //            ->get();
    //    }

    //    /**
    //     * 更新任务信息
    //     * User: CCY
    //     * Date: 2020/12/4
    //     * @param $id
    //     * @param $updateData
    //     * @return int
    //     */
    //    public function updateTask($id, $updateData)
    //    {
    //        return ToSellTask::query()->where('id', $id)->update($updateData);
    //    }

    //    /**
    //     * 更新任务信息
    //     * User: CCY
    //     * Date: 2020/12/9
    //     * @param $ids
    //     * @param $updateData
    //     * @return int
    //     */
    //    public function updateTaskByIds($ids, $updateData)
    //    {
    //        return ToSellTask::query()->whereIn('id', $ids)->update($updateData);
    //    }

    //    /**
    //     * 删除已完成或未开始产品
    //     * User: CCY
    //     * Date: 2020/12/7
    //     * @param $id
    //     * @return mixed
    //     */
    //    public function delSyncProductById($id)
    //    {
    //        return ToSellTaskProduct::query()
    //            ->where('id', $id)
    //            ->companyId($this->getSession()->getCompanyId())
    //            ->where('upload_state', '!=', ToSellTaskProduct::UPLOAD_TWO)
    //            ->oemId($this->getSession()->getOemId())
    //            ->delete();
    //    }

    //    /**
    //     * 获取同步产品信息
    //     * User: CCY
    //     * Date: 2020/12/8
    //     * @param $id
    //     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
    //     */
    //    public function getTaskProductById($id)
    //    {
    //        return ToSellTaskProduct::query()->find($id);
    //    }

    //    /**
    //     * 检测重复性
    //     * User: CCY
    //     * Date: 2020/12/8
    //     * @param $hash_code
    //     * @return mixed
    //     */
    //    public function checkTaskProductRepeat($hash_code)
    //    {
    //        return ToSellTaskProduct::query()
    //            ->CompanyId($this->getSession()->getCompanyId())
    //            ->HashCode($hash_code)
    //            ->oemId($this->getSession()->getOemId())
    //            ->exists();
    //    }

    //    /**
    //     * 检测跟卖产品在同步中的数据
    //     * User: CCY
    //     * Date: 2020/12/8
    //     * @param $pid
    //     * @return mixed
    //     */
    //    public function checkTaskProductSync($pid)
    //    {
    //        return ToSellTaskProduct::query()
    //            ->CompanyId($this->getSession()->getCompanyId())
    //            ->Pid($pid)
    //            ->UploadState(ToSellTaskProduct::UPLOAD_TWO)
    //            ->oemId($this->getSession()->getOemId())
    //            ->exists();
    //    }


    //    /**
    //     * 添加同步产品修改日志
    //     * User: CCY
    //     * Date: 2020/12/8
    //     * @param $task_product_id
    //     * @param $pid
    //     * @param $remark
    //     * @return bool
    //     */
    //    public function addProductLog($task_product_id, $pid, $remark)
    //    {
    //        $insertData = [
    //            'remark'          => $remark,
    //            'pid'             => $pid,
    //            'task_product_id' => $task_product_id,
    //            'truename'        => $this->getSession()->getTruename(),
    //            'user_id'         => $this->getSession()->getUserId(),
    //            'group_id'        => $this->getSession()->getGroupId(),
    //            'company_id'      => $this->getSession()->getCompanyId(),
    //            'oem_id'          => $this->getSession()->getOemId(),
    //            'created_at'      => $this->datetime,
    //            'updated_at'      => $this->datetime
    //        ];
    //        return ToSellTaskProductLog::query()->insert($insertData);
    //    }

    //    /**
    //     * 添加日志
    //     * User: CCY
    //     * Date: 2020/12/9
    //     * @param $insertData
    //     * @return bool
    //     */
    //    public function addTaskProductLog($insertData)
    //    {
    //        return ToSellTaskProductLog::query()->insert($insertData);
    //    }

    //    /**
    //     * 获取同步产品修改日志
    //     * User: CCY
    //     * Date: 2020/12/8
    //     * @param $task_product_id
    //     * @return mixed
    //     */
    //    public function getLogPage($task_product_id)
    //    {
    //        return ToSellTaskProductLog::query()
    //            ->taskProductId($task_product_id)
    //            ->companyId($this->getSession()->getCompanyId())
    //            ->oemId($this->getSession()->getOemId())
    //            ->sort()
    //            ->select(['id', 'remark', 'truename', 'created_at'])
    //            ->paginate($this->limit);
    //    }

    //    /**
    //     * 删除跟卖产品同步编辑日志
    //     * User: CCY
    //     * Date: 2020/12/8
    //     * @param $pid
    //     * @return mixed
    //     */
    //    public function delProductLog($pid)
    //    {
    //        return ToSellTaskProductLog::query()->Pid($pid)->delete();
    //    }


    //    /**
    //     * 删除跟卖产品同步数据
    //     * User: CCY
    //     * Date: 2020/12/8
    //     * @param $pid
    //     * @return mixed
    //     */
    //    public function delSyncProductByPid($pid)
    //    {
    //        return ToSellTaskProduct::query()
    //            ->where('pid', $pid)
    //            ->companyId($this->getSession()->getCompanyId())
    //            ->where('upload_state', '!=', ToSellTaskProduct::UPLOAD_TWO)
    //            ->oemId($this->getSession()->getOemId())
    //            ->delete();
    //    }

    //    /**
    //     * 删除跟卖产品
    //     * User: CCY
    //     * Date: 2020/12/8
    //     * @param $id
    //     * @param $store_id
    //     * @return mixed
    //     */
    //    public function delSellProductById($id, $store_id)
    //    {
    //        return ToSellModel::query()
    //            ->where('id', $id)
    //            ->companyId($this->getSession()->getCompanyId())
    //            ->StoreId($store_id)
    //            ->oemId($this->getSession()->getOemId())
    //            ->delete();
    //    }



    public async Task<object> Init()
    {
        bool toSellPull = true, toSellUpload = true, toSellDelete = true;
        var rule = new
        {
            ToSellPull = toSellPull,
            ToSellUpload = toSellUpload,
            ToSellDelete = toSellDelete
        };
        var storeList = await storeService.CurrentUserGetStores();
        var continents = storeList.Where(s => s.Continent > 0).GroupBy(s => s.Continent).Select(g => new
        {
            label = Extensions.EnumExtension.GetDescription(g.Key),
            value = g.Key,
            children = g.Select(gg => new

            {
                label = gg.Name,
                value = gg.ID,
            })
        });
        return await Task.FromResult( new { rule, stores = continents });
    }

    public Task<bool> Info(int id)
    {
        throw new NotImplementedException();
    }

    public Task<bool> Destroy(int id)
    {
        throw new NotImplementedException();
    }
}