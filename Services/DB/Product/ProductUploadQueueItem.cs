﻿using ERP.Export.Queue;
using ERP.Models.DB.Stores;

namespace ERP.Services.Product;

public record ProductUploadQueueItem(SyncUploadTask Task) : IQueueItem;
