using System.Text;
using System.Text.RegularExpressions;
using ERP.Data;
using ERP.Data.Products;
using ERP.Enums;
using ERP.Exceptions.Export;
using ERP.Export.Models;
using ERP.Export.Queue;
using ERP.Export.Templates;
using ERP.Extensions;
using ERP.Models.DB.Product;
using ERP.Models.DB.Stores;
using ERP.Models.Export;
using ERP.Models.Product;
using ERP.Models.ProductNodeType.Amazon;
using ERP.Models.Stores;
using ERP.Models.View.Products.Product;
using ERP.Services.DB.ProductType;
using ERP.Services.Provider;
using ERP.Services.Stores;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NPOI.SS.UserModel;
using NPOI.XSSF.Streaming;
using NPOI.XSSF.UserModel;
using Sy.Text;
using OEM = ERP.Models.Setting.OEM;
using Unit = MediatR.Unit;
using UnitCache = ERP.Services.Caches.Unit;

namespace ERP.Services.Product;

public class ProductUploadBackground : IQueueHandler<ProductUploadQueueItem>
{
    private readonly ILogger<ProductUploadBackground> _logger;
    private readonly ExcelTemplateFactory _templateFactory;
    private readonly UnitCache _unitCache;
    private readonly IServiceProvider _serviceProvider;
    private ConvertProduct _convertProduct;

    public ProductUploadBackground(IServiceProvider serviceProvider, ILogger<ProductUploadBackground> logger,
        ExcelTemplateFactory templateFactory, UnitCache unitCache)
    {
        _logger = logger;
        _templateFactory = templateFactory;
        _unitCache = unitCache;
        _serviceProvider = serviceProvider.CreateScope().ServiceProvider;
    }

    public async Task<Unit> Handle(ProductUploadQueueItem request, CancellationToken stoppingToken)
    {
        _logger.LogError($"ProductUploadBackground Handle");
        var oemCache = _serviceProvider.GetRequiredService<Caches.OEM>();
        var oem = oemCache.Get(request.Task.OEMID)!;
        var sp = _serviceProvider.CreateScopeWithOem(oem).ServiceProvider;

        var excelBytesResult = await GetExcelBytes(sp, request, stoppingToken);
        if (excelBytesResult.IsFailed)
            return Unit.Value;

        _logger.LogInformation("上传任务[{TaskId}]生成表格完成, 准备执行上传api操作", request.Task.ID);
        var productHostService = sp.GetRequiredService<ProductHost>();
        var dbContext = sp.GetRequiredService<DBContext>();
        var task = dbContext.Entry(request.Task).Entity;
        var plst = await dbContext.productUpload.Where(p => p.SyncUploadTaskId == task.ID).ToListAsync();
        if (!plst.Any())
        {
            _logger.LogInformation("ProductUploadBackground Handle have no upload");
            return Unit.Value;
        }

        var productUpload = plst.FirstOrDefault();

        _logger.LogInformation($"ProductUploadBackground Handle：{JsonConvert.SerializeObject(productUpload)}");
        var type = JsonConvert.DeserializeObject<JObject>(productUpload.Ext);
        string ptype = "";
        if (type.GetValue("feed_product_type") != null)
        {
            ptype = type.GetValue("feed_product_type").ToString();
        }
        else
        {
            _logger.LogInformation("ProductUploadBackground Handle feed_product_type is null");
            return Unit.Value;
        }

        var productParams = new UploadProductParams
        {
            Pids = plst.Select(x => x.ProductId).ToList(),
            ProductType = ptype,
            PathName = productUpload.PathName,
            PathNodes = productUpload.PathNodes,
            Unit = productUpload.Unit,
            Lang = productUpload.Languages,
            MarketPlace = productUpload.MarketPlace,
            Bytes = excelBytesResult.Value,
        };
        _logger.LogInformation($"ProductUploadBackground Handle PushProduct");
        try
        {
            await productHostService.PushProduct(productUpload.StoreId, productUpload.UserID, productUpload.OEMID,
                productParams);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "上传任务[{TaskId}]调用api失败", task.ID);
            task.MarkFailed($"调用api失败: {e.Message}");
            await dbContext.SaveChangesAsync();
        }

        return Unit.Value;
    }

    private async Task<Result<byte[]>> GetExcelBytes(IServiceProvider sp, ProductUploadQueueItem request,
        CancellationToken stoppingToken)
    {
        var result = await GenerateWorkbook(sp, request, stoppingToken);
        if (result.IsFailed)
            return Result.Fail(result.Errors);

        var workbook = result.Value;
        using var ms = new MemoryStream();
        workbook.Write(ms);
        workbook.Close();
        // 删除临时文件
        ((SXSSFWorkbook)workbook).Dispose();
        return Result.Ok(ms.ToArray());
    }

    public async Task OnStart(ITaskQueue<ProductUploadQueueItem> queue, CancellationToken cancellationToken)
    {
        var dbContext = _serviceProvider.CreateScope().ServiceProvider.GetRequiredService<DBContext>();
        var lockedTasks = await LoadAsync(dbContext, cancellationToken, true);
        var exportTasks = await LoadAsync(dbContext, cancellationToken);

        EnqueueTasks(exportTasks);

        var ids = lockedTasks.Select(t => t.ID).ToArray();
        if (lockedTasks.Any())
        {
            do
            {
                await Task.Delay(3000, cancellationToken);
                lockedTasks = await LoadAsync(dbContext, cancellationToken, true, ids);
            } while (lockedTasks.Any() && !cancellationToken.IsCancellationRequested);

            exportTasks = await LoadAsync(dbContext, cancellationToken, false, ids);

            EnqueueTasks(exportTasks);
        }

        void EnqueueTasks(IEnumerable<SyncUploadTask> tasks)
        {
            foreach (var task in tasks)
                queue.Enqueue(new ProductUploadQueueItem(task));
        }

        static Task<SyncUploadTask[]> LoadAsync(DBContext context, CancellationToken cancellationToken,
            bool locked = false,
            int[]? ids = null)
        {
            return context.SyncUploadTasks
                .WhenWhere(ids is not null, t => ids!.Contains(t.ID))
                .Where(t => t.State <= UploadResult.Uploading && t.Locked == locked)
                .OrderBy(t => t.ID)
                .ToArrayAsync(cancellationToken);
        }
    }

    public async Task<Result<IWorkbook>> GenerateWorkbook(IServiceProvider sp, ProductUploadQueueItem request,
        CancellationToken stoppingToken)
    {
        var dbContext = sp.GetRequiredService<DBContext>();
        _convertProduct = sp.GetRequiredService<ConvertProduct>();
        var entry = dbContext.Entry(request.Task);
        await entry.ReloadAsync(stoppingToken);

        if (entry.Entity.Locked)
            return Result.Fail("任务已锁定");
        await entry.Reference(t => t.User).LoadAsync(stoppingToken);
        await entry.Reference(t => t.OEM).LoadAsync(stoppingToken);
        await entry.Reference(t => t.OEM).TargetEntry!.Reference(t => t.Oss).LoadAsync(stoppingToken);
        var task = request.Task;

        var productService = sp.GetRequiredService<ProductService>();
        var syncProductService = sp.GetRequiredService<SyncProductService>();

        task.Locked = true;
        await dbContext.SaveChangesAsync(stoppingToken);

        var user = task.User!;


        _logger.LogInformation("开始{Msg}执行任务{ExportTask}",
            task.State == UploadResult.None ? "首次" : "继续", task);

        var ids = await dbContext.productUpload
            .Where(p => p.CompanyID == user.CompanyID && p.UserID == user.Id)
            .Where(m => m.SyncUploadTaskId == task.ID)
            .Select(m => m.Spid)
            .ToArrayAsync(stoppingToken);
        if (!ids.Any())
        {
            task.MarkFailed("导出产品为空");
            await dbContext.SaveChangesAsync();
            return Result.Fail("导出产品为空");
        }

        Result<IWorkbook> result;
        try
        {
            result = await ExportExcel(dbContext, syncProductService, productService, task, ids,
                stoppingToken);
            if (result.IsFailed)
                task.MarkFailed(result.Errors.First().Message);
        }
        catch (Exception e)
        {
            task.MarkFailed(e.ToString());
            _logger.LogError(e, "导出表格异常");
            result = Result.Fail(e.ToString());
        }

        task.Locked = false;
        await dbContext.SaveChangesAsync(default);
        return result;
    }


    private async Task<Result<IWorkbook>> ExportExcel(DBContext dbContext, SyncProductService syncProductService,
        ProductService productService, SyncUploadTask task, int[] ids, CancellationToken cancellationToken)
    {
        if (!ids.Any())
            return Result.Fail("数据为空,请重试");

        var factory = _templateFactory[TemplateType.Amazon];
        if (factory is null)
            return Result.Fail("无法找到导出的数据类型");

        IWorkbook workbook = new SXSSFWorkbook(new XSSFWorkbook());

        Dictionary<ISheet, int> sheets = new();

        var headers = await factory.GetHead("");
        if (headers is null) return Result.Fail("读取表头出错");
        foreach (var header in headers)
        {
            int rowIndex = 0;
            var sheetName = header.Key;
            var sheet = workbook.CreateSheet(sheetName);
            rowIndex += FullData(sheet, rowIndex, header.Value);
            sheets.Add(sheet, rowIndex);
        }

        task.State = UploadResult.Uploading;
        await dbContext.SaveChangesAsync(default);

        var unit = new List<string>();

        var unitCache = _unitCache.List()!;

        var rate = unit.Count > 0
            ? unitCache.Where(x => unit.Contains(x.Sign)).ToList()
            : unitCache.Where(x => x.Sign == task.User!.UnitConfig).ToList();
        if (rate.Count == 0)
        {
            return Result.Fail("汇率不可为空");
        }

        var curCount = 0;
        do
        {
            // 正在获取产品数据...
            var proId = ids.Skip(curCount).Take(3).ToList();

            var cellDataInfo = await Export_Get_Products(dbContext, syncProductService, productService, task, proId);
            var productUploads = await dbContext.productUpload.Where(a => a.SyncUploadTaskId == task.ID)
                .Where(a => proId.Contains(a.Spid))
                .ToDictionaryAsync(s => s.ProductId, s => s);
            if (cellDataInfo.Any())
            {
                foreach (var cellData in cellDataInfo)
                {
                    try
                    {
                        if (cellData is { State: true, Data: not null })
                        {
                            var parameter = productUploads[cellData.ID].Ext;
                            var role = JsonConvert.DeserializeObject<ExcleRole>(productUploads[cellData.ID].Params)!;
                            var data = await factory.GetData(cellData.Data, role, parameter,
                                rate.Select(r => new ERP.Export.Models.Unit(r.Name, r.Sign, r.Rate, r.RefreceTime))
                                    .ToList());
                            if (data is not null)
                            {
                                Dictionary<ISheet, int> tempSheets = new();
                                var index = 0;
                                foreach (var sheet in sheets)
                                {
                                    if (data.Count > index)
                                    {
                                        var rowIndex = sheet.Value;
                                        try
                                        {
                                            rowIndex += FullData(sheet.Key, sheet.Value, data[index]);
                                        }
                                        catch (Exception ex)
                                        {
                                            _logger.LogError(ex, ex.Message);
                                        }

                                        tempSheets.Add(sheet.Key, rowIndex);
                                        index += 1;
                                    }
                                }

                                sheets = tempSheets;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        cellData.State = false;
                        cellData.CodeInfo = e is ExportException ? e.Message : e.ToString();
                        _logger.LogError(e, "任务{TaskId}导出产品{Pid}出错", task.ID, cellData.ID);
                    }

                    _logger.LogInformation("导出任务{Task}构建产品{Id}: {Message}", task, cellData.ID, cellData.CodeInfo);
                }
            }

            curCount += proId.Count;
            await dbContext.SaveChangesAsync(default);
        } while (ids.Length > curCount && !cancellationToken.IsCancellationRequested);

        await dbContext.SaveChangesAsync(default);

        return Result.Ok(workbook).WithSuccess("导出完成");
    }

    private async Task<List<CellData>> Export_Get_Products(DBContext dbContext, SyncProductService syncProductService,
        ProductService productService, SyncUploadTask task, List<int> ids)
    {
        var suffixParam = task.User is { ProductConfig.ExportImgUrl: true } ? $"?t={DateTime.Now.GetTimeStampMilliseconds()}" : "";

        //获取SyncProduct 产品信息
        var tempSyncProduct = await syncProductService.GetSyncProduct(ids);
        //产品ids
        var pids = tempSyncProduct.Select(a => a.Pid);
        var syncProduct = tempSyncProduct.ToDictionary(a => a.Pid, a => a);
        //查询产品
        var products = await dbContext.Product.Where(a => a.CompanyID == task.CompanyID)
            .Where(a => pids.Contains(a.ID))
            .ToListAsync();
        var productUploads = await dbContext.productUpload.Where(a => a.SyncUploadTaskId == task.ID)
            .Where(a => ids.Contains(a.Spid))
            .ToDictionaryAsync(s => s.ProductId, s => s);
        var templateIds = productUploads.Select(kv => kv.Value.ChangePriceId).Where(id => id != null).ToArray();
        var templates =
            await dbContext.ChangePriceTemplate.Where(c => templateIds.Contains(c.ID))
                .ToDictionaryAsync(c => c.ID, c => c);

        var listProductInfo = new List<CellData>();
        //循环获取产品
        foreach (var product in products)
        {
            try
            {
                var productUpload = productUploads[product.ID];
                List<ProductPriceParams>? changePriceTemplate = null;
                if (productUpload.ChangePriceId != null &&
                    templates.TryGetValue(productUpload.ChangePriceId.Value, out var template))
                    changePriceTemplate = JsonConvert.DeserializeObject<List<ProductPriceParams>>(template.Value);
                listProductInfo.Add(await AssembleProductInfo(productService, task, product, syncProduct[product.ID],
                    productUploads[product.ID].Languages, changePriceTemplate, suffixParam));
            }
            catch (ExportException e)
            {
                listProductInfo.Add(new CellData() { ID = product.ID, State = false, CodeInfo = e.Message });
            }
            catch (Exception e)
            {
                listProductInfo.Add(new CellData()
                    { ID = product.ID, State = false, CodeInfo = $"未知错误, 请联系客服. error: {e}" });
                _logger.LogError(e, "导出任务{Task}构建产品{Id}出错", task, product.ID);
            }
        }

        return listProductInfo;
    }

    public async Task<CellData> AssembleProductInfo(ProductService productService, SyncUploadTask task,
        ProductModel productModel, SyncProductModel syncProductModel,
        Languages language, List<ProductPriceParams>? @params = null, string suffixParam = "")
    {
        var pid = productModel.ID;
        var langId = language == Languages.Default ? 0 : (int)Math.Log2((long)language) + 1;

        var defaultItemModel = await productService.GetItemInfoByLangId(pid, 0);

        var langItemModel = await productService.GetItemInfoByLangId(pid, langId);

        var itemModel = langItemModel ?? defaultItemModel ?? throw new AssembleException($"Pid[{pid}]数据为空#1");

        var proStruct = await productService.GetProductStruct(itemModel.FilePath, productModel);

        if (@params is not null)
        {
            var unit = productModel.Coin.IsNotEmpty() ? productModel.Coin : task.User!.UnitConfig;
            // 金额
            productModel.ChangePrices(proStruct, @params, unit);
        }

        string? showImage = null;
        if (proStruct.MainImage is not null)
        {
            showImage = GetImageUrl(task.OEM!, proStruct.MainImage.Path ?? proStruct.MainImage.Url) + suffixParam;
        }

        var productImages = productModel.Gallery;

        //判断是否有无变体
        var autoGenerate = AutoGenerate(proStruct.Variants.Attributes);
        var skuGenerator = new SkuGenerator(syncProductModel.SkuType, syncProductModel.SourceId,
            syncProductModel.SkuSuffix, autoGenerate);
        //主产品sku
        var mainSku = skuGenerator.GenerateMain(syncProductModel.Sku);

        //变体
        var variants = proStruct.Variants;

        var removeDescriptionHtml = true;
        List<ProductsItem> products = new List<ProductsItem>();
        if (variants.Count > 0)
        {
            for (var index = 0; index < variants.Count; index++)
            {
                var item = variants[index];
                if (item.State == VariantStateEnum.Disable)
                {
                    continue;
                }

                var name = item.GetTitle() ?? proStruct.GetTitle()!;
                var desc = _convertProduct.GetProductDesc(item.GetDescription() ?? proStruct.GetDescription()!,
                    task.OEM!);
                var sketch = item.GetSketches() is { Count: > 0 }
                    ? item.GetSketches()!
                    : (proStruct.GetSketches() is { Count: > 0 } ? proStruct.GetSketches()! : new List<string>(0));
                var bulletPoint = sketch;

                var keyword = item.GetKeyword() ?? proStruct.GetKeyword();

                // 字典的顺序可能是乱的, 此处重新排序
                var ids = item.Ids.OrderBy(m => m.Key).Select(m => m.Value).ToList();

                var variantDto = new VariantDto(item);

                var attributesName =
                    MakeAttribute(syncProductModel, proStruct.Variants.Attributes, variantDto.Ids);

                var sku = skuGenerator.GenerateVariant(item.Sku, index, attributesName);

                // var vImages = GetFullUrl(item.Images, productImages);
                products.Add(new ProductsItem()
                {
                    Name = MakeVariantTitle(syncProductModel, name, attributesName),
                    Description = RemoveHtmlTag(desc, removeDescriptionHtml, TemplateType.Amazon),
                    BulletPoint = bulletPoint,
                    SearchTerms = keyword,
                    Values = ids,
                    Sku = sku,
                    MainImage = item.Images.Main.HasValue
                        ? GetFullUrl(productImages, item.Images.Main, task.OEM!)
                        : null,
                    Images = GetFullUrl(productImages, item.Images.Affiliate, task.OEM!),
                    Quantity = item.Quantity,
                    Price = item.Sale.Value.ToString(),
                    Cost = item.Cost.Value.ToString(),
                    ProduceCode = item.Code ?? string.Empty,
                    //todo Fields
                    SID = item.Sid
                });
            }
        }


        var sketches = proStruct.GetSketches();
        var mainBulletPoint = sketches;
        var prices = proStruct.GetPrices()!;
        var data = new DataItem()
        {
            PID = productModel.ID,
            RawPid = productModel.Pid,
            Name = proStruct.GetTitle()!,
            Sku = mainSku,
            Source = productModel.Source,
            Quantity = proStruct.Quantity,
            ShowImage = showImage,
            Price = new()
            {
                Cost = new() { Max = prices.Cost.Max, Min = prices.Cost.Min },
                Sale = new() { Max = prices.Sale.Max, Min = prices.Sale.Min }
            },
            Description = RemoveHtmlTag(_convertProduct.GetProductDesc(proStruct.GetDescription()!, task.OEM!),
                removeDescriptionHtml, TemplateType.Amazon),
            BulletPoint = mainBulletPoint,
            SearchTerms = proStruct.GetKeyword(),
            Category = productModel.CategoryModel?.Name,
            Attribute = GetProAttribute(proStruct.Variants.Attributes),
            Products = products,
            ProductID = productModel.Pid,
        };

        return new CellData()
        {
            ID = productModel.ID,
            Data = data,
            CodeInfo = "Success",
            State = true
        };
    }

    /// <summary>
    /// 去除文本中的html标签, 保留amazon的br标签
    /// </summary>
    /// <param name="description"></param>
    /// <param name="shouldRemove"></param>
    /// <param name="templateType"></param>
    /// <returns></returns>
    private static string RemoveHtmlTag(string? description, bool shouldRemove, TemplateType templateType)
    {
        if (string.IsNullOrWhiteSpace(description))
            return string.Empty;
        if (!shouldRemove && templateType != TemplateType.Amazon)
            return description;

        var reserves = new List<string>() { "img" };
        if (templateType == TemplateType.Amazon)
            reserves.Add("br");

        if (shouldRemove)
            description = Html.RemoveTag(description, Html.RemoveType.Script_Style, reserves.ToArray());

        if (templateType == TemplateType.Amazon)
            description = description.Replace("\r\n", "<br>").Replace("\n", "<br>");

        return description;
    }

    private int FullData(ISheet sheet, int startRow, List<List<string>> data)
    {
        var rowNum = data.Count;
        var exportNum = 0;
        for (var i = 0; i <= rowNum - 1; i++)
        {
            var row = sheet.CreateRow(startRow + i);
            var rowData = data[i];
            var colNum = rowData.Count;
            var canExport = true;
            // 检测当前数据中是否有超过最大字符的数据,存在则跳过导出
            rowData.ForEach(item =>
            {
                if (item.IsNotNull() && item.Length > 32767)
                {
                    canExport = false;
                    _logger.LogWarning($"超过最大字符的数据{rowData[1]}{item.Substring(0, 50)}");
                    //Log(I18N.Instance.Frm_Export3_Log30(RowData[1], item.Substring(0, 50)));
                    return;
                }
            });
            // 可导出
            if (canExport)
            {
                exportNum += 1;
                for (var colIndex = 0; colIndex <= colNum - 1; colIndex++)
                {
                    row.CreateCell(colIndex).SetCellValue(rowData[colIndex]);
                }
            }
        }

        return exportNum;
    }

    private string GetImageUrl(OEM oem, string str)
    {
        var src = str.Trim('/');
        if (src.StartsWith("http"))
        {
            return src;
        }

        var domain = oem.Oss!.BucketDomain;
        // if (oem.ID != 8)
        // {
        //     domain = "overseasimg4.liulergou.com";
        // }
        
        return $"http://{domain}/{src}";
    }

    /// <summary>
    /// 判断产品是否为无变体产品, 单变体产品当作无变体产品处理
    /// </summary>
    /// <param name="attributes"></param>
    /// <returns></returns>
    private static bool AutoGenerate(Data.Products.Attributes attributes) =>
        attributes.items is { Count: 1 } && attributes.items.First().Value.Value.Length == 1;

    public string MakeVariantTitle(SyncProductModel syncProductModel, string title, string attributeName)
    {
        if (syncProductModel.TitleSuffix == SyncProductModel.TitleSuffixs.FALSE)
            return title;
        return $"{title}-{attributeName}";
    }

    public string MakeAttribute(SyncProductModel syncProductModel, Data.Products.Attributes attributes,
        List<Ids>? ids)
    {
        if (syncProductModel.SkuSuffix == SyncProductModel.SkuSuffixs.FALSE &&
            syncProductModel.TitleSuffix == SyncProductModel.TitleSuffixs.FALSE)
        {
            return string.Empty;
        }

        if (ids is null)
            return string.Empty;

        var str = new StringBuilder();
        foreach (var item in ids)
        {
            if (attributes.items.TryGetValue(item.Tag, out var attr) && attr.Value.Length > item.Val)
            {
                str.Append($"{attr.Value[item.Val]}-");
            }
        }

        if (str.Length > 0)
            str.Remove(str.Length - 1, 1);

        return str.ToString();
    }

    private List<string> GetFullUrl(VariantImages? variantImages,
        ERP.Data.Products.Images? images, OEM oem, string suffixParam)
    {
        if (images is not null && variantImages is not null && variantImages.Affiliate is not null &&
            variantImages.Affiliate.Count > 0)
            return images.Where(x => variantImages.Affiliate.Contains(x.ID))
                .Select(x => GetImageUrl(oem, x.Path ?? string.Empty) + suffixParam)
                .ToList();

        return new List<string>();
    }

    private class SkuGenerator
    {
        private readonly SyncProductModel.SkuTypes _skuType;
        private readonly int _sourceId;
        private readonly SyncProductModel.SkuSuffixs _skuSuffix;
        private readonly bool _hasVariant;
        private string? _mainSku;

        public SkuGenerator(SyncProductModel.SkuTypes skuType, int sourceId, SyncProductModel.SkuSuffixs skuSuffix,
            bool hasVariant)
        {
            _skuType = skuType;
            _sourceId = sourceId;
            _skuSuffix = skuSuffix;
            _hasVariant = hasVariant;
        }

        /// <summary>
        /// 生成主产品sku
        /// </summary>
        /// <param name="sku"></param>
        /// <returns></returns>
        public string GenerateMain(string sku)
        {
            return _mainSku ??= MakeSku(_skuType, _sourceId, sku);
        }

        /// <summary>
        /// 生成变体sku
        /// </summary>
        /// <param name="sku"></param>
        /// <param name="index"></param>
        /// <param name="attributeName"></param>
        /// <returns></returns>
        public string GenerateVariant(string? sku, int index, string attributeName)
        {
            // 变体单独设置sku | 追加属性 | 结果
            // 否            | 否      | 主产品sku + `-` + index
            // 否            | 是      | 主产品sku + `-` + 属性值
            // 是            | 否      | 变体sku
            // 是            | 是      | 变体sku + `-` + 属性值
            if (_mainSku is null)
                throw new Exception("需要先生成主产品sku");
            if (_hasVariant)
                return _mainSku;

            var useMainSku = string.IsNullOrEmpty(sku);
            var sb = new StringBuilder(useMainSku
                ? $"{_mainSku}"
                : MakeSku(_skuType, _sourceId, sku!));


            if (_skuSuffix == SyncProductModel.SkuSuffixs.TRUE && !string.IsNullOrWhiteSpace(attributeName))
            {
                var matches = Regex.Matches(attributeName, "[a-zA-Z0-9\\-]");
                sb.Append($"-{string.Join("", matches.Select(m => m.Value))}");
            }
            else if (useMainSku)
                sb.Append($"-{index}");

            return sb.ToString();
        }

        private static string MakeSku(SyncProductModel.SkuTypes skuType, int sourceId, string sku)
        {
            return skuType == SyncProductModel.SkuTypes.TYPE_TRUE
                ? Helpers.EnCodeSku(sourceId, sku)
                : sku;
        }
    }

    public string? GetFullUrl(Data.Products.Images? images, int? id, OEM oem)
    {
        if (images is null)
        {
            return null;
        }

        if (!id.HasValue)
        {
            return null;
        }

        var image = images.Where(m => m.ID == id).FirstOrDefault();
        if (image is null)
        {
            return null;
        }

        return GetImageUrl(oem, image.Path ?? image.Url);
    }

    public List<string>? GetFullUrl(Data.Products.Images? images, List<int>? ids, OEM oem)
    {
        if (images is not { Count: > 0 } || ids is null)
            return null;

        if (images.Distinct(i => i.ID).Count() != images.Count)
            throw new ExportException("变体图片重复");

        var result = ids.Join(images.ToDictionary(i => i.ID, i => i), id => id, kv => kv.Key, (id, kv) => kv.Value)
            .Select(i => GetImageUrl(oem, i.Path ?? i.Url))
            .ToList();

        return result;
    }

    public Dictionary<string, string[]> GetProAttribute(Data.Products.Attributes attributes)
    {
        var data = new Dictionary<string, string[]>();
        var items = attributes.items;

        foreach (var item in items)
        {
            if (!data.TryAdd(item.Value.Name, item.Value.Value))
                throw new AssembleException($"产品属性名称重复[{item.Value.Name}]");
        }

        return data;
    }
}