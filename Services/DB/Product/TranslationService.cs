using System.Net;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using ERP.Data;
using ERP.Enums;
using ERP.Exceptions.Product;
using ERP.Interface;
using Newtonsoft.Json;
using OrderSDK.Modles.Amazon.Runtime;

namespace ERP.Services.Product;

public class TranslationService : BaseWithUserService
{
    public TranslationService(DBContext dbContext, ISessionProvider sessionProvider) : base(dbContext, sessionProvider)
    {
    }

    protected const string APPID = "7d84a92b";

    protected const string APIKey = "b3c5560ef46ab1d5cd3bf4155671d305";

    protected const string APISecret = "NDQ3MjhmZjdmN2UwZWQ0NmMwNTNhMTUx";

    protected const string Url = "https://itrans.xf-yun.com/v1/its";

    public string GenerateAuthorization(string date)
    {
        var authorizationOrigin = new List<string>();
        authorizationOrigin.Add($"api_key=\"{APIKey}\"");
        authorizationOrigin.Add("algorithm=\"hmac-sha256\"");
        authorizationOrigin.Add("headers=\"host date request-line\"");
        authorizationOrigin.Add($"signature=\"{GenerateSignature(date)}\"");
        return Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Join(",", authorizationOrigin)));
    }

    private string GenerateSignature(string date)
    {
        var hostInfo = new Uri(Url);
        var signature_origin = $"host: {hostInfo.Host}\ndate: {date}\nPOST {hostInfo.AbsolutePath} HTTP/1.1";
        var sha = new HMACSHA256(Encoding.UTF8.GetBytes(APISecret));
        var bytes = Encoding.UTF8.GetBytes(signature_origin);
        var hash = sha.ComputeHash(bytes);
        return Convert.ToBase64String(hash);
    }

    /// <summary>
    /// 请求翻译
    /// </summary>
    /// <param name="form">原语种</param>
    /// <param name="to">目标语种</param>
    /// <param name="str">待翻译文本</param>
    /// <returns></returns>
    /// <exception cref="TranslationException"></exception>
    public async Task<TextVm> Send(string form, string to, string str)
    {
        var date = DateTime.UtcNow.GetDateTimeFormats('r')[0];
        var formData = AssemblyData(form, to, str);
        var client = new HttpClient();
        client.DefaultRequestHeaders.Date = DateTime.Now;
        var response =
            await client.PostAsync(GenerateUrl(date),
                new StringContent(JsonConvert.SerializeObject(formData), Encoding.UTF8, "application/json"));
        var result = JsonConvert.DeserializeObject<ResponseVm>(await response.Content.ReadAsStringAsync());

        if (result is null)
            throw new TranslationException("接口通讯异常");

        if (response.StatusCode == HttpStatusCode.Unauthorized)
            //缺少authorization参数 签名参数解析失败 签名参数解析失败
            throw new TranslationException("签名参数解析失败");

        if (response.StatusCode == HttpStatusCode.Forbidden)
            throw new TranslationException("时钟偏移校验失败");


        if (response.IsSuccessStatusCode)
        {
            if (result.Payload != null)
            {
                var textBytes = Convert.FromBase64String(result.Payload.Result.Text);
                return JsonConvert.DeserializeObject<TextVm>(Encoding.UTF8.GetString(textBytes))!;
            }
        }
        else
        {
            if (result.Header.Message.ToLower() == "no companion route found")
            {
                throw new TranslationException("产品原语种错误#5");
            }
        }

        throw new TranslationException(result.Header.Message);
    }

    private string GenerateUrl(string date)
    {
        var hostInfo = new Uri(Url);
        var query = new Dictionary<string, string>()
        {
            { "authorization", GenerateAuthorization(date) },
            { "host", hostInfo.Host },
            { "date", date }
        };
        var uri = Url;
        var a = uri + "?" + string.Join("&", query.Select(m => $"{m.Key}={Utils.UrlEncode(m.Value)}"));
        return a;
    }



    private TranslationDto AssemblyData(string from, string to, string str)
    {
        if (from == "default")
        {
            from = "en";
        }
        return new TranslationDto()
        {
            Header = new TranslationDto.HeaderDto()
            {
                AppId = APPID,
            },
            Parameter = new TranslationDto.ParameterDto()
            {
                Its = new TranslationDto.ItsDto()
                {
                    From = from,
                    To = to
                }
            },
            Payload = new TranslationDto.PayloadDto()
            {
                InputData = new TranslationDto.InputDataDto()
                {
                    Text = Convert.ToBase64String(Encoding.UTF8.GetBytes(str))
                }
            }
        };
    }
}

public class TranslationDto
{
    [NJP("header")]
    public HeaderDto Header { get; set; }

    [NJP("parameter")]
    public ParameterDto Parameter { get; set; }

    [NJP("payload")]
    public PayloadDto Payload { get; set; }

    public class HeaderDto
    {
        [NJP("app_id")]
        public string AppId { get; set; }

        [NJP("status")]
        public int Status => 3;

        [NJP("res_id")]
        public string? ResId { get; set; }
    }

    public class ParameterDto
    {
        [NJP("its")]
        public ItsDto Its { get; set; }
    }

    public class ItsDto
    {
        [NJP("from")]
        public string From { get; set; }

        [NJP("to")]
        public string To { get; set; }

        [NJP("result")]
        public object Result { get; set; } = new object();
    }

    public class PayloadDto
    {
        [NJP("input_data")]
        public InputDataDto InputData { get; set; }
    }

    public class InputDataDto
    {
        [NJP("encoding")]
        public string Encoding { get; set; } = "utf8";

        [NJP("status")]
        public int Status => 3;

        [NJP("text")]
        public string Text { get; set; }
    }
}

public class ResponseVm
{
    public HeaderVm Header { get; set; }
    public PayloadVm? Payload { get; set; }


    public class PayloadVm
    {
        public ResultVm Result { get; set; }
    }

    public class ResultVm
    {
        public int Seq { get; set; }
        public int Status { get; set; }
        public string Text { get; set; }
    }

    public class HeaderVm
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public string Sid { get; set; }
    }
}

public class TextVm
{
    public string From { get; set; }
    public string To { get; set; }

    [NJP("trans_result")]
    public TransResultVm TransResult { get; set; }

    public class TransResultVm
    {
        public string Dst { get; set; }
        public string Src { get; set; }
    }
}