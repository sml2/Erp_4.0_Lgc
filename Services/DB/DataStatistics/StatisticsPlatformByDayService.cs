using ERP.Data;
using ERP.Enums.Purchase;
using ERP.Extensions;
using ERP.Models.DataStatistics;
using ERP.Models.Setting;
using ERP.Models.Statistics;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace ERP.Services.DataStatistics;

public class StatisticsPlatformByDayService
{
    private readonly DBContext _dbContext;

    public StatisticsPlatformByDayService(DBContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task Handle()
    {
        var now = DateTime.Now;
        
        //产品总数
        var productTotal = await _dbContext.Product.CountAsync();
        //订单总数
        var orderTotal = await _dbContext.Order.CountAsync();
        //采购总数
        var purchaseTotal = await _dbContext.Purchase
            .Where(m => (new List<PurchaseStates>() { PurchaseStates.PURCHASED, PurchaseStates.NOPURCHASEREQUIRED })
                .Contains(m.PurchaseState))
            .CountAsync();
        //运单总数
        var waybillTotal = await _dbContext.Waybill.CountAsync();
        //oem总数
        var oemTotal = await _dbContext.OEM.Where(m => m.State == OEM.States.ENABLE).CountAsync();
        //公司总数
        var companyTotal = await _dbContext.Company.CountAsync();
        //用户总数
        var userTotal = await _dbContext.User.CountAsync();
        //邮箱总数
        var postboxTotal = await _dbContext.UserEmails.CountAsync();
        //邮件总数
        var emailTotal = await _dbContext.EmailInfos.CountAsync();
        //日活总数
        var activityTotal = await _dbContext.User
            .Where(m => m.LoginTime >= now.BeginDayTick() && m.LoginTime <= now.LastDayTick()).CountAsync();
        //库存总数
        var quantityTotal = await _dbContext.Stock.CountAsync();
        //开户总数
        var openTotal = await _dbContext.User
            .Where(m => m.CreatedAt >= now.BeginDayTick() && m.CreatedAt <= now.LastDayTick()).CountAsync();
        //店铺总数
        var storeTotal = await _dbContext.StoreRegion.CountAsync();
        
        var info = await _dbContext.StatisticsChart.Where(m => m.CreatedAt == now.LastDayTick()).FirstOrDefaultAsync();
        if (info == null)
        {
            var insert = new StatisticsChartModel
            {
                ProductTotal = productTotal,
                OrderTotal = orderTotal,
                PurchaseTotal = purchaseTotal,
                OemTotal = oemTotal,
                CompanyTotal = companyTotal,
                WaybillTotal = waybillTotal,
                UserTotal = userTotal,
                MailboxTotal = postboxTotal,
                EmailTotal = emailTotal,
                ActivityTotal = activityTotal,
                OpenTotal = openTotal,
                QuantityTotal = quantityTotal,
                StoreTotal = storeTotal,
                CreatedAt = now.LastDayTick(),
                UpdatedAt = now
            };
            _dbContext.StatisticsChart.Add(insert);
        }
        else
        {
            info.ProductTotal = productTotal;
            info.OrderTotal = orderTotal;
            info.PurchaseTotal = purchaseTotal;
            info.OemTotal = oemTotal;
            info.CompanyTotal = companyTotal;
            info.WaybillTotal = waybillTotal;
            info.UserTotal = userTotal;
            info.MailboxTotal = postboxTotal;
            info.EmailTotal = emailTotal;
            info.ActivityTotal = activityTotal;
            info.OpenTotal = openTotal;
            info.QuantityTotal = quantityTotal;
            info.StoreTotal = storeTotal;
            info.UpdatedAt = now;
            _dbContext.StatisticsChart.Update(info);   
        }

        await _dbContext.SaveChangesAsync();
    }
}