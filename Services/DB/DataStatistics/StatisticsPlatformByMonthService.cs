using ERP.Data;
using ERP.Enums.Purchase;
using ERP.Extensions;
using ERP.Models.DataStatistics;
using ERP.Models.DB.Product;
using ERP.Models.Setting;
using ERP.Models.Statistics;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using OrderSDK.Modles.Amazon.Finances;

namespace ERP.Services.DataStatistics;

public class StatisticsPlatformByMonthService
{
    private readonly DBContext _dbContext;

    public StatisticsPlatformByMonthService(DBContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task Handle()
    {
        var oemList = await _dbContext.OEM.Where(m => m.State == OEM.States.ENABLE).ToListAsync();

        var insertData = new List<MonthCounterModel>();
        var now = DateTime.Now;
        foreach (var oem in oemList)
        {
            var total = await _dbContext.User
                .Where(m => m.OEMID == oem.ID && m.LoginTime >= now.BeginMonthTick() &&
                            m.LoginTime <= now.LastMonthTick())
                .CountAsync();

            insertData.Add(new MonthCounterModel()
            {
                OemId = oem.ID,
                UserTotal = total,
                CreatedAt = now,
                UpdatedAt = now
            });
        }

        await _dbContext.MonthCounter.AddRangeAsync(insertData);
        await _dbContext.SaveChangesAsync();
    }
}