
using Microsoft.EntityFrameworkCore;
using ERP.Models.ProductNodeType.Amazon;
using Model = ERP.Models.ProductNodeType.Amazon.Category;
using ERP.Data;


namespace ERP.Services.DB.ProductType;
public class CategoryService : BaseService
{
    private readonly DBContext _dbContext;
    protected readonly ILogger<CategoryService> _logger;

    public CategoryService(DBContext dbContext, ILogger<CategoryService> logger)
    {
        _dbContext = dbContext;
        _logger = logger;
    }

    /// <summary>
    /// 先将在用的目录，置为删除状态
    /// </summary>
    /// <param name="platform"></param>
    /// <param name="marketPlace"></param>
    /// <returns></returns>
    public async Task<bool> UpdateCategoryStateAsync(Platforms platform, string marketPlace)
    {
        try
        {
            var lst = await _dbContext.NodeCategory.Where(a => a.Platform == platform && a.MarketPlaceID == marketPlace && a.State == CategoryState.InUsed).ToListAsync();

            if (lst.Any())
            {
                lst.ForEach(x => x.State = CategoryState.Deleted);
                _dbContext.NodeCategory.UpdateRange(lst);
                await _dbContext.SaveChangesAsync();
                return true;
            }
            return true;
        }
        catch (Exception e)
        {
            _logger.LogError($" NodeCategory UpdateCategoryStateAsync error:{e.Message}");
            _logger.LogError(e.StackTrace);
            return false;
        }
    }

    public async Task<bool> AddAsync(Model model) => await AddRangeAsync(new List<Model> { model });

    public async Task<bool> AddRangeAsync(List<Model> models)
    {
        List<Model> lst = new List<Model>();
        List<Model> updateLst = new List<Model>();
        try
        {
            foreach (var model in models)
            {
                var Source = await GetCategoryByAsync(model.UniqueHashCode);
                if (Source is null) //这个productType不存在，新增即可
                {
                    lst.Add(model);
                }
                else
                {
                    if (!Source.RowMd5.Equals(model.RowMd5))   //存在，比较一下有没有改变
                    {
                        lst.Add(model);
                        //有变更
                        Source.State = CategoryState.Expired; //设置过期状态
                        updateLst.Add(Source);
                    }
                    else
                    {
                        //没变更，不需要操作
                    }
                }
            }

            if (lst.Count > 0)  //新增的
            {
                await _dbContext.NodeCategory.AddRangeAsync(lst);
            }
            if (updateLst.Count > 0) //更新状态
            {
                _dbContext.NodeCategory.UpdateRange(updateLst);
            }
            await _dbContext.SaveChangesAsync();
            return true;
        }
        catch (Exception e)
        {
            _logger.LogError($" NodeCategory AddInternal error:{e.Message}");
            _logger.LogError(e.StackTrace);
            return false;
        }
    }


    public async Task<Model?> GetCategoryByAsync(ulong unique)
    {
        return await _dbContext.NodeCategory.FirstOrDefaultAsync(a => a.UniqueHashCode == unique && a.State == CategoryState.InUsed);
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="MarketPlaceID"></param>
    /// <returns></returns>
    public async Task<List<Model>> GetProductTypeByMarketPlaceID(string MarketPlaceID)
    {
        return await _dbContext.NodeCategory.Where(x => x.MarketPlaceID == MarketPlaceID && x.State == CategoryState.InUsed).ToListAsync();
    }




    /// <summary>
    /// 获取主目录信息
    /// </summary>
    /// <param name="MarketPlaceID"></param>
    /// <param name="platforms"></param>
    /// <returns></returns>
    public async Task<List<Model>> GetMainNodeCategory(string MarketPlaceID, Platforms platforms = Enums.Platforms.AMAZONSP)
    {
        //SELECT * from nodecategory WHERE IsRoot=1 and Platform=8 and ProductType!='' and State=1

        return await _dbContext.NodeCategory.Where(x => x.MarketPlaceID == MarketPlaceID && x.Platform == platforms && x.IsRoot == true && x.IsRoot == true && x.State == CategoryState.InUsed && x.ProductType != "").OrderBy(x => x.OriginalName).ToListAsync();
    }

    public async Task<List<Model>> GetNodeCategory(string MarketPlaceID, long id, string pathNodes, Platforms platforms = Enums.Platforms.AMAZONSP)
    {
        //SELECT * from nodecategory WHERE IsRoot=1 and Platform=8 and ProductType!='' and State=1

        return await _dbContext.NodeCategory.Where(x => x.MarketPlaceID == MarketPlaceID && x.Platform == platforms && x.State == CategoryState.InUsed && x.CategoryParentId == id && x.PathNodes.Contains(pathNodes) && x.Flag == true).OrderBy(x => x.OriginalName).ToListAsync();
    }

    public async Task ChangeFlag(string marketPlace, string productType, Platforms platforms = Enums.Platforms.AMAZONSP)
    {
        if (!string.IsNullOrWhiteSpace(productType))
        {
            List<long> allIDs = new();
            var modelList = await _dbContext.NodeCategory.Where(x => x.MarketPlaceID == marketPlace && x.Platform == platforms && x.State == CategoryState.InUsed && x.ProductType.Contains(productType)).OrderBy(x => x.OriginalName).ToListAsync();
            if (modelList.Count > 0)
            {
                //foreach (var item in modelList)
                //{
                //    if (!string.IsNullOrWhiteSpace(item.PathNodes))
                //    {                       
                //        var temp = item.PathNodes.Split(",");                       
                //        temp.ForEach(x=> allIDs.Add(Convert.ToInt64(x)));                       
                //    }
                //}
                //var distinctIDs = allIDs.Distinct().ToList();
                //var list = await _dbContext.NodeCategory.Where(x => distinctIDs.Contains(x.CategoryId)).ToListAsync();
                modelList.ForEach(x => { x.Flag = true; });
            }
            _dbContext.NodeCategory.UpdateRange(modelList);
        }
    }

    public async Task<List<Model>> GetLastNodeCategory(string MarketPlaceID, string ids, long id, Platforms platforms = Enums.Platforms.AMAZONSP)
    {
        //SELECT * from nodecategory WHERE IsRoot=1 and Platform=8 and ProductType!='' and State=1
        return await _dbContext.NodeCategory.Where(x => x.MarketPlaceID == MarketPlaceID && x.Platform == platforms && x.State == CategoryState.InUsed && x.CategoryId == id && x.PathNodes.Contains(ids) && x.Flag == true).OrderBy(x => x.OriginalName).ToListAsync();
    }

    public List<string> GetNodeCategoryByFlag(string MarketPlaceID, Platforms platforms = Enums.Platforms.AMAZONSP)
    {

        return _dbContext.NodeCategory.Where(x => x.MarketPlaceID == MarketPlaceID && x.Platform == platforms && x.State == CategoryState.InUsed && x.Flag == false && x.ProductType != "").Select(x => x.ProductType).Distinct().ToList();
    }
}