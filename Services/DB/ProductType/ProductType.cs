using ERP.Controllers;
using ERP.Data;
using ERP.Models.ProductNodeType.Amazon;
using ERP.Services.Product;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using static ERP.Models.View.Operations.TaskLog;
using Model = ERP.Models.ProductNodeType.Amazon.AttributeModel;

namespace ERP.Services.DB.ProductType
{
    public class ProductTypeService
    {                     
        protected readonly ILogger<ProductTypeService> _logger;
        private readonly DBContext _dbContext;
        public ProductTypeService(DBContext dbContext,  ILogger<ProductTypeService> logger)
        {                 
            _dbContext = dbContext;
            _logger = logger;
        }


        public async Task<bool> AddAsync(Model model, CategoryService categoryService) => await AddRangeAsync(new List<Model> { model }, categoryService);

        public async Task<bool> AddRangeAsync(List<Model> models, CategoryService categoryService)
        {
            List<Model> lst = new List<Model>();
            List<Model> updateLst = new List<Model>();
            try
            {
                foreach (var model in models)
                {
                    //Helpers.CreateHashCode(marketPlace + type);                                    
                    Model? Source =  await GetAttributeAsync(model.AttributeHash);
                    if (Source is null) //这个productType不存在，新增即可
                    {
                        lst.Add(model);
                    }
                    else
                    {
                        if (!Source.Version.Equals(model.Version))   //存在，比较一下有没有改变
                        {
                            lst.Add(model);
                            //有变更
                            Source.State = CategoryState.Expired; //设置过期状态
                            updateLst.Add(Source);
                        }
                        else
                        {
                            //没变更，不需要操作
                        }                                        
                    }

                    //更新Nodecategory中的状态
                   await categoryService.ChangeFlag(model.MarketPlaceID,model.ProductType);
                }

                if (lst.Count > 0)  //新增的
                {
                    await _dbContext.AttributeModels.AddRangeAsync(lst);
                }
                if (updateLst.Count > 0) //更新状态
                {
                    _dbContext.AttributeModels.UpdateRange(updateLst);
                }
                await _dbContext.SaveChangesAsync();
                return true;
            }
            catch (Exception e)
            {
                _logger.LogError($" AttributeModels AddInternal error:{e.Message}");
                _logger.LogError(e.StackTrace);
                return false;
            }
        }

        public async Task<Model?> GetAttributeAsync(ulong AttributeHash)
        {
            return await _dbContext.AttributeModels.FirstOrDefaultAsync(x => x.AttributeHash == AttributeHash && x.State == CategoryState.InUsed);
        }      
    }
}
