
using ERP.Services.DB.Orders;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System.Net;
using PlatformDataCache = ERP.Services.Caches.PlatformData;

using ModelView = ERP.Models.View.Products.Product.productUploadView;
//using ModelLog = ERP.Models.DB.Product.ProductUploadLog;
using Model = ERP.Models.DB.Product.ProductUpload;
//using ModelJson = ERP.Models.DB.Product.ProductUploadJson;

using ModelToSell = ERP.Models.Product.ToSellModel;
using ERP.Services.DB.Users;
using ERP.Models.DB.Product;
using ERP.Services.DB.Product;
using System.Text;
using ERP.Services.Product;
using ERP.Models.View.Products.Product;
using Z.EntityFramework.Plus;
using System.Linq;
using ERP.Models.View.Products.SmartHijacking;
using ERP.Models.Api.Amazon.vm;
using ERP.Models.Product;
using OrderSDK.Modles.Amazon.Feeds;
using ERP.Services.Api;
using ERP.Models.ProductNodeType.Amazon;
using OrderSDK.Modles;
using NPOI.SS.Formula.Functions;
using NPOI.Util;
using ERP.Services.Provider;

namespace ERP.Services.DB.ProductType
{
    public class ProductHost
    {
        private readonly IServiceProvider _ServiceProvider;
        private readonly IOrderFactory ApiOrderFactory;
        private readonly ILogger<ProductHost> _logger;
        public ProductHost(IOrderFactory _ApiOrderFactory, IServiceProvider serviceProvider, ILogger<ProductHost> logger)
        {
            ApiOrderFactory = _ApiOrderFactory;
            _ServiceProvider = serviceProvider;
            _logger = logger;
        }


        public async Task PushProduct(int storeID, int userID,int oemid,UploadProductParams productParams)
        {
            _logger.LogError($"PushProduct 开始上传");
            string msg = "";
            List<Model> uploads = new();
            UploadResult uploadResult = UploadResult.None;

            var oemCache = _ServiceProvider.GetRequiredService<Caches.OEM>();
            var oem = oemCache.Get(oemid)!;
            var serviceProvider = _ServiceProvider.CreateScopeWithOem(oem).ServiceProvider;

            //var serviceProvider = _ServiceProvider.CreateScope().ServiceProvider;

            var storeService = serviceProvider.GetRequiredService<ERP.Services.Stores.StoreService>();
            var ProductUploadService = serviceProvider.GetRequiredService<ProductUploadService>();           
            var userService = serviceProvider.GetRequiredService<UserService>();
            var taskService= serviceProvider.GetRequiredService<TaskService>();
            var _dbContext = serviceProvider.GetRequiredService<ERP.Data.DBContext>();

            using (var tranc = _dbContext.Database.BeginTransaction())
            {
                try
                {
                    var store = await storeService.GetStoreByID(storeID);
                    if (store == null)
                    {                        
                        _logger.LogError($"PushProduct store is null,storeID:{storeID}");
                        return;
                    }
                    var user = await userService.GetInfoById(userID);
                    if (user == null)
                    {
                        _logger.LogError($"PushProduct user is null,user:{userID}");
                        return;                       
                    }
                    var pModels = ProductUploadService.GetByPids(productParams.Pids);
                    if (pModels == null || pModels.Count<=0)
                    {
                        _logger.LogError($"PushProduct 上传产品记录已经被删除，{string.Join(',',productParams.Pids)}");
                        return;
                    }
                    var pList =await ProductUploadService.GetListById(productParams.Pids, oemid);
                    if (pList == null || pList.Count <= 0)
                    {
                        _logger.LogError($"PushProduct 产品已经被删除，{string.Join(',', productParams.Pids)}");
                        return;
                    }
                    //获取上传任务结果
                    var task = _dbContext.SyncUploadTasks.Where(x => x.ID == pModels.First().SyncUploadTaskId).FirstOrDefault();
                    if (task == null)
                    {
                        _logger.LogError($"PushProduct 上传任务{pModels.First().SyncUploadTaskId}被删除");
                        return;
                    }                  
                    var _ApiOrderServices = ApiOrderFactory.Create(store.Platform);
                    if (productParams.Bytes.Length <= 9.5 * 1024 * 1024)
                    {
                        _logger.LogInformation($"PushProduct UploadExcelBytes begin");
                        var result = await _ApiOrderServices.UploadExcelBytes(store, productParams.MarketPlace, productParams.Bytes);
                        _logger.LogInformation($"PushProduct UploadExcelBytes end,result:{JsonConvert.SerializeObject(result)}");
                        if (!string.IsNullOrWhiteSpace(result.Item1))
                        {
                            uploadResult = UploadResult.UploadFail;
                            //分析有错误信息
                            switch (result.Item1)
                            {
                                case "FeedIDNull":  //请求过多，请稍后再发起上传操作
                                    msg = "请求过多，请稍后再发起上传操作";
                                    break;
                                case "FATAL":  //亚马逊上传中，出现致命错误
                                    msg = "亚马逊上传中，出现致命错误";
                                    break;
                                case "CANCELLED":  //亚马逊上传中，被取消了
                                    msg = "亚马逊上传中，被取消了";
                                    break;
                                case "ERROR":    //上传过程中，出错了
                                    msg = "上传出错";
                                    break;
                                case "ReportERROR":  //获取报表为空
                                    msg = "亚马逊上传中，获取报表为空";
                                    break;
                                default:
                                    msg = "上传出错";
                                    break;
                            }
                            pushProductResult res = new pushProductResult()
                            {
                                originalRecordNumber = "1",
                                sku = "",
                                errorCode = "",
                                errorType = "",
                                errorMessage = msg,
                            };                           
                            pModels.ForEach(x => { x.Message = JsonConvert.SerializeObject( new List<pushProductResult>() { res }); x.Result = UploadResult.UploadFail; });
                            task.State = UploadResult.UploadFail;
                        }
                        else
                        {
                            //处理结束，保存处理信息
                            var results = result.Item2;
                            //取出错误的sku，和错误code和信息，分别保存
                            pModels.ForEach(x => x.Result = UploadResult.UploadSuccess);
                            foreach (var pinfo in pList)
                            {
                                var items = results.Where(x => x.sku.Contains(pinfo.Sku)).ToList();
                                var pUpload = pModels.Where(x => x.ProductId == pinfo.ID).First();
                                //items.ForEach(x => msg += x.ToString());                            
                                pUpload.Message = JsonConvert.SerializeObject(items);
                                pUpload.Result = UploadResult.UploadFail;
                            }
                            task.State = UploadResult.UploadSuccess;
                        }
                    }
                    else
                    {
                        pushProductResult re = new pushProductResult()
                        {
                            originalRecordNumber = "1",
                            sku = "",
                            errorCode = "",
                            errorType = "",
                            errorMessage = "上传产品数据超过10M限制",
                        };

                        pModels.ForEach(x => { x.Message = JsonConvert.SerializeObject(new List<pushProductResult>() { re }); x.Result = UploadResult.UploadFail; });
                        task.State = UploadResult.UploadFail;
                    }
                                     
                    if (await ProductUploadService.UpdateAsync(pModels))
                    {
                        await _dbContext.SaveChangesAsync();
                        await tranc.CommitAsync();
                    }
                    else
                    {
                        await tranc.RollbackAsync();
                        _logger.LogError($"PushProduct AddInternalsAsync error");
                    }
                }
                catch (Exception e)
                {
                    await tranc.RollbackAsync();
                    _logger.LogError($"PushProduct Exception:{e.Message}");
                    _logger.LogError($"PushProduct Exception:{e.StackTrace}");
                }
            }
        }

        public async Task GetProducts(int storeID, int userID, string MarketPlace, List<string> asins)
        {
            string msg = "";
            var serviceProvider = _ServiceProvider.CreateScope().ServiceProvider;
            var storeService = serviceProvider.GetRequiredService<ERP.Services.Stores.StoreService>();
            var toSellService = serviceProvider.GetRequiredService<ToSellService>();
            var userService = serviceProvider.GetRequiredService<UserService>();
            var _dbContext = serviceProvider.GetRequiredService<ERP.Data.DBContext>();
            try
            {
                var store = await storeService.GetStoreByID(storeID);
                if (store == null)
                {
                    _logger.LogError($"GetProducts store is null,storeID:{storeID}");
                    return;
                }
                var user = await userService.GetInfoById(userID);
                if (user == null)
                {
                    _logger.LogError($"GetProducts user is null,userid:{storeID}");
                    return;
                }
                var _ApiOrderServices = ApiOrderFactory.Create(store.Platform);

                const int n = 20;
                int i = 0;
                while (asins.Skip(n * i).Take(n).Any())
                {
                    List<ModelToSell> Uploads = new();
                    var splitAsins = asins.Skip(n * i).Take(n).ToList();
                    i++;
                    var result = await _ApiOrderServices.SearchCatalogItems(store,MarketPlace,splitAsins);
                    if (result != null)
                    {
                        using (var transaction = await _dbContext.Database.BeginTransactionAsync())
                        {
                            try
                            {
                                foreach (var item in result)
                                {
                                    var asin = item.asin;
                                    var productType = item.productTypes == null ? "" : item.productTypes[0].productType;
                                    string title = "", marketPlace = "", Brand = "", manufacturer = "", ImageUrl = "";
                                    if (item.summaries != null)
                                    {
                                        var summary = item.summaries[0];
                                        title = summary.itemName;
                                        marketPlace = summary.marketplaceId;
                                        Brand = summary.brand;
                                        manufacturer = summary.manufacturer;
                                    }
                                    if (item.images != null && item.images[0].images != null)
                                    {
                                        ImageUrl = item.images[0].images[0].link;
                                    }
                                    string unit = Helpers.GetCurrency(marketPlace);
                                    if (unit.IsNullOrWhiteSpace())
                                    {
                                        msg = $"GetProducts unit is null,{marketPlace}";
                                        _logger.LogError(msg);
                                        throw new Exception(msg);
                                    }
                                    decimal price = 0;
                                    string curr = "";                                    
                                    ModelToSell model = new ModelToSell();
                                    //构造参数                               
                                    model.HashCode = Helpers.CreateHashCode(asin + storeID);
                                    model.Title = title;
                                    model.Asin = asin;
                                    model.ImageUrl = ImageUrl;
                                    model.AsinMarketplace = marketPlace;
                                    model.Brand = Brand;
                                    model.storeRegion = store;
                                    model.CompanyID = user.CompanyID;
                                    model.GroupID = user.GroupID;
                                    model.UserID = user.ID;
                                    model.OEMID = user.OEMID;
                                    model.ProductResult = Models.Product.HijackingResults.WaitSynchronize;
                                    model.ProductType = productType;
                                    model.Msg = "";
                                    model.Currency = curr == "" ? unit : curr;
                                    model.Sku = Helpers.SkuCode();
                                    model.UnitPrice = curr == "" ? MoneyRecord.Empty : new MoneyMeta(curr, price);
                                    Uploads.Add(model);
                                }
                                if (Uploads.Count > 0)
                                {
                                    bool f = await toSellService.Adds(Uploads);
                                    if (f)
                                    {
                                        await _dbContext.SaveChangesAsync();
                                        await transaction.CommitAsync();
                                        _logger.LogInformation($"GetProducts:获取产品成功{string.Join(',', splitAsins)}");
                                    }
                                    else
                                    {
                                        await transaction.RollbackAsync();
                                        msg = $"GetProducts:保存产品失败:marketplace:{MarketPlace},asins:{string.Join(',', splitAsins)}";
                                    }
                                }
                                else
                                {
                                    await transaction.RollbackAsync();
                                    msg = $"GetProducts:获取产品失败:marketplace:{MarketPlace},asins:{string.Join(',', asins)}";
                                    _logger.LogError(msg);
                                }
                            }
                            catch (Exception e)
                            {
                                _logger.LogError($"GetProducts:获取产品失败{e.Message}");
                                _logger.LogError(e.StackTrace);
                                await transaction.RollbackAsync();
                            }
                        }
                    }
                    else
                    {
                        msg = $"GetProducts:获取产品失败！asins:{string.Join(',', asins)}";
                        _logger.LogError(msg);
                    }
                }
            }
            catch (Exception e)
            {
                msg = $"GetProducts:推送亚马逊接口失败,异常信息：{e.Message}";
                _logger.LogError(msg);
                _logger.LogError(e.StackTrace);
            }
        }


        /// <summary>
        /// 针对同一店铺，同一市场
        /// </summary>
        /// <param name="storeID"></param>
        /// <param name="userID"></param>
        /// <param name="MarketPlace"></param>
        /// <param name="jsons"></param>
        /// <returns></returns>
        public async Task HijackingProducts(int storeID, int userID, string MarketPlace, List<int> ids, ActionType actionType = ActionType.PUtOn)
        {
            string msg = "";
            var serviceProvider = _ServiceProvider.CreateScope().ServiceProvider;            
            var storeService = serviceProvider.GetRequiredService<ERP.Services.Stores.StoreService>();
            var toSellService = serviceProvider.GetRequiredService<ToSellService>();
            var userService = serviceProvider.GetRequiredService<UserService>();
            var _dbContext = serviceProvider.GetRequiredService<ERP.Data.DBContext>();
            try
            {
                var store = await storeService.GetStoreByID(storeID);
                if (store == null)
                {
                    throw new Exception($"HijackingProducts store is null,storeID:{storeID}");
                }
                var user = await userService.GetInfoById(userID);
                if (user == null)
                {
                    throw new Exception($"HijackingProducts user is null,userid:{userID}");
                }

                var _ApiOrderServices = ApiOrderFactory.Create(store.Platform);

                //查models
                var models = toSellService.GetModels(ids);

                string asins = string.Join(',', models.Select(x => x.Asin).ToList());
                //同一店铺，同一市场，批量修改，上架，下架，删除
                //构造完整json结构
                string sellerID = store.GetSPAmazon()!.SellerID;
                string jsons = "";
                if (actionType == ActionType.Delete)
                {
                    ListingFeed<DeleteHijackingJson> listingFeed = new();
                    listingFeed._Header = new();
                    listingFeed._Header.SellerId = sellerID;
                    listingFeed._Header.IssueLocale = Helpers.GetAmazonLang("", MarketPlace);
                    listingFeed.Messages = new();
                    jsons = toSellService.JsonCtor(models, listingFeed, actionType);
                }
                else
                {
                    ListingFeed<UpdateHijackingJson> listingFeed = new();
                    listingFeed._Header = new();
                    listingFeed._Header.SellerId = sellerID;
                    listingFeed._Header.IssueLocale = Helpers.GetAmazonLang("", MarketPlace);
                    listingFeed.Messages = new();
                    jsons = toSellService.JsonCtor(models, listingFeed, actionType);
                }
                var result = await _ApiOrderServices.SubmitProduicts(store, jsons, MarketPlace);
                if (result.Item1 != null)
                {
                    if (result.Item1.issues != null && result.Item1.issues.Count > 0)
                    {
                        //跟卖接口失败
                        foreach (var item in models)
                        {
                            item.ProductResult = HijackingResults.SynchronizeFail;
                            item.Msg = JsonConvert.SerializeObject(result.Item1);
                        }
                        msg = $"HijackingProducts:操作产品接口出错:marketplace:{MarketPlace},ids:{string.Join(',', ids)}";
                    }
                    else
                    {
                        //成功                       
                        if (actionType != ActionType.Delete)
                        {
                            //更新同步时间
                            foreach (var item in models)
                            {
                                item.ProductResult = actionType == ActionType.PUtOn ? HijackingResults.PutOn : HijackingResults.TakeOff;
                                item.Msg = "";
                                item.SyncTime = DateTime.Now;
                                item.Quantity = actionType == ActionType.TakeOff ? 0 : item.Quantity;
                                item.Sync = 1;
                            }
                            msg = $"HijackingProducts:跟卖产品成功:marketplace:{MarketPlace},ids:{string.Join(',', ids)}";
                        }
                        else
                        {
                            bool d = await toSellService.Deletes(models);
                            if (d)
                            {
                                _logger.LogInformation($"HijackingProducts:删除产品成功{asins}");
                            }
                            else
                            {
                                msg = $"HijackingProducts:删除产品失败:marketplace:{MarketPlace},ids:{string.Join(',', ids)}";
                            }
                            return;
                        }
                    }
                    //接口失败，或接口成功的上架，下架，修改产品数据

                    bool f = await toSellService.Updates(models);
                    if (f)
                    {
                        _logger.LogInformation(msg);
                    }
                    else
                    {
                        _logger.LogError(msg);
                    }
                }
                else
                {
                    msg = $"HijackingProducts:跟卖产品失败！asins:{string.Join(',', models.Select(x => x.Asin).ToList())}";
                    _logger.LogError(msg);
                }
            }
            catch (Exception e)
            {
                msg = $"HijackingProducts:推送亚马逊接口失败,异常信息：{e.Message}";
                _logger.LogError(msg);
                _logger.LogError(e.StackTrace);
            }
        }
    }
}