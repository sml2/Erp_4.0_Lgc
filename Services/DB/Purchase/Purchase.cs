﻿using ERP.Enums.Finance;
using ERP.Enums.Purchase;
using ERP.Extensions;
using ERP.ViewModels.Purchase;
using Microsoft.EntityFrameworkCore;
using PurchaseLogModel = ERP.Models.DB.Purchase.PurchaseLog;
using CompanyModel = ERP.Models.DB.Users.Company;
using ERP.Interface;
using ERP.Enums.Orders;
using ERP.Services.Stores;
using ERP.Services.DB.Statistics;
using ERP.Services.Finance;
using AmazonOrderModel = ERP.Models.DB.Orders.Order;
using static ERP.Extensions.DbSetExtension;
using PurchaseModel = ERP.Models.DB.Purchase.Purchase;
using ERP.Models.View.Purchase;
using ERP.Storage.Abstraction.Services;
using ERP.Data;
using ERP.Services.Statistics;

namespace ERP.Services.Purchase;
public class Purchase : BaseService
{
    private readonly DBContext _dbContext;
    private readonly ISessionProvider _sessionProvider;
    private readonly Statistic _statistic;
    private readonly IStockService _stockService;
    private readonly StoreService _storeService;
    private readonly StatisticMoney _statisticMoney;
    private readonly BillingRecordsService _billingRecordsService;

    public Purchase(DBContext dbContext, ISessionProvider session, StoreService storeService, Statistic statistic, IStockService stockService, BillingRecordsService billingRecordsService)
    {
        _dbContext = dbContext;
        _statistic = statistic;
        _stockService = stockService;
        _billingRecordsService = billingRecordsService;
        _storeService = storeService;
        _sessionProvider = session;
        _statisticMoney = new StatisticMoney(_statistic);
    }
    private ISession Session { get => _sessionProvider.Session!; }
    private int UserID { get => Session.GetUserID(); }
    private int GroupID { get => Session.GetGroupID(); }
    private int CompanyID { get => Session.GetCompanyID(); }
    private int OEMID { get => Session.GetOEMID(); }

    #region order 调用
    #region
    #endregion

    #region 添加采购信息
    /// <summary>
    /// 添加采购信息
    /// </summary>
    /// <param name="vm"></param>
    /// <param name="order">采购所属订单</param>
    /// <param name="company">采购所属订单公司</param>
    /// <param name="type">1内部、分销下级 2分销上级添加</param>
    /// <returns></returns>
    public async Task<PurchaseModel> InsertPurchase(PurchaseEditVM vm, AmazonOrderModel order, CompanyModel company, int type, Models.DB.Identity.User operateUser,int? billLogId = null/* $good, $bill_log_id, $warehouse = []*/)
    {
        var purchaseAffirm = company.PurchaseAudit != CompanyModel.PurchaseAudits.YES ? PurchaseAffirms.CONFIRMED : PurchaseAffirms.UNCONFIRMED;
        //var purchaseRate = vm.PurchaseFee > 0 ? Helpers.FromConversion(vm.PurchaseFee, vm.PurchaseUnit) : 0;

        #region 仓库 暂时忽略???
        var isStock = false;
        int warehouseId = 0;
        string warehouseName = "";
        //if ($type == 1 && isset($requestData['is_stock']) && $requestData['is_stock'] === PurchaseModel::IS_STOCK_TRUE) {
        //    $is_stock = PurchaseModel::IS_STOCK_TRUE;
        //    $warehouse_id = isset($warehouse['id']) ? $warehouse['id'] : 0;
        //    $warehouse_name = isset($warehouse['name']) ? $warehouse['name'] : '';
        //}
        if (type == 1 && vm.IsStock is true)
        {
            isStock = true;
            //$warehouse_id = isset($warehouse['id']) ? $warehouse['id'] : 0;
            // $warehouse_name = isset($warehouse['name']) ? $warehouse['name'] : '';
        }
        #endregion

        var purchase = new PurchaseModel
        {
            OrderId = order.ID,
            OrderNo = order.OrderNo,
            Name = vm.Name,
            Asin = vm.GoodId,
            OrderGoodId = vm.GoodId,
            Unit = vm.Unit,
            Url = vm.Url,
            PurchaseState = vm.PurchaseState,
            FromUrl = vm.FromUrl,
            Num = vm.Num,
            PlatformId = (int)order.Plateform.Platform,//平台???
            PurchaseOrderNo = vm.PurchaseOrderNo,
            PurchaseTrackingNumber = vm.PurchaseTrackingNumber,
            Remark = vm.Remark,
            StoreId = order.StoreId,
            StoreName = order.Store!.Name,
            SellerSku = vm.SellerSku,
            PurchaseCompanyId = order.PurchaseCompanyId,//采购上报
            //PurchaseRate = purchaseRate,
            PurchaseRateType = vm.PurchaseRateType ?? (PurchaseRateTypes)company.PurchaseRateType,

            PurchaseAffirm = purchaseAffirm,
            CompanyID = company.ID,
            OperateCompanyId = CompanyID,
            CreateCompanyId = CompanyID,
            PurchaseUserId = UserID,
            FindTruename = Session.GetTrueName(),
            AffirmUid = UserID,
            AffirmTruename = Session.GetTrueName(),
            OEMID = OEMID,
            GroupID = GroupID,
            UserID = UserID,
            // money
            //Price = priceMm,
            PurchaseUnit = vm.PurchaseUnit,
            //PurchaseTotal = purchaseTotalMm,
            //PurchaseOther = purchaseOtherMm,
            //PurchasePrice = purchasePriceMm,//采购单价

            Express = order.Express ?? "[]",//运单主键idStr
            // 暂时没用
            IsStock = false,//暂时全都不入库
            WarehouseId = warehouseId,
            WarehouseName = warehouseName,
            OrderItemId = vm.OrderItemId,
            BillLogId = billLogId,// 账单
            ProductId = vm.ProductId, // 产品库ID
            VariantId = vm.VariantId,
            PickId = 0,// 找货id
            PurchaseUrl = vm.PurchaseUrl,// 找货来源
        };

        try
        {
            purchase.PurchaseTotal = purchase.PurchaseTotal.Next(vm.PurchaseUnit, vm.PurchaseTotal);
            purchase.Price = purchase.Price.Next(vm.PurchaseUnit, vm.Price);
            purchase.PurchasePrice = purchase.PurchasePrice.Next(vm.PurchaseUnit, vm.PurchasePrice);
            purchase.PurchaseOther = purchase.PurchaseOther.Next(vm.PurchaseUnit, vm.PurchaseOther);
            purchase.ProductTotal = purchase.PurchaseOther.Next(vm.PurchaseUnit, vm.Num * vm.Price);

            purchase.AddLog("新增采购信息", operateUser);
            await _dbContext.Purchase.AddAsync(purchase);

            await _dbContext.SaveChangesAsync();

            #region 金额统计

            if (vm.PurchaseState == PurchaseStates.PURCHASED)
            {
                await _statisticMoney.InitStatisticWithPurchase(purchase);
            }

            #endregion


            #region 统计-新增采购数量

            await _statistic.InsertOrCount(Session, Statistic.NumColumnEnum.PurchaseInsert,
                Statistic.NumColumnEnum.PurchaseCount);
            await _dbContext.SaveChangesAsync();
            #endregion

            return purchase;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            throw new Exception("新增采购保存数据库出错");
        }
    }
    #endregion

    /// <summary>
    /// 自定义添加采购
    /// </summary>
    /// <param name="vm"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> AddCustomPurchase(PurchaseEditVM vm, Models.DB.Identity.User operateUser)
    {
        PurchaseModel purchase = new();

        purchase.Asin = vm.Asin;
        purchase.OrderGoodId = vm.GoodId;
        purchase.Name = vm.Name;
        purchase.Num = vm.Num;
        purchase.Unit = vm.Unit;
        purchase.Url = vm.Url;
        purchase.Remark = vm.Remark;
        purchase.OrderNo = vm.OrderNo;
        purchase.OrderId = vm.OrderId;
        purchase.FromUrl = vm.FromUrl;
        purchase.SellerSku = vm.SellerSku;
        
        purchase.OrderItemId = vm.OrderItemId;
        purchase.PurchaseTrackingNumber = vm.PurchaseTrackingNumber;
        purchase.PurchaseAffirm = vm.PurchaseAffirm;
        purchase.PurchaseState = vm.PurchaseState == PurchaseStates.None ? PurchaseStates.INPROCUREMENT : vm.PurchaseState;
        purchase.PurchaseUnit = vm.PurchaseUnit;
        purchase.PurchaseUrl = vm.PurchaseUrl;
        purchase.VariantId = vm.VariantId;
        purchase.ProductId = vm.ProductId;
        purchase.CompanyID = CompanyID;
        purchase.PurchaseUserId = UserID;
        purchase.UserID = UserID;
        purchase.GroupID = GroupID;
        purchase.OEMID = OEMID;
        purchase.OperateCompanyId = CompanyID;
        purchase.CreateCompanyId = CompanyID;

        // 是否入库 判断仓库是否存在
        if (vm.IsStock is true)
        {
            var warehouse = await _dbContext.Warehouses.FirstOrDefaultAsync(a => a.ID == vm.WarehouseId);
            if (warehouse is null)
            {
                return ReturnArr(false, "仓库不存在");
            }
            purchase.IsStock = true;
            purchase.WarehouseId = warehouse.ID;
            purchase.WarehouseName = warehouse.Name;
        }

        if (vm.OrderId > 0)
        {
            var orderInfo = await _dbContext.Order
                .FirstOrDefaultAsync(a => a.ID == vm.OrderId);

            if (orderInfo is null)
            {
                return ReturnArr(false, "订单不存在");
            }

            orderInfo.ProcessPurchase = orderInfo.ProcessPurchase == Belongs.NONE ? Belongs.INSIDE : orderInfo.ProcessPurchase;
            _dbContext.Order.Update(orderInfo);

            purchase.Express = orderInfo.Express ?? "";
            purchase.PurchaseCompanyId = orderInfo.PurchaseCompanyId;
        }

        var transaction = await _dbContext.Database.BeginTransactionAsync();

        try
        {

            purchase.PurchaseTotal = purchase.PurchaseTotal.Next(vm.PurchaseUnit, vm.PurchaseTotal);
            purchase.Price = purchase.Price.Next(vm.PurchaseUnit, vm.Price);
            purchase.PurchasePrice = purchase.PurchasePrice.Next(vm.PurchaseUnit, vm.PurchasePrice);
            purchase.PurchaseOther = purchase.PurchaseOther.Next(vm.PurchaseUnit, vm.PurchaseOther);
            purchase.ProductTotal = purchase.PurchaseOther.Next(vm.PurchaseUnit, vm.Num * vm.Price);

            purchase.AddLog("新增采购信息", operateUser);
            _dbContext.Add(purchase);
            await _dbContext.SaveChangesAsync();

            if (purchase.PurchaseState == PurchaseStates.PURCHASED)
            {
                #region 金额统计

                await _statisticMoney.InitStatisticWithPurchase(purchase);

                #endregion
            }
         

            #region 统计-新增采购数量

            await _statistic.InsertOrCount(Session, Statistic.NumColumnEnum.PurchaseInsert,
                Statistic.NumColumnEnum.PurchaseCount);

            #endregion
            
            if (vm.PurchaseState == PurchaseStates.PURCHASED)
            {
                //增加账单 自用 无需审核
                var msg = $"订单编号：【{vm.OrderNo}】；";
                if (!string.IsNullOrEmpty(vm.PurchaseTrackingNumber))
                    msg += $"采购追踪号：【{vm.PurchaseTrackingNumber}】";
                //todo 采购财务
                // var billLogId = await _billingRecordsService.AddBillLogNoNeedReviewWithSelfUse(new AddBillLogDto()
                // {
                //     Money = purchase.PurchaseTotal.Money,
                //     Type = Models.Finance.BillLog.Types.Deduction,
                //     Datetime = DateTime.Now,
                //     Reason = msg,
                //     Remark = "订单采购扣款",
                //     UserID = UserID,
                //     GroupID = GroupID,
                //     CompanyID = CompanyID,
                //     Truename = _sessionProvider.Session.GetTrueName(),
                //     ModuleId = purchase.ID,
                //     StoreId = purchase.StoreId,
                // }, await _dbContext.Company.FirstAsync(m => m.ID == CompanyID), Models.Finance.BillLog.Modules.PURCHASE);
                // purchase.BillLogId = billLogId;
                await _dbContext.Purchase.SingleUpdateAsync(purchase);
                await _dbContext.SaveChangesAsync();
            }
            await transaction.CommitAsync();
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            throw new Exceptions.Exception(e.Message);
        }
        return ReturnArr();
    }

    #region 订单详情获取采购信息
    public async Task<List<PurchaseModel>> GetPurchaseInfo(List<int>? orderIds = null, int? platform = null)
    {
        //    return PurchaseModel::query()
        //        ->OrderId($orderId)
        //        // ->Platform($platform)
        //        ->orderByDesc('id')
        //        ->get();
        return await _dbContext.Purchase
             .WhenWhere(orderIds is not null, a => orderIds!.Contains(a.OrderId!.Value))
             .WhenWhere(platform is not null, a => a.PlatformId == platform)
             .OrderByDescending(a => a.ID)
             .ToListAsync();
        //return ReturnArr();
    }
    #endregion

    #region 采购详情
    /// <summary>
    /// 采购详情
    /// </summary>
    /// <param name="id"></param>
    /// <param name="purchaseTrackingNumber"></param>
    /// <returns></returns>
    public async Task<PurchaseModel> Info(int id, string? purchaseTrackingNumber = null)
    {
        var row = await _dbContext.Purchase
             //$query = PurchaseModel::companyId($sessionData->getCompanyId());
             //.Where(a => a.CompanyID == CompanyID)
             .Where(a => a.ID == id)
             //$row = $query->purchaseTrackingNumber($purchaseTrackingNumber, false)->first();
             .WhenWhere(purchaseTrackingNumber is not null, a => a.PurchaseTrackingNumber == purchaseTrackingNumber)
             .FirstOrDefaultAsync();
        if (row == null) throw new Exception("没有查询到该信息");
        return row;
    }
    #endregion

    public async Task<bool> BatchEdit(BatchEditVM rq)
    {
        var purchase = await _dbContext.Purchase.SingleOrDefaultAsync(a => a.ID == rq.Id);
        if (purchase == null)
        {
            throw new Exception("没有查询到采购单");
        }
        purchase.PurchaseOrderNo = rq.PurchaseOrderNo;
        purchase.PurchaseTrackingNumber = rq.PurchaseTrackingNumber;
        await _dbContext.SaveChangesAsync();
        return true;
    }

    /// <summary>
    /// 获取采购详细信息
    /// </summary>
    /// <param name="id">采购id</param>
    /// <returns></returns>
    public async Task<PurchaseModel> GetPurchaseById(int id)
    {
        var purchase = await _dbContext.Purchase.SingleOrDefaultAsync(a => a.ID == id);
        if (purchase == null) throw new Exception($"未找到采购信息[ID:{id}]");
        return purchase;
    }

    /// <summary>
    /// 更新采购数据
    /// </summary>
    /// <param name="vm"></param>
    /// <returns></returns>
    public async Task UpdatePurchase(PurchaseModel vm)
    {
        await _dbContext.SaveChangesAsync();
    }

    public async Task UpdatePurchase()
    {
        await _dbContext.SaveChangesAsync();
    }
    #endregion



    /**
    * 获取分页列表
    * @param array $data 请求数据
    * @param $store_id
    * @param $store_ids
    * @return LengthAwarePaginator
    */
    public async Task<PaginateStruct<PurchaseListVm>> GetPaginateList(IndexVM data, int storeId)
    {
        List<int> ids = new();
        if (data.WarehouseId != null)
        {
            ids = await _stockService.GetLogPurchaseIdByWarehouse(data.WarehouseId.Value);
        }

        var store = await _storeService.CurrentUserGetStores();
        var storeIds = store.Select(a => a.ID).ToList();
        
        var list =  _dbContext.Purchase
            .Where(a => a.StoreId.HasValue && storeIds.Contains(a.StoreId.Value))
            .Where(a => a.CompanyID == CompanyID && a.OperateCompanyId == CompanyID)
            .WhenWhere(data.WarehouseId != null, a => a.WarehouseId.HasValue && ids.Contains(a.WarehouseId.Value))
            .WhenWhere(data.PurchaseState != null, a => a.PurchaseState == data.PurchaseState)
            .WhenWhere(storeId > 0, a => a.StoreId == storeId)
            .WhenWhere(data.PurchaseAffirm != null, a => a.PurchaseState == data.PurchaseState)
            .WhenWhere(data.OrderNo != null, a => a.OrderNo == data.OrderNo)
            .WhenWhere(data.PurchaseOrderNo != null, a => a.PurchaseOrderNo == data.PurchaseOrderNo)
            .WhenWhere(data.PurchaseTrackingNumber != null, a => a.PurchaseTrackingNumber == data.PurchaseTrackingNumber)
            .WhenWhere(data.Express != null, a => a.Express == data.Express)
            .OrderBy(m => m.OrderNo)
            .GroupBy(m => m.OrderId)
            .Select(m => new PurchaseListVm(m.OrderByDescending(m => m.CreatedAt).Select(i =>i).ToList()))
            .AsEnumerable()
            .ToPaginate();





        // var list = await _dbContext.Purchase
        //     .Where(a => storeIds.Contains(a.StoreId ?? 0) || a.StoreId == 0 || a.StoreId == null)
        //     .WhenWhere(data.WarehouseId != null, a => ids.Contains(a.WarehouseId ?? 0))
        //     .Where(a => a.CompanyID == CompanyID)
        //     .Where(a => a.OperateCompanyId == CompanyID)
        //     .WhenWhere(data.PurchaseState != null, a => a.PurchaseState == data.PurchaseState)
        //     .WhenWhere(storeId > 0, a => a.StoreId == storeId)
        //     //->StoreIds($store_ids)
        //     .WhenWhere(data.PurchaseAffirm != null, a => a.PurchaseState == data.PurchaseState)
        //     .WhenWhere(data.OrderNo != null, a => a.OrderNo == data.OrderNo)
        //     .WhenWhere(data.PurchaseOrderNo != null, a => a.PurchaseOrderNo == data.PurchaseOrderNo)
        //     .WhenWhere(data.PurchaseTrackingNumber != null, a => a.PurchaseTrackingNumber == data.PurchaseTrackingNumber)
        //     .WhenWhere(data.Express != null, a => a.Express == data.Express)
        //     .OrderByDescending(a => a.CreatedAt)
        //     .ToPaginateAsync();
        return list;
    }


    /// <summary>
    /// 获取采购日志
    /// </summary>
    /// <param name="purchaseId"></param>
    /// <returns></returns>
    public async Task<object> GetAllLog(int purchaseId)
    {
        return await _dbContext.PurchaseLog
            .Where(a => a.PurchaseId == purchaseId)
            .OrderByDescending(a => a.ID)
            .Select(a => new
            {
                a.ID,
                a.Action,
                a.TrueName,
                a.CreatedAt
            }).ToListAsync();
    }

    /// <summary>
    /// 无需找货pipeline
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> NoFindProductPipeLine(int id)
    {
        var info = await _dbContext.Purchase.Where(a => a.PurchaseState < PurchaseStates.PURCHASED).FirstOrDefaultAsync(a => a.ID == id);
        if (info is null)
        {
            return ReturnArr(false, "采购信息不存在");
        }
        //$company = Cache::instance()->company()->get();
        //$updateData = [
        //    'purchase_state'  => PurchaseModel::STATE_NOPURCHASEREQUIRED,
        //    'purchase_affirm' => PurchaseModel::PURCHASEAFFIRM_CONFIRMED,
        //    'affirm_uid'      => $this->getSession()->getUserId(),
        //    'affirm_truename' => $this->getSession()->getTruename(),
        //    'is_affirm'       => $company['purchase_audit'],
        //];
        info.PurchaseState = PurchaseStates.NOPURCHASEREQUIRED;
        info.PurchaseAffirm = PurchaseAffirms.CONFIRMED;
        _dbContext.Purchase.Update(info);

        //$info->where = $queryWhere;
        //$info->sessionData = $this->getSession();

        //return app(Pipeline::class)->send($info)
        //    ->through(
        //        [
        //            self::updateAmazonOrder(),
        //            self::updatePurchase(),
        //            self::createLogs(),
        //        ]
        //    )
        //    ->then(fn($purchase) => returnArr(true, '修改成功'));

        return ReturnArr();
    }


    //    /**
    //     * 更新亚马逊订单pipe
    //     * @return Closure
    //     */
    private static object updateAmazonOrder()
    {
        //    return function($purchase, Closure $next) {
        //            $order = [];
        //            $insertData = [];
        //        foreach ($purchase as $value) {
        //                $insertData[] = [
        //                    'platform_id' => $value->platform_id,
        //                    'order_id'    => $value->order_id,
        //                    'purchase_id' => $value->id,
        //                    'action'      => "设置该采购信息为无需采购",
        //                    'user_id'     => $purchase->sessionData->getUserId(),
        //                    'username'    => $purchase->sessionData->getUsername(),
        //                    'truename'    => $purchase->sessionData->getTruename(),
        //                    'group_id'    => $purchase->sessionData->getGroupId(),
        //                    'oem_id'      => $purchase->sessionData->getOemId(),
        //                    'company_id'  => $purchase->sessionData->getCompanyId(),
        //                    'created_at'  => date('Y-m-d H:i:s'),
        //                    'updated_at'  => date('Y-m-d H:i:s'),
        //                ];
        //                $order[$value->order_id] = isset($order[$value->order_id]) ? ($order[$value->order_id] + $value->num) : $value->num;
        //}

        //foreach ($order as $key => $value) {
        //                $updateOrder = [
        //                    'purchase_num' => Db::raw('d_purchase_num+'. $value),
        //                    // 'progress'     => 7,
        //                ];
        //    AmazonOrder::query()->where('id', $key)
        //        ->update($updateOrder);
        //}

        //            $purchase->insertData = $insertData;

        //return $next($purchase);
        //        };
        return new object();
    }//end updateAmazonOrder()


    //    /**
    //     * 添加采购日志pipe
    //     * @return Closure
    //     */
    private static object createLogs()
    {
        //    return function($purchase, Closure $next) {
        //        checkArr($purchase->insertData) && PurchaseLog::insert($purchase->insertData);
        //        return $next($purchase);
        //    };
        return new object();
    }//end createLogs()


    ///**
    // * 批量更新采购
    // * 给where属性赋值为query
    // * 更新数据数据为updateData
    // * @return Closure
    // */
    private static object updatePurchase()
    {
        //    return function($purchase, Closure $next) {
        //            $purchase->where->update($purchase->updateData);
        //        return $next($purchase);

        //    };
        return new object();
    }//end updatePurchase()

    ///**
    // * 编辑采购
    // * @param array $data input
    // * @return array
    // */
    public async Task<ReturnStruct> EditInfo(PurchaseModel vm)
    {
        //        $info = PurchaseModel::where('id', $data['id'])
        //            ->first();
        var info = await _dbContext.Purchase.FirstOrDefaultAsync(a => a.ID == vm.ID);
        if (info is null)
        {
            return ReturnArr(false, "当前采购信息不存在");
        }

        // $sessionData = $this->getSession();
        // $unit = $data['purchase_unit'] ?? $sessionData->getUnitConfig();
        //$updateData = [
        //    'purchase_url'             => ($data['purchase_url'] ?? ''),
        //    'purchase_order_no'        => $data['purchase_order_no'],
        //    'purchase_tracking_number' => $data['purchase_tracking_number'],
        //    'remark'                   => $data['remark'],
        //    'purchase_price'           => FromConversion($data['purchase_price'], $unit),
        //    'purchase_unit'            => $unit,
        //    'num'                      => $data['num'],
        //    'purchase_total'           => FromConversion($data['purchase_total'], $unit),
        //    // @todo 验证是否需要审核
        //    'purchase_affirm'          => ($data['purchase_affirm'] ?? PurchaseModel::PURCHASEAFFIRM_UNCONFIRMED),
        //];

        //isset($data['purchase_state']) && $updateData['purchase_state'] = $data['purchase_state'];
        //if (isset($data['is_stock']) && $data['is_stock'] === PurchaseModel::IS_STOCK_TRUE) {
        //    $warehouse = Warehouse::where('company_id', $this->getSession()->getCompanyId())
        //        ->orWhere('distribution_company_id', $this->getSession()->getCompanyId())
        //        ->where('id', $data['warehouse_id'])
        //        ->first();
        //    if (!$warehouse) {
        //        return returnArr(false, '仓库不存在');
        //    }

        //    $updateData = array_merge(
        //        $updateData,
        //        [
        //            'is_stock'       => PurchaseModel::IS_STOCK_TRUE,
        //            'warehouse_id'   => $warehouse->id,
        //            'warehouse_name' => $warehouse->name,
        //        ]
        //    );
        //}

        //// 添加采购日志.
        //$this->addPurchaseLog($info->order_id, $info->id, $info->platform_id, "修改采购信息");

        //if (!isset($data['purchase_state'])
        //    || $data['purchase_state'] != PurchaseModel::STATE_PURCHASED
        //    || (int)$this->getCompany()['purchase_audit'] === 1
        //) {
        //    PurchaseModel::where('id', $data['id'])->update($updateData);
        //    return returnArr(true, '编辑成功！');
        //}

        //$this->purchaseComplete($info, $updateData);

        //PurchaseModel::where('id', $data['id'])->update($updateData);


        //return returnArr(true, '编辑成功！');
        return ReturnArr();
    }


    //    /**
    //     * 采购确认获取采购信息
    //     * User: CCY
    //     * Date: 2020/7/10 15:43
    //     * @param           $purchase_order_no
    //     * @param           $purchase_tracking_number
    //     * @param int $viewRange
    //     * @return          Builder[]|Collection|PurchaseModel[]
    //     */
    public async Task<List<PurchaseModel>> GetPurchase(string purchase_order_no, string purchase_tracking_number, int viewRange = 2)
    {
        //$where = [
        //    ['company_id', '=', $this->getSession()->getCompanyId()],
        //    //过滤分销下级添加 上级搜索
        //    ['purchase_company_id', '=', 0]
        //];
        //if ($this->getSession()->getIsDistribution() === User::DISTRIBUTION_FALSE && $viewRange == 2) {
        //    // TODO 可能造成性能影响
        //    $orderIds = AmazonOrder::query()->oemId($this->getSession()->oem_id)
        //        ->where('purchase_company_id', 0)
        //        ->where('waybill_company_id', $this->getSession()->company_id)
        //        ->pluck('id');

        //    $where = function($query) use($orderIds, $where) {
        //        $query->where($where)->orWhere('purchase_company_id', $this->getSession()->getCompanyId());
        //        if ($orderIds->isNotEmpty()) {
        //            // 下级只上报运单时，允许上级采购确认
        //            $query->orWhereIn('order_id', $orderIds);
        //        }
        //    };
        //}
        //elseif($this->getSession()->getIsDistribution() === User::DISTRIBUTION_TRUE) {
        //    $where = function($query) use($where) {
        //        $query->where($where)->orWhere([
        //            ['company_id', '=', $this->getSession()->getCompanyId()],
        //            //过滤分销下级添加 上级搜索
        //            ['operate_company_id', '=', $this->getSession()->getCompanyId()]
        //        ]);
        //    };
        //}

        return await _dbContext.Purchase
            .Where(a => a.PurchaseOrderNo == purchase_order_no)
            .Where(a => a.PurchaseTrackingNumber == purchase_tracking_number)
            //.Where(a => a.CompanyID == )
            .Where(a => a.PurchaseCompanyId == 0)
            .OrderBy(a => a.PurchaseState)
            .ToListAsync();
        //.Select(a => new
        //{
        //    a.ID,
        //    a.Url,
        //    a.Name,
        //    a.Num,
        //    a.Price,
        //    a.CreatedAt,
        //    a.PurchaseState,
        //    a.OrderId,
        //    a.PurchaseTotal,
        //    a.PurchaseOrderNo,
        //    a.PurchaseTrackingNumber,
        //    a.PurchaseAffirm,
        //    a.PurchaseUnit,
        //    a.StoreId,
        //    a.CompanyID,
        //    a.IsStock,
        //    a.OEMID,
        //    a.Remark
        //});
    }


    /// <summary>
    /// 拒签
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<bool> Rejection(int id)
    {
        var list = await _dbContext.Purchase.FirstOrDefaultAsync(a => a.ID == id);
        if (list is null) return false;
        list.PurchaseState = PurchaseStates.INPROCUREMENT;
        list.PurchaseAffirm = PurchaseAffirms.UNCONFIRMED;
        var count = await _dbContext.SaveChangesAsync();
        return count > 0;
    }


    //    /**
    //     * 采购完成后置操作
    //     * 1.更新统计数据
    //     * 2.修改订单采购信息，添加订单日志
    //     * 3.财务扣款
    //     * 4.入库
    //     * @param array $updateData 更新字段
    //     * @param array|PurchaseModel $info 采购信息
    //     * @return void
    //     * @author ryu <mo5467@126.com>
    //     */
    private ReturnStruct purchaseComplete(/*$info, &$updateData*/)
    {
        //        // 是否为初次修改.
        //        $firstChangeFlag = false;
        //        if (!isset($info['id']) || $info['purchase_state'] !== PurchaseModel::STATE_PURCHASED) {
        //            // 添加或初次修改为已完成.
        //            $total = ($updateData['purchase_total'] ?? ($info instanceof PurchaseModel ? $info->getRawOriginal('purchase_total') : $info['purchase_total']));
        //            $firstChangeFlag = true;
        //        } else if ($updateData['purchase_total'] != $info->getRawOriginal('purchase_total')) {
        //            $total = ($updateData['purchase_total'] - $info->getRawOriginal('purchase_total'));
        //        } else {
        //            return;
        //        }

        //        // $total = ($updateData['purchase_total'] ?? $info['purchase_total']);
        //        $this->updateBusiness($total, ($info['store_id'] ?? null), $firstChangeFlag);

        //        if ($info['order_id']) {
        //            // 修改订单汇总.
        //            $updateOrder = [
        //                "purchase_fee"     => Db::raw('d_purchase_fee+' . $total),
        //                "profit"           => Db::raw('d_profit-' . $total),
        //                // 'progress'         => AmazonOrder::PROGRESS_PENDING,
        //                'process_purchase' => AmazonOrder::PROCESSPURCHASE_INSIDE,
        //                'purchase_num'     => Db::raw('d_purchase_num+' . $info['num']),
        //            ];
        //            AmazonOrder::where('id', $info['order_id'])->update($updateOrder);
        //            // 增加订单日志.
        //            $orderModel = new AmazonOrderService();
        //            $orderModel->addLog($info["order_id"], '', 0, "确认订单商品采购信息");
        //        }

        //        // 添加时没有id，需要单独处理.
        //        if (isset($info['id'])) {
        //            // 财务扣款记录.
        //            $updateData['bill_log_id'] = $this->updateBillLog($total, $info['company_id'], $info['id'], ($info['store_id'] ?? null), $info);
        //        }

        //        // 采购和确认用户都是自己.
        //        if (intval($this->getCompany()['purchase_audit']) !== 1) {
        //            $userId = $this->getSession()->getUserId();
        //            $truename = $this->getSession()->getTruename();
        //            $updateData = array_merge(
        //                $updateData,
        //                [
        //                    'purchase_user_id' => $userId,
        //                    'find_truename'    => $truename,
        //                    'affirm_uid'       => $userId,
        //                    'affirm_truename'  => $truename,
        //                ]
        //            );
        //        }

        //        $isStock = ($updateData['is_stock'] ?? $info['is_stock'] ?? null);
        //        $warehouseId = ($updateData['warehouse_id'] ?? $info['warehouse_id'] ?? null);
        //        // 采购入库.
        //        if ($firstChangeFlag && $isStock === PurchaseModel::IS_STOCK_TRUE) {
        //            if ($info['product_id'] && $info['variant_id']) {
        //                resolve(Stock::class)->callStockInWithProductId(
        //                    $info['product_id'],
        //                    $info['variant_id'],
        //                    $warehouseId,
        //                    ($updateData['num'] ?? $info['num']),
        //                    '采购添加',
        //                    0,
        //                    0,
        //                    ($updateData['order_no'] ?? $info['order_no']),
        //                    $info['id'] ?? 0,
        //                    $updateData['purchase_tracking_number'] ?? ''
        //                );
        //            } else {
        //                resolve(Stock::class)->callStockInWithoutId(
        //                    [
        //                        'product_name'    => $info['name'],
        //                        // 'purchasing_cost' => ToConversion(($updateData['purchase_total'] ?? $info['purchase_total']), $updateData['purchase_unit'] ?? $info['purchase_unit']),
        //                        'purchasing_cost' => 0,
        //                        'url'             => $info['url'],
        //                    ],
        //                    $warehouseId,
        //                    ($updateData['num'] ?? $info['num']),
        //                    '采购添加',
        //                    ($updateData['order_no'] ?? $info['order_no']),
        //                    $info['id'] ?? 0,
        //                    $updateData['purchase_tracking_number'] ?? ''
        //                );
        //            }//end if
        //        }//end if
        return ReturnArr();
    }


    //    /**
    //     * 更新统计
    //     * @param int|null $storeId 店铺id
    //     * @param bool $first 初次更新
    //     * @param float $money money
    //     * @return void
    //     * @author ryu <mo5467@126.com>
    //     */
    private ReturnStruct updateBusiness(/*$money, $storeId = null, $first = false*/)
    {
        //        $num = $first ? 1 : 0;
        //        (new CounterUserBusiness())->changePurchaseNumAndTotal($this->getSession()->getUserId(), $money, $num);
        //        // 修改店铺计数.
        //        $storeId && (new CounterStoreBusiness())->addPurchaseTotal($storeId, $money);
        //        // 月报.
        //        (new CounterRealFinance())->incrementPurchaseTotal($this->getCompany(), $money);
        //        (new CounterGroupBusiness())->changePurchaseNumAndTotal($this->getSession()->getGroupId(), $money, $num);
        //        // 当前公司计数+1.
        //        (new CounterCompanyBusiness())->changePurchaseNumAndTotal($this->getSession()->getCompanyId(), $money, $num);

        return ReturnArr();
    }//end updateBusiness()


    //    /**
    //     * 更新私人钱包
    //     * @param int $companyId 公司id
    //     * @param int $id id
    //     * @param int|null $storeId 店铺id
    //     * @param PurchaseModel $info 采购obj
    //     * @param float $money 金额
    //     * @return int
    //     * @author ryu <mo5467@126.com>
    //     */
    private ReturnStruct updateBillLog(/*$money, $companyId, $id, $storeId = null, $info = null*/)
    {
        //        (new UserCompany())->expenseWallet($money);

        //        $msg = '';
        //        (isset($info['order_no']) && $info['order_no'] != '') && $msg = '订单编号：' . $info['order_no'] . ';';
        //        (isset($info['purchase_tracking_number']) && $info['purchase_tracking_number'] != '') && $msg .= '物流追踪号：' . $info['purchase_tracking_number'];

        //        return (new BillLog())->makeBillLog(abs($money), $companyId, $id, BillLogModel::BELONG_SELFUSE, $storeId, 2, $msg, $money > 0 ? 2 : 1);
        return ReturnArr();
    }//end updateBillLog()

    //    /**
    //     * 新增或更新采购
    //     * @param int $purchaseId 采购id
    //     * @param array $data data
    //     * @return void
    //     */
    public ReturnStruct insertOrUpdate(/*$purchaseId, &$data*/)
    {
        //        $newData = $data;
        //        unset($newData['purchase_id']);

        //        $purchase = PurchaseModel::find($purchaseId);
        //        if (!$purchase) {
        //            $row = PurchaseModel::create($newData);
        //            $data['purchase_id'] = $row->id;
        //        } else {
        //            PurchaseModel::where('id', $purchaseId)->update($newData);
        //        }

        //        return;
        return ReturnArr();
    }//end insertOrUpdate()


    //    /**
    //     * 更新物流追踪号
    //     * @param mixed $purchase 采购id|采购类
    //     * @param string $trackingNumber 运单号
    //     * @return bool
    //     */
    public async Task<ReturnStruct> EditTrackingNumber(/*$purchase, $trackingNumber*/)
    {
        //        if (is_int($purchase)) {
        //            $purchase = PurchaseModel::find($purchase);
        //            if (!$purchase) {
        //                return false;
        //            }
        //        }

        //        $beforeTrackingNumber = $purchase->purchase_tracking_number;
        //        $purchase->purchase_tracking_number = $trackingNumber;
        //        return $purchase->save();
        //        // @todo: 更新物流表数据
        //        $waybill = WayBill::tracking($beforeTrackingNumber)->first();
        return await Task.FromResult(ReturnArr());
    }//end editTrackingNumber()

    //    /******************************** 采购新增、编辑合并 公共函数********************************/
    /// <summary>
    /// x add
    /// </summary>
    /// <param name="data"></param>
    /// <param name="type">1内部、分销下级 2分销上级添加</param>
    /// <returns></returns>
    public async Task<int> AddPurchase(PurchaseModel data, int type = 1)
    {
        _dbContext.Add(data);
        int count = await _dbContext.SaveChangesAsync();
        return count;
    }

    //    /**
    //     * 新增采购入库
    //     * @param $requestData
    //     * @param $purchaseId
    //     * @param $purchase_tracking_number
    //     * @param $order
    //     * @param $warehouse
    //     * @return array
    //     * User: CCY
    //     * Date: 2020/10/23
    //     */
    public ReturnStruct addPurchaseStock(/*$requestData, $purchaseId, $purchase_tracking_number, $order, $warehouse*/)
    {
        //        $Stock = resolve(Stock::class);
        //        if (isset($requestData['product_id']) && $requestData['product_id'] && isset($requestData['variant_id']) && $requestData['variant_id']) {
        //            return $Stock->callStockInWithProductId(
        //                $requestData['product_id'],
        //                $requestData['variant_id'],
        //                $warehouse['id'],
        //                $requestData['num'],
        //                '采购添加',
        //                0,
        //                0,
        //                $order['order_no'],
        //                $purchaseId,
        //                $purchase_tracking_number
        //            );
        //        } else {
        //            $Stock->callStockInWithoutId(
        //                [
        //                    'product_name'    => $requestData['name'] ?? '',
        //                    'purchasing_cost' => $requestData['purchase_total'],
        //                    'url'             => $requestData['url'],
        //                ],
        //                $warehouse['id'],
        //                $requestData['num'],
        //                '采购添加',
        //                $order['order_no'],
        //                $purchaseId,
        //                $purchase_tracking_number
        //            );
        //        }
        return ReturnArr();
    }

    //    /******************** 批量采购 ****************************/

    //    /**
    //     * 获取批量回填需要采购信息
    //     * @param $orderId
    //     * @return PurchaseModel[]|Builder[]|Collection
    //     * User: CCY
    //     * Date: 2021/2/1
    //     */
    public async Task<List<PurchaseModel>> GetPurchaseByOrderIds(List<int> orderIds)
    {
        //        $field = ['id', 'order_id', 'name', 'purchase_state', 'url', 'order_no', 'from_url', 'purchase_total', 'purchase_order_no', 'purchase_tracking_number'];
        //        return PurchaseModel::query()
        //            ->CompanyId($this->getSession()->getCompanyId())
        //            ->OrderId($orderId)
        //            ->orderByDesc('id')
        //            ->get($field);
        return await _dbContext.Purchase.Where(a => a.OrderId.HasValue && orderIds.Contains(a.OrderId.Value)).ToListAsync();
    }
    public async Task<List<PurchaseModel>> GetPurchaseByOrderIdsWithInprocurement(List<int> orderIds)
    {
        //        $field = ['id', 'order_id', 'name', 'purchase_state', 'url', 'order_no', 'from_url', 'purchase_total', 'purchase_order_no', 'purchase_tracking_number'];
        //        return PurchaseModel::query()
        //            ->CompanyId($this->getSession()->getCompanyId())
        //            ->OrderId($orderId)
        //            ->orderByDesc('id')
        //            ->get($field);
        return await _dbContext.Purchase
            .Where(a => a.OrderId.HasValue && orderIds.Contains(a.OrderId.Value) && a.PurchaseState == PurchaseStates.INPROCUREMENT)
            .ToListAsync();
    }

    //    /**
    //     * 获取批量回填需要采购信息
    //     * @param $purchaseIds
    //     * @param $field
    //     * @return PurchaseModel[]|Builder[]|Collection
    //     * User: CCY
    //     * Date: 2021/2/1
    //     */
    public async Task<List<PurchaseModel>> GetPurchaseByIds(List<int> purchaseIds)
    {
        //        !checkArr($field) && $field = ['id', 'name', 'purchase_state', 'url', 'order_no', 'from_url', 'purchase_total', 'purchase_order_no', 'purchase_tracking_number'];
        //        return PurchaseModel::query()
        //            ->CompanyId($this->getSession()->getCompanyId())
        //            ->IdIn($purchaseIds)
        //            ->orderByDesc('id')
        //            ->get($field);
        return await _dbContext.Purchase.Where(a => purchaseIds.Contains(a.ID)).ToListAsync();
    }


    public async Task<List<int?>> GetPurchaseIDs(string? purchase_order_no, string? purchase_tracking_number)
    {
        return await _dbContext.Purchase
            .WhenWhere(!purchase_order_no.IsNullOrWhiteSpace(), a => a.PurchaseOrderNo == purchase_order_no)
            .WhenWhere(!purchase_tracking_number.IsNullOrWhiteSpace(), a => a.PurchaseTrackingNumber == purchase_tracking_number).Select(a => a.OrderId).Distinct().ToListAsync();
        
    }
}

