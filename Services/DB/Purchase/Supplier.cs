﻿
namespace ERP.Services.Purchase;

using ERP.Data;
using Extensions;
using Microsoft.EntityFrameworkCore;
using SupplierModel = Models.DB.Purchase.Supplier;
public class Supplier : BaseService
{
    private readonly DBContext _MyDbContext;
    public Supplier(DBContext myDbContext)
    {
        _MyDbContext = myDbContext;
    }

    public async Task<DbSetExtension.PaginateStruct<SupplierModel>> SupplierList(Controllers.Purchase.Supplier.IndexQuery req)
    {
        return await _MyDbContext.Supplier
            //->companyId($this->getSession()->getCompanyId())
            .WhenWhere(req.Name != null, a =>(a.Name !=null? a.Name:string.Empty).StartsWith(req.Name!))
            .OrderByDescending(a => a.CreatedAt)
            .ToPaginateAsync(page:req.Page,limit:req.Limit);
    }

    public async Task<ReturnStruct> Add(SupplierModel vm)
    {
        var row = await _MyDbContext.Supplier.FirstOrDefaultAsync(a => a.Name == vm.Name);
        //->where('user_id', $this->getSession()->getUserId())
        if (row != null)
            return ReturnArr(false, "采购商已存在");

        //$insertData = [
        //    'name'       => $input['name'],
        //    'phone'      => ($input['phone'] ?? ''),
        //    'address'    => ($input['address'] ?? ''),
        //    'remark'     => ($input['remark'] ?? ''),
        //    'user_id'    => $this->getSession()->getUserId(),
        //    'company_id' => $this->getSession()->getCompanyId(),
        //    'group_id'   => $this->getSession()->getGroupId(),
        //    'oem_id'     => $this->getSession()->getOemId(),
        //];
        await _MyDbContext.Supplier.AddAsync(vm);
        await _MyDbContext.SaveChangesAsync();
        return ReturnArr();
    }

    public async Task<ReturnStruct> Edit(SupplierModel vm)
    {
        var row = await _MyDbContext.Supplier.AnyAsync(a => a.ID == vm.ID);
        //->where('user_id', $this->getSession()->getUserId())
        if (!row)
            return ReturnArr(false, "未查询到信息");

        var uniqueName = await _MyDbContext.Supplier.FirstOrDefaultAsync(a => a.Name == vm.Name && a.ID != vm.ID);
        if (uniqueName != null)
            return ReturnArr(false, "供应商已存在");
        _MyDbContext.Supplier.Update(vm);
        await _MyDbContext.SaveChangesAsync();
        return ReturnArr();
    }

    public async Task<ReturnStruct> Info(int id)
    {
        var row = await _MyDbContext.Supplier.FirstOrDefaultAsync(a => a.ID == id);
        if (row == null)
            return ReturnArr(false, "未查询到信息");
        return ReturnArr(true, "", row);
    }

    public async Task<ReturnStruct> Delete(int id)
    {
        var row = await _MyDbContext.Supplier.FirstOrDefaultAsync(a => a.ID == id);
        if (row == null)
            return ReturnArr(false, "未查询到信息");
        _MyDbContext.Supplier.Remove(row);
        await _MyDbContext.SaveChangesAsync();
        return ReturnArr();
    }
}

