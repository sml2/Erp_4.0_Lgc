﻿using ERP.Extensions;
using Microsoft.EntityFrameworkCore;
using ERP.Data;
namespace ERP.Services.Purchase;
using PurchasingChannelsModel = Models.DB.Purchase.PurchasingChannels;
public class PurchasingChannels : BaseService
{
    private readonly DBContext _MyDbContext;
    public PurchasingChannels(DBContext myDbContext)
    {
        _MyDbContext = myDbContext;
    }

    public async Task<ReturnStruct> List(string? name)
    {
        var list = await _MyDbContext.PurchasingChannels
            //->companyId($this->getSession()->getCompanyId())
            .WhenWhere(name != null, a => a.SupplierName.Contains(name!))
            //->searchSupplier(($input['name'] ?? null))
            //->searchProduct(($input['name'] ?? null))
            //->searchWarehouse(($input['name'] ?? null))
            .OrderByDescending(a => a.CreatedAt)
            .ToPaginateAsync();
        return ReturnArr(true, "", list);
    }

    public async Task<ReturnStruct> Add(PurchasingChannelsModel vm)
    {
        var row = await _MyDbContext.PurchasingChannels.Where(a => a.Name == vm.Name).FirstAsync();
        //->companyId($this->getSession()->getCompanyId())
        if (row != null)
            return ReturnArr(false, "采购渠道已存在");

        //$variants = isset($input['variants']) ? ($input['variants'] ?: null) : null;
        //$variants = $variants ?: json_encode([]);

        //$insertData = [
        //    'name'           => $input['name'],
        //    'supplier_id'    => $input['supplier_id'],
        //    'supplier_name'  => $input['supplier_name'],
        //    'product_id'     => $input['product_id'],
        //    'product_name'   => $input['product_name'],
        //    'warehouse_id'   => (isset($input['warehouse_id']) ? ($input['warehouse_id'] ?: 0) : 0),
        //    'warehouse_name' => ($input['warehouse_name'] ?? ''),
        //    'variants'       => $variants,
        //    'user_id'        => $this->getSession()->getUserId(),
        //    'company_id'     => $this->getSession()->getCompanyId(),
        //    'group_id'       => $this->getSession()->getGroupId(),
        //    'oem_id'         => $this->getSession()->getOemId(),
        //];
        await _MyDbContext.PurchasingChannels.AddAsync(vm);
        await _MyDbContext.SaveChangesAsync();
        return ReturnArr();
    }

    public async Task<ReturnStruct> Edit(PurchasingChannelsModel vm)
    {
        var row = await _MyDbContext.PurchasingChannels
            //->companyId($this->getSession()->getCompanyId())
            .Where(a => a.Name == vm.Name).FirstAsync();
        if (row == null)
            return ReturnArr(false, "未查询到信息");

        if (row.Name != vm.Name)
        {
            var uniqueName = await _MyDbContext.PurchasingChannels
                .FirstOrDefaultAsync(a => a.Name == vm.Name);
            if (uniqueName != null)
                return ReturnArr(false, "供应商已存在");
            row.Name = vm.Name;
        }

        // @todo 验证sup,prod,warehouse是否存在
        //isset($input['supplier_id']) && $row->supplier_id = $input['supplier_id'];
        //isset($input['supplier_name']) && $row->supplier_name = $input['supplier_name'];
        //isset($input['product_id']) && $row->product_id = $input['product_id'];
        //isset($input['product_name']) && $row->product_name = $input['product_name'];
        //isset($input['warehouse_id']) && $row->warehouse_id = $input['warehouse_id'] ?: 0;
        //isset($input['warehouse_name']) && $row->warehouse_name = $input['warehouse_name'];
        //isset($input['variants']) && $row->variants = $input['variants'] ?: '[]';

        _MyDbContext.PurchasingChannels.Update(vm);
        await _MyDbContext.SaveChangesAsync();
        return ReturnArr();
    }

    public async Task<ReturnStruct> Info(int id)
    {
        var row = await _MyDbContext.PurchasingChannels.FirstOrDefaultAsync(a => a.ID == id);
        if (row == null)
            return ReturnArr(false, "未查询到信息");
        return ReturnArr(true, "", row);
    }

    public async Task<ReturnStruct> Delete(List<int> ids)
    {
        var row = _MyDbContext.PurchasingChannels.Where(a => ids.Contains(a.ID));
        if (row == null)
            return ReturnArr(false, "未查询到信息");
        _MyDbContext.PurchasingChannels.RemoveRange(row);
        await _MyDbContext.SaveChangesAsync();
        return ReturnArr();
    }
}

