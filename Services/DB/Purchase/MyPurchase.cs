﻿using ERP.Extensions;
using ERP.Data;
namespace ERP.Services.Purchase;

public class MyPurchase : BaseService
{
    private readonly DBContext _MyDbContext;
    public MyPurchase(DBContext myDbContext)
    {
        _MyDbContext = myDbContext;
    }

    public async Task<ReturnStruct> MyPurchaseList(Enums.Purchase.PurchaseStates? state)
    {
        //$sessionData = $this->getSession();
        var list = await _MyDbContext.Purchase
            //->PurchaseUserId($sessionData->getUserId())
            //->companyId($this->getSession()->getCompanyId())
            .WhenWhere(state != null, a => a.PurchaseState == state)
            //.Where(a => a.PurchaseCompanyId == 0)
            .OrderBy(a => a.PurchaseState)
            .ToPaginateAsync();
        return ReturnArr(true, "", list);
    }
}
