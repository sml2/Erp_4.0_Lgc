﻿using ERP.Data;
using ERP.Extensions;
using Microsoft.EntityFrameworkCore;
using static ERP.Models.DB.Purchase.Purchase;
using static ERP.Models.DB.Purchase.PurchasePick;

namespace ERP.Services.Purchase;
using PurchaseModel = Models.DB.Purchase.Purchase;
using PurchasePickModel = Models.DB.Purchase.PurchasePick;

public class AffirmPurchase : BaseService
{
    private readonly DBContext _MyDbContext;
    public AffirmPurchase(DBContext myDbContext)
    {
        _MyDbContext = myDbContext;
    }

    /// <summary>
    /// 找货列表分页
    /// </summary>
    /// <returns></returns>
    public async Task<ReturnStruct> FindProductList(int id)
    {
        var list = _MyDbContext.PurchasePick
             .Where(a => a.PurchaseId == id)
             .OrderBy(a => a.State)
             .ThenBy(a => a.CreatedAt);
        return ReturnArr(true, "", new { list = await list.ToListAsync(), count = await list.CountAsync() });
    }//end findProductList()


    /// <summary>
    /// 待找货列表
    /// </summary>
    /// <returns></returns>
    public async Task<ReturnStruct> AffirmPurchaseList()
    {
        var list = await _MyDbContext.Purchase
            //->companyId($this->getSession()->getCompanyId())
            .Where(a => a.PurchaseState == Enums.Purchase.PurchaseStates.INPROCUREMENT)
            .Where(a => a.PurchaseAffirm == Enums.Purchase.PurchaseAffirms.UNCONFIRMED)
            .Where(a => a.PurchaseCompanyId == 0)
            .OrderBy(a => a.ID)
            .ToPaginateAsync();
        return ReturnArr(true, "", list);
    }


    /// <summary>
    /// 添加找货
    /// </summary>
    /// <returns></returns>
    public async Task<ReturnStruct> FindProductAdd(PurchasePickModel purchasePick)
    {
        //purchasePick.OEMID = 0;
        //purchasePick.CompanyID = 0;
        //purchasePick.GroupID = 0;
        //purchasePick.UserID = 0;
        purchasePick.TrueName = "";
        //purchasePick.Unit = purchasePick.Unit ?? $this->getSession()->getUnitConfig();
        //purchasePick.Price = await FromConversion(purchasePick.Price, purchasePick.Unit);
        purchasePick.State = Enums.Purchase.States.TOBECONFIRMED;

        await _MyDbContext.PurchasePick.AddAsync(purchasePick);
        //// 添加采购日志.
        //(new Purchase())->addPurchaseLog(
        //    $info->order_id,
        //    $info->id,
        //    $info->platform_id,
        //    "添加找货信息"
        //);
        await _MyDbContext.SaveChangesAsync();
        return ReturnArr();
    }
    /**
     * 将金额转换后存储数据库
     * @param float  $money 当前需要转换的金额
     * @param string $unit  当前币种
     * @param null   $rate
     * @return float|int 转换后存储进入数据库的数据
     */
    private async Task<decimal> FromConversion(/*$money, $unit, $rate = null*/)
    {
        //if ($money == 0) {
        //    return $money;
        //}
        //if ($rate === null || $rate == 0) {
        //$rate = $unit == 'CNY' ? 10000 : Cache::instance()->unitSingle()->get($unit)['rate'];
        //}
        //// FIXME:  bcmul(): bcmath function argument is not well-formed
        //return number_format(bcmul($money, $rate, 6), 6, '.', '');
        return await Task.FromResult(0);
    }

    public async Task<ReturnStruct> FindProductInfo(int id)
    {
        var info = await _MyDbContext.PurchasePick
            //->authByUser($this->getSessionObj())
            .FirstOrDefaultAsync(a => a.ID == id);
        return ReturnArr(true, "", info);
    }

    public async Task<ReturnStruct> ProductPickEdit(PurchasePickModel vm)
    {
        _MyDbContext.PurchasePick.Update(vm);
        int count = await _MyDbContext.SaveChangesAsync();
        if (count == 1)
            return ReturnArr(true, "编辑成功");
        else
            return ReturnArr(false, "编辑失败");
    }

    ///**
    // * 确认采购找货信息 type 1=确认，2=否决
    // *
    // * @param Purchase $purchase purchase
    // * @param array    $input    input
    // *
    // * @return array
    // * @author ryu <mo5467@126.com>
    // */
    public async Task<ReturnStruct> FindProductAffirm(PurchaseModel purchase, PurchasePickModel vm)
    {
        var purchasePick = await _MyDbContext.PurchasePick.FirstOrDefaultAsync(a => a.ID == vm.ID);
        if (2 == 2)
        {
            if (purchasePick == null) { return ReturnArr(false, "没有找货单信息"); }
            purchasePick.State = Enums.Purchase.States.VETO;
            _MyDbContext.PurchasePick.Update(purchasePick);
            await _MyDbContext.SaveChangesAsync();
            return ReturnArr(true, "否决成功");
        }

        //return ReturnArr(true, "确认成功");
        // 更新采购信息.
        //$updateData = [
        //    'purchase_unit'    => $info->unit,
        //    'purchase_price'   => $info->getRawOriginal('price'),
        //    'purchase_total'   => $info->getRawOriginal('price') * $purchase['num'],
        //    'purchase_url'     => $info['url'],
        //    'purchase_state'   => PurchaseModel::STATE_INPROCUREMENT,
        //    'purchase_affirm'  => PurchaseModel::PURCHASEAFFIRM_CONFIRMED,
        //    'purchase_user_id' => $info['user_id'],
        //    'find_truename'    => $info['truename'],
        //    'is_affirm'        => PurchaseModel::ISAFFIRM_YES,
        //    'pick_id'          => $id,
        //    'affirm_uid'       => $sessionData->getUserId(),
        //    'affirm_truename'  => $sessionData->truename,
        //    'remark'           => $info['remark'],
        //];
        // 当前数据确认.
        //PurchasePick::query()->where('id', $id)->update(['state' => PurchasePick::CONFIRM]);
        //// 其余数据否决.
        //PurchasePick::query()->where('id', '<>', $id)
        //    ->update(['state' => PurchasePick::VETO]);

        //PurchaseModel::query()->where('id', $purchaseId)->update($updateData);
        //// 添加采购日志.
        //(new Purchase())->addPurchaseLog($purchase["order_id"], $purchase["id"], $purchase["platform_id"], "确认采购信息");
        //// 增加订单日志.
        //(new OrderAmazonOrder())->addLog($purchase["order_id"], '待发货', 7, "确认订单商品采购信息");


    }

    public async Task<PurchaseModel?> GetPurchaseId(int ID)
    {
        return await _MyDbContext.Purchase.FirstOrDefaultAsync(x => x.ID == ID);
    }
}

