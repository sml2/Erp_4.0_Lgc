using System;
using ERP.Extensions;
using ERP.Models.Setting;
using Microsoft.EntityFrameworkCore;
using Model = ERP.Models.PlatformData;
using static ERP.Extensions.DbSetExtension;
using ERP.Data;
namespace ERP.Services.DB;
public class PlatformData : BaseService
{
    public readonly DBContext _dbContext;

    public PlatformData(DBContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<IList<Model>> Load()
    {
        return await _dbContext.PlatformData.Include(x=>x.Nation).AsNoTracking().ToListAsync();
    }
}

