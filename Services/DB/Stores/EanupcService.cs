using System.Globalization;
using System.Linq;
using System.Text;
using ERP.Data;
using ERP.Enums.Rule.Config.Internal;
using ERP.Enums.Stores;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.DB.Stores;
using ERP.Models.Stores;
using ERP.ViewModels.Store.Eanupc;
using Microsoft.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json;
using Ryu.Linq;
using Z.EntityFramework.Plus;
using Enumerable = System.Linq.Enumerable;
using Task = System.Threading.Tasks.Task;

namespace ERP.Services.Stores;

public class EanupcService : BaseWithUserService
{
    public readonly int _generate;


    public EanupcService(DBContext dbContext, ISessionProvider session) : base(dbContext,session)
    {
        _generate = 1000;
    }



    /// <summary>
    /// 数量
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    // private async Task<int> GetCodeNum(EanupcModel.TypeEnum type)
    // {
    //     
    //     var companyId = _companyId;
    //     var oemId = _oemId;
    //     var userId = _userId;
    //
    //     //todo EanupcService GetCodeNum 查看范围
    //     //todo 因无权限模块 暂定公司维度
    //     return await _dbContext.Eanupc
    //         .CountAsync(m => m.OEMID == oemId && m.CompanyID == companyId && m.Type == type);
    // }

    private IQueryable<EanupcModel>? DataWatchRange(DataRange_2b range)
    {
        if (range == DataRange_2b.None)
            return null;
        
        var model = _dbContext.Eanupc.Where(m => m.OEMID == _oemId && m.CompanyID == _companyId);

        if (range == DataRange_2b.Group)
            model = model.Where(m => m.GroupID == _groupId);

        if (range == DataRange_2b.User)
            model = model.Where(m => m.UserID == (_userId));

        return model;
    }

    [Obsolete("20230804")]
    public async Task<EanupcListVM> GetList(EanupcListDto req,DataRange_2b range)
    {
        var model = DataWatchRange(range);

        var list = new DbSetExtension.PaginateStruct<EanupcModel>();
        if (model is not null)
        {
            list= await model.Where(m => m.OEMID == _oemId && m.CompanyID == _companyId)
                .WhenWhere(req.Type != null, m => m.Type == req.Type)
                .WhenWhere(req.Select == EanupcModel.SelectModeEnum.Vague, m => m.Value.Contains(req.Value))
                .WhenWhere(req.Select == EanupcModel.SelectModeEnum.Accurate, m => m.Value == req.Value)
                .ToPaginateAsync(page: req.Page, limit: req.Limit); /*page: req.Page, limit: req.Limit*/
        }
        return new EanupcListVM(await GetCodeNum(EanupcModel.TypeEnum.EAN,range), await GetCodeNum(EanupcModel.TypeEnum.UPC,range),
            GetRangeTotal(range), list);
    }
    
    public IQueryable<EanupcModel> GetListQuery(DataRange_2b range)
    {
        var model = DataWatchRange(range);

        if (model is not null)
        {
            return model.Where(m => m.OEMID == _oemId && m.CompanyID == _companyId);
        }
        return Enumerable.Empty<EanupcModel>().AsQueryable();
    }

    public int GetRangeTotal(DataRange_2b range)
    {
        return range switch
        {
            DataRange_2b.Group => 6000,
            DataRange_2b.User => 4000,
            DataRange_2b.Company => 10000,
            _ => 5000
        };
    }

    public async Task<int> GetCodeNum(EanupcModel.TypeEnum type, DataRange_2b range)
    {
        var model = DataWatchRange(range);
        if (model is null)
            return 0;
        
        return await model
            .CountAsync(m => m.Type == type);
    }


    /// <summary>
    /// 获取账号权限对应Ean/Upc数量
    /// </summary>
    /// <returns></returns>
    // private int GetRangeTotal()
    // {
    //     //todo EanupcService GetRangeTotal 权限
    //     // $range = $this->getSession()->getRule()->getStore()->getBiter()->getEanupcRange();
    //
    //     int total = 5000;
    //     var range = EanupcModel.RangeEnum.Group;
    //
    //     switch (range)
    //     {
    //         case EanupcModel.RangeEnum.Group:
    //             total = 6000;
    //             break;
    //         case EanupcModel.RangeEnum.User:
    //             total = 4000;
    //             break;
    //         case EanupcModel.RangeEnum.Company:
    //             total = 10000;
    //             break;
    //     }
    //
    //     return total;
    // }

    // private async Task<bool> CheckCodeNum(EanupcModel.TypeEnum type)
    // {
    //     //获取当前已有数量
    //     var num = await GetCodeNum(type);
    //     //获取用户可生成的总数量
    //     var total = GetRangeTotal();
    //
    //     //一次生成数量
    //     var generate = _generate;
    //
    //     //剩余可生成的数量
    //     var surplus = total - num;
    //
    //     if (surplus < generate)
    //     {
    //         generate = surplus;
    //     }
    //
    //     if (total == num || (num + generate) > total)
    //     {
    //         return false;
    //     }
    //
    //     return true;
    // }

   
   
    /// <summary>
    /// 获取当前可生成的数量
    /// </summary>
    /// <param name="type"></param>
    /// <param name="range"></param>
    /// <returns></returns>
    private async Task<int> GetGenerateNum(EanupcModel.TypeEnum type, DataRange_2b range)
    {
        //获取当前已有数量
        var num = await GetCodeNum(type,range);
        //获取用户可生成的总数量
        var total = GetRangeTotal(range);
        //一次生成数量
        var generate = _generate;
        //剩余可生成的数量
        var surplus = total - num;

        if (surplus > generate)
        {
            return generate;
        }

        return surplus > 0 ? surplus : 0;
    }
    /// <summary>
    /// 新增EAN
    /// </summary>
    /// <param name="req"></param>
    /// <param name="range"></param>
    /// <returns></returns>
    public async Task<Result> AddCode(EAN req,DataRange_2b range)
    {
        //一次生成数量
        var generate = await GetGenerateNum(EanupcModel.TypeEnum.EAN,range);
        if (generate <= 0)
        {
            return Result.Fail("当前数量已满");
        }
        
        var date = DateTime.Now;

        var userId = _userId;
        var groupId = _groupId;
        var companyId = _companyId;
        var oemId = _oemId;

        for (int i = 0; i < generate; i++)
        {
            string strCodeLong = req.EanCode + req.EanManufacturer + "0" + req.EanGoods;

            if (strCodeLong.Length == 12)
            {
                var ean = GetEanCode(strCodeLong);
                if (ean.Length == 13)
                {
                    _dbContext.Eanupc.Add(new EanupcModel
                    {
                        Type = EanupcModel.TypeEnum.EAN,
                        Value = ean,
                        HashCode = ean.GetHashCode(),
                        UserID = userId,
                        GroupID = groupId,
                        CompanyID = companyId,
                        OEMID = oemId,
                        CreatedAt = date,
                        UpdatedAt = date
                    });
                }
            }

            req.EanGoods++;
        }

        await _dbContext.SaveChangesAsync();
        return Result.Ok().WithSuccess(generate != _generate ? $"根据当前剩余数量,此次只生成{generate}个" : "生成成功");
    }

   

    /// <summary>
    /// 新增UPC
    /// </summary>
    /// <param name="req"></param>
    /// <param name="range"></param>
    /// <returns></returns>
    public async Task<string> AddCode(UPC req,DataRange_2b range)
    {
        //一次生成数量
        var generate = await GetGenerateNum(EanupcModel.TypeEnum.UPC,range);
        if (generate <= 0)
        {
            throw new Exception("当前数量已满");
        }

        var date = DateTime.Now;

        var userId = _userId;
        var groupId = _groupId;
        var companyId = _companyId;
        var oemId = _oemId;

        for (int i = 0; i < generate; i++)
        {
            if (req.UpcGoods > 99999)
            {
                req.UpcGoods = (int) Math.Floor((decimal) (req.UpcGoods / 10));
            }

            var strCodeLong = req.UpcCode + req.UpcManufacturer + req.UpcGoods;

            if (strCodeLong.Length == 11)
            {
                var upc = await GetUpcCode(strCodeLong);

                if (upc.Length == 12)
                {
                    await _dbContext.Eanupc.AddAsync(new EanupcModel
                    {
                        Type = EanupcModel.TypeEnum.UPC,
                        Value = upc,
                        HashCode = upc.GetHashCode(),
                        UserID = userId,
                        GroupID = groupId,
                        CompanyID = companyId,
                        OEMID = oemId,
                        CreatedAt = date,
                        UpdatedAt = date
                    });
                }
            }

            req.UpcGoods++;
        }

        await _dbContext.SaveChangesAsync();

        return generate != _generate ? $"根据当前剩余数量,此次只生成{generate}个" : "生成成功";
    }


    public async Task<AddVM> AddEanupcParams()
    {
        List<Object> options = new List<Object>()
        {
            new {value = 1, label = 1},
            new {value = 6, label = 6},
            new {value = 7, label = 7},
            new {value = 8, label = 8},
            new {value = 9, label = 9},
        };
        var rd = new Random();
        var key = rd.Next(options.Count);


        var itme = options[key];
        var info = new
        {
            type = 1,
            //生成EAN
            ean_code = rd.Next(100, 999),
            ean_manufacturer = rd.Next(1000, 9999),
            ean_goods = rd.Next(1000, 9999),
            //生成UPC
            upc_code = itme.GetPropOrFieldValue("value"),
            upc_manufacturer = rd.Next(10000, 99999),
            upc_goods = rd.Next(10000, 99999)
        };
        var enumString = await GetCodeEnum();
        Dictionary<string, Dictionary<string, object>>? codeDictionary =
            new Dictionary<string, Dictionary<string, object>>();
        if (enumString != null)
        {
            codeDictionary = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, object>>>(enumString);
        }

        return new AddVM(info, codeDictionary, options);
    }

    private async Task<string?> GetCodeEnum()
    {
        string path = @"Config/Code.json";
        // 此文本只添加到文件一次。
        if (File.Exists(path))
        {
            string readText = File.ReadAllText(path);
            return await Task.FromResult(readText);
        }

        return null;
    }


    public string GetFileJson(string filepath)
    {
        string json = string.Empty;
        using (FileStream fs = new FileStream(filepath, FileMode.Open, System.IO.FileAccess.Read, FileShare.ReadWrite))
        {
            using (StreamReader sr = new StreamReader(fs, Encoding.GetEncoding("gb2312")))
            {
                json = sr.ReadToEnd().ToString();
            }
        }

        return json;
    }


    /// <summary>
    /// 删除数据
    /// </summary>
    /// <param name="req"></param>
    /// <param name="range"></param>
    /// <returns></returns>
    public async Task<bool> Destroy(DestroyDto req, DataRange_2b range)
    {
        var model = DataWatchRange(range);

        if (model is null)
            return false;
       
        var list = await model.Where(m => req.Ids.Contains(m.ID)).ToListAsync();

        foreach (var info in list)
        {
            _dbContext.Eanupc.Remove(info);
        }

        var state = await _dbContext.SaveChangesAsync();
        return state > 0;
    }

    public async Task<bool> Clear(ClearDto req, DataRange_2b rangee)
    {
        /*
        $where = $this->storeWhereDataRange(self::RANGE);
        if (!count($where)) {
            return returnArr(false, getlang('store.eanupc.MissRange'));
        }
        if ($mode == 3) {
            EanupcModel::query()->where($where)->delete();
        } else {
            EanupcModel::query()->where($where)->type($mode)->delete();
        }
        return true; 
        */

        var model = DataWatchRange(rangee);
        
        if(model is null)
            return false;
        
        if (req.Mode == ModeEnum.All)
        {
            await model.Where(m => m.OEMID == _oemId && m.CompanyID == _companyId).DeleteAsync();
        }else if (req.Mode == ModeEnum.Ean)
        {
            await model
                .Where(m => m.OEMID == _oemId && m.CompanyID == _companyId && m.Type == EanupcModel.TypeEnum.EAN)
                .DeleteAsync();
        }else if (req.Mode == ModeEnum.Upc)
        {
            await model
                .Where(m => m.OEMID == _oemId && m.CompanyID == _companyId && m.Type == EanupcModel.TypeEnum.UPC)
                .DeleteAsync();
        }

        return true;
    }


    //计算ean
    private string GetEanCode(string n /*$n = ''*/)
    {
        var a = (((n[1] + n[3] + n[5] + n[7] + n[9] + n[11]) * 3 + n[0] + n[2] + n[4] + n[6] + n[8] + n[10]) - 48 * 24) % 10;
        a = a == 0 ? 0 : 10 - a;
        return n + a;
    }

    /// <summary>
    /// 计算UPC
    /// </summary>
    /// <param name="strCode"></param>
    /// <returns></returns>
    private async Task<string> GetUpcCode(string strCode /*$strCode = ''*/) //71000155555+x
    {
        /*
        $oddValue = 0; //将所有奇数位置（第1、3、5、7、9和11位）上的数字相加。
        $evenValue = 0;//将所有偶数位置（第2、4、6、8和10位）上的数字相加。
        for ($i = 0; $i < strlen($strCode); $i++) {
            if ($i % 2 == 0) {
                $oddValue += $strCode[$i];
            } else {
                $evenValue += $strCode[$i];
            }
        }
        //然后，将oddValue数乘以3。
        $oddValue = $oddValue * 3;
        $a = ($oddValue + $evenValue) % 10;
        $checkValue = $a == 0 ? 0 : 10 - $a;
        return $strCode . $checkValue;
        */
        var oldValue = 0; //将所有奇数位置（第1、3、5、7、9和11位）上的数字相加。
        var evenValue = 0; //将所有偶数位置（第2、4、6、8和10位）上的数字相加。
        for (int i = 0; i < strCode.Length; i++)
        {
            if (i % 2 == 0)
            {
                oldValue += strCode[i] - 48;
            }
            else
            {
                evenValue += strCode[i] -48;
            }
        }
        //然后，将oddValue数乘以3。
        oldValue *= 3;
        var a = (oldValue + evenValue) % 10;
        var checkValue = a == 0 ? 0 : 10 - a;
        return await Task.FromResult(strCode + checkValue);
    }

    public async Task<(bool State, string Message)> Import(ImportDto req)
    {
        List<string> lstData = new List<string>();
        if (req.File.ContentType == "text/csv" || req.File.ContentType == "text/plain")
        {
            StreamReader sr = new StreamReader(req.File.OpenReadStream(), Encoding.UTF8); 
            string? strLine = null;
            while ((strLine = sr.ReadLine()) != null)
            {
                lstData.Add(strLine);
            }
        }
        else
        {
            return (false, "导入文件格式不正确");
        }

        if (lstData.Count <= 0)
        {
            return (false, "此文件中没有数据");
        }

        var models = new List<EanupcModel>();
        var date = DateTime.Now;
        foreach (var item in lstData)
        {
            models.Add(new EanupcModel()
            {
                Type = req.Type,
                Value = item,
                HashCode = item.GetHashCode(),
                UserID = _userId,
                GroupID = _groupId,
                CompanyID = _companyId,
                OEMID = _oemId,
                CreatedAt = date,
                UpdatedAt = date
            });
        }

        if (models.Count <= 0)
        {
            return (false, "文件内容有误");
        }

        try
        {
            await _dbContext.Eanupc.AddRangeAsync(models);
            await _dbContext.SaveChangesAsync();
            return (true, "导入成功");
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return (false, "导入失败,存在重复");
        }
       
        
    }

    public async Task<List<EanupcModel>> GetFixedQuantityCodes(EanupcModel.TypeEnum type,int num,DataRange_2b range)
    {
        var model = DataWatchRange(range);
        if (model is null)
            return new List<EanupcModel>();
        
        return  await model
            // .Where(m => m.CompanyID == _companyId).Skip(variantNum)
            .Where(m => m.Type == type).Take(num)
            .ToListAsync();
    }


}