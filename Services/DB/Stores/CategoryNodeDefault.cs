namespace ERP.Services.Stores;
using Data;
using Enums.View;
using ERP.Models.Stores;
using Extensions;
using Interface;
using Microsoft.EntityFrameworkCore;
using Models.DB.Stores;
using Models.View.Setting;
using Newtonsoft.Json;
using static Models.Stores.CategoryNodeDefault;
using Model = Models.Stores.CategoryNodeDefault;
using RBNodesModel = Models.Stores.RecommendedBrowseNodes;

public class CategoryNodeDefault : BaseService
{
    public readonly IHttpContextAccessor _httpContextAccessor;

    public readonly DBContext _dbContext;

    private readonly ISessionProvider _SessionProvider;
    public CategoryNodeDefault(IHttpContextAccessor httpContextAccessor, DBContext dbContext, ISessionProvider SessionProvider)
    {
        _httpContextAccessor = httpContextAccessor;
        _dbContext = dbContext;
        _SessionProvider= SessionProvider;
    }
    private ISession Session { get => _SessionProvider.Session!; }
    private int UserID { get => Session.GetUserID(); }
    private int GroupID { get => Session.GetGroupID(); }
    private int CompanyID { get => Session.GetCompanyID(); }
    private int OEMID { get => Session.GetOEMID(); }
    public static bool IsRange(EanupcModel.RangeEnum? range)
    {
        switch (range)
        {
            case EanupcModel.RangeEnum.Company: return true;
            case EanupcModel.RangeEnum.Group: return true;
            case EanupcModel.RangeEnum.User: return true;
            default:return false;
        }
    }
    public static bool IsLevel(Levels? Level)
    {
        switch (Level)
        {
            case Levels.WORKBENCH: return true;
            case Levels.GROUP: return true;
            case Levels.COMPANY: return true;
            default: return false;
        }
    }

    public async Task<ReturnStruct> GetList(Controllers.Store.CategoryNodeDefault.CategoryList indexDto)
    {
        #region MyRegion
        //$type_hash = (isset($data['type_data']) && checkArr($data['type_data'])) ? createHashCode(implode(',', $data['type_data'])) : '';
        //$name_hash = $data['temp_name'] ? createHashCode($data['temp_name']) : '';
        //return CategoryNodeDefaultModel::query()
        //    ->OemId($this->getSession()->getOemId())
        //    ->CompanyId($this->getSession()->getCompanyId())
        //    ->NameHash($name_hash)
        //    ->nation($data['nation_id'])
        //    ->typeHash($type_hash)
        //    ->browserNodeId($data['browser_node_id'])
        //    ->where(function($query) use($range) {
        //    if ($range) {
        //            //组内数据
        //            $query->where('group_id', '=', $this->getSession()->getGroupId());
        //    } else
        //    {
        //            //公司数据
        //            $query->where('level', '=', 1)
        //                ->orWhere(function($query) use($range) {
        //                    $query->where('group_id', '=', $this->getSession()->getGroupId())
        //                        ->where('level', '=', 2);
        //        });
        //    }
        //})
        //    ->LevelSort()
        //    ->Sort()
        //    ->paginate($this->limit);

        #endregion
        int group_id = GroupID;
        //var Range = indexDto.Concierge == Concierges.Child && IsRange(EanupcModel.RangeEnum.Group);

        /*IQueryable<Model>*/var Query =await _dbContext.categoryNodeDefaults
             .WhenWhere(!string.IsNullOrEmpty(indexDto.tempName), x => x.NameHash == Helpers.CreateHashCode(indexDto.tempName!))
             .WhenWhere(!string.IsNullOrEmpty(indexDto.typeHash), x => x.TypeHash == Helpers.CreateHashCode(indexDto.typeHash!))
             .WhenWhere(indexDto.browserNodeId>0,x => x.BrowserNodeId == indexDto.browserNodeId)
             .WhenWhere(indexDto.NationId>0,x => x.NationId == indexDto.NationId)
             .OrderBy(x => x.Level)
             .OrderBy(x => x.Sort).ToPaginateAsync();

        
        //if (Range)
        //{
        //    Query.WhenWhere(group_id >0, x => x.GroupID == group_id).ToPaginate();
        //}
        //else
        //{
        //  await  Query.WhenWhere(!Range,x => x.Level == Levels.COMPANY)
        //        .WhenWhere(Range,x => x.Level == Levels.GROUP)
        //        ;
        //}
        return ReturnArr(true, "", Query);
    }

    //获取用户可使用分类信息
    public async Task<ReturnStruct> GetAllList(string category)
    {
        var Query = await _dbContext.categoryNodeDefaults.WhenWhere(!string.IsNullOrEmpty(category), x => x.TypeHash == Helpers.CreateHashCode(category))
                   .WhenWhere(IsLevel(Levels.WORKBENCH), x => x.Level == Levels.WORKBENCH)
                   .WhenWhere(IsLevel(Levels.COMPANY), x => x.Level == Levels.COMPANY && x.CompanyID == CompanyID)
                   .WhenWhere(IsLevel(Levels.GROUP), x => x.Level == Levels.GROUP && x.GroupID == GroupID && x.CompanyID == CompanyID)
                   .OrderBy(x => x.Sort)
                   .ToListAsync();

        //if(Query.Where(x=>x.Level== Levels.WORKBENCH))
        //{

        //}

        //$type_hash = createHashCode(implode(',', $category));

        //return CategoryNodeDefaultModel::query()
        //    ->typeHash($type_hash)
        //    ->where(function($query) {
        //        $query->where('level', '=', 0)
        //            ->orWhere(function($query) {
        //                $query->where('company_id', '=', $this->getSession()->getCompanyId())
        //                    ->where('level', '=', 1);
        //    })
        //            ->orWhere(function($query) {
        //                $query->where('company_id', '=', $this->getSession()->getCompanyId())
        //                    ->where('group_id', '=', $this->getSession()->getGroupId())
        //                    ->where('level', '=', 2);
        //    });
        //})
        //    ->LevelSort()
        //    ->Sort()
        //    ->select(['temp_name', 'browser_node_id', 'browser_ui', 'level', 'nation_id', 'type_data'])
        //    ->get();
        return ReturnArr(true,"",Query);
    }

    //检测重复性
    public bool CheckExist(int Id)
    {
        var count=_dbContext.categoryNodeDefaults.WhenWhere(Id>0,x=>x.ID==Id).Count();
        return count <= 0 ? true : false;
    }

    // 新增
    public async Task<ReturnStruct> AddData(EditCate editCate)
    {
        DateTime Now = DateTime.Now;
        int company_id = CompanyID;
        Model model = new(Session)
        {
            LanguageId = 0,
            LanguageCode = 0/*string.Empty*/,
            Level= UserID == SuperId ? Levels.WORKBENCH:editCate.Level!.Value,
            TempName = editCate?.Tempname,
            NationId = editCate!.Nationid,
            BrowserNodeId = editCate.Browsernodeid,
            BrowserUi = editCate.Browserui,
            NameHash = Helpers.CreateHashCode(editCate.Tempname??string.Empty),
            Hash = Helpers.CreateHashCode(company_id + editCate.Hash + editCate.TypeData + editCate.Nationid + editCate.Browsernodeid),
            TypeHash = editCate.TypeData != null ? Helpers.CreateHashCode(editCate.TypeData) : 0,
            TypeData = JsonConvert.SerializeObject(editCate.TypeData),
            CreatedAt = Now,
            Sort=- Now.GetTimeStamp()
        };
        await _dbContext.categoryNodeDefaults.AddAsync(model);
        if (_dbContext.SaveChanges() > 0) return ReturnArr(true);

        return ReturnArr(false);
    }

    //编辑
    public async Task<ReturnStruct> EditData(EditCate editCate)
    {
        var Update =await _dbContext.categoryNodeDefaults.SingleOrDefaultAsync(x=>x.ID== editCate.ID);
        if(Update != null) { 
            Update.TempName = editCate.Tempname;
            Update.NationId = editCate.Nationid;
            Update.BrowserNodeId = editCate.Browsernodeid;
            Update.BrowserUi = editCate.Browserui;
            Update.NameHash = !string.IsNullOrEmpty(editCate.Tempname)? Helpers.CreateHashCode(editCate.Tempname!):0;
            Update.Hash = Helpers.CreateHashCode(GroupID + editCate.Hash+editCate.TypeData+ editCate.Nationid+ editCate.Browsernodeid);
            Update.TypeHash = editCate.TypeData!=null? Helpers.CreateHashCode(editCate.TypeData):0;
            Update.TypeData = JsonConvert.SerializeObject(editCate.TypeData);
            Update.UpdatedAt = DateTime.Now;
            _dbContext.categoryNodeDefaults.Update(Update);
            _dbContext.SaveChanges();
            return ReturnArr(true);
        }
        return ReturnArr(false);
    }

    //查找信息
    public async Task<Model?> FindInfo(int id)
    {
        //$field = ['id', 'temp_name', 'nation_id', 'level', 'browser_node_id', 'browser_ui', 'type_data'];
        //$info = CategoryNodeDefaultModel::query()->select($field)->find($id);
        //if ($info === null) {
        //    return null;
        //}
        //$info['type_data'] = json_decode($info['type_data'], true);
        //return $info;
        return await _dbContext.categoryNodeDefaults.FirstOrDefaultAsync(x => x.ID == id);
    }

    public async Task<bool> Del(int id)
    {
        var SingData =await _dbContext.categoryNodeDefaults.SingleOrDefaultAsync(x=>x.ID==id);
        if(SingData != null) _dbContext.categoryNodeDefaults.Remove(SingData);
        return _dbContext.SaveChanges()>0;
    }

    public async Task<ReturnStruct> FindName(string data)
    {
        //var SingData = _dbContext.categoryNodeDefaults.SingleOrDefault(x => x. == id);
        //return RecommendedBrowseNodesModel::query()
        //    ->NodeId($data)
        //    ->first();
        return await Task.FromResult(ReturnArr());
    }
    public async Task<RBNodesModel?> FindName(int id)
    {
        return await _dbContext.RecommendedBrowseNode.FirstOrDefaultAsync(x => x.ID == id);
    }

    //public function scopeGroupId(Builder $query, $group_id)
    //{
    //    if ($group_id) {
    //        return $query->where('group_id', '=', $group_id);
    //    }
    //}
    //public function scopeCompanyId(Builder $query, $company_id)
    //{
    //    if ($company_id) {
    //        return $query->where('company_id', '=', $company_id);
    //    }
    //}
    //public function scopeOemId(Builder $query, $oem_id)
    //{
    //    if ($oem_id) {
    //        return $query->where('oem_id', '=', $oem_id);
    //    }
    //}


    //public function scopeTypeHash(Builder $query, $type_hash)
    //{
    //    if ($type_hash) {
    //        return $query->where('type_hash', '=', $type_hash);
    //    }

    //}
    //public function scopeHash(Builder $query, $hash)
    //{
    //    if ($hash) {
    //        return $query->where('hash', '=', $hash);
    //    }

    //}
    //public function scopeNameHash(Builder $query, $name_hash)
    //{
    //    if ($name_hash) {
    //        return $query->where('name_hash', '=', $name_hash);
    //    }

    //}

    //public function scopeTypeData(Builder $query, $type_data)
    //{
    //    if ($type_data) {
    //        return $query->where('type_data', '=', $type_data);
    //    }

    //}

    //public function scopeNation(Builder $query, $nation_id)
    //{
    //    if ($nation_id) {
    //        return $query->where('nation_id', '=', $nation_id);
    //    }

    //}

    //public function scopeLanguageCode(Builder $query, $language_code)
    //{
    //    if ($language_code) {
    //        return $query->where('language_code', '=', $language_code);
    //    }

    //}


    //public function scopeBrowserNodeId(Builder $query, $browser_node_id)
    //{
    //    if ($browser_node_id) {
    //        return $query->where('browser_node_id', '=', $browser_node_id);
    //    }

    //}

    //public function scopeTempName(Builder $query, $temp_name)
    //{
    //    if ($temp_name) {
    //        return $query->where('temp_name', '=', $temp_name);
    //    }

    //}
    //public function scopeSort(Builder $query)
    //{
    //    return $query->orderBy('sort', 'asc');
    //}

    //public function scopeLevelSort(Builder $query)
    //{
    //    return $query->orderBy('level', 'desc');
    //}
}

