using ERP.Models.DB.Stores;
using ERP.Extensions;
using ERP.Services.FileStorage;
using ERP.ViewModels.Store.Remote;
using Microsoft.EntityFrameworkCore;
using static ERP.Extensions.DbSetExtension;
using ERP.Interface;

namespace ERP.Services.Stores;

using Data;
using ERP.Models.Stores;

public class RemoteService : BaseService
{
    private readonly DBContext _dbContext;
    private readonly ISessionProvider _SessionProvider;

    public RemoteService(DBContext dbContextm, ISessionProvider sessionProvider)
    {
        _dbContext = dbContextm;
        _SessionProvider = sessionProvider;
    }
    private ISession Session { get => _SessionProvider.Session!; }
    private int UserID { get => Session.GetUserID(); }
    private int GroupID { get => Session.GetGroupID(); }
    private int CompanyID { get => Session.GetCompanyID(); }
    private int OEMID { get => Session.GetOEMID(); }

    /// <summary>
    /// 远程管理列表
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    public async Task<PaginateStruct<RemoteModel>> GetList(RemoteListDto req)
    {
        return await _dbContext.Remote
            .WhenWhere(!string.IsNullOrEmpty(req.Name), a => a.Name.Contains(req.Name!))
            .Where(a => a.UserID == UserID)
            .OrderBy(x=>x.Sort)
            .ToPaginateAsync();
    }

    /// <summary>
    /// 修改/添加 远程
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> EditOrAddRemotes([FromQuery]EditRemoteDto req)
    {
        //todo 检查权限
        // if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->sessionData->getRule()->getStore()->getBiter()->hasRemoteAdd()) {
        // return error(getlang('public.authfailed'));
        // }
    
        //检查是否重复
        if (req.ID > 0)
        {
            //修改
            var info = await _dbContext.Remote.Where(m => m.ID == req.ID).FirstOrDefaultAsync();
            if (info == null)
            {
                return ReturnArr(false, "数据不存在");
            }
            info.UserID = UserID;
            info.Name = req.Name;
            info.Address = req.Address;
            info.Password = req.Password;
            info.User = req.User;
            _dbContext.Remote.Update(info);
        }
        else
        {
            //新增
            var isExits = await CheckIsset(req.Address, UserID);
            if (isExits)
            {
                return ReturnArr(false, $"该地址:{req.Address}已存在");
            }

            var insertData = new RemoteModel
            {
                Name = req.Name,
                Address = req.Address,
                Password = req.Password,
                User = req.User,
                UserID = UserID,
                GroupID = GroupID,
                OEMID = OEMID,
                CompanyID = CompanyID
            };

            await _dbContext.Remote.AddAsync(insertData);
        }

        await _dbContext.SaveChangesAsync();
        return ReturnArr();
    }

    public async Task<ReturnStruct> EditRemotes(int id, Models.Stores.RemoteModel data)
    {
        var res = _dbContext.Remote.Update(data);
        var count = await _dbContext.SaveChangesAsync();
        return ReturnArr(true, "");
    }

    public async Task<ReturnStruct> AddRemote(Models.Stores.RemoteModel data)
    {
        var res = _dbContext.Remote.Add(data);
        var count = await _dbContext.SaveChangesAsync();
        return ReturnArr(true, "");
    }

    /// <summary>
    /// 检测数据是否存在
    /// </summary>
    /// <param name="address"></param>
    /// <param name="userId"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    private async Task<bool> CheckIsset(string address, int userId = 0/*, int id = 0*/)
    {
        // $isset = RemoteModels::query()->Address($address)->UserId($user_id)->first();
        // if ($isset)
        // {
        //     return true; //.WhenWhere(id > 0, m => m.ID != id)
        // }
        // return false;
        var info = await _dbContext.Remote
            .Where(m => m.Address == address)
            .Where( m => m.UserID == userId)           
            .FirstOrDefaultAsync();
        return info is not null;
    }


    /// <summary>
    /// 删除远程
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> Del(int id)
    {
        var remote = await _dbContext.Remote.FirstOrDefaultAsync(a => a.ID == id);
        if (remote == null) return ReturnArr(false, "没有数据");

         _dbContext.Remove(remote);
         await _dbContext.SaveChangesAsync();
        return ReturnArr(true);
    }

    /// <summary>
    /// 查询远程详情
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<RemoteModel?> FindInfo(int id)
    {
        return await _dbContext.Remote.FirstOrDefaultAsync(a => a.ID == id);
    }
}