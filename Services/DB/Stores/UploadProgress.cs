﻿namespace ERP.Services.DB.Store
{
    public class UploadProgress:BaseService
    {


        public async Task<object> GetPageList()
        {
          return await Task.FromResult(1);
        }
    }
}
