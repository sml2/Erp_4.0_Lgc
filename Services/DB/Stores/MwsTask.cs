using Model = ERP.Models.Stores.MwsTask;


namespace ERP.Services.DB.Store;

public class MwsTask : BaseService
{
    public async Task<ReturnStruct> Info(/*$field=[], $id= '', $sid= '', $tasks_id= '', $state= ''*/)
    {
        /*
        empty($field) && $field = ["*"];
        return MwsTaskModel::query()
            ->select($field)
            ->id($id)
            ->sid($sid)
            ->tasksId($tasks_id);

        */
        return await Task.FromResult(ReturnArr(true));
    }

    public async Task<Model> GetPageList()
    {
        return await Task.FromResult(new Model());
    }
    public async Task<ReturnStruct> Get(string result)
    {
        /*
        return $result->get();
        */
        return await Task.FromResult(ReturnArr(true));
    }
    public async Task<ReturnStruct> first(string result)
    {
        /*
        return $result->first();

        */
        return await Task.FromResult(ReturnArr(true));
    }
    public async Task<ReturnStruct> update(/*$id, $updateData*/)
    {
        /*
        return MwsTaskModel::query()->where('id', $id)->update($updateData);
        */
        return await Task.FromResult(ReturnArr(true));
    }
    public async Task<ReturnStruct> updateByTasksId(/*$tasks_id, $updateData*/)
    {
        /*
        return MwsTaskModel::query()->where('tasks_id', $tasks_id)->update($updateData);
        */
        return await Task.FromResult(ReturnArr(true));
    }

    //克隆产品
    public async Task<ReturnStruct> cloneProduct(/*int $id, $language, $source= 1*/)
    {
        /*
        if (empty($language)) {
            return returnArr(getlang('store.mwstask.NotSelectLpk'));
        }
        
        $groupId = 0;
        if ($this->getSession()->getIsDistribution() === Company::DISTRIBUTION_TRUE && $source == 2) {
            $companyId = $this->getSession()->getReportId();
            $company   = $this->getCompany($companyId);
            $oemId     = $company['oem_id'];
        } else {
            $companyId = $this->getSession()->getCompanyId();
            $oemId     = $this->getSession()->getOemId();
            $groupId   = $this->getSession()->getGroupId();
        }

        $where       = [
            [
                'company_id',
                $companyId,
            ],
            [
                'id',
                $id,
            ],
            [
                'oem_id',
                $oemId
            ]
        ];
        $productInfo = Product::query()->where($where)->first();
        if (!$productInfo) {
            return returnArr(false, getlang('store.mwstask.ProductNotExist'));
        }

        // 判断产品的源产品id.
        $sourceId = 0;
        if ($this->getSession()->getIsDistribution() === Company::DISTRIBUTION_TRUE && $source == 2) {
            $ext = $productInfo->ext;
            $ext = arrayCombine($ext, 'key');
            if (isset($ext['RawProductId'])) {
                $sourceProduct = Product::query()->where('company_id', $companyId)
                    ->where('id', $ext['RawProductId']['value'])
                    ->first();
                if ($sourceProduct) {
                    $sourceId = $sourceProduct->id;
                }
            }
        }

        $lp          = [];
        $attribute   = $productInfo->attribute;
        $successInfo = '';
        $typeBiter = new Type(Type::FLAG_BASE);
        $languageBiterValue = Language::FLAG_BASE;
        if ($language === 'default') {
            $lp = [
                'n' => $productInfo->lp_n,
                's' => $productInfo->lp_s,
                'k' => $productInfo->lp_k,
                'd' => $productInfo->lp_d,
            ];
        } else {

            $languagePack = $productInfo->languages;
            if (!isset($languagePack[$language])) {
                return returnArr(false, getlang('store.mwstask.LpkNotExist'));
            }

            $currentLanguagePack = $languagePack[$language];
            $lp = [
                'n' => $currentLanguagePack['n'],
                's' => $currentLanguagePack['s'],
                'k' => $currentLanguagePack['k'],
                'd' => $currentLanguagePack['d'],
            ];
            if (!isset($languagePack[$language]['map'])) {
                $successInfo = getlang('store.mwstask.WarningText');
            } else {
                $languageAttribute = $languagePack[$language]['map'];
                $notFoundAttribute = [];
                foreach ($attribute as $k => &$v) {
                    if (!isset($languageAttribute[$v['name']])) {
                        $notFoundAttribute[] = $v['name'];
                    }

                    $v['name'] = ($languageAttribute[$v['name']] ?? $v['name']);
                    foreach ($v['value'] as &$val) {
                        if (!isset($languageAttribute[$val])) {
                            $notFoundAttribute[] = $val;
                        } else {
                            $val = ($languageAttribute[$val] ?? $val);
                        }
                    }
                }

                if (!empty($notFoundAttribute)) {
                    $successInfo = getlang('store.mwstask.Warning').implode(',', $notFoundAttribute).getlang('store.mwstask.TextNotFound');
                }
            }//end if

            $languageBiter = new Language(Language::FLAG_BASE);
            $languageCode = $languageBiter::LanguageCode;

            $language = strtolower($language);

            if (!in_array($language, $languageCode, true)) {
                return returnArr(false, getlang('store.mwstask.IllegleLangCode') . $language);
            }

            $biter = array_search($language, $languageCode, true);

            $languageBiter->setUILanguage($biter + 1);
            $languageBiterValue = $languageBiter->getFlags();
        }//end if
        if (!count($lp)) {
            return returnArr(false, getlang('store.mwstask.ProductInfoLess'));
        }

        $date      = date('Y-m-d H:i:s', request()->input('time'));
        $typeBiter->setUpload(true);
        $insertProductData = [
            'oem_id'      => $this->getSession()->getOemId(),
            'company_id'  => $this->getSession()->getCompanyId(),
            'user_id'     => $this->getSession()->getUserId(),
            'group_id'    => $this->getSession()->getGroupId(),
            'lp_n'        => $lp['n'],
            'lp_s'        => json_encode($lp['s']),
            'lp_k'        => $lp['k'],
            'lp_d'        => $lp['d'],
            'source'      => '',
            'hash_code'   => createHashCode($lp['n']),
            'languages'   => json_encode([]),
            'language'    => $languageBiterValue,
            'type'        => $typeBiter->getFlags(),
            'price'       => $productInfo->price_original,
            'platform_id' => $productInfo->platform_id,
            'platform'    => $productInfo->platform,
            'attribute'   => json_encode($attribute),
            'variants'    => $productInfo->variants_original,
            'ext'         => json_encode($productInfo->ext),
            'images'      => $productInfo->getOriginal('images_original'),
            'image'       => $productInfo->getOriginal('image_original'),
            'size'        => $productInfo->size,
            'pid'         => $productInfo->id,
            'created_at'  => $date,
            'updated_at'  => $date,
        ];
        $imagesIds = array_keys(arrayCombine($productInfo->images, 'id'));
        $pid       = Product::query()->insertGetId($insertProductData);

        if (count($imagesIds)) {
            // FIXME 资源计数+1.
            Resource::query()->whereIn('id', $imagesIds)->increment('reuse', 1);
        }

        request()->merge(['disk' => $productInfo->size]);
        return returnArr(true, $successInfo, ['pid' => $pid, 'source_id' => $sourceId, 'name' => $lp['n']]);
        */
        return await Task.FromResult(ReturnArr());
    }



    //    public function tasks()
    //    {
    //        return $this->belongsTo(MwsTasks::class);
    //    }

    //public function scopeId(Builder $query, $id)
    //{
    //    //        if ($id) {
    //    //            return $query->where('id', $id);
    //    //        }
    //    if ($id) {
    //        if (!is_array($id))
    //        {
    //            return $query->where('id', $id);
    //        }
    //        else
    //        {
    //            return $query->whereIn('id', $id);
    //        }
    //    }


    //}//end scopeId()


    //public function scopeSId(Builder $query, $sid)
    //{
    //    if ($sid) {
    //        if (!is_array($sid))
    //        {
    //            return $query->where('sid', $sid);
    //        }
    //        else
    //        {
    //            return $query->whereIn('sid', $sid);
    //        }
    //    }

    //}//end scopeSId()


    //public function scopeTasksId(Builder $query, $tasks_id)
    //{
    //    if ($tasks_id === null || $tasks_id) {
    //        return $query->where('tasks_id', $tasks_id);
    //    }

    //}//end scopeTasksId()

    //public function scopeState(Builder $query, $state)
    //{
    //    return $query->where('state', $state);
    //}

    //public function scopeUiState(Builder $query, $state)
    //{

    //    if ($state !== null) {
    //        switch ($state) {
    //                case self::UI_WAIT_UPLOAD:
    //                    return $query->where('state', self::STATE_WAITING)->where('tasks_id', null)->where('asin', null);
    //                case self::UI_UPLOADING:
    //                    return $query->where('state', self::STATE_EXECUTING)->where('tasks_id', '<>', null)->where('asin', null);
    //                case self::UI_WAIT_UPDATE:
    //                    return $query->where('state', self::STATE_WAITING)->where('tasks_id', null)->where('asin', '<>', null);
    //                case self::UI_UPDATING:
    //                    return $query->where('state', self::STATE_EXECUTING)->where('tasks_id', '<>', null)->where('asin', '<>', null);
    //                case self::UI_COMPLETE:
    //                    return $query->where('state', self::STATE_SUCCESS)->where('tasks_id', '<>', null);
    //                case self::UI_FAILED_UPLOAD:
    //                    return $query->where('state', self::STATE_ERROR)->where('tasks_id', '<>', null)->where('asin', null);
    //                case self::UI_FAILED_UPDATE:
    //                    return $query->where('state', self::STATE_ERROR)->where('tasks_id', '<>', null)->where('asin', '<>', null);
    //        }
    //    }

    //}//end scopeState()


    //public function scopeProductName(Builder $query, $name)
    //{
    //    if ($name) {
    //        return $query->where('product_name', 'like', '%'.$name.'%');
    //    }

    //    return $query;

    //}//end scopeProductName()

    //public function scopeProductLanguage(Builder $query, $value)
    //{
    //    if ($value) {
    //        return $query->where('product_language', $value);
    //    }

    //    return $query;

    //}//end scopeProductLanguage()

    //public static function exchangeState($state, $task, $asin)
    //{
    //    if ($state === self::STATE_WAITING) {
    //        if ($asin === null) {
    //            return self::UI_WAIT_UPLOAD;
    //        } else
    //        {
    //            return self::UI_WAIT_UPDATE;
    //        }
    //    }
    //    if ($state === self::STATE_EXECUTING) {
    //        if ($asin === null) {
    //            return self::UI_UPLOADING;
    //        } else
    //        {
    //            return self::UI_UPDATING;
    //        }
    //    }
    //    if ($state === self::STATE_SUCCESS) {
    //        return self::UI_COMPLETE;
    //    }
    //    if ($state === self::STATE_ERROR) {
    //        if ($asin === null) {
    //            return self::UI_FAILED_UPLOAD;
    //        } else
    //        {
    //            return self::UI_FAILED_UPDATE;
    //        }
    //    }
    //}
}

