using ERP.Enums;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.Abstract;
using ERP.Models.DB.Users;
using ERP.Models.Statistics;
using ERP.Models.View.Store;
using ERP.Services.DB.Statistics;
using ERP.Services.DB.Users;
using ERP.Services.Statistics;
using ERP.ViewModels.Store.Remote;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Linq.Expressions;
using ERP.DomainEvents;
using ERP.ViewModels.Statistics;
using MediatR;
using ActionEnum = ERP.ViewModels.Store.Remote.ActionEnum;
using Role = ERP.Enums.Identity.Role;

namespace ERP.Services.Stores;

using Data;
using ERP.Exceptions.Stores;
using ERP.Models.DB;
using ERP.ViewModels;
using System.Text;
using static ERP.Models.Stores.StoreRegion;
using static Models.PlatformData;
using AmazonKeyCache = Caches.AmazonKey;
using Model = Models.Stores.StoreRegion;
using ModelMarket = Models.Stores.StoreMarket;
using ModelAsin = Models.Stores.AsinProduct;
using PlatformDataCache = Caches.PlatformData;
using StoreCache = Caches.Store;
using StoreMarketCache = Caches.StoreMarket;
using UnitCache = Caches.Unit;
using Microsoft.Extensions.DependencyInjection;
using DBStatistic = ERP.Services.DB.Statistics.Statistic;
using System.Linq;
using ERP.Models.Setting;
using ERP.Services.Host;

using amazonTokenResponse = OrderSDK.Modles.Amazon.Authorization.TokenResponse;
using OrderSDK.Modles.Amazon.Authorization;
using OrderSDK.Modles.Shopee.Auth;
using ERP.Services.Api;
using static ERP.Models.Abstract.BaseModel;
using ERP.Enums.Orders;

public class StoreService : BaseService
{
    private readonly DBContext _dbContext;
    private readonly CompanyService _companyService;
    private readonly StoreGroupService _storeGroupService;
    private readonly UnitCache _cnitCache;
    private readonly PlatformDataCache _platformDataCache;
    private readonly ISessionProvider _sessionProvider;
    private readonly UserService _userService;

    private readonly StoreCache _storeCache;
    private readonly StoreMarketCache storeMarketCache;

    private readonly IMediator _mediator;

    //private readonly StoreRegionCache storeRegionCache;
    //private readonly CounterUserBusinessService _CounterUserBusinessService;
    //private readonly CounterGroupBusinessService _CounterGroupBusinessService;
    //private readonly CounterCompanyBusinessService _CounterCompanyBusinessService;
    private readonly CounterUserBusinessService _CounterUserBusinessService;
    private readonly CounterGroupBusinessService _CounterGroupBusinessService;
    private readonly CounterCompanyBusinessService _CounterCompanyBusinessService;

    private readonly DBStatistic _statisticService;
    private readonly StatisticMoney _statisticMoney;

    private readonly IOrderFactory _NewApiOrderFactory;

    private readonly IServiceProvider _ServiceProvider;

    private ISession Session
    {
        get => _sessionProvider.Session!;
    }

    private int UserID
    {
        get => Session.GetUserID();
    }

    private int GroupID
    {
        get => Session.GetGroupID();
    }

    private int CompanyID
    {
        get => Session.GetCompanyID();
    }

    private int OEMID
    {
        get => Session.GetOEMID();
    }

    readonly ILogger logger;

    public StoreService(DBContext myDbContext, CompanyService companyService,
        CounterUserBusinessService counterUserBusiness,
        StoreGroupService storeGroupService, CounterGroupBusinessService counterGroupBusinessService,
        CounterCompanyBusinessService counterCompanyBusinessService, UnitCache cnitCache,
        PlatformDataCache platformDataCache,
        StoreCache storeCache, StoreMarketCache _storeMarketCache,
        ISessionProvider sessionProvider, DBStatistic statisticService,
        UserService userService, UnitCache unitCache, IOrderFactory orderFactory, IServiceProvider serviceProvider
        , ILogger<StoreService> _logger, IMediator mediator)
    {
        _CounterCompanyBusinessService = counterCompanyBusinessService;
        _CounterGroupBusinessService = counterGroupBusinessService;
        _CounterUserBusinessService = counterUserBusiness;
        _dbContext = myDbContext;
        _companyService = companyService;
        _storeGroupService = storeGroupService;
        _cnitCache = cnitCache;
        _platformDataCache = platformDataCache;
        _sessionProvider = sessionProvider;
        _statisticService = statisticService;
        _userService = userService;
        _storeCache = storeCache;
        _NewApiOrderFactory = orderFactory;
        _ServiceProvider = serviceProvider.CreateScope().ServiceProvider;
        logger = _logger;
        _mediator = mediator;
        _statisticMoney = new StatisticMoney(_statisticService);
        storeMarketCache = _storeMarketCache;
    }

    public async Task<bool> Update(Model model)
    {
        _dbContext.StoreRegion.Update(model);
        return _dbContext.SaveChanges() > 0 ? true : false;
    }


    public async Task<Model?> GetInfoById(int? id)
    {
        //待优化
        var model = await _dbContext.StoreRegion.Where(m => m.ID == id).FirstOrDefaultAsync();
        if (model.IsNotNull())
        {
            var info = await GetAllMarketInfoByHash(model.UniqueHashCode);
            if (info.IsNotNull())
            {
                model.PlatFormIds = info.Select(x => x.platFormId).ToList();
                model.NationIds = info.Select(x => x.NationId).ToList();
            }

            return model;
        }

        return null;
    }

    public async Task<FileBucket?> GetFileBultById(int? id)
    {
        //待优化
        var model = await _dbContext.StoreRegion.Include(x => x.OEM).Include(x => x.OEM.Oss).Where(m => m.ID == id)
            .FirstOrDefaultAsync();
        if (model.IsNotNull())
        {
            return model.OEM!.Oss;
        }

        return null;
    }


    public async Task<object> GetMarketByHash(int id)
    {
        var unit = _cnitCache.List();
        var store = await GetStoreByID(id);
        if (store != null)
        {
            var lst = await GetAllMarketInfoByHash(store.UniqueHashCode);
            return new
            {
                info = lst.Select(x => new
                {
                    store.GetSPAmazon()!.SellerID,
                    x.UniqueHashCode,
                    x.platFormId,
                    x.NationId,
                    x.NationShort,
                    x.NationName,
                    x.State,
                    x.UnitID,
                    x.UnitName,
                    x.UnitSign,
                    x.Marketplace
                }).Distinct(x => x.NationShort).ToList(),
                unit = unit
            };
        }

        return null;
    }


    public async Task<bool> UpdateAmazonMarketInfo(marketDto req)
    {
        var model = _dbContext.StoreMarket.FirstOrDefault(x => x.ID == req.MID);
        if (model.IsNotNull())
        {
            var u = _cnitCache.Get(req.unitID);
            model.UnitID = u.ID;
            model.UnitSign = u.Sign;
            model.UnitName = u.Name;
            model.State = req.state;
            _dbContext.StoreMarket.Update(model);
            _dbContext.SaveChanges();
            storeMarketCache.Add(model);
            return true;
        }

        return false;
    }

    public async Task<List<ModelMarket>?> GetAllMarketInfoByHash(ulong id)
    {
        return await _dbContext.StoreMarket.Where(x => x.UniqueHashCode == id).ToListAsync();
    }

    //public async Task<List<ModelMarket>?> GetAllMarketInfoByHash(ulong id, int userid)
    //{
    //    return await _dbContext.StoreMarket.Where(x => x.UniqueHashCode == id && x.UserID == userid).ToListAsync();
    //}

    //public async Task<List<ModelMarket>?> GetMarketInfoByHash(ulong id, int userid)
    //{
    //    return await _dbContext.StoreMarket.Where(x => x.UniqueHashCode == id && x.UserID == userid).ToListAsync();
    //}

    //public async Task<List<ModelMarket>?> GetMarketInfoByHashNoTrack(ulong id, int userid)
    //{
    //    return await _dbContext.StoreMarket.Where(x => x.UniqueHashCode == id && x.UserID == userid).AsNoTracking()
    //        .ToListAsync();
    //}

    public async Task<Model?> GetStoreByID(int id)
    {
        return await _dbContext.StoreRegion.FirstOrDefaultAsync(x => x.ID == id);
    }


    /// <summary>
    /// 从store表中获取需要拉取的有效店铺
    /// </summary>
    /// <returns></returns>
    internal async Task<List<Model>> GetNeedPull(Expression<Func<Model, bool>>? expression)
    {
        return await _dbContext.StoreRegion
            .Where(m => m.State == Models.Abstract.BaseModel.StateEnum.Open && m.IsDel == false &&
                        m.IsVerify == Model.IsVerifyEnum.Certified).WhenWhere(expression.IsNotNull(), expression)
            .ToListAsync();
    }

    internal async Task<List<Model>> GetNeedPullOrder(List<int> ids)
    {
        return await _dbContext.StoreRegion
            .Where(m => m.State == Models.Abstract.BaseModel.StateEnum.Open && m.IsDel == false &&
                        m.IsVerify == Model.IsVerifyEnum.Certified && ids.Contains(m.ID))
            .ToListAsync();
    }

    /// <summary>
    /// 从order表中拉取，有效店铺信息
    /// </summary>
    /// <param name="expression"></param>
    /// <returns></returns>
    public async Task<List<Model>?> GetNeedOrderItemPull(Enums.Orders.PullOrderState orderState,
        Expression<Func<Model, bool>> expression)
    {
        var ids = await _dbContext.Order
            .Where(x => x.pullOrderState == orderState).Select(x => x.StoreId).Distinct().ToListAsync();

        if (ids.IsNotNull() && ids.Count > 0)
        {
            return await _dbContext.StoreRegion
                .Where(m => m.State == Models.Abstract.BaseModel.StateEnum.Open && m.IsDel == false &&
                            m.IsVerify == Model.IsVerifyEnum.Certified)
                .WhenWhere(ids.IsNotNull() && ids.Count > 0, x => ids.Contains(x.ID))
                .WhenWhere(expression.IsNotNull(), expression)
                .ToListAsync();
        }

        return null;
    }


    /// <summary>
    /// 根据主sku拉取完整产品信息
    /// </summary>
    /// <param name="storeID"></param>
    /// <param name="number"></param>
    /// <returns></returns>
    internal async Task<List<ModelAsin>> GetAsinNeedPull(int storeID, int number = 200)
    {
        return await _dbContext.AsinProduct.Where(x => x.StoreId == storeID && x.IsMainSku == true)
            .OrderBy(x => x.SyncTime).Distinct().Take(number).ToListAsync();
    }


    /// <summary>
    /// 获取公司店铺相关数量
    /// </summary>
    /// <returns></returns>
    public async Task<object> GetCompanyStoreNum()
    {
        var info = await _companyService.GetLimitInfoById(CompanyID);
        if (info is null)
        {
            return new();
        }

        var n = CountStore(CompanyID);

        return new
        {
            is_open_store = info.IsOpenStore,
            open_store_num = info.OpenStoreNum - n
        };
    }

    public object StoreRule()
    {
        object rule = new object();
        if (Session!.Is(Role.ADMIN))
        {
            rule = new
            {
                storeAdd = true,
                storeEdit = true,
                storeDelete = true,
                storeGroup = true
            };
        }
        else
        {
            rule = new
            {
                storeAdd = true, //=> $biter->hasStoreAdd(),
                storeEdit = true, //=> $biter->hasStoreEdit(),
                storeDelete = true, //=> $biter->hasStoreDelete(),
                storeGroup = true, //=> $biter->hasStoreGroup(),
            };
        }

        return rule;
    }


    /// <summary>
    /// 我的店铺权限
    /// </summary>
    /// <returns></returns>
    public object MyStoreRule()
    {
        object rule = new { };
        if (Session.Is(Role.Employee))
        {
            rule = new
            {
                myStoreAdd = true, //$biter->hasMyStoreAdd(),
                myStoreEdit = true, //$biter->hasMyStoreEdit(),
                myStoreDelete = true, //$biter->hasMyStoreDelete(),
                myStoreGroup = true, //$biter->hasStoreGroup(),
            };
        }
        else
        {
            rule = new
            {
                myStoreAdd = true,
                myStoreEdit = true,
                myStoreDelete = true,
                myStoreGroup = true,
            };
        }

        return rule;
    }


    /// <summary>
    /// 修改亚马逊店铺（单国家、多国家）
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> EditAmazonStore(MwsStoreDto req)
    {
        //todo 检查权限
        // if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->sessionData->getRule()->getStore()->getBiter()->hasStoreEdit()) {
        //     return returnArr(false, getlang('public.authfailed'));
        // }

        var transaction = _dbContext.Database.BeginTransaction();

        if (req.ID <= 0 || req.ID.IsNull())
        {
            return ReturnArr(false);
        }

        var info = await GetInfoById(req.ID);
        if (info == null)
        {
            return ReturnArr(false, "'店铺不存在'");
        }

        if (info.PlatFormIds == null)
        {
            return ReturnArr(false, "'店铺下没有国家'");
        }

        //判断修改的store，是否与数据库中的重复
        ulong UniqueHashCode = Helpers.CreateHashCode(req.Data.SellerID);
        var isExists = await checkRepeatForUpdate(UniqueHashCode, CompanyID, req.ID!);
        if (!isExists)
        {
            return ReturnArr(false, $"修改失败，{req.Data.SellerID}店铺已经存在");
        }
        
        // 根据名称判断店铺是否重复
        if (!await checkRepeatForStoreName(req.Name, CompanyID, req.ID!))
        {
            return ReturnArr(false, $"修改失败，{req.Name}店铺已经存在");
        }

        var pl = _platformDataCache.Get(req.NationIds[0]);
        if (pl.IsNull())
        {
            return ReturnArr(false, "平台缓存传递查询为空!");
        }

        try
        {
            //修改storeRegion 信息  
            var oldHash = info.UniqueHashCode;
            info.Name = req.Name;
            info.UniqueHashCode = UniqueHashCode;
            info.HashCode = Helpers.CreateHashCode(req.Data.SellerID);
            var pd = pl!.GetAmazonData()!;
            info.Continent = pd.Continent;

            Model.SPAmazonData md = info.GetSPAmazon()!;
            //修改国家无需重新验证，修改了SellerID才需要重新验证
            // var oldNations = info.PlatFormIds;
            // var newNations = req.NationIds;
            // oldNations.Sort();
            // newNations.Sort();
            // if (!oldNations.SequenceEqual(newNations))
            // {
            //     info.IsVerify = Model.IsVerifyEnum.Uncertified;
            // }

            if (md.SetData(req.Data.SellerID, req.NationIds[0], pd.KeyId))
                info.IsVerify = Model.IsVerifyEnum.Uncertified;
            info.Data = JsonConvert.SerializeObject(md);
            info.Platform = Platforms.AMAZONSP;
            info.AddMode = req.addMode;
            info.UpdatedAt = DateTime.Now;
            info.State = req.State;
            _dbContext.StoreRegion.Update(info);
            await InsertStoreMarket(req.NationIds, oldHash, UniqueHashCode);
            await _dbContext.SaveChangesAsync();
            transaction.Commit();
            _storeCache.Add(info);
            var lstMarkets = GetMarkets(UniqueHashCode);
            storeMarketCache.AddList(oldHash, lstMarkets);
            return ReturnArr(true);
        }
        catch (Exception e)
        {
            transaction.Rollback();
            return ReturnArr(false, e.Message);
        }
    }


    public async Task<ReturnStruct> AddAmazonStore(MwsStoreDto req)
    {
        //if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->sessionData->getRule()->getStore()->getBiter()->hasStoreAdd()) {
        //    return error(getlang('public.authfailed'));
        //}
        DateTime Now = DateTime.Now;
        var companyInfo = await _companyService.GetInfoById(CompanyID);
        if (companyInfo == null)
        {
            return ReturnArr(false, "没有找到公司");
        }

        //检测剩余可开通店铺数
        var n = await CheckStoreLimit();
        if (!n)
        {
            return ReturnArr(false, $"可开通店铺数为0，已超出限制！");
        }

        ulong UniqueHashCode = Helpers.CreateHashCode(req.Data.SellerID);
        string msg = "";

        var isExists = await checkRepeat(UniqueHashCode, CompanyID, req.ID);
        if (!isExists)
        {
            msg = $"修改失败，{req.Data.SellerID} 的店铺已经存在";
            return ReturnArr(false, msg);
        }
        
        // 根据名称判断店铺是否重复
        if (!await checkRepeatForStoreName(req.Name, CompanyID, null))
        {
            return ReturnArr(false, $"修改失败，{req.Name}店铺已经存在");
        }

        if (req.NationIds[0] == 0)
            return ReturnArr(false, "请选择国家");
        
        var pd = _platformDataCache.Get(req.NationIds[0]);
        if (pd.IsNull())
        {
            return ReturnArr(false, "平台缓存传递查询为空!");
        }

        var Transaction = _dbContext.Database.BeginTransaction();
        try
        {
            var cur = DateTime.Now;
            var region = new Model(Session);
            region.HashCode = Helpers.CreateHashCode(req.Data.SellerID);
            region.Name = req.Name;
            region.AddMode = req.addMode;
            region.UniqueHashCode = UniqueHashCode;
            region.Sort = -cur.GetTimeStamp();
            region.Continent = pd!.GetAmazonData()!.Continent;
            region.Platform = req.platform;
            region.LastOrderTime = cur.AddMonths(-1);
            region.CreatedAt = cur;
            region.UpdatedAt = cur;
            //if (req.platform == Enums.Platforms.AMAZON)
            //{
            //    Model.AmazonData jsonData = new(req.Data, pd!.GetAmazonData()! ?? default!);
            //    region.Data = JsonConvert.SerializeObject(jsonData);
            //}
            //else
            //{
            Model.SPAmazonData spdata = new(req.Data.SellerID, req.NationIds[0], pd!.GetAmazonData()!.KeyId);
            region.Data = JsonConvert.SerializeObject(spdata);
                //}

            //单国家模式下，需要获取国家默认的货币
            if (req.addMode == AddModes.Single)
            {
                region.SetUnitData(_cnitCache.Get(pd.GetAmazonData()!.Unit));
            }

            _dbContext.StoreRegion.Add(region);

            //添加用户组授权
            if (GroupID != 0)
            {
                //StoreGroup
                Models.DB.Users.UserStore userStore = new(_sessionProvider);
                userStore.ID = 0;
                //userStore.Belongs = StoreGroupModel.Belongses.THIRDPART;
                userStore.Platform = req.platform;
                userStore.CreatedAt = cur;
                userStore.UpdatedAt = cur;
                userStore.CorrelationStore = region;
                userStore.CorrelationUserID = UserID;
                await _dbContext.UserStore.AddAsync(userStore);
            }

            await InsertStoreMarket(req.NationIds, UniqueHashCode, UniqueHashCode);

            await _dbContext.SaveChangesAsync();

            #region 统计-新增店铺数量(亚马逊)

            var storeCreatedEvent = new StoreCreatedEvent(region);
            await _mediator.Publish(storeCreatedEvent);

            #endregion

            Transaction.Commit();
            _storeCache.Add(region);
            var lstMarkets = GetMarkets(UniqueHashCode);
            storeMarketCache.AddList(UniqueHashCode, lstMarkets);

        }
        catch (Exception e)
        {
            Transaction.Rollback();
            logger.LogError(e, "AddAmazonStore");
            return ReturnArr(false, e.Message);
        }

        return ReturnArr(true);
    }

    #region mws，sp，shopee  单个店铺的增加

    /// <summary>
    /// 添加单个国家亚马逊店铺
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> AddSingleStore(EditStoreDto req)
    {
        #region 原PHP代码

        /*
//检查权限
if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->sessionData->getRule()->getStore()->getBiter()->hasStoreAdd()) {
    return returnArr(false, getlang('public.authfailed'));
}
//检测剩余可开通店铺数
$CompanyService = new CompanyService();
$companyInfo = $CompanyService->getLimitInfoById($this->sessionData->getCompanyId());
$num = $companyInfo->getOpenStoreNum();
if ($num < 1) {
    return returnArr(false, getlang('store.mystore.NumExceed', ['number' => $num]));
}
$hash_code = createHashCode($requestData['nation_short'] . $requestData['seller_id']);
$res = $this->storeService->checkRepeat($hash_code, $this->sessionData->getCompanyId());
if ($res) {
    return returnArr(false, getlang('store.mystore.StoreExist', ['store' => $requestData['nation']]));
}


Db::beginTransaction();
try {
    $store_id = $this->storeService->insertStore($requestData, $hash_code);
    //向当前用户组授权
    if ($this->sessionData->getConcierge() == UserModel::TYPE_CHILD) {
        $insertStoreGroup = [
            'group_id'   => $this->sessionData->getGroupId(),
            'store_id'   => $store_id,
            'type'       => StoreModels::TYPE_AMAZON,
            'belongs'    => StoreModels::BELONGS_SELF,
            'user_id'    => $this->sessionData->getUserId(),
            'company_id' => $this->sessionData->getCompanyId(),
            'oem_id'     => $this->sessionData->getOemId(),
            'created_at' => $this->datetime,
            'updated_at' => $this->datetime,
        ];
        $this->storeGroupService->insert($insertStoreGroup);

        (new CounterGroupBusiness())->storeNumAdd();
    }
    //添加店铺上报
    $this->storeService->addStoreCount($store_id);
    (new CounterUserBusiness())->storeNumAdd();
    $CounterCompanyBusiness = new CounterCompanyBusiness();
    $CounterCompanyBusiness->storeNumAdd();
    $CounterCompanyBusiness->reportStoreNumAdd();
    Db::commit();
} catch (\Exception $e) {
    Db::rollBack();
    return returnArr(false, $e->getMessage());
}
return returnArr();
*/

        #endregion

        var concierge = Session.GetRole();

        //todo 检查权限
        // if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->sessionData->getRule()->getStore()->getBiter()->hasStoreAdd()) {
        // return returnArr(false, getlang('public.authfailed'));
        // }
        //if (concierge != Concierges.Own && Session)
        //{
        //    return ReturnArr(false, "请检查权限");
        //}

        //检测剩余可开通店铺数
        var companyInfo = await _companyService.GetInfoById(CompanyID);
        if (companyInfo == null)
        {
            return ReturnArr(false, "找不到公司信息");
        }

        var pd = _platformDataCache.Get(req.NationId);
        var UniqueHashCode = Helpers.CreateHashCode(pd.GetAmazonData().Continent + req.Data.SellerID);

        var isExists = await checkRepeat(UniqueHashCode, CompanyID, req.ID);

        if (!isExists)
        {
            return ReturnArr(false, $"修改失败，{req.NationName} 的店铺已经存在");
        }

        var transaction = _dbContext.Database.BeginTransaction();

        try
        {
            var n = await CheckStoreLimit();
            //var openStoreNum = companyInfo.GetOpenStoreNum(CountStore(companyInfo.ID));
            //if (!companyInfo.IsOpenStore)
            //{
            //    companyInfo.OpenStoreNum -= 1;
            //    await _companyService.ChangeOpenStoreNum(companyInfo);
            //    if (openStoreNum <= 0)
            //    {
            //        return ReturnArr(false, $"可开通店铺数为{openStoreNum}，已超出限制！");
            //    }
            //}
            if (!n)
            {
                return ReturnArr(false, $"可开通店铺数为0，已超出限制！");
            }

            var storeId = 1; // await InsertStoreReturnId(req, UniqueHashCode);
            if (storeId == -1)
            {
                return ReturnArr(msg: "国家亚马逊店铺未成功请重试,或者联系管理员");
            }

            if (_sessionProvider.Session!.Is(Role.Employee))
            {
                var dateNow = DateTime.Now;
                var insertStoreUser = new Models.DB.Users.UserStore(Session)
                {
                    CorrelationStoreID = storeId,
                    CorrelationUserID = UserID,
                    Platform = Platforms.AMAZON,
                    IsAutoCreated = true,
                };
                await _dbContext.UserStore.AddAsync(insertStoreUser);
                await _dbContext.SaveChangesAsync();
                await _CounterGroupBusinessService.StoreNumAdd();
            }

            await _dbContext.SaveChangesAsync();


            #region 统计-新增店铺数量(亚马逊)

            //     await _statisticService.IncStoreCount(Session, storeId);

            #endregion

            transaction.Commit();
        }
        catch (Exception e)
        {
            transaction.Rollback();
            return ReturnArr(false, e.Message);
        }

        return ReturnArr();
    }


    ///// <summary>
    ///// MWS-添加单国家模式的店铺
    ///// </summary>
    ///// <param name="req"></param>
    ///// <param name="UniqueHashCode"></param>
    ///// <returns></returns>
    //public async Task<int> InsertStoreReturnId(MwsStoreDto req, ulong UniqueHashCode,Models.DB.PlatformData pd)
    //{  
    //}

    public async Task InsertStoreMarket(List<int?> NationLsts, ulong oldUniqueHashCode, ulong UniqueHashCode)
    {
        var dataLst = new List<ModelMarket>();
        var lst = _dbContext.StoreMarket.Where(x => x.UniqueHashCode == oldUniqueHashCode).ToList();
        if (lst.IsNotNull() && lst.Count > 0)
        {
            _dbContext.StoreMarket.RemoveRange(lst);
        }

        foreach (var mId in NationLsts)
        {
            var pd = _platformDataCache.Get(mId);
            var u = _cnitCache.Get(pd.GetAmazonData()!.Unit);
            var market = new ModelMarket(Session)
            {
                UniqueHashCode = UniqueHashCode,
                NationId = pd?.NationId,
                NationShort = pd.Nation.Short,
                NationName = pd.Nation.Name,
                UnitID = u.ID,
                UnitName = u.Name,
                UnitSign = u.Sign,
                platFormId = pd.ID,
                Marketplace = pd.GetAmazonData()!.Marketplace,
            };
            dataLst.Add(market);
        }

        await _dbContext.StoreMarket.AddRangeAsync(dataLst);       
    }

    #endregion

    public async Task<List<Model>> AmazonList()
    {
        return await _dbContext.StoreRegion
            .Where(m => !m.IsDel && m.Flag == Model.Flags.NO && m.Platform == Platforms.AMAZON)
            .Where(m => m.CompanyID == CompanyID)
            .Where(m => m.UserID == UserID)
            .OrderBy(m => m.State)
            .ThenBy(m => m.Sort)
            .ThenByDescending(m => m.CreatedAt)
            .ToListAsync();
    }





    public async Task<List<Model>> AmazonSpList()
    {
        return await _dbContext.StoreRegion
            .Where(m => !m.IsDel && m.Flag == Model.Flags.NO && m.Platform == Platforms.AMAZONSP)
            .Where(m => m.CompanyID == CompanyID)
            .Where(m => m.UserID == UserID)
            .OrderBy(m => m.State)
            .ThenBy(m => m.Sort)
            .ThenByDescending(m => m.CreatedAt)
            .ToListAsync();
    }

    private IOrderedQueryable<Model> CommonAmazonStoreModel(AmazonStoreListDto req)
    {
        List<Platforms> platforms = new List<Enums.Platforms>();
        if (req.Platforms == null || req.Platforms.Count <= 0)
        {
            platforms.Add(Platforms.AMAZON);
            platforms.Add(Platforms.AMAZONSP);
        }
        else
        {
            platforms = req.Platforms;
        }

        return _dbContext.StoreRegion
            .Where(m => !m.IsDel && m.Flag == Model.Flags.NO && platforms.Contains(m.Platform))
            .Where(m => m.CompanyID == CompanyID)
            .WhenWhere(!string.IsNullOrEmpty(req.Name), m => m.Name.StartsWith(req.Name!))
            .WhenWhere(!string.IsNullOrEmpty(req.SellerID), m => m.HashCode == Helpers.CreateHashCode(req.SellerID!))
            .WhenWhere(req.ContinentsId > 0, m => m.Continent == req.ContinentsId)
            //.WhenWhere(req.NationId > 0, m => m.NationId == req.NationId)
            .WhenWhere(req.lastordertime?.Length == 2 && req.lastordertime != null,
                m => m.LastOrderTime >= Convert.ToDateTime(req.lastordertime![0]) &&
                     m.LastOrderTime <= Convert.ToDateTime(req.lastordertime[1]))
            .OrderBy(m => m.State)
            .OrderBy(m => m.Sort)
            .ThenByDescending(m => m.CreatedAt);
    }

    //private IOrderedQueryable<ModelRegion> nCommonAmazonStoreModel(AmazonStoreListDto req)
    //{
    //    return _dbContext.StoreRegion
    //        .Where(m => !m.IsDel && m.Flag == ModelRegion.Flags.NO && m.Platform == Platforms.AMAZON)
    //        .Where(m => m.CompanyID == CompanyID)
    //        .WhenWhere(!string.IsNullOrEmpty(req.Name), m => m.Name.StartsWith(req.Name!))
    //        .WhenWhere(!string.IsNullOrEmpty(req.SellerID), m => m.HashCode == Helpers.CreateHashCode(req.SellerID!))
    //        .WhenWhere(req.ContinentsId > 0, m => m.Continent == req.ContinentsId)
    //        //.WhenWhere(req.NationId > 0, m => m.NationId == req.NationId)
    //        .WhenWhere(req.lastordertime?.Length == 2 && req.lastordertime != null,
    //            m => m.LastOrderTime >= Convert.ToDateTime(req.lastordertime![0]) &&
    //                 m.LastOrderTime <= Convert.ToDateTime(req.lastordertime[1]))
    //        .OrderBy(m => m.State)
    //        .OrderBy(m => m.Sort)
    //        .ThenByDescending(m => m.CreatedAt);
    //}

    /// <summary>
    /// 店铺列表/我的店铺获取 亚马逊店铺
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    public async Task<AmazonStoreVM> AmazonList(AmazonStoreListDto req)
    {
        var model = CommonAmazonStoreModel(req);

        DbSetExtension.PaginateStruct<Model> stores = new DbSetExtension.PaginateStruct<Model>();
        if (req.Action == ActionEnum.MyStore)
        {
            foreach (var item in model)
            {
                if (item.UserID == UserID)
                {
                    item.Creator = 1;
                }
            }

            List<int> storeIds = await _storeGroupService.GetIdCollectByUserId(UserID);
            stores = await model.Where(m => m.UserID == UserID || storeIds.Contains(m.ID))
                .ToPaginateAsync(page: req.Page, limit: req.Limit);
        }
        else
        {
            stores = await model.ToPaginateAsync(page: req.Page, limit: req.Limit);
        }

        Dictionary<int, UserInfoSelectStruct> users = new();
        if (stores.Items.Count() > 0)
        {
            var items = stores.Items;
            var userIds = items.Select(m => m.UserID).Distinct().ToList();
            var userInfo = await _userService.GetUsers(userIds, m => new UserInfoSelectStruct(m.ID, m.UserName));
            users = userInfo.ToDictionary(x => x.Id, x => x);
        }

        var amazonNation = _platformDataCache.List(new List<Platforms>() { Platforms.AMAZON, Platforms.AMAZONSP })
            .Where(x => x.GetAmazonData()!.Unit.IsNotWhiteSpace())
            .ToDictionary(x => x.ID, x => x.GetAmazonDataWithID());
        var continents = Continents.None.GetIEnumerable();

        var unit = _cnitCache.List();

        return new AmazonStoreVM(stores, users, continents, amazonNation, unit);
    }

    public async Task<List<Model>> GetStoreList(Platforms platforms)
    {
        return await _dbContext.StoreRegion
            .Where(m => !m.IsDel && m.Flag == Model.Flags.NO && platforms == m.Platform)
            .Where(m => m.CompanyID == CompanyID)
            .Where(m => m.IsVerify == IsVerifyEnum.Certified)
            .OrderBy(m => m.State)
            .OrderBy(m => m.Sort)
            .ThenByDescending(m => m.CreatedAt).ToListAsync();
    }

    public async Task<List<ModelMarket>?> GetNationsByStoreID(int storeid)
    {
        var storeInfo = await _dbContext.StoreRegion.Where(x => x.ID == storeid).FirstOrDefaultAsync();
        if (storeInfo is null)
        {
            return null;
        }

        return await _dbContext.StoreMarket
            .Where(x => x.UniqueHashCode == storeInfo.UniqueHashCode && x.CompanyID == CompanyID).ToListAsync();
    }


    /// <summary>
    /// 亚马逊商店列表方法
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    public async Task<DbSetExtension.PaginateStruct<Model>> Indexlist(AmazonStoreListDto req)
    {
        var model = CommonAmazonStoreModel(req);
        return await model
            // .Where(m => m.UserID == UserID)
            .ToPaginateAsync(page: req.Page, limit: req.Limit);
    }

    /// <summary>
    /// 我的店铺列表(自己创建+用户组被分配可见)
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    /*$name, $sellerId, $continentsId, $nationId, $groupStore, $time = []*/
    public async Task<DbSetExtension.PaginateStruct<Model>> MyStoreList(AmazonStoreListDto req)
    {
        var model = CommonAmazonStoreModel(req);
        foreach (var item in model)
        {
            if (item.UserID == UserID)
            {
                item.Creator = 1;
            }
        }

        if (_sessionProvider.Session!.Is(Role.Employee))
        {
            List<int> storeIds = await _storeGroupService.GetIdCollectByUserId(UserID);
            return await model.Where(m => m.UserID == UserID || storeIds.Contains(m.ID))
                .ToPaginateAsync( /*page: req.Page, limit: req.Limit*/);
        }

        return await model.ToPaginateAsync(page: req.Page, limit: req.Limit);
    }

    /// <summary>
    /// MWS  授权验证
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="CacheAmazonKey"></param>
    /// <param name="CachePlatformData"></param>
    /// <returns></returns>
    /// <exception cref="NotExistStoreMarket"></exception>
    /// <exception cref="VerifySaveFail"></exception>
    /// <exception cref="VerifyFail"></exception>
    /// <exception cref="NotExistStore"></exception>
    public async Task<bool> AmazonVerifyStore(int ID, AmazonKeyCache CacheAmazonKey,
        PlatformDataCache CachePlatformData)
    {
        var store = await GetInfoById(ID);
        if (store != null)
        {
            var MarketInfo = await GetAllMarketInfoByHash(store.UniqueHashCode);
            if (MarketInfo == null || MarketInfo.Count <= 0)
            {
                return false;
            }
            var markets = MarketInfo.Select(x => x.Marketplace).ToList();
            var _ApiOrderServices = _NewApiOrderFactory.Create(store.Platform);
            var mLst = await _ApiOrderServices.GetMarketIds(store);
            foreach (var m in markets)
            {
                if (!mLst.Contains(m))
                {
                    throw new VerifyFail(ID);
                }
            }
            store.IsVerify = IsVerifyEnum.Certified;
            if (_dbContext.SaveChanges() > 0)
            {
                return true;
            }
            else
            {
                throw new VerifySaveFail(ID);
            }
        }
        else
        {
            throw new NotExistStore(ID);
        }
    }


    /// <summary>
    /// 修改其他店铺
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> EditOtherStore(EditOtherStoreDto req)
    {
        if (req.ID <= 0)
        {
            var companyInfo = await _companyService.GetInfoById(CompanyID);
            if (companyInfo == null)
            {
                return ReturnArr(false, "找不到公司信息");
            }

            //检测剩余可开通店铺数
            var n = await CheckStoreLimit();
            if (!n)
            {
                return ReturnArr(false, $"可开通店铺数为{n}，已超出限制！");
            }

            if (await AddOtherStore(req) > 0)
            {
                return ReturnArr(true, "添加完成");
            }
            else
            {
                return ReturnArr(false, "添加失败");
            }
        }
        else
        {
            //修改
            var res = await UpdateOtherStore(req);
            if (res > 0)
            {
                return ReturnArr(true, "修改完成");
            }
            else
            {
                return ReturnArr(false, "修改失败");
            }
        }
    }

    /// <summary>
    /// 排序设置
    /// </summary>
    /// <param name="setSortDto"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> SetSort(SetSortDto setSortDto)
    {
        int Now = DateTime.Now.GetTimeStamp();
        int Sort = setSortDto.Type == 1 ? -Now : Now;
        try
        {
            var info = await _dbContext.StoreRegion.Where(x => x.ID == setSortDto.Id).FirstAsync();
            info.Sort = Sort;
            info.CreatedAt = DateTime.Now;
            info.UpdatedAt = DateTime.Now;
            await _dbContext.StoreRegion.SingleUpdateAsync(info);
            var list = await _dbContext.StoreRegion.OrderByDescending(x => x.Sort).ToPaginateAsync();
            if (_dbContext.SaveChanges() > 0)
                return ReturnArr(true, "成功", list);

            return ReturnArr(false, "失败", list);
        }
        catch (Exception e)
        {
            return ReturnArr(false, e.Message, null);
        }
    }


    //public async Task<ReturnStruct> AddMultiStore(AddMultiStoreDto addMultiStore)
    //{

    //    //if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->sessionData->getRule()->getStore()->getBiter()->hasStoreAdd()) {
    //    //    return error(getlang('public.authfailed'));
    //    //}
    //    DateTime Now = DateTime.Now;
    //    var Info = await _companyService.GetLimitInfoById(CompanyID);
    //    if (Info == null)
    //    {
    //        return ReturnArr(false, "没有找到公司");
    //    }

    //    var openStoreNum = Info.GetOpenStoreNum(CountStore(Info.ID));
    //    if (!Info.IsOpenStore)
    //    {
    //        Info.OpenStoreNum -= 1;
    //        await _companyService.ChangeOpenStoreNum(Info);
    //        if (openStoreNum <= 0)
    //        {
    //            return ReturnArr(false, $"可开通店铺数为{openStoreNum}，已超出限制！");
    //        }
    //    }

    //        Model.AmazonData data = new();
    //    data.SetData(addMultiStore.Data);

    //    //Store
    //    Model insertData = new(Session);
    //    insertData.State = Model.StateEnum.Open;
    //    insertData.Platform = Platforms.AMAZON;
    //    insertData.Sort = Now.GetTimeStamp();
    //    insertData.Flag = Model.Flags.NO;


    //    var Transaction = _dbContext.Database.BeginTransaction();
    //    try
    //    {
    //        var Unit = _cnitCache.List();
    //        ulong UniqueHashCode = 0;
    //        int Count = 0;
    //        Info.OpenStoreNum -= addMultiStore.checkList.Count();
    //        await _companyService.ChangeOpenStoreNum(Info);
    //        foreach (var item in addMultiStore.checkList)
    //        {
    //            var platformData = _platformDataCache.Get(item);
    //            if (platformData is not null)
    //            {
    //                var AmazonData = platformData.GetAmazonData();
    //                if (AmazonData is not null)
    //                {
    //                    Count++;
    //                    UniqueHashCode = Helpers.CreateHashCode(AmazonData.Short + addMultiStore.Data.SellerID);
    //                    //判断是否重复
    //                    var tmp = await checkRepeat(UniqueHashCode, CompanyID, addMultiStore.ID);
    //                    if (!tmp)
    //                    {
    //                        return ReturnArr(false, msg: "店铺重复");
    //                    }

    //                    data.SetData(platformData.GetAmazonData()!);
    //                    insertData.AddMode = (Model.AddModes)addMultiStore.AddMode;
    //                    insertData.Continent = AmazonData.Continent;
    //                    insertData.Name = $"{addMultiStore.Name}-{AmazonData.Name}";
    //                    //insertData.NationId = platformData.NationId;
    //                    insertData.SetUnitData(_cnitCache.Get(AmazonData.Unit));
    //                    //insertData.NationName = AmazonData.Name ?? "";
    //                    //insertData.NationShort = AmazonData.Short ?? "";
    //                    insertData.Data = JsonConvert.SerializeObject(data);
    //                    insertData.HashCode = Helpers.CreateHashCode(addMultiStore.Data.SellerID);
    //                    insertData.UniqueHashCode = UniqueHashCode;
    //                    insertData.Sort = -DateTime.Now.GetTimeStamp();
    //                    insertData.IsVerify = Model.IsVerifyEnum.Uncertified;
    //                    insertData.ID = 0;

    //                    //添加店铺
    //                    await _dbContext.Stores.AddAsync(insertData);
    //                    _dbContext.Entry(insertData.Unit!).State = EntityState.Detached;
    //                    _dbContext.SaveChanges();
    //                    _storeCache.Add(insertData);

    //                    //添加用户组授权
    //                    if (GroupID != 0)
    //                    {
    //                        //StoreGroup
    //                        Models.DB.Users.UserStore userStore = new(_sessionProvider);
    //                        userStore.ID = 0;
    //                        //userStore.Belongs = StoreGroupModel.Belongses.THIRDPART;
    //                        userStore.Platform = Platforms.AMAZON;
    //                        userStore.CreatedAt = Now;
    //                        userStore.UpdatedAt = Now;
    //                        userStore.CorrelationStoreID = insertData.ID;
    //                            userStore.CorrelationUserID = insertData.UserID>0? insertData.UserID : UserID;

    //                        await _dbContext.UserStore.AddAsync(userStore);
    //                        _dbContext.Entry(userStore.CorrelationStore).State = EntityState.Detached;
    //                        _dbContext.SaveChanges();
    //                    }

    //                    //添加店铺上报
    //                    //await addStoreCount(insertData.ID);

    //                    #region 统计-新增店铺数量(亚马逊)

    //                    await _statisticService.IncStoreCount(Session, insertData.ID);

    //                    #endregion
    //                }
    //                else
    //                {
    //                    return ReturnArr(false, $"{nameof(AmazonData)} null");
    //                }
    //            }
    //            else
    //            {
    //                return ReturnArr(false, $"{nameof(platformData)} null");
    //            }
    //        }
    //        Transaction.Commit();
    //    }
    //    catch (Exception)
    //    {
    //        Transaction.Rollback();
    //        return ReturnArr(false);
    //    }

    //    return ReturnArr(true);
    //}

    //亚马逊商店更新方法
    public async Task<ReturnStruct> addOrUpdateStore(string data)
    {
        #region MyRegion

        /*
        //基本信息---获取亚马逊国家信息
        $amazonNation = arrayCombine($this->amazonNation(), "id");
        $nation = $amazonNation[$data['nation_id']];
        if (!isset($amazonNation[$data["nation_id"]])) {
            return false;
        }
        $hash_code = createHashCode($nation['short'] . $data['seller_id']);
        $choose = $data['data']['choose'];
        $jsonData = [
            'mws' => $nation['mws'],
            'marketplace' => $nation['marketplace'],
            'choose' => $choose,
            'seller_id' => $data['seller_id'],
            'mws_auth_token' => $choose == StoreGroupModels::BINDMODE_THIRDPART ? $data['data']['mws_auth_token'] : '',
            'aws_access_key' => $choose == StoreGroupModels::BINDMODE_MWSDEV ? $data['data']['aws_access_key'] : '',
            'secret_key' => $choose == StoreGroupModels::BINDMODE_MWSDEV ? $data['data']['secret_key'] : ''
        ];
        $jsonData = json_encode($jsonData, true);
        $insert = [
            'name' => $data['name'],
            'org' => $nation['org'],
            'nation_id' => $data['nation_id'],
            'nation_short' => $data['nation_short'],
            'nation' => $data['nation'],
            'type' => StoreModels::TYPE_AMAZON,
            'data' => $jsonData,
            'sort' => -$this->time,
            'seller_id' => $data['seller_id'],
            'flag' => StoreModels::FLAG_NO,
            'unit' => $data['unit'],
            'unit_sign' => $data['unit_sign'],
            'company_id' => $this->getSession()->getCompanyId(),
            'continents_id' => $nation['continents_id'],
            'hash_code' => $hash_code,
            'key_id' => $nation['key_id'],
            'oem_id' => $this->getSession()->getOemId(),

        ];
        //修改
        if (isset($data['id']) && !empty($data['id'])) {
            $id = $data['id'];
            //查询原数据
            $yuan = StoreModels::query()->find($id);
            $insert['state'] = $data['state'];
            $insert['updated_at'] = $this->datetime;
            if ($data['fix'] == 1) {
                //同步修改参数
                //查询是否存在其他店铺
                $exist = StoreModels::query()
                    ->sellerId($yuan['seller_id'])
                    ->userId($data['user_id'])
                    ->idNot($data['id'])
                    ->select()
                    ->get()
                    ->toArray();
                //dump($exist);

                //存在其他店铺
                DB::beginTransaction();
                try {
                    if (checkArr($exist)) {
                        //循环相同条件店铺
                        $info = [];
                        foreach ($exist as $key => $value) {
                            //赋值
                            //dd($data['seller_id']);
                            $info['seller_id'] = $data['seller_id'];
                            $info['secret_key'] = $data['data']['secret_key'];
                            $info['marketplace'] = $value['data']['marketplace'];
                            $info['mws'] = $value['data']['mws'];
                            $info['choose'] = $data['data']['choose'];
                            $info['aws_access_key'] = $data['data']['aws_access_key'];
                            $info['mws_auth_token'] = $data['data']['mws_auth_token'];
                            $hash_code = createHashCode($value['nation_short'] . $data['seller_id']);
                            $updateDataOne = array(
                                'hash_code' => $hash_code,
                                'seller_id' => $data['seller_id'],
                                'data' => json_encode($info, true)
                            );
                            $updateDataOne['state'] = $data['state'];
                            $updateDataOne['updated_at'] = $this->datetime;
                            StoreModels::query()->where('id', $value['id'])->update($updateDataOne);
                        }
                    }
                    StoreModels::query()->where('id', $id)->update($insert);
                    Db::commit();
                } catch (\Exception $e) {
                    Db::rollback();
                    return returnArr(false, getlang('store.store.ImportFailed'));
                }
                return true;
            } else {
                //fix=2 不同步修改
                return StoreModels::query()->where('id', $id)->update($insert);
            }
        } else {
            //添加
            $lastMonth = strtotime('-1 month');
            $insert['user_id'] = $this->getSession()->getUserId();
            $insert['group_id'] = $this->getSession()->getGroupId();
            $insert['created_at'] = $this->datetime;
            $insert['updated_at'] = $this->datetime;
            $insert['last_order_time'] = date('Y-m-d H:i:s', $lastMonth);
            $insert['state'] = StoreModels::STATE_ENABLE;
            return StoreModels::query()->insertGetId($insert);
        }
        */

        #endregion

        return await Task.FromResult(ReturnArr());
    }

    public async Task<ReturnStruct> CheckMarketIDs(IdDto req)
    {
        try
        {
            var model = _dbContext.StoreRegion.Where(x => x.ID == req.ID).FirstOrDefault();
            if (model is not null)
            {
                var marketLst = await GetAllMarketInfoByHash(model.UniqueHashCode);
                if (marketLst.IsNotNull() && marketLst!.Any())
                {
                    if (model.Platform != Enums.Platforms.AMAZONSP)
                    {
                        return ReturnArr(false, $"{model.Platform.GetDescription()}暂未对接", null);
                    }                   

                    var _ApiOrderServices = _NewApiOrderFactory.Create(model.Platform);
                    var result = await _ApiOrderServices.GetMarketIds(model);
                    if (result.IsNotNull() && result!.Count > 0)
                    {
                        logger.LogInformation(
                            $"CheckMarketIDs store id:{req.ID},markets list:{string.Join(",", result)}");
                        marketLst!.ForEach((x) =>
                        {
                            var pl = _platformDataCache.Get(x.platFormId);
                            if (pl.IsNull())
                            {
                                Console.WriteLine($"方法：CheckMarketIDs，platformData平台id：{x.NationId}不存在，请核查！！！");
                            }

                            if (result.Contains(pl!.GetAmazonData()!.Marketplace))
                            {
                                x.State = BaseModel.StateEnum.Open;
                            }
                            else
                            {
                                x.State = BaseModel.StateEnum.Disabled;
                            }
                        });
                        _dbContext.StoreMarket.UpdateRange(marketLst);
                        _dbContext.SaveChanges();
                        storeMarketCache.AddList(marketLst);
                        return ReturnArr(true, "刷新成功", null);
                    }
                    else
                    {                        
                        logger.LogInformation($"CheckMarketIDs:未找到启用的市场");
                        return ReturnArr(false, "未找到启用的市场", null);
                    }
                }
                else
                {
                    logger.LogError("CheckMarketIDs:没有找到市场信息");
                    return ReturnArr(false, "没有找到市场信息", null);
                }
            }
            else
            {
                logger.LogError("CheckMarketIDs:没用找到该用户信息");
                return ReturnArr(false, "没用找到该用户信息", null);
            }
        }
        catch (Exception e)
        {
            logger.LogError("CheckMarketIDs error:" + e.Message);
            return ReturnArr(false, "市场被封或者没有授权信息");
        }
    }

    public async Task<int> UpdateStoreMarket(ulong oldUniqueHashCode, ulong UniqueHashCode)
    {
        var dataLst = new List<ModelMarket>();
        var lst = _dbContext.StoreMarket.Where(x => x.UniqueHashCode == oldUniqueHashCode).ToList();
        if (lst.IsNotNull() && lst.Count > 0)
        {
            foreach (var mId in lst)
            {
                mId.UniqueHashCode = UniqueHashCode;
            }

            _dbContext.StoreMarket.UpdateRange(lst);
            return 1;
        }

        return -1;
    }

    public List<ModelMarket> GetMarkets(ulong UniqueHashCode)
    {
       return  _dbContext.StoreMarket.Where(x => x.UniqueHashCode == UniqueHashCode && x.State == StateEnum.Open).ToList();       
    }
    


    /// <summary>
    /// 查询相同sellerId 其他店铺
    /// </summary>
    /// <param name="sellerId"></param>
    /// <param name="userId"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<List<Model>> GetSameSellerIdStores(string sellerId, int userId, int id)
    {
        return await _dbContext.StoreRegion
            .Where(x => x.UserID == userId)
            .Where(m => m.ID == id)
            .Where(m => m.HashCode == Helpers.CreateHashCode(sellerId))
            .ToListAsync();
    }

    //删除（假）
    public async Task<bool> del(Model storeModels)
    {
        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            #region 统计数量-删除店铺数

            await _statisticService.DeleteWithCount(_sessionProvider.Session, DBStatistic.NumColumnEnum.StoreDelete,
                DBStatistic.NumColumnEnum.StoreCount);

            #endregion

            #region 清理财务

            //店铺删除清理订单相关信息，不处理采购物流金额
            var storeAmount = await _statisticService.AmountByStore(storeModels);
            await _statisticMoney.RemoveStoreOrderFianceToOem(storeModels.OEMID, storeAmount);
            await _statisticMoney.RemoveStoreOrderFianceToCompany(storeModels.CompanyID, storeAmount);

            //店铺删除清理与用户相关的订单财务，不出处理用户的采购物流金额
            var userIds = await GetStoreWithUserIds(storeModels.ID);
            if (!userIds.Contains(storeModels.UserID))
                userIds.Add(storeModels.UserID);
            var userStoreAmount = _statisticService.UserAmountByTheStore(userIds, storeModels);
            if (userStoreAmount.Count > 0)
                await _statisticMoney.ReduceStoreOrderFianceToUser(userIds, userStoreAmount);

            //店铺删除清理与用户组相关的订单财务，不出处理用户的采购物流金额
            var groupIds = await _userService.GetUserGroupIds(userIds);
            if (!groupIds.Contains(storeModels.GroupID) && storeModels.GroupID != 0)
                groupIds.Add(storeModels.GroupID);
            var groupStoreAmount = _statisticService.GroupAmountByTheStore(groupIds, storeModels);
            if (groupStoreAmount.Count > 0)
                await _statisticMoney.ReduceStoreOrderFianceToGroup(groupIds, groupStoreAmount);
            #endregion

            #region 清理在拉取的订单
            var orders = await _dbContext.Order.Where(x => x.pullOrderState != PullOrderState.Init && x.pullOrderState != PullOrderState.OutOfProcess && x.StoreId == storeModels.ID).ToListAsync();

            if(orders is not null && orders.Count > 0)
            {
                orders.ForEach(x=>x.pullOrderState= PullOrderState.Init);
                _dbContext.Order.UpdateRange(orders);
            }           
            #endregion
            storeModels.IsDel = true;

            var models = await _dbContext.StoreMarket.Where(m => m.UniqueHashCode == storeModels.UniqueHashCode)
                .ToListAsync();
            if (models.IsNotNull())
            {
                _dbContext.StoreMarket.RemoveRange(models);
            }

            await _dbContext.StoreRegion.SingleUpdateAsync(storeModels);
            await _dbContext.SaveChangesAsync();
            await transaction.CommitAsync();
            storeMarketCache.RemoveList(storeModels.UniqueHashCode);
            _storeCache.Remove(storeModels);          
            return true;
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            logger.LogError($"store del fail:{e.Message}");
            logger.LogError($"store del fail:{e.StackTrace}");
            throw new Exception(e.Message);
        }      
    }

    //查询是否可以创建商店
    public async Task<ReturnStruct> checkCreatable(int company_id)
    {
        /*
        $res = CompanyModels::query()->companyId($company_id)->get()->toArray();
        return $res[0];
        */
        return await Task.FromResult(ReturnArr());
    }

    //查询公司下的店铺数量
    public int CountStore(int companyId)
    {
        return _dbContext.StoreRegion.Count(m => m.CompanyID == CompanyID && !m.IsDel);
    }

    //判断是否存在重复店铺
    public async Task<bool> checkRepeat(ulong UniqueHashCode, int companyId,
        int? id = 0 /*$hash_code, $company_id, $id = ''*/)
    {
        var info = await _dbContext.StoreRegion
            //.Where(x => x.ID == id)
            .Where(m => m.IsDel == false)
            .Where(m => m.UniqueHashCode == UniqueHashCode)
            //.Where(m => m.CompanyID == companyId)
            .FirstOrDefaultAsync();

        if (info is not null) return false;

        return true;
    }

    public async Task<bool> checkRepeatForUpdate(ulong UniqueHashCode, int companyId, int? id)
    {
        var info = await _dbContext.StoreRegion
            .Where(x => x.ID != id)
            .Where(m => m.IsDel == false)
            .Where(m => m.UniqueHashCode == UniqueHashCode)
            // .Where(m => m.CompanyID == companyId)
            .FirstOrDefaultAsync();

        if (info is not null) return false;

        return true;
    }
    
    public async Task<bool> checkRepeatForStoreName(string name, int companyId, int? id)
    {
        var info = await _dbContext.StoreRegion
            .WhenWhere(id != null ,x => x.ID != id)
            .Where(m => m.Name == name)
            .Where(m => m.IsDel == false)
            .Where(m => m.CompanyID == companyId)
            .FirstOrDefaultAsync();

        if (info is not null) return false;

        return true;
    }

    //添加的多国店铺中单条店铺信息存库
    public async Task<ReturnStruct> insert( /*StoreGroupModel StoreGroupModel*/)
    {
        /*
        $lastMonth = strtotime('-1 month');
        $data['created_at'] = $this->datetime;
        $data['updated_at'] = $this->datetime;
        $data['user_id'] = $this->getSession()->getUserId();
        $data['company_id'] = $this->getSession()->getCompanyId();
        $data['oem_id'] = $this->getSession()->getOemId();
        $data['group_id'] = $this->getSession()->getGroupId();
        $data['last_order_time'] = date('Y-m-d H:i:s', $lastMonth);
        $res = StoreModels::query()->insertGetId($data);
        //dd($res);
        if (!$res) {
            return returnArr(false, getlang('public.Addfailed'));
        }
        return $res;
        */
        return await Task.FromResult(ReturnArr());
    }

    //获取公司下全部店铺
    public async Task<List<Model>> getStoreByCompanyId(int company_id)
    {
        return await _dbContext.StoreRegion.Where(x =>
                x.CompanyID == company_id && !x.IsDel && x.Flag == Model.Flags.NO &&
                x.IsVerify == IsVerifyEnum.Certified)
            .OrderBy(o => o.Sort).ToListAsync();
    }

    /// <summary>
    /// 根据指定店铺ids获取店铺信息
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    public async Task<List<Model>> GetStoresByIds(List<int?> ids)
    {
        return await _dbContext.StoreRegion.Where(s => ids.Contains(s.ID)).ToListAsync();
    }

    public async Task UpdateStoresPullState(List<Model> stores)
    {
        _dbContext.StoreRegion.UpdateRange(stores);
        await _dbContext.SaveChangesAsync();
    }


    public async Task<List<ModelMarket>> GetStoreMarketsByStoreHash(ulong hash)
    {
        return await _dbContext.StoreMarket.Where(s => s.UniqueHashCode == hash && s.State == BaseModel.StateEnum.Open)
            .ToListAsync();
    }


    public record StoreWithNameDto(int Id, string Name, BaseModel.StateEnum State);

    //获取公司内部店铺分页数据
    public Task<DbSetExtension.PaginateStruct<StoreWithNameDto>> GetStorePage(int companyId, string? name,
        BaseModel.StateEnum? state)
    {
        return _dbContext.StoreRegion.Where(s => s.CompanyID == companyId && s.Flag == Model.Flags.NO && !s.IsDel)
            .WhenWhere(!string.IsNullOrEmpty(name), model => model.Name.StartsWith(name!))
            .WhenWhere(state.HasValue, model => model.State == state)
            .OrderByDescending(s => s.ID)
            .Select(s => new StoreWithNameDto(s.ID, s.Name, s.State))
            .ToPaginateAsync();
    }

    //向数据统计-店铺中加入店铺信息
    public async Task<ReturnStruct> addStoreCount(int StoreIds)
    {
        CounterStoreBusiness counterStore = new(Session);
        counterStore.StoreId = StoreIds;
        await _dbContext.CounterStoreBusiness.AddAsync(counterStore);
        if (await _dbContext.SaveChangesAsync() > 0)
            return ReturnArr(true);

        return ReturnArr(false);
    }

    //获取有效店铺参数
    public async Task<ReturnStruct> getMwsInfo( /*store*/)
    {
        /*
        $data = $store["data"];

        $country = $this->amazonNation();
        $country = arrayCombine($country, "id");
        if(!isset($country[$store['nation_id']])){
            return $data;
        }
        $key_id = $country[$store['nation_id']]['key_id'];
        $amazomKey = Cache::instance()->amazonKey()->get();
        if (isset($amazomKey[$key_id]) && $data["choose"] == 1) {
            $data["secret_key"] = $amazomKey[$key_id]["secret_key"];
            $data["aws_access_key"] = $amazomKey[$key_id]["aws_access_key"];
        }
        $data['id'] = $store['id'];
        return $data;
        */
        return await Task.FromResult(ReturnArr());
    }

    //修改其他类型店铺
    public async Task<int> AddOtherStore(EditOtherStoreDto req)
    {
        var transaction = _dbContext.Database.BeginTransaction();
        var StoreData = new Model.OtherData() { Note = req.Data };
        var jsonData = JsonConvert.SerializeObject(StoreData);
        var dateTime = DateTime.Now;
        try
        {
            var insertStoreData = new Model(Session)
            {
                Name = req.Name,
                Platform = Platforms.OTHERS,
                Sort = -dateTime.GetTimeStamp(),
                State = Models.Abstract.BaseModel.StateEnum.Open,
                Data = jsonData,
                CreatedAt = dateTime,
                UpdatedAt = dateTime
            };
            await _dbContext.StoreRegion.AddAsync(insertStoreData);
            await _dbContext.SaveChangesAsync();

            var insertStoreGroupData = new Models.DB.Users.UserStore(Session)
            {
                CorrelationStoreID = insertStoreData.ID,
                CorrelationUserID = UserID,
                Platform = Platforms.OTHERS,
                IsAutoCreated = true
            };

            await _dbContext.UserStore.AddAsync(insertStoreGroupData);
            await _dbContext.SaveChangesAsync();

            #region 统计-新增店铺数量(其他店铺)

            var storeCreatedEvent = new StoreCreatedEvent(insertStoreData);
            await _mediator.Publish(storeCreatedEvent);

            #endregion


            await transaction.CommitAsync();

            _storeCache.Add(insertStoreData);
            return insertStoreData.ID;
        }
        catch (Exception e)
        {
            transaction.Rollback();
            logger.LogError(
                $"storeService AddOtherStore add other store error:{e.Message},storeModel:{JsonConvert.SerializeObject(req)}");
            return -1;
        }
    }

    public async Task<int> UpdateOtherStore(EditOtherStoreDto req)
    {
        var StoreData = new Model.OtherData() { Note = req.Data };
        var jsonData = JsonConvert.SerializeObject(StoreData);
        //修改
        var info = await _dbContext.StoreRegion.Where(m => m.ID == req.ID).FirstOrDefaultAsync();
        if (info == null)
        {
            return -1;
        }

        info.Name = req.Name;
        info.Platform = Platforms.OTHERS;
        info.State = req.State ?? Models.Abstract.BaseModel.StateEnum.Open;
        info.Data = jsonData;
        info.UpdatedAt = DateTime.Now;

        _dbContext.StoreRegion.Update(info);
        await _dbContext.SaveChangesAsync();
        _storeCache.Add(info);
        return req.ID;
    }

    private IOrderedQueryable<Model> CommonOtherStoreModel(OtherStoreListDto otherStoreDto) //
    {
        return _dbContext.StoreRegion
            .Where(m => m.Platform == Platforms.OTHERS && !m.IsDel)
            .Where(m => m.CompanyID == CompanyID)
            .WhenWhere(!string.IsNullOrEmpty(otherStoreDto.Name), m => m.Name.Contains(otherStoreDto.Name!))
            .OrderBy(m => m.State)
            .OrderBy(m => m.Sort)
            .OrderByDescending(m => m.CreatedAt);
    }

    /// <summary>
    /// 其他店铺列表
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    public async Task<DbSetExtension.PaginateStruct<Model>> OtherList(OtherStoreListDto req)
    {
        /*
        return StoreModels::query()
            ->Type(StoreModels::TYPE_OTHERS)
            ->isDel(StoreModels::NOT_DEL)
            ->name($where['name'])
            ->CompanyId($this->getSession()->getCompanyId())
            ->stateSort()
            ->sort()
            ->createSort()
            ->paginate($this->limit);
        */
        DbSetExtension.PaginateStruct<Model> stores = new DbSetExtension.PaginateStruct<Model>();

        var model = CommonOtherStoreModel(req);

        if (req.Action == ActionEnum.MyStore)
        {
            foreach (var item in model)
            {
                if (item.UserID == UserID)
                {
                    item.Creator = 1;
                }
            }

            //if (_sessionProvider.Session!.Is(Role.Employee))
            //{
            List<int> storeIds = await _storeGroupService.GetIdCollectByUserId(UserID);
            stores = await model.Where(m => m.UserID == UserID || storeIds.Contains(m.ID))
                .ToPaginateAsync(page: req.Page, limit: req.Limit);
            //}
            //else  
            //{
            //    stores = await model.ToPaginateAsync(page: req.Page, limit: req.Limit);
            //}
        }
        else
        {
            stores = await model.ToPaginateAsync(page: req.Page, limit: req.Limit);
        }

        foreach (var item in stores.Items)
        {
            //var data = JsonConvert.DeserializeObject(item.Data);
            item.Data = item.GetOtherData()!.Note;

            if (item.UserID == UserID && req.Action == ActionEnum.MyStore)
            {
                item.Creator = 1;
            }
        }

        return stores;
    }

    /// <summary>
    /// 获取其他店铺列表(店铺管理/我的店铺)
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    public async Task<DbSetExtension.PaginateStruct<Model>> OtherStoreList(OtherStoreListDto req) //
    {
        DbSetExtension.PaginateStruct<Model> stores = new DbSetExtension.PaginateStruct<Model>();
        var model = CommonOtherStoreModel(req);

        if (req.Action == ActionEnum.MyStore)
        {
            if (_sessionProvider.Session!.Is(Role.Employee))
            {
                List<int> storeIds = await _storeGroupService.GetIdCollectByUserId(UserID);

                stores = await model.Where(m => m.UserID == UserID || storeIds.Contains(m.ID))
                    .ToPaginateAsync(page: req.Page, limit: req.Limit);
            }
        }
        else
        {
            stores = await model.ToPaginateAsync(page: req.Page, limit: req.Limit);
        }

        foreach (var item in stores.Items)
        {
            //var data = JsonConvert.DeserializeObject(item.Data);
            item.Data = item.GetOtherData()!.Note;

            if (item.UserID == UserID && req.Action == ActionEnum.MyStore)
            {
                item.Creator = 1;
            }
        }

        return stores;
    }

    /// <summary>
    /// 我的其他店铺列表
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    /*where*/
    public async Task<DbSetExtension.PaginateStruct<Model>> OtherMyList(OtherStoreListDto otherStoreDto) //
    {
        var model = CommonOtherStoreModel(otherStoreDto);
        int userId = UserID, groupId = GroupID;
        //todo 向当前用户组授权
        if (_sessionProvider.Session!.Is(Role.Employee))
        {
            List<int> storeIds = await _storeGroupService.GetIdCollectByUserId(UserID);

            return await model.Where(m => m.UserID == userId || storeIds.Contains(m.ID))
                .ToPaginateAsync( /*page: req.Page, limit: req.Limit*/);
        }

        return await model.ToPaginateAsync( /*page: req.Page, limit: req.Limit*/);
    }

    public async Task<bool> CheckStoreLimit()
    {
        var companyInfo = await _companyService.GetInfoById(CompanyID);
        var openStoreNum = companyInfo.GetOpenStoreNum(CountStore(companyInfo.ID));
        if (!companyInfo.IsOpenStore) //有限制
        {
            return openStoreNum > 0;
        }

        return true;
    }


    #region shopee

    /// <summary>
    /// 添加店铺:shopee、amazonSp
    /// </summary>
    /// <param name="req"></param>
    /// <param name="platform"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> AddShopeeStore(ShopeeAndAmazonSpStoreViewModel req)
    {
        //todo 检查权限
        // if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->sessionData->getRule()->getStore()->getBiter()->hasStoreAdd()) {
        // return returnArr(false, getlang('public.authfailed'));
        // }
        // 

        var UniqueHashCode = Helpers.CreateHashCode(req.Name + req.ID);
        var isExists = await checkRepeat(UniqueHashCode, CompanyID, req.ID);
        if (!isExists)
        {
            return ReturnArr(false, $"添加失败，{req.Name} 的店铺已经存在");
        }

        var transaction = _dbContext.Database.BeginTransaction();
        try
        {
            var n = await CheckStoreLimit();
            if (!n)
            {
                return ReturnArr(false, $"可开通店铺数为0，已超出限制！");
            }

            var storeId = await InsertShopeeAndAmazonSpStoreReturnId(req, UniqueHashCode);

            //todo 向当前用户组授权
            if (_sessionProvider.Session!.Is(Role.Employee))
            {
                //todo userSession                
                var dateNow = DateTime.Now;
                var insertStoreGroupData = new Models.DB.Users.UserStore(Session)
                {
                    IsAutoCreated = true,
                    Platform = req.Platform,
                    CorrelationUserID = UserID,
                    CorrelationStoreID = storeId,
                };
                await _dbContext.UserStore.AddAsync(insertStoreGroupData);


            }

            await _dbContext.SaveChangesAsync();



            transaction.Commit();
        }
        catch (Exception e)
        {
            transaction.Rollback();
            return ReturnArr(false, e.Message);
        }

        return ReturnArr();
    }

    /// <summary>
    /// 添加店铺:shopee、amazonSp
    /// 保存操作
    /// </summary>
    /// <param name="req"></param>
    /// <param name="UniqueHashCode"></param>
    /// <returns></returns>
    public async Task<int> InsertShopeeAndAmazonSpStoreReturnId(ShopeeAndAmazonSpStoreViewModel req,
        ulong UniqueHashCode)
    {
        var cur = DateTime.Now;
        var insertData = new Model(_sessionProvider);
        Model.ShopeeData shopee = new Model.ShopeeData(req?.Remark ?? "");
        insertData.Name = req!.Name;
        insertData.State = req.State;

        insertData.IsVerify = Model.IsVerifyEnum.Uncertified;
        insertData.Platform = req.Platform;
        insertData.UniqueHashCode = UniqueHashCode;
        insertData.Sort = -cur.GetTimeStamp();
        insertData.Flag = Model.Flags.NO;
        insertData.LastOrderTime = cur.AddMonths(-1);
        insertData.CreatedAt = cur;
        insertData.UpdatedAt = cur;
        insertData.Data = JsonConvert.SerializeObject(shopee);
        await _dbContext.StoreRegion.AddAsync(insertData);
        _dbContext.SaveChanges();
        _storeCache.Add(insertData);

        #region 统计-新增店铺数量(亚马逊)

        var storeCreatedEvent = new StoreCreatedEvent(insertData);
        await _mediator.Publish(storeCreatedEvent);

        #endregion

        return insertData.ID;
    }

    /// <summary>
    /// 修改店铺:shopee、amazonSp
    /// </summary>
    /// <param name="req"></param>
    /// <param name="platform"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> EditShopeeStore(ShopeeAndAmazonSpStoreViewModel req)
    {
        //todo 检查权限
        // if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->sessionData->getRule()->getStore()->getBiter()->hasStoreEdit()) {
        //     return returnArr(false, getlang('public.authfailed'));
        // }
        //检查是否有重复
        var UniqueHashCode = Helpers.CreateHashCode(req.Name + req.ID);

        var isExists = await checkRepeat(UniqueHashCode, CompanyID, req.ID);

        if (!isExists)
        {
            return ReturnArr(false, $"修改失败，{req.Name} 的店铺已经存在");
        }

        var transaction = _dbContext.Database.BeginTransaction();
        try
        {
            var store = _dbContext.StoreRegion.FirstOrDefault(x => x.ID == req.ID);

            if (store != null)
            {
                if (req.Platform == Platforms.SHOPEE)
                {
                    Model.ShopeeData md = store.GetShopeeData()!;
                    md.Remark = req.Remark;
                    store.Data = JsonConvert.SerializeObject(md);
                }

                store.Name = req.Name;
                store.HashCode = Helpers.CreateHashCode(req.ID.ToString()!);
                store.UniqueHashCode = UniqueHashCode;
                store.UpdatedAt = DateTime.Now;
                _dbContext.StoreRegion.Update(store);
                await _dbContext.SaveChangesAsync();
                _storeCache.Add(store);
                transaction.Commit();
                return ReturnArr();
            }
            else
            {
                return ReturnArr(state: false, msg: "店铺不存在");
            }
        }
        catch (Exception e)
        {
            transaction.Rollback();
            return ReturnArr(false, e.Message);
        }
    }


    public async Task<DbSetExtension.PaginateStruct<ShopeeStoreList>> IndexShopeelist(ShopeeStoreListDto req)
    {
        try
        {
            DbSetExtension.PaginateStruct<ShopeeStoreList>
                stores = new DbSetExtension.PaginateStruct<ShopeeStoreList>();

            var model = CommonShopeeStoreModel(req);

            if (req.Action == ActionEnum.MyStore)
            {
                foreach (var item in model)
                {
                    if (item.UserID == UserID)
                    {
                        item.Creator = 1;
                    }
                }

                if (_sessionProvider.Session!.Is(Role.Employee))
                {
                    List<int> storeIds = await _storeGroupService.GetIdCollectByUserId(UserID);
                    stores = await model.Where(m => m.UserID == UserID || storeIds.Contains(m.ID))
                        .Select(x => new
                            ShopeeStoreList(x)
                        ).ToPaginateAsync(page: req.Page, limit: req.Limit);
                }
                else
                {
                    stores = await model
                        .Select(x => new
                            ShopeeStoreList(x)
                        ).ToPaginateAsync(page: req.Page, limit: req.Limit);
                }
            }
            else
            {
                stores = await model
                    .Select(x => new
                        ShopeeStoreList(x)
                    ).ToPaginateAsync(page: req.Page, limit: req.Limit);
            }


            return stores;
        }
        catch (Exception e)
        {
            Console.WriteLine("IndexShopeelist:" + e.Message);
            return new();
        }
    }

    private IOrderedQueryable<Model> CommonShopeeStoreModel(ShopeeStoreListDto req)
    {
        return _dbContext.StoreRegion.Include(o => o.User)
            .Where(m => !m.IsDel && m.Flag == Model.Flags.NO && m.Platform == Platforms.SHOPEE)
            .Where(m => m.CompanyID == CompanyID)
            .WhenWhere(!string.IsNullOrEmpty(req.Name), m => m.Name.StartsWith(req.Name!))
            .WhenWhere(!string.IsNullOrEmpty(req.ID), m => m.HashCode == Helpers.CreateHashCode(req.ID!))
            .WhenWhere(req.lastordertime?.Length == 2,
                m => m.LastOrderTime >= Convert.ToDateTime(req.lastordertime![0])
                     && m.LastOrderTime <= Convert.ToDateTime(req.lastordertime![1]))
            // .WhenWhere(UserID > 0, m => m.UserID == UserID)
            .OrderBy(m => m.State)
            .ThenBy(m => m.Sort)
            .ThenByDescending(m => m.CreatedAt);
    }

    public async Task<ReturnStruct> GetShopeeUrl(int id)
    {
        var model = await _dbContext.StoreRegion.Where(x => x.ID == id).FirstOrDefaultAsync();
        if (model is not null)
        {
            if (model.IsDel)
            {
                return ReturnArr(false, "该店铺已经被删除，请刷新！");
            }

            if (model!.IsVerify == Model.IsVerifyEnum.Uncertified)
            {
                var md = model.GetShopeeData()!;
                var _ApiOrderServices = _NewApiOrderFactory.Create(model.Platform);
                var flag = _ApiOrderServices.GetAuthURL(model, id.ToString());
                return ReturnArr(true, "获取地址成功", flag);
            }
            else
            {
                return ReturnArr(false, "已经成功获取授权！");
            }
        }
        else
        {
            return ReturnArr(false, "没有找到该用户数据");
        }
    }

    public async Task<ReturnStruct> GetAmazonSpUrl(IdDto req,string host)
    {
        var model = await GetInfoById(req.ID);
        if (model is not null)
        {
            if (model.IsDel)
            {
                return ReturnArr(false, "该店铺已经被删除，请刷新！");
            }

            if (model!.IsVerify == Model.IsVerifyEnum.Uncertified)
            {
                if (model.OEMID == 8)
                {
                    //道旻
                    host = "HEL_dmkjdzsw";
                }
                else
                {
                    //米境通
                    host = "HEL_erp";
                }
                // var state = $"{host}|{req.ID}|{Helpers.GetRandomString()}";
                var state = $"{host}|{req.ID}|{Helpers.GetRandomString()}|3308";
                var _ApiOrderServices = _NewApiOrderFactory.Create(model.Platform);
                var flag = _ApiOrderServices.GetAuthURL(model, state);
                return ReturnArr(true, "获取地址成功", flag);
            }
            else
            {
                return ReturnArr(false, "已经成功获取授权！");
            }
        }
        else
        {
            return ReturnArr(false, "没有找到该用户数据");
        }
    }

    public async Task SaveAmazonStore(Model model, TokenResponse result)
    {       
        var spamazon = model.GetSPAmazon()!;
        spamazon.AccessToken = result.access_token;
        spamazon.RefreshToken = result.refresh_token;
        spamazon.Date_Created = result.date_Created;
        spamazon.ExpireIn = result.expires_in;
        spamazon.TokenType = result.token_type;
        model.Data = JsonConvert.SerializeObject(spamazon);
        model.QueryEnd = DateTime.Now.AddDays(-7);
        model.IsVerify = Model.IsVerifyEnum.Certified;
        _dbContext.StoreRegion.Update(model);
        await _dbContext.SaveChangesAsync();
        _storeCache.Add(model);
    }

    /// <summary>
    /// 店铺注册时走得方法
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> SaveAmazonAccessToken(AmazonCode req)
    {
        try
        {
            logger.LogInformation($"SaveAmazonAccessToken:{JsonConvert.SerializeObject(req)}");
            var model = await GetInfoById(req.ID);
            if (model is not null)
            {
                var spamazon = model.GetSPAmazon()!;
                if (spamazon.SellerID.Equals(req.PartnerId))
                {
                    var _ApiOrderServices = _NewApiOrderFactory.Create(model.Platform);
                    var result = await _ApiOrderServices.VerifyStore<TokenResponse>(model, req.Code);
                    if (result.IsNotNull() && !string.IsNullOrEmpty(result?.access_token))
                    {
                        await SaveAmazonStore(model, result);
                        //核查市场
                        var r = await CheckMarketIDs(new IdDto() { ID = req.ID });
                        if (r.State)
                        {
                            logger.LogInformation($"SaveAmazonAccessToken:success");
                            return ReturnArr(true, "", null);
                        }
                        else
                        {
                            model.IsVerify = Model.IsVerifyEnum.Uncertified;
                            _dbContext.StoreRegion.Update(model);
                            _dbContext.SaveChanges();
                            logger.LogInformation($"SaveAmazonAccessToken:检查市场操作失败");
                            return ReturnArr(false, "检查市场操作失败", null);
                        }
                    }
                    else
                    {
                        logger.LogInformation($"SaveAmazonAccessToken:请求接口失败");
                        return ReturnArr(false, "请求token接口失败", null);
                    }
                }
                else
                {
                    logger.LogInformation($"SaveAmazonAccessToken:填写的sellerID不正确");
                    return ReturnArr(false, "填写的sellerID不正确，请修改", null);
                }
            }
            else
            {
                logger.LogInformation($"SaveAmazonAccessToken:没用找到该用户信息");
                return ReturnArr(false, "没用找到该用户信息", null);
            }
        }
        catch (Exception ex)
        {
            logger.LogError($"SaveAmazonAccessToken Exception:{ex.Message}");
            logger.LogError($"SaveAmazonAccessToken Exception:{ex.StackTrace}");
            return ReturnArr(false, "Exception", null);
        }
    }


    public async Task<ReturnStruct> RefreshAccessToken(IdDto req)
    {
        var model = await GetInfoById(req.ID);
        if (model is not null)
        {
            if (model.Platform == Enums.Platforms.AMAZONSP)
            {
                var spamazon = model.GetSPAmazon()!;
                if (spamazon.AccessToken == null || string.IsNullOrEmpty(spamazon.AccessToken))
                {
                    return ReturnArr(false, "该用户没有授权过，请先手工授权", null);
                }
                var _ApiOrderServices = _NewApiOrderFactory.Create(model.Platform);
                var result = await _ApiOrderServices.RefreshNormalAccessToken(model);
                if (result)
                {
                    return ReturnArr(result, "授权成功", null);
                }
                else
                {
                    return ReturnArr(result, "授权失败", null);
                }
            }
            else
            {
                return ReturnArr(false, "该店铺所属平台不是amazon spapi", null);
            }
        }
        else
        {
            return ReturnArr(false, "没用找到该店铺信息", null);
        }
    }


    /// <summary>
    /// 更新amazon sp token
    /// </summary>
    /// <param name="id"></param>
    /// <param name="result"></param>
    /// <returns></returns>
    public async Task<bool> UpdateAmazonSPAccesstoken(int id, TokenResponse result, bool flag = true)
    {
        var model = await GetInfoById(id);
        if (model is not null)
        {
            if (!flag)
            {
                //model.IsVerify = IsVerifyEnum.Uncertified;
            }
            else
            {
                model.IsVerify = IsVerifyEnum.Certified;
                var spamazon = model.GetSPAmazon()!;
                if (spamazon.AccessToken == null || string.IsNullOrEmpty(spamazon.AccessToken))
                {
                    return false;
                }

                spamazon.AccessToken = result.access_token;
                spamazon.RefreshToken = result.refresh_token;
                spamazon.ExpireIn = result.expires_in;
                spamazon.Date_Created = result.date_Created;
                spamazon.TokenType = result.token_type;
                model.Data = JsonConvert.SerializeObject(spamazon);
            }

            _dbContext.StoreRegion.Update(model);
            await _dbContext.SaveChangesAsync();
            _storeCache.Add(model);
            return true;
        }
        else 
        {
            return false;
        }
    }

    public async Task<bool> UpdateAmazonToken(int id, amazonTokenResponse result, bool flag = true)
    {
        var model = await GetInfoById(id);
        if (model is not null)
        {
            if (!flag)
            {
                //model.IsVerify = IsVerifyEnum.Uncertified;
            }
            else
            {
                model.IsVerify = IsVerifyEnum.Certified;
                var spamazon = model.GetSPAmazon()!;
                if (spamazon.AccessToken == null || string.IsNullOrEmpty(spamazon.AccessToken))
                {
                    return false;
                }

                spamazon.AccessToken = result.access_token;
                spamazon.RefreshToken = result.refresh_token;
                spamazon.ExpireIn = result.expires_in;
                spamazon.Date_Created = result.date_Created;
                spamazon.TokenType = result.token_type;
                model.Data = JsonConvert.SerializeObject(spamazon);
            }
            _dbContext.StoreRegion.Update(model);
            await _dbContext.SaveChangesAsync();
            _storeCache.Add(model);
            return true;
        }
        else
        {
            return false;
        }
    }

    public async Task<bool> UpdateAmazonSPVerity(int id, int tokenCount, int flag = 0)
    {
        var model = await GetInfoById(id);
        if (model is not null)
        {
            if (flag == 0)
            {
                //model.IsVerify = IsVerifyEnum.Uncertified;
                model.TokenCount = tokenCount++;
            }
            else if (flag == -1)
            {
                //model.IsVerify = IsVerifyEnum.Uncertified;
                model.TokenCount = tokenCount;
            }
            else
            {
                //model.IsVerify = IsVerifyEnum.Certified;
                model.TokenCount = tokenCount;
            }

            _dbContext.StoreRegion.Update(model);
            await _dbContext.SaveChangesAsync();
            _storeCache.Add(model);
            return true;
        }
        else
        {
            return false;
        }
    }


    public async Task<ReturnStruct> SaveShopeeAccessToken(ShopeeCode req)
    {
        List<Model> storeModels = new List<Model>();
        var model = _dbContext.StoreRegion.Where(x => x.ID == req.ID).FirstOrDefault();
        if (model is not null)
        {
            string newCode = "";
            if (req.ShopId != 0) newCode = $"ShopId-{req.ShopId}-{req.Code}";
            if (req.MainAccountId != 0) newCode = $"MainAccountId-{req.MainAccountId}-{req.Code}";
            var _ApiOrderServices = _NewApiOrderFactory.Create(model.Platform);
            var result = await _ApiOrderServices.VerifyStore<ResponseAccessToken>(model, newCode);
            if (result is not null && result.Error.IsNullOrWhiteSpace())
            {
                List<int> shopidlst = new List<int>();
                var shopee = model.GetShopeeData()!;
                shopee.Time = DateTime.Now;
                shopee.AccessToken = result.AccessToken;
                shopee.RefreceToken = result.RefreshToken;
                shopee.Expire_in = result.ExpireIn;
                if (req.ShopId != 0)
                {
                    shopidlst.Add(req.ShopId);
                    shopee.MainAccountId = 0;
                    shopee.CountryType = Model.ShopeeData.CountryTypes.Single;
                }
                else if (req.MainAccountId != 0)
                {
                    shopidlst.AddRange(result.ShopIdList);
                    shopee.MainAccountId = req.MainAccountId;
                    shopee.CountryType = Model.ShopeeData.CountryTypes.Multiple;
                }

                if (shopidlst.Count > 0)
                {
                    for (int i = 0; i < shopidlst.Count; i++)
                    {
                        shopee.ShopID = shopidlst[i];
                        model.Data = JsonConvert.SerializeObject(shopee);
                        model.IsVerify = Model.IsVerifyEnum.Certified;
                        if (i == 0)
                        {
                            _dbContext.StoreRegion.Update(model);
                            _dbContext.SaveChanges();
                            _storeCache.Add(model);
                        }
                        else
                        {
                            Model newStore = new Model();
                            newStore.CompanyID = model.CompanyID;
                            newStore.Continent = model.Continent;
                            newStore.CreatedAt = model.CreatedAt;
                            newStore.Data = model.Data;
                            newStore.Flag = model.Flag;
                            newStore.GroupID = model.GroupID;
                            newStore.HashCode = model.HashCode;
                            newStore.IntervalSeconds = model.IntervalSeconds;
                            newStore.IsVerify = model.IsVerify;
                            newStore.LastAllotTime = model.LastAllotTime;
                            newStore.LastAutoShipTime = model.LastAutoShipTime;
                            newStore.LastAutoUpdateTime = model.LastAutoUpdateTime;
                            newStore.LastOrderTime = model.LastOrderTime;
                            newStore.LastPullStart = model.LastPullStart;
                            newStore.LastShipTime = model.LastShipTime;
                            newStore.Name = model.Name;
                            newStore.NextEvaluate = model.NextEvaluate;
                            newStore.NextPull = model.NextPull;
                            newStore.OEMID = model.OEMID;
                            newStore.Sort = model.Sort;
                            newStore.State = model.State;
                            newStore.Platform = model.Platform;
                            newStore.UniqueHashCode = model.UniqueHashCode;
                            newStore.UnitID = model.UnitID;
                            newStore.UnitName = model.UnitName;
                            newStore.UnitSign = model.UnitSign;
                            newStore.UpdatedAt = model.UpdatedAt;
                            newStore.UserID = model.UserID;
                            storeModels.Add(newStore);
                        }
                    }

                    if (storeModels.Count > 0)
                    {
                        var transaction = await _dbContext.Database.BeginTransactionAsync();
                        try
                        {
                            await _dbContext.StoreRegion.AddRangeAsync(storeModels);
                            await _dbContext.SaveChangesAsync();
                            storeModels.ForEach(_storeCache.Add);
                            foreach (var item in storeModels)
                            {
                                if (!item.IsDel)
                                {
                                    #region 统计-新增店铺数(虾皮)

                                    await _statisticService.IncStoreCount(_sessionProvider.Session, item);

                                    #endregion
                                }
                                else
                                {
                                    //todo 可能会新增已逻辑删除的店铺
                                }
                            }

                            await transaction.CommitAsync();
                        }
                        catch (Exception e)
                        {
                            await transaction.RollbackAsync();
                            Console.WriteLine(e.Message);
                            throw new Exception(e.Message);
                        }
                    }

                    return ReturnArr(true, "", null);
                }
                else
                {
                    return ReturnArr(false, "请选择店铺赋权", null);
                }
            }
            else
            {
                return ReturnArr(false, result?.Message == null ? result!.Error : result.Message, null);
            }
        }
        else
        {
            return ReturnArr(false, "没用找到该用户信息", null);
        }
    }


    public async Task<bool> UpdateShopeeAccssToken(int storeId, ResponseAccessToken token)
    {
        var model = _dbContext.StoreRegion.Where(x => x.ID == storeId).FirstOrDefault();
        if (model is not null)
        {
            var transaction = await _dbContext.Database.BeginTransactionAsync();
            try
            {
                var shopee = model.GetShopeeData()!;
                shopee.Time = DateTime.Now;
                shopee.Expire_in = token.ExpireIn;
                shopee.AccessToken = token.AccessToken;
                shopee.RefreceToken = token.RefreshToken;
                model.Data = JsonConvert.SerializeObject(shopee);
                _dbContext.StoreRegion.Update(model);
                _dbContext.SaveChanges();
                _storeCache.Add(model);
                await transaction.CommitAsync();
                return true;
            }
            catch (Exception e)
            {
                await transaction.RollbackAsync();
                Console.WriteLine(e.Message);
                return false;
            }
        }

        return false;
    }

    #endregion


    /**********************     ******************/
    /// <summary>
    /// 当前用户获取[用户店铺表绑定的]的店铺
    /// </summary>
    /// <param name="enableFilter">false查询所有的店铺 true查询所有启用的店铺</param>
    /// <returns></returns>
    private async Task<List<Model>> EmployeeGetCompanyStores(bool enableFilter = false)
    {
        return await _dbContext.UserStore
            .Where(a => a.CorrelationUserID == UserID)
            .Include(a => a.CorrelationStore).Select(a => a.CorrelationStore)
            .WhenWhere(enableFilter, a => a.State == BaseModel.StateEnum.Open)
            .ToListAsync();
    }

    /// <summary>
    /// 不管是主账户还是子账户，查询自己的店铺时，只要根据所在组，查询所属店铺关系即可
    /// </summary>
    /// <param name="enableFilter">false查询所有的店铺 true查询所有启用的店铺</param>
    /// <returns></returns>
    public async Task<List<Model>> CurrentUserGetStores(bool enableFilter = false)
    {
        if (Session.Is(Role.Employee))
        {
            return await _dbContext.UserStore.Include(x => x.CorrelationStore)
                .Where(x => x.CorrelationUserID == UserID)
                .Where(s => !s.CorrelationStore.IsDel)
                .WhenWhere(enableFilter, a => a.CorrelationStore.State == BaseModel.StateEnum.Open)
                .Select(a => a.CorrelationStore)
                .ToListAsync();
        }
        else
        {
            //return await _dbContext.UserStore.Include(x => x.CorrelationStore)
            // .Where(x => x.CompanyID == CompanyID && x.OEMID==OEMID)
            // .Where(s => !s.CorrelationStore.IsDel)
            // .WhenWhere(enableFilter, a => a.CorrelationStore.State == BaseModel.StateEnum.Open)
            // .Select(a => a.CorrelationStore)
            // .ToListAsync();
            return await _dbContext.StoreRegion
                .Where(x => x.CompanyID == CompanyID && x.OEMID == OEMID && x.State == BaseModel.StateEnum.Open &&
                            x.IsDel == false).ToListAsync();
        }
    }

    public async Task<Model?> UpdateStoreById(Model cacheStore)
    {
        var model = await _dbContext.StoreRegion.Where(m => m.ID == cacheStore.ID).FirstOrDefaultAsync();
        if (model.IsNotNull())
        {
            model.NextEvaluate = cacheStore.NextEvaluate;
            model.PullProgress = cacheStore.PullProgress;
            model.FullCycle = cacheStore.FullCycle;
            model.PullCount = cacheStore.PullCount;
            model.LastPullStart = cacheStore.LastPullStart;
            model.QueryEndPayload = cacheStore.QueryEndPayload;
            model.QueryEnd = cacheStore.QueryEnd;
            model.LastDuration = cacheStore.LastDuration;
            model.IntervalSeconds = cacheStore.IntervalSeconds;
            model.NextPull = cacheStore.NextPull;
            model.LastOrderTime = cacheStore.LastOrderTime;
            model.NextCycleInfo = cacheStore.NextCycleInfo;
            return model;
        }

        return null;
    }

    public async Task<List<int>> GetStoreWithUserIds(int storeId)
    {
        return await _dbContext.UserStore
            .Where(m => m.CorrelationStoreID == storeId)
            .Select(m => m.CorrelationUserID)
            .ToListAsync();
    }
}