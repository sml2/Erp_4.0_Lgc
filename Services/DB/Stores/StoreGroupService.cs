﻿using ERP.Extensions;
using ERP.Interface;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using Model = ERP.Models.DB.Users.UserStore;

namespace ERP.Services.Stores;

using Data;
public class StoreGroupService : BaseService
{
    private readonly DBContext _dbContext;
    private readonly ISessionProvider _session;

    public StoreGroupService(DBContext myDbContext, ISessionProvider session)
    {
        _dbContext = myDbContext;
        _session = session;
    }
     
    //删除分配的用户组权限
    public async Task<ReturnStruct> delete(string where)
    {
        /*
        return StoreGroupModels::query()->where($where)->delete();
        */
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 查找被分配的用户组
    /// </summary>
    /// <param name="storeId"></param>
    /// <returns></returns>
    public async Task<List<int>> FindGroup(int storeId)
    {
        /*
        return StoreGroupModels::query()
            ->storeId($store_id)
            ->select('group_id')
            ->distinct('group_id')
            ->get()
            ->toArray();
        */
        // var s = _dbContext.EmailMasses.DeleteByKey();
        //Debug.Assert() EntityState.Modified.Equals(false);
        // Debug.Assert(context.Entry(newBlog).State == EntityState.Detached);

        // _dbContext.Entry<>.State = EntityState.Added;
        // Debug.Assert(context.Entry(newBlog).State == EntityState.Added);


        return await _dbContext.UserStore
            .WhenWhere(storeId > 0, m => m.CorrelationStoreID == storeId)
            .Select(m => m.CorrelationUserID)
            .Distinct()
            .ToListAsync();
    }

    //获取指定用户组下可查看店铺ids
    public async Task<ReturnStruct> getStoreIdsByGroupId(int group_id)
    {
        /*
        return StoreGroupModels::query()->GroupId($group_id)->select(['store_id'])->get()->toArray();
        */
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 获取指定用户组下的店铺ids(不带key)
    /// </summary>
    /// <param name="groupId"></param>
    /// <returns></returns>
    public async Task<List<int>> GetIdCollectByGroupId(int groupId)
    {
        /*
        return StoreGroupModels::query()->where('company_id',$this->getSession()->getCompanyId())->GroupId($group_id)->pluck('store_id');
        */
        //todo user session
        return await _dbContext.UserStore
            .Where(m => m.CompanyID == _session.Session!.GetCompanyID() && m.GroupID == groupId)
            .Select(m => m.CorrelationStoreID).ToListAsync();
    }
    
    public async Task<List<int>> GetIdCollectByUserId(int userId)
    {
        return await _dbContext.UserStore
            .Where(m => m.CompanyID == _session.Session!.GetCompanyID() 
                        && m.CorrelationUserID == userId)
            .Select(m => m.CorrelationStoreID).ToListAsync();
    }

    //更新用户组授权时删除已有授权信息
    public async Task<ReturnStruct> delAssign(string ids)
    {
        /*
        return StoreGroupModels::query()->storeId($ids)->delete();
        */
        return await Task.FromResult(ReturnArr());
    }
    /// <summary>
    /// 更新用户组授权时删除已有授权信息
    /// </summary>
    /// <param name="ID"></param> 
    /// <returns></returns>
    public async Task<bool> DelAssign(int ID)
    {
        var s = await _dbContext.UserStore.Where(x => x.CorrelationStoreID == ID).ToListAsync();
        if (s.Count <= 0)
        {
            return true;
        }

        foreach (var i in s)
        {
            _dbContext.Remove(i);
        }
        
        return await _dbContext.SaveChangesAsync() > 0;
    }

    //向用户组权限表中插入授权的数据
    public async Task<ReturnStruct> insert(Model[] list)
    {

        await _dbContext.UserStore.AddRangeAsync(list);
        await _dbContext.SaveChangesAsync();
        return ReturnArr(true);
    }

    /// <summary>
    /// 删除指定用户组被分配店铺关联信息
    /// </summary>
    /// <param name="groupId"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DelAllot(int groupId)
    {
        /*
        return StoreGroupModels::query()
            ->GroupId($GroupId)
            ->CompanyId($this->getSession()->getCompanyId())
            ->OemId($this->getSession()->getOemId())
            ->delete();
        */
        var s = await _dbContext.UserStore
            .Where(a => a.GroupID == groupId)
            .Where(a => a.CompanyID == _session.Session!.GetCompanyID())
            .Where(a => a.OEMID == _session.Session!.GetOEMID()).FirstOrDefaultAsync();
        if (s != null)
        {
            _dbContext.Remove(s);
            await _dbContext.SaveChangesAsync();
            return ReturnArr();
        }
        return ReturnArr(false);
    }






    //public function scopeStoreId($query, $store_id)
    //{
    //    if (!empty($store_id))
    //    {
    //        return $query->where('store_id', $store_id);
    //    }
    //}

    //public function scopeStoreIds($query, $store_id)
    //{
    //    if (!empty($store_id))
    //    {
    //        return $query->whereIn('store_id', $store_id);
    //    }
    //}

    //public function scopeGroupId($query, $group_id)
    //{
    //    //group_id = 0 判断分销没有分组的数据
    //    if ($group_id || $group_id === 0) {
    //        return $query->where('group_id', $group_id);
    //    }
    //}

    //public function scopeCompanyId($query, $company_id)
    //{
    //    if ($company_id) {
    //        return $query->where('company_id', $company_id);
    //    }
    //}

    //public function scopeOemId($query, $oem_id)
    //{
    //    if ($oem_id) {
    //        return $query->where('oem_id', $oem_id);
    //    }
    //}

}

