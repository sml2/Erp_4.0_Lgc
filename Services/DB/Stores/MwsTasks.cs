﻿namespace ERP.Services.Stores;

public class MwsTasks : BaseService
{
    public async Task<ReturnStruct> Info(/*$field = [],$id= '',$sid= '',$state= '',$product= '',$variant= '',$pricing= '',$inventory= '',$image= '',$relationship= ''*/)
    {
        /*
        empty($field) && $field = ["*"];
        $res =  MwsTasksModel::query()
            ->select($field)
            ->id($id)
            ->sid($sid)
            ->state($state)
            ->product($product)
            ->variant($variant)
            ->pricing($pricing)
            ->inventory($inventory)
            ->image($image)
            ->relationship($relationship);
        */
        return await Task.FromResult(ReturnArr());
    }
    public async Task<ReturnStruct> Get(string result)
    {
        /*
        return $result->get();
        */
        return await Task.FromResult(ReturnArr());
    }
    public async Task<ReturnStruct> First(string result)
    {
        /*
        return $result->first();
        */
        return await Task.FromResult(ReturnArr());
    }
    public async Task<ReturnStruct> insert(string data)
    {
        /*
        return MwsTasksModel::query()->create($data);
        */
        return await Task.FromResult(ReturnArr());
    }
    public async Task<ReturnStruct> update(/*$tasks_id,$updateData*/)
    {
        /*
        return MwsTasksModel::query()->id($tasks_id)->update($updateData);
        */
        return await Task.FromResult(ReturnArr());
    }
    public async Task<ReturnStruct> updateData(/*$where,$updateData*/)
    {
        /*
        return MwsTasksModel::query()->where($where)->update($updateData); 
        */
        return await Task.FromResult(ReturnArr());
    }








    //    /**
    //   * 相关任务
    //   * @return HasMany
    //   */
    //    public function task()
    //    {
    //        return $this->hasMany(MwsTask::class);

    //    }//end picks()

    //public function scopeId(Builder $query, $id)
    //{
    //    if ($id) {
    //        return $query->where('id', $id);
    //    }
    //}

    //public function scopeSId(Builder $query, $sid)
    //{
    //    if ($sid) {
    //        return $query->where('sid', $sid);
    //    }
    //}

    //public function scopeState(Builder $query, $state)
    //{
    //    if ($state > 0) {
    //        return $query->where('state', $state);
    //    }
    //}

    //public function scopeProduct(Builder $query, $product)
    //{
    //    if ($product) {
    //        return $query->where($this->transformKey('session_product')."->state",$product);
    //        //            return $query->where('session_product', 'like',"%\"state\": $product%");
    //    }
    //}

    //public function scopeVariant(Builder $query, $variant)
    //{
    //    if ($variant) {
    //        return $query->where($this->transformKey('session_variant')."->state",$variant);
    //    }
    //}

    //public function scopePricing(Builder $query, $pricing)
    //{
    //    if ($pricing) {
    //        return $query->where($this->transformKey('session_pricing')."->state",$pricing);
    //    }
    //}

    //public function scopeInventory(Builder $query, $inventory)
    //{
    //    if ($inventory) {
    //        return $query->where($this->transformKey('session_inventory')."->state",$inventory);
    //    }
    //}

    //public function scopeImage(Builder $query, $image)
    //{
    //    if ($image) {
    //        return $query->where($this->transformKey('session_image')."->state",$image);
    //    }
    //}

    //public function scopeRelationship(Builder $query, $relationship)
    //{
    //    if ($relationship) {
    //        return $query->where($this->transformKey('session_relationship')."->state",$relationship);
    //    }
    //}
}

