﻿using ERP.Data;
using ERP.Interface;

namespace ERP.Services.Stores;

public class SyncMws : BaseService
{
    private readonly DBContext _dbContext;
    private readonly ISessionProvider _ISessionProvider;
    public SyncMws(DBContext dbContext, ISessionProvider sessionProvider)
    {
        _dbContext=dbContext;
        _ISessionProvider=sessionProvider;
    }
    private ISession Session { get => _ISessionProvider.Session!; }
    //查看产品信息
    public async Task<ReturnStruct> productInfo(int spid)
    {
        /*
        $syncProduct = SyncProductModel::query()->where(
            [
                ['id', $spid],
                ['uploading', SyncProductModel::UPLOADING_FALSE],
            ]
        )->first();

        if (!$syncProduct) {
            return returnArr(false, getlang('store.syncmws.NoProduct'));
        }


        $skuChangeData = [];
        $mws = SyncMwsModel::query()
            ->where('spid', $spid)
            ->editable()
            ->get();
        if (!$mws->count()) {
            return returnArr(false, getlang('store.syncmws.NoTask'));
        }
        foreach ($mws as $item) {
            $skuChangeData[$item->language][] = [
                'id'              => $item->id,
                'sku_change_data' => $item->sku_change_data
            ];
        }

        $productInfo = $this->productService->info($syncProduct->pid);

        if (!$productInfo['state']) {
            return returnArr(false, $productInfo['msg']);
        }

        $result = [
            'Product'         => $productInfo['data']['info'],
            'languagePack'    => $productInfo['data']['languagePack'],
            'SkuChangeData'   => $skuChangeData,
            'NotEditSkuState' => $syncProduct->already_upload == SyncProductModel::ALREADY_UPLOAD_TRUE,
        ];
        return returnArr(true, '', $result);
        */
        return await Task.FromResult(ReturnArr());
    }

    //
    public async Task<ReturnStruct> editProduct(/*$spid, $product, $skuChangeData*/)
    {
        /*
        $spQuery = SyncProductModel::query()->where('id', $spid)->where('uploading', SyncProductModel::UPLOADING_FALSE);
        $syncProduct = $spQuery->first();

        if (!$syncProduct) {
            throw new LengthException(getlang('store.syncmws.NoProduct'));
        }

        $mws = SyncMwsModel::query()
            ->where('spid', $spid)
            ->editable()
            ->get();
        if (!$mws->count()) {
            throw new LengthException(getlang('store.syncmws.NoTask'));
        }
        $skuChangeDataCount = count(array_reduce($skuChangeData, 'array_merge', []));
        if ($mws->count() !== $skuChangeDataCount) {
            throw new LengthException(getlang('store.syncmws.NoEdit'));
        }
        // 后置修改
        // $newMws = arrayCombine($mws->toArray(), 'language');
        $firstImageUrl = isset($product['images'][0]) ? $product['images'][0]['path'] : '';
        if (substr($firstImageUrl, 0, 5) == 'image') {
            $path = explode('/', $firstImageUrl);
            $firstImageUrl = base64_decode(urldecode($path[2]));
        }
        foreach ($skuChangeData as $lang => $mwsData) {
            foreach ($mwsData as $finalNode) {
                $sku_change_data = self::clearSkuChangeDataErrorMsg($finalNode['sku_change_data']);
                $count = SyncMwsModel::query()->where('id', $finalNode['id'])->editable()
                    ->update([
//                        'sku_change_data' => json_encode($finalNode['sku_change_data']),
                        'sku_change_data' => json_encode($sku_change_data),
                        'product_name'    => $product['languages'][$lang]['n'],
                        'product'         => json_encode(['image' => $firstImageUrl, 'id' => $product['id']]),
                        'upload_id'       => null,
                        'state'           => SyncMwsModel::STATE_WAITING
                    ]);
                if ($count === 0) {
                    throw new LengthException(getlang('store.syncmws.UploadTaskFailed'));
                }
            }
        }
        $count = $spQuery->update([
            'product_name'    => $product['languages']['default']['n'] ?? $product['lp_n'],
            'img_url'         => $firstImageUrl,
            'category_id'     => intval($product['category_id']),
            'sub_category_id' => intval($product['sub_category_id']),
        ]);

        if ($count === 0) {
            throw new LengthException(getlang('store.syncmws.UploadProductFailed'));
        }
        $res = $this->productService->edit($product, true);

        if (!$res['state']) {
            throw new LengthException($res['msg']);
        }
        // 处理EanUpcStatus字段
        if (isset($product['variants']) && count($product['variants']) > 0) {
            $variants = $product['variants'];
            $coded = [];
            foreach ($variants as $k => $v) {
                if (isset($v['code']) && !empty($v['code'])) {
                    $coded[] = $k;
                }
            }
            if (count($coded) === count($product['variants'])) {
                (new SyncProduct())->updateEanupcStatus($product['id'], SyncProductModel::CODE_ALL);
            } else if (count($coded) === 0) {
                (new SyncProduct())->updateEanupcStatus($product['id'], SyncProductModel::CODE_NULL);
            } else if (count($coded) < count($product['variants'])){
                (new SyncProduct())->updateEanupcStatus($product['id'], SyncProductModel::CODE_PART);
            } else {
                (new SyncProduct())->updateEanupcStatus($product['id'], SyncProductModel::CODE_NULL);
            }
        }
        return await Task.FromResult(ReturnArr(true));
        */
        return await Task.FromResult(ReturnArr(true));
    }

    //分页列表数据
    public async Task<ReturnStruct> list(string input)
    {
        /*
        $storeIds = array_column($this->getStores(), 'id');
        $list = SyncMwsModel::query()->whereIn('store_id', $storeIds)
            ->StoreId(($input['store_id'] ?? null))
            ->productName(($input['product_name'] ?? null))
            ->uiState(($input['state'] ?? null))
            ->language(($input['language'] ?? null))
            ->orderByDesc('updated_at')
            ->paginate($this->limit);
        $ids = array_unique(array_column(collect($list->items())->toArray(), 'spid'));
        if (count($ids) === 0) {
            return $list;
        }
        $sProducts = SyncProductModel::query()->whereIn('id', $ids)->select(['id', 'uploading', 'pid'])->get()->toArray();
        $sProducts = arrayCombine($sProducts, 'id');
        // FIXME sp删除mws没有清除关联会报index错误
        foreach ($list->items() as $item) {
            if (isset($sProducts[$item->spid])) {
                $item->uploading = $sProducts[$item->spid]['uploading'] === SyncProductModel::UPLOADING_TRUE;
                $item->pid = $sProducts[$item->spid]['pid'];
            } else {
                Log::error(
                    'sp删除mws没有清除关联会报index错误',
                    [
                        'route'    => request()->getRequestUri() ?? '',
                        'errorMsg' => 'spid:' . $item->spid . ' no exists.mws_id: [' . $item->id . ']',
                        'codeLine' => 'Line207'
                    ]
                );
            }
        }
        return $list;
        */
        return await Task.FromResult(ReturnArr(true));
    }

    //根据spid获取mws列表
    public async Task<ReturnStruct> getListBySpid(int spid)
    {
        /*
        $sp = SyncProductModel::query()->where('uploading', SyncProductModel::UPLOADING_FALSE)->find($spid);
        if (!$sp) {
            return returnArr(false, getlang('store.syncmws.NoProduct'));
        }
        $stores = $this->getStores();
        $storeIds = array_column($stores, 'id');

        $list = SyncMwsModel::query()->whereIn('store_id', $storeIds)
            ->whereSpid($spid)
            ->editable()
            ->select(['id', 'store_name', 'store_id', 'sku_change_data', 'spid', 'language', 'browser_node_id'])
            ->get();
        if (!$list->count()) {
            return returnArr(false, getlang('store.syncmws.NoTask'));
        }
        $languages = Cache::instance()->language()->get();

        // FIXME sp删除mws没有清除关联会报index错误
        foreach ($list as $item) {
            $item->lang_name = isset($languages[$item->language]) ? $languages[$item->language]['name'] : '默认';
            $item->seller_id = $stores[$item->store_id]['seller_id'] ?? '';
        }
        return returnArr(true, '', $list);
        */
        return await Task.FromResult(ReturnArr(true));
    }


    //添加
    public async Task<ReturnStruct> add(/*array $input*/)
    {
        /*
        $myStores = $this->getStores();
        foreach ($input['stores'] as $item) {
            if (!isset($item['store_id']) || !isset($item['language']) || !isset($item['browser_node_id']) || !isset($item['country_of_origin'])) {
                return returnArr(false, getlang('public.ServerMissingParam'));
            }

            if (!isset($myStores[$item['store_id']])) {
                return returnArr(false, getlang('store.syncmwa.StoreNotExist'));
            }
        }
        //查询sync_product表是否有相同数据
        $sku_suffix = $input['sku_suffix'] ? SyncProductModel::SKU_SUFFIX_TRUE : SyncProductModel::SKU_SUFFIX_FALSE;
        $title_suffix = $input['title_suffix'] ? SyncProductModel::TITLE_SUFFIX_TRUE : SyncProductModel::TITLE_SUFFIX_FALSE;
        $same = (new SyncProduct())->checkdata($input['spid']);
//        dd($same,$input);

        if (!$same) {
            $spid = [];
            //不存在->添加
            $store_list = $input['stores'];
            foreach($store_list as $store_list_key => $store_list_value) {

                $res = $this->productService->copyProduct($input['goods_id'], 'default', ($input['source'] ?? 1));

                if (!$res['state']) {
                    return returnArr(false, $res['msg']);
                }

                if (!$res['state']) {
                    return returnArr(false, $res['msg']);
                }
                $spid_temp = (new SyncProduct())->insertSyncProduct(
                    $res['data']['info'],
                    $res['data']['source_id'],
                    $input['sku'],
                    $input['sku_type'],
                    SyncProductModel::PRODUCT_TYPE_ONE,
                    $input['types'],
                    $input['type_data_id'],
                    $sku_suffix,
                    $title_suffix
                );
                array_push($spid, $spid_temp);
                $var = json_decode($res['data']['info']['variants'], true);
            }
            $this->insertSyncMws($input,$res,$myStores,$spid,$var);//添加的时候所有的变体都是一样的，并不需要特殊处理。直接取循环的最后一个来用.
            return returnArr(true, $res['msg']);
        } else {
            //存在->查询条件
            $res['data'] = [
                'pid'       => $same['pid'],
                'source_id' => $same['source_id'],
                'info'      => ProductModels::query()->Id($same['pid'])->first()
            ];
            $res['msg'] = '';
            $spid = $same['id'];
            $var = $res['data']['info']['variants'];
            // @todo: 更新产品分类
            $same->type_data = $input['types'];
            $same->sku = $input['sku'];
            $same->sku_type = $input['sku_type'];
            $same->sku_suffix = $sku_suffix;
            $same->title_suffix = $title_suffix;
            $typeBiter = new Type($same['product_type']);
            $typeBiter->setUpload(true);
            $same->product_type = $typeBiter->getFlags();
            $same->save();
            if($this->insertSyncMws($input,$res,$myStores,$spid,$var)){
                return returnArr(true, $res['msg']);
            }else{
                return returnArr(false, '操作失败！！！');
            }
        }
        */
        return await Task.FromResult(ReturnArr(true));
    }

    //
    private async Task<ReturnStruct> insertSyncMws(/*$input,$res,$myStores,$spid,$var*/)
    {
        /*
        $insertData = [];

        foreach ($input['stores'] as $key => $item) {

            $product = [
                'image' => $res['data']['info']['image'],
                'id'    => $res['data']['info']['id']
            ];
            //通过类型来判断是什么操作，从而获取正确的spid
            if(is_array($spid)){
                $spid_insert = $spid[$key];
            }else{
                $spid_insert = $spid;
            }
            $insertData[] = [
                'store_id'        => $item['store_id'],
                'store_name'      => $myStores[$item['store_id']]['name'],
                'spid'            => $spid_insert,
                'language'        => $item['language'],
                'product_name'    => $res['data']['info']['lp_n'],
                'product'         => json_encode($product),
                'company_id'      => $myStores[$item['store_id']]['company_id'],
                'oem_id'          => $this->getSession()->getOemId(),
                'user_id'         => $this->getSession()->getUserId(),
                'group_id'        => $this->getSession()->getGroupId(),
                'browser_node_id' => $item['browser_node_id'] ?? '',
                'country_of_origin' => $item['country_of_origin'] ?? 'unknown',
                'state'           => SyncMwsModel::STATE_WAITING,
                'lead_time'       => $input['lead_time'],
                'sku_change_data' => json_encode($this->initChangeData($var, $input['sku'])),
                'created_at'      => date('Y-m-d H:i:s', request()->input('time')),
                'updated_at'      => date('Y-m-d H:i:s', request()->input('time')),
            ];

        }//end foreach

        return SyncMwsModel::query()->insert($insertData)==true?'true':'false';
        */
        return await Task.FromResult(ReturnArr(true));
    }

    //初始化sku_change_data,状态全为5
    private async Task<ReturnStruct> initChangeData(/*$variants, $sku*/)
    {
        /*
        $data = [];
        $initState = SyncMwsModel::UPLOAD_UPLOAD_WAITING;

        array_push($data, [
            'Sku'         => $sku,
            'IsMain'      => true,
            'Asin'        => null,
            'ProductData' => ['Action' => $initState, 'Error' => null]
        ]);

        foreach ($variants as $k => $variant) {
            if ($variant['state'] == 1) {//过滤禁用
                array_push($data, [
                    'Sku'              => $k,
                    'IsMain'           => false,
                    'IsDelete'         => false,
                    'Asin'             => null,
                    'VariantData'      => ['Action' => $initState, 'Error' => null],
                    'PricingData'      => ['Action' => $initState, 'Error' => null],
                    'InventoryData'    => ['Action' => $initState, 'Error' => null],
                    'ImageData'        => ['Action' => $initState, 'Error' => null],
                    'RelationshipData' => ['Action' => $initState, 'Error' => null]
                ]);
            }
        }
        return $data;
        */
        return await Task.FromResult(ReturnArr(true));
    }

    //重新分配eanupc重置sku_change_data,状态全为5
    public async Task<ReturnStruct> restoreChangeData(string skuChangeData)
    {
        /*
        $data = [];
        $initState = SyncMwsModel::UPLOAD_UPLOAD_WAITING;
        foreach ($skuChangeData as $item) {
            if ($item['IsMain']) {
                // TODO: 根据主产品是否存在asin判断修改状态为待上传（可能存在问题）
                $state = empty($item['ProductData']['Asin']) ? $initState : $item['ProductData']['Action'];
                $item['ProductData'] = ['Action' => $state, 'Error' => null];
                unset($state);
            } else {
                $item = array_merge($item, [
                    'VariantData'      => ['Action' => $initState, 'Error' => null],
                    'PricingData'      => ['Action' => $initState, 'Error' => null],
                    'InventoryData'    => ['Action' => $initState, 'Error' => null],
                    'ImageData'        => ['Action' => $initState, 'Error' => null],
                    'RelationshipData' => ['Action' => $initState, 'Error' => null]
                ]);
            }
            array_push($data, $item);
        }
        return $data;
        */
        return await Task.FromResult(ReturnArr(true));
    }

    //编辑mws信息
    public async Task<ReturnStruct> edit(string input)
    {
        /*
        $storeIds = array_column($this->getStores(), 'id');
        $query = SyncMwsModel::query()->where('id', $input['id'])->whereIn('store_id', $storeIds)->editable();
        $row = $query->first();
//        dd($row['sku_change_data']);
        $sku_change_data = self::clearSkuChangeDataErrorMsg($row['sku_change_data']);
        if (!$row) {
            return returnArr(false, getlang('store.syncmws.NoInfo'));
        }
        $updateData = [
            'language'        => $input['language'],
            'browser_node_id' => ($input['browser_node_id'] ?? ''),
            'lead_time'       => $input['lead_time'],
            // 修改后将状态改为待执行.
            'state'           => SyncMwsModel::STATE_WAITING,
            'sku_change_data' => json_encode($sku_change_data),
            'upload_id'       => null
        ];

        $query->update($updateData);
        return await Task.FromResult(ReturnArr(true));
        */
        return await Task.FromResult(ReturnArr(true));
    }

    //切换mws状态0<=>1
    public async Task<ReturnStruct> switchState(/*$id, $state*/)
    {
        /*
        $allowStates = [SyncMwsModel::STATE_WAITING_SYNC, SyncMwsModel::STATE_WAITING];
        if (!in_array($state, $allowStates)) {
            return returnArr(false, getlang('store.syncmws.NoModify'));
        }

        $nowState = $state === SyncMwsModel::STATE_WAITING_SYNC ? SyncMwsModel::STATE_WAITING : SyncMwsModel::STATE_WAITING_SYNC;
        $storeIds = array_column($this->getStores(), 'id');
        SyncMwsModel::query()->where('id', $id)->whereIn('store_id', $storeIds)->where('state', $nowState)->update(['state' => $state]);
        return await Task.FromResult(ReturnArr(true));
        */
        return await Task.FromResult(ReturnArr(true));
    }

    //获取店铺
    public async Task<ReturnStruct> getStores()
    {
        

        /*
        $session = $this->getSession();
        if ($session->getConcierge() == UserModel::TYPE_OWN) {
            $stores = $this->getCompanyStore($session->getCompanyId());
        } else {
            $stores = Cache::instance()->storePersonal()->get($session->getCompanyId());
        }

        if (checkArr($stores)) {
            return array_filter($stores, fn($item) => $item['type'] === 1);
        }

        return [];
        */
        return await Task.FromResult(ReturnArr(true));
    }

    //
    public async Task<ReturnStruct> Info(/*$field = [], $id = '', $store_id = '', $upload_id = '', $state = '', $spid = ''*/)
    {
        /*
        empty($field) && $field = ["*"];
        return SyncMwsModel::query()
            ->Select($field)
            ->Id($id)
            ->StoreId($store_id)
            ->UploadId($upload_id)
            ->State($state)
            ->Spid($spid);
        */
        return await Task.FromResult(ReturnArr());
    }

    //
    public async Task<ReturnStruct> updateData(/*$where, $update_data*/)
    {
        /*
        return SyncMwsModel::query()->where($where)->update($update_data);
        */
        return await Task.FromResult(ReturnArr());
    }

    //查找不能删除的任务
    public async Task<ReturnStruct> uploadingProduct(int spid)
    {
        /*
        return SyncMwsModel::query()
            ->where('spid', $spid)
            ->editable()
            ->select()
            ->get();
        */
        return await Task.FromResult(ReturnArr());
    }

    //通过spid删除
    public async Task<ReturnStruct> deleteByspid(int id)
    {
        /*
        return SyncMwsModel::query()
            ->where('spid', $id)
            ->editable()
            ->delete();
        */
        return await Task.FromResult(ReturnArr());
    }

    //获取产品绑定店铺数量
    public async Task<ReturnStruct> getMwsNum(int spid)
    {
        /*
        return SyncMwsModel::query()->where('spid',$spid)->count();
        */
        return await Task.FromResult(ReturnArr());
    }

    //获取mws详情
    public async Task<ReturnStruct> getInfoById(int id)
    {
        /*
        return SyncMwsModel::query()
            ->companyId($this->getSession()->getCompanyId())
            ->oemId($this->getSession()->getOemId())
            ->groupId($this->getSession()->getGroupId())
            ->find($id);
        */
        return await Task.FromResult(ReturnArr(true));
    }

    //
    public async Task<ReturnStruct> getProductInfo(/*$id, $language = ''*/)
    {
        /*
        // TODO: 使用value直接获取单个字段
        $productId = SyncProductModel::query()->find($id)['pid'];
        return $productInfo = $this->productService->info($productId);
        */
        return await Task.FromResult(ReturnArr());
    }

    //
    public async Task<ReturnStruct> clearSkuChangeDataErrorMsg(/*$old_sku_change_datas*/)
    {
        /*
        foreach ($old_sku_change_datas as &$new_sku_change_data){
                if($new_sku_change_data["IsMain"] === true){
                    $new_sku_change_data["ProductData"]["Error"] = null;
                }else{
                    $new_sku_change_data["ImageData"]["Error"] = null;
                    $new_sku_change_data["PricingData"]["Error"] = null;
                    $new_sku_change_data["VariantData"]["Error"] = null;
                    $new_sku_change_data["InventoryData"]["Error"] = null;
                    $new_sku_change_data["RelationshipData"]["Error"] = null;
                }
        }
        return $old_sku_change_datas;
        */
        return await Task.FromResult(ReturnArr());
    }

















    //    public function upload()
    //    {
    //        return $this->belongsTo(SyncUpload::class, 'upload_id');
    //    }

    //public function scopeId(Builder $query, $id)
    //{
    //    if ($id) {
    //        if (!is_array($id))
    //        {
    //            return $query->where('id', $id);
    //        }
    //        else
    //        {
    //            return $query->whereIn('id', $id);
    //        }
    //    }
    //    return $query;
    //}//end scopeId()


    //public function scopeStoreId(Builder $query, $storeId)
    //{
    //    if ($storeId) {
    //        if (!is_array($storeId))
    //        {
    //            return $query->where('store_id', $storeId);
    //        }
    //        else
    //        {
    //            return $query->whereIn('store_id', $storeId);
    //        }
    //    }
    //    return $query;
    //}//end scopeStoreId()

    //public function scopeSpid(Builder $query, $spid)
    //{
    //    if ($spid) {
    //        return $query->whereIn('spid', $spid);
    //    }
    //    return $query;
    //}//end scopeSpid()

    //public function scopeUploadId(Builder $query, $uploadId)
    //{
    //    if ($uploadId === null || $uploadId) {
    //        return $query->where('upload_id', $uploadId);
    //    }
    //    return $query;
    //}//end scopeTasksId()

    //public function scopeState(Builder $query, $state)
    //{
    //    if ($state) {
    //        return $query->where('state', $state);
    //    }
    //    return $query;
    //}

    //public function scopeUiState(Builder $query, $state)
    //{
    //    if ($state) {
    //        if ($state == 1) {
    //            return $query->whereIn('state', [self::STATE_WAITING_SYNC, self::STATE_WAITING])->where('eanupc_status', 2);
    //        }
    //        return $query->where('state', $state);
    //    }
    //    return $query;
    //}

    //public function scopeProductName(Builder $query, $name)
    //{
    //    if ($name) {
    //        return $query->where('product_name', 'like', '%'. $name. '%');
    //    }

    //    return $query;
    //}//end scopeProductName()

    //public function scopeLanguage(Builder $query, $value)
    //{
    //    if ($value) {
    //        return $query->where('language', $value);
    //    }

    //    return $query;
    //}//end scopeProductLanguage()

    //public function scopeEditable(Builder $query)
    //{
    //    return $query->whereIn('state', self::SURE_EDIT);
    //}//end scopeProductLanguage()


    //public function getProductAttribute()
    //{
    //    if (isset($this->attributes['product']))
    //    {
    //            $product = json_decode($this->attributes['product'], true);
    //        if (!isset($product['id']))
    //        {
    //            if (substr($product['image'], 0, 4) !== 'http')
    //            {
    //                    $product['image'] = env('OSS_DOMAIN', 'http://img3.sou1pen.com/'). $product['image'];
    //            }
    //        }
    //        else
    //        {
    //                $product['image'] = $this->encrypImageSrc($product['image'],$product['id']);
    //        }
    //        return $product;
    //    }
    //    return null;
    //}

    //public function getSkuChangeDataAttribute()
    //{
    //    if (isset($this->attributes['sku_change_data']))
    //    {
    //        return json_decode($this->attributes['sku_change_data'], true);
    //    }
    //    return null;
    //}

    //public function scopeOemId(Builder $query, $oem_id)
    //{
    //    if ($oem_id) {
    //        return $query->where('oem_id', '=', $oem_id);
    //    }
    //    return $query;
    //}

    //public function scopeUserId(Builder $query, $user_id)
    //{
    //    if ($user_id) {
    //        return $query->where('user_id', '=', $user_id);
    //    }
    //    return $query;
    //}

    //public function scopeGroupId(Builder $query, $group_id)
    //{
    //    if ($group_id) {
    //        return $query->where('group_id', '=', $group_id);
    //    }
    //    return $query;
    //}

    //public function scopeCompanyId(Builder $query, $company_id)
    //{
    //    if ($company_id) {
    //        return $query->where('company_id', '=', $company_id);
    //    }
    //    return $query;
    //}

    //public function scopeEanupcStatus(Builder $query, $eanupc_status)
    //{
    //    //为0时需要单独写逻辑判断
    //    if ($eanupc_status) {
    //        return $query->where('eanupc_status', '=', $eanupc_status);
    //    }
    //    return $query;
    //}

    //public function syncUpload()
    //{
    //    return $this->hasOne(SyncUpload::class, 'id', 'upload_id');
    //    }

    //    public function syncProduct()
    //{
    //    return $this->hasOne(SyncProduct::class, 'id', 'spid');
    //    }//    public function upload()
    //    {
    //        return $this->belongsTo(SyncUpload::class, 'upload_id');
    //    }

    //public function scopeId(Builder $query, $id)
    //{
    //    if ($id) {
    //        if (!is_array($id))
    //        {
    //            return $query->where('id', $id);
    //        }
    //        else
    //        {
    //            return $query->whereIn('id', $id);
    //        }
    //    }
    //    return $query;
    //}//end scopeId()


    //public function scopeStoreId(Builder $query, $storeId)
    //{
    //    if ($storeId) {
    //        if (!is_array($storeId))
    //        {
    //            return $query->where('store_id', $storeId);
    //        }
    //        else
    //        {
    //            return $query->whereIn('store_id', $storeId);
    //        }
    //    }
    //    return $query;
    //}//end scopeStoreId()

    //public function scopeSpid(Builder $query, $spid)
    //{
    //    if ($spid) {
    //        return $query->whereIn('spid', $spid);
    //    }
    //    return $query;
    //}//end scopeSpid()

    //public function scopeUploadId(Builder $query, $uploadId)
    //{
    //    if ($uploadId === null || $uploadId) {
    //        return $query->where('upload_id', $uploadId);
    //    }
    //    return $query;
    //}//end scopeTasksId()

    //public function scopeState(Builder $query, $state)
    //{
    //    if ($state) {
    //        return $query->where('state', $state);
    //    }
    //    return $query;
    //}

    //public function scopeUiState(Builder $query, $state)
    //{
    //    if ($state) {
    //        if ($state == 1) {
    //            return $query->whereIn('state', [self::STATE_WAITING_SYNC, self::STATE_WAITING])->where('eanupc_status', 2);
    //        }
    //        return $query->where('state', $state);
    //    }
    //    return $query;
    //}

    //public function scopeProductName(Builder $query, $name)
    //{
    //    if ($name) {
    //        return $query->where('product_name', 'like', '%'. $name. '%');
    //    }

    //    return $query;
    //}//end scopeProductName()

    //public function scopeLanguage(Builder $query, $value)
    //{
    //    if ($value) {
    //        return $query->where('language', $value);
    //    }

    //    return $query;
    //}//end scopeProductLanguage()

    //public function scopeEditable(Builder $query)
    //{
    //    return $query->whereIn('state', self::SURE_EDIT);
    //}//end scopeProductLanguage()


    //public function getProductAttribute()
    //{
    //    if (isset($this->attributes['product']))
    //    {
    //            $product = json_decode($this->attributes['product'], true);
    //        if (!isset($product['id']))
    //        {
    //            if (substr($product['image'], 0, 4) !== 'http')
    //            {
    //                    $product['image'] = env('OSS_DOMAIN', 'http://img3.sou1pen.com/'). $product['image'];
    //            }
    //        }
    //        else
    //        {
    //                $product['image'] = $this->encrypImageSrc($product['image'],$product['id']);
    //        }
    //        return $product;
    //    }
    //    return null;
    //}

    //public function getSkuChangeDataAttribute()
    //{
    //    if (isset($this->attributes['sku_change_data']))
    //    {
    //        return json_decode($this->attributes['sku_change_data'], true);
    //    }
    //    return null;
    //}

    //public function scopeOemId(Builder $query, $oem_id)
    //{
    //    if ($oem_id) {
    //        return $query->where('oem_id', '=', $oem_id);
    //    }
    //    return $query;
    //}

    //public function scopeUserId(Builder $query, $user_id)
    //{
    //    if ($user_id) {
    //        return $query->where('user_id', '=', $user_id);
    //    }
    //    return $query;
    //}

    //public function scopeGroupId(Builder $query, $group_id)
    //{
    //    if ($group_id) {
    //        return $query->where('group_id', '=', $group_id);
    //    }
    //    return $query;
    //}

    //public function scopeCompanyId(Builder $query, $company_id)
    //{
    //    if ($company_id) {
    //        return $query->where('company_id', '=', $company_id);
    //    }
    //    return $query;
    //}

    //public function scopeEanupcStatus(Builder $query, $eanupc_status)
    //{
    //    //为0时需要单独写逻辑判断
    //    if ($eanupc_status) {
    //        return $query->where('eanupc_status', '=', $eanupc_status);
    //    }
    //    return $query;
    //}

    //public function syncUpload()
    //{
    //    return $this->hasOne(SyncUpload::class, 'id', 'upload_id');
    //    }

    //    public function syncProduct()
    //{
    //    return $this->hasOne(SyncProduct::class, 'id', 'spid');
    //    }
}

