using ERP.Data;
using ERP.DomainEvents;
using ERP.Extensions;
using ERP.Interface;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Model =ERP.Models.DB.Orders.Order;

using AsinModel = ERP.Models.Stores.AsinProduct;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using NPOI.SS.Formula.Functions;
using NUnit.Framework;
using Microsoft.CodeAnalysis.VisualBasic.Syntax;
using ERP.Services.Host;
using ERP.Models.DB.Product;

namespace ERP.Services.DB.Stores
{
    public class AsinProduct : BaseService
    {
        public readonly DBContext dbContext;
        private readonly ISessionProvider SessionProvider;
        private readonly IMediator mediator;
        protected readonly ILogger<AsinProduct> logger;

        public AsinProduct(DBContext _dbContext, ISessionProvider _SessionProvider, IMediator _mediator, ILogger<AsinProduct> _logger)
        {
            dbContext = _dbContext;
            logger = _logger;
            SessionProvider = _SessionProvider;
            mediator = _mediator;
        }

        private List<AsinModel> asinList = new();

        public async Task<bool> Add(List<AsinInfo> asininfos, int storeId, string sellerID, bool IsMainSku = false) => await AddInternal(asininfos, storeId, sellerID);

        private async Task<bool> AddInternal(List<AsinInfo> asins, int storeId, string sellerID,bool IsMainSku=false)
        {
            List<AsinModel> addList = new();
            List<AsinModel> updateList = new();
            try
            {
                if (asins.Count() > 0)
                {
                    foreach (var model in asins)
                    {
                        var result = await AddInternal(new AsinModel(sellerID, model.SellerSku, model.Asin, storeId, IsMainSku));
                        if (result.Item1)
                        {
                            updateList.Add(result.Item2);
                        }
                        else
                        {
                            addList.Add(result.Item2);
                        }
                    }

                    if (addList.Count > 0)
                    {
                        await dbContext.AsinProduct.AddRangeAsync(addList);
                    }

                    if (updateList.Count > 0)
                    {
                        dbContext.AsinProduct.UpdateRange(updateList);
                    }
                }
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }


        private async Task<Tuple<bool, AsinModel>> AddInternal(AsinModel model)
        {
            Tuple<bool, AsinModel> result;
            var Source = await dbContext.AsinProduct.FirstOrDefaultAsync(a => a.PidHashCode == model.PidHashCode);
            if (Source is null)
            {
                result = new(false, model);
            }
            else
            {
                Source.Pull = model.Pull;//false
                result = new(true, Source);
            }
            return result;
        }

        //public async Task<bool> Update(List<Model> models) => await UpdateInternal(models);


        /// <summary>
        /// 从order中更新
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        //private async Task<bool> UpdateInternal(List<Model> models)
        //{
        //    GetAsinList(models);
        //    if (this.asinList.Count > 0)
        //    {
        //        foreach (var model in this.asinList)
        //        {

        //            if (await UpdateInternal(model, DateTime.MinValue))
        //                continue;
        //            else
        //                return false;

        //        }
        //    }
        //    return true;
        //}

        //private void GetAsinList(List<Model>? models)
        //{
        //    var listAsin = new List<AsinModel>();
        //    foreach (var model in models)
        //    {
        //        foreach (var m in model.GoodsScope())
        //        {
        //            if (!listAsin.Any(x => x.Asin == m.ID))
        //            {
        //                listAsin.Add(new AsinModel(model.Store!.GetSPAmazon()!.SellerID, m.Sku, m.ID, model.Store.ID, m.ImageURL == "" ? false : true));  //如果连图都没有，目前估计是下架产品
        //            }
        //        }
        //    }
        //    this.asinList = listAsin.Distinct(x => x.PidHashCode).ToList();
        //}

        //private async Task<bool> UpdateInternal(AsinModel asin, DateTime dateTime)
        //{
        //    var Source = await dbContext.AsinProduct.FirstOrDefaultAsync(a => a.PidHashCode == asin.PidHashCode);
        //    if (Source is not null)
        //    {
        //        Source.SyncTime = dateTime;
        //        Source.IsActive = asin.IsActive;
        //        Source.Brand = asin.Brand;
        //        Source.Color = asin.Color;
        //        Source.ImageUrl = asin.ImageUrl;
        //        Source.Size = asin.Size;
        //        return true;
        //    }
        //    return false;
        //}


        private async Task<AsinModel?> UpdateInternalModel(AsinModel asin, DateTime dateTime)
        {           
            var Source = await dbContext.AsinProduct.FirstOrDefaultAsync(a => a.PidHashCode == asin.PidHashCode);
            if (Source is not null)
            {
                Source.SyncTime = dateTime;
                Source.Pull = true;
                Source.SyncTime = dateTime;
                Source.IsActive = asin.IsActive;
                Source.Brand = asin.Brand;
                Source.Color = asin.Color;
                Source.ImageUrl = asin.ImageUrl;
                Source.Size = asin.Size;
               return Source;
            }
            return null;
        }


        /// <summary>
        /// 从asin中更新
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        public async Task<bool> UpdateAsin(List<AsinModel> models) => await UpdateInternal(models);

        private async Task<bool> UpdateInternal(List<AsinModel> models)
        {
            List<AsinModel> updateList = new();
            try
            {
                foreach (var model in models)
                {
                    var result = await UpdateInternalModel(model, DateTime.Now);
                    if (result is not null)
                    {
                        updateList.Add(result);
                    }
                    else
                    {
                        logger.LogError($"asinproduct UpdateInternal asin hashvalue:{model.ToString()} asin Not Found in asinproductTable");
                    }
                }
                if (updateList.Count > 0)
                {
                    dbContext.AsinProduct.UpdateRange(updateList);
                }
                return true;
            }
            catch (Exception e)
            {
                logger.LogError($"AsinProduct UpdateInternal error,asinHash:{JsonConvert.SerializeObject(models)}");
                logger.LogError(e, $"AsinProduct UpdateInternal:{e.Message}");
                return false;
            }
        }

        public async Task<AsinModel?> Get(string sellerID,string sku,int storeID)
        {
            ulong pidHashCode = Helpers.CreateHashCode($"{sellerID}{sku}");
            return await dbContext.AsinProduct.FirstOrDefaultAsync(x => x.PidHashCode == pidHashCode && x.StoreId== storeID);
        }

        public async Task<AsinModel?> Get(ulong hashcode, int storeID)
        {          
            return await dbContext.AsinProduct.FirstOrDefaultAsync(x => x.PidHashCode == hashcode && x.StoreId == storeID);
        }

        public async Task<bool> Update(List<AsinModel> list)
        {
            List<AsinModel> updateLst = new();
            List<AsinModel> insertLst = new();
            try
            {
                foreach (var m in list)
                {
                    var model = await Get(m.PidHashCode,m.StoreId);
                    if (model != null)
                    {
                        model.Brand = m.Brand;
                        model.Color = m.Color;
                        model.Size = m.Size;
                        model.ImageUrl = m.ImageUrl;
                        model.LanguageTag = m.LanguageTag;
                        updateLst.Add(model);
                        model.SyncTime = DateTime.Now;
                    }
                    else
                    {
                        insertLst.Add(m);
                    }
                }

                if (updateLst.Count > 0)
                {
                    dbContext.AsinProduct.UpdateRange(updateLst);
                }

                if (insertLst.Count > 0)
                {
                    dbContext.AsinProduct.AddRange(insertLst);
                }
                return true;
            }
            catch (Exception e)
            {
                logger.LogError($"asin Update error:{e.Message}");
                logger.LogError($"asin Update error:{e.StackTrace}");
                return false;
            }                     
        }
    }
}
