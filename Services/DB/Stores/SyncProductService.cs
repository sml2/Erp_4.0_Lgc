using ERP.Data;
using ERP.Data.Products;
using ERP.Enums.Rule.Config.Internal;
using ERP.Exceptions.Export;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.Biter;
using ERP.Models.DB.Identity;
using ERP.Models.DB.Product;
using ERP.Models.DB.Stores;
using ERP.Models.Stores;
using ERP.Models.View.Products.Export;
using ERP.Services.Export;
using ERP.Services.Product;
using ERP.Services.Upload;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Z.EntityFramework.Plus;
using NotFoundException = ERP.Exceptions.SyncProduct.NotFoundException;
using AsinProduct = ERP.Services.DB.Stores.AsinProduct;
using ModelProduct = ERP.Data.Products.Product;
using ImagesStruct = ERP.Data.Products.Images;

namespace ERP.Services.Stores;

public class SyncProductService : BaseService
{
    private readonly ISessionProvider _sessionProvider;
    private readonly SyncExportService _syncExportService;
    private readonly SyncExportProductService _syncExportProductService;
    private readonly ProductService _productService;
    protected readonly DBContext _dbContext;

    public SyncProductService(ISessionProvider sessionProvider, DBContext dbContext,
        SyncExportService syncExportService, ProductService productService, SyncExportProductService syncExportProductService)
    {
        _sessionProvider = sessionProvider;
        _dbContext = dbContext;
        _syncExportService = syncExportService;
        _productService = productService;
        _syncExportProductService = syncExportProductService;
    }

    private ISession _session => _sessionProvider.Session;

    private int _userId => _session.GetUserID();

    private int _groupId => _session.GetGroupID();

    private int _companyId => _session.GetCompanyID();

    private int _oemId => _session.GetOEMID();

    public async Task<DbSetExtension.PaginateStruct<SyncProductModel>> List(SyncProductIndexDto req,
        List<int>? ids = null,  DataRange_2b? range = null)
    {
        return await _dbContext.SyncProduct.WhereRange(range, _companyId, _groupId, _userId)
            .WhenWhere(req.ProductName is not null, m => m.ProductName!.Contains(req.ProductName!))
            .WhenWhere(req.Sku is not null, m => m.Sku.Contains(req.Sku!))
            .WhenWhere(req.Type is not null, m => m.ProductType == req.Type)
            .WhenWhere(ids is not null && ids.Count > 0, m => ids!.Contains(m.ID))
            .WhenWhere(req.Pids is { Length: > 0}, m => req.Pids!.Contains(m.Pid))
            .OrderByDescending(m => m.ID)
            .ToPaginateAsync(page: req.Page, limit: req.Limit);
    }

    /**
     * 增加产品同步关联信息
     * User: CCY
     * Date: 2020/8/5 10:46
     * @param       $proInfo
     * @param       $source_id
     * @param       $sku
     * @param int   $sku_type     1 有前缀（有前缀可以关联产品） 2 无前缀(无法关联产品)
     * @param int   $sku_suffix   1 SKU追加属性信息（更改产品变体属性名会更改上传sku） 2 不追加属性信息
     * @param int   $title_suffix 1 产品标题追加属性信息 2 不追加属性信息
     * @param int   $product_type 1上传 2导出
     * @param array $typeData     分类
     * @param array $typeDataId   分类id
     * @return int
     */
    public async Task<SyncProductModel> InsertSyncProduct(CopyProductViewModel vm, ProductConfig productConfig, string[] typeData, long[] typeDataId)
    {
        var insertModel = new SyncProductModel(_session)
        {
            ProductName = vm.Product.Title,
            ImgUrl = vm.Product.MainImage,
            Pid = vm.Product.ID,
            SourceId = vm.SourceId,
            SourceItemId = vm.SourceItemId,
            Sku = !string.IsNullOrEmpty(vm.Product.Sku) ? vm.Product.Sku: Helpers.SkuCode(),
            SkuType = productConfig.SkuPrefix
                ? SyncProductModel.SkuTypes.TYPE_TRUE
                : SyncProductModel.SkuTypes.FALSE,
            SkuSuffix = productConfig.SkuSuffix
                ? SyncProductModel.SkuSuffixs.TRUE
                : SyncProductModel.SkuSuffixs.FALSE,
            TitleSuffix = productConfig.TitleSuffix
                ? SyncProductModel.TitleSuffixs.TRUE
                : SyncProductModel.TitleSuffixs.FALSE,
            ProductType = SyncProductModel.ProductTypes.Upload,
            CategoryId = vm.Product.CategoryId,
            SubCategoryId = vm.Product.SubCategoryId,
            AlreadyUpload = false,
            Uploading = false,
            EanupcStatus = SyncProductModel.EanupcStatuss.NOTHING,
            TypeData = JsonConvert.SerializeObject(typeData),
            TypeDataId = JsonConvert.SerializeObject(typeDataId),
        };
        _dbContext.SyncProduct.Add(insertModel);
        await _dbContext.SaveChangesAsync();
        return insertModel;
    }

    /**
     * 加入新任务
     * User: CCY
     * Date: 2020/8/5 11:20
     * @param     $info
     * @param int $product_type 1上传 2导出
     * @return mixed
     */
    public async Task<ReturnStruct> joinProductType( /*$info, $product_type = 1*/)
    {
        /*
        $typeBiter = new Type($info['product_type']);
        if ($product_type == 1) {
            $typeBiter->setUpload(true);
        } else {
            $typeBiter->setExport(true);
        }

        return SyncProductModel::query()->id($info['id'])->update(['product_type' => $typeBiter->getFlags()]);
        */
        return await Task.FromResult(ReturnArr(true));
    }

    /**
     * 移除任务关联,保留单一关系
     * User: CCY
     * Date: 2020/8/6 16:24
     * @param     $id
     * @param int $product_type 保留关系 1上传 2导出
     * @return mixed
     */
    public async Task UpdateProductType(int id, SyncProductModel.ProductTypes type)
    {
        var info = await _dbContext.SyncProduct.Where(m => m.ID == id).FirstOrDefaultAsync();

        if (info == null)
        {
            throw new NotFoundException("找不到导出产品相关数据");
        }

        info.ProductType = type;

        await _dbContext.SyncProduct.SingleUpdateAsync(info);

        await _dbContext.SaveChangesAsync();
    }

    //获取详情
    public async Task<SyncProductModel?> GetInfoById(int id)
    {
        return await _dbContext.SyncProduct.Where(m => m.ID == id).FirstOrDefaultAsync();
    }

    //指定ids获取信息
    public async Task<List<SyncProductModel>> GetSyncProduct(List<int> ids)
    {
        return await _dbContext.SyncProduct.Where(a => ids.Contains(a.ID)).ToListAsync();
    }

    //列表页
    public async Task<ReturnStruct> indexList( /*$name = '', $sku = '', $type = '', $ids = []*/)
    {
        /*
        return SyncProductModel::query()
            ->ProductName($name)
            ->SKU($sku)
            ->ProductType($type)
            ->InId($ids)
            ->groupId($this->getSession()->getGroupId())
            ->CompanyId($this->getSession()->getCompanyId())
            ->oemId($this->getSession()->getOemId())
            ->orderby('id', 'DESC')
            ->select()
            ->paginate($this->limit);
        */
        return await Task.FromResult(ReturnArr(true));
    }

    //
    public async Task<ReturnStruct> info( /*$field = [], $id = ''*/)
    {
        /*
        empty($field) && $field = ["*"];
        return SyncProductModel::query()
            ->CompanyId($this->getSession()->getCompanyId())
            ->Select($field)
            ->Id($id);
        */
        return await Task.FromResult(ReturnArr(true));
    }

    //
    public async Task<SyncProductModel?> CheckData(int? id)
    {
        if (id is null)
            return null;

        return await _dbContext.SyncProduct.Where(sp => sp.ID == id).FirstOrDefaultAsync();
    }

    /// <summary>
    /// 主键删除
    /// </summary>
    /// <param name="id"></param>
    /// <param name="type">删除产品类型</param>
    public async Task DeleteSyncProduct(int id, SyncProductModel.ProductTypes type)
    {
        var syncProduct = await GetInfoById(id);
        if (syncProduct is null)
            return;
        if (syncProduct.ProductType == type)
        {
            //syncProduct只被用户导出使用  物理删除复制产品信息
            await _dbContext.SyncProduct.Where(m => m.ID == id).DeleteAsync();
            await _productService.DestroyNoTransaction(syncProduct.Pid, false);
        }
        else
        {
            //更新产品类型为只上传   
            await UpdateProductType(id,
                type == SyncProductModel.ProductTypes.Upload
                    ? SyncProductModel.ProductTypes.Export
                    : SyncProductModel.ProductTypes.Upload);
            await _productService.UpdateProductType(syncProduct.Pid);
        }
    }

    //修改sku
    public async Task<ReturnStruct> editSku(string data)
    {
        /*
        return syncProductModel::query()
            ->Id($data['id'])
            ->Uploading()
            ->update(['sku' => $data['sku'], 'sku_type' => $data['sku_type']]);
        */
        return await Task.FromResult(ReturnArr(true));
    }

    //检查是否可删除
    public async Task<ReturnStruct> checkDeletable(int id)
    {
        /*
        return syncProductModel::query()
            ->find($id);
        */
        return await Task.FromResult(ReturnArr(true));
    }

    //删除
    public async Task<bool> Delete(int id)
    {
        var info = await GetInfoById(id);

        if (info is null)
        {
            throw new NotFoundException("找不到导出产品相关数据#1");
        }
        /**
         * //查询mws表id=spid中是否有进行中
        $data = $this->mwsService->uploadingProduct($id);
        if ($res['already_upload'] === SyncProductModel::ALREADY_UPLOAD_TRUE || checkArr($data)) {
            return error(getlang('store.syncproduct.TaskFirst'));
        }
         */

        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            var exportProduct = await _syncExportProductService.GetExportProductInfo(0, id);

            if (exportProduct is not null)
            {
                //删除导出关联关系
                await _syncExportProductService.DeleteExportProductById(exportProduct.ID);
                //导出任务产品数自减
                await _syncExportService.ExportReduce(exportProduct.Eid);
            }

            //克隆产品物理删除
            await _productService.DestroyNoTransaction(info.Pid, false);
            // $this->mwsService->deleteByspid($id);
            await DeleteById(id);
            await transaction.CommitAsync();
            return true;
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            Console.WriteLine(e.Message);
            throw new ExportException(e.Message);
        }

       
    }

    public async Task DeleteById(int id)
    {
        await _dbContext.SyncProduct.Where(m => m.ID == id).DeleteAsync();
    }


    //根据id更新信息
    public async Task<bool> UpdateDataById(EditSkuDto req)
    {
        var info = await GetInfoById(req.Id);

        if (info is null)
        {
            throw new NotFoundException("找不到相关数据#1");
        }

        if (req.Type != EditSkuDto.RequestType.Batch)
        {
            info.Sku = req.Sku;
        }
        info.SkuSuffix = req.SkuSuffix;
        info.SkuType = req.SkuType;
        info.TitleSuffix = req.TitleSuffix;

        await _dbContext.SyncProduct.SingleUpdateAsync(info);
        await _dbContext.SaveChangesAsync();
        return true;
    }

    //
    public async Task<ReturnStruct> updateData( /*$where, $update_data*/)
    {
        /*
        return SyncProductModel::query()->where($where)->update($update_data);
        */
        return await Task.FromResult(ReturnArr(true));
    }

    /**
      * 更新eanupc状态
      * Date: 2020/9/22
      * User: Ly
      * @param      $status
      * @param bool $changeToInit 修改eanupc后将mws表的sku_change_data全部改为待上传
      * @param      $pid
      * @throws \Throwable
      * @return array
      */
    public async Task<ReturnStruct> updateEanupcStatus( /*$pid, $status, $changeToInit = false*/)
    {
        /*
        $syncproduct = SyncProductModel::query()
            ->Pid($pid)
            ->CompanyId($this->getSession()->getCompanyId())
            ->OemId($this->getSession()->getOemId())
            ->first();
        if (!$syncproduct) {
            return returnArr(false, getlang('store.syncproduct.DataError'));
        }
        DB::beginTransaction();
        try {
            SyncProductModel::query()
                ->Pid($pid)
                ->CompanyId($this->getSession()->getCompanyId())
                ->OemId($this->getSession()->getOemId())
                ->update(['eanupc_status' => $status]);
            //同时更新mws表字段
            $updateData = ['eanupc_status' => $status];
            if ($changeToInit) {
                $syncMwsRow = SyncMwsModel::query()->Spid([$syncproduct['id']])
                    ->CompanyId($this->getSession()->getCompanyId())
                    ->OemId($this->getSession()->getOemId())
                    ->first(['sku_change_data']);
                $newChangeData = (new SyncMws())->restoreChangeData($syncMwsRow->sku_change_data);

                $updateData = array_merge($updateData, [
                    'sku_change_data' => $newChangeData,
                    'upload_id'       => null,
                    'state'           => SyncMwsModel::STATE_WAITING
                ]);
            }
            SyncMwsModel::query()
                ->Spid([$syncproduct['id']])
                ->CompanyId($this->getSession()->getCompanyId())
                ->OemId($this->getSession()->getOemId())
                ->update($updateData);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return returnArr(false, getlang('store.syncproduct.UpdateFailed'));
        }
        return returnArr(true, '', '');
        */
        return await Task.FromResult(ReturnArr());
    }


    //    public function scopeId(Builder $query, $id)
    //    {
    //        if ($id) {
    //            return $query->where('id', '=', $id);
    //        }
    //    }

    //    public function scopeInId(Builder $query, $id)
    //    {
    //        if ($id) {
    //            return $query->whereIn('id', $id);
    //        }
    //    }

    //    public function scopePid(Builder $query, $pid)
    //    {
    //        if ($pid) {
    //            return $query->where('pid', $pid);
    //        }
    //    }

    //    public function scopeOemId(Builder $query, $oem_id)
    //    {
    //        if ($oem_id) {
    //            return $query->where('oem_id', '=', $oem_id);
    //        }
    //    }

    //    public function scopeUserId(Builder $query, $user_id)
    //    {
    //        if ($user_id) {
    //            return $query->where('user_id', '=', $user_id);
    //        }
    //    }


    //    public function scopeGroupId(Builder $query, $group_id)
    //    {
    //        if ($group_id) {
    //            return $query->where('group_id', '=', $group_id);
    //        }
    //    }


    //    public function scopeCompanyId(Builder $query, $company_id)
    //    {
    //        if ($company_id) {
    //            return $query->where('company_id', '=', $company_id);
    //        }
    //    }


    //    public function scopeProductName(Builder $query, $product_name)
    //    {
    //        if ($product_name) {
    //            return $query->where('product_name', 'like', '%'. $product_name. '%');
    //        }
    //    }

    //    public function scopeSKU(Builder $query, $sku)
    //    {
    //        if ($sku) {
    //            return $query->where('sku', 'like', '%'. $sku. '%');
    //        }
    //    }

    //    public function scopeProductType(Builder $query, $type)
    //    {
    //        if (!empty($type))
    //        {
    //            $all = Type::FLAG_IS_UPLOAD + Type::FLAG_IS_EXPORT;
    //            $whereSql = '('. $this->transformKey(
    //                   'product_type'
    //               ). '&'. $all. ')>>'.self::BIT_PROGRESS. '='. $type;
    //            return $query->whereRaw($whereSql);
    //        }
    //    }

    //    public function getTypeAttribute(): int
    //    {
    //        if (isset($this->attributes['product_type'])) {
    //            $all = Type::FLAG_IS_UPLOAD + Type::FLAG_IS_EXPORT;
    //            return ($this->attributes['product_type'] & $all) >> self::BIT_PROGRESS;
    //        }
    //        return 0;
    //    }

    //    public function getImgUrlAttribute()
    //{
    //    if (isset($this->attributes['img_url']))
    //    {
    //            $productId = $this->attributes['pid'];
    //            $image = $this->attributes['img_url'];

    //        return $this->encrypImageSrc($image,$productId);
    //    }
    //}

    //public function scopeUploading(Builder $query, $uploading = 1)
    //{
    //    if ($uploading) {
    //    return $query->where('uploading', '=', $uploading);
    //}
    //    }

    //    public function scopeAlreadyUpload(Builder $query, $already_upload = 1)
    //{
    //    if ($already_upload) {
    //    return $query->where('already_upload', '=', $already_upload);
    //}
    //    }
    public async Task<List<SyncProductModel>> GetListBySku(string sku)
    {
        return await _dbContext.SyncProduct
            .Where(m => m.CompanyID == _companyId && m.Sku == sku)
            .ToListAsync();
    }

    public async Task<string> DistributionCodeToExportProducts(int spid, AllotExportProductDto.ModeEnum mode, bool cover)
    {
        var langId = await _dbContext.SyncExportProduct.Include(sep => sep.Export)
            .Where(sep => sep.Sid == spid)
            .Select(sep => sep.Export.LanguageId)
            .FirstAsync();
        return await DistributionCode(spid, mode, cover, langId);
    }
    
    public async Task<string> DistributionCodeToUploadProducts(int spid, AllotExportProductDto.ModeEnum mode, bool cover)
    {
        var langId = await _dbContext.productUpload.Include(sep => sep.SyncUploadTask)
            .Where(sep => sep.Spid == spid)
            .Select(sep => sep.SyncUploadTask.Language)
            .FirstAsync();
        return await DistributionCode(spid, mode, cover, (int)langId);
    }
    
    private async Task<string> DistributionCode(int spid, AllotExportProductDto.ModeEnum mode, bool cover, int langId)
    {
        //获取syncProduct
        var syncProduct = await GetInfoById(spid);

        if (syncProduct == null)
        {
            throw new Exceptions.Export.NotFoundException("产品不存在#1");
        }

        
        var product = await _productService.GetInfoById(syncProduct.Pid)!;
        await _productService.DistributeEanUpc(new (){{product!, langId}}, mode, cover);
        syncProduct.EanupcStatus = SyncProductModel.EanupcStatuss.ALL;
        await _dbContext.SaveChangesAsync();

        return product!.Title;
    }
}