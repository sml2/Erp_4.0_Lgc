﻿using ERP.Data;
using ERP.Models.Message;
using Microsoft.EntityFrameworkCore;
using System.IO;

namespace ERP.Services.Message;

public class FeedBackService : BaseService
{
    private readonly DBContext _dbContext;

    ////原ERP3.0注释
    #region ERP3.0 PHP
       public FeedBackService(DBContext dbContext)
       {
           _dbContext = dbContext;
       }

    public IQueryable<FeedBack> info(int userId,int oemId,string? title = "",FeedBack.Types? types = null, FeedBack.States? state = null)
    {
        var root = _dbContext.FeedBacks.Where(f => f.UserID == userId && f.OEMID == oemId);
        if (!string.IsNullOrEmpty(title))
            root = root.Where(f => f.Title == title);
        if (types.HasValue)
            root = root.Where(f => f.Type == types);
        if (state.HasValue)
            root = root.Where(f => f.State == state);
        return root;
    }

    public Task<FeedBack?> Info(int id)
    {
        return _dbContext.FeedBacks.FirstOrDefaultAsync(f => f.ID == id);
    }

    public async Task InsertData(FeedBack model)
    {
        _dbContext.FeedBacks.Add(model);
        await _dbContext.SaveChangesAsync();
    }

    public async Task UpdateData(FeedBack fb)
    {
        // _dbContext.FeedBacks.Update(fb);
        await _dbContext.SaveChangesAsync();
    }

    /// <summary>
    /// 编辑产品时删除图片
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<bool> DelImage(int id)
    {
        var res = await _dbContext.TempImages.FirstOrDefaultAsync(i => i.ID == id);

        if (res is not null) {
            // if (Upload::deleteOssImage($res->oss_path))
            {
                _dbContext.TempImages.Remove(res);
                // request()->merge(['disk' => -$res->size]);
            }
        }
        return true;
    }
    #endregion
}

