﻿using ERP.Data;
using ERP.Extensions;
using ERP.Models.Message;
using Microsoft.EntityFrameworkCore;

namespace ERP.Services.Message;

public class WorkOrderProgressService : BaseService
{
    private readonly DBContext _dbContext;

    public WorkOrderProgressService(DBContext dbContext)
    {
        _dbContext = dbContext;
    }

    public Task<DbSetExtension.PaginateStruct<WorkOrderProgress>> GetWorkOrderProgressInfo(int? companyId = null,
        WorkOrderProgress.States? state = null, string? name = null)
    {
        IQueryable<WorkOrderProgress> root = _dbContext.WorkOrderProgresses;
        if (companyId.HasValue)
            root = root.Where(r => r.CompanyID == companyId);
        if (state.HasValue)
            root = root.Where(r => r.State == state);
        if (!string.IsNullOrEmpty(name))
            root = root.Where(r => r.Name == name);
        return root.OrderBy(r => r.State).ToPaginateAsync();
    }
    
    public Task<List<WorkOrderProgress>> GetAll(int companyId,
        WorkOrderProgress.States? state = null)
    {
        IQueryable<WorkOrderProgress> root = _dbContext.WorkOrderProgresses.Where(r => r.CompanyID == companyId);
        if (state.HasValue)
            root = root.Where(r => r.State == state);
        return root.OrderBy(r => r.State).ToListAsync();
    }

    public async Task ProgressInsert(WorkOrderProgress model)
    {
        _dbContext.WorkOrderProgresses.Add(model);
        await _dbContext.SaveChangesAsync();
    }

    public Task<WorkOrderProgress?> GetWorkOrderProgressFirst(int? id = null, string? name = null)
    {
        var root = id.HasValue
            ? _dbContext.WorkOrderProgresses.Where(c => c.ID == id)
            : (!string.IsNullOrEmpty(name)
                ? _dbContext.WorkOrderProgresses.Where(c => c.Name == name)
                : _dbContext.WorkOrderProgresses);
        return root.FirstOrDefaultAsync();
    }

    public async Task ProgressUpdate(WorkOrderProgress model)
    {
        _dbContext.WorkOrderProgresses.Update(model);
        await _dbContext.SaveChangesAsync();
    }

    public async Task ProgressDel(int id)
    {
        var model = new WorkOrderProgress() { ID = id };
        _dbContext.WorkOrderProgresses.Remove(model);
        await _dbContext.SaveChangesAsync();
    }
}