﻿using ERP.Data;
using ERP.Enums.Message;
using ERP.Exceptions.Message;
using ERP.Models.Message;
using Microsoft.EntityFrameworkCore;

namespace ERP.Services.Message;

public class WorkOrderService : BaseService
{
    private readonly DBContext _dbContext;

    public WorkOrderService(DBContext dbContext)
    {
        _dbContext = dbContext;
    }

    public IQueryable<WorkOrder> GetWorkOrderInfo(int? id = null, int? companyId = null, string? title = null,
        int? categoryId = null, int? progressId = null
        , WorkOrder.States? state = null, Commits? commit = null, int? userId = null)
    {
        IQueryable<WorkOrder> root = _dbContext.WorkOrders;
        if (id.HasValue)
            root = root.Where(r => r.ID == id);
        if (companyId.HasValue)
            root = root.Where(r => r.CompanyID == companyId);
        if (!string.IsNullOrEmpty(title))
            root = root.Where(r => r.Title.StartsWith(title));
        if (categoryId.HasValue)
            root = root.Where(r => r.CategoryId == categoryId);
        if (progressId.HasValue)
            root = root.Where(r => r.ProgressId == progressId);
        if (state.HasValue)
            root = root.Where(r => r.State == state);
        if (commit.HasValue)
            root = root.Where(r => r.Commit == commit);
        if (userId.HasValue)
            root = root.Where(r => r.UserID == userId);
        return root;
    }
    
    public Task<WorkOrder?> GetInfoById(int id)
    {
        return _dbContext.WorkOrders.Where(r => r.ID == id).FirstOrDefaultAsync();
    }

    public async Task Create(WorkOrder model)
    {
        var category = await _dbContext.Set<WorkOrderCategory>().Where(c => c.ID == model.CategoryId).Select(c=> c.Name).FirstOrDefaultAsync();
        if (category is null)
            throw new MessageDomainException("标签未找到");
        var progress = await _dbContext.Set<WorkOrderProgress>().Where(c => c.ID == model.ProgressId).Select(c=> c.Name).FirstOrDefaultAsync();
        if (progress is null)
            throw new MessageDomainException("进度未找到");
        model.Category = category;
        model.Progress = progress;
        _dbContext.WorkOrders.Add(model);
        await _dbContext.SaveChangesAsync();
    }

    public async Task Update(WorkOrder model)
    {
        _dbContext.WorkOrders.Update(model);
        await _dbContext.SaveChangesAsync();
    }
}