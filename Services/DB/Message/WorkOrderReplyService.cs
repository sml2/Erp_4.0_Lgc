﻿using ERP.Data;
using ERP.Models.Message;

namespace ERP.Services.Message;
public class WorkOrderReplyService : BaseService
{
    private readonly DBContext _dbContext;
    public WorkOrderReplyService(DBContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task Insert(WorkOrderReply model)
    {
        _dbContext.WorkOrderReplies.Add(model);
        await _dbContext.SaveChangesAsync();
    }

    public IQueryable<WorkOrderReply> GetWorkOrderReplyInfo(int? id = null,int? user_id = null,int? work_order_id = null)
    {
        IQueryable<WorkOrderReply> root = _dbContext.WorkOrderReplies;
        if (id.HasValue)
            root = root.Where(r => r.ID == id);
        if (user_id.HasValue)
            root = root.Where(r => r.UserID == user_id);
        if (work_order_id.HasValue)
            root = root.Where(r => r.WorkOrderId == work_order_id);
        return root;
    }

    public ReturnStruct getPage(/*$result*/)
    {
        //    return $result
        //        //            ->orderBy("created_at" ,"desc")
        //        //            ->orderBy("state" ,"asc")
        //        ->paginate($this->limit);
        return ReturnArr();
    }
}

