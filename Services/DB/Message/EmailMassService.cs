﻿using ERP.Data;
using ERP.Models.Message;
using Microsoft.EntityFrameworkCore;

namespace ERP.Services.Message;

public class EmailMassService : BaseService
{
    private readonly DBContext _dbContext;

    public EmailMassService(DBContext dbContext)
    {
        _dbContext = dbContext;
    }

    public IQueryable<EmailMass> info(int user_id, int company_id, ulong? hashcode = null, string? postbox = null)
    {
        var root = _dbContext.EmailMasses.Where(e => e.UserID == user_id && e.CompanyID == company_id);
        if (hashcode.HasValue)
            root = root.Where(e => e.HashCode == hashcode);

        if (!string.IsNullOrEmpty(postbox))
            root = root.Where(e => e.PostBox == postbox);

        return root;
    }

    public Task<EmailMass?> Info(int id, Models.DB.Identity.User user)
    {
        return (from em in _dbContext.EmailMasses
            where em.ID == id && em.UserID == user.Id && em.CompanyID == user.CompanyID
            select em
            /*select new
            {
                em.ID,
                em.PostBox,
                em.Nick,
                em.UserID,
                em.CompanyID,
            }*/).FirstOrDefaultAsync();
        ;
    }

    /// <summary>
    /// 添加数据
    /// </summary>
    /// <param name="emailMass"></param>
    /// <returns></returns>
    public async Task<bool> InsertData(EmailMass emailMass)
    {
        try
        {
            _dbContext.Add(emailMass);
            await _dbContext.SaveChangesAsync();
        }
        catch (Exception /*e*/)
        {
            return false;
        }

        return true;
    }

    public async Task UpdateData(EmailMass emailMass)
    {
        _dbContext.EmailMasses.Update(emailMass);
        await _dbContext.SaveChangesAsync();
    }

    /// <summary>
    /// 检测是否存在
    /// </summary>
    /// <param name="emailMass"></param>
    /// <returns></returns>
    public Task<bool> CheckExists(EmailMass emailMass)
    {
        return _dbContext.EmailMasses.Where(em =>
                em.UserID == emailMass.UserID && em.CompanyID == emailMass.CompanyID &&
                em.HashCode == emailMass.HashCode)
            .AnyAsync();
    }

    public async Task DeleteAsync(int id, Models.DB.Identity.User user)
    {
        var userId = user.Id;
        var companyId = user.CompanyID;
        var row = await _dbContext.EmailMasses
            .Where(em => em.ID == id && em.UserID == userId && em.CompanyID == companyId)
            .FirstOrDefaultAsync();

        if (row == null) return;

        _dbContext.Remove(row);
        await _dbContext.SaveChangesAsync();
    }
}