﻿using ERP.Data;
using ERP.Extensions;
using ERP.Models.Message;

namespace ERP.Services.Message;

public class EmailInfoService : BaseService
{
    private readonly DBContext _dbContext;

    public EmailInfoService(DBContext dbContext)
    {
        _dbContext = dbContext;
    }

    /// <summary>
    /// 获取详情
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public ValueTask<EmailInfo?> Info(int id)
    {
        return _dbContext.EmailInfos.FindAsync(id);
    }

    public ReturnStruct getPage( /*$result*/)
    {
        //    return $result->paginate($this->limit);
        return ReturnArr();
    }
    
    public Task<DbSetExtension.PaginateStruct<EmailInfo>> CollectList(int userId, EmailInfo.States state, int? emailId = null, string? emailAddress = null)
    {
        var root = _dbContext.EmailInfos.Where(e => e.UserID == userId && e.State == state);
        if (emailId.HasValue)
            root = root.Where(e => e.EmailId == emailId.Value);

        if (!string.IsNullOrEmpty(emailAddress))
            root = root.Where(e => e.EmailAddress == emailAddress);
        
        return root.ToPaginateAsync();
    }

    public async Task<bool> Delete(int id, int userId)
    {
        var model = new EmailInfo() { ID = id, UserID = userId };
        _dbContext.EmailInfos.Remove(model);
        var affected = await _dbContext.SaveChangesAsync();
        return affected > 0;
    }
}