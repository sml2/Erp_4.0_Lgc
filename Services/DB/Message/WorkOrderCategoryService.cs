﻿using ERP.Data;
using ERP.Extensions;
using ERP.Models.Message;
using Microsoft.EntityFrameworkCore;

namespace ERP.Services.Message;

public class WorkOrderCategoryService : BaseService
{
    private readonly DBContext _dbContext;
       public WorkOrderCategoryService(DBContext dbContext)
       {
           _dbContext = dbContext;
       }

    public Task<DbSetExtension.PaginateStruct<WorkOrderCategory>> GetWorkOrderCategoryInfo(int companyId, WorkOrderCategory.States? state = null,string? name= null)
    {
        var root = _dbContext.WorkOrderCategories.Where(c => c.CompanyID == companyId);
        if (state.HasValue)
            root = root.Where(r => r.State == state);
        if (!string.IsNullOrEmpty(name))
            root = root.Where(r => r.Name == name);

        return root.OrderBy(r => r.State).ToPaginateAsync();
    }
    
    public Task<List<WorkOrderCategory>> GetAll(int companyId, WorkOrderCategory.States? state = null)
    {
        var root = _dbContext.WorkOrderCategories.Where(c => c.CompanyID == companyId);
        if (state.HasValue)
            root = root.Where(r => r.State == state);

        return root.OrderBy(r => r.State).ToListAsync();
    }

    public async Task CategoryInsert(WorkOrderCategory model)
    {
        _dbContext.WorkOrderCategories.Add(model);
        await _dbContext.SaveChangesAsync();
    }

    public Task<WorkOrderCategory?> GetWorkOrderCategoryFirst(int? id = null,string? name = null)
    {
        var root = id.HasValue 
            ? _dbContext.WorkOrderCategories.Where(c => c.ID == id)
            : (!string.IsNullOrEmpty(name)
                ? _dbContext.WorkOrderCategories.Where(c => c.Name == name)
                : _dbContext.WorkOrderCategories);
        return root.FirstOrDefaultAsync();
    }

    public async Task CategoryUpdate(WorkOrderCategory model)
    {
        _dbContext.WorkOrderCategories.Update(model);
        await _dbContext.SaveChangesAsync();
    }

    public async Task CategoryDel(int id)
    {
        var model = new WorkOrderCategory() { ID = id };
        _dbContext.WorkOrderCategories.Remove(model);
        await _dbContext.SaveChangesAsync();
    }
}

