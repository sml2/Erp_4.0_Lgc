﻿using ERP.Data;
using ERP.Extensions;
using ERP.Models.Message;
using Microsoft.EntityFrameworkCore;

namespace ERP.Services.Message;

public class NoticeService : BaseService
{
    private readonly DBContext _dbContext;

    public NoticeService(DBContext dbContext)
    {
        _dbContext = dbContext;
    }

    public IQueryable<Notice> info(int userId, int companyId, Enums.Message.States? state = null, Enums.Message.Types? type = null,
        string? title = null)
    {
        var root = _dbContext.Notices.Where(n => n.UserID == userId && n.CompanyID == companyId);
        if (state.HasValue)
            root = root.Where(r => r.State == state);
        if (type.HasValue)
            root = root.Where(r => r.Type == type);
        if (!string.IsNullOrEmpty(title))
            root = root.Where(r => r.Title!.StartsWith(title));

        return root;
    }

    public Task<Notice?> Info(int id, Models.DB.Identity.User user)
    {
        return _dbContext.Notices.FirstOrDefaultAsync(n =>
            n.ID == id && n.CompanyID == user.CompanyID && n.OEMID == user.OEMID);
    }

    public async Task Insert(Notice model)
    {
        _dbContext.Notices.Add(model);
        await _dbContext.SaveChangesAsync();
    }

    public async Task UpdateData(Notice model)
    {
        _dbContext.Notices.Update(model);
        await _dbContext.SaveChangesAsync();
    }
    
    /// <summary>
    /// 首页的公告列表
    /// </summary>
    /// <returns></returns>
    public Task<DbSetExtension.PaginateStruct<Notice>> NoticeList(int companyId, int oemId, bool withTime = false)
    {
        var root = withTime
            ? ListWithTimeQuery(_dbContext, companyId, oemId, DateTime.Now)
            : ListQuery(_dbContext, companyId, oemId);
        
        return root.OrderByDescending(n => n.CreatedAt)
            .ToPaginateAsync();

        IQueryable<Notice> ListQuery(DBContext ctx, int companyId, int oemId) => from n in ctx.Notices
            where n.Type == Enums.Message.Types.ALL //全体用户
                  || n.OEMID == oemId && n.CompanyID != companyId && n.Type == Enums.Message.Types.OEM //当前oem
                  || n.CompanyID == companyId && n.OEMID == oemId && n.Type == Enums.Message.Types.COMPANY //当前公司
            where n.State == Enums.Message.States.ON
            select n;

        IQueryable<Notice> ListWithTimeQuery(DBContext ctx, int companyId, int oemId, DateTime now) => from notice in ListQuery(ctx, companyId, oemId)
            where notice.NoticeBegin < now && notice.NoticeEnd > now
            select notice;
    }

    public async Task Delete(int id)
    {
        var model = new Notice() { ID = id };
        _dbContext.Notices.Remove(model);
        await _dbContext.SaveChangesAsync();
    }
}