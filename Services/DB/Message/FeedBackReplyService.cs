﻿using ERP.Data;
using ERP.Models.Message;
using Microsoft.EntityFrameworkCore;

namespace ERP.Services.Message;

public class FeedBackReplyService : BaseService
{
    private readonly DBContext _dbContext;

    public FeedBackReplyService(DBContext dbContext)
    {
        _dbContext = dbContext;
    }

    public Task<FeedBackReply?> Info(int id)
    {
        return _dbContext.FeedBackReplies.FirstOrDefaultAsync(fb => fb.ID == id);
    }
    
    public Task<List<FeedBackReply>> GetListByFeedBackId(int id)
    {
        return _dbContext.FeedBackReplies.Where(fb => fb.FeedbackId == id).OrderBy(fb => fb.CreatedAt).ToListAsync();
    }

    public async Task InsertData(FeedBackReply model)
    {
        _dbContext.FeedBackReplies.Add(model);
        await _dbContext.SaveChangesAsync();
    }
}