﻿using ERP.Models.DB.Operations;
using ViewModel = ERP.Models.View.Operations.TaskLog;

namespace ERP.Services.DB.Operations;

using ERP.Data;
using ERP.Exceptions.Operation;
using ERP.Extensions;
using ERP.Interface;
using ERP.Services.Caches;
using ERP.Services.Identity;
using static ERP.Extensions.DbSetExtension;
using Model = TaskLog;
public class OperationService : BaseService
{
    private readonly DBContext _DbContext;
    private readonly ILogger<OperationService> _Logger;
    private readonly TaskList taskList;

    public OperationService(DBContext DbContext, ILogger<OperationService> logger, TaskList _taskList)
    {
        _DbContext = DbContext;
        _Logger = logger;
        taskList = _taskList;
    }
  

    public async Task<PaginateStruct<Model>> GetTaskOperations(ViewModel.TaskLogVm taskLogVm)
    {
        var Query = await _DbContext.TaskLogOperation
            .Where(x => x.TypeModuel == taskLogVm.Type)
            .OrderBy(x => x.Sort).ToPaginateAsync(taskLogVm.Page,taskLogVm.Limit);

        return Query;
    }

    public async Task<bool> AddTaskOperations(ViewModel.AddTaskLog AddTasklog)
    {
        AddTasklog.ResDetail = string.Empty;
      
        await _DbContext.TaskLogOperation.AddAsync(new Model(AddTasklog.Type, AddTasklog.results, AddTasklog.endtime, AddTasklog.endtime, AddTasklog.ResDetail));
        if (_DbContext.SaveChanges() > 0) return true;


        _Logger.LogError("TaskLog拉取失败,The SaveChange Eoore");
        return false;
    }
    public async Task Add(Model model) {
       
        await _DbContext.TaskLogOperation.AddAsync(model);
        try
        {
            await _DbContext.SaveChangesAsync();
        }
        catch (Exception)
        {
            throw new OperationInster("TaskLog保存异常");
        }
    }
    public async Task<bool> TaskChangeFlag(ViewModel.ChangeFlag changeFlag)
    {
        try
        {
            var ModelLine = await _DbContext.TaskLogOperation.Where(x => x.ID == changeFlag.Id).UpdateFromQueryAsync(x => new(){ Flag = changeFlag.Flag });
            if (ModelLine > 0) return true;
            else
            {
                _Logger.LogError($"TaskLog{changeFlag.Id}修改Flag错误");
                return false;
            }
        }
        catch (Exception)
        {
            _Logger.LogError($"TaskLog{changeFlag.Id}修改Flag异常");
            return false;
        }
    }
}
