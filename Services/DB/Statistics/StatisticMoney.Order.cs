using ERP.Enums.Finance;
using ERP.Models.DB.Orders;
using ERP.Models.Finance;
using ERP.Services.DB.Statistics;
using ERP.ViewModels.Statistics;

namespace ERP.Services.Statistics;

public partial class StatisticMoney
{
    public async Task InitStatisticWithOrder(Order order)
    {
        var fm = new List<FinancialAffairsModel>();

        #region 新增数据-订单手续费统计/跟踪(1)

        var orderFeeFm = await OrderFeeAdd(order);
        fm.Add(orderFeeFm);

        #endregion

        #region 新增数据-订单总价统计/跟踪(1)

        var orderTotalFm = await OrderTotalAdd(order);
        fm.Add(orderTotalFm);

        #endregion

        #region 新增数据-订单获利跟踪(1)

        var profitFm = await OrderProfitAdd(order);
        fm.Add(profitFm);

        #endregion

        #region 新增数据-订单运费跟踪(1)

        var shippingMoneyFm = await OrderShippingMoneyAdd(order);
        fm.Add(shippingMoneyFm);

        #endregion

        #region 新增数据-订单总价/订单获利  统计/跟踪(2)

        await _statisticService.UpdateTable(fm, order.ID);

        #endregion
    }

    #region 运费

    /// <summary>
    /// 新增数据-订单运费
    /// </summary>
    /// <param name="info"></param>
    /// <returns></returns>
    public async Task<FinancialAffairsModel> OrderShippingMoneyAdd(Order info)
    {
        var shippingMoneyFm = await _statisticService.ReportFinancialRecord(
            new FianceDataDto(info.ShippingMoney, TableFieldEnum.OrderShippingMoney),
            new DataBelongDto(info)
        );

        info.ShippingMoney = info.ShippingMoney.Next(shippingMoneyFm);
        return shippingMoneyFm;
    }

    /// <summary>
    /// 修改数据-运费跟踪
    /// </summary>
    /// <param name="info"></param>
    public async Task OrderShippingMoneyEdit(Order info)
    {
        var shippingMoneyFm = await _statisticService.ReportFinancialRecord(
            new FianceDataDto(info.ShippingMoney, TableFieldEnum.OrderShippingMoney, info.ID,
                info.ShippingMoney.FinancialAffairsID),
            new DataBelongDto(info)
        );

        info.ShippingMoney = info.ShippingMoney.Next(shippingMoneyFm);
    }

    #endregion

    #region 订单获利

    /// <summary>
    ///  修改数据-订单获利
    /// </summary>
    /// <param name="info"></param>
    public async Task OrderProfitEdit(Order info)
    {
        var profitFm = await _statisticService.ReportFinancialRecord(
            new FianceDataDto(info.Profit, TableFieldEnum.OrderProfit, info.ID,
                info.Profit.FinancialAffairsID),
            new DataBelongDto(info)
        );
        info.Profit = info.Profit.Next(profitFm);
    }

    /// <summary>
    /// 新增数据-订单获利
    /// </summary>
    /// <param name="info"></param>
    /// <returns></returns>
    public async Task<FinancialAffairsModel> OrderProfitAdd(Order info)
    {
        var profitFm = await _statisticService.ReportFinancialRecord(
            new FianceDataDto(info.Profit, TableFieldEnum.OrderProfit),
            new DataBelongDto(info)
        );
        info.Profit = info.Profit.Next(profitFm);

        return profitFm;
    }

    #endregion

    #region 订单总价

    /// <summary>
    /// 新增数据-订单总价统计/跟踪(1)
    /// </summary>
    /// <param name="info"></param>
    /// <returns></returns>
    public async Task<FinancialAffairsModel> OrderTotalAdd(Order info)
    {
        var orderTotalFm = await _statisticService.ReportMoney(
            new DataBelongDto(info),
            Statistic.MoneyColumnEnum.OrderTotal,
            new FianceDataDto(info.OrderTotal, TableFieldEnum.OrderTotal)
        );

        info.OrderTotal = info.OrderTotal.Next(orderTotalFm);

        return orderTotalFm;
    }

    /// <summary>
    ///  修改数据-订单总价
    /// </summary>
    /// <param name="info"></param>
    public async Task OrderTotalEdit(Order info)
    {
        var orderTotalFid = await _statisticService.ReportMoney(
            new DataBelongDto(info),
            Statistic.MoneyColumnEnum.OrderTotal,
            new FianceDataDto(info.OrderTotal, TableFieldEnum.OrderTotal, info.ID, info.OrderTotal.FinancialAffairsID)
        );

        info.OrderTotal = info.OrderTotal.Next(orderTotalFid);
    }

    #endregion

    #region 手续费

    /// <summary>
    /// 新增数据-订单手续费统计/跟踪(1)
    /// </summary>
    /// <param name="info"></param>
    /// <returns></returns>
    public async Task<FinancialAffairsModel> OrderFeeAdd(Order info)
    {
        var orderFeeFm = await _statisticService.ReportMoney(
            new DataBelongDto(info),
            Statistic.MoneyColumnEnum.OrderTotalFee,
            new FianceDataDto(info.Fee, TableFieldEnum.OrderFee)
        );
        info.Fee = info.Fee.Next(orderFeeFm);

        return orderFeeFm;
    }

    /// <summary>
    /// 修改数据-订单手续费
    /// </summary>
    /// <param name="info"></param>
    /// <returns></returns>
    public async Task<FinancialAffairsModel> OrderFeeEdit(Order info)
    {
        var orderFeeFm = await _statisticService.ReportMoney(
            new DataBelongDto(info),
            Statistic.MoneyColumnEnum.OrderTotalFee,
            new FianceDataDto(info.Fee, TableFieldEnum.OrderFee, info.ID, info.Fee.FinancialAffairsID)
        );
        info.Fee = info.Fee.Next(orderFeeFm);

        return orderFeeFm;
    }

    #endregion

    #region 订单退款金额

    /// <summary>
    /// 新增数据-订单退款金额统计/跟踪(1)
    /// </summary>
    /// <param name="info"></param>
    /// <returns></returns>
    public async Task<FinancialAffairsModel> OrderRefundAdd(Order info)
    {
        var orderFeeFm = await _statisticService.ReportMoney(
            new DataBelongDto(info),
            Statistic.MoneyColumnEnum.OrderTotalRefund,
            new FianceDataDto(info.Refund, TableFieldEnum.OrderRefund)
        );
        info.Refund = info.Refund.Next(orderFeeFm);

        return orderFeeFm;
    }

    /// <summary>
    /// 修改数据-订单退款金额统计/跟踪(1)
    /// </summary>
    /// <param name="info"></param>
    /// <returns></returns>
    public async Task<FinancialAffairsModel> OrderRefundEdit(Order info)
    {
        var orderFeeFm = await _statisticService.ReportMoney(
            new DataBelongDto(info),
            Statistic.MoneyColumnEnum.OrderTotalRefund,
            new FianceDataDto(info.Refund, TableFieldEnum.OrderRefund, info.ID, info.Refund.FinancialAffairsID)
        );
        info.Refund = info.Refund.Next(orderFeeFm);

        return orderFeeFm;
    }

    #endregion

    #region 订单损耗CNY

    /// <summary>
    /// 新增数据-订单损耗统计/跟踪
    /// </summary>
    /// <param name="info"></param>
    public async Task<FinancialAffairsModel> OrderLossAdd(Order info)
    {
        var lossFm = await _statisticService.ReportMoney(
            new DataBelongDto(info),
            Statistic.MoneyColumnEnum.OrderTotalLoss,
            new FianceDataDto(info.Loss, TableFieldEnum.OrderLoss)
        );

        info.Loss = info.Loss.Next(lossFm);

        return lossFm;
    }

    /// <summary>
    /// 修改数据-订单损耗统计/跟踪
    /// </summary>
    /// <param name="info"></param>
    public async Task OrderLossEdit(Order info)
    {
        var lossFid = await _statisticService.ReportMoney(
            new DataBelongDto(info),
            Statistic.MoneyColumnEnum.OrderTotalLoss,
            new FianceDataDto(info.Loss, TableFieldEnum.OrderLoss, info.ID, info.Loss.FinancialAffairsID)
        );

        info.Loss = info.Loss.Next(lossFid);
    }

    #endregion

    #region 采购花费

    /// <summary>
    /// 修改数据-订单采购费用
    /// </summary>
    /// <param name="info"></param>
    public async Task OrderPurchaseFeeEdit(Order info)
    {
        var purchaseFeeFid = await _statisticService.ReportFinancialRecord(
            new FianceDataDto(info.PurchaseFee, TableFieldEnum.OrderPurchaseFee, info.ID,
                info.PurchaseFee.FinancialAffairsID),
            new DataBelongDto(info)
        );

        info.PurchaseFee = info.PurchaseFee.Next(purchaseFeeFid);
    }

    /// <summary>
    /// 新增数据-订单采购费用
    /// </summary>
    /// <param name="info"></param>
    public async Task<FinancialAffairsModel> OrderPurchaseFeeAdd(Order info)
    {
        var purchaseFeeFm = await _statisticService.ReportFinancialRecord(
            new FianceDataDto(info.PurchaseFee, TableFieldEnum.OrderPurchaseFee),
            new DataBelongDto(info)
        );

        info.PurchaseFee = info.PurchaseFee.Next(purchaseFeeFm);

        return purchaseFeeFm;
    }

    #endregion
}