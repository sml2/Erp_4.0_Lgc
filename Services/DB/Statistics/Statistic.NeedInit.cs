using ERP.Enums.Identity;
using ERP.Enums.Statistics;
using ERP.Extensions;
using ERP.ViewModels.Statistics;
using Newtonsoft.Json;
using Exception = System.Exception;
using Group = ERP.Models.DB.Users.Group;
using OEM = ERP.Models.Setting.OEM;
using Role = ERP.Enums.ID.Role;
using ERP.Models.Stores;
namespace ERP.Services.DB.Statistics;

using Model = Models.DB.Statistics.Statistic;
using Users = Models.DB.Identity.User;
using Company = Models.DB.Users.Company;

public partial class Statistic
{
    #region User/Employee 用户/员工

    /// <summary>
    /// 初始化用户
    /// </summary>
    /// <param name="m"></param>
    public async Task InitUser(Users m)
    {
        await CheckOrAdd(m);
    }


    /// <summary>
    /// 初始化用户
    /// </summary>
    /// <param name="indexId"></param>
    public async Task InitUser(int indexId)
    {
        await CheckOrAdd(_dbContext.User.FirstOrDefault(m => m.ID == indexId)!);
    }

    /// <summary>
    /// 用户新增数量涉及初始化用户统计数据
    /// </summary>
    /// <param name="session"></param>
    /// <param name="indexId"></param>
    /// <param name="role"></param>
    /// <param name="num"></param>
    public async Task IncUserCount(ISession? session, int indexId, Role role, int num = 1)

    {
        await InitUser(indexId);
        if (role == Role.Employee)
        {
            await InsertOrCount(session, NumColumnEnum.EmployeeInsert, NumColumnEnum.EmployeeCount, num);
        }
        else
        {
            await InsertOrCount(session, NumColumnEnum.UserInsert, NumColumnEnum.UserCount, num);
        }
    }

    /// <summary>
    /// 用户新增数量涉及初始化用户统计数据
    /// </summary>
    /// <param name="session"></param>
    /// <param name="m"></param>
    /// <param name="role"></param>
    /// <param name="num"></param>
    public async Task IncUserCount(ISession? session, Users m, Role role, int num = 1)
    {
        await InitUser(m);
        if (role == Role.Employee)
        {
            await InsertOrCount(session, NumColumnEnum.EmployeeInsert, NumColumnEnum.EmployeeCount, num);
        }
        else
        {
            await InsertOrCount(session, NumColumnEnum.UserInsert, NumColumnEnum.UserCount, num);
        }
    }

    public async Task IncUserOrEmployeeCount(Users m, int num = 1)
    {
        await InitUser(m);
        var dataBelongDto = new DataBelongDto(m);
        if (m.Role == Enums.Identity.Role.Employee)
        {
            // NumColumnEnum.EmployeeInsert, NumColumnEnum.EmployeeCount,
            await EmployeeInsertAndCount(dataBelongDto, num);
        }
        else
        {
            //NumColumnEnum.UserInsert, NumColumnEnum.UserCount,
            await UserInsertAndCount(dataBelongDto,  num);
        }
    }

    public async Task IncUserOrEmployeeCount(Users createUser,Users newUser, int num = 1)
    {
        await InitUser(newUser);
        var dataBelongDto = new DataBelongDto(createUser,true);
        if (newUser.Role == Enums.Identity.Role.Employee)
        {
            // await InsertOrCount(dataBelongDto, NumColumnEnum.EmployeeInsert, NumColumnEnum.EmployeeCount, num);
            await EmployeeInsertAndCount(dataBelongDto, num);
        }
        else
        {
            // await InsertOrCount(dataBelongDto, NumColumnEnum.UserInsert, NumColumnEnum.UserCount, num);
            await UserInsertAndCount(dataBelongDto,  num);
        }
    }

    #endregion

    #region UserGroup 用户组

    /// <summary>
    /// 初始化用户组
    /// </summary>
    /// <param name="indexId"></param>
    public async Task InitUserGroup(int indexId)
    {
        await CheckOrAdd(_dbContext.UserGroup.FirstOrDefault(m => m.ID == indexId)!);
    }

    /// <summary>
    /// 初始化用户组
    /// </summary>
    /// <param name="m"></param>
    public async Task InitUserGroup(Group m)
    {
        await CheckOrAdd(m);
    }

    #endregion

    #region Store 店铺

    /// <summary>
    /// 初始化店铺
    /// </summary>
    /// <param name="m"></param>
    public async Task InitStore(StoreRegion m)
    {
        await CheckOrAdd(m);
    }

    /// <summary>
    /// 初始化店铺
    /// </summary>
    /// <param name="indexId"></param>
    public async Task InitStore(int indexId)
    {
        await CheckOrAdd(_dbContext.StoreRegion.FirstOrDefault(m => m.ID == indexId)!);
    }


    /// <summary>
    /// 店铺新增数量涉及初始化店铺统计数据
    /// </summary>
    /// <param name="session"></param>
    /// <param name="m"></param>
    /// <param name="num"></param>
    public async Task IncStoreCount(ISession? session, Models.Stores.StoreRegion m, int num = 1)
    {
        await InitStore(m);
        await InsertOrCount(session, NumColumnEnum.StoreInsert, NumColumnEnum.StoreCount, num);
    }

    /// <summary>
    /// 店铺新增数量涉及初始化店铺统计数据
    /// </summary>
    /// <param name="session"></param>
    /// <param name="indexId"></param>
    /// <param name="num"></param>
    public async Task IncStoreCount(ISession? session, int indexId, int num = 1)
    {
        await InitStore(indexId);
        await InsertOrCount(session, NumColumnEnum.StoreInsert, NumColumnEnum.StoreCount, num);
    }

    public async Task IncStoreCount(Models.Stores.StoreRegion m, int num = 1)
    {
        await InitStore(m);
        await InsertOrCount(new DataBelongDto(m), NumColumnEnum.StoreInsert, NumColumnEnum.StoreCount, num);
    }

    #endregion

    #region Company 公司

    /// <summary>
    /// 初始化公司
    /// </summary>
    /// <param name="indexId"></param>
    public async Task InitCompany(int indexId)
    {
        await CheckOrAdd(_dbContext.Company.FirstOrDefault(m => m.ID == indexId)!);
    }

    /// <summary>
    /// 初始化公司
    /// </summary>
    /// <param name="m"></param>
    public async Task InitCompany(Company m)
    {
        await CheckOrAdd(m);
    }

    /// <summary>
    /// 公司新增数量涉及初始化公司统计数据
    /// </summary>
    /// <param name="session"></param>
    /// <param name="indexId"></param>
    /// <param name="num"></param>
    public async Task IncCompanyCount(ISession? session, int indexId, int num = 1)
    {
        await InitCompany(indexId);
        await InsertOrCount(session, NumColumnEnum.CompanyInsert, NumColumnEnum.CompanyCount, num);
    }

    /// <summary>
    /// 公司新增数量涉及初始化公司统计数据
    /// </summary>
    /// <param name="session"></param>
    /// <param name="m"></param>
    /// <param name="num"></param>
    public async Task IncCompanyCount(ISession? session, Company m, int num = 1)
    {
        await InitCompany(m);
        await InsertOrCount(session, NumColumnEnum.CompanyInsert, NumColumnEnum.CompanyCount, num);
    }

    
    public async Task IncCompanyCount(Users createUser,Company newCompany, int num = 1)
    {
        await InitCompany(newCompany);
        await InsertOrCount(new DataBelongDto(createUser,true), NumColumnEnum.CompanyInsert, NumColumnEnum.CompanyCount, num);
    }
    
 

    #endregion

    #region Oem OEM

    /// <summary>
    /// 初始化OEM
    /// </summary>
    /// <param name="indexId"></param>
    public async Task InitOem(int indexId)
    {
        await CheckOrAdd(_dbContext.OEM.FirstOrDefault(m => m.ID == indexId)!);
    }

    /// <summary>
    /// 初始化OEM
    /// </summary>
    /// <param name="m"></param>
    public async Task InitOem(OEM m)
    {
        await CheckOrAdd(m);
    }

    #endregion
}