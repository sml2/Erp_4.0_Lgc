﻿using ERP.Enums.Statistics;
using Microsoft.EntityFrameworkCore;
using ERP.Extensions;
using ERP.Models.Statistics;
using ERP.Interface;
using ERP.Models.DB.Statistics;
using ERP.ViewModels.Statistics;
using Newtonsoft.Json;
using ERP.Data;

namespace ERP.Services.Statistics;

public class CounterCompanyBusinessService : BaseService
{
    readonly DBContext _MyDbContext;
    private readonly ISessionProvider _SessionProvider;
    public CounterCompanyBusinessService(DBContext dbContext, ISessionProvider SessionProvider)
    {
        _SessionProvider = SessionProvider;
        _MyDbContext = dbContext;
    }
    private ISession Session { get => _SessionProvider.Session!; }
    private int UserID { get => Session.GetUserID(); }
    private int GroupID { get => Session.GetGroupID(); }
    private int CompanyID { get => Session.GetCompanyID(); }
    private int OEMID { get => Session.GetOEMID(); }
    private bool IsDistribution { get => Session.GetIsDistribution(); }
    /// <summary>
    /// 初始化统计数据
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="oemId"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> InitCounterCompanyBusiness(int companyId, int oemId)
    {
        var insertData = new CounterCompanyBusiness() { CompanyID = companyId, OEMID = oemId };

        await _MyDbContext.CounterCompanyBusiness.AddAsync(insertData);
        await _MyDbContext.SaveChangesAsync();
        return ReturnArr(true);
    }

    /// <summary>
    /// 获取指定公司ids业务统计
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    public Task<List<HomePageVM>> GetCountCompanyByIds(List<int> ids)
    {
        // return _MyDbContext.CounterCompanyBusiness.Where(m => ids.Contains(m.CompanyID)).ToListAsync();

        return _MyDbContext.Statistic
            .Where(m => ids.Contains(m.AboutID)
                        && (m.AboutType & Types.Role) == Types.Company
                        && (m.AboutType & Types.Effective) == Types.TOTAL
            ).Select(m => new HomePageVM
            {
               CompanyId = m.AboutID,
               UserNum = m.UserCount,
               ProductNum = m.ProductCount,
               OrderNum = m.OrderCount,
               PurchaseNum = m.PurchaseCount,
               WaybillNum = m.WaybillCount,
               StoreNum= m.StoreCount,
               DistributionOrderNum = m.DistributionOrderCount,
               DistributionPurchaseNum = m.DistributionPurchaseCount,
               DistributionWaybillNum = m.DistributionWaybillCount
            }).ToListAsync();
    }

    /// <summary>
    /// 获取指定公司id业务统计
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> GetCountCompanyByIds(int id)
    {
        var list = await _MyDbContext.CounterCompanyBusiness.Where(m => m.CompanyID == id).ToListAsync();

        return ReturnArr(true, "", list);
    }

    /// <summary>
    /// 增加当前公司创建开通用户数 当前公司操作
    /// </summary>
    /// <param name="count"></param>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> UserNumAdd(int userNum = 1, int companyId = 0)
    {
        if (companyId == 0)
        {
            companyId = CompanyID;
        }
        var c = await _MyDbContext.CounterCompanyBusiness.FirstOrDefaultAsync(a => a.CompanyID == companyId);
        if (c != null)
        {
            c.UserNum += userNum;
            _MyDbContext.CounterCompanyBusiness.Update(c);
            await _MyDbContext.SaveChangesAsync();
            return ReturnArr(true);
        }
        return ReturnArr(false);
    }

    /// <summary>
    /// 增加当前公司创建产品数 当前公司操作
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ProductNumAdd(int count = 1)
    {
        //return counterCompanyBusinessModel::companyId($this->getSession()->getCompanyId())
        //    ->increment('product_num', $product_num);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 减少当前公司创建产品数
    /// </summary>
    /// <param name="count"></param>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ProductNumReduce(int count = 1, int companyId = 0)
    {
        //$companyId = $companyId ?: $this->getSession()->getCompanyId();
        //return counterCompanyBusinessModel::query()
        //    ->companyId($companyId)
        //    ->decrement('product_num', $product_num);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 增加当前公司创建订单数 当前公司操作
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> OrderNumAdd(int count = 1)
    {
        //return counterCompanyBusinessModel::companyId($this->getSession()->getCompanyId())
        //    ->increment('order_num', $order_num);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 增加当前公司创建采购数 当前公司操作
    /// </summary>
    /// <param name="count"></param>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> PurchaseNumAdd(int count = 1, int companyId = 0)
    {
        //$company_id = $company_id ?: $this->getSession()->getCompanyId();
        //return counterCompanyBusinessModel::companyId($company_id)
        //    ->increment('purchase_num', $purchase_num);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 增加当前公司创建运单数 当前公司直接操作，给下级公司操作
    /// </summary>
    /// <param name="waybillCount"></param>
    /// <param name="distributionCount"></param>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> WaybillNumAdd(int waybillCount = 1, int distributionCount = 0, int companyId = 0)
    {
        var GroupBusiness = _MyDbContext.CounterCompanyBusiness.SingleOrDefault(x => x.ID == CompanyID);
        if (GroupBusiness != null)
        {
            GroupBusiness.WaybillNum += waybillCount;
            GroupBusiness.DistributionOrderNum += distributionCount;
            return await Task.FromResult(ReturnArr(true));
        }
        return await Task.FromResult(ReturnArr(false));
    }

    /// <summary>
    /// 增加当前公司创建店铺数 当前公司操作
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> StoreNumAdd(int count = 1)
    {
        //return counterCompanyBusinessModel::companyId($this->getSession()->getCompanyId())
        //   ->increment('store_num', $store_num);
        var Group = await _MyDbContext.CounterCompanyBusiness.Where(x => x.CompanyID == CompanyID).FirstOrDefaultAsync();
        if (Group != null)
            Group.StoreNum += count;
        _MyDbContext.SaveChanges();
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 减少当前公司创建店铺数 当前公司操作
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> StoreNumReduce(int count = 1)
    {
        //return counterCompanyBusinessModel::companyId($this->getSession()->getCompanyId())
        //    ->decrement('store_num', $store_num);


        var data = await _MyDbContext.CounterCompanyBusiness.Where(x => x.CompanyID == 1).FirstOrDefaultAsync();
        if (data != null)
        {
            data.StoreNum = data.StoreNum - count;
            await _MyDbContext.SaveChangesAsync();
            return await Task.FromResult(ReturnArr(true));
        }

        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 增加下级上报创建开通用户数 当前公司操作
    /// </summary>
    /// <param name="userNum"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ReportUserNumAdd(int userNum = 1)
    {
        string str;
        if (IsDistribution)
        {
            str = Session.GetUserPids();
        }
        else
        {
            str = Session.GetReportPids();
        }
        var companyIds = JsonConvert.DeserializeObject<List<int>>(str);
        if (companyIds?.Count()>0)
        {
            var cList = await _MyDbContext.CounterCompanyBusiness.Where(a => companyIds.Contains(a.CompanyID)).ToListAsync();
            foreach (var item in cList)
            {
                item.ReportUserNum += userNum;
            }
            _MyDbContext.Update(cList);
            await _MyDbContext.SaveChangesAsync();
            return ReturnArr(true);
        }
        return ReturnArr(false);
    }

    /// <summary>
    /// 注册用户上报
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ReportRegisterUserNumAdd(int Count = 1)
    {
        //return counterCompanyBusinessModel::inCompanyId($pids)
        //    ->increment('report_user_num', 1);
        var UserNum = await _MyDbContext.CounterCompanyBusiness.Where(x => x.CompanyID == CompanyID).FirstOrDefaultAsync();
        if (UserNum != null)
            UserNum.ReportUserNum += Count;
        _MyDbContext.SaveChanges();
        return ReturnArr(true);
    }

    //public async Task<ReturnStruct> ReportRegisterUserNumAdd(int id)
    //{
    //    //return counterCompanyBusinessModel::inCompanyId($pids)
    //    //    ->increment('report_user_num', 1);

    //    return await Task.FromResult(ReturnArr(true));
    //}

    /// <summary>
    /// 增加下级公司上报产品数 下级公司操作
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ReportProductNumAdd(int count = 1)
    {
        //$company_id = json_decode($this->getSession()->getReportPids(), true);
        //return counterCompanyBusinessModel::inCompanyId($company_id)
        //    ->increment('report_product_num', $product_num);

        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 减少下级公司上报产品数 下级公司操作
    /// </summary>
    /// <param name="ids"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ReportProductNumAdd(List<int> ids, int count = 1)
    {
        //!checkArr($companyId) && $companyId = json_decode($this->getSession()->getReportPids(), true);
        //return counterCompanyBusinessModel::query()
        //    ->inCompanyId($companyId)
        //    ->decrement('report_product_num', $product_num);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 增加下级公司上报订单数 下级公司操作
    /// </summary>
    /// <param name="ids"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ReportOrderNumAdd(List<int> ids, int count = 1)
    {
        //return counterCompanyBusinessModel::inCompanyId($company_ids)
        //    ->increment('report_order_num', $order_num);
        return await Task.FromResult(ReturnArr(true));
    }

    public async Task<ReturnStruct> ReportOrderNumAdd(int id, int count = 1)
    {
        //return counterCompanyBusinessModel::inCompanyId($company_ids)
        //    ->increment('report_order_num', $order_num);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 增加下级公司上报采购数 下级公司操作
    /// </summary>
    /// <param name="ids"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ReportPurchaseNumAdd(List<int> CompanyId, int count = 1)
    {
        //!checkArr($company_id) && $company_id = json_decode($this->getSession()->getReportPids(), true);
        //return counterCompanyBusinessModel::inCompanyId($company_id)
        //    ->increment('report_purchase_num', $purchase_num);

        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 增加下级公司上报运单数 下级公司直接操作
    /// </summary>
    /// <param name="ReportPids"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ReportWaybillNumAdd(List<int> ReportPids, int count = 1)
    {
        //$ReportPids == '' && $ReportPids = json_decode($this->getSession()->getReportPids(), true);
        //return counterCompanyBusinessModel::inCompanyId($ReportPids)
        //    ->increment('report_waybill_num', $waybill_num);

        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 增加下级公司上报店铺数 下级公司操作
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ReportWaybillNumAdd(int count = 1)
    {
        //company_id = json_decode($this->getSession()->getReportPids(), true);
        //return counterCompanyBusinessModel::inCompanyId($company_id)
        //    ->increment('report_store_num', $store_num);

        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 增加下级公司上报分销订单数 下级公司操作
    /// </summary>
    /// <param name="ReportPids"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DistributionOrderNumAdd(List<int> ReportPids, int count = 1)
    {
        //!checkArr($reportPids) && $reportPids = json_decode($this->getSession()->getReportPids(), true);
        //return counterCompanyBusinessModel::inCompanyId($reportPids)
        //    ->increment('distribution_order_num', $distribution_order_num);
        return await Task.FromResult(ReturnArr(true));

    }

    /// <summary>
    /// 增加下级公司处理上报分销运单数 下级公司直接操作 上级公司给下级公司添加操作
    /// </summary>
    /// <param name="ReportPids"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DistributionPurchaseNumAdd(List<int> ReportPids, int count = 1)
    {
        //!checkArr($company_id) && $company_id = json_decode($this->getSession()->getReportPids(), true);
        //return counterCompanyBusinessModel::inCompanyId($company_id)
        //    ->increment('distribution_purchase_num', $distribution_purchase_num);

        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 同时增加当前公司创建订单数、采购数、订单总价、采购总价 当前公司操作
    /// </summary>
    /// <param name="orderNum"></param>
    /// <param name="purchaseNum"></param>
    /// <param name="orderTotal"></param>
    /// <param name="purchaseTotal"></param>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> OrderPurchaseNumAdd(int orderNum = 1, int purchaseNum = 1, int orderTotal = 0, int purchaseTotal = 0, int companyId = 0)
    {
        //$companyId = $companyId ?: $this->getSession()->getCompanyId();
        //$updateData = [
        //    'order_num'    => DB::raw($this->counterCompanyBusiness->transformKey('order_num'). '+'. $order_num),
        //    'purchase_num' => DB::raw($this->counterCompanyBusiness->transformKey('purchase_num'). '+'. $purchase_num)
        //];
        //$order_total != 0 && $updateData['order_total'] = DB::raw($this->counterCompanyBusiness->transformKey('order_total'). '+'. $order_total);
        //$purchase_total != 0 && $updateData['purchase_total'] = DB::raw($this->counterCompanyBusiness->transformKey('purchase_total'). '+'. $purchase_total);
        //return counterCompanyBusinessModel::companyId($companyId)
        //    ->update($updateData);

        return await Task.FromResult(ReturnArr(true));
    }

    /// <su同时增加下级公司上报订单数、采购数、订单总价、采购总价 下级公司操作mmary>
    /// 
    /// </summary>
    /// <param name="orderNum"></param>
    /// <param name="purchaseNum"></param>
    /// <param name="orderTotal"></param>
    /// <param name="purchaseTotal"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ReportOrderPurchaseNumAdd(int orderNum = 1, int purchaseNum = 1, int orderTotal = 0, int purchaseTotal = 0)
    {
        //$company_id = json_decode($this->getSession()->getReportPids(), true);
        //$updateData = [
        //    'report_order_num'    => DB::raw($this->counterCompanyBusiness->transformKey('report_order_num'). '+'. $order_num),
        //    'report_purchase_num' => DB::raw($this->counterCompanyBusiness->transformKey('report_purchase_num'). '+'. $purchase_num)
        //];
        //$order_total != 0 && $updateData['order_total'] = DB::raw($this->counterCompanyBusiness->transformKey('order_total'). '+'. $order_total);
        //$purchase_total != 0 && $updateData['purchase_total'] = DB::raw($this->counterCompanyBusiness->transformKey('purchase_total'). '+'. $purchase_total);
        //return counterCompanyBusinessModel::inCompanyId($company_id)
        //    ->update($updateData);

        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 同时增加下级公司上报分销订单数、采购数 下级公司操作
    /// </summary>
    /// <param name="orderNum"></param>
    /// <param name="purchaseNum"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DistributionOrderPurchaseNumAdd(int orderNum = 1, int purchaseNum = 1)
    {
        // $company_id = json_decode($this->getSession()->getReportPids(), true);
        //$updateData = [
        //    'distribution_order_num'    => DB::raw($this->counterCompanyBusiness->transformKey('distribution_order_num'). '+'. $order_num),
        //    'distribution_purchase_num' => DB::raw($this->counterCompanyBusiness->transformKey('distribution_purchase_num'). '+'. $purchase_num)
        //];
        //return counterCompanyBusinessModel::inCompanyId($company_id)
        //    ->update($updateData);

        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 去除下级公司上报分销采购数 下级公司操作
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DistributionPurchaseNumCut(int count = 1)
    {
        //$company_id = json_decode($this->getSession()->getReportPids(), true);
        //return counterCompanyBusinessModel::inCompanyId($company_id)
        //    ->decrement('distribution_purchase_num', $distribution_purchase_num);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 去除下级公司处理上报分销运单数 下级公司直接操作
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> distributionWaybillNumCut(int count = 1)
    {
        //$company_id = json_decode($this->getSession()->getReportPids(), true);
        //return counterCompanyBusinessModel::inCompanyId($company_id)
        //    ->decrement('distribution_waybill_num', $distribution_waybill_num);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 指定公司充值汇总
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="recharge"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> RechargeNumAdd(int companyId, int recharge)
    {
        //return counterCompanyBusinessModel::companyId($company_id)
        //   ->increment('recharge_total', $recharge);

        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 指定公司充值汇总
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="recharge"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> SelfRechargeNumAdd(int companyId, decimal recharge)
    {
        //return counterCompanyBusinessModel::companyId($company_id)
        //            ->increment('self_recharge_total', $recharge);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 指定公司支出汇总
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="expense"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ExpenseNumAdd(int companyId, int expense)
    {
        //return counterCompanyBusinessModel::companyId($company_id)
        //    ->increment('expense_total', $expense);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 指定公司支出汇总
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="expense"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> SelfExpenseNumAdd(int companyId, decimal expense)
    {
        //return counterCompanyBusinessModel::companyId($company_id)
        //           ->increment('self_expense_total', $expense);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 查询当前公司业务统计数据
    /// </summary>
    /// <param name="userBusiness"></param>
    /// <param name="companyBusiness"></param>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public async Task<HomePageVM?> GetCompany(int companyId,bool userBusiness = true, bool companyBusiness = true)
    {
        IQueryable<Statistic> model = _MyDbContext.Statistic
            .Where(m =>
                m.AboutID == companyId
                && (m.AboutType & Types.Role) == Types.Company
                && (m.AboutType & Types.Effective) == Types.TOTAL
            );

        //分销
        if (userBusiness == false && companyBusiness == false)
        {
            return  await model.Select(m => new HomePageVM()
            {
                UserNum = m.UserCount + m.EmployeeCount, 
                ProductNum = m.ProductCount, 
                OrderNum = m.OrderCount, 
                PurchaseNum = m.PurchaseCount,
                WaybillNum = m.WaybillCount, 
                StoreNum = m.StoreCount
            }).FirstOrDefaultAsync();
        }

        if (companyBusiness == false)
        {
            //代理，无下级分销上报
            return await model.Select(m => new HomePageVM
            {
                UserNum = m.UserCount + m.EmployeeCount,
                ProductNum = m.ProductCount,
                OrderNum = m.OrderCount,
                PurchaseNum = m.PurchaseCount,
                WaybillNum = m.WaybillCount,
                StoreNum = m.StoreCount,
                ReportUserNum = m.ReportUserCount,
                ReportProductNum = m.ReportProductCount,
                ReportOrderNum = m.ReportOrderCount,
                ReportPurchaseNum = m.ReportPurchaseCount,
                ReportWaybillNum = m.ReportWaybillCount,
                ReportStoreNum = m.ReportStoreCount,
            }).FirstOrDefaultAsync();
        }
        //代理，有下级分销上报
        return await model.Select(m => new HomePageVM
        {
            UserNum = m.UserCount + m.EmployeeCount,
            ProductNum = m.ProductCount,
            OrderNum = m.OrderCount,
            PurchaseNum = m.PurchaseCount,
            WaybillNum = m.WaybillCount,
            StoreNum = m.StoreCount,
            ReportUserNum = m.ReportUserCount,
            ReportProductNum = m.ReportProductCount,
            ReportOrderNum = m.ReportOrderCount,
            ReportPurchaseNum = m.ReportPurchaseCount,
            ReportWaybillNum = m.ReportWaybillCount,
            ReportStoreNum = m.ReportStoreCount,
            DistributionOrderNum = m.DistributionOrderCount,
            DistributionPurchaseNum = m.DistributionPurchaseCount,
            DistributionWaybillNum = m.DistributionWaybillCount
        }).FirstOrDefaultAsync();
    }

    /// <summary>
    /// 获取指定公司财务统计信息
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="companyIds"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> GetFinance(int companyId, List<int> companyIds)
    {
        //$field = ['id', 'company_id', 'order_total', 'order_total_fee', 'order_total_refund', 'purchase_total', 'purchase_total_refund', 'waybill_total', 'waybill_total_refund', 'recharge_total', 'expense_total', 'self_recharge_total', 'self_expense_total'];
        //return counterCompanyBusinessModel::query()
        //    ->companyId($company_id)
        //    ->InCompanyId($company_ids)
        //    ->select($field)
        //    ->paginate($this->limit);

        var data = await  _MyDbContext.CounterCompanyBusiness
                     .Where(m => m.CompanyID == companyId)
                     .WhenWhere(companyIds.Count > 0, m => companyIds.Contains(m.CompanyID))
                     .ToPaginateAsync();

        return ReturnArr(true, "", data);
    }


    /// <summary>
    /// 获取指定恭喜内部财务统计信息
    /// </summary>
    /// <param name="company_id"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> GetFinanceById(int companyId)
    {
        //$field = ['order_total', 'order_total_fee', 'order_total_refund', 'order_total_loss', 'purchase_total', 'purchase_total_refund', 'waybill_total', 'waybill_total_refund', 'self_recharge_total', 'self_expense_total'];
        //return counterCompanyBusinessModel::query()
        //    ->companyId($company_id)
        //    ->select($field)
        //    ->first();

        var data = _MyDbContext.CounterCompanyBusiness
                    .Where(m => m.CompanyID == companyId)
                    .ToPaginateAsync();

        return await Task.FromResult(ReturnArr(true, "", data));
    }

    public async Task<ReturnStruct> GetFinanceRealTime(int companyId, string date)
    {
        //$model = new AmazonOrder();
        //$field = [SumFields::orderTotal($model), SumFields::orderTotalFee($model), SumFields::orderTotalRefund($model),
        //    SumFields::orderTotalLoss($model), SumFields::purchaseTotal($model), SumFields::waybillTotal($model)];
        //$realData = AmazonOrder::whereCompanyId($companyId)
        //    ->where('order_state', '!=', AmazonOrder::ORDER_STATE_CANCEL)
        //    ->when(is_array($date) && count($date) === 2, fn($q) => $q->whereBetween('pay_time', $date))
        //    ->select($field)
        //    ->first()->makeHidden(['profit_margin', 'remark', 'progress_original']);
        //$field = ['purchase_total_refund', 'waybill_total_refund', 'self_recharge_total', 'self_expense_total'];
        //$sectionData = counterCompanyBusinessModel::query()
        //    ->companyId($companyId)
        //    ->select($field)
        //    ->first();
        //return array_merge($realData->toArray(), $sectionData->toArray());
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 同时更新采购数和采购总额
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="total"></param>
    /// <param name="number"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ChangePurchaseNumAndTotal(int companyId, int total = 0, int number = 1)
    {
        // $updateUser = [
        //    'purchase_num'   => DB::raw($this->counterCompanyBusiness->transformKey('purchase_num'). '+'. $number),
        //    'purchase_total' => DB::raw($this->counterCompanyBusiness->transformKey('purchase_total'). '+'. $total),
        //];
        //return counterCompanyBusinessModel::where('company_id', $companyId)
        //    ->update($updateUser);

        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 采购汇总    
    /// </summary>
    /// <param name="total"></param>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> PurchaseTotalAdd(int total, int companyId = 0)
    {
        //$companyId = $companyId ?: $this->getSession()->getCompanyId();
        //return counterCompanyBusinessModel::companyId($companyId)
        //    ->increment('purchase_total', $total);

        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 减少采购汇总
    /// </summary>
    /// <param name="total"></param>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> PurchaseTotalReduce(int total, int companyId = 0)
    {
        // $companyId = $companyId ?: $this->getSession()->getCompanyId();
        //return counterCompanyBusinessModel::companyId($companyId)
        //    ->decrement('purchase_total', $total);

        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 同时增加运单数、运费
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="total"></param>
    /// <param name="number"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ChangeWaybillNumAndTotal(int companyId, double total = 0.00, int number = 1)
    {
        //$updateData = [
        //    'waybill_num'   => DB::raw($this->counterCompanyBusiness->transformKey('waybill_num'). '+'. $number),
        //    'waybill_total' => DB::raw($this->counterCompanyBusiness->transformKey('waybill_total'). '+'. $total),
        //];
        //return counterCompanyBusinessModel::companyId($companyId)
        //    ->update($updateData);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 添加运费信息
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="total"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> WaybillTotalAdd(int companyId, double total = 0.00)
    {
        // return _MyDbContext.CounterCompanyBusiness.Where(x => x.CompanyID == companyId);



        //return counterCompanyBusinessModel::companyId($companyId)
        //            ->increment('waybill_total', $total);
        // return ReturnArr(true);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 账单公司汇总 增加
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> AddOrderTotalRefund(int companyId, double money)
    {
        //return counterCompanyBusinessModel::companyId($companyId)
        //    ->increment('order_total_refund', $money);
        return await Task.FromResult(await Task.FromResult(ReturnArr(true)));
    }

    /// <summary>
    /// 账单公司汇总 减少
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ReduceOrderTotalRefund(int companyId, double money)
    {
        //return counterCompanyBusinessModel::companyId($companyId)
        //    ->decrement('order_total_refund', $money);

        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 账单公司汇总 增加 手续费
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> AddOrderTotalFee(int companyId, double money)
    {
        //return counterCompanyBusinessModel::companyId($companyId)
        //    ->increment('order_total_fee', $money);

        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 账单公司汇总 减少 手续费
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ReduceOrderTotalFee(int companyId, double money)
    {
        //return counterCompanyBusinessModel::companyId($companyId)
        //    ->decrement('order_total_fee', $money);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 账单公司汇总 增加 订单损失费用
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> AddOrderLossFee(int companyId, double money)
    {
        //return counterCompanyBusinessModel::query()
        //    ->companyId($companyId)
        //    ->increment('order_total_loss', $money);

        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 账单公司汇总 减少 订单损失费用
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ReduceOrderLossFee(int companyId, double money)
    {
        //return counterCompanyBusinessModel::query()
        //    ->companyId($companyId)
        //    ->decrement('order_total_loss', $money);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 公司上报订单
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="orderNum"></param>
    /// <param name="orderTotal"></param>
    /// <param name="orderTotalFee"></param>
    /// <param name="orderTotalRefund"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> IncrementOrder(int companyId, int orderNum = 0, int orderTotal = 0, double orderTotalFee = 0, double orderTotalRefund = 0)
    {
        //$updateData = [
        //    'order_num'          => DB::raw($this->counterCompanyBusiness->transformKey('order_num'). '+'. $order_num),
        //    'order_total'        => DB::raw($this->counterCompanyBusiness->transformKey('order_total'). '+'. $order_total),
        //    'order_total_fee'    => DB::raw($this->counterCompanyBusiness->transformKey('order_total_fee'). '+'. $order_total_fee),
        //    'order_total_refund' => DB::raw($this->counterCompanyBusiness->transformKey('order_total_refund'). '+'. $order_total_refund)
        //];
        //return counterCompanyBusinessModel::query()
        //    ->companyId($companyId)
        //    ->update($updateData);

        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 公司上报订单 减少
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="orderNum"></param>
    /// <param name="orderTotal"></param>
    /// <param name="orderTotalFee"></param>
    /// <param name="orderTotalRefund"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DecrementOrder(int companyId, int orderNum = 0, int orderTotal = 0, double orderTotalFee = 0, double orderTotalRefund = 0)
    {
        //$updateData = [
        //    'order_num'          => DB::raw($this->counterCompanyBusiness->transformKey('order_num'). '-'. $order_num),
        //    'order_total'        => DB::raw($this->counterCompanyBusiness->transformKey('order_total'). '-'. $order_total),
        //    'order_total_fee'    => DB::raw($this->counterCompanyBusiness->transformKey('order_total_fee'). '-'. $order_total_fee),
        //    'order_total_refund' => DB::raw($this->counterCompanyBusiness->transformKey('order_total_refund'). '-'. $order_total_refund)
        //];
        //return counterCompanyBusinessModel::query()
        //    ->companyId($companyId)
        //    ->update($updateData);

        return await Task.FromResult(ReturnArr(true));
    }
}
