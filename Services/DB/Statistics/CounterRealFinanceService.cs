﻿using Microsoft.EntityFrameworkCore;
using ERP.Extensions;
using ERP.Models.Statistics;
using CompanyModel = ERP.Models.DB.Users.Company;
using ERP.Services.DB.Users;
using ERP.Data;
namespace ERP.Services.Statistics;

public class CounterRealFinanceService : BaseService
{
    public readonly DBContext _dbContext;
    private readonly string _month;

    public CounterRealFinanceService(DBContext dbContext)
    {
        _dbContext = dbContext;
        _month = DateTime.Now.ToString("yyyy-MM");
    }

    /// <summary>
    /// 汇总增加上报订单总价
    /// </summary>
    /// <param name="company"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> IncrementOrderTotal(CompanyModel company, double money)
    {
        //if ($money == 0) {
        //    return true;
        //}
        //$res = realFinanceModel::query()
        //    ->companyId($company['id'])
        //    ->oemId($company['oem_id'])
        //    ->month($this->month)
        //    ->increment('order_total', $money);
        //if (!$res) {
        //    $insertData = [
        //        'month'       => $this->month,
        //        'company_id'  => $company['id'],
        //        'oem_id'      => $company['oem_id'],
        //        'order_total' => $money,
        //        'created_at'  => $this->datetime
        //    ];
        //    $res = realFinanceModel::query()->insert($insertData);
        //}
        //return $res;

        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 汇总减少上报订单总价
    /// </summary>
    /// <param name="companyService"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DecrementOrderTotal(CompanyService companyService, double money)
    {
        //if ($money == 0) {
        //    return true;
        //}
        //$res = realFinanceModel::query()
        //    ->CompanyId($company['id'])
        //    ->OemId($company['oem_id'])
        //    ->month($this->month)
        //    ->decrement('order_total', $money);
        //if (!$res) {
        //    $insertData = [
        //        'month'       => $this->month,
        //        'company_id'  => $company['id'],
        //        'oem_id'      => $company['oem_id'],
        //        'order_total' => -$money,
        //        'created_at'  => $this->datetime
        //    ];
        //    $res = realFinanceModel::query()->insert($insertData);
        //}
        //return $res;
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 汇总增加上报订单手续费
    /// </summary>
    /// <param name="company"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> IncrementOrderTotalFee(CompanyModel company, double money)
    {
        //if ($money == 0) {
        //    return true;
        //}
        //$res = realFinanceModel::query()
        //    ->companyId($company['id'])
        //    ->oemId($company['oem_id'])
        //    ->month($this->month)
        //    ->increment('order_total_fee', $money);
        //if (!$res) {
        //    $insertData = [
        //        'month'           => $this->month,
        //        'company_id'      => $company['id'],
        //        'oem_id'          => $company['oem_id'],
        //        'order_total_fee' => $money,
        //        'created_at'      => $this->datetime
        //    ];
        //    $res = realFinanceModel::query()->insert($insertData);
        //}
        //return $res;
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 汇总减少上报订单手续费
    /// </summary>
    /// <param name="company"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DecrementOrderTotalFee(CompanyModel company, double money)
    {
        //if ($money == 0) {
        //    return true;
        //}
        //$res = realFinanceModel::query()
        //    ->CompanyId($company['id'])
        //    ->OemId($company['oem_id'])
        //    ->month($this->month)
        //    ->decrement('order_total_fee', $money);
        //if (!$res) {
        //    $insertData = [
        //        'month'           => $this->month,
        //        'company_id'      => $company['id'],
        //        'oem_id'          => $company['oem_id'],
        //        'order_total_fee' => -$money,
        //        'created_at'      => $this->datetime
        //    ];
        //    $res = realFinanceModel::query()->insert($insertData);
        //}
        //return $res;
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 汇总增加上报订单损失费用
    /// </summary>
    /// <param name="company"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> IncrementOrderLossFee(CompanyModel company, double money)
    {
        //if ($money == 0) {
        //    return true;
        //}
        //$res = realFinanceModel::query()
        //    ->companyId($company['id'])
        //    ->oemId($company['oem_id'])
        //    ->month($this->month)
        //    ->increment('order_total_loss', $money);
        //if (!$res) {
        //    $insertData = [
        //        'month'            => $this->month,
        //        'company_id'       => $company['id'],
        //        'oem_id'           => $company['oem_id'],
        //        'order_total_loss' => $money,
        //        'created_at'       => $this->datetime
        //    ];
        //    $res = realFinanceModel::query()->insert($insertData);
        //}
        //return $res;
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 汇总减少上报订单损失费用
    /// </summary>
    /// <param name="company"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DecrementOrderLossFee(CompanyModel company, double money)
    {
        //if ($money == 0) {
        //    return true;
        //}
        //$res = realFinanceModel::query()
        //    ->CompanyId($company['id'])
        //    ->OemId($company['oem_id'])
        //    ->month($this->month)
        //    ->decrement('order_total_loss', $money);
        //if (!$res) {
        //    $insertData = [
        //        'month'            => $this->month,
        //        'company_id'       => $company['id'],
        //        'oem_id'           => $company['oem_id'],
        //        'order_total_loss' => -$money,
        //        'created_at'       => $this->datetime
        //    ];
        //    $res = realFinanceModel::query()->insert($insertData);
        //}
        //return $res;
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 汇总增加上报订单退款
    /// </summary>
    /// <param name="company"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> IncrementOrderTotalRefund(CompanyModel company, double money)
    {
        //if ($money == 0) {
        //    return true;
        //}
        //$res = realFinanceModel::query()
        //    ->companyId($company['id'])
        //    ->oemId($company['oem_id'])
        //    ->month($this->month)
        //    ->increment('order_total_refund', $money);
        //if (!$res) {
        //    $insertData = [
        //        'month'              => $this->month,
        //        'company_id'         => $company['id'],
        //        'oem_id'             => $company['oem_id'],
        //        'order_total_refund' => $money,
        //        'created_at'         => $this->datetime
        //    ];
        //    $res = realFinanceModel::query()->insert($insertData);
        //}
        //return $res;
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 汇总减少上报订单退款
    /// </summary>
    /// <param name="company"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DecrementOrderTotalRefund(CompanyModel company, double money)
    {
        //if ($money == 0) {
        //    return true;
        //}
        //$res = realFinanceModel::query()
        //    ->CompanyId($company['id'])
        //    ->OemId($company['oem_id'])
        //    ->month($this->month)
        //    ->decrement('order_total_refund', $money);
        //if (!$res) {
        //    $insertData = [
        //        'month'              => $this->month,
        //        'company_id'         => $company['id'],
        //        'oem_id'             => $company['oem_id'],
        //        'order_total_refund' => -$money,
        //        'created_at'         => $this->datetime
        //    ];
        //    $res = realFinanceModel::query()->insert($insertData);
        //}
        //return $res;
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 汇总增加上报采购总价
    /// </summary>
    /// <param name="company"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> IncrementPurchaseTotal(CompanyModel company, double money)
    {
        //if (0 === (int)$money) {
        //    return true;
        //}
        //$res = realFinanceModel::query()
        //    ->companyId($company['id'])
        //    ->oemId($company['oem_id'])
        //    ->month($this->month)
        //    ->increment('purchase_total', $money);
        //if (!$res) {
        //    $insertData = [
        //        'month'          => $this->month,
        //        'company_id'     => $company['id'],
        //        'oem_id'         => $company['oem_id'],
        //        'purchase_total' => $money,
        //        'created_at'     => $this->datetime
        //    ];
        //    $res = realFinanceModel::query()->insert($insertData);
        //}
        //return $res;
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 汇总减少上报采购总价
    /// </summary>
    /// <param name="company"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DecrementPurchaseTotal(CompanyModel company, double money)
    {
        //if ($money == 0) {
        //    return true;
        //}
        //$res = realFinanceModel::query()
        //    ->CompanyId($company['id'])
        //    ->OemId($company['oem_id'])
        //    ->month($this->month)
        //    ->decrement('purchase_total', $money);
        //if (!$res) {
        //    $insertData = [
        //        'month'          => $this->month,
        //        'company_id'     => $company['id'],
        //        'oem_id'         => $company['oem_id'],
        //        'purchase_total' => -$money,
        //        'created_at'     => $this->datetime
        //    ];
        //    $res = realFinanceModel::query()->insert($insertData);
        //}
        //return $res;
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 汇总增加上报采购退款
    /// </summary>
    /// <param name="company"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> IncrementPurchaseTotalRefund(CompanyModel company, double money)
    {
        //if ($money == 0) {
        //    return true;
        //}
        //$res = realFinanceModel::query()
        //    ->companyId($company['id'])
        //    ->oemId($company['oem_id'])
        //    ->month($this->month)
        //    ->increment('purchase_total_refund', $money);
        //if (!$res) {
        //    $insertData = [
        //        'month'                 => $this->month,
        //        'company_id'            => $company['id'],
        //        'oem_id'                => $company['oem_id'],
        //        'purchase_total_refund' => $money,
        //        'created_at'            => $this->datetime
        //    ];
        //    $res = realFinanceModel::query()->insert($insertData);
        //}
        //return $res;
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 汇总减少上报采购退款
    /// </summary>
    /// <param name="company"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DecrementPurchaseTotalRefund(CompanyModel company, double money)
    {
        //if ($money == 0) {
        //    return true;
        //}
        //$res = realFinanceModel::query()
        //    ->CompanyId($company['id'])
        //    ->OemId($company['oem_id'])
        //    ->month($this->month)
        //    ->decrement('purchase_total_refund', $money);
        //if (!$res) {
        //    $insertData = [
        //        'month'                 => $this->month,
        //        'company_id'            => $company['id'],
        //        'oem_id'                => $company['oem_id'],
        //        'purchase_total_refund' => -$money,
        //        'created_at'            => $this->datetime
        //    ];
        //    $res = realFinanceModel::query()->insert($insertData);
        //}
        //return $res;
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 汇总增加上报运单总价
    /// </summary>
    /// <param name="company"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> IncrementWaybillTotal(CompanyModel Compand, double Money)
    {
        //if ($money == 0) {
        //    return true;
        //}
        //$res = realFinanceModel::query()
        //    ->companyId($company['id'])
        //    ->oemId($company['oem_id'])
        //    ->month($this->month)
        //    ->increment('waybill_total', $money);
        //if (!$res) {
        //    $insertData = [
        //        'month'         => $this->month,
        //        'company_id'    => $company['id'],
        //        'oem_id'        => $company['oem_id'],
        //        'waybill_total' => $money,
        //        'created_at'    => $this->datetime
        //    ];
        //    $res = realFinanceModel::query()->insert($insertData);
        //}
        //return $res;
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 汇总减少上报运单总价
    /// </summary>
    /// <param name="company"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DecrementWaybillTotal(CompanyModel company, double money)
    {
        //if ($money == 0) {
        //    return true;
        //}
        //$res = realFinanceModel::query()
        //    ->CompanyId($company['id'])
        //    ->OemId($company['oem_id'])
        //    ->month($this->month)
        //    ->decrement('waybill_total', $money);
        //if (!$res) {
        //    $insertData = [
        //        'month'         => $this->month,
        //        'company_id'    => $company['id'],
        //        'oem_id'        => $company['oem_id'],
        //        'waybill_total' => -$money,
        //        'created_at'    => $this->datetime
        //    ];
        //    $res = realFinanceModel::query()->insert($insertData);
        //}
        //return $res;
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 汇总增加上报运单退款
    /// </summary>
    /// <param name="company"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> IncrementWaybillTotalRefund(CompanyModel company, double money)
    {
        //if ($money == 0) {
        //    return true;
        //}
        //$res = realFinanceModel::query()
        //    ->companyId($company['id'])
        //    ->oemId($company['oem_id'])
        //    ->month($this->month)
        //    ->increment('waybill_total_refund', $money);
        //if (!$res) {
        //    $insertData = [
        //        'month'                => $this->month,
        //        'company_id'           => $company['id'],
        //        'oem_id'               => $company['oem_id'],
        //        'waybill_total_refund' => $money,
        //        'created_at'           => $this->datetime
        //    ];
        //    $res = realFinanceModel::query()->insert($insertData);
        //}
        //return $res;
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 汇总减少上报运单退款
    /// </summary>
    /// <param name="company"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DecrementWaybillTotalRefund(CompanyModel company, double money)
    {
        //if ($money == 0) {
        //    return true;
        //}
        //$res = realFinanceModel::query()
        //    ->CompanyId($company['id'])
        //    ->OemId($company['oem_id'])
        //    ->month($this->month)
        //    ->decrement('waybill_total_refund', $money);
        //if (!$res) {
        //    $insertData = [
        //        'month'                => $this->month,
        //        'company_id'           => $company['id'],
        //        'oem_id'               => $company['oem_id'],
        //        'waybill_total_refund' => -$money,
        //        'created_at'           => $this->datetime
        //    ];
        //    $res = realFinanceModel::query()->insert($insertData);
        //}
        //return $res;
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 自用钱包充值汇总
    /// </summary>
    /// <param name="company"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> SelfRechargeTotal(CompanyModel company, decimal money)
    {
        //if ($money == 0) {
        //    return true;
        //}
        //$res = realFinanceModel::query()
        //    ->companyId($company['id'])
        //    ->oemId($company['oem_id'])
        //    ->month($this->month)
        //    ->increment('self_recharge_total', $money);
        //if (!$res) {
        //    $insertData = [
        //        'month'               => $this->month,
        //        'company_id'          => $company['id'],
        //        'oem_id'              => $company['oem_id'],
        //        'self_recharge_total' => $money,
        //        'created_at'          => $this->datetime
        //    ];
        //    $res = realFinanceModel::query()->insert($insertData);
        //}
        //return $res;
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 自用钱包支出汇总
    /// </summary>
    /// <param name="company"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> SelfExpenseTotal(CompanyModel company, decimal money)
    {
        //if ($money == 0) {
        //    return true;
        //}
        //$res = realFinanceModel::query()
        //    ->companyId($company['id'])
        //    ->oemId($company['oem_id'])
        //    ->month($this->month)
        //    ->increment('self_expense_total', $money);
        //if (!$res) {
        //    $insertData = [
        //        'month'              => $this->month,
        //        'company_id'         => $company['id'],
        //        'oem_id'             => $company['oem_id'],
        //        'self_expense_total' => $money,
        //        'created_at'         => $this->datetime
        //    ];
        //    $res = realFinanceModel::query()->insert($insertData);
        //}
        //return $res;
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 对账钱包充值汇总
    /// </summary>
    /// <param name="company"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> RechargeTotal(CompanyModel company, double money)
    {
        //    if ($money == 0) {
        //        return true;
        //    }
        //    $res = realFinanceModel::query()
        //        ->companyId($company['id'])
        //        ->oemId($company['oem_id'])
        //        ->month($this->month)
        //        ->increment('recharge_total', $money);
        //    if (!$res) {
        //        $insertData = [
        //            'month'          => $this->month,
        //            'company_id'     => $company['id'],
        //            'oem_id'         => $company['oem_id'],
        //            'recharge_total' => $money,
        //            'created_at'     => $this->datetime
        //        ];
        //        $res = realFinanceModel::query()->insert($insertData);
        //    }
        //    return $res;
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 对账钱包支出汇总
    /// </summary>
    /// <param name="company"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ExpenseTotal(CompanyModel company, double money)
    {
        //if ($money == 0) {
        //    return true;
        //}
        //$res = realFinanceModel::query()
        //    ->companyId($company['id'])
        //    ->oemId($company['oem_id'])
        //    ->month($this->month)
        //    ->increment('expense_total', $money);
        //if (!$res) {
        //    $insertData = [
        //        'month'         => $this->month,
        //        'company_id'    => $company['id'],
        //        'oem_id'        => $company['oem_id'],
        //        'expense_total' => $money,
        //        'created_at'    => $this->datetime
        //    ];
        //    $res = realFinanceModel::query()->insert($insertData);
        //}
        //return $res;
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 汇总增加上报订单、采购总价
    /// </summary>
    /// <param name="company"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> IncrementOrderPurchaseTotal(CompanyModel company, double money)
    {
        //if ($money == 0) {
        //    return true;
        //}

        //$updateData = [
        //    'order_total'    => DB::raw($this->realFinanceModel->transformKey('order_total'). '+'. $money),
        //    'purchase_total' => DB::raw($this->realFinanceModel->transformKey('purchase_total'). '+'. $money)
        //];
        //$res = realFinanceModel::query()
        //    ->companyId($company['id'])
        //    ->oemId($company['oem_id'])
        //    ->month($this->month)
        //    ->update($updateData);
        //if (!$res) {
        //    $insertData = [
        //        'month'          => $this->month,
        //        'company_id'     => $company['id'],
        //        'oem_id'         => $company['oem_id'],
        //        'order_total'    => $money,
        //        'purchase_total' => $money,
        //        'created_at'     => $this->datetime
        //    ];
        //    $res = realFinanceModel::query()->insert($insertData);
        //}
        //return $res;
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 汇总增加上报订单
    /// </summary>
    /// <param name=""></param>
    /// <param name=""></param>
    /// <param name=""></param>
    /// <param name=""></param>
    /// <param name=""></param>
    /// <param name=""></param>
    /// <param name=""></param>
    /// <returns></returns>
    public async Task<ReturnStruct> IncrementOrder(CompanyModel company, int orderTotal = 0, int orderTotalFee = 0, int orderTotalRefund = 0)
    {
        //if ($order_total == 0 && $order_total_fee == 0 && $order_total_refund == 0) {
        //    return true;
        //}
        //$updateData = [
        //    'order_total'        => DB::raw($this->realFinanceModel->transformKey('order_total'). '+'. $order_total),
        //    'order_total_fee'    => DB::raw($this->realFinanceModel->transformKey('order_total_fee'). '+'. $order_total_fee),
        //    'order_total_refund' => DB::raw($this->realFinanceModel->transformKey('order_total_refund'). '+'. $order_total_refund)
        //];
        //$res = realFinanceModel::query()
        //    ->companyId($company['id'])
        //    ->oemId($company['oem_id'])
        //    ->month($this->month)
        //    ->update($updateData);
        //if (!$res) {
        //    $insertData = [
        //        'month'              => $this->month,
        //        'company_id'         => $company['id'],
        //        'oem_id'             => $company['oem_id'],
        //        'order_total'        => $order_total,
        //        'order_total_fee'    => $order_total_fee,
        //        'order_total_refund' => $order_total_refund,
        //        'created_at'         => $this->datetime
        //    ];
        //    $res = realFinanceModel::query()->insert($insertData);
        //}
        //return $res;
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 汇总减少上报订单
    /// </summary>
    /// <param name="company"></param>
    /// <param name="orderTotal"></param>
    /// <param name="orderTotalFee"></param>
    /// <param name="orderTotalRefund"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DecrementOrder(CompanyModel company, int orderTotal = 0, int orderTotalFee = 0, int orderTotalRefund = 0)
    {
        //if ($order_total == 0 && $order_total_fee == 0 && $order_total_refund == 0) {
        //    return true;
        //}

        //$updateData = [
        //    'order_total'        => DB::raw($this->realFinanceModel->transformKey('order_total'). '-'. $order_total),
        //    'order_total_fee'    => DB::raw($this->realFinanceModel->transformKey('order_total_fee'). '-'. $order_total_fee),
        //    'order_total_refund' => DB::raw($this->realFinanceModel->transformKey('order_total_refund'). '-'. $order_total_refund)
        //];
        //$res = realFinanceModel::query()
        //    ->companyId($company['id'])
        //    ->oemId($company['oem_id'])
        //    ->month($this->month)
        //    ->update($updateData);
        //if (!$res) {
        //    $insertData = [
        //        'month'              => $this->month,
        //        'company_id'         => $company['id'],
        //        'oem_id'             => $company['oem_id'],
        //        'order_total'        => $order_total,
        //        'order_total_fee'    => $order_total_fee,
        //        'order_total_refund' => $order_total_refund,
        //        'created_at'         => $this->datetime
        //    ];
        //    $res = realFinanceModel::query()->insert($insertData);
        //}
        //return $res;
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 公司月度报表
    /// </summary>
    /// <param name="month"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> GetFinance(string month = "")
    {
        //    $field = ['month', 'order_total', 'order_total_fee', 'order_total_refund', 'order_total_loss', 'purchase_total', 'purchase_total_refund', 'waybill_total', 'waybill_total_refund', 'self_recharge_total', 'self_expense_total', 'recharge_total', 'expense_total'];
        //    return realFinanceModel::query()
        //        ->companyId($this->getSession()->getCompanyId())
        //        ->oemId($this->getSession()->getOemId())
        //        ->month($month)
        //        ->select($field)
        //        ->paginate($this->limit);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 获取指定公司月度报表
    /// </summary>
    /// <param name="month"></param>
    /// <param name="companyId"></param>
    /// <param name="company_ids"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> GetFastReport(string month, int companyId, List<int> company_ids)
    {
        //    $field = ['id', 'company_id', 'month', 'order_total', 'order_total_fee', 'order_total_refund', 'purchase_total', 'purchase_total_refund', 'waybill_total', 'waybill_total_refund', 'recharge_total', 'expense_total'];
        //    return realFinanceModel::query()
        //        ->companyId($company_id)
        //        ->InCompanyId($company_ids)
        //        ->month($month)
        //        ->select($field)
        //        ->paginate($this->limit);
        return await Task.FromResult(ReturnArr(true));
    }
}

