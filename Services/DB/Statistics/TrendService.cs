﻿using Microsoft.EntityFrameworkCore;
using ERP.Extensions;
using ERP.Models.Statistics;
using ERP.ViewModels.Statistics;
using ERP.Data;
using ERP.Enums.Identity;
using ERP.Enums.Statistics;
using ERP.Models.DB.Users;
using ERP.Models.Users;
using NPOI.SS.Formula.Functions;
using Ryu.Extensions;
using Ryu.Linq;

namespace ERP.Services.Statistics
{
    public class TrendService : BaseService
    {
        readonly DBContext _dbContext;

        public TrendService(DBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<DbSetExtension.PaginateStruct<UserOnlineModel>> GetOnlineUser(string? username)
        {
            return await _dbContext.UserOnline.WhenWhere(!string.IsNullOrEmpty(username), m => m.Username == username)
                .ToPaginateAsync();
        }

        public async Task<object> GetAccount(AccountDto req)
        {
            //$field = ['id', 'username as username', 'truename', 'concierge', 'mobile', 'pid', 'level', 'is_distribution', 'created_at', 'first_login_web', 'first_login_exe'];

            //$list = UsersModel::query()->select($field)->userName($username)->Mobile($mobile)->orderBy('id', 'desc')->paginate($this->limit);
            //$tempPidArr = collect($list->items())->map(function($item) {
            //        return [
            //            'id' => $item->pid
            //        ];
            //    });
            //$pidArr = array_unique(array_column($tempPidArr->toArray(), 'id'));

            //$field = ['id', 'username', 'truename'];
            //$parentArr = UsersModel::query()->select($field)->whereIn('id', $pidArr)->get();
            //$parentArr = arrayCombine($parentArr->toArray(), "id");

            //    collect($list->items())->each(function($item) use($parentArr) {
            //        if (isset($parentArr[$item->pid]))
            //        {
            //        $currentParent = $parentArr[$item->pid];
            //        $item->create_user = $currentParent["username"];
            //        $item->create_username = $currentParent["truename"];
            //        $item->first_login_web = checkStr($item->first_login_web) ? $item->first_login_web : '暂未登录';
            //        $item->first_login_exe = checkStr($item->first_login_exe) ? $item->first_login_exe : '暂未登录';
            //        }
            //        else
            //        {
            //        $item->create_user = '暂无信息';
            //        $item->create_username = '暂无信息';
            //        $item->first_login_web = '暂未登录';
            //        $item->first_login_exe = '暂未登录';
            //        }
            //    });
            //    return $list;

            var query = from u in _dbContext.User
                join pu in _dbContext.User
                    on u.Pid equals pu.Id
                select new
                {
                    User = u,
                    Puser = pu
                };

            return query
                .WhenWhere(req.Name.IsNotEmpty(), m => req.Name.Contains(m.User.UserName))
                .WhenWhere(req.Mobile.IsNotEmpty(), m => m.User.PhoneNumber == req.Mobile)
                .OrderByDescending(m => m.User.Id)
                .Select(m => new AccountViewModel()
                {
                    Role = Ryu.Extensions.EnumExtension.GetDescription(m.User.Role),
                    CreateUser = m.Puser.UserName,
                    CreateUsername = m.Puser.TrueName,
                    CratedAt = m.User.CreatedAt,
                    FirstLoginWeb = m.User.FirstLoginWeb.IsNull() ? "暂未登录" : m.User.FirstLoginWeb.ToString(),
                    Id = m.User.ID,
                    IsDistribution = m.User.IsDistribution,
                    Mobile = m.User.Mobile,
                    OemId = m.User.OEMID,
                    Username = m.User.UserName,
                    Truename = m.User.TrueName
                }).ToPaginate(page: req.Page, limit: req.Limit);
        }

        public async Task<ReturnStruct> GetExpenses(string username)
        {
            //return CounterRequest::query()->username($username ?? '')->paginate($this->limit);
            return await Task.FromResult(ReturnArr(true));
        }

        public async Task<object?> CompanyCount(CompanyCountDto? req)
        {
            var query = from c in _dbContext.Company
                join s in _dbContext.Statistic
                    on c.ID equals s.AboutID
                where (s.AboutType & Types.Role) == Types.Company && (s.AboutType & Types.Effective) == Types.TOTAL
                select new
                {
                    company = new { c.ID, c.Name },
                    statistic = new
                    {
                        s.UserCount,
                        s.EmployeeCount,
                        s.CompanyCount,
                        s.ProductCount,
                        s.StoreCount,
                        s.OrderCount,
                        s.PurchaseCount,
                        s.WaybillCount
                    }
                };

            return query
                .WhenWhere(req.Name.IsNotEmpty(), m => req.Name.Contains(m.company.Name))
                .Select(m => new
                {
                    m.company.ID,
                    m.company.Name,
                    m.statistic.UserCount,
                    m.statistic.EmployeeCount,
                    m.statistic.CompanyCount,
                    m.statistic.ProductCount,
                    m.statistic.StoreCount,
                    m.statistic.OrderCount,
                    m.statistic.PurchaseCount,
                    m.statistic.WaybillCount
                }).ToPaginate(page: req.Page, limit: req.Limit);
        }

        public async Task<object?> UserCount(UserCountDto req)
        {
            var query = from u in _dbContext.User
                join s in _dbContext.Statistic
                    on u.Id equals s.AboutID
                where (s.AboutType & Types.Role) == Types.User
                      && (s.AboutType & Types.Effective) == Types.TOTAL
                      && u.CompanyID == req.CompanyId
                select new
                {
                    user = new { u.ID, u.TrueName },
                    statistic = new
                    {
                        s.UserCount,
                        s.EmployeeCount,
                        s.ProductCount,
                        s.StoreCount,
                        s.OrderCount,
                        s.PurchaseCount,
                        s.WaybillCount
                    }
                };

            return query
                .Select(m => new
                {
                    m.user.ID,
                    Name = m.user.TrueName,
                    m.statistic.UserCount,
                    m.statistic.EmployeeCount,
                    m.statistic.ProductCount,
                    m.statistic.StoreCount,
                    m.statistic.OrderCount,
                    m.statistic.PurchaseCount,
                    m.statistic.WaybillCount
                }).ToPaginate(page: req.Page, limit: req.Limit);
        }

        public async Task<object?> CompanyList()
        {
            return await _dbContext.Company.Where(m => m.State == Company.States.ENABLE)
                .Select(m => new { ID = m.ID, Name = m.Name }).ToListAsync();
        }

        public async Task<DbSetExtension.PaginateStruct<UserOperateLog>> GetAccountLog(AccountLogDto req)
        {
            return await _dbContext.UserOperateLog.Where(m => m.UserID == req.Id)
                .OrderByDescending(m => m.ID)
                .ToPaginateAsync(page: req.Page, limit: req.Limit);
        }
    }
}