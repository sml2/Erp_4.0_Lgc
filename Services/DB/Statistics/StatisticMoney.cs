using ERP.Data;
using ERP.Models.DB.Logistics;
using ERP.Services.DB.Statistics;
using ERP.ViewModels.Statistics;
using Order = ERP.Models.DB.Orders.Order;

namespace ERP.Services.Statistics;

public partial class StatisticMoney
{
    private readonly Statistic _statisticService;


    public StatisticMoney()
    {
    }

    public StatisticMoney(Statistic statisticService)
    {
        _statisticService = statisticService;
    }

}