﻿using System.Linq;
using ERP.Enums.Statistics;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.DB.Statistics;
using ERP.Models.Statistics;
using ERP.ViewModels.Statistics;
using Microsoft.EntityFrameworkCore;
using ERP.Data;
using ERP.Distribution.Abstraction;

namespace ERP.Services.Statistics;

public class CounterUserBusinessService : BaseService
{
    public readonly DBContext _MyDBContext;
    private readonly ISessionProvider _SessionProvider;
    public CounterUserBusinessService(DBContext dbContext, ISessionProvider SessionProvider)
    {
        _SessionProvider = SessionProvider;
        _MyDBContext = dbContext;
    }
    private ISession Session { get => _SessionProvider.Session!; }
    private int UserID { get => Session.GetUserID(); }
    private int GroupID { get => Session.GetGroupID(); }
    private int CompanyID { get => Session.GetCompanyID(); }
    private int OEMID { get => Session.GetOEMID(); }

    /// <summary>
    /// 初始化统计数据
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="companyId"></param>
    /// <param name="oemId"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> InitCounterUserBusiness(int userId, int companyId, int oemId)
    {
        //$insertData = [
        //    'user_id'    => $user_id,
        //    'company_id' => $company_id,
        //    'oem_id'     => $oem_id,
        //    'created_at' => $this->datetime
        //];
        //$this->counterUserBusiness::query()->insert($insertData);

        var insertData = new CounterUserBusiness() { UserID = userId, CompanyID = companyId, OEMID = oemId };

        await _MyDBContext.CounterUserBusiness.AddAsync(insertData);
        await _MyDBContext.SaveChangesAsync();
        return ReturnArr(true);
    }

    /// <summary>
    /// 获取指定用户id业务统计
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="userBusiness"></param>
    /// <returns></returns>
    public async Task<HomePageVM?> GetCountUserById(int userId, bool userBusiness = true)
    {
        //if (userBusiness) {
        //    $field = ['user_id', 'user_num', 'product_num', 'order_num', 'purchase_num', 'waybill_num', 'store_num', 'distribution_order_num', 'distribution_purchase_num', 'distribution_waybill_num'];
        //} else
        //{
        //    $field = ['user_id', 'user_num', 'product_num', 'order_num', 'purchase_num', 'waybill_num', 'store_num'];
        //}
        //return $this->counterUserBusiness::query()->select($field)->where('user_id', int id)->first();
        HomePageVM? model;
        if (userBusiness)
        {
            model = await _MyDBContext.Statistic
                .Where(m => 
                    m.AboutID == userId 
                    && (m.AboutType & Types.Role) == Types.User
                    && (m.AboutType & Types.Effective) == Types.TOTAL
                    )
                .Select(m => new HomePageVM
                {
                    UserId = m.AboutID,
                    UserNum = m.UserCount + m.EmployeeCount,
                    ProductNum = m.ProductCount,
                    OrderNum = m.OrderCount,
                    PurchaseNum = m.PurchaseCount,
                    WaybillNum = m.WaybillCount,
                    StoreNum = m.StoreCount,
                    DistributionOrderNum = m.DistributionOrderCount,
                    DistributionPurchaseNum = m.DistributionPurchaseCount,
                    DistributionWaybillNum = m.DistributionWaybillCount
                }).FirstOrDefaultAsync();
        }
        else
        {
            model = await _MyDBContext.Statistic
                .Where(m =>
                    m.AboutID == userId
                    && (m.AboutType & Types.Role) == Types.User
                    && (m.AboutType & Types.Effective) == Types.TOTAL
                )
                .Select(m => new HomePageVM
                {
                    UserId = m.AboutID,
                    UserNum = m.UserCount + m.EmployeeCount,
                    ProductNum = m.ProductCount,
                    OrderNum = m.OrderCount,
                    PurchaseNum = m.PurchaseCount,
                    WaybillNum = m.WaybillCount,
                    StoreNum = m.StoreCount,
                }).FirstOrDefaultAsync();
        }

        return model;
    }

    /// <summary>
    /// 获取指定用户ids业务统计
    /// </summary>
    /// <param name="ids"></param>
    /// <param name="userBusiness"></param>
    /// <returns></returns>
    public async Task<List<HomePageVM>> getCountUserByIds(List<int> ids, bool userBusiness = true)
    {
        //if ($userBusiness) {
        //    $field = ['user_id', 'user_num', 'product_num', 'order_num', 'purchase_num', 'waybill_num', 'store_num', 'distribution_order_num', 'distribution_purchase_num', 'distribution_waybill_num'];
        //} else
        //{
        //    $field = ['user_id', 'user_num', 'product_num', 'order_num', 'purchase_num', 'waybill_num', 'store_num'];
        //}
        //return $this->counterUserBusiness::query()->inUserId(List<int> ids)->select($field)->get();
        // _MyDBContext.Statistic.Where(m => ids.Contains(m.));
        IQueryable<Statistic> model = _MyDBContext.Statistic
            .Where(m =>
                ids.Contains(m.AboutID)
                && (m.AboutType & Types.Role) == Types.User
                && (m.AboutType & Types.Effective) == Types.TOTAL
            );
        if (userBusiness)
        {
            return await model.Select(m => new HomePageVM
            {
                UserId = m.AboutID,
                UserNum = m.UserCount,
                ProductNum = m.ProductCount,
                OrderNum = m.OrderCount,
                PurchaseNum = m.PurchaseCount,
                WaybillNum = m.WaybillCount,
                StoreNum= m.StoreCount,
                DistributionOrderNum = m.DistributionOrderCount,
                DistributionPurchaseNum = m.DistributionPurchaseCount,
                DistributionWaybillNum = m.DistributionWaybillCount
            }).ToListAsync();
        }

        return await model.Select(m => new HomePageVM
        {
            UserId = m.AboutID,
            UserNum = m.UserCount,
            ProductNum = m.ProductCount,
            OrderNum = m.OrderCount,
            PurchaseNum = m.PurchaseCount,
            WaybillNum = m.WaybillCount,
            StoreNum= m.StoreCount,
        }).ToListAsync();
    }

    /// <summary>
    /// 获取指定员工财务统计信息
    /// </summary>
    /// <param name="userIds"></param>
    /// <returns></returns>
    public async Task<List<UserFinanceDto>> GetFinance(IEnumerable<int> userIds)
    {
        var list = await _MyDBContext.Statistic
            .Where(m =>
                userIds.Contains(m.AboutID)
                && (m.AboutType & Types.Role) == Types.User
                && (m.AboutType & Types.Effective) == Types.TOTAL
            )
                .Select(m => new UserFinanceDto(m.AboutID, m.OrderTotal,
                m.OrderTotalFee,
                m.OrderTotalRefund,
                m.OrderTotalLoss,
                m.PurchaseTotal,
                m.PurchaseTotalRefund,
                m.WaybillTotal,
                m.WaybillTotalRefund,
                m.RechargeTotal,
                m.ExpenseTotal,
                m.SelfRechargeTotal,
                m.SelfExpenseTotal))
                .ToListAsync();

        return list;
    }

    /// <summary>
    /// 增加当前用户创建开通用户数
    /// </summary>
    /// <param name="count"></param>
    /// <param name="userId"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> UserNumAdd(int userNum = 1, int userId = 0)
    {
        if (userId == 0)
        {
            userId = UserID;
        }
        var c = await _MyDBContext.CounterUserBusiness.FirstOrDefaultAsync(a => a.UserID == userId);
        if (c != null)
        {   
            c.UserNum += userNum;
            _MyDBContext.CounterUserBusiness.Update(c);
            await _MyDBContext.SaveChangesAsync();
            return ReturnArr(true);
        }
        return ReturnArr(false);
    }

    /// <summary>
    /// 增加当前用户创建产品数
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ProductNumAdd(int count = 1)
    {
        //return $this->counterUserBusiness
        //    ->userId($this->getSession()->getUserId())
        //    ->increment('product_num', $product_num);
        return await Task.FromResult(ReturnArr(true));;
    }

    /// <summary>
    /// 减少当前用户建产品数
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ProductNumReduce(int userId, int count = 1)
    {
        //$userId == '' && $userId = $this->getSession()->getUserId();
        //return CounterUserBusinessModel::query()
        //    ->userId($userId)
        //    ->decrement('product_num', $product_num);
        return await Task.FromResult(ReturnArr(true));;
    }

    /// <summary>
    /// 增加当前用户创建订单数
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> OrderNumAdd(int count = 1)
    {
        //return $this->counterUserBusiness
        //    ->userId($this->getSession()->getUserId())
        //    ->increment('order_num', $order_num);
        return await Task.FromResult(ReturnArr(true));;
    }

    /// <summary>
    /// 增加当前用户创建采购数
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> PurchaseNumAdd(int count = 1)
    {
        //return $this->counterUserBusiness
        //    ->userId($this->getSession()->getUserId())
        //    ->increment('purchase_num', $purchase_num);
        return await Task.FromResult(ReturnArr(true));;
    }

    /// <summary>
    /// 增加当前用户创建运单数
    /// </summary>
    /// <param name="waybillNum"></param>
    /// <param name="distributionWaybillNum"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> WaybillNumAdd(int waybillNum = 1, int distributionWaybillNum = 0)
    {
        var Business =await _MyDBContext.CounterUserBusiness.SingleOrDefaultAsync(x => x.ID ==UserID);
        if (Business != null)
        {
            Business.WaybillNum += waybillNum;
            Business.DistributionOrderNum += distributionWaybillNum;
            return ReturnArr(true);
        }
        return ReturnArr(false);
    }

    /// <summary>
    /// 增加当前用户创建店铺数
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> StoreNumAdd(int count = 1)
    {
        //return $this->counterUserBusiness
        //    ->userId($this->getSession()->getUserId())
        //    ->increment('store_num', $store_num);


        var Group = await _MyDBContext.CounterUserBusiness.Where(x => x.UserID == UserID).FirstOrDefaultAsync();
        if (Group != null)
            Group.StoreNum += count;
        _MyDBContext.SaveChanges();
        return ReturnArr(true);
    }

    /// <summary>
    /// 减少当前用户创建店铺数
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> StoreNumReduce(int count = 1)
    {
        //return $this->counterUserBusiness
        //    ->userId($this->getSession()->getUserId())
        //    ->decrement('store_num', $store_num);
        return await Task.FromResult(ReturnArr(true));;
    }

    /// <summary>
    /// 增加当前用户处理上报分销订单数
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DistributionOrderNumAdd(int count = 1)
    {
        //return $this->counterUserBusiness
        //    ->userId($this->getSession()->getUserId())
        //    ->increment('distribution_order_num', $distribution_order_num);
        return await Task.FromResult(ReturnArr(true));;
    }

    /// <summary>
    /// 增加当前用户处理上报分销采购数
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DistributionPurchaseNumAdd(int count = 1)
    {
        //return $this->counterUserBusiness
        //    ->userId($this->getSession()->getUserId())
        //    ->increment('distribution_purchase_num', $distribution_purchase_num);
        return await Task.FromResult(ReturnArr(true));;
    }

    /// <summary>
    /// 增加当前用户处理上报分销运单数
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DistributionWaybillNumAdd(int count = 1)
    {
        //return $this->counterUserBusiness
        //    ->userId($this->getSession()->getUserId())
        //    ->increment('distribution_waybill_num', $distribution_waybill_num);
        return await Task.FromResult(ReturnArr(true));;
    }

    /// <summary>
    /// 增加当前用户处理运费
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DistributionWaybillTotalAdd(int? userId, double count)
    {

        //$user_id == '' && $user_id = $this->getSession()->getUserId();
        //return $this->counterUserBusiness
        //    ->userId($user_id)
        //    ->increment('waybill_total', $waybill_total);
        return await Task.FromResult(ReturnArr(true));;
    }

    /// <summary>
    /// 同时增加处理上报分销运单数、运费
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="waybillTotal"></param>
    /// <param name="distributionWaybillNum"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DistributionWaybillNumAndTotal(int userId, int waybillTotal = 0, int distributionWaybillNum = 1)
    {
        //$user_id == '' && $user_id = $this->getSession()->getUserId();
        //$updateData = [
        //    'distribution_waybill_num' => DB::raw($this->counterUserBusiness->transformKey('distribution_waybill_num'). '+'. $distribution_waybill_num),
        //    'waybill_total'            => DB::raw($this->counterUserBusiness->transformKey('waybill_total'). '+'. $waybill_total)
        //];
        //return $this->counterUserBusiness
        //    ->userId($user_id)
        //    ->update($updateData);
        return await Task.FromResult(ReturnArr(true));;
    }

    /// <summary>
    /// 同时增加当前用户创建订单数、采购数、订单总价、采购总价
    /// </summary>
    /// <param name="orderNum"></param>
    /// <param name="purchase_num"></param>
    /// <param name="orderTotal"></param>
    /// <param name="purchaseTotal"></param>
    /// <param name="userId"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> OrderPurchaseNumAdd(int orderNum = 1, int purchase_num = 1, int orderTotal = 0, int purchaseTotal = 0, int userId = 0)
    {
        //$userId = $userId ?: $this->getSession()->getUserId();
        //$updateData = [
        //    'order_num'    => DB::raw($this->counterUserBusiness->transformKey('order_num'). '+'. $order_num),
        //    'purchase_num' => DB::raw($this->counterUserBusiness->transformKey('purchase_num'). '+'. $purchase_num)
        //];
        //$order_total != 0 && $updateData['order_total'] = DB::raw($this->counterUserBusiness->transformKey('order_total'). '+'. $order_total);
        //$purchase_total != 0 && $updateData['purchase_total'] = DB::raw($this->counterUserBusiness->transformKey('purchase_total'). '+'. $purchase_total);
        //return CounterUserBusinessModel::userId($userId)
        //    ->update($updateData);

        return await Task.FromResult(ReturnArr(true));;
    }

    /// <summary>
    /// 同时增加当前用户处理上报订单数、采购数
    /// </summary>
    /// <param name="orderNum"></param>
    /// <param name="purchaseNum"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DistributionOrderPurchaseNumAdd(int orderNum = 1, int purchaseNum = 1)
    {
        //$updateData = [
        //    'distribution_order_num'    => DB::raw($this->counterUserBusiness->transformKey('distribution_order_num'). '+'. $order_num),
        //    'distribution_purchase_num' => DB::raw($this->counterUserBusiness->transformKey('distribution_purchase_num'). '+'. $purchase_num),
        //];
        //return $this->counterUserBusiness
        //    ->userId($this->getSession()->getUserId())
        //    ->update($updateData);
        return await Task.FromResult(ReturnArr(true));;
    }


    /// <summary>
    /// 同时更新采购数和采购总额
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="total"></param>
    /// <param name="number"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ChangePurchaseNumAndTotal(int userId, double total = 0.00, int number = 1)
    {
        //$updateUser = [
        //    'purchase_num'   => DB::raw($this->counterUserBusiness->transformKey('purchase_num'). '+'. $number),
        //    'purchase_total' => DB::raw($this->counterUserBusiness->transformKey('purchase_total'). '+'. $total),
        //];
        //return $this->counterUserBusiness
        //    ->userId($userId)
        //    ->update($updateUser);
        return await Task.FromResult(ReturnArr(true));;

    }

    /// <summary>
    /// 同时更新分销采购数和采购总额
    /// </summary>
    /// <param name="total"></param>
    /// <param name="userId"></param>
    /// <param name="number"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DistributionPurchaseNumAndTotal(int userId, int total = 0, int number = 1)
    {
        //$userId == '' && $userId = $this->getSession()->getUserId();
        //$updateUser = [
        //    'distribution_purchase_num' => DB::raw($this->counterUserBusiness->transformKey('distribution_purchase_num'). '+'. $number),
        //    'purchase_total'            => DB::raw($this->counterUserBusiness->transformKey('purchase_total'). '+'. $total),
        //];
        //return $this->counterUserBusiness
        //    ->userId($userId)
        //    ->update($updateUser);
        return await Task.FromResult(ReturnArr(true));;
    }

    /// <summary>
    /// 采购汇总
    /// </summary>
    /// <param name="total"></param>
    /// <param name="userId"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> PurchaseTotalAdd(int total, int userId)
    {
        //$user_id == '' && $user_id = $this->getSession()->getUserId();
        //return $this->counterUserBusiness
        //    ->userId($user_id)
        //    ->increment('purchase_total', $total);

        return await Task.FromResult(ReturnArr(true));;
    }

    /// <summary>
    /// 指定用户充值汇总
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="recharge"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> RechargeNumAdd(int userId, double recharge)
    {
        //return $this->counterUserBusiness
        //    ->userId($user_id)
        //    ->increment('recharge_total', $recharge);
        return await Task.FromResult(ReturnArr(true));;
    }

    /// <summary>
    /// 指定用户充值汇总
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="recharge"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> SelfRechargeNumAdd(int userId, decimal recharge)
    {
        //return $this->counterUserBusiness
        //    ->userId($user_id)
        //    ->increment('self_recharge_total', $recharge);
        return await Task.FromResult(ReturnArr(true));;
    }

    /// <summary>
    /// 指定用户支出汇总
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="expense"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ExpenseNumAdd(int userId, double expense)
    {
        //return $this->counterUserBusiness
        //    ->userId($user_id)
        //    ->increment('expense_total', $expense);
        return await Task.FromResult(ReturnArr(true));;
    }

    /// <summary>
    /// 账单用户汇总 增加
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> AddOrderTotalRefund(int userId, double money)
    {
        //return $this->counterUserBusiness
        //    ->userId($user_id)
        //    ->increment('order_total_refund', $money);
        return await Task.FromResult(ReturnArr(true));;
    }

    /// <summary>
    /// 账单用户汇总 减少
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ReduceOrderTotalRefund(int userId, double money)
    {
        //return $this->counterUserBusiness
        //    ->userId($user_id)
        //    ->decrement('order_total_refund', $money);
        return await Task.FromResult(ReturnArr(true));;
    }

    /// <summary>
    /// 账单用户汇总 增加 手续费
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> AddOrderTotalFee(int userId, double money)
    {
        //return $this->counterUserBusiness
        //    ->userId($user_id)
        //    ->increment('order_total_fee', $money);
        return await Task.FromResult(ReturnArr(true));;
    }

    /// <summary>
    /// 账单用户汇总 减少 手续费
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ReduceOrderTotalFee(int userId, double money)
    {
        //return $this->counterUserBusiness
        //    ->userId($user_id)
        //    ->decrement('order_total_fee', $money);
        return await Task.FromResult(ReturnArr(true));;
    }

    public async Task<ReturnStruct> StoreUserNumReduce(int Num, int UserId)
    {
        var data = await _MyDBContext.CounterUserBusiness.Where(x => x.UserID == UserId).FirstOrDefaultAsync();
        if (data != null)
        {
            data.StoreNum = data.StoreNum - Num;                  ///************    decrement()
            await _MyDBContext.SaveChangesAsync();
            return ReturnArr(true);
        }
        return ReturnArr(false);
    }
    /// <summary>
    /// 账单用户汇总 增加 订单损失费用
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> AddOrderLossFee(int userId, double money)
    {
        //return CounterUserBusinessModel::query()
        //    ->userId($user_id)
        //    ->increment('order_total_loss', $money);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 账单用户汇总 减少 订单损失费用
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ReduceOrderLossFee(int userId, double money)
    {
        //return CounterUserBusinessModel::query()
        //    ->userId($user_id)
        //    ->decrement('order_total_loss', $money);
        return await Task.FromResult(ReturnArr(true));;
    }

    /// <summary>
    /// 用户上报订单
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="orderNum"></param>
    /// <param name="orderTotal"></param>
    /// <param name="orderTotalFee"></param>
    /// <param name="orderTotalRefund"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> IncrementOrder(int userId, int orderNum = 0, int orderTotal = 0, int orderTotalFee = 0, int orderTotalRefund = 0)
    {
        //$updateData = [
        //    'order_num'          => DB::raw($this->counterUserBusiness->transformKey('order_num'). '+'. $order_num),
        //    'order_total'        => DB::raw($this->counterUserBusiness->transformKey('order_total'). '+'. $order_total),
        //    'order_total_fee'    => DB::raw($this->counterUserBusiness->transformKey('order_total_fee'). '+'. $order_total_fee),
        //    'order_total_refund' => DB::raw($this->counterUserBusiness->transformKey('order_total_refund'). '+'. $order_total_refund)
        //];
        //return $this->counterUserBusiness::query()
        //    ->userId($user_id)
        //    ->update($updateData);
        return await Task.FromResult(ReturnArr(true));;
    }

    /// <summary>
    /// 用户上报订单 减少
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="orderNum"></param>
    /// <param name="orderTotal"></param>
    /// <param name="orderTotalFee"></param>
    /// <param name="orderTotalRefund"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DecrementOrder(int userId, int orderNum = 0, int orderTotal = 0, int orderTotalFee = 0, int orderTotalRefund = 0)
    {
        //$updateData = [
        //    'order_num'          => DB::raw($this->counterUserBusiness->transformKey('order_num'). '-'. $order_num),
        //    'order_total'        => DB::raw($this->counterUserBusiness->transformKey('order_total'). '-'. $order_total),
        //    'order_total_fee'    => DB::raw($this->counterUserBusiness->transformKey('order_total_fee'). '-'. $order_total_fee),
        //    'order_total_refund' => DB::raw($this->counterUserBusiness->transformKey('order_total_refund'). '-'. $order_total_refund)
        //];
        //return $this->counterUserBusiness::query()
        //    ->userId($user_id)
        //    ->update($updateData);
        return await Task.FromResult(ReturnArr(true));;
    }

    /// <summary>
    /// 指定用户支出汇总
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="expense"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> SelfExpenseNumAdd(int userId, decimal expense)
    {
        //return $this->counterUserBusiness
        //    ->userId($user_id)
        //    ->increment('self_expense_total', $expense);

        return await Task.FromResult(ReturnArr(true));
    }
}
