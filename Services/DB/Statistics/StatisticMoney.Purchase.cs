using ERP.Enums.Finance;
using ERP.Models.DB.Logistics;
using ERP.Models.DB.Orders;
using ERP.Models.Finance;
using ERP.Services.DB.Statistics;
using ERP.ViewModels.Statistics;

namespace ERP.Services.Statistics;

public partial class StatisticMoney
{
    public async Task InitStatisticWithPurchase(Models.DB.Purchase.Purchase info)
    {
        var fm = new List<FinancialAffairsModel>();

        #region 新增数据-采购商品单价

        var purchasePriceFm = await PurchasePriceAdd(info);
        fm.Add(purchasePriceFm);

        #endregion

        #region 新增数据-订单商品单价

        var priceFm = await PurchaseOrderGoodPriceAdd(info);
        fm.Add(priceFm);

        #endregion

        #region 新增数据-采购商品总价

        var productTotalFm = await PurchaseProductTotalAdd(info);
        fm.Add(productTotalFm);

        #endregion

        #region 新增数据-采购其他费用

        var purchaseOtherFm = await PurchaseOtherAdd(info);
        fm.Add(purchaseOtherFm);

        #endregion
        
        #region 新增数据-采购总价

        var purchaseTotalFm = await PurchaseTotalAdd(info);
        fm.Add(purchaseTotalFm);

        #endregion

        #region 新增数据-采购商品单价/订单商品单价/采购商品总价/采购其他费用  统计/跟踪(2)

        await _statisticService.UpdateTable(fm, info.ID);

        #endregion
    }

    #region 采购商品单价

    /// <summary>
    /// 修改数据-采购商品单价跟踪
    /// </summary>
    /// <param name="info"></param>
    public async Task PurchasePriceEdit(Models.DB.Purchase.Purchase info)
    {
        var purchasePriceFm = await _statisticService.ReportFinancialRecord(
            new FianceDataDto(info.PurchasePrice, TableFieldEnum.PurchaseGoodPrice,
                info.ID,
                info.PurchasePrice.FinancialAffairsID),
            new DataBelongDto(info)
        );

        info.PurchasePrice.Next(purchasePriceFm);
    }

    /// <summary>
    /// 新增数据-采购商品单价
    /// </summary>
    /// <param name="info"></param>
    public async Task<FinancialAffairsModel> PurchasePriceAdd(Models.DB.Purchase.Purchase info)
    {
        var purchasePriceFm =
            await _statisticService.ReportFinancialRecord(
                new FianceDataDto(info.PurchasePrice, TableFieldEnum.PurchaseGoodPrice),
                new DataBelongDto(info)
            );


        info.PurchasePrice = info.PurchasePrice.Next(purchasePriceFm);
        return purchasePriceFm;
    }

    #endregion

    #region 订单商品单价

    /// <summary>
    /// 新增数据-采购商品单价
    /// </summary>
    /// <param name="info"></param>
    public async Task<FinancialAffairsModel> PurchaseOrderGoodPriceAdd(Models.DB.Purchase.Purchase info)
    {
        var priceFm = await _statisticService.ReportFinancialRecord(
            new FianceDataDto(info.Price, TableFieldEnum.PurchaseOrderGoodPrice),
            new DataBelongDto(info)
        );


        info.Price = info.Price.Next(priceFm);
        return priceFm;
    }

    /// <summary>
    /// 修改数据-采购商品单价
    /// </summary>
    /// <param name="info"></param>
    public async Task PurchaseOrderGoodPriceEdit(Models.DB.Purchase.Purchase info)
    {
        var priceFm = await _statisticService.ReportFinancialRecord(
            new FianceDataDto(info.Price, TableFieldEnum.PurchaseOrderGoodPrice, info.ID,
                info.Price.FinancialAffairsID),
            new DataBelongDto(info)
        );


        info.Price = info.Price.Next(priceFm);
    }

    #endregion

    #region 采购商品总价

    /// <summary>
    /// 修改数据-采购商品总价跟踪
    /// </summary>
    /// <param name="info"></param>
    public async Task PurchaseProductTotalEdit(Models.DB.Purchase.Purchase info)
    {
        var productTotalFm = await _statisticService.ReportFinancialRecord(
            new FianceDataDto(info.ProductTotal, TableFieldEnum.PurchaseGoodTotal,
                info.ID,
                info.ProductTotal.FinancialAffairsID),
            new DataBelongDto(info)
        );


        info.ProductTotal.Next(productTotalFm);
    }

    /// <summary>
    /// 新增数据-采购商品总价跟踪
    /// </summary>
    /// <param name="info"></param>
    /// <returns></returns>
    public async Task<FinancialAffairsModel> PurchaseProductTotalAdd(Models.DB.Purchase.Purchase info)
    {
        var productTotalFm = await _statisticService.ReportFinancialRecord(
            new FianceDataDto(info.ProductTotal, TableFieldEnum.PurchaseGoodTotal),
            new DataBelongDto(info)
        );


        info.ProductTotal.Next(productTotalFm);

        return productTotalFm;
    }

    #endregion

    #region 采购其他费用

    /// <summary>
    /// 新增数据-采购其他费用
    /// </summary>
    /// <param name="info"></param>
    public async Task<FinancialAffairsModel> PurchaseOtherAdd(Models.DB.Purchase.Purchase info)
    {
        var purchaseOtherFm =
            await _statisticService.ReportFinancialRecord(
                new FianceDataDto(info.PurchaseOther, TableFieldEnum.PurchaseOther),
                new DataBelongDto(info)
            );


        info.PurchaseOther = info.PurchaseOther.Next(purchaseOtherFm);

        return purchaseOtherFm;
    }

    /// <summary>
    /// 修改数据-采购其他费用跟踪
    /// </summary>
    /// <param name="info"></param>
    public async Task PurchaseOtherEdit(Models.DB.Purchase.Purchase info)
    {
        var purchaseOtherFm = await _statisticService.ReportFinancialRecord(
            new FianceDataDto(info.PurchaseOther, TableFieldEnum.PurchaseOther, info.ID,
                info.PurchaseOther.FinancialAffairsID),
            new DataBelongDto(info)
        );


        info.PurchaseOther.Next(purchaseOtherFm);
    }

    #endregion

    #region 采购总价
    
    public async Task<FinancialAffairsModel> PurchaseTotalAdd(Models.DB.Purchase.Purchase info)
    {
        //新增统计但财务表中并无产生金额的数据主键id
        var purchaseTotalFm = await _statisticService.ReportMoney(
            new DataBelongDto(info),
            Statistic.MoneyColumnEnum.PurchaseTotal,
            new FianceDataDto(info.PurchaseTotal, TableFieldEnum.PurchaseTotal)
        );


        info.PurchaseTotal = info.PurchaseTotal.Next(purchaseTotalFm);
        return purchaseTotalFm;
    }

    /// <summary>
    /// 修改数据-采购总价统计/跟踪
    /// </summary>
    /// <param name="info"></param>
    public async Task PurchaseTotalEdit(Models.DB.Purchase.Purchase info)
    {
        var purchaseTotalFm = await _statisticService.ReportMoney(
            new DataBelongDto(info),
            Statistic.MoneyColumnEnum.PurchaseTotal,
            new FianceDataDto(info.PurchaseTotal, TableFieldEnum.PurchaseTotal
                , info.ID, info.PurchaseTotal.FinancialAffairsID)
        );


        info.PurchaseTotal.Next(purchaseTotalFm);
    }

    #endregion

    public async Task SetInvalid(Models.DB.Purchase.Purchase info)
    {
        List<int> fIds = new();

        if (info.Price.FinancialAffairsID is > 0)
            fIds.Add((int)info.Price.FinancialAffairsID);

        if (info.PurchasePrice.FinancialAffairsID is > 0)
            fIds.Add((int) info.PurchasePrice.FinancialAffairsID);
        
        if (info.ProductTotal.FinancialAffairsID is > 0)
            fIds.Add((int) info.ProductTotal.FinancialAffairsID);
        
        if (info.PurchaseOther.FinancialAffairsID is > 0)
            fIds.Add((int) info.PurchaseOther.FinancialAffairsID);
        
        if (info.PurchaseTotal.FinancialAffairsID is > 0)
            fIds.Add((int) info.PurchaseTotal.FinancialAffairsID);

        await _statisticService.SetInvalid(fIds, new DataBelongDto(info));
    }
}