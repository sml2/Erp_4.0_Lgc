﻿using ERP.Data;
using ERP.Extensions;
using ERP.Models.Statistics;
using Microsoft.EntityFrameworkCore;
using static ERP.Extensions.DbSetExtension;

namespace ERP.Services.Statistics;

public class CounterRequestService : BaseService
{
    public readonly DBContext _dbContext;

    public CounterRequestService(DBContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<Boolean> InitCounterRequest(int UserId, int GroupId, int CompanyId, Models.DB.Identity.User user)
    {
        var Data = new CounterRequest()
        {
            UserID = UserId,
            GroupID = GroupId,
            CompanyID = CompanyId,
            UserName = user.UserName,
            TrueName = user.TrueName
        };

        await _dbContext.CounterRequest.AddAsync(Data);

        return await _dbContext.SaveChangesAsync() != 0;
    }

    public async Task<List<CounterRequest>> GetCountRequestByUserIds(List<int> Ids)
    {
        //$companyId = $this->getSession()->getCompanyId();
        var CompanyId = 0;
        //$companyId = $companyId == userModel::COMPANY_ID ? '' : $companyId;
        CompanyId = CompanyId == 2 ? 0 : CompanyId;

        return await _dbContext.CounterRequest
            .WhenWhere(CompanyId > 0, m => m.CompanyID == CompanyId)
            .Where(m => Ids.Contains(m.ID))
            .ToListAsync();
    }

    public async Task<Object> GetCompanyTotalRequest(int CompanyId)
    {
        return await _dbContext.CounterRequest
           .WhenWhere(CompanyId > 0, m => m.CompanyID == CompanyId)
           .GroupBy(m => new { })
           .Select(m => new
           {
               TotalReuqest = m.Sum(i => i.Request),
               TotalCpu = m.Sum(i => i.Cpu),
               TotalMemory = m.Sum(i => i.Memory),
               TotalNetwork = m.Sum(i => i.Network),
               TotalDisk = m.Sum(i => i.Disk),
               TotalPicApi = m.Sum(i => i.PicApi),
               TotalTranslateApi = m.Sum(i => i.TranslateApi),
           })

           .ToListAsync();
    }

    public async Task<bool> PicApiNumAdd(int UserId = 0, int Count = 1)
    {
        //$userId == '' && $userId = $this->getSession()->getUserId();
        //return CounterRequestModel::query()
        //    ->InUserId($userId)
        //    ->increment('pic_api', $picApi);

        return await Task.FromResult(true);
    }

    public async Task<bool> TranslateNumAdd(int UserI = 0,int Count = 1)
    {
        //$userId == '' && $userId = $this->getSession()->getUserId();
        //return CounterRequestModel::query()
        //    ->InUserId($userId)
        //    ->increment('translate_api', $num);

        return await Task.FromResult(true);
    }

    public async Task<bool> ProductNumReduce(int UserId = 0, int Count = 1)
    {
        //$userId == '' && $userId = $this->getSession()->getUserId();
        //return CounterRequestModel::query()
        //    ->InUserId($userId)
        //    ->decrement('disk', $disk);

        return await Task.FromResult(true);
    }
}
