using ERP.Enums.Finance;
using ERP.Models.DB.Logistics;
using ERP.Models.DB.Orders;
using ERP.Models.Finance;
using ERP.Services.DB.Statistics;
using ERP.ViewModels.Statistics;

namespace ERP.Services.Statistics;

public partial class StatisticMoney
{
    /// <summary>
    /// 数据调用整合
    /// </summary>
    /// <param name="info"></param>
    public async Task ReportWayBillEdit(Models.DB.Logistics.Waybill info)
    {
        await WaybillCarriageBaseEdit(info);

        await WaybillCarriageOtherEdit(info);

        await WaybillCarriageNumEdit(info);
    }

    public async Task InitStatisticWithWaybill(Waybill info)
    {
        var fm = new List<FinancialAffairsModel>();

        #region 新增数据-基础运费跟踪(1)

        var carriageBaseFm = await WaybillCarriageBaseAdd(info);
        fm.Add(carriageBaseFm);

        #endregion

        #region 新增数据-其他运费

        var carriageOtherFm = await WaybillCarriageOtherAdd(info);
        fm.Add(carriageOtherFm);

        #endregion

        #region 新增数据-最终运费数值

        var carriageNumFm = await WaybillCarriageNumAdd(info);
        fm.Add(carriageNumFm);

        #endregion

        #region 新增数据-基础运费跟踪/其他运费/最终运费数值  统计/跟踪(2)

        await _statisticService.UpdateTable(fm, info.ID);

        #endregion
    }

    #region 基础运费

    /// <summary>
    /// 修改数据-基础运费跟踪
    /// </summary>
    /// <param name="info"></param>
    public async Task WaybillCarriageBaseEdit(Waybill info)
    {
        var carriageBaseFm = await _statisticService.ReportFinancialRecord(
            new FianceDataDto(info.CarriageBase, TableFieldEnum.WaybillCarriageBase, info.ID,
                info.CarriageBase.FinancialAffairsID),
            new DataBelongDto(info)
        );


        info.CarriageBase = info.CarriageBase.Next(carriageBaseFm);
    }

    /// <summary>
    /// 新增数据-基础运费跟踪(1)
    /// </summary>
    /// <param name="info"></param>
    /// <returns></returns>
    public async Task<FinancialAffairsModel> WaybillCarriageBaseAdd(Waybill info)
    {
        var carriageBaseFm = await _statisticService.ReportFinancialRecord(
            new FianceDataDto(info.CarriageBase,
                TableFieldEnum.WaybillCarriageBase),
            new DataBelongDto(info)
        );


        info.CarriageBase = info.CarriageBase.Next(carriageBaseFm);
        return carriageBaseFm;
    }

    #endregion

    #region 其他费用

    /// <summary>
    /// 新增数据-其他运费
    /// </summary>
    /// <param name="info"></param>
    /// <returns></returns>
    public async Task<FinancialAffairsModel> WaybillCarriageOtherAdd(Waybill info)
    {
        var carriageOtherFm = await _statisticService.ReportFinancialRecord(
            new FianceDataDto(info.CarriageOther,
                TableFieldEnum.WaybillCarriageOther),
            new DataBelongDto(info)
        );


        info.CarriageOther = info.CarriageOther.Next(carriageOtherFm);
        return carriageOtherFm;
    }

    /// <summary>
    /// 修改数据-其他费用跟踪
    /// </summary>
    /// <param name="info"></param>
    public async Task WaybillCarriageOtherEdit(Waybill info)
    {
        var carriageOtherFm = await _statisticService.ReportFinancialRecord(
            new FianceDataDto(info.CarriageOther, TableFieldEnum.WaybillCarriageOther,
                info.ID, info.CarriageOther.FinancialAffairsID),
            new DataBelongDto(info)
        );

        info.CarriageOther = info.CarriageOther.Next(carriageOtherFm);
    }

    #endregion

    #region 最终运费数值

    /// <summary>
    /// 新增数据-最终运费数值
    /// </summary>
    /// <param name="info"></param>
    /// <returns></returns>
    public async Task<FinancialAffairsModel> WaybillCarriageNumAdd(Waybill info)
    {
        var carriageNumFm = await _statisticService.ReportMoney(
            new DataBelongDto(info),
            Statistic.MoneyColumnEnum.WaybillTotal,
            new FianceDataDto(info.CarriageNum, TableFieldEnum.WaybillTotal)
        );


        info.CarriageNum = info.CarriageNum.Next(carriageNumFm);
        return carriageNumFm;
    }

    /// <summary>
    /// 修改数据-最终运费数值
    /// </summary>
    /// <param name="info"></param>
    public async Task WaybillCarriageNumEdit(Waybill info)
    {
        var carriageNumFm = await _statisticService.ReportMoney(
            new DataBelongDto(info),
            Statistic.MoneyColumnEnum.WaybillTotal,
            new FianceDataDto(info.CarriageNum, TableFieldEnum.WaybillTotal,
                info.ID, info.CarriageNum.FinancialAffairsID)
        );


        info.CarriageNum = info.CarriageNum.Next(carriageNumFm);
    }

    #endregion
    
    public async Task SetInvalid(Waybill info)
    {
        List<int> fIds = new();

        if (info.CarriageBase.FinancialAffairsID is > 0)
            fIds.Add((int)info.CarriageBase.FinancialAffairsID);

        if (info.CarriageOther.FinancialAffairsID is > 0)
            fIds.Add((int) info.CarriageOther.FinancialAffairsID);
        
        if (info.CarriageNum.FinancialAffairsID is > 0)
            fIds.Add((int) info.CarriageNum.FinancialAffairsID);
        

        await _statisticService.SetInvalid(fIds, new DataBelongDto(info));
    }
}