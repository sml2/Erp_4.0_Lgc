using ERP.Enums.Finance;
using ERP.Models.DB.Logistics;
using ERP.Models.DB.Orders;
using ERP.Models.DB.Users;
using ERP.Models.Finance;
using ERP.Services.DB.Statistics;
using ERP.ViewModels.Statistics;

namespace ERP.Services.Statistics;

public partial class StatisticMoney
{
    /// <summary>
    /// 自用钱包支出汇总
    /// </summary>
    /// <param name="info"></param>
    /// <param name="money"></param>
    public async Task SelfExpenseTotal(Company info, decimal money)
    {
        await _statisticService.ReportMoney(new DataBelongDto(info), Statistic.MoneyColumnEnum.SelfExpenseTotal, money);
    }

    /// <summary>
    /// 自用钱包充值汇总
    /// </summary>
    /// <param name="info"></param>
    /// <param name="money"></param>
    public async Task SelfRechargeTotal(Company info, decimal money)
    {
        await _statisticService.ReportMoney(new DataBelongDto(info), Statistic.MoneyColumnEnum.SelfRechargeTotal,
            money);
    }

    /// <summary>
    /// 自用钱包数据跟踪
    /// </summary>
    /// <param name="info"></param>
    /// <returns></returns>
    public async Task<FinancialAffairsModel> SelfWalletReport(Company info)
    {
        #region 自用钱包数据跟踪

        var privateWalletFm = await _statisticService.ReportFinancialRecord(
            new FianceDataDto(info.PrivateWallet,
                TableFieldEnum.PrivateWallet, info.ID, info.PrivateWallet.FinancialAffairsID),
            new DataBelongDto(info)
        );

        #endregion

        info.PrivateWallet = info.PrivateWallet.Next(privateWalletFm);
        return privateWalletFm;
    }

    /// <summary>
    /// 对公钱包支出汇总
    /// </summary>
    /// <param name="info"></param>
    /// <param name="money"></param>
    public async Task PublicExpenseTotal(Company info, decimal money)
    {
        await _statisticService.ReportMoney(new DataBelongDto(info), Statistic.MoneyColumnEnum.ExpenseTotal, money);
    }
    public async Task PublicExpenseTotal(BillLog info, decimal money)
    {
        if (info.Belong != BillLog.Belongs.RIGHT)
        {
            throw new Exception($"账单类型异常[{info.ID}]");
        }
        await _statisticService.ReportMoney(new DataBelongDto(info), Statistic.MoneyColumnEnum.RechargeTotal, money);
    }

    /// <summary>
    /// 对公钱包充值汇总
    /// </summary>
    /// <param name="info"></param>
    /// <param name="money"></param>
    public async Task PublicRechargeTotal(Company info, decimal money)
    {
        await _statisticService.ReportMoney(new DataBelongDto(info), Statistic.MoneyColumnEnum.RechargeTotal, money);
    }
    
    public async Task PublicRechargeTotal(BillLog info, decimal money)
    {
        if (info.Belong != BillLog.Belongs.RIGHT)
        {
            throw new Exception($"账单类型异常[{info.ID}]");
        }
        await _statisticService.ReportMoney(new DataBelongDto(info), Statistic.MoneyColumnEnum.RechargeTotal, money);
    }
    
    /// <summary>
    /// 对公钱包数据跟踪
    /// </summary>
    /// <param name="info"></param>
    /// <returns></returns>
    public async Task<FinancialAffairsModel> PublicWalletReport(Company info)
    {
        #region 自用钱包数据跟踪

        var privateWalletFm = await _statisticService.ReportFinancialRecord(
            new FianceDataDto(info.PrivateWallet,
                TableFieldEnum.PublicWallet, info.ID, info.PrivateWallet.FinancialAffairsID),
            new DataBelongDto(info)
        );

        #endregion

        info.PrivateWallet = info.PrivateWallet.Next(privateWalletFm);
        return privateWalletFm;
    }
}