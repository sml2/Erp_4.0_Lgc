using ERP.Enums.Finance;
using ERP.Extensions;
using ERP.Models.DB.Logistics;
using ERP.Models.Finance;
using ERP.Models.Product;
using ERP.Services.Statistics;
using ERP.ViewModels.Statistics;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using NPOI.SS.Formula.Functions;
using Exception = System.Exception;
using Task = System.Threading.Tasks.Task;

namespace ERP.Services.DB.Statistics;

using Model = Models.DB.Statistics.Statistic;
using Enums.Statistics;

public partial class Statistic
{
    public enum MoneyColumnEnum
    {
        OrderTotal,
        OrderTotalFee,
        OrderTotalRefund,
        OrderTotalLoss,
        PurchaseTotal,
        PurchaseTotalRefund,
        WaybillTotal,
        WaybillTotalRefund,
        SelfRechargeTotal,
        SelfExpenseTotal,
        RechargeTotal,
        ExpenseTotal
    }
    

    private Dictionary<TableFieldEnum, MoneyColumnEnum> TableFieldWithMoneyColumns = new()
    {
        { TableFieldEnum.PurchaseTotal,MoneyColumnEnum.PurchaseTotal},
        { TableFieldEnum.WaybillTotal,MoneyColumnEnum.WaybillTotal},
    };

    private Dictionary<MoneyColumnEnum, string> MoneyColumns = new()
    {
        {MoneyColumnEnum.OrderTotal, nameof(Model.OrderTotal)},
        {MoneyColumnEnum.OrderTotalFee, nameof(Model.OrderTotalFee)},
        {MoneyColumnEnum.OrderTotalRefund, nameof(Model.OrderTotalRefund)},
        {MoneyColumnEnum.OrderTotalLoss, nameof(Model.OrderTotalLoss)},
        {MoneyColumnEnum.PurchaseTotal, nameof(Model.PurchaseTotal)},
        {MoneyColumnEnum.PurchaseTotalRefund, nameof(Model.PurchaseTotalRefund)},
        {MoneyColumnEnum.WaybillTotal, nameof(Model.WaybillTotal)},
        {MoneyColumnEnum.WaybillTotalRefund, nameof(Model.WaybillTotalRefund)},
        {MoneyColumnEnum.SelfRechargeTotal, nameof(Model.SelfRechargeTotal)},
        {MoneyColumnEnum.SelfExpenseTotal, nameof(Model.SelfExpenseTotal)},
        {MoneyColumnEnum.RechargeTotal, nameof(Model.RechargeTotal)},
        {MoneyColumnEnum.ExpenseTotal, nameof(Model.ExpenseTotal)}
    };

    private Dictionary<MoneyColumnEnum, string> MoneyColumnMessageTitles = new()
    {
        {MoneyColumnEnum.OrderTotal, "订单总价汇总"},
        {MoneyColumnEnum.OrderTotalFee, "订单手续费汇总"},
        {MoneyColumnEnum.OrderTotalRefund, "订单退款汇总"},
        {MoneyColumnEnum.OrderTotalLoss, "订单损耗汇总"},
        {MoneyColumnEnum.PurchaseTotal, "采购总价汇总"},
        {MoneyColumnEnum.PurchaseTotalRefund, "采购退款汇总"},
        {MoneyColumnEnum.WaybillTotal, "运费总价汇总"},
        {MoneyColumnEnum.WaybillTotalRefund, "运费退款汇总"},
        {MoneyColumnEnum.SelfRechargeTotal, "自用钱包充值汇总"},
        {MoneyColumnEnum.SelfExpenseTotal, "自用钱包支出汇总"},
        {MoneyColumnEnum.RechargeTotal, "钱包充值汇总"},
        {MoneyColumnEnum.ExpenseTotal, "钱包支出汇总"}
    };

    public async Task ReportMoney(DataBelongDto belong, MoneyColumnEnum column, MoneyRecord value)
    {
        var money = value.Money;
        var statistics = await GetStatistics(belong);

        foreach (var item in statistics)
        {
            await PlusMoney(item, column, money);
        }
    }


    public async Task<FinancialAffairsModel> ReportMoney(DataBelongDto belong, MoneyColumnEnum column,
        FianceDataDto dto)
    {
        var money = dto.Money.Money;
        if (dto.SourceId.HasValue)
        {
            var financeInfo = await _financialAffairsService.GetInfo(dto.SourceId.Value);

            if (financeInfo != null)
            {
                var sourceMoney = financeInfo.Value.Money;
                money -= sourceMoney;

                if (money == 0)
                {
                    return financeInfo;
                }
            }
        }

        var statistics = await GetStatistics(belong);

        foreach (var item in statistics)
        {
            await PlusMoney(item, column, money);
        }

        return await ReportFinancialRecord(dto, belong);
    }

    private Task<List<Model>> GetStatistics(DataBelongDto belong)
    {
        var statistics = new List<Model>();
        statistics.Add(GetStatisticInfo(belong.CompanyId, Types.Company, Types.DAY).Result);
        statistics.Add(GetStatisticInfo(belong.OemId, Types.OEM, Types.DAY).Result);
        
        if (belong.UserId.HasValue)
            statistics.Add(GetStatisticInfo(belong.UserId, Types.User, Types.DAY).Result);
        if (belong.GroupId.HasValue)
            statistics.Add(GetStatisticInfo(belong.GroupId, Types.UserGroup, Types.DAY).Result);
        if (belong.StoreId.HasValue)
            statistics.Add(GetStatisticInfo(belong.StoreId, Types.Store, Types.DAY).Result);

        return Task.FromResult(statistics);
    }

    public Task<FinancialAffairsModel> ReportFinancialRecord(FinancialAffairsModel dto, DataBelongDto belong)
    {
        return _financialAffairsService.AddFianceData(dto, belong);
    }

    public async Task SetInvalid(List<int> ids,DataBelongDto belong)
    {
        var list = await _dbContext.FinancialAffairs.Where(m => ids.Contains(m.ID)).ToListAsync();

        if (list.Count != ids.Count)
        {
            throw new Exception("财务数据缺失");
        }
        //var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            Dictionary<MoneyColumnEnum, decimal> operate = new Dictionary<MoneyColumnEnum, decimal>();
            var currentTime = DateTime.Now;
            foreach (var item in list)
            {
                if (TableFieldWithMoneyColumns.Keys.Contains(item.TableField))
                {
                    operate.Add(TableFieldWithMoneyColumns[item.TableField],item.Value.Money.Value);
                }
                item.FailureTime = currentTime;
                _dbContext.FinancialAffairs.Update(item);
            }

            if (operate.Count > 0)
            {
                var statistics = await GetStatistics(belong);

                foreach (var item in operate)
                {
                    await PlusMoney(statistics, item.Key, -item.Value);
                } 
            }

            await _dbContext.SaveChangesAsync();
            //await transaction.CommitAsync();
        }
        catch (Exception e)
        {
            //await transaction.RollbackAsync();
            Console.WriteLine(e.Message);
            throw new Exception(e.Message);
        }
    }

    private async Task PlusMoney(List<Model> ms, MoneyColumnEnum column, decimal money)
        {

    
            try
            {
                foreach (Model m in ms)
                {
                    var t = m.GetType();
                    var field = t.GetProperty(MoneyColumns[column]);
                    var pt = field?.PropertyType ?? typeof(string);
                    if (pt.Equals(typeof(Money)))
                    {
                        Money my = (Money) field?.GetValue(m)!;
                        var newV = my + money;
                        field!.SetValue(m, newV);
                    }
                    else
                    {
                        field!.SetValue(m, Convert.ToInt32(field.GetValue(m)) + money);
                    }
                }
                
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                var message = $"{MoneyColumnMessageTitles[column]}上报发生异常,异常信息[{e.Message}]";
                Console.WriteLine(message);
                throw new Exception(message);
            }
        }


    private async Task PlusMoney(Model m, MoneyColumnEnum column, decimal money)
    {
        var t = m.GetType();
        var field = t.GetProperty(MoneyColumns[column]);
        var pt = field?.PropertyType ?? typeof(string);
        if (pt.Equals(typeof(Money)))
        {
            Money my = (Money) field?.GetValue(m)!;
            var newV = my + money;
            field!.SetValue(m, newV);
        }
        else
        {
            field!.SetValue(m, Convert.ToInt32(field.GetValue(m)) + money);
        }

        try
        {
            await _dbContext.SaveChangesAsync();
        }
        catch (Exception e)
        {
            var message = $"{MoneyColumnMessageTitles[column]}上报发生异常,异常信息[{e.Message}]";
            Console.WriteLine(message);
            throw new Exception(message);
        }
    }

    public async Task UpdateFinancialAffairs(int id, int tableId)
    {
        //暂不开启
        return;
        await _financialAffairsService.UpdateTableIndexId(id, tableId);
    }

    public async Task UpdateFinancialAffairs(List<int> id, int tableId)
    {
        //暂不开启
        return;
        await _financialAffairsService.UpdateTableIndexId(id, tableId);
    }

    public async Task UpdateTable(List<FinancialAffairsModel> models, int tableId)
    {
        //暂不开启
        return;
        await _financialAffairsService.UpdateTable(models, tableId);
    }
}