using ERP.Extensions;
using ERP.Services.Finance;
using Microsoft.EntityFrameworkCore;
using ERP.Data;
using ERP.ViewModels.Statistics;
using Logistics.SFCLogisticsReference;
using Z.EntityFramework.Plus;

namespace ERP.Services.DB.Statistics;

using Enums.Statistics;
using Models.DB.Identity;
using Interface;
using Models.DB.Stores;
using Models.DB.Users;
using Models.Setting;
using System.Collections.Generic;
using Model = Models.DB.Statistics.Statistic;
using ERP.Models.Stores;

public partial class Statistic
{
    private readonly DBContext _dbContext;

    private readonly FinancialAffairsService _financialAffairsService;

    public Statistic(DBContext dbContext)
    {
        _dbContext = dbContext;
    }

    public Statistic(DBContext dbContext, FinancialAffairsService financialAffairsService)
    {
        _dbContext = dbContext;
        _financialAffairsService = financialAffairsService;
    }

    internal async Task<List<Model>> NeedUpdate() => await _dbContext.Statistic
        .Where(m => m.NeedUpdate && (m.AboutType & Types.MONTH) > 0).OrderBy(m => m.Validity).ToListAsync();

    public async Task UpdateInfo(Model statistic)
    {
        await _dbContext.Statistic.SingleUpdateAsync(statistic);
    }


    public Task UpdateRangeInfo(List<Model> statistic)
    {
        _dbContext.Statistic.UpdateRange(statistic);
        return Task.CompletedTask;
    }

    internal async System.Threading.Tasks.Task Update(Model statistic)
    {
        var now = DateTime.Now;

        //处理月数据
        var role = statistic.AboutType & Types.Role;
        Model? sum = new Model();

        if ((statistic.AboutType & Types.Effective) == Types.MONTH)
        {
            sum = await GetEffectiveSumStatistic(statistic.AboutID, Types.DAY, role, statistic.Validity);
        }
        else if ((statistic.AboutType & Types.Effective) == Types.YEAR)
        {
            sum = await GetEffectiveSumStatistic(statistic.AboutID, Types.MONTH, role, statistic.Validity);
        }
        else if ((statistic.AboutType & Types.Effective) == Types.TOTAL)
        {
            //统计总数据无需使用时间
            sum = await GetEffectiveSumStatistic(statistic.AboutID, Types.YEAR, role, now);
        }

        //跨月 跨年
        if (now > statistic.Validity)
        {
            statistic.NeedUpdate = false;
        }

        if (sum != null)
        {
            #region 公司管理

            statistic.CompanyInsert = sum.CompanyInsert;
            statistic.CompanyDelete = sum.CompanyDelete;
            statistic.CompanyCount = sum.CompanyCount;
            statistic.CompanyReject = sum.CompanyReject;

            #endregion

            #region 员工管理

            statistic.EmployeeInsert = sum.EmployeeInsert;
            statistic.EmployeeDelete = sum.EmployeeDelete;
            statistic.EmployeeCount = sum.EmployeeCount;
            statistic.EmployeeReject = sum.EmployeeReject;

            #endregion

            #region 开户管理

            statistic.UserInsert = sum.UserInsert;
            statistic.UserDelete = sum.UserDelete;
            statistic.UserCount = sum.UserCount;
            statistic.UserReject = sum.UserReject;

            #endregion

            #region 产品管理

            statistic.AiMatting = sum.AiMatting;
            statistic.ApiTranslate = sum.ApiTranslate;
            statistic.ApiTranslateLanguage = sum.ApiTranslateLanguage;
            statistic.ApiTranslateProduct = sum.ApiTranslateProduct;
            statistic.DiskAdd = sum.DiskAdd;
            statistic.DiskDel = sum.DiskDel;
            statistic.DiskSize = sum.DiskSize;
            statistic.ProductInsert = sum.ProductInsert;
            statistic.ProductDelete = sum.ProductDelete;
            statistic.ProductCount = sum.ProductCount;
            statistic.ProductRecovery = sum.ProductRecovery;

            #endregion

            #region 店铺管理

            statistic.TaskInsert = sum.TaskInsert;
            statistic.TaskDelete = sum.TaskDelete;
            statistic.TaskCount = sum.TaskCount;
            statistic.StoreInsert = sum.StoreInsert;
            statistic.StoreDelete = sum.StoreDelete;
            statistic.StoreCount = sum.StoreCount;

            #endregion

            #region 订单管理

            statistic.OrderInsert = sum.OrderInsert;
            statistic.OrderDelete = sum.OrderDelete;
            statistic.OrderCount = sum.OrderCount;

            #endregion

            #region 采购管理

            statistic.PurchaseInsert = sum.PurchaseInsert;
            statistic.PurchaseDelete = sum.PurchaseDelete;
            statistic.PurchaseCount = sum.PurchaseCount;

            #endregion

            #region 运单管理

            statistic.WaybillInsert = sum.WaybillInsert;
            statistic.WaybillDelete = sum.WaybillDelete;
            statistic.WaybillCount = sum.WaybillCount;

            #endregion

            #region 下级上报

            statistic.ReportCompanyCount = sum.ReportCompanyCount;
            statistic.ReportEmployeeCount = sum.ReportEmployeeCount;
            statistic.ReportUserCount = sum.ReportUserCount;
            statistic.ReportProductCount = sum.ReportProductCount;
            statistic.ReportStoreCount = sum.ReportStoreCount;
            statistic.ReportOrderCount = sum.ReportOrderCount;
            statistic.ReportPurchaseCount = sum.ReportPurchaseCount;
            statistic.ReportWaybillCount = sum.ReportWaybillCount;

            #endregion

            #region 下级分销

            statistic.DistributionOrderCount = sum.DistributionOrderCount;
            statistic.DistributionPurchaseCount = sum.DistributionPurchaseCount;
            statistic.DistributionWaybillCount = sum.DistributionWaybillCount;

            #endregion

            #region 金额

            statistic.OrderTotal = sum.OrderTotal;
            statistic.OrderTotalFee = sum.OrderTotalFee;
            statistic.OrderTotalRefund = sum.OrderTotalRefund;
            statistic.OrderTotalLoss = sum.OrderTotalLoss;
            statistic.PurchaseTotal = sum.PurchaseTotal;
            statistic.PurchaseTotalRefund = sum.PurchaseTotalRefund;
            statistic.WaybillTotal = sum.WaybillTotal;
            statistic.WaybillTotalRefund = sum.WaybillTotalRefund;
            statistic.SelfRechargeTotal = sum.SelfRechargeTotal;
            statistic.SelfExpenseTotal = sum.SelfExpenseTotal;
            statistic.RechargeTotal = sum.RechargeTotal;
            statistic.ExpenseTotal = sum.ExpenseTotal;

            #endregion

            statistic.UpdatedAt = now;
        }

        _dbContext.Statistic.Update(statistic);

        await _dbContext.SaveChangesAsync();


        //var M = MonthStatistic;
        //var Y = ;
        //var T = ;
        ////_dbContext.Database.tr
        //var role = statistic.AboutType & Types.Role;
        //var effective = statistic.AboutType & Types.Effective;
    }

    public async Task<Model?> GetEffectiveSumStatistic(int aboutId, Types effective, Types role, DateTime time)
    {
        //统计月数据 需查询 当月日数据
        //统计年数据 需查询 当年月数据
        return await _dbContext.Statistic.Where(m =>
                m.AboutID == aboutId
                && (m.AboutType & Types.Effective) == effective
                && (m.AboutType & Types.Role) == role
            )
            .WhenWhere(effective == Types.DAY,
                m => m.Validity >= time.BeginMonthTick() && m.Validity <= time.LastMonthTick())
            .WhenWhere(effective == Types.MONTH,
                m => m.Validity >= time.BeginYearTick() && m.Validity <= time.LastYearTick())
            .GroupBy(m => m.AboutID)
            .Select(g => new Models.DB.Statistics.Statistic()
            {
                AboutID = g.Key,

                #region 公司管理

                CompanyInsert = g.Sum(m => m.CompanyInsert),
                CompanyDelete = g.Sum(m => m.CompanyDelete),
                CompanyCount = g.Sum(m => m.CompanyCount),
                CompanyReject = g.Sum(m => m.CompanyReject),

                #endregion

                #region 员工管理

                EmployeeInsert = g.Sum(m => m.EmployeeInsert),
                EmployeeDelete = g.Sum(m => m.EmployeeDelete),
                EmployeeCount = g.Sum(m => m.EmployeeCount),
                EmployeeReject = g.Sum(m => m.EmployeeReject),

                #endregion

                #region 开户管理

                UserInsert = g.Sum(m => m.UserInsert),
                UserDelete = g.Sum(m => m.UserDelete),
                UserCount = g.Sum(m => m.UserCount),
                UserReject = g.Sum(m => m.UserReject),

                #endregion

                #region 产品管理

                AiMatting = g.Sum(m => m.AiMatting),
                ApiTranslate = g.Sum(m => m.ApiTranslate),
                ApiTranslateLanguage = g.Sum(m => m.ApiTranslateLanguage),
                ApiTranslateProduct = g.Sum(m => m.ApiTranslateProduct),
                DiskAdd = g.Sum(m => m.DiskAdd),
                DiskDel = g.Sum(m => m.DiskDel),
                DiskSize = g.Sum(m => m.DiskSize),
                ProductInsert = g.Sum(m => m.ProductInsert),
                ProductDelete = g.Sum(m => m.ProductDelete),
                ProductCount = g.Sum(m => m.ProductCount),
                ProductRecovery = g.Sum(m => m.ProductRecovery),

                #endregion

                #region 店铺管理

                TaskInsert = g.Sum(m => m.TaskInsert),
                TaskDelete = g.Sum(m => m.TaskDelete),
                TaskCount = g.Sum(m => m.TaskCount),
                StoreInsert = g.Sum(m => m.StoreInsert),
                StoreDelete = g.Sum(m => m.StoreDelete),
                StoreCount = g.Sum(m => m.StoreCount),

                #endregion

                #region 订单管理

                OrderInsert = g.Sum(m => m.OrderInsert),
                OrderDelete = g.Sum(m => m.OrderDelete),
                OrderCount = g.Sum(m => m.OrderCount),

                #endregion

                #region 采购管理

                PurchaseInsert = g.Sum(m => m.PurchaseInsert),
                PurchaseDelete = g.Sum(m => m.PurchaseDelete),
                PurchaseCount = g.Sum(m => m.PurchaseCount),

                #endregion

                #region 运单管理

                WaybillInsert = g.Sum(m => m.WaybillInsert),
                WaybillDelete = g.Sum(m => m.WaybillDelete),
                WaybillCount = g.Sum(m => m.WaybillCount),

                #endregion

                #region 下级上报

                ReportCompanyCount = g.Sum(m => m.ReportCompanyCount),
                ReportEmployeeCount = g.Sum(m => m.ReportEmployeeCount),
                ReportUserCount = g.Sum(m => m.ReportUserCount),
                ReportProductCount = g.Sum(m => m.ReportProductCount),
                ReportStoreCount = g.Sum(m => m.ReportStoreCount),
                ReportOrderCount = g.Sum(m => m.ReportOrderCount),
                ReportPurchaseCount = g.Sum(m => m.ReportPurchaseCount),
                ReportWaybillCount = g.Sum(m => m.ReportWaybillCount),

                #endregion

                #region 下级分销

                DistributionOrderCount = g.Sum(m => m.DistributionOrderCount),
                DistributionPurchaseCount = g.Sum(m => m.DistributionPurchaseCount),
                DistributionWaybillCount = g.Sum(m => m.DistributionWaybillCount),

                #endregion

                #region 金额

                OrderTotal = g.Sum(m => m.OrderTotal),
                OrderTotalFee = g.Sum(m => m.OrderTotalFee),
                OrderTotalRefund = g.Sum(m => m.OrderTotalRefund),
                OrderTotalLoss = g.Sum(m => m.OrderTotalLoss),
                PurchaseTotal = g.Sum(m => m.PurchaseTotal),
                PurchaseTotalRefund = g.Sum(m => m.PurchaseTotalRefund),
                WaybillTotal = g.Sum(m => m.WaybillTotal),
                WaybillTotalRefund = g.Sum(m => m.WaybillTotalRefund),
                SelfRechargeTotal = g.Sum(m => m.SelfRechargeTotal),
                SelfExpenseTotal = g.Sum(m => m.SelfExpenseTotal),
                RechargeTotal = g.Sum(m => m.RechargeTotal),
                ExpenseTotal = g.Sum(m => m.ExpenseTotal),

                #endregion
            })
            .FirstOrDefaultAsync();
    }

    private readonly Types[] AllEffectiveTimeRanges = new[] { Types.TOTAL, Types.YEAR, Types.MONTH, Types.DAY };

    private async System.Threading.Tasks.Task CheckOrAdd(int ID, IStatistic IStatistic, Types types)
    {
        var Now = DateTime.Now;
        var Date = Now.Date;
        if (IStatistic.LastCreateStatistic < Date) //是否跨天
        {
            var EffectiveTimeRanges = await _dbContext.Statistic
                .Where(s =>
                    s.AboutID == ID
                    && s.Validity >= Now
                    && (s.AboutType & Types.Role) == types
                )
                .Select(s => s.AboutType & Types.Effective).ToListAsync();
            var NeedAddEffectiveTimeRanges = AllEffectiveTimeRanges.Where(s => !EffectiveTimeRanges.Contains(s));
            await _dbContext.Statistic.AddRangeAsync(
                NeedAddEffectiveTimeRanges.Select(t => new Model(ID, types, t, Date)));
            IStatistic.LastCreateStatistic = Now;

            // var entry = _dbContext.Entry(IStatistic);
            // entry.State = EntityState.Unchanged;
            // entry.Property(nameof(IStatistic.LastCreateStatistic)).IsModified = true;
            await _dbContext.SaveChangesAsync();
        }
    }

    public Task CheckOrAdd(OEM m) => CheckOrAdd(m.ID, m, Types.OEM);
    public Task CheckOrAdd(Company m) => CheckOrAdd(m.ID, m, Types.Company);
    public Task CheckOrAdd(StoreRegion m) => CheckOrAdd(m.ID, m, Types.Store);
    public Task CheckOrAdd(Group m) => CheckOrAdd(m.ID, m, Types.UserGroup);
    public Task CheckOrAdd(User m) => CheckOrAdd(m.ID, m, Types.User);

    public async Task<List<Model>> GetStatisticDayListWithGroup(List<int> ids)
    {
        var begin = DateTime.Now.BeginDayTick();
        var last = DateTime.Now.LastDayTick();
        return await _dbContext.Statistic
            .Where(m =>
                ids.Contains(m.AboutID)
                && (m.AboutType & Types.Effective) == Types.DAY
                && (m.AboutType & Types.Role) == Types.UserGroup
                && (m.Validity >= begin && m.Validity <= last)
            )
            .ToListAsync();
    }

    public async Task<List<Model>> GetStatisticDayListWithUser(List<int> ids)
    {
        var begin = DateTime.Now.BeginDayTick();
        var last = DateTime.Now.LastDayTick();
        return await _dbContext.Statistic
            .Where(m =>
                ids.Contains(m.AboutID)
                && (m.AboutType & Types.Effective) == Types.DAY
                && (m.AboutType & Types.Role) == Types.User
                && (m.Validity >= begin && m.Validity <= last)
            )
            .ToListAsync();
    }

    public async Task<Model> GetStatisticInfo(int? aboutId, Types role, Types effective)
    {
        if (!aboutId.HasValue)
        {
            throw new Exception($"AboutId Is Null");
        }

        if (effective == Types.DAY)
        {
            switch (role)
            {
                case Types.User:
                    await CheckOrAdd(await _dbContext.User.FindAsync(aboutId));
                    break;
                case Types.UserGroup:
                    await CheckOrAdd(await _dbContext.UserGroup.FindAsync(aboutId));
                    break;
                case Types.Company:
                    await CheckOrAdd(await _dbContext.Company.FindAsync(aboutId));
                    break;
                case Types.OEM:
                    await CheckOrAdd(await _dbContext.OEM.FindAsync(aboutId));
                    break;
                case Types.Store:
                    await CheckOrAdd(await _dbContext.StoreRegion.FindAsync(aboutId));
                    break;
            }
        }

        var begin = DateTime.Now.BeginDayTick();
        var last = DateTime.Now.LastDayTick();

        var info = await _dbContext.Statistic
            .Where(m =>
                m.AboutID == aboutId
                && (m.AboutType & Types.Effective) == effective
                && (m.AboutType & Types.Role) == role
                && (m.Validity >= begin && m.Validity <= last)
            )
            .FirstOrDefaultAsync();

        if (info == null)
        {
            throw new Exception($"找不到相关数据#role[{role.GetName()}] | effective[{effective.GetName()}]");
        }

        return info;
    }

    public async Task<List<Model>> GetCountCompanyById(List<int> ids)
    {
        return await GetCountData(ids, Types.Company);
    }

    public async Task<Model?> GetCountCompanyById(int id)
    {
        return await GetCountData(id, Types.Company);
    }

    private async Task<Model?> GetCountData(int id, Types role)
    {
        return await _dbContext.Statistic
            .Where(m =>
                m.AboutID == id
                && (m.AboutType & Types.Effective) == Types.TOTAL
                && (m.AboutType & Types.Role) == role
            )
            .FirstOrDefaultAsync();
    }

    private async Task<List<Model>> GetCountData(List<int> ids, Types role)
    {
        return await _dbContext.Statistic
            .Where(m =>
                ids.Contains(m.AboutID)
                && (m.AboutType & Types.Effective) == Types.TOTAL
                && (m.AboutType & Types.Role) == role
            )
            .ToListAsync();
    }

    /// <summary>
    /// 获取某店铺对应的用户产生的订单金额
    /// </summary>
    /// <param name="userIds"></param>
    /// <param name="store"></param>
    /// <returns></returns>
    public Dictionary<int, UserAmountByOrderVm> UserAmountByTheStore(List<int> userIds, StoreRegion store)
    {
        var storeId = store.ID;
        return _dbContext.Order
            .Where(m => m.StoreId == storeId && userIds.Contains(m.UserID))
            .AsEnumerable()
            .GroupBy(m => m.UserID)
            .Select(g => new UserAmountByOrderVm()
            {
                UserId = g.Key,
                OrderTotal = g.Sum(m => m.OrderTotal),
                OrderTotalFee = g.Sum(m => m.Fee),
                OrderTotalRefund = g.Sum(m => m.Refund),
                OrderTotalLoss = g.Sum(m => m.Loss)
            }).ToDictionary(m => m.UserId, m => m);
    }

    /// <summary>
    /// 获取某店铺对应的用户组产生的订单金额
    /// </summary>
    /// <param name="groupIds"></param>
    /// <param name="store"></param>
    /// <returns></returns>
    public Dictionary<int, GroupAmountByOrderVm> GroupAmountByTheStore(List<int> groupIds, StoreRegion store)
    {
        var storeId = store.ID;
        return _dbContext.Order
            .Where(m => m.StoreId == storeId && groupIds.Contains(m.GroupID))
            .AsEnumerable()
            .GroupBy(m => m.GroupID)
            .Select(g => new GroupAmountByOrderVm()
            {
                GroupId = g.Key,
                OrderTotal = g.Sum(m => m.OrderTotal),
                OrderTotalFee = g.Sum(m => m.Fee),
                OrderTotalRefund = g.Sum(m => m.Refund),
                OrderTotalLoss = g.Sum(m => m.Loss)
            }).ToDictionary(m => m.GroupId, m => m);
    }

    /// <summary>
    /// 获取用户产生的财务
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    public async Task<AmountVm> AmountByUser(int userId)
    {
        var amount = new AmountVm();
        var orderSum = _dbContext.Order
            .Where(m => m.UserID == userId)
            .AsEnumerable()
            .GroupBy(m => m.UserID)
            .Select(g => new AmountByOrderVm()
            {
                OrderTotal = g.Sum(m => m.OrderTotal),
                OrderTotalFee = g.Sum(m => m.Fee),
                OrderTotalRefund = g.Sum(m => m.Refund),
                OrderTotalLoss = g.Sum(m => m.Loss)
            }).FirstOrDefault();
        if (orderSum != null)
        {
            amount.Order = new AmountByOrderVm();
            amount.SetOrder(orderSum);
        }

        var purchaseSum = _dbContext.Purchase
            .Where(m => m.UserID == userId)
            .AsEnumerable()
            .GroupBy(m => m.StoreId)
            .Select(g => new AmountByPurchaseVm()
            {
                PurchaseTotal = g.Sum(m => m.PurchaseTotal),
                PurchaseTotalRefund = 0
            }).FirstOrDefault();
        if (purchaseSum != null)
        {
            amount.Purchase = new AmountByPurchaseVm();
            amount.SetPurchase(purchaseSum);
        }

        var waybillSum = _dbContext.Waybill
            .Where(m => m.UserID == userId)
            .AsEnumerable()
            .GroupBy(m => m.StoreId)
            .Select(g => new AmountByWaybillVm()
            {
                WaybillTotal = g.Sum(m => m.CarriageNum),
                WaybillTotalRefund = 0
            }).FirstOrDefault();
        if (waybillSum != null || waybillSum is not null || waybillSum.IsNotNull())
        {
        }

        if (waybillSum != null)
        {
            amount.Waybill = new AmountByWaybillVm();
            amount.SetWaybill(waybillSum);
        }

        return amount;
    }

    /// <summary>
    /// 获取店铺产生的财务
    /// </summary>
    /// <param name="store"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    public async Task<AmountVm> AmountByStore(StoreRegion store)
    {
        var storeId = store.ID;
        var amount = new AmountVm();

        var orderSum = _dbContext.Order
            .Where(m => m.StoreId == storeId)
            .AsEnumerable()
            .GroupBy(m => m.StoreId)
            .Select(g => new AmountByOrderVm()
            {
                OrderTotal = g.Sum(m => m.OrderTotal),
                OrderTotalFee = g.Sum(m => m.Fee),
                OrderTotalRefund = g.Sum(m => m.Refund),
                OrderTotalLoss = g.Sum(m => m.Loss)
            }).FirstOrDefault();
        if (orderSum != null)
        {
            amount.Order = new AmountByOrderVm();
            amount.SetOrder(orderSum);
        }

        var purchaseSum = _dbContext.Purchase
            .Where(m => m.StoreId == storeId)
            .AsEnumerable()
            .GroupBy(m => m.StoreId)
            .Select(g => new AmountByPurchaseVm()
            {
                PurchaseTotal = g.Sum(m => m.PurchaseTotal),
                PurchaseTotalRefund = 0
            }).FirstOrDefault();
        if (purchaseSum != null)
        {
            amount.Purchase = new AmountByPurchaseVm();
            amount.SetPurchase(purchaseSum);
        }

        var waybillSum = _dbContext.Waybill
            .Where(m => m.StoreId == storeId)
            .AsEnumerable()
            .GroupBy(m => m.StoreId)
            .Select(g => new AmountByWaybillVm()
            {
                WaybillTotal = g.Sum(m => m.CarriageNum),
                WaybillTotalRefund = 0
            }).FirstOrDefault();
        if (waybillSum != null || waybillSum is not null || waybillSum.IsNotNull())
        {
        }

        if (waybillSum != null)
        {
            amount.Waybill = new AmountByWaybillVm();
            amount.SetWaybill(waybillSum);
        }

        return amount;
    }


    /// <summary>
    /// 获取店铺和用户组绑定后产生的财务
    /// </summary>
    /// <param name="storeId"></param>
    /// <param name="groupId"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    public async Task<AmountVm> AmountByTheStoreWithGroup(int storeId, int groupId)
    {
        var store = await _dbContext.StoreRegion.Where(m => m.ID == storeId).FirstOrDefaultAsync();
        if (store == null)
            throw new Exception("找不到相关店铺#1");

        var amount = new AmountVm();

        var orderSum = _dbContext.Order
            .Where(m => m.StoreId == storeId && m.GroupID == groupId)
            .AsEnumerable()
            .GroupBy(m => m.StoreId)
            .Select(g => new AmountByOrderVm()
            {
                OrderTotal = g.Sum(m => m.OrderTotal),
                OrderTotalFee = g.Sum(m => m.Fee),
                OrderTotalRefund = g.Sum(m => m.Refund),
                OrderTotalLoss = g.Sum(m => m.Loss)
            }).FirstOrDefault();
        if (orderSum != null)
        {
            amount.SetOrder(orderSum);
        }

        var purchaseSum = _dbContext.Purchase
            .Where(m => m.StoreId == storeId && m.GroupID == groupId)
            .AsEnumerable()
            .GroupBy(m => m.StoreId)
            .Select(g => new AmountByPurchaseVm()
            {
                PurchaseTotal = g.Sum(m => m.PurchaseTotal),
                PurchaseTotalRefund = 0
            }).FirstOrDefault();
        if (purchaseSum != null)
        {
            amount.SetPurchase(purchaseSum);
        }

        var waybillSum = _dbContext.Waybill
            .Where(m => m.StoreId == storeId && m.GroupID == groupId)
            .AsEnumerable()
            .GroupBy(m => m.StoreId)
            .Select(g => new AmountByWaybillVm()
            {
                WaybillTotal = g.Sum(m => m.CarriageNum),
                WaybillTotalRefund = 0
            }).FirstOrDefault();
        if (waybillSum != null || waybillSum is not null || waybillSum.IsNotNull())
        {
        }

        if (waybillSum != null)
        {
            amount.SetWaybill(waybillSum);
        }

        return amount;
    }
}