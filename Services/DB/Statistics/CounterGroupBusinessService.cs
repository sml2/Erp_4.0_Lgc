﻿using ERP.Enums.Statistics;
using Microsoft.EntityFrameworkCore;
using ERP.Extensions;
using ERP.Models.Statistics;
using ERP.Interface;
using ERP.Models.DB.Statistics;
using ERP.ViewModels.Statistics;
using ERP.Data;
using ERP.Distribution.Abstraction;

namespace ERP.Services.Statistics;

public class CounterGroupBusinessService : BaseService
{

    public readonly DBContext _dbContext;
    private readonly ISessionProvider _SessionProvider;

    public CounterGroupBusinessService(DBContext dbContext, ISessionProvider SessionProvider)
    {
        _SessionProvider = SessionProvider;
        _dbContext = dbContext;
    }
    private int UserID { get => _SessionProvider.Session!.GetUserID(); }
    private int GroupID { get => _SessionProvider.Session!.GetGroupID(); }
    private int CompanyID { get => _SessionProvider.Session!.GetCompanyID(); }
    private int OEMID { get => _SessionProvider.Session!.GetOEMID(); }

    /// <summary>
    /// 初始化统计数据
    /// </summary>
    /// <param name="groupId"></param>
    /// <param name="companyId"></param>
    /// <param name="oemId"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> InitCounterGroupBusiness(int groupId, int companyId, int oemId)
    {
        //$insertData = [
        //    'group_id'   => $group_id,
        //    'company_id' => $company_id,
        //    'oem_id'     => $oem_id,
        //    'created_at' => date("Y-m-d H:i:s", $this->time)
        //];
        //CounterGroupBusinessModel::query()->insert($insertData);

        var insertData = new CounterGroupBusiness() { GroupID = groupId, CompanyID = companyId, OEMID = oemId };

        await _dbContext.CounterGroupBusiness.AddAsync(insertData);
        await _dbContext.SaveChangesAsync();
        return ReturnArr(true);
    }

    /// <summary>
    /// 获取指定用户组ids业务统计
    /// </summary>
    /// <param name="ids"></param>
    /// <param name="userBusiness"></param>
    /// <returns></returns>
    public async Task<List<HomePageVM>> GetCountGroupByIds(List<int> ids, bool userBusiness = true)
    {
        
        IQueryable<Statistic> model = _dbContext.Statistic
            .Where(m =>
                ids.Contains(m.AboutID)
                && (m.AboutType & Types.Role) == Types.UserGroup
                && (m.AboutType & Types.Effective) == Types.TOTAL
            );

        if (userBusiness)
        {
            return await model.Select(m => new HomePageVM
            {
                GroupId = m.AboutID,
                UserNum = m.UserCount,
                ProductNum = m.ProductCount,
                OrderNum = m.OrderCount,
                PurchaseNum = m.PurchaseCount,
                WaybillNum = m.WaybillCount,
                StoreNum = m.StoreCount,
                DistributionOrderNum = m.DistributionOrderCount,
                DistributionPurchaseNum = m.DistributionPurchaseCount,
                DistributionWaybillNum = m.DistributionWaybillCount
            }).ToListAsync();
        }
        else
        {
            return await model.Select(m => new HomePageVM
            {
                GroupId = m.AboutID,
                UserNum = m.UserCount,
                ProductNum = m.ProductCount,
                OrderNum = m.OrderCount,
                PurchaseNum = m.PurchaseCount,
                WaybillNum = m.WaybillCount,
                StoreNum = m.StoreCount
            }).ToListAsync();
        }
    }

    /// <summary>
    /// 获取指定用户组财务统计信息
    /// </summary>
    /// <param name="groupIds"></param>
    /// <returns></returns>
    public async Task<List<GroupFinanceDto>> GetFinance(IEnumerable<int> groupIds)
    {
        var list = await _dbContext.Statistic.Where(m =>
                groupIds.Contains(m.AboutID)
                && (m.AboutType & Types.Role) == Types.UserGroup
                && (m.AboutType & Types.Effective) == Types.TOTAL
            )
            .Select(m => new GroupFinanceDto(m.AboutID, m.OrderTotal,
                m.OrderTotalFee,
                m.OrderTotalRefund,
                m.OrderTotalLoss,
                m.PurchaseTotal,
                m.PurchaseTotalRefund,
                m.WaybillTotal,
                m.WaybillTotalRefund,
                m.RechargeTotal,m.ExpenseTotal, m.SelfRechargeTotal, m.SelfExpenseTotal))
            .ToListAsync();

        return list;

    }

    /// <summary>
    /// 指定用户组删除
    /// </summary>
    /// <param name="groupId"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DelCounterGroupBusiness(int groupId)
    {
        //return $this->counterGroupBusinessModel::query()->groupId($group_id)->delete();
        var info = await _dbContext.CounterGroupBusiness.FirstOrDefaultAsync(m => m.GroupID == groupId);

        if (info == null)
        {
            return ReturnArr(false, "找不到相关数据");
        }
        _dbContext.CounterGroupBusiness.Remove(info);
        await _dbContext.SaveChangesAsync();
        return ReturnArr(true);
    }

    /// <summary>
    /// 增加当前用户组创建开通用户数
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> UserNumAdd(int count = 1)
    {
        //return $this->counterGroupBusinessModel
        //    ->groupId($this->getSession()->getGroupId())
        //    ->increment('user_num', $user_num);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 增加当前用户组创建产品数
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ProductNumAdd(int count = 1)
    {
        //return $this->counterGroupBusinessModel
        //    ->groupId($this->getSession()->getGroupId())
        //    ->increment('product_num', $product_num);
        return await Task.FromResult(ReturnArr(true));
    }

    /**
     * 减少当前用户组建产品数
     * User: CCY
     * Date: 2021/4/15
     * @param string $groupId
     * @param int    $product_num
     * @return mixed
     */
    public async Task<ReturnStruct> ProductNumReduce(int groupId, int count = 1)
    {
        //$groupId == '' && $groupId = $this->getSession()->getGroupId();
        //return CounterGroupBusinessModel::query()
        //    ->groupId($groupId)
        //    ->decrement('product_num', $product_num);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 增加当前用户组创建订单数
    /// </summary>
    /// <param name=""></param>
    /// <returns></returns>
    public async Task<ReturnStruct> OrderNumAdd(int count = 1)
    {
        //return $this->counterGroupBusinessModel
        //    ->groupId($this->getSession()->getGroupId())
        //    ->increment('order_num', $order_num);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 增加当前用户组创建采购数
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> PurchaseNumAdd(int count = 1)
    {
        //return $this->counterGroupBusinessModel
        //    ->groupId($this->getSession()->getGroupId())
        //    ->increment('purchase_num', $purchase_num);
        return await Task.FromResult(ReturnArr(true));

    }

    /// <summary>
    /// 增加当前用户组创建运单数
    /// </summary>
    /// <param name="waybillNum"></param>
    /// <param name="distributionNum"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> WaybillNumAdd(int waybillNum = 1, int distributionNum = 0)
    {
        //  $update = [
        //      'waybill_num'              => DB::raw($this->counterGroupBusinessModel->transformKey('waybill_num'). '+'. $waybill_num),
        //      'distribution_waybill_num' => DB::raw($this->counterGroupBusinessModel->transformKey('distribution_waybill_num'). '+'. $distribution_num)
        //  ];
       var Business=await _dbContext.CounterGroupBusiness.Where(x => x.ID == GroupID).SingleOrDefaultAsync();
        if (Business != null)
        {
            Business.WaybillNum += waybillNum;
            Business.DistributionOrderNum += distributionNum;
            return ReturnArr(true);
        }
        return ReturnArr(false);
    }

    /// <summary>
    /// 增加当前用户组创建店铺数
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> StoreNumAdd(int count = 1)
    {

        var Group = await _dbContext.CounterGroupBusiness.Where(x => x.GroupID == GroupID).FirstOrDefaultAsync();
       if(Group!=null)
        Group.StoreNum += count;
       _dbContext.SaveChanges();
        return ReturnArr(true);
    }


    /// <summary>
    /// 增加当前用户组创建店铺数
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> StoreNumReduce(int count = 1)
    {
        //return $this->counterGroupBusinessModel
        //    ->groupId($this->getSession()->getGroupId())
        //    ->decrement('store_num', $store_num);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 减少当前用户组创建店铺数
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> StoreUserNumReduce(int count, int groupId)
    {
        var data = await _dbContext.CounterGroupBusiness.Where(x => x.GroupID == groupId).FirstOrDefaultAsync();
        if (data != null)
        {
            data.StoreNum = data.StoreNum - count;                 
            await _dbContext.SaveChangesAsync();
            return ReturnArr(true);
        }
        return ReturnArr(false);
    }

    /**
     * 增加当前用户组处理上报分销订单数
     * User: CCY
     * @param int $distribution_order_num
     * @return int
     */
    public async Task<ReturnStruct> DistributionOrderNumAdd(int count = 1)
    {
        //return $this->counterGroupBusinessModel
        //    ->groupId($this->getSession()->getGroupId())
        //    ->increment('distribution_order_num', $distribution_order_num);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 增加当前用户组处理上报分销采购数
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DistributionPurchaseNumAdd(int count = 1)
    {
        //return $this->counterGroupBusinessModel
        //    ->groupId($this->getSession()->getGroupId())
        //    ->increment('distribution_purchase_num', $distribution_purchase_num);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 增加当前用户组处理上报分销运单数
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DistributionWaybillNumAdd(int count = 1)
    {
        //return $this->counterGroupBusinessModel
        //    ->groupId($this->getSession()->getGroupId())
        //    ->increment('distribution_waybill_num', $distribution_waybill_num);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 同时增加当前用户组创建订单数、采购数、订单总价、采购总价
    /// </summary>
    /// <param name="order_num"></param>
    /// <param name="purchase_num"></param>
    /// <param name="order_total"></param>
    /// <param name="purchase_total"></param>
    /// <param name="groupId"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> OrderPurchaseNumAdd(int order_num = 1, int purchase_num = 1, int order_total = 0, int purchase_total = 0, int groupId = 0)
    {
        //  $groupId = $groupId ?: $this->getSession()->getGroupId();
        //  $updateData = [
        //      'order_num'    => DB::raw($this->counterGroupBusinessModel->transformKey('order_num'). '+'. $order_num),
        //      'purchase_num' => DB::raw($this->counterGroupBusinessModel->transformKey('purchase_num'). '+'. $purchase_num)
        //  ];
        //  $order_total != 0 && $updateData['order_total'] = DB::raw($this->counterGroupBusinessModel->transformKey('order_total'). '+'. $order_total);
        //  $purchase_total != 0 && $updateData['purchase_total'] = DB::raw($this->counterGroupBusinessModel->transformKey('purchase_total'). '+'. $purchase_total);
        //return $this->counterGroupBusinessModel
        //    ->groupId($groupId)
        //    ->update($updateData);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 同时增加当前用户组处理上报订单数、采购数
    /// </summary>
    /// <param name="orderNum"></param>
    /// <param name="purchaseNum"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> distributionOrderPurchaseNumAdd(int orderNum = 1, int purchaseNum = 1)
    {
        //  $updateData = [
        //      'distribution_order_num'    => DB::raw($this->counterGroupBusinessModel->transformKey('distribution_order_num'). '+'. $order_num),
        //      'distribution_purchase_num' => DB::raw($this->counterGroupBusinessModel->transformKey('distribution_purchase_num'). '+'. $purchase_num)
        //  ];
        //return $this->counterGroupBusinessModel
        //    ->groupId($this->getSession()->getGroupId())
        //    ->update($updateData);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 同时增加处理上报分销运单数、运费
    /// </summary>
    /// <param name="groupId"></param>
    /// <param name="waybillTotal"></param>
    /// <param name="distributionWaybillNum"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DistributionWaybillNumAndTotal(int groupId, int waybillTotal = 0, int distributionWaybillNum = 1)
    {
        //  $group_id == '' && $group_id = $this->getSession()->getGroupId();
        //  $updateData = [
        //      'distribution_waybill_num' => DB::raw($this->counterGroupBusinessModel->transformKey('distribution_waybill_num'). '+'. $distribution_waybill_num),
        //      'waybill_total'            => DB::raw($this->counterGroupBusinessModel->transformKey('waybill_total'). '+'. $waybill_total)
        //  ];
        //return $this->counterGroupBusinessModel
        //    ->groupId($group_id)
        //    ->update($updateData);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 增加上报运单运费
    /// </summary>
    /// <param name="waybillTotal"></param>
    /// <param name="groupId"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DistributionWaybillTotal(double Money)
    {
        //  $group_id == '' && $group_id = $this->getSession()->getGroupId();
        //return $this->counterGroupBusinessModel
        //    ->groupId($group_id)
        //    ->increment('waybill_total', $waybill_total);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 同时更新分销采购数和采购总额
    /// </summary>
    /// <param name="total"></param>
    /// <param name="number"></param>
    /// <param name="group_id"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DistributionPurchaseNumAndTotal(int group_id, int total = 0, int number = 1)
    {
        //  $group_id == '' && $group_id = $this->getSession()->getGroupId();
        //  $updateData = [
        //      'distribution_purchase_num' => DB::raw($this->counterGroupBusinessModel->transformKey('distribution_purchase_num'). '+'. $number),
        //      'purchase_total'            => DB::raw($this->counterGroupBusinessModel->transformKey('purchase_total'). '+'. $total),
        //  ];
        //return $this->counterGroupBusinessModel
        //    ->groupId($group_id)
        //    ->update($updateData);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 采购汇总
    /// </summary>
    /// <param name="total"></param>
    /// <param name="group_id"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> PurchaseTotalAdd(int total, int group_id)
    {
        //  $group_id == '' && $group_id = $this->getSession()->getGroupId();
        //return $this->counterGroupBusinessModel
        //    ->groupId($group_id)
        //    ->increment('purchase_total', $total);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 指定用户组充值汇总
    /// </summary>
    /// <param name="groupId"></param>
    /// <param name="recharge"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> RechargeNumAdd(int groupId, double recharge)
    {
        //return $this->counterGroupBusinessModel
        //    ->groupId($group_id)
        //    ->increment('recharge_total', $recharge);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 指定用户组充值汇总
    /// </summary>
    /// <param name="groupId"></param>
    /// <param name="recharge"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> SelfRechargeNumAdd(int groupId, decimal recharge)
    {
        //return $this->counterGroupBusinessModel
        //    ->groupId($group_id)
        //    ->increment('self_recharge_total', $recharge);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 指定用户组支出汇总
    /// </summary>
    /// <param name="groupId"></param>
    /// <param name="recharge"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ExpenseNumAddd(int groupId, double recharge)
    {
        //return $this->counterGroupBusinessModel
        //    ->groupId($group_id)
        //    ->increment('expense_total', $expense);
        return await Task.FromResult(ReturnArr(true));
    }


    /// <summary>
    /// 账单用户组汇总 增加 退款
    /// </summary>
    /// <param name="groupId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> addOrderTotalRefund(int groupId, double money)
    {
        //return $this->counterGroupBusinessModel
        //    ->groupId($group_id)
        //    ->increment('order_total_refund', $money);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 账单用户组汇总 减少 退款
    /// </summary>
    /// <param name="groupId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ReduceOrderTotalRefund(int groupId, double money)
    {
        //return $this->counterGroupBusinessModel
        //    ->groupId($group_id)
        //    ->decrement('order_total_refund', $money);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 账单用户组汇总 增加 手续费
    /// </summary>
    /// <param name="groupId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> AddOrderTotalFee(int groupId, double money)
    {
        //return $this->counterGroupBusinessModel
        //    ->groupId($group_id)
        //    ->increment('order_total_fee', $money);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 账单用户组汇总 减少 手续费
    /// </summary>
    /// <param name="groupId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ReduceOrderTotalFee(int groupId, double money)
    {
        //return $this->counterGroupBusinessModel
        //    ->groupId($group_id)
        //    ->decrement('order_total_fee', $money);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 账单用户组汇总 增加 订单损失费用
    /// </summary>
    /// <param name="groupId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> addOrderLossFee(int groupId, double money)
    {
        //if ($group_id == 0) {
        //    return true;
        //}
        //return CounterGroupBusinessModel::query()
        //    ->groupId($group_id)
        //->increment('order_total_loss', $money);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 账单用户组汇总 减少 订单损失费用
    /// </summary>
    /// <param name="groupId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ReduceOrderLossFee(int groupId, double money)
    {
        //if ($group_id == 0) {
        //    return true;
        //}
        //return CounterGroupBusinessModel::query()
        //    ->groupId($group_id)
        //      ->decrement('order_total_loss', $money);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 用户组上报订单
    /// </summary>
    /// <param name="groupId"></param>
    /// <param name="orderNum"></param>
    /// <param name="orderTotal"></param>
    /// <param name="orderTotalFee"></param>
    /// <param name="orderTotalRefund"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> IncrementOrder(int groupId, int orderNum = 0, int orderTotal = 0, int orderTotalFee = 0, int orderTotalRefund = 0)
    {
        //  $updateData = [
        //      'order_num'          => DB::raw($this->counterGroupBusinessModel->transformKey('order_num'). '+'. $order_num),
        //      'order_total'        => DB::raw($this->counterGroupBusinessModel->transformKey('order_total'). '+'. $order_total),
        //      'order_total_fee'    => DB::raw($this->counterGroupBusinessModel->transformKey('order_total_fee'). '+'. $order_total_fee),
        //      'order_total_refund' => DB::raw($this->counterGroupBusinessModel->transformKey('order_total_refund'). '+'. $order_total_refund)
        //  ];
        //return $this->counterGroupBusinessModel::query()
        //    ->groupId($group_id)
        //    ->update($updateData);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 用户组上报订单 减少
    /// </summary>
    /// <param name="groupId"></param>
    /// <param name="orderNum"></param>
    /// <param name="orderTotal"></param>
    /// <param name="orderTotalFee"></param>
    /// <param name="orderTotalRefund"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DecrementOrder(int groupId, int orderNum = 0, int orderTotal = 0, int orderTotalFee = 0, int orderTotalRefund = 0)
    {
        //  $updateData = [
        //      'order_num'          => DB::raw($this->counterGroupBusinessModel->transformKey('order_num'). '-'. $order_num),
        //      'order_total'        => DB::raw($this->counterGroupBusinessModel->transformKey('order_total'). '-'. $order_total),
        //      'order_total_fee'    => DB::raw($this->counterGroupBusinessModel->transformKey('order_total_fee'). '-'. $order_total_fee),
        //      'order_total_refund' => DB::raw($this->counterGroupBusinessModel->transformKey('order_total_refund'). '-'. $order_total_refund)
        //  ];
        //return $this->counterGroupBusinessModel::query()
        //    ->groupId($group_id)
        //    ->update($updateData);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 指定用户组支出汇总
    /// </summary>
    /// <param name="groupId"></param>
    /// <param name="expense"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> SelfExpenseNumAdd(int groupId, decimal expense)
    {
        //return $this->counterGroupBusinessModel
        //    ->groupId($group_id)
        //    ->increment('self_expense_total', $expense);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 同时更新采购数和采购总额
    /// </summary>
    /// <param name="groupId"></param>
    /// <param name="total"></param>
    /// <param name="number"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> changePurchaseNumAndTotal(int groupId, double total = 0.00, int number = 1)
    {
        //  $updateUser = [
        //      'purchase_num'   => DB::raw($this->counterGroupBusinessModel->transformKey('purchase_num'). '+'. $number),
        //      'purchase_total' => DB::raw($this->counterGroupBusinessModel->transformKey('purchase_total'). '+'. $total),
        //  ];
        //return $this->counterGroupBusinessModel
        //    ->where('group_id', $groupId)
        //    ->update($updateUser);
        return await Task.FromResult(ReturnArr(true));
    }

}

