﻿using Microsoft.EntityFrameworkCore;
using ERP.Data;
using ERP.Models.Setting;

namespace ERP.Services.Statistics;

using Extensions;

public class ChartService : BaseService
{
    readonly DBContext _dbContext;

    public ChartService(DBContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<object> GetChart(string name)
    {
        var list = await _dbContext.StatisticsChart
            .ToListAsync();
        var date = new List<string>() { };
        var total = new List<object>() { };

        //处理日期
        foreach (var Item in list)
        {
            date.Add(Item.CreatedAt.ToString("yyyy-MM-dd"));
            total.Add(Item.GetPropOrFieldValue<int?>(name).GetValueOrDefault());
        }

        return new { date, total };
    }

    public async Task<Dictionary<string, object>> GetMonthChart(string name)
    {
        var oemList = await _dbContext.OEM.Where(m => m.State == OEM.States.ENABLE)
            .ToDictionaryAsync(m => m.ID, m => m);

        var monthCount = await _dbContext.MonthCounter.OrderBy(m => m.CreatedAt).ToListAsync();
        var result = new Dictionary<string, object>();

        if (monthCount.Count <= 0)
        {
            result.Add(name, new List<string>(){});
            result.Add("date", new List<string>(){});
            result.Add("legend", new List<string>(){});
            return result;
        }


        var num = new Dictionary<int, List<int>>();
        var legend = new List<string>();
        var data = new Dictionary<string, Dictionary<int, object>>();
        foreach (var item in monthCount)
        {
            if (!oemList.Keys.Contains(item.OemId))
                continue;
            var oemId = item.OemId;
            var oemInfo = oemList[item.OemId];
            legend.Add(oemInfo.Name);

            if (!num.Keys.Contains(oemId))
            {
                num.Add(oemId, new List<int>() { item.GetPropOrFieldValue<int>(name) });
            }
            else
            {
                num[oemId].Add(item.GetPropOrFieldValue<int>(name));
            }

            if (!data.Keys.Contains(name))
            {
                data.Add(name, new Dictionary<int, object>()
                {
                    {
                        oemId, new Dictionary<string, object>()
                        {
                            { "name", oemInfo.Name },
                            { "type", "line" },
                            { "data", num[oemId] }
                        }
                    }
                });
            }
            else
            {
                if (!data[name].Keys.Contains(oemId))
                {
                    data[name].Add(oemId, new Dictionary<string, object>()
                    {
                        { "name", oemInfo.Name },
                        { "type", "line" },
                        { "data", num[oemId] }
                    });
                }
                else
                {
                    data[name][oemId] = new Dictionary<string, object>()
                    {
                        { "name", oemInfo.Name },
                        { "type", "line" },
                        { "data", num[oemId] }
                    };
                }
            }
        }

        var date = monthCount.Select(m => m.CreatedAt.ToString("yyyy-MM")).Distinct().ToList();

        result.Add(name, data[name].Values.ToList());
        result.Add("date", date);
        result.Add("legend", legend.Distinct().ToList());
        return result;
    }
}