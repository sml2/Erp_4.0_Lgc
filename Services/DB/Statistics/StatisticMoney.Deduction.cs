using ERP.Enums.Finance;
using ERP.Enums.Statistics;
using ERP.Exceptions;
using ERP.Models.DB.Orders;
using ERP.Models.Finance;
using ERP.Services.DB.Statistics;
using ERP.ViewModels.Statistics;
using Exception = System.Exception;
using Statistic = ERP.Models.DB.Statistics.Statistic;

namespace ERP.Services.Statistics;

public partial class StatisticMoney
{
    #region 店铺财务

    #endregion

    /// <summary>
    /// 店铺删除清理与用户相关的订单财务，不出处理用户的采购物流金额
    /// </summary>
    /// <param name="userIds"></param>
    /// <param name="userStoreAmount"></param>
    /// <exception cref="Exception"></exception>
    public async Task ReduceStoreOrderFianceToUser(List<int> userIds,
        Dictionary<int, UserAmountByOrderVm> userStoreAmount)
    {
        var data = new List<Statistic>();
        foreach (var userId in userIds)
        {
            data.Add(await _statisticService.GetStatisticInfo(userId, Types.User, Types.DAY));
        }

        if (data.Count != userIds.Count)
        {
            throw new Exception("用户财务数据丢失");
        }

        foreach (var item in data)
        {
            if (userStoreAmount.Keys.Contains(item.AboutID))
            {
                OpReduceOrder(item, userStoreAmount[item.AboutID]);
            }
        }

        await _statisticService.UpdateRangeInfo(data);
    }

    /// <summary>
    /// 店铺删除清理与用户组相关的订单财务，不出处理用户的采购物流金额
    /// </summary>
    /// <param name="groupIds"></param>
    /// <param name="groupStoreAmount"></param>
    /// <exception cref="Exception"></exception>
    public async Task ReduceStoreOrderFianceToGroup(List<int> groupIds,
        Dictionary<int, GroupAmountByOrderVm> groupStoreAmount)
    {
        var data = new List<Statistic>();
        foreach (var groupId in groupIds)
        {
            data.Add(await _statisticService.GetStatisticInfo(groupId, Types.UserGroup, Types.DAY));
        }

        if (data.Count != groupIds.Count)
        {
            throw new Exception("用户组财务数据丢失");
        }

        foreach (var item in data)
        {
            if (groupStoreAmount.Keys.Contains(item.AboutID))
            {
                OpReduceOrder(item, groupStoreAmount[item.AboutID]);
            }
        }

        await _statisticService.UpdateRangeInfo(data);
    }

    /// <summary>
    /// 移除店铺订单财务所在的Oem部分
    /// </summary>
    /// <param name="oemId"></param>
    /// <param name="vm"></param>
    public async Task RemoveStoreOrderFianceToOem(int oemId, AmountVm vm)
    {
        var statistics = await _statisticService.GetStatisticInfo(oemId, Types.OEM, Types.DAY);
        if (vm.Order != null)
        {
            OpReduceOrder(statistics, vm.Order);
            await _statisticService.UpdateInfo(statistics);
        }
    }

    /// <summary>
    /// 移除店铺订单财务所在的公司部分
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="vm"></param>
    public async Task RemoveStoreOrderFianceToCompany(int companyId, AmountVm vm)
    {
        var statistics = await _statisticService.GetStatisticInfo(companyId, Types.Company, Types.DAY);
        if (vm.Order != null)
        {
            OpReduceOrder(statistics, vm.Order);
            await _statisticService.UpdateInfo(statistics);
        }
    }

    public async Task RemoveStoreFinanceWithOem(int oemId, AmountVm vm)
    {
        var statistics = await _statisticService.GetStatisticInfo(oemId, Types.OEM, Types.DAY);
        if (vm.Order != null)
        {
            OpReduceOrder(statistics, vm.Order);
            await _statisticService.UpdateInfo(statistics);
        }
    }

    public async Task RemoveStoreFinanceWithCompany(int companyId, AmountVm vm)
    {
        var statistics = await _statisticService.GetStatisticInfo(companyId, Types.Company, Types.DAY);
        if (vm.Order != null)
        {
            OpReduceOrder(statistics, vm.Order);
            await _statisticService.UpdateInfo(statistics);
        }
    }

    public async Task StoreFinancialWithStore(int storeId, AmountVm vm)
    {
        var statistics = await _statisticService.GetStatisticInfo(storeId, Types.Store, Types.DAY);
        //Order
        if (vm.Order != null)
            OpReduceOrder(statistics, vm.Order);
        //Purchase
        // if(vm.Purchase != null)
        // OpPurchase(statistics,vm.Purchase);
        //Waybill
        // if(vm.Waybill != null)
        // OpWaybill(statistics,vm.Waybill);

        if (vm.Order != null)
            await _statisticService.UpdateInfo(statistics);
    }

    public async Task StoreFinancialWithOem(int oemId, AmountVm vm)
    {
        var statistics = await _statisticService.GetStatisticInfo(oemId, Types.OEM, Types.DAY);
        //Order
        if (vm.Order != null)
            OpReduceOrder(statistics, vm.Order);
        //Purchase
        // if(vm.Purchase != null)
        // OpPurchase(statistics,vm.Purchase);
        //Waybill
        // if(vm.Waybill != null)
        // OpWaybill(statistics,vm.Waybill);

        if (vm.Order != null)
            await _statisticService.UpdateInfo(statistics);
    }

    public async Task StoreFinancialWithCompany(int companyId, AmountVm vm)
    {
        var statistics = await _statisticService.GetStatisticInfo(companyId, Types.Company, Types.DAY);
        //Order
        if (vm.Order != null)
            OpReduceOrder(statistics, vm.Order);
        //Purchase
        // if(vm.Purchase != null)
        //     OpPurchase(statistics,vm.Purchase);
        //Waybill
        // if(vm.Waybill != null)
        //     OpWaybill(statistics,vm.Waybill);

        if (vm.Order != null)
            await _statisticService.UpdateInfo(statistics);
    }

    public async Task StoreFinancialWithUser(int userId, AmountVm vm)
    {
        var statistics = await _statisticService.GetStatisticInfo(userId, Types.User, Types.DAY);
        //Order
        if (vm.Order != null)
            OpReduceOrder(statistics, vm.Order);
        //Purchase
        // if(vm.Purchase != null)
        //     OpPurchase(statistics,vm.Purchase);
        //Waybill
        // if(vm.Waybill != null)
        // OpWaybill(statistics,vm.Waybill);

        if (vm.Order != null)
            await _statisticService.UpdateInfo(statistics);
    }

    /// <summary>
    /// 减订单财务
    /// </summary>
    /// <param name="statistics"></param>
    /// <param name="vm"></param>
    private void OpReduceOrder(Statistic statistics, AmountByOrderVm vm)
    {
        statistics.OrderTotal -= vm.OrderTotal.Money;
        statistics.OrderTotalRefund -= vm.OrderTotalRefund.Money;
        statistics.OrderTotalFee -= vm.OrderTotalFee.Money;
        statistics.OrderTotalLoss -= vm.OrderTotalLoss.Money;
    }

    /// <summary>
    /// 加订单财务
    /// </summary>
    /// <param name="statistics"></param>
    /// <param name="vm"></param>
    private void OpPlusOrder(Statistic statistics, AmountByOrderVm vm)
    {
        statistics.OrderTotal += vm.OrderTotal.Money;
        statistics.OrderTotalRefund += vm.OrderTotalRefund.Money;
        statistics.OrderTotalFee += vm.OrderTotalFee.Money;
        statistics.OrderTotalLoss += vm.OrderTotalLoss.Money;
    }

    /// <summary>
    /// 减采购财务
    /// </summary>
    /// <param name="statistics"></param>
    /// <param name="vm"></param>
    private void OpReducePurchase(Statistic statistics, AmountByPurchaseVm vm)
    {
        statistics.PurchaseTotal -= vm.PurchaseTotal.Money;
        statistics.PurchaseTotalRefund -= vm.PurchaseTotal.Money;
    }

    /// <summary>
    /// 加采购财务
    /// </summary>
    /// <param name="statistics"></param>
    /// <param name="vm"></param>
    private void OpPlusPurchase(Statistic statistics, AmountByPurchaseVm vm)
    {
        statistics.PurchaseTotal += vm.PurchaseTotal.Money;
        statistics.PurchaseTotalRefund += vm.PurchaseTotal.Money;
    }

    /// <summary>
    /// 减物流财务
    /// </summary>
    /// <param name="statistics"></param>
    /// <param name="vm"></param>
    private void OpReduceWaybill(Statistic statistics, AmountByWaybillVm vm)
    {
        statistics.WaybillTotal -= vm.WaybillTotal.Money;
        statistics.WaybillTotalRefund -= vm.WaybillTotalRefund.Money;
    }

    /// <summary>
    /// 加物流财务
    /// </summary>
    /// <param name="statistics"></param>
    /// <param name="vm"></param>
    private void OpPlusWaybill(Statistic statistics, AmountByWaybillVm vm)
    {
        statistics.WaybillTotal += vm.WaybillTotal.Money;
        statistics.WaybillTotalRefund += vm.WaybillTotalRefund.Money;
    }

    /// <summary>
    /// 店铺重新归属于用户将之前产生的财务增加回来
    /// </summary>
    /// <param name="userIds"></param>
    /// <param name="userStoreAmount"></param>
    /// <exception cref="Exception"></exception>
    public async Task PlusStoreOrderFianceToUser(List<int> userIds,
        Dictionary<int, UserAmountByOrderVm> userStoreAmount)
    {
        var data = new List<Statistic>();
        foreach (var userId in userIds)
        {
            data.Add(await _statisticService.GetStatisticInfo(userId, Types.User, Types.DAY));
        }

        if (data.Count != userIds.Count)
        {
            throw new Exception("用户财务数据丢失");
        }

        foreach (var item in data)
        {
            if (userStoreAmount.Keys.Contains(item.AboutID))
            {
                OpPlusOrder(item, userStoreAmount[item.AboutID]);
            }
        }

        await _statisticService.UpdateRangeInfo(data);
    }

    public async Task PlusStoreOrderFianceToGroup(List<int> groupIds,
        Dictionary<int, GroupAmountByOrderVm> groupStoreAmount)
    {
        var data = new List<Statistic>();
        foreach (var groupId in groupIds)
        {
            data.Add(await _statisticService.GetStatisticInfo(groupId, Types.UserGroup, Types.DAY));
        }

        if (data.Count != groupIds.Count)
        {
            throw new Exception("用户组财务数据丢失");
        }

        foreach (var item in data)
        {
            if (groupStoreAmount.Keys.Contains(item.AboutID))
            {
                OpPlusOrder(item, groupStoreAmount[item.AboutID]);
            }
        }

        await _statisticService.UpdateRangeInfo(data);
    }

    public async Task ReduceFianceToGroup(int groupId, AmountVm vm)
    {
        var statistics = await _statisticService.GetStatisticInfo(groupId, Types.UserGroup, Types.DAY);
        //Order
        if (vm.Order != null)
            OpReduceOrder(statistics, vm.Order);
        //Purchase
        if (vm.Purchase != null)
            OpReducePurchase(statistics, vm.Purchase);
        //Waybill
        if (vm.Waybill != null)
            OpReduceWaybill(statistics, vm.Waybill);

        if (vm.Order != null || vm.Purchase != null || vm.Waybill != null)
            await _statisticService.UpdateInfo(statistics);
    }

    public async Task PlusFianceToGroup(int groupId, AmountVm vm)
    {
        var statistics = await _statisticService.GetStatisticInfo(groupId, Types.UserGroup, Types.DAY);
        //Order
        if (vm.Order != null)
            OpPlusOrder(statistics, vm.Order);
        //Purchase
        if (vm.Purchase != null)
            OpPlusPurchase(statistics, vm.Purchase);
        //Waybill
        if (vm.Waybill != null)
            OpPlusWaybill(statistics, vm.Waybill);

        if (vm.Order != null || vm.Purchase != null || vm.Waybill != null)
            await _statisticService.UpdateInfo(statistics);
    }
}