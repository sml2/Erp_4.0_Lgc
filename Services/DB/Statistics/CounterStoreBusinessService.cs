﻿using Microsoft.EntityFrameworkCore;
using ERP.Extensions;
using ERP.Models.Statistics;
using ERP.Data;
using ERP.Distribution.Abstraction;
using ERP.Enums.Statistics;

namespace ERP.Services.Statistics;

public class CounterStoreBusinessService : BaseService
{
    public readonly DBContext _dbContext;

    public CounterStoreBusinessService(DBContext dbContext)
    {
        _dbContext = dbContext;
    }

    /// <summary>
    /// 初始化统计数据
    /// </summary>
    /// <param name="storeId"></param>
    /// <param name="companyId"></param>
    /// <param name="oemId"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> InitCounterStoreBusiness(int storeId, int companyId, int oemId)
    {
        var insertData = new CounterStoreBusiness() { StoreId = storeId, CompanyID = companyId, OEMID = oemId };

        await _dbContext.CounterStoreBusiness.AddAsync(insertData);
        await _dbContext.SaveChangesAsync();
        return ReturnArr(true);
    }

    /// <summary>
    /// 获取指定店铺ids业务统计
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> GetCountStoreByIds(List<int> ids)
    {
        //$field = ['store_id', 'order_total', 'order_total_fee', 'order_total_refund', 'purchase_total', 'purchase_total_refund', 'waybill_total', 'waybill_total_refund', 'self_recharge_total', 'self_expense_total', 'recharge_total', 'expense_total'];
        //return $this->counterStoreBusinessModel::query()
        //    ->inStoreId($ids)
        //    ->select($field)
        //    ->get();

        var list = await _dbContext.CounterStoreBusiness.Where(m => ids.Contains(m.StoreId)).ToListAsync();
        return ReturnArr(true, "", list);
    }

    /// <summary>
    /// 获取指定店铺财务统计信息
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    public async Task<List<StoreFinanceDto>> GetFinance(IEnumerable<int> ids)
    {
        var list = await _dbContext.Statistic
            .Where(m =>
                ids.Contains(m.AboutID)
                && (m.AboutType & Types.Role) == Types.Store
                && (m.AboutType & Types.Effective) == Types.TOTAL
            )
            .Select(m => new StoreFinanceDto(m.AboutID, m.OrderTotal,
                m.OrderTotalFee,
                m.OrderTotalRefund,
                m.OrderTotalLoss,
                m.PurchaseTotal,
                m.PurchaseTotalRefund,
                m.WaybillTotal,
                m.WaybillTotalRefund))
            .ToListAsync();
        return list;
    }

    /// <summary>
    /// 指定店铺删除
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DelCounterStoreBusiness(List<int> ids)
    {
        //return $this->counterStoreBusinessModel::query()->storeId($store_id)->delete();
        var list = await _dbContext.CounterStoreBusiness.Where(m => ids.Contains(m.StoreId)).ToListAsync();

        _dbContext.CounterStoreBusiness.RemoveRange(list);
        await _dbContext.SaveChangesAsync();
        return ReturnArr(true);
    }

    /// <summary>
    /// 增加采购金额
    /// </summary>
    /// <param name="storeId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> AddPurchaseTotal(int storeId, double money)
    {
        //return $this->counterStoreBusinessModel->storeId($storeId)
        //    ->increment('purchase_total', $money);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 减少采购金额
    /// </summary>
    /// <param name=""></param>
    /// <param name=""></param>
    /// <param name=""></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ReducePurchaseTotal(int storeId, double money)
    {
        //return $this->counterStoreBusinessModel->storeId($storeId)
        //    ->decrement('purchase_total', $money);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 增加运费汇总
    /// </summary>
    /// <param name="storeId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> AddWaybillTotal(int? storeId, double money)
    {
        //return $this->counterStoreBusinessModel
        //    ->storeId($store_id)
        //    ->increment('waybill_total', $money);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 订单汇总 退款 增加
    /// </summary>
    /// <param name="storeId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> AddOrderTotalRefund(int storeId, double money)
    {
        //return $this->counterStoreBusinessModel
        //    ->storeId($store_id)
        //    ->increment('order_total_refund', $money);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 订单汇总 退款 减少
    /// </summary>
    /// <param name="storeId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ReduceOrderTotalRefund(int storeId, double money)
    {
        //return $this->counterStoreBusinessModel
        //    ->storeId($store_id)
        //    ->decrement('order_total_refund', $money);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 订单汇总 手续费 增加
    /// </summary>
    /// <param name="storeId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> AddOrderTotalFee(int storeId, double money)
    {
        //return $this->counterStoreBusinessModel
        //    ->storeId($store_id)
        //    ->increment('order_total_fee', $money);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 订单汇总 手续费 减少
    /// </summary>
    /// <param name="storeId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ReduceOrderTotalFee(int storeId, double money)
    {
        //return $this->counterStoreBusinessModel
        //    ->storeId($store_id)
        //    ->decrement('order_total_fee', $money);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 订单汇总 增加 订单损失费用
    /// </summary>
    /// <param name="storeId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> AddOrderLossFee(int storeId, double money)
    {
        //return CounterStoreBusinessModel::query()
        //    ->storeId($store_id)
        //    ->increment('order_total_loss', $money);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 订单汇总 减少 订单损失费用
    /// </summary>
    /// <param name="storeId"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ReduceOrderLossFee(int storeId, double money)
    {
        //return CounterStoreBusinessModel::query()
        //    ->storeId($store_id)
        //    ->decrement('order_total_loss', $money);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 店铺上报订单
    /// </summary>
    /// <param name="storeId"></param>
    /// <param name="orderTotal"></param>
    /// <param name="orderTotalFee"></param>
    /// <param name="orderTotalRefund"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> IncrementOrder(int storeId, int orderTotal = 0, double orderTotalFee = 0, double orderTotalRefund = 0)
    {
        //$updateData = [
        //    'order_total'        => DB::raw($this->counterStoreBusinessModel->transformKey('order_total'). '+'. $order_total),
        //    'order_total_fee'    => DB::raw($this->counterStoreBusinessModel->transformKey('order_total_fee'). '+'. $order_total_fee),
        //    'order_total_refund' => DB::raw($this->counterStoreBusinessModel->transformKey('order_total_refund'). '+'. $order_total_refund)
        //];
        //return $this->counterStoreBusinessModel::query()
        //    ->storeId($store_id)
        //    ->update($updateData);
        return await Task.FromResult(ReturnArr(true));
    }

    /// <summary>
    /// 店铺上报订单 减少
    /// </summary>
    /// <param name="storeId"></param>
    /// <param name="orderTotal"></param>
    /// <param name="orderTotalFee"></param>
    /// <param name="orderTotalRefund"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> DecrementOrder(int storeId, int orderTotal = 0, double orderTotalFee = 0, double orderTotalRefund = 0)
    {
        //$updateData = [
        //    'order_total'        => DB::raw($this->counterStoreBusinessModel->transformKey('order_total'). '-'. $order_total),
        //    'order_total_fee'    => DB::raw($this->counterStoreBusinessModel->transformKey('order_total_fee'). '-'. $order_total_fee),
        //    'order_total_refund' => DB::raw($this->counterStoreBusinessModel->transformKey('order_total_refund'). '-'. $order_total_refund)
        //];
        //return $this->counterStoreBusinessModel::query()
        //    ->storeId($store_id)
        //    ->update($updateData);
        return await Task.FromResult(ReturnArr(true));
    }
}

