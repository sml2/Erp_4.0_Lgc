using ERP.Enums.Statistics;
using ERP.Extensions;
using ERP.Models.DB.Users;
using Newtonsoft.Json;
using Exception = System.Exception;

namespace ERP.Services.DB.Statistics;

using Model = Models.DB.Statistics.Statistic;

public partial class Statistic
{
    /// <summary>
    /// 抠图API计费
    /// </summary>
    /// <param name="session"></param>
    /// <param name="num"></param>
    public async Task AiMatting(ISession? session, int num = 1)
    {
        await Increment(session, NumColumnEnum.AiMatting);
    }

    /// <summary>
    /// 翻译次数
    /// </summary>
    /// <param name="session"></param>
    /// <param name="num"></param>
    public async Task ApiTranslate(ISession? session, int num = 1)
    {
        await Increment(session, NumColumnEnum.ApiTranslate);
    }

    /// <summary>
    /// 产品新增语种数量
    /// </summary>
    /// <param name="session"></param>
    /// <param name="num"></param>
    public async Task ApiTranslateLanguage(ISession? session, int num = 1)
    {
        await Increment(session, NumColumnEnum.ApiTranslateLanguage);
    }

    /// <summary>
    /// 产品翻译次数
    /// </summary>
    /// <param name="session"></param>
    /// <param name="num"></param>
    public async Task ApiTranslateProduct(ISession? session, int num = 1)
    {
        await Increment(session, NumColumnEnum.ApiTranslateProduct);
    }
}