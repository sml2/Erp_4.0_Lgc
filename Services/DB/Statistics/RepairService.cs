using ERP.Data;
using ERP.Enums.Identity;
using ERP.Enums.Statistics;
using ERP.Exceptions.Statistic;
using ERP.Extensions;
using ERP.Models.Abstract;
using ERP.Models.DB.Users;
using ERP.Services.DB.Statistics;
using ERP.ViewModels.Statistics;
using Microsoft.EntityFrameworkCore;
using NPOI.SS.Formula.Functions;
using Statistic = ERP.Services.DB.Statistics.Statistic;
using StatisticModel = ERP.Models.DB.Statistics.Statistic;
using ProductTypes = ERP.Enums.Product.Types;

namespace ERP.Services.Statistics;

public class RepairService : BaseService
{
    private DBContext _dbContext;
    private Statistic _statisticsService;

    public RepairService(DBContext dbContext, Statistic statisticsService)
    {
        _dbContext = dbContext;
        _statisticsService = statisticsService;
    }

    public async Task<bool> Handle(RepairDto req)
    {
        bool state = false;

        switch (req.DataType)
        {
            case DataTypeEnum.Employee:
                state = await ResetEmployeeCount(req.Id, GetRole(req.DataSource));
                break;
            case DataTypeEnum.User:
                state = await ResetUserCount(req.Id, GetRole(req.DataSource));
                break;
            case DataTypeEnum.Product:
                state = await ResetProductCount(req.Id, GetRole(req.DataSource));
                break;
            case DataTypeEnum.Order:
                state = await ResetOrderCount(req.Id, GetRole(req.DataSource));
                break;
            case DataTypeEnum.Purchase:
                state = await ResetPurchaseCount(req.Id, GetRole(req.DataSource));
                break;
            case DataTypeEnum.Waybill:
                state = await ResetWaybillCount(req.Id, GetRole(req.DataSource));
                break;
            case DataTypeEnum.Store:
                state = await ResetStoreCount(req.Id, GetRole(req.DataSource));
                break;
            case DataTypeEnum.Company:
                state = await ResetCompanyCount(req.Id);
                break;
        }

        return state;
    }


    private Types GetRole(SourceEnum source)
    {
        Types role = Types.ZERO;
        switch (source)
        {
            case SourceEnum.Company:
                role = Types.Company;
                break;
            case SourceEnum.User:
                role = Types.User;
                break;
        }

        return role;
    }

    /// <summary>
    /// 员工
    /// </summary>
    /// <param name="aboutId"></param>
    /// <param name="role"></param>
    /// <returns></returns>
    private async Task<bool> ResetEmployeeCount(int aboutId, Types role)
    {
        var realCount = 0;
        switch (role)
        {
            case Types.Company:
                realCount = await _dbContext.User
                    .Where(m => m.CreateCompanyId == aboutId && m.Role == Role.Employee)
                    .CountAsync();
                break;
            case Types.User:
                realCount = await _dbContext.User
                    .Where(m => m.Pid == aboutId && m.Role == Role.Employee)
                    .CountAsync();
                break;
        }

        var todayStatisticInfo = await _statisticsService.GetStatisticInfo(aboutId, role, Types.DAY);
        var sumModel = await GetSum(aboutId, role) ?? new StatisticModel();
        var statisticCount = sumModel.EmployeeCount;

        if (realCount == statisticCount)
        {
            return true;
        }

        var count = realCount - statisticCount;
        todayStatisticInfo.EmployeeCount = GetCount(todayStatisticInfo.CompanyCount, count);
        await _dbContext.Statistic.SingleUpdateAsync(todayStatisticInfo);
        return await _dbContext.SaveChangesAsync() > 0;
    }

    /// <summary>
    /// 独立账户
    /// </summary>
    /// <param name="aboutId"></param>
    /// <param name="role"></param>
    /// <returns></returns>
    private async Task<bool> ResetUserCount(int aboutId, Types role)
    {
        var realCount = 0;
        switch (role)
        {
            case Types.Company:
                realCount = await _dbContext.User
                    .Where(m => m.CreateCompanyId == aboutId && m.Role == Role.ADMIN)
                    .CountAsync();
                break;
            case Types.User:
                realCount = await _dbContext.User
                    .Where(m => m.Pid == aboutId && m.Role == Role.ADMIN)
                    .CountAsync();
                break;
        }

        var todayStatisticInfo = await _statisticsService.GetStatisticInfo(aboutId, role, Types.DAY);
        var sumModel = await GetSum(aboutId, role) ?? new StatisticModel();
        var statisticCount = sumModel.UserCount;

        if (realCount == statisticCount)
        {
            return true;
        }

        var count = realCount - statisticCount;
        todayStatisticInfo.UserCount = GetCount(todayStatisticInfo.UserCount, count);
        await _dbContext.Statistic.SingleUpdateAsync(todayStatisticInfo);
        return await _dbContext.SaveChangesAsync() > 0;
    }

    /// <summary>
    /// 产品
    /// </summary>
    /// <param name="aboutId"></param>
    /// <param name="role"></param>
    /// <returns></returns>
    public async Task<bool> ResetProductCount(int aboutId, Types role)
    {
        //todo 排除回收站以及待上传
        var model = _dbContext.Product.AsEnumerable();
        switch (role)
        {
            case Types.Company:
                model = model.Where(m => m.CompanyID == aboutId);
                break;
            case Types.User:
                model = model.Where(m => m.UserID == aboutId);
                break;
        }

        model = model.Where(m => (m.Type & (long)ProductTypes.OnGoing) != (long)ProductTypes.OnGoing);
        model = model.Where(m => (m.Type & (long)ProductTypes.Export) != (long)ProductTypes.Export);
        model = model.Where(m => (m.Type & (long)ProductTypes.B2C) != (long)ProductTypes.B2C);
        var realCount = model.Count();

        var todayStatisticInfo = await _statisticsService.GetStatisticInfo(aboutId, role, Types.DAY);
        var sumModel = await GetSum(aboutId, role) ?? new StatisticModel();
        var statisticCount = sumModel.ProductCount;

        if (realCount == statisticCount)
        {
            return true;
        }

        var count = realCount - statisticCount;
        todayStatisticInfo.ProductCount = GetCount(todayStatisticInfo.ProductCount, count);
        await _dbContext.Statistic.SingleUpdateAsync(todayStatisticInfo);
        return await _dbContext.SaveChangesAsync() > 0;
    }

    /// <summary>
    /// 订单
    /// </summary>
    /// <param name="aboutId"></param>
    /// <param name="role"></param>
    /// <returns></returns>
    public async Task<bool> ResetOrderCount(int aboutId, Types role)
    {
        var realCount = 0;
        switch (role)
        {
            case Types.Company:
                realCount = await _dbContext.Order
                    .Where(m => m.CompanyID == aboutId)
                    .CountAsync();
                break;
            case Types.User:
                realCount = await _dbContext.Order
                    .Where(m => m.UserID == aboutId)
                    .CountAsync();
                break;
        }

        var todayStatisticInfo = await _statisticsService.GetStatisticInfo(aboutId, role, Types.DAY);
        var sumModel = await GetSum(aboutId, role) ?? new StatisticModel();
        var statisticCount = sumModel.OrderCount;
        if (realCount == statisticCount)
        {
            return true;
        }

        var count = realCount - statisticCount;
        todayStatisticInfo.OrderCount = GetCount(todayStatisticInfo.OrderCount, count);
        await _dbContext.Statistic.SingleUpdateAsync(todayStatisticInfo);
        return await _dbContext.SaveChangesAsync() > 0;
    }

    /// <summary>
    /// 采购
    /// </summary>
    /// <param name="aboutId"></param>
    /// <param name="role"></param>
    /// <returns></returns>
    public async Task<bool> ResetPurchaseCount(int aboutId, Types role)
    {
        var realCount = 0;
        switch (role)
        {
            case Types.Company:
                realCount = await _dbContext.Purchase
                    .Where(m => m.CompanyID == aboutId)
                    .CountAsync();
                break;
            case Types.User:
                realCount = await _dbContext.Purchase
                    .Where(m => m.UserID == aboutId)
                    .CountAsync();
                break;
        }

        var todayStatisticInfo = await _statisticsService.GetStatisticInfo(aboutId, role, Types.DAY);
        var sumModel = await GetSum(aboutId, role) ?? new StatisticModel();
        var statisticCount = sumModel.PurchaseCount;

        if (realCount == statisticCount)
        {
            return true;
        }

        var count = realCount - statisticCount;
        todayStatisticInfo.PurchaseCount = GetCount(todayStatisticInfo.PurchaseCount, count);
        await _dbContext.Statistic.SingleUpdateAsync(todayStatisticInfo);
        return await _dbContext.SaveChangesAsync() > 0;
    }


    /// <summary>
    /// 运单
    /// </summary>
    /// <param name="aboutId"></param>
    /// <param name="role"></param>
    /// <returns></returns>
    public async Task<bool> ResetWaybillCount(int aboutId, Types role)
    {
        var realCount = 0;
        switch (role)
        {
            case Types.Company:
                realCount = await _dbContext.Waybill
                    .Where(m => m.CompanyID == aboutId)
                    .CountAsync();

                break;
            case Types.User:
                realCount = await _dbContext.Waybill
                    .Where(m => m.UserID == aboutId)
                    .CountAsync();
                break;
        }

        var todayStatisticInfo = await _statisticsService.GetStatisticInfo(aboutId, role, Types.DAY);
        var sumModel = await GetSum(aboutId, role) ?? new StatisticModel();
        var statisticCount = sumModel.WaybillCount;

        if (realCount == statisticCount)
        {
            return true;
        }

        var count = realCount - statisticCount;
        todayStatisticInfo.WaybillCount = GetCount(todayStatisticInfo.WaybillCount, count);
        await _dbContext.Statistic.SingleUpdateAsync(todayStatisticInfo);
        return await _dbContext.SaveChangesAsync() > 0;
    }

    /// <summary>
    /// 店铺
    /// </summary>
    /// <param name="aboutId"></param>
    /// <param name="role"></param>
    /// <returns></returns>
    public async Task<bool> ResetStoreCount(int aboutId, Types role)
    {
        var realCount = 0;
        switch (role)
        {
            case Types.Company:
                realCount = await _dbContext.StoreRegion
                    .Where(m => m.CompanyID == aboutId && m.State == BaseModel.StateEnum.Open)
                    .CountAsync();

                break;
            case Types.User:
                realCount = await _dbContext.StoreRegion
                    .Where(m => m.UserID == aboutId && m.State == BaseModel.StateEnum.Open)
                    .CountAsync();
                break;
        }

        var todayStatisticInfo = await _statisticsService.GetStatisticInfo(aboutId, role, Types.DAY);
        var sumModel = await GetSum(aboutId, role) ?? new StatisticModel();
        var statisticCount = sumModel.StoreCount;

        if (realCount == statisticCount)
        {
            return true;
        }

        var count = realCount - statisticCount;
        todayStatisticInfo.StoreCount = GetCount(todayStatisticInfo.StoreCount, count);
        await _dbContext.Statistic.SingleUpdateAsync(todayStatisticInfo);
        return await _dbContext.SaveChangesAsync() > 0;
    }

    /// <summary>
    /// 公司
    /// </summary>
    /// <param name="aboutId"></param>
    /// <returns></returns>
    public async Task<bool> ResetCompanyCount(int aboutId)
    {
        var realCount = await _dbContext.Company
            .Where(m => m.ParentCompanyId == aboutId && m.State == Company.States.ENABLE)
            .CountAsync();

        var todayStatisticInfo = await _statisticsService.GetStatisticInfo(aboutId, Types.Company , Types.DAY);

        var sumModel = await GetSum(aboutId, Types.Company) ?? new StatisticModel();
        var statisticCount = sumModel.CompanyCount;


        if (realCount == statisticCount)
        {
            return true;
        }

        var count = realCount - statisticCount;
        todayStatisticInfo.CompanyCount = GetCount(todayStatisticInfo.CompanyCount, count);
        await _dbContext.Statistic.SingleUpdateAsync(todayStatisticInfo);
        return await _dbContext.SaveChangesAsync() > 0;
    }

    private int GetCount(int modelCount, int count)
    {
        if (modelCount < 0)
        {
            return count;
        }

        return modelCount + count;
    }

    private async Task<StatisticModel?> GetSum(int aboutId, Types role)
    {
        return await _dbContext.Statistic
            .Where(m =>
                m.AboutID == aboutId
                && (m.AboutType & Types.Role) == role
                && (m.AboutType & Types.Effective) == Types.DAY
            ).GroupBy(m => m.AboutID)
            .Select(g => new StatisticModel()
            {
                UserCount = g.Sum(m => m.UserCount),
                ProductCount = g.Sum(m => m.ProductCount),
                OrderCount = g.Sum(m => m.OrderCount),
                PurchaseCount = g.Sum(m => m.PurchaseCount),
                WaybillCount = g.Sum(m => m.WaybillCount),
                StoreCount = g.Sum(m => m.StoreCount),
            })
            .FirstOrDefaultAsync();
    }
}

public enum SourceEnum
{
    None,
    User,
    UserGroup,
    Company,
    Oem,
    Store
}

public enum DataTypeEnum
{
    None,
    User = 1000,
    Employee = 2000,
    Product = 3000,
    Store = 4000,
    Order = 5000,
    Purchase = 6000,
    Waybill = 7000,
    Company = 8000,
}