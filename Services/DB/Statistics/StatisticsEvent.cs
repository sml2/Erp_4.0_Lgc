using ERP.Data;
using ERP.Enums.Statistics;
using ERP.Services.DB.Statistics;
using ERP.ViewModels.Statistics;

namespace ERP.Services.Statistics;

using Model = Models.DB.Statistics.Statistic;


public class StatisticsEvent
{
    public enum NumColumnEnum
    {
        #region 公司管理

        CompanyInsert,
        CompanyDelete,
        CompanyCount,
        CompanyReject,

        #endregion

        #region 员工管理

        EmployeeInsert,
        EmployeeDelete,
        EmployeeCount,
        EmployeeReject,

        #endregion

        #region 开户管理

        UserInsert,
        UserDelete,
        UserCount,
        UserReject,

        #endregion

        #region 产品管理

        AiMatting,
        ApiTranslate,
        ApiTranslateLanguage,
        ApiTranslateProduct,
        DiskAdd,
        DiskDel,
        DiskSize,
        ProductInsert,
        ProductDelete,
        ProductCount,
        ProductRecovery,

        #endregion

        #region 店铺管理

        TaskInsert,
        TaskDelete,
        TaskCount,
        StoreInsert,
        StoreDelete,
        StoreCount,

        #endregion

        #region 订单管理

        OrderInsert,
        OrderDelete,
        OrderCount,

        #endregion

        #region 采购管理

        PurchaseInsert,
        PurchaseDelete,
        PurchaseCount,

        #endregion

        #region 运单管理

        WaybillInsert,
        WaybillDelete,
        WaybillCount,

        #endregion

        #region 下级上报

        ReportCompanyCount,
        ReportEmployeeCount,
        ReportUserCount,
        ReportProductCount,
        ReportStoreCount,
        ReportOrderCount,
        ReportPurchaseCount,
        ReportWaybillCount,

        #endregion

        #region 下级分销上报

        DistributionOrderNum,
        DistributionPurchaseNum,
        DistributionWaybillNum,

        #endregion
    }

    private Dictionary<NumColumnEnum, string> NumColumns = new()
    {
        #region 公司管理

        {NumColumnEnum.CompanyInsert, nameof(Model.CompanyInsert)},
        {NumColumnEnum.CompanyDelete, nameof(Model.CompanyDelete)},
        {NumColumnEnum.CompanyCount, nameof(Model.CompanyCount)},
        {NumColumnEnum.CompanyReject, nameof(Model.CompanyReject)},

        #endregion

        #region 员工管理

        {NumColumnEnum.EmployeeInsert, nameof(Model.EmployeeInsert)},
        {NumColumnEnum.EmployeeDelete, nameof(Model.EmployeeDelete)},
        {NumColumnEnum.EmployeeCount, nameof(Model.EmployeeCount)},
        {NumColumnEnum.EmployeeReject, nameof(Model.EmployeeReject)},

        #endregion

        #region 开户管理

        {NumColumnEnum.UserInsert, nameof(Model.UserInsert)},
        {NumColumnEnum.UserDelete, nameof(Model.UserDelete)},
        {NumColumnEnum.UserCount, nameof(Model.UserCount)},
        {NumColumnEnum.UserReject, nameof(Model.UserReject)},

        #endregion

        #region 产品管理

        {NumColumnEnum.AiMatting, nameof(Model.AiMatting)},
        {NumColumnEnum.ApiTranslate, nameof(Model.ApiTranslate)},
        {NumColumnEnum.ApiTranslateLanguage, nameof(Model.ApiTranslateLanguage)},
        {NumColumnEnum.ApiTranslateProduct, nameof(Model.ApiTranslateProduct)},
        {NumColumnEnum.DiskAdd, nameof(Model.DiskAdd)},
        {NumColumnEnum.DiskDel, nameof(Model.DiskDel)},
        {NumColumnEnum.DiskSize, nameof(Model.DiskSize)},
        {NumColumnEnum.ProductInsert, nameof(Model.ProductInsert)},
        {NumColumnEnum.ProductDelete, nameof(Model.ProductDelete)},
        {NumColumnEnum.ProductCount, nameof(Model.ProductCount)},
        {NumColumnEnum.ProductRecovery, nameof(Model.ProductRecovery)},

        #endregion

        #region 店铺管理

        {NumColumnEnum.TaskInsert, nameof(Model.TaskInsert)},
        {NumColumnEnum.TaskDelete, nameof(Model.TaskDelete)},
        {NumColumnEnum.TaskCount, nameof(Model.TaskCount)},
        {NumColumnEnum.StoreInsert, nameof(Model.StoreInsert)},
        {NumColumnEnum.StoreDelete, nameof(Model.StoreDelete)},
        {NumColumnEnum.StoreCount, nameof(Model.StoreCount)},

        #endregion

        #region 订单管理

        {NumColumnEnum.OrderInsert, nameof(Model.OrderInsert)},
        {NumColumnEnum.OrderDelete, nameof(Model.OrderDelete)},
        {NumColumnEnum.OrderCount, nameof(Model.OrderCount)},

        #endregion

        #region 采购管理

        {NumColumnEnum.PurchaseInsert, nameof(Model.PurchaseInsert)},
        {NumColumnEnum.PurchaseDelete, nameof(Model.PurchaseDelete)},
        {NumColumnEnum.PurchaseCount, nameof(Model.PurchaseCount)},

        #endregion

        #region 运单管理

        {NumColumnEnum.WaybillInsert, nameof(Model.WaybillInsert)},
        {NumColumnEnum.WaybillDelete, nameof(Model.WaybillDelete)},
        {NumColumnEnum.WaybillCount, nameof(Model.WaybillCount)},

        #endregion

        #region 下级上报

        {NumColumnEnum.ReportCompanyCount, nameof(Model.ReportCompanyCount)},
        {NumColumnEnum.ReportEmployeeCount, nameof(Model.ReportEmployeeCount)},
        {NumColumnEnum.ReportUserCount, nameof(Model.ReportUserCount)},
        {NumColumnEnum.ReportProductCount, nameof(Model.ReportProductCount)},
        {NumColumnEnum.ReportStoreCount, nameof(Model.ReportStoreCount)},
        {NumColumnEnum.ReportOrderCount, nameof(Model.ReportOrderCount)},
        {NumColumnEnum.ReportPurchaseCount, nameof(Model.ReportPurchaseCount)},
        {NumColumnEnum.ReportWaybillCount, nameof(Model.ReportWaybillCount)},

        #endregion

        #region 下级分销上报

        {NumColumnEnum.DistributionOrderNum, nameof(Model.DistributionOrderCount)},
        {NumColumnEnum.DistributionPurchaseNum, nameof(Model.DistributionPurchaseCount)},
        {NumColumnEnum.DistributionWaybillNum, nameof(Model.DistributionWaybillCount)},

        #endregion
    };

    private Dictionary<NumColumnEnum, NumColumnEnum> ReportRelation = new()
    {
        {NumColumnEnum.CompanyCount, NumColumnEnum.ReportCompanyCount},
        {NumColumnEnum.EmployeeCount, NumColumnEnum.ReportEmployeeCount},
        {NumColumnEnum.UserCount, NumColumnEnum.ReportUserCount},
        {NumColumnEnum.ProductCount, NumColumnEnum.ReportProductCount},
        {NumColumnEnum.StoreCount, NumColumnEnum.ReportStoreCount},
        {NumColumnEnum.OrderCount, NumColumnEnum.ReportOrderCount},
        {NumColumnEnum.PurchaseCount, NumColumnEnum.ReportPurchaseCount},
        {NumColumnEnum.WaybillDelete, NumColumnEnum.ReportWaybillCount},
    };

    private Dictionary<NumColumnEnum, string> NumColumnMessageTitles = new()
    {
        #region 公司管理

        {NumColumnEnum.CompanyInsert, "新增公司数"},
        {NumColumnEnum.CompanyDelete, "删除公司数"},
        {NumColumnEnum.CompanyCount, "当前公司数"},
        {NumColumnEnum.CompanyReject, "禁用公司数"},

        #endregion

        #region 员工管理

        {NumColumnEnum.EmployeeInsert, "新增员工数"},
        {NumColumnEnum.EmployeeDelete, "删除员工数"},
        {NumColumnEnum.EmployeeCount, "当前员工数"},
        {NumColumnEnum.EmployeeReject, "禁用员工数"},

        #endregion

        #region 开户管理

        {NumColumnEnum.UserInsert, "新增用户数"},
        {NumColumnEnum.UserDelete, "删除用户数"},
        {NumColumnEnum.UserCount, "当前用户数"},
        {NumColumnEnum.UserReject, "禁用用户数"},

        #endregion

        #region 产品管理

        {NumColumnEnum.AiMatting, "抠图次数"},
        {NumColumnEnum.ApiTranslate, "翻译次数"},
        {NumColumnEnum.ApiTranslateLanguage, "产品新增语种数量"},
        {NumColumnEnum.ApiTranslateProduct, "产品翻译次数"},
        {NumColumnEnum.DiskAdd, "新增硬盘开销"},
        {NumColumnEnum.DiskDel, "释放硬盘开销"},
        {NumColumnEnum.DiskSize, "当前硬盘开销"},
        {NumColumnEnum.ProductInsert, "新增产品数"},
        {NumColumnEnum.ProductDelete, "删除产品数"},
        {NumColumnEnum.ProductCount, "当前产品数"},
        {NumColumnEnum.ProductRecovery, "回收站产品数"},

        #endregion

        #region 店铺管理

        {NumColumnEnum.TaskInsert, "新增上传任务数"},
        {NumColumnEnum.TaskDelete, "删除上传任务数"},
        {NumColumnEnum.TaskCount, "当前上传任务数"},
        {NumColumnEnum.StoreInsert, "新增店铺数"},
        {NumColumnEnum.StoreDelete, "删除店铺数"},
        {NumColumnEnum.StoreCount, "当前店铺数"},

        #endregion

        #region 订单管理

        {NumColumnEnum.OrderInsert, "新增订单数"},
        {NumColumnEnum.OrderDelete, "删除订单数"},
        {NumColumnEnum.OrderCount, "订单数量"},

        #endregion

        #region 采购管理

        {NumColumnEnum.PurchaseInsert, "新增采购数"},
        {NumColumnEnum.PurchaseDelete, "删除采购数"},
        {NumColumnEnum.PurchaseCount, "采购数量"},

        #endregion

        #region 运单管理

        {NumColumnEnum.WaybillInsert, "新增运单数"},
        {NumColumnEnum.WaybillDelete, "运单数量"},
        {NumColumnEnum.WaybillCount, "运单数量"},

        #endregion

        #region 下级上报

        {NumColumnEnum.ReportCompanyCount, "下级上报开通公司数量"},
        {NumColumnEnum.ReportEmployeeCount, "下级上报开通员工数量"},
        {NumColumnEnum.ReportUserCount, "下级上报开通用户数量"},
        {NumColumnEnum.ReportStoreCount, "下级上报店铺数量"},
        {NumColumnEnum.ReportProductCount, "下级上报产品数量"},
        {NumColumnEnum.ReportOrderCount, "下级上报订单数量"},
        {NumColumnEnum.ReportPurchaseCount, "下级上报采购数量"},
        {NumColumnEnum.ReportWaybillCount, "下级上报运单数量"},

        #endregion

        #region 下级分销上报

        {NumColumnEnum.DistributionOrderNum, "下级上报分销订单数量"},
        {NumColumnEnum.DistributionPurchaseNum, "下级上报分销采购数量"},
        {NumColumnEnum.DistributionWaybillNum, "下级上报分销运单数量"},

        #endregion
    };

    protected List<Model> currentStatistics = new List<Model>();
    protected List<Model> superiorStatistics = new List<Model>();
    public readonly Statistic _statisticsService;
    private readonly DBContext _dbContext;

    public StatisticsEvent(Statistic statisticsService, DBContext dbContext)
    {
        _statisticsService = statisticsService;
        _dbContext = dbContext;
    }

    public StatisticsEvent(DataBelongDto belong, Statistic statisticsService, DBContext dbContext)
    {
        _statisticsService = statisticsService;
        _dbContext = dbContext;
        currentStatistics.Add(_statisticsService.GetStatisticInfo(belong.UserId,
            Types.User, Types.DAY).Result);

        if (belong.GroupId.HasValue)
        {
            currentStatistics.Add(_statisticsService.GetStatisticInfo(belong.GroupId,
                Types.UserGroup, Types.DAY).Result);
        }

        currentStatistics.Add(_statisticsService.GetStatisticInfo(belong.CompanyId,
            Types.Company, Types.DAY).Result);

        currentStatistics.Add(_statisticsService.GetStatisticInfo(belong.OemId,
            Types.OEM, Types.DAY).Result);

        if (belong.StoreId.HasValue)
        {
            currentStatistics.Add(_statisticsService.GetStatisticInfo(belong.StoreId,
                Types.Store, Types.DAY).Result);
        }

        if (belong.ReportIds != null && belong.ReportIds.Count > 0)
        {
            foreach (var id in belong.ReportIds)
            {
                superiorStatistics.Add(_statisticsService.GetStatisticInfo(id,
                    Types.Company, Types.DAY).Result);
            }
        }
    }

    /// <summary>
    /// 新增
    /// </summary>
    /// <param name="insertColumn"></param>
    /// <param name="countColumn"></param>
    /// <param name="num"></param>
    /// <param name="isDistribution"></param>
    public async Task InsertOrCount(NumColumnEnum insertColumn,
        NumColumnEnum countColumn, int num = 1, bool isDistribution = false)
    {
        var columns = new List<NumColumnEnum> {insertColumn, countColumn};

        await CurrentBusinessIncrement(columns, num);

        if (ReportRelation.Keys.Contains(countColumn))
        {
            await SuperiorBusinessIncrement(new List<NumColumnEnum> {ReportRelation[countColumn]}, num);
        }
    }

    public async Task DeleteWithCount(NumColumnEnum deleteColumn,
        NumColumnEnum countColumn, int num = 1)
    {
        await CurrentBusinessIncrement(deleteColumn, num);
    }


    private async Task<bool> CurrentBusinessIncrement(NumColumnEnum column, int num = 1)
    {
        foreach (var item in currentStatistics)
        {
            await Plus(item, column, num);
        }

        var state = await _dbContext.SaveChangesAsync() > 0;

        if (!state)
        {
            throw new Exception("当前公司增量上报发生异常");
        }

        return state;
    }

    private async Task<bool> CurrentBusinessIncrement(List<NumColumnEnum> columnEnum, int num = 1)
    {
        foreach (var column in columnEnum)
        {
            foreach (var item in currentStatistics)
            {
                await Plus(item, column, num);
            }
        }

        var state = await _dbContext.SaveChangesAsync() > 0;

        if (!state)
        {
            throw new Exception("当前公司增量上报发生异常");
        }

        return state;
    }

    private async Task<bool> SuperiorBusinessIncrement(NumColumnEnum column, int num = 1)
    {
        foreach (var item in superiorStatistics)
        {
            await Plus(item, column, num);
        }

        var state = await _dbContext.SaveChangesAsync() > 0;

        if (!state)
        {
            throw new Exception("当前公司增量上报发生异常");
        }

        return state;
    }

    private async Task<bool> SuperiorBusinessIncrement(List<NumColumnEnum> columnEnum, int num = 1)
    {
        foreach (var column in columnEnum)
        {
            foreach (var item in superiorStatistics)
            {
                await Plus(item, column, num);
            }
        }

        var state = await _dbContext.SaveChangesAsync() > 0;

        if (!state)
        {
            throw new Exception("当前公司增量上报发生异常");
        }

        return state;
    }

    /// <summary>
    /// 增量上报
    /// </summary>
    /// <param name="m"></param>
    /// <param name="column"></param>
    /// <param name="num"></param>
    /// <returns></returns>
    private async Task Plus(Model m, NumColumnEnum column, int num = 1)
    {
        var t = m.GetType();
        var field = t.GetProperty(NumColumns[column]);
        field!.SetValue(m, Convert.ToInt32(field.GetValue(m)) + num);
        _dbContext.Update(m);
    }
}