using ERP.Extensions;
using ERP.Interface;
using ERP.ViewModels.Store.Remote;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using static ERP.Models.Abstract.BaseModel;
using static ERP.Models.Logistics.ShipTemplate;
using Model = ERP.Models.Logistics.ShipTemplate;
using ShipTemplateViewModel = ERP.ViewModels.Logistics.EditShipTemplate;
using ERP.Data;
namespace ERP.Services.DB.Logistics;

public class ShipTemplate : BaseService
{

    ////原ERP3.0注释
    #region ERP3.0 PHP Service中方法签名
    /**
    * 获取发货模版数据
    * User: CCY
    * Date: 2020/8/21 17:10
    * @param $state
    * @param $name
    * @return mixed
    */
    //public ReturnStruct getList(/*$state = '', $name = ''*/)
    //{
    //    //    $field = ['id', 'name', 'state', 'created_at'];
    //    //    return ShipTemplateModel::query()
    //    //        ->name($name)
    //    //        ->state($state)
    //    //        ->userId($this->getSession()->getUserId())
    //    //        ->companyId($this->getSession()->getCompanyId())
    //    //        ->oemId($this->getSession()->getOemId())
    //    //        ->stateSort()
    //    //        ->sort()
    //    //        ->select($field)
    //    //        ->get();
    //    return ReturnArr();
    //}

    ///**
    // * 获取模版详情
    // * User: CCY
    // * Date: 2020/8/21 17:12
    // * @param $id
    // * @return null|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
    // */
    //public ReturnStruct getInfoById(/*$id*/)
    //{
    //    //    return ShipTemplateModel::query()->find($id);
    //    return ReturnArr();
    //}

    /////**
    //// * 获取当前用户发货模版数量
    //// * User: pc
    //// * Date: 2020/8/24
    //// * @return mixed
    //// */
    //public ReturnStruct getShipTemplateNum()
    //{
    //    //    return ShipTemplateModel::query()
    //    //        ->userId($this->getSession()->getUserId())
    //    //        ->companyId($this->getSession()->getCompanyId())
    //    //        ->oemId($this->getSession()->getOemId())
    //    //        ->count();
    //    return ReturnArr();
    //}

    /////**
    //// * 新增发货模版
    //// * User: pc
    //// * Date: 2020/8/24
    //// * @param $requestData
    //// * @return bool
    //// */
    //public ReturnStruct insertShipTemplate(/*$requestData*/)
    //{
    //    //    $insertData = [
    //    //        'sort'       => -$this->time,
    //    //        'user_id'    => $this->getSession()->getUserId(),
    //    //        'group_id'   => $this->getSession()->getGroupId(),
    //    //        'company_id' => $this->getSession()->getCompanyId(),
    //    //        'oem_id'     => $this->getSession()->getOemId(),
    //    //        'created_at' => $this->datetime
    //    //    ];
    //    //    return ShipTemplateModel::query()->insert($insertData + $requestData);
    //    return ReturnArr();
    //}

    /////**
    //// * 更新发货模版
    //// * User: pc
    //// * Date: 2020/8/24
    //// * @param $id
    //// * @param $requestData
    //// * @return int
    //// */
    //public ReturnStruct updateShipTemplate(/*$id, $requestData*/)
    //{
    //    //    return ShipTemplateModel::query()->where('id', $id)->update($requestData);
    //    return ReturnArr();
    //}


    /////**
    //// * 主键删除
    //// * User: CCY
    //// * Date: 2020/8/21 17:14
    //// * @param $id
    //// * @return int
    //// */
    //public ReturnStruct deleteInfoById(/*$id*/)
    //{
    //    //    return ShipTemplateModel::destroy($id);
    //    return ReturnArr();
    //}

    /////**
    //// * 获取有效发货模版
    //// * User: pc
    //// * Date: 2020/8/25
    //// * @param $id
    //// * @return mixed
    //// */
    //public ReturnStruct getInfo(/*$id*/)
    //{
    //    //    return ShipTemplateModel::query()
    //    //        ->state(ShipTemplateModel::STATE_ENABLE)
    //    //        ->userId($this->getSession()->getUserId())
    //    //        ->companyId($this->getSession()->getCompanyId())
    //    //        ->oemId($this->getSession()->getOemId())
    //    //        ->select(['id', 'name', 'declare_zh', 'declare_en', 'customs_code', 'description', 'tag', 'battery', 'length', 'width', 'height', 'weight'])
    //    //        ->find($id);
    //    return ReturnArr();
    //}

    /////**
    //// * 获取发货模版详细数据
    //// * User: CCY
    //// * Date: 2020/8/28
    //// * @return mixed
    //// */
    //public ReturnStruct getFullList()
    //{
    //    //    $field = ['id', 'name', 'declare_zh', 'declare_en', 'customs_code', 'description', 'tag', 'battery', 'length', 'width', 'height', 'weight'];
    //    //    return ShipTemplateModel::query()
    //    //        ->state(ShipTemplateModel::STATE_ENABLE)
    //    //        ->userId($this->getSession()->getUserId())
    //    //        ->companyId($this->getSession()->getCompanyId())
    //    //        ->oemId($this->getSession()->getOemId())
    //    //        ->stateSort()
    //    //        ->sort()
    //    //        ->select($field)
    //    //        ->get();
    //    return ReturnArr();
    //}

    #endregion

    #region PHP->C#

    private readonly DBContext _MyDbContext;
    private readonly ISessionProvider _ISessionProvider;

    public ShipTemplate(DBContext myDbContext, ISessionProvider ISessionProvider)
    {
        _MyDbContext = myDbContext;
        _ISessionProvider = ISessionProvider;
    }
    private int CompanyID { get => _ISessionProvider.Session!.GetCompanyID(); }
    private int UserID { get => _ISessionProvider.Session!.GetUserID(); }
    private int GroupID { get => _ISessionProvider.Session!.GetGroupID(); }
    private int OEMID { get => _ISessionProvider.Session!.GetOEMID(); }
    public async Task<bool> UpdateSort(SetSortDto setSortDto)
    {
        int Now = DateTime.Now.GetTimeStamp();
        int Sort = setSortDto.Type == 1 ? -Now : Now;
        var FindId = await _MyDbContext.ShipTemplate.SingleOrDefaultAsync(x => x.ID == setSortDto.Id);
        if (FindId != null)
        {
            FindId.Sort = Sort;
            _MyDbContext.SaveChanges();
            return true;
        }
        else
        {            
            throw new Exceptions.Logistics.ShipTemplateNotFound();
        }
    }


    /// <summary>
    /// 获取发货模版数据
    /// </summary>
    /// <param name="Name"></param>
    /// <param name="State"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> GetList(string? Name, StateEnum? State)
    {
        var data = await _MyDbContext.ShipTemplate
            .Where(x => x.UserID == UserID && x.CompanyID == CompanyID && x.OEMID == OEMID)
            .WhenWhere(!string.IsNullOrWhiteSpace(Name), x => x.Name.StartsWith(Name!))
            .WhenWhere(State > 0, x => x.State == State)
            .OrderBy(x => x.State)
            .ThenBy(x => x.Sort)
            .Select(x => new { x.ID, x.Name, x.State, x.CreatedAt })
            .ToPaginateAsync();
        if (data != null)
        {
            return ReturnArr(true, "", data);
        }
        return ReturnArr(false);

    }

    public async Task<ReturnStruct> GetListNPage(string? Name, StateEnum? State)
    {
        var data = await _MyDbContext.ShipTemplate
            .Where(x => x.UserID == UserID && x.CompanyID == CompanyID && x.OEMID == OEMID)
            .Where(x => x.State == StateEnum.Open)
            .OrderBy(x => x.Sort)
            .ToListAsync();
        if (data != null)
        {
            return ReturnArr(true, "", data);
        }
        return ReturnArr(false);
    }
    /// <summary>
    /// 获取发货模版详细数据
    /// </summary>
    /// <returns></returns>
    public async Task<object?> GetFullList()
    {
        var Data = await (from Template in _MyDbContext.ShipTemplate
                          where Template.UserID == UserID && Template.CompanyID == CompanyID
                                && Template.OEMID == OEMID && Template.State == StateEnum.Open
                          orderby Template.State
                          orderby Template.Sort descending
                          select new { Template }
                          ).ToListAsync();
        if (Data != null)
        {
            return Data;
        }
        return null;
    }
    /// <summary>
    /// 获取有效发货模版
    /// </summary>
    /// <param name="ID"></param>
    /// <returns></returns>
    public async Task<Model?> GetInfo(int? ID)
    {
        return await (from D in _MyDbContext.ShipTemplate
                      where D.UserID == UserID && D.CompanyID == CompanyID && D.OEMID == OEMID
                      && D.State == StateEnum.Open && D.ID == ID
                      select D).FirstOrDefaultAsync();
    }

    /// <summary>
    /// 获取模版详情
    /// </summary>
    /// <param name="Id"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> GetInfoById(int Id)
    {
        var Data = await _MyDbContext.ShipTemplate.SingleOrDefaultAsync(x => x.ID == Id);

        if (Data != null)
        {
            return ReturnArr(true, "", Data);
        }
        return ReturnArr(false, "");

    }
    /// <summary>
    /// 获取当前用户发货模版数量
    /// </summary>
    /// <returns></returns>
    public int GetShipTemplateNum()
    {
        int Count = _MyDbContext.ShipTemplate.Where(x => (x.CompanyID == CompanyID && x.UserID == UserID && x.OEMID == OEMID)).Count();
        if (Count <= 0)
        {
            return 0;
        }
        return Count;
    }

    /// <summary>
    /// 新增发货模版
    /// </summary>
    /// <returns></returns>
    public ReturnStruct InsertShipTemplate(ShipTemplateViewModel model)
    {

        Model Model = new()
        {
            Name = model.Name,
            Declare_zh = model.Declare_zh,
            Declare_en = model.Declare_en,
            CustomsCode = model.CustomsCode,
            Length = model.Length,
            Width = model.Width,
            Height = model.Height,
            Weight = model.Weight,
            State = model.State,
            Battery = model.Battery ?? false,
            Tag = model.Tag ?? "",
            Description = model.Description,
            Sort = -DateTime.Now.GetTimeStamp(),
            UserID = UserID,
            GroupID = GroupID,
            CompanyID = CompanyID,
            OEMID = OEMID
        };

        _MyDbContext.ShipTemplate.Add(Model);
        if (_MyDbContext.SaveChanges() > 0)
        {
            return ReturnArr(true, "成功");
        }
        return ReturnArr(false, "失败");
    }
    /// <summary>
    /// 主键删除
    /// </summary>
    /// <param name="Id"></param>
    /// <returns></returns>
    public bool DeleteInfoById(int Id)
    {
        var data = _MyDbContext.ShipTemplate.Find(Id);
        if (data != null)
        {
            _MyDbContext.Remove(data);
            if (_MyDbContext.SaveChanges() > 0)
            {
                return true;
            }
        }
        return false;
    }
    /// <summary>
    /// 更新发货模版
    /// </summary>
    /// <param name="Id"></param>
    /// <param name="data"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> UpdateShipTemplate(ShipTemplateViewModel Info)
    {

        var Model = await _MyDbContext.ShipTemplate.FindAsync(Info.Id);
        if (Model != null)
        {
            Model.Name = Info.Name;
            Model.Declare_zh = Info.Declare_zh;
            Model.Declare_en = Info.Declare_en;
            Model.CustomsCode = Info.CustomsCode;
            Model.Length = Info.Length;
            Model.Width = Info.Width;
            Model.Height = Info.Height;
            Model.Weight = Info.Weight;
            Model.State = Info.State;
            Model.Battery = Info.Battery ?? false;
            Model.Tag = Info.Tag ?? "";
            Model.Description = Info.Description;
            Model.IOSS = Info.IOSS;
            _MyDbContext.ShipTemplate.Update(Model);
            if (await _MyDbContext.SaveChangesAsync() > 0)
            {
                return ReturnArr(true, "更新成功");
            }
        }
        return ReturnArr(false, "");
    }

    #endregion

}
