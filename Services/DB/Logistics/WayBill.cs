using ERP.Extensions;
using ERP.Interface;
using ERP.Models.Finance;
using ERP.Models.DB.Logistics;
using ERP.Models.Api.Logistics;
using ERP.Models.View.Logistics.WayBill;
using ERP.Services.DB.Statistics;
using ERP.Services.Distribution;
using ERP.ViewModels.Logistics;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using BillLogModel = ERP.Models.Finance.BillLog;
using Model = ERP.Models.DB.Logistics.Waybill;
using OrderModel = ERP.Models.DB.Orders.Order;
using WayBillLogModel = ERP.Models.DB.Logistics.WaybillLog;
using WayBillViewModel = ERP.ViewModels.Logistics.WayBillVM;
using ERP.Enums;
using ERP.Models.DB.Orders;
using ERP.Enums.Orders;
using ERP.Services.Statistics;
using ERP.Models.Statistics;
using Newtonsoft.Json.Linq;
using ERP.Enums.Finance;
using ERP.Services.Finance;
using BillLogService = ERP.Services.Finance.BillLog;
using ERP.Enums.Logistics;
using ERP.Services.DB.Users;
using ERP.Models.DB.Users;
using ERP.Storage.Abstraction.Services;
using Z.EntityFramework.Plus;
using ERP.Data;
using ERP.Services.Stores;
using PurchaseService =ERP.Services.Purchase.Purchase;
using ERP.Models.View.Products.Product;
using ERP.ViewModels.Finance;
using Microsoft.AspNetCore.Identity;
using ERP.Distribution.Services;

namespace ERP.Services.DB.Logistics;
using CompanyService = Services.DB.Users.CompanyService;
using static Controllers.Logistics.Waybill;
using static Models.DB.Orders.Order;
using static Models.DB.Users.Company;
using OrderService = ERP.Services.DB.Orders.Order;
using static ERP.Models.Finance.BillLog;

public class WayBill : WaybillNoSessionService
{
    private readonly ISessionProvider _SessionProvider;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly IStockService _stockService;
    // private readonly DBContext _dbcontext;
    private readonly DB.Orders.Action _amazonOrderAction;
    private readonly Services.DB.Orders.Order _amazonOrder;
    private readonly CounterGroupBusinessService _CounterGroupBusinessService;
    private readonly CounterUserBusinessService _CounterUserBusinessService;
    private readonly CounterCompanyBusinessService _CounterCompanyBusiness;
    private readonly FinancialAffairsService _FinancialAffairsService;
    private readonly BillLogService _BillLogService;
    private readonly CompanyService _CompanyService;
    private readonly StoreService _storeService;
    private readonly Statistic _statisticService;
    private readonly StatisticMoney _statisticMoney;

    private readonly PurchaseService _PurchaseService;
    private readonly OrderService orderService;

    private readonly UserManager<Models.DB.Identity.User> _userManager;

    private readonly ILogger<WayBill> logger;
    public WayBill(DBContext dbcontext,
        ISessionProvider sessionProvider,
        Orders.Order amazonOrder,
        Orders.Action amazonOrderAction,
        Statistic statisticService,
        CounterGroupBusinessService counterGroupBusinessService,
        CounterUserBusinessService counterUserBusinessService,
        CounterCompanyBusinessService counterCompanyBusinessService,
        FinancialAffairsService financialAffairsService,
        BillLogService BillLogService,
        StoreService storeService,
        CompanyService CompanyService, IHttpContextAccessor httpContextAccessor,
        IStockService stockService, PurchaseService PurchaseService, OrderService _orderService, UserManager<Models.DB.Identity.User> userManager, ILogger<WayBill> _logger
    ) : base(dbcontext)
    {
        // _dbcontext = dbcontext;
        _SessionProvider = sessionProvider;
        _amazonOrder = amazonOrder;
        _amazonOrderAction = amazonOrderAction;
        _statisticService = statisticService;
        _BillLogService = BillLogService;
        _CompanyService = CompanyService;
        _storeService = storeService;
        _FinancialAffairsService = financialAffairsService;
        _CounterGroupBusinessService = counterGroupBusinessService;
        _CounterUserBusinessService = counterUserBusinessService;
        _CounterCompanyBusiness = counterCompanyBusinessService;
        _httpContextAccessor = httpContextAccessor;
        _stockService = stockService;
        _statisticMoney = new StatisticMoney(_statisticService);
        _PurchaseService = PurchaseService;
        orderService = _orderService;
        _userManager= userManager;
        logger = _logger;       
    }
    private ISession Session { get => _SessionProvider.Session!; }
    private int UserID { get => Session.GetUserID(); }
    private int GroupID { get => Session.GetGroupID(); }
    private int CompanyID { get => Session.GetCompanyID(); }
    private int OEMID { get => Session.GetOEMID(); }
    private string UserName { get => Session.GetUserName(); }
    private string TrueName { get => Session.GetTrueName(); }
    ////原ERP3.0注释
    #region ERP3.0 PHPService中方法签名

    public async Task<ReturnStruct> GetCurrency(int Id)
    {
        var data = await _dbcontext.Units.Where(x => x.ID == Id).ToListAsync();   // unit表
        return ReturnArr(true, "", data);
    }

    #region Model

    public async Task<ReturnStruct> logisticParam()
    {
        //return _Mydbcontext.LogisticsParam.Where(x=>_Mydbcontext.Waybill.Any(y=>y.LogisticId==x.Id)).ToList();
        var data = await (from a in _dbcontext.LogisticsParam
                          join b in _dbcontext.Waybill
                          on a.ID equals b.LogisticParamID
                          select new { a, b }).ToListAsync();
        if (data.Any())
        {
            return ReturnArr(true, "成功", data);
        }
        return ReturnArr(false, "失败");
    }


    public object company()
    {
        var data = (from a in _dbcontext.LogisticsParam
                    join b in _dbcontext.Waybill
                        on a.ID equals b.LogisticParamID
                    select new { a, b }).ToList();
        return data;
    }
    #endregion

    /**
     * 运单列表查询方法
     * User: Ly
     * Date: 2020/4/29
     */
    public async Task<DbSetExtension.PaginateStruct<Model>> GetList(List<int> Storeids, WayBills WayBills)
    {
        bool flag = false;
        List<int> ids = new();
        if (WayBills.WarehouseId > 0)
        {
            ids = await _stockService.GetLogWaybillIdByWarehouse(WayBills.WarehouseId);           
        }

        List<int?> orderIds = new();
        if (!string.IsNullOrWhiteSpace(WayBills.PurchaseOrderNo) || !string.IsNullOrWhiteSpace(WayBills.PurchaseTrackingNumber))
        {
            flag = true;
            orderIds = await _PurchaseService.GetPurchaseIDs(WayBills.PurchaseOrderNo, WayBills.PurchaseTrackingNumber);
        }

        var store = await _storeService.CurrentUserGetStores();
        var storeIds = store.Select(a => a.ID);
        var data = await _dbcontext.Waybill
            .Where(x => x.CompanyID == CompanyID && x.OEMID == OEMID)
            .Where(a => storeIds.Contains(a.StoreId) || a.StoreId == 0)
            .WhenWhere(ids.Count > 0, x => ids.Contains(x.ID))
            .WhenWhere(WayBills.WaybillState != null, x => x.State == WayBills.WaybillState)
            .WhenWhere(WayBills.StoreId > 0, x => x.StoreId == WayBills.StoreId)
            .WhenWhere(Storeids.Count > 0, x => Storeids.Contains(x.StoreId))
            .WhenWhere(WayBills.LogisticId > 0, x => x.LogisticParamID == WayBills.LogisticId)
            .WhenWhere(WayBills.NationId > 0, x => x.NationId == WayBills.NationId)
            .WhenWhere(!string.IsNullOrEmpty(WayBills.OrderNo), x => x.OrderNo == WayBills.OrderNo)
            .WhenWhere(!string.IsNullOrEmpty(WayBills.WaybillOrder), x => x.WaybillOrder == WayBills.WaybillOrder)
            .WhenWhere(!string.IsNullOrEmpty(WayBills.Express), x => x.Express == WayBills.Express)
            .WhenWhere(!string.IsNullOrEmpty(WayBills.Tracking), x => x.Tracking == WayBills.Tracking)
            .WhenWhere(flag, x => orderIds.Contains(x.OrderId))            
            .OrderByDescending(m => m.DeliverDate)
           // .ThenByDescending(x => x.CreatedAt)
            .ToPaginateAsync();

        return data;
    }
    
    public async Task<DbSetExtension.PaginateStruct<Model>> GetMergeList(List<int> WayBillIDs, WayBills WayBills)
    {

        var store = await _storeService.CurrentUserGetStores();
        var storeIds = store.Select(a => a.ID);

        var data = await _dbcontext.Waybill
            //.Where(x => x.CompanyID == CompanyID && x.OEMID == OEMID)
           // .Where(a => storeIds.Contains(a.StoreId) || a.StoreId == 0)
            .Where(x => WayBillIDs.Contains(x.ID))

            .WhenWhere(WayBills.WaybillState != null, x => x.State == WayBills.WaybillState)

            //.WhenWhere(WayBills.StoreId > 0, x => x.StoreId == WayBills.StoreId)
           
            .WhenWhere(WayBills.LogisticId > 0, x => x.LogisticParamID == WayBills.LogisticId)

            .WhenWhere(WayBills.NationId > 0, x => x.NationId == WayBills.NationId)

            .WhenWhere(!string.IsNullOrEmpty(WayBills.OrderNo), x => x.OrderNo == WayBills.OrderNo)
            .WhenWhere(!string.IsNullOrEmpty(WayBills.WaybillOrder), x => x.WaybillOrder == WayBills.WaybillOrder)
            .WhenWhere(!string.IsNullOrEmpty(WayBills.Express), x => x.Express == WayBills.Express)
            .WhenWhere(!string.IsNullOrEmpty(WayBills.Tracking), x => x.Tracking == WayBills.Tracking)
           
            .OrderByDescending(m => m.DeliverDate)
            
            .ToPaginateAsync();

        return data;
    }

    

    public async Task<DbSetExtension.PaginateStruct<Model>> GetDisList(int currentCompanyId, WayBills WayBills)
    {
        bool flag = false;
        List<int> ids = new();
        if (WayBills.WarehouseId > 0)
        {
            ids = await _stockService.GetLogWaybillIdByWarehouse(WayBills.WarehouseId);
        }

        List<int?> orderIds = new();
        if (!string.IsNullOrWhiteSpace(WayBills.PurchaseOrderNo) || !string.IsNullOrWhiteSpace(WayBills.PurchaseTrackingNumber))
        {
            flag = true;
            orderIds = await _PurchaseService.GetPurchaseIDs(WayBills.PurchaseOrderNo, WayBills.PurchaseTrackingNumber);
        }
        
        var data = await _dbcontext.Waybill
            .Where(w => w.WaybillCompanyID == currentCompanyId)           
            .WhenWhere(ids.Count > 0, x => ids.Contains(x.ID))
            .WhenWhere(WayBills.WaybillState != null, x => x.State == WayBills.WaybillState)
            .WhenWhere(WayBills.StoreId > 0, x => x.StoreId == WayBills.StoreId)           
            .WhenWhere(WayBills.LogisticId > 0, x => x.LogisticParamID == WayBills.LogisticId)
            .WhenWhere(WayBills.NationId > 0, x => x.NationId == WayBills.NationId)
            .WhenWhere(!string.IsNullOrEmpty(WayBills.OrderNo), x => x.OrderNo == WayBills.OrderNo)
            .WhenWhere(!string.IsNullOrEmpty(WayBills.WaybillOrder), x => x.WaybillOrder == WayBills.WaybillOrder)
            .WhenWhere(!string.IsNullOrEmpty(WayBills.Express), x => x.Express == WayBills.Express)
            .WhenWhere(!string.IsNullOrEmpty(WayBills.Tracking), x => x.Tracking == WayBills.Tracking)
            .WhenWhere(flag, x => orderIds.Contains(x.OrderId))
            .OrderByDescending(m => m.DeliverDate)
            .ToPaginateAsync();

        return data;
    }

    public List<int> TransStr(string s)
    {
        List<int> ints = new List<int>();
        s.Split(",").ForEach(x => ints.Add(Convert.ToInt32(x)));
        return ints;
    }


    public List<int> GetWayIds(string express="",string trackingNumber="")
    {
        List<int> wids = new List<int>();
        wids = _dbcontext.Waybill
            .WhenWhere(express.IsNotWhiteSpace(), m => m.Express == express)
            .WhenWhere(trackingNumber.IsNotWhiteSpace(), m => m.Tracking == trackingNumber)
            .Where(a => a.OrderId > 0).Where(x => x.MergeID <= 0)
            .Select(a => a.OrderId).ToList();

        var wMergeIDs = _dbcontext.Waybill
            .WhenWhere(express.IsNotWhiteSpace(), m => m.Express == express)
            .WhenWhere(trackingNumber.IsNotWhiteSpace(), m => m.Tracking == trackingNumber)
            .Where(x => x.MergeID > 0)
            .Select(a => a.OrderIDs).ToList();
       
        if (wMergeIDs != null && wMergeIDs.Count > 0)
        {
             wMergeIDs.ForEach(x => wids.AddRange(TransStr(x)));
        }
        return wids;
    }





    //    /**
    //     * 按id查询运单详情
    //     * @param $id
    //     * @return WayBillModel|WayBillModel[]|Builder|Builder[]|Collection|Model|null
    //     * User: Ly
    //     * Date: 2020/4/30
    //     */
    public async Task<ReturnStruct> CheckInfo(int Id/*$id*/)
    {
        var data = await _dbcontext.Waybill.FindAsync(Id);
        if (data == null)
        {
            return ReturnArr(false);
        }
        return ReturnArr(true, "成功", data);
    }

    // public async Task<bool> EditWayBill(WayBilEdit wayBilEdit)
    // {
    //     var info = await GetInfoById(wayBilEdit.ID);
    //     if (info == null)
    //     {
    //         return false;
    //     }
    //     try
    //     {
    //         Model model = new();
    //         if (info.IsVerify == false)
    //         {
    //             var ProductInfos = JsonConvert.DeserializeObject<JArray>(wayBilEdit.ProductInfo);
    //
    //             //未确认订单
    //             foreach (var item in ProductInfos)
    //             {
    //                 var StockOutNum = item["stockOutNum"] ?? 0;
    //                 var SendNum = item["SendNum"];
    //                 if ((int)SendNum > (int)StockOutNum)
    //                 {
    //                     //StockOutNum=StockOutNum>0?SendNum:
    //                 }
    //             }
    //
    //             model.Logistic = wayBilEdit.logistic;
    //             model.CarriageBase = wayBilEdit.Carriage_base;
    //             model.CarriageOther = wayBilEdit.Carriage_other;
    //             model.WaybillRate = wayBilEdit.waybill_rate;
    //             model.CarriageNum = wayBilEdit.Carriage_num;
    //             model.WaybillOrder = wayBilEdit.waybill_order;
    //             model.Express = wayBilEdit.express;
    //             model.Tracking = wayBilEdit.Tracking;
    //             model.Remark = wayBilEdit.remark;
    //             model.ProductInfo = wayBilEdit.ProductInfo;
    //         }
    //         else
    //         {
    //             model.Express = wayBilEdit.express;
    //             model.Tracking = wayBilEdit.Tracking;
    //             model.Remark = wayBilEdit.remark;
    //             model.ProductInfo = wayBilEdit.ProductInfo;
    //         }
    //
    //
    //         var transaction = _dbcontext.Database.BeginTransaction();
    //         try
    //         {
    //
    //             if (info.IsVerify == false)
    //             {
    //                 #region 修改数据-最终运费财务统计/跟踪
    //
    //                 var carriageNumMoneyRecord = new MoneyRecord(wayBilEdit.carriage_num, wayBilEdit.Carriage_num);
    //
    //                 var carriageNumFinanceId = await _statisticService.ReportMoney(Session,
    //                     Statistic.MoneyColumnEnum.OrderTotal, carriageNumMoneyRecord, TableFieldEnum.OrderTotal
    //                     , info.ID, info.CarriageNum.FinancialAffairsID);
    //
    //                 #endregion
    //                 
    //                 model.CarriageNum = new MoneyRecordFinancialAffairs(carriageNumMoneyRecord, carriageNumFinanceId);
    //
    //                 #region 修改数据-基础运费财务跟踪
    //
    //                 var carriageBaseMr = new MoneyRecord(wayBilEdit.carriage_base, wayBilEdit.Carriage_base);
    //                 var carriageBaseFid = await _statisticService.ReportFinancialRecord(carriageBaseMr,
    //                     TableFieldEnum.WaybillCarriageBase, info.ID, info.CarriageBase.FinancialAffairsID);
    //
    //                 #endregion
    //                 
    //                 model.CarriageBase = new MoneyRecordFinancialAffairs(carriageBaseMr, carriageBaseFid);
    //                 
    //                 #region 修改数据-其他费用财务跟踪
    //
    //                 var carriageOtherMr = new MoneyRecord(wayBilEdit.carriage_other, wayBilEdit.Carriage_other);
    //                 var carriageOtherFid = await _statisticService.ReportFinancialRecord(carriageOtherMr,
    //                     TableFieldEnum.WaybillCarriageOther, info.ID, info.CarriageOther.FinancialAffairsID);
    //
    //                 #endregion
    //                 
    //                 model.CarriageOther = new MoneyRecordFinancialAffairs(carriageOtherMr, carriageOtherFid);
    //                 
    //                 #region 修改数据-运费手续费财务跟踪
    //
    //                 var waybillRateMr = new MoneyRecord(wayBilEdit.waybill_rate, wayBilEdit.Waybill_rate);
    //                 var  waybillRateFid = await _statisticService.ReportFinancialRecord(waybillRateMr,
    //                     TableFieldEnum.WaybillRate, info.ID, info.WaybillRate.FinancialAffairsID);
    //
    //                 #endregion
    //                 
    //                 model.WaybillRate = new MoneyRecordFinancialAffairs(waybillRateMr, waybillRateFid);
    //
    //             }
    //
    //             _dbcontext.Update(model);
    //             _dbcontext.SaveChanges();
    //
    //             string ACtion = "修改运单信息";
    //             int s = string.Compare(info.Tracking, wayBilEdit.Tracking);
    //             ACtion = s < 0 ? ACtion : ACtion + ",面单追踪号" + $"{info.Tracking}更新" + $"{wayBilEdit.Tracking}";
    //             AddLog(info.OrderId, info.ID, info.Logistic.Platform, ACtion);
    //             transaction.Commit();
    //         }
    //         catch (Exception e)
    //         {
    //             transaction.Rollback();
    //             Console.WriteLine(e.Message);
    //             throw new Exception(e.Message);
    //         }
    //         return true;
    //     }
    //     catch (Exception)
    //     {
    //         return false;
    //         throw;
    //     }
    // }


    public async Task EditWaybill(WayBilEditVM request)
    {
        
    }


    // /// <summary>
    // /// 增加运单日志
    // /// </summary>
    // /// <param name="OrderId"></param>
    // /// <param name="WaybillId"></param>
    // /// <param name="platformInfo"></param>
    // /// <param name="Action"></param>
    // /// <returns></returns>
    // public async Task AddLog(int OrderId, int WaybillId, PlatformInfo platformInfo, string Action)
    // {
    //     Model IDandOrderID = new()
    //     {
    //         ID = WaybillId,
    //         OrderId = OrderId,
    //     };
    //     await AddLogNew(IDandOrderID, Action, platformInfo, 0, 0, GroupID);
    // }

    //    /**
    //     * @param WayBillModel|array $waybill
    //     * @param string $action
    //     * @param int $platform_id
    //     * @param int $oem_id
    //     * @param int $company_id
    //     * @param int $group_id
    //     * @return WayBillLogModel|Builder|Model
    //     * @author ryu <mo5467@126.com>
    //     */
    private async Task AddLogNew(Model wayBill, string Action, PlatformInfo platformInfo, int OemId = 0, int CompanyId = 0, int GroupId = 0)
    {

        if (OemId != 0 && OemId.IsNotNull() && CompanyId != 0 && CompanyId.IsNotNull())
        {
            /*&&OemId==$session->getOemId()*/
            /*&&CompanyId==$session->getCompanyId()*/
        }

        WayBillLogModel waybillLog = new()
        {
            WaybillId = wayBill.ID,
            Action = Action,
            OrderId = wayBill.OrderId,
            UserName = UserName,
            Truename = TrueName,
            UserID = UserID,
            OEMID = OemId > 0 ? OemId : OEMID,
            CompanyID = CompanyId > 0 ? CompanyId : CompanyID,
            GroupID = GroupID,
            Platform = platformInfo,
        };
        await _dbcontext.WaybillLogs.AddAsync(waybillLog);
        await _dbcontext.SaveChangesAsync();
    }
    /// <summary>
    /// 物流详情公共数据
    /// </summary>
    /// <param name="ID"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> WaybillInfo(int ID)
    {
        var query = from Waybill in _dbcontext.Waybill
                    join Logistics in _dbcontext.LogisticsParam on Waybill.LogisticParamID equals Logistics.ID
                    where Waybill.ID == ID
                    select new { Waybill, Logistics };
        return ReturnArr(true, "", await query.ToListAsync());
    }


    /// <summary>
    /// 物流详情公共数据(Include(w => w.LogisticParam))
    /// </summary>
    /// <param name="ID"></param>
    /// <returns></returns>
    public async Task<Model> GetInfoById(int ID)
    {
        var waybill = await _dbcontext.Waybill.Include(w => w.LogisticParam).SingleOrDefaultAsync(a => a.ID == ID);
        if (waybill is null) throw new Exception($"未查询到[ID:{ID}]运单信息");
        return waybill;
    }

    // public async Task<Model> GetWaybillInfoById(int ID)
    // {
    //     var waybill = await _dbcontext.Waybill.Include(w => w.LogisticParam).Include(x=>x.CarriageBase).Include(x => x.CarriageNum).Include(x => x.CarriageOther).SingleOrDefaultAsync(a => a.ID == ID);
    //     if (waybill is null) throw new Exception($"未查询到[ID:{ID}]运单信息");
    //     return waybill;
    // }

    //    /**
    //     * 添加运单操作方法（首页）暂废
    //     * @param $data
    //     * @return bool
    //     * User: Ly
    //     * Date: 2020/5/26
    //     */
    //public async Task<ReturnStruct> AddWayBillAction(Model wayBill)
    //{
    //    Model NewWaybill = new()
    //    {
    //        Logistic = wayBill.Logistic,
    //        ElectronicBusiness = wayBill.ElectronicBusiness,
    //        //$waybill->carriage_num = FromConversion($data['carriage_num'], $data['carriage_unit']);
    //        //CarriageNum=wayBill.CarriageNum.Money.To(),
    //        Express = wayBill.Express,
    //        WaybillOrder = wayBill.WaybillOrder,
    //        Tracking = wayBill.Tracking,
    //        Remark = wayBill.Remark,
    //        OrderId = wayBill.OrderId,
    //        OrderNo = wayBill.OrderNo,
    //        UserID = UserID,
    //        Truename = TrueName,
    //        CompanyID = CompanyID,
    //        OEMID = OEMID,
    //        GroupID = GroupID,
    //        StoreId = _dbcontext.Order.Where(x => x.ID == wayBill.ID).FirstOrDefault()?.StoreId ?? -1,
    //        TrackInfo = "暂无信息",
    //        // ProductInfo = _Mydbcontext.Purchases.Where(x => x.OrderId == wayBill.ID),
    //        ProductInfo = JsonConvert.SerializeObject(_dbcontext.Purchase.Where(x => x.OrderId == wayBill.OrderId).ToList()),
    //    };

    //    var transaction = _dbcontext.Database.BeginTransaction();
    //    try
    //    {
    //        _dbcontext.Waybill.Add(NewWaybill);

    //        #region 统计-新增运单数量
    //        await _statisticService.InsertOrCount(Session, Statistic.NumColumnEnum.WaybillInsert, Statistic.NumColumnEnum.WaybillCount);
    //        #endregion

    //        transaction.Commit();
    //        return ReturnArr(true);
    //    }
    //    catch (Exception e)
    //    {
    //        transaction.Rollback();
    //        return ReturnArr(false, msg: e.Message);
    //    }

    //    // _Mydbcontext.Waybill.Add(NewWaybill);
    //    // if (_Mydbcontext.SaveChanges() > 0)
    //    // {
    //    //     return ReturnArr(true);
    //    // }
    //    // return ReturnArr(false);

    //    //        $product_info = json_encode(PurchaseModel::query()->orderId($data['id'])->get()->toArray(), true);
    //    //        $waybill->product_info = $product_info;
    //    //        return $waybill->save();

    //}


    public bool GetWaybillByLogisticParamID(int logisticParamID)
    {
        return _dbcontext.Waybill.Where(x => x.LogisticParamID == logisticParamID).Any();
    }



    /// <summary>
    /// 添加运单操作方法（内页，分批发货）
    /// </summary>
    /// <param name="Order"></param>
    /// <param name="Data"></param>
    /// <returns></returns>
    public async Task<Model> AddWayBillActions(OrderModel Order, OrderSimpleInfo orderSimple, int? mergeID = -1, string ordersStr = "")
    {
        var carriageNumMoneyMeta = new MoneyMeta(orderSimple.CarriageUnit, orderSimple.CarriageNum);
        string? Url = orderSimple.GoodInfos.FirstOrDefault(a => a.Url.IsNotWhiteSpace())?.Url;
        DateTime Now = DateTime.Now;
        Model waybill = new()
        {
            MergeID = mergeID,
            OrderIDs = ordersStr,
            NationId = Order.ReceiverInternal.NationId,
            Logistic = new PlatformInfo() { Name = orderSimple.LogisticName },
            CarriageNum = carriageNumMoneyMeta,
            Express = orderSimple.Express,
            WaybillOrder = orderSimple.WaybillOrder,
            Tracking = orderSimple.Tracking,
            CarriageUnit = orderSimple.CarriageUnit,
            Remark = orderSimple.Remark,
            OrderId = orderSimple.id,
            OrderNo = orderSimple.OrderNo,
            DeliverDate = Now,
            UserID = UserID,
            Truename = TrueName,
            CompanyID = CompanyID,
            OEMID = OEMID,
            GroupID = GroupID,
            OperatecompanyID=CompanyID,
            WaybillCompanyID=Order.WaybillCompanyId,
            //LogisticParamID = Order.Plateform.Platform.GetID(),
            StoreId = Order.StoreId > 0 ? Order.StoreId : 0,
            TrackInfo = "暂无信息",
            ImgUrl = Url,
            ProductInfo = JsonConvert.SerializeObject(orderSimple.GoodInfos),
            PurchaseTrackingNumber = Order.PurchaseTrackingNumber,
            CreatedAt = Now,
        };
        var statisticService = new Statistic(_dbcontext, new FinancialAffairsService(_dbcontext, _SessionProvider));

        await _dbcontext.Waybill.AddAsync(waybill);

        await _dbcontext.SaveChangesAsync();

        #region 金额统计

        await _statisticMoney.InitStatisticWithWaybill(waybill);

        #endregion


        #region 统计-新增运单数量

        await _statisticService.InsertOrCount(Session, Statistic.NumColumnEnum.WaybillInsert,
            Statistic.NumColumnEnum.WaybillCount);

        #endregion
        return waybill;
    }

    /// <summary>
    /// add Waybill 调用此方法需要开启事务
    /// </summary>
    public async Task<Model> InsertWaybill(Company orderCompany,OrderModel orderInfo, Platforms logInfo, ReportResult report, OrderInfo orderParam,
        string Parameter, SenderInfo senderInfo,int waybillCompanyID=0,int? mergeID=-1,string ordersStr="",Money? freeze = null)
    {
        DateTime Now = DateTime.Now;
        Model model = new Model();
        model.MergeID = mergeID;
        model.OrderIDs = ordersStr;
        model.OrderId = orderInfo.ID;
        model.OrderNo = orderInfo.OrderNo;
        model.Logistic = logInfo;
        model.ImgUrl = orderInfo.Url;
        model.NationId = orderInfo.Receiver.NationId;
        model.StoreId = orderInfo.StoreId;
        model.WaybillOrder = report.CustomNumber;  
        model.Express = report.OrderNum;  
        model.Tracking = report.TrackNum;
        model.State = report.State;
        model.ButtonName = report.TrackButtonName;       
        model.Remark = JsonConvert.SerializeObject("");
        model.ElectronicBusiness = orderInfo.Plateform;
        model.ProductInfo = JsonConvert.SerializeObject(orderParam.GoodInfos);        
        model.LogisticParamID = orderParam.PlatformId;
        model.LogisticParamsData = orderParam.ext;
        model.LogisticExt = Parameter;
        model.ShipMethod = orderParam.ShipMethod;
        model.TrackInfo = "暂无信息";
        model.Sender = JsonConvert.SerializeObject(senderInfo);
        model.CarriageBase = 0;
        model.CarriageOther = 0;
        model.CarriageNum = 0;       
        model.OperatecompanyID = CompanyID;  //分销
        model.WaybillCompanyID = waybillCompanyID;  //分销
        model.IsVerify = VerifyStates.UnConfirm;
        model.WaybillRate = 0;  //分销：上级卖给下级，转差价，差价方式可以按固定值或百分比的方式
        model.WaybillRateType = RateTypes.Percentage;  //分销方式
        model.Problem = JsonConvert.SerializeObject("");  //物流失败原因
        model.PurchaseTrackingNumber = "";  //商品溯源：国内面单转国际面单，新面单绑定到原来的采购记录上用的
        model.DeliverDate = Now;
        model.CreatedAt = Now;
        model.UpdatedAt = Now;
        model.UserID = UserID;
        model.WaybillFreeze = freeze;
       

        //FIXME oemId companyId GroupId 可能不对，3.0代码中这三个字段的值是获取订单中的信息
        // model.OEMID = OEMID;
        // model.CompanyID = CompanyID;
        // model.GroupID = GroupID;
        model.GroupID = orderCompany.ID != CompanyID ? null : GroupID ;
        model.CompanyID = orderCompany.ID;
        model.OEMID = orderCompany.OEMID;
        //model.BillLogId = 0;

        await _dbcontext.Waybill.AddAsync(model);
        //await _dbcontext.SaveChangesAsync();

        #region 金额统计
        await _statisticMoney.InitStatisticWithWaybill(model);
        #endregion

        #region 统计-新增运单数量
        await _statisticService.InsertOrCount(Session, Statistic.NumColumnEnum.WaybillInsert, Statistic.NumColumnEnum.WaybillCount);
        #endregion

        return model;
    }


    //public async Task<int> InsertWaybill(OrderModel orderInfo, Platforms logInfo, ReportParameter report, ReportResult reportResult)
    //{
    //    Model model = new Model();
    //    model.OrderId = orderInfo.ID;
    //    model.OrderNo = orderInfo.OrderNo;
    //    model.Logistic = logInfo;
    //    model.ImgUrl = orderInfo.Url;
    //    model.NationId = orderInfo.Receiver.NationId;
    //    model.WaybillOrder = reportResult.OrderNum;
    //    model.Express = "";  //?
    //    model.Tracking = reportResult.TrackNum;
    //    model.DeliverDate = DateTime.Now;
    //    model.Remark = "";
    //    model.ElectronicBusiness = orderInfo.Plateform;
    //    model.ProductInfo = JsonConvert.SerializeObject(report.OrderInfo.GoodInfos);
    //    model.TrackInfo = "暂无信息";
    //    model.LogisticParamsData = report.OrderInfo.ext;
    //    model.LogisticExt = report.Parameter;
    //    model.State = reportResult.State;
    //    model.ButtonName = reportResult.TrackButtonName;
    //    model.Sender = JsonConvert.SerializeObject(report.SenderInfo);
    //    model.CarriageBase = 0;
    //    model.CarriageOther = 0;
    //    model.CarriageNum = 0;
    //    model.StoreId = orderInfo.StoreId;
    //    model.ShipMethod = "";
    //    model.OperatecompanyID = 0;  //
    //    model.WaybillCompanyID = 0;  //
    //    model.IsVerify = VerifyStates.UnConfirm;
    //    model.WaybillRate = 0;
    //    model.WaybillRateType = RateTypes.Percentage;
    //    model.Problem = "";
    //    model.PurchaseTrackingNumber = "";  //?
    //    model.CreatedAt = DateTime.Now;
    //    model.UpdatedAt = DateTime.Now;
    //    model.OEMID = OEMID;
    //    model.CompanyID = CompanyID;
    //    model.GroupID = GroupID;
    //    model.UserID = UserID;
    //    model.BillLogId = 0;


    //    var statisticService = new Statistic(_dbcontext, new FinancialAffairsService(_dbcontext, _SessionProvider));

    //    var transaction = await _dbcontext.Database.BeginTransactionAsync();

    //    try
    //    {



    //        await _dbcontext.Waybill.AddAsync(model);
    //        await _dbcontext.SaveChangesAsync();

    //        #region 金额统计

    //        await _statisticMoney.InitStatisticWithWaybill(model);

    //        #endregion

    //        #region 统计-新增运单数量

    //        await _statisticService.InsertOrCount(Session, Statistic.NumColumnEnum.WaybillInsert,
    //            Statistic.NumColumnEnum.WaybillCount);

    //        #endregion

    //        await transaction.CommitAsync();
    //        return model.ID;
    //    }
    //    catch (Exception e)
    //    {
    //        await transaction.RollbackAsync();
    //        Console.WriteLine(e.Message);
    //        throw new Exceptions.Exception(e.Message);
    //    }

    //}



    //    /**
    //     * 减order表中的待发货数量
    //     * @param $data
    //     * @return int
    //     * User: Ly
    //     * Date: 2020/5/26
    //     */
    public ReturnStruct ReduceWaitSend(int Id)
    {
        var FindOrder = _dbcontext.Order.Find(Id);

        //        //查询亚马逊订单信息
        //        $amazonObj = AmazonOrderModel::query()->find($id);
        //        $amazon = json_decode($amazonObj['goods'], true);
        //        foreach ($amazon as $k => $v) {
        //            $amazon[$k]['price'] = FromConversion($v['price'], $this->getSession()->getUnitConfig());
        //            $amazon[$k]['total'] = FromConversion($v['total'], $this->getSession()->getUnitConfig());
        //            //减json字段库存，同时增加已发货数量
        //            foreach ($data as $kk => $vv) {
        //                if ($amazon[$k]['good_id'] == $data[$kk]['good_id']) {
        //                    $amazon[$k]['send_num'] -= $data[$kk]['sendnum'];
        //                    $amazon[$k]['delivery'] += $data[$kk]['sendnum'];
        //                    $amazon[$k]['stock_out_num'] = isset($v['stock_out_num']) ? $v['stock_out_num'] + $vv['stock_out_num'] : $vv['stock_out_num'];
        //                }
        //            }
        //        }
        //        $amazonObj->goods = json_encode($amazon);
        //        //添加json之外的总发货数
        //        $amazonObj->delivery += $amount;
        //        return $amazonObj->save();
        return ReturnArr();
    }

    // /// <summary>
    // /// 更新waybill
    // /// </summary>
    // /// <param name="model"></param>
    // /// <returns></returns>
    // public async Task Update(Model model)
    // {
    //     await _dbcontext.SaveChangesAsync();
    // }

    /// <summary>
    /// 修改更新运单信息
    /// </summary>
    /// <param name="Id"></param>
    /// <param name="viewModel"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> updateData(int Id, WayBillViewModel viewModel)
    {
        var FindID = await _dbcontext.Waybill.FindAsync(Id);
        if (FindID != null)
        {
            if (viewModel.WaybillCompanyId >= 0)
            {
                FindID.WaybillCompanyID = viewModel.WaybillCompanyId;
            }
            if (!string.IsNullOrEmpty(viewModel.Problem))
            {
                FindID.Problem = viewModel.Problem;
            }
            //if (wayBillViewModel.WaybillCompanyId.IsNotNull())
            //{
            //    model.WaybillCompanyID = wayBillViewModel.WaybillCompanyId;
            //}
            //else
            //{
            //    model.ButtonName = wayBillViewModel.UserName;
            //    model.Problem = wayBillViewModel.Problem;
            //    wayBillViewModel.Time = DateTime.Now;
            //}
            if (_dbcontext.SaveChanges() > 0)
            {
                return ReturnArr(true);
            }
            return ReturnArr(false);
        }
        return ReturnArr(false);
    }

    public async Task<Model> FindInfo(int Id)
    {
        var data = await _dbcontext.Waybill.SingleOrDefaultAsync(x => x.ID == Id);
        if (data == null) throw new Exception($"查询不到[ID:{Id}]的运单");
        return data;
    }


    //public async Task<bool> Confirm(int Id)
    //{

    //    var Info = await GetInfoById(Id);
    //    var Wallet = GetUsedWallet();
    //    var Order = _amazonOrder.GetOrderById(Info.OrderId);

    //    var Goods = JsonConvert.DeserializeObject<JArray>(Info.ProductInfo);
    //    foreach (var item in Goods)
    //    {
    //        item["stockoutnum"] = item["stockoutnum"] ?? item["SendNum"];
    //    }
    //    var Company = getCompany(Info.CompanyID);
    //    int GorupID = Info.CompanyID == GroupID ? GroupID : 0;

    //    var Money = Info.CarriageNum;   //*

    //    //var BillLogID = -1;
    //    if (Money != 0)
    //    {
    //        //修改订单汇总
    //        _amazonOrder.UpdateWaybill(Info.ID, Money.Money);
    //        //财务扣款记录
    //        BillLogModel.Belongs Belong = CalcCostWallet(Info);
    //        var BelongID = _BillLogService.AddBusinessBill(Money.Money, BillLogModel.Types.Deduction, "运单手动确认",
    //            BillLogModel.Modules.WAYBILL, Info, Belong, GorupID
    //            //$"订单编号：{Order.OrderNo};物流运单号：{Info.WaybillOrder}"
    //            );
    //        //钱包扣款
    //        if (Belong == BillLogModel.Belongs.SELFUSE)
    //        {
    //            await _CompanyService.ExpenseWallet(Money.Money);
    //        }
    //        else
    //        {
    //            await _CompanyService.ExpensePublicWallet(Money.Money, Info.CompanyID);
    //        }
    //    }
    //    ConfirmUpdate(Id, Info.BIllLogId);
    //    AddLog(Info.OrderId, Info.ID, Info.Logistic, "确认运单信息");

    //    //todo 确认运单时无需上报金额吧？ 
    //    // FinancialAffairsModel financial = new()
    //    // {
    //    //     Value = Money,
    //    //     TableField = TableFieldEnum.OrderShippingMoney,
    //    //     TableID = Order.ID,
    //    // };
    //    // await _FinancialAffairsService.AddFianceData(financial);
    //    return true;
    //}
    public ReturnStruct saveData(int Id, string Problem/*$id,$data*/)
    {
        var data = _dbcontext.Waybill.FirstOrDefault(x => x.ID == Id);
        if (data != null)
        {
            data.Problem = Problem;
            _dbcontext.Update(data);
            if (_dbcontext.SaveChanges() > 0)
            {
                return ReturnArr(true, "修改成功", data);
            }
            return ReturnArr(false, "可能没有权限");
        }
        return ReturnArr(false, "请选中");
    }

    //    /**
    //     * 外汇换算人民币
    //     * @param int    $money money
    //     * @param string $unit  单位
    //     * @return float|int
    //     */

    //public int toCny(int Money=0,string Unit="")
    //{
    //    if(Money==0||string.IsNullOrEmpty(Unit))
    //    {
    //        return 0;
    //    }
    //    else
    //    {
    //        Unit = "CNY";
    //    }
    //    return (Money /);
    //    //        return ($money / $this->rate[$unit]['rate']);     Rate?

    //}


    //    /**
    //     * 计算钱包账单价格（人民币）
    //     * @return false|float
    //     */
    public ReturnStruct calculateWalletPrice(Money Price, Model WayBillInfo)
    {
        Money Prices;
        if (WayBillInfo.WaybillRateType == RateTypes.FixedNumber)
        {


            Prices = (Price / 100);
        }
        else
        {
            Prices = Price;
        }
        return ReturnArr(true, "", Math.Round(Prices, 2));
        //        if ($info["waybill_rate_type"] == WayBillModel::RATETYPE_BAR) {
        //            $price = ($price + $price * $info["waybill_rate"] / 100);
        //        } else {
        //            $price = ($price + $info["waybill_rate"]);
        //        }

        //        return round($price, 2);

    }

    //    /**
    //     * 查找运单信息
    //     * Date: 2020/6/11
    //     * User: Ly
    //     * @param $id
    //     * @return Builder|Builder[]|Collection|Model|null
    //     */
    public ReturnStruct findWayBill(int Id /*$id*/)
    {
        var data = _dbcontext.Waybill.Find(Id);
        if (data != null)
        {
            return ReturnArr(true, "", data);
        }
        //        return WayBillModel::query()->find($id);
        return ReturnArr(false, "请输入");
    }

    public Model? SearchWayBill(int Id)
    {
        return _dbcontext.Waybill.FirstOrDefault(x => x.ID == Id);
    }

    public List<Model>? SearchWayBillList(List<int> Ids)
    {
        return _dbcontext.Waybill.Where(x => Ids.Contains(x.ID)).ToList();
    }


    public PrintParam? findWayBillByID(int Id /*$id*/)
    {
        var data = _dbcontext.Waybill.FirstOrDefault(x => x.ID == Id);
        if (data is not null)
        {
            return new PrintParam() { CustomNumber = data?.OrderNo ?? "", LogictisNumber = data?.WaybillOrder, TrackNumber = data?.Tracking ,Express=data?.Express};

        }
        else
        {
            return null;
        }
    }


    public ReturnStruct checkOrderInfo(int OrderId)
    {
        var data = _dbcontext.Waybill
             .Where(x => x.OrderId == OrderId)
             .Select(x => new { ID = x.ID, Name = x.Logistic, Number = x.WaybillOrder })
             .ToList();
        if (data != null)
        {
            return ReturnArr(true, "", data);
        }
        //        return WayBillModel::query()->OrderId($orderid)->select(['id as ID', 'logistic AS Name', 'waybill_order AS Number'])->get()->toArray();
        return ReturnArr(false, "请选中");
    }

    /// <summary>
    /// 根据运单号查找
    /// </summary>
    /// <param name="Express"></param>
    /// <returns></returns>
    public async Task<Model> FindWayBillByWaybillOrder(string? Express)
    {
        var waybill = await _dbcontext.Waybill.FirstOrDefaultAsync(x => x.WaybillOrder == Express);
        if (waybill is null) throw new Exception($"未查询到[WaybillOrder:{Express}]的物流数据");
        return waybill;
    }

    public ReturnStruct findIfPurchase(int OrderId /*$id*/)
    {
        var data = _dbcontext.Purchase.Where(x => x.OrderId == OrderId).Select(x => new { x.ID, x.Express }).ToList();
        if (data != null)
        {
            return ReturnArr(true, "", data);
        }
        return ReturnArr(false, "请输入");
        //  return $purchase = PurchaseModel::query()->orderId($id)->select(['id', 'express'])->get();
    }

    public ReturnStruct fixExpress(int Id, string Express)
    {
        var FindID = _dbcontext.Waybill.FirstOrDefault(o => o.ID == Id);
        if (FindID != null)
        {
            FindID.Express = Express;
            _dbcontext.Update(FindID);
            if (_dbcontext.SaveChanges() > 0)
            {
                return ReturnArr(true, "修改成功");
            }
            return ReturnArr(false, "修改失败");
        }
        return ReturnArr(false, "请选中");
    }


    //    /**
    //     * 获取订单详情展示运单信息
    //     * User: CCY
    //     * Date: 2020/10/21
    //     * @param $order_id
    //     * @return mixed
    //     */
    public ReturnStruct getWaybillByOrderId(int OrderId)
    {
        var data = _dbcontext.Waybill
          .Where(x => x.OrderId == OrderId)
          .OrderByDescending(x => x.ID)       //sort()
          .ToList();
        return ReturnArr(true, "", data);
    }



    //    /**********************************采购新增、编辑关联运单********************************/
    //    /**
    //     * 更新运单 联动采购追踪号
    //     * User: CCY
    //     * Date: 2020/10/22
    //     * @param $id
    //     * @param $purchase_tracking_number
    //     * @return mixed
    //     */
    public ReturnStruct updatePurchaseTrackingNumber(int OrderId, string PurchaseTrackingNumber)
    {
        var FindOrderId = _dbcontext.Waybill.FirstOrDefault(o => o.OrderId == OrderId);
        if (FindOrderId != null)
        {
            FindOrderId.PurchaseTrackingNumber = PurchaseTrackingNumber;
            _dbcontext.Update(FindOrderId);
            if (_dbcontext.SaveChanges() > 0)
            {
                return ReturnArr(true, "修改成功");
            }
            return ReturnArr(false, "修改失败");
        }
        return ReturnArr(false, "请输入订单号");
    }


    //    /**
    //     * 获取运单信息
    //     * User: CCY
    //     * Date: 2021/2/6
    //     * @param $waybillIds
    //     * @param $field
    //     * @return mixed
    //     */
    //public ReturnStruct getWaybillByIds(int[] WaybillIds/*$waybillIds, $field = []*/)
    //{
    //    //->CompanyId($this->getSession()->getCompanyId())
    //    int CompanyId = CompanyID;

    //    var data = _Mydbcontext.Waybill
    //         .WhereIn(WaybillIds)
    //         .OrderByDescending(x => x.ID)
    //         .Select(x => new { })                      //$field = []
    //         .ToList();

    //    return ReturnArr();
    //}

    //    /**
    //     * 获取指定订单下运单信息
    //     * User: CCY
    //     * Date: 2021/2/22
    //     * @param $orderId
    //     * @return mixed
    //     */
    public ReturnStruct getWaybillByOrderIds(int OrderID)
    {
        var data = _dbcontext.Waybill
            .Where(x => x.OrderId == OrderID && x.CompanyID == CompanyID)
            .Select(x => new { x.ID, x.OrderId, Platform = x.ElectronicBusiness.Platform, x.WaybillOrder, x.Express, x.Tracking, x.OrderNo, x.CarriageNum })
            .OrderByDescending(x => x.ID)
            .ToList();
        if (data.Count <= 0)
        {
            return ReturnArr(false, "没有这个订单号");
        }

        return ReturnArr(true, "", data);

    }

    public async Task<List<Model>> getWaybillByOrderIdList(int OrderID)
    {
        return await _dbcontext.Waybill
            .Where(x => x.OrderId == OrderID && x.CompanyID == CompanyID)
            .OrderByDescending(x => x.ID)
            .ToListAsync();
    }


    public async Task<List<Model>> getWaybillByOrderIdList(List<int> OrderIDs)
    {
        //&& x.CompanyID == CompanyID
        return await _dbcontext.Waybill
            .Where(x => OrderIDs!.Contains(x.OrderId)  && x.State!=Enums.Logistics.States.Cancle)  
            .OrderByDescending(x => x.ID)
            .ToListAsync();
    }

    public async Task<List<Model>> getWaybillByOrderIdList(int MergeID, string orderid)
    {
        //&& x.CompanyID == CompanyID
        return await _dbcontext.Waybill
            .Where(x => x.MergeID == MergeID && x.State != Enums.Logistics.States.Cancle)
            .Where(x => x.OrderIDs.Contains(orderid))
            .OrderByDescending(x => x.ID)
            .ToListAsync();
    }


    public async Task<List<Model>> GetWaybill(List<int> waybillIDS)
    {
        return await _dbcontext.Waybill.Where(a => waybillIDS.Contains(a.ID) ).ToListAsync();
    }


    public  async Task<int> GerMergeWaybillCount(int mid)
    {
        return await _dbcontext.Waybill.CountAsync(a => a.MergeID == mid && a.State != Enums.Logistics.States.Cancle);
    }

    /// <summary>
    /// 返回所有运单，包含撤销的
    /// </summary>
    /// <param name="orderID"></param>
    /// <param name="mergeID"></param>
    /// <returns></returns>
    public async Task<List<int>> GetWaybillIDsByOrder(int orderID,int? mergeID,bool isCludeCancle=true)
    {
        var ways = _dbcontext.Waybill.Where(x => x.OrderId == orderID && (x.MergeID ==null || x.MergeID <= 0))
            .WhenWhere(isCludeCancle == false, x => x.State != Enums.Logistics.States.Cancle).Select(x=>x.ID).ToList();
        if (mergeID is not  null && mergeID > 0)  //&& 
        {
            var mergeWays = _dbcontext.Waybill.Where(x => x.MergeID == mergeID)
                .WhenWhere(isCludeCancle==false,x=>x.State!=Enums.Logistics.States.Cancle).ToList();
            if(mergeWays.Any())
            {
                foreach(var w in mergeWays)
                {
                    if(w.OrderIDs.IsNotWhiteSpace())
                    {
                        List<int> ds = new();
                        w.OrderIDs.Split(",").ForEach(x => ds.Add(Convert.ToInt32(x)));
                        if(ds.Contains(orderID))
                        {
                            ways.Add(w.ID);
                        }
                    }                    
                }
            }
        }
        return ways;
    }
    

    #endregion

    #region PHP->C#
    public bool Scopeavailable()   // warehousesmodel
    {
        // * 判断仓库对于个人是否可用
        // * @param $query
        // * @param sessiondata $sessiondata
        // * @return mixed
        // * @author ryu <mo5467@126.com>
        // */
        //public function scopeavailable($query, sessiondata $sessiondata)
        //{
        //    return $query->where('company_id', $sessiondata->getcompanyid())
        //        ->orwhere('distribution_company_id', $sessiondata->getcompanyid());
        //}


        var data = _dbcontext.Warehouses
             .Where(x => x.CompanyID == CompanyID || x.DistributionCompanyId == CompanyID)
            .SingleOrDefault();
        return data != null ? true : false;
    }


    /// <summary>
    /// 按id查询运单详情
    /// </summary>
    /// <param name="Id"></param>
    /// <returns></returns>
    public Model? checkinfo(int Id)
    {
        return _dbcontext.Waybill.Find(Id);
    }


    #endregion

    public async Task<ReturnStruct> GetLogisticByWaybill(int orderId)
    {
        var query = from w in _dbcontext.Waybill
                    join l in _dbcontext.LogisticsParam
                        on w.Logistic.Platform equals l.Platform.Platform
                    where w.OrderId == orderId && l.CompanyID == CompanyID
                    select new { l.CustomizeName, w.Logistic };
        return ReturnArr(true, "", await query.ToListAsync());
    }
    public async Task<Parameter?> GetLogisticByCharge(int id)
    {
        var query = from Waybills in _dbcontext.Waybill
                    join Logistics in _dbcontext.LogisticsParam
                    on id equals Logistics.ID
                    select Logistics;
        return await query.FirstOrDefaultAsync();
    }
    
    public async Task<string> CancleWaybill(Model waybill, ERP.Models.DB.Identity.User user)
    {
        using var transaction = _dbcontext.Database.BeginTransaction();
        //系统运单未取消
        if (waybill!.IsVerify != VerifyStates.Cancel)
        {                       
            try
            {
                List<OrderModel> orders = new ();
                if (waybill.MergeID > 0)
                {
                    var result = await orderService.GetOrdersByMergeID(waybill.MergeID);
                    if (!string.IsNullOrWhiteSpace(result.Item1))
                    {
                       return  $"{result.Item1}";
                    }
                    orders.AddRange(result.Item2!);
                }
                else 
                {
                    var orderInfo = await orderService.GetOrderByIdAsync(waybill.OrderId);
                    if (orderInfo is null)
                    {
                        return  $"订单信息没有找到";
                    }
                    orders.Add(orderInfo);
                }
                if(waybill.ProductInfo.IsNullOrWhiteSpace())
                {
                    return $"运单中没有商品信息";
                }
                foreach (var order in orders)
                {
                    var products = JsonConvert.DeserializeObject<List<ERP.Models.Api.Logistics.GoodInfo>>(waybill.ProductInfo!);

                    orderService.CountGoods(order!, products!, SendType.None, "sub");
                }

                if (waybill.IsVerify == Enums.Logistics.VerifyStates.Confirm)
                {
                    string s = $"订单编号：【{waybill.OrderNo}】；物流运单号：【{waybill.Express}】";
                    var Money = waybill.CarriageNum.Money;
                    var company = await _CompanyService.GetInfoById(waybill.CompanyID);
                    var wallet = waybill.GetUsedWallet(company, user); // 钱包
                    await wallet.Recharge(Money);                   
                    var billLog = wallet.AddLog(Money, true, Modules.WAYBILL, "运单取消，运费返还", s, waybill.StoreId, waybill.ID);
                    waybill.BillLog = billLog;
                }

                waybill.IsVerify = Enums.Logistics.VerifyStates.Cancel;
                _dbcontext.Waybill.Update(waybill);
                waybill.AddLog(user, "取消运单操作");

                //更新订单财务                    
                await orderService.UpdateMergeOrderWaybill(waybill.MergeID, null, waybill.OrderId, waybill.ID);
                await _dbcontext.SaveChangesAsync();
                await transaction.CommitAsync();
            }
            catch (Exception e)
            {
                await transaction.RollbackAsync();
                logger.LogError("CancleWaybill 取消运单出现异常" + e.Message);
                logger.LogError("CancleWaybill 取消运单出现异常" + e.StackTrace);
                return "取消运单失败";
            }                    
        }
        return "";
    }    
}

