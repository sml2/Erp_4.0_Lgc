using ERP.Data;
using ERP.Enums.Identity;
using ERP.Models.View.User;
using Microsoft.EntityFrameworkCore;
using Model = ERP.Models.DB.Logistics.Waybill;
using User = ERP.Models.DB.Identity.User;
using WayBillLogModel = ERP.Models.DB.Logistics.WaybillLog;

namespace ERP.Services.DB.Logistics;

public class WaybillNoSessionService : BaseService
{
    protected readonly DBContext _dbcontext;

    public WaybillNoSessionService(DBContext dbcontext)
    {
        _dbcontext = dbcontext;
    }
    
    
    public async Task<Model> GetWaybillInfoById(int id)
    {
        var waybill = await _dbcontext.Waybill.Include(w => w.LogisticParam).Include(x=>x.CarriageBase).Include(x => x.CarriageNum).Include(x => x.CarriageOther).SingleOrDefaultAsync(a => a.ID == id);
        if (waybill is null) throw new Exception($"未查询到[ID:{id}]运单信息");
        return waybill;
    }
    
    /// <summary>
    /// 更新waybill
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    public async Task Update(Model model)
    {
        await _dbcontext.SaveChangesAsync();
    }
    
    /// <summary>
    /// 增加运单日志
    /// </summary>
    /// <param name="OrderId"></param>
    /// <param name="WaybillId"></param>
    /// <param name="platformInfo"></param>
    /// <param name="Action"></param>
    /// <returns></returns>
    public async Task AddLog(int OrderId, int WaybillId, PlatformInfo platformInfo, string Action,UserInfoDto userInfo)
    {
        Model IDandOrderID = new()
        {
            ID = WaybillId,
            OrderId = OrderId,
        };
        await AddLogNew(IDandOrderID, Action, platformInfo, userInfo);
    }
    
    //    /**
    //     * @param WayBillModel|array $waybill
    //     * @param string $action
    //     * @param int $platform_id
    //     * @param int $oem_id
    //     * @param int $company_id
    //     * @param int $group_id
    //     * @return WayBillLogModel|Builder|Model
    //     * @author ryu <mo5467@126.com>
    //     */
    private async Task AddLogNew(Model wayBill, string Action, PlatformInfo platformInfo,UserInfoDto userInfo)
    {

        // if (OemId != 0 && OemId.IsNotNull() && CompanyId != 0 && CompanyId.IsNotNull())
        // {
            /*&&OemId==$session->getOemId()*/
            /*&&CompanyId==$session->getCompanyId()*/
        // }

        WayBillLogModel waybillLog = new()
        {
            WaybillId = wayBill.ID,
            Action = Action,
            OrderId = wayBill.OrderId,
            UserName = userInfo.Username,
            Truename = userInfo.Truename,
            UserID = userInfo.UserId!.Value,
            OEMID = userInfo.OemId!.Value,
            CompanyID = userInfo.CompanyId!.Value,
            GroupID = userInfo.GroupId!.Value,
            Platform = platformInfo,
        };
        await _dbcontext.WaybillLogs.AddAsync(waybillLog);
        await _dbcontext.SaveChangesAsync();
    }
    
    /// <summary>
    /// 根据orderID查找所有普通运单
    /// </summary>
    /// <param name="OrderId"></param>
    /// <returns></returns>
    public async Task<List<Model>> GetWaybillByOId(int OrderId)
    {
        return await _dbcontext.Waybill.Where(x => x.OrderId == OrderId && (x.MergeID <= 0 || x.MergeID ==null)).ToListAsync();
    }
    
    #region 合并运单操作

    public async Task<List<Model>?> GetMergeWaybillList(int? mergeID)
    {
        if(mergeID is not null)
        {
            return await _dbcontext.Waybill.Where(x=>x.MergeID== mergeID).ToListAsync();
        }
        return null;
    }   

    #endregion

    public async Task<List<Model>> GetWaybillNeedPull(int itemCount,int minute)
    {
        var havGetFeeLogistics = new List<Platforms>()
        { 
            Platforms.YunTu,
            Platforms.RTB_YDH,
            Platforms.RTB_JFCC,
            Platforms.RTB_SDGJ,
            Platforms.SFC,
        };

        return await _dbcontext.Waybill
            .Where(m => havGetFeeLogistics.Contains(m.Logistic.Platform))
            .Where(m => m.CarriageNum.Money <= 0)
            .Where(m => m.LastAutoShipTime == null || (m.LastAutoShipTime.HasValue &&
                                                       (m.LastAutoShipTime.Value - DateTime.Now).Minutes > minute))
            .OrderBy(m => m.LastAutoShipTime)
            // .OrderByDescending(m => m.ID)
            .Take(itemCount).ToListAsync();
    }

    public async Task<List<Model>> GetWaybillNeedPullNoDistribution(DateTime begin,DateTime end,User user)
    {
        var havGetFeeLogistics = new List<Platforms>()
        { 
            Platforms.YunTu,
            Platforms.RTB_YDH,
            Platforms.RTB_JFCC,
            Platforms.RTB_SDGJ,
            Platforms.SFC,
        };
        
        return await _dbcontext.Waybill
            .Where(m => m.OEMID == user.OEMID && m.CompanyID == user.CompanyID)
            .Where(m => havGetFeeLogistics.Contains(m.Logistic.Platform))
            // .Where(m => m.CarriageNum.Money <= 0)
            // .Where(m => m.CreatedAt >= begin && m.CreatedAt <= end)
            .Where(m => m.DeliverDate >= begin && m.DeliverDate <= end)
            .OrderBy(m => m.DeliverDate)
            .ToListAsync();
    }
    
    public async Task<List<Model>> GetWaybillNeedPullIsDistribution(DateTime begin,DateTime end,User user)
    {
        var havGetFeeLogistics = new List<Platforms>()
        { 
            Platforms.YunTu,
            Platforms.RTB_YDH,
            Platforms.RTB_JFCC,
            Platforms.RTB_SDGJ,
            Platforms.SFC,
        };
        
        return await _dbcontext.Waybill
            .Where(m =>  m.WaybillCompanyID == user.CompanyID)
            .Where(m => havGetFeeLogistics.Contains(m.Logistic.Platform))
            // .Where(m => m.CarriageNum.Money <= 0)
            // .Where(m => m.CreatedAt >= begin && m.CreatedAt <= end)
            .Where(m => m.DeliverDate >= begin && m.DeliverDate <= end)/**/
            .OrderBy(m => m.DeliverDate)
            .ToListAsync();
    }
}