using ERP.Extensions;
using ERP.Interface;
using Model = ERP.Models.Logistics.OperationLogging;
using Newtonsoft.Json;
using ERP.Models.DB.Logistics;
using ERP.Data;
using ERP.Models.Api.Logistics;
using ERP.Models.Logistics;

namespace ERP.Services.DB.Logistics
{
    public class OperationLogging : BaseService
    {
        private readonly ISessionProvider _SessionProvider;

        private readonly DBContext _dbcontext;

        private readonly ILogger<OperationLogging> _logger;
        public OperationLogging(ISessionProvider sessionProvider, ILogger<OperationLogging> logger, DBContext dbcontext)
        {
            _logger= logger;
            _dbcontext = dbcontext;
            _SessionProvider = sessionProvider;
        }
        private ISession Session { get => _SessionProvider.Session!; }
        private int UserID { get => Session.GetUserID(); }
        private int GroupID { get => Session.GetGroupID(); }
        private int CompanyID { get => Session.GetCompanyID(); }
        private int OEMID { get => Session.GetOEMID(); }
        private string UserName { get => Session.GetUserName(); }
        private string TrueName { get => Session.GetTrueName(); }
      


        /// <summary>
        /// 单个订单发货请求记录
        /// </summary>
        /// <param name="sendParameter"></param>
        /// <param name="waybillID"></param>
        /// <param name="report"></param>
        /// <returns></returns>
        public async Task Insert(SendParameter sendParameter,int waybillID, ReportResultList report)
        {          
            try
            {
                var data = new
                {
                    LogName = sendParameter.LogName,
                    Parameter = sendParameter.Parameter,
                    SenderInfo = sendParameter.SenderInfo,
                    OrderInfo = sendParameter.OrderInfos.FirstOrDefault(),
                    Type = sendParameter.Type,
                    Express = sendParameter.Express ?? "",
                    TrackNum = sendParameter.TrackNum ?? "",
                    WaybillOrder = sendParameter.WaybillOrder ?? "",
                };
                string request = JsonConvert.SerializeObject(data);
                string resultStr = JsonConvert.SerializeObject(report);
                if (data.OrderInfo is null)
                {
                    _logger.LogError($"OperationLogging Insert  OrderInfo is null");
                    return;
                }
                await InsertSendInfo(data.OrderInfo.id, waybillID, "", "", request, report.Response, resultStr);                
            }
            catch (Exception e)
            {               
                _logger.LogError($"OperationLogging Insert Exception:{e.Message}");
                _logger.LogError($"OperationLogging Insert Exception:{e.StackTrace}");
                throw e;
            }           
        }


        public async Task<ReturnStruct> InsertSendInfo(int orderId, int waybillId, string orderNo, string waybillNo, string request, string response, string result, int? mergeID = null)
        {                     
            var data = new JsonData(request, response, result);
            Model model = new Model();
            var lst=model.LogScope(data, FieldMode.send);
          
            if (waybillId == 0)   //第一次失败send
            {               
                  //add  orderid
                Model operation = new Model();
                operation.OrderId = orderId;
                operation.MergeID = mergeID;
                operation.OrderNoHash = Helpers.CreateHashCode(orderNo);
                operation.Send = model.GetJson(lst);
                operation.SendFirst = DateTime.Now;
                operation.SendNums = 1;
                operation.OEMID = OEMID;
                operation.CompanyID = CompanyID;
                operation.GroupID = GroupID;
                operation.UserID = UserID;
                _dbcontext.OperationLogging.Add(operation);
                _dbcontext.SaveChanges();
            }
            else
            {
                var query = _dbcontext.OperationLogging.FirstOrDefault(x => x.OrderId == orderId && x.WaybillId == waybillId);
                if (query.IsNotNull())   //第n次成功send
                {                    
                    Console.Write($"send exception：orderId={orderId},waybillId={waybillId}");
                }
                else 
                {   //第一次成功send                                    
                    Model operation = new Model();
                    operation.OrderId = orderId;
                    operation.MergeID = mergeID;
                    operation.OrderNoHash = Helpers.CreateHashCode(orderNo);
                    operation.WaybillId = waybillId;
                    operation.WaybillNoHash = Helpers.CreateHashCode(orderNo+waybillNo);
                    operation.Send = model.GetJson(lst);
                    operation.SendFirst = DateTime.Now;
                    operation.SendSuccess= DateTime.Now;
                    operation.SendNums = 1;
                    operation.OEMID = OEMID;
                    operation.CompanyID = CompanyID;
                    operation.GroupID = GroupID;
                    operation.UserID = UserID;
                    _dbcontext.OperationLogging.Add(operation);
                    _dbcontext.SaveChanges();
                }
            }            
            return await Task.FromResult(ReturnArr(true));
        }
       
        public async Task<ReturnStruct> UpdateTrackInfo(int waybillId,string request, string response, string result,bool isSuccess)
        {
            var data = new JsonData(request, response, result);             
            var query = _dbcontext.OperationLogging.FirstOrDefault(x => x.WaybillId == waybillId);
            if(query is not null)
            {
                var lst = query.LogScope(data, FieldMode.track);
                query.Track = query.GetJson(lst);
                query.TrackFirst= query.TrackFirst==null?DateTime.Now: query.TrackFirst;
                if(isSuccess)
                {
                    query.TrackSuccess = query.TrackSuccess == null ? DateTime.Now : query.TrackSuccess;
                }
                query.TrackNums+= 1;
                query.TrackLast = DateTime.Now;
                _dbcontext.OperationLogging.Update(query);
                _dbcontext.SaveChanges();
                return await Task.FromResult(ReturnArr(true));
            }
            return ReturnArr(false, "search no data", query);
        }

        public async Task<ReturnStruct> UpdatePriceInfo(int waybillId, string request, string response, string result, bool isSuccess)
        {
            var data = new JsonData(request, response, result);
            var query = _dbcontext.OperationLogging.FirstOrDefault(x => x.WaybillId == waybillId);
            if (query is not null)
            {
                var lst = query.LogScope(data, FieldMode.price);
                query.Price = query.GetJson(lst);
                query.PriceFirst = query.PriceFirst == null ? DateTime.Now : query.PriceFirst;
                if (isSuccess)
                {
                    query.PriceSuccess = query.PriceSuccess == null ? DateTime.Now : query.PriceSuccess;
                }
                query.PriceNums += 1;
                query.PriceLast = DateTime.Now;
                _dbcontext.OperationLogging.Update(query);
                _dbcontext.SaveChanges();
                return await Task.FromResult(ReturnArr(true));
            }
            return ReturnArr(false, "search no data", query);           
        }       

        public async Task<ReturnStruct> UpdatePrintInfo(int waybillId, string request, string response, string result, bool isSuccess)
        {
            var data = new JsonData(request, response, result);
            var query = _dbcontext.OperationLogging.FirstOrDefault(x => x.WaybillId == waybillId);
            if (query is not null)
            {
                var lst = query.LogScope(data, FieldMode.print);
                query.Print = query.GetJson(lst);
                query.PrintFirst = query.PrintFirst == null ? DateTime.Now : query.PrintFirst;
                if (isSuccess)
                {
                    query.PrintSuccess = query.PrintSuccess == null ? DateTime.Now : query.PrintSuccess;
                }
                query.PrintNums += 1;
                query.PrintLast = DateTime.Now;
                _dbcontext.OperationLogging.Update(query);
                _dbcontext.SaveChanges();
                return await Task.FromResult(ReturnArr(true));
            }           
            return ReturnArr(false, "search no data", query);
        }

        public async Task<ReturnStruct> UpdateCancleInfo(int waybillId, string request, string response, string result, bool isSuccess)
        {
            var data = new JsonData(request, response, result);
            var query = _dbcontext.OperationLogging.FirstOrDefault(x => x.WaybillId == waybillId);
            if (query is not null)
            {
                var lst = query.LogScope(data, FieldMode.cancle);
                query.Cancle = query.GetJson(lst);
                query.CancleFirst = query.CancleFirst == null ? DateTime.Now : query.CancleFirst;
                if (isSuccess)
                {
                    query.CancleSuccess = query.CancleSuccess == null ? DateTime.Now : query.CancleSuccess;
                }
                query.CancleNums += 1;
                query.CancleLast = DateTime.Now;
                _dbcontext.OperationLogging.Update(query);
                _dbcontext.SaveChanges();
                return await Task.FromResult(ReturnArr(true));
            }
            return ReturnArr(false, "search no data", query);
        }
    }
}
