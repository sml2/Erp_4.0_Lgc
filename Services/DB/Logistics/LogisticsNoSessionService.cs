using ERP.Data;
using ERP.Models.DB.Logistics;
using ERP.Models.View.User;
using Microsoft.EntityFrameworkCore;

namespace ERP.Services.DB.Logistics;

public class LogisticsNoSessionService
{
    protected readonly DBContext _dbcontext;
    protected readonly ILogger<LogisticsNoSessionService> _logger;

    public LogisticsNoSessionService(DBContext dbContext, ILogger<LogisticsNoSessionService> logger)
    {
        _dbcontext = dbContext;
        _logger = logger;
    }

   
}