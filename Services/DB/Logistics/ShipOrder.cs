using Microsoft.EntityFrameworkCore;
using Model = ERP.Models.DB.Logistics.ShipOrder;
using ShipTemplateModel = ERP.Models.Logistics.ShipTemplate;
using ShipOrderViewModel = ERP.Models.DB.Logistics.ShipOrder;
using ShipTemplateService = ERP.Services.DB.Logistics.ShipTemplate;
using ERP.ViewModels.Logistics.ShipidOrder;
using ERP.Extensions;
using Newtonsoft.Json;
using ERP.Interface;
using System.Diagnostics;
using ERP.Services.Caches;
using ERP.Data;
namespace ERP.Services.DB.Logistics;
using Models.DB.Orders;

public class ShipOrder : BaseService
{
    ////原ERP3.0注释
    #region ERP3.0 PHPService中方法签名
    /**
     * 获取当前用户下批量发货订单
     * User: pc
     * Date: 2020/8/26
     * @return Collection|ShipOrderModel[]
     */
    //public object getList()
    //{
    //    //    return ShipOrderModel::query()
    //    //        ->userId($this->getSession()->getUserId())
    //    //        ->companyId($this->getSession()->getCompanyId())
    //    //        ->oemId($this->getSession()->getOemId())
    //    //        ->sort()
    //    //        ->get();
    //    return 1;
    //}

    /////**
    //// * 获取批量发货订单详情
    //// * User: CCY
    //// * Date: 2020/8/28
    //// * @param $id
    //// * @return mixed
    //// */
    //public ReturnStruct getInfo(/*$id*/)
    //{
    //    //    return ShipOrderModel::query()
    //    //        ->userId($this->getSession()->getUserId())
    //    //        ->companyId($this->getSession()->getCompanyId())
    //    //        ->oemId($this->getSession()->getOemId())
    //    //        ->find($id);
    //    return ReturnArr();
    //}

    /////**
    //// * 添加批量待发货订单
    //// * User: CCY
    //// * Date: 2020/8/28
    //// * @param $order
    //// * @return bool
    //// */
    //public ReturnStruct addShipOrder(/*$order*/)
    //{
    //    //    $address = [
    //    //        'phone'        => $order['receiver_phone'],
    //    //        'nation_id'    => $order['receiver_nation_id'],
    //    //        'nation_short' => $order['receiver_nation_short'],
    //    //        'nation'       => $order['receiver_nation'],
    //    //        'province'     => $order['receiver_province'],
    //    //        'city'         => $order['receiver_city'],
    //    //        'district'     => $order['receiver_district'],
    //    //        'address'      => $order['receiver_address'],
    //    //        'address2'     => $order['receiver_address2'],
    //    //        'address3'     => $order['receiver_address3'],
    //    //        'zip'          => $order['receiver_zip'],
    //    //        'name'         => $order['receiver_name'],
    //    //        'email'        => $order['receiver_email'],
    //    //    ];
    //    //    $insertData = [
    //    //        'declare'          => $order->getRawOriginal('product_money'),
    //    //        'declare_unit'     => 'USD',
    //    //        'battery'          => 0,
    //    //        'length'           => 0,
    //    //        'width'            => 0,
    //    //        'height'           => 0,
    //    //        'weight'           => 0,
    //    //        'goods'            => $order->getRawOriginal('goods'),
    //    //        'order_id'         => $order['id'],
    //    //        'order_no'         => $order['order_no'],
    //    //        'nation_short'     => $order['receiver_nation_short'],
    //    //        'nation'           => $order['receiver_nation'],
    //    //        'address'          => json_encode($address),
    //    //        'ship_template_id' => 0,
    //    //        'platform_id'      => 1,
    //    //        'user_id'          => $this->getSession()->getUserId(),
    //    //        'group_id'         => $this->getSession()->getGroupId(),
    //    //        'company_id'       => $this->getSession()->getCompanyId(),
    //    //        'oem_id'           => $this->getSession()->getOemId(),
    //    //        'created_at'       => $this->datetime
    //    //    ];
    //    //    return ShipOrderModel::query()->insert($insertData);
    //    return ReturnArr();
    //}

    /////**
    //// * 删除批量待发货订单
    //// * User: CCY
    //// * Date: 2020/8/28
    //// * @param $id
    //// * @return mixed
    //// */
    //public ReturnStruct delShipOrder(/*$id*/)
    //{
    //    //    return ShipOrderModel::query()
    //    //        ->where('id', $id)
    //    //        ->userId($this->getSession()->getUserId())
    //    //        ->companyId($this->getSession()->getCompanyId())
    //    //        ->oemId($this->getSession()->getOemId())
    //    //        ->delete();
    //    return ReturnArr();

    //}

    /////**
    //// * 批量待发货订单绑定发货模版
    //// * User: CCY
    //// * Date: 2020/8/28
    //// * @param $shipTemplateId
    //// * @return mixed
    //// */
    //public ReturnStruct bindShipTemplate(/*$shipTemplateId*/)
    //{
    //    //return ShipOrderModel::query()
    //    //    ->userId($this->getSession()->getUserId())
    //    //    ->companyId($this->getSession()->getCompanyId())
    //    //    ->oemId($this->getSession()->getOemId())
    //    //    ->update(['ship_template_id' => $shipTemplateId]);
    //    return ReturnArr();
    //}



    #endregion

    private readonly Unit _UnitCache;
    private readonly DBContext _MyDbContext;
    //private readonly ShipTemplateService _ShipTemplateService;
    private readonly ISessionProvider _ISessionProvider;
    public ShipOrder(DBContext myDbContext, ISessionProvider sessionProvider, Unit UnitCache)
    {
        _UnitCache = UnitCache;
        _MyDbContext = myDbContext;
        _ISessionProvider = sessionProvider;
    }
    private int CompanyID { get => _ISessionProvider.Session!.GetCompanyID(); }
    private int UserID { get => _ISessionProvider.Session!.GetUserID(); }
    private int GroupID { get => _ISessionProvider.Session!.GetGroupID(); }
    private int OEMID { get => _ISessionProvider.Session!.GetOEMID(); }
    public IList<Model>? GetShipOrder()
    {
        return _MyDbContext.ShipOrder.ToList();
    }
    /// <summary>
    /// 获取当前用户下批量发货订单
    /// </summary>
    /// <returns></returns>
    public async Task<List<Model>?> GetList()
    {
        var data = await _MyDbContext.ShipOrder
            .Where(x => x.UserID == UserID)
            .Where(x => x.OEMID == OEMID)
            .Where(x => x.CompanyID == CompanyID)
            .OrderBy(o => o.ID).ToListAsync();
        //.Select(x => new
        //{
        //    x.Weight,
        //    x.Width,
        //    x.Height,
        //    x.Battery,
        //    x.PlatformId,
        //    x.OrderNo,
        //    x.OrderId,
        //    Address = JsonConvert.DeserializeObject(x.Address!),
        //    Goods =x.Goods != null ? x.Goods.Distinct() : new object(),
        //    x.Description,
        //    x.Declare_zh,
        //    x.Declare_en,
        //    x.DeclareUnit,
        //    x.Declare,
        //    x.CustomsCode,
        //    x.ID,
        //    x.Nation,
        //    x.NationShort,
        //    x.ShipTemplateId
        //}).ToListAsync();
        if (data.Any())
        {
            return data;
        }
        return null;
    }

    public async Task AddShipOrder(Order order)//添加批量待发货订单
    {
        Model ShipOrderInfo = new();
        var Address = order.Receiver;
        //_MyDbContext.Entry(Address.Order).State = EntityState.Detached;                      

        ShipOrderInfo.Address = JsonConvert.SerializeObject(Address, new JsonSerializerSettings()
        {
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        });
        ShipOrderInfo.Declare = order.GoodsMoney.Money;
        ShipOrderInfo.DeclareUnit = "USD";
        ShipOrderInfo.Battery = false;
        ShipOrderInfo.Length = 0;
        ShipOrderInfo.Height = 0;
        ShipOrderInfo.Weight = 0;
        ShipOrderInfo.Goods = order.GetGoodsRaw();
        ShipOrderInfo.OrderId = order.ID;
        ShipOrderInfo.OrderNo = order.OrderNo;
        ShipOrderInfo.NationShort = Address != null ? Address.NationShort : "";
        ShipOrderInfo.Nation = Address != null ? Address.Nation : "";
        ShipOrderInfo.ShipTemplateId = 0;
        ShipOrderInfo.PlatformId = order.Plateform;

        ShipOrderInfo.UserID = UserID;
        ShipOrderInfo.GroupID = GroupID;
        ShipOrderInfo.CompanyID = CompanyID;
        ShipOrderInfo.OEMID = OEMID;
        ShipOrderInfo.CreatedAt = DateTime.Now;

        await _MyDbContext.ShipOrder.AddAsync(ShipOrderInfo);
        await _MyDbContext.SaveChangesAsync();
    }
    /// <summary>
    /// 删除批量待发货订单
    /// </summary>
    /// <param name="Ids"></param>
    /// <returns></returns>
    public async Task<bool> delShipOrder(int Id)
    {
        var Del = await _MyDbContext.ShipOrder.Where(o => o.OEMID == OEMID && o.CompanyID == CompanyID && o.UserID == UserID && o.ID == Id).FirstOrDefaultAsync();
        if (Del != null)
        {
            _MyDbContext.ShipOrder.Remove(Del);
            if (_MyDbContext.SaveChanges() > 0)
                return true;
        }
        return false;
    }



    /// <summary>
    /// 批量待发货订单绑定发货模版
    /// </summary>
    /// <param name="ID"></param>
    /// <returns></returns>
    public bool bindShipTemplate(int ID)
    {
        //->userId($this->getSession()->getUserId())
        // ->companyId($this->getSession()->getCompanyId())
        // ->oemId($this->getSession()->getOemId())

        var Data = _MyDbContext.ShipOrder.Where(x => (x.CompanyID == CompanyID && x.OEMID == OEMID && x.UserID == OEMID)).FirstOrDefault();   // *


        //->update(['ship_template_id' => $shipTemplateId]);
        if (Data != null)
        {
            Data.ID = ID;
            if (_MyDbContext.SaveChanges() > 0)
            {
                return true;
            }
        }

        return false;

    }

    /// <summary>
    /// 获取批量发货订单详情
    /// </summary>
    /// <param name="Id"></param>
    /// <returns></returns>
    public async Task<Model?> GetInfo(int Id)
    {
        var Data = await _MyDbContext.ShipOrder.Where(x => x.OEMID == OEMID && x.UserID == UserID && x.CompanyID == CompanyID && x.ID == Id).FirstOrDefaultAsync();
        if (Data != null)
            return Data;

        return null;
    }
    /// <summary>
    /// 获取发货模版详情
    /// </summary>
    /// <param name="Id"></param>
    /// <returns></returns>
    public async Task<List<Model>?> GetShipTemplateId(int Id)
    {
        var Data = await _MyDbContext.ShipOrder.Where(x => x.OEMID == OEMID && x.UserID == UserID && x.CompanyID == CompanyID)
                    .Where(x => x.ShipTemplateId == Id)
                    .ToListAsync();
        if (Data != null)
            return Data;

        return null;
    }


    //// * 更新批量待发货物流附加参数

    public async Task<ReturnStruct> updateOrder(int ID /*$id, $updateData*/)
    {
        var data = await _MyDbContext.ShipOrder
            .Where(x => x.ID == ID && x.OEMID == OEMID && x.UserID == UserID && x.CompanyID == CompanyID)
            .SingleOrDefaultAsync();
        if (data != null)
        {
            _MyDbContext.ShipOrder.Update(data);        //DATA
            if (await _MyDbContext.SaveChangesAsync() > 0)
            {
                return ReturnArr(true, "", data);
            }
        }
        return ReturnArr();
    }
    public async Task<ReturnStruct> UpdateOrders(int[] Ids, decimal Declare, string? DeclareUnit = null, int Tmpid = 0)
    {

        var data = await _MyDbContext.ShipOrder
            .WhenWhere(Ids.Count() > 0, x => Ids.Contains(x.ID))
            .Where(x => x.OEMID == OEMID && x.UserID == UserID && x.CompanyID == CompanyID)
            .ToListAsync();

        if (data != null)
        {
            foreach (var item in data)
            {
                if (Declare != decimal.MinValue && !string.IsNullOrWhiteSpace(DeclareUnit))
                {
                    //decimal declare = DeclareUnit=="CNY"?10000:_UnitCache.List()!.Where(x => x.Sign == DeclareUnit).Select(x => x.Rate).FirstOrDefault();
                    item.Declare = Declare;
                    item.DeclareUnit = DeclareUnit;
                }
                if (Tmpid > 0)
                {
                    item.ShipTemplateId = Tmpid;
                }
                _MyDbContext.Update(item);
            }
            if (await _MyDbContext.SaveChangesAsync() > 0)
            {
                return ReturnArr(true);
            }
        }
        return ReturnArr(false);
    }
    public async Task<ReturnStruct> updateOrderById(int ID, Money Declare, string? DeclareUnit)
    {
        var data = await _MyDbContext.ShipOrder
            .Where(x => x.ID == ID && x.OEMID == OEMID && x.UserID == UserID && x.CompanyID == CompanyID)
            .SingleOrDefaultAsync();
        if (data != null)
        {
            data.Declare = Declare;
            data.DeclareUnit = DeclareUnit ?? "";
            if (await _MyDbContext.SaveChangesAsync() > 0)
            {
                return ReturnArr(true, "", data);
            }
        }
        return ReturnArr();
    }
}

