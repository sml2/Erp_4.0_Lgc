using ViewModel = ERP.ViewModels.Logistics.ShipRatesEstimationTemplate.EditShipRatesEstimationTemplate;
using Model = ERP.Models.Logistics.ShipRatesEstimationTemplate;
using NationCache = ERP.Services.Caches.Nation;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using ERP.Interface;
using ERP.Extensions;
using ERP.Models.DB.Logistics;
using ERP.Data;

namespace ERP.Services.DB.Logistics;

using ERP.Models.Logistics;
using System;
using System.Linq.Expressions;
using ViewModels.Logistics.ShipRatesEstimationTemplate;

public class ShipRatesEstimationTemplate : BaseService
{
    ////原ERP3.0注释

    #region ERP3.0 PHPService中方法签名

    ////    public $model;

    ////    public function __construct()
    ////    {
    ////        parent::__construct();
    ////        $this->model = new Model();
    ////    }

    ////    /**
    ////     * 数据库增加运费估算模板
    ////     * @param $data
    ////     * @return array
    ////     * @throws Throwable
    ////     * @author ryu <mo5467@126.com>
    ////     */
    //public ReturnStruct create(/*$data*/)//: array
    //{
    //    //        $session = $this->getSession();
    //    //        try {
    //    //            DB::beginTransaction();
    //    //            $data = array_merge($data, [
    //    //                'oem_id'     => $session->getOemId(),
    //    //                'company_id' => $session->getCompanyId(),
    //    //                'group_id'   => $session->getGroupId(),
    //    //                'user_id'    => $session->getUserId(),
    //    //                'data'       => "{}",
    //    //            ]);
    //    //    Model::query()->create($data);
    //    //    DB::commit();
    //    //        } catch (Throwable $e) {
    //    //            DB::rollBack();
    //    //            return returnArr(false, $e->getMessage());
    //    //        }
    //    //        return returnArr();
    //    return ReturnArr();
    //}

    ////    /**
    ////     * 获取当前用户公司下的模板数量
    ////     * @return int
    ////     * @author ryu <mo5467@126.com>
    ////     */
    //public int getTemplateNum()//: int
    //{
    //    //        $session = $this->getSession();
    //    //    return Model::companyId($session->company_id)
    //    //        ->oemId($session->oem_id)
    //    //        ->count();
    //    return 0;
    //}

    /////**
    //// * 更新模板
    //// * @param $id
    //// * @param $data
    //// * @return array
    //// * @throws Throwable
    //// * @author ryu <mo5467@126.com>
    //// */
    //public ReturnStruct update(/*$id, $data*/)//: array
    //{
    //    //        $session = $this->getSession();
    //    //        $row = Model::oemId($session->oem_id)->companyId($session->company_id)->find($id);
    //    //    if (!$row) {
    //    //        return returnArr(false, "模板不存在");
    //    //    }

    //    //    try
    //    //    {
    //    //        DB::beginTransaction();
    //    //            $row->name = $data['name'];
    //    //            $row->save();
    //    //        DB::commit();
    //    //    }
    //    //    catch (Throwable $e) {
    //    //        DB::rollBack();
    //    //        return returnArr(false, $e->getMessage());
    //    //    }
    //    //    return returnArr();
    //    return ReturnArr();
    //}

    ////    /**
    ////     * 删除模板
    ////     * @param $id
    ////     * @return array|bool
    ////     * @throws \Exception
    ////     * @author ryu <mo5467@126.com>
    ////     */
    //public ReturnStruct deleteInfoById(/*$id*/)
    //{
    //    //        $session = $this->getSession();
    //    //        $row = Model::oemId($session->oem_id)->companyId($session->company_id)->find($id);
    //    //        if (!$row) {
    //    //        return returnArr(false, "模板不存在");
    //    //    }
    //    //        $row->delete();
    //    //    return true;
    //    return ReturnArr();
    //}

    #endregion

    private readonly ISessionProvider _SessionProvider;

    #region PHP->C#

    private readonly DBContext _MyDbContext;
    private readonly ERP.Services.Caches.PlatformData _platformDataCache;

    public ShipRatesEstimationTemplate(DBContext _myDbContext, ISessionProvider sessionProvider,
        NationCache nationCache, ERP.Services.Caches.PlatformData platformDataCache)
    {
        _MyDbContext = _myDbContext;
        _SessionProvider = sessionProvider;
        _platformDataCache = platformDataCache;
    }

    private ISession Session
    {
        get => _SessionProvider.Session!;
    }

    private string UserName
    {
        get => Session.GetUserName();
    }

    private string TrueName
    {
        get => Session.GetTrueName();
    }

    private int UserID
    {
        get => Session.GetUserID();
    }

    private int GroupID
    {
        get => Session.GetGroupID();
    }

    private int CompanyID
    {
        get => Session.GetCompanyID();
    }

    private int OEMID
    {
        get => Session.GetOEMID();
    }

    /// <summary>
    /// 获取运费模版数据
    /// </summary>
    /// <param name="Name"></param>
    /// <returns></returns>
    public ReturnStruct GetList(string? Name) //IList<Model>
    {
        //   // ->userId($session->getUserId())->原3.0注释代码
        // ->companyId($session->getCompanyId())
        // ->oemId($session->getOemId())
        var Data = _MyDbContext.ShipRatesEstimationTemplates
            .Where(x => x.CompanyID == CompanyID && x.OEMID == OEMID)
            .WhenWhere(!string.IsNullOrWhiteSpace(Name), x => x.Name.StartsWith(Name!))
            .OrderByDescending(x => x.ID)
            .Select(x => new
            {
                x.ID, x.Name, x.Order, x.Mode, x.CreatedAt, Data = JsonConvert.DeserializeObject(x.Data) ?? string.Empty
            }).ToList();
        if (Data.Count < 0)
        {
            return ReturnArr(false, "失败");
        }

        return ReturnArr(true, "成功", Data);
    }

    /// <summary>
    /// 获取运费模版详情
    /// </summary>
    /// <param name="Id"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> Info(int Id)
    {
        var data = await _MyDbContext.ShipRatesEstimationTemplates
            .Where(a => a.ID == Id && a.CompanyID == CompanyID && a.OEMID == OEMID)
            .FirstOrDefaultAsync();
        if (data != null)
        {
            return ReturnArr(true, "", new
            {
                ID = data.ID,
                Name = data.Name,
                CompanyID = data.CompanyID,
                CreatedAt = data.CreatedAt,
                GroupID = data.GroupID,
                Mode = data.Mode,
                OEMID = data.OEMID,
                Order = data.Order,
                UpdatedAt = data.UpdatedAt,
                Data = JsonConvert.DeserializeObject(data.Data) ?? string.Empty
            });
        }

        return ReturnArr(false);
    }

    //$info["nationList"] = array_values(Cache::instance()->amazonNation()->get());       //Cache
    //$info["unitList"] = Cache::instance()->unit()->get();

    /// <summary>
    /// 数据库增加运费估算模板
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> Create(ViewModel ViewModel)
    {
        Model model = new()
        {
            UserID = UserID,
            CompanyID = CompanyID,
            OEMID = OEMID,
            GroupID = GroupID,
            Name = ViewModel.Name,
            Mode = ViewModel.Mode,
        };
        await _MyDbContext.ShipRatesEstimationTemplates.AddAsync(model);

        if (await _MyDbContext.SaveChangesAsync() > 0)
        {
            return ReturnArr(true, "成功");
        }

        return ReturnArr(false, "失败");
    }

    /// <summary>
    /// 更新发货模版
    /// </summary>
    /// <param name="Id"></param>
    /// <param name="model"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> Update(ViewModel ViewModel)
    {
        var Row = await _MyDbContext.ShipRatesEstimationTemplates
            .Where(x => x.OEMID == OEMID && x.CompanyID == CompanyID)
            .SingleOrDefaultAsync(x => x.ID == ViewModel.ID);

        if (Row != null)
        {
            Row.Name = ViewModel.Name;
            _MyDbContext.Update(Row);
            _MyDbContext.SaveChanges();
            return ReturnArr(true, "成功");
        }

        return ReturnArr(false, "失败");
    }

    public int GetTemplateNum()
    {
        return _MyDbContext.ShipRatesEstimationTemplates
            .Where(x => x.OEMID == OEMID && x.CompanyID == CompanyID)
            .Count();
    }

    /// <summary>
    /// 删除模板
    /// </summary>
    /// <param name="ID"></param>
    /// <returns></returns>
    public bool DeleteInfoById(int ID)
    {
        var Row = _MyDbContext.ShipRatesEstimationTemplates
            .Where(x => x.OEMID == OEMID && x.CompanyID == CompanyID)
            .SingleOrDefault(x => x.ID == ID);
        if (Row == null)
        {
            return false;
        }

        _MyDbContext.RemoveRange(Row);
        if (_MyDbContext.SaveChanges() > 0)
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// 查找ID
    /// </summary>
    /// <param name="ID"></param>
    /// <returns></returns>
    public async Task<Model?> authByUser(int ID)
    {
        var Data = await _MyDbContext.ShipRatesEstimationTemplates
            .Where(x => x.OEMID == OEMID && x.CompanyID == CompanyID && x.ID == ID).FirstOrDefaultAsync();
        if (Data != null)
        {
            return Data;
        }

        return null;
    }

    public async Task<bool> EditTemplateNation(data data)
    {
        var TempModel = await authByUser(data.ID);
        if (TempModel is null)
            return false;

        var ModelData = JsonConvert.DeserializeObject<Dictionary<string, List<TempData>>>(TempModel.Data) ?? new();

        foreach (var item in data.nation)
        {
            var result = _platformDataCache
                .List(Platforms.AMAZON).FirstOrDefault(a => a.GetAmazonData()!.Short.Contains(item.@short)) ?? null;
            var Short = result?.GetAmazonData()?.Short;
            if (Short is null)
            {
                return false;
            }
            else
            {
                if (ModelData is not null && ModelData.Count > 0 && ModelData.ContainsKey(Short))
                {
                    foreach (var MData in ModelData)
                    {
                        if (MData.Key.Equals(Short))
                        {
                            MData.Value.Clear();
                            MData.Value.AddRange(data.list);
                        }
                    }
                }
                else if (ModelData is not null && ModelData.Count > 0 && !ModelData.ContainsKey(Short))
                {
                    ModelData?.Add(Short, data.list);
                }
                else
                {
                    ModelData?.Add(Short, data.list);
                }
            }
        }

        TempModel.Data = JsonConvert.SerializeObject(ModelData);
        _MyDbContext.ShipRatesEstimationTemplates.Update(TempModel);
        return await _MyDbContext.SaveChangesAsync() > 0 ? true : false;
    }

    #endregion
}