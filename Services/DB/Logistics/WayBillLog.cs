﻿using ERP.Data;
using ERP.Extensions;
using ERP.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using WayBillLogModel = ERP.Models.DB.Logistics.WaybillLog;
namespace ERP.Services.DB.Logistics;
public class WayBillLog : BaseService
{
    private readonly ISessionProvider _SessionProvider;

    private readonly DBContext _Mydbcontext;
    public WayBillLog(DBContext _mydbcontext, ISessionProvider sessionProvider)
    {
        _Mydbcontext = _mydbcontext;
        _SessionProvider = sessionProvider;
    }
    private ISession Session { get => _SessionProvider.Session!; }
    private string UserName { get => Session.GetUserName(); }
    private string TrueName { get => Session.GetTrueName(); }
    private int UserID { get => Session.GetUserID(); }
    private int GroupID { get => Session.GetGroupID(); }
    private int CompanyID { get => Session.GetCompanyID(); }
    private int OEMID { get => Session.GetOEMID(); }

    ///**
    // * @param $id
    // * @return mixed
    // * User: Ly
    // * Date: 2020/6/3
    // * 查找运单日志
    // */
    public async Task<object> FindLog(int Id)
    {
        var data = await _Mydbcontext.WaybillLogs.Where(x => x.WaybillId == Id).Select(x => new { x.Action, x.UserName, x.UpdatedAt }).ToListAsync();
        return data;
    }

    ///**
    // * 增加运单日志
    // * User: CCY
    // * Date: 2019/10/25 15:01
    // * @param string $order_id
    // * @param string $waybill_id
    // * @param string $platform_id
    // * @param string $action
    // * @return int|string
    // */
    public ReturnStruct addLog(string OrderId, int WaybillId, Platforms PlatformId, string Action)
    {
        //$session = $this->getSession();

        DateTime Now = DateTime.Now;
        WayBillLogModel waybillLog = new()
        {
            Platform = PlatformId,
            OrderId = int.Parse(OrderId),
            WaybillId = WaybillId,
            Action = Action,
            UserID = UserID,
            Truename = TrueName,
            CompanyID = CompanyID,
            OEMID = OEMID,
            GroupID = GroupID,
            CreatedAt = Now,
            UpdatedAt = Now,
        };
        _Mydbcontext.WaybillLogs.Add(waybillLog);   ///insertGetId    transformAttributes
        if (_Mydbcontext.SaveChanges() > 0)
        {
            return ReturnArr(true);

        }
        return ReturnArr(false);
        //    return WayBillLogModel::query()->insertGetId($data);
    }

}

