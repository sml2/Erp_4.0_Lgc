using ERP.Models.Api.Logistics;
using ERP.Models.DB.Identity;
using ERP.Models.DB.Logistics;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace ERP.Services.DB.Logistics;

using logisticsFactory = ERP.Services.Api.Logistics.Factory;

public class WaybillFeeHost
{
    private readonly IServiceProvider ServiceProvider;
    private readonly ILogger<WaybillFeeHost> _logger;

    public WaybillFeeHost(ILogger<WaybillFeeHost> logger, IServiceProvider serviceProvider)
    {
        _logger = logger;
        ServiceProvider = serviceProvider;
    }

    public async Task PullWaybillFee(bool isDistribution, DateTime begin, DateTime end,User user)
    {
        _logger.LogInformation($"======================================Start Pull Waybill Fee Queue========================================");
        _logger.LogInformation($"Begin:{begin.ToString("yyyy-MM-dd HH:mm:ss")},End:{end.ToString("yyyy-MM-dd HH:mm:ss")},IsDistribution:{isDistribution}");
        const int count = 10;
        const int delayHour = 2;
        try
        {
            var serviceProvider = this.ServiceProvider.CreateScope().ServiceProvider;
            var service = serviceProvider.GetRequiredService<WaybillNoSessionService>();
            var shippingFeeService = serviceProvider.GetRequiredService<ShippingFeeService>();
            var logistics = serviceProvider.GetRequiredService<logisticsFactory>();
            var _dbcontext = serviceProvider.GetRequiredService<ERP.Data.DBContext>();
            List<Waybill> listNeedPull = new List<Waybill>();

            if (isDistribution)
            {
                listNeedPull = await service.GetWaybillNeedPullIsDistribution(begin, end, user);
            }
            else
            {
                listNeedPull = await service.GetWaybillNeedPullNoDistribution(begin, end, user);
            }
            
            if (listNeedPull.IsNull() || listNeedPull!.Count <= 0)
            {
                _logger.LogInformation($"HandPullWaybillFee:have no waybills！");
                return;
            }

            _logger.LogInformation(
                $"HandPullWaybillFee:WaybillIds：{string.Join(",", listNeedPull!.Select(x => x.ID).ToList())}");
            int i = 0;
            while (listNeedPull.Skip(i * count).Take(count).ToList().Any())
            {
                var waybillList = listNeedPull.Skip(i * count).Take(count);
                i++;
                foreach (var item in waybillList)
                {
                    _logger.LogInformation($"HandPullWaybillFee:Start：【{item.ID} [{item.Express}]】");
                    
                    WaybillInfos waybill = new WaybillInfos(item);
                    var result = await logistics.GetFee(waybill);
                    var res = JsonConvert.SerializeObject(result);
                    ;
                    _logger.LogInformation($"Response: {res}");
                    if (result.Success)
                    {
                        var priceInfo = result.Price;
                        if (priceInfo != null && priceInfo.Success)
                        {
                            var transaction = await _dbcontext.Database.BeginTransactionAsync();
                            try
                            {

                                await shippingFeeService.EditWayBillPrice(priceInfo, item.MergeID, user,false);
                                await transaction.CommitAsync();
                                _logger.LogInformation($"Success: Update Fee Success");
                            }
                            catch (Exception e)
                            {
                                _logger.LogError(e.Message, e);
                                await transaction.RollbackAsync();
                                _logger.LogInformation($"Error: 发生异常{e.Message}");
                            }
                        }
                        else
                        {
                            //修改最后拉取的时间
                            item.LastAutoShipTime = DateTime.Now.AddHours(delayHour);
                            await _dbcontext.SaveChangesAsync();
                            _logger.LogInformation($"Delay: 未获取到结果自动拉取延迟{delayHour}小时");
                        }
                    }
                    else
                    {
                        //修改最后拉取的时间
                        item.LastAutoShipTime = DateTime.MaxValue;
                        await _dbcontext.SaveChangesAsync();
                        _logger.LogInformation($"Fail: 无法获取结果");
                    }
                }
            }
            _logger.LogInformation($"======================================End Pull Waybill Fee Queue========================================");
        }
        catch (Exception ex)
        {
            _logger.LogError($"HandPullWaybillFee Exception {ex.Message}");
            _logger.LogError($"HandPullWaybillFee Exception {ex.StackTrace}");
            
            _logger.LogInformation($"======================================End Pull Waybill Fee Queue========================================");
        }
    }
}