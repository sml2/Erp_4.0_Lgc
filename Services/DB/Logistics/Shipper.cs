using ERP.Extensions;
using ERP.Interface;
using Microsoft.EntityFrameworkCore;
using LogisticsShipperModel = ERP.Models.View.Logistics.ShipidOrder.LogisticsVmShipper;
using Model = ERP.Models.Logistics.Shipper;

using ERP.Data;
namespace ERP.Services.DB.Logistics;
public class Shipper : BaseService
{


    private readonly DBContext _myDbContext;
    private readonly ISessionProvider _ISessionProvider;
    public Shipper(DBContext myDbContext, ISessionProvider SessionProvider)
    {
        _myDbContext = myDbContext;
        _ISessionProvider = SessionProvider;
    }
    private int CompanyID { get => _ISessionProvider.Session!.GetCompanyID(); }
    private int UserID { get => _ISessionProvider.Session!.GetUserID(); }
    private int GroupID { get => _ISessionProvider.Session!.GetGroupID(); }
    private int OEMID { get => _ISessionProvider.Session!.GetOEMID(); }
    /// <summary>
    /// 更新寄件人信息
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="logisticsShipper"></param>
    /// <returns></returns>

    public async Task<ReturnStruct> sendInfo(LogisticsShipperModel? logisticsShipper)
    {
        if (logisticsShipper?.ID > 0)
        {
            return await editSendInfo(logisticsShipper);
        }
        else
        {
            return await addSendInfo(logisticsShipper);
        }
    }

    public async Task<ReturnStruct> addSendInfo(LogisticsShipperModel? logisticsShipper)
    {
        Model Model = new();
        Model.LastTime = DateTime.Now;
        Model.UserID = UserID;
        Model.GroupID = GroupID;
        Model.CompanyID = CompanyID;
        Model.OEMID = OEMID;
        Model.Name = logisticsShipper!.Name;
        Model.Email = logisticsShipper.Email;
        Model.Postcode = logisticsShipper.Postcode;
        Model.Phone = logisticsShipper.Phone;
        Model.Province = logisticsShipper.Province;
        Model.City = logisticsShipper.City;
        Model.District = logisticsShipper.District;
        Model.Address = logisticsShipper.Address;
        Model.Company = logisticsShipper.Company ?? null;
        await _myDbContext.LogisticsShipper.AddAsync(Model);
        if (_myDbContext.SaveChanges() > 0)
        {
            return ReturnArr(true, "", Model);
        }
        return ReturnArr(false, "");
    }

    public async Task<ReturnStruct> editSendInfo(LogisticsShipperModel? logisticsShipper)
    {
        Model? Model = new();

        if (logisticsShipper?.ID > 0)
        {
            Model = await _myDbContext.LogisticsShipper.FindAsync(logisticsShipper.ID);
            if (Model == null) return ReturnArr(false);
            Model.UserID = UserID;
            Model.GroupID = GroupID;
            Model.CompanyID = CompanyID;
            Model.OEMID = OEMID;
            Model.Name = logisticsShipper.Name;
            Model.Email = logisticsShipper.Email;
            Model.Postcode = logisticsShipper.Postcode;
            Model.Phone = logisticsShipper.Phone;
            Model.Province = logisticsShipper.Province;
            Model.City = logisticsShipper.City;
            Model.District = logisticsShipper.District;
            Model.Address = logisticsShipper.Address;
            Model.Company = logisticsShipper.Company ?? null;
            _myDbContext.LogisticsShipper.Update(Model);
            if (_myDbContext.SaveChanges() > 0)
            {
                return ReturnArr(true, "", Model);
            }
            return ReturnArr(false, "");
        }
        else
        {
            return ReturnArr(false);
        }
    }


    /// <summary>
    /// 产看公司寄件人信息
    /// </summary>
    /// <param name="CompanyID"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> findInfo(int CompanyID)
    {
        var data = await _myDbContext.LogisticsShipper
            .Where(x => x.CompanyID == CompanyID)
            .OrderByDescending(x => x.LastTime)
            .ToListAsync();
        return ReturnArr(true, "", data);
    }
    /// <summary>
    /// 修改时间显示类型
    /// </summary>
    /// <param name="ID"></param>
    /// <returns></returns>
    public bool updateLastTime(int ID)
    {
        DateTime date = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        var data = from o in _myDbContext.LogisticsShipper
                   where o.CompanyID.Equals(ID)
                   select o.LastTime == date;
        if (data.Any())
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    /// <summary>
    /// 寄件信息列表页
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public async Task<DbSetExtension.PaginateStruct<Model>> getList(string? name)
    {
        return await _myDbContext.LogisticsShipper
          .WhenWhere(!name.IsNullOrEmpty(), x => x.Name.StartsWith(name!))
          .Where(x => x.CompanyID == CompanyID)
          .OrderByDescending(x => x.UpdatedAt)
          .ToPaginateAsync();
        //->paginate($this->limit);   分页
    }


    public async Task<ReturnStruct> GetListNoPage(string? name)
    {
        var data = await _myDbContext.LogisticsShipper
         .WhenWhere(!string.IsNullOrEmpty(name), x => x.Name.Contains(name!))
         .Where(x => x.CompanyID == CompanyID)
         .OrderByDescending(x => x.UpdatedAt)
         .ToListAsync();
        return ReturnArr(true, "", data);
    }
    /// <summary>
    /// 单个删除寄件信息
    /// </summary>
    /// <param name="Id"></param>
    /// <returns></returns>
    public bool DelInfo(int Id)
    {
        var Del = _myDbContext.LogisticsShipper.Find(Id);
        if (Del != null && !Id.Equals(null))
        {
            _myDbContext.LogisticsShipper.RemoveRange(Del);
            if (_myDbContext.SaveChanges() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }


    //public int updateLastTime(int Id)
    //{
    //    //$time = date('Y-m-d H:i:s');
    //    //return logisticsShipperModel::query()->where('id', $id)
    //    //    ->update(['last_time' => $time]);
    //    _myDbContext.LogisticsShipper.Where(x => x.ID == Id).ToList();

    //}
}

