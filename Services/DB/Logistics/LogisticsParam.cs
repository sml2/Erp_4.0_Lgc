using ERP.Data;
using ERP.Enums.Logistics;
using ERP.Extensions;
using ERP.Interface;
using ERP.ViewModels.Logistics;
using ERP.Models.Logistics.ViewModels.Parameter;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using static ERP.Extensions.DbSetExtension;
using LogisticsService = ERP.Services.Caches.Logistics;
using Model = ERP.Models.DB.Logistics.Parameter;

namespace ERP.Services.DB.Logistics;

public class LogisticsParam : BaseService
{

    private readonly DBContext _MyDbContext;
    private LogisticsService _LogisticsService;
    private readonly ISessionProvider _ISessionProvider;
    public LogisticsParam(DBContext myDbContext, LogisticsService logisticsService, ISessionProvider SessionProvider)
    {
        _MyDbContext = myDbContext;
        _LogisticsService = logisticsService;
        _ISessionProvider = SessionProvider;
    }
    private int CompanyID { get => _ISessionProvider.Session!.GetCompanyID(); }
    private int UserID { get => _ISessionProvider.Session!.GetUserID(); }
    private int GroupID { get => _ISessionProvider.Session!.GetGroupID(); }
    private int OEMID { get => _ISessionProvider.Session!.GetOEMID(); }
    /// <summary>
    /// 获取当前公司已有物流的系统物流公司id
    /// </summary>
    /// <returns></returns>
    public List<Platforms> GetLogisticsId()//: \Illuminate\Support\Collection
    {
    
        return _MyDbContext.LogisticsParam.Where(x=>x.CompanyID== CompanyID)
          .Where(x => x.IsSysState == true)
          .Select(x => x.Platform.Platform) //->pluck('logistics_id');    
          .ToList();
        //Dictionary<int,string> Params = new Dictionary<int,string>();

    }

    /// <summary>
    /// 获取当前公司已有物流的系统物流公司
    /// </summary>
    /// <returns></returns>
    public async Task<ReturnStruct> GetUsedLogistics()
    {
        var GetLogistics = _LogisticsService.List();   //缓存$sysLogistics = $this->getLogisticsCache();
        var GetLogisticIds = GetLogisticsId();

        //string Json = JsonConvert.SerializeObject(GetLogisticIds);

        object? data = null;
        List<object> list = new();
        if (GetLogisticIds != null && GetLogistics != null)
        {
            foreach (var item in GetLogisticIds)
            {
                data = await _MyDbContext.LogisticsParam
                           .Where(x => x.Opened == true)
                           .WhenWhere(item > 0, x => x.Platform == item)
                           .Select(o => new { id = o.ID, name = o.Platform.Name }).FirstOrDefaultAsync();
                if (data != null)
                {
                    list.Add(data);
                }
            }
            return ReturnArr(true, "成功", list);
        }
        return ReturnArr(false, "失败");
    }
    /// <summary>
    /// 当前公司物流列表
    /// </summary>
    /// <param name="customize_name"></param>
    /// <param name="logistic_id"></param>
    /// <param name="state"></param>
    /// <returns></returns>
    public async Task<PaginateStruct<LogisParamView>?> GetLogisticListPage(Platforms? logistic_id, bool? state, string? customize_name)
    {
        //var S = _MyDbContext.Database.CreateExecutionStrategy();           

        try
        {
            var data = await _MyDbContext.LogisticsParam
                .Where(x => x.CompanyID == CompanyID)
                .Where(o => o.IsSysState == true)
                .WhenWhere(logistic_id.HasValue && logistic_id != Platforms.None, o => o.Platform.Platform == logistic_id)
                .WhenWhere(state != null, o => o.Opened == state)
                .WhenWhere(!string.IsNullOrEmpty(customize_name), o => o.CustomizeName.StartsWith(customize_name!))
                .OrderBy(o => o.Opened).OrderByDescending(o => o.UpdatedAt).OrderByDescending(o => o.ID)
                .Select(o => new LogisParamView(
                    o.ID,
                    o.CustomizeName,
                    o.Platform.Platform.GetID(),
                    o.Platform.Name,
                    o.Remark,
                    o.Opened,
                    o.Source,
                    o.CreatedAt,
                    o.UpdatedAt
                ))
                .ToPaginateAsync();
            if (data != null)
            {
                return data;
            }
            return null;
        }
        catch (Exception e)
        {
            return null;
        }
    }

    public record LogisParamView(int ID,string CustomizeName, int PlatfromID,string PlatfromSign,
                string? Remark,bool State, Sources Source,DateTime CreatedAt, DateTime UpdatedAt);


    public async Task<Result<List<LogisticsViewModel>>> GetLogisticParamList()
    {
        var data = await _MyDbContext.LogisticsParam.Include(x => x.Platform)
            .Where(o => o.Opened==true && o.IsSysState == true && o.CompanyID == CompanyID)
            .Select(s => new LogisticsViewModel()
            {
                ID = s.ID,
                Sign = s.Platform.Platform.ToString(),
                Name = s.Platform.Name,
                CustomizeName = s.CustomizeName,
                Remark = s.Remark ?? "",
                Data = s.Data ?? "",
                IsDistributed = s.ParentID != 0,
            }).AsNoTracking().ToListAsync();

        return Result.Ok(data);
    }





    //public async Task<ReturnStruct> GetLogisticList()
    //{

    //}

    /// <summary>
    /// 内部添加物流配置参数信息
    /// </summary>
    /// <param name="logisticsParam"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> AddLogisticParam(EditModel LogistVm)
    {
        Model logisticsParam = new()
        {
            CustomizeName = LogistVm.CustomizeName,
            Platform = LogistVm.LogisticsID,
            Remark = LogistVm.Remark,
            Data = LogistVm.Data.ToString(),
            Opened = true,
            IsSysState = true,
            Source = Sources.JUNIOR,
            ParentID = 0,
            Charge = Charges.PRIVATE,
            CompanyID = CompanyID,
            OEMID = OEMID,
            GroupID = GroupID,
            UserID = UserID,
        };

        var data = await _MyDbContext.LogisticsParam.AddAsync(logisticsParam);
        if (_MyDbContext.SaveChanges() > 0)
        {
            return ReturnArr(true, "添加成功", data);
        }

        return ReturnArr(false, "");

    }
    /// <summary>
    /// 接口获取物流列表
    /// </summary>
    /// <param name="CompanyId"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> GetListByCompanyId()
    {
        var data = await _MyDbContext.LogisticsParam
            .Where(x => x.CompanyID == CompanyID && x.Opened == true)
            .OrderByDescending(x => x.LastTime)
            .ToListAsync();
        return ReturnArr(true, "", data);
    }
    /// <summary>
    ///  获取单个物流全部信息
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public Model? GetLogisticsInfo(int ID)
    {
        return _MyDbContext.LogisticsParam.FirstOrDefault(x => x.ID == ID);
    }



    public int GetWaybillCompanyIDByLogisticParamID(int ID)
    {
        var model = GetLogisticsInfo(ID);
        if (model is not null)
        {
            if(model.ParentID!=0)
            {
                var modelP = GetLogisticsInfo(model.ParentID);
                if (modelP is not null)
                {
                    return modelP.CompanyID;
                }
                else
                {
                    throw new Exception("LogisticParam Not Found");
                }
            }
            return 0;
        }
        else
        {
            throw new Exception("LogisticParam Not Found");
        }
    }


    /// <summary>
    /// 删除物流并删除分配下级物流       //*
    /// </summary>
    /// <param name="Id"></param>
    /// <returns></returns>
    public bool Del(int Id)
    {
        var Data = _MyDbContext.LogisticsParam.Find(Id);
        if (Data != null)
        {
            _MyDbContext.LogisticsParam.Remove(Data);
            var ms = _MyDbContext.LogisticsParam.Where(o => o.ParentID == Id && o.Source == Sources.SUPERIOR).ToList();
            if (ms != null && ms.Count > 0)
            {
                _MyDbContext.LogisticsParam.RemoveRange(ms);
            }
            try
            {
                return _MyDbContext.SaveChanges() > 0;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        return true;
    }
    /// <summary>
    /// 获取编辑需要信息
    /// </summary>
    /// <returns></returns>
    //public async Task<ReturnStruct> GetLogisticsEditInfo(int Id, Model Param)//Sources Source, int LogisticsID
    //{
    //    var data = await _MyDbContext.LogisticsParam.Where(x => x.ID == Param.LogisticsID).SingleOrDefaultAsync();
    //    var GetCompanyId = await _MyDbContext.LogisticsParam.Where(x => x.CompanyID == Id).SingleOrDefaultAsync();

    //    if (data == null)
    //    {
    //        return ReturnArr(false, "参数错误");
    //    }
    //    if (GetCompanyId == null)
    //    {
    //        return ReturnArr(false, "物流不存在");
    //    }
    //    else
    //    {
    //        //string[] nameList = Enum.GetNames(typeof(Models.Logistics.LogisticsParam.Sources));
    //        //内部添加物流
    //        if (Param.Source.Equals(Sources.JUNIOR) && Param.Source == Sources.JUNIOR)
    //        {
    //            if (Param.Platform == Platforms.None )
    //            {
    //                return ReturnArr(false, "物流公司错误");
    //            }
    //            else
    //            {
    //                //if (!ModelState.IsValid)
    //                _MyDbContext.LogisticsParam.Update(Param);
    //                if (_MyDbContext.SaveChanges() > 0)
    //                {
    //                    return ReturnArr(true, "修改成功");
    //                }
    //            }
    //        }
    //        else
    //        {
    //            return ReturnArr(false, "参数错误");
    //        }
    //    }
    //    return ReturnArr(false, "编辑的信息不存在");
    //}

    /// <summary>
    /// 更新物流绑定参数
    /// </summary>
    public bool EditLogisticParam(EditInfo editInfo)
    {
        Model? m = _MyDbContext.LogisticsParam.Find(editInfo.ID);
        if (m == null)
        {
            return false;
        }
        else
        {
            m.CustomizeName = editInfo.CustomizeName;
            m.Opened = editInfo.State;
            m.Remark = editInfo.Remark;
            if (!string.IsNullOrEmpty(editInfo.Param))
            {
                m.Data = editInfo.Param;
            }
            return _MyDbContext.SaveChanges() > 0;
        }
    }
    /// <summary>
    /// 同步更新分配出去物流参数
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="param"></param>
    public bool updateAllot(EditInfo editInfo)
    {
        var ms = _MyDbContext.LogisticsParam.Where(o => o.ParentID == editInfo.ID).ToList();
        if (ms.Any())
        {
            // ms.Data= GetEdit.Param;
            ms.ForEach(o => o.Data = editInfo.Param);
            return _MyDbContext.SaveChanges() > 0;//批量Update
        }
        else
        {
            return true;
        }
    }

    public ReturnStruct UpdateLastTime(int ID)
    {
        var Data = _MyDbContext.LogisticsParam.Find(ID);
        if (Data != null)
        {
            Data.LastTime = DateTime.Now;
            if (_MyDbContext.SaveChanges() > 0)
            {
                return ReturnArr(true, "", Data.LastTime);
            }
            return ReturnArr(false, "", Data.LastTime);
        }
        return ReturnArr(false, "错误");
    }

    #region (没有用到)
    /// <summary>
    /// 获取用户添加的物流信息 前获取物流下拉框过滤  用不到
    /// </summary>
    /// <returns></returns>
    //public async Task<ReturnStruct> GetParameter()
    //{
    //    var data = await _MyDbContext.LogisticsParam
    //        .Include(x => x.Platform)
    //        .Where(x => x.Opened == true)
    //        .Where(x => x.Platform.Platform == x.Platform.Platform)
    //        .ToListAsync();
    //    if (data.Count <= 0)
    //    {
    //        return ReturnArr(false, "失败");
    //    }
    //    List<object> list = new();
    //    JToken json = null;
    //    Dictionary<string, string> VModelDataDic = new();
    //    foreach (var items in data)
    //    {
    //        if (items.Data != null)
    //        {
    //            var EnumerableJson = JsonConvert.DeserializeObject<JArray>(items.Data);

    //            Dictionary<string, string> VModelDic = new();
    //            foreach (var item in EnumerableJson)
    //            {
    //                json = item["__vModel__"];
    //                if (json != null)
    //                {
    //                    string TokenName = "";
    //                    string TokenValue = "";
    //                    foreach (JProperty jToken in json) /*{ "userid":"","value:"1"}*/
    //                    {
    //                        TokenName = jToken.Name.ToString();
    //                        if (TokenName == "value")
    //                        {
    //                            TokenValue = jToken.Value.ToString();
    //                        }
    //                    }
    //                    VModelDic.Add(TokenName, TokenValue);
    //                }
    //                else
    //                {
    //                    return ReturnArr(false, "值为空");
    //                }
    //                VModelDataDic = VModelDic;
    //            }
    //        }
    //        else
    //        {
    //            return ReturnArr(false, "值为空");
    //        }
    //        list.Add(new
    //        {
    //            items.ID,
    //            items.Platform,
    //            items.CustomizeName,
    //            items.Remark,
    //            items.LastTime,
    //            items.ParentID,
    //            items.Opened,
    //            items.Source,
    //            items.Charge,
    //            items.IsSysState,
    //            Data = VModelDataDic,
    //            Sign = items.Platform.Platform.ToString(),
    //        });
    //    }
    //    return ReturnArr(true, "", list);
    //}
    #endregion
}




