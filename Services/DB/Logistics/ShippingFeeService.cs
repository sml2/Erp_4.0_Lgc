using ERP.Data;
using ERP.Distribution.Services;
using ERP.Models.Api.Logistics;
using ERP.Models.DB.Users;
using ERP.Models.Finance;
using ERP.Models.View.User;
using ERP.Services.DB.Orders;
using ERP.Services.DB.Statistics;
using ERP.Services.DB.Users;
using ERP.Services.Statistics;
using User = ERP.Enums.Identity.User;

namespace ERP.Services.DB.Logistics;

public class ShippingFeeService : LogisticsNoSessionService
{
    private readonly CompanyNoSessionService _companyNoSessionService;
    private readonly WaybillNoSessionService _waybillNoSessionService;
    private readonly OrderNoSessionService _orderNoSessionService;
    private readonly StatisticMoney _statisticMoney;
    private readonly Statistic _statisticService;


    public ShippingFeeService(DBContext dbContext, CompanyNoSessionService companyNoSessionService,
        ILogger<ShippingFeeService> logger, WaybillNoSessionService waybillNoSessionService, Statistic statisticService, OrderNoSessionService orderNoSessionService) :
        base(dbContext, logger)
    {
        _companyNoSessionService = companyNoSessionService;
        _waybillNoSessionService = waybillNoSessionService;
        _statisticService = statisticService;
        _orderNoSessionService = orderNoSessionService;
        _statisticMoney = new StatisticMoney(_statisticService);
    }
    
    public async Task<bool> EditWayBillPrice(TrackPrice trackPrice,int? mergeId,Models.DB.Identity.User user,bool isAutoPull)
        {

            if(trackPrice.Unit.Equals("RMB",StringComparison.CurrentCultureIgnoreCase))
            {
                trackPrice.Unit = "CNY";
            }

            if (trackPrice.WaybillId == 0)
            {
                return false;
            }
            //洛琴oem跳过更新价格（4.0无洛琴）
            // if (OemId == 2 && HttpContext.Request.Host.ToString().Contains("luoqin"))
            // {
            //     return true;
            // }
            var info = await _waybillNoSessionService.GetWaybillInfoById(trackPrice.WaybillId);
            if (info == null)
            {
                return false;
            }

            var OldMoney = info.CarriageNum.Money;
            var carriage_num = new MoneyMeta(trackPrice.Unit, trackPrice.Price);
            // info.WaybillRate = carriage_num.Rate;

            var n = new Money(Convert.ToDecimal(trackPrice.Price));
            var m = new MoneyRecordFinancialAffairs(n, carriage_num);

            info.CarriageBase = info.CarriageBase.Next(trackPrice.Unit, trackPrice.Price);
            info.CarriageNum  =  info.CarriageBase.Next(trackPrice.Unit, trackPrice.Price);
            info.CarriageUnit = trackPrice.Unit;
            
            string s = $"订单编号：【{info.OrderNo}】；物流运单号：【{info.Express}】"; 
            //物流平台扣费时间
            if (trackPrice.DeductionTime is not null)
            {
                info.DeductionTime = trackPrice.DeductionTime;
                s += $"扣费时间：【{trackPrice.DeductionTime}】";
            }

            if (isAutoPull)
            {
                //标记为后台任务
                s += "(自动拉取)";
            }
            //最后拉取运费的时间
            info.LastAutoShipTime = DateTime.Now;

            var company = await _companyNoSessionService.GetInfoById(info.CompanyID);
            // var user = await _userManager.GetUserAsync(User);
           
            //上级添加 下级分销上报运单
            if (company.IsDistribution && info.WaybillCompanyID != 0 )
            {
                var waybill_rate = info.WaybillRate;
                if (waybill_rate <= 0)
                {
                    waybill_rate = company.WaybillRateType == 1 ? carriage_num.Amount*company.WaybillRate/100 : new MoneyMeta(trackPrice.Unit, company.WaybillRateNum);
                }
                var carriagenum = carriage_num + waybill_rate;
                info.WaybillRate = waybill_rate;
                info.CarriageNum = new MoneyMeta(trackPrice.Unit, carriagenum);
                
                //退回预扣冻结
                if (!info.IsReturnFreeze && info.WaybillFreeze.HasValue && info.WaybillFreeze.Value > 0)
                {
                    var freezeWaybill = new FreezeWaybill(company, user); // 钱包
                    await freezeWaybill.Recharge(info.WaybillFreeze.Value);
                    await _dbcontext.BillLog.AddAsync(freezeWaybill.AddLog(info.WaybillFreeze.Value, true , BillLog.Modules.WAYBILL, "返还运单预付款冻结", s, info.StoreId, info.ID));
                    info.IsReturnFreeze = true;
                }
            }
            //运单不需要审核
            if (company.WaybillConfirm == Company.WaybillConfirms.NO)
            {
                try
                {
                    await _waybillNoSessionService.Update(info);
                    await _waybillNoSessionService.AddLog(info.OrderId, info.ID, info.Logistic, "同步更新运单运费",new UserInfoDto(user));
                }
                catch (Exception /*e*/)
                {
                    return false;
                }
                return true;
            }
            //自动确认           
            // int belong = 0;
            //
            var Money = new Money(info.CarriageNum.Money.Value);            
            // if (company.IsDistribution && info.WaybillCompanyID != 0)
            // {
            //     belong = 2;
            //     await companyService.ExpensePublicWallet(Money, info.CompanyID);
            // }
            // else
            // {
            //     belong = 1;
            //     await companyService.ExpenseWallet(Money);
            // }
            //当前数据确认           
            info.IsVerify = Enums.Logistics.VerifyStates.Confirm;
            //已确认运单，调用钱包接口
            // if (info.IsVerify == Enums.Logistics.VerifyStates.Confirm && OldMoney.Value!= Money.Value)
            if (info.IsVerify == Enums.Logistics.VerifyStates.Confirm && OldMoney.Value!= Money.Value)
            {               
                              
                var b = OldMoney > Money ? true : false;
                string msg = "";
                if (OldMoney.Value == 0)
                {
                    msg = "运费扣款";
                }
                else
                {
                    msg = b == true ? "运费返还" : "运费补款";
                }                
               
                var wallet = info.GetUsedWallet(company, user); // 钱包
               
                //钱包扣款 
                //运费补款扣余额 运费返还加余额
                var absMoney = Math.Abs(OldMoney - Money);
                if (!b)
                {
                    //拉取运费时不再判断钱包是否充足直接扣费。
                    //if (!wallet.IsEnough(absMoney))
                    //{
                    //    throw new Exception("钱包余额不足");
                    //}

                    await wallet.Expense(absMoney);
                }
                else
                {
                    await wallet.Recharge(absMoney);
                }
                // await wallet.Expense(absMoney);
                
                var billLog = wallet.AddLog(absMoney, b, BillLog.Modules.WAYBILL, msg, s, info.StoreId, info.ID);
                info.BillLog = billLog;
            }


            await _waybillNoSessionService.Update(info);
            //同步订单
            await _orderNoSessionService.UpdateMergeOrderWaybill(mergeId, null, info.OrderId);
            //增加运单日志
            await _waybillNoSessionService.AddLog(info.OrderId, info.ID, info.Logistic, "确认运单信息",new UserInfoDto(user));
            // #region 修改数据-基础运费跟踪
            // await _statisticMoney.WaybillCarriageBaseEdit(info);
            // #endregion
            //
            // #region 修改数据-最终运费数值
            //
            // await _statisticMoney.WaybillCarriageNumEdit(info);
            //
            // #endregion
            
            #region 修改数据-运单相关金额上报

            await _statisticMoney.ReportWayBillEdit(info);
            #endregion
            
            return true;
        }
}