using ERP.Data;
using ERP.Enums.Logistics;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.Api.Logistics;
using ERP.ViewModels.Logistics;
using ERP.Models.Logistics.ViewModels.Parameter;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using static ERP.Extensions.DbSetExtension;
using LogisticsService = ERP.Services.Caches.Logistics;
using Model = ERP.Models.Logistics.ShipTypes;
using ModelLog = ERP.Models.Logistics.ShipTypesLog;
using UserModel= ERP.Models.Logistics.UserShipType;

namespace ERP.Services.DB.Logistics
{
    public class ShipType
    {       
        private readonly DBContext _MyDbContext;
        private LogisticsService _LogisticsService;
        private readonly ISessionProvider _ISessionProvider;
        public ShipType(DBContext myDbContext, LogisticsService logisticsService, ISessionProvider SessionProvider)
        {
            _MyDbContext = myDbContext;
            _LogisticsService = logisticsService;
            _ISessionProvider = SessionProvider;
        }
        private int CompanyID { get => _ISessionProvider.Session!.GetCompanyID(); }
        private int UserID { get => _ISessionProvider.Session!.GetUserID(); }
        private int GroupID { get => _ISessionProvider.Session!.GetGroupID(); }
        private int OEMID { get => _ISessionProvider.Session!.GetOEMID(); }

        //add shiptype log

        public async Task AddShipTypesLog(string param,Platforms platforms,int count,string data)
        {

            ModelLog modelLog = new ModelLog(param, platforms, count, data, DateTime.Now);
            modelLog.UserID = UserID;
            modelLog.CompanyID = CompanyID;
            modelLog.GroupID = GroupID;
            modelLog.OEMID = OEMID;
            _MyDbContext.ShipTypesLog.Add(modelLog);
           await _MyDbContext.SaveChangesAsync();
        }

        //add shiptypes
        public async Task AddShipTypes(string param, Platforms platforms, List<CommonResult> results)
        {
            List<Model> addList = new List<Model>();
            List<Model> updateList = new List<Model>();
            List<UserModel> userShips = new List<UserModel>();
            foreach (var r in results)
            {
                ulong hashID = Helpers.CreateHashCode(platforms.GetID() + r.Code);
                var data = _MyDbContext.ShipTypes.Where(x => x.ShipTypeNoHash == hashID).FirstOrDefault();
                if (data.IsNotNull())
                {
                    //update
                    data.Name = JsonConvert.SerializeObject(r);
                    data.UpdateLogisticParameter = param;
                    data.UpdatePullTime = DateTime.Now;
                    updateList.Add(data);
                }
                else
                {
                    Model ship = new Model(hashID, platforms, r.Code, JsonConvert.SerializeObject(r), param, DateTime.Now);
                    ship.UserID = UserID;
                    ship.CompanyID = CompanyID;
                    ship.GroupID = GroupID;
                    ship.OEMID = OEMID;
                    addList.Add(ship);
                }
                userShips = AddUserShipTypes(hashID, platforms, r, param);
            }
            _MyDbContext.ShipTypes.AddRange(addList);
            _MyDbContext.ShipTypes.UpdateRange(updateList);
            _MyDbContext.UserShipType.AddRange(userShips);
            await  _MyDbContext.SaveChangesAsync();
        
        }


        //add userShipType
        public  List<UserModel> AddUserShipTypes(ulong hashID,Platforms platforms, CommonResult result,string param)
        {
            List<UserModel> userShips = new List<UserModel>();
            var data = _MyDbContext.UserShipType.Where(x => x.ShipTypeNoHash == hashID 
            && x.LogisticParameter == param).FirstOrDefault();
            if (data.IsNull())
            {
                UserModel ship = new UserModel(hashID, platforms, result.Code, param);
                ship.UserID = UserID;
                ship.CompanyID = CompanyID;
                ship.GroupID = GroupID;
                ship.OEMID = OEMID;
                userShips.Add(ship);
            }
            return userShips;
        }
    }
}
