﻿using ERP.Exceptions.Oem;
using ERP.Models.Setting;
using ERP.Models.View.Setting.Oem;
using ERP.Services.Caches;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using static ERP.Extensions.DbSetExtension;
using Model = ERP.Models.Setting.OEM;
using OemCache = ERP.Services.Caches.OEM;
using ERP.Data;
using ERP.Enums.Identity;
using ERP.Extensions;
using ERP.Services.DB.Statistics;
using ERP.Services.DB.Users;
using Company = ERP.Enums.ID.Company;

namespace ERP.Services.Setting;

public class OemService : BaseService
{
    private readonly DBContext _myDbContext;
    private readonly FileBucketCache _fileBucketCache;
    private readonly CompanyOem _companyOemService;
    private readonly OemCache _oemCache;

    public OemService(DBContext myDbContext, OemCache oemCache, FileBucketCache fileBucketCache,
        CompanyOem companyOemService)
    {
        _myDbContext = myDbContext;
        _oemCache = oemCache;
        _fileBucketCache = fileBucketCache;
        _companyOemService = companyOemService;
    }

    public async Task<List<Model>> Load()
    {
        return await _myDbContext.OEM.Include(m => m.Oss).AsNoTracking().ToListAsync();
    }

    public async Task<List<HashCodeOemModel>> LoadHashCodeOem()
    {
        var list = await _myDbContext.OEM.AsNoTracking().ToListAsync();

        List<HashCodeOemModel> data = new();


        foreach (var item in list)
        {
            var domains = JsonConvert.DeserializeObject<List<string>>(item.Domain);
            if (domains != null)
                foreach (var domain in domains)
                {
                    data.Add(new HashCodeOemModel()
                    {
                        HashCode = Helpers.CreateHashCode(domain),
                        Domain = domain,
                        Id = item.ID
                    });
                }
        }

        return data;
    }

    public async Task<PaginateStruct<Model>> List(string? name = "", int page = 1, int limit = 20)
    {
        return await _myDbContext.OEM.WhenWhere(!name.IsNullOrWhiteSpace(), a => a.Name.Contains(name))
            .OrderByDescending(a => a.ID).ToPaginateAsync(page, limit);
    }

    public async Task<int> EditOem(OemDataDto req)
    {
        //查重域名
        var domains = new List<string>();

        foreach (var item in req.Domains)
        {
            //查重逻辑
            var isRepeat = await _myDbContext.OEM.Where(m => m.Domain.Contains(item.Value))
                .WhenWhere(req.Id != null, m => m.ID != req.Id).FirstOrDefaultAsync();
            if (isRepeat != null)
            {
                throw new RepeatException($"域名重复{item.Value}");
            }

            domains.Add(item.Value);
        }

        //todo 删除弃用的图片

        var now = DateTime.Now;

        if (req.Id == null) //添加
        {
            var data = new Model
            {
                DatabaseId = req.DatabaseId,
                OssId = Convert.ToInt32(req.OssId),
                RedisId = req.RedisId,
                Style = req.Style,
                Detail = req.Detail,
                Title = req.Title,
                Name = req.Name,
                UrlIcon = req.UrlIcon,
                UrlLogo = req.UrlLogo,
                LoginPage = req.LoginPage,
                CreatedAt = now,
                UpdatedAt = now,
                Domain = JsonConvert.SerializeObject(domains)
            };

            var transaction = await _myDbContext.Database.BeginTransactionAsync();
            try
            {
                await _myDbContext.OEM.AddAsync(data);
                await _myDbContext.SaveChangesAsync();
                //新增oem直接增加绑定小米账号
                await _companyOemService.AddAllot(data.ID, Company.XiaoMi.GetID(), $"{User.XIAOMI}");

                #region 新增oem初始化数据

                var statistic = new Statistic(_myDbContext);

                await statistic.CheckOrAdd(data);

                #endregion

                _oemCache.Add(data);
                await transaction.CommitAsync();
            }
            catch (Exception e)
            {
                await transaction.RollbackAsync();
                Console.WriteLine(e.Message);
                throw new Exceptions.Exception(e.Message);
            }
        }
        else //修改
        {
            var info = await _myDbContext.OEM.Where(m => m.ID == req.Id).FirstOrDefaultAsync();
            if (info == null)
            {
                throw new NotFoundException("找不到相关数据");
            }

            info.Title = req.Title;
            info.Name = req.Name;
            info.DatabaseId = req.DatabaseId;
            info.OssId = Convert.ToInt32(req.OssId);
            info.RedisId = req.RedisId;
            info.Style = req.Style;
            info.Detail = req.Detail;
            info.Domain = JsonConvert.SerializeObject(domains);
            info.UrlIcon = req.UrlIcon;
            info.UrlLogo = req.UrlLogo;
            info.LoginPage = req.LoginPage;
            info.UpdatedAt = now;
            _myDbContext.OEM.Update(info);
            _oemCache.Add(info);
        }

        return await _myDbContext.SaveChangesAsync();
    }

    public async Task<int> EditResources(Model oem)
    {
        //$this->delCurrentOemCache();
        //$this->delOemAllCache();
        _myDbContext.OEM.Update(oem);
        await CreateOemFileConfig(oem.Name, oem.Domain);
        return await _myDbContext.SaveChangesAsync();
    }

    /// <summary>
    /// 单/全 Oem信息
    /// </summary>
    /// <param name="name"></param>
    /// <param name="domain"></param>
    /// <returns></returns>
    public async Task<int> CreateOemFileConfig(string? name = null, string? domain = null)
    {
        //$oemAll = $this->getOemAllCache();
        return await Task.FromResult(0);
    }

    private async Task<int> WriteOemFileConfig()
    {
        //$oemAll = $this->getOemAllCache();
        //if (!is_array($oemAll) || !count($oemAll))
        //{
        //    return returnArr(false, 'OEM缓存获取失败');
        //}

        //$oem = arrayCombine($oemAll, 'domain');
        //if (empty($name))
        //{
        //    foreach ($oem as $k => $v) {
        //    $array = explode('.', $k);
        //    $fileName = reset($array);
        //    $rs = $this->writeOemFileConfig($fileName, $v);
        //        if (!$rs['state']) {
        //            return returnArr(false, $rs['msg']);
        //            break;
        //        }
        //    }
        //}
        //else
        //{
        //$currentOem = [];
        //$sign = explode('.', $domain)[0];
        //    if (!isset($oem[$domain]))
        //    {
        //        foreach ($oem as $k => $v) {
        //            if ($sign == explode('.', $k)[0]) {
        //            $currentOem = $oem[$k];
        //                break;
        //            }
        //        }
        //    }
        //    else
        //    {
        //    $currentOem = $oem[$domain];
        //    }
        //    if (!$currentOem) {
        //        return returnArr(false, "未找到{$name}缓存信息");
        //    }
        //$rs = $this->writeOemFileConfig($sign, $currentOem);
        //    if (!$rs['state']) {
        //        return returnArr(false, $rs['msg']);
        //    }
        //}
        //return returnArr(true);
        return await Task.FromResult(0);
    }


    public object Resources()
    {
        //$data = [
        //'oss'   => $this->getResourceOssCache() ?: [],
        //'redis' => $this->getResourceRedisCache() ?: [],
        //'rds'   => $this->getResourceRdsCache() ?: [],
        //];
        var data = new
        {
            Oss = _fileBucketCache.List()
        };

        return data;
    }

    public async Task<Model?> Info(int id)
    {
        return await _myDbContext.OEM.FirstOrDefaultAsync(a => a.ID == id);
    }

    /// <summary>
    /// oem子域名
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<string?> GetDomains(int id)
    {
        return await _myDbContext.OEM.Where(m => m.ID == id).Select(m => m.Domain).FirstOrDefaultAsync();
    }

    public async Task<bool> EditDomain(OemDomainDto req)
    {
        var info = await _myDbContext.OEM.Where(m => m.ID == req.OemId).FirstOrDefaultAsync();
        if (info == null)
        {
            throw new NotFoundException("找不到相关数据");
        }

        var domains = new List<string>();

        foreach (var item in req.Domains)
        {
            //查重逻辑
            var isRepeat = await _myDbContext.OEM.Where(m => m.Domain.Contains(item.Value) && m.ID != req.OemId)
                .FirstOrDefaultAsync();
            if (isRepeat != null)
            {
                throw new RepeatException($"域名重复{item.Value}");
            }

            domains.Add(item.Value);
        }


        info.Domain = JsonConvert.SerializeObject(domains);
        info.UpdatedAt = DateTime.Now;
        await _myDbContext.OEM.SingleUpdateAsync(info);
        _oemCache.Add(info);

        return await _myDbContext.SaveChangesAsync() > 0;
    }
}