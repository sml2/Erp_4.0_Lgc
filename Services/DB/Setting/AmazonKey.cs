using System;
using ERP.Extensions;
using ERP.Models.Setting;
using Microsoft.EntityFrameworkCore;
using Model = ERP.Models.Setting.AmazonKey;
using ModelSP = ERP.Models.Setting.AmazonSPKey;
using ModelShopee = ERP.Models.Setting.ShopeeKey;
using static ERP.Extensions.DbSetExtension;
using ERP.Data;
namespace ERP.Services.DB.Setting;
public class AmazonKey : BaseService
{
    public readonly DBContext _dbContext;

    public AmazonKey(DBContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<IList<Model>> Load()
    {
        return await _dbContext.AmazonKey.AsNoTracking().ToListAsync();
    }
}


public class AmazonSPKey : BaseService
{
    public readonly DBContext _dbContext;

    public AmazonSPKey(DBContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<IList<ModelSP>> Load()
    {
        return await _dbContext.AmazonSPKey.AsNoTracking().ToListAsync();
    }
}


public class ShopeeKey : BaseService
{
    public readonly DBContext _dbContext;

    public ShopeeKey(DBContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<IList<ModelShopee>> Load()
    {
        return await _dbContext.ShopeeKey.AsNoTracking().ToListAsync();
    }
}
