﻿using ERP.Extensions;
using ERP.Models.Setting;
using Microsoft.EntityFrameworkCore;
using static ERP.Extensions.DbSetExtension;
using ERP.Data;
namespace ERP.Services.Setting
{
    public class RedisService : BaseService
    {
        public readonly DBContext _dbContext;

        public RedisService(DBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<ReturnStruct> GetList(string name = "")
        {
            var data =await _dbContext.Redis
           .WhenWhere(string.IsNullOrEmpty(name), a => a.Name.Contains(name))
           .OrderByDescending(a => a.ID)
           .ToPaginateAsync();

            return ReturnArr(true, "", data);
        }

        public async Task<ReturnStruct> AddOrUpdate(Redis data)
        {
            // 检测redis连接性
            //try
            //{
            //$redis = new \Redis();
            //$redis->connect($data['host'], $data['port']);
            //$redis->auth($data['password']);
            //$redis->ping();
            //}
            //catch (\Exception $e) {
            //    return returnArr(false, $e->getMessage());
            //}
            if (data.ID.IsNotNull())
            {
                var info = await _dbContext.AmazonKey.FirstOrDefaultAsync(m => m.ID == data.ID);
                if (info != null)
                {
                    if (info.UpdatedAt != data.UpdatedAt)
                    {
                        return ReturnArr(false, "");
                    }
                    _dbContext.Redis.Update(data);
                }
                else
                {
                    return ReturnArr(false, "");
                }
            }
            else
            {
                await _dbContext.Redis.AddAsync(data);
            }

            await _dbContext.SaveChangesAsync();
            return ReturnArr(true, "");
        }
    }
}
