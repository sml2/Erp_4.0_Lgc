﻿using ERP.Extensions;
using ERP.Models.Setting;
using Microsoft.EntityFrameworkCore;
using static ERP.Extensions.DbSetExtension;
using ERP.Data;

namespace ERP.Services.Setting;

public class RdsService : BaseService
{
    public readonly DBContext _dbContext;

    public RdsService(DBContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<ReturnStruct> GetList(string name = "")
    {
        var Data =await _dbContext.DataBase
       .WhenWhere(string.IsNullOrEmpty(name), a => a.Name.Contains(name))
       .OrderByDescending(a => a.ID)
       .ToPaginateAsync();

        return ReturnArr(true, "", Data);
    }

    public async Task<ReturnStruct> AddOrUpdate(DataBase data)
    {
        // 检测redis连接性
        //try
        //{
        //    new MysqliConnection(

        //        [
        //            'host'   => $data['host'],
        //            'dbname' => $data['database'],
        //            'port'   => $data['port']
        //        ], $data['username'], $data['password'], []
        //);
        //}
        //catch (MysqliException $e) {
        //    return returnArr(false, $e->getMessage());
        //}
        if (data.ID.IsNotNull())
        {
            var Info = await _dbContext.DataBase.FirstOrDefaultAsync(m => m.ID == data.ID);
            if (Info != null)
            {
                if (Info.UpdatedAt != data.UpdatedAt)
                {
                    return ReturnArr(false, "");
                }
                _dbContext.DataBase.Update(data);
            }
            else
            {
                return ReturnArr(false, "");
            }
        }
        else
        {
            await _dbContext.DataBase.AddAsync(data);
        }

        await _dbContext.SaveChangesAsync();
        return ReturnArr(true, "");
    }
}
