﻿using System;
using ERP.Extensions;
using ERP.Models.Setting;
using Microsoft.EntityFrameworkCore;
using Model = ERP.Models.Setting.Nation;
using static ERP.Extensions.DbSetExtension;
using ERP.Data;
namespace ERP.Services.DB.Setting;
public class Nation : BaseService
{
    public readonly DBContext _dbContext;

    public Nation(DBContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<IList<Model>> Load()
    {
        return await _dbContext.Nations.AsNoTracking().ToListAsync();
    }
}

