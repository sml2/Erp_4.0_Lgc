using ERP.Data;
using ERP.Models.Setting;
using Microsoft.EntityFrameworkCore;

namespace ERP.Services.Setting;

public class LanguagesService : BaseService
{
    private readonly DBContext _dbContext;

    public LanguagesService(DBContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<List<Language>> Load()
    {
        return await _dbContext.Language.ToListAsync();
    }
}