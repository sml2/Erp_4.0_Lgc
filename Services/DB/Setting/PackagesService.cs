using Common.Enums.ResourcePackage;
using ERP.Controllers;
using ERP.Data;
using ERP.Interface;
using ERP.Models.DB.Packages;
using ERP.Models.View.Setting;
using Microsoft.EntityFrameworkCore;
using Ryu.Linq;
using Z.EntityFramework.Plus;
using Task = ERP.Models.Task;

namespace ERP.Services.Setting;

public class PackagesService : BaseWithUserService
{
    private readonly List<int> authUserIds = new() { 444446,444447 };

    public PackagesService(DBContext dbContext, ISessionProvider sessionProvider) : base(dbContext, sessionProvider)
    {
    }

    private bool CheckAuth()
    {
        var isHaveAuth = authUserIds.Contains(_userId);
        if (!isHaveAuth)
        {
            throw new Exception("暂无权限");
        }

        return true;
    }


    // private async Task<List<MattingPackageItemModel>> GetMattingList() =>
    //     await _dbContext.MattingPackageItem.ToListAsync();
    //
    // private async Task<List<TranslationPackageItemModel>> GetTranslationList() =>
    //     await _dbContext.TranslationPackageItem.ToListAsync();


    public async Task<PackagesVm> Index(GetPackagesDto req)
    {
        //检测权限
        CheckAuth();
        var package = await GetPackageInfo(req.Type!.Value);
        var data = new PackagesVm(package);
        data.Items = await GetItemList(req.Type!.Value);
        return data;
    }

    private async Task<List<PackageItemsModel>> GetItemList(PackageTypeEnum type)
    {
        return await _dbContext.PackageItems.Where(m => m.Type == type).ToListAsync();
    }
    public async Task<PackageItemsModel?> GetItemInfoById(int id) =>
        await _dbContext.PackageItems.FirstOrDefaultAsync(m => m.ID == id);

    // public async Task<MattingPackageItemModel?> GetMattingInfoById(int id) =>
    //     await _dbContext.MattingPackageItem.FirstOrDefaultAsync(m => m.ID == id);
    //
    // public async Task<TranslationPackageItemModel?> GetTranslationInfoById(int id) =>
    //     await _dbContext.TranslationPackageItem.FirstOrDefaultAsync(m => m.ID == id);

    // public async Task<bool> MattingEdit(EditMattingDto req)
    // {
    //     //检测权限
    //     CheckAuth();
    //     MattingPackageItemModel? info;
    //     if (req.Id.HasValue)
    //     {
    //         info = await GetMattingInfoById(req.Id.Value);
    //         if (info == null)
    //         {
    //             throw new Exception("找不到相关数据#1");
    //         }
    //
    //         info.Times = req.Times;
    //         info.Price = req.Price;
    //         info.State = req.State;
    //         info.UpdatedAt = DateTime.Now;
    //         await _dbContext.SingleUpdateAsync(info);
    //     }
    //     else
    //     {
    //         info = new MattingPackageItemModel(req);
    //         await _dbContext.AddAsync(info);
    //     }
    //
    //     return await _dbContext.SaveChangesAsync() > 0;
    // }

    public async Task<bool> SetItemState(PackageTypeEnum type, int id)
    {
        //检测权限
        CheckAuth();
        int res = await _dbContext.PackageItems.Where(m => m.ID == id && m.Type == type)
            .UpdateAsync(m => new
            {
                State = m.State == PackageSellStateEnum.OffShelf
                    ? PackageSellStateEnum.PutOnTheShelf
                    : PackageSellStateEnum.OffShelf
            });
        // if (type == PackageTypeEnum.Matting)
        // {
        //     res = await _dbContext.MattingPackageItem.Where(m => m.ID == id)
        //         .UpdateAsync(m => new
        //         {
        //             State = m.State == PackageSellStateEnum.OffShelf
        //                 ? PackageSellStateEnum.PutOnTheShelf
        //                 : PackageSellStateEnum.OffShelf
        //         });
        // }
        // else
        // {
        //     res = await _dbContext.TranslationPackageItem.Where(m => m.ID == id)
        //         .UpdateAsync(m => new
        //         {
        //             State = m.State == PackageSellStateEnum.OffShelf
        //                 ? PackageSellStateEnum.PutOnTheShelf
        //                 : PackageSellStateEnum.OffShelf
        //         });
        // }

        return res > 0;
    }


    public async Task<PackagesModel> GetPackageInfo(PackageTypeEnum type)
    {
        //检测权限
        CheckAuth();
        return await _dbContext.Packages.FirstOrDefaultAsync(m => m.Type == type)! ?? new PackagesModel();
    }

    public async Task<bool> UpdatePackage(UpdatePackageDto req)
    {
        //检测权限
        CheckAuth();
        var info = await _dbContext.Packages
            .FirstOrDefaultAsync(m => m.Type == req.Type && m.ID == req.Id);
        if (info == null)
            return false;
        info.Name = req.Name;
        info.Desc = req.Desc;
        await _dbContext.SingleUpdateAsync(info);
        return await _dbContext.SaveChangesAsync() > 0;
    }

    // public async Task<bool> TranslationEdit(EditTranslationDto req)
    // {
    //     //检测权限
    //     CheckAuth();
    //     TranslationPackageItemModel? info;
    //     if (req.Id.HasValue)
    //     {
    //         info = await GetTranslationInfoById(req.Id.Value);
    //         if (info == null)
    //         {
    //             throw new Exception("找不到相关数据#1");
    //         }
    //
    //         info.Num = req.Num;
    //         info.Price = req.Price;
    //         info.State = req.State;
    //         info.UpdatedAt = DateTime.Now;
    //         await _dbContext.SingleUpdateAsync(info);
    //     }
    //     else
    //     {
    //         info = new TranslationPackageItemModel(req);
    //         await _dbContext.AddAsync(info);
    //     }
    //
    //     return await _dbContext.SaveChangesAsync() > 0;
    // }

    public async Task<bool> DeleteItem(PackageTypeEnum type, int id)
    {
        //检测权限
        CheckAuth();
        int res = await _dbContext.PackageItems.Where(m => m.ID == id && m.Type == type)
        .DeleteAsync();
        // if (type == PackageTypeEnum.Matting)
        // {
        //     res = await _dbContext.MattingPackageItem.Where(m => m.ID == id)
        //         .DeleteAsync();
        // }
        // else
        // {
        //     res = await _dbContext.TranslationPackageItem.Where(m => m.ID == id)
        //         .DeleteAsync();
        // }

        return res > 0;
    }

    public async Task<bool> ItemEdit(PackageItemDto req)
    {
        //检测权限
        CheckAuth();
        PackageItemsModel? info;
        if (req.Id.HasValue)
        {
            info = await GetItemInfoById(req.Id.Value);
            if (info == null)
            {
                throw new Exception("找不到相关数据#1");
            }

            info.Num = req.Num;
            info.Price = req.Price;
            info.Validity = req.Validity;
            info.State = req.State;
            info.UpdatedAt = DateTime.Now;
            await _dbContext.SingleUpdateAsync(info);
        }
        else
        {
            info = new PackageItemsModel(req);
            await _dbContext.AddAsync(info);
        }

        return await _dbContext.SaveChangesAsync() > 0;
    }
}