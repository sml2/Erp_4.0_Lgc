﻿using ERP.Extensions;
using ERP.Models.Setting;
using Microsoft.EntityFrameworkCore;
using static ERP.Extensions.DbSetExtension;
using ERP.Data;

namespace ERP.Services.Setting;

public class FileBucketService : BaseService
{
    public readonly DBContext _dbContext;

    public FileBucketService(DBContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task<IList<FileBucket>> Load()
    {
        return await _dbContext.FileBucket.AsNoTracking().ToListAsync();
    }

    public async Task<ReturnStruct> GetList(string nanme = "")
    {
        var data =await _dbContext.FileBucket
       .WhenWhere(string.IsNullOrEmpty(nanme), a => a.Name.Contains(nanme))
       .OrderByDescending(a => a.ID)
       .ToPaginateAsync();

        return ReturnArr(true, "", data);
    }

    public async Task<ReturnStruct> AddOrUpdate(FileBucket data)
    {
        

        if (data.ID.IsNotNull())
        {
            var info = await _dbContext.FileBucket.FirstOrDefaultAsync(m => m.ID == data.ID);
            if (info != null)
            {
                if (info.UpdatedAt != data.UpdatedAt)
                {
                    return ReturnArr(false, "");
                }
                _dbContext.FileBucket.Update(data);
            }
            else
            {
                return ReturnArr(false, "");
            }
        }
        else
        {
            await _dbContext.FileBucket.AddAsync(data);
        }
        // Cache
        // return Cache::instance()->resourceOss()->del();
        // 写入Oem文件
        //try
        //{
        //$resourceOss->save();
        //$this->delResourceOssCache();
        //$oemService = new Oem();
        //    if ($oemService->oemFileOpen) {
        //    $rs = $oemService->createOemFileConfig(null, null);
        //        if (!$rs['state']) {
        //            throw new \Exception($rs['msg']);
        //        }
        //    }
        //    DB::commit();
        //}
        //catch (\Exception $e) {
        //    DB::rollBack();
        //    return returnArr(false, $e->getMessage());
        //}

        await _dbContext.SaveChangesAsync();
        return ReturnArr(true, "");
    }
}
