﻿using System;
using ERP.Extensions;
using ERP.Models.Setting;
using Microsoft.EntityFrameworkCore;
using Model = ERP.Models.Setting.Unit;
using static ERP.Extensions.DbSetExtension;
using ERP.Data;
namespace ERP.Services.DB.Setting;
public class Unit : BaseService
{
    public readonly DBContext _dbContext;

    public Unit(DBContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<IList<Model>> Load()
    {
        return await _dbContext.Units.AsNoTracking().ToListAsync();
    }

    public async Task<bool> Update(int ID, decimal rate, DateTime RefreceTime)
    {
        var unit = await _dbContext.Units.FindAsync(ID);
        if (unit != null)
        {
            unit.Rate = rate;
            unit.RefreceTime = RefreceTime;
            return await _dbContext.SaveChangesAsync() == 1;
        }
        return false;
    }
    public async Task<bool> Update(Model model)
    {
        _dbContext.Update(model);
        var r =  await _dbContext.SaveChangesAsync() == 1;
        _dbContext.Entry(model).State = EntityState.Detached;
        return r;
    }
}

