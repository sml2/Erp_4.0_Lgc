﻿using Microsoft.EntityFrameworkCore;
using Model = ERP.Models.DB.Setting.Menu;
using ERP.Data;
namespace ERP.Services.DB.Setting;

public class Menu
{
    public readonly DBContext _dbContext;

    public Menu(DBContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<IList<Model>> Load()
    {
        return await _dbContext.Menu.AsNoTracking().Where(m => m.ParentId == null).OrderBy(i => i.Sort)
            .Include(m => m.Children.OrderBy(i => i.Sort)).ToListAsync();
    }


}
