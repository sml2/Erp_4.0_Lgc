﻿using ERP.Extensions;
using ERP.Interface;
using ERP.Models.Setting;
using Microsoft.EntityFrameworkCore;
using static ERP.Extensions.DbSetExtension;
using ERP.Data;
namespace ERP.Services.Setting;

public class LanguageUnitService : BaseService
{
    public readonly DBContext _dbContext;
    private readonly ISessionProvider _ISessionProvider;

    public LanguageUnitService(DBContext dbContext, ISessionProvider SessionProvider)
    {
        _ISessionProvider= SessionProvider;
        _dbContext = dbContext;
    }
    private int CompanyID { get => _ISessionProvider.Session!.GetCompanyID(); }
    private int UserID { get => _ISessionProvider.Session!.GetUserID(); }
    private int GroupID { get => _ISessionProvider.Session!.GetGroupID(); }
    private int OEMID { get => _ISessionProvider.Session!.GetOEMID(); }
    public async Task<ReturnStruct> GetList(int languageId, int unitId)
    {
        var data = await _dbContext.LanguageUnit
            .Where(m => m.LanguageId == languageId && m.UnitId == unitId && m.UserId == UserID && m.CompanyId == CompanyID && m.OemId == OEMID)
            .OrderByDescending(m => m.Sort)
            .Select(m => new { m.ID, m.LanguageId, m.UnitId, m.CreatedAt })
            .ToPaginateAsync();

        return ReturnArr(true, "", data);
    }

    public async Task<ReturnStruct> Add(LanguageUnit data)
    {
        await _dbContext.LanguageUnit.AddAsync(data);
        await _dbContext.SaveChangesAsync();
        return ReturnArr(true);
    }

    public async Task<ReturnStruct> Update(LanguageUnit data)
    {
        if (data.ID.IsNotNull())
        {
            var info = await _dbContext.AmazonKey.FirstOrDefaultAsync(m => m.ID == data.ID);
            if (info != null)
            {
                if (info.UpdatedAt != data.UpdatedAt)
                {
                    return ReturnArr(false, "数据不同步");
                }
                _dbContext.LanguageUnit.Update(data);
                await _dbContext.SaveChangesAsync();
                return ReturnArr(true);
            }
            else
            {
                return ReturnArr(false, "未找到相关数据");
            }
        }
        else
        {
            return ReturnArr(false, "未找到相关数据");
        }
        
    }

    public async Task<Boolean> ChcekLanage(int languageId)
    {
        var info = await _dbContext.LanguageUnit
           .Where(m => m.LanguageId == languageId && m.CompanyId == CompanyID && m.OemId == OEMID).FirstOrDefaultAsync();
        return info == null;
    }

    public async Task<ReturnStruct> GetInfoById(int id, int companyId, int oemId)
    {
        var info = await _dbContext.LanguageUnit
           .Where(m => m.ID == id && m.CompanyId == companyId && m.OemId == oemId)
           .Select(m => new { m.ID, m.LanguageId, m.UnitId, m.CreatedAt })
           .FirstOrDefaultAsync();

        if (info == null)
        {
            return ReturnArr(false, "未找到相关数据");
        }
        return ReturnArr(true, "", info);
    }

    public async Task<Boolean> Delete(int id)
    {
        var info = await _dbContext.LanguageUnit.FirstOrDefaultAsync(m => m.ID == id);

        if (info == null)
        {
            return false;
        }
        _dbContext.LanguageUnit.Remove(info);
        await _dbContext.SaveChangesAsync();
        return true;
    }
}
