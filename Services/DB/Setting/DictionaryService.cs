using ERP.Extensions;
using ERP.Models.Setting;
using ERP.Models.Export;
using ERP.ViewModels.Dictionary;
using Microsoft.EntityFrameworkCore;
using ERP.Models.DB;
using ERP.Models.DB.Setting;
using ERP.Services.DB.Users;
using static ERP.Extensions.DbSetExtension;
using static ERP.Models.PlatformData;
using Newtonsoft.Json;
using static ERP.Models.Abstract.BaseModel;
using ERP.Data;
using ERP.Models;
using Task = System.Threading.Tasks.Task;

namespace ERP.Services.Setting
{
    public class DictionaryService : BaseService
    {
        public readonly DBContext _dbContext;

        public DictionaryService(DBContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public async Task<IList<Platform>> Load()
        {
            return await _dbContext.Platform.AsNoTracking().ToListAsync();
        }

        /// <summary>
        /// 亚马逊key列表
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<PaginateStruct<AmazonKey>> GetAamazonKeyList(CommonDto req)
        {
            return await _dbContext.AmazonKey
                .WhenWhere(!string.IsNullOrEmpty(req.Name), a => a.Name.Contains(req.Name!))
                .OrderByDescending(a => a.ID)
                .ToPaginateAsync(page: req.Page, limit: req.Limit);
        }

        /// <summary>
        /// 添加/修改 亚马逊key
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<ReturnStruct> AddOrUpdateAmazonKey(EditAmazonKeyDto data)
        {
            var info = new AmazonKey();
            if (data.ID != null)
            {
                info = await _dbContext.AmazonKey.FirstOrDefaultAsync(m => m.ID == data.ID);
                if (info != null)
                {
                    if (info.UpdatedAt != data.UpdatedAt)
                    {
                        return ReturnArr(false, "数据不同步");
                    }
                }
                else
                {
                    return ReturnArr(false, "找不到相关数据");
                }
            }

            info.Name = data.Name;
            info.Title = data.Title;
            info.CodeNumber = data.CodeNumber;
            info.AccessKey = data.AwsAccessKey;
            info.SecretKey = data.SecretKey;
            info.Remark = data.Remark ?? "";


            _dbContext.AmazonKey.Update(info);
            await _dbContext.SaveChangesAsync();
            // Cache
            // Cache::instance()->amazonKey()->del();
            return ReturnArr(true);
        }

        /// <summary>
        /// 邮箱配置列表
        /// </summary>
        /// <param name="name"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public async Task<PaginateStruct<EmailConfig>> GetEmailConfigList(CommonDto req)
        {
            return await _dbContext.EmailConfig
                .WhenWhere(!string.IsNullOrEmpty(req.Name), a => a.Name.Contains(req.Name!))
                .WhenWhere(req.State != null, a => a.State.Equals(req.State))
                .OrderByDescending(a => a.State)
                .ThenByDescending(a => a.Sort)
                .ToPaginateAsync(page: req.Page, limit: req.Limit);
        }

        /// <summary>
        /// 添加/修改 邮箱配置
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<ReturnStruct> AddOrUpdateEmailConfig(EditEmailConfigDto data)
        {
            EmailConfig? info = new EmailConfig();
            if (data.ID != null)
            {
                info = await _dbContext.EmailConfig.FirstOrDefaultAsync(m => m.ID == data.ID);
                if (info != null)
                {
                    if (info.UpdatedAt != data.UpdatedAt)
                    {
                        return ReturnArr(false, "数据不同步");
                    }
                }
                else
                {
                    return ReturnArr(false, "找不到相关数据");
                }
            }

            info.Name = data.Name;
            info.Domain = data.Name;
            info.Smtp = data.Name;
            info.SmtpPort = data.SmtpPort;
            info.SmtpSSL = data.SmtpSSL;
            info.Pop = data.Pop;
            info.PopPort = data.PopPort;
            info.PopSSL = data.PopSSL;

            _dbContext.EmailConfig.Update(info);
            await _dbContext.SaveChangesAsync();
            // Cache
            // Cache::instance()->emailConfig()->del();
            return ReturnArr(true);
        }

        /// <summary>
        /// 邮箱配置删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ReturnStruct> EmailConfigDel(int id)
        {
            var info = await _dbContext.EmailConfig.FirstOrDefaultAsync(m => m.ID == id);

            if (info == null)
            {
                return ReturnArr(false, "找不到相关数据");
            }

            _dbContext.EmailConfig.Remove(info);
            await _dbContext.SaveChangesAsync();
            return ReturnArr(true);
        }

        /// <summary>
        /// 亚马逊国家列表
        /// </summary>
        /// <param name="name"></param>
        /// <param name="short"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public  PaginateStruct<AmazonNationListVm> GetAmazonNationList(AmazonNationListDto req)
        {
            return _dbContext.PlatformData
                .Where(a => a.Platform == Enums.Platforms.AMAZON)
                .AsEnumerable()
                .WhenWhere(req.Name != null, a => a.GetAmazonData()!.Name.Contains(req.Name!))
                .WhenWhere(req.Short != null, a => a.GetAmazonData()!.Short.Contains(req.Short!))
                .WhenWhere(req.State != null, a => a.GetAmazonData()!.State.Equals(req.State))
                .Select(a => new AmazonNationListVm(a))
                .ToPaginate(limit:req.Limit,page:req.Page);
        }

        /// <summary>
        /// 添加/修改 亚马逊国家
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<bool> AddOrUpdateAmazonNation(EditAmazonNationDto data)
        {
            PlatformData? info = new();
            if (data.ID != null)
            {
                info = await _dbContext.PlatformData.FirstOrDefaultAsync(m => m.ID == data.ID);
                if (info != null)
                {
                    if (info.UpdatedAtTicks != data.UpdatedAtTicks)
                    {
                        throw new Exception("数据不同步");
                    }
                }
                else
                {
                    throw new Exception("找不到相关数据");
                }
            }


            AmazonData amazonData = new();


            // throw new NotImplementedException("需要使用带参构造器");
            amazonData.Name = data.Name??"";
            amazonData.Short = data.Short;
            amazonData.Unit = data.Unit;
            amazonData.Host = data.Mws;
            amazonData.Marketplace = data.Marketplace;
            amazonData.Org = data.Org;
            amazonData.Continent = data.ContinentId;
            amazonData.KeyId = data.KeyId;
            amazonData.State = data.State;


            info.Data = JsonConvert.SerializeObject(amazonData);
            info.NationId = data.NationId;
            info.Platform = Platforms.AMAZON;

            if (data.ID != 0)
            {
                _dbContext.PlatformData.Add(info);
            }
            else
            {
                _dbContext.PlatformData.Update(info);
            }
            
            return await _dbContext.SaveChangesAsync() > 0;
            // Cache
            // Cache::instance()->amazonNation()->del();
        }

        /// <summary>
        /// 平台配置列表
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<PaginateStruct<Platform>> GetPlatformList(CommonDto req)
        {
            return await _dbContext.Platform
                .WhenWhere(!string.IsNullOrEmpty(req.Name), a => a.Name.Contains(req.Name!))
                .WhenWhere(req.State != null, a => a.State.Equals(req.State))
                .OrderBy(a => a.State)
                .ThenBy(a => a.Sort)
                .ToPaginateAsync(page: req.Page, limit: req.Limit);
        }

        /// <summary>
        /// 添加/修改 平台配置
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<ReturnStruct> AddOrUpdatePlatform(EditPlatformDto data)
        {
            var info = new Platform();
            if (data.ID != null)
            {
                info = await _dbContext.Platform.FirstOrDefaultAsync(m => m.ID == data.ID);
                if (info != null)
                {
                    if (info.UpdatedAt != data.UpdatedAt)
                    {
                        return ReturnArr(false, "数据不同步");
                    }
                }
                else
                {
                    return ReturnArr(false, "找不到相关数据");
                }
            }

            info.Name = data.Name;
            info.English = data.English;
            info.Collect = data.Collect;
            info.Export = data.Export;
            info.State = data.State;
            info.Sort = data.Sort;

            _dbContext.Platform.Update(info);
            await _dbContext.SaveChangesAsync();
            // Cache
            // Cache::instance()->collectPlatform()->del();
            // Cache::instance()->exportPlatform()->del();
            return ReturnArr(state: true, msg: data.ID != null ? "修改成功" : "添加成功");
        }

        /// <summary>
        /// 物流方式list
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<PaginateStruct<LogisticsData>> GetLogisticsList(CommonDto req, Caches.Logistics logistics)
        {
            //return await logis
            //    .WhenWhere(!string.IsNullOrEmpty(req.Name), a => a.Name.Contains(req.Name))
            //    .WhenWhere(req.State != null, a => a.State.Equals(req.State))
            //    .OrderBy(a => a.State)
            //    .ThenBy(a => a.Sort)
            //    .ToPaginateAsync(page: req.Page, limit: req.Limit);
            return await Task.FromResult(logistics.List()!.ToPaginate(page: req.Page, limit: req.Limit));
        }

        /// <summary>
        /// 获取系统物流信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ReturnStruct> GetLogisticsInfo(int id, [FromServices] Caches.Logistics logistics)
        {
            //var info = await _dbContext.Logistics.FirstOrDefaultAsync(a => a.ID == id);
            var info = logistics.Get(id);
            if (info == null)
            {
                return await Task.FromResult(ReturnArr(false, "没有数据"));
            }

            return await Task.FromResult(ReturnArr(true, "", info));
        }

        /// <summary>
        /// 添加/修改 系统物流
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        //public async Task<ReturnStruct> AddOrUpdateLogistics(Models.Setting.Logistics data)
        //{
        //    if (data.ID.IsNotNull())
        //    {
        //        var info = await _dbContext.EmailConfig.FirstOrDefaultAsync(m => m.ID == data.ID);
        //        if (info != null)
        //        {
        //            if (info.UpdatedAt != data.UpdatedAt)
        //            {
        //                return ReturnArr(false, "数据不同步");
        //            }

        //            _dbContext.Logistics.Update(data);
        //        }
        //        else
        //        {
        //            return ReturnArr(false, "没有数据");
        //        }
        //    }
        //    else
        //    {
        //        await _dbContext.Logistics.AddAsync(data);
        //    }

        //    await _dbContext.SaveChangesAsync();
        //    // Cache
        //    // Cache::instance()->logistics()->del();
        //    return ReturnArr(true);
        //}

        /// <summary>
        /// 物流方式删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ReturnStruct> LogisticsDel(int id)
        {
            //var info = await _dbContext.Logistics.FirstOrDefaultAsync(m => m.ID == id);

            //if (info == null)
            //{
            //    return ReturnArr(false, "没有数据");
            //}

            //_dbContext.Logistics.Remove(info);
            //await _dbContext.SaveChangesAsync();
            //// Cache 
            //// Cache::instance()->logistics()->del();
            return await Task.FromResult(ReturnArr(true));
        }

        /// <summary>
        /// 亚马逊国家删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ReturnStruct> AmazonNationDel(int id)
        {
            var info = await _dbContext.PlatformData.FirstOrDefaultAsync(m => m.ID == id);

            if (info == null)
            {
                return ReturnArr(false, "没有数据");
            }

            _dbContext.PlatformData.Remove(info);
            await _dbContext.SaveChangesAsync();
            // Cache
            // Cache::instance()->amazonNation()->del();
            return ReturnArr(true);
        }

        /// <summary>
        /// 平台删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ReturnStruct> PlatFormDel(int id)
        {
            var info = await _dbContext.Platform.FirstOrDefaultAsync(m => m.ID == id);

            if (info == null)
            {
                return ReturnArr(false, "没有数据");
            }

            _dbContext.Platform.Remove(info);
            await _dbContext.SaveChangesAsync();
            // Cache 
            // Cache::instance()->collectPlatform()->del();
            // Cache::instance()->exportPlatform()->del();
            return ReturnArr(true);
        }

        /// <summary>
        /// 获取平台模版数据
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<PaginateStruct<Models.Setting.ExportModel>> GetExportList(ExportListDto req)
        {
            return await _dbContext.Export
                .WhenWhere(!string.IsNullOrEmpty(req.Name), a => a.Name.Contains(req.Name!))
                .WhenWhere(req.PlatformId > 0, a => a.PlatformId.Equals(req.PlatformId))
                .WhenWhere(req.State != null, a => a.State.Equals(req.State))
                .WhenWhere(req.Quality != null, a => a.Quality.Equals(req.Quality))
                .Include(m => m.Platforms)
                .OrderBy(a => a.State)
                .ThenBy(a => a.Sort)
                .ToPaginateAsync(page: req.Page, limit: req.Limit);
        }

        /// <summary>
        /// 添加/修改 导出模版
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<ReturnStruct> AddOrUpdateExport(EditExportDto data)
        {
            var info = new Models.Setting.ExportModel();
            if (data.ID != null)
            {
                info = await _dbContext.Export.FirstOrDefaultAsync(m => m.ID == data.ID);
                if (info != null)
                {
                    if (info.UpdatedAtTicks != data.UpdatedAtTicks)
                    {
                        return ReturnArr(false, "数据不同步");
                    }
                }
                else
                {
                    return ReturnArr(false, "没有数据");
                }
            }

            info.Name = data.Name;
            info.Sign = data.Sign;
            info.Filename = data.Filename;
            info.Remark = data.Remark;
            info.PlatformId = data.PlatformId;
            info.State = data.State;
            info.Quality = data.Quality;

            _dbContext.Export.Update(info);
            await _dbContext.SaveChangesAsync();

            return ReturnArr(true);
        }

        /// <summary>
        /// 删除导出模版
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ReturnStruct> ExportDel(int id)
        {
            var info = await _dbContext.Export.FirstOrDefaultAsync(m => m.ID == id);

            if (info == null)
            {
                return ReturnArr(false, "没有数据");
            }

            // 事务
            _dbContext.Export.Remove(info);

            await this.ExportProductDel(id);

            await _dbContext.SaveChangesAsync();
            return ReturnArr(true);
        }

        /// <summary>
        /// 删除导出模版分配用户
        /// </summary>
        /// <param name="exportId"></param>
        /// <returns></returns>
        public async Task<ReturnStruct> ExportProductDel(int exportId)
        {
            var info = await _dbContext.ExportProduct.FirstOrDefaultAsync(m => m.ExportId == exportId);

            if (info == null)
            {
                return ReturnArr(false, "没有数据");
            }

            _dbContext.ExportProduct.Remove(info);

            await _dbContext.SaveChangesAsync();
            return ReturnArr(true);
        }


        /// <summary>
        /// 获取当前用户可导出模版
        /// </summary>
        /// <returns></returns>
        public async Task<Object> getExportProductExcel()
        {
            var exportIds = await _dbContext.ExportProduct
                //.Where(m => m.OemId == 0 && m.CompanyId == 0)
                .Select(m => m.ExportId)
                .ToListAsync();

            return await _dbContext.Export
                .Where(m => m.State == Models.Setting.ExportModel.StateEnum.Open && exportIds.Contains(m.ID))
                .OrderByDescending(m => m.State)
                .ThenByDescending(m => m.Sort)
                .Select(m => new {m.Name, m.Sign, m.Filename})
                .ToListAsync();
        }

        /// <summary>
        /// 获取私人导出模版被分配用户
        /// </summary>
        /// <param name="exportId"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public async Task<PaginateStruct<ExportProduct>> GetAllotList(int exportId, string username)
        {
            return await _dbContext.ExportProduct
                .WhenWhere(!string.IsNullOrEmpty(username), a => a.Username.Contains(username))
                .WhenWhere(exportId > 0, a => a.ExportId.Equals(exportId))
                .ToPaginateAsync();
        }

        public async Task<bool> CheckExist(int exportId, int companyId)
        {
            var info = await _dbContext.ExportProduct
                .WhenWhere(exportId > 0, a => a.ExportId.Equals(exportId))
                .WhenWhere(companyId > 0, a => a.CompanyID.Equals(companyId))
                .FirstOrDefaultAsync();
            return info == null;
        }

        /// <summary>
        /// 添加关联
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<Boolean> AddAllotExportProduct(AddAllotExportProductDto req)
        {
            //$export_id = $request->input('export_id');
            //if (!$export_id) {
            //return error('参数错误');
            //}
            //$platform_id = $request->input('platform_id');
            //if (!$platform_id) {  
            //return error('参数错误');
            //}

            //$username = $request->input('username');
            //if (!$username) {
            //return error('参数错误');
            //}
            ////查询用户信息
            //$user = userService::checkUserNameStatic($username);
            //if (!$user) {
            //return error('用户不存在');
            //}
            //$res = DictionaryService::checkExist($export_id, $user['company_id']);
            //if ($res) {
            //return error('用户所属公司已关联！');
            //}
            //$res = $this->dictionaryService->addAllotExportProduct($export_id, $platform_id, $user);
            //return $res? success() : error();


            var user = await _dbContext.Users.Where(m => m.UserName == req.Username).FirstOrDefaultAsync();

            if (user == null)
            {
                throw new Exceptions.Exception("用户不存在");
            }

            var res = await CheckExist(req.ExportId, user.CompanyID);
            if (!res)
            {
                throw new Exception("用户所属公司已关联");
            }

            var now = DateTime.Now;
            var insertData = new ExportProduct
            {
                ExportId = req.ExportId,
                Username = user.UserName,
                CompanyID = user.CompanyID,
                OEMID = user.OEMID,
                UserID = user.ID,
                GroupID = user.GroupID,
                PlatformId = req.PlatformId,
                CreatedAt = now,
                UpdatedAt = now,
            };

            await _dbContext.ExportProduct.AddAsync(insertData);
            return await _dbContext.SaveChangesAsync() > 0;
        }

        /// <summary>
        /// 移除关联
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ReturnStruct> DelAllotExportProduct(int id)
        {
            var info = await _dbContext.ExportProduct.FirstOrDefaultAsync(m => m.ID == id);

            if (info == null)
            {
                return ReturnArr(false, "没有数据");
            }

            _dbContext.ExportProduct.Remove(info);
            await _dbContext.SaveChangesAsync();
            return ReturnArr(true);
        }
    }
}