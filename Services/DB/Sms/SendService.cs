using ERP.Data;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models;
using ERP.Services.DB.Sms.Ytx;
using Microsoft.EntityFrameworkCore;
using Task = System.Threading.Tasks.Task;

namespace ERP.Services.DB.Sms;

public class SendService : BaseWithUserService
{
    public static Ytx.Config smsConfig { get; set; } = new Ytx.Config();
    public  Rest _rest;

    public SendService(Rest rest, DBContext dbContext, ISessionProvider sessionProvider) : base(dbContext,sessionProvider)
    {
        _rest = rest;
        _rest.SetAccount(smsConfig.AccountSid, smsConfig.AccountToken);
        _rest.SetAppId(smsConfig.AppId);
    }

    public async Task<SmsCodeModel?> GetEndCode(string mobile, SmsCodeTypeEnum smsCodeType = SmsCodeTypeEnum.Register)
    {
        return await _dbContext.SmsCode
            .Where(m => m.Mobile == mobile && m.Type == smsCodeType && !m.Used)
            .OrderByDescending(m => m.SendTime)
            .FirstOrDefaultAsync();
    }

    public async Task AddSms(string mobile,int code,DateTime sendTime,SmsCodeTypeEnum type,int oemId,string? ip)
    {
        var model = new SmsCodeModel(oemId)
        {
            Type = type,
            Code = code.ToString(),
            Mobile = mobile,
            Used = false,
            SendTime = sendTime,
            CreatedAt = sendTime,
            UpdatedAt = sendTime,
            IP = ip,
        };
        
        

        await _dbContext.SmsCode.AddAsync(model);
        await _dbContext.SaveChangesAsync();
    }

    public async Task UpdateSms()
    {
        
    }


    
    // public function updateSms($data, $code, $type = 1)
    // {
    //     $updateData = [
    //     'code'       => $code,
    //     'is_use'     => SmsCodeModel::USE_FALSE,
    //     'send_time'  => $this->time,
    //     'updated_at' => $this->datetime
    //         ];
    //     return SmsCodeModel::query()->where('id', $data['id'])->type($type)->update($updateData);
    // }


    /// <summary>
    /// send message code
    /// </summary>
    /// <param name="mobile"></param>
    /// <param name="data"></param>
    /// <returns></returns>
    public async Task<(bool state, string msg)> SendBindCode(string mobile, Dictionary<string, object> data)
    {
       var result = await _rest.SendTemplateSMS(mobile, data, smsConfig.Templates.Bind);
       if (result is null)
       {
           return (false, "网络异常");
       }

       if (result.StatusCode == 0)
       {
           return (true, "");
       }

       return (false, result.StatusMsg);
    }

    public async Task<bool> CheckIpRequest(string ip)
    {
        var now = DateTime.Now;
        var count =  await _dbContext.SmsCode
            .Where(m => m.IP == ip &&  m.CreatedAt >= now.BeginDayTick() && m.CreatedAt <= now.LastDayTick())
            .CountAsync();
        return count >= 10;
    }
}