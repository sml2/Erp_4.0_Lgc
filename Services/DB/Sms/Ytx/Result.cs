namespace ERP.Services.DB.Sms.Ytx;

public class Result
{
    public int StatusCode { get; set; }
    public string StatusMsg { get; set; }
}