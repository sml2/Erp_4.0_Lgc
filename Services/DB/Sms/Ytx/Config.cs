namespace ERP.Services.DB.Sms.Ytx;

public class Config
{
    public string AccountSid { get; set; }
    public string AccountToken { get; set; }
    public string AppId { get; set; }
    
    public Templates Templates { get; set; }
}

public class Templates
{
    public int Bind { get; set; }
}