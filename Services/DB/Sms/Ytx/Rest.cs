using System.Net.Http.Headers;
using Newtonsoft.Json;
using Sy.Text;

namespace ERP.Services.DB.Sms.Ytx;

public class Rest
{
    private string AccountSid;
    private string AccountToken;
    private string AppId;
    private string ServerIP = "app.cloopen.com";
    private int ServerPort = 8883;
    private string SoftVersion = "2013-12-26";
    private string Batch; //时间戳
    private DateTime Date; //时间戳
    protected readonly ILogger<Rest> _logger;
    protected readonly HttpClient _client;

    public Rest(HttpClient client, ILogger<Rest> logger)
    {
        Date = DateTime.Now;
        Batch = Date.ToString("yyyyMMddHHmmss");
        _client = client;
        _logger = logger;
    }

    /// <summary>
    /// 设置主帐号
    /// </summary>
    /// <param name="accountSid">主帐号</param>
    /// <param name="accountToken">主帐号Token</param>
    public void SetAccount(string accountSid, string accountToken)
    {
        AccountSid = accountSid;
        AccountToken = accountToken;
    }

    /// <summary>
    /// 设置应用ID
    /// </summary>
    /// <param name="appId">应用ID</param>
    public void SetAppId(string appId) => AppId = appId;

    /// <summary>
    /// 主帐号鉴权
    /// </summary>
    /// <returns></returns>
    public Result? AccAuth()
    {
        var result = new Result();
        if (ServerIP == "")
        {
            result.StatusCode = 172004;
            result.StatusMsg = "IP为空";
            return result;
        }

        if (ServerPort <= 0)
        {
            result.StatusCode = 172005;
            result.StatusMsg = "端口错误（小于等于0）";
            return result;
        }

        if (SoftVersion == "")
        {
            result.StatusCode = 172013;
            result.StatusMsg = "版本号为空";
            return result;
        }

        if (AccountSid == "")
        {
            result.StatusCode = 172006;
            result.StatusMsg = "主帐号为空";
            return result;
        }

        if (AccountToken == "")
        {
            result.StatusCode = 172007;
            result.StatusMsg = "主帐号令牌为空";
            return result;
        }

        if (AppId == "")
        {
            result.StatusCode = 172012;
            result.StatusMsg = "应用ID为空";
            return result;
        }

        return null;
    }

    /**
    * 发送模板短信
    * @param to 短信接收彿手机号码集合,用英文逗号分开
    * @param datas 内容数据
    * @param $tempId 模板Id
    */
    public async Task<Result?> SendTemplateSMS(object to, Dictionary<string, object> datas, object tempId)
    {
        //主帐号鉴权信息验证，对必选参数进行判空。
        Result? auth = AccAuth();
        if (!auth.IsNull())
        {
            return auth;
        }

        // 拼接请求包体
        string data = "";
        foreach (var i in datas)
        {
            data = data + "'" + i.Value + "',";
        }
        data = data.Substring(0,data.Length-1);

        string body = "{'to':'" + to + "','templateId':'" + tempId + "','appId':'" + AppId + "','datas':[" + data + "]}";
        //大写的sig参数 
        var sig = (Sy.Security.MD5.Encrypt(AccountSid + AccountToken + Batch)).ToUpper();
        // 生成请求URL        
        var url = $"https://{ServerIP}:{ServerPort}/{SoftVersion}/Accounts/{AccountSid}/SMS/TemplateSMS?sig={sig}";
        // 生成授权：主帐户Id + 英文冒号 + 时间戳。
        var authen = Encode.Base64_Encode(AccountSid + ":" + Batch);

        var request = new HttpRequestMessage(HttpMethod.Post, url);
        request.Headers.Add("Accept", "application/json");
        request.Headers.TryAddWithoutValidation("Authorization", authen);
        // var requestParams = JsonConvert.SerializeObject(body);
        request.Content = new StringContent(body, Encoding.UTF8);
        request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
        var response = await _client.SendAsync(request);

        var logStr = $"\r\n【接口请求日志】" +
                     $"\r\nDate: {Date}" +
                     $"\r\nUrl: {url}" +
                     $"\r\nParams: {body}" +
                     $"\r\nStatusCode: {(int)response.StatusCode}" +
                     $"\r\nResponse: {await response.Content.ReadAsStringAsync()}";
        
        _logger.LogInformation(logStr);

        return JsonConvert.DeserializeObject<Result>(await response.Content.ReadAsStringAsync());
    }
}

