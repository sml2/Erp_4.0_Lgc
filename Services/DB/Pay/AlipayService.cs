using Aop.Api;
using Aop.Api.Request;
using Aop.Api.Response;
using Aop.Api.Util;
using ERP.Data;
using ERP.Models.Finance;
using ERP.Models.View.Setting.Balance;
using Newtonsoft.Json;
using Ryu.Extensions;

namespace ERP.Services.DB.Pay;

public class AlipayService : BaseService
{
    protected string ServiceUrl = "https://openapi.alipay.com/gateway.do";
    protected string AppId = "2021003186631118";
    protected string MerchantPrivateKey = "MIIEpAIBAAKCAQEAul7ZwPxmljFXAjH4irqOAcPHUXReYnMWPqn5We+bfS4fVwa+XfzCpv2Gd8/mC3I77V8b031ziSGMqrngnXgeIxwoKtOGpQ4L5+O8S2P2wQygVjvSi1xgW5eLWTF/Ls7UKpkVMaV+24pDu3dYf10z+Dn+IJcbcJCqTNAYHlm/pgKKRmyCRtHPF2agRUxsCZzZPdbsm3wCcHLVr0GNwG+pGgzXDJ6OFaNTOCaq79i+67EHXbHMJ7jSkxUXaaC8cVo41qqTRtfuHCgh1A/S5VpJDYfYMfMovGLs63/GpAPQGL69sRZfFC5rtHXMENeIsBN4JXGWUdTj7/K0QRmIuyPezwIDAQABAoIBAQCo8mcPzT7qFFw9vzMB/8/PxbygGI+fS5j0wX0rjEw2FGOQrqpP5VxHqTgG0M5HvgyZSEifVfAwddtgwRPfbA5V4aZ//DTo12zvj8EXzszXgCyxP5XDg8BOB3jpKDeVJSWsYA2YbeAsAJEaFjyBaOo9scSvoovbZSkcbOAQU2eqgr7x28x8eQpCm2LyINjkUj7Cc3I1wmPAxz78t7NHzLspyweR0HQGxI+6BXgsXAPGT5ufs6L7z01Qg8ZvvVHawBDGDBqCbHQQhGMk1ItS2zOXDL3e+lhR4RKvzZVO+btmRQNCyuBfSFoVhcui9tIgN2XpCMH0fu1otOEafmvWMTqxAoGBAPvR60K/Uk0neKFK1V1M2NwPjPErb3VfSBm9gpjf4iy90Bdgv8PN2ZYox6OBG2eEcTI6MHlsV7lSxfm6D2R6uWoWpRnq/Y4WL0VASSaUgOdbW5rArixQSqimEuiq6/Z2oYbf3WWNBDijHfnqGid1VlYMLCY2Kdvj+7fbFf8FiA77AoGBAL12z7JxCpmlpuhMNVuJYTQ0SUo12TuEjNFIaAaojWdxJNhMVrqHqaa6ZE1t6VYMgyPZvwjGJFy5EckeY39HXNbBe5Cvh8kz6cOj0M6riPBFuuiHnX8oUykCKPXgjZJK9S0zSX2VBQPklwMkTrgPjAtI6LqficHn4TEOwgEwE1c9AoGAEeD0kBefU0UDJoh2n5ouJ2mfOxw3XKD4MIOo+wuiNkT/ujkvREqgMS4SBoTLGk1n8AX7nRNZIxpCXhmcsCiac3LKJA3wFJ90mbkXuHqAsGxnE5SsiQ7mThUEcKp5+FSx4S6RvH0HmLHUFgNN+hE1oQRaOhhMXoVQQNx4MlQFNfMCgYAPrjWiXxixfrRBpY0E9mnFGE64hZxlAKNm7sctReXuGb8i566F4ZR46BRFFEOri32PCgZ1g3Ce+/Is4wb5Eb7tRzYi02Q5ksJWHsi5UhkTSbIebJrsWzoGutcJAny1Dt8bzpLWge7YU6Lr76VUNnUwuegJCgsssTRCCyajUodHQQKBgQCUQIuMwGBYIe/TGYDI+Rkct/g4tE62nhpFN2i6CrZUY3UrfOSEry6dvk5eg19OZIgdnTLaO59QsRhF8UFXyxOyNl5EJkvPi19esykGjKARbJBggXZnySwO2FC9N1VGijSwNePegQX7cddLtDJjmU5Peuc9pxF6OmvVtfXXZGSlnQ==";
    protected string AlipayPublicKey =
        "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlR42Sm2TUd39BqXas0gfozakhh/a9LpH+nMzugXVnsKlshA4RGeNkv9/rja4rUeITnUHvRrL27ji6NsO/Cr35odlMx402q3Be+H/WKj+BqRVXM91ld/IPsRXcz6CjyGp1hP5ghox8e7lR1EmOUFp7n0Lxjd881n8hHmYFDeNFHJfBelk1MAMers9S1BgtlIEnOq/WvTfzFzPuHORgOviC+bzAOVMWeljiCTeStldOQ4vA8Jfwyhorg6g3DUE/cPyHvt5qU5658gfKm7saJi03+eZhu87zV/1vBhS07gh8XIZEt3SM8pjR81O0dj4Y5KtV0XEOo/9kU6niSc0Ii16dwIDAQAB";
    
    protected string Format = "json";
    protected string Version = "1.0";
    protected string SignType = "RSA2";
    protected string Charset = "UTF-8";
    protected readonly IHttpContextAccessor _httpContextAccessor;

    private readonly DefaultAopClient _client;

    public AlipayService(IHttpContextAccessor httpContextAccessor)
    {
        _httpContextAccessor = httpContextAccessor;
        _client = new DefaultAopClient(ServiceUrl, AppId, MerchantPrivateKey, Format, Version, SignType,
            AlipayPublicKey,
            Charset, false);
    }

    public string Pay(PayBillModel vm)
    {
        var host = _httpContextAccessor.HttpContext!.Request.Host.Value;
        AlipayTradePagePayRequest request = new AlipayTradePagePayRequest();
        //异步接收地址，仅支持http/https，公网可访问
        request.SetNotifyUrl($"http://{host}/api/setting/integralbalance/notify");
        //同步跳转地址，仅支持http/https
        request.SetReturnUrl($"http://{host}/Notify/alipayReturnUrl");
        /******必传参数******/
        Dictionary<string, object> bizContent = new Dictionary<string, object>();
        //商户订单号，商家自定义，保持唯一性
        bizContent.Add("out_trade_no", vm.OutTradeNo!);
        //支付金额，最小值0.01元
        bizContent.Add("total_amount", vm.TotalAmount.To("CNY").ToFixed());
        //订单标题，不可使用特殊符号
        bizContent.Add("subject", vm.Describe ?? "充值");
        //电脑网站支付场景固定传值FAST_INSTANT_TRADE_PAY
        bizContent.Add("product_code", "FAST_INSTANT_TRADE_PAY");

        /******可选参数******/
        //bizContent.Add("time_expire", "2022-08-01 22:00:00");
        //商品明细信息，按需传入
        // List<object> goodsDetails = new List<object>();
        // Dictionary<string, object> goods1 = new Dictionary<string, object>();
        // goods1.Add("goods_id", "goodsNo1");
        // goods1.Add("goods_name", "子商品1");
        // goods1.Add("quantity", 1);
        // goods1.Add("price", 0.01);
        // goodsDetails.Add(goods1);
        // bizContent.Add("goods_detail", goodsDetails);

        //扩展信息，按需传入
        // Dictionary<string, object> extendParams = new Dictionary<string, object>();
        // extendParams.Add("sys_service_provider_id", "2088501624560335");
        // bizContent.Add("extend_params", extendParams);

        string Contentjson = JsonConvert.SerializeObject(bizContent);
        request.BizContent = Contentjson;
        AlipayTradePagePayResponse response = _client.pageExecute(request);

        // Console.WriteLine(response.Body);

        return response.Body;
    }

    public bool Verify(AlipayNotifyDto vm)
    {
        var content = vm.GetType().GetProperties()
            .ToDictionary(q => q.Name, q => q.GetValue(vm).ToString());
        return AlipaySignature.VerifyV1(content, AlipayPublicKey, Charset, vm.sign_type, false);
    }
}