using ERP.Enums.ID;
using ERP.Enums.Orders;
using ERP.Enums.Statistics;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.DB.Orders;
using ERP.Services.DB.Statistics;
using ERP.ViewModels.Statistics;
using Microsoft.EntityFrameworkCore;
using model = ERP.Models.DB.Statistics.Statistic;
using ERP.Data;
using ERP.Models.Abstract;
using ERP.ViewModels;
using NuGet.Protocol;

namespace ERP.Services.Finance;

public class StatisticsService : BaseService
{
    private readonly DBContext _dbContext;
    private readonly ISessionProvider _session;
    private readonly Services.DB.Statistics.Statistic _statistic;

    public StatisticsService(DBContext dbContext, ISessionProvider session, Statistic statistic)
    {
        _dbContext = dbContext;
        _session = session;
        _statistic = statistic;
    }

    /// <summary>
    /// 财务统计-公司财务统计
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    public async Task<FinanceStatisticsVM?> CompanyTotal(CompanyTotalDto req)
    {
        var unit = _session.Session!.GetUnitConfig();
        var companyId = _session?.Session?.GetCompanyID();

        var model = _dbContext.Statistic.Where(m =>
            m.AboutID == companyId && (m.AboutType & Types.Role) == Types.Company);


        if (req.Date?.Count > 0)
        {
            return model
                .Where(m =>
                    (m.AboutType & Types.Effective) == Types.DAY
                    && m.CreatedAt >= req.Date[0]
                    && m.Validity <= req.Date[1].LastDayTick()
                )
                .AsEnumerable()
                .GroupBy(m => m.AboutID)
                .Select(g => new FinanceStatisticsVM(g, unit))
                .FirstOrDefault() ?? new FinanceStatisticsVM();
        }
        
        var data = await model
            .Where(m =>
                (m.AboutType & Types.Effective) == Types.TOTAL
            ).Select(m => new FinanceStatisticsVM(m, unit)).FirstOrDefaultAsync() ?? new FinanceStatisticsVM();


        var company = await _dbContext.Company.FirstAsync(m => m.ID == companyId);
        data.Balance = company.Balance;
        return data;
    }

    /// <summary>
    /// 公司月统计
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    public async Task<DbSetExtension.PaginateStruct<MonthStatisticsVm>> CompanyMonthStatistics(
        CompanyMonthDto req)
    {
        var companyId = _session?.Session?.GetCompanyID();
        var unit = _session.Session!.GetUnitConfig();

        return await _dbContext.Statistic.Where(m =>
                m.AboutID == companyId
                && (m.AboutType & Types.Effective) == Types.MONTH
                && (m.AboutType & Types.Role) == Types.Company
            )
            .WhenWhere(req.Month != null, m => m.Validity >= req.Month && m.Validity <= req.Month.Value.LastMonthTick())
            .OrderByDescending(m => m.CreatedAt)
            .Select(m => new MonthStatisticsVm(m, unit))
            .ToPaginateAsync(page: req.Page, limit: req.Limit);
    }

    /// <summary>
    /// 用户财务统计 / 店铺财务统计
    /// </summary>
    /// <param name="req"></param>
    /// <param name="action"></param>
    /// <returns></returns>
    public async Task<DbSetExtension.PaginateStruct<UserGroupOrStoreStatisticsVm>> UserGroupOrStoreStatistics(
        UserGroupOrStoreDto req, ActionEnum action)
    {
        var unit = _session.Session!.GetUnitConfig();
        var companyId = _session?.Session?.GetCompanyID();
        var oemId = _session?.Session?.GetOEMID();

        var linkData = new List<UserGroupOrStoreVM>();

        Types role = Types.UserGroup;

        if (action == ActionEnum.UserGroup)
        {
            linkData = await _dbContext.UserGroup
                .Where(m => m.CompanyID == companyId)
                .Where(m => m.OEMID == oemId)
                .WhenWhere(req.State != null, m => m.State == req.State)
                .Select(m => new UserGroupOrStoreVM
                {
                    ID = m.ID,
                    Name = m.Name,
                    State = m.State
                })
                .ToListAsync();
            role = Types.UserGroup;
        }
        else if (action == ActionEnum.Store)
        {
            linkData = await _dbContext.StoreRegion
                .Where(m => m.OEMID == oemId &&  m.CompanyID == companyId && !m.IsDel)
                .WhenWhere(req.State != null, m => m.State == req.State)
                .Select(m => new UserGroupOrStoreVM
                {
                    ID = m.ID,
                    Name = m.Name,
                    State = m.State
                }).ToListAsync();
            role = Types.Store;
        }

        var linkIds = linkData.Select(m => m.ID).ToList();

        DbSetExtension.PaginateStruct<UserGroupOrStoreStatisticsVm> list =
            new DbSetExtension.PaginateStruct<UserGroupOrStoreStatisticsVm>();

        if (req.Date?.Count > 0)
        {
            list = _dbContext.Statistic
                .Where(m =>
                    linkIds.Contains(m.AboutID)
                    && (m.AboutType & Types.Role) == role
                    && (m.AboutType & Types.Effective) == Types.DAY
                    && m.CreatedAt >= req.Date[0]
                    && m.Validity <= req.Date[1].LastDayTick()
                )
                .AsEnumerable()
                .GroupBy(m => m.AboutID)
                .Select(g => new UserGroupOrStoreStatisticsVm(g, unit)
                ).ToPaginate(page: req.Page, limit: req.Limit);
        }
        else
        {
            //如果页面展示数量为10的时候则调用更新统计最新数据
            if (req.Limit == 20)
            {
                var needUpdated = await _dbContext.Statistic
                    .Where(m =>
                        m.NeedUpdate
                        && (m.AboutType & Types.MONTH) > 0
                        && (m.AboutType & Types.Role) == role
                        && linkIds.Contains(m.AboutID)
                    )
                    .OrderBy(m => m.Validity)
                    .ToListAsync();

                foreach (var m in needUpdated)
                {
                    await _statistic.Update(m);
                }

                list = await _dbContext.Statistic
                    .Where(m =>
                        linkIds.Contains(m.AboutID)
                        && (m.AboutType & Types.Effective) == Types.TOTAL
                        && (m.AboutType & Types.Role) == role
                    )
                    .OrderByDescending(m => m.CreatedAt)
                    .Select(m => new UserGroupOrStoreStatisticsVm(m, unit))
                    .ToPaginateAsync(page: req.Page, limit: req.Limit);
            }
        }


        list.Items.ForEach(m => m.Ext = linkData.FirstOrDefault(u => u.ID == m.AboutId));
        list.Items.ForEach(m => m.IsDefault = req.Date is null);

        return list;
    }

    public async Task<OrderVM> OrderStatistics(OrderDto req)
    {
        var sortBy = Enum<SortBy>.ToDictionaryForUIByDescription();
        var stateGroup = OrderState.SearchState;

        var rules = new
        {
            AmazonOrderStatisticDetails = true
        };

        var companyId = _session.Session!.GetCompanyID();
        var oemId = _session?.Session?.GetOEMID();
        var unit = "CNY";

        //todo 订单平台
        var list = await _dbContext.Order
            .Include(m => m.Receiver)
            .Where(m => m.CompanyID == companyId)
            .Where(m => m.OEMID == oemId)
            .WhenWhere(req.OrderNo != "", a => a.OrderNo == req.OrderNo)
            .WhenWhere(req.Date != null && req.Date.Count == 2,
                a => a.CreateTime >= req.Date![0] && a.CreateTime < req.Date[1].LastDayTick())
            .WhenWhere(req.StoreId != null && req.StoreId.Count > 0, a => req.StoreId!.Contains(a.StoreId))
            .WhenWhere(req.OrderState != null, a => a.State == req.OrderState)
            .Include(m => m.Store)
            .OrderByDynamic(req.SortBy != null, GetSortBy(req.SortBy)!)
            .Select(m => new OrderStatisticsVm(m, unit))
            .ToPaginateAsync();

        var storeCompany = await _dbContext.StoreRegion.Where(m =>
                m.State == BaseModel.StateEnum.Open && m.CompanyID == companyId && m.IsDel==false)
            .ToListAsync();
        return new OrderVM(list, rules, sortBy, stateGroup, storeCompany.ToDictionary(m => m.ID, m => m));
    }

    private Action<IQueryable<Order>>? GetSortBy(SortBy? sortBy)
    {
        Action<IQueryable<Order>>? res = sortBy switch
        {
            SortBy.CreateAtASC => dbs => dbs.OrderByDescending(m => m.CreateTime),
            SortBy.CreateAtDESC => dbs => dbs.OrderBy(m => m.CreateTime),
            SortBy.PullTimeAsc => dbs => dbs.OrderByDescending(m => m.CreatedAt),
            SortBy.PullTimeDesc => dbs => dbs.OrderBy(m => m.CreatedAt),
            SortBy.LastSendAsc => dbs => dbs.OrderByDescending(m => m.LatestShipTime),
            SortBy.LastSendDesc => dbs => dbs.OrderBy(m => m.LatestShipTime),
            _ => null,
        };
        return res;
    }

    public async Task<object> StoreInfo(int id)
    {
        var query = from r in _dbContext.StoreRegion
            where r.ID == id
            join m in _dbContext.StoreMarket
                on r.UniqueHashCode equals m.UniqueHashCode
            join o in _dbContext.Order on m.Marketplace equals o.MarketPlace into orderTemp
            from order in orderTemp.DefaultIfEmpty()
            select new
            {
                m = new { r.GetSPAmazon()!.SellerID, m.Marketplace, m.NationName },
                order
            };

        var unit = _session.Session!.GetUnitConfig();

        return query.AsEnumerable()
            .GroupBy(m => m.m)
            .Select(m => new
            {
                m.Key.SellerID,
                m.Key.Marketplace,
                m.Key.NationName,
                OrderTotal = m.Sum(g => g.order is not null ? g.order.OrderTotal : 0).ToString(unit),
                OrderTotalFee = m.Sum(g => g.order is not null ? g.order.Fee : 0).ToString(unit),
                OrderTotalRefund = m.Sum(g => g.order is not null ? g.order.Refund : 0).ToString(unit),
                OrderTotalLoss = m.Sum(g => g.order is not null ? g.order.Loss : 0).ToString(unit),
                PurchaseTotal = m.Sum(g => g.order is not null ? g.order.PurchaseFee : 0).ToString(unit),
                PurchaseTotalRefund = m.Sum(g =>  new MoneyRecord(0)).ToString(unit),
                WaybillTotal = m.Sum(g => g.order is not null ? g.order.ShippingMoney : 0).ToString(unit),
                WaybillTotalRefund = m.Sum(g => new MoneyRecord(0)).ToString(unit),
                Profit = m.Sum(g => g.order is not null ? g.order.Profit : 0).ToString(unit)
            })
            .ToList();
    }
}