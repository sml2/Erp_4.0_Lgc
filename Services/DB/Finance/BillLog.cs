﻿using ERP.Data;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.DB.Users;
using static ERP.Models.Finance.BillLog;
using BillLogModel = ERP.Models.Finance.BillLog;

namespace ERP.Services.Finance;

public class BillLog
{
    private readonly ISessionProvider _SessionProvider;
    private readonly DBContext _MyDbContext;
    public BillLog(ISessionProvider ISessionProvider, DBContext myDbContext)
    {
        _MyDbContext=myDbContext;
        _SessionProvider = ISessionProvider;
    }
    private ISession Session { get => _SessionProvider.Session!; }
    public async Task<int> AddBusinessBill(decimal Money, BillLogModel.Types types,string Reason, BillLogModel.Modules modules,int StoreId, Company company, BillLogModel.Belongs Belong, int GroupId)
    {
        if (Belong== Belongs.RIGHT)
        {
            BillLogModel Model = new();
            Model.Money = Money;
            Model.Belong = Belong;
            Model.Type = types;
            Model.Datetime = DateTime.Now;
            Model.Reason = Reason;
            Model.Remark = Reason;
            Model.Audit = Audits.NoReviewRequired;
            Model.UserID = Session.GetUserID();
            Model.Truename = Session.GetTrueName();
            Model.CompanyID = company.ID;
            Model.CreatCompanyId = Session.GetCompanyID();
            Model.ReportCompanyId = company.ReferrerId;
            Model.OperateCompanyName = company.Name;
            Model.Module = modules;
            Model.StoreId = StoreId>0 ? StoreId : 0;
            Model.GroupID= GroupId;
            Model.OEMID = company.OEMID;
            Model.CreatedAt = DateTime.Now;
            await _MyDbContext.BillLog.AddAsync(Model);
            await _MyDbContext.SaveChangesAsync();
            return Model.ID;
        }
        return -1;
    }
    //public function getMoneyAttribute($money)
    //{
    //    return ToConversion($money, $this->getSession()->getUnitConfig());
    //}

    //public function scopeAudit($query, $name, $isAuditList = false)
    //{
    //    if ($isAuditList) {
    //        return $query->where('audit', self::AUDIT_PENDINGREVIEW);
    //    } else
    //    {
    //        if (!empty($name))
    //        {
    //            return $query->where('audit', $name);
    //        }
    //    }
    //}

    //public function scopeTimestampStart($query, $name)
    //{
    //    if (!empty($name))
    //    {
    //        return $query->where('created_at', ">=", $name);
    //    }
    //}

    //public function scopeTimestampEnd($query, $name)
    //{
    //    if (!empty($name))
    //    {
    //        return $query->where('created_at', "<=", $name);
    //    }
    //}

    //public function scopeSortAudit(Builder $query)
    //{
    //    return $query->orderBy('audit', 'asc');

    //}

    //public function scopeSortId(Builder $query)
    //{
    //    return $query->orderBy('id', 'desc');

    //}

    //public function scopeInCompanyId(Builder $query, $company_ids)
    //{
    //    if ($company_ids) {
    //        return $query->whereIn('company_id', $company_ids);
    //    }

    //}
}

