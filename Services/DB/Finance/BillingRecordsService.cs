using ERP.Data;
using ERP.Distribution.Services;
using ERP.Exceptions;
using BillLogModel = ERP.Models.Finance.BillLog;
using ERP.Models.DB.Users;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;
using static ERP.Models.Finance.BillLog;
using Exception = System.Exception;

namespace ERP.Services.Finance;

public class BillingRecordsService
{
    private readonly DBContext _dbContext;

    public BillingRecordsService(DBContext dbContext)
    {
        _dbContext = dbContext;
    }

    private async Task<int> AddBillLog(AddBillLogDto dto, Company company, Belongs belong,
        Modules module, Audits audit)
    {
        Company? currentCompany;
        if (belong == Belongs.SELFUSE)
        {
            currentCompany = company;
        }
        else
        {
            currentCompany = await _dbContext.Company.FirstOrDefaultAsync(m => m.ID == dto.CompanyID);
        }

        if (company == null)
            throw new Exception("找不到相关公司信息");

        var insertModel = new BillLogModel()
        {
            Money = dto.Money,
            Type = dto.Type,
            Datetime = dto.Datetime,
            Reason = dto.Reason,
            Remark = dto.Remark,
            UserID = dto.UserID,
            Truename = dto.Truename,
            CompanyID = company.ID,
            GroupID = dto.GroupID,
            OEMID = company.OEMID,
            ReportCompanyId = company.ReportId,
            CreatCompanyId = currentCompany!.ID,
            OperateCompanyId = currentCompany.ID,
            OperateCompanyName = currentCompany.Name,
            Module = module,
            ModuleId = dto.ModuleId,
            StoreId = dto.StoreId,
            Audit = audit,
        };

        await _dbContext.BillLog.AddAsync(insertModel);
        var state = await _dbContext.SaveChangesAsync() > 0;
        if (!state)
            throw new Exception("财务账单记录失败");
        return insertModel.ID;
    }
    
    /// <summary>
    /// 自用 财务记录 需要审核
    /// </summary>
    /// <param name="dto"></param>
    /// <param name="company"></param>
    /// <param name="module"></param>
    /// <returns></returns>
    public async Task<int> AddBillLogNeedReviewWithSelfUse(AddBillLogDto dto, Company company,
        Modules module) => await AddBillLog(dto, company, Belongs.SELFUSE , module, Audits.PendingReview);
    
    /// <summary>
    /// 对公 财务记录 需要审核
    /// </summary>
    /// <param name="dto"></param>
    /// <param name="company"></param>
    /// <param name="module"></param>
    /// <returns></returns>
    public async Task<int> AddBillLogNeedReviewWithRight(AddBillLogDto dto, Company company,
        Modules module) => await AddBillLog(dto, company, Belongs.RIGHT , module, Audits.PendingReview);
    
    /// <summary>
    /// 对公|自用 财务记录 需要审核
    /// </summary>
    /// <param name="dto"></param>
    /// <param name="company"></param>
    /// <param name="module"></param>
    /// <param name="belong"></param>
    /// <returns></returns>
    public async Task<int> AddBillLogNeedReviewUnKnownBelong(AddBillLogDto dto, Company company, Belongs belong,
        Modules module) => await AddBillLog(dto, company, belong, module, Audits.NoReviewRequired);

    /// <summary>
    /// 自用 财务记录 无需审核
    /// </summary>
    /// <param name="dto"></param>
    /// <param name="company"></param>
    /// <param name="module"></param>
    /// <returns></returns>
    public async Task<int> AddBillLogNoNeedReviewWithSelfUse(AddBillLogDto dto, Company company,
        Modules module) => await AddBillLog(dto, company, Belongs.SELFUSE, module, Audits.NoReviewRequired);
    
    /// <summary>
    /// 对公 财务记录 无需审核
    /// </summary>
    /// <param name="dto"></param>
    /// <param name="company"></param>
    /// <param name="module"></param>
    /// <returns></returns>
    public async Task<int> AddBillLogNoNeedReviewWithRight(AddBillLogDto dto, Company company,
        Modules module) => await AddBillLog(dto, company, Belongs.RIGHT, module, Audits.NoReviewRequired);
    
    /// <summary>
    /// 对公|自用 财务记录 无需审核
    /// </summary>
    /// <param name="dto"></param>
    /// <param name="company"></param>
    /// <param name="module"></param>
    /// <param name="belong"></param>
    /// <returns></returns>
    public async Task<int> AddBillLogNoNeedReviewUnKnownBelong(AddBillLogDto dto, Company company, Belongs belong,
        Modules module) => await AddBillLog(dto, company, belong, module, Audits.NoReviewRequired);

    /// <summary>
    /// 根据类型，数据id获取详情
    /// </summary>
    /// <param name="moduleId"></param>
    /// <param name="module"></param>
    /// <returns></returns>
    public async Task<BillLogModel?> GetInfoWithModuleAndModuleId(int moduleId, Modules module)
    {
        return await _dbContext.BillLog.Where(m => m.ModuleId == moduleId && m.Module == module).FirstOrDefaultAsync();
    }

    /// <summary>
    /// 修改账单金额
    /// </summary>
    /// <param name="moduleId"></param>
    /// <param name="module"></param>
    /// <param name="money"></param>
    /// <returns></returns>
    public async Task<bool> UpdateBillLogMoney(int moduleId, Modules module,Money money)
    {
         await _dbContext.BillLog.Where(m => m.ModuleId == moduleId && m.Module == module).UpdateAsync(m => new ()
         {
             Money = money
         });
         return await _dbContext.SaveChangesAsync() > 0;
    }
}

public class AddBillLogDto
{
    public Money Money { get; set; }
    public Types Type { get; set; }
    public DateTime? Datetime { get; set; }
    public string? Reason { get; set; }
    public string? Remark { get; set; }
    public int UserID { get; set; }
    public int GroupID { get; set; }
    public int CompanyID { get; set; }
    public string? Truename { get; set; }

    public int? ModuleId { get; set; }
    public int? StoreId { get; set; }
}