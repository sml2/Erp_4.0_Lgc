using ERP.Data;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.Finance;
using ERP.ViewModels.Statistics;
using Microsoft.EntityFrameworkCore;

namespace ERP.Services.Finance;

public class FinancialAffairsService
{
    private readonly DBContext _dbContext;

    private readonly ISessionProvider _sessionProvider;

    public FinancialAffairsService(DBContext dbContext, ISessionProvider sessionProvider)
    {
        _dbContext = dbContext;
        _sessionProvider = sessionProvider;
    }
    
    /// <summary>
    /// 新增财务数据 调用此方法需要开启事务
    /// </summary>
    /// <param name="insearData"></param>
    /// <param name="sourceId"></param>
    /// <returns></returns>
    public async Task<FinancialAffairsModel> AddFianceData(FinancialAffairsModel insearData,DataBelongDto belong)
    {
        var currentTime = DateTime.Now;
        var sourceId = insearData.SourceID;
        //是否将已有数据置为失效
        if (sourceId > 0)
        {
            var info = await _dbContext.FinancialAffairs.Where(m => m.ID == sourceId).FirstOrDefaultAsync();

            if (info != null)
            {
                info.FailureTime = currentTime;
                // _dbContext.FinancialAffairs.Update(info);
            }
        }


        //初始化新增数据
        //生效时间为立即
        insearData.EffectiveTime = currentTime;
        //过期时间为无限大
        insearData.FailureTime = DateTime.MaxValue;

        insearData.UserID = belong.UserId;
        insearData.GroupID = belong.GroupId;
        insearData.CompanyID = (int)belong.CompanyId;
        insearData.OEMID = belong.OemId;

        _dbContext.FinancialAffairs.Add(insearData);
        await _dbContext.SaveChangesAsync();
        // 提交事务
        return insearData;
    }

    /// <summary>
    /// 新增财务数据 调用此方法需要开启事务
    /// </summary>
    /// <param name="insearData"></param>
    /// <param name="sourceId"></param>
    /// <returns></returns>
    public async Task<int> AddFianceData(FinancialAffairsModel insearData)
    {
        var currentTime = DateTime.Now;
        var sourceId = insearData.SourceID;
        //是否将已有数据置为失效
        if (sourceId > 0)
        {
            var info = await _dbContext.FinancialAffairs.Where(m => m.ID == sourceId).FirstOrDefaultAsync();

            if (info != null)
            {
                info.FailureTime = currentTime;
                // _dbContext.FinancialAffairs.Update(info);
            }
        }


        //初始化新增数据
        //生效时间为立即
        insearData.EffectiveTime = currentTime;
        //过期时间为无限大
        insearData.FailureTime = DateTime.MaxValue;

        insearData.UserID = _sessionProvider.Session!.GetUserID();
        insearData.GroupID = _sessionProvider.Session!.GetGroupID();
        insearData.CompanyID = _sessionProvider.Session!.GetCompanyID();
        insearData.OEMID = _sessionProvider.Session!.GetOEMID();

        _dbContext.FinancialAffairs.Add(insearData);
        await _dbContext.SaveChangesAsync();
        // 提交事务
        return insearData.ID;
    }

    /// <summary>
    /// 新增财务数据
    /// </summary>
    /// <param name="insearData"></param>
    /// <param name="sourceData"></param>
    /// <returns></returns>
    public async Task<int> AddFianceData(FinancialAffairsModel insearData, FinancialAffairsModel? sourceData)
    {
        await using var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            var currentTime = DateTime.Now;
            if (sourceData != null)
            {
                sourceData.FailureTime = currentTime;
                _dbContext.FinancialAffairs.Update(sourceData);
                insearData.SourceID = sourceData.ID;
            }

            //初始化新增数据
            //生效时间为立即
            insearData.EffectiveTime = currentTime;
            //过期时间为无限大
            insearData.FailureTime = DateTime.MaxValue;

            insearData.UserID = _sessionProvider.Session!.GetUserID();
            insearData.GroupID = _sessionProvider.Session!.GetGroupID();
            insearData.CompanyID = _sessionProvider.Session!.GetCompanyID();
            insearData.OEMID = _sessionProvider.Session!.GetOEMID();

            _dbContext.FinancialAffairs.Add(insearData);
            await _dbContext.SaveChangesAsync();
            // 提交事务
            await transaction.CommitAsync();
            return insearData.ID;
        }

        catch (Exception)
        {
            return 0;
        }
    }

    public Task<FinancialAffairsModel?> GetInfo(int id)
    {
        return _dbContext.FinancialAffairs.Where(m => m.ID == id).FirstOrDefaultAsync();
    }

    public async Task UpdateTableIndexId(int id, int tableId)
    {
        var info = await _dbContext.FinancialAffairs.Where(m => m.ID == id).FirstOrDefaultAsync();

        if (info == null)
        {
            throw new Exception("财务数据丢失");
        }

        info.TableID = tableId;

        _dbContext.FinancialAffairs.Update(info);
        await _dbContext.SaveChangesAsync();
    }
    
    public async Task UpdateTable(List<FinancialAffairsModel> models, int tableId)
    {

        foreach (var item in models)
        {
            item.TableID = tableId;
            _dbContext.FinancialAffairs.Update(item);
        }

        await _dbContext.SaveChangesAsync();
    }

    public async Task UpdateTableIndexId(List<int> id, int tableId)
    {
        var list = await _dbContext.FinancialAffairs.Where(m => id.Contains(m.ID)).ToListAsync();

        if (list.Count != id.Count)
        {
            throw new Exception("财务数据丢失");
        }

        foreach (var item in list)
        {
            item.TableID = tableId;
            _dbContext.FinancialAffairs.Update(item);
        }

        await _dbContext.SaveChangesAsync();
    }
}