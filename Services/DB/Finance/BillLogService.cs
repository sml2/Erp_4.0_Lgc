using ERP.Data;
using ERP.Extensions;
using ERP.Interface;
using BillLogModel = ERP.Models.Finance.BillLog;
using static ERP.Models.Finance.BillLog;
using ERP.Models.Abstract;
using ERP.Models.DB.Users;
using ERP.Services.DB.Users;
using ERP.ViewModels.Finance;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Z.EntityFramework.Plus;
using Role = ERP.Enums.Identity.Role;
using StoreCache = ERP.Services.Caches.Store;
using ERP.Services.Stores;

namespace ERP.Services.Finance;

public class BillLogService : BaseService
{
    private readonly DBContext _dbContext;
    private readonly ISessionProvider _session;

    private readonly CompanyService _companyService;
    private ISession _Session => _session.Session!;
    private int _userId => _Session.GetUserID();
    private int _groupId => _Session.GetGroupID();
    private int _companyId => _Session.GetCompanyID();
    private int _oemId => _Session.GetOEMID();

    private readonly StoreCache _storeCache;

    private readonly StoreService _storeService;

    public BillLogService(ISessionProvider session, DBContext dbContext, CompanyService companyService,
        IMemoryCache cache, StoreCache storeCache, StoreService storeService)
    {
        _session = session;
        _dbContext = dbContext;
        _companyService = companyService;
        _storeCache = storeCache;
        _storeService = storeService;
    }

    // /// <summary>
    // /// 账单列表
    // /// </summary>
    // /// <param name="req"></param>
    // /// <param name="isAuditList"></param>
    // /// <returns></returns>
    // public async Task<AuditIndexVM> Index(BillLogIndexDto req, bool isAuditList = false)
    // {
    //     object rule;
    //     if (isAuditList)
    //     {
    //         rule = new { };
    //     }
    //     else
    //     {
    //         rule = GetRule();
    //     }
    //
    //     DbSetExtension.PaginateStruct<BillLogModel> list = await GetBillLogList(req, isAuditList);
    //     
    //     var company = await _companyService.GetInfoById(_companyId);
    //     
    //     var store = _storeCache.List();
    //
    //     return new AuditIndexVM(list, store, _companyId, new { Money = company is not null?company!.PrivateWallet:0m }, rule,
    //         Enum<Modules>.ToDictionaryForUIByDescription());
    // }

    public async Task<DbSetExtension.PaginateStruct<BillLogModel>> GetBillLogListWithSelfuse(BillLogIndexDto req,
        bool isAuditList = false)
    {        
        return await _dbContext.BillLog
            .Where(m => m.Belong == Belongs.SELFUSE && m.CompanyID==_companyId)                    
            .WhenWhere(req.Type != null, m => m.Type == req.Type)
            .WhenWhere(isAuditList, m => m.Audit == Audits.PendingReview)
            .WhenWhere(!isAuditList && req.Audit > 0, m => m.Audit == req.Audit)
            .WhenWhere(req.Module > 0, m => m.Module == req.Module)
            .WhenWhere(req.ShopId > 0, m => m.StoreId == req.ShopId)
            .WhenWhere(req.Date?.Count >= 2, m => m.CreatedAt >= req.Date![0] && m.CreatedAt <= req.Date![1].LastDayTick())
            .WhenWhere(!string.IsNullOrEmpty(req.OrderNo), m => m.Remark != null  && m.Remark.Contains(req.OrderNo))
            .WhenWhere(!string.IsNullOrEmpty(req.PurchaseNo), m => m.Remark != null  && m.Remark.Contains(req.PurchaseNo))
            .WhenWhere(!string.IsNullOrEmpty(req.WaybillNo), m => m.Remark != null  && m.Remark.Contains(req.WaybillNo))
            .OrderByDescending(m => m.CreatedAt)
            .ThenBy(m => m.Balance)
            .ToPaginateAsync(limit: req.Limit, page: req.Page);
    }

    /// <summary>
    /// 获取权限
    /// </summary>
    /// <returns></returns>
    public object GetRule()
    {
        object rule;
        if (_Session.Is(Role.Employee))
        {
            rule = new
            {
                BillExport = true, //$rule->hasBillExport(),//账单导出
                BillRecharge = true, //$rule->hasBillRecharge(),//充值
                BillDisburse = true, //$rule->hasBillDisburse(),//支出
                BillDetails = true, //$rule->hasBillDetails()//账单详情
            };
        }
        else
        {
            rule = new
            {
                BillExport = true,
                BillRecharge = true,
                BillDisburse = true,
                BillDetails = true,
            };
        }

        return rule;
    }

    public async Task<BillLogModel?> CheckAuditStatus(int id)
    {
        return await _dbContext.BillLog.Where(m => m.ID == id).FirstOrDefaultAsync();
    }

    /// <summary>
    /// 审核账单
    /// </summary>
    /// <param name="id"></param>
    /// <param name="audit"></param>
    /// <param name="company"></param>
    /// <returns></returns>
    public async Task<bool> CheckBill(int id, Audits audit, Company company)
    {
        var info = await _dbContext.BillLog.Where(m => m.ID == id).FirstOrDefaultAsync();

        if (info == null)
        {
            return false;
        }

        info.Audit = audit;
        info.OperateCompanyName = company.Name;
        info.OperateCompanyId = company.ID;
        info.AuditUserId = _userId;
        info.AuditTruename = _session.Session!.GetTrueName();
        if (info.Audit == Audits.Audited)
        {
            info.Balance = info.Belong == Belongs.SELFUSE ? company.PrivateWallet.Money : company.PublicWallet.Money;
        }
        _dbContext.BillLog.Update(info);

        return await _dbContext.SaveChangesAsync() > 0;
    }

    /// <summary>
    /// 自用钱包添加账单
    /// </summary>
    /// <param name="req"></param>
    /// <param name="money"></param>
    /// <param name="company"></param>
    /// <param name="type"></param>
    /// <returns></returns>
    public async Task<int> AddWalletBill(RechargeDto req, decimal money, Company company, Types type = Types.Recharge)
    {
        var insertDate = new BillLogModel()
        {
            Money = money,
            Belong = Belongs.SELFUSE,
            Type = type,
            Datetime = req.Info.ExecDate,
            Reason = req.Info.Reason,
            Remark = req.Info.Remark,
            Audit = company.BillAudit == Company.BillAudits.YES ? Audits.PendingReview : Audits.Audited,
            UserID = _userId,
            Truename = _session.Session?.GetTrueName(),
            CompanyID = _companyId,
            ReportCompanyId = company.ReportId,
            CreatCompanyId = company.ReportId,
            OperateCompanyId = company.ReportId,
            OperateCompanyName = company.Name,
            // Module = Modules.INTERCHANGEABLE,
            Module = req.Info.Modules ?? Modules.INTERCHANGEABLE,
            ImgAll = JsonConvert.SerializeObject(req.Info.Images),
            GroupID = _groupId,
            OEMID = _oemId,
        };

        //审核通过和无需审核的需要填写余额
        if (insertDate.Audit == Audits.Audited || insertDate.Audit == Audits.NoReviewRequired)
        {
            insertDate.Balance = company.PrivateWallet.Money;
        }

        _dbContext.BillLog.Add(insertDate);
        await _dbContext.SaveChangesAsync();
        return insertDate.ID;
    }


    /// <summary>
    /// 退款记录
    /// </summary>
    /// <param name="dto"></param>
    /// <param name="audit"></param>
    /// <param name="type"></param>
    /// <returns></returns>
    // public async Task<int> RefundBillLog(BillLogDto dto, Audits audit = Audits.NoReviewRequired,
    //     Types type = Types.Recharge)
    // {
    //     return await AddBillLog(dto, audit, type, Modules.ORDER);
    // }
    // /// <summary>
    // /// 手续费
    // /// </summary>
    // /// <param name="dto"></param>
    // /// <param name="audit"></param>
    // /// <param name="type"></param>
    // /// <returns></returns>
    // public async Task<int> FeeBillLog(BillLogDto dto, Audits audit = Audits.NoReviewRequired,
    //     Types type = Types.Recharge)
    // {
    //     return await AddBillLog(dto, audit, type, Modules.FEE);
    // }
    // /// <summary>
    // /// 手续费
    // /// </summary>
    // /// <param name="dto"></param>
    // /// <param name="audit"></param>
    // /// <param name="type"></param>
    // /// <returns></returns>
    // public async Task<int> FeeBillLog(BillLogDto dto, Audits audit = Audits.NoReviewRequired,
    //     Types type = Types.Recharge)
    // {
    //     return await AddBillLog(dto, audit, type, Modules.FEE);
    // }

    /// <summary>
    /// 订单损失
    /// </summary>
    /// <param name="dto"></param>
    /// <param name="audit"></param>
    /// <param name="type"></param>
    /// <returns></returns>
    // public async Task<int> LossBillLog(BillLogDto dto, Audits audit = Audits.NoReviewRequired,
    //     Types type = Types.Recharge)
    // {
    //     return await AddBillLog(dto, audit, type, Modules.LOSS);
    // }
    private async Task<int> AddBillLog(BillLogDto dto, Audits audit, Types type, Modules module)
    {
        var remark = "";

        if (dto.OrderNo != null)
        {
            remark = $"订单编号 : {dto.OrderNo}";
        }

        var trunName = _session.Session!.GetTrueName();

        var company = await _dbContext.Company.Where(m => m.ID == _companyId).FirstOrDefaultAsync();
        if (company is null)
        {
            return -1;
        }

        var insertData = new BillLogModel(
            dto.RefundDifference,
            type,
            DateTime.Now,
            remark,
            audit,
            _userId,
            trunName,
            _userId,
            trunName,
            dto.CompanyId,
            _groupId,
            _oemId,
            _companyId,
            _companyId,
            company.Name,
            module,
            dto.StoreId,
            dto.InfoUrl
        );

        await _dbContext.BillLog.AddAsync(insertData);

        await _dbContext.SaveChangesAsync();

        return insertData.ID;
    }
}