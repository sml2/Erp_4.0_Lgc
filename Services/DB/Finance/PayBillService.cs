﻿using System.Security.Claims;
using ERP.Data;
using ERP.Extensions;
using ERP.Models.Finance;
using ERP.Models.View.Setting.Balance;
using ERP.Services.DB.Pay;
using ERP.Services.DB.Users;
using ERP.Services.IntegralBalance;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Exception = System.Exception;

namespace ERP.Services.Finance;

public class PayBillService
{
    private readonly DBContext _dbContext;
    private readonly AlipayService _alipayService;
    private readonly UserNotSessionService _userNotSessionService;
    private readonly BillingRecordsService _billingRecordsService;

    public PayBillService(DBContext dbContext, AlipayService alipayService, UserNotSessionService userNotSessionService,
        BillingRecordsService billingRecordsService)
    {
        _dbContext = dbContext;
        _alipayService = alipayService;
        _userNotSessionService = userNotSessionService;
        _billingRecordsService = billingRecordsService;
    }

    public Task<DbSetExtension.PaginateStruct<Models.Finance.PayBillModel>> GetPaymentBillPage(BillListDto req,
        int? companyId, int? oemId)
    {
        // .WhenWhere(payConfigId.HasValue, p => p.PayConfigId == payConfigId)
        return _dbContext.PayBill
            .WhenWhere(companyId.HasValue, p => p.CompanyID == companyId)
            .WhenWhere(oemId.HasValue, p => p.OEMID == oemId)
            .WhenWhere(req.Type.HasValue, p => p.Type == req.Type)
            .WhenWhere(req.State.HasValue, p => p.State == req.State)
            .WhenWhere(req.Timespan is { Count: 2 },
                p => p.CreatedAt >= req.Timespan![0] && p.CreatedAt <= req.Timespan[1])
            .WhenWhere(!string.IsNullOrEmpty(req.OutTradeNo),
                p => req.OutTradeNo == p.OutTradeNo)
            .OrderByDescending(l => l.ID).ToPaginateAsync();
    }

    public Task<decimal> GetPaymentBillSum(BillListDto req,
        int? companyId, int? oemId)
    {
        // .WhenWhere(payConfigId.HasValue, p => p.PayConfigId == payConfigId)
        return _dbContext.PayBill
            .WhenWhere(companyId.HasValue, p => p.CompanyID == companyId)
            .WhenWhere(oemId.HasValue, p => p.OEMID == oemId)
            .WhenWhere(req.Type.HasValue, p => p.Type == req.Type)
            .WhenWhere(req.State.HasValue, p => p.State == req.State)
            .WhenWhere(req.Timespan is { Count: 2 },
                p => p.CreatedAt >= req.Timespan![0] && p.CreatedAt <= req.Timespan[1])
            .WhenWhere(!string.IsNullOrEmpty(req.OutTradeNo),
                p => req.OutTradeNo == p.OutTradeNo)
            .SumAsync(s => s.TotalAmount);
    }

    /// <summary>
    /// 获取下级钱包在线充值账单记录
    /// </summary>
    /// <param name="payConfigId"></param>
    /// <param name="outTradeNo"></param>
    /// <param name="state"></param>
    /// <param name="type"></param>
    /// <param name="datetime"></param>
    /// <param name="oemId"></param>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public Task<DbSetExtension.PaginateStruct<Models.Finance.PayBillModel>> GetPaymentBillPage(int? payConfigId,
        string? outTradeNo, PayBillStates? state, PayBillTypes? type = PayBillTypes.Wallet
        , DateTime[]? datetime = null, int? oemId = null, int? companyId = null)
    {
        // .WhenWhere(payConfigId.HasValue, p => p.PayConfigId == payConfigId)
        return _dbContext.PayBill
            .WhenWhere(type.HasValue, p => p.Type == type)
            .WhenWhere(state.HasValue, p => p.State == state)
            .WhenWhere(datetime is { Length: 2 },
                p => p.CreatedAt >= datetime![0] && p.CreatedAt <= datetime[1])
            .WhenWhere(!string.IsNullOrEmpty(outTradeNo),
                p => outTradeNo == p.OutTradeNo)
            .OrderByDescending(l => l.ID).ToPaginateAsync();
    }

    public Task<decimal> GetPaymentBillSum(string? outTradeNo, PayBillStates? state, PayBillTypes? type =
            PayBillTypes.Wallet
        , DateTime[]? datetime = null, int? oemId = null, int? companyId = null)
    {
        // .WhenWhere(payConfigId.HasValue, p => p.PayConfigId == payConfigId)
        return _dbContext.PayBill
            .WhenWhere(companyId.HasValue, p => p.CompanyID == companyId)
            .WhenWhere(oemId.HasValue, p => p.OEMID == oemId)
            .WhenWhere(type.HasValue, p => p.Type == type)
            .WhenWhere(state.HasValue, p => p.State == state)
            .WhenWhere(datetime is { Length: 2 },
                p => p.CreatedAt >= datetime![0] && p.CreatedAt <= datetime[1])
            .WhenWhere(!string.IsNullOrEmpty(outTradeNo),
                p => outTradeNo == p.OutTradeNo)
            .SumAsync(s => s.TotalAmountUi);
    }

    /// <summary>
    /// 提交在线充值本地账单记录
    /// </summary>
    /// <param name="user"></param>
    /// <param name="requestData"></param>
    /// <param name="type"></param>
    /// <param name="name"></param>
    /// <returns></returns>
    public async Task<int> InsertBill(Models.DB.Identity.User user, dynamic requestData,
        PayBillTypes type = PayBillTypes.Wallet, string name = "对公钱包充值")
    {
        var insertData = new Models.Finance.PayBillModel()
        {
            Name = name,
            TotalAmount = MoneyFactory.Instance.Create(requestData.total_amount, "CNY"),
            Unit = "CNY",
            OutTradeNo = requestData.out_trade_no,
            OutHashCode = Helpers.CreateHashCode(requestData.out_trade_no),
            Describe = requestData.describe,
            PayConfigId = requestData.pay_config_id,
            UserID = user.Id,
            GroupID = user.GroupID,
            CompanyID = user.CompanyID,
            OEMID = user.OEMID,
            Type = type,
            State = PayBillStates.No,
            CreatedAt = DateTime.Now,
        };
        _dbContext.PayBill.Add(insertData);
        await _dbContext.SaveChangesAsync();
        return insertData.ID;
    }

    public async Task<PayBillModel?> CreateBill(Models.DB.Identity.User user, SubPaymentDto vm, PayBillTypes type,
        string name)
    {
        var model = new PayBillModel(user);
        model.Name = name;
        model.TotalAmount = MoneyFactory.Instance.Create(vm.TotalAmount, "CNY");
        model.Unit = "CNY";
        model.OutTradeNo = vm.OutTradeNo;
        model.OutHashCode = Helpers.CreateHashCode(vm.OutTradeNo);
        model.Describe = vm.Describe;
        model.Type = type;
        _dbContext.PayBill.Add(model);
        if (await _dbContext.SaveChangesAsync() > 0)
        {
            return model;
        }

        return null;
    }


    /// <summary>
    /// 根据本地商户订单号获取账单信息
    /// </summary>
    /// <param name="outTradeNo"></param>
    /// <returns></returns>
    public Task<Models.Finance.PayBillModel?> GetInfoByOutTradeNo(string outTradeNo)
    {
        var hashCode = Helpers.CreateHashCode(outTradeNo);
        return _dbContext.PayBill.Where(p => p.OutHashCode == hashCode).FirstOrDefaultAsync();
    }


    /// <summary>
    /// 更新在线支付账单信息
    /// </summary>
    /// <param name="data"></param>
    /// <param name="info"></param>
    /// <returns></returns>
    public Task<int> UpdateBill(dynamic data, Models.Finance.PayBillModel info)
    {
        info.Name = data.name;
        info.TotalAmount = data.total_amount;
        info.TradeNo = data.trade_no;
        info.PayOther = data;
        info.State = data.state;
        info.AccountDate = DateTime.Now;
        _dbContext.PayBill.Update(info);
        return _dbContext.SaveChangesAsync();
    }

    /// <summary>
    /// 更新余额充值账单信息
    /// </summary>
    /// <param name="data"></param>
    /// <param name="info"></param>
    /// <returns></returns>
    public Task<int> UpdateBalance(AlipayNotifyDto data, Models.Finance.PayBillModel info)
    {
        info.TotalAmount = MoneyFactory.Instance.Create(data.total_amount, "CNY");
        info.TradeNo = data.trade_no;
        info.PayOther = JsonConvert.SerializeObject(data);
        info.State = PayBillStates.Yes;
        info.AccountDate = DateTime.Now;
        info.UpdatedAt = DateTime.Now;
        _dbContext.PayBill.Update(info);
        return _dbContext.SaveChangesAsync();
    }


    /// <summary>
    /// 未登录充值提交订单
    /// </summary>
    /// <param name="requestData"></param>
    /// <param name="user"></param>
    /// <returns></returns>
    public async Task<int> InsertNoSession(dynamic requestData, Models.DB.Identity.User user)
    {
        var insertData = new Models.Finance.PayBillModel()
        {
            Name = "余额充值",
            TotalAmount = MoneyFactory.Instance.Create(requestData.amount, "CNY"),
            Unit = "CNY",
            OutTradeNo = requestData.out_trade_no,
            OutHashCode = Helpers.CreateHashCode(requestData.out_trade_no),
            Describe = "余额充值",
            PayConfigId = 0,
            UserID = user.Id,
            GroupID = user.GroupID,
            CompanyID = user.CompanyID,
            OEMID = user.OEMID,
            Type = PayBillTypes.Wallet,
            State = PayBillStates.No,
            CreatedAt = DateTime.Now,
        };
        _dbContext.PayBill.Add(insertData);
        await _dbContext.SaveChangesAsync();
        return insertData.ID;
    }

    public record CompanyStatisticsVm(int CompanyId, decimal StatisticsTotalAmount);

    /// <summary>
    /// 获取指定公司充值金额
    /// </summary>
    /// <param name="companyIds"></param>
    /// <returns></returns>
    public Task<List<CompanyStatisticsVm>> GetCompanyStatistics(IEnumerable<int> companyIds)
    {
        return _dbContext.PayBill.Where(p =>
                companyIds.Contains(p.CompanyID) && p.Type == PayBillTypes.Balance && p.State == PayBillStates.Yes)
            .GroupBy(p => p.CompanyID)
            .Select(g => new CompanyStatisticsVm(g.Key, g.Sum(p => p.TotalAmount)))
            .ToListAsync();
    }


    public async Task<string> Recharge(RechargeDto req)
    {
        var info = await GetInfoByOutTradeNo(req.out_trade_no);
        if (info == null)
            throw new Exception("找不到相关订单");
        var totalAmount = info.TotalAmount.To("CNY");
        if (totalAmount != req.total_amount)
            throw new Exception("订单金额异常");
        try
        {
            //增加账单
            var company = await _dbContext.Company.FirstOrDefaultAsync(m => m.ID == info.CompanyID);
            if (company == null)
            {
                throw new Exceptions.Exception("找不到相关公司");
            }

            var user = await _dbContext.User.FirstOrDefaultAsync(m => m.Id == info.UserID);
            if (user == null)
            {
                throw new Exceptions.Exception("找不到相关用户");
            }

            //添加账单 自用 需要审核
            await _billingRecordsService.AddBillLogNeedReviewWithSelfUse(new AddBillLogDto()
            {
                Money = info.TotalAmount,
                Type = Models.Finance.BillLog.Types.Recharge,
                Datetime = DateTime.Now,
                Reason = string.Empty,
                Remark = string.Empty,
                UserID = user.ID,
                GroupID = user.GroupID,
                CompanyID = user.CompanyID,
                Truename = user.TrueName,
                ModuleId = info.ID,
            }, company,  Models.Finance.BillLog.Modules.ONLINEPAY);
            return _alipayService.Pay(info);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw new Exception("支付异常");
        }
    }

    public async Task AlipayNotify(AlipayNotifyDto req)
    {
        var bill = await GetInfoByOutTradeNo(req.out_trade_no);
        if (bill == null)
            throw new Exception("订单不存在");

        var company = await _dbContext.Company.FirstOrDefaultAsync(m => m.ID == bill.CompanyID);
        if (company == null)
            throw new Exception("公司不存在");

        var user = await _dbContext.Users.FirstOrDefaultAsync(m => m.Id == bill.UserID);
        if (user == null)
            throw new Exception("用户不存在");

        if (bill.State == PayBillStates.Yes)
            throw new Exception("已支付");
        var verifyResult = _alipayService.Verify(req);

        if (verifyResult)
        {
            var unit = "CNY";
            var totalAmount = MoneyFactory.Instance.Create(req.total_amount, unit);

            if (req.trade_status == "TRADE_SUCCESS")
            {
                var billLogInfo = await _dbContext.BillLog.FirstOrDefaultAsync(m => m.ModuleId == bill.ID);
                var transaction = await _dbContext.Database.BeginTransactionAsync();
                try
                {
                    //更新订单
                    await UpdateBalance(req, bill);
                
                    //修改审核状态
                    if (billLogInfo != null)
                    {
                        billLogInfo.Audit = Models.Finance.BillLog.Audits.NoReviewRequired;
                        await _dbContext.BillLog.SingleUpdateAsync(billLogInfo);
                    }
                    
                    //添加账单
                    await _userNotSessionService.AddBalanceBillWithRecharge(totalAmount, company.ID, company.OEMID,
                        user,
                        company.Balance + totalAmount, Models.IntegralBalance.BalanceBill.Types.RECHARGE,
                        $"在线充值{req.total_amount} {unit}");

                    if (!company.FirstRecharge)
                    {
                        company.FirstRecharge = true;
                        company.FirstRechargeDate = DateTime.Now;
                    }

                    company.Balance += totalAmount;

                    await _dbContext.Company.SingleUpdateAsync(company);
                    await _dbContext.SaveChangesAsync();
                    await transaction.CommitAsync();
                }
                catch (Exception e)
                {
                    await transaction.RollbackAsync();
                    Console.WriteLine(e);
                    throw;
                }
            }
        }
    }
}