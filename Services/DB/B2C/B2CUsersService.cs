using ERP.Data;
using ERP.Enums.Product;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.Abstract;
using ERP.Models.B2C;
using ERP.Services.Caches;
using ERP.Services.DB.Users;
using ERP.Services.Identity;
using ERP.ViewModels.B2C;
using Microsoft.EntityFrameworkCore;

namespace ERP.Services.DB.B2C;

public class B2CUsersService : BaseWithUserService
{
    private readonly UserService _userService;
    private readonly B2CUsersCache _b2CUsersCache;
    private readonly UserManager _userManager;

    public B2CUsersService(DBContext dbContext, ISessionProvider sessionProvider,
        UserService userService, B2CUsersCache b2CUsersCache, UserManager userManager) :
        base(dbContext, sessionProvider)
    {
        _userService = userService;
        _b2CUsersCache = b2CUsersCache;
        _userManager = userManager;
    }

    public async Task<DbSetExtension.PaginateStruct<B2CUserModel>> List(B2CUsersListDto req)
    {
        var a = _b2CUsersCache.List();
        return await _dbContext.B2CUser
            .WhenWhere(!string.IsNullOrEmpty(req.Username), a => a.Username.Contains(req.Username))
            .WhenWhere(req.Status.HasValue, a => a.Status == req.Status)
            .OrderByDescending(a => a.ID)
            .ToPaginateAsync();
    }

    public async Task<DbSetExtension.PaginateStruct<DomainModel>> DomainList(int userId)
    {
        return await _dbContext.Domain
            .Where(m => m.UserID == userId)
            .OrderByDescending(a => a.ID)
            .ToPaginateAsync();
    }


    public async Task<B2CUserModel?> GetInfoByUserId(int userId) =>
        await _dbContext.B2CUser.Where(m => m.UserId == userId).FirstOrDefaultAsync();

    public async Task<(bool State, string Message)> OpenB2C(string username)
    {
        var user = await _userManager.FindByNameAsync(username);
        if (user == null)
            return (false, $"{username},此用户不存在");

        var b2CInfo = await GetInfoByUserId(user.Id);

        if (b2CInfo != null)
            return (false, $"{username},该用户已开通");

        var model = new B2CUserModel(user, _session);

        await _dbContext.B2CUser.AddAsync(model);

        var res = await _dbContext.SaveChangesAsync();

        if (res > 0)
        {
            _b2CUsersCache.Add(model);
            return (true, "开通成功");
        }

        return (false, "开通失败");
    }

    public async Task<List<B2CUserModel>> Load() =>
        await _dbContext.B2CUser.Where(m => m.Status == BaseModel.StateEnum.Open).ToListAsync();

    public async Task<(bool State, string Message)> SetUserState(SetB2CUserStateDto req)
    {
        var info = await _dbContext.B2CUser.FirstOrDefaultAsync(a => a.UserId == req.UserId);
        if (info == null)
            return (false, $"此用户未开通B2C");

        info.Status = req.State;
        info.UpdatedAt = DateTime.Now;
        await _dbContext.B2CUser.SingleUpdateAsync(info);
        var res = await _dbContext.SaveChangesAsync();

        if (res > 0)
        {
            _b2CUsersCache.Remove(info);
            return (true, "操作成功");
        }

        return (false, "操作失败");
    }

    /// <summary>
    /// 统计域名数量 增加域名 删除域名
    /// </summary>
    /// <param name="userId"></param>
    /// <exception cref="Exception"></exception>
    public async Task SetDomainNum(int userId)
    {
        var info = await _dbContext.B2CUser.FirstOrDefaultAsync(a => a.UserId == userId);
        if (info == null)
            throw new Exception("此用户未开通B2C");

        var domains = await _dbContext.Domain.Where(m => m.UserID == userId).ToListAsync();

        info.DomainNum = domains.Count;
        await _dbContext.B2CUser.SingleUpdateAsync(info);
        await _dbContext.SaveChangesAsync();
    }

    /// <summary>
    /// 统计产品库数量 增加商品 删除商品
    /// </summary>
    /// <param name="userId"></param>
    /// <exception cref="Exception"></exception>
    public async Task SetSelectionNum(int userId)
    {
        var info = await _dbContext.B2CUser.FirstOrDefaultAsync(a => a.UserId == userId);
        if (info == null)
            throw new Exception("此用户未开通B2C");
        var products = await _dbContext.Product
            .Where(m => 
                 m.UserID == userId
                 && (m.Type & (long)Types.B2C) == (long)Types.B2C
            )
            .ToListAsync();
        
        info.SelectionNum = products.Count;
        await _dbContext.B2CUser.SingleUpdateAsync(info);
        await _dbContext.SaveChangesAsync();
    }

    public async Task<bool> Delete(int id)
    {
        var info = await _dbContext.B2CUser.FirstOrDefaultAsync(
            m => m.ID == id);

        if (info is null)
            throw new Exception("数据不存在#1");

        _dbContext.B2CUser.Remove(info);
        return await _dbContext.SaveChangesAsync() > 0;
    }
}