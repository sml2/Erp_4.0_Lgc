using ERP.Data;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.B2C;
using ERP.ViewModels.B2C;
using Microsoft.EntityFrameworkCore;
using Org.BouncyCastle.Ocsp;

namespace ERP.Services.DB.B2C;

public class DomainService : BaseWithUserService
{
    private readonly B2CUsersService _b2CUsersService;

    public DomainService(DBContext dbContext, ISessionProvider sessionProvider, B2CUsersService b2CUsersService) : base(
        dbContext, sessionProvider)
    {
        _b2CUsersService = b2CUsersService;
    }

    public async Task AddOrUpdate(DomainUpdateDto vm)
    {
        var transaction = await _dbContext.Database.BeginTransactionAsync();

        try
        {
            var date = DateTime.Now;
            if (vm.Id.HasValue)
            {
                var info = await _dbContext.Domain.FirstOrDefaultAsync(m => m.ID == vm.Id);
                if(info == null)
                    throw new Exception("找不到相关数据#1");
                
                if (info.UpdatedAtTicks != vm.UpdatedAtTicks)
                    throw new Exception("数据不一致#2");

                info.Update(vm.Name,vm.Url,vm.Domain);
                await _dbContext.Domain.SingleUpdateAsync(info);
            }
            else
            {
                var model = vm.To(_session);
                await _dbContext.Domain.AddAsync(model);
                await _dbContext.SaveChangesAsync();
                // await _b2CUsersService.SetDomainNum(_session.GetUserID());
            }
            await _dbContext.SaveChangesAsync();
            await transaction.CommitAsync();
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            Console.WriteLine(e);
            throw new Exception(e.Message);
        }
    }

    public async Task<DbSetExtension.PaginateStruct<DomainModel>> List(DomainListDto req)
    {
        return await _dbContext.Domain
            .Where(m => m.CompanyID == _companyId)
            .WhenWhere(!string.IsNullOrEmpty(req.Domain), a => a.Domain.Contains(req.Domain))
            .OrderByDescending(a => a.ID)
            .ToPaginateAsync(page: req.Page, limit: req.Limit);
    }

    public async Task<DomainModel?> GetInfoByID(int id)
    {
        return await _dbContext.Domain.FirstOrDefaultAsync(m => m.ID == id);
    }

    public async Task<bool> Delete(int id)
    {
        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            var info = await _dbContext.Domain.FirstOrDefaultAsync(
                m => m.ID == id && m.CompanyID == _session.GetCompanyID());

            if (info is null)
                throw new Exception("数据不存在#1");

            _dbContext.Domain.Remove(info);
            await _dbContext.SaveChangesAsync();
            // await _b2CUsersService.SetDomainNum(info.UserID);
            await transaction.CommitAsync();
            return true;
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            Console.WriteLine(e);
            throw new Exception(e.Message);
        }
    }

    public async Task<List<DomainVM>> GetDomainList(int? companyId = null)
    {
        if (!companyId.HasValue)
            companyId = _companyId;

        return await _dbContext.Domain
            .Where(m => m.CompanyID == companyId)
            .Select(m => new DomainVM(m))
            .ToListAsync();
    }
}