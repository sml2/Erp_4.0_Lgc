using ERP.Data;
using ERP.Data.Products.Feature;
using ERP.Enums.Product;
using ERP.Exceptions.Product;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.Product;
using ERP.Models.View.Products.Export;
using ERP.Models.View.Products.Product;
using ERP.Services.Product;
using ERP.ViewModels.B2C;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using ActionEnum = ERP.ViewModels.B2C.ActionEnum;
using ListDto = ERP.ViewModels.B2C.ListDto;

namespace ERP.Services.DB.B2C;

public class SelectionService : BaseWithUserService
{
    private readonly ProductService _productService;
    private readonly B2CUsersService _b2CUsersService;

    public SelectionService(DBContext dbContext, ISessionProvider sessionProvider, ProductService productService, B2CUsersService b2CUsersService) :
        base(dbContext, sessionProvider)
    {
        _productService = productService;
        _b2CUsersService = b2CUsersService;
    }

    public async Task<DbSetExtension.PaginateStruct<ListViewModel>> List(ListDto req)
    {
        var model = (IQueryable<ProductModel>)_dbContext.Product
            .Include(m => m.CategoryModel)
            .Include(m => m.SubCategoryModel);

        // 排除上传中/导出
        model = model.IsOnGoing(false).IsUpload(false);

        //B2C状态的数据
        if (req.Action == ActionEnum.Selection)
        {
            model = model.IsB2C(false);
        }
        else
        {
            model = model.IsB2C(true);

            if (req.Release.HasValue)
            {
                if (req.Release.Value == ReleaseEnum.Release)
                {
                    model = model.IsB2cRelease(true);
                }
                else
                {
                    model = model.IsB2cRelease(false);
                }
            }
        }

        model = model
            .WhenWhere(req.CategoryId.HasValue, m => m.CategoryId == req.CategoryId)
            .WhenWhere(req.SubCategoryId.HasValue, m => m.SubCategoryId == req.SubCategoryId)
            .WhenWhere(!string.IsNullOrEmpty(req.Name), m => m.Title.Contains(req.Name))
            .WhenWhere(!string.IsNullOrEmpty(req.Pid), m => m.PidHashCode == Helpers.CreateHashCode(req.Pid).ToString())
            .WhenWhere(req.PlatformId.HasValue, m => m.PlatformId == req.PlatformId)
            .WhenWhere(req.UpdatedTime != null && req.UpdatedTime.Count == 2,
                m => m.UpdatedAt >= req.UpdatedTime![0] && m.UpdatedAt < req.UpdatedTime[1].LastDayTick());

        if (req.Action == ActionEnum.Goods)
        {
            model = model.Where(m => m.UserID == _userId);
            // .OrderByDescending(m => m.ID)
            // .ToPaginateAsync(page: req.Page, limit: req.Limit);
        }
        else
        {
            var goodId = req.GoodId.HasValue ? req.GoodId : 0;
            if (_companyId == Enums.ID.Company.XiaoMi.GetNumber())
            {
                model = model.Where(m => m.ID > goodId);
            }
            else
            {
                model = model.Where(m => m.ID > goodId && m.CompanyID == _companyId);
            }

            model.WhenWhere(req.User.HasValue, m => m.UserID == req.User);
        }
        

        var data = await  model
            .OrderByDescending(m => m.ID)
            .Select(m =>
                new ListViewModel(m))
            .ToPaginateAsync(page: req.Page, limit: req.Limit);

        return data.Transform<ListViewModel>(m =>
        {
            m.SetLanguagePack(_productService.GetTranslated(m.Language));
            m.SetPrices(_session.GetUnitConfig());
            return m;
        });
    }

    public async Task<List<LanguagePackViewModel>> GetLanguagePack(int id)
    {
        var info = await _productService.GetInfoById(id);
        if (info is null)
        {
            throw new NotFoundException("找不到相关数据#1");
        }

        return await _productService.GetLanguagePack(info);
    }

    public async Task<CopyProductViewModel> ClonePro(CloneDto req)
    {
        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            var copyVm =  await _productService.B2cClone(req.ProductId, req.Lang);
            // await _b2CUsersService.SetSelectionNum(_session.GetUserID());
            await transaction.CommitAsync();
            return copyVm;
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            Console.WriteLine(e);
            throw new Exception(e.Message);
        }
    }

    public async Task<Feature?> ReleaseInfo(int id)
    {
        var info = await _productService.GetInfoById(id);
        if (info is null)
        {
            throw new NotFoundException("找不到相关数据#1");
        }

        
        if (info.Feature != null)
        {
            var features = JsonConvert.DeserializeObject<Features>(info.Feature);
            if (features != null)
            {
                
                return features.Where(m => m.EnumKey == FeatureEnum.B2C).FirstOrDefault();
            }
            return null;
        }

        return null;
    }
}