using System.Net;
using System.Net.Http.Headers;
using ERP.Data;
using ERP.Data.Products;
using ERP.Exceptions;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.DB.Product;
using ERP.Models.Product;
using ERP.ViewModels.B2C.SituneShop;
using Newtonsoft.Json;
using Sy.Text;
using Attribute = ERP.Data.Products.Attribute;
using Exception = System.Exception;

namespace ERP.Services.DB.B2C;

public class SituneShopService : BaseWithUserService
{
    private readonly HttpClient _client;
    private readonly ILogger<SituneShopService> _logger;
    private readonly ConvertProduct _convertProduct;
    private readonly IOemProvider _oemProvider;

    public SituneShopService(DBContext dbContext, ISessionProvider sessionProvider, HttpClient client,
        ILogger<SituneShopService> logger, ConvertProduct convertProduct, IOemProvider oemProvider) : base(dbContext,
        sessionProvider)
    {
        _client = client;
        _logger = logger;
        _convertProduct = convertProduct;
        _oemProvider = oemProvider;
    }

    public async Task<LoginResultDataViewModel> Login()
    {
        //测试服接口
        // var url = "http://sys.situnedigital.com/api/admin/login";
        //正式服接口
        var url = "https://console.situne.shop/api/admin/login";

        var loginParams = new Dictionary<string, string>()
        {
            //测试账号
            // { "username", "test" },
            // { "password", "4d42bf9c18cb04139f918ff0ae68f8a0" }
            //正式服账号弃用
            // { "username", "mijitong" },
            // { "password", "9fa866a648f5ea3b6ac4ca42938dfc51" }
            //正式账号
            { "username", "mijitong" },
            { "password", "39348ffaf56b336d1adfd1bf2649e316" }
        };
        var request = new HttpRequestMessage(HttpMethod.Post, url);
        var postParams = JsonConvert.SerializeObject(loginParams);
        request.Content = new StringContent(postParams, Encoding.UTF8);
        request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
        var response = await _client.SendAsync(request);

        if (response.StatusCode == HttpStatusCode.OK)
        {
            var responseMsg = await response.Content.ReadAsStringAsync();
            var logStr = $"\r\n【SituneShop Login接口请求日志】" +
                         $"\r\nDate: {DateTime.Now}" +
                         $"\r\nUrl: {url}" +
                         $"\r\nParams: {postParams}" +
                         $"\r\nStatusCode: {(int)response.StatusCode}" +
                         $"\r\nResponse: {await response.Content.ReadAsStringAsync()}";
            _logger.LogInformation(logStr);

            var res = JsonConvert.DeserializeObject<LoginResultViewModel>(responseMsg)!;

            if (res.Code != 200)
            {
                throw new Exception("获取授权异常#1");
            }

            if (string.IsNullOrEmpty(res.Data.Token) || string.IsNullOrEmpty(res.Data.TokenHead))
            {
                throw new Exception("获取授权异常#2");
            }

            return res.Data;
        }

        throw new Exception("获取授权异常#3");
    }
    
    

    public ProductsViewModel AssemblyData(ProductModel product, ProductItemModel itemModel, Data.Products.Product itemStruct,
        int domainId)
    {
        var vm = _convertProduct.ToBeProViewModel(itemStruct, product, itemModel, _session.GetUnitConfig(),
            _oemProvider);

        if (itemStruct.Variants.Count <= 0)
        {
            throw new Exception("该产品变体存在问题");
        }

        var showImage = string.Empty;
        var mainImage = product.MainImage != null ? JsonConvert.DeserializeObject<Image>(product.MainImage) : null;
        if (mainImage != null)
        {
            showImage = _convertProduct.GetImageUrl(mainImage, _oemProvider);
        }

        var allPic = itemStruct.Images?.Select(m => _convertProduct.GetImageUrl(m, _oemProvider)).ToList();

        if (allPic == null)
        {
            allPic = new List<string>();
        }

        var title = itemStruct.GetTitle()!;
        var sketch = itemStruct.GetSketches();
        if (sketch == null)
        {
            sketch = new List<string>();
        }
        var variants = itemStruct.Variants;

        List<ProductViewModel> products = new List<ProductViewModel>();
        foreach (var variant in variants)
        {
            //过滤掉失效变体
            if (variant.State != VariantStateEnum.Enable)
            {
                break;
            }
            var currentIds = variant.Ids;
            products.Add(new ProductViewModel()
            {
                //规格
                spData = JsonConvert.SerializeObject(GetAttributeName(itemStruct.Variants.Attributes.items, currentIds)),
                //sku图片
                pic = variant.Images.Main.HasValue && itemStruct.Images != null
                    ? _convertProduct.GetFullImageUrl((int)variant.Images.Main, itemStruct.Images, _oemProvider)
                    : string.Empty,
                //售价  = 人名币 *  103
                priceRmb = variant.Sale.To("CNY"),
                //原价  = 人名币 *  103
                originalPriceRmb = variant.Cost.To("CNY"),
                //成本价 = 人名币 *  103
                purchasePriceRmb = variant.Cost.To("CNY"),
                //重量 单位g (如果kg请自行转换)重量可以设置0
                weight = 2,
                //长(单位 cm)
                length = 0,
                //高(单位 cm)
                height = 0,
                //宽(单位 cm)
                width = 0,
                //库存
                stock = variant.Quantity,
                //展示状态 1：展示
                showStatus = 1,
            });
        }

        return new ProductsViewModel()
        {
            //商品详情轮播图，图片逗号分隔
            pic = string.Join(',',allPic),
            //名称
            name = title,
            //主图
            advPic = showImage,
            skuList = products,
            sellPointIntroduction = string.Join(',',sketch),
            purchaseLink = product.Source
        };
    }
    private List<Dictionary<string, string>> GetAttributeName(Dictionary<int, Attribute> attributes, Dictionary<int, int> ids)
    {
        var append = new List<Dictionary<string, string>>();
        foreach (var i in ids)
        {
            append.Add(new Dictionary<string, string>()
            {
                {"key",attributes[i.Key].Name},
                {"value",attributes[i.Key].Value[i.Value]},
            });
        }

        return append;
    }
    
}