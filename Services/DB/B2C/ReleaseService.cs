using System.Collections;
using System.Net;
using System.Net.Http.Headers;
using Amazon.Runtime.Internal.Transform;
using ERP.ControllersWithoutSession;
using ERP.Data;
using ERP.Data.Products;
using ERP.Data.Products.Feature;
using ERP.Enums;
using ERP.Enums.Product;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.B2C;
using ERP.Models.DB.Product;
using ERP.Models.Product;
using ERP.Models.View.Products.Product;
using ERP.Services.Product;
using ERP.ViewModels.B2C;
using ERP.ViewModels.B2C.SituneShop;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using NuGet.Protocol;
using Ryu.Extensions;
using Sy.Text;
using Attribute = ERP.Data.Products.Attribute;
using ProductsViewModel = ERP.ViewModels.B2C.ProductsViewModel;
using ProductViewModel = ERP.ViewModels.B2C.ProductViewModel;

namespace ERP.Services.DB.B2C;

public class ReleaseService : BaseWithUserService
{
    private readonly ProductService _productService;
    private readonly SituneShopService _situneShopService;
    private readonly ConvertProduct _convertProduct;
    private readonly IOemProvider _oemProvider;
    private readonly HttpClient _client;
    private readonly ILogger<ReleaseService> _logger;

    public ReleaseService(DBContext dbContext, ISessionProvider sessionProvider, ProductService productService,
        ConvertProduct convertProduct, IOemProvider oemProvider, HttpClient client, ILogger<ReleaseService> logger,
        SituneShopService situneShopService) :
        base(dbContext, sessionProvider)
    {
        _productService = productService;
        _convertProduct = convertProduct;
        _oemProvider = oemProvider;
        _client = client;
        _logger = logger;
        _situneShopService = situneShopService;
    }

    protected async Task<ReleaseCheckViewModel> BeforeReleaseCheck(
        ReleaseDto req)
    {
        var domain = await _dbContext.Domain.FirstOrDefaultAsync(m => m.ID == req.DomainId);

        if (domain is null)
            throw new Exception("平台丢失#1");

        var product = await _dbContext.Product
            .Include(m => m.CategoryModel)
            .Include(m => m.SubCategoryModel)
            .Where(m => (m.Type & (long)Types.B2C) == (long)Types.B2C)
            .Where(m => m.UserID == _userId && m.ID == req.Id).FirstOrDefaultAsync();

        if (product is null)
            throw new Exception("该产品不存在#2");


        var item = await _dbContext.ProductItem
            .FirstOrDefaultAsync(m =>
                m.UserID == _userId
                && m.Pid == product.ID
                && m.LanguageId == Languages.Default.GetNumber()
            );
        if (item is null)
            throw new Exception("该产品不存在#3");
        return new ReleaseCheckViewModel(domain, product, item);
    }

    public async Task BeforeManual(ReleaseDto req)
    {
        var result = await BeforeReleaseCheck(req);
        if (result.Domain.IsSitune)
        {
            await SituneShopManual(req, result);
        }
        else
        {
            await Manual(req, result);
        }
    }


    private async Task<bool> Manual(ReleaseDto req,ReleaseCheckViewModel result)
    {
        var domain = result.Domain;
        var product = result.Product;
        var item = result.Item;

        var productStruct = await _productService.GetProductStruct(item.FilePath, product);

        var request = new HttpRequestMessage(HttpMethod.Post, domain.Url);
        // var assemblyData = AssemblyData(product, item, productStruct);
        var assemblyData = new Dictionary<string, string>
        {
            { "Data", JsonConvert.SerializeObject(AssemblyData(product, item, productStruct, domain.ID)) }
        };
        var postParams = JsonConvert.SerializeObject(assemblyData);
        request.Content = new StringContent(postParams, Encoding.UTF8);
        request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
        var response = await _client.SendAsync(request);


        if (response.StatusCode == HttpStatusCode.OK)
        {
            var responseMsg = await response.Content.ReadAsStringAsync();
            var logStr = $"\r\n【接口请求日志】" +
                         $"\r\nDate: {DateTime.Now}" +
                         $"\r\nPlafrom: {domain.Name}" +
                         $"\r\nUrl: {domain.Url}" +
                         $"\r\nParams: {postParams}" +
                         $"\r\nStatusCode: {(int)response.StatusCode}" +
                         $"\r\nResponse: {await response.Content.ReadAsStringAsync()}";
            _logger.LogInformation(logStr);

            var res = JsonConvert.DeserializeObject<ReleaseResViewModel>(responseMsg)!;


            var features = product.Feature != null
                ? JsonConvert.DeserializeObject<Features>(product.Feature)!
                : new Features();

            var b2CFeature = features.FirstOrDefault(m => m.EnumKey == FeatureEnum.B2C);
            if (b2CFeature == null)
            {
                features.Add(FeatureEnum.B2C, new Dictionary<int, object>() { { req.DomainId, res } });
            }
            else
            {
                var temp = JsonConvert.DeserializeObject<Dictionary<int, object>>(b2CFeature.Value.ToString());

                temp[req.DomainId] = res;
                b2CFeature.Value = temp;
                var index = features.IndexOf(b2CFeature);
                features[index] = b2CFeature;
            }

            var transaction = await _dbContext.Database.BeginTransactionAsync();
            try
            {
                if (res.Id.HasValue && res.Id.Value is > 0)
                {
                    product.SetB2CRelease(true);
                    //寻找源产品标记已发布

                    var original = features.FirstOrDefault(m => m.EnumKey == FeatureEnum.B2cOriginalPid);
                    if (original != null)
                    {
                        var originalPro = await _productService.GetInfoById(Convert.ToInt32(original.Value));
                        if (originalPro != null)
                        {
                            originalPro.SetB2CRelease(true);
                            await _dbContext.Product.SingleUpdateAsync(originalPro);
                        }
                    }
                }

                product.Feature = JsonConvert.SerializeObject(features);

                await _dbContext.Product.SingleUpdateAsync(product);
                await _dbContext.SaveChangesAsync();
                await transaction.CommitAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                await transaction.RollbackAsync();
                throw new Exception(e.Message);
            }


            if (res.Id.HasValue && res.Id.Value is > 0)
            {
                return true;
            }

            throw new Exception(!string.IsNullOrEmpty(res.ERR) ? res.ERR : responseMsg);
        }

        throw new Exception("访问失败");
    }

    private ProductsViewModel AssemblyData(ProductModel product, ProductItemModel itemModel,
        Data.Products.Product itemStruct, int domainId)
    {
        var vm = _convertProduct.ToBeProViewModel(itemStruct, product, itemModel, _session.GetUnitConfig(),
            _oemProvider);

        if (itemStruct.Variants.Count <= 0)
        {
            throw new Exception("该产品变体存在问题");
        }


        var showImage = string.Empty;
        var mainImage = product.MainImage != null ? JsonConvert.DeserializeObject<Image>(product.MainImage) : null;
        if (mainImage != null)
        {
            showImage = _convertProduct.GetImageUrl(mainImage, _oemProvider);
        }

        var variants = itemStruct.Variants;

        var title = itemStruct.GetTitle()!;
        var description = itemStruct.GetDescription() != null
            ? _convertProduct.GetProductDesc(itemStruct.GetDescription()!, _oemProvider)
            : string.Empty;
        var bulletPoint = itemStruct.GetSketches();
        var searchTerms = itemStruct.GetKeyword();

        List<ProductViewModel> products = new List<ProductViewModel>();

        var tagReplace = GetNeedReplaceTag(itemStruct.Variants.Attributes.items);

        foreach (var variant in variants)
        {
            // var currentIds = variant.Ids;
            var attributeName = _convertProduct.GetAttributeName(itemStruct.Variants.Attributes.items, variant.Ids);
            var currentIds = ResetAttributeTag(tagReplace, variant.Ids);

            products.Add(new ProductViewModel()
            {
                Name = _convertProduct.MakeVariantTitle(variant.GetTitle() ?? title, attributeName),
                Description = variant.GetDescription() != null
                    ? _convertProduct.GetProductDesc(variant.GetDescription()!, _oemProvider)
                    : description,
                BulletPoint = variant.GetSketches() ?? bulletPoint,
                SearchTerms = variant.GetKeyword() ?? searchTerms,
                Values = currentIds,
                Sku = variants.IndexOf(variant),
                MainImage = variant.Images.Main.HasValue && itemStruct.Images != null
                    ? _convertProduct.GetFullImageUrl((int)variant.Images.Main, itemStruct.Images, _oemProvider)
                    : string.Empty,
                Images = variant.Images.Affiliate != null && itemStruct.Images != null
                    ? _convertProduct.GetFullImageUrl(variant.Images.Affiliate, itemStruct.Images, _oemProvider)
                    : new List<string>(),
                Quantity = variant.Quantity,
                Price = variant.Sale.To(_session.GetUnitConfig()),
                Cost = variant.Cost.To(_session.GetUnitConfig()),
                ProduceCode = variant.Code,
                Fields = null,
                SID = variant.Sid,
            });
        }

        ReleaseResViewModel? b2c = new();

        if (product.Feature != null)
        {
            var features = JsonConvert.DeserializeObject<Features>(product.Feature);
            var b2CFeature = features?.FirstOrDefault(m => m.EnumKey == FeatureEnum.B2C);
            if (b2CFeature != null)
            {
                var tempB2c =
                    JsonConvert.DeserializeObject<Dictionary<string, ReleaseResViewModel>>(b2CFeature.Value.ToString());
                Dictionary<string, ReleaseResViewModel?> dicB2c = new();
                foreach (var (key, value) in tempB2c)
                {
                    dicB2c.Add(key, value);
                }

                if (dicB2c.Keys.Contains(domainId.ToString()))
                {
                    b2c = dicB2c[domainId.ToString()];
                }
            }
        }


        return new ProductsViewModel()
        {
            B2C = JsonConvert.SerializeObject(b2c),
            PID = product.ID,
            Name = product.Title,
            Source = product.Source,
            Sku = string.Empty,
            Quantity = product.Quantity,
            ShowImg = showImage,
            Price = new PricesViewModel(product.Prices is not null
                ? JsonConvert.DeserializeObject<Prices>(product.Prices)!
                : new Prices(), _session.GetUnitConfig()),
            Description = description,
            BulletPoint = bulletPoint,
            SearchTerms = searchTerms,
            Category = product.CategoryModel?.Name,
            SubCategory = product.SubCategoryModel?.Name,
            Attribute = _convertProduct.RecombinationAttr(itemStruct.Variants.Attributes.items),
            Products = products,
            ProductID = product.Pid,
            Language = Languages.Default.GetDescriptionByKey("Code"),
        };
    }

    private async Task<bool> SituneShopManual(ReleaseDto req,ReleaseCheckViewModel result)
    {
        var domain = result.Domain;
        var product = result.Product;
        var item = result.Item;

        var productStruct = await _productService.GetProductStruct(item.FilePath, product);

        var token = await _situneShopService.Login();

        var request = new HttpRequestMessage(HttpMethod.Post, domain.Url);
        var postParams =
            JsonConvert.SerializeObject(_situneShopService.AssemblyData(product, item, productStruct, domain.ID));
        request.Content = new StringContent(postParams, Encoding.UTF8);
        request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
        request.Headers.Add("Authorization", $"{token.TokenHead}{token.Token}");
        // request.Content.Headers.Add("Authorization", $"{token.TokenHead}{token.Token}");
        var response = await _client.SendAsync(request);
        if (response.StatusCode == HttpStatusCode.OK)
        {
            var responseMsg = await response.Content.ReadAsStringAsync();
            var logStr = $"\r\n【SituneShop 接口请求日志】" +
                         $"\r\nDate: {DateTime.Now}" +
                         $"\r\nPlafrom: {domain.Name}" +
                         $"\r\nUrl: {domain.Url}" +
                         $"\r\nParams: {postParams}" +
                         $"\r\nStatusCode: {(int)response.StatusCode}" +
                         $"\r\nResponse: {await response.Content.ReadAsStringAsync()}";
            _logger.LogInformation(logStr);

            var res = JsonConvert.DeserializeObject<ReleaseResViewModel>(responseMsg)!;
            if (res.Code.HasValue && res.Code.Value == 200 && !string.IsNullOrEmpty(res.Message))
            {
                product.SetB2CRelease(true);
                await _dbContext.Product.SingleUpdateAsync(product);
                return await _dbContext.SaveChangesAsync() > 0;
            }

            throw new Exception(res.Message);
        }

        return false;
    }

    private Dictionary<int, int> GetNeedReplaceTag(Dictionary<int, Attribute> attributes)
    {
        var attrs = attributes.Select(m => new AttributeViewMode(m.Value, m.Key)).ToList();
        var tagReplace = new Dictionary<int, int>();
        foreach (var item in attrs)
        {
            var index = attrs.IndexOf(item);
            if (item.Tag != index)
            {
                tagReplace.Add(item.Tag, index);
            }
        }

        return tagReplace;
    }

    private Dictionary<int, int> ResetAttributeTag(Dictionary<int, int> tagReplace, Dictionary<int, int> ids)
    {
        if (tagReplace.Count <= 0)
        {
            return ids;
        }

        var newIds = new Dictionary<int, int>();
        foreach (var item in ids)
        {
            if (tagReplace.Keys.Contains(item.Key))
            {
                newIds.Add(tagReplace[item.Key], item.Value);
            }
            else
            {
                newIds.Add(item.Key, item.Value);
            }
        }

        return newIds;
    }
}