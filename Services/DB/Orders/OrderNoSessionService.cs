using ERP.Data;
using ERP.Enums.Orders;
using ERP.Models.View.Order.AmazonOrder;
using ERP.Services.DB.Logistics;
using ERP.Services.DB.Statistics;
using ERP.Services.Statistics;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Model = ERP.Models.DB.Orders.Order;
using AddressModel = ERP.Models.DB.Orders.Address;


namespace ERP.Services.DB.Orders;

public class OrderNoSessionService : BaseService
{
    protected readonly DBContext _dbContext;
    protected readonly WaybillNoSessionService _waybillNoSessionService;
    protected readonly ILogger<OrderNoSessionService> _logger;
    protected readonly StatisticMoney _statisticMoney;
    protected readonly Statistic _statisticService;

    public OrderNoSessionService(DBContext dbContext, ILogger<OrderNoSessionService> logger, WaybillNoSessionService waybillNoSessionService, Statistic statisticService)
    {
        _dbContext = dbContext;
        _logger = logger;
        _waybillNoSessionService = waybillNoSessionService;
        _statisticService = statisticService;
        _statisticMoney = new StatisticMoney(_statisticService);
    }

    /// <summary>
   /// 单个订单的运费更新
   /// </summary>
   /// <param name="mergeID">合并订单id</param>
   /// <param name="OrderProgress">手动设置订单进度</param>
   /// <param name="orderid">普通订单id</param>
   /// <param name="cancleWaybillID">被取消的运单id</param>
   /// <returns></returns>
    public async Task UpdateMergeOrderWaybill(int? mergeID, List<ProgressValue>? OrderProgress , int? orderid=0, int? cancleWaybillID = 0)
    {        
        try
        {
            List<Model> orders = new List<Model>();
            if (mergeID > 0)    //属于合并订单，取出涉及的合并订单单子
            {
                var result = await GetOrdersByMIdsAsync(mergeID);
                if (result is null || result.Count<=0)
                {
                    _logger.LogError($"UpdateMergeOrderWaybill 合并订单为空，merferid:{mergeID}");                    
                }
                orders.AddRange(result);
            }
            else if (orderid > 0)   //属于普通订单，取出这个普通订单即可
            {
                var orderInfo = await GetOrderByIdAsync(orderid);
                if (orderInfo is null)
                {
                    _logger.LogError($"UpdateMergeOrderWaybill 订单信息没有找到，order:{orderid}");                   
                }
                orders.Add(orderInfo);
            }
            else
            {
                _logger.LogError($"UpdateMergeOrderWaybill 参数错误");
            }

            if (cancleWaybillID <= 0)//取消运单的时候，这里的订单进度不必修改
            {
                //更新订单进度,优先级：手动设置>非未处理状态（不修改）>只有未处理状态，才自动修改订单进度
                await BatchEditOrderProgress(orders, OrderProgress, BITS.Progresses_PurchaseORreport);
            }

            foreach (var order in orders)
            {
                List<int> expressLst = new();
                order.Express = "[]";
                string unit = order.OrderTotal.Raw.CurrencyCode;
                Money amount = new Ryu.Data.Money();
                //自己的运单费用，更新到orderid的运费中           
                var waybillAlls = await _waybillNoSessionService.GetWaybillByOId(order.ID);
                if (waybillAlls is not null && waybillAlls.Count > 0)
                {
                    var waybill = waybillAlls.Where(x => x.State != ERP.Enums.Logistics.States.Cancle).ToList();
                    if (waybill != null && waybill.Count > 0)
                    {
                        amount = waybill.Sum(x => x.CarriageNum.Money);                        
                    }
                    expressLst.AddRange(waybillAlls.Select(x => x.ID).ToList());
                }
                Money mergeShippFee = new Ryu.Data.Money();

                if (order.MergeId.IsNotNull() && order.MergeId > 0  )
                {
                    //合并订单的相关运单
                    var mergeWaybillAlls = await _waybillNoSessionService.GetMergeWaybillList(order.MergeId);
                    if (mergeWaybillAlls.IsNotNull() && mergeWaybillAlls.Count > 0)
                    {
                        var mergeWaybills = mergeWaybillAlls.Where(x => x.State != ERP.Enums.Logistics.States.Cancle).ToList();
                        if (mergeWaybills is not null && mergeWaybills.Count > 0)
                        {
                            foreach (var mw in mergeWaybills)
                            {
                                //计算合并运单的运费，shippMoney/Order count
                                if (!string.IsNullOrEmpty(mw.OrderIDs))
                                {
                                    //判断ordeid是否存在在以下运单中                               
                                    List<int> ids = new();
                                    mw.OrderIDs.Split(",").ForEach(x => ids.Add(Convert.ToInt32(x)));
                                    if (ids.Contains(order.ID))
                                    {
                                        mergeShippFee += mw.CarriageNum.Money / ids.Count;
                                    }
                                }
                            }
                        }
                        expressLst.AddRange(mergeWaybillAlls.Select(x => x.ID).ToList());
                    }
                }
                order.Express = JsonConvert.SerializeObject(expressLst);
                decimal m = (mergeShippFee + amount).To(unit);

                if (order.ShippingMoney.Raw.CurrencyCode == "")
                {
                    order.ShippingMoney = new MoneyMeta(unit, m);
                }
                else
                {
                    order.ShippingMoney = order.ShippingMoney.Next(unit, m);
                }
                Money amountProfit = order.OrderTotal.Money - order.Fee.Money - order.Refund.Money - order.ShippingMoney.Money - order.Loss.Money - order.PurchaseFee.Money;

                decimal p = Math.Round(amountProfit/ order.OrderTotal.Raw.Rate,2);
                if (order.Profit.Raw.CurrencyCode == "")
                {
                    order.Profit = new MoneyMeta(unit, p);
                }
                else
                {
                    order.Profit = order.Profit.Next(unit, p);
                }
                #region 修改数据-订单获利跟踪
                await _statisticMoney.OrderProfitEdit(order);
                #endregion

                #region 修改数据-订单运费
                await _statisticMoney.OrderShippingMoneyEdit(order);
                #endregion
            }
            _logger.LogInformation($"UpdateMergeOrderWaybill success");            
        }
        catch (Exception e)
        {         
            _logger.LogError($"UpdateMergeOrderWaybill Exception:{e.Message}");
            _logger.LogError($"UpdateMergeOrderWaybill Exception:{e.StackTrace}");
            throw e;
        }
    }
    
    public async Task<List<Model>> GetOrdersByMIdsAsync(int? mid)
    {
        return await _dbContext.Order.Where(x => x.MergeId== mid)
            .Include(x => x.Store)
            .Include(x => x.Receiver).ToListAsync();
    }
    
    /**********************************通用方法********************************/
    /// <summary>
    /// AsTracking()! 根据ID获取订单
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    public async Task<Model> GetOrderByIdAsync(int? id)
    {
        var order = await _dbContext.Order.Include(a => a.Receiver).Include(a => a.Store).SingleOrDefaultAsync(a => a.ID == id);
        if (order == null) throw new Exception($"未找到ID为{id}的订单");
        return order;
    }
    
    /// <summary>
    /// 单订单 - 修改订单进度
    /// </summary>
    /// <param name="order"></param>
    /// <param name="list"></param>
    /// <returns></returns>
    public async Task BatchEditOrderProgress(Model order, List<ProgressValue> list)
    {
        if (list.Count > 0)
        {
            list.ForEach(item =>
            {
                order.SetBits(item.bits, item.state);
            });
            _dbContext.Order.Update(order);
            await _dbContext.SaveChangesAsync();
        }
    }
    
    public async Task BatchEditOrderProgress(List<Model> orders, List<ProgressValue>? list, BITS bITS)
    {
        foreach(var order in orders)
        {
            if (list is not null && list.Count > 0)
            {
                list.ForEach(item =>
                {
                    order.SetBits(item.bits, item.state);
                });
            }
            else
            {
                if(order.Bits== BITS.Progresses_Unprocessed)//如果不等于未处理状态，就不修改
                {
                    order.Bits = bITS;
                }                
            }
        }        
        _dbContext.Order.UpdateRange(orders);
        await _dbContext.SaveChangesAsync();
    }

}