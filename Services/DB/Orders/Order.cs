using ERP.Enums.Orders;
using ERP.Enums.Purchase;
using ERP.Extensions;
using ERP.Interface;
using ERP.ViewModels.Order.AmazonOrder;
using Microsoft.EntityFrameworkCore;
using static ERP.Extensions.DbSetExtension;
using OrderActionModel = ERP.Models.DB.Orders.Action;
using OrderRemarkModel = ERP.Models.DB.Orders.Remark;
using WaybillModel = ERP.Models.DB.Logistics.Waybill;
using AddressModel = ERP.Models.DB.Orders.Address;
using Model = ERP.Models.DB.Orders.Order;
using PurchaseModel = ERP.Models.DB.Purchase.Purchase;
using StoreModel = ERP.Models.Stores.StoreRegion;
using StoreMarket = ERP.Models.Stores.StoreMarket;
using ERP.Models.DB.Users;
using PlatformDataCache = ERP.Services.Caches.PlatformData;
using ERP.ViewModels.Purchase;
using System.Linq.Expressions;
using ERP.Enums.Finance;
using ERP.Services.DB.Statistics;
using States = ERP.Enums.Orders.States;
using ERP.Models.View.Order.AmazonOrder;
using ERP.Services.DB.Logistics;
using Newtonsoft.Json;
using ERP.Models.DB.Logistics;
using ERP.Data;
using ERP.Services.Stores;
using ERP.ViewModels.Order.Merge;
using System.Reflection;
using System.ComponentModel;
using static ERP.Models.DB.Orders.Order;
using System.Text;
using ERP.DomainEvents;
using MediatR;

using PurchaseService = ERP.Services.Purchase.Purchase;
using ERP.Models.View.Order;
using ERP.Services.Statistics;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;
using Microsoft.EntityFrameworkCore.Storage;
using ERP.Models.DB.Identity;
using ERP.ViewModels.Distribution;
using static ERP.Models.DB.Identity.User;
using asinProductService = ERP.Services.DB.Stores.AsinProduct;
using ERP.Services.Caches;
using storeCache = ERP.Services.Caches.Store;
using Company = ERP.Models.DB.Users.Company;
using newDeliverProduct = OrderSDK.Modles.Amazon.vm.DeliverProduct;
using ERP.Models.Api.Amazon.vm;
using ERP.Services.Api;
using ERP.Services.Api.Amazon;
using ERP.Models.DB.Orders;
using Aop.Api.Domain;
using Ryu.Extensions;
using ERP.ViewModels.Logistics;
using ERP.Models.View.Logistics.WayBill;
using ERP.Models.Api.Logistics;
using Coravel.Events.Interfaces;
using ERP.Services.Host;
using Coravel.Queuing.Broadcast;
using Coravel.Queuing.Interfaces;
using OrderHostService = ERP.Services.DB.Orders.OrderHost;
using Statistic = ERP.Services.DB.Statistics.Statistic;
using ERP.Models.Logistics;
using Org.BouncyCastle.Ocsp;

namespace ERP.Services.DB.Orders;
public class Order : OrderNoSessionService
{
    // private readonly DBContext _dbContext;
    private readonly ISessionProvider _sessionProvider;
    // private readonly Statistic _statisticService;
    private readonly Merge MergeService;
    private WayBill WayBillService => serviceProvider.GetRequiredService<WayBill>();
    private readonly PlatformDataCache platformDataCache;
    private readonly StoreService _storeService;
    private readonly IServiceProvider serviceProvider;
    private readonly Caches.Company _companyCache;
    private readonly IMediator _mediator;
    private readonly IOrderFactory ApiOrderFactory;
    // private readonly StatisticMoney _statisticMoney;

    private readonly asinProductService asinProduct;

    // protected readonly ILogger<Order> _logger;

    private readonly storeCache _storeCache;
    private readonly OrderHostService orderHostService;



    private readonly IQueue queue;
    private readonly IListener<QueueTaskCompleted> ilistener;

    public Order(storeCache storeCache, DBContext dbContext, ISessionProvider sessionProvider, Statistic statisticService,
        Merge _MergeService, IOrderFactory _ApiOrderFactory, PlatformDataCache _platformDataCache,
        StoreService storeService, IServiceProvider serviceProvider, Caches.Company companyCache, IMediator mediator, asinProductService _asinProduct, ILogger<Order> logger, IQueue _queue, IListener<QueueTaskCompleted> _ilistener, OrderHostService _orderHostService,WaybillNoSessionService waybillNoSessionService) : base(dbContext,logger,waybillNoSessionService,statisticService)
    {
        // _dbContext = dbContext;
        _sessionProvider = sessionProvider;
        // _statisticService = statisticService;
        MergeService = _MergeService;
        ApiOrderFactory = _ApiOrderFactory;
        platformDataCache = _platformDataCache;
        _storeService = storeService;
        this.serviceProvider = serviceProvider;
        _companyCache = companyCache;
        _mediator = mediator;
        // _statisticMoney = new StatisticMoney(_statisticService);
        asinProduct = _asinProduct;
        // _logger = logger;
        _storeCache = storeCache;
        queue = _queue;
        ilistener = _ilistener;
        orderHostService = _orderHostService;
    }

    private ISession Session { get => _sessionProvider.Session!; }
    private int OEMID { get => Session.GetOEMID(); }
    private int CompanyID { get => Session.GetCompanyID(); }
    private int GroupID { get => Session.GetGroupID(); }
    private int UserID { get => Session.GetUserID(); }

    /// <summary>
    /// add   add orderBasic
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    public async Task<bool> Add(Model model, bool fullpull = false) => await AddInternal(new[] { model }, fullpull);
    public async Task<bool> Add(Model[] models, bool fullpull = false) => await AddInternal(models, fullpull);
    private async Task<bool> AddInternal(Model[] models, bool fullpull)
    {
        foreach (var model in models)
        {
            if (await AddInternal(model, fullpull))
                continue;
            else
                return false;
        }
        return true;
    }


    public Model? GetOrderExist(Model model)
    {
        return _dbContext.Order.Include(x => x.Receiver).Where(a => a.OrderNoHash == model.OrderNoHash && a.CompanyID == model.CompanyID && a.StoreId == model.Store.ID).FirstOrDefault();
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="model"></param>
    /// <param name="fullpull">true:手动拉取标记</param>
    /// <returns></returns>
    private async Task<bool> AddInternal(Model model, bool fullpull)
    {
        try
        {
            var user = await _dbContext.User.FirstOrDefaultAsync(a => a.Id == model.UserID);
            if (user == null) throw new Exception($"未找到用户[ID:{model.UserID}]");

            var Source = GetOrderExist(model);
            if (Source is null)
            {
                var company = _companyCache.Get(model.CompanyID);
                if (company is { IsDistribution: true })
                {
                    if (company.DistributionPurchase == Company.DistributionPurchases.TRUE)
                    {
                        model.PurchaseCompanyId = company.ReportId;
                    }
                    if (company.DistributionWaybill == Company.DistributionWaybills.TRUE)
                    {
                        model.WaybillCompanyId = company.ReportId;
                    }
                }
                await _dbContext.Order.AddAsync(model);
                var orderCreatedEvent = new OrderCreatedEvent(model);
                await _mediator.Publish(orderCreatedEvent);
            }
            else
            {
                if (fullpull)  //手动拉取
                {
                    if (Source.pullOrderState != PullOrderState.OutOfProcess)  //避免手动拉取把原来拉取完整的数据修改掉
                    {
                        Source.BuyerUserName = model.BuyerUserName;
                        Source.PaymentMethod = model.PaymentMethod;
                        Source.CreateTimeRaw = model.CreateTimeRaw;
                        Source.CreateTime = model.CreateTime;
                        Source.PayTimeRaw = model.PayTimeRaw;
                        Source.PayTime = model.PayTime;
                        Source.LatestShipTime = model.LatestShipTime;
                        Source.LastUpdateDate = model.LastUpdateDate;
                        Source.DefaultShipFromLocationAddress = model.DefaultShipFromLocationAddress;
                        var orderItems = model.GoodsScope();
                        Source.GoodsScope(GoodList =>
                        {
                            var NumTemp = 0;
                            foreach (var item in orderItems)
                            {
                                //delivery
                                //unit -> mate
                                Good good = new();
                                good.ID = item.ID;//asin
                                good.OrderItemId = item.OrderItemId;
                                good.FromURL = item.FromURL;
                                good.Name = item.Name;//name
                                good.Sku = item.Sku;//seller_sku
                                good.ImageURL = item.ImageURL;
                                good.Brand = item.Brand;
                                good.Size = item.Size;
                                good.Color = item.Color;
                                NumTemp += item.QuantityOrdered;
                                good.QuantityOrdered = item.QuantityOrdered;//num
                                good.QuantityShipped = item.QuantityShipped;//num-send_num? 已发货数量
                                                                            //产品价格
                                good.ItemPrice = item.ItemPrice;
                                good.ShippingPrice = item.ShippingPrice;
                                good.UnitPrice = good.QuantityOrdered;
                                good.TotalPrice = good.ItemPrice;
                                good.promotionDiscount = item.promotionDiscount;
                                good.ShippingTax = item.ShippingTax;
                                good.ShippingDiscountTax = item.ShippingDiscountTax;
                                good.PromotionDiscountTax = item.PromotionDiscountTax;
                                good.ItemTax = item.ItemTax;
                                GoodList.Add(good);
                            }
                            //ShippingMoney = new(GoodList.Sum(x => x.ShippingPrice.Money));

                            //无需断言两者相等 中间会有很多杂项因素(税费，佣金等)导致价格不匹配
                            //Debug.Assert( OrderTotal.Money == ShippingMoney.Money + ProductMoney.Money );            

                        }, Modes.Save);

                        var receiverInternal = model.ReceiverInternal;

                        Source.Receiver = new()
                        {
                            Name = receiverInternal.Name,
                            Phone = receiverInternal.Phone,
                            Email = receiverInternal.Email,
                            Zip = receiverInternal.Zip,
                            NationId = receiverInternal.NationId,
                            Nation = receiverInternal.Nation,
                            NationShort = receiverInternal.NationShort,
                            Province = receiverInternal.Province,
                            City = receiverInternal.City,
                            County = receiverInternal.County,
                            District = receiverInternal.District,
                            Address1 = receiverInternal.Address1,
                            Address2 = receiverInternal.Address2,
                            Address3 = receiverInternal.Address3,
                        };

                        Source.Receiver.Name = receiverInternal.Name;
                        Source.Receiver.Phone = receiverInternal.Phone;
                        Source.Receiver.Email = receiverInternal.Email;
                        Source.Receiver.Zip = receiverInternal.Zip;
                        Source.Receiver.NationId = receiverInternal.NationId;
                        Source.Receiver.Nation = receiverInternal.Nation;
                        Source.Receiver.NationShort = receiverInternal.NationShort;
                        Source.Receiver.Province = receiverInternal.Province;
                        Source.Receiver.City = receiverInternal.City;
                        Source.Receiver.County = receiverInternal.County;
                        Source.Receiver.District = receiverInternal.District;
                        Source.Receiver.Address1 = receiverInternal.Address1;
                        Source.Receiver.Address2 = receiverInternal.Address2;
                        Source.Receiver.Address3 = receiverInternal.Address3;
                        Source.OrderTotal = model.OrderTotal;
                        Source.ProductNum = model.ProductNum;
                        Source.Delivery = model.Delivery;
                        Source.DeliveryNo = model.DeliveryNo;
                        //Source.State = model.State;
                        Source.ShippingType = model.ShippingType;
                        Source.FeesEstimate = model.FeesEstimate;
                        //Source.Bits = model.Bits;
                        Source.Fee = model.Fee;
                        Source.Refund = model.Refund;
                        Source.Profit = model.Profit;
                        Source.PullCount = model.PullCount;
                        Source.pullOrderState = model.pullOrderState;
                        _dbContext.Order.Update(Source);
                    }
                }
                else  //自动更新
                {
                    if (Source.State == model.State)
                    {
                        await AddLog(Source, user, "同步订单状态[未发生变化]");
                    }
                    else
                    {
                        var old = Source.State;
                        //Source.Bits = model.Bits;
                        Source.State = model.State;
                        Source.OrderTotal = Source.OrderTotal.Next(model.OrderTotal);
                        Source.Fee = Source.Fee.Next(model.Fee);
                        Source.Refund = Source.Refund.Next(model.Refund);
                        Source.Profit = Source.Profit.Next(model.Profit);
                        Source.Delivery = model.Delivery;
                        Source.DeliveryNo = model.DeliveryNo;
                        await AddLog(Source, user, $"更新订单状态[{Extensions.EnumExtension.GetDescription(old)}->{Extensions.EnumExtension.GetDescription(model.State)}]");
                    }
                }
            }
            return true;
        }
        catch (Exception e)
        {
            var innerMsg = e.InnerException.IsNotNull() ? e.InnerException.Message : "";
            _logger.LogError($" order AddInternal error:storeID:{model.StoreId},OrderNo:{model.OrderNo},Msg:{e.Message},inner error:{innerMsg}");
            _logger.LogError(e, e.Message);
        }
        return false;
    }


    /// <summary>
    /// update : add order detail infomation
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    public async Task<bool> UpdateOrder(Model[] models) => await UpdateInternal(models);

    public async Task<bool> UpdateOrder(Model model) => await UpdateInternal(new Model[] { model });


    private async Task<bool> UpdateInternal(Model[] models)
    {
        foreach (var model in models)
        {
            if (await UpdateInternal(model))
                continue;
            else
                return false;
        }
        return true;
    }

    private async Task<bool> UpdateInternal(Model model)
    {
        try
        {
            var Source = GetOrderExist(model);
            if (Source is null)
            {
                //出异常了
                _logger.LogError($"{model.OrderNo}不存在，拉取订单详情失败！");
            }
            else
            {
                var user = await _dbContext.User.FirstOrDefaultAsync(a => a.Id == model.UserID);
                if (user == null) throw new Exception($"未找到用户[ID:{model.UserID}]");
                model.PullCount++;
                _dbContext.Order.Update(model);
                await AddLog(model, user, $"拉取订单详细信息成功");

                //订单汇总sku和goodid
                await SumOrderSkuAndAsin(model);
            }
            return true;
        }
        catch (Exception e)
        {
            _logger.LogError($" order AddInternal error:storeID:{model.StoreId},OrderNo:{model.OrderNo},Msg:{e.Message}");
            _logger.LogError(e, e.Message);
        }
        return false;
    }

    public async Task<bool> UpdateHostOrder(Model model)
    {
        try
        {
            var Source = GetOrderExist(model);
            if (Source is null)
            {
                //出异常了
                _logger.LogError($"{model.OrderNo}不存在，拉取订单详情失败！");
            }
            else
            {
                var user = await _dbContext.User.FirstOrDefaultAsync(a => a.Id == model.UserID);
                if (user == null) throw new Exception($"未找到用户[ID:{model.UserID}]");
                model.PullCount++;

                Source.BuyerUserName = model.BuyerUserName;
                Source.PaymentMethod = model.PaymentMethod;
                Source.CreateTimeRaw = model.CreateTimeRaw;
                Source.CreateTime = model.CreateTime;
                Source.PayTimeRaw = model.PayTimeRaw;
                Source.PayTime = model.PayTime;
                Source.LatestShipTime = model.LatestShipTime;
                Source.LastUpdateDate = model.LastUpdateDate;
                Source.DefaultShipFromLocationAddress = model.DefaultShipFromLocationAddress;
                var orderItems = model.GoodsScope();
                Source.ClearGoods();
                Source.GoodsScope(GoodList =>
                {
                    var NumTemp = 0;
                    foreach (var item in orderItems)
                    {
                        //delivery
                        //unit -> mate
                        Good good = new();
                        good.ID = item.ID;//asin
                        good.FromURL = item.FromURL;
                        good.Name = item.Name;//name
                        good.Sku = item.Sku;//seller_sku
                        good.OrderItemId = item.OrderItemId;
                        good.CustomizedURL = item.CustomizedURL;
                        good.ImageURL = item.ImageURL;// ItemAttributes.SmallImage.IsNull() ? "" : ItemAttributes.SmallImage.URL;
                        good.Brand = item.Brand;// ItemAttributes.Brand.IsNull() ? "" : ItemAttributes.Brand;
                        good.Size = item.Size;// ItemAttributes.Size.IsNull() ? "" : ItemAttributes.Size;
                        good.Color = item.Color;// ItemAttributes.Color.IsNull() ? "" : ItemAttributes.Color;
                        NumTemp += item.QuantityOrdered;
                        good.QuantityOrdered = item.QuantityOrdered;//num
                        good.QuantityShipped = item.QuantityShipped;//num-send_num? 已发货数量
                                                                    //产品价格
                        good.ItemPrice = item.ItemPrice;//.IsNotNull() ? item.ItemPrice.Mate : new Data.MoneyMeta();// var product_money
                        good.ShippingPrice = item.ShippingPrice;//.IsNotNull() ? item.ShippingPrice.Mate : new Data.MoneyMeta();// var shipping_money,amazon_shipping_money
                        good.UnitPrice = item.UnitPrice; //> 0 ? good.ItemPrice with { Money = good.ItemPrice.Money / good.QuantityOrdered } : -1; //price
                        good.TotalPrice = item.ItemPrice;// with { Money = good.ItemPrice.Money + good.ShippingPrice.Money };//total
                                                         //折后价格
                        good.promotionDiscount = item.promotionDiscount;//.IsNotNull() ? item.PromotionDiscount.Mate : new Data.MoneyMeta();
                        good.ShippingTax = item.ShippingTax;//.IsNotNull() ? item.ShippingTax.Mate : new Data.MoneyMeta();//shipping_tax
                        good.ShippingDiscountTax = item.ShippingDiscountTax;//.IsNotNull() ? item.ShippingDiscountTax.Mate : new Data.MoneyMeta();
                        good.PromotionDiscountTax = item.PromotionDiscountTax;//.IsNotNull() ? item.PromotionDiscountTax.Mate : new Data.MoneyMeta();
                        good.ItemTax = item.ItemTax;//.IsNotNull() ? item.ItemTax.Mate : new Data.MoneyMeta();//item_tax
                        GoodList.Add(good);
                    }
                    //ShippingMoney = new(GoodList.Sum(x => x.ShippingPrice.Money));

                    //无需断言两者相等 中间会有很多杂项因素(税费，佣金等)导致价格不匹配
                    //Debug.Assert( OrderTotal.Money == ShippingMoney.Money + ProductMoney.Money );            

                }, Modes.Save);

                var receiverInternal = model.ReceiverInternal;

                Source.Receiver = new()
                {
                    Name = receiverInternal.Name,
                    Phone = receiverInternal.Phone,
                    Email = receiverInternal.Email,
                    Zip = receiverInternal.Zip,
                    NationId = receiverInternal.NationId,
                    Nation = receiverInternal.Nation,
                    NationShort = receiverInternal.NationShort,
                    Province = receiverInternal.Province,
                    City = receiverInternal.City,
                    County = receiverInternal.County,
                    District = receiverInternal.District,
                    Address1 = receiverInternal.Address1,
                    Address2 = receiverInternal.Address2,
                    Address3 = receiverInternal.Address3,
                };

                Source.Receiver.Name = receiverInternal.Name;
                Source.Receiver.Phone = receiverInternal.Phone;
                Source.Receiver.Email = receiverInternal.Email;
                Source.Receiver.Zip = receiverInternal.Zip;
                Source.Receiver.NationId = receiverInternal.NationId;
                Source.Receiver.Nation = receiverInternal.Nation;
                Source.Receiver.NationShort = receiverInternal.NationShort;
                Source.Receiver.Province = receiverInternal.Province;
                Source.Receiver.City = receiverInternal.City;
                Source.Receiver.County = receiverInternal.County;
                Source.Receiver.District = receiverInternal.District;
                Source.Receiver.Address1 = receiverInternal.Address1;
                Source.Receiver.Address2 = receiverInternal.Address2;
                Source.Receiver.Address3 = receiverInternal.Address3;
                Source.OrderTotal = model.OrderTotal;
                Source.ProductNum = model.ProductNum;
                Source.Delivery = model.Delivery;
                Source.DeliveryNo = model.DeliveryNo;
                //Source.State = model.State;
                Source.ShippingType = model.ShippingType;
                Source.FeesEstimate = model.FeesEstimate;
                //Source.Bits = model.Bits;
                Source.Fee = model.Fee;
                Source.Refund = model.Refund;
                Source.Profit = model.Profit;
                Source.PullCount = model.PullCount;
                Source.pullOrderState = model.pullOrderState;
                _dbContext.Order.Update(Source);
                await AddLog(model, user, $"更新订单详细信息成功");
                return true;
            }
        }
        catch (Exception e)
        {
            _logger.LogError($" order Update error:storeID:{model.StoreId},OrderNo:{model.OrderNo},Msg:{e.Message}");
            _logger.LogError(e, e.Message);
        }
        return false;
    }

    /// <summary>
    /// 根据storeId，查找需要更新的order信息
    /// </summary>
    /// <param name="storeID"></param>
    /// <param name="expression"></param>
    /// <returns></returns>
    public List<Model> GetInPullOrders(int storeID, Expression<Func<Model, bool>> expression, int ItemCount = 100)
    {
        return _dbContext.Order.Include(x => x.Receiver).Where(x => x.StoreId == storeID).WhenWhere(expression.IsNotNull(), expression).OrderByDescending(x => x.LastUpdateDate).Take(ItemCount).ToList();
    }

    public async Task<IList<WaitOrder>> GetInGProcessOrders(IList<WaitOrder> waitOrders)
    {
        IList<WaitOrder> orders = new List<WaitOrder>();
        foreach (var waitOrder in waitOrders)
        {
            var orderNoHash = Helpers.CreateHashCode(waitOrder.GetAmazoNewParam().AmazonOrderId);
            var b = await GetOrder(orderNoHash);
            if (b)//已经拉取完毕的就不要再拉取了
            {
                orders.Add(waitOrder);
            }
        }
        return orders;
    }

    public async Task<bool> GetOrder(ulong orderNopHash)
    {
        var o = await _dbContext.Order.FirstOrDefaultAsync(a => a.OrderNoHash == orderNopHash && a.pullOrderState == PullOrderState.OutOfProcess);
        if (o == null)
        { return true; }
        else { return false; }

    }


    public void UpdateDeliveryFail(int id)
    {
         _dbContext.Order.Where(x => x.ID == id).UpdateFromQuery(x => new
        {
            DeliveryFail = x.DeliveryFail + 1
        });
    }

    public void UpdateDeliveryFail(List<int> ids)
    {
        foreach (var id in ids)
        {
            _dbContext.Order.Where(x => x.ID == id).UpdateFromQuery(x => new
            {
                DeliveryFail = x.DeliveryFail + 1
            });
        }
    }


    //获取订单数据
    public async Task<PaginateStruct<Model, OrderSumDto>> GetOrderListPage(OrderIndexListQueryVM rq)
    {
        var now = DateTime.Now;
        var latestShipTimeFilter = rq.LatestShipTimeFilter switch
        {
            "WithInOne" => new { start = now, end = now.AddDays(1) },
            "WithInThree" => new { start = now, end = now.AddDays(3) },
            "Exceed" => new { start = new DateTime(1970, 1, 1), end = now },
            _ => new { start = now, end = now },
        };

        List<int>? PurchaseCondition = null;
        if (rq.PurchaseOrderNo.IsNotWhiteSpace() || rq.PurchaseTrackingNumber.IsNotWhiteSpace())
        {
            PurchaseCondition = _dbContext.Purchase
                .WhenWhere(rq.PurchaseOrderNo.IsNotWhiteSpace(), m => m.PurchaseOrderNo == rq.PurchaseOrderNo)
                .WhenWhere(rq.PurchaseTrackingNumber.IsNotWhiteSpace(), m => m.PurchaseTrackingNumber == rq.PurchaseTrackingNumber)
                .Where(a => a.OrderId.HasValue)
                .Select(a => a.OrderId!.Value).ToList();
            if (PurchaseCondition.Count == 0)
            {
                return new PaginateStruct<Model, OrderSumDto>();
            }
        }
        List<int>? WaybillCondition = null;
        if (rq.Express.IsNotWhiteSpace() || rq.TrackingNumber.IsNotWhiteSpace())
        {
            //普通运单
            WaybillCondition = WayBillService.GetWayIds(rq.Express, rq.TrackingNumber);   
            //_dbContext.Waybill
            //    .WhenWhere(rq.Express.IsNotWhiteSpace(), m => m.Express == rq.Express)
            //    .WhenWhere(rq.TrackingNumber.IsNotWhiteSpace(), m => m.Tracking == rq.TrackingNumber)
            //    .Where(a => a.OrderId > 0).Where(x=>x.MergeID<=0)
            //    .Select(a => a.OrderId).ToList();

            //var WaybillMergeIDs = _dbContext.Waybill
            //    .WhenWhere(rq.Express.IsNotWhiteSpace(), m => m.Express == rq.Express)
            //    .WhenWhere(rq.TrackingNumber.IsNotWhiteSpace(), m => m.Tracking == rq.TrackingNumber)
            //    .Where(x => x.MergeID>0)
            //    .Select(a => a.OrderIDs).ToList();

            //if(WaybillMergeIDs !=null && WaybillMergeIDs .Count>0)
            //{
            //    WaybillMergeIDs.ForEach(x => WaybillCondition.AddRange(TransStr(x)));
            //}
            if (WaybillCondition.Count == 0)
            {
                return new PaginateStruct<Model, OrderSumDto>();
            }
        }
        List<int> IDS = new();
        if (PurchaseCondition.IsNotNull() && WaybillCondition.IsNotNull())
        {
            IDS = PurchaseCondition!.Where(i => WaybillCondition!.Contains(i)).ToList();
        }
        else if (PurchaseCondition.IsNotNull())
        {
            IDS = PurchaseCondition!;
        }
        else if (WaybillCondition.IsNotNull())
        {
            IDS = WaybillCondition!;
        }

        // 根据sku或asin搜
        if (!rq.Sku.IsNullOrWhiteSpace())
        {
            var sku = await _dbContext.OrderGoodSku.FirstOrDefaultAsync(a => a.Value == rq.Sku!);
            if (sku == null) return new PaginateStruct<Model, OrderSumDto>();
            IDS.AddRange(sku.GetOrderIds());
        }
        if (!rq.GoodId.IsNullOrWhiteSpace())
        {
            var goodId = await _dbContext.OrderGoodId.FirstOrDefaultAsync(a => a.Value == rq.GoodId!);
            if (goodId == null) return new PaginateStruct<Model, OrderSumDto>();
            IDS.AddRange(goodId.GetOrderIds());
        }

        var store = await _storeService.CurrentUserGetStores();
        var storeIds = store.Select(a => a.ID);
        var root = _dbContext.Order
            .Where(a => a.OEMID == OEMID)
            .Where(a => a.CompanyID == CompanyID) 
            .WhenWhere(rq.OnlyDistributed is not true, a => storeIds.Contains(a.StoreId))
            // .WhenWhere(rq.OnlyDistributed is not true, a => storeIds.Contains(a.StoreId) || ids.Contains(a.ID))
            .WhenWhere(rq.OnlyDistributed is true, db => db.WhereDistributed(_dbContext, UserID))
            .Where(a => a.pullOrderState == PullOrderState.OutOfProcess)
            .WhenWhere(rq.OrderNo != "", a => a.OrderNo == rq.OrderNo)
            .WhenWhere(rq.StartAndEndTime != null && rq.StartAndEndTime.Count == 2,
                a => a.CreateTime >= rq.StartAndEndTime![0] && a.CreateTime < rq.StartAndEndTime[1].AddDays(1))
            .WhenWhere(IDS.Count > 0, o => IDS!.Contains(o.ID))
            .WhenWhere(rq.Plateform != Platforms.None, a => a.Plateform == rq.Plateform)
            .WhenWhere(rq.Store?.Count > 0, a => rq.Store!.Contains(a.StoreId))
            .WhenWhere(rq.OrderState != null, a => a.State == rq.OrderState)
            .WhenWhere(rq.Progress.Mask != 0, a => ((ulong)a.Bits & rq.Progress!.Mask) == rq.Progress.Value)
            .WhenWhere(rq.LatestShipTimeFilter != null && rq.LatestShipTimeFilter != "Exceed",
                a => a.State == Enums.Orders.States.UNSHIPPED && a.LatestShipTime > latestShipTimeFilter.start &&
                     a.LatestShipTime < latestShipTimeFilter.end)
            .WhenWhere(rq.LatestShipTimeFilter != null && rq.LatestShipTimeFilter == "Exceed",
                a => a.State == Enums.Orders.States.UNSHIPPED && a.LatestShipTime > latestShipTimeFilter.start && a.LatestShipTime < latestShipTimeFilter.end)
            .WhenWhere(!string.IsNullOrEmpty(rq.GoodName),m => m.GoodData.Contains(rq.GoodName))
            .WhenWhere(rq.Seller?.Count > 0, a => rq.Seller!.Contains(a.MarketPlace))

            //.WhenWhere(rq.PurchaseOrderNo.IsNotWhiteSpace(), a => a.OrderNo == rq.OrderNo)
            //.WhenWhere(rq.PurchaseTrackingNumber == string.Empty, a => a.OrderNo == rq.OrderNo)
            //.OrderByDescending(a => a.CreateTime)
            .GetSortBy(rq.SortBy)
            .Include(a => a.Store)
            .Include(a => a.Receiver);
        var sum = new OrderSumDto
        {
            OrderTotal = Helpers.TransMoney(await root.SumAsync(o => o.OrderTotal.Money), Session.GetUnitConfig()),
            Fee = Helpers.TransMoney(await root.SumAsync(o => o.Fee.Money), Session.GetUnitConfig()),
            Refund = Helpers.TransMoney(await root.SumAsync(o => o.Refund.Money), Session.GetUnitConfig()),
            ShippingMoney = Helpers.TransMoney(await root.SumAsync(o => o.ShippingMoney.Money), Session.GetUnitConfig()),
            PurchaseFee = Helpers.TransMoney(await root.SumAsync(o => o.PurchaseFee.Money), Session.GetUnitConfig()),
            Loss = Helpers.TransMoney(await root.SumAsync(o => o.Loss.Money), Session.GetUnitConfig()),
            Unit = Session.GetUnitConfig(),
        };
        var orderData = await root
            .AsNoTracking().ToPaginateAsync();
        var result = new PaginateStruct<Models.DB.Orders.Order, OrderSumDto>()
        {
            CurrentPage = orderData.CurrentPage,
            Total = orderData.Total,
            Items = orderData.Items,
            PageSize = orderData.PageSize,
            Group = sum
        };
        return result;
    }

  


    //    /**
    //     * 获取条件下导出订单数量
    //     */
    public async Task<object> getOrderExportNum(OrderIndexListQueryVM rq)
    {

        var now = DateTime.Now;
        var latestShipTimeFilter = rq.LatestShipTimeFilter switch
        {
            "WithInOne" => new { start = now, end = now.AddDays(1) },
            "WithInThree" => new { start = now, end = now.AddDays(3) },
            "Exceed" => new { start = new DateTime(1970, 1, 1), end = now },
            _ => new { start = now, end = now },
        };

        int[]? PurchaseCondition = null;
        if (rq.PurchaseOrderNo.IsNotWhiteSpace() || rq.PurchaseTrackingNumber.IsNotWhiteSpace())
        {
            PurchaseCondition = _dbContext.Purchase
                .WhenWhere(rq.PurchaseOrderNo.IsNotWhiteSpace(), m => m.PurchaseOrderNo == rq.PurchaseOrderNo)
                .WhenWhere(rq.PurchaseTrackingNumber.IsNotWhiteSpace(), m => m.PurchaseTrackingNumber == rq.PurchaseTrackingNumber)
                .Where(a => a.OrderId.HasValue)
                .Select(a => a.OrderId!.Value).ToArray();
            if (PurchaseCondition.Length == 0)
            {
                return 0;
            }
        }
        int[]? WaybillCondition = null;
        if (rq.Express.IsNotWhiteSpace() || rq.TrackingNumber.IsNotWhiteSpace())
        {
            WaybillCondition = WayBillService.GetWayIds(rq.Express, rq.TrackingNumber).ToArray();
            //_dbContext.Waybill
            //    .WhenWhere(rq.Express.IsNotWhiteSpace(), m => m.WaybillOrder == rq.Express)
            //    .WhenWhere(rq.TrackingNumber.IsNotWhiteSpace(), m => m.Tracking == rq.TrackingNumber)
            //    .Where(a => a.OrderId > 0)
            //    .Select(a => a.OrderId).ToArray();
            if (WaybillCondition.Length == 0)
            {
                return 0;
            }
        }
        int[]? IDS = null;
        if (PurchaseCondition.IsNotNull() && WaybillCondition.IsNotNull())
        {
            IDS = PurchaseCondition!.Where(i => WaybillCondition!.Contains(i)).ToArray();
        }
        else if (PurchaseCondition.IsNotNull())
        {
            IDS = PurchaseCondition;
        }
        else if (WaybillCondition.IsNotNull())
        {
            IDS = WaybillCondition;
        }
        // user只能看到(自己添加的店铺+上级绑定给他的店铺)的订单
        var store = await _storeService.CurrentUserGetStores();
        var storeIds = store.Select(a => a.ID);
        var orderDatas = await _dbContext.Order
            .Where(a => a.OEMID == OEMID)
            .Where(a => a.CompanyID == CompanyID)
            .Where(a => storeIds.Contains(a.StoreId))
            .Where(a => a.pullOrderState == PullOrderState.OutOfProcess)
            .WhenWhere(rq.OrderNo != "", a => a.OrderNo == rq.OrderNo)
            .WhenWhere(rq.StartAndEndTime != null && rq.StartAndEndTime.Count == 2, a => a.CreateTime >= rq.StartAndEndTime![0] && a.CreateTime < rq.StartAndEndTime[1].AddDays(1))
            .WhenWhere(IDS.IsNotNull(), o => IDS!.Contains(o.ID))
            .WhenWhere(rq.Plateform != Platforms.None, a => a.Plateform == rq.Plateform)
            .WhenWhere(rq.Store?.Count > 0, a => rq.Store!.Contains(a.StoreId))
            .WhenWhere(rq.OrderState != null, a => a.State == rq.OrderState)
            .WhenWhere(rq.Progress.Mask != 0, a => ((ulong)a.Bits & rq.Progress!.Mask) == rq.Progress.Value)
            .WhenWhere(rq.LatestShipTimeFilter != null, a => a.State == Enums.Orders.States.UNSHIPPED && a.LatestShipTime > latestShipTimeFilter.start && a.LatestShipTime < latestShipTimeFilter.end && a.State == States.UNSHIPPED)
            //.Include(a => a.Store)
            //.Include(a => a.Receiver)
            .Select(s => s.ID)
            .ToArrayAsync();

        return new
        {
            count = orderDatas.Count(),
            ids = orderDatas
        };


        ////        $field = ['entry_mode', 'id', 'receiver_nation', 'receiver_nation_short', 'url', 'earliest_ship_time', 'order_no', 'pay_money', 'company_id', 'product_num', 'order_state', 'create_time', 'shipping_type', 'fake_product_time', 'true_product_time', 'store_id', 'id', 'coin', 'truename', 'receiver_name', 'receiver_email', 'progress', 'latest_ship_time', 'merge_id', 'process_waybill', 'process_purchase', 'purchase_company_id', 'waybill_company_id', 'created_at', 'rate', 'is_batch_ship', 'seller_id','profit'];
        //        $field = ['id'];
        //        empty($sort_by) && $sort_by = ["create_time", "desc"];
        //        $model = AmazonOrderModel::query()
        //            ->storeIdIn($shopId)
        //            ->deliver($deliver)
        //            ->orderNo($orderNo)
        //            ->createTimeEnd($createTimeEnd)
        //            ->createTimeStart($createTimeStart)
        //            ->orderStateIn($orderStatus)
        //            ->companyId($this->getSession()->getCompanyId())
        //            ->OemId($this->getSession()->getOemId())
        //            ->StoreIds($store_ids)
        //            ->purchaseTrackingNumber($purchaseTrackingNumber,false,$purchaseOrderNo)
        //            ->waybillOrder($express)
        //            ->OrderProgress((int)$progress[0], $progress[1], $progress[2] !== 'false', $progress[3] !== 'false')
        //            ->OrderBy($sort_by[0], $sort_by[1])
        //            ->SellerId($seller_id)
        //            ->LatestShipTimeFilter($latest_ship_time_filter);
        //        $count = $model->count();
        //        $ids = $model->pluck('id')->toArray();
        //        return [
        //            'count' => $count ?? 0,
        //            'ids' => $ids
        //        ];


        //return ReturnArr();
    }

    /// <summary>
    /// 订单获取关联的采购和运单
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    public async Task<object> GetPurchaseWaybill(List<int> ids)
    {
        List<object> datas = new();
        var purchase = await _dbContext.Purchase
            .AsNoTracking()
            .Where(a => a.OrderId.HasValue && ids.Contains(a.OrderId.Value))
            .Where(a => a.PurchaseTrackingNumber != null && a.PurchaseTrackingNumber.Length > 0)
            .Select(a => new
            {
                a.ID,
                PurchaseTrackingNumber = a.PurchaseTrackingNumber ?? "",
                a.OrderId,
            }).ToListAsync();
       
        var orders = await GetOrdersByIdsAsync(ids);

        foreach (var order in orders)
        {
            var waybillIDs = await WayBillService.GetWaybillIDsByOrder(order.ID, order.MergeId);                    
            var data = new
            {
                order.ID,
                purchaseTrackingNumber = purchase.Where(a => a.OrderId == order.ID).ToList(),
                waybillOrder = waybillIDs.IsNotNull() && waybillIDs.Count() > 0? await GetWayByOrder(waybillIDs):null,
            };
            datas.Add(data);
        }
        return datas;
    }

    public async Task<List<WayInfo>?> GetWayByOrder(List<int>? waybillIDs)
    {
       return  await _dbContext.Waybill
        .Where(a => waybillIDs.Contains(a.ID))
        .Where(a => a.WaybillOrder != null && a.WaybillOrder.Length > 0)
        .Select(a => new WayInfo
        {
            ID= a.ID,
            WaybillOrder= a.WaybillOrder,
            Express= a.Express,
            State = a.State,           
            OrderId = a.OrderId,
        }).AsNoTracking().ToListAsync();
    }
        

    public async Task<ReturnStruct> GetExtraNumPageAttr(object list)
    {
        //        $orderIds = array_map(fn($item) => $item->id, $list->items());
        //        $purchases = PurchaseModel::query()->whereIn('order_id', $orderIds)->select(['id', 'order_id', 'purchase_tracking_number', 'purchase_order_no'])->get()->toArray();
        //        $fun = function($arr) {
        //            $tmp = [];
        //            foreach ($arr as $item) {
        //                if (!empty($item['purchase_tracking_number']))
        //                {
        //                    $tmp[$item['order_id']]['purchase_tracking_number'][] = $item;
        //                }
        //                elseif(!empty($item['purchase_order_no'])) {
        //                    $tmp[$item['order_id']]['purchase_order_no'][] = $item;
        //                }

        //                if (!empty($item['waybill_order']))
        //                {
        //                    $tmp[$item['order_id']]['waybill_order'][] = $item;
        //                }
        //                elseif(!empty($item['express'])) {
        //                    $tmp[$item['order_id']]['express'][] = $item;
        //                }

        //            }
        //            return $tmp;
        //        };
        //        $purchases = $fun($purchases);

        //        $waybills = WayBill::query()->whereIn('order_id', $orderIds)->get(['id', 'order_id', 'waybill_order', 'express'])->toArray();
        //        $waybills = $fun($waybills);


        //        foreach ($list->items() as $item) {
        //            $item->purchaseTrackingNumber = $purchases[$item->id]['purchase_tracking_number'] ?? [];
        //            $item->purchaseOrderNo = $purchases[$item->id]['purchase_order_no'] ?? [];

        //            $item->waybillOrder = $waybills[$item->id]['waybill_order'] ?? [];
        //            $item->express = $waybills[$item->id]['express'] ?? [];
        //        }
        return await Task.FromResult(ReturnArr());
    }

    public async Task<Model?> Info(int id/*$field = [], $company_id = '', $store_id = '', $order_no = '', $order_state = '', $createTimeStart = '', $createTimeEnd = '', $store_id_array = []*/)
    {
        //        empty($field) && $field = ["*"];
        //        return AmazonOrderModel::query()
        //            ->select($field)
        //            ->companyId($company_id)
        //            ->storeId($store_id)
        //            ->orderNo($order_no)
        //            ->orderState($order_state)
        //            ->createTimeStart($createTimeStart)
        //            ->createTimeEnd($createTimeEnd)
        //            ->StoreIdIn($store_id_array);
        return await _dbContext.Order.Where(x => x.ID == id).FirstOrDefaultAsync();
    }

    //根据id获取订单数据
    public async Task<Model?> GetOrderInfoFirst(int order_id)
    {
        //        //有时你可能希望在查找首个结果但找不到值时执行其他动作。
        //        //firstOr 方法将会在查找到结果时返回首个结果，如果没有结果，将会执行给定的回调。
        //        //回调的返回值将会作为 firstOr 方法的返回值
        //        return $this->AmazonOrderModel::query()->where('id', $order_id)->firstOr(function() {
        //            return false;
        //        });
        return await _dbContext.Order.FirstOrDefaultAsync(a => a.ID == order_id);
        //return ReturnArr();
    }

    /// <summary>
    /// 查询待合并订单
    /// </summary>
    /// <returns></returns>
    public async Task<List<Model>?> GetOrderInfoWhere()
    {
        return await _dbContext.Order.Where(a => a.OperateUserId == UserID && a.MergeId == 0).ToListAsync();
    }

    public async Task<(string, List<Model>? orders)> GetOrdersByMergeID(int? MergeID)
    {
        if (MergeID is null || MergeID == 0)
        {
            return ("合并订单参数异常！", null);
        }
       
        var orders = await GetOrdersByMIdsAsync(MergeID);
        if (orders is null || orders.Count <= 0)
        {
            return ("该合并订单内没有订单信息！", null);
        }
        return ("", orders);
    }
   

    public async Task<Model?> GetOrderInfoWhereFirst(/*$field = [],*/ int? id = default, string? orderNo = default, int? companyId = default)
    {
        //        empty($field) && $field = ["*"];
        //        return $this->AmazonOrderModel::query()
        //            ->select($field)
        //            ->Id($id)
        //            ->OrderNo($order_no)
        //            ->CompanyId($company_id)
        //            ->first();

        return await _dbContext.Order
            .WhenWhere(id is not null, a => a.ID == id)
            .WhenWhere(orderNo is not null, a => a.OrderNo == orderNo)
            .WhenWhere(companyId is not null, a => a.CompanyID == companyId)
            .FirstOrDefaultAsync();
        //return ReturnArr();
    }

    // 移除合并订单
    public async Task<int> RemoveOrderPending(int? id = null)
    {
        // return checkArr($data) ? $this->AmazonOrderModel::query()->where($data)->update(["merge_id" => -1, "operate_user_id" => 0, "is_main" => 1]) : $this->AmazonOrderModel::query()->where("id", $data)->update(["merge_id" => -1, "operate_user_id" => 0, "is_main" => 1]);
        if (id == null)
        {
            var list = _dbContext.Order.Where(a => a.OperateUserId == UserID && a.MergeId == 0);
            foreach (var item in list)
            {
                item.MergeId = -1;
                item.OperateUserId = 0;
                item.IsMain = IsMains.VICE;
            }
            _dbContext.Order.UpdateRange(list);
        }
        else
        {
            var Finditems = await _dbContext.Order.FindAsync(id);
            if (Finditems != null)
            {
                Finditems.MergeId = -1;
                Finditems.OperateUserId = 0;
                Finditems.IsMain = IsMains.VICE;
                _dbContext.Order.Update(Finditems);
            }
        }
        return await _dbContext.SaveChangesAsync();
    }

    /// <summary>
    /// 修改收货人信息
    /// </summary>
    /// <param name="order"></param>
    /// <returns></returns>
    public async Task<bool> EditInformationBegin(Model order)
    {
        _dbContext.Order.Update(order);
        //添加日志
        if (await _dbContext.SaveChangesAsync() == 0) throw new Exception("修改收货人信息失败");
        await AddLog(order.ID, 0, "修改订单收货人地址");
        return true;
    }

    /// <summary>
    /// 添加内部备注
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public async Task DoAddRemarkCreate(OrderRemarkModel data)
    {
        await _dbContext.OrderRemark.AddAsync(data);
        await _dbContext.SaveChangesAsync();
    }



    public async Task<ReturnStruct> GetMergeLists(string OrderNo)
    {
        return await Task.FromResult(ReturnArr());
    }
    //        //获取手订单续费特定数据
    public ReturnStruct getOrderInfoFee(/*$id*/)
    {
        //            if (
        //            $orderInfo = AmazonOrderModel::query()
        //                ->select(["id", "store_id", "fee", "user_id", "company_id", "coin", "order_state", "order_no", "order_money"])
        //            ->where("id", $id)->first()) {
        //            return $orderInfo;
        //        }
        //        return false;
        return ReturnArr();
    }

    //    /**
    //     * 执行合并订单事务处理
    //     * @param $insertData
    //     * @param $where
    //     * @return bool
    //     */


    public ReturnStruct getOrderInfoPage(/*$id*/)
    {
        //        $field = "id,receive_nation,url,earliest_ship_time,order_no,pay_money,company_id,product_num,order_status,create_time,shipping_type,fake_product_time,true_product_time,store_id,id,coin,level,truename,receiver_name,receiver_email,progress,latest_ship_time,merge_id";
        //        $fieldArr = explode(',', $field);
        //        $list = $this->AmazonOrderModel::query()
        //            ->where("id", "=", $id)
        //            ->select($fieldArr)
        //            ->paginate($this->limit);
        //            return $list;
        return ReturnArr();
    }

    public ReturnStruct getReceiverNameFirst(/*$id*/)
    {
        //        return AmazonOrderModel::query()->select(["receiver_name", "receiver_email"])->where("id", $id)->first();
        return ReturnArr();
    }

    //    /**
    //     * 付款信息
    //     * @param $pwhere
    //     * @return array
    //     */
    public ReturnStruct purchaseInfo(/*$pwhere*/)
    {
        //        $res = PurchaseModel::query()->where($pwhere)->get()->toArray();
        //        return $res;
        return ReturnArr();
    }

    /// <summary>
    /// 待发货订单
    /// </summary>
    /// <returns></returns>
    public async Task<PaginateStruct<Model>> Shipped(DeliverVM deliverVM)
    {

        DateTime StartTime = DateTime.MinValue;
        DateTime LastTime = DateTime.MinValue;
        if (deliverVM.start != null)
        {
            StartTime = deliverVM.start[0];
            LastTime = deliverVM.start[1];
        }
        DateTime IFlatestShipTime = DateTime.MinValue;
        if (deliverVM.latestShipTime != null)
        {
            IFlatestShipTime = deliverVM switch
            {
                { latestShipTime: 1 } => DateTime.Now.AddDays(-3).Date.AddMinutes(0),
                { latestShipTime: 2 } => DateTime.Now.Date.AddMinutes(0),
                _ => Convert.ToDateTime(DateTime.Now.ToString("1970-01-01")),
            };
        }

        var data = await _dbContext.Order
            .Where(x => x.CompanyID == CompanyID)
            .WhenWhere(!string.IsNullOrEmpty(deliverVM.orderNo), x => x.OrderNo == deliverVM.orderNo)
            //.Where(x=>x.WaybillCompanyId == 0 && x.EntryMode == EntryModes.API)
            .WhenWhere(IFlatestShipTime != DateTime.MinValue, x => x.ShippingTime <= IFlatestShipTime)
            .WhenWhere(StartTime != DateTime.MinValue, x => x.CreatedAt >= StartTime)
            .WhenWhere(LastTime != DateTime.MinValue, x => x.CreatedAt <= StartTime)
            .WhenWhere(StartTime != DateTime.MinValue, x => x.CreatedAt < StartTime)
            .Include(x => x.Receiver)
            //.OrderBy(x=>x.OrderState==OrderStates.PENDING)                                //* PROCESSING
            .GetSortBy(deliverVM.sortBy)
            .ToPaginateAsync();

        return data;

    }

    public async Task<Model?> FindAddress(int id)
    {
        //        return AmazonOrderModel::query()->find($id);
        return await _dbContext.Order.FirstOrDefaultAsync(a => a.ID == id);
        //return ReturnArr();
    }

    //    //添加订单数据
    public async Task<int> InsertOrder(Model order)
    {
        //        return $this->AmazonOrderModel::query()->create($insertData);
        await _dbContext.Order.AddAsync(order);

        return await _dbContext.SaveChangesAsync();
    }

    //    /********************************亚马逊订单列表详情页*************************************/

    public Task<List<OrderActionListDto>> GetOrderAction(IEnumerable<int> orderIds)
    {
        return _dbContext.OrderAction.Where(a => orderIds.Contains(a.OrderId))
            .OrderByDescending(a => a.CreatedAt)
            .Select(a => new OrderActionListDto(a.Value, a.Truename, a.CreatedAt))
            .ToListAsync();
    }

    public async Task<List<OrderRemarkModel>> GetOrderRemarks(List<int> orderIds)
    {
        return await _dbContext.OrderRemark
            .Where(a => orderIds.Contains(a.OrderId))
            .Where(a => a.CompanyID == CompanyID)
            .OrderByDescending(a => a.CreatedAt)
            .ToListAsync();
    }
    public Task<List<OrderRemarkListDto>> GetOrderRemarks(IEnumerable<int> orderIds)
    {
        return _dbContext.OrderRemark
            .Where(a => orderIds.Contains(a.OrderId))
            .Where(a => a.CompanyID == CompanyID)
            .OrderByDescending(a => a.CreatedAt)
            .Select(r => new OrderRemarkListDto(r.Value, r.Truename, r.CreatedAt))
            .ToListAsync();
    }

    //获取物流信息
    public async Task<List<WaybillModel>> GetWaybill(/*$field,*/ int order_id, Platforms platform_id)
    {
        //        $list = WaybillModel::query()
        //            ->select($field)
        //            ->orderId($order_id)
        //            ->platformId($platform_id)
        //            ->get();
        //        return $list;
        var list = await _dbContext.Waybill.Where(a => a.OrderId == order_id /*&& a.ElectronicBusiness.Platform == platform_id*/).ToListAsync();
        return list;
    }

    public async Task<List<WaybillModel>> GetWaybill(List<int> waybillIDS, Platforms platform_id)
    {
        return await _dbContext.Waybill.Where(a => waybillIDS.Contains(a.ID)).ToListAsync();
    }

    /// <summary>
    /// 更新订单
    /// </summary>
    /// <param name="orderData"></param>
    /// <returns></returns>
    public async Task<bool> Update(Model orderData)
    {
        _dbContext.Order.Update(orderData);
        await _dbContext.SaveChangesAsync();
        return true;
    }

    public async Task<bool> Updates(List<Model> orderData)
    {
        // return $this->AmazonOrderModel->where("id", $id)->update($updateData);
        _dbContext.UpdateRange(orderData);
        return await _dbContext.SaveChangesAsync() > 0;
        //return ReturnArr();
    }

    public ReturnStruct updateData(/*$where, $updateData*/)
    {
        //        return AmazonOrderModel::query()->where($where)->update($updateData);
        return ReturnArr();
    }

    //    /*************************************已合并订单列表***********************************/


    //public async Task<IList<AmazonOrderModel>> GetOrderMergeDesc(int OperateUserID, int MergeId)
    //{
    //    return await _MyDbContext.AmazonOrder
    //             .Where(x => x.OperateUserId == OperateUserID)
    //             .Where(x => x.MergeId == MergeId)
    //             .OrderByDescending(x => x.IsMain)
    //             .ToListAsync();
    //}
    public async Task<List<Model>?> GetOrderMergeDesc()
    {
        //var Stores = _CacheService.getStoreCompanyCache();
        int MergeId = 0;
        var data = await _dbContext.Order
                 .Where(x => x.OperateUserId == UserID)
                 .Where(x => x.MergeId == MergeId)
                 .Include(x => x.Store)
                 .Include(x => x.Receiver)
                 .OrderByDescending(x => x.IsMain)
                 .ToListAsync();

        if (data != null)
        {
            return data;
        }
        return null;
    }


    public async Task<ReturnStruct> RemoveMergeBegin(string? OrderInfo, Controllers.Orders.Merge.RemoveMerges removeMerges /*$order_info, $merge_id, $order_id*/)
    {
        //var OrderInfos=  JsonConvert.DeserializeObject<JArray>(OrderInfo);

        try
        {
            var MergeOrder = await _dbContext.OrderMerge.Where(x => x.ID == removeMerges.MergeId).ToListAsync();
            MergeOrder.ForEach(x => x.OrderInfo = OrderInfo);
            await _dbContext.SaveChangesAsync();
            var Update = new { MergeId = -1, IsMain = Enums.Orders.IsMains.VICE, OperateUserId = 0 };
            var Updates = await _dbContext.Order.Where(x => x.ID == removeMerges.OrderId).ToListAsync();
            foreach (var item in Updates)
            {
                item.MergeId = -1;
                item.IsMain = Enums.Orders.IsMains.VICE;
                item.OperateUserId = 0;
            }
            if (_dbContext.SaveChanges() > 0)
            {
                return ReturnArr(true);
            }
        }
        catch (Exception)
        {

            return ReturnArr(false);
        }
        return ReturnArr(false);
    }





    //    /**********************************公共函数********************************/


    //    /**
    //     * 更新采购追踪号
    //     * @author ryu <mo5467@126.com>
    //     * @param string $newValue 新值
    //     * @param string $oldValue 旧值
    //     * @param int    $orderId  订单id
    //     * @return void
    //     */
    public static object updatePurchaseTrackingNumber(/*$orderId, $newValue, $oldValue = null*/)
    {
        //        self::updateNumber('purchase_tracking_number', $orderId, $newValue, $oldValue);
        return new object();
    }//end updatePurchaseTrackingNumber()


    //    /**
    //     * 更新运单订单号
    //     * @author ryu <mo5467@126.com>
    //     * @param string $newValue 新值
    //     * @param string $oldValue 旧值
    //     * @param int    $orderId  订单id
    //     * @return void
    //     */
    public static object updateWaybillOrder(/*$orderId, $newValue, $oldValue = null*/)
    {
        //        self::updateNumber('express', $orderId, $newValue, $oldValue);
        return new object();

    }//end updateExpress()


    //    /**
    //     * 更新采购/物流单号
    //     * · 只有新值，旧值为null为 新增
    //     * · 只有旧值，新值为null为 删除
    //     * · 两个都有值为 更新
    //     * @author ryu <mo5467@126.com>
    //     * @param int    $orderId  订单id
    //     * @param string $newValue 新值
    //     * @param string $oldValue 旧值
    //     * @param string $key      purchase_tracking_number 采购， express 运单
    //     * @return void
    //     */
    private static object updateNumber(/*$key, $orderId, $newValue, $oldValue = null*/)
    {
        //        $numArr = AmazonOrderModel::where('id', $orderId)->value($key) ?: [];

        //        if (!$oldValue) {
        //            // 添加一条记录.
        //            array_push($numArr, $newValue);
        //        }

        //        $num = join(',', array_unique($numArr));
        //        if ($key === 'express') {
        //            PurchaseModel::where('order_id', $orderId)->update([$key => $num]);
        //        } else if ($key === 'purchase_tracking_number') {
        //            WayBill::where('order_id', $orderId)->update([$key => $num]);
        //        }
        //        AmazonOrderModel::where('id', $orderId)->update([$key => $num]);
        return new object();
    }//end updateNumber()

    /// <summary>
    /// 获取MFN订单
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    public Task<List<Model>> GetMfnOrderByIds(IEnumerable<int> ids)
    {
        //        $field = ['id', 'order_no', 'receiver_nation', 'url', 'product_num', 'pay_money', 'coin', 'order_state', 'progress', 'company_id', 'create_time', 'latest_ship_time', 'store_id', 'purchase_tracking_number', 'rate'];

        return _dbContext.Set<Model>().Where(o => o.ShippingType == ShippingTypes.MFN && ids.Contains(o.ID))
            .ToListAsync();
    }

    /// <summary>
    /// 获取MFN 拉取订单订单
    /// </summary>
    /// <param name="ids"></param>
    /// <param name="shippingType"></param>
    /// <returns></returns>
    public async Task<List<Model>> GetAllOrderByIds(List<int> ids, ShippingTypes? shippingType)
    {
        return await _dbContext.Order.WhenWhere(shippingType.IsNotNull(), a => a.ShippingType == shippingType)
            .Include(a => a.Receiver)
            .Where(a => ids.Contains(a.ID))
            .ToListAsync();
    }

    public async Task<List<Model>> GetOrdersByHashs(List<ulong> hashs)
    {
        return await _dbContext.Order
        .Include(a => a.Receiver)
            .Where(a => hashs.Contains(a.OrderNoHash))
            .ToListAsync();
    }


    /// <summary>
    /// 设置为主订单
    /// </summary>
    public async Task<ReturnStruct> IsMainOne(int Id)
    {

        try
        {
            // AmazonOrderModel::query()->where("operate_user_id", $this->getSessionObj()->user_id)->update(["is_main" => 1]);
            //    AmazonOrderModel::query()->where("id", $id)->update(["is_main" => 2]);
            var WhereUser = await _dbContext.Order.Where(x => x.OperateUserId == UserID).ToListAsync();
            if (WhereUser != null)
            {
                WhereUser.ForEach(x => x.IsMain = Enums.Orders.IsMains.VICE);
            }
            var WhereOrder = await _dbContext.Order.Where(x => x.ID == Id).SingleOrDefaultAsync();
            if (WhereOrder != null)
            {
                if (WhereOrder.IsMain == Enums.Orders.IsMains.THELORD)
                {
                    return ReturnArr(false, "已设置为是主订单");
                }
                WhereOrder.IsMain = Enums.Orders.IsMains.THELORD;
            }
            if (await _dbContext.SaveChangesAsync() > 0)
            {
                return ReturnArr(true, "设置主订单成功");
            }
            return ReturnArr(false, "设置主订单失败");
        }
        catch (Exception)
        {
            return ReturnArr(false, "请求错误");
        }
        //return ReturnArr(false, "请求错误");

    }

    public Task<Model?> GetOrderById(int id) => _dbContext.Order.SingleOrDefaultAsync(x => x.ID == id);

    /**********************************采购新增、编辑关联订单********************************/
    public enum AsyncType
    {
        [Description("新增")]
        Create,
        [Description("编辑")]
        Edit,
        [Description("取消")]
        Cancel
    }
    /// <summary>
    /// 订单和采购 同步(采购花费/采购数量)
    /// </summary>
    /// <param name="orderId"></param>
    /// <param name="purchaseId"></param>
    /// <param name="purchaseFee_old"></param>
    /// <param name="purchaseFee_new"></param>
    /// <param name="num_old"></param>
    /// <param name="num_new"></param>
    /// <returns></returns>
    public async Task UpdateOrderPurchase(int orderId, MoneyRecordFinancialAffairs FeeOld, MoneyRecordFinancialAffairs FeeNew, int num_old, int num_new)
    {
        var order = await GetOrderPurchaseByIdAsync(orderId);
        string unit = order.OrderTotal.Raw.CurrencyCode;
        var Fee_new = FeeNew.Money.To(unit);
        var Fee_old = FeeOld.Money.To(unit);

        order.Bits = BITS.Progresses_PurchaseORreport;
        //order.Bits |= BITS.Progresses_PurchaseORreport;
        //order.Bits &= ~BITS.Progresses_Unprocessed; // 置为0

        Money amount = new Ryu.Data.Money();
        order.PurchaseTrackingNumber = "[]";
        var Purchases = order.Purchases.Where(x => x.PurchaseAffirm != PurchaseAffirms.Cancel).ToList();
        if (Purchases != null && Purchases.Count > 0)
        {
            foreach (var purchase in Purchases)
            {
                amount += purchase.PurchaseTotal.Money;
            }
            order.PurchaseTrackingNumber = JsonConvert.SerializeObject(Purchases.Where(x => !string.IsNullOrWhiteSpace(x.PurchaseTrackingNumber)).Select(x => x.PurchaseTrackingNumber));
        }
        var newa = new MoneyMeta(unit, amount);
        decimal m = amount.To(unit);
        if (order.PurchaseFee.Raw.CurrencyCode == "")  //新建
        {
            order.PurchaseFee = new MoneyMeta(unit, m);

        }
        else
        {  //修改
            order.PurchaseFee = order.PurchaseFee.Next(unit, m);//负数
        }
        Money amountProfit = new Ryu.Data.Money();
        amountProfit = order.OrderTotal.Money - order.Fee.Money - order.Refund.Money - order.ShippingMoney.Money - order.Loss.Money - order.PurchaseFee.Money;       
       // decimal p = Math.Round(amountProfit.To(unit), 2);
        decimal p = Math.Round(amountProfit / order.OrderTotal.Raw.Rate, 2);

        if (order.Profit.Raw.CurrencyCode == "")
        {
            order.Profit = new MoneyMeta(unit, p);
        }
        else
        {
            order.Profit = order.Profit.Next(unit, p);
        }
        order.PurchaseNum = order.PurchaseNum - num_old + num_new;

        #region 修改数据-订单获利跟踪

        await _statisticMoney.OrderProfitEdit(order);

        #endregion

        #region 修改数据-订单采购费用

        await _statisticMoney.OrderPurchaseFeeEdit(order);

        #endregion

        await _dbContext.SaveChangesAsync();
    }

    /// <summary>
    /// 订单和运单 同步(运费)
    /// </summary>
    /// <param name="orderId"></param>
    /// <param name="purchaseId"></param>
    /// <param name="purchaseFee_old"></param>
    /// <param name="purchaseFee_new"></param>
    /// <param name="num_old"></param>
    /// <param name="num_new"></param>
    /// <returns></returns>
    //public async Task UpdateOrderWaybill(int? orderId, bool f = false)
    //{
    //    List<int> expressLst = new();
    //    Money mergeShippFee = new Ryu.Data.Money();
    //    var order = await GetOrderWaybillByIdAsync(orderId);
    //    order.Express = "[]";
    //    string unit = order.OrderTotal.Raw.CurrencyCode;       
    //    if (!f)
    //    {
    //        order.Bits = BITS.Progresses_PurchaseORreport;
    //    }
    //    if(order.MergeId>0)
    //    {
    //        //合并订单的相关运单
    //        var mergeWaybills = await WayBillService.GetMergeWaybillList(order.MergeId, order.ID);
    //        if (mergeWaybills.IsNotNull() && mergeWaybills.Count > 0)
    //        {
    //            List<int> mergeWaybillIDs = new();
    //            foreach (var mergeWaybill in mergeWaybills)
    //            {
    //                mergeWaybillIDs.Add(mergeWaybill.ID);
    //                //计算合并运单的运费，shippMoney/Order count
    //                if (!string.IsNullOrEmpty(mergeWaybill.OrderIDs))
    //                {
    //                    var ids = mergeWaybill.OrderIDs.Split(",").Cast<int>().ToList();
    //                    mergeShippFee += mergeWaybill.CarriageNum.Money / ids.Count;
    //                }
    //            }
    //            expressLst.AddRange(mergeWaybillIDs);
    //        }
    //    }
    //    var waybills = order.Waybills.Where(x => x.State != ERP.Enums.Logistics.States.Cancle).ToList();
        
      
    //    if (waybills != null && waybills.Count > 0)
    //    {
    //        foreach (var money in waybills)
    //        {
    //            amount += money.CarriageNum.Money;
    //        }

    //        order.Express = JsonConvert.SerializeObject(waybills.Select(x => x.ID));
    //    }

    //    decimal m = amount.To(unit);

    //    if (order.ShippingMoney.Raw.CurrencyCode == "")
    //    {
    //        order.ShippingMoney = new MoneyMeta(unit, m);
    //    }
    //    else
    //    {
    //        order.ShippingMoney = order.ShippingMoney.Next(unit, m);
    //    }
    //    Money amountProfit = order.OrderTotal.Money - order.Fee.Money - order.Refund.Money - order.ShippingMoney.Money - order.Loss.Money - order.PurchaseFee.Money;
    //    decimal p = amountProfit.To(unit);
    //    if (order.Profit.Raw.CurrencyCode == "")
    //    {
    //        order.Profit = new MoneyMeta(unit, p);
    //    }
    //    else
    //    {
    //        order.Profit = order.Profit.Next(unit, p);
    //    }

    //    #region 修改数据-订单获利跟踪

    //    await _statisticMoney.OrderProfitEdit(order);

    //    #endregion

    //    #region 修改数据-订单运费

    //    await _statisticMoney.OrderShippingMoneyEdit(order);

    //    #endregion

    //    await _dbContext.SaveChangesAsync();
    //}

   

    /// <summary>
    /// 完成采购更新订单状态
    /// </summary>
    /// <param name="order"></param>
    /// <param name="info"></param>
    /// <param name="money"></param>
    /// <param name="requestData"></param>
    /// <param name="isAdd"></param>
    /// <returns></returns>
    public async Task UpdateOrderByPurchase(Model order, PurchaseModel info, Money money, PurchaseEditVM requestData, bool isAdd = true)
    {
        var profitSid = order.Profit.FinancialAffairsID;
        if (isAdd)// 判断采购是增加的还是删除的???
        {
            order.PurchaseFee += money;
            order.Profit -= money;
        }
        else
        {
            order.PurchaseFee -= money;
            order.Profit += money;
        }

        if (info.PurchaseState == PurchaseStates.PURCHASED)
        {
            if (info.Num > requestData.Num)
            {
                var num = info.Num - requestData.Num;
                //order.PurchaseNum = ???
            }
        }
        else
        {

        }

        try
        {

            #region 修改数据-订单获利跟踪

            await _statisticMoney.OrderProfitEdit(order);

            #endregion

            #region 修改数据-订单采购费用

            await _statisticMoney.OrderPurchaseFeeEdit(order);

            #endregion

            await _dbContext.SaveChangesAsync();
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            throw new Exceptions.Exception(e.Message);
        }
    }




    public async Task<Model> GetOrderWaybillByIdAsync(int? id)
    {
        var order = await _dbContext.Order.Include(a => a.Receiver).Include(a => a.Store).Include(x => x.Waybills).SingleOrDefaultAsync(a => a.ID == id);
        if (order == null) throw new Exception($"未找到ID为{id}的订单");
        return order;
    }

    public async Task<Model> GetOrderPurchaseByIdAsync(int? id)
    {
        var order = await _dbContext.Order.Include(a => a.Receiver).Include(a => a.Store).Include(x => x.Purchases).SingleOrDefaultAsync(a => a.ID == id);
        if (order == null) throw new Exception($"未找到ID为{id}的订单");
        return order;
    }

    public async Task<Model> GetOrderByIdNoTrackAsync(int? id)
    {
        var order = await _dbContext.Order.Where(a => a.ID == id).AsNoTracking().ToListAsync();
        if (order == null) throw new Exception($"未找到ID为{id}的订单");
        return order[0];
    }

    public async Task<Model> GetOrderInfo(int? id)
    {
        var order = await _dbContext.Order.Include(a => a.Store).SingleOrDefaultAsync(a => a.ID == id);
        if (order == null) throw new Exception($"未找到ID为{id}的订单");
        return order;
    }

    public async Task<List<Model>> GetOrdersByIdsAsync(List<int> OrderIds)
    {
        return await _dbContext.Order.Where(x => OrderIds.Contains(x.ID))
                 .Include(x => x.Store)
                 .Include(x => x.Receiver).ToListAsync();
    }



    /// <summary>
    /// 添加订单日志
    /// </summary>
    /// <param name="orderId"></param>
    /// <param name="orderStatus"></param>
    /// <param name="action"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    public async Task<bool> AddLog(int orderId, States orderStatus = 0, string action = "", string DescText = "", int OemId = -1, int CommpanyId = -1)
    {
        if (OemId == -1) OemId = OEMID;
        if (CommpanyId == -1) CommpanyId = CompanyID;
        if (DescText.IsNullOrEmpty()) DescText = Extensions.EnumExtension.GetDescription(orderStatus);
        var data = new OrderActionModel
        {
            UserID = UserID,
            GroupID = GroupID,
            CompanyID = CommpanyId,
            OEMID = OemId,
            OrderId = orderId,
            Value = action,
            UserName = Session.GetUserName(),
            Truename = Session.GetTrueName(),
            OrderStatus = orderStatus,
            OrderStatusText = DescText,
            CreatedAt = DateTime.Now
        };
        await _dbContext.OrderAction.AddAsync(data);
        if (await _dbContext.SaveChangesAsync() == 0) throw new Exception("添加订单日志失败");
        return true;
    }
    public async Task<bool> AddLog(Model order, User user, string action)
    {
        var data = new OrderActionModel(action, user, order);
        await _dbContext.OrderAction.AddAsync(data);
        //await _dbContext.SaveChangesAsync();
        return true;
    }

    public async Task<AddressModel> GetAddressById(int id)
    {
        var address = await _dbContext.Address.SingleOrDefaultAsync(a => a.ID == id);
        if (address == null) throw new Exception("未找到地址");
        return address;
    }

    public async Task<AddressModel> GetAddressByOrderId(int orderId)
    {
        var order = await GetOrderByIdAsync(orderId);
        //var address = await _dbContext.Address.SingleOrDefaultAsync(a => a.ID == order.ReceiverID);
        if (order.Receiver == null) throw new Exception("未找到地址");
        return order.Receiver;
    }

    public async Task<bool> EditAddress(AddressModel address)
    {
        _dbContext.Address.Update(address);
        //_dbContext.Entry(address.Order).State = EntityState.Detached;
        if (await _dbContext.SaveChangesAsync() == 0)
        {
            throw new Exception("更新地址失败");
        };
        return true;
    }


    public void CountGoods(Model order, List<ERP.Models.Api.Logistics.GoodInfo> GoodInfos, SendType sendType, string type = "add")
    {
        order.GoodsScope(goods => goods.ForEach(good =>
        {
            var g = GoodInfos.SingleOrDefault(a => a.goodId == good.OrderItemId);
            if (g != null)
            {
                if (type == "add")
                {
                    if (sendType == SendType.Auto)
                    {
                        order.DeliverySuccess++;
                    }
                    good.QuantityShipped += g.Count;
                    order.Delivery += g.Count;
                }
                else
                {
                    good.QuantityShipped -= g.Count;
                    order.Delivery -= g.Count;
                }
            }
        }), Modes.Save);
    }






    /// <summary>
    /// 批量修改订单进度
    /// </summary>
    /// <param name="rq"></param>
    /// <returns></returns>
    public async Task<bool> BatchChangeState(ChangeStateVM rq)
    {
        var orders = _dbContext.Order.Where(a => rq.OrderIds.Contains(a.ID));
        if (orders == null)
        {
            return false;
        }
        foreach (var order in orders)
        {
            order.SetBits(rq.OrderProgress, rq.State);
        }
        return (await _dbContext.SaveChangesAsync()) > 0;
    }



    private List<int> getTrueWaybillOrderIds(Model order, Models.DB.Orders.Merge merge)
    {
        if ((order.IsMain == IsMains.THELORD && merge.Type != MergeTypes.PublicOrder) || merge.Type == MergeTypes.IndependenceOrder)
        {
            return new List<int>(order.ID);
        }
        var GetOrderInfoIds = JsonConvert.DeserializeObject<List<MergeOrderInfo>>(merge.OrderInfo ?? string.Empty) ?? new();
        if (GetOrderInfoIds.Count <= 0 && GetOrderInfoIds is null)
        {
            return new List<int>();
        }
        if (merge.Type == MergeTypes.PublicOrder)
        {
            return GetOrderInfoIds.Select(x => x.order_id).ToList();
        }
        var main_id = GetOrderInfoIds.ToDictionary(x => x.is_main)[IsMains.THELORD].order_id;
        if (merge.Type == MergeTypes.MainOrder)
        {
            return new List<int>(main_id);
        }
        List<int> ids = new();
        ids.Add(order.ID);
        ids.Add(main_id);
        return ids;
    }


    //同步亚马逊
    //public async Task<string> DeliverGoods(Model order, DeliveryGoodVM deliveryGood)
    //{
    //    if (order.State == ERP.Enums.Orders.States.CANCELED)
    //    {
    //        return "当前订单状态无法同步";
    //    }

    //    if (Enum.IsDefined(typeof(Enums.Orders.States), deliveryGood.OrderStatus) &&
    //        Enum.TryParse<Enums.Orders.States>(deliveryGood.OrderStatus.ToString(), out var OrderStatusEnum))
    //    {
    //        if (OrderStatusEnum.IsNull())
    //        {
    //            return "订单状态有误";
    //        }
    //    }
    //    else
    //    {
    //        return "订单状态有误";
    //    }
    //    var store = await _storeService.GetInfoById(order.StoreId);
    //    if (store.IsNull())
    //    {
    //        return "未找到店铺";
    //    }
    //    if (store!.Platform != Platforms.AMAZON && store.Platform != Platforms.AMAZONSP)
    //    {
    //        return "这不是亚马逊的订单";
    //    }

    //    List<int> orderIds = new List<int>();
    //    List<DeliverProduct> deliverProducts = new();
    //    if (!deliveryGood.HandWrite)
    //    {
    //        // 运单信息
    //        if (order.MergeId > 0)
    //        {
    //            //orderMerge 查询
    //            var merge = await MergeService.GetOrderMergeListFirst(order.MergeId);
    //            if (merge is null)
    //            {
    //                return "未找到合并的订单信息";
    //            }
    //            orderIds.AddRange(getTrueWaybillOrderIds(order, merge));
    //        }
    //        else
    //        {
    //            //取orderid
    //            orderIds.Add(order.ID);
    //        }
    //        var time = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ");

    //        var waybills = await WayBill.getWaybillByOrderIdList(orderIds);
    //        if (waybills is null || waybills.Count <= 0)
    //        {
    //            return "没有运单信息";
    //        }
    //        foreach (var waybill in waybills)
    //        {
    //            //转成对象 
    //            var orderInfo = JsonConvert.DeserializeObject<List<ProductInfoItem>>(waybill.ProductInfo ?? string.Empty);
    //            if (orderInfo is not null && orderInfo.Count > 0)
    //                foreach (var value in orderInfo)
    //                {
    //                    var tracking = waybill.Tracking.IsNullOrEmpty() ? waybill.WaybillOrder : waybill.Tracking;
    //                    var goodid = value.GoodInfos.goodId.IsNullOrEmpty() ? "" : value.GoodInfos.goodId;

    //                    deliverProducts.Add(new DeliverProduct(order.OrderNo ?? string.Empty, time, deliveryGood.CarrierName,
    //                        waybill.ShipMethod,
    //                        tracking ?? string.Empty, goodid, value.GoodInfos.Num));
    //                }
    //        }
    //    }
    //    else
    //    {   //选择手动填写
    //        if (deliveryGood.ShippingMethod.IsNullOrWhiteSpace() || deliveryGood.TrackingNumber.IsNullOrWhiteSpace())
    //        {
    //            return "请输入货运方式以及运单号";
    //        }
    //        var time = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ssZ");
    //        // 获取订单下的商品信息
    //        var goods = order.GoodsScope();
    //        if (goods.IsNull() || goods.Count <= 0)
    //        {
    //            return "没有找到购买信息";
    //        }
    //        foreach (var good in goods)
    //        {
    //            deliverProducts.Add(new DeliverProduct(order.OrderNo ?? "暂无订单编号", time, deliveryGood.CarrierName, deliveryGood.ShippingMethod, deliveryGood.TrackingNumber, good.ID ?? "暂无商品信息", good.QuantityShipped));
    //        }
    //    }
    //    var _ApiOrderServices = ApiOrderFactory.Create(store.Platform);

    //    if (store.Platform != Platforms.AMAZONSP)
    //    {
    //        return $"{store.Platform.GetDescription()}平台未实现";           
    //    }
    //    List<newDeliverProduct> newList = new();
    //    foreach (var item in deliverProducts)
    //    {
    //        newDeliverProduct newP = new newDeliverProduct(item.AmazonOrderID, item.FulfillmentDate, item.CarrierName, item.ShippingMethod, item.ShipperTrackingNumber, item.AmazonOrderItemCode, item.Quantity);
    //        newList.Add(newP);
    //    }
    //    string requestID = await _ApiOrderServices.DeliveryGoods(store, newList, order.MarketPlace);
    //    if (requestID.IsNullOrWhiteSpace())
    //    {
    //        return "推送亚马逊接口失败";
    //    }
    //    else
    //    {
    //        //update order
    //        var status = deliveryGood.OrderStatus < 0 ? 1 : deliveryGood.OrderStatus;
    //        order.State = OrderStatusEnum;
    //        bool b = await Update(order);
    //        if (b)
    //        {
    //            //order.SetBits(BITS.Waybill_HasSync);
    //            await AddLog(order.ID, OrderStatusEnum, "更新订单状态");
    //        }
    //        else
    //        {
    //            return "更新订单状态失败";
    //        }
    //        return "";
    //    }
    //}

    public async Task<string> SyncAmazon(int? orderId, SyncToAmazon syncAmazon, string express, string? trackNum,bool handWrite=true)
    {
        int u = UserID;
        try
        {
            DeliveryGoodVM deliveryGood = new DeliveryGoodVM()
            {
                ID = orderId,
                HandWrite = handWrite,
                CarrierName = syncAmazon.CarrierName,
                ShippingMethod = syncAmazon.ShippingMethod,
                TrackingNumber = string.IsNullOrWhiteSpace(trackNum) ? express : trackNum,
            };
            var result = await NewDeliverGoods(deliveryGood);
            if (!string.IsNullOrWhiteSpace(result.Item1))
            {
                return result.Item1;
            }
            DeliveryGoodsInfo deliveryGoodsInfo = result.Item2;
            if (deliveryGoodsInfo != null)
            {
                var listener = (ilistener as TaskCompletedListener)!;
                var guid = Guid.NewGuid();


                listener.AddTask(guid);
                queue.QueueTask(async () =>
                {
                    try
                    {
                        await orderHostService.DeliverGoods(u, deliveryGoodsInfo);
                    }
                    catch (Exception e)
                    {
                        _logger.LogError($"DeliverGoods  Exception:{e.Message}");
                        _logger.LogError($"DeliverGoods  Exception:{e.StackTrace}");
                    }
                    listener.RunningTasks[guid] = true;
                });
            }
            else
            {
                return "同步运单参数出错！";
            }
        }
        catch (Exception ex)
        {
            _logger.LogError($"DeliverGoods  Exception:{ex.Message}");
            _logger.LogError($"DeliverGoods  Exception:{ex.StackTrace}");
        }
        return "";
    }




    public async Task<(string, DeliveryGoodsInfo?)> NewDeliverGoods(DeliveryGoodVM deliveryGood)
    {

        //为什么需要判断订单状态  to do...
        //if (deliveryGood.OrderStatus is not null)  //需要判断订单状态
        //{
        //    if (Enum.IsDefined(typeof(Enums.Orders.States), deliveryGood.OrderStatus) &&
        //        Enum.TryParse<Enums.Orders.States>(deliveryGood.OrderStatus.ToString(), out var OrderStatusEnum))
        //    {
        //        if (OrderStatusEnum.IsNull())
        //        {
        //            return ReturnArr(false, "订单状态有误");
        //        }
        //    }
        //    else
        //    {
        //        return ReturnArr(false, "订单状态有误");
        //    }
        //}

        var order = await GetOrderInfo(deliveryGood.ID);
        if (order.State == ERP.Enums.Orders.States.CANCELED)
        {
            return ("该订单已经被撤销", null);
        }
        var store = await _storeService.GetInfoById(order.StoreId);
        if (store.IsNull())
        {
            return ("未找到店铺", null); 
        }
        if (store!.Platform != Platforms.AMAZONSP)
        {
            return ($"暂未对接该{store.Platform.GetDescription()}平台", null);           
        }

        List<int> orderIds = new List<int>();
        List<DeliverProduct> deliverProducts = new();
        string time = "";
        if (order.LatestShipTime.IsNull())
        {
            var t = order.CreateTime.AddHours(-8).AddDays(1);
            var temp = t > DateTime.UtcNow ? DateTime.UtcNow.AddMilliseconds(-3) : t;
            time = temp.ToString("yyyy-MM-ddTHH:mm:ssZ");
        }
        else
        {
            var t = ((DateTime)order.LatestShipTime!).AddHours(-8).AddMilliseconds(-3); //utc时间格式,提前3mins          
            var temp = t > DateTime.UtcNow ? DateTime.UtcNow.AddMilliseconds(-3) : t;
            time = temp.ToString("yyyy-MM-ddTHH:mm:ssZ");           
        }

        if (!deliveryGood.HandWrite)
        {
            //运单信息，根据orderid查找的+根据mergeid查找的，其中orderids中存在该orderid的运单，汇总           
            var wids = await WayBillService.GetWaybillIDsByOrder(order.ID, order.MergeId, false);
            if (wids is null || wids.Count <= 0)
            {
                return ("没有运单信息", null);
            }
            var waybills = await WayBillService.GetWaybill(wids);                       
            foreach (var waybill in waybills)
            {
                var orderInfo = JsonConvert.DeserializeObject<List<ProductInfoItem>>(waybill.ProductInfo ?? string.Empty);
                if (orderInfo is null || orderInfo.Count <= 0)
                {
                    return ("运单信息中没有商品信息", null);
                }
                foreach (var value in orderInfo)
                {
                    var tracking = waybill.Tracking.IsNullOrEmpty() ? waybill.Express : waybill.Tracking;
                    var goodid = value.GoodInfos.goodId.IsNullOrEmpty() ? "" : value.GoodInfos.goodId;
                    deliverProducts.Add(new DeliverProduct(order.OrderNo ?? string.Empty, time, deliveryGood.CarrierName, waybill.ShipMethod, tracking ?? string.Empty, goodid, value.GoodInfos.Num));
                }
            }
        }
        else
        {   //选择手动填写                       
            //获取订单下的商品信息
            var goods = order.GoodsScope();
            if (goods.IsNull() || goods.Count <= 0)
            {
                return ("该订单内没有商品信息", null);
            }
            foreach (var good in goods)
            {
                deliverProducts.Add(new DeliverProduct(order.OrderNo ?? "暂无订单编号", time, deliveryGood.CarrierName, deliveryGood.ShippingMethod, deliveryGood.TrackingNumber, good.ID ?? "暂无商品信息", good.QuantityShipped));
            }
        }
        DeliveryGoodsInfo deliveryGoodsInfo = new DeliveryGoodsInfo(store, deliverProducts, order.MarketPlace, deliveryGood);
        return ("", deliveryGoodsInfo);
    }


    /// <summary>
    /// 订单关联的自定义标记(批量)
    /// </summary>
    /// <param name="orderIds"></param>
    /// <param name="cm"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    public async Task SetOrderCustomMark(List<int> orderIds, CustomMarkJson cm)
    {
        var orderList = await _dbContext.Order.Where(c => orderIds.Contains(c.ID)).ToListAsync();
        orderList.ForEach(item =>
        {
            item.SetCustomMark(cm);
        });
        _dbContext.UpdateRange(orderList);
        await _dbContext.SaveChangesAsync();
    }

    //public async Task UploadInvoice(Model order, StoreModel store)
    //{
    //    //判断是不是亚马逊的
    //    //if (store.GetAmazonData().AWSAccessKeyId.IsNull())
    //    //{
    //    var CacheAmazonKey = serviceProvider.GetRequiredService<Caches.AmazonKey>();
    //    var CachePlatformData = serviceProvider.GetRequiredService<PlatformDataCache>();
    //    store.InjectCache(CacheAmazonKey, CachePlatformData);
    //    //}
    //    Models.DB.Stores.StoreRegion.AmazonData md = store.GetAmazonData()!;
    //    var _ApiOrderServices = ApiOrderFactory.Create(order.Plateform);

    //    // var response = await _ApiOrderServices.GetFeesEstimate(store, order);
    //    //amazonsp  to  do...
    //    var tuple = _ApiOrderServices.UploadInvoice(store, order);
    //    ERP.Models.Invoice invoice = new ERP.Models.Invoice(tuple.Item1, tuple.Item2);
    //    order.Invoice = JsonConvert.SerializeObject(invoice);
    //    order.SetBits(BITS.Waybill_HasUploadInvoice);
    //    _dbContext.Order.Update(order);
    //    await _dbContext.SaveChangesAsync();
    //}


    public async Task SumOrderSkuAndAsin(Model[] order) => await SumOrderSkuAndAsinInternal(order);
    public async Task SumOrderSkuAndAsin(Model order) => await SumOrderSkuAndAsinInternal(order);
    private async Task SumOrderSkuAndAsinInternal(Model[] models)
    {
        foreach (var model in models)
        {
            await SumOrderSkuAndAsinInternal(model);
        }
    }

    /// <summary>
    /// 新增订单汇总sku和goodid
    /// </summary>
    /// <param name="order"></param>
    /// <returns></returns>
    public async Task SumOrderSkuAndAsinInternal(Model order)
    {
        if (order.ID <= 0) return;
        var goods = order.GoodsScope().ToList();
        foreach (var good in goods)
        {
            var orderGoodSku = await _dbContext.OrderGoodSku.FirstOrDefaultAsync(a => a.Hash == Helpers.CreateHashCode(good.Sku ?? string.Empty));
            if (orderGoodSku == null)
            {
                await _dbContext.OrderGoodSku.AddAsync(new(order.ID, good));
            }
            else
            {
                orderGoodSku.Update(order.ID, good.QuantityOrdered);
            }
            var orderGoodId = await _dbContext.OrderGoodId.FirstOrDefaultAsync(a => a.Hash == Helpers.CreateHashCode(good.ID ?? string.Empty));
            if (orderGoodId == null)
            {
                await _dbContext.OrderGoodId.AddAsync(new(order.ID, good));
            }
            else
            {
                orderGoodId.Update(order.ID, good.QuantityOrdered);
            }
        }
        await _dbContext.SaveChangesAsync();
        //order.GoodsScope(goods =>
        //{
        //    goods.ForEach(async good =>
        //    {
        //        var orderGoodSku = await _dbContext.OrderGoodSku.FirstOrDefaultAsync(a => a.Hash == Helpers.CreateHashCode(good.Sku ?? string.Empty));
        //        if (orderGoodSku == null)
        //        {

        //            _dbContext.OrderGoodSku.Add(new(order.ID, good));
        //        }
        //        else
        //        {
        //            orderGoodSku.Update(order.ID, good.QuantityOrdered);
        //        }
        //        var orderGoodId = await _dbContext.OrderGoodId.FirstOrDefaultAsync(a => a.Hash == Helpers.CreateHashCode(good.ID ?? string.Empty));
        //        if (orderGoodId == null)
        //        {

        //            _dbContext.OrderGoodId.Add(new(order.ID, good));
        //        }
        //        else
        //        {
        //            orderGoodId.Update(order.ID, good.QuantityOrdered);
        //        }
        //        await _dbContext.SaveChangesAsync();
        //    });
        //});
    }


    public record SalesRanking
    {
        public string GoodId;
        public string Name;
        public string? Sku;
        public string? ImageURL;
        public string? Brand;
        public string? Size;
        public string? Color;
        public int Number;
    }
    /// <summary>
    /// 平台订单产品销量排行
    /// </summary>
    /// <returns></returns>
    public async Task<List<SalesRanking>> OrderProductSalesRanking(OrderProductSalesRankingDto request)
    {
        // 平台订单产品销量排行：按订单产品数根据订单产品唯一asin聚集从高到低排序（考虑新开菜单入口）
        // SKU是否唯一     goodId/asin 唯一
        // 根据时间段、店铺、SKU\ASIN搜索
        // 不分页
        List<int> IDS = new();
        // 根据sku或asin搜
        if (!request.Sku.IsNullOrWhiteSpace())
        {
            var sku = await _dbContext.OrderGoodSku.FirstOrDefaultAsync(a => a.Value == request.Sku!);
            if (sku == null) return new();
            IDS.AddRange(sku.GetOrderIds());
        }
        if (!request.GoodId.IsNullOrWhiteSpace())
        {
            var goodId = await _dbContext.OrderGoodId.FirstOrDefaultAsync(a => a.Value == request.GoodId!);
            if (goodId == null) return new();
            IDS.AddRange(goodId.GetOrderIds());
        }
        //var goods = await _dbContext.OrderGoodId.OrderByDescending(a => a.Number).ToListAsync();
        var store = await _storeService.CurrentUserGetStores();
        var storeIds = store.Select(a => a.ID);
        var orders = await _dbContext.Order
            .Where(a => a.CompanyID == CompanyID)
            .Where(a => storeIds.Contains(a.StoreId))
            .WhenWhere(request.StoreId > 0, a => a.StoreId == request.StoreId)
            .WhenWhere(IDS.Count > 0, a => IDS.Contains(a.ID))
            .WhenWhere(request.StartAndEndTime != null && request.StartAndEndTime.Count == 2, a => a.CreateTime >= request.StartAndEndTime![0] && a.CreateTime < request.StartAndEndTime![1].Date.AddDays(1))
            .ToListAsync();
        var goodsList = orders.SelectMany(a => a.GoodsScope());
        //var x = goodsList.GroupBy(a => new { a.ID, a.Sku, a.ImageURL, a.Brand, a.Color, a.Size }).Select(g => new
        //{
        //    GoodId = g.Key.ID,
        var list = goodsList
            .WhenWhere(!string.IsNullOrEmpty(request.Sku), m => m.Sku == request.Sku)
            .WhenWhere(!string.IsNullOrEmpty(request.GoodId), m => m.ID == request.GoodId)
            .GroupBy(a => a.ID)
            .Select(g => new SalesRanking
            {
                GoodId = g.Key ?? "",
                // ...
                Name = goodsList.First(a => a.ID == g.Key).Name,
                Sku = goodsList.First(a => a.ID == g.Key).Sku,
                ImageURL = goodsList.First(a => a.ID == g.Key).ImageURL,
                Brand = goodsList.First(a => a.ID == g.Key).Brand,
                Size = goodsList.First(a => a.ID == g.Key).Size,
                Color = goodsList.First(a => a.ID == g.Key).Color,
                Number = g.Sum(a => a.QuantityOrdered),
            }).OrderByDescending(a => a.Number).Take(100).ToList();
        return list;
    }
    
    public async Task<bool> AddSyncLogisticInfoLog(DeliveryGoodVM vm)
    {
        if (!vm.Country.IsNullOrWhiteSpace())
        {
            var info = await _dbContext.SyncLogisticInfoLog.Where(m => m.UserID == UserID).FirstOrDefaultAsync();
            if (info == null)
            {
                var data = new SyncLogisticInfoLogModel(vm,UserID);
                await _dbContext.SyncLogisticInfoLog.AddAsync(data); 
            }
            else
            {
                info.CarrierName = vm.CarrierName;
                info.Country = vm.Country!;
                info.HandWrite = vm.HandWrite;
                info.ShippingMethod = vm.ShippingMethod;
                await _dbContext.SyncLogisticInfoLog.SingleUpdateAsync(info); 
            }
            await _dbContext.SaveChangesAsync();
            return true;
        }

        return false;
    }

    public async Task<SyncLogisticInfoLogModel?> LoadLastLogisticLog()
    {
        return await _dbContext.SyncLogisticInfoLog.Where(m => m.UserID == UserID).FirstOrDefaultAsync();
    }
}

public static class OrderQueryExtension
{
    /// <summary>
    /// 获取订单列表排序条件
    /// </summary>
    /// <param name="model"></param>
    /// <param name="sortBy"></param>
    /// <returns></returns>
    public static IQueryable<Model> GetSortBy(this IQueryable<Model> model, int? sortBy) =>
        model.GetSortBy(Enum.TryParse<SortBy>(sortBy.ToString(), out var sort) ? null : sort);

    public static IQueryable<Model> GetSortBy(this IQueryable<Model> model, SortBy? sortBy)
    {
        IQueryable<Model> res = sortBy switch
        {
            SortBy.CreateAtDESC => model.OrderByDescending(m => m.CreateTime),
            SortBy.CreateAtASC => model.OrderBy(m => m.CreateTime),
            SortBy.PullTimeDesc => model.OrderByDescending(m => m.CreatedAt),
            SortBy.PullTimeAsc => model.OrderBy(m => m.CreatedAt),
            SortBy.LastSendDesc => model.OrderByDescending(m => m.LatestShipTime),
            SortBy.LastSendAsc => model.OrderBy(m => m.LatestShipTime),
            _ => model.OrderByDescending(m => m.CreateTime), // 默认按订单生成时间倒序
        };
        return res;
    }

}
