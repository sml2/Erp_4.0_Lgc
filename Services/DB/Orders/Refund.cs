﻿using ERP.Data;
using ERP.Extensions;
using Microsoft.EntityFrameworkCore;
using static ERP.Models.DB.Orders.Refund;
using Model = ERP.Models.DB.Orders.Refund;

namespace ERP.Services.DB.Orders;

public class Refund : BaseService
{

    private readonly DBContext _MyDbContext;

    public Refund(DBContext MyDbContext)
    {
        _MyDbContext = MyDbContext;
    }
    ////原erp3.0 注释
    #region ERP3.0 PHPService中方法签名
    public async Task<ReturnStruct> GetOrderRefundListPage(ViewModels.Order.OrderRefund.OrderRefund Refund,int CompanyId/*$order_no,$status,$shop_id,$company_id*/)
    {
        var data = await _MyDbContext.OrderRefund
               .Where(x => x.OrderNo == Refund.OrderNo)
               .Where(x => x.State == Refund.State)
               .Where(x => x.StoreId == Refund.ShopID)
               .Where(x => x.CompanyID == CompanyId)
               .OrderBy(x => x.CreatedAt)
               .OrderByDescending(x => x.State)
               .ToPaginateAsync();
                if(data==null)
                {
                    return ReturnArr(false,"错误");
                }
                return ReturnArr(true, "", data);
    }

    ////拒绝订单退款操作
    public async Task<ReturnStruct> RefuseOrderRefund(int ID)
    {
        var Find= await  _MyDbContext.OrderRefund.FindAsync(ID);
        if (Find != null)
        {
            Find.State = RefundState.STATUS_REFUSE;
            if (await _MyDbContext.SaveChangesAsync() > 0)
            {
                return ReturnArr(true, "拒绝退款申请成功");
            }
        }
        return ReturnArr(false, "拒绝退款申请失败");
    }
    ////根据ID获取退款订单数据
    public async Task<Model?> GetOrderRefundInfoFirst(int ID)
    {
          return await _MyDbContext.OrderRefund.Where(x=>x.ID==ID).FirstOrDefaultAsync();
    }
    #endregion
}

