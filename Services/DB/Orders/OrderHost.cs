
using Model = ERP.Models.DB.Orders.Order;
using PlatformDataCache = ERP.Services.Caches.PlatformData;

using ERP.Enums.Orders;
using ERP.Models.View.Order.AmazonOrder;
using System;

using newDeliverProduct = OrderSDK.Modles.Amazon.vm.DeliverProduct;
using ERP.Services.Api;

namespace ERP.Services.DB.Orders
{
    public class OrderHost
    {
        private readonly IServiceProvider ServiceProvider;
        private readonly IOrderFactory NewApiOrderFactory;
        private readonly ILogger<OrderHost> _logger;
        public OrderHost(IOrderFactory _ApiOrderFactory, IServiceProvider serviceProvider, ILogger<OrderHost> logger)
        {
            NewApiOrderFactory = _ApiOrderFactory;
            this.ServiceProvider = serviceProvider;
            _logger = logger;
        }


        /// <summary>
        /// 手动拉取订单，每10个一保存
        /// </summary>
        /// <param name="storeList"></param>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public async Task PullOrder(List<int?> ids, DateTime begin, DateTime end)
        {
            try
            {
                const int count = 10;
                var serviceProvider = this.ServiceProvider.CreateScope().ServiceProvider;
                var AsinProducrService = serviceProvider.GetRequiredService<DB.Stores.AsinProduct>();
                var AmazonOrder = serviceProvider.GetRequiredService<DB.Orders.Order>();
                var _storeService = serviceProvider.GetRequiredService<ERP.Services.Stores.StoreService>();
                var _dbcontext = serviceProvider.GetService<ERP.Data.DBContext>();

                var LstNeedPull = await _storeService.GetStoresByIds(ids);

                if (LstNeedPull == null || LstNeedPull.Count <= 0)
                {
                    throw new AggregateException($"PullOrder store is not found {string.Join(",", ids)}");
                }
                foreach (var store in LstNeedPull)
                {
                    //开始时间亚马逊最早应该是不能超过半年的，不然之前的拉不到或者可能返回错误，截止时间如果超过当前时间应该也会
                    if (begin == DateTime.MinValue)
                    {
                        if (DateTime.Now.AddMonths(-6) > store.LastOrderTime)
                        {
                            begin = DateTime.Now.AddMonths(-6);
                        }
                        else
                        {
                            begin = store.LastOrderTime;
                        }
                    }

                    if (begin > end) end = DateTime.Now.AddMinutes(-5);

                    if (store.Platform == Enums.Platforms.AMAZONSP)
                    {
                        var marketLst = await _storeService.GetAllMarketInfoByHash(store.UniqueHashCode);
                        if (marketLst.IsNotNull() && marketLst!.Count() > 0)
                        {
                            store.MarketPlaces = marketLst!.Select(x => x.Marketplace).ToList();
                            var _ApiOrderServices = NewApiOrderFactory.Create(store.Platform);


                            List<Model> ordersAll = new();
                            var listResult = await _ApiOrderServices.List(new(store, begin, end));
                            if (listResult is not null && listResult.HasOrder)
                            {
                                var waitOrders = listResult.waitOrders!;
                                _logger.LogInformation($"HandPullOrder:[{store.Name}] has {waitOrders.Count} orders;");
                                var OutProcessOrders = await AmazonOrder.GetInGProcessOrders(waitOrders);//只查询数据库中没有完成的订单
                                _logger.LogInformation($"HandPullOrder:[{store.Name}] need pull count:{waitOrders.Count} orders;");

                                int i = 0;
                                while (OutProcessOrders.Skip(i * count).Take(count).ToList().Any())
                                {
                                    var orderList = OutProcessOrders.Skip(i * count).Take(count);
                                    i++;
                                    List<Model> orders = new();
                                    var asinProduct = new List<Models.Stores.AsinProduct>();
                                    foreach (var o in orderList)
                                    {
                                        var m = _ApiOrderServices.GetOrderBasic(store, o);
                                        if (m is not null)
                                        {
                                            orders.Add(m);
                                        }
                                        else
                                        {
                                            _logger.LogError($"HandPullOrder:{store.Name} 【{o.GetAmazoNewParam().AmazonOrderId}】:Get Fail");
                                            continue;
                                        }
                                    }
                                    if (orders.Count > 0)
                                    {
                                        var asins = await _ApiOrderServices.GetOrderItem(store, orders, AsinProducrService);
                                        if (asins.Item1.Count > 0)
                                        {
                                            asinProduct.AddRange(asins.Item1);
                                        }
                                        await _ApiOrderServices.GetOrderAddress(store, orders);
                                        await _ApiOrderServices.GetOrderBuyerInfo(store, orders);
                                        await _ApiOrderServices.GetOrderFinanciall(store, orders);

                                        using (var transcation = await _dbcontext!.Database.BeginTransactionAsync())
                                        {
                                            try
                                            {
                                                if (await AmazonOrder.Add(orders.ToArray(), true) && await AsinProducrService.Update(asinProduct))
                                                {
                                                    ordersAll.AddRange(orders);
                                                    await _dbcontext.SaveChangesAsync();
                                                    await AmazonOrder.SumOrderSkuAndAsin(orders.ToArray());

                                                    await transcation.CommitAsync(); _logger.LogInformation($"HandPullOrder:{store.Name} Add Success [" +
                                                        $"{string.Join(",", orders.Select(x => x.OrderNo).ToList())}]");
                                                }
                                                else
                                                {
                                                    await transcation.RollbackAsync();
                                                    _logger.LogError($"HandPullOrder:{store.Name} Add Fail");
                                                }
                                            }
                                            catch (Exception e)
                                            {
                                                await transcation.RollbackAsync();
                                                _logger.LogError($" HandPullOrder:{store.Name} save error:{e.Message}");
                                                _logger.LogError($" HandPullOrder:{store.Name} {e.StackTrace}");
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                _logger.LogInformation($"HandPullOrder: [{store.Name}] No Order;");
                            }
                        }
                        else
                        {
                            _logger.LogError($"HandPullOrder: [{store.Name}] Adds Fail:no markets");
                        }
                    }
                    else //if (store.Platform == Enums.Platforms.SHOPEE)
                    {
                        _logger.LogError($"HandPullOrder:暂未实现该平台{store.Platform.GetDescription()}, PullOrder order");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"HandPullOrder Exception {ex.Message}");
                _logger.LogError($"HandPullOrder Exception {ex.StackTrace}");
            }   
        }


        public async Task DeliverGoods(int userid, DeliveryGoodsInfo deliveryGoodsInfo)
        {
            string msg = "";
            var deliveryGood = deliveryGoodsInfo.deliveryGood;
            var serviceProvider = this.ServiceProvider.CreateScope().ServiceProvider;
            var orderService = serviceProvider.GetRequiredService<ERP.Services.DB.Orders.Order>();
            var storeService = serviceProvider.GetRequiredService<ERP.Services.Stores.StoreService>();
            var order = await orderService.GetOrderByIdNoTrackAsync(deliveryGood.ID);
            var userService = serviceProvider.GetRequiredService<ERP.Services.DB.Users.UserService>();
            var _dbContext = serviceProvider.GetRequiredService<ERP.Data.DBContext>();           

            var store = await storeService.GetStoreByID(deliveryGoodsInfo.store.ID);
            if (store is null)
            {
                throw new AggregateException($"DeliverGoods store is not found {deliveryGoodsInfo.store.ID}");
            }
            var user = await userService.GetCurrentUser(userid);
            if (user is not null)
            {
                if (store.Platform == Platforms.AMAZONSP)
                {
                    var _ApiOrderServices = NewApiOrderFactory.Create(deliveryGoodsInfo.store.Platform);
                    List<newDeliverProduct> newList = new();

                    foreach (var item in deliveryGoodsInfo.deliverProducts)
                    {
                        newDeliverProduct newP = new newDeliverProduct(item.AmazonOrderID, item.FulfillmentDate, item.CarrierName, item.ShippingMethod, item.ShipperTrackingNumber, item.AmazonOrderItemCode, item.Quantity);
                        newList.Add(newP);
                    }
                    string result = await _ApiOrderServices.DeliveryGoods(store, newList, deliveryGoodsInfo.MarketplaceID);
                    if (!result.IsNullOrWhiteSpace())
                    {
                        msg = $"DeliverGoods:推送亚马逊接口失败:{result}";
                    }
                    else
                    {
                        msg = "";
                    }
                }
                else
                {
                    msg = $"DeliverGoods:暂未实现该平台{store.Platform.GetDescription()}, DeliverGoods order";
                }

                using (var transaction = await _dbContext.Database.BeginTransactionAsync())
                {
                    try
                    {
                        if (msg != "")
                        {
                            _logger.LogError(msg);
                        }
                        else
                        {
                            if (deliveryGood.OrderStatus is not null)
                            {
                                var status = deliveryGood.OrderStatus < 0 ? 1 : deliveryGood.OrderStatus;
                                //如果订单状态没有选择，那么
                                Enum.TryParse<Enums.Orders.States>(status.ToString(), out var OrderStatusEnum);
                                order.State = OrderStatusEnum;
                                await orderService.Update(order);
                            }
                            //order.SetBits(BITS.Waybill_HasSync);
                        }
                        await orderService.AddLog(order, user, msg == "" ? "同步运单信息成功" : msg);
                        _dbContext.Order.Update(order);
                        await _dbContext.SaveChangesAsync();
                        await transaction.CommitAsync();
                    }
                    catch (Exception e)
                    {
                        await transaction.RollbackAsync();
                        _logger.LogError($"DeliverGoods:同步运单信息失败,,异常原因：{e.Message}");
                    }
                }
            }
            else
            {
                msg = $"DeliverGoods:同步运单信息失败,用户没有找到。店铺名称：{store.Name},订单No：{order.OrderNo},userid:{userid}";
                _logger.LogError(msg);
            }
        }


        /// <summary>
        /// 手动更新订单信息
        /// </summary>
        /// <param name="order"></param>
        /// <param name="store"></param>
        /// <returns></returns>
        public async Task<string> UpdateOrderInfo(Model order, int storeID)
        {
            var serviceProvider = this.ServiceProvider.CreateScope().ServiceProvider;
            var orderService = serviceProvider.GetRequiredService<ERP.Services.DB.Orders.Order>();
            var storeService = serviceProvider.GetRequiredService<ERP.Services.Stores.StoreService>();           
            var _dbContext = serviceProvider.GetRequiredService<ERP.Data.DBContext>();
            var store = await storeService.GetStoreByID(storeID);
            if (store is null)
            {
                throw new AggregateException($"UpdateOrderInfo store is not found {storeID}");
            }
            if (store.Platform == Platforms.AMAZONSP)
            {
                var _ApiOrderServices = NewApiOrderFactory.Create(order.Plateform);
                await _ApiOrderServices.UpdateOrder(store, order);

                using (var transaction = await _dbContext.Database.BeginTransactionAsync())
                {
                    try
                    {
                        if (await orderService.UpdateHostOrder(order))
                        {
                            await _dbContext.SaveChangesAsync();
                            await transaction.CommitAsync();
                            _logger.LogError($"UpdateOrderInfo:更新订单信息成功,店铺名称：{store.Name},订单No：{order.OrderNo}");
                        }
                        else
                        {
                            await transaction.RollbackAsync();
                            _logger.LogError($"UpdateOrderInfo:更新订单信息失败,店铺名称：{store.Name},订单No：{order.OrderNo}");
                            return "";
                        }
                    }
                    catch (Exception e)
                    {
                        await transaction.RollbackAsync();
                        _logger.LogError($"UpdateOrderInfo:更新订单信息失败,店铺名称：{store.Name},订单No：{order.OrderNo}，错误信息：{e.Message}");
                    }
                }
            }
            else if (store.Platform == Platforms.SHOPEE)
            {
                //shopee  to do
            }
            return "";
        }
    }
}
