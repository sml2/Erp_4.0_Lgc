﻿using ERP.Extensions;
using ERP.Models.Product;
using Model = ERP.Models.DB.Orders.Action;
using Microsoft.EntityFrameworkCore;
using ERP.Interface;
using ERP.Data;
namespace ERP.Services.DB.Orders;

public class Action : BaseService
{
    private readonly DBContext _dbContext;
    private readonly ISessionProvider _session;
    public Action(DBContext myDbContext, ISessionProvider session)
    {
        _dbContext = myDbContext;
        _session = session;
    }
    public async Task<int> AddLog(int order_id = 0, string action = "", int order_status = 0, string order_status_text = "")
    {
        var time = DateTime.Now;
        Model a = new();
        a.OrderId = order_id;
        a.Value = action;
        a.UserID = _session.Session!.GetUserID();
        a.GroupID = _session.Session!.GetGroupID();
        a.CompanyID = _session.Session!.GetCompanyID();
        a.OEMID = _session.Session!.GetOEMID();
        a.UserName = _session.Session!.GetUserName();
        a.Truename = _session.Session!.GetTrueName();
        // a.OrderStatus = order_status;
        a.OrderStatusText = order_status_text;
        a.CreatedAt = time;
        a.UpdatedAt = time;
        await _dbContext.OrderAction.AddAsync(a);
        await _dbContext.SaveChangesAsync();
        return a.ID;
    }
}

