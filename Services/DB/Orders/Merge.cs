using ERP.Data;
using ERP.Extensions;
using Microsoft.EntityFrameworkCore;
using OrderMergeModel = ERP.Models.DB.Orders.Merge;
using ERP.ViewModels.Order.Merge;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ERP.Enums.Orders;
using ERP.Interface;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using System.Linq;

namespace ERP.Services.DB.Orders;
using static Models.DB.Orders.Order;
using OrderModel = Models.DB.Orders.Order;

public class Merge : BaseService
{   
    private readonly DBContext _dbContext;
    private readonly ISessionProvider _SessionProvider;
    public Merge(DBContext MyContext, ISessionProvider sessionProvider)
    {
        _dbContext = MyContext;
        _SessionProvider = sessionProvider;
    }
    private ISession Session { get => _SessionProvider.Session!; }
    private int OEMID { get => Session.GetOEMID(); }
    private int CompanyID { get => Session.GetCompanyID(); }
    private int GroupID { get => Session.GetGroupID(); }
    private int UserID { get => Session.GetUserID(); }

    public record ExecSplitCS
    {
        public int MergeID { get; set; }
        public IsMains IsMains { get; set; }
        public int OperateUserID { get; set; }
    }

    public async Task<List<OrderModel>> GetAmazonlList(MergeList merge,Expression<Func<OrderModel, bool>> expression)
    {
        return await _dbContext.Order               
                .Where(expression)
                .Where(x => x.MergeId > 0)
                .WhenWhere(!string.IsNullOrEmpty(merge.OrderNo), x => x.OrderNo!.Contains(merge.OrderNo!))
                .Select(x => new OrderModel { MergeId = x.MergeId, ID = x.ID, OrderNo = x.OrderNo })
                .OrderByDescending(x => x.ID)
                .ToListAsync();
    }

    public async Task<List<OrderMergeModel>> GetOrderMergeList(List<int> MerId, MergeList merge, Expression<Func<OrderMergeModel, bool>> expression)
    {
        var Merge = await _dbContext.OrderMerge
             .Where(expression)            
            .WhenWhere(merge.TypeStatus != null, x => x.Type == merge.TypeStatus)
            .WhenWhere(MerId.Count > 0, x => MerId.Contains(x.ID))
            .ToListAsync();
        return Merge;
    }  

    /// <summary>
    /// 根据订单编号和合并订单编号查询数据
    /// </summary>
    /// <returns></returns>
    public async Task<IList<OrderModel>?> GetOrderMergeId(int MerId, string OrderNo)
    {
        var data = await _dbContext.Order
              .Where(x => x.MergeId == MerId)
              .Where(x => x.OrderNo == OrderNo)
              .Select(x => new OrderModel() { MergeId = x.MergeId, ID = x.ID, OrderNo = x.OrderNo })
              .ToListAsync();
        if (data == null)
        {
            return null;
        }
        return data;
    }

    public async Task<OrderMergeModel?> GetOrderMergeListFirst(int? MergeId/*$merge_id*/)
    {
        var data = await _dbContext.OrderMerge.Where(x => x.ID == MergeId).FirstOrDefaultAsync();
        //        return OrderMergeModel::where("id", $merge_id)->first();
        if (data == null)
        {
            return null;
        }
        return data;
    }
     

    /// <summary>
    /// 执行合并订单事务处理
    /// </summary>
    /// <param name="insertData"></param>
    /// <param name="list"></param>
    /// <returns></returns>
    public async Task DoMergeBegin(OrderMergeModel insertData, List<OrderModel> list)
    {
        var transcation = _dbContext.Database.BeginTransaction();
        try
        {
            // 合并订单表
            var s = await _dbContext.OrderMerge.AddAsync(insertData);
            await _dbContext.SaveChangesAsync();

            // 改订单表mergeId
            foreach (var item in list)
            {
                item.MergeId = insertData.ID;
            }
            _dbContext.Order.UpdateRange(list);
            await _dbContext.SaveChangesAsync();
            transcation.Commit();
        }
        catch (Exception ex)
        {
            transcation.Rollback();
            throw new Exception(ex.Message);
        }
    }

    /// <summary>
    /// 执行拆分合并订单
    /// </summary>
    /// <param name="MergeId"></param>
    /// <param name="OrderInfo"></param>
    /// <returns></returns>
    public async Task<ReturnStruct> ExecSplit(int MergeId, string OrderInfo)
    {
        ExecSplitCS execSplitCS = new()
        {
            MergeID = -1,
            IsMains = IsMains.VICE,
            OperateUserID = 0
        };
        var RES = await ExecSplitBegin(MergeId, OrderInfo, execSplitCS);
        return RES.State ? ReturnArr(true, "拆分订单成功") : ReturnArr(false, "拆分订单失败");
    }

    public async Task<ReturnStruct> ExecSplitBegin(int? MergeId, string OrderInfo, ExecSplitCS execSplitCS)   //OrderInfo
    {
        var Update = await _dbContext.OrderMerge.SingleOrDefaultAsync(x => x.ID == MergeId);
        if (Update == null) throw new Exception($"未找到[ID:{MergeId}]的合并订单");
        var Transaction = _dbContext.Database.BeginTransaction();
        try
        {
            var OrderInfos = JsonConvert.DeserializeObject<List<MergeOrderInfo>>(OrderInfo);
            // 删除 合并订单表
            _dbContext.Remove(Update);
            if (OrderInfos != null)
            {
                var orderIds = OrderInfos.Select(a => a.order_id).ToList();

                var list = await _dbContext.Order.Where(x => orderIds.Contains(x.ID)).ToListAsync();
                if (list == null) return ReturnArr(false);
                foreach (var order in list)
                {
                    order.MergeId = execSplitCS.MergeID;
                    order.IsMain = execSplitCS.IsMains;
                    order.OperateUserId = execSplitCS.OperateUserID;
                }
                _dbContext.UpdateRange(list);
                await _dbContext.SaveChangesAsync();
            }
            Transaction.Commit();
        }
        catch (Exception)
        {
            Transaction.Rollback();
            return ReturnArr(false);
        }
        return ReturnArr(true);
    }
}
