using Common.Enums.ResourcePackage;
using ERP.Data;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.DB.Packages;
using ERP.Models.View.Setting;
using ERP.Services.DB.Users;
using ERP.Services.IntegralBalance;
using Microsoft.EntityFrameworkCore;
using Ryu.Extensions;
using Ryu.Linq;
using Z.EntityFramework.Plus;
using BalanceBill = ERP.Models.IntegralBalance.BalanceBill;
using Task = ERP.Models.Task;

namespace ERP.Services.DB.CompanyResource;

public class PackagesResourceService : BaseWithUserService
{
    private readonly CompanyService _companyService;
    private readonly UserOperateLogService _userOperateLogServiceService;
    private readonly IntegralBalanceService _integralBalanceService;
    private readonly UserService _userService;

    public PackagesResourceService(DBContext dbContext, ISessionProvider sessionProvider, CompanyService companyService,
        UserOperateLogService userOperateLogServiceService, IntegralBalanceService integralBalanceService,
        UserService userService)
        : base(dbContext,
            sessionProvider)
    {
        _companyService = companyService;
        _userOperateLogServiceService = userOperateLogServiceService;
        _integralBalanceService = integralBalanceService;
        _userService = userService;
    }

    /// <summary>
    /// 列表
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    public async Task<DbSetExtension.PaginateStruct<CompanyResourcePackagesModel>> Index(GetPackagesDto req)
    {
        return await _dbContext.CompanyResourcePackages
            .WhenWhere(req.Type.IsNotNull(),m => m.Type == req.Type)
            .WhenWhere(!string.IsNullOrEmpty(req.Name), m => m.Name.Contains(req.Name!))
            .WhenWhere(req.CompanyId.HasValue, m => m.CompanyID == req.CompanyId)
            .WhenWhere(!req.CompanyId.HasValue, m => m.CompanyID == _companyId)
            .ToPaginateAsync();
    }

    /// <summary>
    /// 详情
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<Dictionary<string, object>> Info(int id)
    {
        var info = await _dbContext.CompanyResourcePackages
            .Where(m => m.ID == id)
            // .Where(m => m.ID == id && m.CompanyID == _companyId)
            .FirstOrDefaultAsync();
        if (info == null)
            throw new Exception("找不到相关数据#1");
        var now = DateTime.Now;
        var todayUse = await _dbContext.CompanyResourceConsumptionLog
            .Where(m =>
                m.PackageId == info.ID
                && m.CreatedAt >= now.BeginDayTick() && m.CreatedAt <= now.LastDayTick()
            ).SumAsync(m => m.Measure);
        return new Dictionary<string, object>()
        {
            { "info", info },
            { "todayUse", todayUse }
        };
    }

    /// <summary>
    /// 检测公司下是否有可用包
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public async Task<bool> CheckIsHaveAvailableResource(PackageTypeEnum type)
    {
        return await GetConsumablePackage(type) != null;
    }

    /// <summary>
    /// 获取资源包
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public async Task<CompanyResourcePackagesModel?> GetConsumablePackage(PackageTypeEnum type)
    {
        return await _dbContext.CompanyResourcePackages
            .Where(m => m.CompanyID == _companyId && m.Type == type && m.State == PackageUseStateEnum.Effective)
            .OrderBy(m => m.ID)
            .FirstOrDefaultAsync();
    }

    /// <summary>
    /// 消费抠图量包，调用此方法需要启用事务
    /// </summary>
    /// <param name="times"></param>
    /// <returns></returns>
    public async Task<(bool State, string Msg)> ConsumptionMatting(int times = 1)
    {
        //获取可消费的抠图资源包
        var pack = await GetConsumablePackage(PackageTypeEnum.Matting);
        if (pack == null)
        {
            return (false, "无可用资源包");
        }

        pack.RemainingQuantity -= 1;
        //设置为已用完
        if (pack.RemainingQuantity == 0)
            pack.State = PackageUseStateEnum.UsedUp;
        await _dbContext.CompanyResourcePackages.SingleUpdateAsync(pack);

        //消费日志
        await ConsumptionLog(pack.ID, PackageTypeEnum.Matting);

        // var consumption = new CompanyResourceConsumptionLogModel(_session);
        // consumption.Type = PackageTypeEnum.Matting;
        // consumption.Measure = 1;
        // consumption.PackageId = pack.ID;
        // consumption.UseUserId = _userId;
        // consumption.UseTruename = _session.GetTrueName();
        // await _dbContext.CompanyResourceConsumptionLog.AddAsync(consumption);

        var res = await _dbContext.SaveChangesAsync() > 0;
        return (res, "");
    }

    private async System.Threading.Tasks.Task ConsumptionLog(int packId, PackageTypeEnum type)
    {
        var consumption = new CompanyResourceConsumptionLogModel(_session);
        consumption.Type = type;
        consumption.Measure = 1;
        consumption.PackageId = packId;
        consumption.UseUserId = _userId;
        consumption.UseTruename = _session.GetTrueName();
        await _dbContext.CompanyResourceConsumptionLog.AddAsync(consumption);
    }

    /// <summary>
    /// 消费图片翻译量包，调用此方法需要启用事务
    /// </summary>
    /// <param name="times"></param>
    /// <returns></returns>
    /// 
    public async Task<(bool State, string Msg)> ConsumptionImgTran(int times = 1)
    {
        //获取可消费的抠图资源包
        var pack = await GetConsumablePackage(PackageTypeEnum.ImgTranslation);
        if (pack == null)
        {
            return (false, "无可用资源包");
        }

        pack.RemainingQuantity -= 1;
        //设置为已用完
        if (pack.RemainingQuantity == 0)
            pack.State = PackageUseStateEnum.UsedUp;
        await _dbContext.CompanyResourcePackages.SingleUpdateAsync(pack);

        //消费日志
        // var consumption = new CompanyResourceConsumptionLogModel(_session);
        // consumption.Type = PackageTypeEnum.Matting;
        // consumption.Measure = 1;
        // consumption.PackageId = pack.ID;
        // consumption.UseUserId = _userId;
        // consumption.UseTruename = _session.GetTrueName();
        // await _dbContext.CompanyResourceConsumptionLog.AddAsync(consumption);

        await ConsumptionLog(pack.ID, PackageTypeEnum.ImgTranslation);

        var res = await _dbContext.SaveChangesAsync() > 0;
        return (res, "");
    }


    /// <summary>
    /// 获取所有可用的翻译包
    /// </summary>
    /// <returns></returns>
    public async Task<List<CompanyResourcePackagesModel>> GetTranslationPackages()
    {
        return await _dbContext.CompanyResourcePackages
            .Where(m =>
                m.CompanyID == _companyId
                && m.Type == PackageTypeEnum.Translation
                && m.State == PackageUseStateEnum.Effective
            ).OrderBy(m => m.ID)
            .ToListAsync();
    }

    /// <summary>
    /// 判断翻译包是否够用
    /// </summary>
    /// <param name="packages"></param>
    /// <param name="num"></param>
    /// <returns></returns>
    public async Task<bool> CheckTranslationPackageIsEnough(List<CompanyResourcePackagesModel> packages, int num)
    {
        var characterCount = packages.Sum(m => m.RemainingQuantity);
        if (characterCount < num)
            return false;
        return true;
    }

    /// <summary>
    /// 消费翻译量包，调用此方法需要启用事务
    /// </summary>
    /// <param name="times"></param>
    /// <returns></returns>
    public async Task<(bool State, string Msg)> ConsumptionTranslation(int num)
    {
        //获取可消费的抠图资源包
        var packages = await GetTranslationPackages();
        var isEnough = await CheckTranslationPackageIsEnough(packages, num);
        if (!isEnough)
        {
            return (false, "翻译字符数不足");
        }

        //id num
        var useInfo = new Dictionary<int, int>();
        var usePackages = new List<CompanyResourcePackagesModel>();
        foreach (var item in packages)
        {
            if (num == 0)
            {
                break;
            }

            if (item.RemainingQuantity > num)
            {
                useInfo.Add(item.ID, num);
                usePackages.Add(item);
                break;
            }

            useInfo.Add(item.ID, item.RemainingQuantity);
            usePackages.Add(item);
            num -= item.RemainingQuantity;
        }

        var consumptions = new List<CompanyResourceConsumptionLogModel>();

        foreach (var item in usePackages)
        {
            var measure = useInfo[item.ID];
            //消费资源
            item.RemainingQuantity -= measure;
            //设置为已用完
            if (item.RemainingQuantity == 0)
                item.State = PackageUseStateEnum.UsedUp;
            await _dbContext.CompanyResourcePackages.SingleUpdateAsync(item);
            //消费日志
            var consumption = new CompanyResourceConsumptionLogModel(_session);
            consumption.Type = PackageTypeEnum.Translation;
            consumption.Measure = measure;
            consumption.PackageId = item.ID;
            consumption.UseUserId = _userId;
            consumption.UseTruename = _session.GetTrueName();
            consumptions.Add(consumption);
        }

        await _dbContext.CompanyResourceConsumptionLog.AddRangeAsync(consumptions);
        var res = await _dbContext.SaveChangesAsync() > 0;
        return (res, "");
    }

    public async Task<object> Packages(PackageTypeEnum type, bool isPutOnTheShelf = true)
    {
        var pack = await _dbContext.Packages.Where(m => m.Type == type).FirstAsync();
        // if (type == PackageTypeEnum.Matting)
        // {
        //     items = await _dbContext.MattingPackageItem
        //         .WhenWhere(isPutOnTheShelf, m => m.State == PackageSellStateEnum.PutOnTheShelf)
        //         .ToDictionaryAsync(m => m.ID, m => m);
        // }
        // else if(type == PackageTypeEnum.ImgTranslation)
        // {
        //     items = await _dbContext.TranslationPackageItem
        //         .WhenWhere(isPutOnTheShelf, m => m.State == PackageSellStateEnum.PutOnTheShelf)
        //         .ToDictionaryAsync(m => m.ID, m => m);
        // }

        var items = await _dbContext.PackageItems
            .Where(m => m.Type == type)
            .WhenWhere(isPutOnTheShelf, m => m.State == PackageSellStateEnum.PutOnTheShelf)
            .ToDictionaryAsync(m => m.ID, m => m);

        return new Dictionary<string, object>()
        {
            { "info", pack },
            { "items", items },
        };
    }

    public async Task<(bool State, string Message)> SubPresent(SubPresentDto req)
    {
        if (req.Num <= 0)
            return (false, "赠送数量不可为0");
        if (req.ItemId <= 0)
            return (false, "非法的包");
        var company = await _companyService.GetInfoById(req.CompanyId);
        var item = await _dbContext.PackageItems
            .Where(m => m.ID == req.ItemId)
            .FirstOrDefaultAsync();
        if (item == null)
            return (false, "找不到相关资源包");
        var totalPrice = req.Num * item.Price;
        var money = Helpers.FromConversion(totalPrice, "CNY");
        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            var user = await _userService.GetInfoById(_userId);
            List<CompanyResourcePackagesModel> insertData = new List<CompanyResourcePackagesModel>();
            var info = await _dbContext.Packages.Where(m => m.Type == req.Type).FirstAsync();
            for (var i = 0; i < req.Num; i++)
            {
                var pack = await _dbContext.PackageItems
                    .Where(m => m.ID == req.ItemId && m.Type == req.Type)
                    .FirstOrDefaultAsync();
                if (pack == null)
                    throw new Exception("找不到相关资源包");
                var model = new CompanyResourcePackagesModel(info, pack, company);
                insertData.Add(model);
            }

            var msg = $"{insertData[0].Name}*{req.Num}";
            await _dbContext.CompanyResourcePackages.AddRangeAsync(insertData);

            // await _userOperateLogServiceService.AddUserLog(_userId, $"赠送{msg}给公司名为{company.Name},总价值{totalPrice}CNY");
            await _integralBalanceService.AddBalanceBill(money, _companyId, _oemId, user, 0,
                BalanceBill.Types.EXPENSE, $"赠送{msg}给公司名为{company.Name},总价值{totalPrice.ToFixed(2)}CNY");

            await _dbContext.SaveChangesAsync();
            await transaction.CommitAsync();
            return (true, "");
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            Console.WriteLine(e);
            return (false, e.Message);
        }
    }

    public async Task<(bool State, string Message)> SubPurchase(SubPurchaseDto req)
    {
        if (req.Num <= 0)
            return (false, "购买数量不可为0");
        if (req.ItemId <= 0)
            return (false, "非法的包");

        var company = await _companyService.GetInfoById(_companyId);
        decimal totalPrice = 0;
        
        var item = await _dbContext.PackageItems
            .Where(m => m.ID == req.ItemId && m.Type == req.Type)
            .FirstOrDefaultAsync();
        if (item == null)
            return (false, "找不到相关资源包");
        totalPrice = req.Num * item.Price;


        // var money = Helpers.FromConversion(totalPrice, "CNY");
        var money = MoneyFactory.Instance.Create(totalPrice, "CNY");

        if (company.Balance < money)
            return (false, "余额不足");

        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            var user = await _userService.GetInfoById(_userId);
            List<CompanyResourcePackagesModel> insertData = new List<CompanyResourcePackagesModel>();
            var info = await _dbContext.Packages.Where(m => m.Type == req.Type).FirstAsync();
            
            for (var i = 0; i < req.Num; i++)
            {
                var pack = await _dbContext.PackageItems
                    .Where(m => m.ID == req.ItemId && m.Type == req.Type)
                    .FirstOrDefaultAsync();
                if (pack == null)
                    throw new Exception("找不到相关资源包");
                var model = new CompanyResourcePackagesModel(info, pack, _session);
                insertData.Add(model);
            }

            var msg = $"{insertData[0].Name}*{req.Num}";
            await _dbContext.CompanyResourcePackages.AddRangeAsync(insertData);
            await _companyService.ChargeSubtract(_session.GetCompanyID(), money);

            await _userOperateLogServiceService.AddUserLog(_userId, $"购买{msg},消费{totalPrice.ToFixed(2)}CNY");
            await _integralBalanceService.AddBalanceBill(money, _companyId, _oemId, user, company.Balance,
                BalanceBill.Types.EXPENSE, $"购买{msg}");
            //扣费
            await _dbContext.SaveChangesAsync();
            await transaction.CommitAsync();
            return (true, "");
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            Console.WriteLine(e);
            return (false, e.Message);
        }
    }

    public async Task<DbSetExtension.PaginateStruct<CompanyResourceConsumptionLogModel>> UseInfo(int id)
    {
        return await _dbContext.CompanyResourceConsumptionLog
            .Where(m => m.PackageId == id)
            .OrderByDescending(m => m.ID)
            .ToPaginateAsync();
    }
}