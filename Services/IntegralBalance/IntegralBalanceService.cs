﻿using ERP.Data;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.View.Setting;
using Microsoft.EntityFrameworkCore;

namespace ERP.Services.IntegralBalance;

public class IntegralBalanceService : BaseWithUserService
{
    public IntegralBalanceService(DBContext dbContext, ISessionProvider sessionProvider) : base(dbContext, sessionProvider)
    {
    }

    private DbSet<Models.IntegralBalance.IntegralBill> IntegralBills => _dbContext.IntegralBills;

    private DbSet<Models.IntegralBalance.BalanceBill> BalanceBills =>
        _dbContext.Set<Models.IntegralBalance.BalanceBill>();

    /******************************** 积分 ***************************************************/

    /// <summary>
    /// 获取积分账单列表
    /// </summary>
    /// <param name="type"></param>
    /// <param name="datetime"></param>
    public Task<DbSetExtension.PaginateStruct<IntegralBillListDto>> GetIntegralListPage(Models.DB.Identity.User user,
        Models.IntegralBalance.IntegralBill.Types? type, List<DateTime>? datetime)
    {
        return IntegralBills.Where(i => i.CompanyID == user.CompanyID && i.OEMID == user.OEMID)
            .WhenWhere(type.HasValue, i => i.Type == type)
            .WhenWhere(datetime.IsNotNull() && datetime.Count >= 2, i => i.CreatedAt >= datetime![0] && i.CreatedAt <= datetime[1])
            .Select(i =>
                new IntegralBillListDto(i.ID, i.Type, i.Integral, i.surplus, i.Reason, i.Remark, i.Datetime,
                    i.CreatedAt))
            .ToPaginateAsync();
    }

    /// <summary>
    /// 增加积分账单数据
    /// </summary>
    public Models.IntegralBalance.IntegralBill AddIntegralBill(int integral, Models.DB.Identity.User user,
        decimal surplus = 0,
        Models.IntegralBalance.IntegralBill.Types type = Models.IntegralBalance.IntegralBill.Types.EXPENSE,
        string reason = "", string remark = "")
    {
        var model = new Models.IntegralBalance.IntegralBill()
        {
            CompanyID = user.CompanyID,
            OEMID = user.OEMID,
            Type = type,
            Integral = integral,
            surplus = surplus,
            Reason = reason,
            Remark = remark,
            Datetime = DateTime.Now,
            UserName = user.UserName,
            TrueName = user.TrueName,
            OperateUserId = user.Id,
            OperateCompanyId = user.CompanyID,
            CreatedAt = DateTime.Now,
        };
        return IntegralBills.Add(model).Entity;
    }
    //
    // /**
    //  * 登录积分充值、扣除账单
    //  * User: CCY
    //  * Date: 2020/11/24
    //  * @param        $integral
    //  * @param int    $surplus
    //  * @param        $company
    //  * @param        $user
    //  * @param int    $type
    //  * @param string $reason
    //  * @param string $remark
    //  * @param array|null $ext
    //  * @return int
    //  */
    // public function addLoginIntegralBill($integral, $surplus = 0, $company, $user, $type = IntegralBillModel::TYPE_EXPENSE, $reason = '', $remark = '',$ext = null)
    // {
    //     $insertData = [
    //         'company_id'         => $company['id'],
    //         'oem_id'             => $company['oem_id'],
    //         'type'               => $type,
    //         'integral'           => $integral,
    //         'surplus'            => $surplus,
    //         'reason'             => $reason,
    //         'remark'             => $remark,
    //         'datetime'           => $this->datetime,
    //         'username'           => $user['username'],
    //         'truename'           => $user['truename'],
    //         'operate_user_id'    => $user['id'],
    //         'operate_company_id' => $company['id'],
    //         'created_at'         => $this->datetime,
    //         'ext'                => is_array($ext) ? json_encode($ext):null
    //     ];
    //     return IntegralBillModel::query()->insertGetId($insertData);
    // }
    //
    /******************************** 余额 ***************************************************/

    /// <summary>
    /// 获取余额账单列表
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="type"></param>
    /// <param name="datetime"></param>
    /// <param name="operateUserId"></param>
    /// <param name="byUserId"></param>
    /// <returns></returns>
    public Task<DbSetExtension.PaginateStruct<BalanceBillListDto>> GetBalanceListPage(int? companyId,
        Models.IntegralBalance.BalanceBill.Types? type, DateTime[]? datetime, int? operateUserId, int? byUserId)
    {
        return BalanceBills.WhenWhere(companyId.HasValue, i => i.CompanyID == companyId)
            .WhenWhere(type.HasValue, i => i.Type == type)
            .WhenWhere(datetime is {Length: >= 2}, i => i.CreatedAt >= datetime![0] && i.CreatedAt <= datetime[1])
            .WhenWhere(operateUserId.HasValue, i => i.OperateCompanyId == operateUserId)
            .WhenWhere(byUserId.HasValue, i => i.ByUserId == byUserId)
            .Select(b => new BalanceBillListDto(b.ID, b.Type, b.Balance, b.Surplus, b.Reason, b.Remark, b.Datetime,
                b.CreatedAt, b.TrueName, b.UserName, b.ByuserName, b.Ext))
            .ToPaginateAsync();
    }

    /// <summary>
    /// 增加余额账单数据
    /// </summary>
    public async Task<bool> AddBalanceBill(decimal balance, int companyId, int oemId,
        Models.DB.Identity.User user, decimal surplus = 0,
        Models.IntegralBalance.BalanceBill.Types type = Models.IntegralBalance.BalanceBill.Types.EXPENSE,
        string reason = "", string remark = "")
    {
        var model = new Models.IntegralBalance.BalanceBill()
        {
            CompanyID = companyId,
            OEMID = oemId,
            Type = type,
            Balance = balance,
            Surplus = surplus,
            Reason = reason,
            Remark = !string.IsNullOrEmpty(remark) ? remark : reason,
            Datetime = DateTime.Now,
            UserName = _session.GetUserName(),
            TrueName = _session.GetTrueName(),
            OperateUserId = _userId,
            OperateCompanyId = _companyId,

            BytrueName = user.TrueName,
            ByuserName = user.UserName,
            ByUserId = user.Id,
            ByCompanyId = user.CompanyID,
            CreatedAt = DateTime.Now,
        };
        await  _dbContext.BalanceBills.AddAsync(model);
        return await _dbContext.SaveChangesAsync() > 0;
    }

    // /**
    //  * 登录余额账单数据
    //  * User: CCY
    //  * Date: 2020/11/23
    //  * @param        $balance 基准数值
    //  * @param        $company
    //  * @param        $user
    //  * @param int    $type
    //  * @param string $reason
    //  * @param string $remark
    //  * @param array|null $ext
    //  * @return int
    //  */
    // public function addLoginBalanceBill($balance, $surplus = 0, $company, $user, $type = BalanceBillModel::TYPE_EXPENSE, $reason = '', $remark = '',$ext = null)
    // {
    //     $insertData = [
    //         'company_id'         => $company['id'],
    //         'oem_id'             => $company['oem_id'],
    //         'type'               => $type,
    //         'balance'            => $balance,
    //         'surplus'            => $surplus,
    //         'reason'             => $reason,
    //         'remark'             => $remark,
    //         'datetime'           => $this->datetime,
    //         'username'           => $user['username'],
    //         'truename'           => $user['truename'],
    //         'operate_user_id'    => $user['id'],
    //         'operate_company_id' => $company['id'],
    //         'created_at'         => $this->datetime,
    //         'ext'                => is_array($ext) ? json_encode($ext):null
    //     ];
    //     return BalanceBillModel::query()->insertGetId($insertData);
    // }
    //
    // /**
    //  * 推广返现账单
    //  * User: CCY
    //  * Date: 2020/12/25
    //  * @param $balance
    //  * @param $surplus
    //  * @param $company
    //  * @param $operate_company_id
    //  * @return int
    //  */
    // public function addCashbackBalanceBill($balance, $surplus, $company, $operate_company_id, $reason = '推广返现')
    // {
    //     $insertData = [
    //         'company_id'         => $company['id'],
    //         'oem_id'             => $company['oem_id'],
    //         'type'               => BalanceBillModel::TYPE_RECHARGE,
    //         'balance'            => $balance,
    //         'surplus'            => $surplus,
    //         'reason'             => $reason,
    //         'remark'             => '返现',
    //         'datetime'           => $this->datetime,
    //         'username'           => '系统充值',
    //         'truename'           => '系统充值',
    //         'operate_user_id'    => 0,
    //         'operate_company_id' => $operate_company_id,
    //         'created_at'         => $this->datetime
    //     ];
    //     return BalanceBillModel::query()->insertGetId($insertData);
    // }
    //
    //
    // /******************************** 资源 ***************************************************/
    //
    // /**
    //  * 获取公司已支付积分资源数据
    //  * User: CCY
    //  * Date: 2020/11/16
    //  * @param $company_id
    //  * @param $oem_id
    //  * @return CompanyReduceRequest|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
    //  */
    // public function getCompanyRequest($company_id, $oem_id)
    // {
    //     return CompanyReduceRequest::query()->CompanyId($company_id)->OemId($oem_id)->first();
    // }
    //
    // public function getCompanyStatistics($company_id, $oem_id)
    // {
    //     return CounterCompanyBusiness::query()
    //         ->CompanyId($company_id)
    //         ->OemId($oem_id)
    //         ->select(['user_num','product_num','order_num','purchase_num','waybill_num','store_num'])
    //         ->first();
    // }
    //
    // /**
    //  * 添加公司已支付积分资源数据
    //  * User: CCY
    //  * Date: 2020/11/16
    //  * @param $data
    //  * @param $company
    //  * @return int
    //  */
    // public function addCompanyReduceRequest($data, $company)
    // {
    //     $insertData = [
    //         'company_id' => $company['id'],
    //         'oem_id'     => $company['oem_id'],
    //         'request'    => $data['total_request'],
    //         'cpu'        => $data['total_cpu'],
    //         'memory'     => $data['total_memory'],
    //         'network'    => $data['total_network'],
    //         'disk'       => $data['total_disk'],
    //         'pic_api'    => (int)$data['total_pic_api'],
    //         'translate_api'    => $data['total_translate_api'],
    //         'created_at' => $this->datetime,
    //         'updated_at' => $this->datetime
    //     ];
    //     return CompanyReduceRequest::query()->insertGetId($insertData);
    // }
    //
    // /**
    //  * 更新公司已支付积分资源数据
    //  * User: CCY
    //  * Date: 2020/11/16
    //  * @param $data
    //  * @param $company
    //  * @param $updated_at
    //  * @return bool|int
    //  */
    // public function editCompanyReduceRequest($data, $company, $updated_at = '')
    // {
    //     $where = [
    //         ['company_id', '=', $company['id']],
    //         ['oem_id', '=', $company['oem_id']]
    //     ];
    //     $updated_at && $where[] = ['updated_at', '=', $updated_at];
    //     $updateData = [
    //         'request'    => $data['total_request'],
    //         'cpu'        => $data['total_cpu'],
    //         'memory'     => $data['total_memory'],
    //         'network'    => $data['total_network'],
    //         'disk'       => $data['total_disk'],
    //         'pic_api'    => (int)$data['total_pic_api'],
    //         'translate_api'    => $data['total_translate_api'],
    //         'updated_at' => $this->datetime
    //     ];
    //     return CompanyReduceRequest::query()->where($where)->update($updateData);
    // }
    //
    // public function editCompanyReduce($updateData, $company)
    // {
    //     $where = [
    //         ['company_id', '=', $company['id']],
    //         ['oem_id', '=', $company['oem_id']]
    //     ];
    //     $updateData['pic_api'] = isset($updateData['pic_api']) ?(int)$updateData['pic_api']:0;
    //     return CompanyReduceRequest::query()->where($where)->update($updateData);
    // }
}