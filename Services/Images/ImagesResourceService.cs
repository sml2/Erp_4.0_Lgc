using ERP.Data;
using ERP.Data.Products;
using ERP.Interface;
using ERP.Models.DB.Product;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace ERP.Services.Images;

public class ImagesResourceService : BaseWithUserService
{
    public ImagesResourceService(DBContext dbContext, ISessionProvider sessionProvider) : base(dbContext,
        sessionProvider)
    {
    }

    /// <summary>
    /// 删除正式资源数据
    /// </summary>
    /// <param name="ids"></param>
    public async Task Delete(List<int> ids)
    {
        await _dbContext.Resource.Where(m => ids.Contains(m.ID))
            .DeleteAsync();
    }

    /// <summary>
    /// 删除正式资源数据
    /// </summary>
    /// <param name="id"></param>
    public async Task Delete(int id)
    {
        await _dbContext.Resource.Where(m => m.ID == id)
            .DeleteAsync();
    }

    public async Task<ResourceModel?> GetInfoById(int id)
    {
        return await _dbContext.Resource.FirstOrDefaultAsync(m => m.ID == id);
    }

    public async Task<List<ResourceModel>> GetImagesByPid(int id, ResourceModel.Types type)
    {
        return await _dbContext.Resource
            .Where(m => m.ProductId == id && m.Type == type)
            .ToListAsync();
    }
}