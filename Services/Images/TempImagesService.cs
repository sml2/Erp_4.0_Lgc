using ERP.Data;
using ERP.Exceptions.TempImages;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.DB.Product;
using Microsoft.EntityFrameworkCore;

namespace ERP.Services.Images;

public class TempImagesService : BaseWithUserService
{

    public TempImagesService(DBContext dbContext, ISessionProvider sessionProvider) : base(dbContext,sessionProvider)
    {
    }


    /// <summary>
    /// 存储临时图片
    /// </summary>
    /// <param name="file"></param>
    /// <param name="filePath"></param>
    /// <returns></returns>
    /// <exception cref="SavedException"></exception>
    public async Task<TempImagesModel> InsertTempImages(IFormFile file, string filePath)
    {
        var imageFile = new TempImagesModel(file.Length, filePath, _oemId,
            _companyId, _groupId, _userId);
        await _dbContext.TempImages.AddAsync(imageFile);
        if (await _dbContext.SaveChangesAsync() >= 1)
        {
            return imageFile;
        }

        throw new SavedException("临时图片存储失败#");
    }
    
    /// <summary>
    /// 存储临时图片
    /// </summary>
    /// <param name="length"></param>
    /// <param name="filePath"></param>
    /// <returns></returns>
    /// <exception cref="SavedException"></exception>
    public async Task<TempImagesModel> InsertTempImages(long length, string filePath)
    {
        var imageFile = new TempImagesModel(length, filePath, _oemId,
            _companyId, _groupId, _userId);
        await _dbContext.TempImages.AddAsync(imageFile);
        if (await _dbContext.SaveChangesAsync() >= 1)
        {
            return imageFile;
        }

        throw new SavedException("临时图片存储失败#");
    }
    
   /// <summary>
   /// 迁移图片到临时图片库后期脚本删除
   /// </summary>
   /// <param name="pid">产品id</param>
   /// <returns>迁移的图片ids</returns>
    public async Task<List<int>> Transfer(int pid)
    {
        var images = await _dbContext.Resource
            .Where(m => m.ProductId == pid)
            .Select(m => new TempImagesModel(m))
            .ToListAsync();

        await _dbContext.TempImages.AddRangeAsync(images);

        return images.Select(m => m.ID).ToList();
    }

   public async Task<TempImagesModel?> GetInfoById(int id)
   {
       return await _dbContext.TempImages.FirstOrDefaultAsync(m => m.ID == id);
   }
}