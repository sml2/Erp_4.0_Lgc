﻿namespace ERP.Services.Api
{
    public interface OperationsLog
    {
        public delegate void ReportOperationsLogHandle(string Msg);
    }
}
