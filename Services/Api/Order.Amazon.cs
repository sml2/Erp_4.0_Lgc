
using ERP.Models.Stores;
using Newtonsoft.Json;

using OrderSDK.Modles.Amazon.vm;
using OrderSDK.Services;
using amazon = OrderSDK.Services.IAmazon;

using Model = ERP.Models.DB.Orders.Order;
using AsinProducrService = ERP.Services.DB.Stores.AsinProduct;
using AsinModel = ERP.Models.Stores.AsinProduct;
using ProductModel = ERP.Data.Products.Product;
using ProductResult = ERP.Data.Products.ProductResult;
using AsinInfo = ERP.Models.DB.Product.AsinInfo;
using commonTypeStore = OrderSDK.Modles.CommonType.StoreRegion;
using OrderSDK.Modles.Amazon.Finances;
using ERP.Enums.Orders;
using static ERP.Models.DB.Orders.Order;

using OrderSDK.Modles.Amazon;

using ERP.Extensions;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json.Linq;
using ERP.Models.ProductNodeType.Amazon;
using System;
using System.IO.Compression;
using System.Net;
using OrderSDK.Modles.Amazon.Reports;

using apiListParameter = OrderSDK.Modles.Amazon.vm.ListParameter;
using ListParameter = ERP.Models.Api.Amazon.vm.ListParameter;
using OrderSDK.Modles.Amazon.CatalogItems;
using OrderSDK.Modles.Amazon.Feeds;
using ERP.Services.Api.Amazon;
using OrderSDK.Modles.Amazon.ProductTypes;
using ERP.Models.ProductNodeType.Amazon;
using OrderSDK.Modles;
using OrderSDK.Modles.Amazon.Order;

namespace ERP.Services.Api
{
    public class AmazonOrder : Order
    {
        private readonly Caches.Nation nationCache;
        private readonly Caches.PlatformData platformDataCache;
        private readonly Caches.AmazonSPKey amazonSPKeyCache;
        private readonly Caches.Store storeCache;
        private readonly Caches.StoreMarket storeMarket;


        private readonly OrderSDK.Services.IAmazon amazon;
        private readonly ILogger<AmazonOrder> logger;
        public AmazonOrder(Caches.PlatformData _platformDataCache, Caches.AmazonSPKey _amazonSPKeyCache, Caches.Nation _nation,
            Caches.Store _storeCache, Caches.StoreMarket _storeMarket,IServiceProvider serviceProvider, ILogger<AmazonOrder> _logger)
        {
            platformDataCache = _platformDataCache;
            amazonSPKeyCache = _amazonSPKeyCache;
            storeCache = _storeCache;
            storeMarket = _storeMarket;
            nationCache = _nation;
            amazon = serviceProvider.CreateScope().ServiceProvider.GetRequiredService<IAmazon>();
            logger = _logger;
        }
      
        public override int MaxGet { get => 10; }

        public override int MaxGets { get => 50; }

        public override commonTypeStore GetStore(StoreRegion store)
        {
            var storeCache = this.storeCache.Get(store.ID);
            if (storeCache == null)
            {
                throw new AggregateException($"{store.ID}-{store.Name} not founed in cache");
            }
            var Key = storeCache.GetSPAmazon()!;
            var developInfo = amazonSPKeyCache.Get(Key.keyID)!;   //amazon开发者信息
            var data = platformDataCache.Get(Key.PlatformDataID)!.GetAmazonData();
            if (data == null)
            {
                throw new Exception($"platformDataCache {Key.PlatformDataID} is null");
            }

            if (store.MarketPlaces == null || store.MarketPlaces.Count == 0)
            {
                logger.LogInformation($"store market is null,get from cache ");
                store.MarketPlaces = storeMarket.List(storeCache.UniqueHashCode).Select(x=>x.Marketplace).ToList();
            }

            store.amazonToken = new OrderSDK.Modles.CommonType.AmazonToken(Key.AccessToken, Key.RefreshToken, Key.ExpireIn, Key.Date_Created, Key.TokenType, Key.SellerID, Key.PlatformDataID, Key.keyID);
            
            store.amazonCredential = new AmazonCredential(developInfo.AccessKey, developInfo.SecretKey, developInfo.RoleArn, developInfo.ClientId, developInfo.ClientSecret, developInfo.RedirectUri, data.Host, data.RegionName);
                     
            if(storeCache.amazonStore is null)
            {
                storeCache.amazonStore= new commonTypeStore(store.ID, store.Name, store.MarketPlaces, store.amazonToken, store.amazonCredential);
            }
            return storeCache.amazonStore;
        }

        #region 店铺相关
        public override string GetAuthURL(StoreRegion store, string state)
        {
            var Key = store.GetSPAmazon()!;
            var sp = amazonSPKeyCache.Get(Key.keyID)!;   //amazon开发者信息

            //亚马逊市场，所属市场的url等信息
            string orgUrl = platformDataCache.Get(Key.PlatformDataID)!.GetAmazonData()!.SellerCentralURL!;
            //sp.RedirectUri
            var redirectUri = "https://api.huasishuo.com/aws/amaStoreInfo/author";
            return amazon.GetAuthURL(orgUrl, sp.ApplicationId, redirectUri, state);
        }

        /// <summary>
        /// 验证授权
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="store"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        public override async Task<T?> VerifyStore<T>(StoreRegion store, string code = "") where T : class
        {
            var s = GetStore(store);
            var m = await amazon.VerifyStore(s, code);
            return m as T;
        }

        /// <summary>
        /// 刷新token
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        public override async Task<bool> RefreshNormalAccessToken(StoreRegion store)
        {
            var s = GetStore(store);
            var res = await amazon.RefreshNormalAccessToken(s);
            return res.IsNotNull() ? true : false;
        }


        /// <summary>
        /// 获取店铺下市场信息
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        public override async Task<List<string>?> GetMarketIds(StoreRegion store)
        {
            var s = GetStore(store);
            var res = await amazon.GetMarketIds(s);
            if (!string.IsNullOrWhiteSpace(res.Item1))
            {
                logger.LogError($"GetMarketIds error {store.Name}:{res.Item1}");
            }
            else
            {
                if (res.Item2 != null)
                {
                    return res.Item2.Select(x => x.Marketplace.Id).ToList();
                }
            }
            return null;
        }

        #endregion


        #region 订单
        /// <summary>
        /// 测试使用
        /// </summary>
        /// <param name="store"></param>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        public override async Task<Model?> GetTest(StoreRegion store, string orderNo)
        {
            try
            {
                var s = GetStore(store);
                var orderBasicRes = await amazon.GetOrder(s, orderNo);
                if (!string.IsNullOrWhiteSpace(orderBasicRes.Item1))
                {
                    logger.LogError($"GetTest GetOrder error {store.Name}-{orderNo}:{orderBasicRes.Item1}");
                    return null;
                }
                var orderBase = orderBasicRes.Item2;
                if (orderBase == null)
                {
                    logger.LogError($"GetTest GetOrder is null,orderNo:{orderNo}");
                    return null;
                }
                var MarketplaceId = orderBase.MarketplaceId;
                //订单详情
                var OrderItemsRes = await amazon.GetOrderItem(s, orderNo);
                if (!string.IsNullOrWhiteSpace(OrderItemsRes.ErrorMsg))
                {
                    logger.LogError($"GetTest GetOrderItem error {store.Name}-{orderNo}:{OrderItemsRes.ErrorMsg}");
                    return null;
                }
                var orderItems = OrderItemsRes.orderItems;
                if (orderItems is null || orderItems.Count <= 0)
                {
                    logger.LogError($"GetTest GetOrderItem {store.Name}-{orderNo} result is null");
                    return null;
                }
                foreach (var item in orderItems)
                {
                    if (string.IsNullOrEmpty(item.ASIN))
                    {
                        logger.LogError($"GetTest {store.Name}-{orderNo} asin is  null");
                        return null;
                    }

                    var catelogItemRes = await amazon.GetCatalogItem(item.ASIN, MarketplaceId, s);
                    if (!string.IsNullOrWhiteSpace(catelogItemRes.Item1))
                    {
                        logger.LogError($"GetTest GetCatalogItem error {store.Name}-{orderNo}:{catelogItemRes.Item1}");
                        return null;
                    }
                    var catelogItem = catelogItemRes.Item2;
                    if (catelogItem.IsNull() || catelogItem.AttributeSets.IsNull() || catelogItem.AttributeSets.Count <= 0)
                    {
                        logger.LogError($"GetTest GetCatalogItem {store.Name}-{orderNo} result is  null");
                        return null;
                    }
                    item.AttributeSet = catelogItem.AttributeSets[0];

                    var MyFeesEstimateResultRes = await amazon.GetFeesEstimate(s, MarketplaceId, item);
                    if (!string.IsNullOrWhiteSpace(MyFeesEstimateResultRes.Item1))
                    {
                        logger.LogError($"GetTest GetFeesEstimate error {store.Name}-{orderNo}:{MyFeesEstimateResultRes.Item1}");
                        return null;
                    }
                    var MyFeesEstimateResult = MyFeesEstimateResultRes.Item2;
                    if (MyFeesEstimateResult is not null && MyFeesEstimateResult.FeesEstimate is not null && MyFeesEstimateResult.FeesEstimate.TotalFeesEstimate is not null)
                    {
                        item.TotalFeesEstimate = MyFeesEstimateResult.FeesEstimate.TotalFeesEstimate;
                    }
                }

                var addressRes = await amazon.GetAddress(s, orderNo);
                if (!string.IsNullOrWhiteSpace(addressRes.Item1))
                {
                    logger.LogError($"GetTest GetAddress error {store.Name}-{orderNo}:{addressRes.Item1}");
                    return null;
                }
                var address = addressRes.Item2;
                if (address.IsNull())
                {
                    logger.LogError($"GetTest GetAddress {store.Name}-{orderNo} result is  null");
                    return null;
                }
                orderBase.ShippingAddress = address.ShippingAddress;


                var orderBuyerInfoRes = await amazon.GetOrderBuyerInfo(s, orderNo);
                if (!string.IsNullOrWhiteSpace(orderBuyerInfoRes.Item1))
                {
                    logger.LogError($"GetTest GetOrderBuyerInfo error {store.Name}-{orderNo}:{orderBuyerInfoRes.Item1}");
                    return null;
                }
                var BuyerInfo = orderBuyerInfoRes.Item2;
                if (BuyerInfo.IsNull())
                {
                    logger.LogError($"GetTest GetOrderBuyerInfo {store.Name}-{orderNo} result is  null");
                    return null;
                }
                orderBase.BuyerInfo.BuyerCounty = BuyerInfo.BuyerCounty;
                orderBase.BuyerInfo.BuyerEmail = BuyerInfo.BuyerEmail;
                orderBase.BuyerInfo.BuyerName = BuyerInfo.BuyerName;
                orderBase.BuyerInfo.BuyerTaxInfo = BuyerInfo.BuyerTaxInfo;

                //财务信息可能没有，属于正常
                OrderSDK.Modles.Amazon.Finances.ShipmentEventList shipmentEvents = new();
                OrderSDK.Modles.Amazon.Finances.ShipmentEventList refundEventList = new();
                var financialEventsRes = await amazon.ListFinancialEventsByOrderId(s, orderNo);

                if (!string.IsNullOrWhiteSpace(financialEventsRes.Item1))
                {
                    logger.LogError($"GetTest ListFinancialEventsByOrderId error {store.Name}-{orderNo}:{financialEventsRes.Item1}");
                    return null;
                }
                var financialEvents = financialEventsRes.Item2;
                if (financialEvents is not null && financialEvents.Count >= 0)
                {
                    shipmentEvents = financialEvents[0].ShipmentEventList;
                    refundEventList = financialEvents[0].RefundEventList;
                }
                return new Model(store, orderBase, orderItems, shipmentEvents, refundEventList, nationCache, platformDataCache);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);

            }
            return null;
        }


        #region pull order

        public override async Task<ListResult> ListImp(ListParameter parameter)
        {
            var s = GetStore(parameter.store);
            var p = new apiListParameter(s, parameter.Start, parameter.End, parameter.NextToken);
            var res = await amazon.ListImp(p);

            if (!string.IsNullOrWhiteSpace(res.Item1))
            {
                logger.LogError($"List  error {parameter.store.ID}:{res.Item1}");
                return new(res.Item1);
            }
            else
            {
                var orders = res.Item2.orderList;
                var NextToken = res.Item2.NextToken;
                return new(!string.IsNullOrEmpty(NextToken), NextToken, orders.Select(i => new WaitOrder(i)).ToList());
            }
        }

        public override Model GetOrderBasic(StoreRegion store, WaitOrder order)
        {
            var orderBasic = order.GetAmazoNewParam();
            return new Model(store, orderBasic, nationCache, platformDataCache);
        }


        public override async Task<(List<AsinModel>?, string)> GetOrderItem(StoreRegion store, List<Model> orders, AsinProducrService asinProducrService)
        {
            string result = "";
            var s = GetStore(store);
            var key = store.GetSPAmazon();
            List<AsinModel> asinProducts = new();
            foreach (var order in orders)
            {
                if (order.OrderNo.IsNullOrEmpty())
                {
                    logger.LogError($"GetOrderItem {store.Name} order.OrderNo is empty");
                    continue;
                }
                if (order.pullOrderState != Enums.Orders.PullOrderState.Item)
                {
                    continue;
                }
                string orderNo = order.OrderNo!;
                string MarketplaceId = order.MarketPlace;

                var OrderItemsRes = await amazon.GetOrderItem(s, orderNo);
                if (!string.IsNullOrWhiteSpace(OrderItemsRes.ErrorMsg))
                {
                    logger.LogError($"UpdateOrder GetOrderItem error {store.Name}-{orderNo}:{OrderItemsRes.ErrorMsg}");
                    if (OrderItemsRes.ErrorMsg.Contains("Unauthorized", StringComparison.CurrentCultureIgnoreCase))
                    {
                        result = "-1";
                        return (null, result);
                    }
                    continue;
                }
                var orderItems = OrderItemsRes.orderItems;
                if (orderItems is null || orderItems.Count <= 0)
                {
                    logger.LogError($"UpdateOrder GetOrderItem is null,{store.Name}-{orderNo}");
                    continue;
                }
                var NumTemp = 0;
                order.ClearGoods();
                //获取order中的产品数据，如果产品下架，为null也正常                                                                    
                foreach (var item in orderItems)
                {
                    if (string.IsNullOrEmpty(item.ASIN))
                    {
                        logger.LogError($"GetOrderItem  asin is empty");
                        continue;
                    }

                    ////接口调用太慢，预估价格功能
                    //var feeResponse = await amazon.GetFeesEstimate(s, MarketplaceId, item);
                    //if (!string.IsNullOrWhiteSpace(feeResponse.Item1))
                    //{
                    //    logger.LogError($"UpdateOrder GetFeesEstimate error:{feeResponse.Item1},{store.Name}-{orderNo}");
                    //    if (feeResponse.Item1.Contains("Unauthorized", StringComparison.CurrentCultureIgnoreCase))
                    //    {
                    //        result = "-1";
                    //        return (null, result);
                    //    }
                    //    continue;
                    //}
                    //var feesEstimateResult = feeResponse.Item2;
                    //if (feesEstimateResult.IsNull())
                    //{
                    //    logger.LogError($"UpdateOrder GetFeesEstimate is null,{store.Name}-{orderNo}");
                    //    continue;
                    //}

                    //if (feesEstimateResult.FeesEstimate is not null && feesEstimateResult.FeesEstimate.TotalFeesEstimate is not null)
                    //{
                    //    item.TotalFeesEstimate = feesEstimateResult.FeesEstimate.TotalFeesEstimate;
                    //}
                    //order.FeesEstimate = orderItems.Sum(s => s.TotalFeesEstimate == null ? 0 : new MoneyMeta(s.TotalFeesEstimate.CurrencyCode, (decimal)s.TotalFeesEstimate.Amount));

                    string SmallImage = "", Brand = "", Size = "", Color = "";
                    OrderItemBuyerInfo? buyInfoItem = null;
                    var product = await asinProducrService.Get(key.SellerID, item.SellerSKU, store.ID);
                    if (product is not null && product!.ImageUrl.IsNotNull())
                    {
                        SmallImage = product.ImageUrl ?? "";
                        Brand = product.Brand ?? "";
                        Size = product.Size ?? "";
                        Color = product.Color ?? "";
                    }
                    else
                    {
                        var catelogItemRes = await amazon.GetCatalogItem(item.ASIN, MarketplaceId, s);
                        if (!string.IsNullOrWhiteSpace(catelogItemRes.Item1))
                        {
                            logger.LogError($"UpdateOrder GetCatalogItem error:{catelogItemRes.Item1},{store.Name}-{orderNo}");
                            if (catelogItemRes.Item1.Contains("Unauthorized", StringComparison.CurrentCultureIgnoreCase))
                            {
                                result = "-1";
                                return (null,result);
                            }
                            continue;
                        }
                        var catelogItem = catelogItemRes.Item2;
                        if (catelogItem.IsNull() || catelogItem.AttributeSets.IsNull() || catelogItem.AttributeSets.Count <= 0)
                        {
                            logger.LogError($"UpdateOrder GetCatalogItem is null,{store.Name}-{orderNo}");
                            continue;
                        }
                        var attr = catelogItem.AttributeSets[0];
                        SmallImage = attr.SmallImage.IsNull() ? "" : attr.SmallImage.URL;
                        Brand = attr.Brand.IsNull() ? "" : attr.Brand;
                        Size = attr.Size.IsNull() ? "" : attr.Size;
                        Color = attr.Color.IsNull() ? "" : attr.Color;
                        var newAsin = new AsinModel(key.SellerID, item.SellerSKU, item.ASIN, store.ID, Brand, Size, Color, SmallImage);
                        asinProducts.Add(newAsin);
                        
                        var orderItemsBuyInfo = await amazon.GetOrderItemsBuyerInfo(s, orderNo);
                        if (!string.IsNullOrWhiteSpace(orderItemsBuyInfo.Item1))
                        {
                            logger.LogError($"UpdateOrder GetOrderItemsBuyerInfo error:{orderItemsBuyInfo.Item1},{store.Name}-{orderNo}");
                            continue;
                        }
                        var orderItemsBuyInfoItem = orderItemsBuyInfo.Item2;
                        if (orderItemsBuyInfoItem.IsNull() || orderItemsBuyInfoItem.OrderItems.Count <= 0)
                        {
                            logger.LogError($"UpdateOrder orderItemsBuyInfoItem is null,{store.Name}-{orderNo}");
                            continue;
                        }
                        buyInfoItem = orderItemsBuyInfoItem.OrderItems
                            .FirstOrDefault(m => m.OrderItemId == item.OrderItemId);

                    }

                    order.GoodsScope(GoodList =>
                    {
                        Good good = new();
                        good.ID = item.ASIN;
                        good.OrderItemId = item.OrderItemId;
                        good.CustomizedURL = buyInfoItem?.BuyerCustomizedInfo?.CustomizedURL;
                        good.FromURL = $"{platformDataCache.GetOrg(store.Platform, MarketplaceId)}/dp/{item.ASIN}";
                        good.Name = item.Title;
                        good.Sku = item.SellerSKU;
                        good.ImageURL = SmallImage;
                        good.Brand = Brand;
                        good.Size = Size;
                        good.Color = Color;
                        NumTemp += item.QuantityOrdered;
                        good.QuantityOrdered = item.QuantityOrdered;
                        good.QuantityShipped = item.QuantityShipped;
                        good.ItemPrice = item.ItemPrice.IsNotNull() ? item.ItemPrice.Mate : new Data.MoneyMeta();
                        good.ShippingPrice = item.ShippingPrice.IsNotNull() ? item.ShippingPrice.Mate : new Data.MoneyMeta();
                        good.UnitPrice = good.QuantityOrdered > 0 ? good.ItemPrice with { Money = good.ItemPrice.Money / good.QuantityOrdered } : -1;
                        good.TotalPrice = good.ItemPrice with { Money = good.ItemPrice.Money + good.ShippingPrice.Money };
                        good.promotionDiscount = item.PromotionDiscount.IsNotNull() ? item.PromotionDiscount.Mate : new Data.MoneyMeta();
                        good.ShippingTax = item.ShippingTax.IsNotNull() ? item.ShippingTax.Mate : new Data.MoneyMeta();//shipping_tax
                        good.ShippingDiscountTax = item.ShippingDiscountTax.IsNotNull() ? item.ShippingDiscountTax.Mate : new Data.MoneyMeta();
                        good.PromotionDiscountTax = item.PromotionDiscountTax.IsNotNull() ? item.PromotionDiscountTax.Mate : new Data.MoneyMeta();
                        good.ItemTax = item.ItemTax.IsNotNull() ? item.ItemTax.Mate : new Data.MoneyMeta();//item_tax
                        GoodList.Add(good);
                    }, Modes.Save);
                }
                order.pullOrderState = Enums.Orders.PullOrderState.Address;
                order.ProductNum = NumTemp;
            }
            return (asinProducts,result);
        }

        public override async Task GetOrderItem(StoreRegion store, string orderNo, string Marketplaced)
        {             
            var s = GetStore(store);
            var key = store.GetSPAmazon();
            List<AsinModel> asinProducts = new();
            var OrderItemsRes = await amazon.GetOrderItem(s, orderNo);
            if (!string.IsNullOrWhiteSpace(OrderItemsRes.ErrorMsg))
            {
                logger.LogError($"UpdateOrder GetOrderItem error {store.Name}-{orderNo}:{OrderItemsRes.ErrorMsg}");                
            }
            var orderItems = OrderItemsRes.orderItems;
            if (orderItems is null || orderItems.Count <= 0)
            {
                logger.LogError($"UpdateOrder GetOrderItem is null,{store.Name}-{orderNo}");
            }
            //获取order中的产品数据，如果产品下架，为null也正常

            foreach (var item in orderItems)
            {
                if (string.IsNullOrEmpty(item.ASIN))
                {
                    logger.LogError($"GetOrderItem  asin is empty");
                }

                var catelogItemRes = await amazon.GetCatalogItem(item.ASIN, Marketplaced, s);
                if (!string.IsNullOrWhiteSpace(catelogItemRes.Item1))
                {
                    logger.LogError($"UpdateOrder GetCatalogItem error:{catelogItemRes.Item1},{store.Name}-{orderNo}");
                }
                var catelogItem = catelogItemRes.Item2;


                //接口调用太慢，暂时屏蔽添加预估价格功能
                var feeResponse = await amazon.GetFeesEstimate(s, Marketplaced, item);

                var feesEstimateResult = feeResponse.Item2;
                           
            }
        }


        public override async Task<string> GetOrderAddress(StoreRegion store, List<Model> orders)
        {
            string result = "";
            var s = GetStore(store);
            foreach (var order in orders)
            {
                if (order.OrderNo.IsNullOrEmpty())
                {
                    logger.LogError($"GetOrderAddress {store.Name} order.OrderNo is empty");
                    continue;
                }
                if (order.pullOrderState != Enums.Orders.PullOrderState.Address)
                {
                    continue;
                }
                string orderNo = order.OrderNo!;
                string MarketplaceId = order.MarketPlace;
                string tip = $"【{store.Name}】-【{orderNo}】";

                var addressRes = await amazon.GetAddress(s, orderNo);
                if (!string.IsNullOrWhiteSpace(addressRes.Item1))
                {
                    logger.LogError($"GetOrderAddress error {addressRes.Item1},{store.Name}-{orderNo}");
                    if (addressRes.Item1.Contains("Unauthorized", StringComparison.CurrentCultureIgnoreCase))
                    {
                        result= "-1";
                        return result;
                    }
                    continue;
                }
                var address = addressRes.Item2;
                if (address.IsNull())
                {
                    logger.LogError($"GetOrderAddress is  null,{store.Name}-{orderNo}");
                    continue;
                }

                order.pullOrderState = Enums.Orders.PullOrderState.BuyerInfo;
                if (address.ShippingAddress is not null)
                {
                    var shippingAddress = address.ShippingAddress;
                    var nation = nationCache.Get(shippingAddress.CountryCode) ?? nationCache.Get(platformDataCache.GetAmazonNationId(store.Platform, MarketplaceId));
                    order.Receiver = new()
                    {
                        Name = shippingAddress.Name,
                        Phone = shippingAddress.Phone,
                        //Email = orderBuyerInfo.BuyerEmail,
                        Zip = shippingAddress.PostalCode,
                        NationId = nation?.ID,
                        Nation = nation?.Name,
                        NationShort = nation?.Short,
                        Province = shippingAddress.StateOrRegion,
                        City = string.IsNullOrWhiteSpace(shippingAddress.City) ? shippingAddress.StateOrRegion : shippingAddress.City,
                        County = shippingAddress.County,
                        District = shippingAddress.District,
                        Address1 = shippingAddress.AddressLine1,
                        Address2 = shippingAddress.AddressLine2,
                        Address3 = shippingAddress.AddressLine3,
                    };
                }
                else
                {
                    var nation = nationCache.Get(platformDataCache.GetAmazonNationId(store.Platform, MarketplaceId));
                    order.Receiver = new()
                    {
                        Name = "",
                        Phone = "",
                        Email = "",
                        Zip = "",
                        NationId = nation?.ID,
                        Nation = nation?.Name,
                        NationShort = nation?.Short,
                        Province = "",
                        City = "",
                        County = "",
                        District = "",
                        Address1 = "",
                        Address2 = "",
                        Address3 = "",
                    };
                }
            }
            return result;
        }





        public override async Task<string> GetOrderBuyerInfo(StoreRegion store, List<Model> orders)
        {
            string result = "";
            var s = GetStore(store);
            foreach (var order in orders)
            {
                if (order.OrderNo.IsNullOrEmpty())
                {
                    logger.LogError($"GetOrderBuyerInfo {store.Name} {order.ID} orderNo is empty");
                    continue;
                }
                if (order.pullOrderState != Enums.Orders.PullOrderState.BuyerInfo)
                {
                    continue;
                }
                string orderNo = order.OrderNo!;
                string tip = $"【{store.Name}】-【{orderNo}】";

                var orderBuyerInfoRes = await amazon.GetOrderBuyerInfo(s, orderNo);
                if (!string.IsNullOrWhiteSpace(orderBuyerInfoRes.Item1))
                {
                    logger.LogError($"GetOrderBuyerInfo error {orderBuyerInfoRes.Item1},{store.Name}-{orderNo}");
                    if (orderBuyerInfoRes.Item1.Contains("Unauthorized", StringComparison.CurrentCultureIgnoreCase))
                    {
                        result = "-1";
                        return result;
                    }
                    continue;
                }
                var BuyerInfo = orderBuyerInfoRes.Item2;
                if (BuyerInfo.IsNull())
                {
                    logger.LogError($"GetOrderBuyerInfo is null,{store.Name}-{orderNo}");
                    continue;
                }
                if (order.Receiver is not null)
                {
                    order.Receiver.Email = BuyerInfo.BuyerEmail;
                }
                order.BuyerUserName = BuyerInfo.BuyerName;
                order.pullOrderState = Enums.Orders.PullOrderState.Financiall;
            }
            return result;
        }

        public override async Task<string> GetOrderFinanciall(StoreRegion store, List<Model> orders)
        {
            try
            {
                string result = "";
                var s = GetStore(store);
                foreach (var order in orders)
                {
                    if (order.OrderNo.IsNullOrEmpty())
                    {
                        logger.LogError($"GetOrderFinanciall {store.Name} {order.ID} orderNo is empty");
                        continue;
                    }
                    if (order.pullOrderState != Enums.Orders.PullOrderState.Financiall)
                    {
                        continue;
                    }
                    string orderNo = order.OrderNo!;
                    string MarketplaceId = order.MarketPlace;
                    string tip = $"【{store.Name}】-【{orderNo}】";
                    double fee = 0;
                    double refund = 0;

                    //财务信息可能没有，属于正常
                    OrderSDK.Modles.Amazon.Finances.ShipmentEventList shipmentEventList = new();
                    OrderSDK.Modles.Amazon.Finances.ShipmentEventList refundEventList = new();
                    var financialEventsRes = await amazon.ListFinancialEventsByOrderId(s, orderNo);

                    if (!string.IsNullOrWhiteSpace(financialEventsRes.Item1))
                    {
                        logger.LogError($"ListFinancialEventsByOrderId error {financialEventsRes.Item1},{store.Name}-{orderNo}");
                        if (financialEventsRes.Item1.Contains("Unauthorized", StringComparison.CurrentCultureIgnoreCase))
                        {
                            result = "-1";
                            return result;
                        }
                        continue;
                    }
                    var financialEvents = financialEventsRes.Item2;
                    if (financialEvents is not null && financialEvents.Count >= 0)
                    {
                        shipmentEventList = financialEvents[0].ShipmentEventList;
                        refundEventList = financialEvents[0].RefundEventList;

                        //装运事件列表。                 
                        if (shipmentEventList is not null && shipmentEventList.Count > 0)
                        {
                            foreach (var ShipmentItem in shipmentEventList)
                            {
                                if (ShipmentItem.IsNotNull() && ShipmentItem.ShipmentItemList.IsNotNull() && ShipmentItem.ShipmentItemList.Count > 0)
                                {
                                    //ShipmentItemList  与货件相关的费用列表。                               
                                    foreach (var shipmentItemList in ShipmentItem.ShipmentItemList)
                                    {
                                        if (shipmentItemList.IsNotNull())
                                        {
                                            //ItemTaxWithheldList 预扣税信息列表
                                            if (shipmentItemList.ItemTaxWithheldList.IsNotNull() && shipmentItemList.ItemTaxWithheldList.Count > 0)
                                            {
                                                foreach (var TaxesWithheld in shipmentItemList.ItemTaxWithheldList)
                                                {
                                                    if (TaxesWithheld.IsNotNull() && TaxesWithheld.TaxesWithheld.IsNotNull() && TaxesWithheld.TaxesWithheld.Count > 0)
                                                    {
                                                        foreach (var ChargeAmount in TaxesWithheld.TaxesWithheld)
                                                        {
                                                            if (ChargeAmount.IsNotNull())
                                                            {
                                                                fee += ChargeAmount.ChargeAmount.CurrencyAmount;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if (shipmentItemList.ItemFeeList.IsNotNull() && shipmentItemList.ItemFeeList.Count > 0)
                                            {
                                                //亚马逊平台扣除的佣金
                                                foreach (var FeeComponent in shipmentItemList.ItemFeeList)
                                                {
                                                    if (FeeComponent.IsNotNull())
                                                    {
                                                        fee += FeeComponent.FeeAmount.CurrencyAmount;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                        }

                        //退款事件列表
                        if (refundEventList is not null && refundEventList.Count > 0)
                        {
                            foreach (var ShipmentEvent in refundEventList)
                            {
                                if (ShipmentEvent.ShipmentItemAdjustmentList.IsNotNull() && ShipmentEvent.ShipmentItemAdjustmentList.Count > 0)
                                {
                                    foreach (var ItemAdjustment in ShipmentEvent.ShipmentItemAdjustmentList)
                                    {
                                        //与装运项目相关的费用调整列表。 仅当退款、保证索赔和退款事件时才会返回该值。
                                        //ItemChargeAdjustmentList

                                        if (ItemAdjustment.ItemChargeAdjustmentList.IsNotNull())
                                        {
                                            foreach (var ItemChargeList in ItemAdjustment.ItemChargeAdjustmentList)
                                            {
                                                //退款金额
                                                if (ItemChargeList.IsNotNull())
                                                {
                                                    refund += ItemChargeList.ChargeAmount.CurrencyAmount;
                                                }
                                            }
                                        }

                                        //与装运项目相关的费用调整列表。 仅当退款、保证索赔和退款事件时才会返回该值。
                                        //ItemFeeAdjustmentList
                                        if (ItemAdjustment.ItemFeeAdjustmentList.IsNotNull())
                                        {
                                            foreach (var ItemFeeAdjustmentList in ItemAdjustment.ItemFeeAdjustmentList)
                                            {
                                                //退款手续费
                                                if (ItemFeeAdjustmentList.IsNotNull())
                                                {
                                                    fee += ItemFeeAdjustmentList.FeeAmount.CurrencyAmount;
                                                }
                                            }
                                        }
                                        //扣税信息列表                                    
                                        if (ItemAdjustment.ItemTaxWithheldList.IsNotNull() && ItemAdjustment.ItemTaxWithheldList.Count > 0)
                                        {
                                            foreach (var ItemTaxWithheldList in ItemAdjustment.ItemTaxWithheldList)
                                            {
                                                if (ItemTaxWithheldList.TaxesWithheld.IsNotNull() && ItemTaxWithheldList.TaxesWithheld.Count > 0)
                                                {
                                                    foreach (var ChargeComponent in ItemTaxWithheldList.TaxesWithheld)
                                                    {
                                                        //税 2021.4.8 董哥要求
                                                        if (ChargeComponent.ChargeAmount.IsNotNull())
                                                        {
                                                            fee += ChargeComponent.ChargeAmount.CurrencyAmount;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        var unit = order.OrderTotal.Raw.CurrencyCode;
                        if(!string.IsNullOrEmpty(unit))
                        {
                            order.Fee = new MoneyMeta(unit, (decimal)Math.Abs(fee));
                            if (order.Fee.Raw.Amount > 0)
                            {
                                order.State = States.SENT;
                            }
                            order.Refund = new MoneyMeta(unit, (decimal)Math.Abs(refund));
                            var p = order.OrderTotal.Money - order.Fee.Money - order.Refund.Money -
                             order.ShippingMoney.Money - order.Loss.Money - order.PurchaseFee.Money;

                            decimal m = Math.Round(p / order.OrderTotal.Raw.Rate, 2);

                            order.Profit = new MoneyMeta(unit, m);
                        }                                               
                    }
                    order.pullOrderState = Enums.Orders.PullOrderState.OutOfProcess;
                }
                return result;
            }
            catch (Exception ex)
            {
                logger.LogError($"GetOrderFinanciall:Exception {ex.Message}");
                logger.LogError($"GetOrderFinanciall Exception: {ex.StackTrace}");
                throw ex;
            }
        }
        
        public override async Task GetOrderFinanciall(StoreRegion store, string orderNo)
        {
            var s = GetStore(store);

            //财务信息可能没有，属于正常
            OrderSDK.Modles.Amazon.Finances.ShipmentEventList shipmentEvents = new();
            OrderSDK.Modles.Amazon.Finances.ShipmentEventList refundEventList = new();
            var financialEventsRes = await amazon.ListFinancialEventsByOrderId(s, orderNo);

            if (!string.IsNullOrWhiteSpace(financialEventsRes.Item1))
            {
                logger.LogError($"ListFinancialEventsByOrderId error {financialEventsRes.Item1},{store.Name}-{orderNo}");
            }
            var financialEvents = financialEventsRes.Item2;
        } 

        #endregion

        #endregion


        #region  产品信息

        public async Task<Stream> HttpDownload(string url)
        {
            using (var client = new WebClient())
            {
                return await client.OpenReadTaskAsync(url);
            }
        }

        public void DownExcel(Stream stream, int storeID)
        {
            var path = $"{storeID}-{DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss")}-asin.xls";
            using (var file = new FileStream(path, FileMode.Create))
            {
                file.CopyTo(stream);
            }
        }

        /// <summary>
        /// 获取指定店铺指定市场下的所有产品
        /// 支持多个市场id
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        public override async Task<List<AsinInfo>> GetAllAsin(StoreRegion store)
        {
            var result = new List<AsinInfo>();
            var s = GetStore(store);
            var report = await amazon.GetAllAsin(s);
            if (report.IsNotNull())
            {
                try
                {
                    Stream stream = Stream.Null;
                    var Encoding = System.Text.Encoding.Default;
                    bool downLoad = false;
                    //解压gzip，读取stream流
                    var compressedFileStream = HttpDownload(report!.Url).Result;
                    if (report.CompressionAlgorithm != null)
                    {
                        if (report.CompressionAlgorithm == ReportDocument.CompressionAlgorithmEnum.GZIP)
                        {
                            using (var decompressStream = new GZipStream(compressedFileStream, CompressionMode.Decompress))
                            {
                                stream = decompressStream;
                            }
                        }
                        else
                        {
                            throw new ArgumentNullException("# GetAllAsin unknown CompressionAlgorithm");
                        }
                    }
                    else
                    {
                        stream = compressedFileStream;
                    }

                    using (var Reader = new StreamReader(stream, Encoding))
                    {
                        const char Spliter = '\t';
                        var HeaderLine = Reader.ReadLine();
                        if (HeaderLine is null)
                        {
                            throw new ArgumentNullException("# GetAllAsin HeaderLine is null");
                        }
                        else
                        {
                            int ColNum = 0;
                            int? AsinIndex = null, SkuIndex = null, NameIndex = null, DescriptionIndex = null, priceIndex = null, quantityIndex = null,
                               imageurlIndex = null, marketplaceIndex = null, productIdtypeIndex = null, productIdIndex = null;
                            foreach (var item in HeaderLine.Split(Spliter))
                            {
                                //item-name	item-description	listing-id	seller-sku	price	quantity	open-date	image-url	item-is-marketplace	product-id-type	zshop-shipping-fee	item-note	item-condition	zshop-category1	zshop-browse-path	zshop-storefront-feature	asin1	asin2	asin3	will-ship-internationally	expedited-shipping	zshop-boldface	product-id	bid-for-featured-placement	add-delete	pending-quantity	fulfillment-channel	merchant-shipping-group	status	Minimum order quantity	Sell remainder

                                if (item.Equals("Asin1", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    AsinIndex = ColNum;
                                }
                                if (item.Equals("seller-sku", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    SkuIndex = ColNum;
                                }
                                if (item.Equals("item-name", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    NameIndex = ColNum;
                                }
                                if (item.Equals("item-description", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    DescriptionIndex = ColNum;
                                }
                                if (item.Equals("price", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    priceIndex = ColNum;
                                }
                                if (item.Equals("quantity", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    quantityIndex = ColNum;
                                }
                                if (item.Equals("image-url", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    imageurlIndex = ColNum;
                                }
                                if (item.Equals("item-is-marketplace", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    marketplaceIndex = ColNum;
                                }
                                if (item.Equals("product-id-type", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    productIdtypeIndex = ColNum;
                                }
                                if (item.Equals("product-id", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    productIdIndex = ColNum;
                                }
                                ColNum++;
                            }
                            if (AsinIndex.HasValue && SkuIndex.HasValue)
                            {
                                string? Line;
                                while ((Line = Reader.ReadLine()) is not null)
                                {
                                    var Cols = Line.Split(Spliter);
                                    var CurColNum = Cols.Count();
                                    if (CurColNum != ColNum)
                                    {
                                        throw new IndexOutOfRangeException("#GetAllAsin column number not equal");
                                    }
                                    else
                                    {
                                        var Asin = Cols[AsinIndex.Value];
                                        var Sku = Cols[SkuIndex.Value];
                                        var Name = Cols[NameIndex.Value];
                                        var Description = Cols[DescriptionIndex.Value];
                                        var Price = Cols[priceIndex.Value];
                                        var Quantity = Cols[quantityIndex.Value];
                                        var ImageUrl = Cols[imageurlIndex.Value];
                                        var Marketplace = Cols[marketplaceIndex.Value];
                                        var ProductIDType = Cols[productIdtypeIndex.Value];
                                        var ProductID = Cols[productIdIndex.Value];
                                        var asinInfo = new AsinInfo(Asin, Sku, Name, Description, Price, Quantity, ImageUrl, Marketplace, ProductIDType, ProductID);
                                        result.Add(asinInfo);
                                    }
                                }
                            }
                            else
                            {
                                throw new InvalidDataException("GetAllAsin Asin1 or Sku not found");
                            }
                        }
                    }
                    //如果downLoad==true，下载文件分析
                    if (downLoad)
                    {
                        DownExcel(stream, store.ID);
                    }
                }
                catch (Exception e)
                {
                    logger.LogError($"GetAllAsin decompress and read stream error:{e.Message},{e}");
                }
            }
            return result;
        }

        public override async Task<List<ItemRes>?> SearchCatalogItems(StoreRegion store, string marketPlaceIds, List<string> asins)
        {
            var s = GetStore(store);
            return await amazon.SearchCatalogItems(s, marketPlaceIds, asins);
        }


        /// <summary>
        /// 获取亚马逊产品的具体信息
        /// </summary>
        /// <param name="store"></param>
        /// <param name="asin"></param>
        /// <returns></returns>
        public override async Task<List<ProductModel>> GetProductInfo(StoreRegion store, List<AsinModel> asins)
        {
            var s = GetStore(store);
            List<ProductModel> products = new();
            var marketPlaceIds = store.MarketPlaces;
            var asinList = asins.Select(x => x.Asin).ToList();
            string asinStr = string.Join(',', asinList);
            var Items = await amazon.SearchCatalogItems(s, marketPlaceIds[0], asinList);

            var Prices = await amazon.GetPricing(s, marketPlaceIds[0], asinList);

            if (!string.IsNullOrWhiteSpace(Prices.Item1))
            {
                logger.LogError($"GetProductInfo GetPricing error {Prices.Item1},{store.ID},{asinStr}");
            }

            foreach (var a in asins)
            {
                if (Items is not null && Prices.Item2 is not null)
                {
                    var asin = a.Asin;
                    var price = Prices.Item2.FirstOrDefault(x => x.ASIN == asin);
                    var item = Items.FirstOrDefault(x => x.asin == asin);
                    var listInfo = await amazon.getListingsItem(s, marketPlaceIds[0], asins.First(x => x.Asin == asin).Sku);
                    if (price != null && listInfo != null)
                    {
                        ProductModel productModel = new ProductResult(price, item, listInfo).ToProduct(a);
                        products.Add(productModel);
                    }
                }
            }
            return products;
        }

        #endregion


        #region 综合使用

        public override async Task<(FeedResult?, string)> SubmitProduicts(StoreRegion store, string data, string marketPlace)
        {
            var s = GetStore(store);
            return await amazon.SubmitProducts(s, data, marketPlace);
        }



        /// <summary>
        /// 手动更新订单信息
        /// </summary>
        /// <param name="store"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public override async Task UpdateOrder(StoreRegion store, Model order)
        {
            var s = GetStore(store);
            if (string.IsNullOrWhiteSpace(order.OrderNo))
            {
                logger.LogError($"{order.ID} OrderNo is null");
            }
            string orderNo = order.OrderNo!;
            string MarketplaceId = order.MarketPlace;

            var orderBasicRes = await amazon.GetOrder(s, orderNo);
            if (!string.IsNullOrWhiteSpace(orderBasicRes.Item1))
            {
                logger.LogError($"UpdateOrder GetOrder error {store.Name}-{orderNo}:{orderBasicRes.Item1}");
                return;
            }
            var orderBase = orderBasicRes.Item2;
            if (orderBase == null)
            {
                logger.LogError($"UpdateOrder GetOrder is null,orderNo:{orderNo}");
                return;
            }
            var orderBasic = new WaitOrder(orderBase).GetAmazoNewParam();
            if (orderBasic.BuyerInfo is not null)
            {
                order.BuyerUserName = orderBasic.BuyerInfo.BuyerName;
            }

            order.PaymentMethod = orderBasic.PaymentMethod.IsNotNull() ? orderBasic.PaymentMethod!.Value.GetName() : string.Empty;
            if (!string.IsNullOrWhiteSpace(orderBasic.PurchaseDate))
            {
                order.CreateTimeRaw = orderBasic.PurchaseDate;
                order.CreateTime = Convert.ToDateTime(orderBasic.PurchaseDate);
                order.PayTimeRaw = orderBasic.PurchaseDate;
                order.PayTime = Convert.ToDateTime(orderBasic.PurchaseDate);
            }
            if (!string.IsNullOrWhiteSpace(orderBasic.LatestShipDate))
            {
                order.LatestShipTime = Convert.ToDateTime(orderBasic.LatestShipDate);
            }
            if (!string.IsNullOrWhiteSpace(orderBasic.LastUpdateDate))
            {
                order.LastUpdateDate = Convert.ToDateTime(orderBasic.LastUpdateDate);
            }
            order.pullOrderState = PullOrderState.Item;
            order.PullCount = 1;
            order.DefaultShipFromLocationAddress = JsonConvert.SerializeObject(orderBasic.DefaultShipFromLocationAddress);
            order.OrderTotal = orderBasic.OrderTotal.IsNotNull() ? orderBasic.OrderTotal.Mate : new Data.MoneyMeta();
            order.Delivery = orderBasic.NumberOfItemsShipped;
            order.DeliveryNo = orderBasic.NumberOfItemsUnshipped;
            if(orderBasic.OrderStatus!=0)
            {
                order.State = ConvertState.GetState(orderBasic.OrderStatus);
            }
            if (orderBasic.FulfillmentChannel is not null)
            {
                order.ShippingType = ConvertState.ShippingTypes(orderBasic.FulfillmentChannel);
            }
            //订单详情
            var OrderItemsRes = await amazon.GetOrderItem(s, orderNo);
            if (!string.IsNullOrWhiteSpace(OrderItemsRes.ErrorMsg))
            {
                logger.LogError($"UpdateOrder GetOrderItem error {store.Name}-{orderNo}:{OrderItemsRes.ErrorMsg}");
                return;
            }
            var orderItems = OrderItemsRes.orderItems;
            if (orderItems is null || orderItems.Count <= 0)
            {
                logger.LogError($"UpdateOrder GetOrderItem is null,{store.Name}-{orderNo}");
                return;
            }
            var NumTemp = 0;
            order.ClearGoods();//更新订单，先置空产品信息

            //获取order中的产品数据，如果产品下架，为null也正常                                                                    
            foreach (var item in orderItems)
            {
                if (string.IsNullOrEmpty(item.ASIN))
                {
                    logger.LogError($"UpdateOrder  asin is null");
                    continue;
                }

                //接口调用太慢，暂时屏蔽添加预估价格功能
                //var feeResponse= GetFeesEstimate(store, MarketplaceId, item);
                //if (feeResponse is not null && feeResponse.FeesEstimateResult is not null && feeResponse.FeesEstimateResult.FeesEstimate is not null && feeResponse.FeesEstimateResult.FeesEstimate.TotalFeesEstimate is not null)
                //{
                //    item.TotalFeesEstimate = feeResponse.FeesEstimateResult.FeesEstimate.TotalFeesEstimate;
                //}
                //order.FeesEstimate = OrderItems.Sum(s => s.TotalFeesEstimate != null ? s.TotalFeesEstimate.Mate : 0);

                string SmallImage = "", Brand = "", Size = "", Color = "";
                var catelogItemRes = await amazon.GetCatalogItem(item.ASIN, MarketplaceId, s);
                if (!string.IsNullOrWhiteSpace(catelogItemRes.Item1))
                {
                    logger.LogError($"UpdateOrder GetCatalogItem error:{catelogItemRes.Item1},{store.Name}-{orderNo}");
                    return;
                }
                var catelogItem = catelogItemRes.Item2;
                if (catelogItem.IsNull() || catelogItem.AttributeSets.Count <= 0)
                {
                    logger.LogError($"UpdateOrder GetCatalogItem is null,{store.Name}-{orderNo}");
                    return;
                }
                var orderItemsBuyInfo = await amazon.GetOrderItemsBuyerInfo(s, orderNo);
                if (!string.IsNullOrWhiteSpace(orderItemsBuyInfo.Item1))
                {
                    logger.LogError($"UpdateOrder GetOrderItemsBuyerInfo error:{orderItemsBuyInfo.Item1},{store.Name}-{orderNo}");
                    return;
                }
                var orderItemsBuyInfoItem = orderItemsBuyInfo.Item2;
                if (orderItemsBuyInfoItem.IsNull() || orderItemsBuyInfoItem.OrderItems.Count <= 0)
                {
                    logger.LogError($"UpdateOrder orderItemsBuyInfoItem is null,{store.Name}-{orderNo}");
                    return;
                }
                var attr = catelogItem.AttributeSets[0];
                SmallImage = attr.SmallImage.IsNull() ? "" : attr.SmallImage.URL;
                Brand = attr.Brand.IsNull() ? "" : attr.Brand;
                Size = attr.Size.IsNull() ? "" : attr.Size;
                Color = attr.Color.IsNull() ? "" : attr.Color;
                var buyInfoItem = orderItemsBuyInfoItem.OrderItems
                    .FirstOrDefault(m => m.OrderItemId == item.OrderItemId);
                order.GoodsScope(GoodList =>
                {
                    Good good = new();
                    good.ID = item.ASIN;
                    good.OrderItemId = item.OrderItemId;
                    good.CustomizedURL = buyInfoItem?.BuyerCustomizedInfo?.CustomizedURL;
                    good.FromURL = $"{platformDataCache.GetOrg(store.Platform, MarketplaceId)}/dp/{item.ASIN}";
                    good.Name = item.Title;
                    good.Sku = item.SellerSKU;
                    good.ImageURL = SmallImage;
                    good.Brand = Brand;
                    good.Size = Size;
                    good.Color = Color;
                    NumTemp += item.QuantityOrdered;
                    good.QuantityOrdered = item.QuantityOrdered;
                    good.QuantityShipped = item.QuantityShipped;
                    good.ItemPrice = item.ItemPrice.IsNotNull() ? item.ItemPrice.Mate : new Data.MoneyMeta();
                    good.ShippingPrice = item.ShippingPrice.IsNotNull() ? item.ShippingPrice.Mate : new Data.MoneyMeta();
                    good.UnitPrice = good.QuantityOrdered > 0 ? good.ItemPrice with { Money = good.ItemPrice.Money / good.QuantityOrdered } : -1;
                    good.TotalPrice = good.ItemPrice with { Money = good.ItemPrice.Money + good.ShippingPrice.Money };
                    good.promotionDiscount = item.PromotionDiscount.IsNotNull() ? item.PromotionDiscount.Mate : new Data.MoneyMeta();
                    good.ShippingTax = item.ShippingTax.IsNotNull() ? item.ShippingTax.Mate : new Data.MoneyMeta();//shipping_tax
                    good.ShippingDiscountTax = item.ShippingDiscountTax.IsNotNull() ? item.ShippingDiscountTax.Mate : new Data.MoneyMeta();
                    good.PromotionDiscountTax = item.PromotionDiscountTax.IsNotNull() ? item.PromotionDiscountTax.Mate : new Data.MoneyMeta();
                    good.ItemTax = item.ItemTax.IsNotNull() ? item.ItemTax.Mate : new Data.MoneyMeta();//item_tax
                    GoodList.Add(good);
                }, Modes.Save);
            }
            order.pullOrderState = Enums.Orders.PullOrderState.Address;
            order.ProductNum = NumTemp;
            order.Profit = order.OrderTotal;
            var orders = new List<Model> { order };
            await GetOrderAddress(store, orders);
            await GetOrderBuyerInfo(store, orders);
            await GetOrderFinanciall(store, orders);
        }

        /// <summary>
        /// 更新亚马逊配送信息
        /// </summary>
        /// <param name="store"></param>
        /// <param name="DP"></param>
        /// <param name="MarketPlaceId"></param>
        /// <returns></returns>
        public override async Task<string> DeliveryGoods(StoreRegion store, List<DeliverProduct> DP, string MarketPlaceId)
        {
            var s = GetStore(store);
            return await amazon.DeliveryGoods(s, DP, MarketPlaceId);
        }

        #endregion



        #region UploadExcelUrl

        public override async Task<string> UploadExcelUrl(StoreRegion store, string MarketPlaceId,string fileUrl)
        {
            var s = GetStore(store);
            return await amazon.UploadExcelUrl(s, MarketPlaceId,fileUrl);
        }


        public override async Task<(string, List<pushProductResult>)> UploadExcelBytes(StoreRegion store, string MarketPlaceId, byte[] bytes)
        {
            var s = GetStore(store);
            return await amazon.UploadExcelBytes(s, MarketPlaceId, bytes);
        }      
        
       
        public override async Task<(FeedResult?, string)> GetFeedDetail(string feedID, StoreRegion store)
        {
            var s = GetStore(store);
            return await amazon.GetFeedDetail(feedID,s);
        }

        #endregion

        #region amazon市场节点和产品信息

        /// <summary>
        /// 获取市场分类节点
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        public override async Task<CategoiesData?> GetReportBowerIDs(StoreRegion store,string marketPlace)
        {
            var s = GetStore(store);
            return await amazon.GetReportBowerID(s, marketPlace);
        }

        /// <summary>
        /// 搜索并返回具有可用定义的亚马逊产品类型列表
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        public override async Task<ProductTypeList?> SearchDefinitionsProductTypes(StoreRegion store)
        {
            var s = GetStore(store);
            return await amazon.SearchDefinitionsProductTypes(s);
        }


        public override async Task<ProductTypeDefinition?> GetDefinitionsProductTypes(StoreRegion store,string marketPlace, string productType)
        {
            var s = GetStore(store);
            return await amazon.GetDefinitionsProductTypes(s, marketPlace, productType);
        }

        /// <summary>
        /// 获取产品schemaJson
        /// </summary>
        /// <param name="store"></param>
        /// <param name="marketPlace"></param>
        /// <param name="productTypeName"></param>
        /// <returns></returns>
        public override async Task<List<AttributeModel>> GetProductTypeModels(StoreRegion store, string marketPlace, string productTypeName)
        {
            List<AttributeModel> attributeModels = new();
            try
            {
                var s = GetStore(store);
                var result = await amazon.GetProductTypeModels(s, marketPlace, productTypeName);
                if (result is not null)
                {
                    var SchemaJson = result.Value.Item1;
                    if (SchemaJson != null)
                    {
                        var pInfo = result.Value.Item2;
                        var requiredAttrLst = SchemaJson.Required;

                        foreach (var p in SchemaJson.Properties)  //几大分组
                        {
                            var name = p.Key;
                            var item = SchemaJson.Properties[name];
                            //解析节点属性
                            if (item is not null)
                            {
                                bool isweight = name.Contains("weight", StringComparison.CurrentCultureIgnoreCase);
                                ///格式修改
                                UpdateDefault(isweight, item, "");
                            }
                        }
                        var json = SchemaJson.ToString();
                        AttributeModel attribute = new AttributeModel();
                        attribute.Platform = Platforms.AMAZONSP;
                        attribute.MarketPlaceID = marketPlace;
                        attribute.ProductType = productTypeName;  // watch
                        var temp = marketPlace + pInfo.type;
                        attribute.AttributeHash = Helpers.CreateHashCode(temp);
                        attribute.Version = pInfo.version;
                        attribute.Locale = pInfo.locale;
                        attribute.State = CategoryState.InUsed;
                        attribute.SchemaJson = json;
                        attributeModels.Add(attribute);
                    }
                    else
                    {
                        logger.LogInformation($"GetProductTypeModels SchemaJson is null,{store.ID}{productTypeName}");
                    }
                }
                else
                {
                    logger.LogInformation($"GetProductTypeModels is null,{store.ID} {productTypeName} is null");
                }
            }
            catch (Exception e)
            {
                logger.LogError($"GetProductTypeModels:{e.Message}");
                logger.LogError($"GetProductTypeModels:{e.StackTrace}");
            }
            return attributeModels;
        }


        public void UpdateDefault(bool isWeight, JSchema jSchema, string name)
        {
            if (jSchema.Items.Count > 0)
            {
                UpdateDefault(isWeight, jSchema.Items[0], "");
            }
            else if (jSchema.Items.Count <= 0 && jSchema.Properties.Count > 0)
            {
                foreach (var properity in jSchema.Properties.Keys)
                {
                    var itemP = jSchema.Properties[properity];
                    UpdateDefault(isWeight, itemP, properity);
                }
            }
            else if (jSchema.Items.Count <= 0 && jSchema.Properties.Count <= 0)
            {
                if (name == "marketplace_id" || name == "language_tag")
                {
                    jSchema.Title = name;
                    if (jSchema.AnyOf.Count > 1)
                    {
                        jSchema.AnyOf.RemoveAt(0);
                    }
                }
                else
                {
                    if (jSchema.AnyOf.Count > 1)
                    {
                        jSchema.AnyOf.RemoveAt(0);
                        var temp = jSchema.AnyOf[0];
                        if (temp.Enum is not null && temp.Enum.Count > 0)
                        {
                            if (temp.Default is null)
                            {
                                temp.Default = temp.Enum.First();
                                jSchema.Default = temp.Default;
                            }
                        }
                    }

                    if (jSchema.OneOf.Count > 1)
                    {
                        jSchema.OneOf.RemoveAt(1);
                        var temp = jSchema.OneOf[0];
                        //if (temp.Enum is not null && temp.Enum.Count > 0)
                        //{
                        //    if (temp.Default is null)
                        //    {
                        //        temp.Default = temp.Enum.First();
                        //        jSchema.Default = temp.Default;
                        //    }
                        //}
                    }
                    if (jSchema.Type == JSchemaType.String)
                    {
                        if (jSchema.Enum is not null && jSchema.Enum.Count > 0)
                        {
                            if (jSchema.Default is null) jSchema.Default = jSchema.Enum.First();
                        }
                        else if (jSchema.Default is null)
                        {
                            jSchema.Default = " ";
                        }
                    }
                    else if (jSchema.Type == JSchemaType.Integer || jSchema.Type == JSchemaType.Number)
                    {
                        if (jSchema.Default is null)
                        {
                            if (isWeight)
                            {
                                jSchema.Default = 1;
                            }
                            else
                            {
                                jSchema.Default = 0;
                            }
                        }
                    }
                    else if (jSchema.Type == JSchemaType.Boolean)
                    {
                        if (jSchema.Default is null) jSchema.Default = false;
                    }
                }
            }
            else
            {
                throw new Exception($"出现特殊情况:{jSchema.Title}");
            }
        }


        #endregion

    }
}


