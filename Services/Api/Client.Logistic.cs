﻿using ERP.Models.Api.Logistics;
using Newtonsoft.Json;
using System.Text;
using System.Xml.Serialization;

using ERP.Exceptions.Logistics;
using ERP.Models.Api;
using System.Web;
using System.Net.Http.Headers;

namespace ERP.Services.Api;

public class LogisticClient: Client
{
    public LogisticClient(IHttpClientFactory IHttpClientFactory, ILogger<LogisticClient> ILogger)
    {
        if (HttpClient is null)
        {
            HttpClient = IHttpClientFactory.CreateClient(nameof(Logistics));
        }
        logger = ILogger;
    }
    readonly ILogger logger;
    readonly HttpClient HttpClient;

    public async Task<ResponseResult<T>> Send<T>(RequestLogistic request) where T : class
    {
        try
        {
            T desc = default(T)!;
            HttpRequestMessage request_message;
            if (request.Method == HttpMethod.Post)
            {
                request_message = new HttpRequestMessage(HttpMethod.Post, request.Url);
                if (request.ContentType == "application/xml")
                {

                    request_message.Content = new ByteArrayContent(Encoding.UTF8.GetBytes(request.ToPayload()));
                }
                //else if(reques
                //t.ContentType == "application/x-www-form-urlencoded") {
                //    var Payload = request.ToPayload();
                //    List <KeyValuePair<string, string> > Values= new();
                //    foreach (var item in Payload.Split('&'))
                //    {
                //        var kvdata = item.Split('=');
                //        if (kvdata.Length == 2)
                //        {
                //            Values.Add(new KeyValuePair<string, string>(kvdata[0], kvdata[1]));
                //        }
                //        else {
                //            throw new ArgumentException($"Data Invalid:{kvdata}");
                //        }
                //    }
                //    request_message.Content = new FormUrlEncodedContent(Values);
                //}                
                else
                {
                    
                    request_message.Content = new StringContent(request.ToPayload(), Encoding.UTF8, request.ContentType);
                }
            }
            else
            {
                request_message = new HttpRequestMessage(HttpMethod.Get, request.Url);
            }
            if (request.headers is not null)
            {
                foreach (var v in request.headers)
                {
                    //if (v.Key.Contains("Authorization", StringComparison.CurrentCultureIgnoreCase))
                    //{
                    //    var t = v.Value.Split(' ').First();
                    //    var s = v.Value.Replace($"{v.Value.Split(' ').First()} ", "");
                    //    var w = v.Value.Split(' ').First();
                    //    request_message.Headers.Authorization = new AuthenticationHeaderValue("Basic", s);
                    //}
                    //else
                    //{
                        request_message.Headers.Add(v.Key, v.Value);
                   // }                  
                }
            }           
            string content = "";
            var response = await HttpClient.SendAsync(request_message);
            byte[] bytes = await response.Content.ReadAsByteArrayAsync();

            content = System.Text.Encoding.UTF8.GetString(bytes);
            if (response.IsSuccessStatusCode)
            {
                //{text/html; charset=GBK}                
                if (response.Content.Headers.ContentType is not null && response.Content.Headers.ContentType.ToString().Contains("GBK", StringComparison.CurrentCultureIgnoreCase))
                {
                    Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                    // 此处不应该使用UrlDecode
                    content = Encoding.GetEncoding("gbk").GetString(bytes);
                    // content = HttpUtility.UrlDecode(bytes, Encoding.GetEncoding("gbk"));
                }
                else
                {
                    content = System.Text.Encoding.UTF8.GetString(bytes);
                }

                if (request.ContentType == "application/xml")
                {
                    using (StringReader sr = new StringReader(content))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(T));
                        try
                        {
                            desc = serializer.Deserialize(sr) as T ?? default!;
                        }
                        catch (Exception e)
                        {
                            logger.LogWarning($"结果解析失败，解析类型目标类型：{typeof(T)},返回的数据为：{0}", content);
                            return new ResponseResult<T>(new ParseResultError(typeof(T).ToString()));
                        }
                    }
                }
                else  //默认json
                {
                    try
                    {
                        desc = JsonConvert.DeserializeObject<T>(content) ?? default!;
                    }
                    catch (Exception e)
                    {
                        logger.LogWarning($"结果解析失败，解析类型目标类型：{typeof(T)},返回的数据为：{0}", content);
                        return new ResponseResult<T>(new ParseResultError(typeof(T).ToString()));
                    }
                }
                if (desc is null)
                {
                    //尚未生成                                                  
                    logger.LogWarning($"Not Generated data");
                    return new ResponseResult<T>(new NotGeneratedError());
                }
                else
                {
                    return new ResponseResult<T>(desc);
                }
            }
            else
            {
                if(response.StatusCode.ToString()== "Unauthorized")
                {
                    logger.LogWarning($"http request fail,httpStatusCode：{response.StatusCode.ToString()}，errorMessage：{0}", content);
                    return new ResponseResult<T>(new AccountError());
                }
                else
                {
                    logger.LogWarning($"http request fail,httpStatusCode：{response.StatusCode.ToString()}，errorMessage：{0}", content);
                    return new ResponseResult<T>(new NetworkRequestError());
                }                
            }
        }
        catch (Exception e)
        {
            string s = e.Message;
            logger.LogWarning($"http request fail，errorMessage：{0}", e.Message);
            return new ResponseResult<T>(new NetworkRequestError());
        }
    }

    public async Task<ResponseResult<HttpResponseMessage>> SendPrint(RequestLogistic request)
    {
        HttpRequestMessage request_message;
        if (request.Method == HttpMethod.Post)
        {
            request_message = new HttpRequestMessage(HttpMethod.Post, request.Url);
            if (request.ContentType == "application/xml")
            {
                request_message.Content = new ByteArrayContent(Encoding.UTF8.GetBytes(request.ToPayload()));
            }
            else
            {
                request_message.Content = new StringContent(request.ToPayload(), Encoding.UTF8, request.ContentType);
            }
        }
        else
        {
            request_message = new HttpRequestMessage(HttpMethod.Get, request.Url);
        }
        if (request.headers is not null)
        {
            foreach (var v in request.headers)
            {
                request_message.Headers.Add(v.Key, v.Value);
            }
        }
        var response = await HttpClient.SendAsync(request_message);       
        string Code = response.StatusCode.ToString();
        byte[] content = await response.Content.ReadAsByteArrayAsync();
        if (response.IsSuccessStatusCode)
        {
            return new ResponseResult<HttpResponseMessage>(response);
        }
        else
        {
            string msg = await response.Content.ReadAsStringAsync();
            logger.LogWarning($"http request fail,httpStatusCode：{response.StatusCode.ToString()}，errorMessage：{0}");
            return new ResponseResult<HttpResponseMessage>(new NetworkRequestError(msg));
        }            
    }
}
