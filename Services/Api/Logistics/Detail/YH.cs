namespace ERP.Services.Api.Logistics.Detail;

using Enums;
using Models.Api.Logistics;
using Models.Api.Logistics.YH;
using Exceptions.Logistics;
using Enums.Logistics;

public class YH : Base<Parameter>
{
    public override bool PrintEnabled { get; set; } = false;
    public string logName = "";


    public YH(ILogger<YH> logger, LogisticClient request, IServiceProvider serviceProvider)
        : base(Platforms.YH, logger, request, serviceProvider)
    {
        logName = base.Platform.GetDescription();
    }    

    public override async Task<CommonResultList> GetShipTypes(ShipTypeParameter shipTypeParam)
    {
        List<CommonResult> shipTypes = new List<CommonResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(shipTypeParam.Parameter);
        ShipTypesRequest request = new ShipTypesRequest(parameter);
        var myResult = await Request.Send<Response<List<ShipType>>>(request);
        if (myResult.Success && myResult.Result.IsNotNull())
        {
            var response = myResult.Result;
            if(response.state==200 && response.success)
            {
                response.data.ForEach(r => shipTypes.Add(new CommonResult(r.warehouseCode, r.warehouseName)));
                return new CommonResultList(shipTypes);
            }
            else
            {
                logger.LogError(response.message);
                return new CommonResultList(new UnKnownError(response.message));
            }           
        }
        else
        {
            return new CommonResultList(myResult.exception);
        }
    }

    public override async Task<ReportResultList> Send(SendParameter sendParameter)
    {
        Parameter parameter = Helpers.GetParamType<Parameter>(sendParameter.Parameter);
        List<ReportResult> results = new List<ReportResult>();

        List<AddOrderRequest> datas = new List<AddOrderRequest>();
        foreach (var orderinfo in sendParameter.OrderInfos)
        {            
            ExtParameter ext = Helpers.GetParamType<ExtParameter>(orderinfo.ext);
            List<ErrorMember> errorList = ext.IsValid<ExtParameter>();
            AddOrderRequest data = new AddOrderRequest(parameter, sendParameter.SenderInfo,orderinfo, ext);                         
            errorList.AddRange(data.IsValid<AddOrderRequest>());  //检查接口参数
            if (errorList.Count <= 0)
            {
                datas.Add(data);
            }
            else
            {
                logger.LogWarning(ParameterErrorType.ParameterFormat.GetDescription(), toMsg(errorList));
                results.Add(new ReportResult(orderinfo.id, orderinfo.OrderNo, new ParameterError(ParameterErrorType.ParameterFormat, toMsg(errorList))));               
            }
        }
        if (datas.Count > 0)
        {            
            foreach(var data in datas)
            {
                var sendResult = await Request.Send<AddOrderResponse>(data);
                string res = toJson(sendResult);
                if (sendResult.Success && sendResult.Result.IsNotNull())
                {
                    var response = sendResult.Result;                                      
                    if (response.state==200 && response.success && response.data.IsNotNull())
                    {                       
                            results.Add(new ReportResult(data.Id,response.data.ordeSn,"",
                              "", "", 0,
                               data.ext, res));                                                                      
                    }
                    else
                    {
                        logger.LogError(response!.message);                       
                        results.Add(new ReportResult(data.Id, data.amazonOrderId, new UnKnownError($"{response.message}"), res));
                    }
                }
                else
                {                   
                    results.Add(new ReportResult(data.Id, data.amazonOrderId, sendResult.exception, res));
                }
            }           
        }       
        return new ReportResultList(results);
    }
  

   
    public override async Task<TrackResultList> Track(TrackParameter trackParameter)
    {
        List<TrackResult> trackResults = new List<TrackResult>();
        Parameter parameter = parameter = Helpers.GetParamType<Parameter>(trackParameter.Parameter);
        if (trackParameter.TrackOrderInfoLst is not null)
        {
            foreach (var trackInfo in trackParameter.TrackOrderInfoLst)
            {
                List<TrackInfo> trackInfos = new List<TrackInfo>();
                TrackRequest request = new TrackRequest(parameter, trackInfo.CustomNumber!);
                var myResult = await Request.Send<Response<cl_data>>(request);
                if (myResult.Success)
                {
                    if (myResult.Result.IsNotNull())
                    {
                        var response = myResult.Result;
                        string TrackInfo = "";
                        string refNo = "";//参考号
                        string trackNumber = "", express = "";//面单号
                        string rawState = "";
                        double fee = 0;
                        if (response.state == 200 && response.success)
                        {
                            if (response.data is not null && response.data.logisticsList.Count > 0)
                            {
                                foreach (var v in response.data.logisticsList)
                                {
                                    trackNumber = v.trackingNo;
                                    express = v.dsConsignmentNo;
                                    refNo = v.refNo;
                                    rawState = v.status;
                                    trackInfos.Add(new TrackInfo("", v.status, "", v.ToString()));
                                    string Message ="物流状态："+ v.status+","+v.ToString();
                                    TrackInfo += Message + "\r\n";
                                    fee =Convert.ToDouble(v.actualFreightFee);
                                }                           
                            }
                            else
                            {
                                //尚未生成追踪信息
                                logger.LogWarning($"{trackParameter.LogName.GetDescription()}-{trackInfo.CustomNumber}");
                                trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new NotGeneratedTrackError(), toJson(myResult.Result)));
                            }

                            var trackPrice = new TrackPrice(trackInfo.waybillId, trackInfo.CustomNumber, trackInfo.LogictisNumber, 
                                trackNumber, "CNY", fee, "", "", "");


                            trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, express, trackNumber,
                      TrackInfo, rawState, States.TRANSITING, trackPrice, trackInfos, toJson(response)));

                        }
                        else
                        {
                            string msg = response.message;
                            logger.LogError($"{trackParameter.LogName.GetDescription()}:Track-{response.message}");
                            trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new UnKnownError(response.message), toJson(response)));
                        }
                    }
                    else
                    {
                        //尚未生成追踪信息
                        logger.LogWarning($"{trackParameter.LogName.GetDescription()}-{trackInfo.CustomNumber}");
                        trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new NotGeneratedTrackError(), toJson(myResult.Result)));
                    }
                }
                else
                {
                    logger.LogError("track error" + myResult.exception.Message);
                    trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, myResult.exception, toJson(myResult.exception)));
                }
            }
        }
        return new TrackResultList(trackResults);
    }
    
}
