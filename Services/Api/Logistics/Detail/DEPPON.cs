namespace ERP.Services.Api.Logistics.Detail;
using DEPPONReference;
using Enums;
using Models.Api.Logistics;
using Models.Api.Logistics.DEPPON;
using Services.Api.Logistics;
using Newtonsoft.Json;
using OrderService = DB.Orders.Order;
using Enums.Logistics;

using Exceptions.Logistics;
using System.Net;

public class DEPPON : Base<Parameter>
{
    public string logName = "";
    public DEPPON(ILogger<DEPPON> logger, LogisticClient request, IServiceProvider serviceProvider) :
        base(Platforms.DEPPON, logger, request, serviceProvider)
    {
        logName = base.Platform.GetDescription();
    }

    public override async Task<CommonResultList> GetShipTypes(ShipTypeParameter shipTypeParam)
    {
        Parameter parameter = Helpers.GetParamType<Parameter>(shipTypeParam.Parameter);
        if (shipTypeParam.CountryCode.IsNull())
        {
            logger.LogWarning($"{shipTypeParam.LogName.GetDescription()} CountryCode is null");
            return new CommonResultList(new ParameterError(ParameterErrorType.CountryCodeNull));
        }
        ShipTypeRequest Request = new ShipTypeRequest(parameter, shipTypeParam?.CountryCode??string.Empty);
        var tuple = await WSDLRequest(Request.ToPayload(), parameter, Request.Method);
        if (tuple.Item1.IsNotNull())
        {
            ShipTypeResponse response = JsonConvert.DeserializeObject<ShipTypeResponse>(tuple.Item1)??default!;
            List<CommonResult> shipTypes = new List<CommonResult>();
            if (response is not null)
            {
                if (response.ask.Contains("success", StringComparison.CurrentCultureIgnoreCase))
                {
                    response.Data.ForEach(r => shipTypes.Add(new CommonResult(r.code, r.cn_name)));
                    return new CommonResultList(shipTypes);
                }
                else
                {
                    string msg = "";
                    if (response.Error.IsNotNull() && response.Error.Count > 0)
                    {
                        foreach (var v in response.Error)
                        {
                            msg += v.errCode + ":" + v.errMessage + ";";
                        }
                    }
                    logger.LogError($"{shipTypeParam?.LogName.GetDescription()},{msg}");
                    return new CommonResultList(new UnKnownError(msg));
                }
            }
            else
            {
                logger.LogWarning($"Parse Result Error:{typeof(ShipTypeResponse).ToString()},{tuple.Item1}");
                return new CommonResultList(new ParseResultError(typeof(ShipTypeResponse).ToString()));
            }
        }
        else
        {
            return new CommonResultList(tuple.Item2);
        }
    }

    public override async Task<ReportResultList> Send(SendParameter sendParameter)
    {
        List<ReportResult> results = new List<ReportResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(sendParameter.Parameter);
        SenderInfo senderInfo = sendParameter.SenderInfo;
        List<OrderInfo> orderLists = sendParameter.OrderInfos;
        List<AddOrderRequest> datas = new List<AddOrderRequest>();
        foreach (var orderinfo in orderLists)
        {
            ExtParameter Ext = Helpers.GetParamType<ExtParameter>(orderinfo.ext);
            List<ErrorMember> errMsg = Ext.IsValid<ExtParameter>(); //检查窗体参数格式
            AddOrderRequest request = new AddOrderRequest(parameter, senderInfo, orderinfo, Ext);
            errMsg.AddRange(request.IsValid<AddOrderRequest>());
            if (errMsg.Count <= 0)
            {
                datas.Add(request);
            }
            else
            {
                logger.LogWarning(ParameterErrorType.ParameterFormat.GetDescription(), toMsg(errMsg));
                results.Add(new ReportResult(orderinfo.id, request.orderNo, new ParameterError(ParameterErrorType.ParameterFormat, toMsg(errMsg))));
                continue;
            }
        }
        if (datas.Count > 0)
        {
            foreach (var request in datas)
            {
                var tuple = await WSDLRequest(JsonConvert.SerializeObject(request), parameter, "CreateOrder");
                if (tuple.Item1.IsNotNull())
                {
                    string res = toJson(tuple.Item1);
                    AddOrderResponse response = JsonConvert.DeserializeObject<AddOrderResponse>(tuple.Item1)??default!;
                    if (response.IsNotNull())
                    {
                        if (response!.ask.Contains("success", StringComparison.CurrentCultureIgnoreCase))
                        {
                            results.Add(new ReportResult(request.Id, response.reference_no, response.order_code, response.agent_number, "", 0,
                             orderLists.Where(x => x.id == request.Id).Select(s => s.ext).FirstOrDefault() ?? "", res));
                        }
                        else
                        {
                            if (response.Error.errMessage.Contains("参考单号已存在"))
                            {
                                logger.LogWarning($"{sendParameter.LogName.GetDescription()},Orderid:{request.Id} Order Already Exists");
                                results.Add(new ReportResult(request.Id, request.orderNo, new OrderAlreadyExists(), res));
                            }
                            else
                            {
                                logger.LogWarning($"{sendParameter.LogName.GetDescription()},{response.Error.errCode + "-" + response.Error.errMessage}");
                                results.Add(new ReportResult(request.Id, request.orderNo, new UnKnownError(response.Error.errCode + "-" + response.Error.errMessage), res));
                            }
                        }
                    }
                    else
                    {
                        results.Add(new ReportResult(request.Id, request.orderNo, new ParseResultError(typeof(AddOrderResponse).ToString())));
                    }
                }
                else
                {
                    results.Add(new ReportResult(request.Id, request.orderNo, tuple.Item2, toJson(tuple)));
                }
            }
        }
        return new ReportResultList(results);
    }

    public async Task<TrackPrice> GetFeeBySingle(Parameter parameter, string CustomOrderNumber, int waybillId)
    {
        GetFeeRequest request = new GetFeeRequest(parameter, CustomOrderNumber);
        var tuple = await WSDLRequest(request.ToPayload(), parameter, request.Method);
        string res = toJson(tuple);
        if (tuple.Item1.IsNotNull())
        {
            GetFeeResponse response = JsonConvert.DeserializeObject<GetFeeResponse>(tuple.Item1)??default!;
            if (response is not null)
            {
                if (response.ask.Contains("success", StringComparison.CurrentCultureIgnoreCase))
                {
                    if (response.Data.IsNotNull() && response.Data.Count > 0)
                    {
                        var d = response.Data[0];
                        var trackPrice = new TrackPrice(waybillId, d.CustomerOrderNumber, d.TrackingNumber, d.WaybillNumber, "CNY", d.TotalFee, d.SettleWeight, "", res);
                        return trackPrice;
                    }
                    else
                    {
                        if (response.Error.IsNotNull())
                        {
                            string msg = response.Error.errCode + ":" + response.Error.errMessage + ";";
                            return new TrackPrice(waybillId, CustomOrderNumber, new UnKnownError(msg), res);
                        }
                        else
                        {
                            logger.LogWarning($"NotGenerated Price Error:{CustomOrderNumber}");
                            return new TrackPrice(waybillId, CustomOrderNumber, new NotGeneratedPriceError(), res);
                        }
                    }
                }
                else
                {
                    logger.LogWarning($"UnKnown Error:{CustomOrderNumber}-{response.message}");
                    return new TrackPrice(waybillId, CustomOrderNumber, new UnKnownError(response.message), res);
                }
            }
            else
            {
                logger.LogWarning($"Parse Result Error:{typeof(ShipTypeResponse).ToString()},{tuple.Item1}");
                return new TrackPrice(waybillId, CustomOrderNumber, new ParseResultError(typeof(ShipTypeResponse).ToString()), res);
            }
        }
        else
        {
            return new TrackPrice(waybillId, CustomOrderNumber, tuple.Item2, res);
        }
    }


    public override async Task<WaybillPrice> GetFee(WaybillInfos wayBillParameter)
    {
        Parameter parameter = Helpers.GetParamType<Parameter>(wayBillParameter.Parameter);
        var price = await GetFeeBySingle(parameter, wayBillParameter.CustomNumber, wayBillParameter.waybillId);
        return new WaybillPrice(price);
    }

    public override async Task<TrackResultList> Track(TrackParameter trackParameter)
    {
        List<TrackResult> trackResults = new List<TrackResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(trackParameter.Parameter);
        if (trackParameter.TrackOrderInfoLst is not null&& trackParameter.TrackOrderInfoLst.Count>0)
        {
            foreach (var trackInfo in trackParameter.TrackOrderInfoLst)
            {
                List<TrackInfo> trackInfos = new List<TrackInfo>();
                TrackPrice trackPrice = new TrackPrice();
                TrackRequest request = new TrackRequest(parameter, trackInfo?.LogictisNumber ?? string.Empty);
                var tuple = await WSDLRequest(request.ToPayload(), parameter, request.Method);
                string res = toJson(tuple);
                
                if (tuple.Item1.IsNotNull())
                {
                    TrackResponse Response = JsonConvert.DeserializeObject<TrackResponse>(tuple.Item1)??default!;
                    if (Response is not null)
                    {
                        if (Response.ask.Contains("success", StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (Response.Data.IsNotNull() && Response.Data.Count > 0)
                            {
                                var d = Response.Data[0];
                                string TrackInfo = "";
                                //NO(已预报) ND(已发货)
                                string Status = Response.Data[0].Status;
                                States states = States.OTHER;
                                if (Status == "NO")
                                {
                                    states = States.COMMITTED;
                                }
                                else if (Status == "ND")
                                {
                                    states = States.TRANSITING;
                                }
                                foreach (var v in d.Detail)
                                {
                                    trackInfos.Add(new TrackInfo(v.Occur_date, states.GetDescription(), "", v.Comment));
                                    string Message = $"Time: {v.Occur_date}|Location: ''|Track Description: {v.Comment}";
                                    TrackInfo += Message + "\r\n";
                                }

                                //if (trackInfo?.Price == 0)
                                //{
                                    trackPrice = await GetFeeBySingle(parameter, trackInfo.CustomNumber, trackInfo.waybillId);
                                //}
                                trackResults.Add(new TrackResult(trackInfo!.waybillId, trackInfo.CustomNumber, trackInfo.LogictisNumber, trackInfo.TrackNumber, TrackInfo, d.Status, states, trackPrice, trackInfos, res));
                            }
                            else
                            {
                                //尚未生成物流信息
                                logger.LogWarning($"NotGenerated Track Error:{trackInfo?.CustomNumber}");
                                trackResults.Add(new TrackResult(trackInfo!.waybillId, trackInfo.CustomNumber, new NotGeneratedTrackError(), res));
                            }
                        }
                        else
                        {
                            if (Response.Error.IsNotNull() && Response.Error.Count > 0)
                            {
                                string msg = "";
                                foreach (var e in Response.Error)
                                {
                                    msg += $"{Response.Error[0].errCode}:{Response.Error[0].errMessage}";
                                }
                                logger.LogError($"{trackInfo?.CustomNumber}-{msg}");
                                trackResults.Add(new TrackResult(trackInfo!.waybillId, trackInfo.CustomNumber, new UnKnownError($"{msg}"), res));
                            }
                            else
                            {
                                logger.LogError($"UnKnown Error:{trackInfo?.CustomNumber}");
                                trackResults.Add(new TrackResult(trackInfo!.waybillId, trackInfo.CustomNumber, new UnKnownError($"logistic error"), res));
                            }
                        }
                    }
                    else
                    {
                        logger.LogWarning($"Parse Result Error:{trackInfo!.CustomNumber},{typeof(TrackResponse).ToString()},{tuple.Item1}");
                        trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new ParseResultError(typeof(TrackResponse).ToString()), res));
                    }
                }
                else
                {
                    trackResults.Add(new TrackResult(trackInfo!.waybillId, trackInfo.CustomNumber, tuple.Item2, res));
                }
            }
        }
        return new TrackResultList(trackResults);
    }

    public override bool ContentType { get; set; } = true;
    public override bool PaperType { get; set; } = true;

    public override bool IsUrl { get; set; } = true;
    //批量
    public override async Task<printResultList> Print(MultiPrint multiPrint)
    {
        List<printResult> printResults = new List<printResult>();
        int ContentType = 0, PaperType = 0;
        Parameter parameter = Helpers.GetParamType<Parameter>(multiPrint.Parameter);
        if (multiPrint.PaperType is not null && Enum.IsDefined(typeof(PaperType), multiPrint.PaperType) && Enum.TryParse<PaperType>(multiPrint.PaperType, out var labelPaperType))
        {
            PaperType = (int)labelPaperType;
        }
        else
        {
            logger.LogWarning($"{multiPrint.LogName.GetDescription()}-{multiPrint.PaperType} mismatched error ");
            return new printResultList(new ParameterError(PrintSettingEnum.PaperType, multiPrint.PaperType?? "Empty 数据"));
        }
        if (multiPrint.ContentType is not null && Enum.IsDefined(typeof(ContentType), multiPrint.ContentType) && Enum.TryParse<ContentType>(multiPrint.ContentType, out var labelContentType))
        {
            ContentType = (int)labelContentType;
        }
        else
        {
            logger.LogWarning($"{multiPrint.LogName.GetDescription()}-{multiPrint.ContentType} mismatched error ");
            return new printResultList(new ParameterError(PrintSettingEnum.ContentType, multiPrint.ContentType ?? "Empty 数据"));
        }
        var express = multiPrint.printParams?.Select(s => s.Express!).ToList()??Array.Empty<string>().ToList();
        PrintUrlRequest Request = new PrintUrlRequest(parameter, express, PaperType, ContentType);
        var tuple = await WSDLRequest(Request.ToPayload(), parameter, Request.Method);
        string res = toJson(tuple);
        if (!tuple.Item1.IsNullOrWhiteSpace())
        {
            PrintUrlResponse? Response = JsonConvert.DeserializeObject<PrintUrlResponse>(tuple.Item1);
            if (Response is not null)
            {
                if (Response.state == "1" || Response.state == "2")
                {
                    foreach (var d in Response.result)
                    {
                        if (d.printInfo.Where(x => x.state == "1").Count() > 0) //错误的记录
                        {
                            string msg = "";
                            d.printInfo.Where(x => x.state == "1").ForEach(s => msg += s.errCode + ":" + s.msg + ";");
                            var customList = d.printInfo.Where(x => x.state == "1").Select(x => x.code).ToList();
                            logger.LogWarning($"{msg}", String.Join(',', customList));
                            printResults.Add(new printResult(customList, new UnKnownError(msg), res));
                        }
                        printResults.Add(new printResult(d.printInfo.Where(x => x.state == "0").Select(s => s.code).ToList(), d.url, res));
                    }
                    return new printResultList(printResults);
                }
                else
                {
                    string msg = "";
                    Response.Error.ForEach(s => msg += s.errCode + ":" + s.errMessage + ";");
                    logger.LogWarning($"{msg}");
                    return new printResultList(new UnKnownError(msg), res);
                }
            }
            else
            {
                //尚未生成
                string customNumbers = string.Join(',', multiPrint.printParams?.Select(s => s.CustomNumber).ToList()??new());
                logger.LogWarning($"{customNumbers} Not Generated Label Error ");
                return new printResultList(new NotGeneratedLabelError(), res);
            }
        }
        else
        {
            return new printResultList(tuple.Item2, res);
        }
    }

    public override async Task<PrintParamResult> GetPrint(FileTypeParameter fileTypeParameter)
    {
        List<CommonResult> paperTyperesults = new List<CommonResult>();
        foreach (PaperType paperType in Enum.GetValues(typeof(PaperType)))
        {
            paperTyperesults.Add(new CommonResult(paperType.ToString(), paperType.GetDescription()));
        }
        List<CommonResult> ContentTyperesults = new List<CommonResult>();
        foreach (ContentType contentType in Enum.GetValues(typeof(ContentType)))
        {
            ContentTyperesults.Add(new CommonResult(contentType.ToString(), contentType.GetDescription()));
        }
        return await Task.FromResult(new PrintParamResult(ContentType, FileType, PaperType, ContentTyperesults, new(), paperTyperesults));
    }

    public async Task<Tuple<string, NetworkRequestError>> WSDLRequest(string Json, Parameter parameter, string Method)
    {
        callServiceRequestBody Body = new callServiceRequestBody(Json, parameter.AppToken, parameter.AppKey, "", "", Method);
        callServiceRequest ServiceRequest = new callServiceRequest(Body);
        Ec Client = new EcClient();
        try
        {
            callServiceResponse ServiceResponse = await Client.callServiceAsync(ServiceRequest);
            return Tuple.Create(ServiceResponse.Body.response, new NetworkRequestError(""));
        }
        catch (Exception e)
        {
            logger.LogError($"json:{Json},method:{Method},result:{e.Message}");
            return Tuple.Create("", new NetworkRequestError($"json:{Json},method:{Method},result:{e.Message}"));
        }
    }
}
