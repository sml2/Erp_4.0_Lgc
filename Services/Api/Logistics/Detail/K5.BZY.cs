﻿namespace ERP.Services.Api.Logistics.Detail;
using Enums;
using AmazonService = DB.Orders.Order;

public class BZY : K5
{
    public BZY(ILogger<BZY> logger, LogisticClient request, IServiceProvider serviceProvider) : base(Platforms.K5_BZY, logger, request, serviceProvider)
    {
    }
}
