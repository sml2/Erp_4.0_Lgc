namespace ERP.Services.Api.Logistics.Detail;
using AmazonService = DB.Orders.Order;
using Models.Api.Logistics;
using Models.Api.Logistics.Y7F;
using static Models.Api.Logistics.Y7F.AddOrderRequest;
using Enums;
using Exceptions.Logistics;

public class Y7F : Base<Parameter>
{
    protected string logName = "";
    public Y7F(ILogger<Y7F> logger, LogisticClient request, IServiceProvider serviceProvider) : base(Platforms.Y7F, logger, request, serviceProvider)
    {
        logName = base.Platform.GetDescription();
    }

    public override async Task<CommonResultList> GetShipTypes(ShipTypeParameter shipTypeParam)
    {
        var logName = shipTypeParam.LogName.GetDescription();
        List<CommonResult> shipTypes = new List<CommonResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(shipTypeParam.Parameter);
        ShipTypeRequest request = new ShipTypeRequest(parameter);
        var response = await Request.Send<ShipTypesResponse>(request);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            if (myResult.flag && myResult.productInfos.IsNotNull() && myResult.productInfos.Count > 0)
            {
                myResult.productInfos.ForEach(r => shipTypes.Add(new CommonResult(r.productCode, r.productName)));
                return new CommonResultList(shipTypes);
            }
            else
            {
                logger.LogError($"{logName}-GetShipTypes-{myResult.msg}");
                return new CommonResultList(new UnKnownError($"{myResult.msg}"));
            }
        }
        else
        {
            return new CommonResultList(response.exception);
        }
    }

    public override async Task<ReportResultList> Send(SendParameter sendParameter)
    {
        List<AddOrderRequest> requests = new List<AddOrderRequest>();
        List<ReportResult> results = new List<ReportResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(sendParameter.Parameter);
        SenderInfo senderInfo = sendParameter.SenderInfo;
        List<OrderInfo> orderLists = sendParameter.OrderInfos;
        AddOrderRequest request = new AddOrderRequest(parameter);
        List<Cls_apiOrders> apiOrders = new List<Cls_apiOrders>();//;
        foreach (var orderinfo in orderLists)
        {
            ExtParameter Ext = Helpers.GetParamType<ExtParameter>(orderinfo.ext);
            Cls_apiOrders data = new Cls_apiOrders(senderInfo, orderinfo, Ext);
            List<ErrorMember> errMsg = data.IsValid<Cls_apiOrders>();
            errMsg.AddRange(Ext.IsValid<ExtParameter>());
            if (errMsg.Count <= 0)
            {
                apiOrders.Add(data);
            }
            else
            {
                logger.LogWarning(ParameterErrorType.ParameterFormat.GetDescription(), toMsg(errMsg));
                results.Add(new ReportResult(orderinfo.id, orderinfo.OrderNo, new ParameterError(ParameterErrorType.ParameterFormat, toMsg(errMsg))));
                continue;
            }
        }
        if (apiOrders.Count > 0)
        {
            request.apiOrders = apiOrders;
            var sendResult = await Request.Send<AddOrderResponse>(request);
            string res = toJson(sendResult);
            if (sendResult.Success && sendResult.Result.IsNotNull())
            {
                var myResult = sendResult.Result;
                if (myResult.flag && myResult.fail <= 0)
                {
                    foreach (var order in myResult.successOrders)
                    {                        
                        var referenceNo = Helpers.GetOldOrderNo(order.referenceNo);
                        var o = orderLists.Where(x => x.OrderNo == referenceNo).FirstOrDefault();
                        results.Add(new ReportResult(o?.id > 0 ? o.id : -1, order.referenceNo, order.insideNumber,
                            /*order.deliveryNumber == null ? "" :*/ order.deliveryNumber.ToString()??"", "", 0,
                            o?.ext??string.Empty, res));
                    }
                    foreach (var order in myResult.failOrders)
                    {
                        var referenceNo = Helpers.GetOldOrderNo(order.referenceNo);
                        var o = orderLists.Where(x => x.OrderNo == referenceNo).FirstOrDefault();
                        results.Add(new ReportResult(o?.id>0?o.id:-1, order.referenceNo, new UnKnownError(order.errorMessage), res));
                    }
                    return new ReportResultList(results);
                }
                else
                {
                    string msg = "";
                    myResult.failOrders.ForEach(v => msg += $"{v.referenceNo} error:{v.errorMessage};");
                    logger.LogError(msg);
                    return new ReportResultList(new UnKnownError(msg), res);
                }
            }
            else
            {
                return new ReportResultList(sendResult.exception, res);
            }
        }
        else
        {
            return new ReportResultList(results);
        }
    }


    private async Task<TrackPrice> GetFeeInfo(Parameter parameter, string prodcutId, string logName, string customNumber, string logNumber, string trackNumber, int waybillId)
    {
        GetFeeRequest request = new GetFeeRequest(parameter, prodcutId);
        var response = await Request.Send<GetFeeResponse>(request);
        string res = toJson(response);
        if (response.Success && response.Result.IsNotNull()) //请求成功
        {
            var myResult = response.Result;
            if (myResult.flag)
            {
                if (myResult.obj is not null)
                {
                    var price = new TrackPrice(waybillId, customNumber, logNumber, trackNumber, "CNY", myResult.obj.totalPrice, myResult.obj.totalWeight.ToString(), "", res);
                    return price;
                }
                else
                {
                    //price 尚未生成
                    logger.LogWarning($"{logName} price Not Generated ", customNumber);
                    return new TrackPrice(waybillId, customNumber, new NotGeneratedPriceError(), res);
                }
            }
            else
            {
                logger.LogWarning($"{logName} getprice  Error:{myResult.msg}", customNumber);
                return new TrackPrice(waybillId, customNumber, new UnKnownError(/*myResult.msg is null ? "" :*/ myResult.msg.ToString()??""), res);
            }
        }
        else
        {
            return new TrackPrice(waybillId, customNumber, response.exception, res);
        }
    }




    public override async Task<WaybillPrice> GetFee(WaybillInfos wayBillParameter)
    {
        var logName = wayBillParameter.LogName.GetDescription();
        Parameter parameter = Helpers.GetParamType<Parameter>(wayBillParameter.Parameter);

        var orderInfo = await GetFeeByOrderNum(parameter, wayBillParameter.CustomNumber, wayBillParameter.LogictisNumber ?? "暂无物流系统运单号", logName);
        if (!orderInfo.Item1.IsNullOrWhiteSpace())
        {        
            var price = await GetFeeInfo(parameter, orderInfo.Item1, logName,
            wayBillParameter.CustomNumber, wayBillParameter.LogictisNumber, wayBillParameter.TrackNumber, wayBillParameter.waybillId);
            return new WaybillPrice(price);
        }
        return new WaybillPrice(new NotGeneratedFeeError());
    }



    private async Task<Tuple<string?, Exception, string>> GetFeeByOrderNum(Parameter parameter, string customNumber, string LogictisNumber, string logName)
    {
        GetOrderDetailsRequest request = new GetOrderDetailsRequest(parameter, LogictisNumber);
        var response = await Request.Send<GetOrderDetailsResponse>(request);
        string res = toJson(response);
        if (response.Success && response.Result.IsNotNull()) //请求成功
        {
            var myResult = response.Result;
            if (myResult.flag)
            {
                if (myResult.orderDetails is not null && myResult.orderDetails.Count > 0)
                {
                    var order = myResult.orderDetails[0];
                    return new Tuple<string?, Exception, string>(order.orderId, new UnKnownError(""), res);
                }
                else
                {
                    //尚未生成                                 
                    logger.LogWarning($"{logName} orderid Not Generated  Error,CustomNumber:{customNumber}");
                    return new Tuple<string?, Exception, string>(null, new NotGeneratedPriceError(), res);
                }
            }
            else
            {
                string msg = $"{myResult.msg};";
                logger.LogWarning($"{logName} orderid error,CustomNumber:{customNumber}-{msg}");
                return new Tuple<string?, Exception, string>(null, new UnKnownError($"{msg}"), res);
            }
        }
        else
        {
            return new Tuple<string?, Exception, string>(null, response.exception, res);
        }
    }


    public override async Task<TrackResultList> Track(TrackParameter trackParameter)
    {
        var logName = trackParameter.LogName.GetDescription();
        List<TrackResult> trackResults = new List<TrackResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(trackParameter.Parameter);
        foreach (var trackInfo in trackParameter.TrackOrderInfoLst)
        {
            List<TrackInfo> trackInfos = new List<TrackInfo>();
            TrackPrice priceInfo = new TrackPrice();
            TrackRequest request = new TrackRequest(parameter, trackInfo.LogictisNumber??"暂无物流系统运单号");
            var response = await Request.Send<TrackResponse>(request);
            string res = toJson(response);
            if (response.Success && response.Result.IsNotNull()) //请求成功
            {
                var myResult = response.Result;  //分析结果
                if (myResult.flag)
                {
                    if (myResult.trackingInformations.IsNotNull() && myResult.trackingInformations.Count > 0)
                    {
                        var info = myResult.trackingInformations[0];
                        string TrackInfo = "";
                        var Details = info.trackingInfoDetails;
                        string rawState = info.trackingInfoDetails[0].trackingStatus;

                        string trackNumber = /*info.trackingInfoDetails[0].deliveryNumber == null ? "" :*/ info.trackingInfoDetails[0].deliveryNumber.ToString()??"";
                        trackInfo.TrackNumber = trackInfo.TrackNumber.IsNullOrWhiteSpace() ? trackNumber : trackInfo.TrackNumber;
                        Enums.Logistics.States states = GetStatus(rawState);
                        Details.Reverse();
                        foreach (var v in Details)
                        {
                            var s = v.statusDesc.IsNull() ? "" : v.statusDesc.ToString();
                            var l = v.location.IsNull() ? "" : v.location.ToString();
                            trackInfos.Add(new TrackInfo(v.createTime, $"{v.trackingStatus}-{s}", "l", v.description));
                            string Message = $"Time: {v.createTime}|status:{v.trackingStatus}-{s}|Location: {l}|Track Description: {v.description}";
                            TrackInfo += Message + "\r\n";
                        }
                        //if (trackInfo.Price.IsNull() || trackInfo.Price == 0)
                        //{
                            var orderInfo = await GetFeeByOrderNum(parameter, trackInfo.CustomNumber, trackInfo.LogictisNumber?? "暂无物流系统运单号", logName);
                            if (orderInfo.Item1.IsNullOrWhiteSpace())
                            {
                                trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, orderInfo.Item2, orderInfo.Item3));
                            }
                            else
                            {
                                //getfee
                                priceInfo = await GetFeeInfo(parameter, orderInfo.Item1??"", logName, trackInfo.CustomNumber, trackInfo.LogictisNumber?? "暂无物流系统运单号", trackInfo.TrackNumber?? "暂无物流跟踪号", trackInfo.waybillId);
                                trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, trackInfo.LogictisNumber, trackInfo.TrackNumber,
                                  TrackInfo, rawState, states, priceInfo, trackInfos, res));
                            }
                        //}
                        //else
                        //{
                        //    trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, trackInfo.LogictisNumber, trackInfo.TrackNumber,
                        //       TrackInfo, rawState, states, trackInfos, res));
                        //}
                    }
                    else
                    {
                        //尚未生成                   
                        logger.LogWarning($"{logName} getTrack Not Generated Track Error,CustomNumber:{trackInfo.CustomNumber}");
                        trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new NotGeneratedTrackError(), res));
                    }
                }
                else
                {
                    logger.LogError($"{logName}-track error-{myResult.msg}");
                    trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new UnKnownError($"{myResult.msg}"), res));
                }
            }
            else
            {
                trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, response.exception, res));
            }
        }
        return new TrackResultList(trackResults);
    }

    private Enums.Logistics.States GetStatus(string trackingStatus)
    {
        Enums.Logistics.States states = Enums.Logistics.States.TRANSITING;
        if (Enum.IsDefined(typeof(TrackStatus), trackingStatus) && Enum.TryParse<TrackStatus>(trackingStatus, out var logStates))
        {
            if (logStates == TrackStatus.NO_RESULT) return Enums.Logistics.States.OTHER; //查询不到
            if (logStates == TrackStatus.TAKE_DELIVERY) return Enums.Logistics.States.RECEIVED; //仓库收货
            if (logStates == TrackStatus.IN_TRANSIT) return Enums.Logistics.States.TRANSITING;//运输途中
            if (logStates == TrackStatus.WAIT_TO_PICKUP) return Enums.Logistics.States.WAITTOPICKUP; //到达待取
            if (logStates == TrackStatus.DELIVERY_FAILED) return Enums.Logistics.States.DELIVERYFAILED; //投递失败
            if (logStates == TrackStatus.SIGNING_SUCCESS) return Enums.Logistics.States.SINGED; //成功签收
            if (logStates == TrackStatus.POSSIBLE_ABNORMALITY) return Enums.Logistics.States.OTHER; //可能异常
            if (logStates == TrackStatus.LONG_TRANSPORT) return Enums.Logistics.States.OTHER; //运输过久
            if (logStates == TrackStatus.TAKE_OFF) return Enums.Logistics.States.TRANSITING; //飞机起飞
            if (logStates == TrackStatus.LANDING) return Enums.Logistics.States.TRANSITING; //飞机落地
            if (logStates == TrackStatus.CLEARANCE) return Enums.Logistics.States.TRANSITING; //清关中
        }
        else
        {
            Console.WriteLine("Y7F get Track State error:" + trackingStatus);
        }
        return states;
    }

    public override async Task<printResultList> Print(MultiPrint multiPrint)
    {
        try
        {
            List<printResult> printResults = new List<printResult>();
            Parameter Parameter = Helpers.GetParamType<Parameter>(multiPrint.Parameter);
            int itemCount = 15;
            int num = 0;
            var resultNum = multiPrint.printParams?.Count / itemCount;
            var remainder = multiPrint.printParams?.Count % itemCount;
            if (remainder > 0)
            {
                resultNum += 1;
            }
            for (int j = 0; j < resultNum; j++)
            {
                var prints = multiPrint.printParams!.Skip(itemCount * num).Take(itemCount * (num + 1));
                PrintUrlRequest request = new PrintUrlRequest(Parameter, prints.Select(x => x.CustomNumber).ToList());
                var response = await Request.SendPrint(request);
                string res = toJson(response);
                if (response.Success && response.IsNotNull())
                {
                    var stream = await response.Result.Content.ReadAsStreamAsync();
                    printResults.Add(new printResult(prints.Select(x => x.CustomNumber).ToList(), stream, "application/pdf", ".pdf", res));
                }
                else
                {
                    printResults.Add(new printResult(prints.Select(x => x.CustomNumber).ToList(), response.exception, res));
                }
            }
            return new printResultList(printResults);
        }
        catch (Exception e)
        {
            return new printResultList(new UnKnownError(e.Message));
        }
    }

    public override bool GetCountryEnabled { get; set; } = true;
    public override async Task<Countries> GetCounties(BaseParameter baseParameter)
    {
        Parameter parameter = Helpers.GetParamType<Parameter>(baseParameter.Parameter);
        GetCountriesRequest request = new GetCountriesRequest(parameter);
        var response = await Request.Send<GetCountriesResponse>(request);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            //目的地
            List<CommonResult> DestinationShipTypes = new List<CommonResult>();
            //出发地
            List<CommonResult> DepartureShipTypes = new List<CommonResult>();
            if (myResult.obj.IsNotNull())
            {
                if (myResult.obj.destinations.IsNotNull() && myResult.obj.destinations.Count > 0)
                {
                    foreach (var r in myResult.obj.destinations)
                    {
                        DestinationShipTypes.Add(new CommonResult(r.code, r.name));
                    }
                }
                if (myResult.obj.departures.IsNotNull() && myResult.obj.departures.Count > 0)
                {
                    foreach (var r in myResult.obj.departures)
                    {
                        DepartureShipTypes.Add(new CommonResult(r.code, r.name));
                    }
                }
                return new Countries(DestinationShipTypes, DepartureShipTypes);
            }
            else
            {
                logger.LogError($"GetCounties  error:{myResult.msg}");
                return new Countries(new UnKnownError($"Departure:{myResult.msg}"));
            }
        }
        else
        {
            logger.LogError($"GetCounties error");
            return new Countries(response.exception);
        }
    }

    public override bool EstimatePriceEnabled { get; set; } = true;
    public override async Task<EstimatePriceResult> EstimatePrice(PriceParameter priceParameter)
    {
        Parameter parameter = Helpers.GetParamType<Parameter>(priceParameter.Parameter);
        ExtParameter Ext = Helpers.GetParamType<ExtParameter>(priceParameter.ext);
        GetEstimatePriceRequest request = new GetEstimatePriceRequest(parameter, priceParameter?.DestinationsCode ?? "暂无", priceParameter?.DepartureCode ?? "暂无", Ext);
        //string url = "http://www.17feia.com/customer/v1/freightCalculation/calFreight";
        List<CommonResult> lst = new List<CommonResult>();
        var response = await Request.Send<GetEstimatePriceResponse>(request);
        string res = toJson(response);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            if (myResult.flag)
            {
                if (myResult.rows.IsNotNull() && myResult.rows.Count > 0)
                {
                    foreach (var r in myResult.rows)
                    {
                        lst.Add(new CommonResult(r.productType,$"\r\nThe cost of shipping method [{r.productType}] is as follows:\r\nbasicFee:{r.feeDetail.basicFee}\r\notherFee:{r.feeDetail.otherFee}\r\ntotalPrice{r.totalPrice}"));
                    }
                }
                return new EstimatePriceResult(lst, res);
            }
            else
            {
                logger.LogError($"EstimatePrice  error:" + myResult.msg.ToString());
                return new EstimatePriceResult(new UnKnownError(myResult.msg.ToString()??string.Empty), res);
            }
        }
        else
        {
            logger.LogError($"EstimatePrice error ");
            return new EstimatePriceResult(response.exception, res);
        }
    }
}
