namespace ERP.Services.Api.Logistics.Detail;
using Models.Api.Logistics;
using Models.Api.Logistics.KJHY;
using Enums;
using AmazonService = DB.Orders.Order;
using Exceptions.Logistics;

public class KJHYXB : Base<Parameter>
{
    protected string logName = "";
    public KJHYXB(ILogger<KJHYXB> logger, LogisticClient request, IServiceProvider serviceProvider) : base(Platforms.KJHYXB, logger, request, serviceProvider)
    {
        logName = base.Platform.GetDescription();
    }
    public override async Task<CommonResultList> GetShipTypes(ShipTypeParameter shipTypeParam)
    {
        List<CommonResult> commonResults = new List<CommonResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(shipTypeParam.Parameter);
        var flag = await CheckAccesstoken(parameter);
        if (!flag)
        {
            logger.LogWarning($"{logName} token expiration error");
            return new CommonResultList(new AccountTokenExpirationError());
        }
        if (shipTypeParam.transportType.IsNullOrWhiteSpace())
        {
            logger.LogWarning($"{shipTypeParam.LogName.GetDescription()} transportType parameter is null");
            return new CommonResultList(new ParameterError(ParameterErrorType.TransportTypeNull));
        }
        ShipTypeRequest request = new ShipTypeRequest(parameter, shipTypeParam.transportType??string.Empty);
        var response = await Request.Send<ShipTypeResponse>(request);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            if (myResult.data.IsNotNull() && myResult.data.transports.IsNotNull() && myResult.data.transports.Count > 0)
            {
                myResult.data.transports.ForEach(r => commonResults.Add(new CommonResult(r.id, r.name)));
                return new CommonResultList(commonResults);
            }
            else
            {
                logger.LogError($"{logName}-{myResult.code}-{myResult.msg}");
                return new CommonResultList(new UnKnownError($"GetShipTypes-{myResult.code}-{myResult.msg}"));
            }
        }
        else
        {
            return new CommonResultList(response.exception);
        }
    }
    public async Task<bool> CheckAccesstoken(Parameter parameter)
    {
        if (parameter.accessToken.IsNullOrEmpty() || DateTime.Now.AddMilliseconds(parameter.expires_in * -1) >= parameter.Time)
        {
            if (parameter.appid.IsNotEmpty() && parameter.appsecret.IsNotEmpty())
            {
                AccessTokenRequest request = new AccessTokenRequest(parameter);
                var response = await Request.Send<AccessTokenResponse>(request);
                if (response.Success && response.Result.IsNotNull())
                {
                    try
                    {
                        var myresult = response.Result;
                        parameter.accessToken = myresult.access_token;
                        parameter.expires_in = myresult.expires_in;
                        parameter.Time = DateTime.Now;
                        return true;
                    }
                    catch (Exception e)
                    {
                        logger.LogError($"KJHYXB CheckAccesstoken {e.Message}");
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public override async Task<ReportResultList> Send(SendParameter sendParameter)
    {
        List<AddOrderRequest> requests = new List<AddOrderRequest>();
        List<ReportResult> reportResults = new List<ReportResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(sendParameter.Parameter);
        SenderInfo senderInfo = sendParameter.SenderInfo;
        List<OrderInfo> orderLists = sendParameter.OrderInfos;
        var flag = await CheckAccesstoken(parameter);
        if (!flag)
        {
            logger.LogWarning($"{sendParameter.LogName.GetDescription()} token expiration error");
            return new ReportResultList(new AccountTokenExpirationError());
        }
        foreach (var OrderInfo in orderLists)
        {
            ExtParameter Ext = Helpers.GetParamType<ExtParameter>(OrderInfo.ext);
            AddOrderRequest request = new AddOrderRequest(parameter, senderInfo, OrderInfo, Ext);
            List<ErrorMember> errMsg = request.IsValid<AddOrderRequest>();
            errMsg.AddRange(Ext.IsValid<ExtParameter>());
            if (errMsg.Count <= 0)
            {
                requests.Add(request);
            }
            else
            {
                logger.LogWarning(ParameterErrorType.ParameterFormat.GetDescription(), toMsg(errMsg));
                reportResults.Add(new ReportResult(OrderInfo.id, request.orderNo, new ParameterError(ParameterErrorType.ParameterFormat, toMsg(errMsg))));
                continue;
            }
        }
        if (requests.Count > 0)
        {
            foreach (var request in requests)
            {
                var response = await Request.Send<AddOrderResponse>(request);
                string res = toJson(response);
                if (response.Success && response.Result.IsNotNull())
                {
                    var myResult = response.Result;
                    if (myResult.code == "200")
                    {
                        if (myResult.data.IsNotNull() && myResult.data.orders.IsNotNull() && myResult.data.orders.Count > 0)
                        {
                            foreach (var a in myResult.data.orders)
                            {
                                reportResults.Add(new ReportResult(request.Id, a.order_number, a.order_id, a.tracking_number, "", 0,
                                    orderLists.Where(x => x.id == request.Id).Select(s => s.ext).FirstOrDefault()??string.Empty, res));
                            }
                        }
                        else
                        {
                            logger.LogError($"{request.orderNo}:{myResult.msg}");
                            reportResults.Add(new ReportResult(request.Id, request.orderNo, new UnKnownError($"{myResult.msg}"), res));
                        }
                    }
                    else
                    {
                        logger.LogError($"{request.orderNo}:{myResult.msg}");
                        reportResults.Add(new ReportResult(request.Id, request.orderNo, new UnKnownError($"{myResult.msg}"), res));
                    }
                }
                else
                {
                    reportResults.Add(new ReportResult(request.Id, request.orderNo, response.exception, res));
                }
            }
        }
        return new ReportResultList(reportResults);
    }

    public override async Task<TrackResultList> Track(TrackParameter trackParameter)
    {
        var logName = trackParameter.LogName.GetDescription();
        List<TrackResult> trackResults = new List<TrackResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(trackParameter.Parameter);
        var flag = await CheckAccesstoken(parameter);
        if (!flag)
        {
            logger.LogWarning($"{trackParameter.LogName.GetDescription()} token expiration error");
            return new TrackResultList(new AccountTokenExpirationError());
        }
        foreach (var trackInfo in trackParameter.TrackOrderInfoLst)
        {
            List<TrackInfo> trackInfos = new List<TrackInfo>();
            TrackRequest request = new TrackRequest(parameter, trackInfo.CustomNumber);
            var Response = await Request.Send<TrackResponse>(request);
            string res = toJson(Response);
            if (Response.Success && Response.Result.IsNotNull())
            {
                var myResult = Response.Result;
                if (myResult.code == "200")
                {
                    if (myResult.data.IsNotNull())
                    {
                        var order = myResult.data.orders[0];
                        if (order.code == "200")
                        {
                            string TrackInfo = "";
                            foreach (var d in order.logistics_logs)
                            {
                                trackInfos.Add(new TrackInfo(d.logistics_time,"","",d.logistics_info));
                                string Message = $"[{d.logistics_time}]{d.logistics_info}";
                                TrackInfo += Message + "\r\n";
                            }
                            TrackPrice trackPrice = new TrackPrice();
                            if (trackInfo.Price == 0 && order.TotalFee > 0)
                            {
                                DateTime? datetime = null;
                                trackPrice = new TrackPrice(trackInfo.waybillId, trackInfo.CustomNumber, trackInfo.LogictisNumber, order.tracking_number, "CNY", order.TotalFee,
                               order.actual_total_weight.ToString(), "",res,datetime);
                            }
                            trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, trackInfo.LogictisNumber, order.tracking_number, TrackInfo, order.order_status.ToString(),
                                Enums.Logistics.States.TRANSITING, trackPrice, trackInfos, res));
                        }
                        else
                        {
                            logger.LogError($"{logName}-track error:{order.code}-{order.msg}");
                            trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new UnKnownError($"{order.code}-{order.msg}"), res));
                        }
                    }
                    else
                    {
                        //尚未生成                                                  
                        logger.LogWarning($"{logName} Not Generated Track Error,customerNumber:{trackInfo.CustomNumber}");
                        trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new NotGeneratedTrackError(), res));
                    }
                }
                else
                {
                    logger.LogError($"{logName}-track error:{myResult.code}-{myResult.msg}");
                    trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new UnKnownError($"{myResult.code}-{myResult.msg}"), res));
                }
            }
            else
            {
                trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, Response.exception, res));
            }
        }
        return new TrackResultList(trackResults);
    }

    /// <summary>
    /// ??? 没有参数，咋打印
    /// </summary>
    /// <param name="multiPrint"></param>
    /// <returns></returns>
    public override async Task<printResultList> Print(MultiPrint multiPrint)
    {
        List<printResult> printResults = new List<printResult>();
        var logName = multiPrint.LogName.GetDescription();
        Parameter parameter = Helpers.GetParamType<Parameter>(multiPrint.Parameter);
        var flag = await CheckAccesstoken(parameter);
        if (!flag)
        {
            logger.LogWarning($"{logName} token expiration error");
            return new printResultList(new AccountTokenExpirationError());
        }
        if (multiPrint.printParams is not null && multiPrint.printParams.Count > 0)
        {
            foreach (var printInfo in multiPrint.printParams)
            {
                PrintRequest request = new PrintRequest(parameter);
                var response = await Request.SendPrint(request);
                string res = toJson(response);
                if (response.Success && response.Result.IsNotNull())
                {
                    var stream = await response.Result.Content.ReadAsStreamAsync();
                    printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, stream, "application/pdf", ".pdf", res));
                }
                else
                {
                    printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, response.exception, res));
                }
            }
        }
        return new printResultList(printResults);
    }
}
