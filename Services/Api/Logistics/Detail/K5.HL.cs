﻿namespace ERP.Services.Api.Logistics.Detail;
using Enums;
using AmazonService = DB.Orders.Order;

public class HL : K5
{
    public HL(ILogger<HL> logger, LogisticClient request, IServiceProvider serviceProvider) : base(Platforms.K5_HL, logger, request, serviceProvider)
    {
    }
}
