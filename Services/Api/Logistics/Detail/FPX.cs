namespace ERP.Services.Api.Logistics.Detail;
using Models.Api.Logistics;
using Models.Api.Logistics.FPX;
using Enums;
using Exceptions.Logistics;
using Enums.Logistics;
using System.Net;

public class FPX : Base<Parameter>
{
    protected string logName = "";
    public FPX(ILogger<FPX> logger, LogisticClient request, IServiceProvider serviceProvider) : base(Platforms.FPX, logger, request, serviceProvider)
    {
        logName = base.Platform.GetDescription();
    }

    public override async Task<CommonResultList> GetShipTypes(ShipTypeParameter shipTypeParam)
    {
        List<CommonResult> CommonResults = new List<CommonResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(shipTypeParam.Parameter);
        ShipTypeRequest request = new ShipTypeRequest(parameter);
        var response = await Request.Send<ShipTypeResponse>(request);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            if (myResult.result == "1")
            {
                if (myResult.data is not null && myResult.data.Count > 0)
                {
                    myResult.data.ForEach(r => CommonResults.Add(new CommonResult(r.logistics_product_code, r.logistics_product_name_cn)));
                }
                return new CommonResultList(CommonResults);
            }
            else
            {
                if (myResult.errors.IsNotNull() && myResult.errors.Count() > 0)
                {
                    string mssg = "";
                    foreach (var e in myResult.errors)
                    {
                        mssg += e.error_code + "-" + e.error_msg + ";";
                    }
                    return new CommonResultList(new UnKnownError(mssg));
                }
                else
                {
                    return new CommonResultList(new UnKnownError(myResult.msg));
                }
            }
        }
        else
        {
            return new CommonResultList(response.exception);
        }
    }

    public override async Task<ReportResultList> Send(SendParameter sendParameter)
    {
        List<AddOrderRequest> datas = new List<AddOrderRequest>();
        List<ReportResult> results = new List<ReportResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(sendParameter.Parameter);
        SenderInfo senderInfo = sendParameter.SenderInfo;
        List<OrderInfo> orderLists = sendParameter.OrderInfos;
        foreach (var orderinfo in orderLists)
        {
            ExtParameter Ext = Helpers.GetParamType<ExtParameter>(orderinfo.ext);
            AddOrderRequest request = new AddOrderRequest(parameter, senderInfo, orderinfo, Ext);
            List<ErrorMember> errMsg = request.IsValid<AddOrderRequest>();
            errMsg.AddRange(Ext.IsValid<ExtParameter>());
            if (errMsg.Count <= 0)
            {
                datas.Add(request);
            }
            else
            {
                logger.LogWarning(ParameterErrorType.ParameterFormat.GetDescription(), toMsg(errMsg));
                results.Add(new ReportResult(orderinfo.id, request.orderNo, new ParameterError(ParameterErrorType.ParameterFormat, toMsg(errMsg))));
                continue;
            }
        }
        if (datas.Count > 0)
        {
            foreach (var request in datas)
            {
                var response = await Request.Send<AddOrderResponse>(request);
                string res = toJson(response);
                if (response.Success && response.Result.IsNotNull())
                {
                    var myResult = response.Result;
                    if (myResult.IsNotNull() && myResult.result == "1")
                    {
                        results.Add(new ReportResult(request.Id, myResult.data.ref_no, myResult.data.ds_consignment_no, myResult.data._4px_tracking_no, "", 0,
                            orderLists.Where(x => x.id == request.Id).Select(x => x.ext).FirstOrDefault()??string.Empty, res));
                    }
                    else
                    {
                        string msg = "";
                        if (myResult.errors.IsNotNull() && myResult.errors.Count > 0)
                        {
                            myResult.errors.ForEach(s => msg += s.error_code + ":" + s.error_msg + ";");
                        }
                        logger.LogWarning($"{sendParameter.LogName.GetDescription()},{myResult.msg + "-" + msg}");
                        results.Add(new ReportResult(request.Id, request.orderNo, new UnKnownError(myResult.msg + "-" + msg), res));
                    }
                }
                else
                {
                    results.Add(new ReportResult(request.Id, request.orderNo, response.exception, res));
                }
            }

        }
        return new ReportResultList(results);
    }

    public async Task<TrackPrice> GetFeeInfo(string CustomNum, Parameter parameter, Platforms platforms, string? logNumber, int waybillId)
    {
        GetFeeRequest request = new GetFeeRequest(parameter, CustomNum);
        var response = await Request.Send<GetFeeResponse>(request);
        string res = toJson(response);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            if (myResult.result == "1")
            {
                if (myResult.data.IsNotNull() && myResult.data?.billinglist.Count > 0)
                {
                    var a = myResult.data.billinglist[0];
                    return new TrackPrice(waybillId, CustomNum, logNumber, "", a.currency, Convert.ToDouble(a.billing_amount),
                      "", ((BillTypeEnum)Convert.ToInt32(a.billing_type)).GetDescription(), res);
                }
                else
                {
                    //尚未生成
                    logger.LogError($"{CustomNum} not Generated Price Error");
                    return new TrackPrice(waybillId, CustomNum, new NotGeneratedPriceError(), res);
                }
            }
            else
            {
                string msg = "";
                myResult.errors.ForEach(s => msg += s.error_code + ":" + s.error_msg + ";");
                logger.LogError($"{platforms.GetDescription()}-{CustomNum}-GetFee-{msg}");
                return new TrackPrice(waybillId, CustomNum, new UnKnownError($"GetFee-{msg}"), res);
            }
        }
        else
        {
            return new TrackPrice(waybillId, CustomNum, response.exception, res);
        }
    }


    public override async Task<WaybillPrice> GetFee(WaybillInfos wayBillParameter)
    {
        Parameter parameter = Helpers.GetParamType<Parameter>(wayBillParameter.Parameter);
        var price = await GetFeeInfo(wayBillParameter.CustomNumber, parameter, wayBillParameter.LogName, wayBillParameter.LogictisNumber, wayBillParameter.waybillId);
        return new WaybillPrice(price);
    }


    public override async Task<TrackResultList> Track(TrackParameter trackParameter)
    {
        List<TrackResult> trackResults = new List<TrackResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(trackParameter.Parameter);
        foreach (var trackInfo in trackParameter.TrackOrderInfoLst)
        {
            TrackPrice price = new TrackPrice();
            List<TrackInfo> trackInfos = new List<TrackInfo>();
            TrackRequest request = new TrackRequest(parameter, trackInfo.TrackNumber??string.Empty);
            var response = await Request.Send<TrackResponse>(request);
            string res = toJson(response);
            if (response.Success && response.Result.IsNotNull())
            {
                var myresult = response.Result;
                if (myresult.result != "1")
                {
                    if (myresult.data.IsNotNull())
                    {
                        string TrackInfo = "";
                        States states = States.TRANSITING;
                        string RawStatues = "";
                        List<TrackResponse.Data.TrackingListItem> TrackData = myresult.data.trackingList;
                        TrackData.Reverse();
                        foreach (var v in TrackData)
                        {
                            RawStatues = v.businessLinkCode;
                            var s = GetState(RawStatues).GetDescription();
                            trackInfos.Add(new TrackInfo(v.occurDatetime, s, v.occurLocation, v.trackingContent));
                            string Message = $"Time: {v.occurDatetime}|state:{s}|Location: {v.occurLocation}|Track Description: {v.trackingContent}";
                            TrackInfo += Message + "\r\n";                           
                        }

                        //if (trackInfo.Price == 0)
                        //{
                            price = await GetFeeInfo(trackInfo.CustomNumber, parameter, trackParameter.LogName, trackInfo.LogictisNumber,trackInfo.waybillId);
                        //}
                        trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, trackInfo.LogictisNumber, trackInfo.TrackNumber, TrackInfo, RawStatues, states, price, trackInfos, res));                      
                    }
                    else
                    {
                        //暂无track信息
                        logger.LogWarning($"{trackInfo.CustomNumber}-Not Generated Track Error");
                        trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new NotGeneratedTrackError(), res));
                    }
                }
                else
                {
                    string msg = "";
                    myresult.errors.ForEach(s => msg += s.error_code + ":" + s.error_msg + ";");
                    logger.LogError($"{trackInfo.CustomNumber}-{msg}");
                    trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new UnKnownError($"{msg}"), res));
                }
            }
            else
            {
                trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, response.exception, res));
            }
        }
        return new TrackResultList(trackResults);    
    }

    public States GetState(string Statues)
    {
        States states = States.TRANSITING;
        if (Statues == "FPX_C_SPQS")
        {
            states = States.COMMITTED;
        }
        else if (Statues == "FPX_C_SPLS" || Statues == "FPX_C_AAF" || Statues == "FPX_C_RDFF")
        {
            states = States.RECEIVED;
        }
        else if (Statues == "FPX_C_BTC" || Statues == "FPX_M_PRTS" || Statues == "FPX_D_SR" || Statues == "FPX_O_PRUC" || Statues == "FPX_O_PRRR" || Statues == "FPX_O_PRIA" || Statues == "FPX_O_PRRF" || Statues == "FPX_O_RTOC" || Statues == "FPX_O_RTHM")
        {
            states = States.RETURNING;
        }
        else if (Statues == "FPX_S_OKSC")
        {
            states = States.SINGED;
        }
        return states;
    }

    public override bool IsUrl { get; set; } = true;

    //单个
    public override async Task<printResultList> Print(MultiPrint multiPrint)
    {
        List<printResult> printResults = new List<printResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(multiPrint.Parameter);
        if (multiPrint.printParams is not null)
        foreach (var printInfo in multiPrint.printParams)
        {
            PrintLabelRequest request = new PrintLabelRequest(parameter, printInfo.CustomNumber);
            var response = await Request.Send<PrintLabelResponse>(request);
            string res = toJson(response);
            if (response.Success && response.Result.IsNotNull())
            {
                printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, response.Result.data.label_url_info.logistics_label, res));
            }
            else
            {
                printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, response.exception, res));
            }
        }
        return new printResultList(printResults);
    }


    public override bool CancleEnabled { get; set; } = true;


    public override async Task<CancelResult> CancleOrder(CancleParameter cancleParameter)
    {
        Parameter parameter = Helpers.GetParamType<Parameter>(cancleParameter.Parameter);
        InterceptRequest request = new InterceptRequest(parameter, cancleParameter.LogictisNumber);
        var response = await Request.Send<InterceptResponse>(request);
        string res = toJson(response);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            if (myResult.result =="1")
            {
                return await this.CancleOrder(cancleParameter);
            }
            else
            {
                return new CancelResult(new UnKnownError(myResult.errors[0].error_code+":"+ myResult.errors[0].error_msg), res);
            }
        }
        else
        {
            logger.LogError($"Cancle error");
            return new CancelResult(response.exception, res);
        }
    }
}
