namespace ERP.Services.Api.Logistics.Detail;
using Models.Api.Logistics;
using Models.Api.Logistics.SF;
using System.Xml.Serialization;
using Enums;
using AmazonService = DB.Orders.Order;
using Exceptions.Logistics;
using System.Net;

public class SFGJ : Base<Parameter>
{
    protected string logName = "";
    public override bool TrackEnabled { get; set; } = false;
    public SFGJ(ILogger<SFGJ> logger, LogisticClient request, IServiceProvider serviceProvider) : base(Platforms.SFGJ, logger, request, serviceProvider)
    {
        logName = base.Platform.GetDescription();
    }

    public override async Task<CommonResultList> GetShipTypes(ShipTypeParameter shipTypeParam)
    {
        List<CommonResult> lst = new List<CommonResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(shipTypeParam.Parameter);
        var AllShips = Sy.Enum.GetItems<ExpressType>();
        AllShips.ForEach((r) => lst.Add(new CommonResult(r.Number.ToString(), r.Name)));
        return await Task.FromResult(new CommonResultList(lst));
    }

    public override async Task<ReportResultList> Send(SendParameter sendParameter)
    {
        List<AddOrderRequest> requests = new List<AddOrderRequest>();
        Parameter parameter = Helpers.GetParamType<Parameter>(sendParameter.Parameter);
        SenderInfo senderInfo = sendParameter.SenderInfo;
        List<OrderInfo> orderLists = sendParameter.OrderInfos;
        List<ReportResult> results = new List<ReportResult>();
        foreach (var orderInfo in orderLists)
        {
            List<ErrorMember> errMsg = new List<ErrorMember>();
            ExtParameter Ext = Helpers.GetParamType<ExtParameter>(orderInfo.ext);
            AddOrderRequest request = new AddOrderRequest(parameter, senderInfo, orderInfo, Ext);
            errMsg = Ext.IsValid<ExtParameter>();
            errMsg.AddRange(request.IsValid<AddOrderRequest>());
            if (errMsg.Count <= 0)
            {
                requests.Add(request);
            }
            else
            {
                logger.LogWarning(ParameterErrorType.ParameterFormat.GetDescription(), toMsg(errMsg));
                results.Add(new ReportResult(orderInfo.id, orderInfo.OrderNo, new ParameterError(ParameterErrorType.ParameterFormat, toMsg(errMsg))));
                continue;
            }
        }
        if (requests.Count > 0)
        {
            foreach (var request in requests)
            {
                try
                {
                    string xml = XmlSerializer(request);
                    SFServiceReference.SfKtsServiceClient Client = new SFServiceReference.SfKtsServiceClient();
                    string html = await Client.sfKtsServiceAsync(xml, GetMD5(xml, parameter.checkword));
                    AddOrderResponse response = new AddOrderResponse();
                    using (StringReader sr = new StringReader(html))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(AddOrderResponse));
                        response = serializer.Deserialize(sr) as AddOrderResponse??new();
                        string res = toJson(response);
                        if (response.IsNotNull() && response.Body.IsNotNull())
                        {
                            var myResult = response.Body;
                            results.Add(new ReportResult(request.Id, myResult.OrderResponse.orderid, myResult.OrderResponse.mailno, "", "", 0,
                                orderLists.Where(x => x.id == request.Id).Select(s => s.ext).FirstOrDefault()??string.Empty, res));
                        }
                        else
                        {
                            string msg = request.Body.Order.platform_order_id + ":" + response?.ERROR.code + "-" + response?.ERROR.ErrorValue;
                            logger.LogError($"{request.Body.Order.platform_order_id}:{msg}");
                            results.Add(new ReportResult(request.Id, request.Body.Order.platform_order_id, new UnKnownError($"{msg}"), res));
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.LogError($"{request.Body.Order.platform_order_id}:{e.Message}");
                    results.Add(new ReportResult(request.Id, request.Body.Order.platform_order_id, new UnKnownError($"{e.Message}")));
                }
            }
        }
        return new ReportResultList(results);
    }

    public override bool IsUrl { get; set; } = true;


    public override async Task<printResultList> Print(MultiPrint multiPrint)
    {
        List<printResult> printResults = new List<printResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(multiPrint.Parameter);
        if (multiPrint.printParams is not null)
        {
            foreach (var printInfo in multiPrint.printParams)
            {
                PrintUrlRequest request = new PrintUrlRequest(parameter, printInfo.CustomNumber, printInfo.Express ?? string.Empty);
                var response = await Request.Send<PrintUrlResponse>(request);
                string res = toJson(response);
                if (response.Success && response.Result.IsNotNull())
                {
                    var myResult = response.Result;
                    if (myResult.success)
                    {
                        printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, myResult.array[0].url, res));
                    }
                    else
                    {
                        logger.LogError($"{printInfo.CustomNumber} print error");
                        printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, new UnKnownError($"print error"), res));
                    }
                }
                else
                {
                    logger.LogError($"{printInfo.CustomNumber} print error");
                    printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, response.exception, res));
                }
            }
        }
        return new printResultList(printResults);
    }

    //private async Task<List<TrackPrice>> GetFee(List<string> trackNumbers, Parameter parameter)
    //{
    //    string msg = "";
    //    List<TrackePrice> trackePrices = new List<TrackePrice>();
    //    GetFeeRequest Request = new GetFeeRequest(parameter, trackNumbers);
    //    var response = await base.Request.Send<GetFeeResponse>(Request);
    //    if (response.Success && response.Result.IsNotNull())
    //    {
    //        if (response.Result.weightAndFreightList.IsNotNull() && response.Result.weightAndFreightList.Count > 0)
    //        {
    //            response.Result.weightAndFreightList.ForEach(x => trackePrices.Add(new TrackePrice(x.tracking_no, x.Total)));
    //        }
    //        else
    //        {
    //            msg = response.Result.message;
    //            logger.LogError(msg);
    //        }
    //    }
    //    else
    //    {
    //        msg = "SF Bad Request!";
    //        logger.LogError(msg);
    //    }
    //    return trackePrices;
    //}

    public string GetMD5(string xml, string CheckWord)
    {
        return Sy.Security.MD5.Encrypt($"{xml}{CheckWord}");
    }

    public static string XmlSerializer(object obj)
    {
        try
        {
            StringWriter sw = new StringWriter();
            //创建XML命名空间
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("service", "OrderService");
            ns.Add("lang", "zh_CN");
            XmlSerializer serializer = new XmlSerializer(obj.GetType());
            serializer.Serialize(sw, obj, ns);
            sw.Close();
            return sw.ToString().Replace("xmlns:", "").Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }

}
