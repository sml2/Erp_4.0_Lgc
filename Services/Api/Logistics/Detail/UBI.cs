namespace ERP.Services.Api.Logistics.Detail;
using Models.Api.Logistics;
using Models.Api.Logistics.UBI;
using Enums;
using AmazonService = DB.Orders.Order;
using Exceptions.Logistics;
using Enums.Logistics;

public class UBI : Base<Parameter>
{
    protected string logName = "";
    public UBI(ILogger<UBI> logger, LogisticClient request, IServiceProvider serviceProvider) : base(Platforms.UBI, logger, request, serviceProvider)
    {
        logName = base.Platform.GetDescription();
    }

    public override async Task<CommonResultList> GetShipTypes(ShipTypeParameter shipTypeParam)
    {
        var logName = shipTypeParam.LogName.GetDescription();
        List<CommonResult> commonResults = new List<CommonResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(shipTypeParam.Parameter);
        ShipTypeRequest request = new ShipTypeRequest(parameter);
        var response = await Request.Send<ShipTypeResponse>(request);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            if (myResult.errors.IsNull()&& myResult.status.Contains("Success",StringComparison.CurrentCultureIgnoreCase))
            {
                foreach (var r in myResult.data)
                {
                    List<string> serviceOptions = new List<string>();
                    List<string> origins = new List<string>();
                    if (r.serviceOptions.IsNotNull()&& r.serviceOptions.Count>0)
                    {
                        serviceOptions = r.serviceOptions;
                    }
                    if (r.origins.IsNotNull() && r.origins.Count > 0)
                    {
                        origins = r.origins.Select(x => x.country).ToList();
                    }
                    commonResults.Add(new ShipTypeUBI(r.serviceCode, r.serviceName, serviceOptions, origins));
                }
                return new CommonResultList(commonResults);
            }
            else
            {
                string msg = "";
                myResult.errors.ForEach(e => msg += $"{e.code}:{e.message}");
                logger.LogError($"{logName}-GetShipTypes-{msg}");
                return new CommonResultList(new UnKnownError($"{msg}"));
            }
        }
        else
        {
            return new CommonResultList(response.exception);
        }
    }

    public override async Task<ReportResultList> Send(SendParameter sendParameter)
    {
        List<AddOrderRequest> requests = new List<AddOrderRequest>();
        List<ReportResult> results = new List<ReportResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(sendParameter.Parameter);
        UBIRequest uBIRequest = new UBIRequest(parameter);
        SenderInfo senderInfo = sendParameter.SenderInfo;
        List<OrderInfo> orderLists = sendParameter.OrderInfos;
        foreach (var orderinfo in orderLists)
        {
            ExtParameter Ext = Helpers.GetParamType<ExtParameter>(orderinfo.ext);
            AddOrderRequest data = new AddOrderRequest(parameter, senderInfo, orderinfo, Ext);
            List<ErrorMember> errMsg = data.IsValid<AddOrderRequest>();
            errMsg.AddRange(Ext.IsValid<ExtParameter>());
            if (errMsg.Count <= 0)
            {
                requests.Add(data);
            }
            else
            {
                logger.LogWarning(ParameterErrorType.ParameterFormat.GetDescription(), toMsg(errMsg));
                results.Add(new ReportResult(orderinfo.id, orderinfo.OrderNo, new ParameterError(ParameterErrorType.ParameterFormat, toMsg(errMsg))));
                continue;
            }
        }
        if (requests.Count > 0)
        {
            uBIRequest.requests = requests;
            var response = await Request.Send<AddOrderResponse>(uBIRequest);
            string res = toJson(response);
            if (response.Success && response.Result.IsNotNull())
            {
                var myResult = response.Result;
                if (myResult.data.IsNotNull() && myResult.data.Count > 0)
                {
                    foreach (var v in myResult.data)
                    {
                        var referenceNo = Helpers.GetOldOrderNo(v.referenceNo);
                        var order = orderLists.Where(x => x.OrderNo == referenceNo).FirstOrDefault();
                        if (v.status.Contains("Success", StringComparison.CurrentCultureIgnoreCase))
                        {
                            results.Add(new ReportResult(order?.id>0?order.id:-1, v.referenceNo, v.orderId,
                             v.trackingNo, "", 0,
                           order?.ext??string.Empty, res));
                        }
                        else
                        {
                            string msg = "";
                            v.errors.ForEach((r) => msg += $"{v.referenceNo}:{r.code}-{r.message};");
                            logger.LogError(msg);
                            results.Add(new ReportResult(order?.id > 0 ?order.id:-1, v.referenceNo, new UnKnownError(msg), res));
                        }
                    }
                    return new ReportResultList(results);
                }
                else
                {
                    string msg = "";
                    myResult.errors.ForEach((r) => msg += $"{r.code}:{r.message};");
                    logger.LogError(msg);
                    return new ReportResultList(new UnKnownError(msg), res);
                }
            }
            else
            {
                return new ReportResultList(response.exception, res);
            }
        }
        else
        {
            return new ReportResultList(results);
        }
    }


    public override async Task<TrackResultList> Track(TrackParameter trackParameter)
    {
        var logName = trackParameter.LogName.GetDescription();
        List<TrackResult> trackResults = new();
        Parameter parameter = Helpers.GetParamType<Parameter>(trackParameter.Parameter);
        if (trackParameter.TrackOrderInfoLst is not null)
        {
            foreach (var trackInfo in trackParameter.TrackOrderInfoLst)
            {
                List<TrackInfo> trackInfos = new List<TrackInfo>();
                if (trackInfo.TrackNumber.IsNullOrWhiteSpace())
                {
                    logger.LogWarning($"{logName} TrackNumber is NullOrWhiteSpace");
                    trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new ParameterError(ParameterErrorType.TrackNumberNull)));
                    continue;
                }
                TrackRequest request = new TrackRequest(parameter, trackInfo.TrackNumber ?? "");
                var response = await Request.Send<TrackResponse>(request);
                string res = toJson(response);
                if (response.Success && response.Result.IsNotNull())
                {
                    var myResult = response.Result;
                    if (myResult.status.Contains("Success", StringComparison.CurrentCultureIgnoreCase))
                    {
                        if (myResult.data is not null&&myResult.data.Count>0)
                        {
                            string TrackInfo = "";
                            string EventCode = "";
                            var d = myResult.data[0];
                            var s = GetStatus(d.status);
                            var details = d.events;
                            details.Reverse();
                            foreach (var v in details)
                            {
                                trackInfos.Add(new TrackInfo(v.eventTime,s.GetDescription(), v.location,""));
                                string Message = $"Time: {v.eventTime}|Location: {v.location}|Track Description:''";
                                TrackInfo += Message + "\r\n";
                                EventCode = v.eventCode;
                            }
                            var trackNum = d.events.FirstOrDefault()?.trackingNo;
                            trackInfo.TrackNumber = trackInfo.TrackNumber.IsNullOrWhiteSpace() ? trackNum : trackInfo.TrackNumber;
                            trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, trackInfo.LogictisNumber, trackInfo.TrackNumber,
                                TrackInfo, d.status,s, trackInfos, res));
                        }
                        else
                        {
                            //尚未生成                   
                            logger.LogWarning($"{logName} getTrack Not Generated Track Error,CustomNumber:{trackInfo.CustomNumber}");
                            trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new NotGeneratedTrackError(), res));
                        }
                    }
                    else
                    {
                        string msg = "";
                        myResult.errors.ForEach(x => msg += $"[{x.code}]:{x.message}");
                        logger.LogError($"{logName}-track error-{msg}");
                        trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new UnKnownError($"{msg}"), res));
                    }
                }
                else
                {
                    trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, response.exception, res));
                }
            }
        }
        return new TrackResultList(trackResults);
    }


    private Enums.Logistics.States GetStatus(string Status)
    {
        Enums.Logistics.States states = Enums.Logistics.States.TRANSITING;

        switch (Status)
        {
            case "INF": //Shipping information received
                states = Enums.Logistics.States.RECEIVED;
                break;
            case "RCV": //Received shipment 
                states = Enums.Logistics.States.TRANSITING;
                break;
            case "SCN": //Processed at origin hub
                states = Enums.Logistics.States.TRANSITING;
                break;
            case "CCD": //Customs Cleared
                states = Enums.Logistics.States.TRANSITING;
                break;
            case "HLD": //Customs held
                states = Enums.Logistics.States.OTHER;
                break;
            case "DLV": //Attempt delivery
                states = Enums.Logistics.States.TRANSITING;
                break;
            case "CRD": //Carded，and left at nearby carrier facility for pickup
                states = Enums.Logistics.States.TRANSITING;
                break;
            case "DLD": //Delivered
                states = Enums.Logistics.States.WAITTOPICKUP;
                break;
            case "RTN": //Returned
                states = Enums.Logistics.States.RETURNING;
                break;
        }
        return states;
    }


    public override bool ContentType { get; set; } = true;
    public override bool FileType { get; set; } = true;

    /// <summary>
    /// 多个单个，非批量
    /// </summary>
    /// <param name="multiPrint"></param>
    /// <returns></returns>
    public override async Task<printResultList> Print(MultiPrint multiPrint)
    {
        string contenttype = "", ext = "";
        int labelType = 0;bool packinglist = false;string labelFormat = "";
        List<printResult> printResults = new List<printResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(multiPrint.Parameter);
        if (multiPrint.ContentType is not null && Enum.IsDefined(typeof(ContentTypeEnum), multiPrint.ContentType) && Enum.TryParse<ContentTypeEnum>(multiPrint.ContentType, out var labelContentType))
        {
            switch (labelContentType)
            {
                case ContentTypeEnum.Label1:
                    labelType = 1;
                    packinglist = true;
                    break;
                case ContentTypeEnum.Label2:
                    labelType = 1;
                    packinglist = false;
                    break;
                case ContentTypeEnum.Label3:
                    labelType = 4;
                    packinglist = true;
                    break;
                case ContentTypeEnum.Label4:
                    labelType = 4;
                    packinglist = false;
                    break;
                default:
                    labelType = 1;
                    packinglist = true;
                    break;
            }
        }
        else
        {
            logger.LogWarning($"{multiPrint.LogName.GetDescription()}-{multiPrint.ContentType} mismatched error ");
            return new printResultList(new ParameterError(PrintSettingEnum.ContentType, multiPrint.ContentType?? "Empty 数据"));
        }

        if (multiPrint.FileType is not null && Enum.IsDefined(typeof(FileTypeEnum), multiPrint.FileType) && Enum.TryParse<FileTypeEnum>(multiPrint.FileType, out var labelFileType))
        {
            labelFormat = labelFileType.GetDescription();
        }
        else
        {
            logger.LogWarning($"{multiPrint.LogName.GetDescription()}-{multiPrint.FileType} mismatched error ");
            return new printResultList(new ParameterError(PrintSettingEnum.FileType, multiPrint.FileType ?? "Empty 数据"));
        }
        switch(labelFormat)
        {
            case "PDF":
                contenttype = "application/pdf";
                ext = ".pdf";
                break;
            case "JPG":
                contenttype = "image/jpeg";
                ext = ".jpg";
                break;
            default:
                contenttype = "application/pdf";
                ext = ".pdf";
                break;
        }
        if (multiPrint.printParams is not null)
        {
            foreach (var printInfo in multiPrint.printParams)
            {
                PrintLabelRequest request = new PrintLabelRequest(parameter, printInfo.Express?? "SF 暂无追踪号", labelType, packinglist, labelFormat);
                var response = await Request.Send<PrintLabelResponse>(request);
                string res = toJson(response);
                if (response.Success && response.Result.IsNotNull())
                {
                    var myResult = response.Result;
                    if (myResult.status.Contains("success", StringComparison.CurrentCultureIgnoreCase))
                    {
                        HttpResponseMessage httpResponse = new HttpResponseMessage(System.Net.HttpStatusCode.OK);
                        string labelContent = myResult.data[0].labelContent;
                        byte[] byteData = Convert.FromBase64String(labelContent);
                        using (var content = new MemoryStream(byteData))
                        {
                            printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, content, contenttype, ext, res));
                        }
                    }
                    else
                    {
                        string msg = "";
                        myResult.errors.ForEach(x => msg += x.code + ":" + x.message + ";");
                        logger.LogError($"{printInfo.CustomNumber} print error-{msg}");
                        printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, new UnKnownError($"{msg}"), res));
                    }
                }
                else
                {
                    logger.LogError($"{printInfo.CustomNumber} print error");
                    printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, response.exception, res));
                }
            }
        }
        return new printResultList(printResults);
    }


    public override async Task<PrintParamResult> GetPrint(FileTypeParameter fileTypeParameter)
    {
        List<CommonResult> FileTyperesults = new List<CommonResult>();
        foreach (FileTypeEnum fileType in Enum.GetValues(typeof(FileTypeEnum)))
        {
            FileTyperesults.Add(new CommonResult(fileType.ToString(), fileType.GetDescription()));
        }
        List<CommonResult> ContentTyperesults = new List<CommonResult>();
        foreach (ContentTypeEnum contentType in Enum.GetValues(typeof(ContentTypeEnum)))
        {
            ContentTyperesults.Add(new CommonResult(contentType.ToString(), contentType.GetDescription()));
        }
        return await Task.FromResult(new PrintParamResult(ContentType, FileType, PaperType, ContentTyperesults, FileTyperesults, new()));
    }
}
