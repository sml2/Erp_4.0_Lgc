namespace ERP.Services.Api.Logistics.Detail;
using Services.Api.Logistics;
using Models.Api.Logistics;
using Models.Api.Logistics.K5;
using static Models.Api.Logistics.K5.EstimatePriceResponse;
using Enums;
using Exceptions.Logistics;
using Enums.Logistics;
using System.Net;

public abstract class K5 : Base<Parameter>
{
    protected string logName = "";
    //public Platforms[] Supports { get; set; }
    public K5(Platforms platforms, ILogger logger, LogisticClient request, IServiceProvider serviceProvider) : base(platforms, logger, request, serviceProvider)
    {
        logName = base.Platform.GetDescription();
    }

    public override async Task<CommonResultList> GetShipTypes(ShipTypeParameter shipTypeParam)
    {
        Parameter parameter = Helpers.GetParamType<Parameter>(shipTypeParam.Parameter);
        List<CommonResult> commonResults = new List<CommonResult>();
        var Site = parameter.GetSite();
        if (Site is null)
        {
            logger.LogWarning($"{logName}:{parameter.Domain} url is error");
            return new CommonResultList(new ParameterError(ParameterErrorType.ParameterUrl, parameter.Domain));
        }
        ShipTypeRequest request = new ShipTypeRequest(parameter);
        request.Domain = Site;
        var response = await Request.Send<ShipTypesResponse>(request);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            if (myResult.statusCode.Contains("success", StringComparison.CurrentCultureIgnoreCase))
            {
                myResult.returnDatas.ForEach(r => commonResults.Add(new CommonResult(r.code, r.cnname)));
                return new CommonResultList(commonResults);
            }
            else
            {
                logger.LogError($"{logName}-{myResult.message}");
                return new CommonResultList(new UnKnownError($"GetShipTypes-{myResult.message}"));
            }
        }
        else
        {
            return new CommonResultList(response.exception);
        }
    }

    public override async Task<ReportResultList> Send(SendParameter sendParameter)
    {
        List<AddOrderRequest> requests = new List<AddOrderRequest>();
        List<ReportResult> results = new List<ReportResult>();
        SenderInfo senderInfo = sendParameter.SenderInfo;
        List<OrderInfo> orderLists = sendParameter.OrderInfos;
        Parameter parameter = Helpers.GetParamType<Parameter>(sendParameter.Parameter);
        var Site = parameter.GetSite();
        if (Site is null)
        {
            logger.LogWarning($"{logName}:{parameter.Domain} url is error");
            return new ReportResultList(new ParameterError(ParameterErrorType.ParameterUrl, parameter.Domain));
        }
        List<AddOrderRequest.OrderDatasItem> lsts = new List<AddOrderRequest.OrderDatasItem>();
        foreach (var OrderInfo in orderLists)
        {
            ExtParameter Ext = Helpers.GetParamType<ExtParameter>(OrderInfo.ext);
            var OrderData = new AddOrderRequest.OrderDatasItem(senderInfo, OrderInfo, Ext);
            List<ErrorMember> errMsg = OrderData.IsValid<AddOrderRequest.OrderDatasItem>();
            errMsg.AddRange(Ext.IsValid<ExtParameter>());
            if (errMsg.Count <= 0)
            {
                lsts.Add(OrderData);
            }
            else
            {
                logger.LogWarning(ParameterErrorType.ParameterFormat.GetDescription(), toMsg(errMsg));
                results.Add(new ReportResult(OrderInfo.id, OrderData.orderNo, new ParameterError(ParameterErrorType.ParameterFormat, toMsg(errMsg))));
                continue;
            }
        }
        if (lsts.Count > 0)
        {
            AddOrderRequest request = new AddOrderRequest(parameter, lsts);
            request.Domain = Site;
            var response = await Request.Send<AddOrderResponse>(request);
            string res = toJson(response);
            if (response.Success && response.Result.IsNotNull())
            {
                var myResult = response.Result;
                if (myResult.statusCode.Contains("success", StringComparison.CurrentCultureIgnoreCase))
                {
                    if (myResult.returnDatas.IsNotNull() && myResult.returnDatas.Count > 0)
                    {
                        foreach (var v in myResult.returnDatas)
                        {
                            //不清楚多个订单k5是如何返回的，目前系统只支持一个订单，如果将来支持多订单发送，这里逻辑需要调整                            
                            var referenceNo = Helpers.GetOldOrderNo(v.customerNumber);
                            var order = orderLists.Where(x => x.OrderNo == referenceNo).FirstOrDefault();
                            int whereOrderId = order?.id > 0 ? order.id : -1;
                            if (v.statusCode.Contains("success", StringComparison.CurrentCultureIgnoreCase))
                            {
                                results.Add(new ReportResult(whereOrderId, v.customerNumber, v.corpBillid, v.trackNumber, "", 0,
                                   order?.ext??string.Empty, res));
                            }
                            else
                            {
                                logger.LogError($"{v.customerNumber}，error：{v.message}");
                                results.Add(new ReportResult(whereOrderId, v.customerNumber, new UnKnownError($"{v.message}"), res));
                            }
                        }
                    }
                    return new ReportResultList(results);
                }
                else
                {
                    string message = myResult.message.IsNullOrWhiteSpace()?"":myResult.message+";";
                    if(myResult.returnDatas.IsNotNull() && myResult.returnDatas.Count>0)
                    {
                        myResult.returnDatas.Where(x => x.statusCode.Contains("error", StringComparison.CurrentCultureIgnoreCase)).ForEach(s => message += s.message + ";");
                    }
                    logger.LogError(message);
                    return new ReportResultList(new UnKnownError(message), res);
                }
            }
            else
            {
                return new ReportResultList(response.exception, res);
            }
        }
        else
        {
            return new ReportResultList(results);
        }
    }


    public override async Task<TrackResultList> Track(TrackParameter trackParameter)
    {
        var logName = trackParameter.LogName.GetDescription();
        List<TrackResult> trackResults = new List<TrackResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(trackParameter.Parameter);
        var Site = parameter.GetSite();
        if (Site is null)
        {
            logger.LogWarning($"{logName}:{parameter.Domain} url is error");
            return new TrackResultList(new ParameterError(ParameterErrorType.ParameterUrl, parameter.Domain));
        }
        foreach (var trackInfo in trackParameter.TrackOrderInfoLst)
        {
            List<TrackInfo> trackInfos = new List<TrackInfo>();
            if (trackInfo.TrackNumber.IsNullOrWhiteSpace())
            {
                OrderTracknumberRequest GetTrackNumRequest = new OrderTracknumberRequest(parameter, trackInfo.LogictisNumber ?? string.Empty);
                GetTrackNumRequest.Domain = Site;
                var tuple = await GetSingleTrackNumbers(GetTrackNumRequest, logName);
                if (tuple.Item1.IsNotNull())
                {
                    trackInfo.TrackNumber = tuple.Item1;
                }
                else
                {
                    trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, tuple.Item2, tuple.Item3));
                    continue;
                }
            }
            TrackRequest request = new TrackRequest(parameter, trackInfo.TrackNumber ?? string.Empty);
            request.Domain = Site;
            var TraceResponse = await Request.Send<TrackResponse>(request);
            string res = toJson(TraceResponse);
            if (TraceResponse.Success && TraceResponse.Result.IsNotNull())
            {
                var myResult = TraceResponse.Result;
                if (myResult.statusCode.Contains("success", StringComparison.CurrentCultureIgnoreCase))
                {
                    if (myResult.returnDatas.IsNotNull() && myResult.returnDatas.Count > 0)
                    {
                        var r = myResult.returnDatas[0];
                        if (r.statusCode.Contains("success", StringComparison.CurrentCultureIgnoreCase))
                        {
                            string TrackInfo = "";
                            var TrackItems = r.items;
                            TrackItems.Reverse();
                            foreach (var item in TrackItems)
                            {
                                trackInfos.Add(new TrackInfo(item.dateTime,"",item.location,item.info));
                                string Message = item.ToString();
                                TrackInfo += Message + "\r\n";
                            }
                            trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, trackInfo.LogictisNumber, trackInfo.TrackNumber, TrackInfo,
                                r.track.status, Enums.Logistics.States.TRANSITING, trackInfos,res));
                        }
                        else
                        {
                            logger.LogError($"k5-track error-{r.message}");
                            trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new UnKnownError($"track {r.message}"), res));
                        }
                    }
                    else
                    {
                        //尚未生成                                
                        logger.LogWarning($"{logName} Not Generated Track Error,customerNumber:{trackInfo.CustomNumber}");
                        trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new NotGeneratedTrackError(), res));
                    }
                }
                else
                {
                    logger.LogError($"{logName}-track error-{myResult.message}");
                    trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new UnKnownError($"track error:{myResult.message}"), res));
                }
            }
            else
            {
                trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, TraceResponse.exception, res));
            }
        }
        return new TrackResultList(trackResults);
    }


    public async Task<Tuple<string?, Exception, string>> GetSingleTrackNumbers(OrderTracknumberRequest request, string logName)
    {
        var response = await Request.Send<GetTrackNumResponse>(request);
        string res = toJson(response);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            if (myResult.statusCode.Contains("success", StringComparison.CurrentCultureIgnoreCase))
            {
                if (myResult.returnDatas.IsNotNull() && myResult.returnDatas.Count > 0)
                {
                    var v = myResult.returnDatas[0];

                    if (v.statusCode.Contains("success", StringComparison.CurrentCultureIgnoreCase))
                    {
                        return new Tuple<string?, Exception, string>(v.trackNumber, new UnKnownError("GetSingleTrackNumbers error"), res);
                    }
                    else
                    {
                        string msg = $"{v.customerNumber}-{v.message};";
                        logger.LogWarning($"{logName} getTrackNumber error,lognumber:{request.CorpBillidDatas[0].CorpBillid}-{msg}");
                        return new Tuple<string?, Exception, string>(null, new UnKnownError($"{msg}"), res);
                    }
                }
                else
                {
                    //尚未生成                   
                    logger.LogWarning($"{logName} trackNumber Not Generated Track Error,lognumber:{request.CorpBillidDatas[0].CorpBillid}");
                    return new Tuple<string?, Exception, string>(null, new NotGeneratedTrackError(), res);
                }
            }
            else
            {
                logger.LogWarning($"{logName} getTrackNumber error lognumber:{request.CorpBillidDatas[0].CorpBillid}");
                return new Tuple<string?, Exception, string>(null, new UnKnownError($"{myResult.message}"), res);
            }
        }
        else
        {
            return new Tuple<string?, Exception, string>(null, response.exception, res);
        }
    }

    public override bool ContentType { get; set; } = true;

    public override bool PaperType { get; set; } = true;

    public override bool FileType { get; set; } = true;

    public override bool IsUrl { get; set; } = true;
    //批量
    public override async Task<printResultList> Print(MultiPrint multiPrint)
    {
        string ContentType = "", PaperType = "";
        var logName = multiPrint.LogName.GetDescription();
        List<printResult> printResults = new List<printResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(multiPrint.Parameter);
        var Site = parameter.GetSite();
        if (Site is null)
        {
            logger.LogWarning($"{logName}:{parameter.Domain} url is error");
            return new printResultList(new ParameterError(ParameterErrorType.ParameterUrl, parameter.Domain));
        }

        if (Enum.IsDefined(typeof(PaperType), multiPrint.PaperType ?? string.Empty) && Enum.TryParse<PaperType>(multiPrint.PaperType, out var labelPaperType))
        {
            PaperType = labelPaperType.ToString();
        }
        else
        {
            logger.LogWarning($"{multiPrint.LogName.GetDescription()}-{multiPrint.PaperType} mismatched error ");
            return new printResultList(new ParameterError(PrintSettingEnum.PaperType, multiPrint.PaperType ?? "暂无类型"));
        }
        if (multiPrint.ContentType is not null && Enum.IsDefined(typeof(ContentType), multiPrint.ContentType) && Enum.TryParse<ContentType>(multiPrint.ContentType, out var labelContentType))
        {
            Sy.String printContent = labelContentType.ToString();
            printContent = printContent.Replace("And", ",").TrimStart(',');
            ContentType = printContent;
        }
        else
        {
            logger.LogWarning($"{multiPrint.LogName.GetDescription()}-{multiPrint.ContentType} mismatched error ");
            return new printResultList(new ParameterError(PrintSettingEnum.ContentType, multiPrint.ContentType ?? "暂无类型"));
        }
        
        var customers = multiPrint.printParams?.Select(s => s.CustomNumber!).ToList() ?? Array.Empty<string>().ToList();
        var express = multiPrint.printParams?.Select(s => s.Express!).ToList() ?? Array.Empty<string>().ToList(); ;
        PrintLabelRequest request = new PrintLabelRequest(parameter, express, PaperType, ContentType);
        request.Domain = Site;
        var response = await Request.Send<PrintLabelResponse>(request);
        string res = toJson(response);
        if (response.Success && response.Result.IsNotNull())
        {
            var myresult = response.Result;
            if (myresult.statusCode.Contains("success", StringComparison.CurrentCultureIgnoreCase))
            {
                printResults.Add(new printResult(customers, myresult.url, res));
            }
            else
            {
                logger.LogError($"{string.Join(',', customers)} get print error:{myresult.message}");
                printResults.Add(new printResult(customers, new UnKnownError(myresult.message), res));
            }
        }
        else
        {
            printResults.Add(new printResult(customers, response.exception, res));
        }
        return new printResultList(printResults);
    }


    public override bool EstimatePriceEnabled { get; set; } = true;

    public override async Task<EstimatePriceResult> EstimatePrice(PriceParameter priceParameter)
    {
        var logName = priceParameter.LogName.GetDescription();
        Parameter parameter = Helpers.GetParamType<Parameter>(priceParameter.Parameter);
        ExtParameter Ext = Helpers.GetParamType<ExtParameter>(priceParameter.ext);
        if (Ext.Weight <= 0)
        {
            logger.LogError($"{logName} EstimatePrice Ext.Weight less then 0");
            return new EstimatePriceResult(new ParameterError(ParameterErrorType.WeightNull));
        }
        if (priceParameter.CountryCode is null)
        {
            logger.LogError($"{logName} EstimatePrice CountryCode is null");
            return new EstimatePriceResult(new ParameterError(ParameterErrorType.CountryCodeNull));
        }
        EstimatePriceRequest request = new EstimatePriceRequest(parameter, priceParameter.CountryCode, Ext.Weight.ToString());
        var Site = parameter.GetSite();
        if (Site is null)
        {
            logger.LogWarning($"{logName}:{parameter.Domain} url is error");
            return new EstimatePriceResult(new ParameterError(ParameterErrorType.ParameterUrl, parameter.Domain));
        }
        request.Domain = Site;
        var response = await Request.Send<EstimatePriceResponse>(request);
        string res = toJson(response);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            List<CommonResult> datas = new List<CommonResult>();
            if (myResult.statusCode.Contains("success", StringComparison.CurrentCultureIgnoreCase))
            {
                myResult.returnDatas.ForEach(v => datas.Add(new CommonResult(v.channelName+$"[{v.channelCode}]",v.ToString())));
                return new EstimatePriceResult(datas, res);
            }
            else
            {
                logger.LogError($"EstimatePrice  error:{myResult.message}");
                return new EstimatePriceResult(new UnKnownError($"LoadChannelCode:{myResult.message}"), res);
            }
        }
        else
        {
            logger.LogError($"EstimatePrice error");
            return new EstimatePriceResult(response.exception, res);
        }
    }


    public override bool ChannelCode { get; set; } = true;

    /// <summary>
    /// 获取渠道方式
    /// </summary>
    /// <param name="baseParameter"></param>
    /// <returns></returns>
    public override async Task<CommonResultList> LoadChannelCode(Models.Api.Logistics.BaseParameter baseParameter)
    {
        string logName = baseParameter.LogName.GetDescription();
        Parameter parameter = Helpers.GetParamType<Parameter>(baseParameter.Parameter);
        ChannelCodeRequest request = new ChannelCodeRequest(parameter);
        var Site = parameter.GetSite();
        if (Site is null)
        {
            logger.LogWarning($"{logName}:{parameter.Domain} url is error");
            return new CommonResultList(new ParameterError(ParameterErrorType.ParameterUrl, parameter.Domain));
        }
        request.Domain = Site;
        var response = await Request.Send<ShipTypesResponse>(request);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            List<CommonResult> shipTypes = new List<CommonResult>();
            if (myResult.returnDatas.IsNotNull() && myResult.returnDatas.Count > 0)
            {
                foreach (var r in myResult.returnDatas)
                {
                    shipTypes.Add(new CommonResult(r.code, $"{r.cnname}[{r.code}]"));
                }
                return new CommonResultList(shipTypes);
            }
            else
            {
                logger.LogError($"LoadChannelCode  error:{myResult.message}");
                return new CommonResultList(new UnKnownError($"LoadChannelCode:{myResult.message}"));
            }
        }
        else
        {
            logger.LogError($"LoadChannelCode error");
            return new CommonResultList(response.exception);
        }
    }

    public override async Task<PrintParamResult> GetPrint(FileTypeParameter fileTypeParameter)
    {
        List<CommonResult> PaperTyperesults = new List<CommonResult>();
        foreach (PaperType paperType in Enum.GetValues(typeof(PaperType)))
        {
            PaperTyperesults.Add(new CommonResult(paperType.ToString(), paperType.GetDescription()));
        }
        List<CommonResult> ContentTyperesults = new List<CommonResult>();
        foreach (ContentType contentType in Enum.GetValues(typeof(ContentType)))
        {
            ContentTyperesults.Add(new CommonResult(contentType.ToString(), contentType.GetDescription()));
        }
        List<CommonResult> FileTyperesults = new List<CommonResult>();
        var response = await LoadCbFileType(fileTypeParameter);
        if (response.Success)
        {
            FileTyperesults = response.CommonResults;
        }
        else
        {
            return new PrintParamResult(response.exception);
        }
        return new PrintParamResult(ContentType, FileType, PaperType, ContentTyperesults, FileTyperesults, PaperTyperesults);
    }

    /// <summary>
    /// 获取纸张类型
    /// </summary>
    /// <param name="fileTypeParameter"></param>
    /// <returns></returns>
    public async Task<CommonResultList> LoadCbFileType(FileTypeParameter fileTypeParameter)
    {
        string logName = fileTypeParameter.LogName.GetDescription();
        Parameter parameter = Helpers.GetParamType<Parameter>(fileTypeParameter.Parameter);
        PrintPaperRequest request = new PrintPaperRequest(parameter, fileTypeParameter.code ?? "");
        var Site = parameter.GetSite();
        if (Site is null)
        {
            logger.LogWarning($"{logName}:{parameter.Domain} url is error");
            return new CommonResultList(new ParameterError(ParameterErrorType.ParameterUrl, parameter.Domain));
        }
        request.Domain = Site;
        var response = await Request.Send<ShipTypesResponse>(request);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            List<CommonResult> shipTypes = new List<CommonResult>();
            if (myResult.returnDatas.IsNotNull() && myResult.returnDatas.Count > 0)
            {
                foreach (var r in myResult.returnDatas)
                {
                    shipTypes.Add(new CommonResult(r.code, $"{r.cnname}[{r.code}]"));
                }
                return new CommonResultList(shipTypes);
            }
            else
            {
                logger.LogError($"LoadCbFileType  error:{myResult.message}");
                return new CommonResultList(new UnKnownError($"LoadCbFileType:{myResult.message}"));
            }
        }
        else
        {
            logger.LogError($"LoadCbFileType error");
            return new CommonResultList(response.exception);
        }
    }
}
