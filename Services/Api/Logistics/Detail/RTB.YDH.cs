namespace ERP.Services.Api.Logistics.Detail;
using Enums;
using ERP.Exceptions.Logistics;
using ERP.Models.Api.Logistics;
using ERP.Models.Api.Logistics.SDGJ;
using ERP.Models.Api.Logistics.SDGJ.YDH;
using AmazonService = DB.Orders.Order;

public class YDH : RTB
{
    public YDH(ILogger<YDH> logger, LogisticClient request, IServiceProvider serviceProvider) : base(Platforms.RTB_YDH, logger, request, serviceProvider)
    {
    }


    public override async Task<ReportResultList> Send(SendParameter sendParameter)
    {
        List<YDHAddOrderRequest> requests = new List<YDHAddOrderRequest>();
        List<ReportResult> results = new List<ReportResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(sendParameter.Parameter);
        SenderInfo senderInfo = sendParameter.SenderInfo;
        List<OrderInfo> orderLists = sendParameter.OrderInfos;
        foreach (var orderinfo in orderLists)
        {
            ExtParameter ext = Helpers.GetParamType<ExtParameter>(orderinfo.ext);
            YDHAddOrderRequest request = new YDHAddOrderRequest(parameter, senderInfo, orderinfo, ext, Platform);
            List<ErrorMember> errMsg = request.IsValid<YDHAddOrderRequest>();
            errMsg.AddRange(ext.IsValid<ExtParameter>());
            if (errMsg.Count <= 0)
            {
                requests.Add(request);
            }
            else
            {
                logger.LogWarning(ParameterErrorType.ParameterFormat.GetDescription(), toMsg(errMsg));
                results.Add(new ReportResult(request.Id, request.orderNo, new ParameterError(ParameterErrorType.ParameterFormat, toMsg(errMsg))));
                continue;
            }
        }
        if (requests.Count > 0)
        {
            foreach (var request in requests)
            {
                var response = await Request.Send<AddOrderResponse>(request);
                string res = toJson(response);
                if (response.Success && response.Result.IsNotNull())
                {
                    var myResult = response.Result;
                    if (myResult.success == 1)
                    {
                        results.Add(new ReportResult(request.Id, myResult.data.refrence_no, myResult.data.shipping_method_no, myResult.data.shipping_method_no,
                        "", 0, orderLists.Where(x => x.id == request.Id).Select(s => s.ext).FirstOrDefault() ?? string.Empty, res));
                    }
                    else if (response.Result.success == 2)
                    {
                        //var OrderService = ServiceProvider.GetRequiredService<OrderService>();
                        //OrderService.UpdateDeliveryFail(request.Id);
                        logger.LogWarning($"{sendParameter.LogName.GetDescription()},Orderid:{request.Id} Order Already Exists");
                        results.Add(new ReportResult(request.Id, request.orderNo, new OrderAlreadyExists(), res));
                    }
                    else
                    {
                        logger.LogWarning($"{sendParameter.LogName.GetDescription()},{myResult.cnmessage}");
                        results.Add(new ReportResult(request.Id, request.orderNo, new UnKnownError(myResult.cnmessage), res));
                    }
                }
                else
                {
                    results.Add(new ReportResult(request.Id, request.orderNo, response.exception, res));
                }
            }
        }
        return new ReportResultList(results);
    }


}
