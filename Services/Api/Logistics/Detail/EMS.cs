namespace ERP.Services.Api.Logistics.Detail;
using Enums;
using Exceptions.Logistics;
using Models.Api.Logistics;
using Models.Api.Logistics.EMS;

public class EMS : Base<Parameter>
{
    public string logName = "";
    public EMS(ILogger<EMS> logger, LogisticClient request, IServiceProvider serviceProvider) : base(Platforms.EMS, logger, request, serviceProvider)
    {
        logName = base.Platform.GetDescription();
    }

    public override async Task<CommonResultList> GetShipTypes(ShipTypeParameter shipTypeParam)
    {
        Parameter parameter = Helpers.GetParamType<Parameter>(shipTypeParam.Parameter);
        ShipTypesRequest shipTypesRequest = new ShipTypesRequest(parameter);
        var response = await Request.Send<ShipTypeResponse>(shipTypesRequest);
        if (response.Success && response.Result.IsNotNull())
        {
            List<CommonResult> shipTypes = new List<CommonResult>();
            var result = response.Result;
            if (result.data.IsNotNull() && result.data.Count > 0)
            {
                result.data.ForEach(r => shipTypes.Add(new CommonResult(r.businessCode, r.businessName)));
                return new CommonResultList(shipTypes);
            }
            else
            {
                return new CommonResultList(shipTypes);
            }
        }
        else
        {
            return new CommonResultList(response.exception);
        }
    }

    public override async Task<ReportResultList> Send(SendParameter sendParameter)
    {
        List<AddOrderRequest> requests = new List<AddOrderRequest>();
        List<ReportResult> results = new List<ReportResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(sendParameter.Parameter);
        SenderInfo senderInfo = sendParameter.SenderInfo;
        List<OrderInfo> orderLists = sendParameter.OrderInfos;
        foreach (var orderinfo in orderLists)
        {
            ExtParameter Ext = Helpers.GetParamType<ExtParameter>(orderinfo.ext);
            AddOrderRequest data = new AddOrderRequest(parameter, senderInfo, orderinfo, Ext);
            List<ErrorMember> errMsg = Ext.IsValid<ExtParameter>();
            errMsg.AddRange(data.IsValid<AddOrderRequest>());
            if (errMsg.Count <= 0)
            {
                requests.Add(data);
            }
            else
            {
                logger.LogWarning(ParameterErrorType.ParameterFormat.GetDescription(), toMsg(errMsg));
                results.Add(new ReportResult(orderinfo.id, orderinfo.OrderNo, new ParameterError(ParameterErrorType.ParameterFormat, toMsg(errMsg))));
                continue;
            }
        }
        if (requests.Count > 0)
        {
            foreach (var request in requests)
            {
                var response = await Request.Send<AddOrderResponse>(request);
                string res = toJson(response);
                if (response.Success && response.Result.IsNotNull())
                {
                    if (response.Result.success.Contains("true", StringComparison.CurrentCultureIgnoreCase))
                    {
                        var result = response.Result;
                        results.Add(new ReportResult(request.Id, request.logistics_order_no, result.waybillNo, "", "", 0,
                            orderLists.Where(x => x.id == request.Id).Select(s => s.ext).FirstOrDefault()??string.Empty, res));
                    }
                    else
                    {
                        string msg = response.Result.reason + "-" + response.Result.msg + ";";
                        logger.LogError($"{request.orderNo}:{msg}");
                        results.Add(new ReportResult(request.Id, request.orderNo, new UnKnownError($"{msg}"), res));
                    }
                }
                else
                {
                    results.Add(new ReportResult(request.Id, request.orderNo, response.exception, res));
                }
            }
        }
        return new ReportResultList(results);
    }

    private async Task<TrackPrice> GetFeeBySingle(Parameter parameter, string customNum, string LogNumber, int waybillId)
    {
        GetFeeRequest getFeeRequest = new GetFeeRequest(parameter, LogNumber);
        var myResult = await Request.Send<GetFeeResponse>(getFeeRequest);
        string res = toJson(myResult);
        if (myResult.Success && myResult.Result.IsNotNull())
        {
            var response = myResult.Result;
            if (response.return_success.Contains("success", StringComparison.CurrentCultureIgnoreCase))
            {
                if (response.data.Count > 0)
                {
                    var a = response.data[0];
                    return new TrackPrice(waybillId, customNum, LogNumber, a.traceNo, "CNY", a.postageTotal, a.realWeight.ToString(), "", res);
                }
                else
                {
                    //尚未生成
                    logger.LogError($"{customNum}-GetFeeBySingle");
                    return new TrackPrice(waybillId, customNum, new NotGeneratedPriceError(), res);
                }
            }
            else
            {
                string msg = response.return_reason + "-" + response.return_msg;
                logger.LogError($"{customNum}-GetFeeBySingle-{msg}");
                return new TrackPrice(waybillId, customNum, new UnKnownError($"GetFeeBySingle-{msg}"), res);
            }
        }
        else
        {
            return new TrackPrice(waybillId, customNum, myResult.exception, res);
        }
    }


    public override async Task<WaybillPrice> GetFee(WaybillInfos wayBillParameter)
    {
        Parameter parameter = Helpers.GetParamType<Parameter>(wayBillParameter.Parameter);
        var price = await GetFeeBySingle(parameter, wayBillParameter.CustomNumber, wayBillParameter.LogictisNumber, wayBillParameter.waybillId);
        return new WaybillPrice(price);
    }


    public override async Task<TrackResultList> Track(TrackParameter trackParameter)
    {
        TrackPrice myPrice = new TrackPrice();
        List<TrackResult> trackResults = new List<TrackResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(trackParameter.Parameter);
        foreach (var trackInfo in trackParameter.TrackOrderInfoLst)
        {
            List<TrackInfo> trackInfos = new List<TrackInfo>();
            //费用
            //if (trackInfo.Price == 0 || trackInfo.TrackNumber.IsNullOrEmpty())
            //{
                myPrice = await GetFeeBySingle(parameter, trackInfo.CustomNumber, trackInfo.LogictisNumber?? "暂无物流系统运单号", trackInfo.waybillId);
                if (!myPrice.TrackNum.IsNullOrWhiteSpace())
                {
                    //获取跟踪信息
                    trackInfo.TrackNumber = myPrice.TrackNum;
                }
                else
                {
                    //返回exception
                    logger.LogError($"{trackInfo.CustomNumber}-GetFee Error");
                    trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, myPrice.exception, myPrice.Response));
                    continue;
                }
            //}
            TrackRequest trackRequest = new TrackRequest(parameter, trackInfo.TrackNumber ?? string.Empty);
            var myResult = await Request.Send<TrackResponse>(trackRequest);
            string res = toJson(myResult);
            if (myResult.Success && myResult.Result.IsNotNull())
            {
                var response = myResult.Result;
                if (response.success)
                {
                    var lst = response.value.domesticMailResultDtoList;
                    if (lst.Count > 0)
                    {
                        string TrackInfo = "";
                        var a = response.value.domesticMailResultDtoList[0];
                        var RawState = a.resCode + "-" + a.resMsg;
                        var obj = a.resObj;
                        if (obj.Count > 0)
                        {
                            obj.Reverse();
                            foreach (var v in obj)
                            {
                                trackInfos.Add(new TrackInfo(v.opTime.ToString(),$"{v.opCode}-{v.opName}", v.opOrgCity, v.opDesc));
                                string Message = $"Time: {v.opTime.ToString()}|state:{v.opCode}-{v.opName}|Location: {v.opOrgCity}|Track Description: {v.opDesc}";
                                TrackInfo += Message + "\r\n";
                            }
                            trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, trackInfo.LogictisNumber, trackInfo.TrackNumber, TrackInfo, RawState, Enums.Logistics.States.TRANSITING, myPrice, trackInfos, res));
                        }
                        else
                        {
                            //暂无track信息
                            logger.LogWarning($"{trackInfo.CustomNumber}-Not Generated Track Error");
                            trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new NotGeneratedTrackError(), res));
                        }
                    }
                    else
                    {
                        //暂无track信息
                        logger.LogWarning($"{trackInfo.CustomNumber}-Not Generated Track Error");
                        trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new NotGeneratedTrackError(), res));
                    }
                }
                else
                {
                    logger.LogError($"{trackInfo.CustomNumber}-{response.message}");
                    trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new UnKnownError($"{response.message}"), res));
                }
            }
            else
            {
                trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, myResult.exception, res));
            }
        }
        return new TrackResultList(trackResults);
    }

    //单个
    public override async Task<printResultList> Print(MultiPrint multiPrint)
    {
        List<printResult> printResults = new List<printResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(multiPrint.Parameter);
        if (multiPrint.printParams is not null)
        foreach (var printInfo in multiPrint.printParams)
        {
            PrintUrlRequest printUrlRequest = new PrintUrlRequest(parameter, printInfo.Express?? "SF 追踪号暂无");
            var response = await Request.Send<PrintUrlResponse>(printUrlRequest);
            string res = toJson(response);
            if (response.Success && response.Result.IsNotNull())
            {
                var myResult = response.Result;
                if (myResult.success.Contains("true", StringComparison.CurrentCultureIgnoreCase))
                {
                    HttpResponseMessage httpResponse = new HttpResponseMessage(System.Net.HttpStatusCode.OK);
                    byte[] byteData = Helpers.HexStringToString(myResult.data);
                    using (var content = new MemoryStream(byteData))
                    {
                        printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, content, "application/pdf", ".pdf", res));
                    }
                }
                else
                {
                    string msg = $"{printInfo.CustomNumber}-{myResult.err_code}-{myResult.msg}";
                    logger.LogError(msg);
                    printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, new UnKnownError(msg), res));
                }
            }
            else
            {
                printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, response.exception, res));
            }
        }
        return new printResultList(printResults);
    }
}
