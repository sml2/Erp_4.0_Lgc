﻿
using ERP.Enums;
using ERP.Models.Api.Logistics;
using ERP.Models.Api.Logistics.HL;

namespace ERP.Services.Api.Logistics.Detail
{
    public class HuaLei_YH : HuaLei
    {       
        public HuaLei_YH(ILogger<HuaLei_YH> logger, LogisticClient request, IServiceProvider serviceProvider) :
            base(Platforms.HuaLei_YH,logger, request, serviceProvider)
        {
        }

        //永合保留的物流线路ID
        private static readonly string[] AllowedProductId = new string[]
        {
           "12913",
"12773",
"7463",
"10753",
"5721",
"7462",
"8853",
"10273",
"12533",
"8813",
"8833",
"12296",
"12314",
"12313",
"12295",
"12294",
"12333",
"12753",
"12733",
"4001",
"7281",
"6781",
"6761",
"5662",
"8773",
"12673",
"12393",
"9813",
"9833",
"5501",
"9993",
"5761",
"4101",
"12513",
"4021",
"12833",
"12653",
"12813",
"11913",
"12793",
"10193",
"1082",
"9733",
"12693",
"7441",
"10194",
"9753",
"4581",
"5681",
"5381",
"5361",
"10073",
"7421",
"6561",
"5561",
"5581",
"5701",
"5661",
        };


        public override async Task<CommonResultList> GetShipTypes(ShipTypeParameter shipTypeParam)
        {
           var CommonResults = await  base.GetShipTypes(shipTypeParam);
           var lst = CommonResults.CommonResults.Where(s => AllowedProductId.Contains(s.Code)).ToList();
           return new CommonResultList(lst);
        }



    }
}
