namespace ERP.Services.Api.Logistics.Detail;
using Models.Api.Logistics;
using Models.Api.Logistics.YunTu;
using Enums;
using Exceptions.Logistics;
using System.Net;
using Microsoft.AspNetCore.StaticFiles;

public class YunTu : Base<Parameter>
{
    protected string logName = "";
    public YunTu(ILogger<YunTu> logger, LogisticClient request, IServiceProvider serviceProvider) : base(Platforms.YunTu, logger, request, serviceProvider)
    {
        logName = base.Platform.GetDescription();
    }

    public override async Task<CommonResultList> GetShipTypes(ShipTypeParameter shipTypeParam)
    {
        var logName = shipTypeParam.LogName.GetDescription();
        List<CommonResult> shipTypes = new List<CommonResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(shipTypeParam.Parameter);
        if (shipTypeParam.CountryCode is null)
        {
            logger.LogWarning($"{logName} CountryCode parameter is null");
            return new CommonResultList(new ParameterError(ParameterErrorType.CountryCodeNull));
        }
        ShipTypeRequest request = new ShipTypeRequest(parameter, shipTypeParam.CountryCode);
        var response = await Request.Send<ShipTypeResponse>(request);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;

            if (myResult.Items.IsNotNull() && myResult.Items.Count > 0)
            {
                myResult.Items.ForEach(r => shipTypes.Add(new CommonResult(r.Code, r.CName)));
                return new CommonResultList(shipTypes);
            }
            else
            {
                logger.LogError($"{logName}-GetShipTypes-{myResult.Code}:{myResult.Message}");
                return new CommonResultList(new UnKnownError($"{myResult.Code}:{myResult.Message}"));

            }
        }
        else
        {
            return new CommonResultList(response.exception);
        }
    }

    public override async Task<ReportResultList> Send(SendParameter sendParameter)
    {
        List<AddOrderRequest> requests = new List<AddOrderRequest>();
        List<ReportResult> results = new List<ReportResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(sendParameter.Parameter);
        SenderInfo senderInfo = sendParameter.SenderInfo;
        List<OrderInfo> orderLists = sendParameter.OrderInfos;
        foreach (var OrderInfo in orderLists)
        {
            ExtParameter Ext = Helpers.GetParamType<ExtParameter>(OrderInfo.ext);
            AddOrderRequest request = new AddOrderRequest(parameter, senderInfo, OrderInfo, Ext);
            List<ErrorMember> errMsg = request.IsValid<AddOrderRequest>();
            errMsg.AddRange(Ext.IsValid<ExtParameter>());
            if (errMsg.Count <= 0)
            {
                requests.Add(request);
            }
            else
            {
                logger.LogWarning(ParameterErrorType.ParameterFormat.GetDescription(), toMsg(errMsg));
                results.Add(new ReportResult(OrderInfo.id, request.orderNo, new ParameterError(ParameterErrorType.ParameterFormat, toMsg(errMsg))));
                continue;
            }
        }
        YunTuRequest yunTuRequest = new YunTuRequest(parameter);
        yunTuRequest.Datas = requests;
        var response = await Request.Send<AddOrderResponse>(yunTuRequest);
        string res = toJson(response);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            if (myResult.Item.IsNotNull() && myResult.Item.Count > 0)
            {
                foreach (var r in myResult.Item)
                {
                    var referenceNo = Helpers.GetOldOrderNo(r.CustomerOrderNumber);
                    var id = orderLists.Where(x => x.OrderNo == referenceNo).Select(x => x.id).FirstOrDefault();
                    if (r.Success == 1)
                    {
                        results.Add(new(id, r.CustomerOrderNumber, r.WayBillNumber, r.TrackingNumber, "", 0,
                            orderLists.Where(x => x.id == id).Select(s => s.ext).FirstOrDefault()??"", res
                            ));
                    }
                    else
                    {
                        if (r.Remark.Contains("重复"))
                        {
                            logger.LogError($"{r.CustomerOrderNumber} error：订单重复");
                            results.Add(new ReportResult(id, r.CustomerOrderNumber, new OrderAlreadyExists(), res));
                        }
                        else
                        {
                            logger.LogError($"customNumber:{r.CustomerOrderNumber}，error：{r.Remark}");
                            results.Add(new ReportResult(id, r.CustomerOrderNumber, new UnKnownError($"{r.Remark}"), res));
                        }
                    }
                }
            }
            else
            {
                logger.LogError(myResult.Message);
                return new ReportResultList(new UnKnownError(myResult.Message), res);
            }
        }
        else
        {
            return new ReportResultList(response.exception, res);
        }
        return new ReportResultList(results);
    }

    private async Task<TrackPrice> GetFeeInfo(Parameter parameter, string CustomNumber, string LogisticNumber, string logName, int waybillId)
    {
        GetFeeRequest request = new GetFeeRequest(parameter, LogisticNumber);
        var response = await Request.Send<GetFeeResponse>(request);
        string res = toJson(response);
        if (response.Success && response.Result.IsNotNull()) //请求成功
        {
            var myResult = response.Result;
            if (myResult.Code == "0000")
            {
                if (myResult.Item is not null )
                {
                    // if (Math.Max(myResult.Item.TotalFee, myResult.Item.StandardMoney) != 0)
                    // {
                        DateTime? deduction = null;
                        if (!string.IsNullOrWhiteSpace(myResult.Item.OccurrenceTime))
                        {
                            deduction = Convert.ToDateTime(myResult.Item.OccurrenceTime);
                        }
                        
                        TrackPrice price = new TrackPrice(waybillId, CustomNumber, LogisticNumber, myResult.Item.TrackingNumber, "CNY", Math.Max(myResult.Item.TotalFee, myResult.Item.StandardMoney), myResult.Item.ChargeWeight, "", res,deduction);
                        return price;
                    // }
                    //
                    // //尚未生成  
                    // logger.LogWarning($"{logName} price Not Generated  Error,CustomNumber:{CustomNumber}");
                    // return new TrackPrice(waybillId, CustomNumber, new NotGeneratedPriceError(), res);

                }
                else
                {
                    //尚未生成                                 
                    logger.LogWarning($"{logName} price Not Generated  Error,CustomNumber:{CustomNumber}");
                    return new TrackPrice(waybillId, CustomNumber, new NotGeneratedPriceError(), res);
                }
            }
            else
            {
                string msg = $"{myResult.Message};";
                logger.LogWarning($"{logName} getPrice error,CustomNumber:{CustomNumber}-{msg}");
                return new TrackPrice(waybillId, CustomNumber, new UnKnownError($"{msg}"), res);
            }
        }
        else
        {
            return new TrackPrice(waybillId, CustomNumber, response.exception, res);
        }
    }



    public override async Task<WaybillPrice> GetFee(WaybillInfos wayBillParameter)
    {
        Parameter parameter = Helpers.GetParamType<Parameter>(wayBillParameter.Parameter);       
        var price = await GetFeeInfo(parameter, wayBillParameter.CustomNumber, wayBillParameter.LogictisNumber, wayBillParameter.LogName.GetDescription(), wayBillParameter.waybillId);
        return new WaybillPrice(price);
    }


    public override async Task<TrackResultList> Track(TrackParameter trackParameter)
    {
        List<TrackResult> trackResults = new List<TrackResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(trackParameter.Parameter);
        foreach (var trackInfo in trackParameter.TrackOrderInfoLst)
        {
            List<TrackInfo> trackInfos = new List<TrackInfo>();
            TrackPrice trackPrice = new TrackPrice();
            //获取物流id
            if (trackInfo.LogictisNumber.IsNullOrWhiteSpace())
            {
                Tuple<string?, Exception, string> tuple = await GetTrackNumber(parameter, trackInfo.CustomNumber, logName);
                if (tuple.Item1.IsNotNull())
                {
                    trackInfo.TrackNumber = tuple.Item1;
                }
                else
                {
                    trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, tuple.Item2, tuple.Item3));
                }
            }
            TrackRequest request = new TrackRequest(parameter, trackInfo.LogictisNumber ?? "-1");
            var response = await Request.Send<TrackResponse>(request);
            string res = toJson(response);
            if (response.Success && response.Result.IsNotNull())
            {
                var myResult = response.Result;
                if (myResult.Code == "0000")
                {
                    string TrackInfo = "";
                    if (myResult.Item.IsNotNull() && myResult.Item.OrderTrackingDetails.IsNotNull() && myResult.Item.OrderTrackingDetails.Count > 0)
                    {
                        trackInfo.TrackNumber = trackInfo.TrackNumber.IsNullOrWhiteSpace() ? myResult.Item.TrackingNumber : trackInfo.TrackNumber;
                        var s = getState(myResult.Item.PackageState);
                        var ss = s.GetDescription();
                        foreach (var v in myResult.Item.OrderTrackingDetails)
                        {
                            trackInfos.Add(new TrackInfo(v.ProcessDate, s.GetDescription(), v.ProcessLocation, v.ProcessContent));
                            string Message = $"Time: {v.ProcessDate}|Location: {v.ProcessLocation}|Track Description: {v.ProcessContent}";
                            TrackInfo += Message + "\r\n";
                        }                       
                        var RawState = myResult.Item.PackageState.ToString() + "-" + ((EnumPackageState)Convert.ToInt32(myResult.Item.PackageState)).GetDescription();
                        //getfee
                        //if (trackInfo.Price.IsNull() || trackInfo.Price == 0)
                        //{
                            trackPrice = await GetFeeInfo(parameter, trackInfo.CustomNumber, trackInfo.LogictisNumber!, logName, trackInfo.waybillId);
                       // }
                        trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, trackInfo.LogictisNumber, trackInfo.TrackNumber,
                           TrackInfo, RawState, s, trackPrice, trackInfos, res));
                    }
                    else
                    {
                        //尚未生成                   
                        logger.LogWarning($"{logName} getTrack Not Generated Track Error,CustomNumber:{trackInfo.CustomNumber}");
                        trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new NotGeneratedTrackError(), res));
                    }
                }
                else
                {
                    logger.LogError($"{logName}-track error-{myResult.Message}");
                    trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new UnKnownError($"{myResult.Message}"), res));
                }
            }
            else
            {
                trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, response.exception, res));
            }
        }
        return new TrackResultList(trackResults);
    }

    public Enums.Logistics.States  getState(int PackageState)
    {
        Enums.Logistics.States states = Enums.Logistics.States.COMMITTED;
        if (PackageState == 0)
        {
            states = Enums.Logistics.States.OTHER;
        }
        else if (PackageState == 1)
        {
            states = Enums.Logistics.States.COMMITTED;
        }
        else if (PackageState == 2 ||PackageState == 4)
        {
            states = Enums.Logistics.States.TRANSITING;
        }
        else if (PackageState == 3)
        {
            states = Enums.Logistics.States.SINGED;
        }
        else if (PackageState == 6 || PackageState == 5)
        {
            states = Enums.Logistics.States.DELETED;
        }
        else if (PackageState == 7)
        {
            states = Enums.Logistics.States.RETURNDEPOT;
        }
        return states;
    }
    public async Task<Tuple<string?, Exception, string>> GetTrackNumber(Parameter Parameter, string CustomNumber, string logName)
    {
        GetTrackNumberRequest request = new GetTrackNumberRequest(Parameter, CustomNumber);
        var response = await Request.Send<GetTrackNumberResponse>(request);
        string res = toJson(response);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            if (myResult.Code == "0000")
            {
                if (myResult.Item.IsNotNull())
                {
                    var item = myResult.Item[0];
                    return new(item.WayBillNumber, new Exception(""), res);
                }
                else
                {
                    //尚未生成                   
                    logger.LogWarning($"{logName} trackNumber Not Generated,customNumber:{CustomNumber}");
                    return new Tuple<string?, Exception, string>(null, new NotGeneratedTrackError(), res);
                }
            }
            else
            {
                logger.LogWarning($"{logName} getTrackNumber error,customNumber:{CustomNumber}-{myResult.Message}");
                return new Tuple<string?, Exception, string>(null, new UnKnownError($"{myResult.Message}"), res);
            }
        }
        else
        {
            return new Tuple<string?, Exception, string>(null, response.exception, res);
        }
    }

    public override bool IsUrl { get; set; } = true;

    /// <summary>
    /// 支持批量
    /// </summary>
    /// <param name="multiPrint"></param>
    /// <returns></returns>
    public override async Task<printResultList> Print(MultiPrint multiPrint)
    {
        List<printResult> printResults = new List<printResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(multiPrint.Parameter);        
        if (multiPrint.printParams is not null)
        {
            foreach (var printInfo in multiPrint.printParams)
            {
                PrintUrlRequest request = new PrintUrlRequest(parameter, printInfo.Express ?? string.Empty);
                var Response = await Request.Send<PrintUrlResponse>(request);
                string res = toJson(Response);
                if (Response.Success && Response.Result.IsNotNull())
                {
                    var myResult = Response.Result;
                    if (myResult.Item.IsNotNull() && myResult.Item.Count > 0)
                    {
                        printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, myResult.Item[0].Url, res));
                    }
                    else
                    {
                        if (myResult.Code.IsNullOrWhiteSpace())
                        {
                            logger.LogError($"{printInfo.CustomNumber} Label information has not been generated ");
                            printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, new NotGeneratedLabelError(), res));
                        }
                        else
                        {
                            logger.LogError($"{printInfo.CustomNumber} print error:{myResult.Code}-{myResult.Message}");
                            printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, new UnKnownError($"{myResult.Code}-{myResult.Message}"), res));
                        }
                    }
                }
                else

                {
                    logger.LogError($"{printInfo.CustomNumber} print error");
                    printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, Response.exception, res));
                }
            }
        }
        return new printResultList(printResults);
    }

    public override bool EstimatePriceEnabled { get; set; } = true;
    public override async Task<EstimatePriceResult> EstimatePrice(PriceParameter priceParameter)
    {
        Parameter parameter = Helpers.GetParamType<Parameter>(priceParameter.Parameter);
        ExtParameter Ext = Helpers.GetParamType<ExtParameter>(priceParameter.ext);
        if (Ext.Length <= 0 || Ext.Width <= 0 || Ext.Height <= 0 || Ext.Weight <= 0)
        {
            logger.LogError($"{logName} EstimatePrice：Length or Width or Height or Weight less then 0");
            return new EstimatePriceResult(new ParameterError(ParameterErrorType.PackageNull));
        }
        if (priceParameter.CountryCode.IsNull())
        {
            logger.LogError($"{logName} EstimatePrice CountryCode is null");
            return new EstimatePriceResult(new ParameterError(ParameterErrorType.CountryCodeNull));
        }
        GetEstimatePriceRequest request = new GetEstimatePriceRequest(parameter, Ext, priceParameter.CountryCode ?? "暂无");
        var response = await Request.Send<GetEstimatePriceResponse>(request);
        string res = toJson(response);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            List<CommonResult> lst = new List<CommonResult>();
            if (myResult.Code.Equals("0000")) //
            {
                if (myResult.Items.IsNotNull() && myResult.Items.Count > 0)
                {
                    myResult.Items.ForEach(v => lst.Add(new CommonResult(v.Code, $"{v.Code}-{v.CName}:{v.TotalFee}\r\n")));
                }
                return new EstimatePriceResult(lst, res);
            }
            else
            {
                logger.LogError($"EstimatePrice  error:" + myResult.Message);
                return new EstimatePriceResult(new UnKnownError(myResult.Message), res);
            }
        }
        else
        {
            logger.LogError($"EstimatePrice error");
            return new EstimatePriceResult(response.exception, res);
        }
    }

    public override bool CancleEnabled { get; set; } = true;

    public override async Task<CancelResult> CancleOrder(CancleParameter cancleParameter)
    {
        Parameter parameter = Helpers.GetParamType<Parameter>(cancleParameter.Parameter);
        InterceptRequest request = new InterceptRequest(parameter, cancleParameter.LogictisNumber);
        var response = await Request.Send<InterceptResponse>(request);
        string res = toJson(response);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            if(myResult.Code.Equals("0000") || myResult.Code.Equals("25"))
            {
                return new CancelResult(res);
            }
            else
            {
                //未在物流平台创建运单或者物流平台那边已删除，接口返回找不到运单，导致平台运单无法取消
                if (myResult.Code.Equals("9006") && myResult.Message.Contains("订单不存在"))
                {
                    return new CancelResult(res);
                }
                return new CancelResult(new UnKnownError(myResult.Message),res);
            }
        }
        else
        {
            logger.LogError($"Cancle error");
            return new CancelResult(response.exception, res);
        }
    }
}
