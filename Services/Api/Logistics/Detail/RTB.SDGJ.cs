﻿namespace ERP.Services.Api.Logistics.Detail;
using Enums;
using AmazonService = DB.Orders.Order;

public class SDGJ : RTB
{
    public SDGJ(ILogger<SDGJ> logger, LogisticClient request, IServiceProvider serviceProvider) : base(Platforms.RTB_SDGJ, logger, request, serviceProvider)
    {
    }
}
