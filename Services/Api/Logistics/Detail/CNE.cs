namespace ERP.Services.Api.Logistics.Detail;
using Enums;
using Interface;
using Models.Api.Logistics;
using Models.Api.Logistics.CNE;

using Exceptions.Logistics;
using static Models.Api.Logistics.CNE.TrackResponse;
using Enums.Logistics;

public abstract class CNE : Base<Parameter>
{
    public string logName = "";
    public CNE(Platforms platforms, ILogger logger, LogisticClient request, IServiceProvider serviceProvider) :
        base(platforms, logger, request, serviceProvider)
    {
        logName = platforms.GetDescription();
    }
    public override async Task<CommonResultList> GetShipTypes(ShipTypeParameter shipTypeParam)
    {
        List<CommonResult> shipTypes = new List<CommonResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(shipTypeParam.Parameter);
        ShipTypesRequest request = new ShipTypesRequest(parameter, Platform);
        var myResult = await Request.Send<ShipTypesResponse>(request);
        if (myResult.Success && myResult.Result.IsNotNull())
        {
            var response = myResult.Result;
            if (response.ReturnValue > 0)
            {
                response.List.ForEach(r => shipTypes.Add(new CommonResult(r.oName, r.cName)));
                return new CommonResultList(shipTypes);
            }
            else
            {
                logger.LogError(response.cMess);
                return new CommonResultList(new UnKnownError(response.cMess));
            }
        }
        else
        {
            return new CommonResultList(myResult.exception);
        }
    }

    public override async Task<ReportResultList> Send(SendParameter sendParameter)
    {
        Parameter parameter = Helpers.GetParamType<Parameter>(sendParameter.Parameter);
        List<ReportResult> results = new List<ReportResult>();
        List<Cls_RecList> datas = new List<Cls_RecList>();
        AddOrderRequest request = new AddOrderRequest(parameter, Platform);
        foreach (var orderinfo in sendParameter.OrderInfos)
        {
            ExtParameter ext = Helpers.GetParamType<ExtParameter>(orderinfo.ext);
            List<ErrorMember> errorList = ext.IsValid<ExtParameter>();
            Cls_RecList data = new Cls_RecList(sendParameter.SenderInfo, orderinfo, ext);
            errorList.AddRange(data.IsValid<Cls_RecList>());  //检查接口参数
            if (errorList.Count <= 0)
            {
                datas.Add(data);
            }
            else
            {
                logger.LogWarning(ParameterErrorType.ParameterFormat.GetDescription(), toMsg(errorList));
                results.Add(new ReportResult(orderinfo.id, orderinfo.OrderNo, new ParameterError(ParameterErrorType.ParameterFormat, toMsg(errorList))));
                continue;
            }
        }
        if (datas.Count > 0)
        {
            request.RecList = datas;
            var sendResult = await Request.Send<AddOrderResponse>(request);
            string res = toJson(sendResult);
            if (sendResult.Success && sendResult.Result.IsNotNull())
            {
                var response = sendResult.Result;
                if (response != null && response.ReturnValue > 0 && response.ErrList.Count > 0)
                {
                    foreach (var r in response.ErrList)
                    {
                        var referenceNo = Helpers.GetOldOrderNo(r.cRNo);
                        var order = sendParameter.OrderInfos.Where(x => x.OrderNo == referenceNo).FirstOrDefault();
                        if (r.iID > 0)
                        {
                            results.Add(new(order?.id>0?order.id:-1, r.cRNo, r.cNo, "", "", 0,
                              order?.ext??string.Empty, res));
                        }
                        else
                        {
                            logger.LogError($"customNumber:{r.cRNo}，error：{r.cMess}");
                            results.Add(new ReportResult(order?.id > 0 ? order.id : -1, r.cRNo, new UnKnownError(r.cMess), res));
                        }
                    }
                    return new ReportResultList(results);
                }
                else
                {
                    logger.LogError(response!.cMess);
                    return new ReportResultList(new UnKnownError(response.cMess), res);
                }
            }
            else
            {
                return new ReportResultList(sendResult.exception, res);
            }
        }
        else
        {
            return new ReportResultList(results);
        }
    }

    private static string getStatus(string state)
    {
        string s = "";
        if (!string.IsNullOrWhiteSpace(state) && !"null".Equals(state))
        {
            s = state + ":" + ((EnumStatus)Convert.ToInt32(state)).ToString() + "-" + ((EnumStatus)Convert.ToInt32(state)).GetDescription();
        }
        else
        {
            s = state;
        }
        return s;
    }

    private Enums.Logistics.States GetLogStates(string state)
    {
        EnumStatus enumStatus = (EnumStatus)Convert.ToInt32(state);
        switch (enumStatus)
        {
            case EnumStatus.Warehoused:  //已入仓 
                return Enums.Logistics.States.COMMITTED;
            case EnumStatus.OutOfWarehouse:   //已出仓
                return Enums.Logistics.States.WAITTRANSPORT;
            case EnumStatus.InTransit: //转运中
                return Enums.Logistics.States.TRANSITING;
            case EnumStatus.Delivered:   //送达
                return Enums.Logistics.States.RECEIVED;
            case EnumStatus.Overtime:     //超时            
                return Enums.Logistics.States.OTHER;
            case EnumStatus.Detained:   //扣关
                return Enums.Logistics.States.OTHER;
            case EnumStatus.AddressError: //地址错误
                return Enums.Logistics.States.OTHER;
            case EnumStatus.Missing:   //快件丢失
                return Enums.Logistics.States.LOST;
            case EnumStatus.Returned:   //退件
                return Enums.Logistics.States.RETURNING;
            case EnumStatus.OtherError:   //其它异常
                return Enums.Logistics.States.OTHER;
            case EnumStatus.Destroy:   //销毁
                return Enums.Logistics.States.OTHER;
            default:
                return Enums.Logistics.States.TRANSITING;
        }
    }

    public override async Task<TrackResultList> Track(TrackParameter trackParameter)
    {
        List<TrackResult> trackResults = new List<TrackResult>();
        Parameter parameter = parameter = Helpers.GetParamType<Parameter>(trackParameter.Parameter);
        if (trackParameter.TrackOrderInfoLst is not null)
        {
            foreach (var trackInfo in trackParameter.TrackOrderInfoLst)
            {
                List<TrackInfo> trackInfos = new List<TrackInfo>();
                TrackRequest request = new TrackRequest(parameter, trackInfo.LogictisNumber??"暂无", Platform);
                var myResult = await Request.Send<TrackResponse>(request);
                if (myResult.Success)
                {
                    if (myResult.Result.IsNotNull())
                    {
                        var response = myResult.Result;
                        string TrackInfo = "";
                        if (response.ReturnValue > 0)
                        {
                            if (response.trackingEventList is not null && response.trackingEventList.Count > 0)
                            {
                                foreach (var v in response.trackingEventList)
                                {
                                    trackInfos.Add(new TrackInfo(v.date.ToString(), getStatus(v.state), v.place, v.details));
                                    string Message = $"Time: {v.date.ToString()}|state:{getStatus(v.state)}|Location: {v.place}|Track Description: {v.details}";
                                    TrackInfo += Message + "\r\n";
                                }
                            }
                            trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, response.Response_Info.trackingNbr, response.Response_Info.trackingNbr,
                                TrackInfo, getStatus(response.Response_Info.status), GetLogStates(response.Response_Info.status), trackInfos, toJson(response)));
                        }
                        else
                        {
                            string msg = response.cMess;
                            logger.LogError($"{trackParameter.LogName.GetDescription()}:Track-{response.cMess}");
                            trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new UnKnownError(response.cMess), toJson(response)));
                        }
                    }
                    else
                    {
                        //尚未生成追踪信息
                        logger.LogWarning($"{trackParameter.LogName.GetDescription()}-{trackInfo.CustomNumber}");
                        trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new NotGeneratedTrackError(), toJson(myResult.Result)));
                    }
                }
                else
                {
                    logger.LogError("track error" + myResult.exception.Message);
                    trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, myResult.exception, toJson(myResult.exception)));
                }
            }
        }
        return new TrackResultList(trackResults);
    }

    public override bool PaperType { get; set; } = true;


   
    public override bool IsUrl { get; set; } = true;

    //支持批量下载
    public override async Task<printResultList> Print(MultiPrint multiPrint)
    {
        List<printResult> printResults = new List<printResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(multiPrint.Parameter);

        if (multiPrint.PaperType is not null && Enum.IsDefined(typeof(PaperTypeEnum), multiPrint.PaperType))
        {
            Enum.TryParse<PaperTypeEnum>(multiPrint.PaperType, out var labelPaperType2);
            var s = labelPaperType2;
        }

        var express = multiPrint.printParams?.Select(s => s.Express!).ToList() ?? Array.Empty<string>().ToList();
        if (multiPrint.PaperType is not null && Enum.IsDefined(typeof(PaperTypeEnum), multiPrint.PaperType) && Enum.TryParse<PaperTypeEnum>(multiPrint.PaperType, out var labelPaperType))
        {
           
            if (Platform == Platforms.CNE_HZGJ)
            {
                var paperTypeArry = labelPaperType.ToString().Split("_");
                var modelName = "标准" + paperTypeArry[0];
                PrintLabelRequest printLabelRequest = new PrintLabelRequest(parameter, express, modelName, Platform);
                var response = await Request.SendPrint(printLabelRequest);
                string res = toJson(response);
                if (response.Success)
                {
                    var stream = await response.Result.Content.ReadAsStreamAsync();
                    printResults.Add(new printResult(express, stream, "application/pdf", ".pdf", res));
                    return new printResultList(printResults);
                }
                else
                {
                    return new printResultList(response.exception, res);
                }
            }
            else
            {
                PrintNewLabelRequest newRequest = new PrintNewLabelRequest(parameter, labelPaperType.ToString(), express, Platform);
                var response = await Request.SendPrint(newRequest);
                string res = toJson(response);
                if (response.Success)
                {
                    var s= await response.Result.Content.ReadAsStringAsync();
                    var stream = await response.Result.Content.ReadAsStreamAsync();
                    printResults.Add(new printResult(express, stream, "application/pdf", ".pdf",newRequest.Url, res));
                    return new printResultList(printResults);
                }
                else
                {
                    return new printResultList(response.exception, res);
                }
            }
        }
        else
        {
            logger.LogWarning($"{multiPrint.LogName.GetDescription()}-{multiPrint.PaperType} mismatched error ");
            return new printResultList(new ParameterError(PrintSettingEnum.PaperType, multiPrint.PaperType??"暂无"));
        }
    }

    public override async Task<PrintParamResult> GetPrint(FileTypeParameter fileTypeParameter)
    {
        Parameter parameter = Helpers.GetParamType<Parameter>(fileTypeParameter.Parameter);
        List<CommonResult> results = new List<CommonResult>();
        foreach (PaperTypeEnum paperType in Enum.GetValues(typeof(PaperTypeEnum)))
        {
            results.Add(new CommonResult(paperType.ToString(), paperType.GetDescription()));
        }
        return await Task.FromResult(new PrintParamResult(ContentType, FileType, PaperType, new(), new(), results));
    }
}
