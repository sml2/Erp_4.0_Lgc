namespace ERP.Services.Api.Logistics.Detail;
using Enums;
using Models.Api.Logistics;
using Models.Api.Logistics.HL;
using AmazonService = DB.Orders.Order;

using Exceptions.Logistics;
using Enums.Logistics;

public class HuaLei : Base<Parameter>
{
    public string logName = "";
    public HuaLei(Platforms platforms, ILogger<HuaLei> logger, LogisticClient request, IServiceProvider serviceProvider) : base(platforms, logger, request, serviceProvider)
    {
        logName = base.Platform.GetDescription();
    }

    public override async Task<CommonResultList> GetShipTypes(ShipTypeParameter shipTypeParam)
    {
        List<CommonResult> CommonResults = new List<CommonResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(shipTypeParam.Parameter);
        ShipTypeRequest resquest = new ShipTypeRequest(parameter);
        var response = await Request.Send<List<ShipTypeResponse>>(resquest);
        if (response.Success && response.Result.IsNotNull())
        {
            foreach (var r in response.Result)
            {
                CommonResults.Add(new CommonResult(r.product_id, $"{r.product_shortname}({r.express_type})"));
            }
            return new CommonResultList(CommonResults);
        }
        else
        {
            return new CommonResultList(response.exception);
        }
    }

    public async Task<UserAccountInfo?> GetUserInfo(Parameter p)
    {
        UserRequest request = new UserRequest(p);
        var responsse = await Request.Send<UserAccountInfo>(request);
        if (responsse.Success && responsse.Result.IsNotNull())
        {
            if (responsse.Result.IsNull() || responsse.Result.ack == "false") return null;
            return responsse.Result;
        }
        else
        {
            logger.LogError($"HaLei GetUserInfo is null");
            return null;
        }
    }

    public override async Task<ReportResultList> Send(SendParameter sendParameter)
    {
        List<AddOrderRequest> requests = new List<AddOrderRequest>();
        List<ReportResult> results = new List<ReportResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(sendParameter.Parameter);
        SenderInfo senderInfo = sendParameter.SenderInfo;
        List<OrderInfo> orderLists = sendParameter.OrderInfos;
        UserAccountInfo? userAccountInfo = await GetUserInfo(parameter);
        if (userAccountInfo is null)
        {
            logger.LogError($"HaLei UserInfo is null");
            return new ReportResultList(new AccountError());
        }
        foreach (var orderInfo in orderLists)
        {
            ExtParameter Ext = Helpers.GetParamType<ExtParameter>(orderInfo.ext);
            AddOrderRequest request = new AddOrderRequest(parameter, senderInfo, orderInfo, userAccountInfo, Ext);
            List<ErrorMember> errMsg = request.IsValid<AddOrderRequest>();
            errMsg.AddRange(Ext.IsValid<ExtParameter>());
            if (errMsg.Count <= 0)
            {
                requests.Add(request);
            }
            else
            {
                logger.LogWarning(ParameterErrorType.ParameterFormat.GetDescription(), toMsg(errMsg));
                results.Add(new ReportResult(request.Id, request.orderNo, new ParameterError(ParameterErrorType.ParameterFormat, toMsg(errMsg))));
                continue;
            }
        }
        if (requests.Count > 0)
        {
            foreach (var request in requests)
            {
                
                var response = await Request.Send<AddOrderResponse>(request);
                string res = toJson(response);
                if (response.Success && response.Result.IsNotNull())
                {
                    var myResult = response.Result;
                    if (myResult.ack.Contains("true", StringComparison.CurrentCultureIgnoreCase))
                    {
                        results.Add(new ReportResult(request.Id, myResult.reference_number, myResult.order_id, myResult.tracking_number, "", 0,
                              orderLists.Where(x => x.id == request.Id).Select(x => x.ext).FirstOrDefault()??string.Empty, res));
                    }
                    else
                    {
                        var msg = myResult.GetDecodedMessage();
                        logger.LogWarning($"{sendParameter.LogName.GetDescription()},{msg}");
                        results.Add(new ReportResult(request.Id, request.orderNo, new UnKnownError(msg), res));
                    }
                }
                else
                {
                    results.Add(new ReportResult(request.Id, request.orderNo, response.exception, res));
                }
            }
        }
        return new ReportResultList(results);
    }


    //private async Task<double> GetFee(ExtParameter Extparameter, Parameter parameter, string countryCode)
    //{
    //    GetFeeRequest getFeeRequest = new GetFeeRequest(parameter, Extparameter.weight, countryCode);
    //    var response = await Request.Send<GetFeeResponse>(getFeeRequest);
    //    if (response.Success && response.Result.IsNotNull())
    //    {
    //        return Convert.ToDouble(response.Result.total_amount);
    //    }
    //    return 0;
    //}

    private async Task<Tuple<string, Exception>> GetTrackNum(Parameter parameter, string LogictisNumber)
    {
        GetTRackNumRequest request = new GetTRackNumRequest(parameter, LogictisNumber);
        var response = await Request.Send<GetTrackNumResponse>(request);
        if (response.Success && response.Result.IsNotNull())
        {
            return Tuple.Create(response.Result.order_serveinvoicecode, new Exception(""));
        }
        else
        {
            return Tuple.Create("", response.exception);
        }
    }

    public override async Task<TrackResultList> Track(TrackParameter trackParameter)
    {
        List<TrackResult> trackResults = new List<TrackResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(trackParameter.Parameter);
        foreach (var trackInfo in trackParameter.TrackOrderInfoLst)
        {
            List<TrackInfo> trackInfos = new List<TrackInfo>();
            ////获取跟踪号
            //if (trackInfo.TrackNumber.IsNullOrEmpty())
            //{
            //    var tuple = await GetTrackNum(parameter, trackInfo.LogictisNumber);
            //    if (!tuple.Item1.IsNullOrWhiteSpace())
            //    {
            //        trackInfo.TrackNumber = tuple.Item1;
            //    }
            //    else
            //    {
            //        logger.LogWarning("hl GetTrackNum error");
            //        trackResults.Add(new TrackResult(trackInfo.CustomNumber, tuple.Item2));
            //    }
            //}
            TrackRequest request = new TrackRequest(parameter, trackInfo.CustomNumber);
            var response = await Request.Send<List<TrackResponse>>(request);
            string res = toJson(response);
            if (response.Success && response.Result.IsNotNull())
            {
                var myResult = response.Result[0];
                if (myResult.ack.Contains("true", StringComparison.CurrentCultureIgnoreCase))
                {
                    if (myResult.data is not null && myResult.data.Count > 0)
                    {
                        var d = myResult.data[0];
                        if (trackInfo.TrackNumber.IsNullOrWhiteSpace()) trackInfo.TrackNumber = d.trackingNumber;

                        string TrackInfo = "";
                        if (d.trackDetails.IsNotNull() && d.trackDetails.Count > 0)
                        {
                            var TrackDetails = d.trackDetails;
                            TrackDetails.Reverse();
                            foreach (var v in TrackDetails)
                            {
                                trackInfos.Add(new TrackInfo(v.track_date, "", v.track_location, v.track_content));
                                string Message = $"Time: {v.track_date}|Location: {v.track_location}|Track Description: {v.track_content}";
                                TrackInfo += Message + "\r\n";
                            }
                        }
                        else
                        {
                            trackInfos.Add(new TrackInfo(d.trackDate, "", d.trackLocation, d.trackContent));
                            string Message = $"Time: {d.trackDate}|Location: {d.trackLocation}|Track Description: {d.trackContent}";
                            TrackInfo += Message + "\r\n";
                        }
                        trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, trackInfo.LogictisNumber, trackInfo.TrackNumber,
                            TrackInfo, "", Enums.Logistics.States.TRANSITING, trackInfos, res));
                    }
                    else
                    {
                        //尚未生成
                        logger.LogWarning($"{trackInfo.CustomNumber}-Not Generated Track Error");
                        //trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new NotGeneratedTrackError(), res));
                        return new TrackResultList(new NotGeneratedTrackError());
                    
                    }
                }
                else
                {
                    var msg = Sy.Text.Encode.Url_Decode(myResult.message);
                    logger.LogError($"{trackInfo.CustomNumber}-{msg}");
                    //trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new UnKnownError($"{msg}"), res));
                    return new TrackResultList(new UnKnownError($"{msg}"));
                }
            }
            else
            {
                trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, response.exception, res));
                return new TrackResultList(new UnKnownError($"{response.exception}"));
            }
        }
        return new TrackResultList(trackResults);
    }

    public override bool IsUrl { get; set; } = true;

    public override bool ContentType { get; set; } = true;

    //单个
    public override async Task<printResultList> Print(MultiPrint multiPrint)
    {
        List<printResult> printResults = new List<printResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(multiPrint.Parameter);
        string labelFormat = "";
        //FileTypeEnum
        if (multiPrint.FileType is not null && Enum.IsDefined(typeof(FileTypeEnum), multiPrint.FileType) && Enum.TryParse<FileTypeEnum>(multiPrint.FileType, out var labelFileType))
        {
            labelFormat = labelFileType.GetDescription();
        }
        else
        {
            logger.LogWarning($"{multiPrint.LogName.GetDescription()}-{multiPrint.FileType} mismatched error ");
            return new printResultList(new ParameterError(PrintSettingEnum.FileType, multiPrint.FileType?? "Empty 数据"));
        }

        if (multiPrint.ContentType.IsNullOrWhiteSpace())
        {
            logger.LogWarning($"{multiPrint.LogName.GetDescription()}-{multiPrint.ContentType} is null");
            return new printResultList(new ParameterError(PrintSettingEnum.ContentType, multiPrint.ContentType ?? "暂无标签"));
        }
        if (multiPrint.printParams is not null)
        {
            foreach (var printInfo in multiPrint.printParams)
            {
                PrintRequest printRequest = new PrintRequest(parameter, multiPrint.ContentType ?? "暂无标签", printInfo.Express ?? "暂无追踪号");
                var response = await Request.SendPrint(printRequest);
                string res = toJson(response);
                if (response.Success && response.Result.IsNotNull())
                {
                    var stream = await response.Result.Content.ReadAsStreamAsync();
                    printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, stream, "application/pdf", ".pdf", printRequest.Url, res));
                }
                else
                {
                    printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, response.exception, res));
                }
            }
        }
        return new printResultList(printResults);
    }

    public override bool EstimatePriceEnabled { get; set; } = true;
    public override async Task<EstimatePriceResult> EstimatePrice(PriceParameter priceParameter)
    {
        var logName = priceParameter.LogName.GetDescription();
        Parameter parameter = Helpers.GetParamType<Parameter>(priceParameter.Parameter);
        ExtParameter Ext = Helpers.GetParamType<ExtParameter>(priceParameter.ext);
        if (Ext.weight <= 0)
        {
            logger.LogError($"{logName} EstimatePrice Ext.Weight less then 0");
            return new EstimatePriceResult(new ParameterError(ParameterErrorType.WeightNull));
        }
        if (priceParameter.CountryCode is null)
        {
            logger.LogError($"{logName} EstimatePrice CountryCode is null");
            return new EstimatePriceResult(new ParameterError(ParameterErrorType.CountryCodeNull));
        }

        GetPriceRequest request = new GetPriceRequest(parameter, Ext.weight, Ext.length, Ext.width, Ext.height, Ext.ShipMethod, priceParameter.CountryCode);
        var response = await Request.Send<List<GetFeeResponse>>(request);
        string res = toJson(response);
        if (response.Success && response.Result.IsNotNull())
        {
            List<CommonResult> datas = new List<CommonResult>();
            foreach (var r in response.Result)
            {
                datas.Add(new CommonResult(r.product_name,$"The cost of shipping method [{r.product_name}] is as follows:\r\nfreight_amount:{r.freight_amount}\r\noil_amount:{r.oil_amount}\r\nother_amount:{r.other_amount}\r\ntotal_amount:{r.total_amount}"));
            }
            return new EstimatePriceResult(datas, res);
        }
        else
        {
            logger.LogError($"EstimatePrice error");
            return new EstimatePriceResult(response.exception, res);
        }
    }

    public override bool FileType { get; set; } = true;


    public override async Task<PrintParamResult> GetPrint(FileTypeParameter fileTypeParameter)
    {
        List<CommonResult> fileTyperesults = new List<CommonResult>();
        foreach (FileTypeEnum fileType in Enum.GetValues(typeof(FileTypeEnum)))
        {
            fileTyperesults.Add(new CommonResult(fileType.ToString(), fileType.GetDescription()));
        }
        List<CommonResult> contentTypeResults = new List<CommonResult>();
        var response = await LoadCbContentType(fileTypeParameter);
        if (response.Success)
        {
            contentTypeResults = response.CommonResults;
        }
        else
        {
            return new PrintParamResult(response.exception);
        }

        return new PrintParamResult(ContentType, FileType, PaperType, contentTypeResults, fileTyperesults, new());
    }

    /// <summary>
    /// 获取纸张类型
    /// </summary>
    /// <param name="fileTypeParameter"></param>
    /// <returns></returns>
    public async Task<CommonResultList> LoadCbContentType(FileTypeParameter fileTypeParameter)
    {
        List<CommonResult> contentTypes = new List<CommonResult>();
        string logName = fileTypeParameter.LogName.GetDescription();
        Parameter parameter = Helpers.GetParamType<Parameter>(fileTypeParameter.Parameter);
        ContentTypeRequest request = new ContentTypeRequest(parameter);
        var response = await Request.Send<List<LabelTypeItem>>(request);
        if (response.Success && response.Result.IsNotNull())
        {
            response.Result.ForEach(r => contentTypes.Add(new CommonResult(r.format_id, r.PrintData)));
            return new CommonResultList(contentTypes);
        }
        else
        {
            logger.LogError($"HL LoadContentType error");
            return new CommonResultList(response.exception);
        }
    }
}