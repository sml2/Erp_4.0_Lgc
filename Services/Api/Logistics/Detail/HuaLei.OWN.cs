﻿
using ERP.Enums;
using ERP.Models.Api.Logistics;
using ERP.Models.Api.Logistics.HL;

namespace ERP.Services.Api.Logistics.Detail
{
    public class HuaLei_OWN : HuaLei
    {       
        public HuaLei_OWN(ILogger<HuaLei_OWN> logger, LogisticClient request, IServiceProvider serviceProvider) :
            base(Platforms.HuaLei_OWN, logger, request, serviceProvider)
        {
        }
       



    }
}
