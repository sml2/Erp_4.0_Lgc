namespace ERP.Services.Api.Logistics.Detail;
using AmazonService = DB.Orders.Order;
using Models.Api.Logistics;
using Models.Api.Logistics.WANB;
using static Models.Api.Logistics.WANB.ExtParameter;
using Enums;
using Exceptions.Logistics;
using static Models.Api.Logistics.WANB.ResponseTrace;

public class WANB : Base<Parameter>
{
    protected string logName = "";
    public WANB(ILogger<WANB> logger, LogisticClient request, IServiceProvider serviceProvider) : base(Platforms.WANB, logger, request, serviceProvider)
    {
        logName = base.Platform.GetDescription();
    }


    public override async Task<CommonResultList> GetShipTypes(ShipTypeParameter shipTypeParam)
    {
        var logName = shipTypeParam.LogName.GetDescription();
        List<CommonResult> shipTypes = new List<CommonResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(shipTypeParam.Parameter);
        ShippingMethodRequest request = new ShippingMethodRequest(parameter);
        var response = await Request.Send<ResponseBaseData<Cls_ParcelShippingMethod>>(request);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            if (myResult.Succeeded)
            {
                myResult.Data.ShippingMethods.ForEach(r => shipTypes.Add(new CommonResult(r.Code, r.Name)));
                return new CommonResultList(shipTypes);
            }
            else
            {
                logger.LogError($"{logName}-GetShipTypes-{myResult.Error.Code}:{myResult.Error.Message}");
                return new CommonResultList(new UnKnownError($"{myResult.Error.Code}:{myResult.Error.Message}"));
            }
        }
        else
        {
            return new CommonResultList(response.exception);
        }
    }



    public override async Task<ReportResultList> Send(SendParameter sendParameter)
    {
        List<AddOrderRequest> addOrderRequests = new List<AddOrderRequest>();
        List<ReportResult> results = new List<ReportResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(sendParameter.Parameter);
        SenderInfo senderInfo = sendParameter.SenderInfo;
        List<OrderInfo> orderLists = sendParameter.OrderInfos;
        foreach (var orderinfo in orderLists)
        {
            ExtParameter ext = Helpers.GetParamType<ExtParameter>(orderinfo.ext);
            AddOrderRequest orderRequest = new AddOrderRequest(parameter, senderInfo, orderinfo, ext);
            List<ErrorMember> errMsg = ext.IsValid<ExtParameter>();
            errMsg.AddRange(orderRequest.IsValid<AddOrderRequest>());
            if (errMsg.Count <= 0)
            {
                addOrderRequests.Add(orderRequest);
            }
            else
            {
                logger.LogWarning(ParameterErrorType.ParameterFormat.GetDescription(), toMsg(errMsg));
                results.Add(new ReportResult(orderinfo.id, orderinfo.OrderNo, new ParameterError(ParameterErrorType.ParameterFormat, toMsg(errMsg))));
                continue;
            }
        }
        if (addOrderRequests.Count > 0)
        {
            foreach (var request in addOrderRequests)
            {
                var response = await Request.Send<ResponseBaseData<ResponseAddOrder>>(request);
                string res = toJson(response);
                if (response.Success && response.Result.IsNotNull())
                {
                    if (response.Result.Succeeded)
                    {
                        var myResult = response.Result;
                        results.Add(new ReportResult(request.Id, myResult.Data.ReferenceId, myResult.Data.ProcessCode,
                            myResult.Data.TrackingNumber, "", 0,
                            orderLists.Where(x => x.id == request.Id).Select(s => s.ext).FirstOrDefault()??"", res));
                    }
                    else
                    {
                        string msg = response.Result.Error.Code + ":" + response.Result.Error.Message + ";";
                        logger.LogError($"{request.orderNo}:{msg}");
                        results.Add(new ReportResult(request.Id, request.orderNo, new UnKnownError($"{request.orderNo}:{msg}"), res));
                    }
                }
                else
                {
                    results.Add(new ReportResult(request.Id, request.orderNo, response.exception, res));
                }
            }
        }
        return new ReportResultList(results);
    }

    /// <summary>
    /// trackingNumber的值可为处理号、跟踪号或者客单号，客户订单号："waimaomvp" + orderInfo.OrderNo;
    /// </summary>
    /// <param name="trackParameter"></param>
    /// <returns></returns>
    public override async Task<TrackResultList> Track(TrackParameter trackParameter)
    {
        var logName = trackParameter.LogName.GetDescription();
        List<TrackResult> trackResults = new List<TrackResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(trackParameter.Parameter);
        foreach (var trackInfo in trackParameter.TrackOrderInfoLst)
        {           
            List<TrackInfo> trackInfos = new List<TrackInfo>();
            var customNumber = "waimaomvp" + trackInfo.CustomNumber;
            RequestTrace requestTrace = new RequestTrace(parameter, customNumber);
            var response = await Request.Send<ResponseBaseData<ResponseTrace>>(requestTrace);
            string res = toJson(response);
            if (response.Success && response.Result.IsNotNull())
            {
                var myResult = response.Result;
                if (myResult.Succeeded)
                {
                    if (myResult.Data.IsNotNull())
                    {
                        if (!"Unknown".Equals(myResult.Data.Match))
                        {
                            string TrackInfo = "";
                            var TrackData = myResult.Data.TrackPoints;
                            foreach (var v in TrackData)
                            {
                                trackInfos.Add(new TrackInfo(v.Time, v.Status, v.Location, v.Content));
                                string Message = $"Time: {v.Time}|Status:{v.Status}|Location: {v.Location}|Track Description: {v.Content}";
                                TrackInfo += Message + "\r\n";
                            }
                            trackInfo.TrackNumber = trackInfo.TrackNumber.IsNullOrWhiteSpace() ? myResult.Data.TrackingNumber : trackInfo.TrackNumber;
                            trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, trackInfo.LogictisNumber, trackInfo.TrackNumber,
                                TrackInfo, myResult.Data.Status, GetStatus(myResult.Data.Status), trackInfos, res));
                        }
                        else
                        {
                            logger.LogError($"{logName}-track error");
                            trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new UnKnownError($"track unknown"),res));
                        }
                    }
                    else
                    {
                        //尚未生成                   
                        logger.LogWarning($"{logName} getTrack Not Generated Track Error,CustomNumber:{trackInfo.CustomNumber}");
                        trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new NotGeneratedTrackError(),res));
                    }
                }
                else
                {
                    logger.LogError($"{logName}-track error-{myResult.Error.Code}:{myResult.Error.Message}");
                    trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new UnKnownError($"{logName}-track error-{myResult.Error.Code}:{myResult.Error.Message}"),res));
                }
            }
            else
            {
                trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, response.exception,res));
            }
        }
        return new TrackResultList(trackResults);
    }


    private Enums.Logistics.States GetStatus(string trackingStatus)
    {
        Enums.Logistics.States states = Enums.Logistics.States.TRANSITING;
        if (Enum.IsDefined(typeof(EnumStatus), trackingStatus) && Enum.TryParse<EnumStatus>(trackingStatus, out var logStates))
        {
            switch (logStates)
            {
                case EnumStatus.DataReceived: //收到数据
                    states = Enums.Logistics.States.RECEIVED;
                    break;
                case EnumStatus.InTransit: //运输途中
                    states = Enums.Logistics.States.TRANSITING;
                    break;
                case EnumStatus.DeliveryReady: //到达待取
                    states = Enums.Logistics.States.WAITTOPICKUP;
                    break;
                case EnumStatus.DeliveryTried: //尝试投递失败。部分渠道会尝试投递数次
                    states = Enums.Logistics.States.DELIVERYFAILED;
                    break;
                case EnumStatus.Delivered: //已妥投
                    states = Enums.Logistics.States.WAITTOPICKUP;
                    break;
                case EnumStatus.DeliveryFailed: //投递失败。 地址问题、尝试投递数次均失败、收件人搬家或拒收等
                    states = Enums.Logistics.States.DELIVERYFAILED;
                    break;
                case EnumStatus.Returned: //已退回
                    states = Enums.Logistics.States.RETURNING;
                    break;
                case EnumStatus.Lost: //包裹遗失
                    states = Enums.Logistics.States.LOST;
                    break;
            }
        }
        else
        {
            Console.WriteLine("Wanb get Track State error:" + trackingStatus);
        }
        return states;
    }
   

    /// <summary>
    /// 批量打印，此接口限定只能对同一发货渠道的包裹进行批量打印！
    /// </summary>
    /// <param name="trackParameter"></param>
    /// <returns></returns>
    public override async Task<printResultList> Print(MultiPrint multiPrint)
    {
        List<printResult> printResults = new List<printResult>();      
        Parameter parameter = Helpers.GetParamType<Parameter>(multiPrint.Parameter);
        if (multiPrint.printParams is not null)
        {
            foreach (var printInfo in multiPrint.printParams)
            {
                PrintRequest printRequest = new PrintRequest(parameter, printInfo.Express?? "追踪号 is Empty");
                var response = await Request.SendPrint(printRequest);
                string res = toJson(response);
                if (response.Success && response.IsNotNull())
                {
                    var stream = await response.Result.Content.ReadAsStreamAsync();
                    printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, stream, "application/pdf", ".pdf", res));
                }
                else
                {
                    logger.LogError($"{printInfo.CustomNumber} print error");
                    printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, response.exception, res));
                }
            }
        }
        return new printResultList(printResults);
    }

    public override bool WarehouseEnabled { get; set; } = true;

    public override async Task<CommonResultList> GetWarehouseCode(BaseParameter baseParameter)
    {
        Parameter p = Helpers.GetParamType<Parameter>(baseParameter.Parameter);
        WarehouseRequest warehouseRequest = new WarehouseRequest(p);
        var response = await Request.Send<ResponseBaseData<Cls_Warehouses>>(warehouseRequest);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult=response.Result;
            if (myResult.Succeeded)
            {
                
                List<CommonResult> shipTypes = new List<CommonResult>();
                myResult.Data.Warehouses.ForEach(r => shipTypes.Add(new CommonResult(r.Code, r.Name)));
                return new CommonResultList(shipTypes);
            }
            else
            {
                logger.LogError($"GetWarehouseCode  error:{myResult.Error.Code}-{myResult.Error.Message}");
                return new CommonResultList(new UnKnownError($"{myResult.Error.Code}-{myResult.Error.Message}"));            
            }
        }
        else
        {
            return new CommonResultList(response.exception);
        }
    }

    public override bool CancleEnabled { get; set; } = true;

    /// <summary>
    /// 此过程一般会在1分钟内完成,需要重复调用此接口，通过 Status 获知取消状态
    /// </summary>
    /// <param name="cancleParameter"></param>
    /// <returns></returns>
    public override async  Task<CancelResult> CancleOrder(CancleParameter cancleParameter)
    {
        Parameter parameter = Helpers.GetParamType<Parameter>(cancleParameter.Parameter);
        InterceptRequest request = new InterceptRequest(parameter, cancleParameter.LogictisNumber);
        var response = await Request.Send<ResponseBaseData<InterceptResponse>>(request);
        string res = toJson(response);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            if (myResult.Succeeded && myResult.Data.IsNotNull())
            {
               if(myResult.Data.Status.Contains("Accepted", StringComparison.CurrentCultureIgnoreCase))
                {
                    return new CancelResult(res);//拦截成功
                }
                else if (myResult.Data.Status.Contains("Rejected", StringComparison.CurrentCultureIgnoreCase))
                {
                    return new CancelResult(new UnKnownError("截件失败"),res);
                }
                else
                {
                  return await this.CancleOrder(cancleParameter);
                }
            }
            else
            {
                return new CancelResult(new UnKnownError(myResult.Error.Code+":"+myResult.Error.Message), res);
            }
        }
        else
        {
            logger.LogError($"Cancle error");
            return new CancelResult(response.exception, res);
        }
    }
}