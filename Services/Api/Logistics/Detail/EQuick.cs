namespace ERP.Services.Api.Logistics.Detail;

using Enums;
using Exceptions.Logistics;
using Models.Api.Logistics;
using Models.Api.Logistics.EQuick;
using Services.Api.Logistics;
using AmazonService = DB.Orders.Order;
public class EQuick : Base<Parameter>
{
    protected string logName = "";
    public EQuick(ILogger<EQuick> logger, LogisticClient request, IServiceProvider serviceProvider) : base(Platforms.EOC, logger, request, serviceProvider)
    {
        logName = base.Platform.GetDescription();
    }
    public override async Task<CommonResultList> GetShipTypes(ShipTypeParameter shipTypeParam)
    {
        List<CommonResult> shipTypes = new List<CommonResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(shipTypeParam.Parameter);
        ShipTypeRequest shipTypeRequest = new ShipTypeRequest(parameter);
        var response = await Request.Send<List<ShipTypeResponse.ShipType>>(shipTypeRequest);
        if (response.Success && response.Result.IsNotNull())
        {
            var result = response.Result;
            if (result.Count > 0)
            {
                result.ForEach(r => shipTypes.Add(new CommonResult(r.EOCQuickType, r.EOCQuickTypeName)));
                return new CommonResultList(shipTypes);
            }
            else
            {
                return new CommonResultList(shipTypes);
            }
        }
        else
        {
            return new CommonResultList(response.exception);
        }
    }

    public override async Task<ReportResultList> Send(SendParameter sendParameter)
    {
        List<ReportResult> results = new List<ReportResult>();
        List<AddOrderRequest> datas = new List<AddOrderRequest>();
        Parameter parameter = Helpers.GetParamType<Parameter>(sendParameter.Parameter);
        SenderInfo senderInfo = sendParameter.SenderInfo;
        List<OrderInfo> orderLists = sendParameter.OrderInfos;
        foreach (var orderinfo in orderLists)
        {
            ExtParameter Ext = Helpers.GetParamType<ExtParameter>(orderinfo.ext);
            AddOrderRequest data = new AddOrderRequest(parameter, senderInfo, orderinfo, Ext);
            List<ErrorMember> errMsg = Ext.IsValid<ExtParameter>();
            errMsg.AddRange(data.IsValid<AddOrderRequest>());
            if (errMsg.Count <= 0)
            {
                datas.Add(data);
            }
            else
            {
                logger.LogWarning(ParameterErrorType.ParameterFormat.GetDescription(), toMsg(errMsg));
                results.Add(new ReportResult(orderinfo.id, orderinfo.OrderNo, new ParameterError(ParameterErrorType.ParameterFormat, toMsg(errMsg))));
                continue;
            }
        }
        if (datas.Count > 0)
        {
            foreach (var request in datas)
            {
                var response = await Request.Send<AddOrderResponse>(request);
                string res = toJson(response);
                if (response.Success && response.Result.IsNotNull())
                {
                    var myResult = response.Result;
                    if (myResult.ShipmentCompleted.ReturnValue == -1)
                    {
                        results.Add(new ReportResult(request.Id, myResult.BookingShipment.BookingCustRefNo, myResult.ShipmentCompleted.EquickWBNo, myResult.BookingShipment.TrackTraceLabelNo,
                           myResult.BookingShipment.GoodsCurrency, 0, orderLists.Where(x => x.id == request.Id).Select(s => s.ext).FirstOrDefault()??"", res));
                    }
                    else
                    {
                        string msg = myResult.ShipmentCompleted.ReturnMessage + ";";
                        logger.LogError($"{request.orderNo}-{msg}");
                        results.Add(new ReportResult(request.Id, request.orderNo, new UnKnownError($"{msg}"), res));
                    }
                }
                else
                {
                    results.Add(new ReportResult(request.Id, request.orderNo, response.exception, res));
                }
            }
        }
        return new ReportResultList(results);
    }

    private async Task<TrackPrice> GetFeeInfo(Parameter parameter, string customNum, string? logNumber, string trackNum, Platforms platforms, int waybillId)
    {
        EOCBookingFreightRequest request = new EOCBookingFreightRequest(parameter, logNumber??string.Empty);
        var response = await Request.Send<GetFeeResponse>(request);
        string res = toJson(response);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            if (myResult.BookingFreightCompleted.ReturnValue == -1)
            {
                return new TrackPrice(waybillId, customNum, logNumber, trackNum, "CNY", myResult.BookingFreight.TotFreight, myResult.BookingFreight.WhtFactWht.ToString(), "", res);
            }
            else if (myResult.BookingFreightCompleted.ReturnValue == 0)
            {
                //尚未生成
                logger.LogError($"{customNum} Not Generated Price Error");
                return new TrackPrice(waybillId, customNum, new NotGeneratedPriceError(), res);
            }
            else
            {
                string msg = myResult.BookingFreightCompleted.ReturnMessage;
                logger.LogError($"{platforms.GetDescription()}-{customNum}-GetFee-{msg}", $"logNumber:{logNumber}");
                return new TrackPrice(waybillId, customNum, new UnKnownError($"GetFee-{msg}"), res);
            }
        }
        else
        {
            return new TrackPrice(waybillId, customNum, response.exception, res);
        }
    }



    public override async Task<WaybillPrice> GetFee(WaybillInfos wayBillParameter)
    {
        Parameter parameter = Helpers.GetParamType<Parameter>(wayBillParameter.Parameter);
        var price = await GetFeeInfo(parameter, wayBillParameter.CustomNumber, wayBillParameter.LogictisNumber,
wayBillParameter.TrackNumber, wayBillParameter.LogName,wayBillParameter.waybillId);
        return new WaybillPrice(price);
    }



    public string GetStatus(int TraceKind)
    {
        string msg = "";
        // >=200 – 国外部分 >=100<=199 – 国内部分 特别关注： 299 – 妥投 298 – 派送异常
        if (TraceKind >= 200)
        {
            msg = "国外部分";
            if (TraceKind == 299)
            {
                msg = "妥投";
            }
            else if (TraceKind == 298)
            {
                msg = "派送异常";
            }
        }
        else if (TraceKind >= 100 && TraceKind <= 199)
        {
            msg = "国内部分";
        }
        return msg;
    }


    public override async Task<TrackResultList> Track(TrackParameter trackParameter)
    {
        List<TrackResult> trackStates = new List<TrackResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(trackParameter.Parameter);
        foreach (var trackInfo in trackParameter.TrackOrderInfoLst)
        {
            TrackPrice price = new TrackPrice();
            List<TrackInfo> trackInfos = new List<TrackInfo>();
            TrackRequest request = new TrackRequest(parameter, trackInfo.LogictisNumber ?? string.Empty, trackInfo.TrackNumber ?? string.Empty);
            var myResult = await Request.Send<TrackResponse>(request);
            string res = toJson(myResult);
            if (myResult.Success && myResult.Result.IsNotNull())
            {
                var response = myResult.Result;
                string TrackInfo = "", trackNum = "";
                if (response.datas.IsNotNull() && response.datas.Count > 0)
                {
                    foreach (var v in response.datas)
                    {
                        trackInfos.Add(new TrackInfo(v.TraceDateTime, GetStatus(v.TraceKind),$"{v.TraceCountry}-{v.TraceLocation}", v.TraceContent));
                        trackNum = v.DestinationWBNo;        //目的地单号既跟踪号
                        string Message = $"Time: {v.TraceDateTime}|state:{GetStatus(v.TraceKind)}|Location: {v.TraceCountry}-{v.TraceLocation}|Track Description: {v.TraceContent}";
                        TrackInfo += Message + "\r\n";
                    }
                    //if (trackInfo.Price == 0)
                    //{
                        price = await GetFeeInfo(parameter, trackInfo.CustomNumber, trackInfo.LogictisNumber, trackNum, trackParameter.LogName, trackInfo.waybillId);
                  //}
                    trackStates.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, trackInfo.LogictisNumber, trackNum, TrackInfo, "", Enums.Logistics.States.TRANSITING, price, trackInfos, res));
                }
                else
                {
                    //尚未生成物流信息
                    logger.LogWarning($"NotGenerated Track Error:{trackInfo.CustomNumber}");
                    trackStates.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new NotGeneratedTrackError(), res));
                }
            }
            else
            {
                trackStates.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, myResult.exception, res));
            }
        }
        return new TrackResultList(trackStates);
    }

    //单个
    public override async Task<printResultList> Print(MultiPrint multiPrint)
    {
        List<printResult> printResults = new List<printResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(multiPrint.Parameter);
        if (multiPrint.printParams is not null)
        {
            foreach (var printInfo in multiPrint.printParams)
            {
                EOCQuickLabelRequest request = new EOCQuickLabelRequest(parameter, printInfo.Express ?? string.Empty);
                var response = await Request.Send<EOCQuickLabelResponse>(request);
                string res = toJson(response);
                if (response.Success && response.Result.IsNotNull())
                {
                    var myResult = response.Result;
                    if (myResult.QuickLabel.IsNotNull())
                    {
                        HttpResponseMessage httpResponseMessage = new HttpResponseMessage(System.Net.HttpStatusCode.OK);
                        byte[] byteData = System.Text.Encoding.Default.GetBytes(myResult.QuickLabel.LabelData);
                        using (var content = new MemoryStream(byteData))
                        {
                            printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, content, "application/pdf", ".pdf", res));
                        }
                    }
                    else
                    {
                        string msg = $"{multiPrint.LogName.GetDescription()}-{printInfo.CustomNumber}-PrintBinLabel-{myResult.QuickLabelCompleted.ReturnMessage}";
                        logger.LogError(msg, $"LogictisNumber:{printInfo.Express}");
                        printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, new UnKnownError($"print error"), res));
                    }
                }
                else
                {
                    printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, response.exception, res));
                }
            }
        }
        return new printResultList(printResults);
    }


    public override bool CancleEnabled { get; set; } = true;

    public override async Task<CancelResult> CancleOrder(CancleParameter cancleParameter)
    {
        Parameter parameter = Helpers.GetParamType<Parameter>(cancleParameter.Parameter);
        InterceptRequest request = new InterceptRequest(parameter, cancleParameter.LogictisNumber);
        var response = await Request.Send<InterceptResponse>(request);
        string res = toJson(response);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            if (myResult.ReturnValue ==0)
            {
                return await this.CancleOrder(cancleParameter);
            }
            else
            {
                //未在物流平台创建运单或者物流平台那边已删除，接口返回找不到运单，导致平台运单无法取消
                if (myResult.ReturnValue == 1 && myResult.ReturnMessage.Contains("没有权限不能拦截"))
                {
                    return new CancelResult(res);
                }
                
                return new CancelResult(new UnKnownError(myResult.ReturnMessage), res);
            }
        }
        else
        {
            logger.LogError($"Cancle error");
            return new CancelResult(response.exception, res);
        }
    }

}
