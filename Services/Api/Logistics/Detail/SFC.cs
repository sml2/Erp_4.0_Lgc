using Logistics.SFCLogisticsReference;
using SFCTrack = Logistics.SFCTrackReference;
using SFCTrackServiceReference = Logistics.SFCTrackReference;
namespace ERP.Services.Api.Logistics.Detail;
using Models.Api.Logistics;
using Models.Api.Logistics.SFC;
using Enums.Logistics;
using Enums;

using Exceptions.Logistics;

public class SFC : Base<Parameter>
{
    protected string logName = "";
    public SFC(ILogger<SFC> logger, LogisticClient request, IServiceProvider serviceProvider) : base(Platforms.SFC, logger, request, serviceProvider)
    {
        logName = base.Platform.GetDescription();
    }

    /// <summary>
    /// 获取请求头
    /// </summary>
    /// <param name="parameter"></param>
    /// <returns></returns>
    private HeaderRequest GetHeaderRequest(Parameter parameter)
    {
        HeaderRequest HeaderRequest = new HeaderRequest();
        HeaderRequest.userId = parameter.userId;
        HeaderRequest.appKey = parameter.appKey;
        HeaderRequest.token = parameter.token;
        return HeaderRequest;
    }

    public override async Task<CommonResultList> GetShipTypes(ShipTypeParameter shipTypeParam)
    {
        var logName = shipTypeParam.LogName.GetDescription();
        Parameter parameter = Helpers.GetParamType<Parameter>(shipTypeParam.Parameter);
        List<CommonResult> commonResults = new List<CommonResult>();
        //构建参数
        var HeaderRequest = GetHeaderRequest(parameter);
        ShipRateClient Client = new ShipRateClient();
        try
        {
            var ShipTypes = Client.getShipTypesAsync(HeaderRequest, 1).Result.shiptypes;
            if (ShipTypes.IsNotNull())
            {
                ShipTypes.ToList().ForEach(x => commonResults.Add(new CommonResult(x.method_code, x.cn_name)));
                return await Task.FromResult(new CommonResultList(commonResults));
            }
            else
            {
                logger.LogError($"{logName}-GetShipTypes-Service Result is null");
                return new CommonResultList(new ServiceResultError());
            }
        }
        catch (Exception e)
        {
            logger.LogError($"{logName}:{e.Message}");
            return new CommonResultList(new UnKnownError($"{logName}:{e.Message}"));
        }
    }


    public override async Task<ReportResultList> Send(SendParameter sendParameter)
    {
        List<ReportResult> results = new List<ReportResult>();
        var requests = new List<addOrderRequestInfoArray>();
        Parameter parameter = Helpers.GetParamType<Parameter>(sendParameter.Parameter);
        SenderInfo senderInfo = sendParameter.SenderInfo;
        List<OrderInfo> orderLists = sendParameter.OrderInfos;
        foreach (var orderInfo in orderLists)
        {            
            ExtParameter Ext = Helpers.GetParamType<ExtParameter>(orderInfo.ext);
            MoneyRecord money = new Data.MoneyMeta(Ext.Currency, Ext.MaxPrice);
            string value = money.ToString("USD", false);
            var amount = Convert.ToDecimal(value);

            addOrderRequestInfoArray request = new addOrderRequestInfoArray();
            request.shipperName = senderInfo.Name;
            request.shipperEmail = senderInfo.Mail;
            request.shipperAddress = senderInfo.Address;
            request.shipperZipCode = senderInfo.PostalCode;
            request.shipperCompanyName = senderInfo.Company;
            request.shippingMethod = Ext.ShipMethod;//运输方式
            request.shipperAddressType = 2;
            request.shipperCity = senderInfo.City;
            //Recipient
            request.recipientName = orderInfo.ReceiverAddress.Name;
            request.recipientOrganization = orderInfo.ReceiverAddress.Name;
            request.recipientState = orderInfo.ReceiverAddress.Province;    //receive_nation_short
            request.recipientCountry = orderInfo.ReceiverAddress.NationShort;
            request.recipientCity = orderInfo.ReceiverAddress.City;
            string Address = orderInfo.ReceiverAddress.Address;
            if (orderInfo.ReceiverAddress.Address2.IsNotWhiteSpace()) Address += $" {orderInfo.ReceiverAddress.Address2}";
            if (orderInfo.ReceiverAddress.Address3.IsNotWhiteSpace()) Address += $" {orderInfo.ReceiverAddress.Address3}";
            request.recipientAddress = Address;
            request.recipientZipCode = orderInfo.ReceiverAddress.Zip;
            request.recipientPhone = orderInfo.ReceiverAddress.Phone;
            request.recipientEmail = orderInfo.ReceiverAddress.Email;
            //Produce
            request.goodsDescription = Ext.goodsDescription;
            request.customerOrderNo = Helpers.GetOrderNo(orderInfo.OrderNo, orderInfo.SendNum, orderInfo.FailNum);
            int Quantity = 0;
            orderInfo.GoodInfos.ForEach((o) => Quantity += o.Num);
            request.goodsQuantity = Quantity.ToString();
            request.goodsDeclareWorth = value;            
            request.iossNo = Ext.IOSS;
            request.IDCR = Ext.IDCR;
            request.taxesNumber = Ext.taxesNumber;
            List<goodsDetailsArray> goodDetails = new List<goodsDetailsArray>();
            int num = orderInfo.GoodInfos.Sum((r) => r.Num);
            foreach (var r in orderInfo.GoodInfos)
            {
                goodsDetailsArray goodDetail = new goodsDetailsArray();
                goodDetail.detailDescription = Ext.detailDescription;
                goodDetail.detailQuantity = r.Count.ToString();
                goodDetail.detailWorth =Helpers.RoundData((amount / num)).ToString();
                goodDetail.detailCustomLabel = Ext.detailCustomLabel;
                goodDetail.detailDescriptionCN = Ext.detailDescriptionCN;//中文描述
                goodDetail.hsCode = Ext.HsCode;//海关编码                
                goodDetails.Add(goodDetail);
            }
            request.goodsDetails = goodDetails.ToArray();
            List<ErrorMember> errMsg = Ext.IsValid<ExtParameter>();
            if (errMsg.Count <= 0)
            {
                requests.Add(request);
            }
            else
            {
                logger.LogWarning(ParameterErrorType.ParameterFormat.GetDescription(), toMsg(errMsg));
                results.Add(new ReportResult(orderInfo.id, orderInfo.OrderNo, new ParameterError(ParameterErrorType.ParameterFormat, toMsg(errMsg))));
                continue;
            }
        }
        if (requests.Count > 0)
        {
            foreach (var request in requests)
            {
                var referenceNo = Helpers.GetOldOrderNo(request.customerOrderNo);
                var order = orderLists.Where(x => x.OrderNo == referenceNo).FirstOrDefault();
                int WhereorderId = order?.id > 0 ? order.id : -1;
                //请求头
                HeaderRequest HeaderRequest = GetHeaderRequest(parameter);
                addOrderRequest addOrderRequest = new addOrderRequest(HeaderRequest, request);
                ShipRateClient Client = new ShipRateClient();
                addOrderResponse result;
                try
                {
                    result = await ((ShipRate)Client).addOrderAsync(addOrderRequest);
                }
                catch (Exception e)
                {

                    logger.LogError(request.customerOrderNo + ":" + e.Message);
                    results.Add(new ReportResult(WhereorderId, request.customerOrderNo, new UnKnownError($"{e.Message}")));
                    continue;
                }
                string res = toJson(result);
                if (result.orderActionStatus == "N")
                {
                    logger.LogError($"{request.customerOrderNo}:{result.note}");
                    results.Add(new ReportResult(WhereorderId, request.customerOrderNo, new UnKnownError($"{result.note}"), res));
                    continue;
                }
                else
                {
                    results.Add(new ReportResult(WhereorderId, result.customerOrderNo, result.orderCode, result.trackingNumber, "", 0,
                       order?.ext??string.Empty, res));
                }
            }
        }
        return new ReportResultList(results);
    }

    /// <summary>
    /// 获取运费
    /// </summary>
    /// <param name="HeaderRequest"></param>
    /// <param name="Client"></param>
    /// <param name="OrderCode"></param>
    /// <returns></returns>
    private async Task<TrackPrice> GetFeeInfo(HeaderRequest HeaderRequest, string OrderCode, string customNumber, string trackNumber, int waybillId)
    {
        ShipRateClient Client = new ShipRateClient();
        getFeeByOrderCodeRequest GetFeeRequest = new getFeeByOrderCodeRequest(HeaderRequest, OrderCode);
        getFeeByOrderCodeResponse GetFeeResponse;
        try
        {
            GetFeeResponse = await ((ShipRate)Client).getFeeByOrderCodeAsync(GetFeeRequest);
            string res = toJson(GetFeeResponse);
            if (GetFeeResponse.ask.Contains("success", StringComparison.CurrentCultureIgnoreCase))
            {
                double TotalFee = Convert.ToDouble(GetFeeResponse.totalFee);
                GetFeeResponse.otherFee.ForEach(x => TotalFee += Convert.ToDouble(x.fee));
                DateTime? chargebackTime = null;
                if (!string.IsNullOrWhiteSpace(GetFeeResponse.chargebackTime))
                {
                    chargebackTime = Convert.ToDateTime(GetFeeResponse.chargebackTime);
                }
                return new TrackPrice(waybillId, customNumber, OrderCode, trackNumber, GetFeeResponse.currencyCode, TotalFee, GetFeeResponse.feeWeight, "",res,chargebackTime);
            }
            else
            {
                logger.LogError($"{customNumber}-{GetFeeResponse.msg}");
                return new TrackPrice(waybillId, customNumber, new UnKnownError($"{GetFeeResponse.msg}"));
            }
        }
        catch (Exception e)
        {
            logger.LogError($"{customNumber}-{e.Message}");
            return new TrackPrice(waybillId, customNumber, new UnKnownError($"{e.Message}"));
        }
    }



    public override async Task<WaybillPrice> GetFee(WaybillInfos wayBillParameter)
    {
        Parameter parameter = Helpers.GetParamType<Parameter>(wayBillParameter.Parameter);
        HeaderRequest HeaderRequest = GetHeaderRequest(parameter);
        var price = await GetFeeInfo(HeaderRequest,  wayBillParameter.LogictisNumber, wayBillParameter.CustomNumber, wayBillParameter.TrackNumber, wayBillParameter.waybillId);
        return new WaybillPrice(price);
    }


    public override async Task<TrackResultList> Track(TrackParameter trackParameter)
    {
        var logName = trackParameter.LogName.GetDescription();
        List<TrackResult> trackResults = new();
        Parameter parameter = Helpers.GetParamType<Parameter>(trackParameter.Parameter);
        HeaderRequest HeaderRequest = GetHeaderRequest(parameter);
        foreach (var trackInfo in trackParameter.TrackOrderInfoLst)
        {
            List<TrackInfo> trackInfos = new List<TrackInfo>();
            TrackPrice priceInfo = new TrackPrice();
            SFCTrack.getTrackingInfoRequest RequestInfo = new SFCTrack.getTrackingInfoRequest();
            if (trackInfo.TrackNumber.IsNotWhiteSpace())
            {
                RequestInfo.number = trackInfo.TrackNumber;
            }
            else if (trackInfo.LogictisNumber.IsNotWhiteSpace())
            {
                RequestInfo.number = trackInfo.LogictisNumber;
            }
            else
            {
                RequestInfo.number = trackInfo.CustomNumber;
            }
            SFCTrackServiceReference.trackPortsClient client = new SFCTrackServiceReference.trackPortsClient();
            SFCTrackServiceReference.getTrackingInfoResponse retVal;
            try
            {
                retVal = await ((SFCTrackServiceReference.trackPorts)client).getTrackingInfoAsync(RequestInfo);
                string res = toJson(retVal);
                if (retVal.IsNotNull())
                {
                    string TrackInfo = "";
                    States Status = States.TRANSITING;
                    //尚未更新物流信息(手动上报时填写错误跟踪号/运单号)
                    if (retVal.order.IsNull())
                    {
                        logger.LogWarning($"{logName} order Not Found ,{RequestInfo.number}");
                        trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new OrderDataNotFound(), res));
                    }
                    if (retVal.track.IsNull() || retVal.track.Length == 0)
                    {
                        logger.LogWarning($"{logName} getTrack Not Generated Track Error,CustomNumber:{trackInfo.CustomNumber}");
                        trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new NotGeneratedTrackError(), res));
                    }
                    else
                    {
                        //显示物流信息               
                        var TrackInfos = retVal.track.Reverse();
                        //状态(1：运输途中 2：运输过久 3：完成 4：退件 5：到达待取)
                        if (retVal.order.status == "1" || retVal.order.status == "2")
                        {
                            Status = States.TRANSITING;
                        }
                        else if (retVal.order.status == "3")
                        {
                            Status = States.SINGED;
                        }
                        else if (retVal.order.status == "4")
                        {
                            Status = States.RETURNING;
                        }
                        else if (retVal.order.status == "5")
                        {
                            Status = States.TRANSITING;
                        }
                        foreach (var v in TrackInfos)
                        {
                            trackInfos.Add(new TrackInfo(v.track_date, Status.GetDescription(), v.location, v.activity));
                            string Message = $"Time: {v.track_date}|Location: {v.location}|Track Description: {v.activity}";
                            TrackInfo += Message;
                        }                       
                       
                    }
                    //获取物流价格请求
                    
                        priceInfo = await GetFeeInfo(HeaderRequest, trackInfo.LogictisNumber ?? string.Empty, trackInfo.CustomNumber, trackInfo.TrackNumber ?? string.Empty, trackInfo.waybillId);
                        trackInfo.TrackNumber = trackInfo.TrackNumber.IsNullOrWhiteSpace() ? retVal.order.track_number : trackInfo.TrackNumber;
                    
                    trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, trackInfo.LogictisNumber, trackInfo.TrackNumber,
                       TrackInfo, retVal.order.status, Status, priceInfo, trackInfos, res));
                }
                else
                {
                    logger.LogWarning($"{logName} track Not Generated ,{RequestInfo.number}");
                    trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new NotGeneratedTrackError(), res));
                }
            }
            catch (Exception e)
            {
                logger.LogWarning($"{logName} track error ,{RequestInfo.number}:{e.Message}");
                trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new UnKnownError($"{e.Message}")));
            }
        }
        return new TrackResultList(trackResults);
    }

    public override bool FileType { get; set; } = true;

    public override bool IsUrl { get; set; } = true;
    public override async Task<printResultList> Print(MultiPrint multiPrint)
    {
        List<printResult> printResults = new List<printResult>();
        if (multiPrint.FileType is null || !Enum.TryParse<FileTypeEnum>(multiPrint.FileType, true, out var filetype))
        {
            logger.LogWarning("{LogName}-{FileType} mismatched error ", multiPrint.LogName.GetDescription(), multiPrint.FileType);
            return new printResultList(new ParameterError(PrintSettingEnum.FileType, multiPrint.FileType??""));
        }
        Parameter parameter = Helpers.GetParamType<Parameter>(multiPrint.Parameter);
        var express = multiPrint.printParams?.Where(s => !string.IsNullOrEmpty(s.Express)).Select(s => s.Express!).ToList()??Array.Empty<string>().ToList();

        printUrlRequest request = new printUrlRequest(parameter, express, filetype);
        //var response = await Request.SendPrint(request);
        //string res = toJson(response);
        //if (response.Success && response.Result.IsNotNull())
        //{
        //    var stream = await response.Result.Content.ReadAsStreamAsync();
        //    printResults.Add(new printResult(express, stream, "application/pdf", ".pdf", res));
        //}
        //else
        //{
        //    printResults.Add(new printResult(express, response.exception, res));
        //}
        printResults.Add(new printResult(express, request.Url, null));
        return new printResultList(printResults);
    }

    public override async Task<PrintParamResult> GetPrint(FileTypeParameter fileTypeParameter)
    {
        List<CommonResult> FileTyperesults = new List<CommonResult>();
        foreach (FileTypeEnum fileType in Enum.GetValues(typeof(FileTypeEnum)))
        {
            FileTyperesults.Add(new CommonResult(fileType.ToString(), fileType.GetDescription()));
        }
        return await Task.FromResult(new PrintParamResult(ContentType, FileType, PaperType, new(), FileTyperesults, new()));
    }

    public override bool GetCountryEnabled { get; set; } = true;
    public override async Task<Countries> GetCounties(BaseParameter baseParameter)
    {
        try
        {
            Parameter parameter = Helpers.GetParamType<Parameter>(baseParameter.Parameter);
            List<CommonResult> Destination = new List<CommonResult>();
            HeaderRequest Header = GetHeaderRequest(parameter);
            ShipRateClient Client = new ShipRateClient();
            getCountriesResponse getCountriesResponse = await Client.getCountriesAsync(Header);
            if (getCountriesResponse.IsNotNull() && getCountriesResponse.countries.Length > 0)
            {
                getCountriesResponse.countries.ToList().ForEach(x => Destination.Add(new CommonResult(x.enName, x.enName)));
                return new Countries(Destination, new());
            }
            else
            {
                logger.LogError($"GetCounties  error");
                return new Countries(new UnKnownError($"GetCounties  error"));
            }
        }
        catch (Exception ex)
        {
            logger.LogError($"GetCounties  error:{ex.Message}");
            return new Countries(new UnKnownError($"GetCounties  error"));
        }
    }

    public override bool EstimatePriceEnabled { get; set; } = true;

    /// <summary>
    /// 预估价格
    /// </summary>
    /// <param name="parameter"></param>
    /// <param name="Ext"></param>
    public override async Task<EstimatePriceResult> EstimatePrice(PriceParameter priceParameter)
    {
        var logName = priceParameter.LogName.GetDescription();
        List<CommonResult> lst = new List<CommonResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(priceParameter.Parameter);
        ExtParameter Ext = Helpers.GetParamType<ExtParameter>(priceParameter.ext);
        if (Ext.Length <= 0 || Ext.Width <= 0 || Ext.Height <= 0 || Ext.Weight <= 0)
        {
            logger.LogError($"{logName} EstimatePrice: Length or Width or Height or Weight less then 0");
            return new EstimatePriceResult(new ParameterError(ParameterErrorType.PackageNull));
        }
        if (priceParameter.CountryCode.IsNullOrWhiteSpace())
        {
            logger.LogError($"{logName} EstimatePrice CountryCode is null");
            return new EstimatePriceResult(new ParameterError(ParameterErrorType.CountryCodeNull));
        }
        try
        {
            HeaderRequest Header = GetHeaderRequest(parameter);
            ShipRateClient Client = new ShipRateClient();
            getRatesRequestInfo ratesRequestInfo = new getRatesRequestInfo();
            ratesRequestInfo.country = priceParameter.CountryCode;
            ratesRequestInfo.length = Ext.Length.ToString();
            ratesRequestInfo.width = Ext.Width.ToString();
            ratesRequestInfo.height = Ext.Height.ToString();
            ratesRequestInfo.weight = Ext.Weight.ToString();
            // 输入运费的类型： 1.通过用户物流折扣计算的运输费用 2.通过用户仓储折扣计算的运输费用 3.对外公布运输费用
            ratesRequestInfo.priceType = 1;
            getRatesResponse RatesResponse = await Client.getRatesAsync(Header, ratesRequestInfo);
            string res = toJson(RatesResponse);
            if (RatesResponse.IsNotNull() && RatesResponse.rates.Length > 0)
            {
                logger.LogInformation($"该货运国家[{priceParameter.CountryCode}]总共有{RatesResponse.rates.Length}条货运方式:");
                foreach (var r in RatesResponse.rates)
                {
                    lst.Add( new CommonResult(r.shiptypecode, $"\r\nThe cost of shipping method [{r.shiptypecnname}] is as follows:\r\nThe cost of fees:{r.costfee}\r\nProcessing fee:{r.dealfee}\r\nRegistration fee:{r.regfee}\r\nAdditional fee:{r.addons}\r\ntotalPrice:{r.totalfee}"));
                }
                return new EstimatePriceResult(lst, res);
            }
            else
            {
                logger.LogError($"EstimatePrice  error");
                return new EstimatePriceResult(new UnKnownError($"EstimatePrice  error"), res);
            }
        }
        catch (Exception ex)
        {
            logger.LogError($"EstimatePrice error:" + ex.Message);
            return new EstimatePriceResult(new UnKnownError(ex.Message));
        }
    }


    public override bool CancleEnabled { get; set; } = true;


    public override async Task<CancelResult> CancleOrder(CancleParameter cancleParameter)
    {
        var logName = cancleParameter.LogName.GetDescription();
        Parameter parameter = Helpers.GetParamType<Parameter>(cancleParameter.Parameter);
        List<CommonResult> commonResults = new List<CommonResult>();
        //构建参数
        var HeaderRequest = GetHeaderRequest(parameter);

        //修改状态:preprocess(预处理)、confirmed(已确认)、sumbmitted(已交寄)、send(已发货)、delete(已删除)
        var UpdateOrderStatusInfoArray = new UpdateOrderStatusInfoArray();
        UpdateOrderStatusInfoArray.orderStatus = "delete";
        UpdateOrderStatusInfoArray.authenticate = Sy.Security.MD5.Encrypt(cancleParameter.LogictisNumber+ parameter.userId);// md5(‘订单号’+‘用户code’)
        var updateOrderStatusRequest = new updateOrderStatusRequest(HeaderRequest, UpdateOrderStatusInfoArray, cancleParameter.LogictisNumber);
        ShipRateClient Client = new ShipRateClient();
               
        try
        {
            var response = Client.updateOrderStatusAsync(updateOrderStatusRequest).Result;
            string res = toJson(response);
            if (response.IsNotNull())
            {
                if(!response.ask.Contains("Error",StringComparison.CurrentCultureIgnoreCase))
                {
                    return new CancelResult(res);
                }
                else
                {
                    return new CancelResult(new UnKnownError(response.message), res);
                }
            }
            else
            {
                logger.LogError($"{logName}-CancleOrder- Result is null");
                return new CancelResult(new UnKnownError($"{logName}-CancleOrder Result is null"),"");
            }
        }
        catch (Exception e)
        {
            logger.LogError($"{logName}:{e.Message}");
            return new CancelResult(new UnKnownError($"{logName}:{e.Message}"),"");
        }
    }

}
