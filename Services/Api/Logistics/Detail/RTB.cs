namespace ERP.Services.Api.Logistics.Detail;
using OrderService = DB.Orders.Order;
using Enums;
using Services.Api.Logistics;
using Models.Api.Logistics;
using Models.Api.Logistics.SDGJ;
using Exceptions.Logistics;
using Enums.Logistics;
using System.Net;

public abstract class RTB : Base<Parameter>
{
    protected string logName = "";
    public RTB(Platforms platforms, ILogger logger, LogisticClient request, IServiceProvider serviceProvider) : base(platforms, logger, request, serviceProvider)
    {
        logName = base.Platform.GetDescription();
    }

    public override async Task<CommonResultList> GetShipTypes(ShipTypeParameter shipTypeParam)
    {
        var logName = shipTypeParam.LogName.GetDescription();
        List<CommonResult> commonResults = new List<CommonResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(shipTypeParam.Parameter);
        ShipTypesRequest shipTypesRequest = new(parameter, Platform);
        var response = await Request.Send<ShipTypesResponse>(shipTypesRequest);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            if (myResult.success > 0)
            {
                myResult.data.ForEach(r => commonResults.Add(new CommonResult(r.code, $"{r.cnname}[{r.code}]")));                             
                return new CommonResultList(commonResults);
            }
            else
            {
                logger.LogError($"{logName}-GetShipTypes-{logName}-{myResult.enmessage}");
                return new CommonResultList(new UnKnownError($"{myResult.enmessage}"));
            }
        }
        else
        {
            return new CommonResultList(response.exception);
        }
    }


    public override async Task<ReportResultList> Send(SendParameter sendParameter)
    {
        List<AddOrderRequest> requests = new List<AddOrderRequest>();
        List<ReportResult> results = new List<ReportResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(sendParameter.Parameter);
        SenderInfo senderInfo = sendParameter.SenderInfo;
        List<OrderInfo> orderLists = sendParameter.OrderInfos;
        foreach (var orderinfo in orderLists)
        {
            ExtParameter ext = Helpers.GetParamType<ExtParameter>(orderinfo.ext);
            AddOrderRequest request = new AddOrderRequest(parameter, senderInfo, orderinfo, ext, Platform);
            List<ErrorMember> errMsg = request.IsValid<AddOrderRequest>();
            errMsg.AddRange(ext.IsValid<ExtParameter>());
            if (errMsg.Count <= 0)
            {
                requests.Add(request);
            }
            else
            {
                logger.LogWarning(ParameterErrorType.ParameterFormat.GetDescription(), toMsg(errMsg));
                results.Add(new ReportResult(request.Id, request.orderNo, new ParameterError(ParameterErrorType.ParameterFormat, toMsg(errMsg))));
                continue;
            }
        }
        if (requests.Count > 0)
        {
            foreach (var request in requests)
            {
                var response = await Request.Send<AddOrderResponse>(request);
                string res = toJson(response);
                logger.LogInformation($"【RTB】创建运单接口日志：Request[{toJson(request)}],Response[{res}]");
                if (response.Success && response.Result.IsNotNull())
                {
                    var myResult = response.Result;
                    if (myResult.success == 1)
                    {
                        results.Add(new ReportResult(request.Id, myResult.data.refrence_no, myResult.data.shipping_method_no, myResult.data.shipping_method_no,
                        "", 0, orderLists.Where(x => x.id == request.Id).Select(s => s.ext).FirstOrDefault()??string.Empty, res));
                    }
                    else if (response.Result.success == 2)
                    {
                        //var OrderService = ServiceProvider.GetRequiredService<OrderService>();
                        //OrderService.UpdateDeliveryFail(request.Id);
                        logger.LogWarning($"{sendParameter.LogName.GetDescription()},Orderid:{request.Id} Order Already Exists");
                        results.Add(new ReportResult(request.Id, request.orderNo, new OrderAlreadyExists(), res));
                    }
                    else
                    {
                        logger.LogWarning($"{sendParameter.LogName.GetDescription()},{myResult.cnmessage}");
                        results.Add(new ReportResult(request.Id, request.orderNo, new UnKnownError(myResult.cnmessage), res));
                    }
                }
                else
                {
                    results.Add(new ReportResult(request.Id, request.orderNo, response.exception, res));
                }
            }
        }
        return new ReportResultList(results);
    }


    /// <summary>
    /// 获取物流费用
    /// </summary>
    /// <param name="CustomOrderNumber"></param>
    /// <param name="Log"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    public async Task<TrackPrice> GetFeeInfo(string CustomNumber, string? logNumber, string? trackNumber, Parameter parameter, string logName, int waybillId)
    {
        GetFeeRequest request = new GetFeeRequest(parameter, CustomNumber, Platform);
        var response = await Request.Send<GetFeeResponse>(request);
        string res = toJson(response);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            if (myResult.success > 0)
            {
                if (myResult.data.Count > 0)
                {
                    double price = 0;
                    string unit = "";
                    DateTime? deduction = null;
                    foreach(var d in myResult.data)
                    {
                        price += Convert.ToDouble(d.amount);                     
                        if (!string.IsNullOrEmpty(d.currency_code)) unit = d.currency_code;
                        if (deduction is not null)
                        {
                            if (!string.IsNullOrWhiteSpace(d.create_date))
                            {
                                var tempDeduction = Convert.ToDateTime(d.create_date);
                                if (tempDeduction > deduction)
                                {
                                    deduction = tempDeduction;
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(d.create_date))
                            {
                                deduction = Convert.ToDateTime(d.create_date);
                                
                            }
                        }
                       
                    }                   
                    return new TrackPrice(waybillId,CustomNumber, logNumber, trackNumber, unit, price, "", "", res,deduction);
                }
                else
                {
                    //尚未生成                                 
                    logger.LogWarning($"{logName} price Not Generated  Error,CustomNumber:{CustomNumber}");
                    return new TrackPrice(waybillId,CustomNumber, new NotGeneratedPriceError(), res);
                }
            }
            else
            {
                string msg = $"{myResult.enmessage}-{myResult.cnmessage};";
                logger.LogWarning($"{logName} getPrice error,CustomNumber:{CustomNumber}-{msg}");
                return new TrackPrice(waybillId,CustomNumber, new UnKnownError($"getPrice error:{msg}"), res);
            }
        }
        else
        {
            return new TrackPrice(waybillId,CustomNumber, response.exception, res);
        }
    }


    public override async Task<WaybillPrice> GetFee(WaybillInfos wayBillParameter)
    {
        Parameter parameter = Helpers.GetParamType<Parameter>(wayBillParameter.Parameter);
        var price = await GetFeeInfo(wayBillParameter.CustomNumber, wayBillParameter.LogictisNumber, wayBillParameter.TrackNumber, parameter, wayBillParameter.LogName.GetDescription(),wayBillParameter.waybillId);
        return new WaybillPrice(price);
    }






    /// <summary>
    /// 跟踪号支持同时接收最多20个服务商单号用,号分隔
    /// </summary>       
    /// <returns></returns>    
    public override async Task<TrackResultList> Track(TrackParameter trackParameter)
    {
        var logName = trackParameter.LogName.GetDescription();
        List<TrackResult> trackResults = new List<TrackResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(trackParameter.Parameter);
        foreach (var trackInfo in trackParameter.TrackOrderInfoLst)
        {
            List<TrackInfo> trackInfos = new List<TrackInfo>();
            TrackPrice priceInfo = new TrackPrice();
            if (trackInfo.TrackNumber.IsNullOrWhiteSpace())
            {
                //获取跟踪号
                Tuple<string?, Exception,string> tuple = await GetTrackNumber(trackInfo.CustomNumber, parameter, logName);
                if (tuple.Item1.IsNotNull())
                {
                    trackInfo.TrackNumber = tuple.Item1;
                }
                else
                {
                    trackResults.Add(new TrackResult(trackInfo.waybillId,trackInfo.CustomNumber, tuple.Item2,tuple.Item3));
                    continue;
                }
            }
            //请求track
            TrackInfoRequest trackInfoRequest = new TrackInfoRequest(parameter, trackInfo.TrackNumber??string.Empty, Platform);
            var response = await Request.Send<GetTrackInfoResponse>(trackInfoRequest);
            string res = toJson(response);
            if (response.Success && response.Result.IsNotNull())
            {
                var myResult = response.Result;
                if (myResult.success == 1)
                {
                    if (myResult.data.IsNotNull())
                    {
                        string TrackInfo = "";
                        var d = myResult.data[0];

                        var Details = d.details;
                        Details.Reverse();
                        //状态如何统一                         
                        Enums.Logistics.States states = Enums.Logistics.States.TRANSITING;
                        //track_status:ND(运输中) CC(派送妥投)  NO(未上网)
                        if (d.track_status == "NO")
                        {
                            states = Enums.Logistics.States.COMMITTED;
                        }
                        else if (d.track_status == "ND")
                        {
                            states = Enums.Logistics.States.TRANSITING;
                        }
                        else if (d.track_status == "CC")
                        {
                            states = Enums.Logistics.States.SINGED;
                        }
                        foreach (var v in Details)
                        {
                            trackInfos.Add(new TrackInfo(v.track_occur_date, states.GetDescription(), v.track_location, v.track_description));
                            string Message = $"Time: {v.track_occur_date}|Location: {v.track_location}|Track Description: {v.track_description}";
                            TrackInfo += Message + "\r\n";
                        }
                        
                        //上报价格
                        //if (trackInfo.Price.IsNull() || trackInfo.Price == 0)
                        //{
                            priceInfo = await GetFeeInfo(trackInfo.CustomNumber, trackInfo.TrackNumber, trackInfo.TrackNumber, parameter, logName, trackInfo.waybillId);
                       // }
                        trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, trackInfo.TrackNumber, trackInfo.TrackNumber,
                           TrackInfo, d.track_status, states, priceInfo, trackInfos, res));
                    }
                    else
                    {
                        //尚未生成                   
                        logger.LogWarning($"{logName} getTrack Not Generated Track Error,CustomNumber:{trackInfo.CustomNumber}");
                        trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new NotGeneratedTrackError(),res));
                    }
                }
                else
                {
                    logger.LogError($"{logName}-track error-{myResult.enmessage}:{myResult.cnmessage}");
                    trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, new UnKnownError($"{myResult.enmessage}:{myResult.cnmessage}"),res));
                }
            }
            else
            {
                trackResults.Add(new TrackResult(trackInfo.waybillId, trackInfo.CustomNumber, response.exception,res));
            }
        }
        return new TrackResultList(trackResults);
    }

    /// <summary>
    /// 获取跟踪号
    /// </summary>
    /// <param name="CustomOrderID"></param>
    /// <param name="token"></param>
    /// <param name="Log"></param>
    /// <returns></returns>
    public async Task<Tuple<string?, Exception,string>> GetTrackNumber(string customNumber, Parameter parameter, string logName)
    {
        TrackNumRequest trackNumRequest = new TrackNumRequest(parameter, customNumber, Platform);
        var response = await Request.Send<GetTrackNumResponse>(trackNumRequest);
        string res = toJson(response);
        if (response.Success && response.IsNotNull())
        {
            var myResult = response.Result;
            if (myResult.success == 1)
            {
                if (myResult.data.IsNotNull())
                {
                    return new Tuple< string?, Exception,string> (myResult.data.shipping_method_no, new UnKnownError($"{myResult.enmessage}:{myResult.cnmessage}"),res);
                }
                else
                {
                    //尚未生成                   
                    logger.LogWarning($"{logName} trackNumber Not Generated,customNumber:{customNumber}");
                    return new Tuple<string?, Exception,string>(null, new NotGeneratedTrackError(),res);
                }
            }
            else
            {
                string msg = $"{myResult.enmessage}-{myResult.cnmessage};";
                logger.LogWarning($"{logName} getTrackNumber error,customNumber:{customNumber}-{msg}");
                return new Tuple<string?, Exception,string>(null, new UnKnownError($"{msg}"),res);
            }
        }
        else
        {
            return new Tuple<string?, Exception,string>(null, response.exception,res);
        }
    }


    public override bool FileType { get; set; } = true;

    public override bool ContentType { get; set; } = true;

    public override bool PaperType { get; set; } = true;

    public override bool IsUrl { get; set; } = true;

    /// <summary>
    /// 多个单个，非批量
    /// </summary>
    /// <param name="multiPrint"></param>
    /// <returns></returns>
    public override async Task<printResultList> Print(MultiPrint multiPrint)
    {
        List<printResult> printResults = new List<printResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(multiPrint.Parameter);
        string filetype = "", papertype = "", contentype = "";
        //设置打印类型
        //1.PNG文件 2.PDF文件
        if (Enum.IsDefined(typeof(LabelFileTypeEnum), multiPrint.FileType ?? string.Empty) && Enum.TryParse<LabelFileTypeEnum>(multiPrint.FileType, out var labelFileTypeEnum))
        {
            filetype = ((int)labelFileTypeEnum).ToString();
        }
        else
        {
            logger.LogWarning($"{multiPrint.LogName.GetDescription()}-{multiPrint.FileType} mismatched error ");
            return new printResultList(new ParameterError(PrintSettingEnum.FileType, multiPrint.FileType ?? "暂无"));
        }
        //纸张类型
        if (Enum.IsDefined(typeof(PaperType), multiPrint.PaperType ?? string.Empty) && Enum.TryParse<PaperType>(multiPrint.PaperType, out var paperTypeEnum))
        {
            papertype = ((int)paperTypeEnum).ToString();
        }
        else
        {
            logger.LogWarning($"{multiPrint.LogName.GetDescription()}-{multiPrint.PaperType} mismatched error ");
            return new printResultList(new ParameterError(PrintSettingEnum.PaperType, multiPrint.PaperType ?? "暂无"));
        }
        //标签代码类型 1：标签 2：报关单3：配货单4：标签 + 报关单5：标签 + 配货单6：标签 + 报关单 + 配货单
        // configInfo.lable_content_type = (Convert.ToInt32(ContentType) + 1).ToString();
        if (multiPrint.ContentType is not null && Enum.IsDefined(typeof(ContentType), multiPrint.ContentType) && Enum.TryParse<ContentType>(multiPrint.ContentType, out var labelContentType))
        {
            contentype = ((int)labelContentType).ToString();
        }
        else
        {
            logger.LogWarning($"{multiPrint.LogName.GetDescription()}-{multiPrint.ContentType} mismatched error ");
            return new printResultList(new ParameterError(PrintSettingEnum.ContentType, multiPrint.ContentType ?? "暂无"));
        }
        if (multiPrint.printParams is not null)
        {


            foreach (var printInfo in multiPrint.printParams)
            {
                PrintUrlRequest printUrlRequest = new PrintUrlRequest(parameter, filetype, papertype, contentype, printInfo.LogictisNumber, Platform);
                var response = await Request.Send<PrintUrlResponse>(printUrlRequest);
                string res = toJson(response);
                if (response.Success && response.Result.IsNotNull())
                {
                    List<string> lst = new List<string>();
                    var myResult = response.Result;
                    if (myResult.success == 1 || myResult.success == 2)
                    {
                        if (myResult.data.IsNotNull() && myResult.data.Count > 0)
                        {
                            printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, myResult.data[0].lable_file, res));
                        }
                        else
                        {
                            //尚未生成
                            logger.LogWarning($"{printInfo.CustomNumber} Not Generated Label Error ");
                            return new printResultList(new NotGeneratedLabelError(), res);
                        }
                    }
                    else
                    {
                        logger.LogError($"{printInfo.CustomNumber} print error-{myResult.enmessage}:{myResult.cnmessage}");
                        new printResult(new List<string>() { printInfo.CustomNumber }, new UnKnownError($"{myResult.enmessage}:{myResult.cnmessage}"), res);
                        return new printResultList(new UnKnownError($"{myResult.enmessage}:{myResult.cnmessage}"), res);
                    }
                }
                else
                {
                    printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, response.exception, res));
                }
            }
        }
        return new printResultList(printResults);
    }

    public override async  Task<PrintParamResult> GetPrint(FileTypeParameter fileTypeParameter)
    {
        List<CommonResult> PaperTyperesults = new List<CommonResult>();
        foreach (PaperType paperType in Enum.GetValues(typeof(PaperType)))
        {
            PaperTyperesults.Add(new CommonResult(paperType.ToString(), paperType.GetDescription()));
        }

        List<CommonResult> FileTyperesults = new List<CommonResult>();
        foreach (LabelFileTypeEnum contentType in Enum.GetValues(typeof(LabelFileTypeEnum)))
        {
            FileTyperesults.Add(new CommonResult(contentType.ToString(), contentType.GetDescription()));
        }

        List<CommonResult> ContentTyperesults = new List<CommonResult>();
        foreach (ContentType contentType in Enum.GetValues(typeof(ContentType)))
        {
            ContentTyperesults.Add(new CommonResult(contentType.ToString(), contentType.GetDescription()));
        }
        
        return await Task.FromResult(new PrintParamResult(ContentType, FileType, PaperType, ContentTyperesults, FileTyperesults, PaperTyperesults));
    }

    public override bool EstimatePriceEnabled { get; set; } = true;
    public override async Task<EstimatePriceResult> EstimatePrice(PriceParameter priceParameter)
    {
        var logName = priceParameter.LogName.GetDescription();
        Parameter parameter = Helpers.GetParamType<Parameter>(priceParameter.Parameter);
        ExtParameter Ext = Helpers.GetParamType<ExtParameter>(priceParameter.ext);
        if (Ext.Weight <= 0)
        {
            logger.LogError($"{logName} EstimatePrice Ext.Weight less then 0");
            return new EstimatePriceResult(new ParameterError(ParameterErrorType.WeightNull));
        }
        if (priceParameter.CountryCode.IsNullOrWhiteSpace())
        {
            logger.LogError($"{logName} EstimatePrice CountryCode is null");
            return new EstimatePriceResult(new ParameterError(ParameterErrorType.CountryCodeNull));
        }
        EstimatePriceRequest request = new EstimatePriceRequest(parameter, Ext, priceParameter.CountryCode??"暂无", Platform);
        List<CommonResult> datas = new List<CommonResult>();
        var response = await Request.Send<GetEstimatePriceResponse>(request);
        string res = toJson(response);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            if (myResult.success > 0)
            {
                myResult.data.ForEach(v => datas.Add( new CommonResult(v.ServiceCode,v.ToString())));
                return new EstimatePriceResult(datas, res);
            }
            else
            {
                logger.LogError($"EstimatePrice  error:{myResult.enmessage}-{myResult.cnmessage}");
                return new EstimatePriceResult(new UnKnownError($"EstimatePrice:{myResult.enmessage}-{myResult.cnmessage}"), res);
            }           
        }
        else
        {
            logger.LogError($"EstimatePrice error");
            return new EstimatePriceResult(response.exception, res);
        }
    }


    public override bool CancleEnabled { get; set; } = true;

    public override async Task<CancelResult> CancleOrder(CancleParameter cancleParameter)
    {
        Parameter parameter = Helpers.GetParamType<Parameter>(cancleParameter.Parameter);
        InterceptRequest request = new InterceptRequest(parameter,cancleParameter.LogictisNumber, Platform);
        var response = await Request.Send<InterceptResponse>(request);
        string res = toJson(response);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            if (myResult.success == 1)
            {
                return await this.CancleOrder(cancleParameter);
            }
            else
            {
                //未在物流平台创建运单或者物流平台那边已删除，接口返回找不到运单，导致平台运单无法取消
                if (myResult.success == 2 && myResult.cnmessage.Contains("订单不存在"))
                {
                    return new CancelResult(res);
                }
                return new CancelResult(new UnKnownError(myResult.cnmessage), res);
            }
        }
        else
        {
            logger.LogError($"Cancle error");
            return new CancelResult(response.exception, res);
        }
    }



    //客户流水接口
    public  async Task LogisticBilling(BillParameter billParameter)
    {
        Parameter parameter = Helpers.GetParamType<Parameter>(billParameter.Parameter);
        BillingRequest request = new BillingRequest(parameter, billParameter.CustomNumber, billParameter.BeginDate, billParameter.EndDate, Platform);
        var response = await Request.Send<BillResponse>(request);
        string res = toJson(response);
        if (response.Success && response.Result.IsNotNull())
        {
            var myResult = response.Result;
            if (myResult.Success == 1)
            {
               //return await this.CancleOrder(cancleParameter);
            }
            else
            {
                //return new CancelResult(new UnKnownError(myResult.cnmessage), res);
            }
        }
        else
        {
            logger.LogError($"Cancle error");
            //return new CancelResult(response.exception, res);
        }
    }

}
