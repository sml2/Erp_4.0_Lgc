namespace ERP.Services.Api.Logistics.Detail;

using Models.Api.Logistics;
using Models.Api.Logistics.YanWen;
using Enums;
using static Models.Api.Logistics.YanWen.EstimatePriceResponse.Result;
using Exceptions.Logistics;
using Enums.Logistics;
using Microsoft.CodeAnalysis.VisualBasic.Syntax;

public class YanWen : Base<Parameter>
{
    protected string logName = "";
    public YanWen(ILogger<YanWen> logger, LogisticClient request, IServiceProvider serviceProvider) 
        : base(Platforms.YanWen, logger, request, serviceProvider)
    {
        logName = base.Platform.GetDescription();
    }


    public override async Task<CommonResultList> GetShipTypes(ShipTypeParameter shipTypeParam)
    {
        List<CommonResult> shipTypes = new List<CommonResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(shipTypeParam.Parameter);
        ShipTypeRequest request = new ShipTypeRequest(parameter);
        var response = await Request.Send<CommonResponse<ShipTypeResponse>> (request);
        if (response.Success && response.Result.IsNotNull()) //请求成功
        {
            var myResult = response.Result;
            if (myResult.IsNotNull())
            {
                if(myResult.Success)
                {
                    myResult.Data.ForEach(r => shipTypes.Add(new CommonResult(r.ID.ToString(), r.NameCh)));
                    return new CommonResultList(shipTypes);
                }
                else
                {
                    return new CommonResultList(new UnKnownError(myResult.Message));
                }
            }
            else
            {
                return new CommonResultList(new UnKnownError("has no result"));
            }          
        }
        else
        {
            return new CommonResultList(response.exception);
        }
    }

   
    public override async Task<ReportResultList> Send(SendParameter sendParameter)
    {
        List<AddOrderRequest> requests = new List<AddOrderRequest>();
        List<ReportResult> results = new List<ReportResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(sendParameter.Parameter);
        SenderInfo senderInfo = sendParameter.SenderInfo;
        List<OrderInfo> orderLists = sendParameter.OrderInfos;
        foreach (var orderinfo in orderLists)
        {
            ExtParameter Ext = Helpers.GetParamType<ExtParameter>(orderinfo.ext);
            AddOrderRequest request = new AddOrderRequest(parameter, senderInfo, orderinfo, Ext);
            List<ErrorMember> errMsg = request.IsValid<AddOrderRequest>();
            errMsg.AddRange(Ext.IsValid<ExtParameter>());          
            if (errMsg.Count <= 0)
            {
                requests.Add(request);
            }           
            else
            {
                logger.LogWarning(ParameterErrorType.ParameterFormat.GetDescription(), toJson(errMsg));
                results.Add(new ReportResult(orderinfo.id, orderinfo.OrderNo, new ParameterError(ParameterErrorType.ParameterFormat, toJson(errMsg))));                
            }
        }
        if (requests.Count > 0)
        {
            foreach (var request in requests)
            {
                var response = await Request.Send<CommonResponse<AddOrderResponse>>(request);
                string res = toJson(response);
                if(response is not null)
                {
                    var result = response.Result;
                    if (result is not null && result.Success)
                    {
                        results.Add(new ReportResult(request.Id, request.OrderNumber, result.Data.WaybillNumber,
                          "", "", 0,
                           orderLists.Where(x => x.id == request.Id).Select(s => s.ext).FirstOrDefault() ?? string.Empty, res));
                    }
                    else
                    {
                        logger.LogError($"{request.orderNo}:{result.Message}");
                        results.Add(new ReportResult(request.Id, request.orderNo, new UnKnownError($"{result.Message}"), res));
                    }
                }
                else
                {
                    results.Add(new ReportResult(request.Id, request.orderNo, response.exception, res));
                }               
            }
        }
        return new ReportResultList(results);
    }

    public override async Task<TrackResultList> Track(TrackParameter trackParameter)
    {
        const int limmitCount = 30;
        string numbers = "";
        var logName = trackParameter.LogName.GetDescription();
        List<TrackResult> trackResults = new List<TrackResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(trackParameter.Parameter);

        List<TrackInfo> trackInfos = new List<TrackInfo>();

        var nums = trackParameter.TrackOrderInfoLst.Select(x => x.LogictisNumber).ToList();

        if (nums.Count > 0 && nums.Count <= limmitCount)
        {
            numbers = string.Join(",", nums);
        }
        else
        {
            trackResults.Add(new TrackResult(0, numbers, new ArgumentException($"yanwen track logNumbers more than {limmitCount.ToString()} or less than 0"), ""));
            return new TrackResultList(trackResults);
        }
        TrackRequest request = new TrackRequest(parameter, numbers);
        var response = await Request.Send<TrackResponse>(request);
        string res = toJson(response);
        if (response.Success && response.Result.IsNotNull()) //请求成功
        {
            var myResult = response.Result;
            if (myResult.result.IsNotNull() && myResult.code == 0)
            {
                foreach (var item in myResult.result)
                {
                    int wid = trackParameter.TrackOrderInfoLst.First(x => x.LogictisNumber == item.TrackingNumber).waybillId;
                    string TrackInfo = "";
                    Enums.Logistics.States states = Enums.Logistics.States.TRANSITING;
                    if (item.Checkpoints.IsNotNull() && item.Checkpoints.Count > 0)
                    {
                        var details = item.Checkpoints;
                        details.Reverse();
                        if (item.TrackingStatus == "PU01") //燕文揽收
                        {
                            states = Enums.Logistics.States.COMMITTED;
                        }
                        else if (item.TrackingStatus == "SC30")//退件组包
                        {
                            states = Enums.Logistics.States.RETURNING;
                        }
                        else if (item.TrackingStatus == "LM40") //派送延期
                        {
                            states = Enums.Logistics.States.SINGED;
                        }
                        else if (item.TrackingStatus == "LM30") //到达待取
                        {
                            states = Enums.Logistics.States.WAITTOPICKUP;
                        }
                        else if (item.TrackingStatus == "LM50") //妥投失败
                        {
                            states = Enums.Logistics.States.DELIVERYFAILED;
                        }
                        else if (item.TrackingStatus == "LM90") //包裹退回
                        {
                            states = Enums.Logistics.States.RETURNING;
                        }
                        else if (item.TrackingStatus == "OTHER")
                        {
                            states = Enums.Logistics.States.OTHER;
                        }

                        foreach (var v in details)
                        {

                            var date = Convert.ToDateTime(v.TimeStamp);

                            char[] chars=v.TimeZone.ToCharArray();
                            if (chars[0]=='+')
                            {                                
                                date.AddHours(Convert.ToInt32(chars[1]));
                            }
                            else if(chars[0] == '-')
                            {
                                date.AddHours(-Convert.ToInt32(chars[1]));
                            }
                            else
                            {
                                throw new AggregateException($"yanwen timezone flag is not support");
                            }

                            trackInfos.Add(new TrackInfo(date.ToString("yyyy-MM-dd HH:mm:ss"), states.GetDescription(), v.Location, v.Message));
                            string Message = $"Time: {v.TimeStamp}|statue:{states.GetDescription()}|Location: {v.Location}|Track Description: {v.Message}";
                            TrackInfo += Message + "\r\n";
                        }
                        trackResults.Add(new TrackResult(wid, "", item.WaybillNumber, item.TrackingNumber,
                           TrackInfo, item.TrackingStatus, states, trackInfos, res));
                    }
                    else
                    {
                        //尚未生成                   
                        logger.LogWarning($"{logName} getTrack Not Generated Track Error,运单号:{item.WaybillNumber}");
                        trackResults.Add(new TrackResult(wid, item.WaybillNumber, new NotGeneratedTrackError(), res));
                    }
                }
            }
            else
            {
                string msg = $"{myResult.code}:{myResult.message}";
                logger.LogError($"{logName}-track error-{msg}");
                trackResults.Add(new TrackResult(0, numbers, new UnKnownError($"{msg}"), res));
            }
        }
        else
        {
            trackResults.Add(new TrackResult(0, numbers, response.exception, res));
        }

        return new TrackResultList(trackResults);
    }

    public override async Task<PrintParamResult> GetPrint(FileTypeParameter fileTypeParameter)
    {
        List<CommonResult> ContentTyperesults = new List<CommonResult>();
        foreach (ContentTypeEnum ContentType in Enum.GetValues(typeof(ContentTypeEnum)))
        {
            ContentTyperesults.Add(new CommonResult(ContentType.ToString(), ContentType.GetDescription()));
        }
        return await Task.FromResult(new PrintParamResult(ContentType, FileType, PaperType, ContentTyperesults, new(), new()));
    }


    public override bool ContentType { get; set; } = false;
    public override async Task<printResultList> Print(MultiPrint multiPrint)
    {
        List<printResult> printResults = new List<printResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(multiPrint.Parameter);
        
        if (multiPrint.printParams is not null)
        {
            foreach (var printInfo in multiPrint.printParams)
            {
                if(!string.IsNullOrEmpty(printInfo.Express))
                {
                    PrintRequest request = new PrintRequest(parameter, printInfo.Express);
                    var response = await Request.Send<CommonResponse<PrintResponse>>(request);
                    string res = toJson(response);
                    if (response.Success && response.IsNotNull())
                    {
                        var result = response.Result;
                        if (result is not null)
                        {
                            if (result.Success)
                            {
                                if (result.Data.IsSuccess)
                                {                                   
                                    byte[] buffer = Convert.FromBase64String(result.Data.Base64String);
                                     var stream = new System.IO.MemoryStream(buffer);
                                    printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, stream, "application/pdf", ".pdf",null, res));
                                }
                                else
                                {
                                    logger.LogError($"{printInfo.CustomNumber} print error,{result.Data.ErrorMsg}");
                                    printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, new UnKnownError(result.Data.ErrorMsg), res));
                                }
                            }
                            else
                            {
                                logger.LogError($"{printInfo.CustomNumber} print error,{result.Message}");
                                printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, new UnKnownError(result.Message), res));
                            }
                        }
                        else
                        {
                            logger.LogError($"{printInfo.CustomNumber} print error,{result.Message}");
                            printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, new UnKnownError("have no result"), res));
                        }
                    }
                    else
                    {
                        logger.LogError($"{printInfo.CustomNumber} print error");
                        printResults.Add(new printResult(new List<string>() { printInfo.CustomNumber }, response.exception, res));
                    }
                }
                else{

                }
            }
        }
        return new printResultList(printResults);
    }
   
    public override bool GetCountryEnabled { get; set; } = false;
    public override async Task<Countries> GetCounties(BaseParameter baseParameter)
    {
        List<CommonResult> DestinationShipTypes = new List<CommonResult>();
        Parameter parameter = Helpers.GetParamType<Parameter>(baseParameter.Parameter);
        GetCountriesRequest request = new GetCountriesRequest(parameter);
        var response = await Request.Send<CommonResponse<List<GetCountriesResponse>>>(request);
        if (response.Success && response.Result.IsNotNull()) //请求成功
        {
            var myResult = response.Result;

            if (myResult.Success)
            {
                if (myResult.Data.IsNotNull() && myResult.Data.Count > 0)
                {
                    foreach (var r in myResult.Data)
                    {
                        DestinationShipTypes.Add(new CommonResult(r.Code, r.ToString()));
                    }
                }
                else
                {
                    logger.LogError($"GetCounties Destination error:{myResult.Code}-{myResult.Message}");
                    return new Countries(new UnKnownError($"Destination:{myResult.Code}-{myResult.Message}"));
                }
            }                        
            return new Countries(DestinationShipTypes);
        }
        else
        {
            logger.LogError($"GetCounties error");
            return new Countries(response.exception); 
        }
    }

    public override bool CancleEnabled { get; set; } = true;

    public override async  Task<CancelResult> CancleOrder(CancleParameter cancleParameter)
    {
        Parameter parameter = Helpers.GetParamType<Parameter>(cancleParameter.Parameter);
        if (!string.IsNullOrEmpty(cancleParameter.LogictisNumber))
        {
            InterceptRequest request = new InterceptRequest(parameter, cancleParameter.LogictisNumber);
            var response = await Request.Send<CommonResponse>(request);
            string res = toJson(response);
            logger.LogInformation($"【燕文】取消运单接口日志：Request[{toJson(request)}],Response[{res}]");

            if (response.Success && response.Result.IsNotNull())
            {
                var myResult = response.Result;
                if (myResult.Success)
                {
                    return new CancelResult(res);
                }
                else
                {
                    //未在物流平台创建运单或者物流平台那边已删除，接口返回找不到运单，导致平台运单无法取消
                    if (myResult.Code == "1" && myResult.Message.Contains("未查询到此运单"))
                    {
                        return new CancelResult(res);
                    }
                    return new CancelResult(new UnKnownError($"Cancle Fail:{myResult.Message}"), res);
                }
            }
            else
            {
                logger.LogError($"Cancle error");
                return new CancelResult(response.exception, res);
            }
        }
        else
        {
            return new CancelResult(new UnKnownError("Cancle 运单号为空"));
        }
      
    }
}
