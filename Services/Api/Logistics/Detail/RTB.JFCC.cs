﻿namespace ERP.Services.Api.Logistics.Detail;
using Enums;
using AmazonService = DB.Orders.Order;

public class JFCC : RTB
{
    public JFCC(ILogger<JFCC> logger, LogisticClient request, IServiceProvider serviceProvider) : base(Platforms.RTB_JFCC, logger, request, serviceProvider)
    {
    }
}
