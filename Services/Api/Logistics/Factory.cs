
namespace ERP.Services.Api.Logistics;
using Enums;
using Detail;
using Models.Api.Logistics;
using Exceptions.Logistics;
using Interface;

public class Factory
{
    public Dictionary<Platforms, ILogistics> baseObservers = new();
    public Factory(
        DYGJ dYGJ, HZGJ hZGJ,
        DEPPON dEPPON,
        EMS eMS,
        EQuick eQuick,
        FPX fPX,
        HuaLei_OWN huaLei_OWN,
        HuaLei_YH huaLei_YH,
        BZY bZY, HL hL, JHGJ jHGJ,
        KJHYXB kJHYXB,
        JFCC jFCC, SDGJ sDGJ, YDH yDH, ZHXT zHXT,
        SFC sFC,
        SFGJ sFGJ,
        UBI uBI,
        WANB wANB,
        Y7F y7F,
        YanWen yanWen,
        YunTu yunTu,
        YH yh
        )
    {
        Add(dYGJ); Add(hZGJ);
        Add(dEPPON);
        Add(eMS);
        Add(eQuick);
        Add(fPX);
        Add(huaLei_OWN);
        Add(huaLei_YH);
        Add(bZY); Add(hL); Add(jHGJ);
        Add(kJHYXB);
        Add(jFCC); Add(sDGJ); Add(yDH); Add(zHXT);
        Add(sFC);
        Add(sFGJ);
        Add(uBI);
        Add(wANB);
        Add(y7F);
        Add(yanWen);
        Add(yunTu);
        Add(yh);
    }
    private void Add(ILogistics logistics) => baseObservers.Add(logistics.Platform, logistics);

    public ILogistics Get(Platforms platform)
    {
        return baseObservers[platform];
    }

    public async Task<PlatformMethodResult> methodDefineds(BaseParameter baseParameter)
    {
        var type = Get(baseParameter.LogName).CheckParameter(baseParameter);
        if (type == ParameterErrorType.None)
        {
            return await Get(baseParameter.LogName).methodDefineds(baseParameter);
        }
        return new PlatformMethodResult(new ParameterError(type));
    }

    public async Task<ReportResultList> Send(SendParameter sendParameter)
    {
        var type = Get(sendParameter.LogName).CheckParameter(sendParameter);
        if (type == ParameterErrorType.None)
        {
            return await Get(sendParameter.LogName).Send(sendParameter);
        }
        var sendType = Get(sendParameter.LogName).CheckOrderInfoParameter(sendParameter);
        if (sendType == ParameterErrorType.None)
        {
            return await Get(sendParameter.LogName).Send(sendParameter);
        }
        return new ReportResultList(new ParameterError(type));
    }

    public async Task<ReportResultList> SendReport(SendParameter sendParameter)
    {
        var type = Get(sendParameter.LogName).CheckParameter(sendParameter);
        if (type == ParameterErrorType.None)
        {
            return await Get(sendParameter.LogName).SendReport(sendParameter);
        }
       var res= new ReportResult(sendParameter.OrderInfos.First().id, sendParameter.WaybillOrder, new ParameterError(type));
        return new ReportResultList(new List<ReportResult>() { res});
    }

    public async Task<WaybillPrice> GetFee(WaybillInfos waybillParam)
    {
        var type = Get(waybillParam.LogName).CheckParameter(waybillParam);
        if (type == ParameterErrorType.None)
        {
            return await Get(waybillParam.LogName).GetFee(waybillParam);
        }
        return new WaybillPrice(new ParameterError(type));
    }

    public async Task<TrackResultList> Track(TrackParameter trackParameter)
    {
        var type = Get(trackParameter.LogName).CheckParameter(trackParameter);
        if (type == ParameterErrorType.None)
        {
            return await Get(trackParameter.LogName).Track(trackParameter);
        }
        return new TrackResultList(new ParameterError(type));
    }

    public async Task<printResultList> Print(MultiPrint multiPrint)
    {
        var type = Get(multiPrint.LogName).CheckParameter(multiPrint);
        if (type == ParameterErrorType.None)
        {
            return await Get(multiPrint.LogName).Print(multiPrint);
        }
        return new printResultList(new ParameterError(type));
    }

    public async Task<CommonResultList> GetShipTypes(ShipTypeParameter shipTypeParameter)
    {
        var type = Get(shipTypeParameter.LogName).CheckParameter(shipTypeParameter);
        if (type == ParameterErrorType.None)
        {
            return await Get(shipTypeParameter.LogName).GetShipTypes(shipTypeParameter);
        }
        return new CommonResultList(new ParameterError(type));
    }

    public async Task<CommonResultList> GetWarehouseCode(BaseParameter baseParameter)
    {
        var type = Get(baseParameter.LogName).CheckParameter(baseParameter);
        if (type == ParameterErrorType.None)
        {
            return await Get(baseParameter.LogName).GetWarehouseCode(baseParameter);
        }
        return new CommonResultList(new ParameterError(type));
    }

    public async Task<EstimatePriceResult> EstimatePrice(PriceParameter priceParameter)
    {
        var type = Get(priceParameter.LogName).CheckParameter(priceParameter);
        if (type == ParameterErrorType.None)
        {
            return await Get(priceParameter.LogName).EstimatePrice(priceParameter);
        }
        return new EstimatePriceResult(new ParameterError(type));
    }

    public async Task<Countries> GetCounties(BaseParameter baseParameter)
    {
        var type = Get(baseParameter.LogName).CheckParameter(baseParameter);
        if (type == ParameterErrorType.None)
        {
            return await Get(baseParameter.LogName).GetCounties(baseParameter);
        }
        return new Countries(new ParameterError(type));
    }


    public async Task<PrintParamResult> GetPrint(FileTypeParameter fileTypeParameter)
    {
        return await Get(fileTypeParameter.LogName).GetPrint(fileTypeParameter);
    }


    public async Task<CommonResultList> LoadChannelCode(BaseParameter baseParameter)
    {
        return await Get(baseParameter.LogName).LoadChannelCode(baseParameter);
    }

    public async Task<CancelResult> CancleOrder(CancleParameter cancleParameter)
    {
        return await Get(cancleParameter.LogName).CancleOrder(cancleParameter);
    }

}
