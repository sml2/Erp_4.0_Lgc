namespace ERP.Services.Api.Logistics;
using Models.Api.Logistics;
using Enums;
using System.Reflection;
using Exceptions.Logistics;
using ERP.Interface;

public abstract class Base<T> : Interface.ILogistics
{
    protected readonly ILogger logger;
    protected readonly LogisticClient Request;
    protected readonly IServiceProvider ServiceProvider;
    public Platforms Platform { get; }
    public virtual bool PrintEnabled { get; set; } = true;
    public virtual bool TrackEnabled { get; set; } = true;

    public virtual bool WarehouseEnabled { get; set; } = false;
    public virtual bool GetCountryEnabled { get; set; } = false;
    public virtual bool EstimatePriceEnabled { get; set; } = false;
    public virtual bool CancleEnabled { get; set; } = false;
    public virtual bool ChannelCode { get; set; } = false;
    public virtual bool ContentType { get; set; } = false;
    public virtual bool FileType { get; set; } = false;
    public virtual bool PaperType { get; set; } = false;
    public virtual bool IsUrl { get; set; } = false;
   

    public async Task<PlatformMethodResult> methodDefineds(BaseParameter baseParameter)
    {
        var p = new PlatformMethod(Platform, true, true, TrackEnabled, true, WarehouseEnabled, GetCountryEnabled, EstimatePriceEnabled, PrintEnabled, CancleEnabled,
           ChannelCode, ContentType, FileType, PaperType, IsUrl);
        return await Task.FromResult(new PlatformMethodResult(p));
    }

    public Base(Platforms platform, ILogger Logger, LogisticClient request, IServiceProvider serviceProvider)
    {
        Platform = platform;
        logger = Logger;
        Request = request;
        ServiceProvider = serviceProvider.CreateScope().ServiceProvider;


    }

    public abstract Task<ReportResultList> Send(SendParameter sendParameter);
    public async Task<ReportResultList> SendReport(SendParameter sendParameter)
    {
        var info = sendParameter.OrderInfos.First();
        var res= new ReportResult(info.id, sendParameter.WaybillOrder, sendParameter.Express, sendParameter.TrackNum);
        return await Task.FromResult(new ReportResultList(res));
    }
    
    public abstract Task<CommonResultList> GetShipTypes(ShipTypeParameter shipTypeParameter);

    public virtual async Task<printResultList> Print(MultiPrint multiPrint)
    {
        return await Task.FromResult(new printResultList(new MethodNotRealized("Print")));
    }

    public virtual async Task<TrackResultList> Track(TrackParameter trackParameter)
    {
        return await Task.FromResult(new TrackResultList(new MethodNotRealized("Track")));
    }

    public virtual async Task<EstimatePriceResult> EstimatePrice(PriceParameter priceParameter)
    {
        return await Task.FromResult(new EstimatePriceResult(new MethodNotRealized("EstimatePrice")));
    }
    public virtual async Task<Countries> GetCounties(BaseParameter baseParameter)
    {
        return await Task.FromResult(new Countries(new MethodNotRealized("GetCounties")));
    }
    public virtual async Task<CommonResultList> GetWarehouseCode(BaseParameter baseParameter)
    {
        return await Task.FromResult(new CommonResultList(new MethodNotRealized("GetWarehouseCode")));
    }

    public PropertyInfo[] GetParameters() => typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

    public ParameterErrorType CheckParameter(BaseParameter Parameter)
    {
        try
        {
            var parameter = Helpers.GetParamType<BaseParam>(Parameter.Parameter);
            if (parameter.IsNull())
            {
                logger.LogWarning(ParameterErrorType.Parameter.GetDescription(), Parameter.Parameter);
                return ParameterErrorType.ParameterNull;
            }
        }
        catch (Exception e)
        {
            logger.LogWarning(ParameterErrorType.Parameter.GetDescription() + "," + e.Message, Parameter.Parameter);
            return ParameterErrorType.Parameter;
        }
        return ParameterErrorType.None;
    }

    public ParameterErrorType CheckOrderInfoParameter(SendParameter sendParameter)
    {
        if (sendParameter.OrderInfos.IsNull() || sendParameter.OrderInfos.Count <= 0)
        {
            logger.LogWarning(ParameterErrorType.OrderInfoParameter.GetDescription(), sendParameter.OrderInfos);
            return ParameterErrorType.OrderInfoParameter;
        }
        foreach (var order in sendParameter.OrderInfos)
        {
            try
            {
                var extParameter = Helpers.GetParamType<BaseExtParam>(order.ext);
                if (extParameter.IsNull())
                {
                    logger.LogWarning(ParameterErrorType.Parameter.GetDescription(), order.ext);
                    return ParameterErrorType.Parameter;
                }
            }
            catch (Exception /*e*/)
            {
                logger.LogWarning(ParameterErrorType.Parameter.GetDescription(), order.ext);
                return ParameterErrorType.Parameter;
            }
        }
        return ParameterErrorType.None;
    }

    public virtual async Task<CommonResultList> LoadChannelCode(BaseParameter baseParameter)
    {
        return await Task.FromResult(new CommonResultList(new MethodNotRealized("LoadChannelCode")));
    }

    public virtual async Task<PrintParamResult> GetPrint(FileTypeParameter fileTypeParameter)
    {
        return await Task.FromResult(new PrintParamResult(new MethodNotRealized("GetPrint")));
    }

    public virtual async Task<CancelResult> CancleOrder(CancleParameter cancleParameter)
    {
        return await Task.FromResult(new CancelResult(new MethodNotRealized("CancleOrder"), "MethodNotRealized"));
    }


    public string toJson(object obj)
    {
        return Newtonsoft.Json.JsonConvert.SerializeObject(obj);
    }

    public string toMsg(List<ErrorMember> errors)
    {
        return String.Join(',', errors.Select(x => x.ErrorMemberName + ":" + x.ErrorMessage));
    }
  

    public virtual async Task<WaybillPrice> GetFee(WaybillInfos waybillParam)
    {
        return await Task.FromResult(new WaybillPrice(new MethodNotRealized("GetFee")));
    }

}

