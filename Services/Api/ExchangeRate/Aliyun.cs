﻿using System;
using System.Net.Http.Headers;
using Newtonsoft.Json.Schema;

namespace ERP.Services.Api.ExchangeRate
{
    /// <summary>
    /// 汇率查询-极速数据 阿里云 https://market.aliyun.com/products/57000002/cmapi011221.html
    /// </summary>
    public class Aliyun
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public Aliyun(IHttpClientFactory httpClientFactory)
        {
            this._httpClientFactory = httpClientFactory;
        }
        public async Task<Dictionary<string, Json.ResultEntry.Item>?> Get(ILogger logger)
        {
            var httpClient = _httpClientFactory.CreateClient();
            var host = @"https://jisuhuilv.market.alicloudapi.com";
            var path = @"/exchange/single";
            var query = "currency=CNY";
            var url = $"{host}{path}?{query}";
            //url = "https://api.sml2.com/";
            var appcode = @"2d72d998c0c642e9835d9fad8cb8039e";
            logger.LogInformation("Request:{url}", url);
            var message = new HttpRequestMessage(HttpMethod.Get, url);
            message.Headers.Authorization = new AuthenticationHeaderValue("APPCODE", appcode);
            message.Content = JsonContent.Create(string.Empty);
            var Result = await httpClient.SendAsync(message);
            if (Result.IsSuccessStatusCode)
            {
                //{"status":0,"msg":"ok","result":{"currency":"CNY","name":"人民币","list":{"USD":{"name":"美元","rate":"0.1578","updatetime":"2022-02-16 16:38:12"},"EUR":{"name":"欧元","rate":"0.138540","updatetime":"2022-02-16 16:42:33"},"JPY":{"name":"日元","rate":"18.246000","updatetime":"2022-02-16 16:42:33"},"HKD":{"name":"港币","rate":"1.231000","updatetime":"2022-02-16 16:44:33"},"KRW":{"name":"韩元","rate":"188.909000","updatetime":"2022-02-16 16:42:33"},"RUB":{"name":"卢布","rate":"11.831500","updatetime":"2022-02-16 16:42:33"},"GBP":{"name":"英镑","rate":"0.116370","updatetime":"2022-02-16 16:42:33"},"SGD":{"name":"新加坡元","rate":"0.211600","updatetime":"2022-02-16 16:42:33"},"TWD":{"name":"新台币","rate":"4.400900","updatetime":"2022-02-16 16:42:33"},"CAD":{"name":"加拿大元","rate":"0.199800","updatetime":"2022-02-16 16:42:33"},"AUD":{"name":"澳大利亚元","rate":"0.215000","updatetime":"2022-02-16 16:42:33"},"BRL":{"name":"巴西雷亚尔","rate":"0.813850","updatetime":"2022-02-16 11:07:36"},"INR":{"name":"印度卢比","rate":"11.847400","updatetime":"2022-02-16 16:42:33"},"CHF":{"name":"瑞士法郎","rate":"0.145500","updatetime":"2022-02-16 16:42:33"},"THB":{"name":"泰国铢","rate":"5.101500","updatetime":"2022-02-16 16:42:33"},"MOP":{"name":"澳门元","rate":"1.26749","updatetime":"2022-02-16 16:43:15"},"NZD":{"name":"新西兰元","rate":"0.232000","updatetime":"2022-02-16 16:42:33"},"ZAR":{"name":"南非兰特","rate":"2.378500","updatetime":"2022-02-16 16:43:33"},"IDR":{"name":"印尼卢比","rate":"2252.200000","updatetime":"2022-02-16 15:02:36"},"MXN":{"name":"墨西哥比索","rate":"3.21802","updatetime":"2022-02-16 16:44:54"},"ARS":{"name":"阿根廷比索","rate":"16.7763","updatetime":"2022-02-16 16:43:15"},"MYR":{"name":"林吉特","rate":"0.660850","updatetime":"2022-02-16 16:42:33"},"OMR":{"name":"阿曼里亚尔","rate":"0.06072","updatetime":"2022-02-16 16:43:15"},"EGP":{"name":"埃及镑","rate":"2.47346","updatetime":"2022-02-16 16:43:15"},"PKR":{"name":"巴基斯坦卢比","rate":"27.70337","updatetime":"2022-02-16 16:43:15"},"PYG":{"name":"巴拉圭瓜拉尼","rate":"1096.58637","updatetime":"2022-02-16 16:43:15"},"BHD":{"name":"巴林第纳尔","rate":"0.05945","updatetime":"2022-02-16 16:43:15"},"PAB":{"name":"巴拿马巴尔博亚","rate":"0.15775","updatetime":"2022-02-16 16:43:15"},"BMD":{"name":"百慕大元","rate":"0.15775","updatetime":"2022-02-16 16:43:15"},"BGN":{"name":"保加利亚列弗","rate":"0.27195","updatetime":"2022-02-16 16:45:01"},"ISK":{"name":"冰岛克朗","rate":"19.58576","updatetime":"2022-02-16 16:43:15"},"PLN":{"name":"波兰兹罗提","rate":"0.621930","updatetime":"2022-02-16 16:42:33"},"BOB":{"name":"玻利维亚诺","rate":"1.07267","updatetime":"2022-02-16 16:43:15"},"BWP":{"name":"博茨瓦纳普拉","rate":"1.809000","updatetime":"2022-02-16 16:22:33"},"DKK":{"name":"丹麦克朗","rate":"1.03363","updatetime":"2022-02-16 16:44:54"},"PHP":{"name":"菲律宾比索","rate":"8.093000","updatetime":"2022-02-16 16:42:33"},"COP":{"name":"哥伦比亚比索","rate":"622.71702","updatetime":"2022-02-16 16:43:15"},"CUP":{"name":"古巴比索","rate":"3.94365","updatetime":"2022-02-16 16:43:15"},"KZT":{"name":"哈萨克斯坦坚戈","rate":"67.68571","updatetime":"2022-02-16 16:43:15"},"ANG":{"name":"荷兰盾","rate":"0.28158","updatetime":"2022-02-16 16:43:15"},"CZK":{"name":"捷克克朗","rate":"3.38113","updatetime":"2022-02-16 16:44:50"},"ZWL":{"name":"津巴布韦元","rate":"50.63651","updatetime":"2022-02-16 16:45:01"},"QAR":{"name":"卡塔尔里亚尔","rate":"0.5742","updatetime":"2022-02-16 16:43:15"},"KWD":{"name":"科威特第纳尔","rate":"0.04767","updatetime":"2022-02-16 16:43:15"},"HRK":{"name":"克罗地亚库纳","rate":"1.04054","updatetime":"2022-02-16 16:45:01"},"KES":{"name":"肯尼亚先令","rate":"17.91207","updatetime":"2022-02-16 16:43:15"},"LVL":{"name":"拉脱维亚拉图","rate":"0.0801","updatetime":"2022-02-16 16:45:01"},"LAK":{"name":"老挝基普","rate":"1803.600000","updatetime":"2022-02-16 11:07:36"},"LBP":{"name":"黎巴嫩镑","rate":"237.48679","updatetime":"2022-02-16 16:43:15"},"LTL":{"name":"立陶宛立特","rate":"0.44713","updatetime":"2022-02-16 16:43:15"},"RON":{"name":"罗马尼亚列伊","rate":"0.68492","updatetime":"2022-02-16 16:44:46"},"MUR":{"name":"毛里求斯卢比","rate":"6.82252","updatetime":"2022-02-16 16:43:15"},"MNT":{"name":"蒙古图格里克","rate":"450.37622","updatetime":"2022-02-16 16:43:15"},"BDT":{"name":"孟加拉塔卡","rate":"13.36425","updatetime":"2022-02-16 16:43:15"},"PEN":{"name":"秘鲁新索尔","rate":"0.59814","updatetime":"2022-02-16 16:43:15"},"MAD":{"name":"摩洛哥迪拉姆","rate":"1.47761","updatetime":"2022-02-16 16:44:49"},"NOK":{"name":"挪威克朗","rate":"1.40397","updatetime":"2022-02-16 16:44:54"},"SAR":{"name":"沙特里亚尔","rate":"0.59188","updatetime":"2022-02-16 16:43:15"},"SOS":{"name":"索马里先令","rate":"90.38853","updatetime":"2022-02-16 16:43:15"},"TZS":{"name":"坦桑尼亚先令","rate":"363.92031","updatetime":"2022-02-16 16:43:15"},"TND":{"name":"突尼斯第纳尔","rate":"0.45462","updatetime":"2022-02-16 16:43:15"},"TRY":{"name":"土耳其里拉","rate":"2.14465","updatetime":"2022-02-16 16:44:55"},"GTQ":{"name":"危地马拉格查尔","rate":"1.21275","updatetime":"2022-02-16 16:43:15"},"UYU":{"name":"乌拉圭比索","rate":"6.79744","updatetime":"2022-02-16 16:43:15"},"HUF":{"name":"匈牙利福林","rate":"49.12688","updatetime":"2022-02-16 16:44:53"},"JMD":{"name":"牙买加元","rate":"24.68885","updatetime":"2022-02-16 16:43:15"},"ILS":{"name":"以色列谢克尔","rate":"0.50583","updatetime":"2022-02-16 16:44:54"},"JOD":{"name":"约旦第纳尔","rate":"0.11165","updatetime":"2022-02-16 16:43:15"},"VND":{"name":"越南盾","rate":"3593.300000","updatetime":"2022-02-16 11:07:36"},"CLP":{"name":"智利比索","rate":"126.22845","updatetime":"2022-02-16 16:43:15"},"PGK":{"name":"巴布亚新几内亚基那","rate":"0.55311","updatetime":"2022-02-16 16:43:15"},"KPW":{"name":"朝鲜圆","rate":"141.97151","updatetime":"2022-02-16 16:43:15"},"LSL":{"name":"莱索托洛提","rate":"2.38512","updatetime":"2022-02-16 16:43:15"},"LYD":{"name":"利比亚第纳尔","rate":"0.72295","updatetime":"2022-02-16 16:43:15"},"RWF":{"name":"卢旺达法郎","rate":"158.4055","updatetime":"2022-02-16 16:43:15"},"MMK":{"name":"缅甸元","rate":"279.510000","updatetime":"2022-02-16 11:07:36"},"MRO":{"name":"毛里塔尼亚乌吉亚","rate":"55.99987","updatetime":"2022-02-16 16:45:01"},"MWK":{"name":"马拉维克瓦查","rate":"125.49493","updatetime":"2022-02-16 16:43:15"},"NIO":{"name":"尼加拉瓜科多巴","rate":"5.58421","updatetime":"2022-02-16 16:43:15"},"NPR":{"name":"尼泊尔卢比","rate":"19.0368","updatetime":"2022-02-16 16:43:15"},"SBD":{"name":"所罗门群岛元","rate":"1.27432","updatetime":"2022-02-16 16:43:15"},"SCR":{"name":"塞舌尔法郎","rate":"2.2379","updatetime":"2022-02-16 16:43:15"},"BND":{"name":"文莱元","rate":"0.211860","updatetime":"2022-02-16 16:42:33"},"SYP":{"name":"叙利亚镑","rate":"396.10052","updatetime":"2022-02-16 16:43:15"},"DZD":{"name":"阿尔及利亚第纳尔","rate":"22.09313","updatetime":"2022-02-16 16:44:46"},"AED":{"name":"阿联酋迪拉姆","rate":"0.57934","updatetime":"2022-02-16 16:43:15"},"BBD":{"name":"巴巴多斯元","rate":"0.31549","updatetime":"2022-02-16 16:43:15"},"AFN":{"name":"阿富汗尼","rate":"14.43062","updatetime":"2022-02-16 16:43:15"},"ALL":{"name":"阿尔巴尼亚勒克","rate":"16.83309","updatetime":"2022-02-16 16:45:01"},"AMD":{"name":"亚美尼亚德拉姆","rate":"75.52727","updatetime":"2022-02-16 16:45:01"},"AOA":{"name":"安哥拉宽扎","rate":"83.84703","updatetime":"2022-02-16 16:45:01"},"AWG":{"name":"阿鲁巴盾弗罗林","rate":"0.28079","updatetime":"2022-02-16 16:43:15"},"AZN":{"name":"阿塞拜疆新马纳特","rate":"0.26809","updatetime":"2022-02-16 16:43:15"},"BAM":{"name":"波斯尼亚马尔卡","rate":"0.260000","updatetime":"2022-02-16 15:20:05"},"BIF":{"name":"布隆迪法郎","rate":"315.07264","updatetime":"2022-02-16 16:43:15"},"BSD":{"name":"巴哈马元","rate":"0.15775","updatetime":"2022-02-16 16:43:15"},"BTN":{"name":"不丹努扎姆","rate":"11.93034","updatetime":"2022-02-16 16:45:01"},"BYR":{"name":"白俄罗斯卢布","rate":"3158.07739","updatetime":"2022-02-16 16:45:01"},"BYN":{"name":"白俄罗斯卢布（新）","rate":"0.40383","updatetime":"2022-02-16 16:43:15"},"BZD":{"name":"伯利兹美元","rate":"0.31513","updatetime":"2022-02-16 16:43:15"},"CDF":{"name":"刚果法郎","rate":"315.07106","updatetime":"2022-02-16 16:43:15"},"CRC":{"name":"哥斯达黎加科朗","rate":"100.33127","updatetime":"2022-02-16 16:43:15"},"CUC":{"name":"古巴可兑换比索","rate":"0.15775","updatetime":"2022-02-16 16:43:15"},"CVE":{"name":"佛得角埃斯库多","rate":"15.35974","updatetime":"2022-02-16 16:43:15"},"DJF":{"name":"吉布提法郎","rate":"27.99994","updatetime":"2022-02-16 16:43:15"},"DOP":{"name":"多明尼加比索","rate":"8.94263","updatetime":"2022-02-16 16:43:15"},"NGN":{"name":"尼日利亚奈拉","rate":"65.51354","updatetime":"2022-02-16 16:43:15"},"ERN":{"name":"厄立特里亚纳克法","rate":"2.37408","updatetime":"2022-02-16 16:43:15"},"ETB":{"name":"埃塞俄比亚比尔","rate":"7.93463","updatetime":"2022-02-16 16:43:15"},"FJD":{"name":"斐济元","rate":"0.33395","updatetime":"2022-02-16 16:43:15"},"FKP":{"name":"福克兰镑","rate":"0.11656","updatetime":"2022-02-16 16:43:15"},"GEL":{"name":"格鲁吉亚拉里","rate":"0.46672","updatetime":"2022-02-16 16:43:15"},"GIP":{"name":"直布罗陀镑","rate":"0.11661","updatetime":"2022-02-16 16:43:15"},"GMD":{"name":"冈比亚达拉西","rate":"8.29745","updatetime":"2022-02-16 16:43:15"},"GNF":{"name":"几内亚法郎","rate":"1411.03907","updatetime":"2022-02-16 16:43:15"},"GYD":{"name":"圭亚那元","rate":"32.79384","updatetime":"2022-02-16 16:43:15"},"HNL":{"name":"洪都拉斯伦皮拉","rate":"3.8592","updatetime":"2022-02-16 16:43:15"},"HTG":{"name":"海地古德","rate":"15.83613","updatetime":"2022-02-16 16:43:15"},"IQD":{"name":"伊拉克第纳尔","rate":"230.07588","updatetime":"2022-02-16 16:43:15"},"IRR":{"name":"伊朗里亚尔","rate":"6625.33718","updatetime":"2022-02-16 16:43:15"},"KGS":{"name":"吉尔吉斯斯坦索姆","rate":"13.37208","updatetime":"2022-02-16 16:45:01"},"KHR":{"name":"柬埔寨瑞尔","rate":"640.510000","updatetime":"2022-02-16 11:07:36"},"KMF":{"name":"科摩罗法郎","rate":"68.51072","updatetime":"2022-02-16 16:43:15"},"KYD":{"name":"开曼群岛元","rate":"0.12935","updatetime":"2022-02-16 16:43:15"},"LRD":{"name":"利比里亚元","rate":"24.09099","updatetime":"2022-02-16 16:43:15"},"MDL":{"name":"摩尔多瓦列伊","rate":"2.80551","updatetime":"2022-02-16 16:43:15"},"MGA":{"name":"马尔加什阿里亚","rate":"623.09719","updatetime":"2022-02-16 16:43:15"},"MKD":{"name":"马其顿第纳尔","rate":"8.54826","updatetime":"2022-02-16 16:44:19"},"MVR":{"name":"马尔代夫拉菲亚","rate":"2.45611","updatetime":"2022-02-16 16:43:15"},"MZN":{"name":"新莫桑比克梅蒂卡尔","rate":"9.96798","updatetime":"2022-02-16 16:43:15"},"NAD":{"name":"纳米比亚元","rate":"2.384","updatetime":"2022-02-16 16:44:51"},"SDG":{"name":"苏丹镑","rate":"69.81836","updatetime":"2022-02-16 16:43:15"},"SHP":{"name":"圣圣赫勒拿镑","rate":"0.11661","updatetime":"2022-02-16 16:43:15"},"SLL":{"name":"塞拉利昂利昂","rate":"1809.01677","updatetime":"2022-02-16 16:43:15"},"SRD":{"name":"苏里南元","rate":"3.20067","updatetime":"2022-02-16 16:43:15"},"STD":{"name":"圣多美多布拉","rate":"3379.77379","updatetime":"2022-02-16 16:45:01"},"SZL":{"name":"斯威士兰里兰吉尼","rate":"2.37993","updatetime":"2022-02-16 16:44:54"},"TJS":{"name":"塔吉克斯坦索莫尼","rate":"1.78245","updatetime":"2022-02-16 16:43:15"},"TMT":{"name":"土库曼斯坦马纳特","rate":"0.53003","updatetime":"2022-02-16 16:43:15"},"TOP":{"name":"汤加潘加","rate":"0.36157","updatetime":"2022-02-16 16:43:15"},"TTD":{"name":"特立尼达多巴哥元","rate":"1.06606","updatetime":"2022-02-16 16:43:15"},"UAH":{"name":"乌克兰格里夫纳","rate":"4.49892","updatetime":"2022-02-16 16:43:15"},"UZS":{"name":"乌兹别克斯坦苏姆","rate":"1709.81339","updatetime":"2022-02-16 16:43:15"},"VEF":{"name":"委内瑞拉玻利瓦尔","rate":"39154.00754","updatetime":"2022-02-16 16:43:15"},"VUV":{"name":"瓦努阿图瓦图","rate":"17.75275","updatetime":"2022-02-16 16:43:15"},"WST":{"name":"西萨摩亚塔拉","rate":"0.41342","updatetime":"2022-02-16 16:43:15"},"XAF":{"name":"中非金融合作法郎","rate":"91.02109","updatetime":"2022-02-16 16:43:15"},"XCD":{"name":"东加勒比元","rate":"0.42591","updatetime":"2022-02-16 16:43:15"},"XOF":{"name":"西非法郎","rate":"91.02109","updatetime":"2022-02-16 16:43:15"},"XPF":{"name":"法属波利尼西亚法郎","rate":"16.53968","updatetime":"2022-02-16 16:43:15"},"YER":{"name":"也门里亚尔","rate":"39.43338","updatetime":"2022-02-16 16:43:15"},"ZMW":{"name":"赞比亚克瓦查","rate":"2.8552","updatetime":"2022-02-16 16:43:15"},"SVC":{"name":"萨尔瓦多科朗","rate":"1.38028","updatetime":"2022-02-16 16:43:15"},"GHS":{"name":"加纳塞地","rate":"1.011200","updatetime":"2022-02-16 11:07:36"},"MRU":{"name":"毛里塔尼亚乌吉亚","rate":"5.7041","updatetime":"2022-02-16 16:43:15"},"SEK":{"name":"瑞典克朗","rate":1.4610320000000001,"updatetime":"2022-02-16 16:45:10"},"UGX":{"name":"乌干达先令","rate":553.81488000000002,"updatetime":"2022-02-16 16:45:10"}}}}
                //Console.WriteLine(await Result.Content.ReadAsStringAsync());
                //var json =await Result.Content.ReadFromJsonAsync<Json>();
                var str = await Result.Content.ReadAsStringAsync();
                logger.LogInformation(str);

                var json = Newtonsoft.Json.JsonConvert.DeserializeObject<Json>(str);
                if (json is not null && json.Status == 0 && json.Result is not null && json.Result.List is not null)
                {
                    return json.Result.List;
                }
                else
                {
                    logger.LogWarning("Fail39");
                }
            }
            else
            {
                logger.LogWarning("Fail43");
            }
            return null;
        }

        public class Json
        {
            public int Status { get; set; }
            public string? Msg { get; set; }
            public ResultEntry? Result { get; set; }

            public class ResultEntry
            {
                public string? Currency { get; set; }
                public string? Name { get; set; }
                public Dictionary<string, Item>? List { get; set; }

                public class Item
                {
                    public override string ToString()
                    {
                        return $"{Name}";
                    }
                    public string? Name { get; set; }
                    public decimal Rate { get; set; }
                    public DateTime Updatetime { get; set; }
                    public DateTime RefreceTime { get => Updatetime; }
                }
            }
        }



    }

}

