
using Model = ERP.Models.DB.Orders.Order;
using AsinProducrService = ERP.Services.DB.Stores.AsinProduct;
using AsinModel = ERP.Models.Stores.AsinProduct;
using ProductModel = ERP.Data.Products.Product;
using OrderSDK.Modles.Amazon.vm;
using ERP.Models.ProductNodeType.Amazon;
using OrderSDK.Modles.Amazon;
using AsinInfo = ERP.Models.DB.Product.AsinInfo;
using commonTypeStore = OrderSDK.Modles.CommonType.StoreRegion;

using ListParameter = ERP.Models.Api.Amazon.vm.ListParameter;
using OrderSDK.Modles.Amazon.CatalogItems;
using OrderSDK.Modles.Amazon.Feeds;
using ERP.Services.Api.Amazon;
using OrderSDK.Modles.Amazon.ProductTypes;
using ERP.Models.Stores;
using OrderSDK.Modles;

namespace ERP.Services.Api
{
    public interface IOrder
    {      
        /// <summary>
        /// 分批次拉取方式中，每次拉取数量
        /// </summary>
        int MaxGet { get; }

        /// <summary>
        /// 单次拉取中，拉取数量
        /// </summary>
        int MaxGets { get; }

        commonTypeStore GetStore(StoreRegion store);

        #region 店铺信息
        /// <summary>
        /// 获取授权url
        /// </summary>
        /// <param name="store"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        string GetAuthURL(StoreRegion store, string id);

        /// <summary>
        /// 验证授权
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="store"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        Task<T?> VerifyStore<T>(StoreRegion store, string code = "") where T : class;

        /// <summary>
        /// 刷新token
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        Task<bool> RefreshNormalAccessToken(StoreRegion store);

        /// <summary>
        /// 获取店铺下市场信息
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        Task<List<string>?> GetMarketIds(StoreRegion store);

        #endregion

        #region 拉取订单
        /// <summary>
        /// 测试使用
        /// </summary>
        /// <param name="store"></param>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        Task<Model?> GetTest(StoreRegion store, string orderNo);

        Task<ListResult> List(ListParameter parameter);  // to do... ListResult 最后要搬到models中去

        Task<ListResult> ListNext(ListParameter parameter);

        Model GetOrderBasic(StoreRegion store, WaitOrder order);

        Task<(List<AsinModel>?,string)> GetOrderItem(StoreRegion store, List<Model> orders, AsinProducrService asinProducrService);

        Task GetOrderItem(StoreRegion store, string orderNo, string Marketplaced);
       
      
        Task<string> GetOrderAddress(StoreRegion store, List<Model> orders);



        Task<string> GetOrderBuyerInfo(StoreRegion store, List<Model> orders);




        Task<string> GetOrderFinanciall(StoreRegion store, List<Model> orders);


        Task GetOrderFinanciall(StoreRegion store, string orderNo);


        #endregion

        #region 产品信息

        /// <summary>
        /// 获取指定店铺指定市场下的所有产品
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        Task<List<AsinInfo>> GetAllAsin(StoreRegion store);

        Task<List<ItemRes>?> SearchCatalogItems(StoreRegion store, string marketPlaceIds, List<string> asins);
        /// <summary>
        /// 获取亚马逊产品的具体信息
        /// </summary>
        /// <param name="store"></param>
        /// <param name="asins"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        Task<List<ProductModel>> GetProductInfo(StoreRegion store, List<AsinModel> asins);

        #endregion



        #region 综合使用

        Task<(FeedResult?, string)> SubmitProduicts(StoreRegion store, string data, string marketPlace);

        /// <summary>
        /// 手动更新订单信息
        /// </summary>
        /// <param name="store"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        Task UpdateOrder(StoreRegion store, Model order);


        /// <summary>
        /// 更新亚马逊配送信息
        /// </summary>
        /// <param name="store"></param>
        /// <param name="DP"></param>
        /// <param name="MarketPlaceId"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        Task<string> DeliveryGoods(StoreRegion store, List<DeliverProduct> DP, string MarketPlaceId);

        #endregion


        Task<string> UploadExcelUrl(StoreRegion store, string MarketPlaceId, string fileUrl);

        Task<(string, List<pushProductResult>)> UploadExcelBytes(StoreRegion store, string MarketPlaceId, byte[] bytes);
      
        Task<(FeedResult?, string)> GetFeedDetail(string feedID, StoreRegion store);

        #region 亚马逊节点、产品schemajson 基础信息
        /// <summary>
        /// 搜索并返回具有可用定义的亚马逊产品类型列表
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        Task<ProductTypeList?> SearchDefinitionsProductTypes(StoreRegion store);


        /// <summary>
        /// 检索 Amazon 产品类型定义
        /// </summary>
        /// <param name="store"></param>
        /// <param name="productType"></param>
        /// <returns></returns>
        Task<ProductTypeDefinition?> GetDefinitionsProductTypes(StoreRegion store, string marketPlace, string productTypeName);


        /// <summary>
        /// 获取市场分类节点
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        Task<CategoiesData?> GetReportBowerIDs(StoreRegion store, string marketPlace);

        /// <summary>
        /// 获取产品schemaJson
        /// </summary>
        /// <param name="store"></param>
        /// <param name="marketPlace"></param>
        /// <param name="productTypeName"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        Task<List<AttributeModel>> GetProductTypeModels(StoreRegion store, string marketPlace, string productTypeName);

        #endregion


        

    }
}
