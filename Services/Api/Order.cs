using ERP.Models.DB.Stores;
using Model = ERP.Models.DB.Orders.Order;
using AsinProducrService = ERP.Services.DB.Stores.AsinProduct;
using AsinModel = ERP.Models.Stores.AsinProduct;
using ProductModel = ERP.Data.Products.Product;
using ProductResult = ERP.Data.Products.ProductResult;
using AsinInfo = ERP.Models.DB.Product.AsinInfo;
using OrderSDK.Modles.Amazon.vm;
using OrderSDK.Modles.Amazon;
using ERP.Models.ProductNodeType.Amazon;
using commonTypeStore = OrderSDK.Modles.CommonType.StoreRegion;
using ListParameter = ERP.Models.Api.Amazon.vm.ListParameter;
using OrderSDK.Modles.Amazon.Feeds;
using OrderSDK.Modles.Amazon.CatalogItems;
using ERP.Services.Api.Amazon;
using OrderSDK.Modles.Amazon.ProductTypes;
using ERP.Models.Stores;
using OrderSDK.Modles;

namespace ERP.Services.Api
{
    public abstract class Order : IOrder
    {      
        /// <summary>
        /// 分批次拉取方式中，每次拉取数量
        /// </summary>
        public virtual int MaxGet => throw new NotSupportedException();

        /// <summary>
        /// 单次拉取中，拉取数量
        /// </summary>
        public virtual int MaxGets => throw new NotSupportedException();


        public virtual commonTypeStore GetStore(StoreRegion store)
        {
            throw new NotSupportedException();
        }

        #region 店铺相关
        /// <summary>
        /// 获取授权url
        /// </summary>
        /// <param name="store"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        public virtual string GetAuthURL(StoreRegion store, string id)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// 验证授权
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="store"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        public virtual Task<T?> VerifyStore<T>(StoreRegion store, string code = "") where T : class
        {
            throw new NotSupportedException();
        }


        /// <summary>
        /// 刷新token
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        public virtual Task<bool> RefreshNormalAccessToken(StoreRegion store)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// 获取店铺下市场信息
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        public virtual Task<List<string>?> GetMarketIds(StoreRegion store)
        {
            throw new NotSupportedException();
        }

        #endregion

        #region 拉取订单
        /// <summary>
        /// 测试使用
        /// </summary>
        /// <param name="store"></param>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        public virtual Task<Model?> GetTest(StoreRegion store, string orderNo)
        {
            throw new NotSupportedException();
        }


        public virtual async Task<ListResult> List(ListParameter parameter)
        {
            string msg = "";
            //截止到当前时间==false
            if (parameter.Start >= parameter.End)
            {
                msg = $"结束时间[{parameter.End}]不能早于开始时间[{parameter.Start}]！";
                return new(false, string.Empty, null, msg);
            }
            var now = DateTime.Now;
            if (parameter.End >= now)
            {
                msg = $"结束时间[{parameter.End}]不能超过当前时间[{now}]！";
                return new(false, string.Empty, null, msg);
            }
            return await ListImp(parameter);
        }

        public virtual Task<ListResult> ListImp(ListParameter parameter)
        {
            throw new NotSupportedException();
        }

        public virtual Task<ListResult> ListNext(ListParameter parameter)
        {
            throw new NotSupportedException();
        }

        public virtual Model GetOrderBasic(StoreRegion store, WaitOrder order)
        {
            throw new NotSupportedException();
        }

        public virtual Task<(List<AsinModel>?, string)> GetOrderItem(StoreRegion store, List<Model> orders, AsinProducrService asinProducrService)
        {
            throw new NotSupportedException();
        }

        public virtual Task GetOrderItem(StoreRegion store, string orderNo, string Marketplaced)
        {
            throw new NotSupportedException();
        }

        public virtual Task<string> GetOrderAddress(StoreRegion store, List<Model> orders)
        {
            throw new NotSupportedException();
        }


        public virtual Task<string> GetOrderBuyerInfo(StoreRegion store, List<Model> orders)
        {
            throw new NotSupportedException();
        }




        public virtual Task<string> GetOrderFinanciall(StoreRegion store, List<Model> orders)
        {
            throw new NotSupportedException();
        }

        public virtual Task GetOrderFinanciall(StoreRegion store, string orderNo)
        {
            throw new NotSupportedException();
        }
        #endregion

        #region 产品信息

        /// <summary>
        /// 获取指定店铺指定市场下的所有产品
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        public virtual Task<List<AsinInfo>> GetAllAsin(StoreRegion store)
        {
            throw new NotSupportedException();
        }

        public virtual Task<List<ItemRes>?> SearchCatalogItems(StoreRegion store, string marketPlaceIds, List<string> asins)
        {
            throw new NotSupportedException();
        }
       

        /// <summary>
        /// 获取亚马逊产品的具体信息
        /// </summary>
        /// <param name="store"></param>
        /// <param name="asins"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        public virtual Task<List<ProductModel>> GetProductInfo(StoreRegion store, List<AsinModel> asins)
        {
            throw new NotSupportedException();
        }

        #endregion



        #region 综合使用

        public virtual Task<(FeedResult?, string)> SubmitProduicts(StoreRegion store, string data, string marketPlace)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// 手动更新订单信息
        /// </summary>
        /// <param name="store"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        public virtual Task UpdateOrder(StoreRegion store, Model order)
        {
            throw new NotSupportedException();
        }


        /// <summary>
        /// 更新亚马逊配送信息
        /// </summary>
        /// <param name="store"></param>
        /// <param name="DP"></param>
        /// <param name="MarketPlaceId"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        public virtual Task<string> DeliveryGoods(StoreRegion store, List<DeliverProduct> DP, string MarketPlaceId)
        {
            throw new NotSupportedException();
        }

        #endregion



        public virtual  Task<string> UploadExcelUrl(StoreRegion store, string MarketPlaceId, string fileUrl)
        {
            throw new NotSupportedException();
        }


        public virtual Task<(string, List<pushProductResult>)> UploadExcelBytes(StoreRegion store, string MarketPlaceId, byte[] bytes)
        {
            throw new NotSupportedException();
        }      

        public virtual Task<(FeedResult?, string)> GetFeedDetail(string feedID, StoreRegion store)
        {
            throw new NotSupportedException();
        }

        #region 亚马逊节点、产品schemajson 基础信息

        public virtual Task<ProductTypeList?> SearchDefinitionsProductTypes(StoreRegion store)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="store"></param>
        /// <param name="productType"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        public virtual Task<ProductTypeDefinition?> GetDefinitionsProductTypes(StoreRegion store, string marketPlace, string productTypeName)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// 获取市场分类节点
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        public virtual Task<CategoiesData?> GetReportBowerIDs(StoreRegion store, string marketPlace)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// 获取产品schemaJson
        /// </summary>
        /// <param name="store"></param>
        /// <param name="marketPlace"></param>
        /// <param name="productTypeName"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        public virtual Task<List<AttributeModel>> GetProductTypeModels(StoreRegion store, string marketPlace, string productTypeName)
        {
            throw new NotSupportedException();
        }



        #endregion



        #region UploadExcelUrl


        #endregion

    }
}
