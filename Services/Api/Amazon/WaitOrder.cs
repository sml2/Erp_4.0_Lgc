﻿
using System;

using AmazonNewSPOrder = OrderSDK.Modles.Amazon.Order.Order;
using ShopeeOrder = OrderSDK.Modles.Shopee.Order.ResponseGetOrderList.Order;

namespace ERP.Services.Api.Amazon
{
    public class WaitOrder
    {
        private readonly object param;

        public WaitOrder(object param)
        {
            this.param = param;
        }
        private T GetParam<T>() => (T)param;     
        public AmazonNewSPOrder GetAmazoNewParam() => GetParam<AmazonNewSPOrder>();
        public ShopeeOrder GetShopeeParam() => GetParam<ShopeeOrder>();
    }
}

