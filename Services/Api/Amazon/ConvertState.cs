﻿using static OrderSDK.Modles.Amazon.Order.Order;
using oState = ERP.Enums.Orders.States;
using shipState = ERP.Enums.Orders.ShippingTypes;

namespace ERP.Services.Api.Amazon
{
    public class ConvertState
    {
        public static shipState? ShippingTypes(FulfillmentChannelEnum? FulfillmentChannel)
        {
            if (FulfillmentChannel.IsNotNull())
            {
                return FulfillmentChannel == FulfillmentChannelEnum.MFN ? shipState.MFN : shipState.AFN;
            }
            return null;
        }
        public static oState GetState(OrderStatusEnum orderStatus)
        {
            oState states = new oState();
            switch (orderStatus)
            {
                //已下订单，但尚未授权付款
                case OrderStatusEnum.Pending:
                    states = oState.UNPAID;
                    break;
                //已授权付款，订单已准备好发货，但订单中的任何项目尚未发货
                case OrderStatusEnum.Unshipped:
                    states = oState.UNSHIPPED;
                    break;
                //订单中的一个或多个，但不是全部物品已装运               
                case OrderStatusEnum.PartiallyShipped:
                    states = oState.PARTIALLYSHIPPED;
                    break;
                //订单中的所有项目均已装运
                case OrderStatusEnum.Shipped:
                    states = oState.SENT;
                    break;
                //订单已取消
                case OrderStatusEnum.Canceled:
                    states = oState.CANCELED;
                    break;
                //订单无法履行。此状态仅适用于多渠道履行订单
                case OrderStatusEnum.Unfulfillable:
                    states = oState.PROGRAM;
                    break;

                //订单中的所有项目均已发货。卖方尚未向亚马逊确认发票已发货给买方
                case OrderStatusEnum.InvoiceUnconfirmed:
                    states = oState.SENT;
                    break;
                //此状态仅适用于预订单。订单已下，付款尚未授权，项目的发布日期为未来
                case OrderStatusEnum.PendingAvailability:
                    states = oState.UNPAID;
                    break;
                default: throw new ArgumentOutOfRangeException(nameof(orderStatus));
            }
            return states;
        }
    }
}
