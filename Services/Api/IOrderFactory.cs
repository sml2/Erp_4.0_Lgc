using ERP.Services.Api.Amazon;

namespace ERP.Services.Api;


public enum GetTypes
{
    Single, Multiple
}


public interface IOrderFactory
{
    IOrder Create(Platforms platforms);
}
public record ListResult
{
    public bool HasNext { get; set; }
    public string NextCycleInfo { get; set; }
    public bool HasOrder { get => waitOrders.IsNotEmpty(); }
    public IList<WaitOrder>? waitOrders { get; set; }

    public string Msg { get; set; }

    public ListResult(bool hasNext, string nextCycleInfo, IList<WaitOrder>? waitOrders, string msg = "")
    {
        HasNext = hasNext;
        NextCycleInfo = nextCycleInfo;
        this.waitOrders = waitOrders;
        Msg = msg;
    }

    public ListResult(string msg) : this(false, string.Empty, null, msg) { }
}