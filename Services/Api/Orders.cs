
namespace ERP.Services.Api
{

    public class Orders : IOrderFactory
    {

        private IOrder? _orderAmazonSP;

        private IOrder OrderAmazonSP
        {
            get
            {
                if (_orderAmazonSP is null)
                    _orderAmazonSP = ActivatorUtilities.CreateInstance<AmazonOrder>(serviceProvider);
                return _orderAmazonSP;
            }
        }

        private readonly IServiceProvider serviceProvider;

        public Orders(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public IOrder Create(Platforms platforms)
        {
            return platforms switch
            {
                Platforms.AMAZONSP => OrderAmazonSP,
                _ => throw new NotImplementedException($"暂未对接的平台{platforms}")
            };
        }

    }
}
