﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imp_Amazon
{
    public class DetailExt
    {
        /// <summary>
        /// 通过ID获取完整Div
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        public static string GetWholenessDivByID(Sy.String Html, string id)
        {
            string Check_ID = $"id=\"{id}\"";
            if (Html.Contains(Check_ID))
            {
                var DescDirty = Html.After(Check_ID).After(">");
                const string BeginDiv = "<div", EndDiv = "</div>";
                if (DescDirty.Contains(BeginDiv))
                {
                    var Description_Temp = new StringBuilder($"{BeginDiv} {Check_ID}>");
                    int Leave = 0;
                    foreach (var divOuter in DescDirty.GetSplitIterator(BeginDiv).Skip(1))
                    {
                        Leave += 1;
                        if (divOuter.Contains(EndDiv))
                        {
                            Description_Temp.Append(BeginDiv);
                            var Lst = divOuter.GetSplitIterator(EndDiv).ToList();
                            for (int i = 0; i < Lst.Count; i++)
                            {
                                Leave -= 1;
                                Description_Temp.Append($"{Lst[i]}{EndDiv}");
                                if (Leave <= 0) { break; }
                            }
                            if (Leave <= 0) { break; }
                        }
                    }
                    return Description_Temp.ToString();
                }
            }
            return string.Empty;
        }
    }
}
