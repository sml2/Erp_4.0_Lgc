﻿using System.Xml.Serialization;
using System.Text;
using System.Security.Cryptography;
using System.Globalization;
using System.Net;
using System.Linq;
using static Mod_GlobalVariables;
using Sy.Threading;
using Sy.Net;

namespace Imp_Amazon
{
    public class MWS
    {
        static long BadProxy = 0;
        private static string HTTPReuest(string Url,Func<HTTP, HTTP> Lgc,LogSub Log)
        {
            int TryCount = 1;
            bool CheckFun(string Temp){
                bool Reslut = !string.IsNullOrEmpty(Temp) && !Temp.Contains("RequestThrottled");// && !Temp.ToLower().Contains("badproxy");
                if (!Reslut && Log.IsNotNull())
                    Log(I18N.Instance.get_MWS_Log69(TryCount.ToString()));//$"请求失败,正在重试第[{TryCount}]次..."
                TryCount++;
                return Reslut;
            }
            string page = null;
            if (BadProxy <= 0) {
                page = Lgc(Factory.Http(Url).SetProxy(new WebProxy("proxy.miwaimao.com", 443))).Try(CheckFun);
                //---------443的标准webProxy
                //0 永远用标准代理
                //1 仅当出错时重试一次HttpProxyApi
                //9999999999 出错一次后，一直使用HttpProxyApi(使用9999999999次)
                //---------80的HttpProxyApi
                if (page.ToLower().Contains("badproxy") || page.Length < 0) BadProxy+=22;
            }
            if (BadProxy>0) {
                //全站加速
                page = Lgc(Factory.Http("http://httpproxy.miwaimao.com/").SetHeader("ProxyUrl", Url)).Try(CheckFun);
                BadProxy--;
            }
            return page;
        }
     
        public static INF_Mws.Result Upload(string xml, string FeedType, Imp_ShopInfo shop, out string FeedSubmissionId, ref string ErrMessage, LogSub Log = null )
        {
            INF_Mws.Result re = INF_Mws.Result.Fail;
            FeedSubmissionId = null;
            OrderParameter OP = new OrderParameter(shop,DateTime.Now, DateTime.Now, null,null, null);
            //获取Response
            bool IsNotFinished = true;
            do
            {
                Sy.String html = Gethtml(OP, "SubmitFeed", "", null, "", xml, FeedType, null, null, "", Log);
                SubmitFeedResponse response = new SubmitFeedResponse();
                try
                {
                    using (StringReader sr = new StringReader(html))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(SubmitFeedResponse));
                        response = serializer.Deserialize(sr) as SubmitFeedResponse;
                        FeedSubmissionId = response.SubmitFeedResult.FeedSubmissionInfo.FeedSubmissionId;
                        re = INF_Mws.Result.Success;
                        IsNotFinished = false;
                    }
                }
                catch { response = null; }
                if (response == null)
                {
                    try
                    {
                        using (StringReader sr = new StringReader(html))
                        {
                            XmlSerializer serializer = new XmlSerializer(typeof(UploadErrorResponse));
                            UploadErrorResponse ErRe = serializer.Deserialize(sr) as UploadErrorResponse;
                            if(Log.IsNotNull()) 
                                Log(I18N.Instance.get_MWS_Log46(ErRe.Error.Code,ErRe.Error.Message));
                            //$"请求失败,失败原因:[错误码:{}]错误消息:{}"
                            if (ErRe.Error.Code == "RequestThrottled")
                            {
                                if (Log.IsNotNull()) Log(I18N.Instance.MWS_Log60);
                                //"上传商品频率过高导致请求失败,正在执行延迟操作..."
                                //(注意:重新上传请更新商品的SKU及UPC或EAN码)
                                Thread.Sleep(12 * 60 * 1000);
                                if (Log.IsNotNull()) Log(I18N.Instance.MWS_Log48);
                                continue;
                            }
                            else if (ErRe.Error.Code == "QuotaExceeded")//You exceeded your quota of 30.0 requests per 1 hour for operation Feeds/2009-01-01/SubmitFeed. Your quota will reset on Fri Mar 20 09:52:00 UTC 2020
                            {
                                //Log("该请求已经超出亚马逊后台设置的每1小时30个请求的配额,正在执行延迟操作...");//(注意:重新上传请更新商品的SKU及UPC或EAN码)
                                //Thread.Sleep(12 * 60 * 1000);
                                if (Log.IsNotNull()) Log(I18N.Instance.MWS_Log49);
                                continue;
                            }
                            else IsNotFinished = false;
                            FeedSubmissionId = ErRe.RequestID;
                        }
                    }
                    catch
                    {
                        //网络因素请求失败
                        FeedSubmissionId = null;
                        IsNotFinished = false;
                        ErrMessage = I18N.Instance.MWS_Log50;
                        if (Log.IsNotNull()) Log(I18N.Instance.MWS_Log50);
                    }
                }
            }
            while (IsNotFinished);
            return re;
        }
        public static INF_Mws.Result Upload(string pdfDocument, string FeedType, INF_ShopInfo shop, string FeedOptions, out string FeedSubmissionId, ref string ErrMessage, LogSub Log = null)
        {
            INF_Mws.Result re = INF_Mws.Result.Fail;
            FeedSubmissionId = null;
            OrderParameter OP = new OrderParameter(shop, DateTime.Now, DateTime.Now, null, null, null);
            Sy.String html = Gethtml(OP, "SubmitFeed", "", null, "", pdfDocument, FeedType, null, null, FeedOptions, Log);
            SubmitFeedResponse response = new SubmitFeedResponse();
            try
            {
                using (StringReader sr = new StringReader(html))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(SubmitFeedResponse));
                    response = serializer.Deserialize(sr) as SubmitFeedResponse;
                    FeedSubmissionId = response.SubmitFeedResult.FeedSubmissionInfo.FeedSubmissionId;
                    re = INF_Mws.Result.Success;
                }
            }
            catch { response = null; }
            if (response == null)
            {
                try
                {
                    using (StringReader sr = new StringReader(html))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(UploadErrorResponse));
                        UploadErrorResponse ErRe = serializer.Deserialize(sr) as UploadErrorResponse;
                        ErrMessage = $"[{ErRe.Error.Code}]{ErRe.Error.Message}";
                    }
                }
                catch
                {
                    //网络因素请求失败
                    FeedSubmissionId = null;
                    ErrMessage = I18N.Instance.MWS_Log50;
                }
            }
            return re;
        }
        /// <summary>
        /// GetFeedSubmissionList：获取上传状态_SUBMITTED_|_IN_PROGRESS_|_DONE_
        /// </summary>
        /// <param name="FeedSubmissionId"></param>
        /// <param name="shop"></param>
        /// <returns></returns>
        public static string UpdateResult(string FeedSubmissionId, Imp_ShopInfo shop, DateTime Time)
        {
            DateTime FromDate = Time.AddHours(-1);
            DateTime ToDate = Time.AddHours(+1);
            OrderParameter OP = new OrderParameter(shop, FromDate, ToDate, null, null, null);
            string html = Gethtml(OP, "GetFeedSubmissionList");
            GetFeedSubmissionListResponse response = new GetFeedSubmissionListResponse();
            try
            {
                using (StringReader sr = new StringReader(html))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(GetFeedSubmissionListResponse));
                    response = serializer.Deserialize(sr) as GetFeedSubmissionListResponse;
                }
            }
            catch
            {
                return I18N.Instance.MWS_Log51;
            }
            foreach (var Item in response.GetFeedSubmissionListResult.FeedSubmissionInfo)
            {
                if (Item.FeedSubmissionId == FeedSubmissionId)
                {
                    return Item.FeedProcessingStatus;
                }
            }
            return "";
        }
        /// <summary>
        /// 查询上传状态
        /// </summary>
        /// <param name="FeedSubmissionId"></param>
        /// <param name="Shop"></param>
        /// <returns></returns>
        public static string UpdateResult(string FeedSubmissionId, Imp_ShopInfo Shop,ref string ErrMessage)
        {
            OrderParameter OP = new OrderParameter(Shop, DateTime.Now, DateTime.Now, null, null, null);
            string html = Gethtml(OP, "GetFeedSubmissionList", FeedSubmissionId);
            GetFeedSubmissionListResponse response = new GetFeedSubmissionListResponse();
            try
            {
                using (StringReader sr = new StringReader(html))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(GetFeedSubmissionListResponse));
                    response = serializer.Deserialize(sr) as GetFeedSubmissionListResponse;
                }
            }
            catch
            {
                ErrMessage = I18N.Instance.MWS_Log51;
                if (html.Contains("AccessDenied"))
                {
                    ErrMessage = "AccessDenied";
                }
                return null;
            }
            return response.GetFeedSubmissionListResult.FeedSubmissionInfo[0].FeedProcessingStatus;
        }
        /// <summary>
        /// 查询上传是否成功
        /// </summary>
        /// <param name="FeedSubmissionId"></param>
        /// <param name="outmessage"></param>
        /// <param name="shop"></param>
        /// <returns></returns>
        public static bool GetSubmitResult(string FeedSubmissionId, LogSub Log, Imp_ShopInfo shop)
        {
            OrderParameter OP = new OrderParameter(shop, DateTime.Now, DateTime.Now, null, null, null);
            string html = Gethtml(OP, "GetFeedSubmissionResult", "", null, "", "", FeedSubmissionId);
            Cls_AmazonEnvelope response = new Cls_AmazonEnvelope();
            try
            {
                using (StringReader sr = new StringReader(html))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Cls_AmazonEnvelope));
                    response = serializer.Deserialize(sr) as Cls_AmazonEnvelope;
                }
            }
            catch
            {
                Log(I18N.Instance.MWS_Log51);
                return false;
            }
            //string SkuCode = "";
            if (response.Message != null && response.Message.Count > 0)
            {
                bool flag = true;
                foreach (var m in response.Message)
                {
                    if (m.ProcessingReport.Result != null && m.ProcessingReport.Result.Count > 0)
                    {
                        flag = false;
                        Log(I18N.Instance.MWS_Log52);
                        foreach (var r in m.ProcessingReport.Result)
                        {
                            if (r.ResultCode == "Warning") continue;
                            string Message = $"ID:{r.MessageID}\r\n{I18N.Instance.MWS_Log53}:{r.ResultMessageCode}\r\n{I18N.Instance.MWS_Log54}:{r.ResultDescription}";
                            Log(Message);
                            //if (r.AdditionalInfo != null)
                            //{
                            //    SkuCode += $"\"{r.AdditionalInfo.SKU}\",";
                            //}
                        }
                    }
                }
                if (flag)
                {
                    return true;
                }
            }
            return false;
        }
        public static Dictionary<string,string> GetSubmitResult(string FeedSubmissionId, Imp_ShopInfo shop, ref string ErrMessage)
        {
            Dictionary<string, string> ErrDic = new Dictionary<string, string>();
            OrderParameter OP = new OrderParameter(shop, DateTime.Now, DateTime.Now, null, null, null);
            string html = Gethtml(OP, "GetFeedSubmissionResult", "", null, "", "", FeedSubmissionId);
            Cls_AmazonEnvelope response = new Cls_AmazonEnvelope();
            try
            {
                if (html.ToLower() == "badproxy")
                {
                    ErrMessage = $"{I18N.Instance.MWS_Log50}[BadProxy]";
                    return null;
                }
                using (StringReader sr = new StringReader(html))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Cls_AmazonEnvelope));
                    response = serializer.Deserialize(sr) as Cls_AmazonEnvelope;
                }
            }
            catch
            {
                //ErrMessage = "查询失败";
                return null;
            }
            //string SkuCode = "";
            if (response.Message != null && response.Message.Count > 0)
            {
                foreach (var m in response.Message)
                {
                    if (m.ProcessingReport.Result != null && m.ProcessingReport.Result.Count > 0)
                    {
                        foreach (var r in m.ProcessingReport.Result)
                        {
                            string Sku = r.AdditionalInfo?.SKU;
                            int Success = Convert.ToInt32(m.ProcessingReport.ProcessingSummary?.MessagesSuccessful ?? "0");
                            string Message = $"{I18N.Instance.MWS_Log53}:{r.ResultMessageCode};{I18N.Instance.MWS_Log54}:{r.ResultDescription}";
                            
                            if (Sku.IsNull() && Success == 0) ErrMessage += $"AllError:{Message}"; //return null;
                            else if (Sku.IsNotNull())
                            {
                                if (ErrDic.ContainsKey(Sku)) ErrDic[Sku] += "/r/n" + Message;
                                else ErrDic.Add(Sku, Message);
                            }
                        }
                    }
                }
            }
            return ErrDic;
        }
        /*
        Feed Processing Summary:
        Number of records processed		1
        Number of records successful		0

        original-record-number	sku	error-code	error-type	error-message
        1		79525	Error	Amazon will generate invoice for shipmentId N/A, orderId N/A
         */
        public static string GetSubmitResult(string FeedSubmissionId, Imp_ShopInfo shop)
        {
            OrderParameter OP = new OrderParameter(shop, DateTime.Now, DateTime.Now, null, null, null);
            Sy.String html = Gethtml(OP, "GetFeedSubmissionResult", "", null, "", "", FeedSubmissionId);
            string SpliterSuccessS = "Number of records successful		";
            if (html.Contains(SpliterSuccessS))
            {
                var Temp = html.After(SpliterSuccessS);
                if (Temp.StartsWith("1")) return null;
            }
            return "Error";
        }
        public static Dictionary<string, string> GetAsin(List<string> skus, Imp_ShopInfo shop)
        {
            Dictionary<string, string> Asins = new Dictionary<string, string>();
            OrderParameter OP = new OrderParameter(shop, DateTime.Now, DateTime.Now, null, null, null);
            do
            {
                //每次最多获取五个Asin
                var TempSkus = skus.Take(5).ToList();
                Sy.String html = Gethtml(OP, "GetMatchingProductForId", "", null, "", "", "", TempSkus, null);
                string SpliterSku = "<GetMatchingProductForIdResult ";
                if (html.Contains(SpliterSku))
                {
                    var SkuPages = html.Split(SpliterSku).Skip(1);
                    foreach (var SkuPage in SkuPages)
                    {
                        string TempSku = "";
                        string Status = "";
                        string TempAsin = "";
                        //Sku
                        string SpliterSkuS = "Id=\"", SpliterSkuE = "\"";
                        if (SkuPage.CanMid(SpliterSkuS, SpliterSkuE)) TempSku = SkuPage.Mid(SpliterSkuS, SpliterSkuE);
                        //status
                        string SpliterStatusS = "status=\"", SpliterStatusE = "\"";
                        if (SkuPage.CanMid(SpliterStatusS, SpliterStatusE)) Status = SkuPage.Mid(SpliterStatusS, SpliterStatusE);
                        if (Status != "Success") continue;
                        //Status获取成功记录Asin
                        string SpliterAsinS = "<ASIN>", SpliterAsinE = "</ASIN>";
                        if (SkuPage.CanMid(SpliterAsinS, SpliterAsinE)) TempAsin = SkuPage.Mid(SpliterAsinS, SpliterAsinE);
                        if (TempSku.IsNotWhiteSpace() && TempAsin.IsNotWhiteSpace() && !Asins.ContainsKey(TempSku))
                        {
                            Asins.Add(TempSku, TempAsin);
                        }
                    }
                }
                skus.RemoveRange(0, TempSkus.Count);
            } while (skus.Count > 0);
            return Asins;
        }
        /// <summary>
        /// 获取Asin:Asin的位置=ParentSku
        /// </summary>
        /// <param name="Sku"></param>
        /// <param name="shop"></param>
        /// <param name="Log"></param>
        /// <returns></returns>
        public static string GetAsin(string Sku, Imp_ShopInfo shop, LogSub Log)
        {
            OrderParameter OP = new OrderParameter(shop, DateTime.Now, DateTime.Now, null, null, null);
            Log(I18N.Instance.MWS_Log55);
            Sy.String html = Gethtml(OP, "GetMatchingProductForId", "", null, "", "","", new List<string>() { Sku }, null, "", Log);
            string SpliterS = "<ASIN>", SpliterE = "</ASIN>";
            if (html.IsNotEmpty() && html.CanMid(SpliterS, SpliterE))
            {
                string Asin = html.Mid(SpliterS, SpliterE);
                Log($"{I18N.Instance.MWS_Log56},Asin:[{Asin}]");
                return Asin;
            }
            Log(I18N.Instance.MWS_Log57);
            return null;
        }
        public class ComparerString : SingletonObject<ComparerString>, IComparer<string>
        {
            public int Compare(string x, string y)
            {
                return string.CompareOrdinal(x, y);
            }
        }
        /// <summary>
        /// 获取对应事件下的构建URL参数（及签名参数）
        /// </summary>
        /// <param name="OP"></param>
        /// <param name="action"></param>
        /// <param name="AmazonID"></param>
        /// <param name="Asin"></param>
        /// <param name="NextToken"></param>
        /// <param name="ContentMD5Value"></param>
        /// <param name="FeedType"></param>
        /// <returns></returns>
        private static SortedList<string, string> GetSortList(OrderParameter OP, string action, string AmazonID = "", List<string> Asins = null, string NextToken = "", string ContentMD5Value = "", string FeedType = "", List<string> Skus = null, List<string> AmazonOrderIDs = null, string FeedOptions = "")
        {
            //第三方信息
            string AWSAccessKeyId = OP.Shop.AwsAccessKey.Trim();
            //卖家信息
            string SellerId = OP.Shop.SellerID.Trim();// "A19I5LAQZSOD9A";
            string MWSAuthToken = OP.Shop.MwsAuthToken.Trim();
            if (!string.IsNullOrEmpty(MWSAuthToken))
            {
                if (!MWSAuthToken.StartsWith("amzn.mws.")) { MWSAuthToken = "amzn.mws." + MWSAuthToken; }
            }
            //亚马逊提供信息
            string MarketplacedId = OP.Shop.MarketPlace.ID.Trim();
            string Timestamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ", DateTimeFormatInfo.InvariantInfo);
            string AfterTime = Convert.ToDateTime(OP.CreateAfterTime).ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ", DateTimeFormatInfo.InvariantInfo);
            string BeforeTime = Convert.ToDateTime(OP.CreateBeforeTime).ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ", DateTimeFormatInfo.InvariantInfo);

            SortedList<string, string> keyValuePairs = new SortedList<string, string>(ComparerString.Instance);
            keyValuePairs["AWSAccessKeyId"] = AWSAccessKeyId;
            keyValuePairs["Action"] = action;
            if (action == "ListOrders")
            {
                keyValuePairs["MarketplaceId.Id.1"] = MarketplacedId;
                keyValuePairs["OrderStatus.Status.1"] = "Unshipped";
                if (AfterTime != BeforeTime)//手动拉取订单逻辑
                {
                    keyValuePairs["CreatedAfter"] = AfterTime;
                    keyValuePairs["CreatedBefore"] = BeforeTime;
                    keyValuePairs["OrderStatus.Status.2"] = "PartiallyShipped";
                    keyValuePairs["OrderStatus.Status.3"] = "Shipped";
                    keyValuePairs["OrderStatus.Status.4"] = "Unfulfillable";
                    keyValuePairs["OrderStatus.Status.5"] = "Canceled";
                }
                else keyValuePairs["CreatedAfter"] = Convert.ToDateTime(OP.CreateAfterTime.AddMonths(-1)).ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ", DateTimeFormatInfo.InvariantInfo); //默认获取2个月内未发货订单
            }
            else if (action == "ListOrdersByNextToken")
            {
                keyValuePairs["NextToken"] = NextToken;
            }
            else if (action == "ListOrderItems")
            {
                keyValuePairs["AmazonOrderId"] = AmazonID;
            }
            else if (action == "ListFinancialEvents")
            {
                keyValuePairs["AmazonOrderId"] = AmazonID;
            }
            else if (action == "ListOrderItemsByNextToken")
            {
                keyValuePairs["NextToken"] = NextToken;
            }
            else if (action == "GetMatchingProduct" || action == "GetCompetitivePricingForASIN")//image
            {
                for (int i = 0; i < Asins.Count; i++)
                {
                    keyValuePairs[$"ASINList.ASIN.{i + 1}"] = Asins[i];
                }
                keyValuePairs["MarketplaceId"] = MarketplacedId;
            }
            else if (action == "GetMatchingProductForId")
            {
                keyValuePairs["MarketplaceId"] = MarketplacedId;
                keyValuePairs["IdType"] = "SellerSKU";
                if (Skus.IsNotNull())
                {
                    for (int i = 0; i < Skus.Count; i++)
                    {
                        keyValuePairs[$"IdList.Id.{i + 1}"] = Skus[i];
                    }
                }
            }
            else if (action == "GetOrder")
            {
                //keyValuePairs["AmazonOrderId.Id.1"] = AmazonID;
                for (int i = 0; i < AmazonOrderIDs.Count; i++)
                {
                    keyValuePairs[$"AmazonOrderId.Id.{i + 1}"] = AmazonOrderIDs[i];
                }
            }
            else if (action == "SubmitFeed")//上传商品
            {
                //计算ContentMD5
                keyValuePairs["ContentMD5Value"] = ContentMD5Value;
                keyValuePairs["FeedType"] = FeedType;
                keyValuePairs["MarketplaceIdList.Id.1"] = MarketplacedId;
                if (FeedType == "_UPLOAD_VAT_INVOICE_")
                {
                    keyValuePairs["PurgeAndReplace"] = "true";
                    keyValuePairs["FeedOptions"] = FeedOptions;
                }
                else keyValuePairs["PurgeAndReplace"] = "false";
            }
            else if (action == "GetFeedSubmissionList")
            {
                if (AmazonID.IsNullOrEmpty())
                {
                    keyValuePairs["MaxCount"] = "100";
                    keyValuePairs["SubmittedFromDate"] = AfterTime;
                    keyValuePairs["SubmittedToDate"] = BeforeTime;
                }
                else
                {
                    keyValuePairs["FeedSubmissionIdList.Id.1"] = AmazonID;
                }
            }
            else if (action == "GetFeedSubmissionResult")
            {
                keyValuePairs["FeedSubmissionId"] = FeedType;
            }
            //通用值
            if (!string.IsNullOrEmpty(MWSAuthToken)) keyValuePairs["MWSAuthToken"] = MWSAuthToken;
            keyValuePairs["SignatureMethod"] = "HmacSHA256";
            keyValuePairs["SignatureVersion"] = "2";
            keyValuePairs["Timestamp"] = Timestamp;//"2019-05-09T09:22:16Z";
            if (action == "SubmitFeed" || action == "GetFeedSubmissionList" || action == "GetFeedSubmissionResult")
            {
                keyValuePairs["Version"] = "2009-01-01";
                keyValuePairs["Merchant"] = SellerId;
            }
            else
            {
                keyValuePairs["SellerId"] = SellerId;
                if (action == "GetMatchingProductForId") keyValuePairs["Version"] = "2011-10-01";
                else if (action == "ListFinancialEvents") keyValuePairs["Version"] = "2015-05-01";
                else if (action == "ListMarketplaceParticipations") keyValuePairs["Version"] = "2011-07-01";
                else if (Asins.IsNull()) keyValuePairs["Version"] = "2013-09-01";
                else { keyValuePairs["Version"] = "2011-10-01"; }
            }
            return keyValuePairs;
        }
        /// <summary>
        /// 请求获取Html值
        /// </summary>
        /// <param name="OP">第三方&卖家信息</param>
        /// <param name="action">执行事件</param>
        /// <param name="AmazonID"></param>
        /// <param name="Asin"></param>
        /// <param name="NextToToken"></param>
        /// <param name="xml">POST请求数据</param>
        /// <param name="FeedType">上传数据中表示上传类型;获取上传结果时表示FeedSubmissionId</param>
        /// <returns></returns>
        public static string Gethtml(OrderParameter OP, string action, string AmazonID = "", List<string> Asins = null, string NextToToken = "", string xml = "", string FeedType = "", List<string> Skus = null, List<string> AmazonOrderIDs = null, string FeedOptions="", LogSub Log = null)
        {
            string SecretKey = OP.Shop.SecretKey.Trim();
            string host = OP.Shop.MarketPlace.URL.Trim();
            string Header = "";
            string Methods = "";
            //POST数据（该数据的编码方式与上传数据保持一致,用于亚马逊后台校验）
            byte[] _buffer = Encoding.UTF8.GetBytes(xml);
            MemoryStream stream = new MemoryStream(_buffer);
            //计算ContentMD5（计算MD5的编码方式==国家对应的编码方式）
            MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider();
            byte[] hash = provider.ComputeHash(stream);
            //provider.Dispose();
            string ContentMD5Value = Convert.ToBase64String(hash);

            if (action == "SubmitFeed" || action == "GetFeedSubmissionList" || action == "GetFeedSubmissionResult")
            {
                Methods = "POST";
                Header = "/";
            }
            else
            {
                Methods = "GET";
                Header = "/Orders/2013-09-01";
                if (Asins.IsNotNull() || action == "GetMatchingProductForId") Header = "/Products/2011-10-01";
                if (action == "ListFinancialEvents") Header = "/Finances/2015-05-01";
                if (action == "ListMarketplaceParticipations") Header = "/Sellers/2011-07-01";
            }
            SortedList<string, string> keyValuePairs = GetSortList(OP, action, AmazonID, Asins, NextToToken, ContentMD5Value, FeedType, Skus, AmazonOrderIDs, FeedOptions);

            //计算签字
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(Methods);
            stringBuilder.Append("\n");
            stringBuilder.Append(host.ToLower());
            stringBuilder.Append("\n");
            stringBuilder.Append(Header);
            stringBuilder.Append("\n");
            StringBuilder TempString = new StringBuilder();
            foreach (KeyValuePair<string, string> kv in keyValuePairs)
            {
                if (TempString.Length > 0)
                {
                    TempString.Append('&');
                }
                TempString.Append(kv.Key + "=" + Sy.Text.Encode.Url_Encode(kv.Value));
            }
            stringBuilder.Append(TempString.ToString());
            HMACSHA256 hMACSHA = new HMACSHA256(OP.Shop.MarketPlace.Encoding.GetBytes(SecretKey));
            var data = OP.Shop.MarketPlace.Encoding.GetBytes(stringBuilder.ToString());
            byte[] buffer = hMACSHA.ComputeHash(data);
            keyValuePairs["Signature"] = Sy.Text.Encode.Base64_Encode(buffer);//Signature
            stringBuilder.Length = 0;
            foreach (KeyValuePair<string, string> kv in keyValuePairs)
            {
                if (stringBuilder.Length > 0)
                {
                    stringBuilder.Append('&');
                }
                stringBuilder.Append(kv.Key + "=" + Sy.Text.Encode.Url_Encode(kv.Value));
            }
            string url = "https://" + host + Header + "?" + stringBuilder.ToString();
            string html = "";
            if (Methods == "POST")
            {
                HttpWebResponse response = null;
                html = HTTPReuest(url, (http) => http.SetData(xml).Set((r) =>
                {
                    r.Headers["Content-MD5"] = ContentMD5Value;
                    r.UserAgent = "x-amazon-user-agent: AmazonJavascriptScratchpad/1.0 (Language=Javascript)";
                    r.ContentType = "text/xml";
                }).Set(Encoding.UTF8).SetAfterResponse((r) => { response = r; return true; }), Log);
                if (action == "SubmitFeed" && response.IsNotNull())
                {
                    Thread.Sleep(10000);
                    //判断亚马逊请求当前剩余请求次数
                    var Headers = response.Headers;
                    string HeaderValue = "x-mws-quota-remaining";
                    if (Headers.AllKeys.Contains(HeaderValue))
                    {
                        string Value = response.Headers[HeaderValue];
                        try
                        {
                            int Times = Convert.ToInt32(Convert.ToDouble(Value));
                            //if(Log.IsNotNull()) Log(Times.ToString());
                            if (Times <= 0)
                            {
                                if (Log.IsNotNull()) Log(I18N.Instance.MWS_Log58);
                                Thread.Sleep(12 * 60 * 1000);
                                if (Log.IsNotNull()) Log(I18N.Instance.MWS_Log59);
                            }
                        }
                        catch { }
                    }
                }
            }
            else
            {
                //html = Sy.Net.Factory.Http(url).Set(Encoding.UTF8).Try(checkfun);
                html = HTTPReuest(url,(h)=>h.Set(Encoding.UTF8),Log);
            }
            return html;
        }
    }
}
