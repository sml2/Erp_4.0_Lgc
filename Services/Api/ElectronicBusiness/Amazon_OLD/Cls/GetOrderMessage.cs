﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Imp_Amazon
{
    public class GetOrderMessage
    {
        public ListOrdersResponse.Cls_ListOrdersResult.Cls_Orders.OrderItem OrderBasic { get; set; }
        public List<Cls_ListOrderItemsResponse.Cls_ListOrderItemsResult.Cls_OrderItems.OrderItemItem> OrderMessage{ get; set; }
        public ShipmentEventList ShipmentEventList { get; set; }
        public RefundEventList RefundEventList { get; set; }
    }
}
