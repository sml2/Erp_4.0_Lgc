﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sy.Data;

namespace Imp_Amazon
{
    public class VerifyReport:JsonObject<VerifyReport>
    {
        public VerifyReport() { }
        public VerifyReport(int iD, bool isSuccess, string message)
        {
            ID = iD;
            IsSuccess = isSuccess;
            Message = message;
        }

        public int ID { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
