﻿using Inf_Amazon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Mod_GlobalVariables;

namespace Imp_Amazon
{
    public class DeliverParameter: IDeliverGoodsParameter2
    {
        public INF_ShopInfo Shop { get; set; }
        public List<IDeliverGoodsParameter2.IProduct> Products { get; set; }
        public LogSub Log { get; set; }
        public void SetUpdate(Action update) {
            this.update = update;
        }
        private Action update;
        public void Update()
        {
            update?.Invoke();
        }
    }
    /// <summary>
    /// 把1.0的产品信息转成2.0的 方便统一调用2.0的接口
    /// </summary>
    public class DeliverProduct : IDeliverGoodsParameter2.IProduct
    {
        public string AmazonOrderID { get; set; }
        public string FulfillmentDate { get; set; }
        public string CarrierName { get; set; }
        public string ShippingMethod { get; set; }
        public string ShipperTrackingNumber { get; set; }
        public string AmazonOrderItemCode { get; set; }
        public int Quantity { get; set; }
        public bool HasAmazonOrderItemCode => true;
    }
}
