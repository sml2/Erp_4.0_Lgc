﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace YY
{
    /// <summary>
    /// 上传商品普通信息、库存、价格、图片、变体
    /// </summary>
    public class AmazonEnvelope
    {
        public Cls_Header Header { get; set; }
        public class Cls_Header
        {
            public string DocumentVersion { get; set; }
            public string MerchantIdentifier { get; set; }
        }
        public string MessageType { get; set; }
        public string PurgeAndReplace { get; set; }
        [XmlElement("Message")]
        public List<Message> Message { get; set; }

    }
    [XmlType(AnonymousType = true)]
    [XmlRoot("Message", IsNullable = false)]
    public class Message
    {
        public string MessageID { get; set; }
        public string OperationType { get; set; }
        //普通商品信息
        public Cls_Product Product { get; set; }
        public class Cls_Product
        {
            public string SKU { get; set; }
            public Cls_StandardProductID StandardProductID { get; set; }
            public class Cls_StandardProductID
            {
                public string Type { get; set; }
                public string Value { get; set; }
            }
            public string ProductTaxCode { get; set; }
            public Cls_Condition Condition { get; set; }
            public class Cls_Condition
            {
                public string ConditionType { get; set; }
            }
            /// <summary>
            /// Health类型下要求填写
            /// </summary>
            public string ItemPackageQuantity { get; set; }
            public Cls_DescriptionData DescriptionData { get; set; }
            public class Cls_DescriptionData
            {
                public string Title { get; set; }
                public string Brand { get; set; }
                public string Description { get; set; }
                [XmlElement(ElementName = "BulletPoint")]
                public List<string> BulletPoint { get; set; }
                /// <summary>
                /// 最大订单数量
                /// </summary>
                public string MaxOrderQuantity { get; set; }
                /// <summary>
                /// 生产厂家 必须值
                /// </summary>
                public string Manufacturer { get; set; }
                public string MfrPartNumber { get; set; }
                public string ModelNumber { get; set; }
                [XmlElement(ElementName = "SearchTerms")]
                public List<string> SearchTerms { get; set; }
                //public string ExternalProductInformation { get; set; }
                public string ItemType { get; set; }
                /// <summary>
                /// ToysBaby>>ToysAndGames必有字段
                /// </summary>
                public string TargetAudience { get; set; }
                public string IsGiftWrapAvailable { get; set; }
                public string IsGiftMessageAvailable { get; set; }
                /// <summary>
                /// 必须值
                /// </summary>
                public string RecommendedBrowseNode { get; set; }
                [XmlElement("ItemWeight")]
                public WeightDimension ItemWeight { get; set; }
                public class WeightDimension
                {
                    [XmlAttribute(AttributeName = "unitOfMeasure")]
                    public string unitOfMeasure { get; set; }
                    [XmlText]
                    public string WeightValue { get; set; }
                }
                public string CountryOfOrigin { get; set; }
                public bool IsExpirationDatedProduct { get; set; }
            }

            #region
            /// <summary>
            /// 商品的分类描述（Data和Type由参数传进）
            /// 且ProductType中的参数随时添加种类===
            /// </summary>
            public Cls_ProductData ProductData { get; set; }
            public class Cls_ProductData
            {
                public Cls_ProductType ProductType { get; set; }
                public class Cls_ProductType
                {
                    //类别下的描述
                    public string Material { get; set; }
                    public string Color { get; set; }
                    public Cls_VariationData VariationData { get; set; }
                    public string Wattage { get; set; }
                    /// <summary>
                    /// 分辨率五位数(CellularPhone特有字段)
                    /// </summary>
                    [XmlElement("FrontWebcamResolution")]
                    public Cls_FrontWebcamResolution FrontWebcamResolution { get; set; }
                    public class Cls_FrontWebcamResolution
                    {
                        [XmlAttribute(AttributeName = "unitOfMeasure")]
                        public string unitOfMeasure { get; set; }
                        [XmlText]
                        public string UnitCountValue { get; set; }
                    }
                    public string ModelName { get; set; }
                    /// <summary>
                    /// 无限提供商（WirelessLockedPhone特有字段）
                    /// </summary>
                    public string WirelessProvider { get; set; }
                    /// <summary>
                    /// 兼容的手机型号(WirelessAccessories美国站特有字段)
                    /// </summary>
                    public string CompatiblePhoneModels { get; set; }
                    /// <summary>
                    /// 美国站食品专有字段/美国Health必填字段
                    /// </summary>
                    [XmlElement("UnitCount")]
                    public Cls_UnitCount UnitCount { get; set; }
                    public class Cls_UnitCount
                    {
                        [XmlAttribute(AttributeName = "unitOfMeasure")]
                        public string unitOfMeasure { get; set; }
                        [XmlText]
                        public double UnitCountValue { get; set; }
                    }
                    /// <summary>
                    /// 日本(Health类型专有字段)
                    /// </summary>
                    public string IsAdultProduct { get; set; }
                    public Cls_Stone Stone { get; set; }
                    public class Cls_Stone
                    {
                        //可选择值
                        public string GemType { get; set; }
                    }
                    public Cls_ColorSpecification ColorSpecification { get; set; }
                    public class Cls_ColorSpecification
                    {
                        public string Color { get; set; }
                        public string ColorMap { get; set; }
                    }
                    public string Size { get; set; }
                    public Cls_ClassificationData ClassificationData { get; set; }
                    public class Cls_ClassificationData
                    {
                        public string ClothingType { get; set; }
                        public string Department { get; set; }
                        public string ColorMap { get; set; }
                        public string MaterialComposition { get; set; }
                        public string OuterMaterial { get; set; }
                        public string SizeMap { get; set; }
                        public string MaterialType { get; set; }
                        public string TargetGender { get; set; }
                    }
                    #region Clotheing特有
                    public string AgeRangeDescription { get; set; }
                    public string ApparelBodyType { get; set; }
                    public string ApparelHeightType { get; set; }
                    public string ApparelSize { get; set; }
                    public string ShoeSize { get; set; }
                    #region DRESS, SWEATER, KURTA, COAT, TUNIC, SOCKS, UNDERPANTS, SWEATSHIRT, PAJAMAS, SUIT, ROBE, TIGHTS, BLAZER, TRACK_SUIT, VEST, SALWAR_SUIT_SET 特有
                    public string ApparelSizeClass { get; set; }
                    public string ApparelSizeSystem { get; set; }
                    #endregion 
                    #region Shirt 特有
                    public string ShirtBodyType { get; set; }
                    public string ShirtHeightType { get; set; }
                    public string ShirtSize { get; set; }
                    public string ShirtSizeClass { get; set; }
                    public string ShirtSizeSystem { get; set; }
                    #endregion
                    #region PANTS, SHORTS, OVERALLS 特有
                    public string BottomsBodyType { get; set; }
                    public string BottomsHeightType { get; set; }
                    public string BottomsSize { get; set; }
                    public string BottomsSizeClass { get; set; }
                    public string BottomsSizeSystem { get; set; }
                    #endregion
                    #region SWIMWEAR, BRA, CORSET, UNDERGARMENTSLIP 特有
                    public string ShapewearBodyType { get; set; }
                    public string ShapewearHeightType { get; set; }
                    public string ShapewearSize { get; set; }
                    public string ShapewearSizeClass { get; set; }
                    public string ShapewearSizeSystem { get; set; }
                    #endregion
                    #region HAT 特有
                    public string HeadwearSize { get; set; }
                    public string HeadwearSizeClass { get; set; }
                    public string HeadwearSizeSystem { get; set; }
                    #endregion
                    #region SKIRT 特有
                    public string SkirtBodyType { get; set; }
                    public string SkirtHeightType { get; set; }
                    public string SkirtSize { get; set; }
                    public string SkirtSizeClass { get; set; }
                    public string SkirtSizeSystem { get; set; }
                    #endregion
                    #endregion
                    public Cls_ShoeSizeComplianceData ShoeSizeComplianceData { get; set; }
                    public class Cls_ShoeSizeComplianceData
                    {
                        public string AgeRangeDescription { get; set; }
                        /// <summary>
                        /// us_footwear_size_system
                        /// eu_footwear_size_system
                        /// uk_footwear_size_system
                        /// jp_footwear_size_system
                        /// </summary>
                        public string FootwearSizeSystem { get; set; }
                        public string ShoeSizeAgeGroup { get; set; }
                        public string ShoeSizeGender { get; set; }
                        public string ShoeSizeClass { get; set; }
                        public string ShoeSizeWidth { get; set; }
                        public string ShoeSize { get; set; }

                    }
                    //首饰类特有字段
                    public string DepartmentName { get; set; }
                    //PC类特有字段
                    /// <summary>
                    /// 硬盘大小
                    /// </summary>
                    [XmlElement("HardDriveSize")]
                    public Cls_HardDriveSize HardDriveSizeize { get; set; }
                    public class Cls_HardDriveSize
                    {
                        /// <summary>
                        /// 内存大小_单位
                        /// </summary>
                        [XmlAttribute(AttributeName = "unitOfMeasure")]
                        public string unitOfMeasure { get; set; }
                        /// <summary>
                        /// 内存大小_值
                        /// </summary>
                        [XmlText]
                        public string HardDriveSizeValue { get; set; }
                    }
                    /// <summary>
                    /// 硬盘接口(硬盘接口类型值)usb_3.0
                    /// </summary>
                    public string HardDriveInterface { get; set; }
                    /// <summary>
                    /// 计算机内存类型(计算机内存类型值)ddr4_sdram
                    /// </summary>
                    public string ComputerMemoryType { get; set; }
                    /// <summary>
                    /// 内存大小
                    /// </summary>
                    [XmlElement("RAMSize")]
                    public Cls_RAMSize RAMSize { get; set; }
                    public class Cls_RAMSize
                    {
                        /// <summary>
                        /// 内存大小_单位
                        /// </summary>
                        [XmlAttribute(AttributeName = "unitOfMeasure")]
                        public string unitOfMeasure { get; set; }
                        /// <summary>
                        /// 内存大小_值
                        /// </summary>
                        [XmlText]
                        public string RAMSizeValue { get; set; }
                    }
                    /// <summary>
                    /// 处理器品牌
                    /// </summary>
                    public string ProcessorBrand { get; set; }
                    /// <summary>
                    /// 处理器速度<ProcessorSpeed unitOfMeasure="Hz|KHz|MHz|THz">
                    /// </summary>
                    [XmlElement("ProcessorSpeed")]
                    public Cls_ProcessorSpeed ProcessorSpeed { get; set; }
                    public class Cls_ProcessorSpeed
                    {
                        /// <summary>
                        /// 处理器速度_单位
                        /// </summary>
                        [XmlAttribute(AttributeName = "unitOfMeasure")]
                        public string unitOfMeasure { get; set; }
                        /// <summary>
                        /// 处理器速度_值
                        /// </summary>
                        [XmlText]
                        public string ProcessorSpeedValue { get; set; }
                    }
                    /// <summary>
                    /// 处理器类型=""
                    /// </summary>
                    public string ProcessorType { get; set; }
                    /// <summary>
                    /// 处理器数量（int）
                    /// </summary>
                    public string ProcessorCount { get; set; }
                    /// <summary>
                    /// 硬件平台
                    /// </summary>
                    public string HardwarePlatform { get; set; }
                    //PDA特有字段
                    /// <summary>
                    /// 屏幕分辨率
                    /// </summary>
                    public string ScreenResolution { get; set; }
                    /// <summary>
                    /// 彩色屏幕
                    /// </summary>
                    public string ColorScreen { get; set; }
                }
                public Cls_VariationData VariationData { get; set; }
                public class Cls_VariationData
                {
                    public string Parentage { get; set; }
                    public string VariationTheme { get; set; }
                    public string RingSize { get; set; }
                    public string Size { get; set; }
                    public string Color { get; set; }
                    public string ColorMap { get; set; }
                    /// <summary>
                    /// 味道、气味(食物分类专有变体字段)
                    /// </summary>
                    public string Flavor { get; set; }
                    public string Wattage { get; set; }
                }
                /// <summary>
                /// Miscellaneous特有+Computers
                /// </summary>
                public string Color { get; set; }
                public string StyleName { get; set; }
                public string Size { get; set; }
                public string MetalType { get; set; }
                public Cls_AgeRecommendation AgeRecommendation { get; set; }
                #region CameraPhoto特有字段
                /// <summary>
                /// Camera包装类型的值
                /// </summary>
                public string CustomerPackageType { get; set; }
                public string Parentage { get; set; }
                /// <summary>
                /// Camera包装类型,都取默认是"CustomerPackageType"
                /// </summary>
                public string VariationTheme { get; set; }
                #endregion
                #region Toys特有字段
                public string IsAdultProduct { get; set; }
                #endregion 
            }
            #endregion
            #region "Toys分类特有必带字段"
            public Cls_AgeRecommendation AgeRecommendation { get; set; }
            public class Cls_AgeRecommendation
            {
                public AgeDimension MinimumManufacturerAgeRecommended { get; set; }
                public AgeDimension MaximumManufacturerAgeRecommended { get; set; }
                public AgeDimension MinimumMerchantAgeRecommended { get; set; }
                public AgeDimension MaximumMerchantAgeRecommended { get; set; }
                public class AgeDimension
                {
                    /// <summary>
                    /// years | months
                    /// </summary>
                    [XmlAttribute(AttributeName = "unitOfMeasure")]
                    public string unitOfMeasure { get; set; }
                    /// <summary>
                    /// 非负整数
                    /// </summary>
                    [XmlText]
                    public string AgeValue { get; set; }
                }
            }
            #endregion
            public bool IsHeatSensitive { get; set; }
        }
        //库存
        public Cls_Inventory Inventory { get; set; }
        public class Cls_Inventory
        {
            public string SKU { get; set; }
            public string Quantity { get; set; }
            public string FulfillmentLatency { get; set; }
            public string SwitchFulfillmentTo { get; set; }
        }
        //价格
        public Cls_Price Price { get; set; }
        public class Cls_Price
        {
            public string SKU { get; set; }
            [XmlElement("StandardPrice")]
            public Cls_StandardPrice StandardPrice { get; set; }
        }
        //图片
        public Cls_ProductImage ProductImage { get; set; }
        public class Cls_ProductImage
        {
            public string SKU { get; set; }
            public ImageType ImageType { get; set; }
            public string ImageLocation { get; set; }
        }
        //变体
        public Cls_Relationship Relationship { get; set; }
        public class Cls_Relationship
        {
            public string ParentSKU { get; set; }
            [XmlElement("Relation")]
            public List<Relation> Relation { get; set; }
        }
        //运单更新
        public Cls_OrderFulfillment OrderFulfillment { get; set; }
        public class Cls_OrderFulfillment
        {
            public string AmazonOrderID { get; set; }
            public string FulfillmentDate { get; set; }
            public Cls_FulfillmentData FulfillmentData { get; set; }
            public class Cls_FulfillmentData
            {
                //public string CarrierCode { get; set; }
                public string CarrierName { get; set; }
                public string ShippingMethod { get; set; }
                public string ShipperTrackingNumber { get; set; }
            }
            [XmlElement("Item")]
            public List<Cls_Item> Item { get; set; }
            [XmlType(AnonymousType = true)]
            [XmlRoot("Item", IsNullable = true)]
            public class Cls_Item
            {
                public string AmazonOrderItemCode { get; set; }
                public string Quantity { get; set; }
            }
        }
        /// <summary>
        /// 更新发票信息
        /// </summary>
        public Cls_InvoiceConfirmation InvoiceConfirmation { get; set; }
        public class Cls_InvoiceConfirmation
        { 
            public string AmazonOrderID { get; set; }
            public string InvoiceSentDate { get; set; }
            [XmlElement("Item")]
            public List<Cls_Item> Item { get; set; }
            [XmlType(AnonymousType = true)]
            [XmlRoot("Item", IsNullable = true)]
            public class Cls_Item
            {
                public string AmazonOrderItemCode { get; set; }
                public string QuantityConfirmed { get; set; }
            }
        }
    }
    public class Cls_StandardPrice
    {
        [XmlAttribute(AttributeName = "currency")]
        public string currency { get; set; }
        [XmlText]
        public string PriceValue { get; set; }
    }
    [XmlType(AnonymousType = true)]
    [XmlRoot("Relation", IsNullable = false)]
    public class Relation
    {
        public string SKU { get; set; }
        public string Type { get; set; }
    }
   
    public enum ImageType
    {
        Main = 0,
        Swatch = 1,
        PT1 = 2,
        PT2 = 3,
        PT3 = 4,
        PT4 = 5,
        PT5 = 6,
        PT6 = 7,
        PT7 = 8,
        PT8 = 9,
        Search = 10,
    }
}
