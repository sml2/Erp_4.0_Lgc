﻿using Inf_Amazon;
using Inf_Collecting;
using Sy.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using YY;
using static Inf_Amazon.INF_Mws;
using static Mod_GlobalVariables;

namespace Imp_Amazon
{
    public class Report : JsonObject<Report>, INF_Report
    {
        static Report()
        {
            Result defParentA(Report r,LogSub Log) { return r.Parent.GetInvokeUpload(r, Log); }
            Result defParentB(Report r, LogSub Log) { return r.Parent.GetInvokeAmazonUpload(r, Log); }
            Result defChildA(Report r, LogSub Log) { return r.Childs.GetInvokeUpload(r, Log); }
            Result defChildB(Report r, LogSub Log) { return r.Childs.GetInvokeAmazonUpload(r, Log); }
            Result defInventoryA(Report r, LogSub Log) { return r.Inventory.GetInvokeUpload(r, Log); }
            Result defInventoryB(Report r, LogSub Log) { return r.Inventory.GetInvokeAmazonUpload(r, Log); }
            Result defPriceA(Report r, LogSub Log) { return r.UpPrice.GetInvokeUpload(r, Log); }
            Result defPriceB(Report r, LogSub Log) { return r.UpPrice.GetInvokeAmazonUpload(r, Log); }
            Result defPictureA(Report r, LogSub Log) { return r.Pics.GetInvokeUpload(r, Log); }
            Result defPictureB(Report r, LogSub Log) { return r.Pics.GetInvokeAmazonUpload(r, Log); }
            Result defRelationA(Report r, LogSub Log) { return r.Relation.GetInvokeUpload(r, Log); }
            Result defRelationB(Report r, LogSub Log) { return r.Relation.GetInvokeAmazonUpload(r, Log); }
            AllProgress.Add(Result.ParentProduct_IN_PROGRESS_, defParentA);
            AllProgress.Add(Result.ParentProduct_DONE_, defParentB);
            AllProgress.Add(Result.ChildProduct_IN_PROGRESS_, defChildA);
            AllProgress.Add(Result.ChildProduct_DONE_, defChildB);
            AllProgress.Add(Result.Inventory_IN_PROGRESS_, defInventoryA);
            AllProgress.Add(Result.Inventory_DONE_, defInventoryB);
            AllProgress.Add(Result.Price_IN_PROGRESS_, defPriceA);
            AllProgress.Add(Result.Price_DONE_, defPriceB);
            AllProgress.Add(Result.Picture_IN_PROGRESS_, defPictureA);
            AllProgress.Add(Result.Picture_DONE_, defPictureB);
            AllProgress.Add(Result.Relation_IN_PROGRESS_, defRelationA);
            AllProgress.Add(Result.Relation_DONE_, defRelationB);
        }
        private static readonly Dictionary<Result, Func<Report,LogSub, Result>> AllProgress = new Dictionary<Result, Func<Report, LogSub,Result>> ();
        public string Title { get; set; }
        public string Price { get; set; }
        public string ParentSku;
        public List<string> ChildsSku;
        public Session Parent;
        public Session Childs;
        public Session Inventory;
        public Session UpPrice;
        public Session Pics;
        public Session Relation;
        public Imp_ShopInfo shop;
        public Result result;
        public int Count => ChildsSku != null ? ChildsSku.Count : 0;
        //public int PicCount => Pics != null ? Pics.Count : 0;
        public string UIresult => result.ToString();
        public int ProgressValue { get; set; }
        public bool NeedUpdate
        {
            get
            {
                if (result < 0) return false;
                return result < Result.Success;
            }
        }
        public StateColor Color
        {
            get
            {
                if (result == Result.Success)
                    return StateColor.Green;
                else if (result > Result.Success || result < 0)
                    return StateColor.Red;//red:失败
                else
                    return StateColor.Yellow;
            }
        }
        public DateTime Time { get; set; }
        //更新上传状态s
        public bool Update(LogSub Log, Func<string, int, bool> EditServerData)
        {
            Result old = result;
            Result Temp = old;
            while (NeedUpdate)
            {
                result = AllProgress[result].Invoke(this, Log);
                //if (result.GetDescription().Contains("请求正在处理")) { Log((result - 1).GetDescription()); }
                if (Temp == result && result != Result.ParentProduct_IN_PROGRESS_) return false;
                Temp = result;
                Log(result.GetDescription());
                if (old == result) { return false; }
                //if (Temp == result) return false;
                //Temp = result;
            }
            var r = old != result;
            if (r) EditServerData(ToJson(), (int)result);
            return r;
            #region
            //if (Parent == null) { Log("No Upload"); return false; }
            //List<Session> UploadSession = new List<Session>()
            //{
            //    Parent,Childs,Inventory,UpPrice,Pics,Relation
            //};
            //foreach (Session se in UploadSession)
            //{
            //    if (se != null)
            //    {
            //        if (!se.IsDowned)
            //        {
            //            AllProgress.Add(result, () => GetInvokeUpload(se, Log));
            //            AllProgress[result].Invoke();
            //        }
            //        if (!se.IsSucceed)
            //        {
            //            AllProgress.Add(result, () => GetInvokeAmazonUpload(se, Log));
            //            AllProgress[result].Invoke();
            //        }
            //    }
            //}
            //Log("Upload Success"); return true;
            #endregion
        }
        public string GetAsin(LogSub Log)
        {
            return MWS.GetAsin(ParentSku, shop, Log);
        }
    }
}
