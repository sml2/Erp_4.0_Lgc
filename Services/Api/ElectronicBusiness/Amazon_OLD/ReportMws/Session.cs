﻿using static Inf_Amazon.INF_Mws;
using static Mod_GlobalVariables;

namespace Imp_Amazon
{
    public class Session
    {
        public int Progress;//当前任务所占用的权重数
        public string FeedMissionId;
        public bool IsDowned = false;
        public bool IsSucceed = false;
        public object Cause;

        public Session() { }
        public Session(string feedMissionId, int progress)
        {
            Progress = progress;
            FeedMissionId = feedMissionId;
        }
        public Result GetInvokeUpload(Report r, LogSub Log)
        {
            //if (se.FeedMissionId == "") { Log($"{se.Cause}:Failed,获取对象失败！");result=Result.UploadFail_ChildProduct; }
            string UploadXmlStatues = MWS.UpdateResult(FeedMissionId, r.shop,r.Time);
            if (UploadXmlStatues == "_DONE_")
            {
                IsDowned = true;
                r.result += 1;
            }
            return r.result;
        }
        public Result GetInvokeAmazonUpload(Report r, LogSub Log)
        {
            if (MWS.GetSubmitResult(FeedMissionId, Log, r.shop))//检测上传亚马逊内容是否成功
            {
                IsSucceed = true;
                r.ProgressValue += Progress;
                if (Progress == 60 || Progress == 20) { r.result += 5; }
                else { r.result += 2; }
            }
            else
            {
                //跟进outmessage来判断输出的result
                r.result = (Result)((int)r.result * (-1));
            }
            return r.result;
        }
    }
}
