﻿using Inf_Collecting;

namespace Imp_Amazon
{
        public class TarckReport : SingletonObject<TarckReport>,INF_TarckReport
        {
            public INF_Report Analysis(string Json)
            {
                INF_Report report = Report.DeserializeSafety(Json);
                return report;
            }
        }
}
