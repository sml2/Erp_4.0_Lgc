﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Imp_Amazon
{
    [XmlRoot(ElementName = "AmazonEnvelope")]
    public class Cls_AmazonEnvelope //: XmlObject
    {
        public Cls_Header Header { get; set; }
        public class Cls_Header
        {
            public string DocumentVersion { get; set; }
            public string MerchantIdentifier { get; set; }
        }
        public string MessageType { get; set; }
        [XmlElement("Message")]
        public List<Cls_Message> Message { get; set; }
        public class Cls_Message
        {
            public string MessageID { get; set; }
            public Cls_ProcessingReport ProcessingReport { get; set; }
            public class Cls_ProcessingReport
            {
                public string DocumentTransactionID { get; set; }
                public string StatusCode { get; set; }
                public Cls_ProcessingSummary ProcessingSummary { get; set; }
                public class Cls_ProcessingSummary
                {
                    public string MessagesProcessed { get; set; }
                    public string MessagesSuccessful { get; set; }
                    public string MessagesWithError { get; set; }
                    public string MessagesWithWarning { get; set; }
                }
                [XmlElement("Result")]
                public List<Cls_Result> Result { get; set; }
                public class Cls_Result
                {
                    public string MessageID { get; set; }
                    public string ResultCode { get; set; }
                    public string ResultMessageCode { get; set; }
                    public string ResultDescription { get; set; }
                    public Cls_AdditionalInfo AdditionalInfo { get; set; }
                    public class Cls_AdditionalInfo
                    {
                        public string SKU { get; set; }
                    }
                }
            }
        }
    }
}
