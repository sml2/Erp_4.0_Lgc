﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Imp_Amazon
{
    [XmlRoot(ElementName = "ListMarketplaceParticipationsResponse", Namespace = "https://mws.amazonservices.com/Sellers/2011-07-01")]
    public class ListMarketplaceParticipationsResponse //: XmlObject
    {
        public ListMarketplaceParticipationsResult ListMarketplaceParticipationsResult { get; set; }
        public ResponseMetadata ResponseMetadata { get; set; }
    }
    public class ListMarketplaceParticipationsResult
    {
        public ListParticipations ListParticipations { get; set; }

        public ListMarketplaces ListMarketplaces { get; set; }
    }
    public class ListParticipations
    {
        [XmlElement("Participation")]
        public List<Participation> Participation { get; set; }
    }
    public class Participation
    {

        public string MarketplaceId { get; set; }

        public string SellerId { get; set; }

        public string HasSellerSuspendedListings { get; set; }
    }
    public class ListMarketplaces
    {
        [XmlElement("Marketplace")]
        public List<Marketplace> Marketplace { get; set; }
    }
    public class Marketplace
    {

        public string MarketplaceId { get; set; }

        public string DefaultCountryCode { get; set; }

        public string DomainName { get; set; }

        public string Name { get; set; }

        public string DefaultCurrencyCode { get; set; }

        public string DefaultLanguageCode { get; set; }
    }
    //public class ResponseMetadata
    //{
    //    public string RequestId { get; set; }
    //}
}
