﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Imp_Amazon
{
    [XmlRoot(ElementName = "GetFeedSubmissionListResponse", Namespace = "http://mws.amazonservices.com/schema/Products/2011-10-01")]
    public class GetLowestPricedOffersForASINResponse //: XmlObject
    {
        public class GetLowestPricedOffersForASINResult
        {
            public IdentifierItem Identifier { get; set; }
            public class IdentifierItem
            {

                public string MarketplaceId { get; set; }

                public string ASIN { get; set; }

                public string ItemCondition { get; set; }
            }
            public List<OffersItem> Offers { get; set; }
            public class OffersItem
            {

                public string SubCondition { get; set; }

                public SellerFeedbackRatingItem SellerFeedbackRating { get; set; }
                public class SellerFeedbackRatingItem
                {

                    public string SellerPositiveFeedbackRating { get; set; }

                    public string FeedbackCount { get; set; }
                }
                public ListingPriceItem ListingPrice { get; set; }
                public class ListingPriceItem
                {

                    public string CurrencyCode { get; set; }

                    public string Amount { get; set; }
                }
                public ShippingItem Shipping { get; set; }
                public class ShippingItem
                {

                    public string CurrencyCode { get; set; }

                    public string Amount { get; set; }
                }
            }
        }
    }
}
