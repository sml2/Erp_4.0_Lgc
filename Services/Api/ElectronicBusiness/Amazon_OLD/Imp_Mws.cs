﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using YY;
using static Mod_GlobalVariables;
using Sy.Threading;

namespace Imp_Amazon
{
    public partial class Imp_Mws : SingletonObject<Imp_Mws>,INF_Mws
    {
        public Imp_Mws() {}
        #region 3.0
        private List<ITaskProduct> TaskProducts = null;
        private Dictionary<string, Cls_SkuChangeData> SkuToChangeData;
        private bool IsFinish = false;
        /// <summary>
        /// GetReslut中执行的操作
        /// </summary>
        /// <param name="Tp"></param>
        /// <returns></returns>
        public bool TaskSync(TaskParameter Tp)
        {
            IsFinish = false;
            TaskProducts = new List<ITaskProduct>();
            Tp.Products.ForEach((r) => TaskProducts.Add(r));
            #region 
            //SkuToChangeData = new Dictionary<string, Cls_SkuChangeData>();
            //Tp.Products.ForEach((r) =>
            //{
            //    r.SkuChangeData.ForEach((v) =>
            //    {
            //        if (!SkuToChangeData.ContainsKey(v.Sku))
            //            if (r.Sku == v.Sku)//此时为单变体
            //            {
            //                var tempv = v;
            //                tempv.VariantData = null;
            //                tempv.RelationshipData = null;
            //                SkuToChangeData.Add(v.Sku, tempv);
            //            }
            //            else SkuToChangeData.Add(v.Sku, v);
            //    });
            //});
            #endregion
            //SkuList
            Dictionary<string, List<string>> AllSku = new Dictionary<string, List<string>>(); 
            //店铺信息
            var shopInfo = Tp.Shop;
            Imp_ShopInfo Shop = new Imp_ShopInfo(shopInfo.MarketPlace, shopInfo.SellerID, shopInfo.MwsAuthToken, shopInfo.AwsAccessKey, shopInfo.SecretKey);
            if (TaskProducts.IsNull() || TaskProducts.Count == 0)
            {
                Tp.Finish(false, SessionType.PRODUCT, I18N.Instance.MWS_Log1);
                return true;
            }
            #region 主产品
            SessionState ProState = Tp.GetSessionState(SessionType.PRODUCT);
            switch (ProState)
            {
                case SessionState.Wait:
                case SessionState.Submitting:
                    //执行更改上传状态
                    if (ProState == SessionState.Wait) Tp.SetSessionState(SessionType.PRODUCT, SessionState.Submitting);
                    //1.执行上传主产品
                    string ErrMessage = "";
                    string ProFeedMissionID = InvokeUploadProduct(Tp, true, ref ErrMessage);
                    //上传成功
                    if (!ProFeedMissionID.IsNullOrEmpty())
                    {
                        //上报
                        Tp.SetSessionID(SessionType.PRODUCT, ProFeedMissionID);
                        Tp.SetSessionState(SessionType.PRODUCT, SessionState.Submitted);
                        break;
                    }
                    else
                    {
                        if (ErrMessage.IsNotWhiteSpace() && ErrMessage == "NoMessage")
                        {
                            Tp.SetSessionState(SessionType.PRODUCT, SessionState.Complete);
                            break;
                        }
                        if (ErrMessage.IsNotWhiteSpace()) Tp.Finish(false, SessionType.PRODUCT, ErrMessage);
                        else Tp.Finish(false, SessionType.PRODUCT, I18N.Instance.MWS_Log2);
                        return true;
                    }
                case SessionState.Submitted:
                case SessionState.Processing:
                    ErrMessage = "";
                    if (!InvokeStatus(Tp, SessionType.PRODUCT, ref ErrMessage))
                    {
                        if (ErrMessage == "AccessDenied" || ErrMessage == "FeedMissionID") return true;
                    }
                    break;
                case SessionState.Down:
                case SessionState.Result:
                    if (!InvokeDown(Tp, SessionType.PRODUCT, true)) return true;
                    break;
                case SessionState.Complete:
                    break;
            }
            #endregion 
            #region 子产品
            SessionState VarState = Tp.GetSessionState(SessionType.VARIANT);
            switch (VarState)
            {
                case SessionState.Wait:
                case SessionState.Submitting:
                    //执行更改上传状态
                    if (VarState == SessionState.Wait) Tp.SetSessionState(SessionType.VARIANT, SessionState.Submitting);
                    //2.执行上传子产品
                    string ErrMessage = "";
                    string ProFeedMissionID = InvokeUploadProduct(Tp, false, ref ErrMessage);
                    //上传成功
                    if (!ProFeedMissionID.IsNullOrEmpty())
                    {
                        //上报
                        Tp.SetSessionID(SessionType.VARIANT, ProFeedMissionID);
                        Tp.SetSessionState(SessionType.VARIANT, SessionState.Submitted);
                        break;
                    }
                    else
                    {
                        if (ErrMessage.IsNotWhiteSpace() && ErrMessage == "NoMessage")
                        {
                            Tp.SetSessionState(SessionType.VARIANT, SessionState.Complete);
                            break;
                        }
                        if (ErrMessage.IsNotWhiteSpace()) Tp.Finish(false, SessionType.VARIANT, ErrMessage);
                        else Tp.Finish(false, SessionType.VARIANT, I18N.Instance.MWS_Log2);
                        return true;
                    }
                case SessionState.Submitted:
                case SessionState.Processing:
                    string ErrVARIANTMessage = "";
                    if (!InvokeStatus(Tp, SessionType.VARIANT, ref ErrVARIANTMessage))
                    {
                        if (ErrVARIANTMessage == "AccessDenied" || ErrVARIANTMessage == "FeedMissionID") return true;
                    }
                    break;
                case SessionState.Down:
                case SessionState.Result:
                    if (!InvokeDown(Tp, SessionType.VARIANT, false)) return true;
                    break;
                case SessionState.Complete:
                    break;
            }
            #endregion 
            #region 库存
            SessionState InventoryState = Tp.GetSessionState(SessionType.INVENTORY);
            switch (InventoryState)
            {
                case SessionState.Wait:
                case SessionState.Submitting:
                    //执行更改上传状态
                    if (InventoryState == SessionState.Wait) Tp.SetSessionState(SessionType.INVENTORY, SessionState.Submitting);
                    //3.执行上传库存
                    string ErrMessage = "";
                    AmazonEnvelope Inventory = GetMutiProInventory(TaskProducts, Shop, ref ErrMessage);
                    if (Inventory == null) 
                    {
                        if (ErrMessage.IsNotWhiteSpace() && ErrMessage == "NoMessage")
                        {
                            Tp.SetSessionState(SessionType.INVENTORY, SessionState.Complete);
                            break;
                        }
                        Tp.Finish(false, SessionType.INVENTORY, I18N.Instance.MWS_Log3);
                        return true;
                    }
                    string xml = GetOtherXml(Inventory);//获取xml逻辑要改掉
                    //验证xml
                    //string ErrMsg = "";
                    //if (!Validatexml(xml, Tp.Log, ref ErrMsg))
                    //{
                    //    Tp.Log("验证xml失败,请修改后重新上传");
                    //    Tp.Finish(false, SessionType.INVENTORY, ErrMsg);
                    //    return true;
                    //}
                    SaveFile(xml, SessionType.INVENTORY);
                    INF_Mws.Result UploadProInfo = MWS.Upload(xml, "_POST_INVENTORY_AVAILABILITY_DATA_", Shop, out string ProFeedSubmissionId, ref ErrMessage, Tp.Log);
                    if (UploadProInfo.Equals(INF_Mws.Result.Fail)) return false;
                    string ProFeedMissionID = ProFeedSubmissionId;
                    //上传成功
                    if (!ProFeedMissionID.IsNullOrEmpty())
                    {
                        //上报
                        Tp.SetSessionID(SessionType.INVENTORY, ProFeedMissionID);
                        Tp.SetSessionState(SessionType.INVENTORY, SessionState.Submitted);
                        break;
                    }
                    else return false;
                case SessionState.Submitted:
                case SessionState.Processing:
                    string ErrINVENTORYMessage = "";
                    if (!InvokeStatus(Tp, SessionType.INVENTORY, ref ErrINVENTORYMessage))
                    {
                        if (ErrINVENTORYMessage == "AccessDenied" || ErrINVENTORYMessage == "FeedMissionID") return true;
                    }
                    break;
                case SessionState.Down:
                case SessionState.Result:
                    if (!InvokeDown(Tp, SessionType.INVENTORY, false)) return true;
                    break;
                case SessionState.Complete:
                    break;
            }
            #endregion 
            #region 价格
            SessionState PriceState = Tp.GetSessionState(SessionType.PRICING);
            switch (PriceState)
            {
                case SessionState.Wait:
                case SessionState.Submitting:
                    //执行更改上传状态
                    if (PriceState == SessionState.Wait) Tp.SetSessionState(SessionType.PRICING, SessionState.Submitting);
                    //4.执行上传价格
                    string ErrMessage = "";
                    AmazonEnvelope Price = GetMutiProPrice(TaskProducts, Shop, ref ErrMessage);
                    if (Price == null) 
                    {
                        if (ErrMessage.IsNotWhiteSpace() && ErrMessage == "NoMessage")
                        {
                            Tp.SetSessionState(SessionType.PRICING, SessionState.Complete);
                            break;
                        }
                        Tp.Finish(false, SessionType.PRICING, I18N.Instance.MWS_Log4);
                        return true; 
                    }
                    //ProductInfo(商品)
                    string xml = GetOtherXml(Price);
                    //验证xml
                    //string ErrMsg = "";
                    //if (!Validatexml(xml, Tp.Log, ref ErrMsg))
                    //{
                    //    Tp.Log("验证xml失败,请修改后重新上传");
                    //    Tp.Finish(false, SessionType.PRICING, ErrMsg);
                    //    return true;
                    //}
                    SaveFile(xml, SessionType.PRICING);
                    INF_Mws.Result UploadProInfo = MWS.Upload(xml, "_POST_PRODUCT_PRICING_DATA_", Shop, out string ProFeedSubmissionId, ref ErrMessage, Tp.Log);
                    if (UploadProInfo.Equals(INF_Mws.Result.Fail)) return false;
                    string ProFeedMissionID = ProFeedSubmissionId;
                    //上传成功
                    if (!ProFeedMissionID.IsNullOrEmpty())
                    {
                        //上报
                        Tp.SetSessionID(SessionType.PRICING, ProFeedMissionID);
                        Tp.SetSessionState(SessionType.PRICING, SessionState.Submitted);
                        break;
                    }
                    else return false;
                case SessionState.Submitted:
                case SessionState.Processing:
                    string ErrPRICINGMessage = "";
                    if (!InvokeStatus(Tp, SessionType.PRICING, ref ErrPRICINGMessage))
                    {
                        if (ErrPRICINGMessage == "AccessDenied" || ErrPRICINGMessage == "FeedMissionID") return true;
                    }
                    break;
                case SessionState.Down:
                case SessionState.Result:
                    if (!InvokeDown(Tp, SessionType.PRICING, false)) return true;
                    break;
                case SessionState.Complete:
                    break;
            }
            #endregion 
            #region 图片
            SessionState ImageState = Tp.GetSessionState(SessionType.IMAGE);
            switch (ImageState)
            {
                case SessionState.Wait:
                case SessionState.Submitting:
                    //执行更改上传状态
                    Tp.SetSessionState(SessionType.IMAGE, SessionState.Submitting);
                    //5.执行上传图片
                    string ErrMessage = "";
                    AmazonEnvelope Image = GetMutiProImage(TaskProducts, Shop, ref ErrMessage);
                    if (Image == null) 
                    {
                        if (ErrMessage.IsNotWhiteSpace() && ErrMessage == "NoMessage")
                        {
                            Tp.SetSessionState(SessionType.IMAGE, SessionState.Complete);
                            break;
                        }
                        Tp.Finish(false, SessionType.IMAGE, I18N.Instance.MWS_Log5);
                        return true;
                    }
                    //ProductInfo(商品)
                    string xml = GetOtherXml(Image);
                    //验证xml
                    //string ErrMsg = "";
                    //if (!Validatexml(xml, Tp.Log, ref ErrMsg))
                    //{
                    //    Tp.Log("验证xml失败,请修改后重新上传");
                    //    Tp.Finish(false, SessionType.IMAGE, ErrMsg);
                    //    return true;
                    //}
                    SaveFile(xml, SessionType.IMAGE);
                    INF_Mws.Result UploadProInfo = MWS.Upload(xml, "_POST_PRODUCT_IMAGE_DATA_", Shop, out string ProFeedSubmissionId, ref ErrMessage, Tp.Log);
                    if (UploadProInfo.Equals(INF_Mws.Result.Fail)) return false;
                    string ProFeedMissionID = ProFeedSubmissionId;
                    //上传成功
                    if (!ProFeedMissionID.IsNullOrEmpty())
                    {
                        //上报
                        Tp.SetSessionID(SessionType.IMAGE, ProFeedMissionID);
                        Tp.SetSessionState(SessionType.IMAGE, SessionState.Submitted);
                        break;
                    }
                    else return false;
                case SessionState.Submitted:
                case SessionState.Processing:
                    string ErrIMAGEMessage = "";
                    if (!InvokeStatus(Tp, SessionType.IMAGE,ref ErrIMAGEMessage))
                    {
                        if (ErrIMAGEMessage == "AccessDenied" || ErrIMAGEMessage == "FeedMissionID") return true;
                    }
                    break;
                case SessionState.Down:
                case SessionState.Result:
                    if (!InvokeDown(Tp, SessionType.IMAGE, false)) return true;
                    break;
                case SessionState.Complete:
                    break;
            }
            #endregion 
            #region 变体关系
            SessionState RelationState = Tp.GetSessionState(SessionType.RELATIONSHIP);
            switch (RelationState)
            {
                case SessionState.Wait:
                case SessionState.Submitting:
                    //执行更改上传状态
                    Tp.SetSessionState(SessionType.RELATIONSHIP, SessionState.Submitting);
                    //1.执行上传变体
                    string ErrMessage = "";
                    AmazonEnvelope RelationShip = GetMutiProRelationShip(TaskProducts, Shop, ref ErrMessage);
                    if (RelationShip == null) 
                    {
                        if (ErrMessage.IsNotWhiteSpace() && ErrMessage == "NoMessage")
                        {
                            Tp.SetSessionState(SessionType.RELATIONSHIP, SessionState.Complete);
                            InvokeFinish(Tp, SessionType.RELATIONSHIP);
                            break;
                        }
                        Tp.Finish(false, SessionType.RELATIONSHIP, I18N.Instance.MWS_Log6);
                        return true;
                    }
                    //ProductInfo(商品)
                    string xml = GetOtherXml(RelationShip);
                    //验证xml
                    //string ErrMsg = "";
                    //if (!Validatexml(xml, Tp.Log, ref ErrMsg))
                    //{
                    //    Tp.Log("验证xml失败,请修改后重新上传");
                    //    Tp.Finish(false, SessionType.RELATIONSHIP, ErrMsg);
                    //    return true;
                    //}
                    SaveFile(xml, SessionType.RELATIONSHIP);
                    INF_Mws.Result UploadProInfo = MWS.Upload(xml, "_POST_PRODUCT_RELATIONSHIP_DATA_", Shop, out string ProFeedSubmissionId, ref ErrMessage, Tp.Log);
                    if (UploadProInfo.Equals(INF_Mws.Result.Fail)) return false;
                    string ProFeedMissionID = ProFeedSubmissionId;
                    //上传成功
                    if (!ProFeedMissionID.IsNullOrEmpty())
                    {
                        //上报
                        Tp.SetSessionID(SessionType.RELATIONSHIP, ProFeedMissionID);
                        Tp.SetSessionState(SessionType.RELATIONSHIP, SessionState.Submitted);
                        break;
                    }
                    else return false;
                case SessionState.Submitted:
                case SessionState.Processing:
                    string ErrRELATIONSHIPMessage = "";
                    if (!InvokeStatus(Tp, SessionType.RELATIONSHIP, ref ErrRELATIONSHIPMessage))
                    {
                        if (ErrRELATIONSHIPMessage == "AccessDenied" || ErrRELATIONSHIPMessage == "FeedMissionID") return true;
                    }
                    //Down时执行获取Result
                    break;
                case SessionState.Down:
                case SessionState.Result:
                    if (!InvokeDown(Tp, SessionType.RELATIONSHIP, false)) return true;
                    break;
                case SessionState.Complete:
                    if(!IsFinish)InvokeFinish(Tp, SessionType.RELATIONSHIP);
                    break;
            }
            #endregion 
            return IsFinish;
        }
        public bool Sync(TaskParameter Tp)
        {
            IsFinish = false;
            TaskProducts = new List<ITaskProduct>();
            Tp.Products.ForEach((r) => TaskProducts.Add(r));
            SkuToChangeData = new Dictionary<string, Cls_SkuChangeData>();
            Tp.Products.ForEach((r) =>
            {
                r.SkuChangeData.ForEach((v) =>
                {
                    if (!SkuToChangeData.ContainsKey(v.Sku))
                        if (r.Sku == v.Sku)//此时为单变体
                        {
                            var tempv = v;
                            tempv.VariantData = null;
                            tempv.RelationshipData = null;
                            SkuToChangeData.Add(v.Sku, tempv);
                        }
                        else SkuToChangeData.Add(v.Sku, v);
                });
            });
            //return true;
            //SkuList
            Dictionary<string, List<string>> AllSku = new Dictionary<string, List<string>>();
            //店铺信息
            var shopInfo = Tp.Shop;
            Imp_ShopInfo Shop = new Imp_ShopInfo(shopInfo.MarketPlace, shopInfo.SellerID, shopInfo.MwsAuthToken, shopInfo.AwsAccessKey, shopInfo.SecretKey);
            if (TaskProducts.IsNull() || TaskProducts.Count == 0)
            {
                Tp.Finish(false, SessionType.PRODUCT, I18N.Instance.MWS_Log1);
                return true;
            }
            #region 主产品
            SessionState ProState = Tp.GetSessionState(SessionType.PRODUCT);
            switch (ProState)
            {
                case SessionState.Wait:
                case SessionState.Submitting:
                    //执行更改上传状态
                    if (ProState == SessionState.Wait) Tp.SetSessionState(SessionType.PRODUCT, SessionState.Submitting);
                    //1.执行上传主产品
                    string ErrMessage = "";
                    string ProFeedMissionID = InvokeUploadProduct(Tp, true, ref ErrMessage);
                    //上传成功
                    if (!ProFeedMissionID.IsNullOrEmpty())
                    {
                        //上报
                        Tp.SetSessionID(SessionType.PRODUCT, ProFeedMissionID);
                        Tp.SetSessionState(SessionType.PRODUCT, SessionState.Submitted);
                        break;
                    }
                    else
                    {
                        if (ErrMessage.IsNotWhiteSpace() && ErrMessage == "NoMessage")
                        {
                            Tp.SetSessionState(SessionType.PRODUCT, SessionState.Complete);
                            break;
                        }
                        if (ErrMessage.IsNotWhiteSpace()) Tp.Finish(false, SessionType.PRODUCT, ErrMessage);
                        else Tp.Finish(false, SessionType.PRODUCT, I18N.Instance.MWS_Log2);
                        return true;
                    }
                case SessionState.Submitted:
                case SessionState.Processing:
                case SessionState.Down:
                case SessionState.Result:
                case SessionState.Complete:
                    break;
            }
            #endregion 
            #region 子产品
            SessionState VarState = Tp.GetSessionState(SessionType.VARIANT);
            switch (VarState)
            {
                case SessionState.Wait:
                case SessionState.Submitting:
                    //执行更改上传状态
                    if (VarState == SessionState.Wait) Tp.SetSessionState(SessionType.VARIANT, SessionState.Submitting);
                    //2.执行上传子产品
                    string ErrMessage = "";
                    string ProFeedMissionID = InvokeUploadProduct(Tp, false, ref ErrMessage);
                    //上传成功
                    if (!ProFeedMissionID.IsNullOrEmpty())
                    {
                        //上报
                        Tp.SetSessionID(SessionType.VARIANT, ProFeedMissionID);
                        Tp.SetSessionState(SessionType.VARIANT, SessionState.Submitted);
                        break;
                    }
                    else
                    {
                        if (ErrMessage.IsNotWhiteSpace() && ErrMessage == "NoMessage")
                        {
                            Tp.SetSessionState(SessionType.VARIANT, SessionState.Complete);
                            break;
                        }
                        if (ErrMessage.IsNotWhiteSpace()) Tp.Finish(false, SessionType.VARIANT, ErrMessage);
                        else Tp.Finish(false, SessionType.VARIANT, I18N.Instance.MWS_Log2);
                        return true;
                    }
                case SessionState.Submitted:
                case SessionState.Processing:
                case SessionState.Down:
                case SessionState.Result:
                case SessionState.Complete:
                    break;
            }
            #endregion 
            #region 库存
            SessionState InventoryState = Tp.GetSessionState(SessionType.INVENTORY);
            switch (InventoryState)
            {
                case SessionState.Wait:
                case SessionState.Submitting:
                    //执行更改上传状态
                    if (InventoryState == SessionState.Wait) Tp.SetSessionState(SessionType.INVENTORY, SessionState.Submitting);
                    //3.执行上传库存
                    string ErrMessage = "";
                    AmazonEnvelope Inventory = GetMutiProInventory(TaskProducts, Shop, ref ErrMessage);
                    if (Inventory == null)
                    {
                        if (ErrMessage.IsNotWhiteSpace() && ErrMessage == "NoMessage")
                        {
                            Tp.SetSessionState(SessionType.INVENTORY, SessionState.Complete);
                            break;
                        }
                        Tp.Finish(false, SessionType.INVENTORY, I18N.Instance.MWS_Log3);
                        return true;
                    }
                    string xml = GetOtherXml(Inventory);//获取xml逻辑要改掉
                    //验证xml
                    //string ErrMsg = "";
                    //if (!Validatexml(xml, Tp.Log, ref ErrMsg))
                    //{
                    //    Tp.Log("验证xml失败,请修改后重新上传");
                    //    Tp.Finish(false, SessionType.INVENTORY, ErrMsg);
                    //    return true;
                    //}
                    SaveFile(xml, SessionType.INVENTORY);
                    INF_Mws.Result UploadProInfo = MWS.Upload(xml, "_POST_INVENTORY_AVAILABILITY_DATA_", Shop, out string ProFeedSubmissionId, ref ErrMessage, Tp.Log);
                    if (UploadProInfo.Equals(INF_Mws.Result.Fail)) return false;
                    string ProFeedMissionID = ProFeedSubmissionId;
                    //上传成功
                    if (!ProFeedMissionID.IsNullOrEmpty())
                    {
                        //上报
                        Tp.SetSessionID(SessionType.INVENTORY, ProFeedMissionID);
                        Tp.SetSessionState(SessionType.INVENTORY, SessionState.Submitted);
                        break;
                    }
                    else return false;
                case SessionState.Submitted:
                case SessionState.Processing:
                case SessionState.Down:
                case SessionState.Result:
                case SessionState.Complete:
                    break;
            }
            #endregion 
            #region 价格
            SessionState PriceState = Tp.GetSessionState(SessionType.PRICING);
            switch (PriceState)
            {
                case SessionState.Wait:
                case SessionState.Submitting:
                    //执行更改上传状态
                    if (PriceState == SessionState.Wait) Tp.SetSessionState(SessionType.PRICING, SessionState.Submitting);
                    //4.执行上传价格
                    string ErrMessage = "";
                    AmazonEnvelope Price = GetMutiProPrice(TaskProducts, Shop, ref ErrMessage);
                    if (Price == null)
                    {
                        if (ErrMessage.IsNotWhiteSpace() && ErrMessage == "NoMessage")
                        {
                            Tp.SetSessionState(SessionType.PRICING, SessionState.Complete);
                            break;
                        }
                        Tp.Finish(false, SessionType.PRICING, I18N.Instance.MWS_Log4);
                        return true;
                    }
                    //ProductInfo(商品)
                    string xml = GetOtherXml(Price);
                    //验证xml
                    //string ErrMsg = "";
                    //if (!Validatexml(xml, Tp.Log, ref ErrMsg))
                    //{
                    //    Tp.Log("验证xml失败,请修改后重新上传");
                    //    Tp.Finish(false, SessionType.PRICING, ErrMsg);
                    //    return true;
                    //}
                    SaveFile(xml, SessionType.PRICING);
                    INF_Mws.Result UploadProInfo = MWS.Upload(xml, "_POST_PRODUCT_PRICING_DATA_", Shop, out string ProFeedSubmissionId, ref ErrMessage, Tp.Log);
                    if (UploadProInfo.Equals(INF_Mws.Result.Fail)) return false;
                    string ProFeedMissionID = ProFeedSubmissionId;
                    //上传成功
                    if (!ProFeedMissionID.IsNullOrEmpty())
                    {
                        //上报
                        Tp.SetSessionID(SessionType.PRICING, ProFeedMissionID);
                        Tp.SetSessionState(SessionType.PRICING, SessionState.Submitted);
                        break;
                    }
                    else return false;
                case SessionState.Submitted:
                case SessionState.Processing:
                case SessionState.Down:
                case SessionState.Result:
                case SessionState.Complete:
                    break;
            }
            #endregion 
            #region 图片
            SessionState ImageState = Tp.GetSessionState(SessionType.IMAGE);
            switch (ImageState)
            {
                case SessionState.Wait:
                case SessionState.Submitting:
                    //执行更改上传状态
                    if (ImageState == SessionState.Wait) Tp.SetSessionState(SessionType.IMAGE, SessionState.Submitting);
                    //5.执行上传图片
                    string ErrMessage = "";
                    AmazonEnvelope Image = GetMutiProImage(TaskProducts, Shop, ref ErrMessage);
                    if (Image == null)
                    {
                        if (ErrMessage.IsNotWhiteSpace() && ErrMessage == "NoMessage")
                        {
                            Tp.SetSessionState(SessionType.IMAGE, SessionState.Complete);
                            break;
                        }
                        Tp.Finish(false, SessionType.IMAGE, I18N.Instance.MWS_Log5);
                        return true;
                    }
                    //ProductInfo(商品)
                    string xml = GetOtherXml(Image);
                    //验证xml
                    //string ErrMsg = "";
                    //if (!Validatexml(xml, Tp.Log, ref ErrMsg))
                    //{
                    //    Tp.Log("验证xml失败,请修改后重新上传");
                    //    Tp.Finish(false, SessionType.IMAGE, ErrMsg);
                    //    return true;
                    //}
                    SaveFile(xml, SessionType.IMAGE);
                    INF_Mws.Result UploadProInfo = MWS.Upload(xml, "_POST_PRODUCT_IMAGE_DATA_", Shop, out string ProFeedSubmissionId, ref ErrMessage, Tp.Log);
                    if (UploadProInfo.Equals(INF_Mws.Result.Fail)) return false;
                    string ProFeedMissionID = ProFeedSubmissionId;
                    //上传成功
                    if (!ProFeedMissionID.IsNullOrEmpty())
                    {
                        //上报
                        Tp.SetSessionID(SessionType.IMAGE, ProFeedMissionID);
                        Tp.SetSessionState(SessionType.IMAGE, SessionState.Submitted);
                        break;
                    }
                    else return false;
                case SessionState.Submitted:
                case SessionState.Processing:
                case SessionState.Down:
                case SessionState.Result:
                case SessionState.Complete:
                    break;
            }
            #endregion 
            #region 变体关系
            SessionState RelationState = Tp.GetSessionState(SessionType.RELATIONSHIP);
            switch (RelationState)
            {
                case SessionState.Wait:
                case SessionState.Submitting:
                    //执行更改上传状态
                    if (RelationState == SessionState.Wait) Tp.SetSessionState(SessionType.RELATIONSHIP, SessionState.Submitting);
                    //1.执行上传变体
                    string ErrMessage = "";
                    AmazonEnvelope RelationShip = GetMutiProRelationShip(TaskProducts, Shop, ref ErrMessage);
                    if (RelationShip == null)
                    {
                        if (ErrMessage.IsNotWhiteSpace() && ErrMessage == "NoMessage")
                        {
                            Tp.SetSessionState(SessionType.RELATIONSHIP, SessionState.Complete);
                            InvokeFinish(Tp, SessionType.RELATIONSHIP);
                            break;
                        }
                        Tp.Finish(false, SessionType.RELATIONSHIP, I18N.Instance.MWS_Log6);
                        return true;
                    }
                    //ProductInfo(商品)
                    string xml = GetOtherXml(RelationShip);
                    //验证xml
                    //string ErrMsg = "";
                    //if (!Validatexml(xml, Tp.Log, ref ErrMsg))
                    //{
                    //    Tp.Log("验证xml失败,请修改后重新上传");
                    //    Tp.Finish(false, SessionType.RELATIONSHIP, ErrMsg);
                    //    return true;
                    //}
                    SaveFile(xml, SessionType.RELATIONSHIP);
                    INF_Mws.Result UploadProInfo = MWS.Upload(xml, "_POST_PRODUCT_RELATIONSHIP_DATA_", Shop, out string ProFeedSubmissionId, ref ErrMessage, Tp.Log);
                    if (UploadProInfo.Equals(INF_Mws.Result.Fail)) return false;
                    string ProFeedMissionID = ProFeedSubmissionId;
                    //上传成功
                    if (!ProFeedMissionID.IsNullOrEmpty())
                    {
                        //上报
                        Tp.SetSessionID(SessionType.RELATIONSHIP, ProFeedMissionID);
                        Tp.SetSessionState(SessionType.RELATIONSHIP, SessionState.Submitted);
                        break;
                    }
                    else return false;
                case SessionState.Submitted:
                case SessionState.Processing:
                case SessionState.Down:
                case SessionState.Result:
                case SessionState.Complete:
                    break;
            }
            #endregion 
            return IsFinish;
        }
        public void GetResult(GetResultTask GetReslutTask)
        {
            DateTime Now = DateTime.Now;
            TimeSpan Second = Now - GetReslutTask.Time;
            double DelayTime = Second.TotalSeconds - 3 * 60;
            if (DelayTime < 0) Thread.Sleep(Convert.ToInt32(DelayTime * (-1) * 1000));
            InvokeSyncGetResult(GetReslutTask.Task);
        }
        public void InvokeSyncGetResult(TaskParameter Task)
        {
            if (!TaskSync(Task))
            {
                Thread.Sleep(1 * 60 * 1000);
                InvokeSyncGetResult(Task);
            }
        }
        private void SaveFile(string xml, SessionType Type)
        {
            //将xml保存至Config目录
            string FilePath = Path.Combine(Sy.Environment.AppPath, "Config", "AmazonUploadXml");
            string FileName = Path.Combine(FilePath, $"{Mod_GlobalFunction.GetTimeStamp(Mod_GlobalFunction.TimestampMode.UtcSecond)}_{Type}.txt");
            //文件是否存在
            string Directory = Path.GetDirectoryName(FileName);
            if (!System.IO.Directory.Exists(Directory))
            {
                System.IO.Directory.CreateDirectory(Directory);
            }
            try
            {
                File.WriteAllText(FileName, xml);
            }
            catch { }
        }
        /// <summary>
        /// 验证xml
        /// </summary>
        /// <param name="xmlText"></param>
        /// <returns></returns>
        private bool Validatexml(string xmlText, LogSub Log, ref string ErrMessage)
        {
            bool result = true;
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(xmlText);
            //验证xml的xsd路径
            string FilePath = Path.Combine(Sy.Environment.AppPath, "Config", "AllxsdFile");
            if (!Directory.Exists(FilePath))
            {
                Log("未查找到验证的xsd文件,请联系管理员重新配置!");
                return false;
            }
            //读取所有xsd
            List<string> xsdPaths = Directory.GetFiles(FilePath).ToList();
            //验证xml格式是否正确 通过xsd验证 
            string error = "";
            //声明XmlSchema 
            XmlSchemaSet schemas = new XmlSchemaSet();
            foreach (var xsdPath in xsdPaths)
            {
                schemas.Add("", XmlReader.Create(xsdPath));
            }
            //声明事件处理方法 
            ValidationEventHandler eventHandler = new ValidationEventHandler(delegate (object sender, ValidationEventArgs e)
            {
                switch (e.Severity)
                {
                    case XmlSeverityType.Error:
                        result = false;
                        error += e.Message;
                        break;
                    case XmlSeverityType.Warning:
                        break;
                }
            });
            xml.Schemas = schemas;
            //验证xml 
            xml.Validate(eventHandler);
            //检查是否有异常 如果格式不正确就抛出来 
            ErrMessage = error;
            return result;
        }
         /// <summary>
        /// 1.执行上传主产品+子产品
        /// </summary>
        /// <returns></returns>
        private string InvokeUploadProduct(TaskParameter Tp, bool IsMainProduct, ref string ErrMessage)
        {
            //店铺信息
            var shopInfo = Tp.Shop;
            Imp_ShopInfo Shop = new Imp_ShopInfo(shopInfo.MarketPlace, shopInfo.SellerID, shopInfo.MwsAuthToken, shopInfo.AwsAccessKey, shopInfo.SecretKey);
            //bool IsAmerican = shopInfo.MarketPlace.ID == "ATVPDKIKX0DER";
            List<ITaskProduct> TempProducts = new List<ITaskProduct>();
            TaskProducts.ForEach((r) => TempProducts.Add(r));
            AmazonEnvelope pro = GetMutiProInfo(TempProducts, Shop, IsMainProduct, shopInfo.MarketPlace.ID, ref ErrMessage);
            if (pro == null) 
            { 
                return null;
            }
            //ProductInfo(商品)
            string xml = GetXml(pro, IsMainProduct);
            //验证xml
            //if (!Validatexml(xml, Tp.Log, ref ErrMessage))
            //{
            //    Tp.Log("验证xml失败,请修改后重新上传");
            //    return null;
            //}
            SessionType Type = IsMainProduct ? SessionType.PRODUCT : SessionType.VARIANT;
            SaveFile(xml, Type);
            INF_Mws.Result UploadProInfo = MWS.Upload(xml, "_POST_PRODUCT_DATA_", Shop, out string ProFeedSubmissionId, ref ErrMessage, Tp.Log);
            if (UploadProInfo.Equals(INF_Mws.Result.Fail)) return null;
            return ProFeedSubmissionId;
        }
        private List<string> GetProductData(ITaskProduct Product, ref string ErrMessage)
        {
            string ProductData, ProductType;
            string ExtValue = "";
            bool IsErrorData = false;
            if (Product.TypeDataID.IsNotNull() && Product.TypeDataID.Count == 2)
            {
                if (Product.TypeDataID[0].IsNull() || Product.TypeDataID[1].IsNull()) IsErrorData = true;
            }
            if (Product.TypeDataID.IsNull() || Product.TypeDataID.Count < 2 || IsErrorData)
            {
                if (Product.TypeData.IsNull() || Product.TypeData.Count < 2)
                {
                    ErrMessage = "未定义节点分类";
                    return null;
                }
                var Spliter = ",";
                Sy.String Fdata = Product.TypeData[0];
                Sy.String Sdata = Product.TypeData[1];
                if (Fdata.Contains(Spliter) && Sdata.Contains(Spliter))
                {
                    ProductData = Fdata.After(Spliter);
                    ProductType = Sdata.After(Spliter);
                    if (ProductType == "Watches") ProductType = "Watch";
                    if (Product.TypeData.Count == 3 && Product.TypeData[2].Contains(Spliter))
                    {
                        Sy.String Ext = Product.TypeData[2];
                        ExtValue = Ext.After(Spliter);
                    }
                }
                else
                {
                    ErrMessage = "节点分类错误,无法解析";
                    return null;
                }
            }
            else
            {
                ProductData = ((EnumProductData)Convert.ToInt32(Product.TypeDataID[0])).ToString();
                ProductType = ((EnumProductData)Convert.ToInt32(Product.TypeDataID[1])).ToString();
               if (Product.TypeDataID.Count == 3) ExtValue = ((EnumProductData)Convert.ToInt32(Product.TypeDataID[2])).ToString();
            }
            ProductData = ProductData.Replace("Parent_", "").Replace("Child_", "");
            ProductType = ProductType.Replace("Parent_", "").Replace("Child_", "");
            ExtValue = ExtValue.Replace("_", "-");
            return new List<string>() { ProductData, ProductType, ExtValue };
        }
        /// <summary>
        /// 基础信息部分
        /// </summary>
        /// <param name="Products"></param>
        /// <param name="Shop"></param>
        /// <returns></returns>
        private AmazonEnvelope GetMutiProInfo(List<ITaskProduct> Products, Imp_ShopInfo Shop, bool IsMainProduct, string MarketPlaceID, ref string ErrMessage)
        {
            AmazonEnvelope pro = new AmazonEnvelope
            {
                Header = new AmazonEnvelope.Cls_Header()
            };
            pro.Header.DocumentVersion = "1.01";
            pro.Header.MerchantIdentifier = Shop.SellerID;
            pro.MessageType = "Product";
            pro.PurgeAndReplace = "false";
            pro.Message = new List<Message>();
            int i = 1;
            foreach (var Product in Products)
            {
                var Data = GetProductData(Product, ref ErrMessage);
                if (Data.IsNull()) return null;
                string ProductData = Data[0];
                string ProductType = Data[1];
                string ExtValue = Data[2];
                //1.
                if (IsMainProduct)
                {
                    var ChangeData = GetChangeData(Product.Sku);
                    if (ChangeData.IsNull() || ChangeData.ProductData.IsNull()) continue;
                    AmazonAction Action = ChangeData.ProductData.Action;
                    //不需要上传的条件
                    if (Action == AmazonAction.Update || Action == AmazonAction.PartialUpdate || Action == AmazonAction.Delete) continue;
                    string InvokeAction = ((AmazonAction)((int)Action - 4)).ToString();
                    ITaskVaritant var = null;
                    if (Product.autoGenerate && Product.Products.Count > 0) var = Product.Products[0];
                    Message message = UploadPro(Product, i, var, Shop, InvokeAction, ProductData, ProductType, ref ErrMessage);//Message为基础信息
                    if (message == null)
                    {
                        TaskProducts.Remove(Product);
                        Product.ProductReportError( ErrMessage);
                    }
                    else
                    {
                        string ParentTag = "parent";
                        if (Product.autoGenerate && Product.Products.IsNotNull() && Product.Products.Count > 0) ParentTag = "base-product";
                        message.Product.ProductData = GetProData(Product, ParentTag, ProductData, ProductType, ExtValue, MarketPlaceID, ref ErrMessage);
                        pro.Message.Add(message);
                    }
                    i++;
                }
                else
                {
                    //变体
                    foreach(var Pro in Product.Products)
                    {
                        var ChangeData = GetChangeData(Pro.Sku);
                        if (ChangeData.IsNull() || ChangeData.VariantData.IsNull()) continue;
                        string InvokeAction = "";
                        if (ChangeData.IsDelete) InvokeAction = AmazonAction.Delete.ToString();
                        else
                        {
                            AmazonAction Action = ChangeData.VariantData.Action;
                            //不需要上传的条件
                            if (Action == AmazonAction.Update || Action == AmazonAction.PartialUpdate || Action == AmazonAction.Delete) continue;
                            InvokeAction = ((AmazonAction)((int)Action - 4)).ToString();
                        }
                        Message message = UploadPro(Product, i, Pro, Shop, InvokeAction, ProductData, ProductType, ref ErrMessage);
                        if (message == null)
                        {
                            TaskProducts.Remove(Product);
                            Pro.ReportError(SessionType.VARIANT, ErrMessage);
                            break;
                        }
                        else
                        {
                            if (InvokeAction != "Delete")
                            {
                                var TempData = GetProData(Product, "child", ProductData, ProductType, ExtValue, MarketPlaceID, ref ErrMessage, Pro);
                                if (TempData.IsNull()) return null;
                                message.Product.ProductData = TempData;
                            }
                            pro.Message.Add(message);
                        }
                        i++;
                    }
                }
            }
            if (TaskProducts.Count == 0) return null;
            if (pro.Message.Count == 0)
            {
                ErrMessage = "NoMessage";
                return null;
            }
            return pro;
        }
        /// <summary>
        /// 基础信息Message部分
        /// </summary>
        /// <param name="Product"></param>
        /// <param name="i"></param>
        /// <param name="VarPro"></param>
        /// <param name="Shop"></param>
        /// <param name="ErrMessage"></param>
        /// <returns></returns>
        private Message UploadPro(ITaskProduct Product, int i, ITaskVaritant VarPro, Imp_ShopInfo Shop, string Action, string ProductData, string ProductType, ref string ErrMessage)
        {
            //父产品
            if (VarPro.IsNull())
            {
                //判断是否设置SKU
                if (string.IsNullOrEmpty(Product.Sku))
                {
                    ErrMessage = I18N.Instance.MWS_Log7;
                    return null;
                }
                Message message = new Message
                {
                    MessageID = i.ToString(),
                    OperationType = Action,
                    Product = new Message.Cls_Product()
                };
                message.Product.SKU = Product.Sku;
                if (Product.autoGenerate ) //|| ProductData == "Miscellaneous"'
                {
                    message.Product.StandardProductID = new Message.Cls_Product.Cls_StandardProductID();
                    switch (VarPro.ProduceCodeType)
                    {
                        case CodeType.None:
                            ErrMessage = I18N.Instance.MWS_Log8; return null;
                        case CodeType.EAN:
                            message.Product.StandardProductID.Type = "EAN";
                            break;
                        case CodeType.UPC:
                            message.Product.StandardProductID.Type = "UPC";
                            break;
                        default:
                            throw new NotSupportedException();
                    }
                    message.Product.StandardProductID.Value = VarPro.ProduceCode;
                }
                //BasicMessage
                message.Product.ProductTaxCode = "A_GEN_TAX";
                if (ProductData == "Health") message.Product.ItemPackageQuantity = "1";
                message.Product.DescriptionData = new Message.Cls_Product.Cls_DescriptionData();
                //title
                if (Product.Name.Length > 200) { ErrMessage = I18N.Instance.MWS_Log9; return null; }
                message.Product.DescriptionData.Title = Product.Name.Replace("<", "[").Replace(">", "]");//SP.title;
                //Brand
                string Brand = "DTKJ";
                string Manufacturer= "DTKJ";
                double Weight = 0;
                //检验Brand是否为空
                if (Product.Fields.IsNotNull())
                {
                    foreach (var v in Product.Fields)
                    {
                        if (v.Name == "brand" && v.Data.ToString().IsNotWhiteSpace()) Brand = v.Data.ToString();
                        if (v.Name == "facturer" && v.Data.ToString().IsNotWhiteSpace()) Manufacturer = v.Data.ToString();
                        if (v.Name == "weight" && v.Data.ToString().IsNotWhiteSpace()) Weight = Convert.ToDouble(v.Data);
                    }
                }
                message.Product.DescriptionData.Brand = Brand;//品牌 必要的参数
                                                                      //Description
                if (Product.Description != null)
                {
                    if (Product.Description.Length > 2000) { ErrMessage = I18N.Instance.MWS_Log10; return null; }
                    message.Product.DescriptionData.Description = Product.Description;
                }
                //BulletPoint
                if (!Product.BulletPoint.IsNullOrEmpty())
                {
                    if (!Product.BulletPoint.All((bp) => bp.Length < 500 && bp.Trim().Length > 0)) { ErrMessage = I18N.Instance.MWS_Log11; return null; }
                    if (Product.BulletPoint.Count > 5) { ErrMessage = I18N.Instance.MWS_Log12; return null; }
                    else
                    {
                        message.Product.DescriptionData.BulletPoint = Product.BulletPoint;
                    }
                }
                //MaxOrderQuantity
                message.Product.DescriptionData.MaxOrderQuantity = "5";
                //Manufacturer
                message.Product.DescriptionData.Manufacturer = Manufacturer;//生产厂家 必要的参数
                if (ProductData == "ToysBaby" || ProductData == "Toys")
                {
                    message.Product.DescriptionData.TargetAudience = "Unisex";
                }
                //SearchTerms
                if (!Product.SearchTerms.IsNullOrEmpty() )
                {
                    if (Product.SearchTerms.Length >= 250)
                    {
                        ErrMessage = I18N.Instance.MWS_Log13;
                        return null;
                    }
                    message.Product.DescriptionData.SearchTerms = new List<string>() { Product.SearchTerms };
                }
                //message.Product.DescriptionData.ExternalProductInformation = "Default";
                //RecommendedBrowseNode该信息仅用于加拿大，欧洲和日本的商家帐户
                string Area = Shop.MarketPlace.URL;
                if (Area.Contains(".uk") || Area.Contains(".ca") || Area.Contains(".fr") || Area.Contains(".de") || Area.Contains(".it") || Area.Contains(".es") || Area.Contains(".in") || Area.Contains(".jp") || Area.Contains(".com.au"))
                {
                    string NodeID = Product.BrowserNodeID;
                    if (NodeID.IsNullOrEmpty())
                    {
                        NodeID = "0";
                    }
                    message.Product.DescriptionData.RecommendedBrowseNode = NodeID;//必要的参数
                }
                else
                {//com美国站直接采用网页端设定的BrowserNodeID字符串
                    message.Product.DescriptionData.ItemType = Product.BrowserNodeID;
                }
                //重量
                if (Weight == 0) Weight = 1000;//前台传过来的单位为g
                message.Product.DescriptionData.ItemWeight = new Message.Cls_Product.Cls_DescriptionData.WeightDimension();
                message.Product.DescriptionData.ItemWeight.WeightValue = (Weight / 1000).ToString("0.00");
                message.Product.DescriptionData.ItemWeight.unitOfMeasure = "KG";
                message.Product.DescriptionData.CountryOfOrigin = Product.CountryOfOrigin;
                if (ProductType == "Office") message.Product.DescriptionData.IsExpirationDatedProduct = false;
                if (ProductData == "Beauty") message.Product.IsHeatSensitive = false;
                return message;
            }
            else
            {
                if (VarPro.Sku.IsNullOrEmpty())
                {
                    ErrMessage = I18N.Instance.MWS_Log7;
                    return null;
                }
                Message message = new Message
                {
                    MessageID = i.ToString(),
                    OperationType = Action,
                };
                message.Product = new Message.Cls_Product();
                message.Product.SKU = VarPro.Sku;
                if (Action == "Delete") return message;
                message.Product.StandardProductID = new Message.Cls_Product.Cls_StandardProductID();
                switch (VarPro.ProduceCodeType)
                {
                    case CodeType.None:
                        ErrMessage = I18N.Instance.MWS_Log8; return null;
                    case CodeType.EAN:
                        message.Product.StandardProductID.Type = "EAN";
                        break;
                    case CodeType.UPC:
                        message.Product.StandardProductID.Type = "UPC";
                        break;
                    default:
                        throw new NotSupportedException();
                }
                message.Product.StandardProductID.Value = VarPro.ProduceCode;
                //BasicMessage
                message.Product.ProductTaxCode = "A_GEN_TAX";
                if (ProductData == "Health") message.Product.ItemPackageQuantity = "1";
                message.Product.DescriptionData = new Message.Cls_Product.Cls_DescriptionData();
                //title
                if (VarPro.Name.Length > 200) { ErrMessage = I18N.Instance.MWS_Log9; return null; }
                message.Product.DescriptionData.Title = VarPro.Name.Replace("<", "[").Replace(">", "]");
                //品牌 必要的参数
                //Brand
                string Brand = "DTKJ";
                string Manufacturer = "DTKJ";
                double Weight = 0;
                //检验Brand是否为空
                if (VarPro.Fields.IsNotNull())
                {
                    foreach (var v in VarPro.Fields)
                    {
                        if (v.Name == "brand" && v.Data.ToString().IsNotWhiteSpace()) Brand = v.Data.ToString();
                        if (v.Name == "facturer" && v.Data.ToString().IsNotWhiteSpace()) Manufacturer = v.Data.ToString();
                        if (v.Name == "weight" && v.Data.ToString().IsNotWhiteSpace()) Weight = Convert.ToDouble(v.Data);
                    }
                }
                message.Product.DescriptionData.Brand = Brand;//品牌 必要的参数
                //Description
                if (VarPro.Description != null)
                {
                    if (VarPro.Description.Length > 2000) { ErrMessage = I18N.Instance.MWS_Log10; return null; }
                    message.Product.DescriptionData.Description = VarPro.Description;
                }
                //BulletPoint
                if (!VarPro.BulletPoint.IsNullOrEmpty())
                {
                    if (!VarPro.BulletPoint.All((bp) => bp.Length < 500 && bp.Trim().Length > 0)) { ErrMessage = I18N.Instance.MWS_Log11; return null; }
                    if (VarPro.BulletPoint.Count > 5) { ErrMessage = I18N.Instance.MWS_Log12; return null; }
                    else
                    {
                        message.Product.DescriptionData.BulletPoint = VarPro.BulletPoint;
                    }
                }
                //MaxOrderQuantity
                message.Product.DescriptionData.MaxOrderQuantity = "5";
                //Manufacturer
                message.Product.DescriptionData.Manufacturer = Manufacturer;//生产厂家 必要的参数
                                                                            //特殊字段
                //特殊字段
                if (ProductData == "AutoAccessoryMisc" || ProductData == "Jewelry" || ProductData == "Health" || ProductData == "Office" || ProductData == "Home" || ProductData == "AutoAccessory" || ProductData == "Beauty")
                    message.Product.DescriptionData.MfrPartNumber = VarPro.ProduceCode;
                //特殊字段
                if (ProductData == "ToysBaby" || ProductData == "Toys")
                {
                    message.Product.DescriptionData.TargetAudience = "Baby or Adult";
                }
                //SearchTerms
                if (!VarPro.SearchTerms.IsNullOrEmpty())
                {
                    if (VarPro.SearchTerms.Length > 250)
                    {
                        ErrMessage = I18N.Instance.MWS_Log13;
                        return null;
                    }
                    message.Product.DescriptionData.SearchTerms = new List<string> { VarPro.SearchTerms };
                }
                //message.Product.DescriptionData.ExternalProductInformation = "Default";
                //RecommendedBrowseNode该信息仅用于加拿大，欧洲和日本的商家帐户
                string Area = Shop.MarketPlace.URL;
                if (Area.Contains(".uk") || Area.Contains(".ca") || Area.Contains(".fr") || Area.Contains(".de") || Area.Contains(".it") || Area.Contains(".es") || Area.Contains(".in") || Area.Contains(".jp") || Area.Contains(".com.au"))
                {
                    string NodeID = Product.BrowserNodeID;
                    if (NodeID.IsNullOrEmpty())
                    {
                        NodeID = "0";
                    }
                    message.Product.DescriptionData.RecommendedBrowseNode = NodeID;//必要的参数
                }
                //重量
                if (Weight == 0) Weight = 1000;//前台传过来的单位为g
                message.Product.DescriptionData.ItemWeight = new Message.Cls_Product.Cls_DescriptionData.WeightDimension();
                message.Product.DescriptionData.ItemWeight.WeightValue = (Weight / 1000).ToString("0.00");
                message.Product.DescriptionData.ItemWeight.unitOfMeasure = "KG";
                message.Product.DescriptionData.CountryOfOrigin = Product.CountryOfOrigin;
                if (ProductType== "Office") message.Product.DescriptionData.IsExpirationDatedProduct = false;
                if (ProductData == "Beauty") message.Product.IsHeatSensitive = false;
                return message;
            }
        }
        private int GetByteCount(string Data)
        {
            return System.Text.Encoding.UTF8.GetByteCount(Data);
        }
        private Cls_SkuChangeData GetChangeData(string Sku)
        {
            if (SkuToChangeData.IsNotNull() && SkuToChangeData.ContainsKey(Sku)) return SkuToChangeData[Sku];
            return null;
        }
        private string GetFootwearSizeSystemByMarketID(string MarketPlaceID)
        {
            if (MarketPlaceID == "ATVPDKIKX0DER") return "us_footwear_size_system";
            if (MarketPlaceID == "A1F83G8C2ARO7P") return "uk_footwear_size_system";
            if (MarketPlaceID == "A1VC38T7YXB528") return "jp_footwear_size_system";
            return "eu_footwear_size_system";
        }
        private string GetApparelSizeSystemByMarketID(string MarketPlaceID)
        {
            if (MarketPlaceID == "A1F83G8C2ARO7P") return "as8";
            if (MarketPlaceID == "A13V1IB3VIYZZH" || MarketPlaceID == "A1RKKUPIHCS9HS" || MarketPlaceID == "A2VIGQ35RCS4UG" || MarketPlaceID == "A17E79C6D8DWNP") return "as4";
            if (MarketPlaceID == "A1PA6795UKMFR9" || MarketPlaceID == "A1805IZSGTT6HS" || MarketPlaceID == "A2NODRKZP88ZB9" || MarketPlaceID == "A1C3SOZRARQ6R3") return "as3";
            if (MarketPlaceID == "APJ6JRA9NG5V4") return "as6";
            if (MarketPlaceID == "A21TJRUUN4KGV") return "as5";
            if (MarketPlaceID == "A1VC38T7YXB528") return "as7";
            if (MarketPlaceID == "A2Q3Y263D00KWC") return "as2";
            else return "as1";
        }
        /// <summary>
        /// 产品类型属性部分
        /// </summary>
        /// <param name="Up"></param>
        /// <param name="ParentTag"></param>
        /// <param name="ProductData"></param>
        /// <param name="ProductType"></param>
        /// <param name="AttrPro"></param>
        /// <returns></returns>
        private Message.Cls_Product.Cls_ProductData GetProData(ITaskProduct Product, string ParentTag, string ProductData, string ProductType, string ExtValue, string MarketPlaceID, ref string ErrMessage, ITaskVaritant AttrPro = null)
        {
            Message.Cls_Product.Cls_ProductData pd = new Message.Cls_Product.Cls_ProductData();
            pd.ProductType = new Message.Cls_Product.Cls_ProductData.Cls_ProductType();
            if (ProductData == "ToysBaby" && ProductType == "ToysAndGames" || ProductData == "Toys")
            {
                pd.AgeRecommendation = new Message.Cls_Product.Cls_AgeRecommendation();
                pd.AgeRecommendation.MinimumManufacturerAgeRecommended = new Message.Cls_Product.Cls_AgeRecommendation.AgeDimension();
                pd.AgeRecommendation.MinimumManufacturerAgeRecommended.unitOfMeasure = "months";
                pd.AgeRecommendation.MinimumManufacturerAgeRecommended.AgeValue = "1";
                if (ParentTag == "child") pd.IsAdultProduct = "true";
            }
            if (ParentTag == "base-product") return pd;
            //CameraPhoto与其它xml类型不一致（不存在VariationData）
            if (ProductData == "CameraPhoto")
            {
                pd.VariationTheme = "CustomerPackageType";
                pd.Parentage = ParentTag;//在parentdata下，应在parenttype下
                if (ParentTag == "child")
                {
                    string Value = GetAttributes(Product, AttrPro);
                    pd.CustomerPackageType = Value;
                }
                return pd;
            }
            pd.ProductType.VariationData = new Message.Cls_Product.Cls_ProductData.Cls_VariationData();
            ////ParentTag在XML中的位置(有变体的Parentage设置)
            if (ProductData != "Home") pd.ProductType.VariationData.Parentage = ParentTag;
            else if (ProductData == "Home") pd.Parentage = ParentTag;
            //变体存在样式'{Color:[Red,Blue],Size:['xx','xxx']}
            Sy.String AllAttributeText = string.Join(",", Product.Attribute.Keys).ToLower();
            //其余分类属性设置
            string[] Sizes = new string[] { "サイズ", "tamaño de las bandas", "talla", "taglia", "size", "尺寸", "尺码", "size name", "lantern size", "tamaño", "大きさ", "tamaño del zapato", "band width", "ring size", "longitud del envoltorio", "stretched length", "compatible models", "number of tiers", "belt length", "lunch box capacity", "number of compartments", "kid size", "bands size", "cup size", "christmas hat size", "rozmiar", "chima", "사이즈", "größe", "la taille", "dimensione", "размер", "tamanho", "size_name", "length", "ball diameter", "christmas bell size", "christmas tree height" };
            string[] Colors = new string[] { "颜色", "color", "cor", "colour", "color name", "眼色", "颜", "色", "couleur", "farbe", "colore", "xim", "koloro", "색깔", "kolor", "9", "顏色", "colour name", "색상", "renk", "Цвет", "צבע", "اللون", "metal color", "band color", "handle color", "カラー", "nombre del color" };
            //直接设置Ligthting分类
            if (ProductData == "Lighting" && !string.IsNullOrEmpty(AllAttributeText))
            {
                if (ProductType == "LightsAndFixtures")
                {
                    pd.ProductType.VariationData.VariationTheme = "color";
                    if (ParentTag == "child")
                    {
                        string Value = GetAttributes(Product, AttrPro);
                        pd.ProductType.VariationData.Color = Value;
                        pd.ProductType.VariationData.ColorMap = Value;
                    }
                }
                else if (ProductType == "LightingAccessories") pd.ProductType.VariationData = null;
                else //if (ProductType == "LightBulbs" || ProductType == "LightFixture")
                {
                    pd.ProductType.VariationData.VariationTheme = "color";
                    if (ParentTag == "child")
                    {
                        string WattageValue = GetOtherAttribute(Product, AttrPro, Colors, Sizes);
                        string ColorValue = GetAttributes(Product, AttrPro);
                        if (WattageValue.IsNullOrEmpty())
                        {
                            ErrMessage = "该分类需要设置Wattage(瓦数)属性!";
                            return null;
                        }
                        int WattageVal = Convert.ToInt32(Inf_Collecting.Cls_DefaultProperty.Instance.get_PriceValue(WattageValue));
                        if (WattageVal == 0)
                        {
                            ErrMessage = "该分类需要设置Wattage(瓦数)属性!";
                            return null;
                        }
                        if (ProductType == "LightBulbs")
                        {
                            pd.ProductType.VariationData.Color = ColorValue;
                            pd.ProductType.VariationData.Wattage = WattageVal.ToString();//所有小类都有的属性
                        }
                        else //if (ProductType == "Candle" || ProductType == "StringLight" || ProductType == "Lamp" || ProductType == "LightFixture")
                        {
                            pd.ProductType.Color = ColorValue;
                            pd.ProductType.Wattage = WattageVal.ToString();//所有小类都有的属性
                        }
                    }
                }
                return pd;
            }
            //直接设置Ligthting分类
            if (ProductData == "Office" || ProductData == "AutoAccessory")
            {
                pd.ProductType.VariationData.VariationTheme = "Color";
                if (ParentTag == "child")
                {
                    pd.ProductType.ColorSpecification = new Message.Cls_Product.Cls_ProductData.Cls_ProductType.Cls_ColorSpecification();
                    string ColorValue = GetAttributes(Product, AttrPro);
                    pd.ProductType.ColorSpecification.Color = ColorValue;
                    pd.ProductType.ColorSpecification.ColorMap = ColorValue;
                    //if (ProductData == "AutoAccessory")
                    //{
                    //}
                }
                return pd;
            }
            //电子产品
            if (ProductData == "CE")
            {
                if (ProductType != "ConsumerElectronics" && ProductType != "NetworkAdapter" && ProductType != "KindleAccessories" && ProductType != "KindleEReaderAccessories" && ProductType != "KindleFireAccessories" && ProductType != "CellularPhoneCase" && ProductType != "ScreenProtector" && ProductType != "ChargingAdapter" && ProductType != "PowerBank" && ProductType != "CellularPhone")
                {
                    pd.ProductType.VariationData = null;
                    if (ProductType == "PC")
                    {
                        pd.ProductType.HardDriveSizeize = new Message.Cls_Product.Cls_ProductData.Cls_ProductType.Cls_HardDriveSize()
                        {
                            HardDriveSizeValue = "1",
                            unitOfMeasure = "TB"
                        };
                        pd.ProductType.HardDriveInterface = "usb_3.0";
                        pd.ProductType.ComputerMemoryType = "ddr4_sdram";
                        pd.ProductType.RAMSize = new Message.Cls_Product.Cls_ProductData.Cls_ProductType.Cls_RAMSize()
                        {
                            RAMSizeValue = "4",
                            unitOfMeasure = "GB"
                        };
                        pd.ProductType.ProcessorBrand = "Brand";
                        pd.ProductType.ProcessorSpeed = new Message.Cls_Product.Cls_ProductData.Cls_ProductType.Cls_ProcessorSpeed()
                        {
                            ProcessorSpeedValue = "1",
                            unitOfMeasure = "Hz"
                        };
                        pd.ProductType.ProcessorType = "";
                        pd.ProductType.ProcessorCount = "1";
                        pd.ProductType.HardwarePlatform = "Plateform";
                    }
                    else if (ProductType == "PDA")
                    {
                        pd.ProductType.ComputerMemoryType = "ddr4_sdram";
                        pd.ProductType.RAMSize = new Message.Cls_Product.Cls_ProductData.Cls_ProductType.Cls_RAMSize()
                        {
                            RAMSizeValue = "4",
                            unitOfMeasure = "GB"
                        };
                        pd.ProductType.ScreenResolution = "ScreenResolution";
                        pd.ProductType.ColorScreen = "true";
                    }
                    else return pd;
                }
            }
            //食品分类专有设置字段
            if (ProductData == "FoodAndBeverages" || ProductData == "Gourmet")
            {
                pd.ProductType.VariationData.VariationTheme = "Flavor";
                if (ParentTag == "child")
                {
                    string AttroValue = GetAttributes(Product, AttrPro);
                    pd.ProductType.VariationData.Flavor = AttroValue;
                }
                pd.ProductType.UnitCount = new Message.Cls_Product.Cls_ProductData.Cls_ProductType.Cls_UnitCount()
                {
                    unitOfMeasure = "Default",
                    UnitCountValue = 1
                };
                return pd;
            }
            //美国站 日本站 Health设置必填字段
            if ((ProductData == "Health" || ProductData == "Beauty" || ProductType == "HealthMisc") && ParentTag == "child")//&& (MarketPlaceID == "ATVPDKIKX0DER" || MarketPlaceID == "A1VC38T7YXB528")
            {
                pd.ProductType.UnitCount = new Message.Cls_Product.Cls_ProductData.Cls_ProductType.Cls_UnitCount()
                {
                    unitOfMeasure = "Count",
                    UnitCountValue = 1
                };
            }
            //SizeColor
            string Color = "", Size = "", OtherValue = "";
            if (AttrPro.IsNotNull())
            {
                Color = GetAttribute(Product, AttrPro, Colors);
                Size = GetAttribute(Product, AttrPro, Sizes);
                OtherValue = GetOtherAttribute(Product, AttrPro, Colors, Sizes);
            }
            //SizeColor
            if (AllAttributeText.ContainsOne(Colors) && AllAttributeText.ContainsOne(Sizes))
            {
                //将额外属性值也填充之已有变体属性中
                if (OtherValue.Length > 0) Size = $"{Size},{OtherValue}";
                if (ProductData == "MusicalInstruments" || ProductData == "PetSupplies")
                {
                    if (ProductType == "InstrumentPartsAndAccessories" || ProductType == "PercussionInstruments" || ProductType == "StringedInstruments" || ProductData == "PetSupplies")
                        pd.ProductType.VariationData.VariationTheme = "SizeColor";
                    else pd.ProductType.VariationData.VariationTheme = "Color";
                    if (ParentTag == "child")
                    {
                        pd.ProductType.ColorSpecification = new Message.Cls_Product.Cls_ProductData.Cls_ProductType.Cls_ColorSpecification();
                        pd.ProductType.ColorSpecification.Color = Color;
                        pd.ProductType.ColorSpecification.ColorMap = Color;
                        pd.ProductType.Size = Size;
                    }
                }
                else
                {
                    if (ProductData == "Clothing" || ProductData == "Shoes") pd.ProductType.VariationData.VariationTheme = "SizeColor";
                    else if (ProductData == "Sports") pd.ProductType.VariationData.VariationTheme = "ColorSize";
                    else if (ProductData == "Jewelry") SetJewelryVariationTheme(pd, ProductType);
                    else pd.ProductType.VariationData.VariationTheme = "Size-Color";
                    if (ParentTag == "child")
                    {
                        if (ProductData != "Office" && ProductData != "AutoAccessory")
                        {
                            string GemType = ExtValue;
                            if (ProductData != "Jewelry")
                            {
                                pd.ProductType.VariationData.Size = Size;
                                pd.ProductType.VariationData.Color = Color;
                            }
                            else SetJewelryVariationData(pd, ProductType, GetAttributes(Product, AttrPro), GemType, Color, Size);//Jewelry
                        }
                    }
                }
            }
            else if (AllAttributeText.ContainsOne(Sizes))
            {
                //将额外属性值也填充之已有变体属性中
                if (OtherValue.Length > 0) Size = $"{Size},{OtherValue}";
                pd.ProductType.VariationData.VariationTheme = "Size";
                if (ProductData == "Clothing" || ProductData == "Shoes")
                {
                    pd.ProductType.VariationData.VariationTheme = "SizeColor";
                    if (ParentTag == "child")
                    {
                        pd.ProductType.VariationData.Size = Size;
                        pd.ProductType.VariationData.Color = "As the picture shows";
                    }
                }
                else if (ProductData == "MusicalInstruments" || ProductData == "PetSupplies")
                {
                    if (ProductType == "InstrumentPartsAndAccessories" || ProductType == "PercussionInstruments" || ProductType == "StringedInstruments" || ProductData == "PetSupplies")
                        pd.ProductType.VariationData.VariationTheme = "Size";
                    else pd.ProductType.VariationData.VariationTheme = null;
                    if (ParentTag == "child")
                    {
                        pd.ProductType.Size = Size;
                    }
                }
                else
                {
                    if (ProductData == "Jewelry") SetJewelryVariationTheme(pd, ProductType);
                    if (ParentTag == "child")
                    {
                        if (ProductData != "Office" && ProductData != "AutoAccessory")
                        {
                            string GemType = ExtValue;
                            if (ProductData == "Jewelry") SetJewelryVariationData(pd, ProductType, Size, GemType, null, Size);//Jewelry
                            else
                            {
                                //pd.ProductType.VariationData.Color = "As the picture shows";
                                pd.ProductType.VariationData.Size = Size;
                            }
                        }
                    }
                }
            }
            else if (AllAttributeText.ContainsOne(Colors))
            {
                //将额外属性值也填充之已有变体属性中
                if (OtherValue.Length > 0) Color = $"{Color},{OtherValue}";
                pd.ProductType.VariationData.VariationTheme = "Color";
                if (ProductData == "Clothing" || ProductData == "Shoes")
                {
                    pd.ProductType.VariationData.VariationTheme = "SizeColor";
                    if (ParentTag == "child")
                    {
                        pd.ProductType.VariationData.Size = "As the picture shows";
                        pd.ProductType.VariationData.Color = Color;
                    }
                }
                else if (ProductData == "MusicalInstruments" || ProductData == "PetSupplies")
                {
                    if (ParentTag == "child")
                    {
                        pd.ProductType.ColorSpecification = new Message.Cls_Product.Cls_ProductData.Cls_ProductType.Cls_ColorSpecification();
                        pd.ProductType.ColorSpecification.Color = Color;
                        pd.ProductType.ColorSpecification.ColorMap = Color;
                    }
                }
                else
                {
                    if (ProductData == "Jewelry") SetJewelryVariationTheme(pd, ProductType);
                    if (ParentTag == "child")
                    {
                        if (ProductData != "Office" && ProductData != "AutoAccessory")
                        {
                            string GemType = ExtValue;
                            if (ProductData == "Jewelry") SetJewelryVariationData(pd, ProductType, Color, GemType, Color, null);//Jewelry
                            else
                            {
                                pd.ProductType.VariationData.Color = Color;
                            }
                        }
                    }
                }
            }
            //未提供Size和Color得情况
            else
            {
                string StyleName = "";
                if (AttrPro.IsNotNull())
                {
                    StyleName = GetAttributes(Product, AttrPro);
                }
                pd.ProductType.VariationData.VariationTheme = "Color";
                if (ProductData == "Clothing" || ProductData == "Shoes")
                {
                    pd.ProductType.VariationData.VariationTheme = "SizeColor";
                    if (ParentTag == "child")
                    {
                        pd.ProductType.VariationData.Size = "As the picture shows";
                        pd.ProductType.VariationData.Color = StyleName;
                    }
                }
                else if (ProductData == "MusicalInstruments" || ProductData == "PetSupplies")
                {
                    if (ParentTag == "child")
                    {
                        pd.ProductType.ColorSpecification = new Message.Cls_Product.Cls_ProductData.Cls_ProductType.Cls_ColorSpecification();
                        pd.ProductType.ColorSpecification.Color = StyleName;
                        pd.ProductType.ColorSpecification.ColorMap = StyleName;
                    }
                }
                else
                {
                    if (ProductData == "Jewelry") SetJewelryVariationTheme(pd, ProductType);
                    if (ParentTag == "child")
                    {
                        if (ProductData != "Office" && ProductData != "AutoAccessory")
                        {
                            string GemType = ExtValue;
                            if (ProductData == "Jewelry") SetJewelryVariationData(pd, ProductType, StyleName, GemType);//Jewelry
                            else
                            {
                                pd.ProductType.VariationData.Color = StyleName;
                            }
                        }
                    }
                }
            }
            //Miscellaneous分类特殊处理
            if (ProductData == "Miscellaneous" && ParentTag == "child")
            {
                if (Color.IsNotWhiteSpace()) pd.Color = Color;
                if (Size.IsNotWhiteSpace()) pd.Size = Size;
            }
            if (ProductData == "Computers" || ProductData == "CE" && ParentTag == "child")
            {
                if (Color.IsNotWhiteSpace())
                {
                    pd.Color = Color;
                    pd.ProductType.VariationData.Color = null;
                }
                if (Size.IsNotWhiteSpace())
                {
                    pd.Size = Size;
                    pd.ProductType.VariationData.Size = null;
                }
            }
            if (ProductType == "CellularPhone")
            {
                pd.ProductType.FrontWebcamResolution = new Message.Cls_Product.Cls_ProductData.Cls_ProductType.Cls_FrontWebcamResolution();
                pd.ProductType.FrontWebcamResolution.unitOfMeasure = "megapixels";
                pd.ProductType.FrontWebcamResolution.UnitCountValue = "800";
                pd.ProductType.ModelName = "Default";
            }
            //特殊分类特殊处理
            if (ProductData == "Clothing" || ProductData == "Shoes")
            {
                pd.ProductType.ClassificationData = new Message.Cls_Product.Cls_ProductData.Cls_ProductType.Cls_ClassificationData();
                pd.ProductType.ClassificationData.ClothingType = ProductType;
                pd.ProductType.ClassificationData.Department = "womens";
                if (ParentTag == "child" && Color.Length > 0) pd.ProductType.ClassificationData.ColorMap = Color;
                pd.ProductType.ClassificationData.MaterialComposition = "Defalut";
                if (ProductData == "Clothing") pd.ProductType.ClassificationData.OuterMaterial = "Defalut";
                if (ParentTag == "child" && Size.Length > 0) pd.ProductType.ClassificationData.SizeMap = Size;
                pd.ProductType.ClassificationData.MaterialType = "Canvas";
                pd.ProductType.ClassificationData.TargetGender = "female";//if (ProductData == "Shoes") 
                if (ParentTag == "child" && ProductData == "Clothing")//&& ProductType == "Blazer"
                {
                    pd.ProductType.AgeRangeDescription = "Adult";
                    if (ProductType.ToLower() == "shirt")
                    {
                        pd.ProductType.ShirtBodyType = "regular";
                        pd.ProductType.ShirtHeightType = "short";
                        pd.ProductType.ShirtSize = "one_size";
                        pd.ProductType.ShirtSizeClass = "alpha";
                        pd.ProductType.ShirtSizeSystem = GetApparelSizeSystemByMarketID(MarketPlaceID);
                    }
                    else if (ProductType.ToLower() == "pants" || ProductType.ToLower() == "shorts" || ProductType.ToLower() == "overalls")
                    {
                        pd.ProductType.BottomsBodyType = "regular";
                        pd.ProductType.BottomsHeightType = "short";
                        pd.ProductType.BottomsSize = Size;//"one_size"
                        pd.ProductType.BottomsSizeClass = "alpha";
                        pd.ProductType.BottomsSizeSystem = GetApparelSizeSystemByMarketID(MarketPlaceID);
                    }
                    else if (ProductType.ToLower() == "swimwear" || ProductType.ToLower() == "bra" || ProductType.ToLower() == "corset" || ProductType.ToLower() == "undergarmentslip")
                    {
                        pd.ProductType.ShapewearBodyType = "regular";
                        pd.ProductType.ShapewearHeightType = "short";
                        pd.ProductType.ShapewearSize = "one_size";
                        pd.ProductType.ShapewearSizeClass = "alpha";
                        pd.ProductType.ShapewearSizeSystem = GetApparelSizeSystemByMarketID(MarketPlaceID);
                    }
                    else if (ProductType.ToLower() == "hat")
                    {
                        pd.ProductType.HeadwearSize = "one_size";
                        pd.ProductType.HeadwearSizeClass = "alpha";
                        pd.ProductType.HeadwearSizeSystem = GetApparelSizeSystemByMarketID(MarketPlaceID);
                    }
                    else if (ProductType.ToLower() == "skirt")
                    {
                        pd.ProductType.SkirtBodyType = "regular";
                        pd.ProductType.SkirtHeightType = "short";
                        pd.ProductType.SkirtSize = "one_size";
                        pd.ProductType.SkirtSizeClass = "alpha";
                        pd.ProductType.SkirtSizeSystem = GetApparelSizeSystemByMarketID(MarketPlaceID);
                    }
                    else if (ProductType.ToLower() == "shoes")
                    {
                        pd.ProductType.ShoeSize = "one_size";
                    }
                    else
                    {
                        pd.ProductType.ApparelSizeClass = "alpha";
                        pd.ProductType.ApparelSizeSystem = GetApparelSizeSystemByMarketID(MarketPlaceID);
                        pd.ProductType.ApparelBodyType = "regular";
                        if (MarketPlaceID == "A1VC38T7YXB528") pd.ProductType.ApparelHeightType = "long";
                        else pd.ProductType.ApparelHeightType = "regular";
                        pd.ProductType.ApparelSize = "one_size";
                    }
                }
                if (ParentTag == "child" && ProductData == "Shoes" && (ProductType == "Shoes" || ProductType == "Boot" || ProductType == "Sandal" || ProductType == "TechnicalSportShoe"))
                {
                    //if (MarketPlaceID == "ATVPDKIKX0DER" || MarketPlaceID == "A1F83G8C2ARO7P" || MarketPlaceID == "A1RKKUPIHCS9HS" || MarketPlaceID == "A13V1IB3VIYZZH" || MarketPlaceID == "APJ6JRA9NG5V4" || MarketPlaceID == "A1PA6795UKMFR9" || MarketPlaceID == "A21TJRUUN4KGV" || MarketPlaceID == "A1VC38T7YXB528")
                    //{ }
                    pd.ProductType.ShoeSizeComplianceData = new Message.Cls_Product.Cls_ProductData.Cls_ProductType.Cls_ShoeSizeComplianceData();
                    pd.ProductType.ShoeSizeComplianceData.AgeRangeDescription = "adult";
                    string FootwearSizeSystem = GetFootwearSizeSystemByMarketID(MarketPlaceID);
                    pd.ProductType.ShoeSizeComplianceData.FootwearSizeSystem = FootwearSizeSystem;
                    pd.ProductType.ShoeSizeComplianceData.ShoeSizeAgeGroup = "adult";
                    if (MarketPlaceID == "ATVPDKIKX0DER" || MarketPlaceID == "A1F83G8C2ARO7P" || MarketPlaceID == "A21TJRUUN4KGV") pd.ProductType.ShoeSizeComplianceData.ShoeSizeGender = "women";
                    pd.ProductType.ShoeSizeComplianceData.ShoeSizeClass = "alpha";
                    //if (MarketPlaceID == "A1VC38T7YXB528") pd.ProductType.ShoeSizeComplianceData.ShoeSizeClass = "alpha_jaspo";
                    pd.ProductType.ShoeSizeComplianceData.ShoeSizeWidth = "1";
                    pd.ProductType.ShoeSizeComplianceData.ShoeSize = "one_size";
                }
            }
            if (ProductData == "ToysBaby" && ParentTag == "child")
            {
                if (ProductType == "BabyProducts") 
                {
                    pd.VariationData = pd.ProductType.VariationData;
                    pd.VariationData.Size = null;
                    pd.VariationData.Color = null;
                    if (Size.Length > 0) pd.Size = Size;
                    if (Color.Length > 0) pd.Color = Color;
                }
                pd.ProductType.VariationData = null;
            }
            if (ProductData == "Sports" && ParentTag == "child")
            {
                pd.VariationData = pd.ProductType.VariationData;
                pd.ProductType.VariationData = null;
            }
            if (ProductData == "Apparel" && ParentTag == "child" && (Color.Length > 0 || Size.Length > 0))
            {
                pd.ProductType.ClassificationData = new Message.Cls_Product.Cls_ProductData.Cls_ProductType.Cls_ClassificationData();
                if (Color.Length > 0) pd.ProductType.ClassificationData.ColorMap = Color;
                if (Size.Length > 0) pd.ProductType.ClassificationData.SizeMap = Size;
            }
            if (ProductData == "Health" && ParentTag == "child")
            {
                pd.ProductType.IsAdultProduct = "true";
            }
            if (ProductType == "WirelessLockedPhone" && ParentTag == "child")
            {
                pd.ProductType.WirelessProvider = "unknown_wireless_provider_type";
            }
            if (ProductType == "WirelessAccessories" && ParentTag == "child")
            {
                pd.ProductType.CompatiblePhoneModels = "unknown";
            }
            return pd;
        }
        /// <summary>
        /// 获取该变体的所有属性值
        /// </summary>
        /// <param name="Product"></param>
        /// <param name="AttrPro"></param>
        /// <returns></returns>
        private string GetAttributes(ITaskProduct Product, ITaskVaritant AttrPro)
        {
            string Value = "";
            if (AttrPro.IsNotNull() && AttrPro.IDs.IsNotNull() && AttrPro.IDs.Length > 0)
            {
                int i = 0;
                Product.Attribute.Values.ForEach((r) =>
                {
                    if (Value.Length > 0) Value += ",";
                    //Sy.MiniDump.Write(Sy.Environment.Path.get_Desktop("xxx.dmp"));
                    Value += r[AttrPro.IDs[i]];
                    i++;
                });
            }
            if (string.IsNullOrEmpty(Value)) Value = "As the picture shows";
            return Value;
        }
        /// <summary>
        /// 获取该变体所需的属性值
        /// </summary>
        /// <returns></returns>
        private string GetAttribute(ITaskProduct Product, ITaskVaritant AttrPro, string[] Attri)
        {
            string Value = "";
            if (AttrPro.IDs.IsNotNull() && AttrPro.IDs.Length > 0)
            {
                int i = 0;
                Product.Attribute.ForEach((r) =>
                {
                    Sy.String Key = r.Key.ToLower();
                    if (Key.ContainsOne(Attri) && r.Value.Length > AttrPro.IDs[i])
                    {
                        Value = r.Value[AttrPro.IDs[i]];
                        return;
                    }
                    i++;
                });
            }
            if (string.IsNullOrEmpty(Value)) Value = "As the picture shows";
            return Value;
        }
        /// <summary>
        /// 获取该变体所需以外的其它属性值
        /// </summary>
        /// <returns></returns>
        private string GetOtherAttribute(ITaskProduct Product, ITaskVaritant AttrPro, string[] Color,string[] Size)
        {
            string Value = "";
            if (AttrPro.IDs.IsNotNull() && AttrPro.IDs.Length > 0)
            {
                int i = 0;
                Product.Attribute.ForEach((r) =>
                {
                    Sy.String Key = r.Key.ToLower();
                    if (!Key.ContainsOne(Color) && !Key.ContainsOne(Size) && AttrPro.IDs.Length > i && r.Value.Length > AttrPro.IDs[i])
                    {
                        if (Value.Length > 0) Value += ",";
                        Value += r.Value[AttrPro.IDs[i]];
                        return;
                    }
                    i++;
                });
            }
            //if (string.IsNullOrEmpty(Value)) Value = "As the picture shows";
            return Value;
        }
        /// <summary>
        /// 库存Message
        /// </summary>
        /// <param name="Products"></param>
        /// <param name="Shop"></param>
        /// <returns></returns>
        private AmazonEnvelope GetMutiProInventory(List<ITaskProduct> Products, Imp_ShopInfo Shop, ref string ErrMessage)
        {
            //商品的Sku在上传时获取
            AmazonEnvelope ProInventory = new AmazonEnvelope();
            ProInventory.Header = new AmazonEnvelope.Cls_Header();
            ProInventory.Header.DocumentVersion = "1.01";
            ProInventory.Header.MerchantIdentifier = Shop.SellerID;
            ProInventory.MessageType = "Inventory";
            ProInventory.Message = new List<Message>();
            int i = 1;
            foreach (var SinglePro in Products)
            {
                foreach (var VarPro in SinglePro.Products)
                {
                    var ChangeData = GetChangeData(VarPro.Sku);
                    if (ChangeData.IsNull() || ChangeData.InventoryData.IsNull()) continue;
                    AmazonAction Action = ChangeData.InventoryData.Action;
                    //不需要上传的条件
                    if (ChangeData.IsDelete || Action == AmazonAction.Update || Action == AmazonAction.PartialUpdate || Action == AmazonAction.Delete) continue;
                    string InvokeAction = ((AmazonAction)((int)Action - 4)).ToString();

                    Message message = new Message();
                    message.MessageID = i.ToString();
                    message.OperationType = InvokeAction;
                    message.Inventory = new Message.Cls_Inventory();
                    message.Inventory.SKU = VarPro.Sku;
                    message.Inventory.Quantity = VarPro.Quantity.ToString();
                    int FulfillmentLatency = SinglePro.FulfillmentLatency;
                    if (FulfillmentLatency <= 0) FulfillmentLatency = 1;
                    message.Inventory.FulfillmentLatency = FulfillmentLatency.ToString();
                    message.Inventory.SwitchFulfillmentTo = "MFN";
                    ProInventory.Message.Add(message);
                    i++;
                }
            }
            if (ProInventory.Message.Count == 0)
            {
                ErrMessage = "NoMessage";
                return null;
            }
            return ProInventory;
        }
        /// <summary>
        /// 价格Message
        /// </summary>
        /// <param name="Up"></param>
        /// <returns></returns>
        private AmazonEnvelope GetMutiProPrice(List<ITaskProduct> Products, Imp_ShopInfo Shop, ref string ErrMessage)
        {
            //商品的Sku在上传时获取
            AmazonEnvelope ProPrice = new AmazonEnvelope();
            ProPrice.Header = new AmazonEnvelope.Cls_Header();
            ProPrice.Header.DocumentVersion = "1.01";
            ProPrice.Header.MerchantIdentifier = Shop.SellerID;
            ProPrice.MessageType = "Price";
            ProPrice.Message = new List<Message>();
            int i = 1;
            foreach (var SinglePro in Products)
            {
                foreach (var VarPro in SinglePro.Products)
                {
                    var ChangeData = GetChangeData(VarPro.Sku);
                    if (ChangeData.IsNull() || ChangeData.PricingData.IsNull()) continue;
                    AmazonAction Action = ChangeData.PricingData.Action;
                    //不需要上传的条件
                    if (ChangeData.IsDelete || Action == AmazonAction.Update || Action == AmazonAction.PartialUpdate || Action == AmazonAction.Delete) continue;
                    string InvokeAction = ((AmazonAction)((int)Action - 4)).ToString();

                    Message message = new Message();
                    message.MessageID = i.ToString();
                    message.OperationType = InvokeAction;
                    message.Price = new Message.Cls_Price();
                    message.Price.SKU = VarPro.Sku;
                    message.Price.StandardPrice = new Cls_StandardPrice();
                    message.Price.StandardPrice.currency = SinglePro.Currency.ToString();
                    message.Price.StandardPrice.PriceValue = VarPro.Price.ToString();
                    ProPrice.Message.Add(message);
                    i++;
                }
            }
            if (ProPrice.Message.Count == 0)
            {
                ErrMessage = "NoMessage";
                return null;
            }
            return ProPrice;
        }
        /// <summary>
        /// 图片Message
        /// </summary>
        /// <param name="Up"></param>
        /// <returns></returns>
        private AmazonEnvelope GetMutiProImage(List<ITaskProduct> Products, Imp_ShopInfo Shop, ref string ErrMessage)
        {
            //商品SKU在上传时获取
            AmazonEnvelope proPic = new AmazonEnvelope();
            proPic.Header = new AmazonEnvelope.Cls_Header();
            proPic.Header.DocumentVersion = "1.01";
            proPic.Header.MerchantIdentifier = Shop.SellerID;
            proPic.MessageType = "ProductImage";
            proPic.Message = new List<Message>();
            int num = 1;
            foreach (var SinglePro in Products)
            {
                foreach (var VarPro in SinglePro.Products)
                {
                    var ChangeData = GetChangeData(VarPro.Sku);
                    if (ChangeData.IsNull() || ChangeData.ImageData.IsNull()) continue;
                    AmazonAction Action = ChangeData.ImageData.Action;
                    //不需要上传的条件
                    if (ChangeData.IsDelete || Action == AmazonAction.Update || Action == AmazonAction.PartialUpdate || Action == AmazonAction.Delete) continue;
                    string InvokeAction = ((AmazonAction)((int)Action - 4)).ToString();

                    List<string> ChildPath = new List<string>();
                    if (VarPro.MainImage.IsNotWhiteSpace())
                    {
                        ChildPath.Add(VarPro.MainImage);//主图
                        ChildPath.Add(VarPro.MainImage);//缩略图（变体图）
                    }
                    //详情图
                    if(VarPro.Images.IsNotNull()) ChildPath.AddRange(VarPro.Images.Take(8).ToList());
                    //未获取到该变体的图片
                    if (ChildPath.Count == 0)
                    {
                        VarPro.ReportError(SessionType.IMAGE, I18N.Instance.MWS_Log14);
                        continue;
                    }
                    int Childi = 0;
                    foreach (string img in ChildPath)
                    {

                        Message message = new Message();
                        message.MessageID = num.ToString();
                        message.OperationType = InvokeAction;
                        message.ProductImage = new Message.Cls_ProductImage();
                        message.ProductImage.SKU = VarPro.Sku; 
                        message.ProductImage.ImageType = (YY.ImageType)Childi;
                        message.ProductImage.ImageLocation = img;
                        proPic.Message.Add(message);
                        num++; Childi++;
                    }
                }
            }
            if (proPic.Message.Count == 0)
            {
                ErrMessage = "NoMessage";
                return null;
            }
            return proPic;
        }
        /// <summary>
        /// 变体Message
        /// </summary>
        /// <param name="Up"></param>
        /// <param name="DicSku"></param>
        /// <returns></returns>
        private AmazonEnvelope GetMutiProRelationShip(List<ITaskProduct> Products, Imp_ShopInfo Shop, ref string ErrMessage)
        {
            AmazonEnvelope proRelationShip = new AmazonEnvelope();
            proRelationShip.Header = new AmazonEnvelope.Cls_Header();
            proRelationShip.Header.DocumentVersion = "1.01";
            proRelationShip.Header.MerchantIdentifier = Shop.SellerID;
            proRelationShip.MessageType = "Relationship";
            proRelationShip.Message = new List<Message>();
            int MessageID = 1;
            foreach (var SinglePro in Products)
            {
                string InvokeAction = "Update";
                Message message = new Message();
                message.MessageID = MessageID.ToString();
                message.OperationType = InvokeAction;
                message.Relationship = new Message.Cls_Relationship();
                message.Relationship.ParentSKU = SinglePro.Sku;
                message.Relationship.Relation = new List<Relation>();
                foreach (var VarPro in SinglePro.Products)
                {
                    //判断是否执行
                    var ChangeData = GetChangeData(VarPro.Sku);
                    if (ChangeData.IsNull() || ChangeData.RelationshipData.IsNull()) continue;
                    AmazonAction Action = ChangeData.RelationshipData.Action;
                    //不需要上传的条件
                    if (ChangeData.IsDelete || Action == AmazonAction.Update || Action == AmazonAction.PartialUpdate || Action == AmazonAction.Delete) continue;
                    Relation relation = new Relation();
                    relation.SKU = VarPro.Sku;
                    relation.Type = "Variation";
                    message.Relationship.Relation.Add(relation);
                }
                if (message.Relationship.Relation.Count > 0) proRelationShip.Message.Add(message);
                MessageID++;
            }
            if (proRelationShip.Message.Count == 0)
            {
                ErrMessage = "NoMessage";
                return null;
            }
            return proRelationShip;
        }
        /// <summary>
        /// 追踪上传进度的状态
        /// </summary>
        /// <param name="Tp"></param>
        /// <param name="SessionType"></param>
        private bool InvokeStatus(TaskParameter Tp, SessionType SessionType, ref string ErrMessage)
        {
            string GetProFeedMissionID = Tp.GetSessionID(SessionType);
            if (GetProFeedMissionID.IsNullOrEmpty())
            {
                ErrMessage = "FeedMissionID";//2020.09.08 
                Tp.Finish(false, SessionType, I18N.Instance.MWS_Log70);
                //$"EAN重复,请修改EAN后重试,{SessionType}_ErrNum:#1408"
                return false;
            }
            //店铺信息
            var shopInfo = Tp.Shop;
            Imp_ShopInfo Shop = new Imp_ShopInfo(shopInfo.MarketPlace, shopInfo.SellerID, shopInfo.MwsAuthToken, shopInfo.AwsAccessKey, shopInfo.SecretKey);
            //执行跟踪上传逻辑
            string UpStatus = MWS.UpdateResult(GetProFeedMissionID, Shop, ref ErrMessage);
            if (UpStatus == "_DONE_")
            {
                Tp.SetSessionState(SessionType, SessionState.Down);
                return InvokeDown(Tp, SessionType, SessionType == SessionType.PRODUCT);
            }
            else if (UpStatus == "_IN_PROGRESS_")
            {
                if(Tp.GetSessionState(SessionType)!=SessionState.Processing)Tp.SetSessionState(SessionType, SessionState.Processing);
            }
            else if (UpStatus == null && ErrMessage == "AccessDenied")
            {
                Tp.Finish(false, SessionType.PRODUCT, I18N.Instance.MWS_Log2);//店铺被封或拒绝访问或者网络因素导致的上传失败
                return false;
            }
            return true;
        }
        /// <summary>
        /// 追踪Reslut
        /// </summary>
        /// <param name="Tp"></param>
        /// <param name="SessionType"></param>
        private bool InvokeDown(TaskParameter Tp, SessionType SessionType, bool IsMainProduct)
        {
            string ProFeedMissionID = Tp.GetSessionID(SessionType);
            //店铺信息
            var shopInfo = Tp.Shop;
            Imp_ShopInfo Shop = new Imp_ShopInfo(shopInfo.MarketPlace, shopInfo.SellerID, shopInfo.MwsAuthToken, shopInfo.AwsAccessKey, shopInfo.SecretKey);
            string ErrMessage = "";
            Dictionary<string, string> ErrDic = MWS.GetSubmitResult(ProFeedMissionID, Shop, ref ErrMessage);
            List<string> ErrParnetSku = new List<string>();
            List<string> ErrSku = new List<string>();
            if (ErrDic.IsNull())
            {
                //查询失败(网络请求原因)
                if (ErrMessage.IsNullOrEmpty())
                {
                    ErrMessage = I18N.Instance.MWS_Log50;//I18N.Instance.MWS_Log2
                }
                Tp.Finish(false, SessionType, ErrMessage);
                return false;
            }
            else if (ErrDic.Count == 0 && ErrMessage.IsNotWhiteSpace() && ErrMessage.StartsWith("AllError:"))
            {
                ErrMessage = ErrMessage.Replace("AllError:", "");
                Tp.Products.ForEach((r) =>
                {
                    if (IsMainProduct)
                    {

                        r.ProductReportError(ErrMessage);
                        return;
                    }
                    else
                    {
                        r.Products.ForEach((v) =>
                        {
                            if (!r.autoGenerate)
                            {
                                ErrSku.Add(v.Sku);
                                v.ReportError(SessionType, ErrMessage);
                                return;
                            }
                        });
                    }
                });
            }
            else if (ErrDic.Count > 0)
            {
                //根据Sku上报失败信息
                foreach (var Err in ErrDic)
                {
                    //定位到任务中哪个产品出错
                    Tp.Products.ForEach((r) =>
                    {
                        if (IsMainProduct)
                        {
                            if (r.Sku == Err.Key)
                            {
                                ErrParnetSku.Add(r.Sku);
                                r.ProductReportError(Err.Value);
                                return;
                            }
                        }
                        else
                        {
                            r.Products.ForEach((v) =>
                            {
                                if (v.Sku == Err.Key)//&& !r.autoGenerate
                                {
                                    ErrSku.Add(v.Sku);
                                    v.ReportError(SessionType, Err.Value);
                                    return;
                                }
                            });
                        }
                    });
                }
            }
            Tp.SetSessionState(SessionType, SessionState.Complete);
            //主产品上报Asin
            if (SessionType == SessionType.PRODUCT)
            {
                List<string> ParentSku = new List<string>();
                foreach (var p in TaskProducts)
                {
                    p.SkuChangeData.ForEach((v) =>
                    {
                        if (v.IsMain && v.Asin.IsNullOrEmpty() && !v.IsDelete && !ErrParnetSku.Contains(v.Sku)) ParentSku.Add(v.Sku);
                    });
                }
                if (ParentSku.Count > 0)
                {
                    Dictionary<string, string> AllSkuToAsin = MWS.GetAsin(ParentSku, Shop);
                    foreach (var p in TaskProducts)
                    {
                        Dictionary<string, string> SkuToAsin = new Dictionary<string, string>();
                        string TempSku = p.Sku;
                        if (AllSkuToAsin.ContainsKey(TempSku)) SkuToAsin.Add(TempSku, AllSkuToAsin[TempSku]);//主产品
                        string SkuToAsinJson = JsonObject.Serialize(SkuToAsin);
                        p.Success(SkuToAsinJson);
                    }
                }
            }
            //变体上报Asin
            if (SessionType == SessionType.VARIANT)
            {
                //子产品完成状态下通过所有sku获取所有的Asin
                List<string> Skus = new List<string>();
                foreach (var p in TaskProducts)
                {
                    p.SkuChangeData.ForEach((v) =>
                    {
                        //排除上传失败的和主产品
                        if (!v.IsMain && v.Asin.IsNullOrEmpty() && !v.IsDelete && !ErrSku.Contains(v.Sku)) Skus.Add(v.Sku);
                    });
                }
                if (Skus.Count > 0)
                {
                    Dictionary<string, string> AllSkuToAsin = MWS.GetAsin(Skus, Shop);
                    foreach (var p in TaskProducts)
                    {
                        Dictionary<string, string> SkuToAsin = new Dictionary<string, string>();
                        //string TempSku = p.Sku;
                        //if (AllSkuToAsin.ContainsKey(TempSku)) SkuToAsin.Add(TempSku, AllSkuToAsin[TempSku]);//主产品
                        p.Products.ForEach((v) =>
                        {
                            string TempSku = v.Sku;
                            if (AllSkuToAsin.ContainsKey(TempSku) && !SkuToAsin.ContainsKey(TempSku)) SkuToAsin.Add(TempSku, AllSkuToAsin[TempSku]);
                            //防止无变体的情况异常
                        });
                        //上报Asin
                        string SkuToAsinJson = JsonObject.Serialize(SkuToAsin);
                        p.Success(SkuToAsinJson);
                    }
                }
            }
            if (!InvokeFinish(Tp,SessionType)) return true;
            IsFinish = true;
            return true;
        }

        private bool InvokeFinish(TaskParameter Tp,SessionType SessionType)
        {
            //上传成功后获取Asin并上报
            for (int i = 0; i <= 5; i++)
            {
                if (Tp.GetSessionState((SessionType)i) != SessionState.Complete)
                {
                    return false;//未结束直接再次轮询
                }
            }
            //所有都已经Complete
            //表示当前任务已结束（上传xml成功的,与GetResult无关的）
            Tp.Finish(true, SessionType, "");
            IsFinish = true;
            return true;
        }
        /// <summary>
        /// 产品Xml序列化
        /// </summary>
        /// <param name="pro"></param>
        /// <returns></returns>
        private string GetXml(AmazonEnvelope pro, bool IsMainProduct)
        {
            Sy.String FinalXml = XmlObject.Serialize(pro).Replace("utf-16", "utf-8");
            string SpliterMessage = "<Message>";
            if (FinalXml.Contains(SpliterMessage))
            {
                var XmlLst = FinalXml.Split(SpliterMessage);
                for (int i = 1; i <= XmlLst.Length - 1; i++)
                {
                    Sy.String xml = XmlLst[i];
                    #region 通过Sku获取产品类型
                    string SkuSpliterS = "<SKU>", SkuSpliterE = "</SKU>";
                    string Sku = "";
                    if (xml.CanMid(SkuSpliterS, SkuSpliterE)) Sku = xml.Mid(SkuSpliterS, SkuSpliterE);
                    ITaskProduct Product = null;
                    TaskProducts.ForEach((r) =>
                    {
                        if (IsMainProduct)
                        {
                            if (r.Sku == Sku)
                            {
                                Product = r;
                                return;
                            }
                        }
                        else
                        {
                            r.Products.ForEach((v) =>
                            {
                                if (v.Sku == Sku)
                                {
                                    Product = r;
                                    return;
                                }
                            });
                        }
                    });
                    if (Product.IsNull()) continue;
                    //string ProductType = Product.ProductType.Name;
                    //string ProdataData = Product.ProductType.Parent.Name;
                    string ErrMessage = "";
                    var Data = GetProductData(Product, ref ErrMessage);
                    if (Data.IsNull()) return null;
                    string ProductData = Data[0];
                    string ProductType = Data[1];
                    string ExtValue = Data[2];
                    #endregion 
                    string SpliterCam = "<ProductType />";
                    if (xml.Contains(SpliterCam)) xml = xml.Replace(SpliterCam, "<ProductType></ProductType>");
                    string SpliterDataS = "<ProductData>", SpliterDataE = "</ProductData>", SpliterTypeS = "<ProductType>", SpliterTypeE = "</ProductType>";
                    xml = AddProData(xml, SpliterDataS, ProductData);
                    xml = AddProData(xml, SpliterDataE, ProductData);
                    //父产品增加分类ProductType
                    if (!xml.Contains("Parentage") && ProductData != "Clothing" && ProductData != "Shoes" && ProductData != "Miscellaneous" && ProductData != "Sports" && ProductData != "ToysBaby")
                    {
                        xml = xml.Replace("<ProductType></ProductType>", $"<ProductType><{ProductType}/></ProductType>");
                    }
                    else
                    {
                        if (ProductData == "Sports" || ProductData == "ToysBaby")
                        {
                            xml = xml.Replace(SpliterTypeE, "");
                            xml = xml.Replace(SpliterTypeS, $"{SpliterTypeS}{ProductType}{SpliterTypeE}");
                        }
                        //增加PeoductType具体分类节点数据
                        else if (ProductData != "Clothing" && ProductData != "Shoes" && ProductData != "Miscellaneous")
                        {
                            xml = AddProData(xml, SpliterTypeS, ProductType);
                            xml = AddProData(xml, SpliterTypeE, ProductType);
                        }
                        else
                        {
                            xml = xml.Replace(SpliterTypeS, "").Replace(SpliterTypeE, "");
                            //将VariationTheme/Color或者Size的位置区分
                            string SpliterVariationThemeS = "<VariationTheme>", SpliterVariationThemeE = "</VariationTheme>";
                            Sy.String _xml = xml;
                            if (_xml.CanMid(SpliterVariationThemeS, SpliterVariationThemeE))
                            {
                                string VariationThemeData = _xml.Mid(SpliterVariationThemeS, SpliterVariationThemeE);
                                xml = xml.Replace($"{SpliterVariationThemeS}{VariationThemeData}{SpliterVariationThemeE}", "");
                                xml = xml.Replace("</VariationData>", $"{SpliterVariationThemeS}{VariationThemeData}{SpliterVariationThemeE}</VariationData>");
                            }
                        }
                        string SpliterVariationDataS = "<VariationData>", SpliterVariationDataE = "</VariationData>";
                        string VariationValueS = "</VariationTheme>", VariationValueE = "</VariationData>";
                        if (ProductData == "HomeImprovement" || ProductData == "Wireless" || ProductData == "Lighting")
                        {
                            if (xml.CanMid(VariationValueS, VariationValueE))
                            {
                                string Value = xml.Mid(VariationValueS, VariationValueE);
                                if (!string.IsNullOrEmpty(Value.Trim()))
                                {
                                    xml = xml.Replace(Value, "");
                                    //将Theme值放置于ProductData处
                                    if (ProductData == "Jewelry")
                                    {
                                        int index = xml.IndexOf($"</{ProductData}>");
                                        xml = xml.Insert(index, Value);
                                    }
                                    //将Theme值放置于ProductType处
                                    else if (ProductData == "HomeImprovement" || ProductData == "Wireless" || ProductData == "Lighting")
                                    {
                                        int index = xml.IndexOf($"</{ProductType}>");
                                        xml = xml.Insert(index, Value);
                                    }
                                    if (ProductType == "WirelessLockedPhone")
                                    {
                                        xml = xml.Replace("<Color>", "<ColorName>").Replace("</Color>", "</ColorName>").Replace("<Size>", "<SizeName>").Replace("</Size>", "</SizeName>");
                                    }
                                    if (ProductType == "WirelessAccessories")//
                                    {
                                        //替换Theme和CompatiblePhoneModels的位置（Color在上）
                                        string SpliterCompS = "<CompatiblePhoneModels>", SpliterCompE = "</CompatiblePhoneModels>";
                                        if (xml.ContainsAll(SpliterCompS, SpliterCompE))
                                        {
                                            string CompValue = xml.Mid(SpliterCompS, SpliterCompE);
                                            xml = xml.Replace($"{SpliterCompS}{CompValue}{SpliterCompE}", "");
                                            int CompIndex = xml.IndexOf($"</{ProductType}>");
                                            xml = xml.Insert(CompIndex, $"{SpliterCompS}{CompValue}{SpliterCompE}");
                                        }
                                    }
                                }
                            }
                        }
                        if (ProductData == "CameraPhoto")
                        {
                            xml = xml.Replace($"<{ProductType}>", $"<{ProductType}/>").Replace($"</{ProductType}>", "");
                        }
                        if (ProductData == "Sports" || ProductData == "ToysBaby")
                        {
                            //Size Color 换位置
                            string SpliterColorS = "<Color>", SpliterColorE = "</Color>", SpliterSizeS = "<Size>";
                            if (ProductData == "Sports" && xml.ContainsAll(SpliterColorS, SpliterSizeS))
                            {
                                string ColorValue = xml.Mid(SpliterColorS, SpliterColorE);
                                //原先位置的color删除
                                xml = xml.Replace($"{SpliterColorS}{ColorValue}{SpliterColorE}", "");
                                int Sizeindex = xml.IndexOf(SpliterSizeS);
                                //Size前面加上该值
                                xml = xml.Insert(Sizeindex, $"{SpliterColorS}{ColorValue}{SpliterColorE}");
                            }
                        }
                        if (ProductData == "Shoes")
                        {
                            string SpliterClothingTypeS = "<ClothingType>", SpliterClothingTypeE = "</ClothingType>";
                            if (xml.CanMid(SpliterClothingTypeS, SpliterClothingTypeE))
                            {
                                string Temp = $"{SpliterClothingTypeS}{xml.Mid(SpliterClothingTypeS, SpliterClothingTypeE)}{SpliterClothingTypeE}";
                                xml = xml.Replace(Temp, "");
                                string SpliterVariationData = "<VariationData>";
                                if (xml.Contains(SpliterVariationData))
                                {
                                    int index = xml.IndexOf(SpliterVariationData);
                                    xml = xml.Insert(index, Temp);
                                }
                            }
                            string SpliterClassificationDataS = "<ClassificationData>",SpliterColorMapS= "<ColorMap>", SpliterColorMapE= "</ColorMap>", SpliterSizeMapS = "<SizeMap>", SpliterSizeMapE = "</SizeMap>";
                            int IndexClassificationData = xml.IndexOf(SpliterClassificationDataS) + SpliterClassificationDataS.Length;
                            if (xml.CanMid(SpliterSizeMapS, SpliterSizeMapE))
                            {
                                string Temp = $"{SpliterSizeMapS}{xml.Mid(SpliterSizeMapS, SpliterSizeMapE)}{SpliterSizeMapE}";
                                xml = xml.Replace(Temp, "");
                                xml = xml.Insert(IndexClassificationData, Temp);
                            }
                            if (xml.CanMid(SpliterColorMapS, SpliterColorMapE))
                            {
                                string Temp = $"{SpliterColorMapS}{xml.Mid(SpliterColorMapS, SpliterColorMapE)}{SpliterColorMapE}";
                                xml = xml.Replace(Temp, "");
                                xml = xml.Insert(IndexClassificationData, Temp);
                            }
                        }
                        if (ProductData == "Miscellaneous")
                        {
                            if (xml.CanMid(SpliterVariationDataS, SpliterVariationDataE))
                            {
                                string VariantData = $"{SpliterVariationDataS}{xml.Mid(SpliterVariationDataS, SpliterVariationDataE)}{SpliterVariationDataE}";
                                xml = xml.Replace(VariantData, "");
                            }
                            string SpliterMiscellaneous = "<Miscellaneous>";
                            int Index = xml.IndexOf(SpliterMiscellaneous) + SpliterMiscellaneous.Length ;
                            xml = xml.Insert(Index, $"<ProductType>{ProductType}</ProductType>");
                        }
                        if (ProductData == "Toys")
                        {
                            if (xml.CanMid(SpliterVariationDataS, SpliterVariationDataE))
                            {
                                string VariantData = $"{SpliterVariationDataS}{xml.Mid(SpliterVariationDataS, SpliterVariationDataE)}{SpliterVariationDataE}";
                                xml = xml.Replace(VariantData, "");
                                string SpliterProductType = $"<ProductType>";
                                int Index = xml.IndexOf(SpliterProductType);
                                xml = xml.Insert(Index, VariantData);
                                string SpliterColorS = "<Color>", SpliterColorE = "</Color>", SpliterSizeS = "<Size>", SpliterSizeE = "</Size>";
                                string ProductTypeS = "<ProductType>", ProductTypeE = "</ProductType>";
                                //Size放在ProductType后面 Color放在ProductType中间
                                if (xml.CanMid(SpliterSizeS, SpliterSizeE))
                                {
                                    string SizeValue = $"{SpliterSizeS}{xml.Mid(SpliterSizeS, SpliterSizeE)}{SpliterSizeE}";
                                    xml = xml.Replace(SizeValue, "");
                                    int SizeIndex = xml.IndexOf($"</{ProductData}>");
                                    if (SizeIndex > 0) xml = xml.Insert(SizeIndex, SizeValue);
                                }
                                if (xml.CanMid(SpliterColorS, SpliterColorE))
                                {
                                    string ColorValue = $"{SpliterColorS}{xml.Mid(SpliterColorS, SpliterColorE)}{SpliterColorE}";
                                    xml = xml.Replace(ColorValue, "");
                                    int ColorIndex = xml.IndexOf($"</{ProductType}>");
                                    if (ColorIndex > 0) xml = xml.Insert(ColorIndex, ColorValue);
                                }
                                else
                                {
                                    if (xml.CanMid(ProductTypeS, ProductTypeE))
                                    {
                                        string ProductTypeValue = $"{ProductTypeS}{xml.Mid(ProductTypeS, ProductTypeE)}{ProductTypeE}";
                                        xml = xml.Replace(ProductTypeValue, $"{ProductTypeS}<{ProductType}/>{ProductTypeE}");
                                    }
                                }
                            }
                        }
                    }
                    XmlLst[i] = xml;
                }
                FinalXml = "";
                for (int i = 0; i <= XmlLst.Length - 1; i++)
                {
                    if (i == XmlLst.Length - 1)
                        FinalXml += XmlLst[i];
                    else
                        FinalXml += $"{XmlLst[i]}{SpliterMessage}";
                }
            }
            return FinalXml;
        }
        #endregion
        #region 多产品上传逻辑
        //Dictionary<string, List<string>> DicSku ;
        //public INF_Mws.Result UploadMuti(UploadMutiParameter Up)
        //{
        //    DicSku = new Dictionary<string, List<string>>();
        //    //类型参数
        //    string ProductData = Up.ProductType.Parent.Name;
        //    string ProductType = Up.ProductType.Name;
        //    //店铺参数
        //    var shop = Up.Shop;
        //    Imp_ShopInfo AmazonShop = new Imp_ShopInfo(shop.MarketPlace, shop.SellerID, shop.MwsAuthToken, shop.AwsAccessKey, shop.SecretKey);
        //    //先检查变体对象中是否存在相同的值
        //    foreach (var SinglePro in Up.Products)
        //    {
        //        if (!CheckAttrValue(SinglePro))
        //        {
        //            Up.Log($"上传失败,失败原因:该商品的变体中存在相同值,请修改后重新上传！");
        //            return INF_Mws.Result.Fail;
        //        }
        //    }
        //    //1.1 执行上传主商品
        //    AmazonEnvelope pro = GetMutiProInfo(Up, ProductData, ProductType);
        //    if (pro == null) { return INF_Mws.Result.Fail; }
        //    //ProductInfo(主商品)
        //    string xml = GetXml(pro, ProductData, ProductType);
        //    INF_Mws.Result UploadProInfo = MWS.Upload(xml, "_POST_PRODUCT_DATA_", AmazonShop, out string ProFeedSubmissionId, Up.Log);
        //    if (UploadProInfo.Equals(INF_Mws.Result.Fail))return INF_Mws.Result.Fail;
        //    Up.Log($"上传主商品：Successed;FeedSubmissionId:{ProFeedSubmissionId}");
        //    //1.2执行上传子产品
        //    AmazonEnvelope ChildPro = GetMutiChildInfo(Up, ProductData, ProductType);
        //    if (ChildPro == null) { }//无变体
        //    //{ return INF_Mws.Result.Fail; }
        //    string Childxml = GetXml(ChildPro, ProductData, ProductType);//(子商品)
        //    INF_Mws.Result UploadChildProInfo = MWS.Upload(Childxml, "_POST_PRODUCT_DATA_", AmazonShop, out string ChildProFeedSubmissionId, Up.Log);
        //    if (UploadChildProInfo.Equals(INF_Mws.Result.Fail)) return INF_Mws.Result.Fail;
        //    Up.Log($"上传子商品：Successed;FeedSubmissionId:{ChildProFeedSubmissionId}");
        //    //2.执行上传库存
        //    string ProInventoryXml = GetOtherXml(GetMutiProInventory(Up));
        //    INF_Mws.Result UploadProInventory = MWS.Upload(ProInventoryXml, "_POST_INVENTORY_AVAILABILITY_DATA_", AmazonShop, out string InventoryFeedSubmissionId, Up.Log);
        //    if (UploadProInventory.Equals(INF_Mws.Result.Fail)) return INF_Mws.Result.InventoryFail;
        //    Up.Log($"上传库存：Successed;FeedSubmissionId:{InventoryFeedSubmissionId}");
        //    //3.执行上传价格
        //    string ProPriceXml = GetOtherXml(GetMutiProPrice(Up));
        //    INF_Mws.Result UploadProPrice = MWS.Upload(ProPriceXml, "_POST_PRODUCT_PRICING_DATA_", AmazonShop, out string PriceFeedSubmissionId, Up.Log);
        //    if (UploadProPrice.Equals(INF_Mws.Result.Fail)) return INF_Mws.Result.PriceFail;
        //    Up.Log($"上传价格：Successed;FeedSubmissionId:{PriceFeedSubmissionId}");
        //    //4.执行上传图片
        //    AmazonEnvelope Pic_Obj = GetMutiProPic(Up);
        //    if (Pic_Obj.IsNull()) return INF_Mws.Result.PicFail;
        //    string ProPicXml = GetOtherXml(Pic_Obj);//==>图片是否为空
        //    INF_Mws.Result UploadPic = MWS.Upload(ProPicXml, "_POST_PRODUCT_IMAGE_DATA_", AmazonShop, out string PicFeedSubmissionId, Up.Log);
        //    if (UploadPic.Equals(INF_Mws.Result.Fail)) return INF_Mws.Result.PicFail;
        //    Up.Log($"上传图片：Successed;FeedSubmissionId:{PicFeedSubmissionId}");
        //    //5.执行上传变体
        //    AmazonEnvelope amazonRela = GetMutiProRelationShip(Up, DicSku);
        //    if (amazonRela == null) return INF_Mws.Result.Success;
        //    string ProRelaXML = GetOtherXml(amazonRela);
        //    INF_Mws.Result UploadRelation = MWS.Upload(ProRelaXML, "_POST_PRODUCT_RELATIONSHIP_DATA_", AmazonShop, out string RelaFeedSubmissionId, Up.Log);
        //    if (UploadRelation.Equals(INF_Mws.Result.Fail)) return INF_Mws.Result.RelaFail;
        //    Up.Log($"上传变体：Successed;FeedSubmissionId:{RelaFeedSubmissionId}");
        //    return INF_Mws.Result.Success;
        //}
        //private AmazonEnvelope GetMutiProInfo(UploadMutiParameter Up, string ProductData, string ProductType)
        //{
        //    AmazonEnvelope pro = new AmazonEnvelope
        //    {
        //        Header = new AmazonEnvelope.Cls_Header()
        //    };
        //    pro.Header.DocumentVersion = "1.01";
        //    pro.Header.MerchantIdentifier = Up.Shop.SellerID;
        //    pro.MessageType = "Product";
        //    pro.PurgeAndReplace = "false";
        //    pro.Message = new List<Message>();
        //    int i = 1;
        //    foreach (var SinglePro in Up.Products)
        //    {
        //        UploadParameter parameter = new UploadParameter(Up.Shop, SinglePro, Up.Category, Up.ProductType, Up.ExtTypeValue, Up.Brand, Up.Manufacturer, Up.Log);
        //        //1.上传主商品
        //        if (SinglePro.Products.Count == 0)// 若Count = 0 单变体
        //        {
        //            Message message = UploadPro(parameter, i, ProductData);//Message为基础信息
        //            if (message == null) return null;
        //            message.Product.ProductData = GetProData(parameter, "base-product", ProductData, ProductType);
        //            pro.Message.Add(message);
        //        }
        //        else//若Count > 0  包含多个变体的产品
        //        {
        //            Message message = UploadPro(parameter, i, ProductData, "Parent");
        //            if (message == null) return null;
        //            message.Product.ProductData = GetProData(parameter, "parent", ProductData, ProductType);
        //            pro.Message.Add(message);
        //        }
        //        i++;
        //    }
        //    return pro;
        //}
        //private AmazonEnvelope GetMutiChildInfo(UploadMutiParameter Up, string ProductData, string ProductType)
        //{
        //    AmazonEnvelope pro = new AmazonEnvelope
        //    {
        //        Header = new AmazonEnvelope.Cls_Header
        //        {
        //            DocumentVersion = "1.01",
        //            MerchantIdentifier = Up.Shop.SellerID
        //        },
        //        MessageType = "Product",
        //        PurgeAndReplace = "false",
        //        Message = new List<Message>()
        //    };
        //    int i = 1;
        //    foreach (var SinglePro in Up.Products)
        //    {
        //        UploadParameter parameter = new UploadParameter(Up.Shop, SinglePro, Up.Category, Up.ProductType, Up.ExtTypeValue, Up.Brand, Up.Manufacturer, Up.Log);
        //        //2.上传子商品
        //        string ParentSku = SinglePro.Skucode;
        //        List<string> ChildSku = new List<string>();
        //        if (SinglePro.Products != null && SinglePro.Products.Count > 0)
        //        {
        //            foreach (var AttrPro in SinglePro.Products)
        //            {
        //                Message SonMessage = UploadPro(parameter, i, ProductData, "Child", AttrPro);
        //                if (SonMessage == null) { return null; }
        //                SonMessage.Product.ProductData = GetProData(parameter, "child", ProductData, ProductType, AttrPro);
        //                pro.Message.Add(SonMessage);
        //                ChildSku.Add(AttrPro.Skucode);
        //                i++;
        //            }
        //        }
        //        DicSku.Add(ParentSku, ChildSku);
        //    }
        //    if (pro.Message.Count == 0) return null;
        //    return pro;
        //}
        //private AmazonEnvelope GetMutiProInventory(UploadMutiParameter Up)
        //{
        //    //商品的Sku在上传时获取
        //    AmazonEnvelope ProInventory = new AmazonEnvelope();
        //    ProInventory.Header = new AmazonEnvelope.Cls_Header();
        //    ProInventory.Header.DocumentVersion = "1.01";
        //    ProInventory.Header.MerchantIdentifier = Up.Shop.SellerID;
        //    ProInventory.MessageType = "Inventory";
        //    ProInventory.Message = new List<Message>();
        //    int i = 1;
        //    foreach (var SinglePro in Up.Products)
        //    {
        //        UploadParameter parameter = new UploadParameter(Up.Shop, SinglePro, Up.Category, Up.ProductType, Up.ExtTypeValue, Up.Brand, Up.Manufacturer, Up.Log);
        //        if (SinglePro.Products != null && SinglePro.Products.Count > 0)
        //        {
        //            foreach (var AttrPro in SinglePro.Products)
        //            {
        //                Message ChildMessage = GetInventoryMessage(parameter, i, AttrPro);
        //                ProInventory.Message.Add(ChildMessage);
        //                i++;
        //            }
        //        }
        //        else
        //        {
        //            Message message = GetInventoryMessage(parameter, i);
        //            ProInventory.Message.Add(message);
        //        }
        //        i++;
        //    }
        //    return ProInventory;
        //}
        //private AmazonEnvelope GetMutiProPrice(UploadMutiParameter Up)
        //{
        //    //商品的Sku在上传时获取
        //    AmazonEnvelope ProPrice = new AmazonEnvelope();
        //    ProPrice.Header = new AmazonEnvelope.Cls_Header();
        //    ProPrice.Header.DocumentVersion = "1.01";
        //    ProPrice.Header.MerchantIdentifier = Up.Shop.SellerID;
        //    ProPrice.MessageType = "Price";
        //    ProPrice.Message = new List<Message>();
        //    int i = 1;
        //    foreach (var SinglePro in Up.Products)
        //    {
        //        UploadParameter parameter = new UploadParameter(Up.Shop, SinglePro, Up.Category, Up.ProductType, Up.ExtTypeValue, Up.Brand, Up.Manufacturer, Up.Log);
        //        if (SinglePro.Products != null && SinglePro.Products.Count > 0)
        //        {
        //            foreach (var AttrPro in SinglePro.Products)
        //            {
        //                Message message = GetPriceMessage(parameter, i, AttrPro);
        //                ProPrice.Message.Add(message);
        //                i++;
        //            }
        //        }
        //        else
        //        {
        //            Message Pmessage = GetPriceMessage(parameter, i);
        //            ProPrice.Message.Add(Pmessage);
        //        }
        //    }
        //    return ProPrice;
        //}
        //private AmazonEnvelope GetMutiProPic(UploadMutiParameter Up)
        //{
        //    //商品SKU在上传时获取
        //    AmazonEnvelope proPic = new AmazonEnvelope();
        //    proPic.Header = new AmazonEnvelope.Cls_Header();
        //    proPic.Header.DocumentVersion = "1.01";
        //    proPic.Header.MerchantIdentifier = Up.Shop.SellerID;
        //    proPic.MessageType = "ProductImage";
        //    proPic.Message = new List<Message>();
        //    int num = 1;
        //    foreach (var SinglePro in Up.Products)
        //    {
        //        List<string> ParentPath = SinglePro.Images?.ConvertAll((img) => img.URL).Take(10).ToList();
        //        if (!(ParentPath?.Count > 0)) return null;
        //        UploadParameter parameter = new UploadParameter(Up.Shop, SinglePro, Up.Category, Up.ProductType, Up.ExtTypeValue, Up.Brand, Up.Manufacturer, Up.Log);
        //        //if(ParentPath==null || ParentPath.Count=0) return null;
        //        //有子商品时只上传子商品的图片
        //        if (SinglePro.Products != null && SinglePro.Products.Count > 0)
        //        {
        //            foreach (var AttrPro in SinglePro.Products)
        //            {
        //                List<string> ChildPath = AttrPro.Images?.ConvertAll((img) => img.URL).Take(10).ToList();
        //                if (!(ChildPath?.Count > 0)) return null;
        //                int Childi = 0;
        //                foreach (string img in ChildPath)
        //                {
        //                    Message message = GetPicMessage(parameter, num, Childi, img, AttrPro);
        //                    proPic.Message.Add(message);
        //                    num++; Childi++;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            int i = 0;
        //            ParentPath?.All((img) =>
        //            {
        //                Message message = GetPicMessage(parameter, num, i, img);
        //                proPic.Message.Add(message);
        //                num++; i++;
        //                return true;
        //            });
        //        }
        //    }
        //    return proPic;
        //}
        //private AmazonEnvelope GetMutiProRelationShip(UploadMutiParameter Up, Dictionary<string,List<string>> DicSku)
        //{
        //    AmazonEnvelope proRelationShip = new AmazonEnvelope();
        //    proRelationShip.Header = new AmazonEnvelope.Cls_Header();
        //    proRelationShip.Header.DocumentVersion = "1.01";
        //    proRelationShip.Header.MerchantIdentifier = Up.Shop.SellerID;
        //    proRelationShip.MessageType = "Relationship";
        //    proRelationShip.Message = new List<Message>();
        //    int MessageID = 1;
        //    foreach (var PCSku in DicSku)
        //    {
        //        string PSku = PCSku.Key;
        //        List<string> CSku = PCSku.Value;
        //        if (CSku != null && CSku.Count > 0)
        //        {
        //            Message message = new Message();
        //            message.MessageID = MessageID.ToString();
        //            message.OperationType = "Update";
        //            message.Relationship = new Message.Cls_Relationship();
        //            message.Relationship.ParentSKU = PSku;
        //            message.Relationship.Relation = new List<Relation>();
        //            foreach (string ChildSku in CSku)
        //            {
        //                Relation relation = new Relation();
        //                relation.SKU = ChildSku;
        //                relation.Type = "Variation";
        //                message.Relationship.Relation.Add(relation);
        //            }
        //            proRelationShip.Message.Add(message);
        //            MessageID++;
        //        }
        //    }
        //    if (proRelationShip.Message.Count == 0) return null;
        //    return proRelationShip;
        //}
        #endregion
        #region 单个产品上传逻辑
        private string ParentSku ;
        private List<string> ChildrenSku ;
        public INF_Mws.Result Upload(UploadParameter Up)
        {
            #region
            ////1.执行上传商品
            //string _xml = GetXml(GetProInfo(Up), _ProductData, _ProductType);//ProductInfo
            ////2.执行上传库存
            //string _ProInventoryXml = GetOtherXml(GetProInventory(Up));
            ////3.执行上传价格
            //string _ProPriceXml = GetOtherXml(GetProPrice(Up));
            ////4.执行上传图片
            //string _ProPicXml = GetOtherXml(GetProPic(Up));
            ////5.执行上传变体
            //string _ProRelaXML = GetOtherXml(GetProRelationShip(ParentSku, ChildrenSku));
            //return INF_TestMws.Result.Success;
            #endregion
            ParentSku = "";
            ChildrenSku = new List<string>();
            string ProductData = Up.ProductType.Parent.Name;
            string ProductType = Up.ProductType.Name;
            string ErrMessage = "";
            //先检查变体对象中是否存在相同的值
            if (!CheckAttrValue(Up.Product)) 
            {
                Up.Log($"上传失败,失败原因:该商品的变体中存在相同值,请修改后重新上传！");
               return  INF_Mws.Result.Fail;
            }
            //1.执行上传主商品
            AmazonEnvelope pro = GetProInfo(Up, ProductData, ProductType);
            if (pro == null) { return INF_Mws.Result.Fail; }
            //Report
            Report report = new Report
            {
                Title = Up.Product.Name,
                Price = Up.Product.Price.ToString(),
                ParentSku = ParentSku
            };
            var shop = Up.Shop;
            Imp_ShopInfo AmazonShop= new Imp_ShopInfo(shop.MarketPlace, shop.SellerID, shop.MwsAuthToken, shop.AwsAccessKey, shop.SecretKey );
            report.shop = AmazonShop;
            report.Time = DateTime.Now;
            string xml = GetXml(pro, ProductData, ProductType);//ProductInfo(主商品)
            INF_Mws.Result UploadProInfo = MWS.Upload(xml, "_POST_PRODUCT_DATA_", AmazonShop, out string ProFeedSubmissionId, ref ErrMessage, Up.Log);
            if (UploadProInfo.Equals(INF_Mws.Result.Fail))
            {
                report.Parent = new Session(ProFeedSubmissionId, 0);
                report.result = INF_Mws.Result.UploadFail_ParentProduct;
                Up.Report(JsonObject.Serialize(report));
                Up.Log($"{report.result.GetDescription()}");
                return report.result;
            }
            Up.Log($"上传主商品：Successed;FeedSubmissionId:{ProFeedSubmissionId}");
            if (Up.Product.Products.Count > 0)
            {
                report.Parent = new Session(ProFeedSubmissionId,  30);// "Upload Product"
                report.result = INF_Mws.Result.ParentProduct_IN_PROGRESS_;//给定初值，从主产品上传中开始
                AmazonEnvelope ChildPro = GetChildInfo(Up, ProductData, ProductType);
                if (ChildPro == null)
                {
                    report.Childs = new Session("",  0);//""
                    report.result = INF_Mws.Result.UploadFail_ChildProduct;
                    Up.Report(JsonObject.Serialize(report));
                    Up.Log($"{report.result.GetDescription()}");
                    return report.result;
                }
                string Childxml = GetXml(ChildPro, ProductData, ProductType);//(子商品)
                INF_Mws.Result UploadChildProInfo = MWS.Upload(Childxml, "_POST_PRODUCT_DATA_", AmazonShop, out string ChildProFeedSubmissionId, ref ErrMessage, Up.Log);
                //ChildrenSku
                if (ChildrenSku != null && ChildrenSku.Count > 0) { report.ChildsSku = ChildrenSku; }
                if (UploadChildProInfo.Equals(INF_Mws.Result.Fail))
                {
                    report.Childs = new Session( ChildProFeedSubmissionId,  0 );//"Upload Child Product"
                    report.result = INF_Mws.Result.UploadFail_ChildProduct;
                    Up.Report(JsonObject.Serialize(report));
                    Up.Log($"{report.result.GetDescription()}");
                    return report.result;
                }
                report.Childs = new Session( ChildProFeedSubmissionId,30 );//"Upload Child Product"
                Up.Log($"上传子商品：Successed;FeedSubmissionId:{ChildProFeedSubmissionId}");
            }
            else
            {
                report.Parent = new Session( ProFeedSubmissionId,  60 );//"Upload Product"
                report.result = INF_Mws.Result.ParentProduct_IN_PROGRESS_;
            }
            //2.执行上传库存
            string ProInventoryXml = GetOtherXml(GetProInventory(Up));
            INF_Mws.Result UploadProInventory = MWS.Upload(ProInventoryXml, "_POST_INVENTORY_AVAILABILITY_DATA_", AmazonShop, out string InventoryFeedSubmissionId, ref ErrMessage, Up.Log);
            if (UploadProInventory.Equals(INF_Mws.Result.Fail))
            {
                report.Inventory = new Session( InventoryFeedSubmissionId,  0 );//"Upload Inventory"
                report.result = INF_Mws.Result.InventoryFail;
                Up.Report(JsonObject.Serialize(report));
                Up.Log("上传库存：Failed");
                return INF_Mws.Result.InventoryFail;
            }
            report.Inventory= new Session( InventoryFeedSubmissionId,  10);// "Upload Inventory"
            Up.Log($"上传库存：Successed;FeedSubmissionId:{InventoryFeedSubmissionId}");
            //3.执行上传价格
            string ProPriceXml = GetOtherXml(GetProPrice(Up));
            INF_Mws.Result UploadProPrice = MWS.Upload(ProPriceXml, "_POST_PRODUCT_PRICING_DATA_", AmazonShop, out string PriceFeedSubmissionId, ref ErrMessage, Up.Log);
            if (UploadProPrice.Equals(INF_Mws.Result.Fail))
            {
                report.UpPrice= new Session( PriceFeedSubmissionId, 0);// "Upload Price"
                report.result = INF_Mws.Result.PriceFail;
                Up.Report(JsonObject.Serialize(report));
                Up.Log("上传价格：Failed");
                return INF_Mws.Result.PriceFail;
            }
            report.UpPrice= new Session( PriceFeedSubmissionId,10);// "Upload Price"
            Up.Log($"上传价格：Successed;FeedSubmissionId:{PriceFeedSubmissionId}");
            //4.执行上传图片
            AmazonEnvelope Pic_Obj = GetProPic(Up);
            if (Pic_Obj.IsNull())
            {
                report.Pics = new Session("", 0);//"Upload Picture"
                report.result = INF_Mws.Result.PicFail;
                Up.Report(JsonObject.Serialize(report));
                Up.Log("上传图片：Failed,失败原因:存在有商品或变体图片不存在，请完善后重新上传！");
                return INF_Mws.Result.PicFail;
            }
            string ProPicXml = GetOtherXml(Pic_Obj);//==>图片是否为空
            INF_Mws.Result UploadPic = MWS.Upload(ProPicXml, "_POST_PRODUCT_IMAGE_DATA_", AmazonShop, out string PicFeedSubmissionId, ref ErrMessage, Up.Log);
            if (UploadPic.Equals(INF_Mws.Result.Fail))
            {
                report.Pics = new Session( PicFeedSubmissionId,0);//"Upload Picture"
                report.result = INF_Mws.Result.PicFail;
                Up.Report(JsonObject.Serialize(report));
                Up.Log("上传图片：Failed");
                return INF_Mws.Result.PicFail;
            }
            AmazonEnvelope amazonRela = GetProRelationShip(Up, ParentSku, ChildrenSku);
            if (amazonRela == null)
            {
                report.Pics= new Session(PicFeedSubmissionId, 20);//"Upload Picture"
                Up.Log($"上传图片：Successed;FeedSubmissionId:{PicFeedSubmissionId}");
                Up.Report(JsonObject.Serialize(report));
                return INF_Mws.Result.Success;
            }//该商品没有变体
            report.Pics = new Session( PicFeedSubmissionId, 10 );//"Upload Picture"
            Up.Log($"上传图片：Successed;FeedSubmissionId:{PicFeedSubmissionId}");
            //5.执行上传变体
            string ProRelaXML = GetOtherXml(amazonRela);
            INF_Mws.Result UploadRelation = MWS.Upload(ProRelaXML, "_POST_PRODUCT_RELATIONSHIP_DATA_", AmazonShop, out string RelaFeedSubmissionId, ref ErrMessage, Up.Log);
            if (UploadRelation.Equals(INF_Mws.Result.Fail))
            {
                report.Relation= new Session(RelaFeedSubmissionId, 0);// "Upload Variants"
                report.result = INF_Mws.Result.RelaFail;
                Up.Report(JsonObject.Serialize(report));
                Up.Log("上传变体：Failed");
                return INF_Mws.Result.RelaFail;
            }
            report.Relation=new Session(RelaFeedSubmissionId, 10 ); //"Upload Variants"'
            string jsondata = JsonObject.Serialize(report);
            Up.Report(jsondata);
            Up.Log($"上传变体：Successed;FeedSubmissionId:{RelaFeedSubmissionId}");
            return INF_Mws.Result.Success;
        }

        public bool CheckAttrValue(IServerProduct Product)
        {
            List<string> LstAttr = new List<string>();
            Product.Products.ForEach(r => LstAttr.Add(r.Values));
            return LstAttr.Count  == LstAttr.Distinct().ToList().Count;
        }
        //public Session UpdateStatues( string FeedSubmissionId,  int prog)
        //{
        //    Session se = new Session
        //    {
        //        FeedMissionId = FeedSubmissionId,
        //        Progress = prog,
        //    };
        //    return se;
        //    #region
        //    //if (Childs == null)
        //    //{ GetSession(Childs, FeedSubmissionId, _result, prog, Cause); return; }
        //    //if (Inventory == null)
        //    //{ GetSession(Inventory, FeedSubmissionId, _result, prog, Cause); return; }
        //    //if (Price == null)
        //    //{ GetSession(UpPrice, FeedSubmissionId, _result, prog, Cause); return; }
        //    //if (Pics == null)
        //    //{ GetSession(Pics, FeedSubmissionId, _result, prog, Cause); return; }
        //    //if (Relation == null)
        //    //{ GetSession(Relation, FeedSubmissionId, _result, prog, Cause); return; }
        //    #endregion
        //}
        //===========================
        /// <summary>
        /// 获得商品基本信息
        /// </summary>
        /// <param name="P">由其他平台的商品</param>
        /// <param name="SKU">分配的商品标识符</param>
        /// <param name="Type">商品标识码的分类</param>
        /// <param name="TypeValue">商品标识码的值</param>
        /// <returns></returns>
        private AmazonEnvelope GetProInfo(UploadParameter Up,string ProductData,string ProductType)
        {
            AmazonEnvelope pro = new AmazonEnvelope
            {
                Header = new AmazonEnvelope.Cls_Header()
            };
            pro.Header.DocumentVersion = "1.01";
            pro.Header.MerchantIdentifier = Up.Shop.SellerID;
            pro.MessageType = "Product";
            pro.PurgeAndReplace = "false";
            pro.Message = new List<Message>();
            int i = 1;
            //1.上传主商品
            if (Up.Product.Products.Count == 0)// 若Count = 0 则基础信息为父产品
            {
                Message message = UploadPro(Up, i, ProductData);//Message为基础信息
                if (message == null) return null;
                message.Product.ProductData = GetProData(Up, "base-product", ProductData, ProductType);
                pro.Message.Add(message);
                ParentSku = Up.Product.Skucode;
            }
            else//若Count > 0  则取第一个产品为父产品，其余为子产品
            {
                //IVaritantProduct ParentPro = Up.Product.Products[0];//Message为变体[0]信息
                Message message = UploadPro(Up, i, ProductData, "Parent");
                if (message == null) return null;
                message.Product.ProductData = GetProData(Up, "parent", ProductData, ProductType);
                pro.Message.Add(message);
                ParentSku = Up.Product.Skucode;
            }
            return pro;
        }
        private AmazonEnvelope GetChildInfo(UploadParameter Up,string ProductData,string ProductType)
        {
            AmazonEnvelope pro = new AmazonEnvelope
            {
                Header = new AmazonEnvelope.Cls_Header
                {
                    DocumentVersion = "1.01",
                    MerchantIdentifier = Up.Shop.SellerID
                },
                MessageType = "Product",
                PurgeAndReplace = "false",
                Message = new List<Message>()
            };
            int i = 1;
            //2.上传子商品(Up.Product.shuxing)
            if (Up.Product.Products != null && Up.Product.Products.Count > 0)
            {
                foreach (var AttrPro in Up.Product.Products)
                {
                        Message SonMessage = UploadPro(Up, i, ProductData, "Child", AttrPro);
                        if (SonMessage == null) { return null; }
                        SonMessage.Product.ProductData = GetProData(Up, "child", ProductData, ProductType, AttrPro);
                        pro.Message.Add(SonMessage);
                        ChildrenSku.Add(AttrPro.Skucode);
                        i++;
                }
            }
            return pro;
        }
        private Message UploadPro(UploadParameter Up, int i,string ProductData,string Parentag="", IVaritantProduct AttrPro =null)
        {
            //判断是否设置SKU
            if (string.IsNullOrEmpty(Up.Product.Skucode))
            {
                Up.Log("未设置产品SKU码");
                return null;
            }
            Message message = new Message
            {
                MessageID = i.ToString(),
                OperationType = "Update",
                Product = new Message.Cls_Product()
            };
            //区分是否含有变体
            CodeType codeType = CodeType.None;
            string ProduceCode = "", SKUCode = "";
            if (Parentag == "")
            {
                SKUCode = Up.Product.Skucode;
                codeType = Up.Product.ProduceCodeType;
                ProduceCode = Up.Product.ProduceCode;
            }
            else
            {
                if (Parentag == "Parent")
                {
                    SKUCode = Up.Product.Skucode;
                }
                else
                {
                    codeType = AttrPro.ProduceCodeType;
                    ProduceCode = AttrPro.ProduceCode;
                    if (string.IsNullOrEmpty(AttrPro.Skucode))
                    {
                        SKUCode = Up.Product.NewSku;//新生成唯一的sku=>赋值给变体
                        AttrPro.Skucode = SKUCode;
                    }
                    else SKUCode = AttrPro.Skucode;
                }
            }
            message.Product.SKU = SKUCode;
            if (!string.IsNullOrEmpty(ProduceCode))
            {
                message.Product.StandardProductID = new Message.Cls_Product.Cls_StandardProductID();
                switch (codeType)
                {
                    case CodeType.None:
                        Up.Log("未分配UPC或EAN码！"); return null;
                    case CodeType.EAN:
                        message.Product.StandardProductID.Type = "EAN";
                        break;
                    case CodeType.UPC:
                        message.Product.StandardProductID.Type = "UPC";
                        break;
                    default:
                        throw new NotSupportedException();
                }
                message.Product.StandardProductID.Value = ProduceCode;
            }
            //BasicMessage
            message.Product.ProductTaxCode = "A_GEN_TAX";
            if (Up.ProductType.Parent.Name == "Health") message.Product.ItemPackageQuantity = "1";
            message.Product.DescriptionData = new Message.Cls_Product.Cls_DescriptionData();
            //title
            if (Up.Product.Name.Length > 200) { Up.Log("上传失败：商品名称长度超过200！");return null; }
            message.Product.DescriptionData.Title = Up.Product.Name.Replace("<","[").Replace(">","]");//SP.title;
            //Brand
            if (string.IsNullOrEmpty(Up.Brand)){Up.Log("上传失败：Brand为空！");return null;}//检验Brand是否为空
            message.Product.DescriptionData.Brand = Up.Brand;//品牌 必要的参数
            //Description
            if (Up.Product.Description != null)
            {
                if (Up.Product.Description.Length > 2000) { Up.Log("上传失败：商品的描述长度超过2000！"); return null; }
                message.Product.DescriptionData.Description = Up.Product.Description;
            }
            //BulletPoint
            if (!Up.Product.BulletPoint.IsNullOrEmpty())
            {
                if (!Up.Product.BulletPoint.All((bp)=> bp.Length <  500 && bp.Trim().Length > 0)) { Up.Log("上传失败：商品的卖点长度超过500！"); return null; }
                if (Up.Product.BulletPoint.Count > 5) { Up.Log("上传失败：商品的卖点超过5个！"); return null; }
                else
                {
                    message.Product.DescriptionData.BulletPoint = Up.Product.BulletPoint;
                }
            }
            //MaxOrderQuantity
            message.Product.DescriptionData.MaxOrderQuantity = "5";
            //Manufacturer
            if (string.IsNullOrEmpty(Up.Manufacturer)) { Up.Log("Manufacturer为空！"); return null; }//检验Manufacturer是否为空
            message.Product.DescriptionData.Manufacturer = Up.Manufacturer;//生产厂家 必要的参数
            //特殊字段
            if (Parentag != "Parent")
            {
                if (ProductData == "AutoAccessoryMisc" || ProductData == "Jewelry" || ProductData == "Health" || ProductData == "Office" || ProductData == "AutoAccessory" || ProductData == "Beauty")
                    message.Product.DescriptionData.MfrPartNumber = ProduceCode;
            }
            if (ProductData == "ToysBaby" || ProductData == "Toys")
            {
                message.Product.DescriptionData.TargetAudience = "Baby or Adult";
            }
            //SearchTerms
            //The enforced limit for all countries is 250 bytes not counting spaces with the exception of non-clothing or luggage product types in Japan, which are allowed up to 500 bytes not counting spaces.
            if (!Up.Product.SearchTerms.IsNullOrEmpty() )
            {
                if (Up.Product.SearchTerms.Count <= 1)
                {
                    bool CheckSearchTerms = true;
                    Up.Product.SearchTerms.ForEach((r) =>
                    {
                        if (r.Length == 0 || r.Length > 250)//System.Text.Encoding.UTF8.GetBytes(r)
                        {
                            CheckSearchTerms = false;
                            return;
                        }
                    });
                    if (!CheckSearchTerms) 
                    {
                        Up.Log("上传失败：商品存在关键词字段为空或长度大于250！"); 
                        return null; 
                    }
                    message.Product.DescriptionData.SearchTerms = Up.Product.SearchTerms;
                }
                else
                {
                    Up.Log("上传失败：商品的关键词只能存在1个！"); return null;
                }
            }
            //RecommendedBrowseNode该信息仅用于加拿大，欧洲和日本的商家帐户+澳大利亚
            string Area = Up.Shop.MarketPlace.URL;
            if (Area.Contains(".uk")|| Area.Contains(".ca")||Area.Contains(".fr")|| Area.Contains(".de")|| Area.Contains(".it") || Area.Contains(".es") || Area.Contains(".in") || Area.Contains(".jp")|| Area.Contains(".com.au"))
            {
                string NodeID = Up.Category;
                if (NodeID.IsNullOrEmpty())
                {
                    NodeID = "0";
                }
                message.Product.DescriptionData.RecommendedBrowseNode = NodeID;//必要的参数
            }
            //重量
            double Weight = 0;
            Up.Product.Fields.ForEach((o) =>
            {
                if (o.Name == "weight" && o.Data.ToString().IsNotWhiteSpace()) Weight = Convert.ToDouble(o.Data);
            });
            if (Weight == 0) Weight = 1000;//前台传过来的单位为g
            message.Product.DescriptionData.ItemWeight = new Message.Cls_Product.Cls_DescriptionData.WeightDimension();
            message.Product.DescriptionData.ItemWeight.WeightValue = (Weight / 1000).ToString("0.00");
            message.Product.DescriptionData.ItemWeight.unitOfMeasure = "KG";
            message.Product.DescriptionData.IsExpirationDatedProduct = false;
            return message;
        }
        /// <summary>
        /// 将商品序列化为xml，分类由参数传入
        /// </summary>
        /// <param name="pro"></param>
        /// <param name="ProdataData"></param>
        /// <param name="ProductType"></param>
        /// <returns></returns>
        private string GetXml(AmazonEnvelope pro,string ProdataData,string ProductType)
        {
            Sy.String FinalXml= XmlObject.Serialize(pro).Replace("utf-16", "utf-8");
            string SpliterMessage = "<Message>";
            if (FinalXml.Contains(SpliterMessage))
            {
                var XmlLst = FinalXml.Split(SpliterMessage);
                for (int Id = 1; Id <= XmlLst.Length - 1; Id++)
                {
                    Sy.String  xml = XmlLst[Id];
                    string SpliterCam = "<ProductType />";
                    if (xml.Contains(SpliterCam)) xml = xml.Replace(SpliterCam, "<ProductType></ProductType>");
                    string SpliterDataS = "<ProductData>", SpliterDataE = "</ProductData>", SpliterTypeS = "<ProductType>", SpliterTypeE = "</ProductType>";
                    xml = AddProData(xml, SpliterDataS, ProdataData);
                    xml = AddProData(xml, SpliterDataE, ProdataData);
                    if (!xml.Contains("Parentage"))
                    {
                        xml = xml.Replace("<ProductType></ProductType>", $"<ProductType><{ProductType}/></ProductType>");
                    }
                    else
                    {

                        //Clothing分类下没用Type
                        if (ProdataData != "Clothing")
                        {
                            xml = AddProData(xml, SpliterTypeS, ProductType);
                            xml = AddProData(xml, SpliterTypeE, ProductType);
                        }
                        else
                        {
                            xml = xml.Replace(SpliterTypeS, "").Replace(SpliterTypeE, "");
                            //将VariationTheme/Color或者Size的位置区分
                            string SpliterVariationThemeS = "<VariationTheme>", SpliterVariationThemeE = "</VariationTheme>";
                            Sy.String _xml = xml;
                            if (_xml.CanMid(SpliterVariationThemeS, SpliterVariationThemeE))
                            {
                                string VariationThemeData = _xml.Mid(SpliterVariationThemeS, SpliterVariationThemeE);
                                xml = xml.Replace($"{SpliterVariationThemeS}{VariationThemeData}{SpliterVariationThemeE}", "");
                                xml = xml.Replace("</VariationData>", $"{SpliterVariationThemeS}{VariationThemeData}{SpliterVariationThemeE}</VariationData>");
                            }
                        }
                        string SpliterVariationDataS = "<VariationData>", SpliterVariationDataE = "</VariationData>";
                        string VariationValueS = "</VariationTheme>", VariationValueE = "</VariationData>";
                        if (ProdataData == "HomeImprovement" || ProdataData == "Wireless" || ProdataData == "Lighting")
                        {
                            if (xml.CanMid(VariationValueS, VariationValueE))
                            {
                                string Value = xml.Mid(VariationValueS, VariationValueE);
                                if (!string.IsNullOrEmpty(Value.Trim()))
                                {
                                    xml = xml.Replace(Value, "");
                                    if (ProdataData == "HomeImprovement" || ProdataData == "Wireless" || ProdataData == "Lighting")//将Theme值放置于ProductType处
                                    {
                                        int index = xml.IndexOf($"</{ProductType}>");
                                        xml = xml.Insert(index, Value);
                                    }
                                    if (ProductType == "WirelessLockedPhone")
                                    {
                                        xml = xml.Replace("<Color>", "<ColorName>").Replace("</Color>", "</ColorName>").Replace("<Size>", "<SizeName>").Replace("</Size>", "</SizeName>");
                                    }
                                    if (ProductType == "WirelessAccessories")//
                                    {
                                        //替换Theme和CompatiblePhoneModels的位置（Color在上）
                                        string SpliterCompS = "<CompatiblePhoneModels>", SpliterCompE = "</CompatiblePhoneModels>";
                                        if (xml.ContainsAll(SpliterCompS, SpliterCompE))
                                        {
                                            string CompValue = xml.Mid(SpliterCompS, SpliterCompE);
                                            xml = xml.Replace($"{SpliterCompS}{CompValue}{SpliterCompE}", "");
                                            int CompIndex = xml.IndexOf($"</{ProductType}>");
                                            xml = xml.Insert(CompIndex, $"{SpliterCompS}{CompValue}{SpliterCompE}");
                                        }
                                    }
                                }
                            }
                        }
                        if (ProdataData == "CameraPhoto")
                        {
                            xml = xml.Replace($"<{ProductType}>", $"<{ProductType}/>").Replace($"</{ProductType}>", "");
                        }
                        if (ProdataData == "Sports" || ProdataData == "ToysBaby")
                        {
                            //将VariationData中所有数据移到ProductType同级别处
                            //Sy.String Temp = xml;
                            if (xml.ContainsAll(SpliterVariationDataS, SpliterVariationDataE))
                            {
                                Sy.String VariationData = xml.Mid(SpliterVariationDataS, SpliterVariationDataE);
                                xml = xml.Replace(SpliterVariationDataS, "").Replace(SpliterVariationDataE, "").Replace(VariationData, "");//去除原先位置数据
                                                                                                                                           //Color在前 Size在后（是否都包含）处理VariationData值
                                string SpliterColorS = "<Color>", SpliterColorE = "</Color>", SpliterSizeS = "<Size>";
                                if (VariationData.ContainsAll(SpliterColorS, SpliterSizeS))
                                {
                                    string ColorValue = VariationData.Mid(SpliterColorS, SpliterColorE);
                                    VariationData = VariationData.Replace($"{SpliterColorS}{ColorValue}{SpliterColorE}", "");//原先位置的color删除
                                    int Sizeindex = VariationData.IndexOf(SpliterSizeS);
                                    VariationData = VariationData.Insert(Sizeindex, $"{SpliterColorS}{ColorValue}{SpliterColorE}");//Size前面加上该值
                                }
                                VariationData = SpliterVariationDataS + VariationData + SpliterVariationDataE;
                                int index = xml.IndexOf($"</{ProdataData}>");
                                xml = xml.Insert(index, VariationData);//添加现有数据
                                xml = xml.Replace($"<{ProductType}>", "").Replace($"</{ProductType}>", "");//替换ProductType数据（不允许有空格和其他任意字符出现）
                                string ProTypeValue = xml.Mid("<ProductType>", "</ProductType>");
                                xml = xml.Replace($"<ProductType>{ProTypeValue}</ProductType>", $"<ProductType>{ProductType}</ProductType>");
                                //ToysBaby分类
                                if (ProdataData == "ToysBaby")
                                {
                                    //Theme的Values值在外层
                                    string Value = xml.Mid(VariationValueS, VariationValueE);
                                    xml = xml.Replace($"{VariationValueS}{Value}{VariationValueE}", $"{VariationValueS}{VariationValueE}");
                                    int Typeindex = xml.IndexOf($"</{ProdataData}>");
                                    xml = xml.Insert(Typeindex, Value);
                                    //ToysAndGames分类没有VariationData
                                    if (ProductType == "ToysAndGames")
                                    {
                                        //不包含VariationData部分
                                        if (xml.ContainsAll(SpliterVariationDataS, SpliterVariationDataE))
                                        {
                                            Sy.String _VariationData = xml.Mid(SpliterVariationDataS, SpliterVariationDataE);
                                            xml = xml.Replace(SpliterVariationDataS, "").Replace(SpliterVariationDataE, "").Replace(_VariationData, "");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    XmlLst[Id] = xml;
                }
                FinalXml = "";
                for (int i = 0; i <= XmlLst.Length - 1; i++)
                {
                    if (i == XmlLst.Length - 1)
                        FinalXml += XmlLst[i];
                    else
                        FinalXml += $"{XmlLst[i]}{SpliterMessage}";
                }
            }
            return FinalXml;
        }
        private string AddProData(string xml,string ProdataData,string _ProdataData)
        {
            if (xml.Contains(ProdataData))
            {
                string[] xmls = xml.Split(new string[] { ProdataData }, StringSplitOptions.None);
                xml = "";
                for (int i = 0; i < xmls.Length; i++)
                {
                    if (i == xmls.Length - 1) { xml += xmls[i]; }
                    else
                    {
                        if (ProdataData.Contains("/")) { xml += xmls[i] + $"</{_ProdataData}>" + ProdataData ; }
                        else { xml += xmls[i] + ProdataData + $"<{_ProdataData}>"; }
                    }
                }
            }
            return xml;
        }
        /// <summary>
        /// 获取商品分类信息
        /// </summary>
        /// <param name="P"></param>
        /// <param name="Parentage">根据是否有变体来设置值</param>
        /// <returns></returns>
        private Message.Cls_Product.Cls_ProductData GetProData( UploadParameter Up, string ParentTag,string ProductData,string ProductType,IVaritantProduct AttrPro = null)
        {
            Message.Cls_Product.Cls_ProductData pd = new Message.Cls_Product.Cls_ProductData();
            pd.ProductType = new Message.Cls_Product.Cls_ProductData.Cls_ProductType();
            if (ProductData == "ToysBaby" && ProductType == "ToysAndGames" || ProductData == "Toys")//该分类不存在有变体的情况
            {
                pd.AgeRecommendation = new Message.Cls_Product.Cls_AgeRecommendation();
                pd.AgeRecommendation.MinimumManufacturerAgeRecommended = new Message.Cls_Product.Cls_AgeRecommendation.AgeDimension();
                pd.AgeRecommendation.MinimumManufacturerAgeRecommended.unitOfMeasure = "months";
                pd.AgeRecommendation.MinimumManufacturerAgeRecommended.AgeValue = "1";
            }
            if (ParentTag == "base-product") return pd;
            //CameraPhoto与其它xml类型不一致（不存在VariationData）
            if (ProductData == "CameraPhoto")
            {
                pd.VariationTheme = "CustomerPackageType";
                pd.Parentage = ParentTag;//在parentdata下，应在parenttype下
                if (ParentTag == "child")
                {
                    string Value = AttrPro.Values;
                    if (string.IsNullOrEmpty(Value)) Value = "As the picture shows";
                    pd.CustomerPackageType = Value;
                }
                return pd;
            }
            pd.ProductType.VariationData = new Message.Cls_Product.Cls_ProductData.Cls_VariationData();
            ////ParentTag在XML中的位置(有变体的Parentage设置)
            if (Up.Product.Products != null && Up.Product.Products.Count > 0 && ProductData != "Home") pd.ProductType.VariationData.Parentage = ParentTag;
            else if (Up.Product.Products != null && Up.Product.Products.Count > 0 && ProductData == "Home") pd.Parentage = ParentTag;
            Sy.String AllAttributeText = Up.Product.AllAttributeText.ToLower();
            //if (!string.IsNullOrEmpty(AllAttributeText))
            //{
            //直接设置Ligthting分类
            if (ProductData == "Lighting" && !string.IsNullOrEmpty(AllAttributeText))
            {
                if (ProductType == "LightsAndFixtures")
                {
                    pd.ProductType.VariationData.VariationTheme = "Color";
                    if (ParentTag == "child")
                    {
                        pd.ProductType.VariationData.Color = AttrPro.Values;
                        pd.ProductType.VariationData.ColorMap = AttrPro.Values;
                    }
                }
                else if (ProductType == "LightBulbs") pd.ProductType.VariationData.VariationTheme = "Wattage";
                else if (ProductType == "LightingAccessories") pd.ProductType.VariationData = null;
                //if (ParentTag == "child") pd.ProductType.VariationData.Wattage = 1.ToString();//所有小类都有的属性
                return pd;
            }
            //直接设置Ligthting分类
            if (ProductData == "Office" || ProductData == "AutoAccessory")
            {
                pd.ProductType.VariationData.VariationTheme = "Color";
                if (ParentTag == "child")
                {
                    pd.ProductType.ColorSpecification = new Message.Cls_Product.Cls_ProductData.Cls_ProductType.Cls_ColorSpecification();
                    string ColorValue = AttrPro.Values;
                    if (string.IsNullOrEmpty(ColorValue)) ColorValue = "As the picture shows";
                    pd.ProductType.ColorSpecification.Color = ColorValue;
                    pd.ProductType.ColorSpecification.ColorMap = ColorValue;
                    //if (ProductData == "AutoAccessory")
                    //{
                    //}
                }
                return pd;
            }
            //电子产品
            if (ProductData == "CE")
            {
                if (ProductType != "ConsumerElectronics" || ProductType != "NetworkAdapter" || ProductType != "KindleAccessories" || ProductType != "KindleEReaderAccessories" || ProductType != "KindleFireAccessories" || ProductType != "CellularPhoneCase" || ProductType != "ScreenProtector" || ProductType != "ChargingAdapter" || ProductType != "PowerBank" || ProductType != "CellularPhone")
                {
                    pd.ProductType.VariationData = null;
                    if (ProductType == "PC")
                    {
                        pd.ProductType.HardDriveSizeize = new Message.Cls_Product.Cls_ProductData.Cls_ProductType.Cls_HardDriveSize()
                        {
                            HardDriveSizeValue = "1",
                            unitOfMeasure = "TB"
                        };
                        pd.ProductType.HardDriveInterface = "usb_3.0";
                        pd.ProductType.ComputerMemoryType = "ddr4_sdram";
                        pd.ProductType.RAMSize = new Message.Cls_Product.Cls_ProductData.Cls_ProductType.Cls_RAMSize()
                        {
                            RAMSizeValue = "4",
                            unitOfMeasure = "GB"
                        };
                        pd.ProductType.ProcessorBrand = "Brand";
                        pd.ProductType.ProcessorSpeed = new Message.Cls_Product.Cls_ProductData.Cls_ProductType.Cls_ProcessorSpeed()
                        {
                            ProcessorSpeedValue = "1",
                            unitOfMeasure = "Hz"
                        };
                        pd.ProductType.ProcessorType = "";
                        pd.ProductType.ProcessorCount = "1";
                        pd.ProductType.HardwarePlatform = "Plateform";
                    }
                    else if (ProductType == "PDA")
                    {
                        pd.ProductType.ComputerMemoryType = "ddr4_sdram";
                        pd.ProductType.RAMSize = new Message.Cls_Product.Cls_ProductData.Cls_ProductType.Cls_RAMSize()
                        {
                            RAMSizeValue = "4",
                            unitOfMeasure = "GB"
                        };
                        pd.ProductType.ScreenResolution = "ScreenResolution";
                        pd.ProductType.ColorScreen = "true";
                    }
                    return pd;
                }
            }
            //食品分类专有设置字段
            if (ProductData == "FoodAndBeverages" ||  ProductData == "Gourmet")
            {
                pd.ProductType.VariationData.VariationTheme = "Flavor";
                if (ParentTag == "child")
                {
                    string AttroValue = AttrPro.Values;
                    if (string.IsNullOrEmpty(AttroValue)) AttroValue = "As the picture shows";
                    pd.ProductType.VariationData.Flavor = AttroValue;
                }
                pd.ProductType.UnitCount = new Message.Cls_Product.Cls_ProductData.Cls_ProductType.Cls_UnitCount()
                {
                    unitOfMeasure = "Default",
                    UnitCountValue = 1
                };
                return pd;
            }
            //美国站 Health设置必填字段
            if ((ProductData == "Health" || ProductType == "HealthMisc") && ParentTag == "child")
            {
                pd.ProductType.UnitCount = new Message.Cls_Product.Cls_ProductData.Cls_ProductType.Cls_UnitCount()
                {
                    unitOfMeasure = "Count",
                    UnitCountValue = 1
                };
            }
            //其余分类属性设置
            string[] Sizes = new string[] { "size", "尺寸", "尺码", "Tamaño" };
            string[] Colors = new string[] { "color", "colour", "颜色", "サイズ", "色" };
            if (AllAttributeText.ContainsOne(Colors) && AllAttributeText.ContainsOne(Sizes))
            {
                if (ProductData == "MusicalInstruments" || ProductData== "PetSupplies")
                {
                    if (ProductType == "InstrumentPartsAndAccessories" || ProductType == "PercussionInstruments" || ProductType == "StringedInstruments" || ProductData == "PetSupplies")
                        pd.ProductType.VariationData.VariationTheme = "SizeColor";
                    else pd.ProductType.VariationData.VariationTheme = "Color";
                    if (ParentTag == "child")
                    {
                        string AttrValues = AttrPro.Values;
                        pd.ProductType.ColorSpecification = new Message.Cls_Product.Cls_ProductData.Cls_ProductType.Cls_ColorSpecification();
                        string ColorValue = AttrValues.Split(',')[1];
                        if (string.IsNullOrEmpty(ColorValue)) ColorValue = "As the picture shows";
                        pd.ProductType.ColorSpecification.Color = ColorValue;
                        pd.ProductType.ColorSpecification.ColorMap = ColorValue;
                        pd.ProductType.Size= AttrValues.Split(',')[0];
                    }
                }
                else
                {
                    if (ProductData == "Clothing") pd.ProductType.VariationData.VariationTheme = "SizeColor";
                    else if(ProductData== "Sports") pd.ProductType.VariationData.VariationTheme = "ColorSize";
                    else if (ProductData == "Jewelry") SetJewelryVariationTheme(pd, ProductType);
                    else pd.ProductType.VariationData.VariationTheme = "Size-Color";
                    if (ParentTag == "child")
                    {
                        if (ProductData != "Office" && ProductData != "AutoAccessory")
                        {
                            string AttrValues = AttrPro.Values;
                            string GemType = Up.ExtTypeValue?.Name;
                            if (ProductData != "Jewelry")
                            {
                                pd.ProductType.VariationData.Size = AttrValues.Split(',')[0];
                                pd.ProductType.VariationData.Color = AttrValues.Split(',')[1];
                            }
                            else SetJewelryVariationData(pd, ProductType, AttrPro.Values, GemType);//Jewelry
                        }
                    }
                }
            }
            else if (AllAttributeText.ContainsOne(Sizes))
            {
                pd.ProductType.VariationData.VariationTheme = "Size";
                if (ProductData == "Clothing")
                {
                    pd.ProductType.VariationData.VariationTheme = "SizeColor";
                    if (ParentTag == "child")
                    {
                        pd.ProductType.VariationData.Size = AttrPro.Values;
                        pd.ProductType.VariationData.Color = "As the picture shows";
                    }
                }
                else if (ProductData == "MusicalInstruments" || ProductData == "PetSupplies")
                {
                    if (ProductType == "InstrumentPartsAndAccessories" || ProductType == "PercussionInstruments" || ProductType == "StringedInstruments" || ProductData == "PetSupplies")
                        pd.ProductType.VariationData.VariationTheme = "Size";
                    else pd.ProductType.VariationData.VariationTheme = null;
                    if (ParentTag == "child")
                    {
                        string AttrValues = AttrPro.Values;
                        if (string.IsNullOrEmpty(AttrValues)) AttrValues = "One Size";
                        pd.ProductType.Size = AttrValues;
                    }
                }
                else
                {
                    if (ProductData == "Jewelry") SetJewelryVariationTheme(pd, ProductType);
                    if (ParentTag == "child")
                    {
                        if (ProductData != "Office" && ProductData != "AutoAccessory")
                        {
                            string AttrValues = AttrPro.Values;
                            string GemType = Up.ExtTypeValue?.Name;
                            if (string.IsNullOrEmpty(AttrValues)) AttrValues = "One size";
                            if (ProductData == "Jewelry") SetJewelryVariationData(pd, ProductType, AttrPro.Values, GemType);//Jewelry
                            else
                            {
                                //pd.ProductType.VariationData.Color = "As the picture shows";
                                pd.ProductType.VariationData.Size = AttrValues;
                            }
                        }
                    }
                }
            }
            else if (AllAttributeText.ContainsOne(Colors))
            {
                pd.ProductType.VariationData.VariationTheme = "Color";
                if (ProductData == "Clothing")
                {
                    pd.ProductType.VariationData.VariationTheme = "SizeColor";
                    if (ParentTag == "child")
                    {
                        pd.ProductType.VariationData.Size = "As the picture shows";
                        pd.ProductType.VariationData.Color = AttrPro.Values;
                    }
                }
                else if (ProductData == "MusicalInstruments" || ProductData == "PetSupplies")
                {
                    if (ParentTag == "child")
                    {
                        string AttrValues = AttrPro.Values;
                        pd.ProductType.ColorSpecification = new Message.Cls_Product.Cls_ProductData.Cls_ProductType.Cls_ColorSpecification();
                        if (string.IsNullOrEmpty(AttrValues)) AttrValues = "As the picture shows";
                        pd.ProductType.ColorSpecification.Color = AttrValues;
                        pd.ProductType.ColorSpecification.ColorMap = AttrValues;
                    }
                }
                else 
                {
                    if (ProductData == "Jewelry") SetJewelryVariationTheme(pd, ProductType);
                    if (ParentTag == "child")
                    {
                        if (ProductData != "Office" && ProductData != "AutoAccessory")
                        {
                            string AttrValues = AttrPro.Values;
                            string GemType = Up.ExtTypeValue?.Name;
                            if (string.IsNullOrEmpty(AttrValues)) AttrValues = "As the picture shows";
                            if (ProductData == "Jewelry") SetJewelryVariationData(pd, ProductType, AttrPro.Values, GemType);//Jewelry
                            else
                            {
                                pd.ProductType.VariationData.Color = AttrValues;
                            }
                        }
                    }
                }
            }
            //特殊分类特殊处理
            if (ProductData == "Clothing")
            {
                pd.ProductType.ClassificationData = new Message.Cls_Product.Cls_ProductData.Cls_ProductType.Cls_ClassificationData();
                pd.ProductType.ClassificationData.ClothingType = ProductType;
                pd.ProductType.ClassificationData.Department = "womens";
                pd.ProductType.ClassificationData.ColorMap = "As The Picture Shows";
                pd.ProductType.ClassificationData.MaterialComposition = "Defalut";
                pd.ProductType.ClassificationData.OuterMaterial = "Defalut";
                pd.ProductType.ClassificationData.SizeMap = "As The Picture Shows";
            }
            if (ProductData == "Health" && ParentTag == "child")
            {
                pd.ProductType.IsAdultProduct = "true";
            }
            if (ProductType == "WirelessLockedPhone" && ParentTag == "child")
            {
                pd.ProductType.WirelessProvider = "unknown_wireless_provider_type";
            }
            if (ProductType == "WirelessAccessories" && ParentTag == "child")
            {
                pd.ProductType.CompatiblePhoneModels = "unknown";
            }
            return pd;
        }
        private void SetJewelryVariationTheme(Message.Cls_Product.Cls_ProductData pd,string ProductType)
        {
            if (ProductType == "Watch") pd.ProductType.VariationData.VariationTheme = "StyleName";
            //else if (ProductType == "FashionRing") pd.ProductType.VariationData.VariationTheme = "RingSize";
            //else if (ProductType == "FineNecklaceBraceletAnklet" || ProductType== "FineRing" || ProductType== "FineEarring") pd.ProductType.VariationData.VariationTheme = "GemType";
            else pd.ProductType.VariationData.VariationTheme = "MetalType";
        }
        private void SetJewelryVariationData(Message.Cls_Product.Cls_ProductData pd, string ProductType, string ColorValue, string GemType, string Color = null, string Size = null)
        {
            if (Color.IsNotWhiteSpace()) pd.Color = Color;
            if (ProductType == "Watch") pd.StyleName = ColorValue;
            else 
            {
                pd.ProductType.Stone = new Message.Cls_Product.Cls_ProductData.Cls_ProductType.Cls_Stone();
                //高级宝石（需要审核）||时尚宝石
                pd.ProductType.Stone.GemType = GemType?? "unknown";
            }
            //if (ProductType == "FashionRing")
            //{
            //    pd.ProductType.VariationData.VariationTheme = "Color";//"RingSize";
            //    pd.ProductType.VariationData.Color = ColorValue;
            //}
            if (ProductType == "FineRing") pd.ProductType.VariationData.RingSize = "1";
            pd.StyleName = ColorValue;
            if (Size.IsNotWhiteSpace()) pd.Size = Size;
            pd.MetalType = ColorValue;
            pd.ProductType.DepartmentName = ColorValue;//该值为美国站Jewelry必设值
        }
        /// <summary>
        /// 上传商品的库存
        /// </summary>
        /// <param name="Up"></param>
        /// <returns></returns>
        private AmazonEnvelope GetProInventory(UploadParameter Up)
        {
            //商品的Sku在上传时获取
            AmazonEnvelope ProInventory = new AmazonEnvelope();
            ProInventory.Header = new AmazonEnvelope.Cls_Header();
            ProInventory.Header.DocumentVersion = "1.01";
            ProInventory.Header.MerchantIdentifier = Up.Shop.SellerID;
            ProInventory.MessageType = "Inventory";
            ProInventory.Message = new List<Message>();
            int i = 1;
            if (Up.Product.Products != null && Up.Product.Products.Count > 0)
            {
                foreach (var AttrPro in Up.Product.Products)
                {
                    Message ChildMessage = GetInventoryMessage(Up, i, AttrPro);
                    ProInventory.Message.Add(ChildMessage);
                    i++;
                }
            }
            else
            {
                Message message = GetInventoryMessage(Up, i);
                ProInventory.Message.Add(message);
            }
            return ProInventory;
        }
        private Message GetInventoryMessage(UploadParameter Up, int i, IVaritantProduct AttrPro = null)
        {
            Message message = new Message();
            message.MessageID = i.ToString();
            message.OperationType = "Update";
            message.Inventory = new Message.Cls_Inventory();
            if (AttrPro!=null)
            {
                message.Inventory.SKU = AttrPro.Skucode;
                message.Inventory.Quantity = AttrPro.Quantity.ToString();
            }
            else
            {
                message.Inventory.SKU = Up.Product.Skucode;
                message.Inventory.Quantity = Up.Product.Quantity.ToString();
            }
            message.Inventory.FulfillmentLatency = "1";
            message.Inventory.SwitchFulfillmentTo = "MFN";
            return message;
        }
        /// <summary>
        /// 上传商品的价格
        /// </summary>
        /// <param name="Up"></param>
        /// <returns></returns>
        private AmazonEnvelope GetProPrice(UploadParameter Up)
        {
            //商品的Sku在上传时获取
            AmazonEnvelope ProPrice = new AmazonEnvelope();
            ProPrice.Header = new AmazonEnvelope.Cls_Header();
            ProPrice.Header.DocumentVersion = "1.01";
            ProPrice.Header.MerchantIdentifier = Up.Shop.SellerID;
            ProPrice.MessageType = "Price";
            ProPrice.Message = new List<Message>();
            int i = 1;
            if (Up.Product.Products != null && Up.Product.Products.Count > 0)
            {
                foreach (var AttrPro in Up.Product.Products)
                {
                    Message message = GetPriceMessage(Up, i, AttrPro);
                    ProPrice.Message.Add(message);
                    i++;
                }
            }
            else
            {
                Message Pmessage = GetPriceMessage(Up, i);
                ProPrice.Message.Add(Pmessage);
            }
            return ProPrice;
        }
        private Message GetPriceMessage(UploadParameter Up, int i, IVaritantProduct AttrPro=null)
        {
            Message message = new Message();
            message.MessageID = i.ToString();
            message.Price = new Message.Cls_Price();
            if (AttrPro!=null) { message.Price.SKU = AttrPro.Skucode; }
            else { message.Price.SKU = Up.Product.Skucode; }
            message.Price.StandardPrice = new Cls_StandardPrice();
            Cls_StandardPrice StandardPrice = new Cls_StandardPrice();
            StandardPrice.currency = Up.Product.Currency.ToString();
            if(AttrPro.IsNull()) StandardPrice.PriceValue = Up.Product.Price.ToString();
            else StandardPrice.PriceValue = AttrPro.Price.ToString();
            message.Price.StandardPrice = StandardPrice;
            return message;
        }
        /// <summary>
        /// 上传商品的图片，并赋予主图及其他
        /// </summary>
        /// <param name="Up"></param>
        /// <returns></returns>
        private AmazonEnvelope GetProPic(UploadParameter Up)
        {
            //商品SKU在上传时获取
            AmazonEnvelope proPic = new AmazonEnvelope();
            proPic.Header = new AmazonEnvelope.Cls_Header();
            proPic.Header.DocumentVersion = "1.01";
            proPic.Header.MerchantIdentifier = Up.Shop.SellerID;
            proPic.MessageType = "ProductImage";
            proPic.Message = new List<Message>();
            List<string> ParentPath = Up.Product.Images?.ConvertAll((img) => img.URL).Take(10).ToList();
            if (!(ParentPath?.Count > 0)) return null;
            //if(ParentPath==null || ParentPath.Count=0) return null;
            int num = 1;
            //有子商品时只上传子商品的图片
            if (Up.Product.Products != null && Up.Product.Products.Count > 0)
            {
                foreach (var AttrPro in Up.Product.Products)
                {
                    List<string> ChildPath = AttrPro.Images?.ConvertAll((img) => img.URL).Take(10).ToList();
                    if (!(ChildPath?.Count > 0)) return null;
                    int Childi = 0;
                    foreach (string img in ChildPath)
                    {
                        Message message = GetPicMessage(Up, num, Childi, img, AttrPro);
                        proPic.Message.Add(message);
                        num++; Childi++;
                    }
                }
            }
            else
            {
                int i = 0;
                ParentPath?.All((img) =>
                {
                    Message message = GetPicMessage(Up, num, i, img);
                    proPic.Message.Add(message);
                    num++; i++;
                    return true;
                });              
            }
            return proPic;
        }
        private Message GetPicMessage(UploadParameter Up, int num,int i,string img, IVaritantProduct AttrPro = null)
        {
            Message message = new Message();
            message.MessageID = num.ToString();
            message.OperationType = "Update";
            message.ProductImage = new Message.Cls_ProductImage();
            if (AttrPro == null) { message.ProductImage.SKU = Up.Product.Skucode; }
            else { message.ProductImage.SKU = AttrPro.Skucode; }
            message.ProductImage.ImageType = (YY.ImageType)i;
            message.ProductImage.ImageLocation = img;
            return message;
        }
        /// <summary>
        /// 获得变体对象
        /// </summary>
        /// <returns></returns>
        private AmazonEnvelope GetProRelationShip(UploadParameter Up, string _ParentSku, List<string> _ChildrenSku)
        {
            if (_ChildrenSku != null && _ChildrenSku.Count > 0)
            {
                AmazonEnvelope proRelationShip = new AmazonEnvelope();
                proRelationShip.Header = new AmazonEnvelope.Cls_Header();
                proRelationShip.Header.DocumentVersion = "1.01";
                proRelationShip.Header.MerchantIdentifier = Up.Shop.SellerID;
                proRelationShip.MessageType = "Relationship";
                proRelationShip.Message = new List<Message>();
                Message message = new Message();
                message.MessageID = "1";
                message.OperationType = "Update";
                message.Relationship = new Message.Cls_Relationship();
                message.Relationship.ParentSKU = _ParentSku;
                message.Relationship.Relation = new List<Relation>();
                foreach (string ChildSku in _ChildrenSku)
                {
                    Relation relation = new Relation();
                    relation.SKU = ChildSku;
                    relation.Type = "Variation";
                    message.Relationship.Relation.Add(relation);
                }
                proRelationShip.Message.Add(message);
                return proRelationShip;
            }
            return null; 
        }
        private string GetOtherXml(AmazonEnvelope ProOtherInfo)
        {
            return XmlObject.Serialize(ProOtherInfo).Replace("utf-16", "utf-8");
        }
        #endregion 
        #region 订单
        public void GetOrder(OrderParameter OP)
        {
            //截止到当前时间==false
            if (OP.CreateAfterTime > OP.CreateBeforeTime)
            {
                OP.Log(I18N.Instance.MWS_Log16);
                return;
            }
            if (OP.CreateBeforeTime >= DateTime.Now)
            {
                OP.Log(I18N.Instance.MWS_Log17);
                return;
            }
            var AllOrder = GetOrderList(OP);
            if (AllOrder.IsNotNull() && AllOrder.Count > 0)
            {
                GetOrderInfo(OP, AllOrder);
            }
        }
        /// <summary>
        /// 拉取订单
        /// </summary>
        /// <param name="OP"></param>
        public List<ListOrdersResponse.Cls_ListOrdersResult.Cls_Orders.OrderItem> GetOrderList(OrderParameter OP)
        {
            OP.Log = Mod_GlobalFunction.GetLoger(OP.Log);
            OP.Log($"{I18N.Instance.MWS_Log18}\n");
            string action = "ListOrders";
            string xml = MWS.Gethtml(OP, action);//请求订单列表
            if (xml.Contains("AccessDenied"))
            {
                OP.Log(I18N.Instance.MWS_Log19);
                if (OP.FinishAuto.IsNotNull()) OP.FinishAuto();
                return null;
            }
            else if (xml.ToLower().Contains("badproxy"))
            {
                OP.Log(I18N.Instance.MWS_Log20);
                return null;
            }
            ListOrdersResponse lst = new ListOrdersResponse();
            var AllOrder = new List<ListOrdersResponse.Cls_ListOrdersResult.Cls_Orders.OrderItem>();
            try
            {
                using (StringReader sr = new StringReader(xml))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ListOrdersResponse));
                    lst = serializer.Deserialize(sr) as ListOrdersResponse;
                }
                AllOrder = lst.ListOrdersResult.Orders.Order;
            }
            catch { OP.Log(I18N.Instance.MWS_Log21); return null; }
            //获取的订单列表中是否NextToken为空===查询nextToken中的订单
            //1.通过获取的NextToken获取其他的ListOrdersResponse
            //2.将获取的对象获取order加入lst的order的list中
            List<GetOrderMessage> OrderMessages = new List<GetOrderMessage>();
            if ( AllOrder == null || AllOrder.Count == 0)
            {
                if (OP.FinishAuto.IsNotNull()) OP.FinishAuto();
                OP.Log(I18N.Instance.MWS_Log22);
                return null;
            }
            if (!string.IsNullOrEmpty(lst.ListOrdersResult.NextToken))
            {
                GetOrdersByNextToToken(OP, lst.ListOrdersResult.NextToken, AllOrder);
            }
            return AllOrder;
        }
        public void GetOrderInfo(OrderParameter OP, List<ListOrdersResponse.Cls_ListOrdersResult.Cls_Orders.OrderItem> AllOrder)
        {
            MultiThread MutiThread = new MultiThread();
            foreach (var order in AllOrder)
            {
                MutiThread.AddTask(() =>
                {
                    GetOrderMessage SingleOrder = new GetOrderMessage();
                    SingleOrder.OrderBasic = order;
                    //获取订单详细信息
                    OP.Log(I18N.Instance.get_MWS_Log23(order.AmazonOrderId));//$"正在获取[{}]订单详细信息..."
                    Cls_ListOrderItemsResponse OrderMessage = GetOrderItem(OP, order.AmazonOrderId);//请求订单详细信息
                    if (OrderMessage == null)
                    {
                        OP.Log(I18N.Instance.get_MWS_Log24(order.AmazonOrderId)); //$"获取[{}]订单详细信息失败！"
                        return;
                    }
                    OP.Log(I18N.Instance.get_MWS_Log25(order.AmazonOrderId));//$"获取[{}]订单详细信息成功！"
                    //存放所有的订单中产品的详细信息
                    var AllItems = OrderMessage.ListOrderItemsResult.OrderItems.OrderItem;
                    //string OrderItemsNextToken = OrderMessage.ListOrderItemsResult.NextToken;
                    //if (!string.IsNullOrEmpty(OrderItemsNextToken))
                    //{
                    //    GetOrderItemsByNextToToken(OP, OrderItemsNextToken, AllItems);
                    //}
                    var orderItems = new List<Cls_ListOrderItemsResponse.Cls_ListOrderItemsResult.Cls_OrderItems.OrderItemItem>();
                    foreach (Cls_ListOrderItemsResponse.Cls_ListOrderItemsResult.Cls_OrderItems.OrderItemItem orderItem in AllItems)
                    {
                        string itemUrl = GetOrderUrl(OP, orderItem.ASIN);//请求订单图片信息
                        if (!string.IsNullOrEmpty(itemUrl))
                        {
                            orderItem.Url = itemUrl;
                            orderItems.Add(orderItem);
                        }
                    }
                    SingleOrder.OrderMessage = orderItems;
                    //获取金额部分
                    //Unshipped/Canceled
                    //Shipped 
                    if (order.OrderStatus == "Shipped")
                    {
                        OP.Log(I18N.Instance.get_MWS_Log26(order.AmazonOrderId));//$"正在获取[{}]订单具体费用..."
                        GetShipment(OP, order.AmazonOrderId, SingleOrder);
                        OP.Log(I18N.Instance.get_MWS_Log29(order.AmazonOrderId));//$"正在同步[{}]订单..."
                    }
                    string json = JsonObject.Serialize(SingleOrder);
                    json = Sy.Text.Encode.RemoveEscape_HTML(json);
                    OP.UpLoadServer(json);
                    OP.Log($"{I18N.Instance.get_MWS_Log30(order.AmazonOrderId)}\r\n");
                    //$"同步[{}]订单结束！\r\n"
                    //订单号:{_Orderitem.OrderBasic.AmazonOrderId}
                });
            }
            MutiThread.SetAllTasked(() =>
            {
                if(OP.FinishAuto.IsNotNull()) OP.FinishAuto();
                OP.Log(I18N.Instance.MWS_Log31);
            });
            MutiThread.SetAllThreadStarted(() => OP.Log(I18N.Instance.MWS_Log32));
            MutiThread.SetMaxThreads(10);
            MutiThread.Start();
        }
        /// <summary>
        /// 通过Token获取下一页订单列表
        /// </summary>
        /// <param name="OP"></param>
        /// <param name="NextToToken"></param>
        /// <param name="AllOrder"></param>
        private void GetOrdersByNextToToken(OrderParameter OP,string NextToToken, List<ListOrdersResponse.Cls_ListOrdersResult.Cls_Orders.OrderItem> AllOrder)
        {
            string action = "ListOrdersByNextToken";
            string xml = MWS.Gethtml(OP, action,"", null, NextToToken);
            ListOrdersResponseByNextToken lst = new ListOrdersResponseByNextToken();
            try
            {
                using (StringReader sr = new StringReader(xml))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ListOrdersResponseByNextToken));
                    lst = serializer.Deserialize(sr) as ListOrdersResponseByNextToken;
                }
                if (lst != null && lst.ListOrdersByNextTokenResult.Orders.Order.Count > 0)
                {
                    AllOrder.AddRange(lst.ListOrdersByNextTokenResult.Orders.Order);
                    string NextToken = lst.ListOrdersByNextTokenResult.NextToken;
                    if (!string.IsNullOrEmpty(NextToken))
                    {
                        GetOrdersByNextToToken(OP, NextToken, AllOrder);
                    }
                }
            }
            catch { OP.Log(I18N.Instance.MWS_Log33); return; }
        }
        /// <summary>
        /// 通过Token获取下一页订单详细信息
        /// </summary>
        /// <param name="OP"></param>
        /// <param name="NextToToken"></param>
        /// <param name="AllOrderItems"></param>
        private void GetOrderItemsByNextToToken(OrderParameter OP, string NextToToken, List<Cls_ListOrderItemsResponse.Cls_ListOrderItemsResult.Cls_OrderItems.OrderItemItem> AllOrderItems)
        {
            string action = "ListOrderItemsByNextToken";
            string xml = MWS.Gethtml(OP, action, "", null, NextToToken);
            Cls_ListOrderItemsResponseByNextToken lst = new Cls_ListOrderItemsResponseByNextToken();
            try
            {
                using (StringReader sr = new StringReader(xml))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Cls_ListOrderItemsResponseByNextToken));
                    lst = serializer.Deserialize(sr) as Cls_ListOrderItemsResponseByNextToken;
                }
                if (lst != null && lst.ListOrderItemsByNextTokenResult.OrderItems.OrderItem.Count > 0)
                {
                    foreach (Cls_ListOrderItemsResponse.Cls_ListOrderItemsResult.Cls_OrderItems.OrderItemItem orderItem in lst.ListOrderItemsByNextTokenResult.OrderItems.OrderItem)
                    {
                        AllOrderItems.Add(orderItem);
                    }
                    string NextToken = lst.ListOrderItemsByNextTokenResult.NextToken;
                    if (!string.IsNullOrEmpty(NextToken))
                    {
                        GetOrderItemsByNextToToken(OP, NextToken, AllOrderItems);
                    }
                }
            }
            catch { OP.Log(I18N.Instance.MWS_Log34); return; }
        }
        /// <summary>
        /// 获取订单图片
        /// </summary>
        /// <param name="OP"></param>
        /// <param name="Asin"></param>
        /// <returns></returns>
        public string GetOrderUrl(OrderParameter OP, string Asin)
        {
            string action = "GetMatchingProduct";
            Sy.String xml = MWS.Gethtml(OP, action, "", new List<string>() { Asin });
            string SpliterUrlS = "<ns2:URL>", SpliterUrlE = "</ns2:URL>";
            if (xml.CanMid(SpliterUrlS, SpliterUrlE))
            {
                return xml.Mid(SpliterUrlS, SpliterUrlE);
            }
            OP.Log(I18N.Instance.MWS_Log35);
            return "/public/static/images/order.png";
        }
        /// <summary>
        /// 获取订单相信信息
        /// </summary>
        /// <param name="OP"></param>
        /// <param name="AmazonID"></param>
        /// <returns></returns>
        private Cls_ListOrderItemsResponse GetOrderItem(OrderParameter OP, string AmazonID)
        {
            string action = "ListOrderItems";
            string xml = MWS.Gethtml(OP, action, AmazonID);
            Cls_ListOrderItemsResponse item = new Cls_ListOrderItemsResponse(); 
            try
            {
                using (StringReader sr = new StringReader(xml))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Cls_ListOrderItemsResponse));
                    item = serializer.Deserialize(sr) as Cls_ListOrderItemsResponse;
                }
            }
            catch { OP.Log(I18N.Instance.MWS_Log36);return null; }
            return item;
        }
        private void GetShipment(OrderParameter OP, string AmazonID, GetOrderMessage OrderMessage)
        {
            string action = "ListFinancialEvents";
            string xml = MWS.Gethtml(OP, action, AmazonID);
            ListFinancialEventsResponse item = new ListFinancialEventsResponse();
            try
            {
                using (StringReader sr = new StringReader(xml))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ListFinancialEventsResponse));
                    item = serializer.Deserialize(sr) as ListFinancialEventsResponse;
                }
            }
            catch
            { 
                OP.Log(I18N.Instance.MWS_Log36); 
                return; 
            }
            if (item.ListFinancialEventsResult.FinancialEvents.ShipmentEventList == null)
            {
                OP.Log(I18N.Instance.get_MWS_Log27(AmazonID));
                //$"获取[{}]订单具体费用失败"
            }
            OrderMessage.ShipmentEventList = item?.ListFinancialEventsResult?.FinancialEvents?.ShipmentEventList;
            OrderMessage.RefundEventList = item?.ListFinancialEventsResult?.FinancialEvents?.RefundEventList;
            OP.Log(I18N.Instance.get_MWS_Log28(AmazonID));//$"获取[{}]订单具体费用成功"
            //if (item.ListFinancialEventsResult.FinancialEvents.RefundEventList == null)
            //{
            //    OP.Log(I18N.Instance.get_MWS_Log27(AmazonID));
            //    //$"获取[{}]订单具体费用失败"
            //}
        }
        /// <summary>
        /// 同步运单至亚马逊(上传运单信息)
        /// </summary>
        /// <param name="DP"></param>
        /// <returns></returns>
        public string DeliverGoods(DeliverGoodsParameter DP)
        {
            DeliverParameter DP2 = new DeliverParameter();
            DP2.Shop = DP.Shop;
            DP2.Products = new List<IDeliverGoodsParameter2.IProduct>();
            DeliverProduct pro = new DeliverProduct();
            pro.AmazonOrderID = DP.AmazonOrderID;
            pro.FulfillmentDate = DP.FulfillmentDate;
            pro.CarrierName = DP.CarrierName;
            pro.ShippingMethod = DP.ShippingMethod;
            pro.ShipperTrackingNumber = DP.ShipperTrackingNumber;
            pro.AmazonOrderItemCode = DP.AmazonOrderItemCode;
            pro.Quantity = Convert.ToInt32(DP.Quantity);
            DP2.Products.Add(pro);
            DP2.Log = DP.Log;
            return DeliverGoods(DP2);
        }
        /// <summary>
        /// 获取订单状态
        /// </summary>
        /// <param name="DP"></param>
        /// <returns></returns>
        public bool GetOrdeItem(DeliverGoodsParameter DP)
        {
            string action = "GetOrder";
            DateTime dt = DateTime.Now;
            OrderParameter OP = new OrderParameter(DP.Shop, dt, dt, DP.Log, DP.UpLoadServer, null);
            DP.Log(I18N.Instance.get_MWS_Log38(string.Join(",", DP.AmazonOrderIDs)));//$"正在获取{}订单状态..."
            Sy.String xml = MWS.Gethtml(OP, action, "", null, "", "", "", null, DP.AmazonOrderIDs);
            if (string.IsNullOrEmpty(xml))
            {
                DP.Log(I18N.Instance.get_MWS_Log39(string.Join(",", DP.AmazonOrderIDs)));//$"请求获取{}订单状态失败!"
                return false;
            }
            if (xml.Contains("AccessDenied"))
            {
                OP.Log(I18N.Instance.MWS_Log19);
                if (OP.FinishAuto.IsNotNull()) OP.FinishAuto();
                return false;
            }
            else if (xml.ToLower().Contains("badproxy"))
            {
                OP.Log(I18N.Instance.MWS_Log20);
                return false;
            }
            GetOrderResponse lst = new GetOrderResponse();
            var AllOrder = new List<ListOrdersResponse.Cls_ListOrdersResult.Cls_Orders.OrderItem>();
            try
            {
                using (StringReader sr = new StringReader(xml))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(GetOrderResponse));
                    lst = serializer.Deserialize(sr) as GetOrderResponse;
                }
                AllOrder = lst.GetOrderResult.Orders.Order;
            }
            catch
            {
                DP.Log(I18N.Instance.get_MWS_Log41(string.Join(",", DP.AmazonOrderIDs)));
                //$"获取{}订单状态失败,请检查订单号及亚马逊参数是否正确!" 
                return false;
            }
            if (AllOrder == null || AllOrder.Count == 0)
            {
                DP.Log(I18N.Instance.get_MWS_Log41(DP.AmazonOrderID));
                //$"获取{}订单状态失败,请检查订单号及亚马逊参数是否正确!"
                return false;
            }
            var NeedUpdateOrder = new List<ListOrdersResponse.Cls_ListOrdersResult.Cls_Orders.OrderItem>();
            foreach (var TempOrder in AllOrder)
            {
                var state = TempOrder.OrderStatus;
                if(DP.Report.IsNotNull()) DP.Report(state);
                DP.Log(I18N.Instance.get_MWS_Log40(DP.AmazonOrderID, state));
                //$"获取{}订单状态成功!订单状态:[{}]"
                //拉取订单信息
                if (state != "Unshipped") NeedUpdateOrder.Add(TempOrder);
            }
            if (NeedUpdateOrder.Count > 0) GetOrderInfo(OP, NeedUpdateOrder);
            return true;
        }
        public string DeliverGoods(IDeliverGoodsParameter2 DP)
        {
            AmazonEnvelope amazon = new AmazonEnvelope();
            amazon.Header = new AmazonEnvelope.Cls_Header
            {
                DocumentVersion = "1.01",
                MerchantIdentifier = DP.Shop.SellerID
            };
            DP.Log(I18N.Instance.MWS_Log42);
            amazon.MessageType = "OrderFulfillment";
            amazon.Message = new List<Message>();
            //2019-11-02T18:59:59Z
            if (DP.Products.IsNull() || DP.Products.Count == 0)
            {
                DP.Log($"{I18N.Instance.MWS_Log45}[Products is null]");
                return string.Empty;
            }
            int ID = 1;
            string AmazonOrderID = DP.Products.Last().AmazonOrderID;
            string FulfillmentDate = DP.Products.Last().FulfillmentDate;
            string CarrierName = DP.Products.Last().CarrierName ?? "Other";
            string ShippingMethod = DP.Products.Last().ShippingMethod ?? "Standard";
            string ShipperTrackingNumber = DP.Products.Last().ShipperTrackingNumber;
            Message message = new Message();
            message.MessageID = ID++.ToString();
            message.OrderFulfillment = new Message.Cls_OrderFulfillment();

            message.OrderFulfillment.AmazonOrderID = AmazonOrderID;
            message.OrderFulfillment.FulfillmentDate = FulfillmentDate;
            message.OrderFulfillment.FulfillmentData = new Message.Cls_OrderFulfillment.Cls_FulfillmentData();
            message.OrderFulfillment.FulfillmentData.CarrierName = CarrierName;
            message.OrderFulfillment.FulfillmentData.ShippingMethod = ShippingMethod;
            message.OrderFulfillment.FulfillmentData.ShipperTrackingNumber = ShipperTrackingNumber;
            var items = new List<Message.Cls_OrderFulfillment.Cls_Item>();
            foreach (var pro in DP.Products)
            {
                if (pro.HasAmazonOrderItemCode)
                {
                    var item = new Message.Cls_OrderFulfillment.Cls_Item();
                    item.AmazonOrderItemCode = pro.AmazonOrderItemCode;
                    item.Quantity = pro.Quantity.ToString();
                    items.Add(item);
                }
            }
            if (items.Count == 0) message.OrderFulfillment.Item = null;
            else message.OrderFulfillment.Item = items;
            amazon.Message.Add(message);
            string DeliverGoodsxml = GetOtherXml(amazon);
            SaveFile(DeliverGoodsxml, SessionType.PRODUCT);
            Imp_ShopInfo shop = new Imp_ShopInfo(DP.Shop.MarketPlace, DP.Shop.SellerID, DP.Shop.MwsAuthToken, DP.Shop.AwsAccessKey, DP.Shop.SecretKey);
            DP.Log(I18N.Instance.MWS_Log43);
            string ErrMessage = "";
            INF_Mws.Result UploadDeliverGoodsInfo = MWS.Upload(DeliverGoodsxml, "_POST_ORDER_FULFILLMENT_DATA_", shop, out string OrderFeedSubMissionID, ref ErrMessage, DP.Log);
            if (UploadDeliverGoodsInfo.Equals(INF_Mws.Result.Success))
            {
                DP.Update();
                DP.Log(I18N.Instance.get_MWS_Log44(""));
                return OrderFeedSubMissionID;
            }
            DP.Log(I18N.Instance.MWS_Log45);
            return string.Empty;
        }
        public string DeliverGoods(List<IDeliverGoodsParameter2> DPs)
        {
            if (DPs.IsNull() || DPs.Count <= 0) return string.Empty;
            var Shop = DPs[0].Shop;
            var Log = DPs[0].Log;
            AmazonEnvelope amazon = new AmazonEnvelope();
            amazon.Header = new AmazonEnvelope.Cls_Header
            {
                DocumentVersion = "1.01",
                MerchantIdentifier = Shop.SellerID
            };
            Log(I18N.Instance.MWS_Log42);
            amazon.MessageType = "OrderFulfillment";
            amazon.Message = new List<Message>();
            //2019-11-02T18:59:59Z
            int ID = 1;
            foreach (var DP in DPs)
            {
                if (DP.Products.IsNull() || DP.Products.Count == 0) continue;
                string AmazonOrderID = DP.Products.Last().AmazonOrderID;
                string FulfillmentDate = DP.Products.Last().FulfillmentDate;
                string CarrierName = DP.Products.Last().CarrierName ?? "Other";
                string ShippingMethod = DP.Products.Last().ShippingMethod ?? "Standard";
                string ShipperTrackingNumber = DP.Products.Last().ShipperTrackingNumber;
                Message message = new Message();
                message.MessageID = ID++.ToString();
                message.OrderFulfillment = new Message.Cls_OrderFulfillment();
                message.OrderFulfillment.AmazonOrderID = AmazonOrderID;
                message.OrderFulfillment.FulfillmentDate = FulfillmentDate;
                message.OrderFulfillment.FulfillmentData = new Message.Cls_OrderFulfillment.Cls_FulfillmentData();
                message.OrderFulfillment.FulfillmentData.CarrierName = CarrierName;
                message.OrderFulfillment.FulfillmentData.ShippingMethod = ShippingMethod;
                message.OrderFulfillment.FulfillmentData.ShipperTrackingNumber = ShipperTrackingNumber;
                var items = new List<Message.Cls_OrderFulfillment.Cls_Item>();
                foreach (var pro in DP.Products)
                {
                    if (pro.HasAmazonOrderItemCode)
                    {
                        var item = new Message.Cls_OrderFulfillment.Cls_Item();
                        item.AmazonOrderItemCode = pro.AmazonOrderItemCode;
                        item.Quantity = pro.Quantity.ToString();
                        items.Add(item);
                    }
                }
                if (items.Count == 0) message.OrderFulfillment.Item = null;
                else message.OrderFulfillment.Item = items;
                amazon.Message.Add(message);
            }
            string DeliverGoodsxml = GetOtherXml(amazon);
            SaveFile(DeliverGoodsxml, SessionType.PRODUCT);
            Imp_ShopInfo shop = new Imp_ShopInfo(Shop.MarketPlace, Shop.SellerID, Shop.MwsAuthToken, Shop.AwsAccessKey, Shop.SecretKey);
            Log(I18N.Instance.MWS_Log43);
            string ErrMessage = "";
            INF_Mws.Result UploadDeliverGoodsInfo = MWS.Upload(DeliverGoodsxml, "_POST_ORDER_FULFILLMENT_DATA_", shop, out string OrderFeedSubMissionID, ref ErrMessage, Log);
            if (UploadDeliverGoodsInfo.Equals(INF_Mws.Result.Success))
            {
                return OrderFeedSubMissionID;
            }
            Log(I18N.Instance.MWS_Log45);
            return string.Empty;
        }
        #region 发票确认
        /// <summary>
        /// 发票确认
        /// </summary>
        /// <param name="DP"></param>
        /// <returns></returns>
        public string UpdateInvoice(IDeliverGoodsParameter2 DP)
        {
            AmazonEnvelope amazon = new AmazonEnvelope();
            amazon.Header = new AmazonEnvelope.Cls_Header
            {
                DocumentVersion = "1.01",
                MerchantIdentifier = DP.Shop.SellerID
            };
            DP.Log(I18N.Instance.MWS_Log42);
            amazon.MessageType = "InvoiceConfirmation";
            amazon.Message = new List<Message>();
            //2019-11-02T18:59:59Z
            if (DP.Products.IsNull() || DP.Products.Count == 0)
            {
                DP.Log($"{I18N.Instance.MWS_Log45}[Products is null]");
                return string.Empty;
            }
            int ID = 1;
            string AmazonOrderID = DP.Products[0].AmazonOrderID;
            Message message = new Message();
            message.MessageID = ID++.ToString();
            message.InvoiceConfirmation = new Message.Cls_InvoiceConfirmation();

            message.InvoiceConfirmation.AmazonOrderID = AmazonOrderID;
            message.InvoiceConfirmation.InvoiceSentDate = DateTime.Now.ToString();
            var items = new List<Message.Cls_InvoiceConfirmation.Cls_Item>();
            foreach (var pro in DP.Products)
            {
                if (pro.HasAmazonOrderItemCode)
                {
                    var item = new Message.Cls_InvoiceConfirmation.Cls_Item();
                    item.AmazonOrderItemCode = pro.AmazonOrderItemCode;
                    item.QuantityConfirmed = pro.Quantity.ToString();
                    items.Add(item);
                }
            }
            if (items.Count == 0) message.InvoiceConfirmation.Item = null;
            else message.InvoiceConfirmation.Item = items;
            amazon.Message.Add(message);
            string DeliverGoodsxml = GetOtherXml(amazon);
            SaveFile(DeliverGoodsxml, SessionType.PRODUCT);
            Imp_ShopInfo shop = new Imp_ShopInfo(DP.Shop.MarketPlace, DP.Shop.SellerID, DP.Shop.MwsAuthToken, DP.Shop.AwsAccessKey, DP.Shop.SecretKey);
            DP.Log(I18N.Instance.MWS_Log43);
            string ErrMessage = "";
            INF_Mws.Result UploadDeliverGoodsInfo = MWS.Upload(DeliverGoodsxml, "_POST_INVOICE_CONFIRMATION_DATA_", shop, out string OrderFeedSubMissionID, ref ErrMessage, DP.Log);
            if (UploadDeliverGoodsInfo.Equals(INF_Mws.Result.Success))
            {
                DP.Update();
                DP.Log(I18N.Instance.get_MWS_Log44(""));
                return OrderFeedSubMissionID;
            }
            DP.Log(I18N.Instance.MWS_Log45);
            return string.Empty;
        }
        #endregion 
        #endregion
        #region 跟卖
        /// <summary>
        /// True:表任务已结束
        /// </summary>
        /// <param name="Tp"></param>
        /// <returns></returns>
        public bool FollowUpSync(FollowUpTaskParameter Tp)
        {
            FollowUpIsFinished = false;
#region 产品
            SessionState VarState = Tp.GetSessionState(FollowUpSessionType.PRODUCT);
            switch (VarState)
            {
                case SessionState.Wait:
                case SessionState.Submitting:
                    //执行更改上传状态
                    if (VarState == SessionState.Wait) Tp.SetSession(FollowUpSessionType.PRODUCT, SessionState.Submitting, "");
                    //2.执行上传子产品
                    string ErrMessage = "";
                    string ProFeedMissionID = InvokeUploadFollowUpProduct(Tp, ref ErrMessage);
                    //上传成功
                    if (!ProFeedMissionID.IsNullOrEmpty())
                    {
                        //上报
                        Tp.SetSession(FollowUpSessionType.PRODUCT, SessionState.Submitted, ProFeedMissionID);
                        break;
                    }
                    else
                    {
                        if (ErrMessage.IsNotWhiteSpace() && ErrMessage == "NoMessage")
                        {
                            Tp.SetSession(FollowUpSessionType.PRODUCT, SessionState.Complete, "");
                            break;
                        }
                        if (ErrMessage.IsNotWhiteSpace()) Tp.Finish(false, FollowUpSessionType.PRODUCT, ErrMessage);
                        else Tp.Finish(false, FollowUpSessionType.PRODUCT, I18N.Instance.MWS_Log2);
                        return true;
                    }
                case SessionState.Submitted:
                case SessionState.Processing:
                    ErrMessage = "";
                    if (!InvokeFollowUpStatus(Tp, FollowUpSessionType.PRODUCT, ref ErrMessage))
                    {
                        //false为上传失败
                        if (ErrMessage == "AccessDenied" || ErrMessage == "FeedMissionID") return true;
                    }
                    break;
                case SessionState.Down:
                case SessionState.Result:
                    if (!InvokeFollowUpDown(Tp, FollowUpSessionType.PRODUCT) && FollowUpIsFinished) return true;
                    break;
                case SessionState.Complete:
                    break;
            }
#endregion
#region 价格
            SessionState PriceState = Tp.GetSessionState(FollowUpSessionType.PRICING);
            switch (PriceState)
            {
                case SessionState.Wait:
                case SessionState.Submitting:
                    //执行更改上传状态
                    if (VarState == SessionState.Wait) Tp.SetSession(FollowUpSessionType.PRICING, SessionState.Submitting, "");
                    //2.执行上传子产品
                    string ErrMessage = "";
                    string ProFeedMissionID = InvokeUploadFollowUpPrice(Tp, ref ErrMessage);
                    //上传成功
                    if (!ProFeedMissionID.IsNullOrEmpty())
                    {
                        //上报
                        Tp.SetSession(FollowUpSessionType.PRICING, SessionState.Submitted, ProFeedMissionID);
                        break;
                    }
                    else
                    {
                        if (ErrMessage.IsNotWhiteSpace() && ErrMessage == "NoMessage")
                        {
                            Tp.SetSession(FollowUpSessionType.PRICING, SessionState.Complete, "");
                            break;
                        }
                        if (ErrMessage.IsNotWhiteSpace()) Tp.Finish(false, FollowUpSessionType.PRICING, ErrMessage);
                        else Tp.Finish(false, FollowUpSessionType.PRICING, I18N.Instance.MWS_Log2);
                        return true;
                    }
                case SessionState.Submitted:
                case SessionState.Processing:
                    ErrMessage = "";
                    if (!InvokeFollowUpStatus(Tp, FollowUpSessionType.PRICING, ref ErrMessage))
                    {
                        if (ErrMessage == "AccessDenied" || ErrMessage == "FeedMissionID") return true;
                    }
                    break;
                case SessionState.Down:
                case SessionState.Result:
                    if (!InvokeFollowUpDown(Tp, FollowUpSessionType.PRICING) && FollowUpIsFinished) return true;
                    break;
                case SessionState.Complete:
                    break;
            }
#endregion
#region 库存
            SessionState InventoryState = Tp.GetSessionState(FollowUpSessionType.INVENTORY);
            switch (InventoryState)
            {
                case SessionState.Wait:
                case SessionState.Submitting:
                    //执行更改上传状态
                    if (VarState == SessionState.Wait) Tp.SetSession(FollowUpSessionType.INVENTORY, SessionState.Submitting, "");
                    //2.执行上传子产品
                    string ErrMessage = "";
                    string ProFeedMissionID = InvokeUploadFollowUpInventory(Tp, ref ErrMessage);
                    //上传成功
                    if (!ProFeedMissionID.IsNullOrEmpty())
                    {
                        //上报
                        Tp.SetSession(FollowUpSessionType.INVENTORY, SessionState.Submitted, ProFeedMissionID);
                        break;
                    }
                    else
                    {
                        if (ErrMessage.IsNotWhiteSpace() && ErrMessage == "NoMessage")
                        {
                            Tp.SetSession(FollowUpSessionType.INVENTORY, SessionState.Complete, "");
                            break;
                        }
                        if (ErrMessage.IsNotWhiteSpace()) Tp.Finish(false, FollowUpSessionType.INVENTORY, ErrMessage);
                        else Tp.Finish(false, FollowUpSessionType.INVENTORY, I18N.Instance.MWS_Log2);
                        return true;
                    }
                case SessionState.Submitted:
                case SessionState.Processing:
                    ErrMessage = "";
                    if (!InvokeFollowUpStatus(Tp, FollowUpSessionType.INVENTORY, ref ErrMessage))
                    {
                        if (ErrMessage == "AccessDenied" || ErrMessage == "FeedMissionID") return true;
                    }
                    break;
                case SessionState.Down:
                case SessionState.Result:
                    if (!InvokeFollowUpDown(Tp, FollowUpSessionType.INVENTORY) && FollowUpIsFinished) return true;
                    break;
                case SessionState.Complete:
                    for (int i = 0; i <= 2; i++)
                    {
                        if (Tp.GetSessionState((FollowUpSessionType)i) != SessionState.Complete)
                        {
                            return false;//未结束直接再次轮询
                        }
                    }
                    break;
            }
#endregion
            return FollowUpIsFinished;
        }
        private string InvokeUploadFollowUpProduct(FollowUpTaskParameter Tp, ref string ErrMessage)
        {
            var shopInfo = Tp.Shop;
            Imp_ShopInfo Shop = new Imp_ShopInfo(shopInfo.MarketPlace, shopInfo.SellerID, shopInfo.MwsAuthToken, shopInfo.AwsAccessKey, shopInfo.SecretKey);
            AmazonEnvelope pro = new AmazonEnvelope
            {
                Header = new AmazonEnvelope.Cls_Header()
            };
            pro.Header.DocumentVersion = "1.01";
            pro.Header.MerchantIdentifier = Shop.SellerID;
            pro.MessageType = "Product";
            pro.PurgeAndReplace = "false";
            pro.Message = new List<Message>();
            int i = 1;
            foreach (var product in Tp.Products)
            {
                string OperationType = product.ProductType ? "Update" : "";
                if (OperationType.IsNullOrEmpty()) continue;
                Message message = new Message();
                message.MessageID = i.ToString();
                message.OperationType = OperationType;
                message.Product = new Message.Cls_Product();
                message.Product.SKU = product.Sku;
                message.Product.StandardProductID = new Message.Cls_Product.Cls_StandardProductID();
                message.Product.StandardProductID.Type = "ASIN";
                message.Product.StandardProductID.Value = product.Asin;
                message.Product.Condition = new Message.Cls_Product.Cls_Condition();
                message.Product.Condition.ConditionType = product.ProductState;
                message.Product.DescriptionData = new Message.Cls_Product.Cls_DescriptionData();
                message.Product.DescriptionData.Title = product.Title;
                message.Product.DescriptionData.CountryOfOrigin = "CN";
                pro.Message.Add(message);
                i++;
            }
            if (pro.Message.Count == 0)
            {
                ErrMessage = "NoMessage";
                return null;
            }
            string xml = GetOtherXml(pro);
            SaveFile(xml, SessionType.PRODUCT);
            INF_Mws.Result UploadProInfo = MWS.Upload(xml, "_POST_PRODUCT_DATA_", Shop, out string ProFeedSubmissionId, ref ErrMessage, Tp.Log);
            if (UploadProInfo.Equals(INF_Mws.Result.Fail)) return null;
            return ProFeedSubmissionId;
        }
        private string InvokeUploadFollowUpPrice(FollowUpTaskParameter Tp, ref string ErrMessage)
        {
            var shopInfo = Tp.Shop;
            Imp_ShopInfo Shop = new Imp_ShopInfo(shopInfo.MarketPlace, shopInfo.SellerID, shopInfo.MwsAuthToken, shopInfo.AwsAccessKey, shopInfo.SecretKey);
            AmazonEnvelope ProPrice = new AmazonEnvelope();
            ProPrice.Header = new AmazonEnvelope.Cls_Header();
            ProPrice.Header.DocumentVersion = "1.01";
            ProPrice.Header.MerchantIdentifier = Shop.SellerID;
            ProPrice.MessageType = "Price";
            ProPrice.Message = new List<Message>();
            int i = 1;
            foreach (var SinglePro in Tp.Products)
            {
                string OperationType = SinglePro.PriceType ? "Update" : "";
                if (OperationType.IsNullOrEmpty()) continue;
                Message message = new Message();
                message.MessageID = i.ToString();
                message.OperationType = OperationType;
                message.Price = new Message.Cls_Price();
                message.Price.SKU = SinglePro.Sku;
                message.Price.StandardPrice = new Cls_StandardPrice();
                message.Price.StandardPrice.currency = SinglePro.Unit.ToString();
                message.Price.StandardPrice.PriceValue = SinglePro.Price.ToString();
                ProPrice.Message.Add(message);
                i++;
            }
            if (ProPrice.Message.Count == 0)
            {
                ErrMessage = "NoMessage";
                return null;
            }
            string xml = GetOtherXml(ProPrice);
            SaveFile(xml, SessionType.PRICING);
            INF_Mws.Result UploadProInfo = MWS.Upload(xml, "_POST_PRODUCT_PRICING_DATA_", Shop, out string ProFeedSubmissionId, ref ErrMessage, Tp.Log);
            if (UploadProInfo.Equals(INF_Mws.Result.Fail)) return null;
            string ProFeedMissionID = ProFeedSubmissionId;
            return ProFeedMissionID;
        }
        private string InvokeUploadFollowUpInventory(FollowUpTaskParameter Tp, ref string ErrMessage)
        {
            var shopInfo = Tp.Shop;
            Imp_ShopInfo Shop = new Imp_ShopInfo(shopInfo.MarketPlace, shopInfo.SellerID, shopInfo.MwsAuthToken, shopInfo.AwsAccessKey, shopInfo.SecretKey);
            AmazonEnvelope ProInventory = new AmazonEnvelope();
            ProInventory.Header = new AmazonEnvelope.Cls_Header();
            ProInventory.Header.DocumentVersion = "1.01";
            ProInventory.Header.MerchantIdentifier = Shop.SellerID;
            ProInventory.MessageType = "Inventory";
            ProInventory.Message = new List<Message>();
            int i = 1;
            foreach (var SinglePro in Tp.Products)
            {
                string OperationType = SinglePro.InventoryType ? "Update" : "";
                if (OperationType.IsNullOrEmpty()) continue;
                Message message = new Message();
                message.MessageID = i.ToString();
                message.OperationType = OperationType;
                message.Inventory = new Message.Cls_Inventory();
                message.Inventory.SKU = SinglePro.Sku;
                message.Inventory.Quantity = SinglePro.Stock.ToString();
                int FulfillmentLatency = SinglePro.FulfillmentLatency;
                if (FulfillmentLatency <= 0) FulfillmentLatency = 2;
                message.Inventory.FulfillmentLatency = FulfillmentLatency.ToString();
                message.Inventory.SwitchFulfillmentTo = "MFN";
                ProInventory.Message.Add(message);
                i++;
            }
            if (ProInventory.Message.Count == 0)
            {
                ErrMessage = "NoMessage";
                return null;
            }
            string xml = GetOtherXml(ProInventory);
            SaveFile(xml, SessionType.INVENTORY);
            INF_Mws.Result UploadProInfo = MWS.Upload(xml, "_POST_INVENTORY_AVAILABILITY_DATA_", Shop, out string ProFeedSubmissionId, ref ErrMessage, Tp.Log);
            if (UploadProInfo.Equals(INF_Mws.Result.Fail)) return null;
            string ProFeedMissionID = ProFeedSubmissionId;
            return ProFeedMissionID;
        }
        private bool FollowUpIsFinished = false;
        private bool InvokeFollowUpStatus(FollowUpTaskParameter Tp, FollowUpSessionType SessionType, ref string ErrMessage)
        {
            string GetProFeedMissionID = Tp.GetSessionID(SessionType);
            if (GetProFeedMissionID.IsNullOrEmpty())
            {
                ErrMessage = "FeedMissionID";
                Tp.Finish(false, SessionType, I18N.Instance.MWS_Log70);
                //$"EAN重复,请修改EAN后重试,{SessionType}_ErrNum:#1408"
                FollowUpIsFinished = true;
                return false;
            }
            //店铺信息
            var shopInfo = Tp.Shop;
            Imp_ShopInfo Shop = new Imp_ShopInfo(shopInfo.MarketPlace, shopInfo.SellerID, shopInfo.MwsAuthToken, shopInfo.AwsAccessKey, shopInfo.SecretKey);
            //执行跟踪上传逻辑
            string UpStatus = MWS.UpdateResult(GetProFeedMissionID, Shop, ref ErrMessage);
            if (UpStatus == "_DONE_")
            {
                Tp.SetSession(SessionType, SessionState.Down, "");
                return InvokeFollowUpDown(Tp, SessionType);
            }
            else if (UpStatus == "_IN_PROGRESS_")
            {
                if (Tp.GetSessionState(SessionType) != SessionState.Processing) Tp.SetSession(SessionType, SessionState.Processing, "");
            }
            else if (UpStatus == null && ErrMessage == "AccessDenied")
            {
                Tp.Finish(false, SessionType, I18N.Instance.MWS_Log2);//店铺被封或拒绝访问或者网络因素导致的上传失败
                FollowUpIsFinished = true;
                return false;
            }
            return true;
        }
        /// <summary>
        /// False:任务失败或者未结束
        /// </summary>
        /// <param name="Tp"></param>
        /// <param name="SessionType"></param>
        /// <returns></returns>
        private bool InvokeFollowUpDown(FollowUpTaskParameter Tp, FollowUpSessionType SessionType)
        {
            string ProFeedMissionID = Tp.GetSessionID(SessionType);
            //店铺信息
            var shopInfo = Tp.Shop;
            Imp_ShopInfo Shop = new Imp_ShopInfo(shopInfo.MarketPlace, shopInfo.SellerID, shopInfo.MwsAuthToken, shopInfo.AwsAccessKey, shopInfo.SecretKey);
            string ErrMessage = "";
            Dictionary<string, string> ErrDic = MWS.GetSubmitResult(ProFeedMissionID, Shop, ref ErrMessage);
            if (ErrDic.IsNull())
            {
                //查询失败(网络请求原因)
                if (ErrMessage.IsNullOrEmpty())
                {
                    ErrMessage = I18N.Instance.MWS_Log50;//I18N.Instance.MWS_Log2
                }
                FollowUpIsFinished = true;
                Tp.Finish(false, SessionType, ErrMessage);
                return false;
            }
            else if (ErrDic.Count == 0 && ErrMessage.IsNotWhiteSpace() && ErrMessage.StartsWith("AllError:"))
            {
                ErrMessage = ErrMessage.Replace("AllError:", "");
                Tp.Products.ForEach((r) =>
                {
                    r.ProductReportError(ErrMessage, SessionType);
                });
            }
            else if (ErrDic.Count > 0)
            {
                //根据Sku上报失败信息
                foreach (var Err in ErrDic)
                {
                    //定位到任务中哪个产品出错
                    Tp.Products.ForEach((r) =>
                    {
                        if (Err.Key == r.Sku)
                        {
                            string ErrMsg = Err.Value.IsNullOrEmpty() ? " " : Err.Value;
                            r.ProductReportError(ErrMsg, SessionType);
                        }
                    });
                }
            }
            Tp.SetSession(SessionType, SessionState.Complete, "");

            for (int i = 0; i <= 2; i++)
            {
                if (Tp.GetSessionState((FollowUpSessionType)i) != SessionState.Complete)
                {
                    return false;//未结束直接再次轮询
                }
            }
            //所有都已经Complete
            //表示当前任务已结束（上传xml成功的,与GetResult无关的）
            FollowUpIsFinished = true;
            Tp.Finish(true, SessionType, "");
            return true;
        }
        public List<IGetMatchingProduct> GetProductInfo(ProductParameter PP)
        {
            List<IGetMatchingProduct> Products = new List<IGetMatchingProduct>();
            string action = "GetMatchingProduct";
            OrderParameter OP = new OrderParameter(PP.Shop, DateTime.Now, DateTime.Now, PP.Log, null, null);
            Sy.String xml = MWS.Gethtml(OP, action, "", PP.Asins);
            string SpliterProduct = "<GetMatchingProductResult";
            if (xml.Contains("AccessDenied"))
            {
                PP.Log(I18N.Instance.MWS_Log19);
                return null;
            }
            else if (xml.ToLower().Contains("badproxy"))
            {
                PP.Log(I18N.Instance.MWS_Log20);
                return null;
            }
            else if (!xml.Contains(SpliterProduct))
            {
                PP.Log(I18N.Instance.MWS_Log71);//未获取到商品数据,请检查Asin是否正确
                return null;
            }
            action = "GetCompetitivePricingForASIN";
            Sy.String PriceXml= MWS.Gethtml(OP, action, "", PP.Asins);
            Dictionary<string, Currency> AsinPrice = GetLowestPrice(PriceXml, PP);
            //if (AsinPrice.IsNull() || AsinPrice.Count == 0)
            //{
            //    PP.Log(I18N.Instance.MWS_Log72);//获取最低价失败!
            //    return null;
            //}
            var ProductPages = xml.Split(SpliterProduct).Skip(1);
            string SpliterAsinS = "ASIN=\"", SpliterStatusS = "status=\"", SpliterE1 = "\"", SpliterBrandS = "<ns2:Brand>", SpliterManufacturerS = "<ns2:Manufacturer>", SpliterUrlS = "<ns2:URL>", SpliterTitle = "<ns2:Title>", SpliterE2 = "<";
            foreach (var Page in ProductPages)
            {
                string Asin = Imp_Common.Instance.GetMidValue(Page, SpliterAsinS, SpliterE1);
                string Statues= Imp_Common.Instance.GetMidValue(Page, SpliterStatusS, SpliterE1);
                if (Statues == "Success")
                {
                    string Brand = Imp_Common.Instance.GetMidValue(Page, SpliterBrandS, SpliterE2);
                    string Manufacturer = Imp_Common.Instance.GetMidValue(Page, SpliterManufacturerS, SpliterE2);
                    string Url = Imp_Common.Instance.GetMidValue(Page, SpliterUrlS, SpliterE2);
                    string Title = Imp_Common.Instance.GetMidValue(Page, SpliterTitle, SpliterE2);
                    if (!Title.IsNullOrEmpty() && !Asin.IsNullOrEmpty())
                    {
                        string Price = "0", Unit = "CNY", Shipping = "0";
                        if (AsinPrice.ContainsKey(Asin))
                        {
                            Price = AsinPrice[Asin].Price;
                            Shipping = AsinPrice[Asin].Shipping;
                            Unit = AsinPrice[Asin].Unit;
                        }
                        GetMatchingProduct Product = new GetMatchingProduct(PP.StoreId, PP.Shop.MarketPlace.ID, Title, Asin, Price, Shipping, Brand, Manufacturer, Url, Unit);
                        Products.Add(Product);
                    }
                }
            }
            return Products;
        }
        public class Currency
        {
            public Currency() { }
            public Currency(string price, string shipping, string unit)
            {
                Price = price;
                Shipping = shipping;
                Unit = unit;
            }

            public string Price { get; set; }
            public string Shipping { get; set; }
            public string Unit { get; set; }
        }
        private Dictionary<string, Currency> GetLowestPrice(Sy.String xml, ProductParameter PP)
        {
            string SpliterProduct = "<GetCompetitivePricingForASINResult";
            if (!xml.Contains(SpliterProduct))
            {
                PP.Log(I18N.Instance.MWS_Log73);//未获取到商品价格数据,请检查Asin是否正确
                return null;
            }
            Dictionary<string, Currency> AsinPrice = new Dictionary<string, Currency>();
            var ProductPages = xml.Split(SpliterProduct).Skip(1);
            string SpliterAsinS = "ASIN=\"", SpliterStatusS = "status=\"", SpliterE1 = "\"", SpliterListPriceS = "<ListingPrice>", SpliterListPriceE = "</ListingPrice>", SpliterAmountS = "<Amount>", SpliterAmountE = "</Amount>", SpliterCurrencyS = "<CurrencyCode>", SpliterCurrencyE = "</CurrencyCode>", SpliterShippingS = "<Shipping>", SpliterShippingE = "</Shipping>";
            foreach (var Page in ProductPages)
            {
                string Asin = Imp_Common.Instance.GetMidValue(Page, SpliterAsinS, SpliterE1);
                string Statues = Imp_Common.Instance.GetMidValue(Page, SpliterStatusS, SpliterE1);
                if (!Asin.IsNullOrEmpty() && Statues == "Success")
                {
                    string Shipping = "0";
                    var ShippingPage = Imp_Common.Instance.GetMidValue(Page, SpliterShippingS, SpliterShippingE);
                    if (!ShippingPage.IsNullOrEmpty())
                    {
                        if (ShippingPage.CanMid(SpliterAmountS, SpliterAmountE)) Shipping = ShippingPage.Mid(SpliterAmountS, SpliterAmountE);
                    }
                    var ListPrice = Imp_Common.Instance.GetMidValue(Page, SpliterListPriceS, SpliterListPriceE);
                    if (!ListPrice.IsNullOrEmpty())
                    {
                        string Unit = "", Price = "";
                        if (ListPrice.CanMid(SpliterCurrencyS, SpliterCurrencyE)) Unit = ListPrice.Mid(SpliterCurrencyS, SpliterCurrencyE);
                        if (ListPrice.CanMid(SpliterAmountS, SpliterAmountE)) Price = ListPrice.Mid(SpliterAmountS, SpliterAmountE);
                        if (Asin.IsNotWhiteSpace() && Price.IsNotWhiteSpace() && Unit.IsNotWhiteSpace() && !AsinPrice.ContainsKey(Asin)) AsinPrice.Add(Asin, new Currency(Price, Shipping, Unit));
                    }
                }
            }
            return AsinPrice;
        }
        #endregion
        #region 验证店铺
        public string VerifyStore(VerifyStoreParameter VP)
        {
            var Log = Mod_GlobalFunction.GetLoger(VP.Log);
            OrderParameter OP = new OrderParameter(VP.Shop, DateTime.Now, DateTime.Now, Log, null, null);
            Sy.String xml = MWS.Gethtml(OP, "ListMarketplaceParticipations");
            if (xml.Contains("<ErrorResponse"))//"AccessDenied"
            {
                try
                {
                    using (StringReader sr = new StringReader(xml))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(SellerErrorResponse));
                        SellerErrorResponse ErRe = serializer.Deserialize(sr) as SellerErrorResponse;
                        return JsonObject.Serialize(new VerifyReport(VP.ID, false, $"[{ErRe.Error.Code}]{ErRe.Error.Message}"));
                    }
                }
                catch
                {
                    //网络因素请求失败
                    return JsonObject.Serialize(new VerifyReport(VP.ID, false, I18N.Instance.MWS_Log50));
                }
            }
            else if (xml.ToLower().Contains("badproxy"))
            {
                return JsonObject.Serialize(new VerifyReport(VP.ID, false, I18N.Instance.MWS_Log20));
            }
            var StoreInfo = new ListMarketplaceParticipationsResponse();
            try
            {
                using (StringReader sr = new StringReader(xml))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ListMarketplaceParticipationsResponse));
                    StoreInfo = serializer.Deserialize(sr) as ListMarketplaceParticipationsResponse;
                }
            }
            catch
            {
                return JsonObject.Serialize(new VerifyReport(VP.ID, false, I18N.Instance.MWS_Log75));
            }
            if (StoreInfo.ListMarketplaceParticipationsResult.IsNotNull() && StoreInfo.ListMarketplaceParticipationsResult.ListParticipations.IsNotNull() && StoreInfo.ListMarketplaceParticipationsResult.ListParticipations.Participation.IsNotNull())
            {
                var Stores = StoreInfo.ListMarketplaceParticipationsResult.ListParticipations.Participation;
                foreach (var s in Stores)
                {
                    if (s.MarketplaceId == VP.Shop.MarketPlace.ID)
                    {
                        VP.UploadServer();
                        return JsonObject.Serialize(new VerifyReport(VP.ID, true, ""));
                    } 
                }
            }
            return JsonObject.Serialize(new VerifyReport(VP.ID, false, I18N.Instance.MWS_Log76));
        }
        #endregion
        #region 上传发票
        public bool UploadInvoice(UploadInvoiceParameter UP)
        {
            //获取PDF文件数据
            var pdfDocument = GetInvoicePDF(UP);//, ref AllTotal, ref AllTotalTax
            if (pdfDocument.IsNullOrEmpty())
            {
                UP.UploadServer(false, I18N.Instance.MWS_Log77);
                return false;
            }
            //FeedOptions
            Dictionary<string, string> FeedOption = new Dictionary<string, string>();
            FeedOption.Add("metadata:OrderId", UP.Order.order_no);
            //FeedOption.Add("metadata:totalAmount", AllTotal.ToString());
            //FeedOption.Add("metadata:totalvatamount", AllTotalTax.ToString());
            FeedOption.Add("metadata:InvoiceNumber", Mod_GlobalFunction.GetTimeStamp(Mod_GlobalFunction.TimestampMode.Second).ToString());
            string Option = "";
            FeedOption.ForEach((r) => Option += $"{r.Key}={r.Value};");
            Option += "metadata:documenttype=Invoice;metadata:transfer-encoding:chunked;data:application/pdf;base64";
            string ErrMessage = "";
            INF_Mws.Result UploadInvoice = MWS.Upload(pdfDocument, "_UPLOAD_VAT_INVOICE_", UP.Shop, Option, out string FeedSubmissionId, ref ErrMessage, UP.Log);
            if (UploadInvoice.Equals(INF_Mws.Result.Fail)) 
            {
                //上报错误
                UP.UploadServer(false, ErrMessage);
                return false;
            }
            return CheckInvoice(UP, FeedSubmissionId);
        }

        private bool CheckInvoice(UploadInvoiceParameter UP, string FeedMissionID)
        {
            System.Threading.Thread.Sleep(30 * 1000);
            string ErrMessage = "";
            Imp_ShopInfo Shop = new Imp_ShopInfo(UP.Shop.MarketPlace, UP.Shop.SellerID, UP.Shop.MwsAuthToken, UP.Shop.AwsAccessKey, UP.Shop.SecretKey);
            string UpStatus = MWS.UpdateResult(FeedMissionID, Shop, ref ErrMessage);
            if (UpStatus == "_DONE_")
            {
               var Result = MWS.GetSubmitResult(FeedMissionID, Shop);
                UP.UploadServer(Result.IsNull(), Result);
                return Result.IsNull();
            }
            else if (UpStatus == "_IN_PROGRESS_")
            {
                return CheckInvoice(UP, FeedMissionID);
            }
            UP.UploadServer(false, I18N.Instance.MWS_Log2);//店铺被封或拒绝访问或者网络因素导致的上传失败
            return false;
        }
        private string GetInvoicePDF(UploadInvoiceParameter UP)//, ref double AllTotal, ref double AllTotalTax
        {
            //1.获取模板信息
            string ExtName = $"{UP.Shop.MarketPlace.ID}_Invoice";
            string FilePath = Path.Combine(Sy.Environment.AppPath, "Config", "Invoice");
            string FileName = Path.Combine(FilePath, $"{ExtName}.xlsx");
            //2.创建保存路径
            string SaveFloder = Path.Combine(Sy.Environment.AppPath, "Config", "TempInvoice");
            string NewExtName = $"Invoice_{UP.Order.order_no}";
            string SaveFilePath= Path.Combine(SaveFloder, $"{NewExtName}.xlsx");
            string SavePDFPath= Path.Combine(SaveFloder, $"{NewExtName}.pdf");
            if (!Directory.Exists(SaveFloder)) Directory.CreateDirectory(SaveFloder);
            //3.写入文件
            if (File.Exists(FileName))
            {
                try
                {
                    XSSFWorkbook IWorkbook = null;
                    using (var fs = new FileStream(FileName, FileMode.Open, FileAccess.Read))
                    {
                        IWorkbook = new XSSFWorkbook(fs);
                    }
                    var Sheet = IWorkbook.GetSheetAt(0);
                    //订单号+卖家信息
                    var Row5 = Sheet.GetRow(4);
                    Row5.GetCell(0).SetCellValue(UP.Order.order_no);
                    string Address = "";
                    if (UP.Order.default_ship_from_location_address.IsNotNull())
                    {
                        var ShipAddress = UP.Order.default_ship_from_location_address;
                        if (ShipAddress.AddressLine1.IsNotWhiteSpace()) Address += ShipAddress.AddressLine1;
                        if (ShipAddress.AddressLine2.IsNotWhiteSpace())
                        {
                            if (Address.Length > 0) Address += ",";
                            Address += ShipAddress.AddressLine2;
                        }
                        if (ShipAddress.AddressLine3.IsNotWhiteSpace())
                        {
                            if (Address.Length > 0) Address += ",";
                            Address += ShipAddress.AddressLine3;
                        }
                        Address += $",{ShipAddress.StateOrRegion}";
                        Address += $",{ShipAddress.City}";
                        Address += $",{ShipAddress.CountryCode}";
                    }
                    Row5.GetCell(6).SetCellValue(Address);
                    //收件人信息
                    var Row7 = Sheet.GetRow(6);
                    Row7.GetCell(1).SetCellValue(UP.Order.receiver_address);
                    var Row8 = Sheet.GetRow(7);
                    Row8.GetCell(1).SetCellValue(UP.Order.receiver_address2);
                    var Row9 = Sheet.GetRow(8);
                    Row9.GetCell(1).SetCellValue(UP.Order.receiver_address3);
                    //日期
                    var Row15 = Sheet.GetRow(14);
                    Row15.GetCell(6).SetCellValue(DateTime.Now.ToString("yyyy-M-dd"));
                    double AllTotal = 0;//总费用和=单价和+运费和
                    double AllProTotal = 0;//单价总和
                    double AllProTax = 0;//单价和
                    double AllShipTotla = 0;
                    double AllShipTax = 0;//运费和
                    string Unit = "";
                    //产品信息
                    for (int i = 0; i <= UP.Order.Products.Count - 1; i++)
                    {
                        var Pro = UP.Order.Products[i];
                        Unit = Pro.unit;
                        AllTotal += Pro.total * Pro.Num;
                        AllProTotal = Pro.price * Pro.Num;
                        AllProTax += Pro.item_tax * Pro.Num;
                        AllShipTotla += Pro.amazon_shipping_money * Pro.Num;
                        AllShipTax += Pro.shipping_tax * Pro.Num;
                        if (i == 0)
                        {
                            var RowPro = Sheet.GetRow(19 + i);
                            RowPro.GetCell(0).SetCellValue(i.ToString());
                            RowPro.GetCell(1).SetCellValue(Pro.Num);
                            RowPro.GetCell(2).SetCellValue(Pro.seller_sku);//sku
                            RowPro.GetCell(3).SetCellValue(Pro.Name);
                            RowPro.GetCell(5).SetCellValue($"{Pro.unit}{GetPrice(Pro.total, UP)}");//单价
                            RowPro.GetCell(7).SetCellValue($"{Pro.unit}{GetPrice(Pro.total * Pro.Num, UP)}");//总价
                        }
                        else
                        {
                            var RowPro = Sheet.CreateRow(19 + i);
                            RowPro.GetCell(0).SetCellValue(i.ToString("000"));
                            RowPro.GetCell(1).SetCellValue(Pro.Num);
                            RowPro.GetCell(2).SetCellValue(Pro.seller_sku);//sku
                            RowPro.GetCell(3).SetCellValue(Pro.Name);
                            RowPro.GetCell(5).SetCellValue($"{GetPrice(Pro.total, UP)}");
                            RowPro.GetCell(7).SetCellValue($"{GetPrice(Pro.total * Pro.Num, UP)}");
                        }
                    }
                    //(AllTotal * TaxRate[UP.Shop.MarketPlace.ID]).ToString("0.00");
                    //单价总和-单价的税费
                    var RowAllPro = Sheet.GetRow(19 + UP.Order.Products.Count);
                    RowAllPro.GetCell(7).SetCellValue($"{Unit}{GetPrice(AllProTotal - AllProTax, UP)}");
                    //运费-运费的税费
                    var RowFee = Sheet.GetRow(19 + UP.Order.Products.Count + 1);
                    RowFee.GetCell(7).SetCellValue($"{Unit}{GetPrice(AllShipTotla - AllShipTax, UP)}");
                    //总税费
                    var RowTax = Sheet.GetRow(19 + UP.Order.Products.Count + 3);
                    RowTax.GetCell(7).SetCellValue($"{Unit}{GetPrice(AllShipTax, UP)}");//税费
                    //发票金额
                    var RowInvoice = Sheet.GetRow(19 + UP.Order.Products.Count + 4);
                    RowInvoice.GetCell(7).SetCellValue($"{Unit}{GetPrice(AllTotal, UP)}");
                    using (var fs = File.OpenWrite(SaveFilePath))
                    {
                        IWorkbook.Write(fs);
                    }
                    //保存为PDF
                    var workbook = new Aspose.Cells.Workbook(SaveFilePath);
                    workbook.Save(SavePDFPath, Aspose.Cells.SaveFormat.Pdf);

                    //Workbook wb = new Workbook();
                    //wb.LoadFromFile(SaveFilePath);
                    //wb.SaveToFile(SavePDFPath, FileFormat.PDF);
                    var pdfDocument = File.ReadAllText(SavePDFPath);
                    //读完之后删掉
                    File.Delete(SaveFilePath);
                    //File.Delete(SavePDFPath);
                    return pdfDocument;
                }
                catch (Exception ex)
                {
                    UP.Log($"{I18N.Instance.MWS_Log78}:{ex.Message}");
                    return null;
                }
            }
            else
            {
                UP.Log(I18N.Instance.MWS_Log79);
                return null;
            }
        }
        /// <summary>
        /// 修改价格数据,欧洲除英国外,价格的.都是,
        /// </summary>
        /// <param name="Price"></param>
        /// <param name="Marketplace"></param>
        /// <returns></returns>
        private string GetPrice(double Price, UploadInvoiceParameter UP)
        {
            string Marketplace = UP.Shop.MarketPlace.ID;
            if (Marketplace == "A1F83G8C2ARO7P") return Price.ToString();
            return Price.ToString().Replace(".", ",");
        }
        #endregion 
    }
}