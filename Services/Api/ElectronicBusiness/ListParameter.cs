using ERP.Models.Stores;
using ERP.Services.DB.Operations;

namespace ERP.Services.Api.ElectronicBusiness;

public record ListParameter
{
    public StoreRegion store;
    public DateTime Start;
    public DateTime End;
    public OperationService? OperationsService = null;
    //public OperationsLog.ReportOperationsLogHandle? reportOperationsLogHandle = null;
    /*
        await OLog.Add(new(TypeModuels.Amazom, Enums.Operations.Results.Error, StartTime, DateTime.Now, $"Result:{ResultMessage}\r\nrunTimeInfo:{runTimeInfo} "));
        await OLog.Add(new(TypeModuels.Amazom, Enums.Operations.Results.Success, StartTime, DateTime.Now, $"Result:{ResultMessage}\r\nrunTimeInfo:{runTimeInfo} ")); 
     */

    public ListParameter(StoreRegion store, DateTime start, DateTime end)  //
    {
        this.store = store;       
        Start = start;
        End = end;
    }

    //, List<StoreMarket> markets
    public ListParameter(StoreRegion store, DateTime start, DateTime end, OperationService OLog)
        : this(store, start, end)
    {
        //this.reportOperationsLogHandle = reportOperationsLogHandle;
        OperationsService = OLog;
    }
}
