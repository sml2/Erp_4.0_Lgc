﻿using ERP.Interface;
using Newtonsoft.Json;

namespace ERP.Services.Provider;

using OemCache = Caches.OEM;
using Model = Models.Setting.OEM;

public class OemProvider : IOemProvider
{
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly IHostEnvironment _environment;
    private readonly OemSetter _oemSetter;
    private readonly ILogger<OemProvider> _logger;
    private readonly IServiceProvider _serviceProvider;

    private OemCache Cache => _serviceProvider.GetRequiredService<OemCache>();

    public OemProvider(IHttpContextAccessor httpContextAccessor, IServiceProvider serviceProvider, IHostEnvironment environment,
        OemSetter oemSetter, ILogger<OemProvider> logger)
    {
        _httpContextAccessor = httpContextAccessor;
        _environment = environment;
        _oemSetter = oemSetter;
        _logger = logger;
        _serviceProvider = serviceProvider;
    }

    private Model? _oem;

    Model? IOemProvider.OEM
    {
        get
        {
            if (_oem is not null) return _oem;

            // 获取执行后台任务时手动设置的oem, 使用IServiceProvider.CreateScopeWithOem()设置
            if (_oemSetter.Oem is not null)
                return _oem = _oemSetter.Oem;

            var http = _httpContextAccessor.HttpContext;
            // if (http is not null)
            // {
            //     var domain = http.Request.Host.Host.Split('.').First();
            //     var Oem = cache.Get(domain!);
            //     if (Oem is not null)
            //         _OEM = Oem;
            //     else
            //         throw new NullReferenceException(nameof(Oem));
            // }

            if (http is null)
            {
                _logger.LogCritical("代码不应该执行到此处, 出现该日志意味着可能执行在后台任务线程中, 需要设置上面的OemSetter");
                return null;
            }

            if (!_environment.IsProduction())
            {
                return _oem = Cache.List()?.Where(m => m.ID == (int)Enums.ID.OEM.MiJingTong).First();
            }

            var domainHashcode = Helpers.CreateHashCode(http.Request.Host.Host);
            _logger.LogInformation($"Host: [{http.Request.Host}]");
            _logger.LogInformation($"Host.Host: [{http.Request.Host.Host}]");
            _logger.LogInformation($"DomainHashcode: [{domainHashcode}]");
            _logger.LogInformation($"X-Forwarded-For:{http.Request.Headers["X-Forwarded-For"].ToString()},IP：{http.Request.HttpContext.Connection.RemoteIpAddress?.MapToIPv4()}");
            var oem = Cache.Get(domainHashcode);
            return _oem = oem ?? throw new NullReferenceException(nameof(oem));
        }
    }
}