﻿using ERP.Models.Setting;

namespace ERP.Services.Provider;

/// <summary>
/// 用于后台任务手动设置全局oem, 避免<see cref="OemProvider"/>获取oem为null, 一个scoped周期内只能被设置一次
/// </summary>
public class OemSetter
{
    private Models.Setting.OEM? _oem;
    
    /// <summary>
    /// 判断oem值是否被访问过
    /// </summary>
    private bool _isVisited;

    public Models.Setting.OEM? Oem
    {
        get
        {
            _isVisited = true;
            return _oem;
        }
        set
        {
            if (_oem is not null)
                throw new Exception("oem在一个生命周期内只能设置一次");

            if (_isVisited)
                throw new Exception("oem值在生命周期内已被访问过, 不应该再次赋值, 需要检查代码确保在第一次访问前赋值");
            
            _oem = value ?? throw new ArgumentNullException(nameof(value), "需要手动设置的oem不能为空");
        }
    }
}

public static class OemSetterExtension
{
    /// <summary>
    /// 创建一个指定oem的服务提供器
    /// </summary>
    /// <param name="sp"></param>
    /// <param name="oem"></param>
    /// <returns></returns>
    public static IServiceScope CreateScopeWithOem(this IServiceProvider sp, OEM oem)
    {
        var scoped = sp.CreateScope();
        var oemSetter = scoped.ServiceProvider.GetRequiredService<OemSetter>();
        oemSetter.Oem = oem;
        return scoped;
    }
}