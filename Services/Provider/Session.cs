﻿using ERP.Interface;

namespace ERP.Services.Provider;
public class Session : ISessionProvider
{
    private readonly IHttpContextAccessor httpContextAccessor;

    public Session(IHttpContextAccessor httpContextAccessor)
    {
        this.httpContextAccessor = httpContextAccessor;
    }
    ISession ISessionProvider.Session => httpContextAccessor.HttpContext!.Session;
}
