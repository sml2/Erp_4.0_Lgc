﻿namespace ERP.Services.Rule;

using System.Collections.Generic;
using ERP.Authorization;
using ERP.Enums.Identity;
using Models.DB.Setting;
using Models.DB.Users;
using Model = Models.Rule.Config;

public class Manager : Manager<RuleTemplate>
{
    public Manager(ILogger<Manager<RuleTemplate>> logger) : base(logger)
    {
    }
    public Model? Get(Menu menu) => Get(menu.ID);
    public override Model? Get<T>(T t) { 
        if(t is Menu menu)
            return Get(menu.ID);
        throw new NotSupportedException($"{t?.GetType()}");
    }

}

