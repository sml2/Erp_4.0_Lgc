using ERP.Data.Task;

using ERP.Services.Caches;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;

namespace ERP.Services.Host;
public abstract class Invoker<T> : IHostedService, IDisposable where T : Invoker<T>
{
    const int DefaultMinInterval = 300000;
    const int DefaultQueueCount = 1;

    readonly CancellationTokenSource _shutdown = new();
    private readonly IServiceProvider _serviceProvider;
    protected readonly IServiceProvider ServiceProvider;
    private readonly ILogger<T> _logger;
    private Task[] _tasks = Array.Empty<Task>();
    public Invoker(ILogger<T> logger, IServiceProvider serviceProvider)
    {
        _logger = logger;
        _serviceProvider = serviceProvider;
        ServiceProvider = _serviceProvider.CreateScope().ServiceProvider;
    }


    /// <summary>
    /// 开启队列数量
    /// </summary>
    public virtual int QueueCount { get; set; } = DefaultQueueCount;
    public Task StartAsync(CancellationToken cancellationToken)
    {        
        _logger.LogInformation($"Queuing service [{QueueCount}] is now start.", QueueCount);
        var Name = GetType().Name;//typeof(T).Name
        var TaskList = ServiceProvider.GetRequiredService<TaskList>();
        _tasks = new Task[QueueCount];
        for (int i = 0; i < QueueCount; i++)
        {
            var index = i;
            State state = new()
            {
                ClassName = Name,
                Index = index,
                Count = QueueCount,
            };
            Warper warper = new(_logger, state);
            TaskList.Set(state);
            _tasks[i] = Task.Run(() => InvokeInternal(index, warper), default);
        }
        _logger.LogInformation($"Queuing service [{QueueCount}] is now started.", QueueCount);
        return Task.CompletedTask;
    }

    /// <summary>
    /// 最小执行间隔(ms)
    /// </summary>
    public virtual int MinInterval { get; } = DefaultMinInterval;
    private async Task InvokeInternal(int index, Warper logger)
    {
        logger.LogInformation($"Queuing service[{index}/{QueueCount}] is now working.", index, QueueCount);
        logger.taskState.StartTime = DateTime.Now;
        while (!_shutdown.IsCancellationRequested)
        {
            ExecuteSpend executeSpend = new();
            try
            {
                await Invoke(index, logger, _shutdown.Token);
            }
            catch (Exception ex)
            {
                logger.LogError($"Queuing service[{index}/{QueueCount}] - {ex}", index, QueueCount, ex);
            }
            executeSpend.End();
            logger.taskState.AddExecuteSpend(executeSpend);
            var sleep = Math.Max(MinInterval - (int)(executeSpend.SpendTime).TotalMilliseconds, 0);
            logger.LogTrace($"Queuing service[{index}/{QueueCount}] Delay:{sleep}", index, QueueCount, sleep);
            await Task.Delay(sleep, _shutdown.Token);
        }
        _logger.LogInformation($"Queuing service[{index}/{QueueCount}] is now work over.", index, QueueCount);
    }
    public abstract Task Invoke(int Index, ILogger logger);
    public virtual Task Invoke(int Index, ILogger logger, CancellationToken token) => Invoke(Index, logger);

    public Task StopAsync(CancellationToken cancellationToken)
    {
        _shutdown.Cancel();
        _logger.LogInformation("Queuing service[{QueueCount}] is now stopping.", QueueCount);
        while (_tasks.Any(t => !t.IsCompleted))
        {
            try
            {
                Task.WaitAll(_tasks);
            }
            catch (AggregateException e)
            {
                if (e.InnerExceptions.Any(ex => ex is not TaskCanceledException))
                    throw;
            }
        }
        return Task.CompletedTask;
    }

    public void Dispose()
    {
        _logger.LogInformation($"Queuing service[{QueueCount}] is now stopped.", QueueCount);
        GC.SuppressFinalize(this);
    }

}