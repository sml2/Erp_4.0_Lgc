using ERP.Enums.Orders;
using ERP.Services.Stores;
using AsinProducrService = ERP.Services.DB.Stores.AsinProduct;
using Task = System.Threading.Tasks.Task;
using ERP.Services.Api;
namespace ERP.Services.Host.OrderNew;

/// <summary>
/// 拉取订单列表
/// </summary>
public class OrderItem : Invoker<OrderItem>
{
    public override int QueueCount => Convert.ToInt32(_configuration.GetSection("OrderTaskNum").Value.Trim());
    public override int MinInterval => 60000;

    private readonly IOrderFactory _ApiOrderFactory;
    private readonly IConfiguration _configuration;

    public OrderItem(ILogger<OrderItem> logger, IConfiguration configuration, IOrderFactory orderFactory, IServiceProvider serviceProvider) : base(logger, serviceProvider)
    {
        _ApiOrderFactory = orderFactory;
        _configuration = configuration;
    }

    public override async Task Invoke(int index, ILogger _logger)
    {
        const int ItemCount = 50;
        try
        {
            var ServiceProvider = this.ServiceProvider.CreateScope().ServiceProvider;           
            var StoreService = ServiceProvider.GetRequiredService<ERP.Services.Stores.StoreService>();
            var AmazonService = ServiceProvider.GetRequiredService<DB.Orders.Order>();
            var AsinService = ServiceProvider.GetRequiredService<AsinProducrService>();
            var _dbContext = ServiceProvider.GetRequiredService<Data.DBContext>();

            var LstNeedPull = await StoreService.GetNeedOrderItemPull(PullOrderState.Item, m => m.ID % QueueCount == index);
            if (LstNeedPull.IsNull() || LstNeedPull!.Count <= 0)
            {
                _logger.LogInformation($"OrderItems:have no stores！");
                return;
            }
            _logger.LogInformation($"OrderItems:storeIDs：{string.Join(",", LstNeedPull!.Select(x => x.ID).ToList())}");

            foreach (var store in LstNeedPull!)
            {
                var marketLst = await StoreService.GetAllMarketInfoByHash(store.UniqueHashCode);
                if (marketLst.IsNull() || marketLst!.Count <= 0)
                {
                    _logger.LogInformation($"OrderItems:{store.ID}【{store.Name}】no markets");
                    continue;
                }
                store.MarketPlaces = marketLst!.Select(x => x.Marketplace).ToList();
                var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);

                var InPullOrders = AmazonService.GetInPullOrders(store.ID, x => x.pullOrderState == PullOrderState.Item, ItemCount);
                if (InPullOrders.IsNull() || InPullOrders.Count <= 0)
                {
                    _logger.LogInformation($"OrderItems:{store.ID}【{store.Name}】have no order to pull");
                    continue;
                }
                var result= await _ApiOrderServices.GetOrderItem(store, InPullOrders!, AsinService);
                if (result.Item2 == "-1")
                {
                    continue;
                }
                var AsinList = result.Item1;

                using (var transaction = await _dbContext.Database.BeginTransactionAsync())
                {
                    try
                    {
                        if (await AmazonService.UpdateOrder(InPullOrders.ToArray()) && await AsinService.Update(AsinList))
                        {
                            await _dbContext.SaveChangesAsync();
                            await transaction.CommitAsync();
                            _logger.LogInformation($"OrderItems:{store.ID}【{store.Name}】Updates Success {InPullOrders.Count()}");
                        }
                        else
                        {
                            await transaction.RollbackAsync();
                            _logger.LogError($"OrderItems:{store.ID}【{store.Name}】Updates Fail");
                        }
                    }
                    catch (Exception e)
                    {
                        await transaction.RollbackAsync();
                        _logger.LogError($"OrderItems:{store.ID}【{store.Name}】Exception:{e.Message}");
                        _logger.LogError($"OrderItems:{store.ID}【{store.Name}】Exception:{e.StackTrace}");
                    }
                }
            }
        }
        catch (Exception e)
        {
            _logger.LogError($"OrderItems Exception:{e.Message}");
            _logger.LogError($"OrderItems Exception:{e.StackTrace}");
        }
    }
}