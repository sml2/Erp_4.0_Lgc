
using ERP.Enums.Orders;
using ERP.Models.Abstract;

using ERP.Services.Caches;
using ERP.Services.DB.Orders;
using ERP.Services.Stores;
using ERP.Services.Api;
namespace ERP.Services.Host.OrderNew;

/// <summary>
/// 拉取订单列表
/// </summary>
public class OrderFinancial : Invoker<OrderFinancial>
{
    public override int QueueCount => Convert.ToInt32(_configuration.GetSection("OrderTaskNum").Value.Trim());
    public override int MinInterval => 60000;

    private readonly WaitOrders _waitOrders;
    private readonly IOrderFactory _ApiOrderFactory;
    private readonly IConfiguration _configuration;

    public OrderFinancial(ILogger<OrderFinancial> logger, IConfiguration configuration, IOrderFactory orderFactory, IServiceProvider serviceProvider) : base(logger, serviceProvider)
    {
        _ApiOrderFactory = orderFactory;
        _configuration = configuration;
    }

    public override async Task Invoke(int index, ILogger _logger)
    {
        const int ItemCount = 50;
        try
        {
            var ServiceProvider = this.ServiceProvider.CreateScope().ServiceProvider;           
            var StoreService = ServiceProvider.GetRequiredService<ERP.Services.Stores.StoreService>();
            var AmazonService = ServiceProvider.GetRequiredService<DB.Orders.Order>();
            var _dbContext = ServiceProvider.GetRequiredService<Data.DBContext>();

            var LstNeedPull = await StoreService.GetNeedOrderItemPull(PullOrderState.Financiall, m => m.ID % QueueCount == index);//@分配线程
            if (LstNeedPull.IsNull() || LstNeedPull!.Count <= 0)
            {
                _logger.LogInformation($"OrderFinanciall:have no stores！");
                return;
            }
            _logger.LogInformation($"OrderFinanciall:storeIDs：{string.Join(",", LstNeedPull!.Select(x => x.ID).ToList())}");
            foreach (var store in LstNeedPull!)
            {
                var marketLst = await StoreService.GetAllMarketInfoByHash(store.UniqueHashCode);
                if (marketLst.IsNull() || marketLst!.Count <= 0)
                {
                    _logger.LogInformation($"OrderFinanciall:{store.ID}【{store.Name}】no markets");
                    continue;
                }

                store.MarketPlaces = marketLst!.Select(x => x.Marketplace).ToList();
                var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);

                var InPullOrders = AmazonService.GetInPullOrders(store.ID, x => x.pullOrderState == PullOrderState.Financiall, ItemCount);
                if (InPullOrders.IsNull() || InPullOrders.Count <= 0)
                {
                    _logger.LogInformation($"OrderFinanciall:{store.ID}【{store.Name}】have no order to pull");
                    continue;
                }
                var result= await _ApiOrderServices.GetOrderFinanciall(store, InPullOrders!);
                if (result == "-1")
                {
                    continue;
                }
                using (var transaction = await _dbContext.Database.BeginTransactionAsync())
                {
                    try
                    {
                        if (await AmazonService.UpdateOrder(InPullOrders.ToArray()))
                        {
                            await _dbContext.SaveChangesAsync();
                            await transaction.CommitAsync();
                            _logger.LogInformation($"OrderFinanciall:{store.ID}【{store.Name}】Updates Success {InPullOrders.Count()}");
                        }
                        else
                        {
                            await transaction.RollbackAsync();
                            _logger.LogError($"OrderFinanciall:{store.ID}【{store.Name}】Updates Fail");
                        }
                    }
                    catch (Exception e)
                    {
                        await transaction.RollbackAsync();
                        _logger.LogError($"OrderFinanciall:{store.ID}【{store.Name}】Exception:{e.Message}");
                        _logger.LogError($"OrderFinanciall:{store.ID}【{store.Name}】Exception:{e.StackTrace}");                        
                    }
                }


            }

        }
        catch (Exception e)
        {
            _logger.LogError($"OrderFinanciall Exception:{e.Message}");
            _logger.LogError($"OrderFinanciall Exception:{e.StackTrace}");
        }
    }
}