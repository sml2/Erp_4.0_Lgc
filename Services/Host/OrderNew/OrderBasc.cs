

using ERP.Models.Host;
using ERP.Models.Stores;
using ERP.Services.Api;
using ERP.Services.Stores;

namespace ERP.Services.Host.OrderNew;
using Model = Models.DB.Orders.Order;
using OrderService = ERP.Services.DB.Orders.Order;

/// <summary>
/// 新拉取订单列表
/// </summary>
public class OrderBasc : Invoker<OrderBasc>
{
    public override int QueueCount => Convert.ToInt32(_configuration.GetSection("OrderBascTaskNum").Value.Trim());
    public override int MinInterval => 120000;
    /// <summary>
    /// 最大拉取间隔（秒）
    /// </summary>
    const int MaxIntervalSeconds = 900;//15分钟
    /// <summary>
    /// 评估间隔(小时)
    /// </summary>
    const int EvaluateIntervalHours = 3;//3小时

    private readonly IOrderFactory _ApiOrderFactory;

    private readonly IConfiguration _configuration;

    public OrderBasc(ILogger<OrderBasc> logger, IConfiguration configuration,IOrderFactory orderFactory, IServiceProvider serviceProvider) : base(logger, serviceProvider)
    {
        _ApiOrderFactory = orderFactory;
        _configuration = configuration;
    }

    public override async Task Invoke(int index, ILogger _logger)
    {
        var StartTime = DateTime.Now;
        try
        {
            var ServiceProvider = this.ServiceProvider.CreateScope().ServiceProvider; 
            var storeCache = ServiceProvider.GetRequiredService<Caches.Store>();
            var StoreService = ServiceProvider.GetRequiredService<StoreService>();
            var AmazonOrder = ServiceProvider.GetRequiredService<OrderService>();
            var _dbContext = ServiceProvider.GetRequiredService<ERP.Data.DBContext>();
           // var OLog = ServiceProvider.GetRequiredService<DB.Operations.OperationService>();

            //storeID对线程数量取余，指定店铺在同样的线程上运行
            var LstNeedPull = await StoreService.GetNeedPull(m => m.ID % QueueCount == index && m.NextPull < DateTime.Now);
            if (LstNeedPull != null && LstNeedPull.Count > 0)
            {
                _logger.LogInformation($"OrderBasc:所有待拉取订单列表的店铺：{string.Join(",", LstNeedPull.Select(x => x.Name).ToList())}");
                foreach (var store in LstNeedPull)
                {
                    var hasError = false;
                    var marketLst = await StoreService.GetAllMarketInfoByHash(store.UniqueHashCode);
                    if (marketLst.IsNull() || marketLst!.Count <= 0)
                    {
                        _logger.LogError($"{store.ID}【{store.Name}】 have no marketplaces");
                        continue;
                    }
                    store.MarketPlaces = marketLst!.Select(x => x.Marketplace).ToList();
                    var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);

                    //@适应业务策略
                    IOrderWork orderJob = store;
                    var Now = DateTime.Now;
                    orderJob.LastPullStart = Now;
                    if (orderJob.NextEvaluate <= Now) //判断是否需要重新评估拉取间隔
                    {
                        var MinIntervalSeconds = MinInterval / 1000 - 1;
                        //评估拉取间隔，最后取值应当[MinIntervalSeconds,MaxIntervalSeconds];
                        var IntervalSeconds = 0;
                        IntervalSeconds = Math.Min(MinIntervalSeconds, IntervalSeconds);
                        IntervalSeconds = Math.Max(MaxIntervalSeconds, IntervalSeconds);
                        orderJob.IntervalSeconds = IntervalSeconds;
                        orderJob.NextEvaluate = Now.AddHours(EvaluateIntervalHours);
                    }

                    //拉取订单
                    ListResult? listResult;
                    if (orderJob.PullProgress == PullProgresses.Pulling)
                    {
                        listResult = await _ApiOrderServices.List(new(store, orderJob.QueryEnd, orderJob.QueryEndPayload, orderJob.NextCycleInfo ?? string.Empty));                       
                        _logger.LogInformation($"OrderBasc {store.ID} 【{store.Name}】 pull orders begin {orderJob.QueryEnd.ToString("yyyy-MM-dd HH:mm:ss")} to end {orderJob.QueryEndPayload.ToString("yyyy-MM-dd HH:mm:ss")}");
                    }
                    else
                    {
                        orderJob.QueryEndPayload = Now.AddMinutes(-2);
                        listResult =await _ApiOrderServices.List(new(store, orderJob.QueryEnd, orderJob.QueryEndPayload));
                        _logger.LogInformation($"OrderBasc [{store.Name}] pull orders begin {orderJob.QueryEnd.ToString("yyyy-MM-dd HH:mm:ss")} to end {orderJob.QueryEndPayload.ToString("yyyy-MM-dd HH:mm:ss")})");
                    }
                    if(!string.IsNullOrWhiteSpace(listResult.Msg))
                    {
                        _logger.LogError($"{store.ID}【{store.Name}】 {listResult.Msg}");
                        // if (listResult.Msg.Contains("Unauthorized", StringComparison.CurrentCultureIgnoreCase))
                        // {
                        //     //未授权的改为未验证
                        //     orderJob.IsVerify = StoreRegion.IsVerifyEnum.Uncertified;
                        //     using (var transaction = await _dbContext.Database.BeginTransactionAsync())
                        //     {
                        //         await _dbContext.SaveChangesAsync();
                        //         await transaction.CommitAsync();
                        //         storeCache.Add(store);
                        var s = $"QueryEnd：{store.Name.ToString()}无法获取授权，Msg:{listResult.Msg.ToString()}";
                        _logger.LogInformation($"OrderBasc {store.ID} 【{store.Name}】 Error Job:{s}");
                        //     }
                        // }
                        continue;
                    }
                    if (listResult.HasOrder)
                    {
                        var waitOrders = listResult.waitOrders!;
                        _logger.LogInformation($"OrderBasc {store.ID} 【{store.Name}】 has {waitOrders.Count} orders pulled;");

                        List<Model> orders = new();
                        List<Model> ordersAll = new();
                        var MaxTime = DateTime.MinValue;

                        //取前50个订单进行拉取并纪录最后订单时间                              
                        int i = 0;
                        while (waitOrders.Skip(i * _ApiOrderServices.MaxGets).Take(_ApiOrderServices.MaxGets).ToList().Any())
                        {
                            orders.Clear();
                            var waitOrdersSplit = waitOrders.Skip(i * _ApiOrderServices.MaxGets).Take(_ApiOrderServices.MaxGets).ToList();

                            foreach (var o in waitOrdersSplit)
                            {
                                var m = _ApiOrderServices.GetOrderBasic(store, o);
                                if (m.IsNotNull())
                                {
                                    orders.Add(m!);
                                }
                                else
                                {
                                    //转成model的时候，出现了错误，标记一下，这个时间段需要重新拉
                                    _logger.LogError($"OrderBasc GetOrderBasic error");
                                    hasError = true;
                                }
                            }
                            i++;
                            if (orders.Count > 0)
                            {
                                ordersAll.AddRange(orders);
                                using (var transaction = await _dbContext.Database.BeginTransactionAsync())
                                {
                                    try
                                    {
                                        if (await AmazonOrder.Add(orders.ToArray()))
                                        {
                                            await _dbContext.SaveChangesAsync();
                                            await transaction.CommitAsync();
                                            _logger.LogInformation($"OrderBasc {store.ID} 【{store.Name}】Adds order Success {orders.Count}");
                                        }
                                        else
                                        {
                                            await transaction.RollbackAsync();
                                            hasError = true;
                                            _logger.LogError($"OrderBasc [{store.Name}] Adds order Fail");
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        await transaction.RollbackAsync();
                                        hasError = true;
                                        _logger.LogError($"OrderBasc Exception:{e.Message}");
                                        _logger.LogError($"OrderBasc Exception:{e.StackTrace}");
                                    }
                                }
                            }
                            else
                            {
                                hasError = true;
                            }
                        }

                        if (ordersAll.Count > 0) MaxTime = ordersAll.Max(m => m.CreateTime);

                        if (MaxTime > orderJob.LastOrderTime) orderJob.LastOrderTime = MaxTime;
                        if (!hasError)
                        {
                            orderJob.PullCount++;
                            if (listResult.HasNext)
                            {
                                orderJob.NextCycleInfo = listResult.NextCycleInfo;
                                orderJob.PullProgress = PullProgresses.Pulling;
                            }
                            else
                            {
                                orderJob.FullCycle++;
                                orderJob.QueryEnd = orderJob.QueryEndPayload;
                                orderJob.NextPull = DateTime.Now.AddSeconds(orderJob.IntervalSeconds);
                                orderJob.PullProgress = PullProgresses.Pulled;
                            }
                        }
                    }
                    else
                    {
                        if (listResult.Msg.Contains("Unauthorized", StringComparison.CurrentCultureIgnoreCase))
                        {
                            return;
                        }
                        //该时间段内没有订单
                        orderJob.FullCycle++;
                        orderJob.QueryEnd = orderJob.QueryEndPayload;
                        orderJob.NextPull = DateTime.Now.AddSeconds(orderJob.IntervalSeconds);
                        orderJob.PullProgress = PullProgresses.Pulled;
                    }
                    orderJob.LastDuration = (DateTime.Now - Now).Milliseconds;
                    using (var transaction = await _dbContext.Database.BeginTransactionAsync())
                    {
                        await _dbContext.SaveChangesAsync();
                        await transaction.CommitAsync();
                        storeCache.Add(store);
                        var s = $"QueryEnd：{store.QueryEnd.ToString()}拉取完毕,next time is：{store.NextPull.ToString()}";
                        _logger.LogInformation($"OrderBasc {store.ID} 【{store.Name}】 Update Job:{s}");
                    }
                }
            }
            else
            {
                _logger.LogInformation($"OrderBasc:have no stores");
                return;
            }
        }
        catch (Exception e)
        {
            _logger.LogError($"OrderBasc Exception：{e.Message}");
            _logger.LogError($"OrderBasc Exception：{e.StackTrace}");
        }
    }
}