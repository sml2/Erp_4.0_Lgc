using ERP.Services.Stores;
using ERP.Services.DB.ProductType;
using Model = ERP.Models.ProductNodeType.Amazon.AttributeModel;
using ProductTypeService = ERP.Services.DB.ProductType.ProductTypeService;
using ERP.Services.Api;
namespace ERP.Services.Host.ProductNew;

public class ProductType : Invoker<ProductType>
{
    public override int QueueCount => 1;
    public override int MinInterval => 1800000; //30分钟拉一次

    private readonly IOrderFactory _ApiOrderFactory;

    protected const int ItemCount = 5;
    private readonly IConfiguration _configuration;
    public ProductType(ILogger<ProductType> logger, IConfiguration configuration, IOrderFactory orderFactory, IServiceProvider serviceProvider) : base(logger, serviceProvider)
    {
        _ApiOrderFactory = orderFactory;
        _configuration= configuration;
    }

    public override async Task Invoke(int index, ILogger _logger)
    {
        var ServiceProvider = this.ServiceProvider.CreateScope().ServiceProvider;
        var storeService = ServiceProvider.GetRequiredService<StoreService>();
        var categoryService = ServiceProvider.GetRequiredService<CategoryService>();
        var productTypeService = ServiceProvider.GetRequiredService<ProductTypeService>();
        var _dbContextJob = ServiceProvider.GetRequiredService<ERP.Data.DBContext>();

        var storeids = _configuration.GetSection("AmazonStoreIDs").Value.Trim();
        if (string.IsNullOrWhiteSpace(storeids))
        {
            _logger.LogInformation($"ProductType:have no stores！");
            return;
        }
        _logger.LogInformation($"ProductType:storeids {storeids}");        
        List<int> ids = Array.ConvertAll(storeids.Split(","), int.Parse).ToList();
        var LstNeedPull = await storeService.GetNeedPullOrder(ids);
        if (LstNeedPull.IsNull() || LstNeedPull.Count <= 0)
        {
            _logger.LogInformation($"ProductType:have no stores！");
            return;
        }
        _logger.LogInformation($"ProductType:LstNeedPull { string.Join(",", LstNeedPull.Select(x=>x.ID).ToList())}");
        foreach (var store in LstNeedPull)
        {
            try
            {
                var marketLst = await storeService.GetAllMarketInfoByHash(store.UniqueHashCode);
                if (marketLst.IsNull() || marketLst!.Count <= 0)
                {
                    _logger.LogInformation($"ProductType:{store.ID}【{store.Name}】no markets");
                    continue;
                }
                store.MarketPlaces = marketLst.Select(x=>x.Marketplace).ToList();
                var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);

                foreach (var market in marketLst)
                {
                    bool isContinue = true;
                    int i = 0;
                    string marketPlace = market.Marketplace;
                    _logger.LogInformation($"ProductType：pull {store.ID}-{store.Name}-{market.NationName}-{marketPlace}");                   
                    var list = categoryService.GetNodeCategoryByFlag(marketPlace);
                    //var list = _ApiOrderServices.SearchDefinitionsProductTypes(store); //取出该地区所有的产品分类
                    if (list == null || list.Count <= 0)
                    {
                        _logger.LogInformation($"ProductType:{store.ID}【{store.Name}】{market.NationName}-{marketPlace}no pull productType");
                        continue;
                    }
                    while (list.Skip(i * ItemCount).Take(ItemCount).ToList().Any())
                    {
                        if (!isContinue)
                        {
                            break;
                        }
                        List<Model> typeList = new List<Model>();
                        var itemList = list.Skip(i * ItemCount).Take(ItemCount).ToList();
                        string productTypes = string.Join(',', itemList);
                        i++;
                        foreach (var item in itemList)
                        {
                            string producttype = item;
                            if (item.Contains(","))
                            {
                                producttype = item.Split(',')[0];
                            }
                            var attributeList = await _ApiOrderServices.GetProductTypeModels(store, marketPlace, producttype);
                            if (attributeList != null && attributeList.Count > 0)
                            {
                                typeList.AddRange(attributeList);
                            }
                            else
                            {
                                _logger.LogInformation($"ProductType pull is null:{store.ID}【{store.Name}】{market.NationName}-{marketPlace}-【{producttype}】");
                                //存在请求不通过的，推出当前市场，请求下一个市场
                                isContinue = false;
                                break;
                            }
                        }
                        if (typeList.Count > 0)
                        {
                            using (var transaction = await _dbContextJob.Database.BeginTransactionAsync())
                            {
                                try
                                {
                                    if (await productTypeService.AddRangeAsync(typeList, categoryService))
                                    {
                                        await transaction.CommitAsync();
                                        _logger.LogInformation($"ProductType:AddRangeAsync success,{store.ID}【{store.Name}】{marketPlace}:{productTypes}");
                                    }
                                    else
                                    {
                                        await transaction.RollbackAsync();
                                        _logger.LogError($"ProductType:AddRangeAsync fail，{store.ID}【{store.Name}】{marketPlace}:{productTypes}");
                                    }
                                }
                                catch (Exception e)
                                {
                                    await transaction.RollbackAsync();
                                    _logger.LogError($"ProductType:AddRangeAsync Exception:{e.Message},{store.ID}【{store.Name}】{marketPlace}");
                                    _logger.LogError($"ProductType:AddRangeAsync Exception:{e.StackTrace}");
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.LogError($"ProductType:Exception:{ex.Message},{store.ID}【{store.Name}】");
                _logger.LogError($"ProductType:Exception:{ex.StackTrace}");
            }
        }
    }
}

