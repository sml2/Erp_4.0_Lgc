using ERP.Services.Stores;
using ERP.Services.DB.ProductType;
using Newtonsoft.Json;
using Model = ERP.Models.ProductNodeType.Amazon.Category;
using storeModel = ERP.Models.Stores.StoreRegion;
using ERP.Services.Api;

namespace ERP.Services.Host.ProductNew;

public class NodeCategory : Invoker<NodeCategory>
{
    public override int QueueCount => 1;
    private readonly IConfiguration _configuration;
    public override int MinInterval => 1800000;   //30分钟拉一次
    private readonly IOrderFactory _ApiOrderFactory;
    protected const int ItemCount = 2000;
    public NodeCategory(ILogger<NodeCategory> logger, IConfiguration configuration, IOrderFactory orderFactory, IServiceProvider serviceProvider) : base(logger, serviceProvider)
    {
        _ApiOrderFactory = orderFactory;
        _configuration= configuration;
    }

    /// <summary>
    /// 指定店铺拉取：需要选定覆盖亚马逊全部市场的几个店铺
    /// </summary>
    /// <param name="index"></param>
    /// <param name="_logger"></param>
    /// <returns></returns>
    public override async Task Invoke(int index, ILogger _logger)
    {
        var ServiceProvider = this.ServiceProvider.CreateScope().ServiceProvider;       
        var storeService = ServiceProvider.GetRequiredService<StoreService>();
        var categoryService = ServiceProvider.GetRequiredService<CategoryService>();
        var _dbContext = ServiceProvider.GetRequiredService<ERP.Data.DBContext>();

        var storeids = _configuration.GetSection("AmazonStoreIDs").Value.Trim();
        if(string.IsNullOrWhiteSpace(storeids))
        {
            _logger.LogInformation($"NodeCategory:have no stores！");
            return;
        }
        _logger.LogInformation($"NodeCategory:storeids {storeids}");
        List<int> ids = Array.ConvertAll(storeids.Split(","), int.Parse).ToList(); //storeids.Split(",").Cast<int>().ToList();        
        var LstNeedPull = await storeService.GetNeedPullOrder(ids);
        if (LstNeedPull.IsNull() || LstNeedPull.Count <= 0)
        {
            _logger.LogInformation($"NodeCategory:have no stores！");
            return;
        }
        _logger.LogInformation($"NodeCategory:LstNeedPull {string.Join(",", LstNeedPull.Select(x => x.ID).ToList())}");
        foreach (var store in LstNeedPull)
        {
            var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
            var marketLst = await storeService.GetAllMarketInfoByHash(store.UniqueHashCode);
            if (marketLst.IsNull() || marketLst!.Count <= 0)
            {
                _logger.LogInformation($"NodeCategory:{store.ID}【{store.Name}】no markets");
                continue;
            }
            store.MarketPlaces = marketLst!.Select(x => x.Marketplace).ToList();
            foreach (var market in marketLst)
            {
                int i = 0;
                string marketPlace = market.Marketplace;
                var Datas = await _ApiOrderServices.GetReportBowerIDs(store, marketPlace);
                if (Datas is null || Datas.Node is null)
                {
                    _logger.LogError($"NodeCategory is null:{store.ID}【{store.Name}】{marketPlace}");
                    continue;
                }
                var listNodes = Datas!.Node;
                int Items = listNodes!.Count / ItemCount;
                try
                {
                    while (listNodes.Skip(i * ItemCount).Take(ItemCount).ToList().Any())
                    {
                        List<Model> categoryList = new List<Model>();
                        var Nodes = listNodes.Skip(i * ItemCount).Take(ItemCount).ToList();
                        i++;
                        string BrowseNodeIds=string.Join(',', Nodes.Select(x=>x.BrowseNodeId).ToList());
                        foreach (var data in Nodes)
                        {
                            try
                            {
                                long categoryId = 0, parentCategoryId = 0;
                                if (!long.TryParse(data.BrowseNodeId, out categoryId))
                                {
                                    _logger.LogError($"NodeCategory:BrowseNodeId to int fail,{store.ID}【{store.Name}】-{marketPlace}-{data.BrowseNodeId}");
                                    continue;
                                }

                                int root = 0;
                                var pathID = data.BrowsePathById;
                                if (!pathID.IsNullOrWhiteSpace())
                                {
                                    var pId = pathID.Split(',').Reverse().ToArray();
                                    root = pId.Length;
                                    if (!long.TryParse(pId[1], out parentCategoryId))
                                    {
                                        _logger.LogError($"NodeCategory:pid to int fail,parentCategoryId:{store.ID}【{store.Name}】-{marketPlace}-{pathID}");
                                        continue;
                                    }
                                }
                                var productType = data.ProductTypeDefinitions;
                                Model category = new Model(data.BrowseNodeName, data.BrowseNodeStoreContextName, ERP.Enums.Platforms.AMAZONSP, categoryId, parentCategoryId, productType, market.Marketplace, data.HasChildren, data.BrowsePathById, root == 2 ? true : false);
                                categoryList.Add(category);
                            }
                            catch (Exception e)
                            {
                                _logger.LogError($"NodeCategory:data SerializeObject fail:{e.Message},{store.ID}【{store.Name}】-{marketPlace}-{BrowseNodeIds}");
                                _logger.LogError($"NodeCategory:data SerializeObject fail:{e.StackTrace}");
                                continue;
                            }
                        }
                        if (categoryList.Count > 0)
                        {
                            using (var transaction = await _dbContext.Database.BeginTransactionAsync())
                            {
                                try
                                {
                                    if (await categoryService.AddRangeAsync(categoryList))
                                    {
                                        await transaction.CommitAsync();
                                        _logger.LogInformation($"NodeCategory:AddRangeAsync success");
                                    }
                                    else
                                    {
                                        await transaction.RollbackAsync();
                                        _logger.LogError($"NodeCategory:AddRangeAsync fail");
                                    }
                                }
                                catch (Exception e)
                                {
                                    await transaction.RollbackAsync();
                                    _logger.LogError($"NodeCategory:AddRangeAsync Exception:{e.Message},{store.ID}【{store.Name}】-{marketPlace}");
                                    _logger.LogError($"NodeCategory:AddRangeAsync Exception:{e.StackTrace}");
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError($"NodeCategory:Exception:{e.Message},{store.ID}【{store.Name}】-{marketPlace}");
                    _logger.LogError($"NodeCategory:Exception:{e.StackTrace}");
                    continue;
                }
            }
        }
    }
}
