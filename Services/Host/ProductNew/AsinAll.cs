
using ERP.Services.Stores;
using ERP.Services.Api;
namespace ERP.Services.Host.ProductNew;

/// <summary>
/// 拉取产品列表
/// </summary>
public class AsinAll : Invoker<AsinAll>
{
    public override int QueueCount => 1;
    public override int MinInterval => 300000; //5分钟

    private readonly IOrderFactory _ApiOrderFactory;

    public AsinAll(ILogger<AsinAll> logger, IOrderFactory orderFactory, IServiceProvider serviceProvider) : base(logger, serviceProvider)
    {
        _ApiOrderFactory = orderFactory;
    }

    /// <summary>
    /// 拉取店铺产品asin
    /// </summary>
    /// <param name="index"></param>
    /// <param name="_logger"></param>
    /// <returns></returns>
    public override async Task Invoke(int index, ILogger _logger)
    {
        bool IsMainSku = true;
        var StoreService = ServiceProvider.GetRequiredService<StoreService>();       
        var AsinProduct = ServiceProvider.GetRequiredService<DB.Stores.AsinProduct>();
        var _dbContext = ServiceProvider.GetRequiredService<ERP.Data.DBContext>();

        var LstNeedPull = await StoreService.GetNeedPull(m => m.ID % QueueCount == index);//@分配
        if (LstNeedPull.IsNull() || LstNeedPull.Count <= 0)
        {
            _logger.LogInformation($"AsinAll:have no stores！");
            return;
        }
        _logger.LogInformation($"AsinAll:storeIDs：{string.Join(",", LstNeedPull!.Select(x => x.ID).ToList())}");
        try
        {
            foreach (var store in LstNeedPull)
            {
                string SellerID = store.GetSPAmazon()!.SellerID;
                var marketLst = await StoreService.GetAllMarketInfoByHash(store.UniqueHashCode);
                if (marketLst.IsNull() || marketLst!.Count <= 0)
                {
                    _logger.LogInformation($"AsinAll:{store.ID}【{store.Name}】no markets");
                    continue;
                }
                store.MarketPlaces = marketLst!.Select(x => x.Marketplace).ToList();
                var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);
                var dicAsins = await _ApiOrderServices.GetAllAsin(store);
                if (!dicAsins.Any())
                {
                    _logger.LogError($"AsinAll Get Null，{store.ID}【{store.Name}】");
                    continue;
                }
                using (var transaction = await _dbContext.Database.BeginTransactionAsync())
                {
                    try
                    {
                        if (dicAsins.IsNotNull() && dicAsins.Count > 0)
                        {
                            if (await AsinProduct.Add(dicAsins, store.ID, SellerID, IsMainSku))
                            {
                                await _dbContext.SaveChangesAsync();
                                await transaction.CommitAsync();
                                _logger.LogInformation($"AsinAll Add Success，{store.ID}【{store.Name}】");
                            }
                        }
                        else
                        {
                            _logger.LogError($"AsinAll Add Fail，{store.ID}【{store.Name}】");
                        }
                    }
                    catch (Exception e)
                    {
                        await transaction.RollbackAsync();
                        _logger.LogError($"AsinAll Add Exception:{e.Message},{store.ID}【{store.Name}】");
                        _logger.LogError($"asinList Add Exception:{e.StackTrace},{store.ID}【{store.Name}】");
                    }
                }
            }
        }
        catch (Exception e)
        {
            _logger.LogError($"AsinAll Exception:{e.Message}");
            _logger.LogError($"AsinAll Exception:{e.StackTrace}");
        }
    }
}