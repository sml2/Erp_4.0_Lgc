using ERP.Services.Stores;

namespace ERP.Services.Host.ProductNew;
using ERP.Services.Api;
using Product = ERP.Data.Products.Product;

/// <summary>
/// 拉取产品详细信息
/// </summary>
public class Asin : Invoker<Asin>
{
    public override int QueueCount => 1;
    public override int MinInterval => 120000; //两分钟

    private readonly IOrderFactory _ApiOrderFactory;

    public Asin(ILogger<Asin> logger, IOrderFactory orderFactory, IServiceProvider serviceProvider) : base(logger, serviceProvider)
    {
        _ApiOrderFactory = orderFactory;
    }

    /// <summary>
    /// 拉取产品详细数据
    /// </summary>
    /// <param name="index"></param>
    /// <param name="_logger"></param>
    /// <returns></returns>
    public override async Task Invoke(int index, ILogger _logger)
    {
        //查找店铺待拉取的前20个产品，接口最大获取20个
        const int itemCount = 20;
        var StoreService = ServiceProvider.GetRequiredService<StoreService>();
        var productService = ServiceProvider.GetRequiredService<Services.Product.UploadAmazonService>();
        var Environment = ServiceProvider.GetRequiredService<IHostEnvironment>();
        var _dbContext = ServiceProvider.GetRequiredService<ERP.Data.DBContext>();

        var LstNeedPull = await StoreService.GetNeedPull(m => m.ID % QueueCount == index);

        if (LstNeedPull.IsNull() || LstNeedPull.Count <= 0)
        {
            _logger.LogInformation($"Asin:have no stores！");
            return;
        }
        _logger.LogInformation($"Asin:storeIDs：{string.Join(",", LstNeedPull!.Select(x => x.ID).ToList())}");

        //第一步，拉取店铺所有产品，并保存
        //第二步，从订单表中，找到asin表中，按拉取次数来         
        try
        {
            foreach (var store in LstNeedPull)
            {
                var list = new List<Product>();
                var marketLst = await StoreService.GetAllMarketInfoByHash(store.UniqueHashCode);
                if (marketLst.IsNull() || marketLst!.Count <= 0)
                {
                    _logger.LogInformation($"Asin:{store.ID}【{store.Name}】no markets");
                    continue;
                }
                store.MarketPlaces = marketLst.Select(x => x.Marketplace).ToList();
                foreach (var market in marketLst)
                {
                    var AsinNeedPull = await StoreService.GetAsinNeedPull(store.ID);
                    if (AsinNeedPull.IsNull() || AsinNeedPull.Count <= 0)
                    {
                        _logger.LogInformation($"Asin:{store.ID}【{store.Name}】have no asin to pull");
                        continue;
                    }
                    var _ApiOrderServices = _ApiOrderFactory.Create(store.Platform);

                    int i = 0;
                    while (AsinNeedPull.Skip(i * itemCount).Take(itemCount).Any())
                    {
                        var asins = AsinNeedPull.Skip(i * itemCount).Take(itemCount).ToList();
                        list = await _ApiOrderServices.GetProductInfo(store, asins);
                        string asinStr = string.Join(',', list.Select(x => x.Asin).ToList());
                        i++;
                        using (var transaction = await _dbContext.Database.BeginTransactionAsync())
                        {
                            try
                            {
                                if (list.Count > 0)
                                {
                                    await productService.UploadProduct(list, market.NationShort, Environment, _logger);
                                    await _dbContext.SaveChangesAsync();
                                    await transaction.CommitAsync();
                                    _logger.LogInformation($" asinproducts Add Success,{store.ID}【{store.Name}】 [{asinStr}]");
                                }
                                else
                                {
                                    await transaction.RollbackAsync();
                                    _logger.LogInformation($" asinproducts Add Fail,{store.ID}【{store.Name}】 [{asinStr}]");
                                }

                            }
                            catch (Exception e)
                            {
                                await transaction.RollbackAsync();
                                _logger.LogError($"asinproducts Add product Exception,{e.Message},{store.ID}【{store.Name}】,{asinStr}");
                                _logger.LogError($"asinproducts Add product Exception,{e.StackTrace},{store.ID}【{store.Name}】");
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            _logger.LogError($"Asin Exception:{e.Message}");
            _logger.LogError($"Asin Exception:{e.StackTrace}");
        }
    }
}