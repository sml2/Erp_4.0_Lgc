﻿using ERP.Data;
using ERP.Models.Export;
using Microsoft.EntityFrameworkCore;

namespace ERP.Services.Host;

/// <summary>
/// 删除已被下载过的本地临时表格
/// </summary>
public class RemoveExportedFilesBackground : Invoker<RemoveExportedFilesBackground>
{
    private readonly ILogger<RemoveExportedFilesBackground> _logger;
    public override int QueueCount { get; set; } = 1;
    public override int MinInterval => 60_000 * 30;

    public RemoveExportedFilesBackground(ILogger<RemoveExportedFilesBackground> logger, IServiceProvider serviceProvider) : base(logger,
        serviceProvider)
    {
        _logger = logger;
    }

    public override async Task Invoke(int index, ILogger logger)
    {
        throw new NotImplementedException();
    }

    public override async Task Invoke(int index, ILogger logger, CancellationToken stoppingToken)
    {
        var scope = ServiceProvider.CreateScope().ServiceProvider;
        var dbContext = scope.GetRequiredService<DBContext>();

        const int batchSize = 100;
        do
        {
            var tasks = await dbContext.ExportTasks
                .Where(t => t.State == ExportTask.TaskState.Downloaded)
                .OrderBy(t => t.ID)
                .Take(batchSize)
                .ToListAsync(stoppingToken);

            if (!tasks.Any())
            {
                _logger.LogInformation("暂时没有待删除的临时文件");
                break;
            }

            var queue = new Queue<ExportTask>(tasks);

            while (queue.Count > 0 && !stoppingToken.IsCancellationRequested)
            {
                var task = queue.Dequeue();
                System.IO.File.Delete(task.TempPath);
                task.State = ExportTask.TaskState.Deleted;
                _logger.LogDebug("删除任务{Task}临时文件", task);
            }

            await dbContext.SaveChangesAsync(default);

        } while (!stoppingToken.IsCancellationRequested);
    }
}