using System.Text;

namespace ERP.Services.Host;

using Service = Services.DB.Statistics.Statistic;

public class Statistic : Invoker<Statistic>
{
    public override int QueueCount => 1;
    public override int MinInterval => 30 * 60 * 1000;


    public Statistic(ILogger<Statistic> logger, IServiceProvider serviceProvider) : base(logger,
        serviceProvider)
    {
    }

    public override async Task Invoke(int index, ILogger _logger)
    {
        _logger.LogInformation($"更新统计数据！当前时间：{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}");
        var Service = ServiceProvider.GetRequiredService<Service>();
        //创建
        foreach (var o in ServiceProvider.GetRequiredService<Caches.OEM>().List()!) await Service.CheckOrAdd(o);
        foreach (var o in ServiceProvider.GetRequiredService<Caches.Store>().List()!) await Service.CheckOrAdd(o);
        foreach (var o in ServiceProvider.GetRequiredService<Caches.Company>().List()!) await Service.CheckOrAdd(o);
        foreach (var o in ServiceProvider.GetRequiredService<Caches.UserGroup>().List()!) await Service.CheckOrAdd(o);

        var needUpdateList = await Service.NeedUpdate();
        //统计
        foreach (var s in needUpdateList)
        {
            await Service.Update(s);
        }
    }
}