﻿using Coravel.Events.Interfaces;
using Coravel.Queuing.Broadcast;
using NPOI.SS.Formula.Functions;
using System.Collections.Concurrent;

namespace ERP.Services.Host
{
    public class TaskCompletedListener : IListener<QueueTaskCompleted>
    {
        // Constructor etc.
        public ConcurrentDictionary<Guid, bool> RunningTasks { get; set; } = new();

        public void AddTask(Guid guid)
        {
            RunningTasks.TryAdd(guid, false);
        }

        public bool IsCompleted(Guid guid)
        {
            if (RunningTasks.TryGetValue(guid, out var value))
            {
                if (value)
                {
                    RunningTasks.TryRemove(guid, out _);
                }
                return value;
            }

            return true;
        }


        public async Task HandleAsync(QueueTaskCompleted broadcasted)
        {
            if (RunningTasks.ContainsKey(broadcasted.Guid))
            {
                RunningTasks[broadcasted.Guid] = true;
            }
        }
    }
}
