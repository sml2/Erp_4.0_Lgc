using ERP.Data;
using ERP.Models.Api.Logistics;
using ERP.Models.DB.Identity;
using ERP.Models.DB.Users;
using ERP.Services.DB.Logistics;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using logisticsFactory = ERP.Services.Api.Logistics.Factory;

namespace ERP.Services.Host.Waybill;

public class PullShippingFeeHost : Invoker<PullShippingFeeHost>
{
    public override int QueueCount => Convert.ToInt32(_configuration.GetSection("WaybillPullShippingFeeTaskNum").Value.Trim());

    public override int MinInterval => 15 * 60 * 1000;


    private readonly IConfiguration _configuration;
    
    public PullShippingFeeHost(ILogger<PullShippingFeeHost> logger, IServiceProvider serviceProvider,
        IConfiguration configuration) : base(logger, serviceProvider)
    {
        _configuration = configuration;
    }
    
    // public override Task Invoke(int Index, ILogger logger)
    // {
    //     throw new NotImplementedException();
    // }

    public override async Task Invoke(int index, ILogger logger)
    {
        const int itemCount = 10;
        const int minute = 15;
        const int delayHour = 2;
        try
        {
            logger.LogInformation($"======================================Start Pull Waybill Fee Host========================================");
            logger.LogInformation($"DateTime:{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}");
            var service = ServiceProvider.GetRequiredService<WaybillNoSessionService>();
            var shippingFeeService = ServiceProvider.GetRequiredService<ShippingFeeService>();
            var logistics = ServiceProvider.GetRequiredService<logisticsFactory>();
            var _dbcontext = ServiceProvider.GetRequiredService<ERP.Data.DBContext>();
            var listNeedPull = await service.GetWaybillNeedPull(itemCount,minute);
            if (listNeedPull.IsNull() || listNeedPull!.Count <= 0)
            {
                logger.LogInformation($"PullShippingFeeHost:have no waybills！");
                logger.LogInformation($"======================================End Pull Waybill Fee Host========================================");
                return;
            }
            logger.LogInformation($"PullShippingFeeHost:WaybillIds：{string.Join(",", listNeedPull!.Select(x => x.ID).ToList())}");
            foreach (var item in listNeedPull)
            {
                logger.LogInformation($"PullShippingFeeHost:Start：【{item.ID} [{item.Express}]】");
                WaybillInfos waybill = new WaybillInfos(item); 
                var result = await logistics.GetFee(waybill);
                var res = JsonConvert.SerializeObject(result);;
                logger.LogInformation($"Response：{res}");
                if (result.Success)
                {
                    var priceInfo = result.Price;
                    if (priceInfo != null && priceInfo.Success)
                    {
                        var transaction = await _dbcontext.Database.BeginTransactionAsync();
                        try
                        {
                            var user = await _dbcontext.Users.Where(m => m.Id == item.UserID).FirstOrDefaultAsync();
                            if (user is null)
                            {
                                throw new Exception($"Error: User is no found{item.UserID}");
                            }
                            
                            await shippingFeeService.EditWayBillPrice(priceInfo, item.MergeID,user,true);
                            await transaction.CommitAsync();
                            logger.LogInformation($"Success: Update Fee Success");
                        }
                        catch (Exception e)
                        {
                            logger.LogError(e.Message,e);
                            await transaction.RollbackAsync();
                            logger.LogInformation($"Error: 发生异常{e.Message}");
                        }
                    }
                    else
                    {
                        //修改最后拉取的时间
                        item.LastAutoShipTime = DateTime.Now.AddHours(delayHour);
                        await _dbcontext.SaveChangesAsync();
                        logger.LogInformation($"Delay: 未获取到结果延迟{delayHour}小时");
                    }
                }
                else
                {
                    //修改最后拉取的时间
                    item.LastAutoShipTime = DateTime.MaxValue;
                    await _dbcontext.SaveChangesAsync();
                    logger.LogInformation($"Fail: 无法获取结果，自动拉取不在获取");
                }
            }
            logger.LogInformation($"======================================End Pull Waybill Fee Host========================================");
        }
        catch (Exception e)
        {
            logger.LogError($"PullShippingFeeHost Exception:{e.Message}");
            logger.LogError($"PullShippingFeeHost Exception:{e.StackTrace}");
            logger.LogError(e,e.Message);
            logger.LogInformation($"======================================End Pull Waybill Fee Host========================================");
        }
    }
}