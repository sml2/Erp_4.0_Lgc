using ERP.Extensions;
using NLog.Web;

//创建构造
var builder = WebApplication.CreateBuilder(args);
builder.Logging.ClearProviders();
builder.Host.UseNLog();
builder.Configuration.AddEnvironmentVariables("ERP_");
//添加服务
builder.AddServices();
//构建应用
var app = builder.Build();
//配置运行
app.Configure().Run();