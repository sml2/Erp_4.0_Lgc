using ERP.Models.View.Order.AmazonOrder;
using System.ComponentModel;

namespace ERP.Enums.Orders;
//public enum States
//{
//    [Description("未知")]
//    None,

//    [Description("未付款")]
//    UNPAID,

//    [Description("取消")]
//    CANCELED,

//    [Description("审核")]
//    审核,

//    [Description("找货")]
//    找货,

//    [Description("缺货")]
//    缺货,

//    [Description("配货")]
//    DISTRIBUTION,
//    /// <summary>
//    /// 未发货/待发货
//    /// </summary>
//    [Description("待发货")]
//    UNSHIPPED,

//    [Description("发货中")]
//    PARTIALLYSHIPPED,

//    [Description("已发货")]
//    SENT,

//    [Description("交付")]
//    DELIVERY,

//    [Description("问题")]
//    PROGRAM,

//    [Description("补发")]
//    补发,

//    [Description("退货")]
//    退货,

//    [Description("备查")]
//    备查
//}

public static class OrderState
{
    /// <summary>
    /// 订单状态
    /// </summary>
    private static readonly States[] Supports = { States.UNPAID, States.CANCELED, States.DISTRIBUTION, States.UNSHIPPED
        , States.PARTIALLYSHIPPED, States.SENT, States.DELIVERY };

    public static readonly Dictionary<int, string> SearchState = Supports.ToDictionary(@enum => (int)@enum
        , @enum => Extensions.EnumExtension.GetDescription(@enum));

    /// <summary>
    /// 获取订单进度列表 订单详情页
    /// </summary>
    /// <param name="bits">订单BITS</param>
    /// <returns></returns>
    public static Dictionary<int, object> GetOrderProgress(BITS bits) => new()
    {
        { 1, new { key = BITS.Progresses_Unprocessed, state = true, value = "未处理", buttonState = (bits & BITS.Progresses_Unprocessed) == BITS.Progresses_Unprocessed } },
        { 2, new { key = BITS.Progresses_PurchaseORreport, state = true, value = "采购或运单申报", buttonState = (bits & BITS.Progresses_PurchaseORreport) == BITS.Progresses_PurchaseORreport } },
        { 3, new { key = BITS.Progresses_PrintLabel_INSTOCK, state = true, value = "有货", buttonState = (bits & BITS.Progresses_PrintLabel_BitMask) == BITS.Progresses_PrintLabel_INSTOCK } },//
        { 4, new { key = BITS.Progresses_PrintLabel_OUTOFSTOCK, state = true, value = "缺货", buttonState = (bits & BITS.Progresses_PrintLabel_BitMask) == BITS.Progresses_PrintLabel_OUTOFSTOCK } },//
        { 5, new { key = BITS.Progresses_Shipment_Success, state = true, value = "仓库发货成功", buttonState = (bits & BITS.Progresses_Shipment_BitMask) == BITS.Progresses_Shipment_Success } },//
        { 6, new { key = BITS.Progresses_Shipment_Error, state = true, value = "仓库发货异常", buttonState = (bits & BITS.Progresses_Shipment_BitMask) == BITS.Progresses_Shipment_Error } },//
        { 7, new { key = BITS.Progresses_Error, state = true, value = "异常订单", buttonState = (bits & BITS.Progresses_Error) == BITS.Progresses_Error } },
        { 8, new { key = BITS.Waybill_HasSync, state = true, value = "已虚拟发货", buttonState = (bits & BITS.Waybill_HasSync) == BITS.Waybill_HasSync } },
        { 9, new { key = BITS.Waybill_HasSync, state = false, value = "未虚拟发货", buttonState = (bits & BITS.Waybill_HasSync) != BITS.Waybill_HasSync } },
        { 10, new { key = BITS.Waybill_HasPrintLabel, state = true, value = "已打印面单", buttonState = (bits & BITS.Waybill_HasPrintLabel) == BITS.Waybill_HasPrintLabel } },
        { 11, new { key = BITS.Waybill_HasPrintLabel, state = false, value = "未打印面单", buttonState = (bits & BITS.Waybill_HasPrintLabel) != BITS.Waybill_HasPrintLabel } },
        { 12, new { key = BITS.Waybill_HasUploadInvoice, state = true, value = "已上传发票", buttonState = (bits & BITS.Waybill_HasUploadInvoice) == BITS.Waybill_HasUploadInvoice } },
        { 13, new { key = BITS.Waybill_HasUploadInvoice, state = false, value = "未上传发票", buttonState = (bits & BITS.Waybill_HasUploadInvoice) != BITS.Waybill_HasUploadInvoice } },
    };

    /// <summary>
    /// 订单进度
    /// </summary>
    public static readonly Dictionary<int, object> OrderProgressData = new()
    {
        { 0, new { mask = 1 << 9, key = 1 << 9, state = true, value = "未处理" } },
        { 1, new { mask = 1 << 7, key = 1 << 6, state = true, value = "有货" } },
        { 2, new { mask = 1 << 7, key = 1 << 6, state = false, value = "缺货" } },
        { 3, new { mask = 1 << 11, key = 1 << 11, state = true, value = "已虚拟发货" } },
        { 4, new { mask = 1 << 11, key = 1 << 11, state = false, value = "未虚拟发货" } },
        { 5, new { mask = 1 << 12, key = 1 << 12, state = true, value = "已打印面单" } },
        { 6, new { mask = 1 << 12, key = 1 << 12, state = false, value = "未打印面单" } },
        { 7, new { mask = 1 << 5, key = 1 << 4, state = false, value = "仓库发货成功" } },
        { 8, new { mask = 1 << 5, key = 1 << 4, state = true, value = "仓库发货失败" } },
        { 9, new { mask = 1 << 10, key = 1 << 10, state = true, value = "已上传发票" } },
        { 10, new { mask = 1 << 10, key = 1 << 10, state = false, value = "未上传发票" } },
    };

    /// <summary>
    /// 下拉框获取订单进度数据
    /// </summary>
    public static readonly List<object> OrderProgressSelect = new()
    {
        new
        {
            label = "订单进度",
            options = new List<object>
            {
                new { value = new ProgressValue { id = 1, bits = BITS.Progresses_Unprocessed, state = true }, label = "未处理" },
                new { value = new ProgressValue { id = 2, bits = BITS.Progresses_PurchaseORreport, state = true }, label = "采购或运单申报" },
                new { value = new ProgressValue { id = 3, bits = BITS.Progresses_PrintLabel_INSTOCK, state = true }, label = "打印面单-有货" },
                new { value = new ProgressValue { id = 4, bits = BITS.Progresses_PrintLabel_OUTOFSTOCK, state = true }, label = "打印面单-缺货" },
                new { value = new ProgressValue { id = 5, bits = BITS.Progresses_Shipment_Success, state = true }, label = "仓库发货-成功" },
                new { value = new ProgressValue { id = 6, bits = BITS.Progresses_Shipment_Error, state = true }, label = "仓库发货-异常" },
                new { value = new ProgressValue { id = 7, bits = BITS.Progresses_Error_UserAction, state = true }, label = "异常订单" },
            }
        },
        new
        {
            label = "同步运单(虚拟发货)",
            options = new List<object>
            {
                new { value = new ProgressValue { id = 8, bits = BITS.Waybill_HasSync, state=true }, label = "已虚拟发货" },
                new { value = new ProgressValue { id = 9, bits = BITS.Waybill_HasSync, state = false }, label = "未虚拟发货"}
            }
        },
        new
        {
            label = "上传发票",
            options = new List<object>
            {
                new { value = new ProgressValue { id = 10, bits = BITS.Waybill_HasUploadInvoice, state = true }, label = "已上传发票" },
                new { value = new ProgressValue { id = 11, bits = BITS.Waybill_HasUploadInvoice, state = false }, label = "未上传发票" }
            }
        },
        new
        {
            label = "打印面单",
            options = new List<object>
            {
                new { value = new ProgressValue { id = 12, bits = BITS.Waybill_HasPrintLabel, state = true }, label = "已打印面单" },
                new { value = new ProgressValue { id = 13, bits = BITS.Waybill_HasPrintLabel, state = false }, label = "未打印面单" }
            }
        }
    };
}

