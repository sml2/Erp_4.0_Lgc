﻿using System.ComponentModel;

namespace ERP.Enums.Api.ElectronicBusinessPlatform.Amazon;
using Orders;
public enum Status : byte
{
    /// <summary>
    /// 付款已经过授权，订单已准备好进行发货，但订单中商品尚未发运。
    /// </summary>
    [Description("未发货")]
    Unshipped  = States.UNSHIPPED,
    /// <summary>
    /// 订单中的一个或多个（但并非全部）商品已经发货。
    /// </summary>
    [Description("部分发货")]
    PartiallyShipped  = States.PARTIALLYSHIPPED,
    /// <summary>
    /// 订单中的所有商品均已发货。
    /// </summary>
    [Description("已发货")]
    Shipped = States.SENT,
    /// <summary>
    /// 订单中的所有商品均已发货。但是卖家还没有向亚马逊确认已经向买家寄出发票。请注意：此参数仅适用于中国地区。。
    /// </summary>
    [Description("待寄出发票")]
    InvoiceUnconfirmed = States.DELIVERY,
    /// <summary>
    /// 订单无法进行配送。该状态仅适用于通过亚马逊零售网站之外的渠道下达但由亚马逊进行配送的订单。
    /// </summary>
    [Description("无法实现")]
    Unfulfillable  = States.DISTRIBUTION,
    /// <summary>
    /// 订单已取消。
    /// </summary>
    [Description("取消")]
    Canceled  = States.CANCELED,
    /// <summary>
    /// 只有预订订单才有此状态。订单已生成，但是付款未授权且商品的发售日期是将来的某一天。订单尚不能进行发货。请注意：仅在日本 (JP)，Preorder 才可能是一个 OrderType 值。
    /// </summary>
    [Description("预订/待定可用性")]
    PendingAvailability  = States.UNPAID,
    /// <summary>
    /// 订单已生成，但是付款未授权。订单尚不能进行发货。请注意：对于 OrderType = Standard 的订单，初始的订单状态是 Pending。对于 OrderType = Preorder 的订单（仅适用于 JP），初始的订单状态是 PendingAvailability，当进入付款授权流程时，订单状态将变为 Pending。
    /// </summary>
    [Description("待办的/预订待付款")]
    Pending = States.UNPAID,
}

