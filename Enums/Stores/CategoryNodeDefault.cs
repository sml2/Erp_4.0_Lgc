﻿using System.ComponentModel;

namespace ERP.Enums.Stores
{
    public class CategoryNodeDefault
    {
        public enum CategryMode
        {
            [Description("公司数据")]
            CompanyData,
            [Description("组内数据")]
            GroupData
        }
    }
}
