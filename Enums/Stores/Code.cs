using NUnit.Framework;

namespace ERP.Enums.Stores;

public class Codes
{
    public static Dictionary<string, Dictionary<string, object>> Params = new()
    {
        {
            "us", new()
            {
                {"label", "美国"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"000", "019"},
                        new() {"030", "039"},
                        new() {"060", "139"},
                    }
                }
            }
        },

        {
            "fr", new()
            {
                {"label", "法国"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"300", "379"}
                    }
                }
            }
        },
        {
            "bg", new()
            {
                {"label", "保加利亚"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"380"}
                    }
                }
            }
        },
        {
            "si", new()
            {
                {"label", "斯洛文尼亚"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"383"}
                    }
                }
            }
        },
        {
            "hr", new()
            {
                {"label", "克罗地亚"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"385"}
                    }
                }
            }
        },
        {
            "7", new()
            {
                {"label", "波斯尼亚"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"387"}
                    }
                }
            }
        },
        {
            "me", new()
            {
                {"label", "黑山"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"389"}
                    }
                }
            }
        },
        {
            "de", new()
            {
                {"label", "德国"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"400", "440"}
                    }
                }
            }
        },
        {
            "jp", new()
            {
                {"label", "日本"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"450", "459"},
                        new() {"490", "499"}
                    }
                }
            }
        },
        {
            "ru", new()
            {
                {"label", "俄罗斯"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"460", "469"}
                    }
                }
            }
        },
        {
            "kg", new()
            {
                {"label", "吉尔吉斯斯坦"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"470"}
                    }
                }
            }
        },
        {
            "tw", new()
            {
                {"label", "中国台湾"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"471"}
                    }
                }
            }
        },
        {
            "ee", new()
            {
                {"label", "爱沙尼亚"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"474"}
                    }
                }
            }
        },
        {
            "lv", new()
            {
                {"label", "拉脱维亚"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"475"}
                    }
                }
            }
        },
        {
            "az", new()
            {
                {"label", "阿塞拜疆"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"476"}
                    }
                }
            }
        },
        {
            "lt", new()
            {
                {"label", "立陶宛"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"477"}
                    }
                }
            }
        },
        {
            "uz", new()
            {
                {"label", "乌兹别克斯坦"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"478"}
                    }
                }
            }
        },
        {
            "lk", new()
            {
                {"label", "斯里兰卡"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"479"}
                    }
                }
            }
        },
        {
            "ph", new()
            {
                {"label", "菲律宾"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"480"}
                    }
                }
            }
        },
        {
            "by", new()
            {
                {"label", "白俄罗斯"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"481"}
                    }
                }
            }
        },
        {
            "ua", new()
            {
                {"label", "乌克兰"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"482"}
                    }
                }
            }
        },
        {
            "md", new()
            {
                {"label", "摩尔多瓦"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"484"}
                    }
                }
            }
        },
        {
            "am", new()
            {
                {"label", "亚美尼亚"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"485"}
                    }
                }
            }
        },
        {
            "ge", new()
            {
                {"label", "格鲁吉亚"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"486"}
                    }
                }
            }
        },
        {
            "kz", new()
            {
                {"label", "哈萨克斯坦"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"487"}
                    }
                }
            }
        },
        {
            "tj", new()
            {
                {"label", "塔吉克斯坦"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"488"}
                    }
                }
            }
        },
        {
            "hk", new()
            {
                {"label", "中国香港"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"489"}
                    }
                }
            }
        },
        {
            "gb", new()
            {
                {"label", "英国"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"500","509"}
                    }
                }
            }
        },
        {
            "gr", new()
            {
                {"label", "希腊"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"520","521"}
                    }
                }
            }
        }
        ,
        {
            "lb", new()
            {
                {"label", "黎巴嫩"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"528"}
                    }
                }
            }
        },
        {
            "cy", new()
            {
                {"label", "塞浦路斯"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"529"}
                    }
                }
            }
        },
        {
            "al", new()
            {
                {"label", "阿尔巴尼亚"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"530"}
                    }
                }
            }
        },
        {
            "mk", new()
            {
                {"label", "马其顿"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"531"}
                    }
                }
            }
        },
        {
            "mt", new()
            {
                {"label", "马耳他"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"535"}
                    }
                }
            }
        },
        {
            "ie", new()
            {
                {"label", "爱尔兰"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"539"}
                    }
                }
            }
        },
        {
            "be", new()
            {
                {"label", "比利时"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"540","549"}
                    }
                }
            }
        },
        {
            "pt", new()
            {
                {"label", "葡萄牙"},
                {
                    "code", new List<List<string>>()
                    {
                        new() {"560"}
                    }
                }
            }
        },
        {
            "is", new()
            {
                {"label", "冰岛"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"569"}
                    }
                }
            }
        },
        {
            "dk", new()
            {
                {"label", "丹麦"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"570","579"}
                    }
                }
            }
        },
        {
            "pl", new()
            {
                {"label", "波兰"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"590"}
                    }
                }
            }
        },
        {
            "ro", new()
            {
                {"label", "罗马尼亚"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"594"}
                    }
                }
            }
        },
        {
            "hu", new()
            {
                {"label", "匈牙利"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"599"}
                    }
                }
            }
        },
        {
            "za", new()
            {
                {"label", "南非"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"600","601"}
                    }
                }
            }
        },
        {
            "gh", new()
            {
                {"label", "加纳"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"603"}
                    }
                }
            }
        },
        {
            "sn", new()
            {
                {"label", "塞内加尔"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"604"}
                    }
                }
            }
        },
        {
            "bh", new()
            {
                {"label", "巴林"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"608"}
                    }
                }
            }
        },
        {
            "mu", new()
            {
                {"label", "毛里求斯"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"609"}
                    }
                }
            }
        },
        {
            "ma", new()
            {
                {"label", "摩洛哥"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"611"}
                    }
                }
            }
        },
        {
            "dz", new()
            {
                {"label", "阿尔及利亚"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"613"}
                    }
                }
            }
        },
        {
            "ng", new()
            {
                {"label", "尼日利亚"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"615"}
                    }
                }
            }
        },
        {
            "ke", new()
            {
                {"label", "肯尼亚"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"616"}
                    }
                }
            }
        },
        {
            "ci", new()
            {
                {"label", "科特迪瓦"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"618"}
                    }
                }
            }
        },
        {
            "tn", new()
            {
                {"label", "突尼斯"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"619"}
                    }
                }
            }
        },
        {
            "sy", new()
            {
                {"label", "叙利亚"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"621"}
                    }
                }
            }
        },
        {
            "eg", new()
            {
                {"label", "埃及"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"622"}
                    }
                }
            }
        },
        {
            "ly", new()
            {
                {"label", "利比亚"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"624"}
                    }
                }
            }
        },
        {
            "jo", new()
            {
                {"label", "约旦"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"625"}
                    }
                }
            }
        },
        {
            "ir", new()
            {
                {"label", "伊朗"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"626"}
                    }
                }
            }
        },
        {
            "kw", new()
            {
                {"label", "科威特"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"627"}
                    }
                }
            }
        },
        {
            "sa", new()
            {
                {"label", "沙特阿拉伯"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"628"}
                    }
                }
            }
        },
        {
            "ae", new()
            {
                {"label", "阿联酋"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"629"}
                    }
                }
            }
        },
        {
            "fi", new()
            {
                {"label", "芬兰"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"640","649"}
                    }
                }
            }
        },
        {
            "cn", new()
            {
                {"label", "中国"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"690","695"}
                    }
                }
            }
        },
        {
            "no", new()
            {
                {"label", "挪威"},
                {
                    "code", new List<List<string>>
                    {
                        new() {"700","709"}
                    }
                }
            }
        }
    };
}