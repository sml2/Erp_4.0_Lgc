﻿namespace ERP.Enums.Rule;

public enum Scenes
{
    Permission,
    Visible,
    Value
}
