﻿namespace ERP.Enums.Rule
{
    /// <summary>
    /// 权限修改模式
    /// </summary>
    public enum Modes
    {
        /// <summary>
        /// 业务角色权限模板
        /// </summary>
        RuleTemplate,
        /// <summary>
        /// 角色
        /// </summary>
        Role,
        /// <summary>
        /// 用户
        /// </summary>
        User,
    }
}
