﻿using System.ComponentModel;

namespace ERP.Enums.Operations
{
    public enum TypeModuels
    {
        [Description("物流")]
        Logistce = Platforms.Logistic,
        [Description("亚马逊")]
        Amazom = Platforms.AMAZON,
        [Description("虾皮")]
        Shoppe = Platforms.SHOPEE,
        [Description("汇率")]
        Rate = Platforms.OTHERS,
    }
    public enum Results
    {
        [Description("未知")]
        Unknown = 0,
        [Description("完成")]
        Success,
        [Description("失败")]
        Error,
    }
}
