﻿using System.ComponentModel;

namespace ERP.Enums
{

    public enum ParameterErrorType
    {
        None=0,
        [Description("权限参数")]
        Parameter,
        [Description("Send参数")]
        SendParameter,
        [Description("OrderInfo参数")]
        OrderInfoParameter,
        [Description("窗口参数")]
        EXTParameter,
        [Description("参数格式")]
        ParameterFormat,       
        [Description("参数错误")]
        ParameterIsNullorError,
        [Description("URL错误")]
        ParameterUrl,
        [Description("国家简称为空")]
        CountryCodeNull,
        [Description("运输方式为空")]
        TransportTypeNull,
        [Description("物流单号为空")]
        LogictisNumberNull,
        [Description("跟踪号为空")]
        TrackNumberNull,
        [Description("物流账号为空")]
        ParameterNull,
        [Description("请填写包裹重量")]
        WeightNull,
        [Description("请填写包裹的长度，宽度，高度以及重量")]
        PackageNull,
    }
}
