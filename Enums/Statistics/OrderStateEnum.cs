using System.ComponentModel;

namespace ERP.Enums.Statistics;

public enum OrderStateEnum
{
    [Description("未 付 款")]
    Unpaid = 1,

    [Description("取 消")]
    Cancel,

    [Description("配 货'")]
    Distribution,

    [Description("待 发 货")]
    ToBeDelivered,

    [Description("交 付")]
    Delivery
}