﻿using System.ComponentModel;

namespace ERP.Enums.Message;

public enum Commits
{
    [Description("当前公司")]
    CURRENTCOMPANY = 1,

    [Description("上级公司")]
    HIGHERCOMPANY = 2
}

