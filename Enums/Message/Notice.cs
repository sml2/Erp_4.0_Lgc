﻿using System.ComponentModel;

namespace ERP.Enums.Message
{
    public enum Sizes
    {
        [Description("弹窗大")]
        BIG = 1,

        [Description("弹窗中")]
        MIDDLE = 2,

        [Description("弹窗小")]
        SMALL = 2
    }
    public enum States
    {
        [Description("启用")]
        ON = 1,

        [Description("草稿")]
        DRAFT = 2
    }
    public enum Types
    {
        [Description("系统所有人员")]
        ALL = 1,

        [Description("公司外部")]
        OEM = 2,

        [Description("小组内部")]
        GROUP = 3,

        [Description("公司内部")]
        COMPANY = 4,
    }
}
