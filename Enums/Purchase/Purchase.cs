﻿using System.ComponentModel;

namespace ERP.Enums.Purchase;

/// <summary>
/// 采购状态
/// </summary>
public enum PurchaseStates
{
    None,
    [Description("采购中")]
    INPROCUREMENT = 2,

    [Description("已采购")]
    PURCHASED = 3,

    [Description("无需采购")]
    NOPURCHASEREQUIRED = 4,

    [Description("上级采购审核拒绝")]
    REFUSED = 5
}

/// <summary>
/// 确认状态
/// </summary>
public enum PurchaseAffirms
{
    [Description("未确认")]
    UNCONFIRMED = 1,

    [Description("已确认")]
    CONFIRMED = 2,

    [Description("已取消")]
    Cancel = 3
}

//public enum IsAffirms
//{
//    [Description("是")]
//    Yes = 1,

//    [Description("否")]
//    No = 2
//}

/// <summary>
/// 手续费类型
/// </summary>
public enum PurchaseRateTypes
{
    [Description("百分比")]
    BAR = 1,

    [Description("固定数值")]
    NUM = 2
}

public enum IsStocks
{
    [Description("入库")]
    TRUE = 2,

    [Description("不入库")]
    FALSE = 1
}

#region purchasePick
public enum States
{
    [Description("待确定")]
    TOBECONFIRMED = 1,

    [Description("确定")]
    CONFIRM = 2,

    [Description("否决")]
    VETO = 3
}
#endregion