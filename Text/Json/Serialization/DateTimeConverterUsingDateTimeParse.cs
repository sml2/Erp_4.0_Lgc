﻿using System;
using System.Text.Json.Serialization;

namespace ERP.Text.Json.Serialization;
/// <summary>
/// 常规日期格式转换
/// <code>2022-02-02 02:02:02</code>
/// </summary>
public class DateTimeConverterUsingDateTimeParse : JsonConverter<DateTime>
{
    public override DateTime Read(ref System.Text.Json.Utf8JsonReader reader, Type typeToConvert, System.Text.Json.JsonSerializerOptions options)
    {
        System.Diagnostics.Debug.Assert(typeToConvert == typeof(DateTime));
        var s = reader.GetString();
        if (s is not null)
            return DateTime.Parse(s);
        throw new NullReferenceException(nameof(s));
    }

    public override void Write(System.Text.Json.Utf8JsonWriter writer, DateTime value, System.Text.Json.JsonSerializerOptions options)
    {
        writer.WriteStringValue(value.ToString());
    }
}


