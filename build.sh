﻿#!/usr/bin/bash
# build.sh [version]
# version 镜像版本，默认latest,指定版本号最后的tag名称为`v版本号`

# 构建ui,更改npm源为本地源
#docker build ../erp_ui/ -t erp_ui:latest --build-arg NPM_REGISTRY=http://mirrors.local.com/npm

tags='-t swr.cn-east-3.myhuaweicloud.com/erp4/erp:latest'
if [ $1 ]; then
    tags="$tags -t swr.cn-east-3.myhuaweicloud.com/erp4/erp:$1"
fi
echo $tags
docker build . $tags