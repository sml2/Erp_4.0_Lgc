﻿START TRANSACTION;

ALTER TABLE `AttributeModels` DROP COLUMN `Datas`;

ALTER TABLE `AttributeModels` DROP COLUMN `Description`;

ALTER TABLE `AttributeModels` DROP COLUMN `ExtentData`;

ALTER TABLE `AttributeModels` DROP COLUMN `GroupName`;

ALTER TABLE `AttributeModels` DROP COLUMN `PropertyName`;

ALTER TABLE `AttributeModels` DROP COLUMN `Required`;

ALTER TABLE `AttributeModels` DROP COLUMN `Requireds`;

ALTER TABLE `AttributeModels` DROP COLUMN `Row`;

ALTER TABLE `AttributeModels` DROP COLUMN `RowMd5`;

ALTER TABLE `AttributeModels` DROP COLUMN `Title`;

ALTER TABLE `AttributeModels` DROP COLUMN `Type`;

ALTER TABLE `NodeCategory` MODIFY COLUMN `IsRoot` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '是否是根节点';

ALTER TABLE `NodeCategory` MODIFY COLUMN `HasChild` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '是否有子目录节点';

UPDATE `AttributeModels` SET `Version` = ''
WHERE `Version` IS NULL;
SELECT ROW_COUNT();


ALTER TABLE `AttributeModels` MODIFY COLUMN `Version` longtext CHARACTER SET utf8mb4 NOT NULL;

ALTER TABLE `AttributeModels` ADD `SchemaJson` longtext CHARACTER SET utf8mb4 NULL COMMENT 'schemaJson';

INSERT INTO `OEM` (`ID`, `Alipay`, `CreatedAt`, `DatabaseId`, `Detail`, `Domain`, `LastCreateStatistic`, `LoginPage`, `Name`, `OssId`, `RedisId`, `State`, `Style`, `ThemeId`, `Title`, `UpdatedAt`, `UrlIcon`, `UrlLogo`)
VALUES (95, '', TIMESTAMP '2021-11-19 13:38:00', 1, '逊利优跨境ERP系统', '["xly4.liulergou.com"]', TIMESTAMP '0001-01-01 00:00:00', 'UniversalLogin', '逊利优跨境ERP系统', 1, 1, 1, 1, 1, '逊利优跨境ERP系统', TIMESTAMP '2021-11-19 13:38:00', 'https://img4.waimaomvp.com/7/2023/01/31/images/444447/1675150231_3F897BAFEE468628.png', 'https://img4.waimaomvp.com/7/2023/01/31/images/444447/1675150235_BAC884CDF277F47.ico');
INSERT INTO `OEM` (`ID`, `Alipay`, `CreatedAt`, `DatabaseId`, `Detail`, `Domain`, `LastCreateStatistic`, `LoginPage`, `Name`, `OssId`, `RedisId`, `State`, `Style`, `ThemeId`, `Title`, `UpdatedAt`, `UrlIcon`, `UrlLogo`)
VALUES (96, '', TIMESTAMP '2021-11-19 13:38:00', 1, 'CTG跨境', '["ctg4.liulergou.com"]', TIMESTAMP '0001-01-01 00:00:00', 'UniversalLogin', 'CTG跨境', 1, 1, 1, 1, 1, 'CTG跨境', TIMESTAMP '2021-11-19 13:38:00', 'https://img4.waimaomvp.com/7/2023/01/31/images/444447/1675150388_6189AD25E396894A.png', 'https://img4.waimaomvp.com/7/2023/01/31/images/444447/1675150390_7D75CC1A44E62D.ico');

UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-01-31 15:53:44', `PasswordHash` = 'AQAAAAEAACcQAAAAEBxIOl8SZUf22R8j1nxTBFSmKY+tRSWQnEMTjnzgx7HNovcL2PTNVesf9KqylcqHeQ==', `UpdatedAt` = TIMESTAMP '2023-01-31 15:53:44'
WHERE `Id` = 444445;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-01-31 15:53:44', `PasswordHash` = 'AQAAAAEAACcQAAAAEEg6Ih7+LT+jqu3kjVNoEXlqXdCwPpEhBhgM1eUtdivnJdUTzjCWBZszrD+aP2u3Ow==', `UpdatedAt` = TIMESTAMP '2023-01-31 15:53:44'
WHERE `Id` = 444446;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-01-31 15:53:44', `PasswordHash` = 'AQAAAAEAACcQAAAAEETzlVykEWvGM/Iigbp1EtnHMZCumrhSD6WRuZ+rHDiswi8TWY+oqWw5iWVaZU0/ZA==', `UpdatedAt` = TIMESTAMP '2023-01-31 15:53:44'
WHERE `Id` = 444447;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-01-31 15:53:44', `PasswordHash` = 'AQAAAAEAACcQAAAAEE51OvXfu/oXRY3gLWL5PV8AMHei6OY+8BFrgfTTlyx4JhGQTciWL0Xg5lwFH6vteg==', `UpdatedAt` = TIMESTAMP '2023-01-31 15:53:44'
WHERE `Id` = 444448;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-01-31 15:53:44', `PasswordHash` = 'AQAAAAEAACcQAAAAEKEKA3pimRw88X4hFSOeX+pxLp3po0n5Ugz/jN5aKoYs6HkN3Uzg56q4BRtGjfwebg==', `UpdatedAt` = TIMESTAMP '2023-01-31 15:53:44'
WHERE `Id` = 444449;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-01-31 15:53:44', `PasswordHash` = 'AQAAAAEAACcQAAAAEL0gC7ZCm1Dfs/SN4rkoLLgG3uESRmcxmy5Yrpf+YAD2pssfH4AQN7ej5WiFx9q4jQ==', `UpdatedAt` = TIMESTAMP '2023-01-31 15:53:44'
WHERE `Id` = 444450;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-01-31 15:53:44', `PasswordHash` = 'AQAAAAEAACcQAAAAEFZ9WKJzdGAqwVRbMXIMsEaj9TZjQYxfqnxunc0YDi8Q5A8/RrJV2QL/oXxuvjLMGA==', `UpdatedAt` = TIMESTAMP '2023-01-31 15:53:44'
WHERE `Id` = 444451;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-01-31 15:53:44', `PasswordHash` = 'AQAAAAEAACcQAAAAEHz/kEvwLRpot+cFriJajEMfxGMo/GQOhqmUAbam4zCnvliJZPnQOqWZRhek3qxhzg==', `UpdatedAt` = TIMESTAMP '2023-01-31 15:53:44'
WHERE `Id` = 444452;
SELECT ROW_COUNT();


UPDATE `UserGroup` SET `Sort` = -1675151624
WHERE `ID` = 1;
SELECT ROW_COUNT();


UPDATE `UserValidities` SET `CreatedAt` = TIMESTAMP '2023-01-31 15:53:44', `UpdatedAt` = TIMESTAMP '2023-01-31 15:53:44'
WHERE `ID` = 1;
SELECT ROW_COUNT();


UPDATE `UserValidities` SET `CreatedAt` = TIMESTAMP '2023-01-31 15:53:44', `UpdatedAt` = TIMESTAMP '2023-01-31 15:53:44'
WHERE `ID` = 2;
SELECT ROW_COUNT();


UPDATE `UserValidities` SET `CreatedAt` = TIMESTAMP '2023-01-31 15:53:44', `UpdatedAt` = TIMESTAMP '2023-01-31 15:53:44'
WHERE `ID` = 3;
SELECT ROW_COUNT();


INSERT INTO `CompanyOem` (`ID`, `AllotTrueName`, `AllotUserId`, `CompanyID`, `CreatedAt`, `OEMID`, `UpdatedAt`, `UserName`)
VALUES (95, 'XIAOMI', 444446, 2, TIMESTAMP '2021-11-19 13:38:00', 95, TIMESTAMP '2021-11-19 13:38:00', 'XIAOMI');

INSERT INTO `CompanyOem` (`ID`, `AllotTrueName`, `AllotUserId`, `CompanyID`, `CreatedAt`, `OEMID`, `UpdatedAt`, `UserName`)
VALUES (96, 'XIAOMI', 444446, 2, TIMESTAMP '2021-11-19 13:38:00', 96, TIMESTAMP '2021-11-19 13:38:00', 'XIAOMI');

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20230131075348_t30', '6.0.5');

COMMIT;

