﻿START TRANSACTION;

ALTER TABLE `NodeCategory` DROP COLUMN `Market`;

ALTER TABLE `OperationLogging` ADD `Cancle` JSON NULL;

ALTER TABLE `OperationLogging` ADD `CancleFirst` datetime(6) NULL COMMENT '第一次Cancle时间';

ALTER TABLE `OperationLogging` ADD `CancleLast` datetime(6) NULL COMMENT '最后一次Cancle时间';

ALTER TABLE `OperationLogging` ADD `CancleNums` int NOT NULL DEFAULT 0 COMMENT 'Cancle发送次数';

ALTER TABLE `OperationLogging` ADD `CancleSuccess` datetime(6) NULL COMMENT '第一次Cancle成功获取到内容的时间';

ALTER TABLE `NodeCategory` ADD `MarketPlaceID` longtext CHARACTER SET utf8mb4 NULL;

CREATE TABLE `AttributeModels` (
    `ID` int NOT NULL AUTO_INCREMENT,
    `AttributeHash` bigint unsigned NOT NULL COMMENT 'AttributeHash',
    `PropertyHash` bigint unsigned NOT NULL COMMENT 'hash',
    `Platform_Platform` int NOT NULL COMMENT '平台类型',
    `Platform_Name` longtext CHARACTER SET utf8mb4 NOT NULL COMMENT '平台名称',
    `MarketPlaceID` longtext CHARACTER SET utf8mb4 NOT NULL COMMENT '国家/市场',
    `ProductType` longtext CHARACTER SET utf8mb4 NULL COMMENT '产品类型',
    `GroupName` longtext CHARACTER SET utf8mb4 NULL COMMENT '属性分组名称',
    `PropertyName` longtext CHARACTER SET utf8mb4 NOT NULL COMMENT '属性名称',
    `Title` longtext CHARACTER SET utf8mb4 NOT NULL COMMENT '属性标题',
    `Description` longtext CHARACTER SET utf8mb4 NOT NULL COMMENT '属性描述',
    `Type` int NOT NULL COMMENT '属性类型',
    `Required` tinyint(1) NOT NULL COMMENT '是否是必填属性',
    `ExtentData` longtext CHARACTER SET utf8mb4 NULL COMMENT '属性约束json',
    `Datas` longtext CHARACTER SET utf8mb4 NULL COMMENT '具体属性细节',
    `Version` longtext CHARACTER SET utf8mb4 NULL COMMENT '版本号',
    `State` tinyint(1) NOT NULL COMMENT '属性是否在用',
    `Locale` longtext CHARACTER SET utf8mb4 NOT NULL COMMENT '语言',
    CONSTRAINT `PK_AttributeModels` PRIMARY KEY (`ID`)
) CHARACTER SET=utf8mb4;




UPDATE `User` SET `CreatedAt` = TIMESTAMP '2022-11-05 17:45:43', `PasswordHash` = 'AQAAAAEAACcQAAAAEOhyERiFdD2UP2hknk4EjX2gM3930xtWtJBagJQ3sgk3D1Tu72ZZWbycYWb4H8fpTQ==', `UpdatedAt` = TIMESTAMP '2022-11-05 17:45:43'
WHERE `Id` = 444445;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2022-11-05 17:45:43', `PasswordHash` = 'AQAAAAEAACcQAAAAEGoReoqMkRkmV31tmWApQwFShtH2HZ0VUShwruNof2y2JtRUw7UPL+t2KFUmNP/mKw==', `UpdatedAt` = TIMESTAMP '2022-11-05 17:45:43'
WHERE `Id` = 444446;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2022-11-05 17:45:43', `PasswordHash` = 'AQAAAAEAACcQAAAAEBsay8tXtIzb2skUGqJBk1ITj5AOMWkH78m4MdHdfvPI2y5o6Qarq3xzs6TWtCHhPQ==', `UpdatedAt` = TIMESTAMP '2022-11-05 17:45:43'
WHERE `Id` = 444447;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2022-11-05 17:45:43', `PasswordHash` = 'AQAAAAEAACcQAAAAENWQHYJdNCYxwzTYLOnHvQs/uuUm34xAaAaEy6c4kK5TFlY2SabyvIhdj5duSTf61Q==', `UpdatedAt` = TIMESTAMP '2022-11-05 17:45:43'
WHERE `Id` = 444448;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2022-11-05 17:45:43', `PasswordHash` = 'AQAAAAEAACcQAAAAEOKM4kLQXYHDd/QApDZbdpa4CIiArVQPRPkhwxfacS4+8k6LjKh9JXW0nn7UwXAUjg==', `UpdatedAt` = TIMESTAMP '2022-11-05 17:45:43'
WHERE `Id` = 444449;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2022-11-05 17:45:43', `PasswordHash` = 'AQAAAAEAACcQAAAAEGm7b9f8CQztVQsZlNItKwazKIqZkXPffdMbAFMU8jVpOjAgP/Ah2HtT+o/+kWjrNQ==', `UpdatedAt` = TIMESTAMP '2022-11-05 17:45:43'
WHERE `Id` = 444450;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2022-11-05 17:45:43', `PasswordHash` = 'AQAAAAEAACcQAAAAEGuxyBbsDyzJZR6mvVf3pRrkKFVoyZDRD7Mlz1nBXUnTE1x/ZGjDC01y9CtA6/ZNog==', `UpdatedAt` = TIMESTAMP '2022-11-05 17:45:43'
WHERE `Id` = 444451;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2022-11-05 17:45:43', `PasswordHash` = 'AQAAAAEAACcQAAAAENH0gIJM9eWYUeoYO1cRuxQOHhOEL9je1iGqD54SJRx3b5pykLX8+imQy2un0sFCzA==', `UpdatedAt` = TIMESTAMP '2022-11-05 17:45:43'
WHERE `Id` = 444452;
SELECT ROW_COUNT();


UPDATE `UserGroup` SET `Sort` = -1667641543
WHERE `ID` = 1;
SELECT ROW_COUNT();


UPDATE `UserValidities` SET `CreatedAt` = TIMESTAMP '2022-11-05 17:45:43', `UpdatedAt` = TIMESTAMP '2022-11-05 17:45:43'
WHERE `ID` = 1;
SELECT ROW_COUNT();


UPDATE `UserValidities` SET `CreatedAt` = TIMESTAMP '2022-11-05 17:45:43', `UpdatedAt` = TIMESTAMP '2022-11-05 17:45:43'
WHERE `ID` = 2;
SELECT ROW_COUNT();


UPDATE `UserValidities` SET `CreatedAt` = TIMESTAMP '2022-11-05 17:45:43', `UpdatedAt` = TIMESTAMP '2022-11-05 17:45:43'
WHERE `ID` = 3;
SELECT ROW_COUNT();


INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20221105094548_t21', '6.0.5');

COMMIT;

