﻿START TRANSACTION;

ALTER TABLE `SmsCode` DROP FOREIGN KEY `FK_SmsCode_Company_CompanyID`;

ALTER TABLE `SmsCode` DROP FOREIGN KEY `FK_SmsCode_User_UserID`;

ALTER TABLE `SmsCode` DROP FOREIGN KEY `FK_SmsCode_UserGroup_GroupID`;

ALTER TABLE `SmsCode` DROP INDEX `IX_SmsCode_CompanyID`;

ALTER TABLE `SmsCode` DROP INDEX `IX_SmsCode_GroupID`;

ALTER TABLE `SmsCode` DROP INDEX `IX_SmsCode_UserID`;

ALTER TABLE `SmsCode` DROP COLUMN `CompanyID`;

ALTER TABLE `SmsCode` DROP COLUMN `GroupID`;

ALTER TABLE `SmsCode` DROP COLUMN `UserID`;


INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20230322071240_t54', '6.0.5');

COMMIT;

