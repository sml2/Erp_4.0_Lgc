﻿using ERP.Extensions;
using System.ComponentModel;

namespace ERP.Enums;

public enum Platforms
{
    [Description("未知")]
    None = 0,
    //1-9
    ElectronicBusinessMask = (1 << 10) - 1,
    ElectronicBusiness = 1,
    [Description("亚马逊")]
    AMAZON,
    [Description("其他")]
    OTHERS,
    [Description("速卖通")]
    ALIEXPRESS,
    [Description("Wish")]
    WISH,
    [Description("Shopee")]
    SHOPEE,
    [Description("EBay")]
    EBay,
    [Description("亚马逊")]
    AMAZONSP,
    //10-19
    LogisticMask = ((1 << 20) - 1) ^ ElectronicBusinessMask,
    Logistic = 1 << 10,
    [Description("前海三态")]
    SFC,
    [Description("一起飞")]
    Y7F,
    [Description("云途")]
    YunTu,
    [Description("永合国际物流")]
    HuaLei_YH,//HL
    [Description("速递管家")]
    RTB_SDGJ,
    [Description("燕文")]
    YanWen,
    [Description("递四方")]
    FPX,
    [Description("递一国际")]
    CNE_DYGJ,
    [Description("杰航国际物流")]
    K5_JHGJ,
    [Description("利通物流")]
    UBI,
    [Description("八爪鱼")]
    K5_BZY,
    [Description("华路物流")]
    K5_HL,
    [Description("德邦物流")]
    DEPPON,
    [Description("义达物流")]
    RTB_YDH,
    [Description("航泽国际")]
    CNE_HZGJ,
    [Description("纵横讯通")]
    RTB_ZHXT,
    [Description("EMS_中国邮政寄递平台")]
    EMS,
    [Description("集风仓储")]
    RTB_JFCC,
    [Description("顺丰国际")]
    SFGJ,//SF
    [Description("跨境好运小包")]
    KJHYXB,
    [Description("EQUICK")]
    EOC,
    [Description("WanbExpress")]
    WANB,
    [Description("云盒")]
    YH,
    [Description("华磊")]
    HuaLei_OWN,
}

public static class PlatformsType
{
    private static readonly Platforms[] Supports = { Platforms.AMAZONSP, Platforms.SHOPEE, Platforms.OTHERS };

    public static readonly Dictionary<int, string> platformElectronicBusiness = Supports.ToDictionary(p => p.GetNumber(), p => Extensions.EnumExtension.GetDescription(p));
}
