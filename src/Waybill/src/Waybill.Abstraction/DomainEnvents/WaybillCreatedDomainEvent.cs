using ERP.Models.DB.Identity;
using MediatR;

namespace ERP.WaybillModule.Abstraction.DomainEnvents;

/// <summary>
/// 运单创建成功触发事件
/// </summary>
public class WaybillCreatedDomainEvent : INotification
{
    public Models.DB.Logistics.Waybill Waybill { get; }
    
    /// <summary>
    /// 待出库订单产品，供仓库模块使用
    /// </summary>]
    public IEnumerable<StockOutGood>? OutGoods { get; }

    public User User { get; set; } = default!;


    public WaybillCreatedDomainEvent(Models.DB.Logistics.Waybill waybill, IEnumerable<StockOutGood>? outGoods)
    {
        Waybill = waybill;
        OutGoods = outGoods;
    }
}

public record StockOutGood(int StockProductId, int WarehouseId, int Quantity);