﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel;

namespace ERP.Enums.Logistics;

/// <summary>
///  //原ERP 3.0   100已提交200已收货201待转运300运输中400已签收500已删除501退货中502已退回503退回在仓700其他     INT类型
/// </summary>
public enum States
{
    [Description("暂无状态")]
    None = 0,
    [Description("已提交")]
    COMMITTED = 100,
    [Description("已收货")]
    RECEIVED = 200,
    [Description("待转运")]
    WAITTRANSPORT = 201,
    [Description("运输中")]
    TRANSITING = 300,
    [Description("已签收")]
    SINGED = 400,
    [Description("到达待取")]
    WAITTOPICKUP = 401,
    [Description("已删除")]
    DELETED = 500,
    [Description("退货中")]
    RETURNING = 501,
    [Description("已退回")]
    RETURNED = 502,
    [Description("退回在仓")]
    RETURNDEPOT = 503,
    [Description("投递失败")]
    DELIVERYFAILED = 504,
    [Description("包裹遗失")]
    LOST = 505,
    [Description("撤销运单")]
    Cancle = 600,
    [Description("其他")]
    OTHER = 700,
}
public enum RateTypes
{
    [Description("百分比")]
    Percentage,
    [Description("固定数值")]
    FixedNumber
}

public enum VerifyStates
{
    [Description("未确认")]
    UnConfirm,
    [Description("确认")]
    Confirm,
    [Description("已取消")]
    Cancel
}

public enum PrintSettingEnum
{
    [Description("面单文件类型")]
    FileType,
    [Description("面单内容类型")]
    ContentType,
    [Description("面单纸张类型")]
    PaperType,
}

