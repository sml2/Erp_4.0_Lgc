﻿
using System.ComponentModel;

namespace ERP.Enums.Logistics;


public enum Sources
{
    [Description("内部添加")]
    JUNIOR= 1,
    [Description("上级分配")]
    SUPERIOR= 2
}
public enum Charges
{
    [Description("对公账户")]
    PUBLIC= 1,
    [Description("自用钱包")]
    PRIVATE=2
}