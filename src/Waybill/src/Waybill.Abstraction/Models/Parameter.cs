﻿using ERP.Data;
using ERP.Enums;
using ERP.Enums.Logistics;
using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.Models.DB.Logistics
{
    /// <summary>
    /// 物流参数
    /// </summary>
    public class Parameter : UserModel
    {
        /// <summary>
        /// 物流类型
        /// </summary>
        [Comment("物流类型")]

        public PlatformInfo Platform { get; set; } = PlatformInfo.Empty;

        /// <summary>
        /// 物流参数
        /// </summary>
        [Comment("物流参数")]
        [Column(TypeName = "JSON")]
        public string? Data { get; set; }

        /// <summary>
        /// 自定义名称
        /// </summary>
        [Comment("自定义名称")]
        public string CustomizeName { get; set; }=string.Empty;

        /// <summary>
        /// 备注
        /// </summary>

        [Comment("备注")]
        public string? Remark { get; set; }

        /// <summary>
        /// 最后一条运单时间
        /// </summary>

        [Comment("最后一条运单时间")]
        public DateTime LastTime { get; set; }

        /// <summary>
        /// 上级分配物流原ID
        /// </summary>
        [Comment("上级分配物流原id")]
        public int ParentID { get; set; }
        /// <summary>
        /// 使用状态 1启用 2禁用
        /// </summary>

        [Comment("使用状态")]
        public bool Opened { get; set; }  //Erp3.0使用状态 1启用 2禁用"
        /// <summary>
        /// 数据来源 1内部添加 2上级分配
        /// </summary>

        public Sources Source { get; set; }

        /// <summary>
        /// 分配物流扣款放 1对公账户 2自用钱包
        /// </summary>

        [Comment("分配物流扣款放 1对公账户 2自用钱包")]
        public Charges Charge { get; set; }

        /// <summary>
        /// ERP3.0 系统物流状态 1有效2无效
        /// </summary>
        
        [Comment("系统物流状态")]
        public bool IsSysState { get; set; }

    }
}


//------------------------------
//--Table structure for erp3_logistics_param
//------------------------------
//DROP TABLE IF EXISTS `erp3_logistics_param`;
//CREATE TABLE `erp3_logistics_param`  (
//  `id` bigint unsigned NOT NULL,
//  `i_logistics_id` int unsigned NOT NULL COMMENT '物流id',
//  `d_data` json NULL COMMENT '物流参数',
//  `i_company_id` int unsigned NOT NULL COMMENT '公司id',
//  `d_customize_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '自定义名称',
//  `d_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '备注',
//  `i_user_id` int unsigned NOT NULL COMMENT '用户id',
//  `i_group_id` int unsigned NOT NULL COMMENT '用户组id',
//  `i_oem_id` int unsigned NOT NULL COMMENT 'oemid',
//  `t_state` tinyint unsigned NOT NULL COMMENT '使用状态 1启用 2禁用',
//  `t_is_sys_state` tinyint unsigned NOT NULL COMMENT '系统物流状态 1有效2无效',
//  `t_source` tinyint unsigned NOT NULL COMMENT '数据来源 1内部添加 2上级分配',
//  `i_pid` int unsigned NOT NULL COMMENT '上级分配物流原id',
//  `t_charge` tinyint unsigned NOT NULL COMMENT '分配物流扣款方 1对公账户 2自用钱包',
//  `d_last_time` timestamp(0) NULL DEFAULT NULL COMMENT '最后一条运单时间',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
// PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `i_group_id`(`i_group_id`) USING BTREE,
// INDEX `i_user_id`(`i_user_id`) USING BTREE,
// INDEX `is_sys_state`(`t_is_sys_state`) USING BTREE,
// INDEX `logistics_id`(`i_logistics_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `pid`(`i_pid`) USING BTREE,
// INDEX `source`(`t_source`) USING BTREE,
// INDEX `state`(`t_state`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 602 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '运单管理_物流参数' ROW_FORMAT = Dynamic;
