using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.InteropServices;
using ERP.Data;
using ERP.Extensions;
using ERP.Models.DB.Users;
using ERP.Models.Finance;
using Ryu.Data;
using Ryu.Extensions;

namespace ERP.Models.DB.Logistics;

using Enums.Logistics;
using Models.DB.Identity;

//[Index(nameof(CommandID), nameof(Express), nameof(IsVerify), nameof(NationId), nameof(OperatecompanyID), nameof(OrderNo), nameof(PlatformID),
//    nameof(Tracking), nameof(WaybillCompanyID), nameof(WaybillOrder), nameof(State))]


[Index(nameof(LastAutoShipTime))]
// [Index("Logistic_Platform",nameof(LastAutoShipTime),nameof(CarriageNum))]
public class Waybill : UserModel
{
    public Waybill()
    {
        _logs = new List<WaybillLog>();
    }

    [Comment("订单id")]
    public int OrderId { get; set; }
    [Comment("订单号")]
    public string? OrderNo { get; set; }

    public int? MergeID { get; set; }

    public string? OrderIDs { get; set; }

    public PlatformInfo ElectronicBusiness { get; set; } = PlatformInfo.Empty;
    //public PlatformInfo Logistic = PlatformInfo.Empty;
    [Comment("平台id")]
    public PlatformInfo Logistic { get; set; } = PlatformInfo.Empty;

    public int? LogisticParamID { get; set; }
    public Parameter? LogisticParam { get; set; }
    
    [Comment("物流参数")] //[Column(TypeName = "json")]
    public string LogisticParamsData { get; set; } = string.Empty;
    [Comment("物流附加参数  配送方式/海关简称(中文)/海关简称(英文)/海关编码/申报货币单位/保险货币单位/保险数值/总重(单位克)/包裹长单位厘米(cm)/包裹宽单位厘米(cm)/包裹高单位厘米(cm)/包裹的数量")]//[Column(TypeName = "json")]
    public string LogisticExt { get; set; } = string.Empty;
    public string? ImgUrl { get; set; }

    [Comment("订单收件人所属国家id")]
    public int? NationId { get; set; }

    //[Comment("第三方订单号")]
    //public string? MiddlemanOrder { get; set; } = string.Empty;

    [Comment("运单面单号")]
    public string? WaybillOrder { get; set; } = string.Empty;
    [Comment("运单订单号")]
    public string Express { get; set; } = string.Empty;
    [Comment("面单追踪号")]
    public string? Tracking { get; set; } = string.Empty;
    [Comment("发货日期")]
    public DateTime DeliverDate { get; set; }
    [Comment("备注")]
    public string? Remark { get; set; }
    public string Truename { get; set; } = string.Empty;

    [Comment("日志id")]
    public int? BillLogId { get; set; }
    public BillLog? BillLog { get; set; }

    //public WaybillLog waybillLog { get; set; }
    [Comment("运单产品信息purchase_id 采购商品idnumber采购商品数img_url产品图片urlproname产品名称from_url产品订单来源地址")]
    [Column(TypeName = "JSON")]
    public string? ProductInfo { get; set; }     //JSON
    [Comment("物流追踪信息timeline时间节点message物流信息pageage_status包裹状态")]
    [Column(TypeName = "Text")]
    public string? TrackInfo { get; set; }    //text
    /// <summary>
    /// //ERP 3.0 INT 100已提交200已收货201待转运300运输中400已签收500已删除501退货中502已退回503退回在仓700其他   
    /// </summary>
    [Comment("物流状态")]
    public States State { get; set; }

    [NotMapped]
    public string StateDes => Extensions.EnumExtension.GetDescription(State);
    [Comment("物流状态按钮名称")]
    public string ButtonName { get; set; } = string.Empty;
    [Comment("寄件人信息")]//Shipper
    //[Column(TypeName = "json")]
    public string Sender { get; set; } = string.Empty; //Json类型
    [Comment("基础运费(基准货币)")]
    public MoneyRecordFinancialAffairs CarriageBase { get; set; } = MoneyRecordFinancialAffairs.Empty;
    [Comment("其他费用(基准货币)")]
    public MoneyRecordFinancialAffairs CarriageOther { get; set; } = MoneyRecordFinancialAffairs.Empty;
    [Comment("最终运费数值(基准货币)")]
    public MoneyRecordFinancialAffairs CarriageNum { get; set; } = MoneyRecordFinancialAffairs.Empty;

    [Comment("卖家店铺id（平台-商店)")]
    public int StoreId { get; set; }
    [Comment("货运方式")]
    public string ShipMethod { get; set; } = string.Empty;
    public string? CarriageUnit { get; set; } = string.Empty;
    [Comment("添加处理公司id")]
    public int OperatecompanyID { get; set; }
    [Comment("物流上报公司id")]
    public int? WaybillCompanyID { get; set; }
    /// <summary>
    /// 本系统运单是否确认（非接口结果）
    /// </summary>
    [Comment("0.未确认 1.确认 2.已取消")]
    public VerifyStates IsVerify { get; set; }   //ERP3.0  tinyint    ISVERIFY_NO = 1;     ISVERIFY_YES = 2;
    [Comment("运费手续费(基准货币)[分销用字段]")]
    public decimal WaybillRate { get; set; }
    
    [Comment("运单预冻结金额[分销用字段]")]
    public Money? WaybillFreeze { get; set; }
    [Comment("运单预冻结是否返还[分销用字段]")]
    public Boolean IsReturnFreeze { get; set; } = false;
    [Comment("运单扣费时间(物流平台返回)")]
    public DateTime? DeductionTime { get; set; }
    
    [Comment("自动拉取触发的时间")]
    public DateTime? LastAutoShipTime { get; set; }
    
    [Comment("运费手续费类型1百分比2固定数值[分销用字段]")]
    public RateTypes WaybillRateType { get; set; }  //ERP3.0 tinyint     RATETYPE_BAR = 1;  RATETYPE_NUM = 2;

    [Comment("问题")]
    [Column(TypeName = "json")]
    public string Problem { get; set; } = "[]";   //JSONnA
    [Comment("采购主键ID(逗号连接)")]
    public string? PurchaseTrackingNumber { get; set; }
    
    public int? GroupID { get; set; }

    private readonly List<WaybillLog> _logs;
    public IReadOnlyCollection<WaybillLog> Logs => _logs;

    public IWallet GetUsedWallet(Company walletCompany ,User operateUser)
    {
        var belongs = CalcCostWallet(operateUser);

        if (belongs == BillLog.Belongs.SELFUSE)
            return new SelfWallet(walletCompany, operateUser);

        return new PublicWallet(walletCompany, operateUser);
    }
    
    /// <summary>
    /// 运单确认对公、个人钱包消耗使用逻辑：
    /// 1. 普通用户即不是分销的运单使用个人钱包
    /// 2. 分销上级并且是分销运单使用对公钱包
    /// 3. 分销下级根据分配的物流方式使用，钱包消耗方式(charge)
    /// </summary>
    /// <returns></returns>
    public BillLog.Belongs CalcCostWallet(User operateUser)
    {
        var result = BillLog.Belongs.SELFUSE;

        // FIXME 这里还会出现问题，如果添加运单方和确认方不同，会按照确认方处理，
        //       例上级添加的运单，下级进行确认，这时候会根据物流分配处理，可能出现扣下级个人钱包的情况
        
        // 判断是否是上报的运单并且当前公司为上级
        if (WaybillCompanyID != 0)
        {
            result = BillLog.Belongs.RIGHT;
        }
        else if (operateUser.IsDistribution)
        {
            // 如果下级使用了上级物流并且没有上报，该如何处理
            // 查询运单物流信息
            if (LogisticParam is { ParentID: > 0, Charge: Charges.PUBLIC })
            {
                result = BillLog.Belongs.RIGHT;
            }
        }
        return result;
    }

    /// <summary>
    /// 添加运单日志
    /// </summary>
    public void AddLog(User operateUser, string action)
    {
        var log = new WaybillLog()
        {
            CompanyID = CompanyID,
            OEMID = OEMID,
            GroupID = operateUser.GroupID ,//== GroupID ? GroupID : 0,
            UserID = operateUser.Id,
            UserName = operateUser.UserName,
            Truename = operateUser.TrueName,
            Platform = Logistic,
            Action = action,
        };
        
        _logs.Add(log);
    }
}

public class WaybillProblems
{
    public int Id { get; set; }
    public string Problem { get; set; } = string.Empty;
    public string User { get; set; } = string.Empty;//name
    public DateTime Time { get; set; }
}

//-- ----------------------------
//-- Table structure for erp3_waybill
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_waybill`;
//CREATE TABLE `erp3_waybill`  (
//  `id` bigint unsigned NOT NULL,
//  `i_order_id` int unsigned NOT NULL COMMENT '订单id',
//  `i_logistic_id` int unsigned NOT NULL COMMENT '物流id',
//  `d_logistic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '物流名称',
//  `i_nation_id` int unsigned NOT NULL COMMENT '订单收件人所属国家id',
//  `d_waybill_order` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '运单面单号',
//  `d_express` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '运单订单号',
//  `d_tracking` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '面单追踪号',
//  `d_deliver_date` datetime(0) NULL DEFAULT NULL COMMENT '发货日期',
//  `d_remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '备注',
//  `i_user_id` int unsigned NOT NULL COMMENT '添加用户id',
//  `d_truename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '添加用户姓名',
//  `i_bill_log_id` int unsigned NOT NULL COMMENT '账单id',
//  `i_platform_id` int unsigned NOT NULL COMMENT '所属平台id',
//  `d_order_no` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '订单编号',
//  `d_img_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '订单产品图url',
//  `d_product_info` json NULL COMMENT '运单产品信息purchase_id 采购商品idnumber采购商品数img_url产品图片urlproname产品名称from_url产品订单来源地址',
//  `d_track_info` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '物流追踪信息timeline时间节点message物流信息pageage_status包裹状态',
//  `d_logistic_params` json NULL COMMENT '物流参数',
//  `d_logistic_ext` json NULL COMMENT '物流附加参数  配送方式/海关简称(中文)/海关简称(英文)/海关编码/申报货币单位/保险货币单位/保险数值/总重(单位克)/包裹长单位厘米(cm)/包裹宽单位厘米(cm)/包裹高单位厘米(cm)/包裹的数量',
//  `t_waybill_state` int unsigned NOT NULL COMMENT '物流状态100已提交200已收货201待转运300运输中400已签收500已删除501退货中502已退回503退回在仓700其他',
//  `d_button_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '物流状态按钮名称',
//  `d_sender` json NULL COMMENT '寄件人信息',
//  `d_carriage_base` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '基础运费(基准货币)',
//  `d_carriage_other` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '其他费用(基准货币)',
//  `d_carriage_num` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '最终运费数值(基准货币)',
//  `d_carriage_unit` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '运费货币单位',
//  `i_store_id` int unsigned NOT NULL COMMENT '卖家店铺id（平台-商店）',
//  `d_ship_method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '货运方式',
//  `t_is_verify` tinyint unsigned NOT NULL COMMENT '确认1否2是',
//  `d_waybill_rate` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '运费手续费(基准货币)',
//  `t_waybill_rate_type` tinyint unsigned NOT NULL COMMENT '运费手续费类型1百分比2固定数值',
//  `d_rate` double (11, 4) UNSIGNED NOT NULL DEFAULT 0.0000 COMMENT '汇率(添加运单当时汇率)',
//  `d_problem` json NULL COMMENT '问题',

//  `u_purchase_tracking_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '采购主键ID(逗号连接)',
