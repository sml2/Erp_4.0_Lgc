﻿
using Microsoft.EntityFrameworkCore;
namespace ERP.Data;
using Enums;
using Extensions;

[Owned]
[Index(nameof(Platform))]
public record PlatformInfo
{
    
    public PlatformInfo() { }
    public PlatformInfo(Platforms platform) {
        Platform = platform;
    }

    public PlatformInfo(string name)
    {
        Name = name;
    }
    public static PlatformInfo Empty => new();
    private Platforms Platform_ = Platforms.None;
    [Comment("平台类型")]
    public Platforms Platform { get => Platform_; set { Platform_ = value; if(value != Platforms.None) Name = value.GetDescription(); } }
    [Comment("平台名称")]
    public string Name { get; set; } = string.Empty;

    public int GetID()
    {
        return Platform_.GetID();
    }

    public static implicit operator PlatformInfo(Platforms platform) => new(platform);
    public static implicit operator Platforms(PlatformInfo  platformInfo) => platformInfo.Platform;
}
