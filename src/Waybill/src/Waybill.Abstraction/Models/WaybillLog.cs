using ERP.Data;
using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.DB.Logistics;

[Comment("运单管理_运单操作")
, Index(nameof(WaybillId))
, Index(nameof(OrderId))]
public class WaybillLog : UserModel
{
    [Comment("运单id")]
    public int WaybillId { get; set; }

    public Waybill? Waybill { get; set; }

    [Comment("用户")]
    public string UserName { get; set; } = string.Empty;
    [Comment("动作内容")]
    public string Action { get; set; } = string.Empty;
    [Comment("订单id")]
    public int OrderId { get; set; }
    [Comment("平台id")]
    public PlatformInfo? Platform { get; set; }
    [Comment("操作用户姓名")]
    public string Truename { get; set; } = string.Empty;

}

