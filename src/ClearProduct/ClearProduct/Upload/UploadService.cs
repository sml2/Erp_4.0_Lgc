using System.Net;
using System.Text.RegularExpressions;
using ERP.Data;
using ERP.Exceptions.Product;
using ERP.Exceptions.Upload;
using ERP.Interface;
using ERP.Models.DB.Identity;
using ERP.Models.Product;
using ERP.Models.Setting;
using Microsoft.AspNetCore.Http;
using Model = ERP.Data.Products.Product;
using Newtonsoft.Json;

namespace ERP.Services.Upload;

public class UploadService : BaseService
{
    private readonly string _imageFileTypes =
        "image/bmp,image/jpeg,image/jpg,,image/png,image/x-icon,image/vnd.microsoft.icon,bmp,jpeg,png,jpg";

    private readonly Dictionary<string, string> _contentTypeToExtension = new Dictionary<string, string>()
    {
        { "image/bmp", ".bmp" },
        { "image/jpeg", ".jpeg" },
        { "image/jpg", ".jpg" },
        { "image/png", ".png" },
        { "image/x-icon", ".icon" },
        { "image/vnd.microsoft.icon", ".icon" },
    };

    private object _tempImagesService;
    private readonly ILogger logger;


    public UploadService(IOemProvider oemProvider, IHostEnvironment environment,
        ISessionProvider sessionProvider,ILogger<UploadService> logger) : base(oemProvider, environment, logger)
    {               
    }
    
    public UploadService(FileBucket model,IHostEnvironment environment) : base(model,environment)
    {               
    }

    public async Task<string> UploadImageByNetwork(string url, int oemId, int userId)
    {
        Path.GetExtension(url);

        WebRequest wreq = WebRequest.Create(url);
        HttpWebResponse wresp = (HttpWebResponse)wreq.GetResponse();
        Stream s = wresp.GetResponseStream();

        if (_imageFileTypes.Contains(wresp.ContentType, StringComparison.CurrentCultureIgnoreCase))
        {
            var lowerContentType = wresp.ContentType.ToLower();
            if (_contentTypeToExtension.Keys.Contains(lowerContentType))
            {
                string filePath = await FilePath(oemId, userId, _contentTypeToExtension[lowerContentType]);
                //保存图片
                var state = await PutStream(filePath, s);
                if (state)
                {
                    return filePath;
                }
            }
        }

        throw new UploadException("上传图片格式有问题！支持的图片格式有：" + _imageFileTypes);
    }

    public async Task<string> UploadImageByBase64(string base64, string extension, int oemId, int userId)
    {
        if (_imageFileTypes.Contains(extension, StringComparison.CurrentCultureIgnoreCase))
        {
            var reg = new Regex("data:image/(.*);base64,");
            var imageBase64 = reg.Replace(base64, "");
            byte[] imageByte = Convert.FromBase64String(imageBase64);
            var stream = new MemoryStream(imageByte);

            string filePath = await FilePath(oemId, userId, extension.StartsWith('.') ? extension : $".{extension}");

            var state = await PutStream(filePath, stream);
            if (state)
            {
                return filePath;
            }
        }


        throw new UploadException("上传图片格式有问题！支持的图片格式有：" + _imageFileTypes);
    }

    public async Task<HttpWebResponse> DownloadImage(string url)
    {
        Path.GetExtension(url);

        WebRequest wreq = WebRequest.Create(url);
        HttpWebResponse wresp = (HttpWebResponse)wreq.GetResponse();
        if (!_imageFileTypes.Contains(wresp.ContentType, StringComparison.CurrentCultureIgnoreCase))
        {
            throw new UploadException("图片格式有问题！支持的图片格式有：" + _imageFileTypes);
        }

        return wresp;
    }

    public async Task<string> UploadImageByStream(IFormFile file, int oemId, int userId)
    {
        if (file.Length > 0)
        {
            string fileTypes = _imageFileTypes;
            if (fileTypes.Contains(file.ContentType, StringComparison.CurrentCultureIgnoreCase))
            {
                string filePath = await FilePath(oemId, userId, Path.GetExtension(file.FileName));
                //保存图片
                var state = await PutStream(filePath, file.OpenReadStream());
                if (state)
                {
                    return filePath;
                }
            }

            throw new UploadException("上传图片格式有问题！支持的图片格式有：" + fileTypes);
        }

        throw new UploadException("上传图片无大小");
    }

    public async Task<object?> PutStream(IFormFile file, int oemId, int userId)
    {
        string filePath = await FilePath(oemId, userId, Path.GetExtension(file.FileName));
        //保存图片
        var state = await PutStream(filePath, file.OpenReadStream());

        if (state)
        {
            return new
            {
                Url = await GetUrl(filePath)
            };
        }

        return null;
    }

    public async Task<object?> PutStream(Model model, int oemId, int userId)
    {
        string filePath = await FilePath(oemId, userId, model.Pid);
        var obj = JsonConvert.SerializeObject(model);
        using (var memery = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(obj)))
        {
            var state = await PutStream(filePath, memery);
            if (state)
            {
                return new
                {
                    Url = GetUrl(filePath)
                };
            }
        }

        return null;
    }
    
    public async Task<string> GetImagePath(string path, ProductModel model)
    {
        var pathArr = path.Split('.');

        return await FilePath(model.OEMID, model.UserID, pathArr[pathArr.Length - 1]);
    }
    
    // public async Task<string> UploadStructFile(string jsonProduct, long version, int id,User user)
    // {
    //     var jsonFilePath = GetProductJsonPath(version, id,user);
    //
    //     var uploadState = await PutAsync(jsonFilePath, jsonProduct);
    //
    //     if (!uploadState)
    //     {
    //         throw new SavedException("数据包存储失败");
    //     }
    //
    //     return jsonFilePath;
    // }
    
    // public string GetProductJsonPath(long version, int pid,User user)
    // {
        //userId作为路径文件名的组成可用于产品编辑日志所属人；
        // var t = DateTimeOffset.FromUnixTimeMilliseconds(version);
        // return $"{user.OEMID}/{user.CompanyID}/products/{t.Year:0000}/{t.Month:00}/{t.Day:00}/{pid}/v_{user.ID}_{version}.json";

        //userId作为路径会存在问题，A用户修改B用户的产品信息新增一个版本的json文件，在回滚历史版本的时候会导致文件找不到
        // return $"{_oemId}/{t.Year:0000}/{t.Month:00}/{t.Day:00}/{_companyId}/{_userId}/products/{pid}/v_{version}.json";
    // }
}