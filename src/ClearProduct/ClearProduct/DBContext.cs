﻿using ERP.Extensions;
using ERP.Interface;
using ERP.Interface.Rule;
using ERP.Models;
using ERP.Models.DB.Identity;
using ERP.Models.DB.Product;
using ERP.Models.DB.Statistics;
using ERP.Models.DB.Users;
using ERP.Models.Product;
using ERP.Models.Setting;
using Microsoft.EntityFrameworkCore;

namespace ERP.Data;

public class DBContext : BaseDBContext
{
    public DbSet<OEM> OEM { get; set; } = null!;
    public DbSet<User> User { get; set; } = null!;
    public DbSet<Group> UserGroup { get; set; } = null!;
    public DbSet<Company> Company { get; set; } = null!;
    public DbSet<ProductModel> Product { get; set; } = null!;
    public DbSet<ProductItemModel> ProductItem { get; set; } = null!;
    public DbSet<TempImagesModel> TempImages { get; set; } = null!;
    public DbSet<ResourceModel> Resource { get; set; } = null!;
    public DbSet<StatisticModel> Statistic { get; set; } = null!;

    public DBContext(DbContextOptions<DBContext> options, IOemProvider oemProvider) : base(options, oemProvider)
    {
        this.HandleDataChanged();
    }
}