using ERP.Models.DB.Identity;
using ERP.Models.DB.Users;
using ERP.Models.Product;

namespace ClearProduct.ViewModels;

public class DataBelongDto
{
    public DataBelongDto(Company info)
    {
        CompanyId = info.ID;
        OemId = info.OEMID;
    }

    public DataBelongDto(int userId, int? groupId, int companyId, int oemId, int storeId)
    {
        UserId = userId;
        if (groupId != 1)
        {
            GroupId = groupId;
        }

        CompanyId = companyId;
        OemId = oemId;
        StoreId = storeId;
    }




    public DataBelongDto(User m)
    {
        CompanyId = m.CompanyID;
        OemId = m.OEMID;
        UserId = m.Pid;
    }

    public DataBelongDto(User m, bool isNew)
    {
        CompanyId = m.CompanyID;
        OemId = m.OEMID;
        UserId = m.Id;
    }

    public DataBelongDto(ProductModel m)
    {
        UserId = m.UserID;
        if (m.GroupID != 1)
        {
            GroupId = m.GroupID;
        }

        CompanyId = m.CompanyID;
        OemId = m.OEMID;
    }
    public int? UserId { get; set; }
    public int? GroupId { get; set; }
    public int? CompanyId { get; set; }
    public int OemId { get; set; }
    public int? StoreId { get; set; }
    public List<int>? ReportIds { get; set; }
}