using ClearProduct;
using ClearProduct.Service;
using ERP.Data;
using ERP.Interface;
using ERP.Models.Setting;
using ERP.Services.FileStorage;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NLog.Web;

var host = Host.CreateDefaultBuilder(args)
    .ConfigureLogging(builder => builder.ClearProviders())
    .UseNLog()
    .ConfigureAppConfiguration(builder => builder.AddEnvironmentVariables("ERP_"))
    .ConfigureServices((context, services) =>
    {
        //过期移入回收站
        // services.AddHostedService<RecycleBackground>();
        //标记删除 逻辑删除
        // services.AddHostedService<MarkDeletionBackground>();
        //删除 物理删除
        services.AddHostedService<ClearProductBackground>();
        services.AddScoped<BaseService>();
        services.AddScoped<ClearProductService>();
        services.AddScoped<RecycleProductService>();
        services.AddScoped<MarkDeletionService>();
        services.AddScoped<StatisticService>();
        services.AddSingleton<IOemProvider, NullOemProvider>();
        //
        var configuration = context.Configuration;
        configuration.GetSection("Disk").Bind(FileSystemFactory.diskConfig);
        AddDbContext(services, configuration);
    })
    .Build();


host.Run();

static void AddDbContext(IServiceCollection services, IConfiguration configuration)
{
    var connectionStr = configuration.GetConnectionString("mysql");
    if (string.IsNullOrWhiteSpace(connectionStr))
        throw new Exception("没有配置mysql数据库连接字符串");

    services.AddDbContext<DBContext>(options =>
    {
        options.UseMySql(connectionStr, ServerVersion.AutoDetect(connectionStr)).EnableSensitiveDataLogging()
            .EnableDetailedErrors();
    });
}

internal class NullOemProvider : IOemProvider
{
    public OEM? OEM => null;
}