using ClearProduct.Service;
using ERP.Data;
using ERP.Enums.Product;
using ERP.Extensions;
using Microsoft.EntityFrameworkCore;

namespace ClearProduct;

public class MarkDeletionBackground : BackgroundService
{
    private readonly IServiceProvider _provider;
    private readonly ILogger<MarkDeletionBackground> _logger;

    public MarkDeletionBackground(IServiceProvider provider, ILogger<MarkDeletionBackground> logger)
    {
        _provider = provider;
        _logger = logger;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        const int batch = 1;
        const int interval = 10_000;
        const int residence = 7;
        const int expire = 6;

        while (!stoppingToken.IsCancellationRequested)
        {
            var now = DateTime.Now;
            var validity = now.AddMonths(-expire).AddDays(-residence - 1);
            var scope = _provider.CreateScope().ServiceProvider;
            var dbContext = scope.GetRequiredService<DBContext>();
            var markDeletionService = scope.GetRequiredService<MarkDeletionService>();
            var delayed = await markDeletionService.GetDelayed(expire);

            _logger.LogInformation("==========================Start==========================");
            _logger.LogInformation("{now}开始执行将回收站中超过滞留期限的产品标记为待删除", now);
            _logger.LogInformation("创建时间在此时间【{validity}】之前为需清理的产品", validity.ToString("yyyy-MM-dd"));

            if (delayed.Count == 0)
            {
                _logger.LogInformation("无客户存在延期业务");
            }
            else
            {
                _logger.LogInformation("{count}个客户延期有效", delayed.Count);
                foreach (var item in delayed)
                {
                    _logger.LogInformation("客户【{UserId}】延期至{Validity}", item.UserId,
                        item.Validity.ToString("yyyy-MM-dd"));
                }
            }

            var delayedUserId = delayed.Select(m => m.UserId).ToList();

            //已经移入回收站后开通延期业务，不考虑从回收站恢复。因为不确定是脚本将其移动到回收站还是用户主动移入。恢复操作交给用户主动选择操作
            var products = await dbContext.Product
                .Where(m => m.CreatedAt <= validity.LastDayTick())
                .Where(m => (m.Type & (long)Types.Recycle) == (long)Types.Recycle)
                .Where(m => (m.Type & (long)Types.Delete) != (long)Types.Delete)
                .OrderBy(m => m.ID)
                .Where(m => !delayedUserId.Contains(m.UserID))
                .Take(batch)
                .ToArrayAsync(stoppingToken);
    

            if (!products.Any())
            {
                _logger.LogInformation("当前无超过滞留期限产品");
                _logger.LogInformation("===========================End===========================");
                await Task.Delay(interval, stoppingToken);
                continue;
            }

            while (products.Any())
            {
                try
                {
                    _logger.LogInformation("本次标记待删除产品数量为【{Count}】;", products.Length);
                    foreach (var product in products)
                    {
                        if (stoppingToken.IsCancellationRequested)
                            break;

                        var pUserId = product.UserID;
                        var pid = product.ID;
                        _logger.LogInformation("开始标记删除；用户ID【{UserID}，产品ID【{Pid}】;", pUserId, pid);
                        await markDeletionService.Handle(product);
                        _logger.LogInformation("标记删除成功，用户ID【{UserID}，产品ID【{Pid}】;", pUserId, pid);
                    }

                    _logger.LogInformation("===========================End===========================");
                    break;
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "标记待删除报错");
                    _logger.LogInformation("===========================End===========================");
                    break;
                }
            }
        }
    }
}