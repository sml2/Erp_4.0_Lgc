using ClearProduct.ViewModels;
using ERP.Data;
using ERP.Enums.Statistics;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.DB.Identity;
using ERP.Models.DB.Statistics;
using ERP.Models.DB.Users;
using ERP.Models.Setting;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace ClearProduct.Service;

using Model = StatisticModel;

public class StatisticService
{
    private readonly DBContext _dbContext;

    public enum NumColumnEnum
    {
        #region 产品管理

        AiMatting,
        ApiTranslate,
        ApiTranslateLanguage,
        ApiTranslateProduct,
        DiskAdd,
        DiskDel,
        DiskSize,
        ProductInsert,
        ProductDelete,
        ProductCount,
        ProductRecovery,

        #endregion

        #region 下级上报

        ReportProductCount,

        #endregion
    }

    private Dictionary<NumColumnEnum, string> NumColumns = new()
    {
        #region 产品管理

        { NumColumnEnum.AiMatting, nameof(Model.AiMatting) },
        { NumColumnEnum.ApiTranslate, nameof(Model.ApiTranslate) },
        { NumColumnEnum.ApiTranslateLanguage, nameof(Model.ApiTranslateLanguage) },
        { NumColumnEnum.ApiTranslateProduct, nameof(Model.ApiTranslateProduct) },
        { NumColumnEnum.DiskAdd, nameof(Model.DiskAdd) },
        { NumColumnEnum.DiskDel, nameof(Model.DiskDel) },
        { NumColumnEnum.DiskSize, nameof(Model.DiskSize) },
        { NumColumnEnum.ProductInsert, nameof(Model.ProductInsert) },
        { NumColumnEnum.ProductDelete, nameof(Model.ProductDelete) },
        { NumColumnEnum.ProductCount, nameof(Model.ProductCount) },
        { NumColumnEnum.ProductRecovery, nameof(Model.ProductRecovery) },

        #endregion

        #region 下级上报

        { NumColumnEnum.ReportProductCount, nameof(Model.ReportProductCount) },

        #endregion
    };

    private Dictionary<NumColumnEnum, NumColumnEnum> ReportRelation = new()
    {
        { NumColumnEnum.ProductCount, NumColumnEnum.ReportProductCount },
    };


    private Dictionary<NumColumnEnum, string> NumColumnMessageTitles = new()
    {
        #region 产品管理

        { NumColumnEnum.AiMatting, "抠图次数" },
        { NumColumnEnum.ApiTranslate, "翻译次数" },
        { NumColumnEnum.ApiTranslateLanguage, "产品新增语种数量" },
        { NumColumnEnum.ApiTranslateProduct, "产品翻译次数" },
        { NumColumnEnum.DiskAdd, "新增硬盘开销" },
        { NumColumnEnum.DiskDel, "释放硬盘开销" },
        { NumColumnEnum.DiskSize, "当前硬盘开销" },
        { NumColumnEnum.ProductInsert, "新增产品数" },
        { NumColumnEnum.ProductDelete, "删除产品数" },
        { NumColumnEnum.ProductCount, "当前产品数" },
        { NumColumnEnum.ProductRecovery, "回收站产品数" },

        #endregion

        #region 下级上报

        { NumColumnEnum.ReportProductCount, "下级上报产品数量" },

        #endregion
    };

    public StatisticService(DBContext dbContext)
    {
        _dbContext = dbContext;
    }


    public async Task Increase(DataBelongDto dataBelongDto, NumColumnEnum countColumn, int num = 1)
    {
        await Increment(dataBelongDto, countColumn, num);
        //多级上报
        await ReportInc(dataBelongDto, countColumn, true, num);
    }

    public async Task Decrease(DataBelongDto dataBelongDto, NumColumnEnum countColumn, int num = 1)
    {
        await Decrement(dataBelongDto, countColumn, num);
        //多级上报
        await ReportInc(dataBelongDto, countColumn, false, num);
    }

    private async Task ReportInc(DataBelongDto dataBelongDto,
        NumColumnEnum countColumn, bool isPlus, int num = 1)
    {
        if (ReportRelation.Keys.Contains(countColumn))
        {
            var companyInfo =
                await _dbContext.Company.Where(m => m.ID == dataBelongDto.CompanyId).FirstOrDefaultAsync();
            if (companyInfo == null)
            {
                throw new Exception($"找不到相关公司CompanyId[{dataBelongDto.CompanyId}]");
            }

            //多级上报
            var reportIds = JsonConvert.DeserializeObject<List<int>>(companyInfo.Pids);

            if (reportIds is not null)
                foreach (var id in reportIds)
                {
                    if (isPlus)
                    {
                        await Plus(await GetStatisticInfo(id, Types.Company, Types.DAY),
                            ReportRelation[countColumn], num);
                    }
                    else
                    {
                        await Cut(await GetStatisticInfo(id, Types.Company, Types.DAY),
                            ReportRelation[countColumn], num);
                    }
                }
        }
    }


    private async Task Increment(DataBelongDto dataBelongDto, NumColumnEnum columnEnum, int num = 1)
    {
        if (dataBelongDto.UserId.HasValue)
        {
            await Plus(await GetStatisticInfo(dataBelongDto.UserId,
                Types.User, Types.DAY), columnEnum, num);
        }

        if (dataBelongDto.GroupId.HasValue)
        {
            await Plus(await GetStatisticInfo(dataBelongDto.GroupId,
                Types.UserGroup, Types.DAY), columnEnum, num);
        }

        if (dataBelongDto.StoreId.HasValue)
        {
            await Plus(await GetStatisticInfo(dataBelongDto.StoreId,
                Types.Store, Types.DAY), columnEnum, num);
        }

        if (dataBelongDto.CompanyId.HasValue)
        {
            await Plus(await GetStatisticInfo(dataBelongDto.CompanyId,
                Types.Company, Types.DAY), columnEnum, num);
        }

        await Plus(await GetStatisticInfo(dataBelongDto.OemId,
            Types.OEM, Types.DAY), columnEnum, num);
    }

    private async Task Decrement(DataBelongDto dataBelongDto, NumColumnEnum columnEnum, int num = 1)
    {
        if (dataBelongDto.UserId.HasValue)
        {
            await Cut(await GetStatisticInfo(dataBelongDto.UserId,
                Types.User, Types.DAY), columnEnum, num);
        }

        if (dataBelongDto.GroupId.HasValue)
        {
            await Cut(await GetStatisticInfo(dataBelongDto.GroupId,
                Types.UserGroup, Types.DAY), columnEnum, num);
        }

        if (dataBelongDto.StoreId.HasValue)
        {
            await Cut(await GetStatisticInfo(dataBelongDto.StoreId,
                Types.Store, Types.DAY), columnEnum, num);
        }

        if (dataBelongDto.CompanyId.HasValue)
        {
            await Cut(await GetStatisticInfo(dataBelongDto.CompanyId,
                Types.Company, Types.DAY), columnEnum, num);
        }

        await Cut(await GetStatisticInfo(dataBelongDto.OemId,
            Types.OEM, Types.DAY), columnEnum, num);
    }


    /// <summary>
    /// 增量上报
    /// </summary>
    /// <param name="m"></param>
    /// <param name="column"></param>
    /// <param name="num"></param>
    /// <returns></returns>
    private async Task<bool> Plus(Model m, NumColumnEnum column, int num = 1)
    {
        _dbContext.Entry(m).Property<int>(NumColumns[column]).CurrentValue += num;
        var state = await _dbContext.SaveChangesAsync() > 0;

        if (!state)
        {
            throw new Exception("增量上报发生异常");
        }

        return state;
    }


    /// <summary>
    /// 减量上报
    /// </summary>
    /// <param name="m"></param>
    /// <param name="column"></param>
    /// <param name="num"></param>
    /// <returns></returns>
    public async Task<bool> Cut(Model m, NumColumnEnum column, int num = 1)
    {
        _dbContext.Entry(m).Property<int>(NumColumns[column]).CurrentValue -= num;
        var state = await _dbContext.SaveChangesAsync() > 0;
        if (!state)
        {
            throw new Exception($"{NumColumnMessageTitles[column]}上报发生异常");
        }

        return state;
    }

    #region 基础方法

    public Task CheckOrAdd(OEM m) => CheckOrAdd(m.ID, m, Types.OEM);
    public Task CheckOrAdd(Company m) => CheckOrAdd(m.ID, m, Types.Company);
    public Task CheckOrAdd(Group m) => CheckOrAdd(m.ID, m, Types.UserGroup);
    public Task CheckOrAdd(User m) => CheckOrAdd(m.ID, m, Types.User);

    public async Task<Model> GetStatisticInfo(int? aboutId, Types role, Types effective)
    {
        if (!aboutId.HasValue)
        {
            throw new Exception($"AboutId Is Null");
        }

        if (effective == Types.DAY)
        {
            switch (role)
            {
                case Types.User:
                    await CheckOrAdd(await _dbContext.User.FindAsync(aboutId));
                    break;
                case Types.UserGroup:
                    await CheckOrAdd(await _dbContext.UserGroup.FindAsync(aboutId));
                    break;
                case Types.Company:
                    await CheckOrAdd(await _dbContext.Company.FindAsync(aboutId));
                    break;
                case Types.OEM:
                    await CheckOrAdd(await _dbContext.OEM.FindAsync(aboutId));
                    break;
            }
        }

        var begin = DateTime.Now.BeginDayTick();
        var last = DateTime.Now.LastDayTick();

        var info = await _dbContext.Statistic
            .Where(m =>
                m.AboutID == aboutId
                && (m.AboutType & Types.Effective) == effective
                && (m.AboutType & Types.Role) == role
                && (m.Validity >= begin && m.Validity <= last)
            )
            .FirstOrDefaultAsync();

        if (info == null)
        {
            throw new Exception($"找不到相关数据#role[{role.GetName()}] | effective[{effective.GetName()}]");
        }

        return info;
    }

    private readonly Types[] AllEffectiveTimeRanges = new[] { Types.TOTAL, Types.YEAR, Types.MONTH, Types.DAY };

    private async System.Threading.Tasks.Task CheckOrAdd(int ID, IStatistic IStatistic, Types types)
    {
        var Now = DateTime.Now;
        var Date = Now.Date;
        if (IStatistic.LastCreateStatistic < Date) //是否跨天
        {
            var EffectiveTimeRanges = await _dbContext.Statistic
                .Where(s =>
                    s.AboutID == ID
                    && s.Validity >= Now
                    && (s.AboutType & Types.Role) == types
                )
                .Select(s => s.AboutType & Types.Effective).ToListAsync();
            var NeedAddEffectiveTimeRanges = AllEffectiveTimeRanges.Where(s => !EffectiveTimeRanges.Contains(s));
            await _dbContext.Statistic.AddRangeAsync(
                NeedAddEffectiveTimeRanges.Select(t => new Model(ID, types, t, Date)));
            IStatistic.LastCreateStatistic = Now;
            await _dbContext.SaveChangesAsync();
        }
    }

    #endregion
}