using ClearProduct.ViewModels;
using ERP.Data;
using ERP.Models.Product;

namespace ClearProduct.Service;

public class RecycleProductService : BaseService
{
    private readonly ILogger<ClearProductService> _logger;
    private StatisticService _statisticService;

    public RecycleProductService(DBContext dbContext, ILogger<ClearProductService> logger,
        IHostEnvironment environment, StatisticService statisticService) : base(dbContext, environment)
    {
        _logger = logger;
        _statisticService = statisticService;
    }

    public async Task Handle(ProductModel product)
    {
        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            // foreach (var product in products)
            // {
            if (!product.IsOngoing() && !product.IsExport() && !product.IsUpload())
            {
                await _statisticService.Increase(new DataBelongDto(product),
                    StatisticService.NumColumnEnum.ProductRecovery);
            }

            product.SetRecycle(true);
            // }
            await _dbContext.SaveChangesAsync();
            await transaction.CommitAsync();
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            _logger.LogError(e, e.Message);
            throw;
        }
    }

    public async Task Handle(ProductModel[] products)
    {
    }
}