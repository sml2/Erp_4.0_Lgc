using ERP.Data;
using ERP.Extensions;
using ERP.Models.DB.Users;
using ERP.Models.Product;
using Microsoft.EntityFrameworkCore;

namespace ClearProduct.Service;

public class BaseService
{
    protected readonly DBContext _dbContext;
    protected readonly IHostEnvironment _environment;

    public BaseService(DBContext dbContext, IHostEnvironment environment)
    {
        _dbContext = dbContext;
        _environment = environment;
    }

    /// <summary>
    /// 获取有效延长期得用户id
    /// </summary>
    /// <param name="cancellationToken"></param>
    /// <param name="expire"></param>
    /// <param name="residence"></param>
    /// <returns></returns>
    public async Task<List<ProductValidityModel>> GetDelayed(int expire,int residence = -1)
    {
        var validity = DateTime.Now.AddMonths(-expire).AddDays(residence).LastDayTick();
        return _dbContext.ProductValidity
            .AsEnumerable()
            .Where(m => m.Validity > validity)
            .GroupBy(m => m.UserId)
            .Select(m => m.OrderByDescending(m => m.Validity).First())
            .ToList();
    }
}