using ClearProduct.Exceptions;
using ClearProduct.Service;
using ClearProduct.ViewModels;
using ERP.Data;
using ERP.Data.Products;
using ERP.DomainEvents.Product;
using ERP.Models.DB.Users;
using ERP.Models.Product;
using ERP.Services.Upload;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace ClearProduct.Service;

public class ClearProductService : BaseService
{
    private readonly ILogger<ClearProductService> _logger;
    private UploadService _uploadService;
    private StatisticService _statisticService;

    public ClearProductService(DBContext dbContext, ILogger<ClearProductService> logger,
        IHostEnvironment environment, StatisticService statisticService) : base(dbContext, environment)
    {
        _logger = logger;
        _statisticService = statisticService;
    }




    public async Task RemoveDbImage(Images gallery)
    {
        var imageIds = gallery.Select(m => m.ID).ToList();
        await _dbContext.Resource.Where(m => imageIds.Contains(m.ID)).DeleteAsync();
    }

    public async Task RemoveObsImage(Images gallery)
    {
        foreach (var image in gallery)
        {
            if (!image.IsNetwork)
            {
                await _uploadService.Delete(image.Path!);
            }
        }
    }

    public async Task Handle(ProductModel product)
    {
        var oem = await _dbContext.OEM.Include(m => m.Oss).FirstOrDefaultAsync(m => m.ID == product.OEMID);
        if (oem is null || oem.Oss is null)
        {
            throw new NotFoundException("找不到Oem相关数据#1");
        }

        _uploadService = new UploadService(oem.Oss, _environment);

        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            //清理图片
            var gallery = product.Gallery;
            if (gallery != null)
            {
                //obs
                await RemoveObsImage(gallery);
                //db
                await RemoveDbImage(gallery);
            }

            var productItems = await _dbContext.ProductItem
                .Where(m => m.Pid == product.ID).ToListAsync();

            // productItems.Count == 0 无子产品，说明可能是上传中的产品或者异常产品，可直接删除
            if (productItems.Count > 0)
            {
                var versionList = productItems
                    .Select(m =>
                        m.HistoryVersionList
                            .Select(m => GetProductJsonPathFolder(m, product.ID, product.OEMID, product.CompanyID)).Distinct().ToList())
                    .ToList();

                foreach (var item in versionList)
                {
                    var paths = new List<string>();
                    foreach (var path in item)
                    {
                        paths = await _uploadService.ListObjects(path);
                        if (paths.Count > 0)
                        {
                            await _uploadService.BatchDeleteAsync(paths,true);
                        }
                    }
                }
            
                await _dbContext.ProductItem.Where(m => m.Pid == product.ID).DeleteAsync(); 
            }
            
            await _dbContext.Product.Where(m => m.ID == product.ID).DeleteAsync();
            
            //回收站产品
            if (!product.IsOngoing() && !product.IsExport() && !product.IsUpload())
            {
                if (product.IsRecycle())
                {
                    await _statisticService.Decrease(new DataBelongDto(product), StatisticService.NumColumnEnum.ProductRecovery);
                }

                await _statisticService.Increase(new DataBelongDto(product), StatisticService.NumColumnEnum.ProductDelete);
                await _statisticService.Decrease(new DataBelongDto(product), StatisticService.NumColumnEnum.ProductCount);
            }

            await _dbContext.SaveChangesAsync();
            await transaction.CommitAsync();
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            _logger.LogError(e, e.Message);
            throw;
        }
    }

    public string GetProductJsonPathFolder(long version, int pid, int oemId, int companyId)
    {
        var t = DateTimeOffset.FromUnixTimeMilliseconds(version);
        return $"{oemId}/{companyId}/products/{t.Year:0000}/{t.Month:00}/{t.Day:00}/{pid}/";
    }
}