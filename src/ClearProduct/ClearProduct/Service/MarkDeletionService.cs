using System.Diagnostics.CodeAnalysis;
using ClearProduct.ViewModels;
using ERP.Data;
using ERP.Models.Product;

namespace ClearProduct.Service;

public class MarkDeletionService : BaseService
{
    private readonly ILogger<MarkDeletionService> _logger;
    private StatisticService _statisticService;

    public MarkDeletionService(DBContext dbContext, ILogger<MarkDeletionService> logger,
        IHostEnvironment environment, StatisticService statisticService) : base(dbContext, environment)
    {
        _logger = logger;
        _statisticService = statisticService;
    }

    [SuppressMessage("ReSharper.DPA", "DPA0000: DPA issues")]
    public async Task Handle(ProductModel product)
    { 
        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {

            await _statisticService.Decrease(new DataBelongDto(product),
                StatisticService.NumColumnEnum.ProductRecovery);
            await _statisticService.Decrease(new DataBelongDto(product),
                StatisticService.NumColumnEnum.ProductCount);
            await _statisticService.Increase(new DataBelongDto(product),
                StatisticService.NumColumnEnum.ProductDelete);

            product.SetDelete(true);
            
            //记录日志
            await _dbContext.ProductMarkDeletion.AddAsync(new ProductMarkDeletionModel()
            {
                OEMID = product.OEMID,
                Pid = product.ID,
                TriggerTime = DateTime.Now,
                PCreatedAt = product.CreatedAt,
                CompanyID = product.CompanyID,
                UserID = product.UserID,
                GroupID = product.GroupID,
            });
            await _dbContext.SaveChangesAsync();
            await transaction.CommitAsync();
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            _logger.LogError(e, e.Message);
            throw;
        }
        
    }
}