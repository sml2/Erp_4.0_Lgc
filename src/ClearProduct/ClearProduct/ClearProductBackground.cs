using ClearProduct.Service;
using ERP.Data;
using ERP.Enums.Product;
using ERP.Extensions;
using Microsoft.EntityFrameworkCore;

namespace ClearProduct;

public class ClearProductBackground : BackgroundService
{
    private readonly IServiceProvider _provider;
    private readonly ILogger<ClearProductBackground> _logger;

    public ClearProductBackground(IServiceProvider provider, ILogger<ClearProductBackground> logger)
    {
        _provider = provider;
        _logger = logger;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        const int batch = 5;
        const int interval = 10_000;
        const int residence = 7;
        const int expire = 6;
        while (!stoppingToken.IsCancellationRequested)
        {
            var scope = _provider.CreateScope().ServiceProvider;
            var dbContext = scope.GetRequiredService<DBContext>();
            var clearProductService = scope.GetRequiredService<ClearProductService>();
            var now = DateTime.Now;
            var validity = now.AddMonths(-expire).AddDays(-residence - 1);

            _logger.LogInformation("==========================Start==========================");
            _logger.LogInformation("{now}开始执行将超过滞留期限的产品删除", now);
            _logger.LogInformation("创建时间在此时间【{validity}】之前为需清理的产品", validity.ToString("yyyy-MM-dd"));
            var delayed = await clearProductService.GetDelayed(expire);
            
            if (delayed.Count == 0)
            {
                _logger.LogInformation("无客户存在延期业务");
            }
            else
            {
                _logger.LogInformation("{count}个客户延期有效", delayed.Count);
                foreach (var item in delayed)
                {
                    _logger.LogInformation("客户【{UserId}】延期至{Validity}", item.UserId,item.Validity.ToString("yyyy-MM-dd"));
                }
            }

            var delayedUserId = delayed.Select(m => m.UserId).ToList();


            var products = await dbContext.Product
                .Where(m => (m.Type & (long)Types.Delete) == (long)Types.Delete)
                .Where(m => m.CreatedAt <= validity.LastDayTick())
                .Where(m => !delayedUserId.Contains(m.UserID))
                .OrderBy(m => m.ID)
                .Take(batch)
                .ToArrayAsync(stoppingToken);
            if (!products.Any())
            {
                _logger.LogInformation("当前无超过滞留期限产品");
                _logger.LogInformation("===========================End===========================");
                await Task.Delay(interval, stoppingToken);
                continue;
            }

            while (products.Any())
            {
                try
                {
                    foreach (var product in products)
                    {
                        if (stoppingToken.IsCancellationRequested)
                            break;

                        var pUserId = product.UserID;
                        var pid = product.ID;
                        var pCreated = product.CreatedAt.ToString("yyyy-MM-dd hh:mm:ss");

                        _logger.LogInformation("开始删除产品；用户ID【{Id}】，删除产品【{ProductId}】，产品创建时间【{pCreated}】", pUserId, pid,pCreated);
                        await clearProductService.Handle(product);
                        _logger.LogInformation("删除产品成功，用户ID【{Id}】，删除产品【{ProductId}】", pUserId, pid);
                        // await Task.Delay(interval, stoppingToken);
                    }
                    _logger.LogInformation("===========================End===========================");
                    await Task.Delay(interval, stoppingToken);
                    break;
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "删除产品报错");
                    _logger.LogInformation("===========================End===========================");
                    break;
                }
            }
        }
    }
}