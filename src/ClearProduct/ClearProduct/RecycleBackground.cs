using ClearProduct.Service;
using ERP.Data;
using ERP.Enums.Product;
using ERP.Extensions;
using Microsoft.EntityFrameworkCore;

namespace ClearProduct;

public class RecycleBackground : BackgroundService
{
    private readonly IServiceProvider _provider;
    private readonly ILogger<RecycleBackground> _logger;

    public RecycleBackground(IServiceProvider provider, ILogger<RecycleBackground> logger)
    {
        _provider = provider;
        _logger = logger;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        const int batch = 100;
        const int interval = 10_000;
        const int residence = 7;
        const int expire = 6;

        while (!stoppingToken.IsCancellationRequested)
        {
            var now = DateTime.Now;
            var scope = _provider.CreateScope().ServiceProvider;
            var dbContext = scope.GetRequiredService<DBContext>();
            var recycleProductService = scope.GetRequiredService<RecycleProductService>();
            var delayed = await recycleProductService.GetDelayed(expire);
            

            _logger.LogInformation("==========================Start==========================");
            _logger.LogInformation("{now}开始执行将过期产品移入回收站", now);

            if (delayed.Count == 0)
            {
                _logger.LogInformation("无客户存在延期业务");
            }
            else
            {
                _logger.LogInformation("{count}个客户延期有效", delayed.Count);
            }

            var delayedUserId = delayed.Select(m => m.UserId).ToList();


            var products = await dbContext.Product
                .Where(m => m.CreatedAt <= now.AddMonths(-expire).AddDays(-1).LastDayTick())
                .Where(m => (m.Type & (long)Types.Recycle) != (long)Types.Recycle)
                .Where(m => !delayedUserId.Contains(m.UserID))
                .Take(batch).ToArrayAsync(stoppingToken);

            if (!products.Any())
            {
                _logger.LogInformation("当前无过期产品");
                _logger.LogInformation("===========================End===========================");
                await Task.Delay(interval, stoppingToken);
                continue;
            }

            while (products.Any())
            {
                try
                {
                    _logger.LogInformation("本次移入回收站产品数量为【{Count}】;", products.Length);
                    foreach (var product in products)
                    {
                        if (stoppingToken.IsCancellationRequested)
                            break;
                        
                        var pUserId = product.UserID;
                        var pid = product.ID;
                        _logger.LogInformation("开始移入回收站；用户ID【{UserID}】，产品ID【{Pid}】;", pUserId, pid);
                        await recycleProductService.Handle(product);
                        _logger.LogInformation("移入回收站成功，用户ID【{UserID}】，产品ID【{Pid}】;", pUserId, pid);
                    }
                    _logger.LogInformation("===========================End===========================");
                    break;
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "移入回收站报错");
                    _logger.LogInformation("===========================End===========================");
                    break;
                }
            }
            await Task.Delay(interval, stoppingToken);
        }
    }
}