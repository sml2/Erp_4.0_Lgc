﻿using ERP.Data;
using ERP.Models.DB.Identity;

namespace ERP.Storage.Services.Internal;

public class StockService : BaseService, IStockService
{
    private readonly Base _dbContext;
    private readonly ILogger<StockService> _logger;
    private readonly IWarehouseService _warehouseService;

    /// <summary>
    /// 根据产品id或产品名称查询
    /// </summary>
    private static readonly Func<Base, User, int?, string?, IOrderedQueryable<StockProduct>> SearchNameQuery =
        (db, user, productId, name)
            => db.Set<StockProduct>().Where(sp => sp.CompanyID == user.CompanyID)
                .WhenWhere(productId.HasValue, sp => sp.ProductId == productId)
                .WhenWhere(!string.IsNullOrEmpty(name), sp => sp.ProductName.StartsWith(name!))
                .OrderByDescending(sp => sp.UpdatedAt);

    public StockService(Base dbContext, ILogger<StockService> logger, IWarehouseService warehouseService)
    {
        _dbContext = dbContext;
        _logger = logger;
        _warehouseService = warehouseService;
    }

    /// <summary>
    /// 库存商品列表
    /// </summary>
    /// <returns></returns>
    public Task<DbSetExtension.PaginateStruct<StockProductDto>> StockProductList(User user, int? productId,
        string? name, string? spu)
    {
        return SearchNameQuery(_dbContext, user, productId, name)
            .Select(s => new StockProductDto(s.ID, s.ProductName, s.ProductId, s.TotalQuantity, s.UpdatedAt))
            .ToPaginateAsync();
    }

    public async Task<DbSetExtension.PaginateStruct<StockDto>> StockList(User user, int? productId, string? name,
        int? warehouseId, int? shelfId, string? spu)
    {
        IQueryable<Stock> query = _dbContext.Set<Stock>();
        if (user.Company.IsDistribution)
        {
            query = query.Where(s => s.CompanyID == user.CompanyID || s.CompanyID == user.Pid);
        }
        else
        {
            query = query.Where(s => s.CompanyID == user.CompanyID);
        }

        var res = await query.WhenWhere(productId.HasValue, s => s.ProductId == productId)
            .WhenWhere(warehouseId.HasValue, s => s.WarehouseId == warehouseId)
            .WhenWhere(shelfId.HasValue, s => s.ShelfId == shelfId)
            .WhenWhere(!string.IsNullOrEmpty(name), s => s.ProductName.StartsWith(name!))
            .Include(s => s.StockProduct)
            .OrderByDescending(s => s.UpdatedAt)
            // .Select(s => new StockDto(s.ID, s.ProductName, s.ProductId, s.Quantity, s.WarehouseId, s.WarehouseName,
            //     s.ShelfId, s.UpdatedAt))
            .Select(s => new StockDto(s))
            .ToPaginateAsync();
        
         IQueryable<StockProduct> stockProductsQuery = _dbContext.Set<StockProduct>();

        return res;
    }
    
    public IQueryable<Stock> StockListQuery(User user, int? productId, string? name,
        int? warehouseId, int? shelfId, string? spu)
    {
        IQueryable<Stock> query = _dbContext.Set<Stock>();
        if (user.Company.IsDistribution)
        {
            query = query.Where(s => s.CompanyID == user.CompanyID || s.CompanyID == user.Pid);
        }
        else
        {
            query = query.Where(s => s.CompanyID == user.CompanyID);
        }

        var res = query.WhenWhere(productId.HasValue, s => s.ProductId == productId)
            .WhenWhere(warehouseId.HasValue, s => s.WarehouseId == warehouseId)
            .WhenWhere(shelfId.HasValue, s => s.ShelfId == shelfId)
            .WhenWhere(!string.IsNullOrEmpty(name), s => s.ProductName.StartsWith(name!))
            .Include(s => s.StockProduct)
            .Include(s => s.Warehouse)
            .Include(s => s.Shelf)
            .OrderByDescending(s => s.UpdatedAt);

        return res;
    }


    public async Task<bool> In(int stockProductId, int warehouseId, int? shelfId, int quantity, User operateUser,
        StockExtData data)
    {
        var warehouse = await _dbContext.Set<Warehouse>().FirstOrDefaultAsync(s => s.ID == warehouseId);
        if (warehouse is null)
            throw new StorageDomainException("仓库不存在");

        var stockProduct = await _dbContext.Set<StockProduct>().FirstOrDefaultAsync(s => s.ID == stockProductId);
        if (stockProduct is null)
            throw new StorageDomainException("产品不存在");

        stockProduct.UpdateTotalQuantity(quantity);

        if (shelfId.HasValue)
        {
            var shelf = await _dbContext.Set<Shelf>().FirstOrDefaultAsync(s => s.ID == shelfId);
            if (shelf is null)
                throw new StorageDomainException("货架不存在");

            return await In(stockProduct, shelf, quantity, operateUser, data);
        }


        return await In(stockProduct, warehouse, quantity, operateUser, data);
    }

    public async Task<bool> In(StockProduct product, Warehouse warehouse, int quantity, User operateUser,
        StockExtData data)
    {
        if (warehouse.UseShelf)
            throw new StorageDomainException("使用该仓库必须选择货架");
        var stock = await _dbContext.Set<Stock>()
            .Where(s => s.StockProductId == product.ID && s.WarehouseId == warehouse.ID)
            .FirstOrDefaultAsync() ?? CreateStock(product, warehouse, operateUser);

        stock.Quantity += quantity;
        stock.AddLog(quantity, StockLog.States.IN, StockLog.Shelves.FALSE, operateUser, data);
        await _dbContext.SaveChangesAsync();
        return true;
    }

    public async Task<bool> In(StockProduct product, Shelf shelf, int quantity, User operateUser, StockExtData data)
    {
        var stock = await _dbContext.Set<Stock>()
            .Where(s => s.StockProductId == product.ID && s.ShelfId == shelf.ID)
            .FirstOrDefaultAsync() ?? CreateStock(product, shelf, operateUser);

        stock.Quantity += quantity;
        stock.AddLog(quantity, StockLog.States.IN, StockLog.Shelves.FALSE, operateUser, data);
        await _dbContext.SaveChangesAsync();
        return true;
    }

    private Stock CreateStock(StockProduct product, Warehouse warehouse, User user)
    {
        var stock = new Stock(product, warehouse, user.Id, user.GroupID, user.CompanyID, user.OEMID);

        return _dbContext.Add(stock).Entity;
    }

    private Stock CreateStock(StockProduct product, Shelf shelf, User user)
    {
        var stock = new Stock(product, shelf, user.Id, user.GroupID, user.CompanyID, user.OEMID);

        return _dbContext.Add(stock).Entity;
    }

    private async Task<bool> AddLog(Stock stock, int quantity, string remark, StockLog.States state,
        StockLog.Shelves isShelve, User user, string orderNo = "", int? purchaseId = null,
        string purchaseTrackingNumber = "")
    {
        var log = new StockLog(stock, user.Id, stock.GroupID, stock.CompanyID, user.Company.Name, stock.OEMID, quantity,
            state, isShelve, remark, orderNo, purchaseId, purchaseTrackingNumber)
        {
            UserName = user.UserName,

            ExecuteCompanyId = user.CompanyID,
            ExecuteUserId = user.Id,
            ExecuteUserName = user.TrueName,

            ToWarehouseId = 0,
            ToWarehouseName = "",
            WaybillOrder = "",
        };

        _dbContext.Add(log);
        await _dbContext.SaveChangesAsync();

        return true;
    }

    // private StockProduct CreateStockProduct(Warehouse warehouse, string productName, string url, User operateUser)
    // {
    //     var product = new StockProduct(productName, url, operateUser.Id, operateUser.UserName, warehouse.CompanyID,
    //         warehouse.GroupID, warehouse.OEMID, 0);
    //
    //     return _dbContext.Add(product).Entity;
    // }


    /// <summary>
    /// Stock out
    /// </summary>
    /// <returns></returns>
    public async Task<bool> StockOut(int stockProductId, int warehouseId, int quantity, User operateUser,
        StockExtData data)
    {
        var product = await _dbContext.Set<StockProduct>().SingleOrDefaultAsync(x => x.ID == stockProductId);
        if (product is null)
        {
            throw new StorageDomainException("没有找到合适的产品处理");
        }

        var warehouse = await _dbContext.Set<Warehouse>().SingleOrDefaultAsync(x => x.ID == warehouseId);
        if (warehouse is null)
        {
            throw new StorageDomainException("没有找到仓库");
        }

        return await Out(product, warehouse, quantity, operateUser, data);
    }

    public async Task<bool> Out(StockProduct stockProduct, Warehouse warehouse, int quantity, User operateUser,
        StockExtData data)
    {
        var stock = await _dbContext.Set<Stock>()
            .FirstOrDefaultAsync(x => x.StockProductId == stockProduct.ID && x.WarehouseId == warehouse.ID);
        if (stock is null)
        {
            throw new StorageDomainException("没有找到库存");
        }

        if (warehouse.Type == Warehouse.Types.Shared && warehouse.DistributionCompanyId == operateUser.CompanyID)
        {
            await AddLog(stock, quantity, "", StockLog.States.IN, StockLog.Shelves.FALSE, operateUser);
        }

        if (stock.Quantity < quantity)
            throw new StorageDomainException("库存不足");

        stock.Quantity -= quantity;
        stock.AddLog(quantity, StockLog.States.OUT, StockLog.Shelves.FALSE, operateUser, data);

        await _dbContext.SaveChangesAsync();
        return true;
    }

    public async Task<bool> Transfer(int stockProductId, int warehouseId, int toWarehouseId,
        int quantity, User operateUser, StockExtData data)
    {
        var product = await _dbContext.Set<StockProduct>().SingleOrDefaultAsync(x => x.ID == stockProductId);
        if (product is null)
        {
            throw new StorageDomainException("没有找到合适的产品处理");
        }

        var warehouse = await _dbContext.Set<Warehouse>().SingleOrDefaultAsync(x => x.ID == warehouseId);
        if (warehouse is null)
        {
            throw new StorageDomainException("没有找到仓库");
        }

        var toWarehouse = await _dbContext.Set<Warehouse>().SingleOrDefaultAsync(x => x.ID == toWarehouseId);
        if (toWarehouse is null)
        {
            throw new StorageDomainException("没有找到仓库");
        }

        return await Transfer(product, warehouse, toWarehouse, quantity, operateUser, data);
    }

    public async Task<bool> Transfer(StockProduct stockProduct, Warehouse warehouse, Warehouse toWarehouse,
        int quantity, User operateUser, StockExtData data)
    {
        var outStock = await _dbContext.Set<Stock>()
            .FirstOrDefaultAsync(x => x.StockProductId == stockProduct.ID && x.WarehouseId == warehouse.ID);
        if (outStock is null)
        {
            throw new StorageDomainException("没有找到库存");
        }

        if (warehouse.Type == Warehouse.Types.Shared && warehouse.DistributionCompanyId == operateUser.CompanyID)
        {
            await AddLog(outStock, quantity, "", StockLog.States.IN, StockLog.Shelves.FALSE, operateUser);
        }

        if (outStock.Quantity < quantity)
            throw new StorageDomainException("库存不足");

        outStock.Quantity -= quantity;

        var inStock = await _dbContext.Set<Stock>()
            .Where(s => s.StockProductId == stockProduct.ID && s.WarehouseId == warehouse.ID)
            .FirstOrDefaultAsync() ?? CreateStock(stockProduct, warehouse, operateUser);

        inStock.Quantity += quantity;

        outStock.AddLog(quantity, StockLog.States.Transfer, StockLog.Shelves.FALSE, operateUser, data,
            toWarehouse: toWarehouse);

        await _dbContext.SaveChangesAsync();
        return true;
    }


    /// <summary>
    /// Stock information
    /// </summary>
    /// <returns></returns>
    public async Task<StockProduct?> Info(User user, int id)
    {
        var isDistribution = user.Company.IsDistribution == true;
        IQueryable<StockProduct> root = _dbContext.Set<StockProduct>().Where(sp => sp.ID == id);
        if (isDistribution)
        {
            root = root.Where(sp => sp.CompanyID == user.CompanyID || sp.CompanyID == user.Pid);
        }
        else
        {
            root = root.Where(sp => sp.CompanyID == user.CompanyID);
        }

        var stockProduct = await root.SingleOrDefaultAsync();

        // await _dbContext.Entry(stockProduct).Reference(sp => sp.Product).LoadAsync();

        return stockProduct;
    }

    /// <summary>
    /// 仓储日志分页列表
    /// </summary>
    /// <returns></returns>
    public async Task<DbSetExtension.PaginateStruct<StockLog>> LogPageList(User user, StoreLogViewModel request)
    {
        IQueryable<StockLog> query = _dbContext.Set<StockLog>();
        if (!string.IsNullOrEmpty(request.productName))
        {
            query = query.Where(sl => sl.ProductName == request.productName);
        }

        if (request.id.HasValue)
        {
            query = query.Where(sl => sl.StockProductId == request.id);
        }
        else if (request.isStock)
        {
            query = query.Where(sl => sl.StockProductId == 0);
        }

        if (request.warehouseId.HasValue)
        {
            query = query.Where(sl => sl.WarehouseId == request.warehouseId);
        }

        if (request.startDate.HasValue)
        {
            query = query.Where(sl => sl.CreatedAt > request.startDate.Value);
        }

        if (request.endDate.HasValue)
        {
            query = query.Where(sl => sl.CreatedAt < request.endDate.Value);
        }

        if (!string.IsNullOrEmpty(request.orderNo))
        {
            query = query.Where(sl => sl.OrderNoHashcode == Helpers.CreateHashCode(request.orderNo));
        }

        if (!string.IsNullOrEmpty(request.purchaseTrackingNumber))
        {
            query = query.Where(sl => sl.PurchaseTrackingNumber == request.purchaseTrackingNumber);
        }

        if (request.waybillId.HasValue)
        {
            query = query.Where(sl => sl.WaybillId > request.waybillId);
        }

        return await query.Where(sl => sl.CompanyID == user.CompanyID || sl.ExecuteCompanyId == user.CompanyID)
            .OrderByDescending(sl => sl.CreatedAt)
            .ToPaginateAsync();
    }

    /// <summary>
    /// 仓储日志列表
    /// </summary>
    /// <returns></returns>
    public async Task<List<StockLog>> LogList(User sessionData, string? orderNo, bool isInfo = false)
    {
        IQueryable<StockLog> root = _dbContext.Set<StockLog>();

        if (isInfo)
        {
            root = root.Where(sl => sl.ExecuteCompanyId == sessionData.CompanyID);
        }
        else
        {
            root = root.Where(sl => sl.Shelve == StockLog.Shelves.FALSE);
        }

        return await root
            .Where(
                sl => sl.CompanyID ==
                      sessionData.CompanyID /* && sl.OrderNoHashcode == Helpers.CreateHashCode(orderNo)*/)
            .WhenWhere(!string.IsNullOrEmpty(orderNo), sl => sl.OrderNoHashcode == Helpers.CreateHashCode(orderNo!))
            .OrderByDescending(sl => sl.CreatedAt)
            .ToListAsync();
    }

    /// <summary>
    /// 待确认仓储日志列表
    /// </summary>
    /// <returns></returns>
    public async Task<DbSetExtension.PaginateStruct<StockLog>> AuditLogPageList(User user, StockLog.Shelves shelve,
        int? warehouseId = null, string orderNo = "", DateTime? startDate = null,
        DateTime? endDate = null)
    {
        IQueryable<StockLog> root = _dbContext.Set<StockLog>();

        if (warehouseId.HasValue)
        {
            root = root.Where(sl => sl.WarehouseId == warehouseId.Value);
        }

        if (startDate.HasValue)
        {
            root = root.Where(sl => sl.CreatedAt > startDate.Value);
        }

        if (endDate.HasValue)
        {
            root = root.Where(sl => sl.CreatedAt < endDate.Value);
        }

        return await root.Where(sl => sl.CompanyID == user.CompanyID || sl.ExecuteCompanyId == user.CompanyID)
            .Where(sl => sl.OrderNoHashcode == Helpers.CreateHashCode(orderNo) && sl.Shelve == shelve)
            .OrderByDescending(sl => sl.CreatedAt)
            .ToPaginateAsync();
    }

    /// <summary>
    /// 待确认仓储日志列表
    /// </summary>
    /// <returns></returns>
    public async Task<List<StockLog>> AuditLogList(User user, string orderNo)
    {
        return await _dbContext.Set<StockLog>().Where(sl => sl.CompanyID == user.CompanyID &&
                                                            sl.OrderNoHashcode == Helpers.CreateHashCode(orderNo)
                                                            && sl.Shelve == StockLog.Shelves.TRUE)
            .OrderByDescending(sl => sl.CreatedAt)
            .ToListAsync();
    }


    /// <summary>
    /// 获取有库存的仓库
    /// </summary>
    /// <returns></returns>
    public async Task<DbSetExtension.PaginateStruct<Stock>> StockOutWarehouseList(int id, User user)
    {
        var warehouses = (await _warehouseService.AvailableWarehouses(user)).KeyBy(w => w.ID);
        return await _dbContext.Set<Stock>()
            .Where(s => warehouses.Keys.Contains(s.WarehouseId) && s.StockProductId == id).ToPaginateAsync();

        //        foreach ($list as $item) {
        //            $item->type = $warehouses[$item->warehouse_id]['type'];
        //        }
        // return list;
    }

    /// <summary>
    /// 根据产品id，变体id获取有库存的仓库
    /// </summary>
    /// <param name="productId">产品id</param>
    /// <param name="variantId">变体hashcode</param>
    /// <param name="allWarehouse">是否展示所有仓库，没有库存的仓库库存为0</param>
    /// <returns></returns>
    public async Task<List<Stock>> StockOutWarehouseListByProduct(int productId, int variantId, User user,
        bool allWarehouse = false)
    {
        var warehouses = (await _warehouseService.AvailableWarehouses(user)).KeyBy(w => w.ID);

        var list = await _dbContext.Set<Stock>().Where(s => warehouses.Keys.Contains(s.WarehouseId)
                                                            && s.ProductId == productId && s.VariantId == variantId
        ).ToListAsync();

        //        if ($allWarehouse) {
        //            $keyList = arrayCombine($list->toArray(), 'warehouse_id');
        //            foreach ($warehousesList as $item) {
        //                $item->quantity = isset($keyList[$item->id]) ? $keyList[$item->id]['quantity'] : 0;
        //                $item->warehouse_id = $item->id;
        //                $item->warehouse_name = $item->name;
        //            }
        //            $list = $warehousesList;
        //        } else {
        //            foreach ($list as $item) {
        //                $item->type = $warehouses[$item->warehouse_id]['type'];
        //            }
        //        }
        //        return $list;
        return list;
    }


    /// <summary>
    /// 获取有库存的仓库(分页)
    /// </summary>
    /// <returns></returns>
    public async Task<DbSetExtension.PaginateStruct<Stock>> StockOutWarehousePaginate(User user, int? id,
        int? productId = null, int? hashcode = null)
    {
        var warehouses = (await _warehouseService.AvailableWarehouses(user)).KeyBy(w => w.ID);
        var list = await _dbContext.Set<Stock>().Where(s => warehouses.Keys.Contains(s.WarehouseId))
            .WhenWhere(id.HasValue, s => s.ID == id!.Value)
            .WhenWhere(productId.HasValue, s => s.ProductId == productId!.Value)
            .WhenWhere(hashcode.HasValue, s => s.VariantId == hashcode!.Value)
            .ToPaginateAsync();

        //        foreach ($list->items() as $item) {
        //            $item->type = $warehouses[$item->warehouse_id]['type'];
        //        }
        //        return $list;
        return list;
    }

    /// <summary>
    /// 判断是否存在库存商品
    /// </summary>
    /// <param name="productId">库存商品id</param>
    /// <returns></returns>
    public Task<StockProduct?> Exists(int productId)
    {
        return _dbContext.Set<StockProduct>().FirstOrDefaultAsync(sp => sp.ID == productId);
    }

    /// <summary>
    /// 判断是否存在库存商品
    /// </summary>
    /// <param name="sku">库存商品sku</param>
    /// <returns></returns>
    public Task<StockProduct?> Exists(string sku)
    {
        return _dbContext.Set<StockProduct>().FirstOrDefaultAsync(sp => sp.Sku == sku);
    }

    public Task<bool> HasStockInWarehouse(int warehouseId)
    {
        return _dbContext.Set<Stock>().Where(s => s.WarehouseId == warehouseId && s.Quantity > 0)
            .AnyAsync();
    }

    public Task<bool> HasStockInShelf(Shelf shelf)
    {
        return _dbContext.Set<Stock>()
            .Where(s => s.WarehouseId == shelf.WarehouseId && s.ShelfId == s.ID && s.Quantity > 0)
            .AnyAsync();
    }

    public Task<bool> HasStockByStockProductId(int stockProductId)
    {
        return _dbContext.Set<Stock>().Where(s => s.StockProductId == stockProductId && s.Quantity > 0).AnyAsync();
    }

    public Task<List<int>> GetLogWaybillIdByWarehouse(int warehouseId)
    {
        return _dbContext.Set<StockLog>().Where(s => s.WarehouseId == warehouseId && s.WaybillId.HasValue)
            .Select(s => s.WaybillId)
            .Cast<int>()
            .ToListAsync();
    }

    public Task<List<int>> GetLogPurchaseIdByWarehouse(int warehouseId)
    {
        return _dbContext.Set<StockLog>().Where(s => s.WarehouseId == warehouseId && s.PurchaseId.HasValue)
            .Select(s => s.PurchaseId)
            .Cast<int>()
            .ToListAsync();
    }

    /// <summary>
    /// 检测订单goods字段是否可以扣减仓库
    /// </summary>
    /// <param name="v">单条good信息</param>
    /// <returns></returns>
    public static bool CheckGoodsOut(Dictionary<string, object> v)
    {
        return v.TryGetValue("warehouse_id", out var value) && Convert.ToInt64(value) > 0;
    }
}

public class StorageDomainException : Exception
{
    public string? Log { get; }

    public StorageDomainException(string message) : base(message)
    {
    }

    public StorageDomainException(string message, string log) : this(message)
    {
        Log = log;
    }
}