﻿using ERP.Data;
using ERP.Models.DB.Identity;

namespace ERP.Storage.Services.Internal;

public class WarehouseService : BaseService, IWarehouseService
{
    private readonly Base _dbContext;
    private DbSet<Warehouse> Warehouses => _dbContext.Set<Warehouse>();
    
    private IQueryable<Warehouse> WarehousesNt => _dbContext.Set<Warehouse>().AsNoTracking();
    
    /// <summary>
    /// 构造函数
    /// </summary>
    /// <param name="dbContext"></param>
    public WarehouseService(Base dbContext)
    {
        _dbContext = dbContext;
    }

    /// <summary>
    /// 获取仓库列表分页
    /// </summary>
    /// <returns></returns>
    public Task<DbSetExtension.PaginateStruct<Warehouse>> List(User currentUser,string name, Warehouse.Types? type, int limit, int page)
    {
        return ListQuery(currentUser, name, type)
            .ToPaginateAsync();
    }

    public IQueryable<Warehouse> ListQuery(User currentUser, string name, Warehouse.Types? type)
    {
        var companyId = currentUser.CompanyID;
        var isDistribution = currentUser.Company.IsDistribution;
        IQueryable<Warehouse> query = WarehousesNt;
        query = isDistribution
            ? query.Where(w => w.CompanyID == companyId || w.DistributionCompanyId == companyId)
            : query.Where(w => w.CompanyID == companyId);

        if (!string.IsNullOrEmpty(name))
        {
            query = query.Where(w => w.Name.StartsWith(name));
        }

        if (type.HasValue)
        {
            query = query.Where(w => w.Type == type);
        }

        return query.OrderByDescending(w => w.ID);
    }

    /// <summary>
    /// 获取仓库列表数组
    /// </summary>
    /// <returns></returns>
    public async Task<List<Warehouse>> AvailableWarehouses(User user)
    {
        var isDistribution = user.IsDistribution;
        var companyId = user.CompanyID;
        IQueryable<Warehouse> query = WarehousesNt;
        query = isDistribution
            ? query.Where(w => w.CompanyID == companyId || w.DistributionCompanyId == companyId)
            : query.Where(w => w.CompanyID == companyId);
        return await query
            .OrderByDescending(w => w.ID)
            .ToListAsync();
    }


    /// <summary>
    /// 仓库详情
    /// </summary>
    /// <param name="user"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<Warehouse?> Info(User user,int id)
    {
        return await Warehouses
            .Where(warehouse => warehouse.ID == id)
            .Where(w => w.CompanyID == user.CompanyID)
            .SingleOrDefaultAsync();
    }
    
    /// <summary>
    /// 仓库详情
    /// </summary>
    /// <param name="user"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    public IQueryable<Warehouse> InfoQuery(User user,int id)
    {
        return Warehouses
            .Where(warehouse => warehouse.ID == id)
            .Where(w => w.CompanyID == user.CompanyID);
    }

    public Task<Warehouse?> GetWarehouseByName(string name, int companyId)
    {
        return Warehouses.Where(w => w.Name == name && w.CompanyID == companyId)
            .SingleOrDefaultAsync();
    }

    public Task<Warehouse?> GetWarehouseById(int id, int companyId)
    {
        return Warehouses.Where(warehouse => warehouse.ID == id && warehouse.CompanyID == companyId)
            .FirstOrDefaultAsync();
    }
}