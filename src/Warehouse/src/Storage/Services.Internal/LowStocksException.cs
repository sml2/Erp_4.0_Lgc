﻿namespace ERP.Storage.Services;
/// <summary>
/// 库存不足异常
/// </summary>
public class LowStocksException: Exception
{
    public LowStocksException() : base("库存不足") {}
}

