﻿using ERP.Data;
using ERP.Models.DB.Identity;

namespace ERP.Storage.Services.Internal;

public class ShelfService : BaseService, IShelfService
{
    private readonly Base _dbContext;

    private DbSet<Shelf> Shelves => _dbContext.Set<Shelf>();
    
    private IQueryable<Shelf> ShelvesNt => _dbContext.Set<Shelf>().AsNoTracking();

    public ShelfService(Base dbContext)
    {
        _dbContext = dbContext;
    }
    
    public void Remove(Shelf shelf)
    {
        Shelves.Remove(shelf);
    }

    public Task<Shelf?> GetShelfById(int id, User operateUser) => GetShelfByIdQuery(id, operateUser).SingleOrDefaultAsync();
    public IQueryable<Shelf> GetShelfByIdQuery(int id, User operateUser) => Shelves.Where(s => s.ID == id);

    public Task<Shelf?> GetShelfByName(string name, int warehouseId)
    {
        return Shelves.SingleOrDefaultAsync(s => s.Name == name && s.WarehouseId == warehouseId);
    }

    public Task<List<Shelf>> GetShelvesByWarehouseId(int warehouseId)
    {
        return ShelvesNt.Where(s => s.WarehouseId == warehouseId).ToListAsync();
    }

    public Task<DbSetExtension.PaginateStruct<Shelf>> SearchShelves(int companyId, string? name, int? warehouseId)
    {
        return SearchShelvesQuery(companyId, name, warehouseId)
            .ToPaginateAsync();
    }
    
    public IQueryable<Shelf> SearchShelvesQuery(int companyId, string? name, int? warehouseId)
    {
        var root = ShelvesNt.Where(s => s.CompanyID == companyId);
        if (!string.IsNullOrEmpty(name))
            root = root.Where(s => s.Name.StartsWith(name));
        
        if (warehouseId.HasValue)
            root = root.Where(s => s.WarehouseId == warehouseId.Value);
        return root
            .Include(m => m.Warehouse)
            .OrderByDescending(r => r.ID);
    }
}