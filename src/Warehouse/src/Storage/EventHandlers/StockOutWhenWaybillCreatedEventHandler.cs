namespace ERP.Storage.EventHandlers;

public class StockOutWhenWaybillCreatedEventHandler : INotificationHandler<WaybillCreatedDomainEvent>
{
    private readonly IStockService _stockService;

    public StockOutWhenWaybillCreatedEventHandler(IStockService stockService)
    {
        _stockService = stockService;
    }
    
    public async Task Handle(WaybillCreatedDomainEvent notification, CancellationToken cancellationToken)
    {
        if (notification.OutGoods is null)
        {
            return;
        }

        var waybill = notification.Waybill;
        var logData = new StockExtData("报备发货出库", orderNo: waybill.OrderNo, waybillId: waybill.ID,
            waybillOrder: waybill.WaybillOrder);
        foreach (var good in notification.OutGoods)
        {
            if (good.StockProductId > 0)
            {
                var res = await _stockService.StockOut(good.StockProductId, good.WarehouseId, good.Quantity, notification.User, logData);
            }
        }

        //     if (!$res['state']) {
        //             // FIXME: product_id为0时，无法添加
        //             $stockService->callShelveOut(
        //                 $good['product_id'],
        //                 $good['variant_id'],
        //                 $good['stock_out_num'],
        //                 $orderNo,
        //                 $remark . ',库存不足需手动处理',
        //                 [
        //                     'warehouseId'  => $good['warehouse_id'],
        //                     'waybillId'    => $waybillId,
        //                     'waybillOrder' => $waybillOrder,
        //                 ],
        //             );
        //             $msg = "[{$good['warehouse_name']}]库存不足";
        //             $exception = $exception ? new LowStocksException($msg, 0, $exception) : new LowStocksException($msg);
        //         }
        //     }
        // }
        // if ($exception) {
        //     throw $exception;
        // }
    }
}