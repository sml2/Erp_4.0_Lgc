﻿using ERP.Models.DB.Identity;

namespace ERP.Storage.Abstraction.Services;

public interface IWarehouseService
{

    /// <summary>
    /// 获取仓库列表分页
    /// </summary>
    /// <returns></returns>
    [Obsolete("使用graphql")]
    Task<DbSetExtension.PaginateStruct<Warehouse>> List(User currentUser,string name, Warehouse.Types? type, int limit, int page);
    IQueryable<Warehouse> ListQuery(User currentUser, string name, Warehouse.Types? type);

    /// <summary>
    /// 获取仓库列表数组
    /// </summary>
    /// <returns></returns>
    Task<List<Warehouse>> AvailableWarehouses(User user);

    /// <summary>
    /// 仓库详情
    /// </summary>
    /// <returns></returns>
    Task<Warehouse?> Info(User user, int id);
    
    /// <summary>
    /// 仓库详情Query
    /// </summary>
    /// <returns></returns>
    IQueryable<Warehouse> InfoQuery(User user, int id);

    Task<Warehouse?> GetWarehouseByName(string name, int companyId);
    Task<Warehouse?> GetWarehouseById(int id, int companyId);
}