﻿using ERP.Models.DB.Identity;

namespace ERP.Storage.Abstraction.Services;

public interface IShelfService
{
    public Task<Shelf?> GetShelfById(int id, User operateUser);
    public IQueryable<Shelf> GetShelfByIdQuery(int id, User operateUser);
    public void Remove(Shelf shelf);
    
    public Task<Shelf?> GetShelfByName(string name, int warehouseId);

    public Task<List<Shelf>> GetShelvesByWarehouseId(int warehouseId);
    
    [Obsolete("使用graphql")]
    public Task<DbSetExtension.PaginateStruct<Shelf>> SearchShelves(int companyId,string? name, int? warehouseId);
    public IQueryable<Shelf> SearchShelvesQuery(int companyId,string? name, int? warehouseId);
}