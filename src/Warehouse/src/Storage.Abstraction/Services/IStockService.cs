﻿using ERP.Models.DB.Identity;

namespace ERP.Storage.Abstraction.Services;

public interface IStockService
{
    /// <summary>
    /// 库存商品列表
    /// </summary>
    /// <returns></returns>
    Task<DbSetExtension.PaginateStruct<StockProductDto>> StockProductList(User user, int? productId, string? name,
        string? spu);

    /// <summary>
    /// 库存列表
    /// </summary>
    /// <param name="productId">产品id</param>
    /// <param name="name">产品名称</param>
    /// <param name="warehouseId">仓库id</param>
    /// <param name="shelfId">货架id</param>
    /// <param name="spu">产品spu</param>
    /// <returns></returns>
    [Obsolete("使用graphql")]
    Task<DbSetExtension.PaginateStruct<StockDto>> StockList(User user, int? productId, string? name, int? warehouseId, int? shelfId, string? spu);
    
    /// <summary>
    /// 库存列表
    /// </summary>
    /// <param name="productId">产品id</param>
    /// <param name="name">产品名称</param>
    /// <param name="warehouseId">仓库id</param>
    /// <param name="shelfId">货架id</param>
    /// <param name="spu">产品spu</param>
    /// <returns></returns>
    IQueryable<Stock> StockListQuery(User user, int? productId, string? name, int? warehouseId, int? shelfId, string? spu);

    /// <summary>
    /// 已存在商品入库
    /// </summary>
    /// <returns></returns>
    Task<bool> In(int stockProductId, int warehouseId, int? shelfId, int quantity, User operateUser, StockExtData data);
    /// <summary>
    /// 已存在商品入库
    /// </summary>
    /// <returns></returns>
    Task<bool> In(StockProduct product, Warehouse warehouse, int quantity, User operateUser, StockExtData data);
    Task<bool> In(StockProduct product, Shelf shelf, int quantity, User operateUser, StockExtData data);

    /// <summary>
    /// 库存产品出库
    /// </summary>
    /// <returns></returns>
    Task<bool> StockOut(int stockProductId, int warehouseId, int quantity, User operateUser, StockExtData data);

    /// <summary>
    /// 库存产品调拨/转仓
    /// </summary>
    /// <param name="stockProductId"></param>
    /// <param name="warehouseId"></param>
    /// <param name="toWarehouseId"></param>
    /// <param name="quantity"></param>
    /// <param name="operateUser"></param>
    /// <returns></returns>
    Task<bool> Transfer(int stockProductId, int warehouseId, int toWarehouseId, int quantity, User operateUser, StockExtData data);
    
    /// <summary>
    /// 库存产品调拨/转仓
    /// </summary>
    /// <param name="stockProduct"></param>
    /// <param name="warehouse"></param>
    /// <param name="toWarehouse"></param>
    /// <param name="quantity"></param>
    /// <param name="operateUser"></param>
    /// <returns></returns>
    Task<bool> Transfer(StockProduct stockProduct, Warehouse warehouse, Warehouse toWarehouse, int quantity,
        User operateUser, StockExtData data);

    /// <summary>
    /// Stock information
    /// </summary>
    /// <returns></returns>
    Task<StockProduct?> Info(User user, int id);

    /// <summary>
    /// 仓储日志分页列表
    /// </summary>
    /// <returns></returns>
    Task<DbSetExtension.PaginateStruct<StockLog>> LogPageList(User user, StoreLogViewModel request);

    /// <summary>
    /// 仓储日志列表
    /// </summary>
    /// <returns></returns>
    Task<List<StockLog>> LogList(User sessionData, string? orderNo, bool isInfo = false);

    /// <summary>
    /// 待确认仓储日志列表
    /// </summary>
    /// <returns></returns>
    Task<DbSetExtension.PaginateStruct<StockLog>> AuditLogPageList(User user, StockLog.Shelves shelve, int? warehouseId = null, string orderNo = "", DateTime? startDate = null,
        DateTime? endDate = null);

    /// <summary>
    /// 待确认仓储日志列表
    /// </summary>
    /// <returns></returns>
    Task<List<StockLog>> AuditLogList(User user, string orderNo);

    /// <summary>
    /// 获取有库存的仓库
    /// </summary>  
    /// <returns></returns>
    Task<DbSetExtension.PaginateStruct<Stock>> StockOutWarehouseList(int id, User user);

    /// <summary>
    /// 根据产品id，变体id获取有库存的仓库
    /// </summary>
    /// <param name="productId">产品id</param>
    /// <param name="variantId">变体hashcode</param>
    /// <param name="allWarehouse">是否展示所有仓库，没有库存的仓库库存为0</param>
    /// <returns></returns>
    Task<List<Stock>> StockOutWarehouseListByProduct(int productId, int variantId, User user, bool allWarehouse = false);

    /// <summary>
    /// 获取有库存的仓库(分页)
    /// </summary>
    /// <returns></returns>
    Task<DbSetExtension.PaginateStruct<Stock>> StockOutWarehousePaginate(User user, int? id, int? productId = null, int? hashcode = null);

    /// <summary>
    /// 判断是否存在库存商品
    /// </summary>
    /// <param name="productId">库存商品id</param>
    /// <returns></returns>
    Task<StockProduct?> Exists(int productId);

    /// <summary>
    /// 判断是否存在库存商品
    /// </summary>
    /// <param name="productName">库存商品名称</param>
    /// <returns></returns>
    Task<StockProduct?> Exists(string productName);

    /// <summary>
    /// 对应仓库是否存在库存
    /// </summary>
    /// <param name="warehouseId"></param>
    /// <returns></returns>
    Task<bool> HasStockInWarehouse(int warehouseId);
    /// <summary>
    /// 对应货架是否存在库存
    /// </summary>
    /// <param name="shelf"></param>
    /// <returns></returns>
    Task<bool> HasStockInShelf(Shelf shelf);

    /// <summary>
    /// 对应产品是否存在库存
    /// </summary>
    /// <param name="stockProductId"></param>
    /// <returns></returns>
    Task<bool> HasStockByStockProductId(int stockProductId);

    /// <summary>
    /// 通过仓库id获得有库存操作的运单id
    /// </summary>
    /// <param name="warehouseId"></param>
    /// <returns></returns>
    Task<List<int>> GetLogWaybillIdByWarehouse(int warehouseId);
    
    /// <summary>
    /// 通过仓库id获得有库存操作的采购id
    /// </summary>
    /// <param name="warehouseId"></param>
    /// <returns></returns>
    Task<List<int>> GetLogPurchaseIdByWarehouse(int warehouseId);
}

/// <summary>
/// 提供给库存日志使用
/// </summary>
public record StockExtData
{
    public StockExtData(string remark, string? orderNo = null, int? waybillId = null, string? waybillOrder = null, int? purchaseId = null, string? purchaseTrackingNumber = null)
    {
        Remark = remark;
        OrderNo = orderNo ?? string.Empty;
        WaybillId = waybillId;
        PurchaseId = purchaseId;
        PurchaseTrackingNumber = purchaseTrackingNumber ?? string.Empty;
        WaybillOrder = waybillOrder ?? string.Empty;
    }
    
    public string Remark { get; }
    public string OrderNo { get;}
    public int? WaybillId { get; }
    public int? PurchaseId { get; }
    public string PurchaseTrackingNumber { get; }
    public string WaybillOrder { get;}

    public static implicit operator StockExtData(string remark) => new (remark);
}