﻿using Ryu.Data;
using Ryu.String;

namespace ERP.ViewModels.Storage;

public class StockProductDto
{
    public StockProductDto(int id, string productName, int? productId, int totalQuantity, DateTime updatedAt)
    {
        ProductName = productName;
        ProductId = productId ?? 0;
        TotalQuantity = totalQuantity;
        UpdatedAt = updatedAt;
        Id = id;
    }

    public string? Sku { get; set; }
    public string? Spu { get; set; }
    
    public int Id { get; set; }
    public string ProductName { get; set; }
    public int ProductId { get; set; }
    public int TotalQuantity { get; set; }
    public DateTime UpdatedAt { get; set; }
}

public class StockDto
{
    public StockDto(int id, string productName, int? productId, int quantity, int warehouseId, string warehouseName, int? shelfId, DateTime updatedAt)
    {
        ProductName = productName;
        ProductId = productId ?? 0;
        Quantity = quantity;
        WarehouseId = warehouseId;
        WarehouseName = warehouseName.Contains('[') ? warehouseName.Before("[") : warehouseName;
        ShelfId = shelfId;
        ShelfName = warehouseName.After("[").Before("]");
        UpdatedAt = updatedAt;
        Id = id;
    }

    public StockDto(Stock s)
    {
        ProductName = s.ProductName;
        ProductId = s.ProductId ?? 0;
        Quantity = s.Quantity;
        WarehouseId = s.WarehouseId;
        WarehouseName = s.WarehouseName.Contains('[') ? s.WarehouseName.Before("[") : s.WarehouseName;
        Warehouse = s.WarehouseName.Contains('[') ? s.WarehouseName.Before("[") : s.WarehouseName;
        ShelfId = s.ShelfId;
        ShelfName = s.WarehouseName.After($"{Warehouse}[{Warehouse}-").Before("]");
        UpdatedAt = s.UpdatedAt;
        Id = s.ID;
        Url = s.StockProduct?.Url;
        Price = s.StockProduct!.PurchasingCost;
        Sku = s.StockProduct.Sku;
        StockProductId = s.StockProductId;
    }

    public int StockProductId { get; set; }

    public string? Url { get; set; }
    public Money Price { get; set; }
    public string Warehouse { get; set; }


    public string? Sku { get; set; }
    
    public int Id { get; set; }
    public string ProductName { get; set; }
    public int ProductId { get; set; }
    public int Quantity { get; set; }
    public int WarehouseId { get; }
    public string WarehouseName { get; }
    public int? ShelfId { get; }
    public string ShelfName { get; }
    public DateTime UpdatedAt { get; set; }
}