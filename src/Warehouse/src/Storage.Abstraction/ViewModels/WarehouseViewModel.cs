﻿namespace ERP.ViewModels.Storage;

public class WarehouseViewModel
{
    public  int   Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Address { get; set; } = string.Empty;
    public string KeeperName { get; set; } = string.Empty;
    public string KeeperPhone { get; set; } = string.Empty;
    public string Remark { get; set; } = string.Empty;
    public Warehouse.Types Type { get; set; } = Warehouse.Types.Personal;
}

public class StoreLogViewModel
{
    public int? id { get; set; }
    public string productName { get; set; } = string.Empty;
    public string warehouseName { get; set; } = string.Empty;
    public int? warehouseId { get; set; }
    public string date { get; set; } = string.Empty;
    public float limit { get; set; }
    public string orderNo { get; set; } = string.Empty;
    public string purchaseTrackingNumber { get; set; } = string.Empty;
    public int? waybillId { get; set; }
    public int shelve { get; set; }
    public bool isStock { get; set; } = false;
    public DateTime? startDate { get; set; } = null;
    public DateTime? endDate { get; set; } = null;
}