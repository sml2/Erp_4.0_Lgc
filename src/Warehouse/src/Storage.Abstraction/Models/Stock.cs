﻿using ERP.Models.DB.Identity;
using ERP.Storage.Abstraction.Services;
using HotChocolate;

namespace ERP.Models.DB.Storage;

[Index(nameof(ProductId), nameof(StockProductId), nameof(VariantId), nameof(WarehouseId)), Comment("仓储管理_库存商品表")]
public class Stock : UserModel
{
    private readonly List<StockLog> _stockLogs;

    public Stock()
    {
        _stockLogs = new List<StockLog>();
    }
    public Stock(StockProduct stockProduct, Warehouse warehouse,
        int userId, int groupId, int companyId, int oemId,
        int quantity = 0, Ryu.Data.Money purchasingCost = default, decimal discount = 0) : this()
    {

        UpdateStockProduct(stockProduct ?? throw new ArgumentNullException(nameof(stockProduct)));
        UpdateWarehouse(warehouse ?? throw new ArgumentNullException(nameof(warehouse)));

        UserID = userId;
        GroupID = groupId;
        CompanyID = companyId;
        OEMID = oemId;

        Quantity = quantity;
        PurchasingCost = purchasingCost;
        Discount = discount;
    }

    public Stock(StockProduct stockProduct, Shelf shelf, int userId, int groupId, int companyId, int oemId,
        int quantity = 0, Ryu.Data.Money purchasingCost = default, decimal discount = 0) : this()
    {
        UpdateStockProduct(stockProduct ?? throw new ArgumentNullException(nameof(stockProduct)));
        UpdateShelf(shelf ?? throw new ArgumentNullException(nameof(shelf)));

        UserID = userId;
        GroupID = groupId;
        CompanyID = companyId;
        OEMID = oemId;

        Quantity = quantity;
        PurchasingCost = purchasingCost;
        Discount = discount;
    }
    
    public Stock(StockProduct stockProduct, Shelf shelf,User user) : this()
    {
        UpdateStockProduct(stockProduct ?? throw new ArgumentNullException(nameof(stockProduct)));
        UpdateShelf(shelf ?? throw new ArgumentNullException(nameof(shelf)));

        UserID = user.Id;
        GroupID = user.GroupID;
        CompanyID = user.GroupID;
        OEMID = user.OEMID;

        Quantity = stockProduct.TotalQuantity;
        PurchasingCost = stockProduct.PurchasingCost;
        // Discount = stockProduct.D;
    }

    /// <summary>
    /// 仓储商品id
    /// </summary>
    [Comment("仓储商品id")]
    public int StockProductId { get; set; }

    public StockProduct? StockProduct { get; set; }

    /// <summary>
    /// 商品id
    /// </summary>
    [Comment("商品id")]
    public int? ProductId { get; set; }

    /// <summary>
    /// 商品变体id
    /// </summary>
    [Comment("商品变体id")]
    public int VariantId { get; set; }

    /// <summary>
    /// 商品名称
    /// </summary>
    [Comment("商品名称")]
    public string ProductName { get; set; } = null!;

    /// <summary>
    /// 商品图片
    /// </summary>
    [Comment("商品图片")]
    public string Url { get; set; }=string.Empty;

    /// <summary>
    /// 仓库id
    /// </summary>
    [Comment("仓库id")]
    public int WarehouseId { get; set; }

    public Warehouse Warehouse { get; set; } = null!;
    
    /// <summary>
    /// 货架id
    /// </summary>
    [Comment("货架id")]
    public int? ShelfId { get; set; }

    public Shelf? Shelf { get; set; }

    /// <summary>
    /// 仓库名称
    /// </summary>
    [Comment("仓库名称")]
    public string WarehouseName { get; set; } = null!;

    /// <summary>
    /// 数量
    /// </summary>
    [Comment("数量")]
    public int Quantity { get; set; }

    // /// <summary>
    // /// 所有仓库的总数量
    // /// </summary>
    // [Comment("所有仓库的数量")]
    // public int TotalQuantity { get; set; }

    // public enum States
    // {
    //     [Description("不需要")]
    //     NONEED = 0,
    //
    //     [Description("需要")]
    //     NEED = 1
    // }

    // /// <summary>
    // /// 是否需要库存:0=不需要,1=需要
    // /// </summary>
    // [Comment("是否需要库存:0=不需要,1=需要")]
    // public States State { get; set; }

    /// <summary>
    /// 进货单价
    /// </summary>
    [Comment("进货单价"), GraphQLIgnore]
    public Ryu.Data.Money PurchasingCost { get; set; }
    
    /// <summary>
    /// 进货单价
    /// </summary>
    [Comment("总价"), GraphQLIgnore]
    public Ryu.Data.Money TotalPrice { get; set; }

    /// <summary>
    /// 折扣
    /// </summary>
    [Comment("折扣")]
    public decimal Discount { get; set; }

    public IReadOnlyCollection<StockLog> StockLogs => _stockLogs;

    public void UpdateStockProduct(StockProduct stockProduct)
    {
        StockProduct = stockProduct;
        StockProductId = stockProduct.ID;
        ProductId = stockProduct.ProductId;
        ProductName = stockProduct.ProductName;
        Url = stockProduct.Url;
    }

    public void UpdateWarehouse(Warehouse warehouse)
    {
        Warehouse = warehouse;
        WarehouseName = warehouse.Name;
    }
    
    public void UpdateShelf(Shelf shelf)
    {
        UpdateWarehouse(shelf.Warehouse);
        Shelf = shelf;
        WarehouseName += $"[{shelf.Name}]";
    }

    /// <summary>
    /// 添加出入库日志
    /// </summary>
    /// <param name="quantity"></param>
    /// <param name="remark"></param>
    /// <param name="state"></param>
    /// <param name="isShelve"></param>
    /// <param name="user"></param>
    /// <param name="orderNo"></param>
    /// <param name="purchaseId"></param>
    /// <param name="purchaseTrackingNumber"></param>
    /// <returns></returns>
    public bool AddLog(int quantity, StockLog.States state, StockLog.Shelves isShelve, User user, StockExtData data, Warehouse? toWarehouse = null )
    {
        var log = new StockLog(this, user.Id, GroupID, CompanyID, user.Company.Name, OEMID, quantity, state, isShelve, data.Remark, data.OrderNo, data.PurchaseId, data.PurchaseTrackingNumber)
        {
            UserName = user.UserName,

            ExecuteCompanyId = user.CompanyID,
            ExecuteUserId = user.Id,
            ExecuteUserName = user.TrueName,
            ToWarehouseId = toWarehouse?.ID,
            ToWarehouseName = toWarehouse?.Name ?? string.Empty,
            WaybillOrder = data.WaybillOrder,
            WaybillId = data.WaybillId,
        };
        
        _stockLogs.Add(log);
        return true;
    }
}