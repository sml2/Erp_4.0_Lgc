using ERP.Models.DB.Identity;

namespace ERP.Models.DB.Storage;

/// <summary>
/// 货架表，表示仓库中的某个区域或具体货架，为仓库下属的某个单元划分
/// </summary>
[Comment("库存管理_货架列表")]
public class Shelf : UserModel
{
    public Shelf() {}
    
    public Shelf(string name, Warehouse warehouse, User createUser,ShelvesStateEnum state, Company? distributionCompany = null) : this()
    {
        Name = name;
        Warehouse = warehouse;
        DistributionCompany = distributionCompany;
        UserID = createUser.Id;
        GroupID = createUser.GroupID;
        CompanyID = createUser.CompanyID;
        OEMID = createUser.OEMID;
        State = state;
    }

    /// <summary>
    /// 货架名称
    /// </summary>
    [Comment("货架名称")]
    public string Name { get; private set; } = null!;
    
    [Comment("所属仓库id")]
    public int WarehouseId { get; init; }

    [ForeignKey(nameof(WarehouseId))] public Warehouse Warehouse { get; init; } = null!;
    
    [Comment("共享分销公司id")]
    public int? DistributionCompanyId { get; init; }
    public Company? DistributionCompany { get; init; }
    
    public ShelvesStateEnum State { get; set; }

    public void SetName(string name)
    {
        Name = name;
    }
    
    public void SetState(ShelvesStateEnum state)
    {
        State = state;
    }

    public override string ToString()
    {
        return $"{Name}@{ID}#Warehouse:{Warehouse}";
    }
    
}

public enum ShelvesStateEnum
{
    /// <summary>
    /// 正常
    /// </summary>
    Normal,
    /// <summary>
    /// 停用货架
    /// </summary>
    DeactivateShelves,
    /// <summary>
    /// 停止收货
    /// </summary>
    StopReceivingGoods
    
}