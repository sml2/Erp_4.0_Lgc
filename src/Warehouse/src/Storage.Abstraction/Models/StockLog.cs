﻿using HotChocolate;

namespace ERP.Models.DB.Storage;

[Comment("仓储管理_库存日志表")]
public class StockLog : UserModel
{
    public StockLog() { }
    public StockLog(Stock stock, int userId, int groupId, int companyId, string companyName, int oemId, int quantity, States state, Shelves isShelve,
        string remark = "", string orderNo = "", int? purchaseId = null, string purchaseTrackingNumber = "") : this()
    {
        StockProduct = stock.StockProduct;
        StockProductId = stock.StockProductId;
        ProductName = stock.ProductName;

        WarehouseId = stock.WarehouseId;
        ShelfId = stock.ShelfId;
        WarehouseName = stock.WarehouseName;

        UserID = userId;
        GroupID = groupId;
        CompanyID = companyId;
        CompanyName = companyName;
        OEMID = oemId;

        State = state;
        Shelve = isShelve;
        Quantity = quantity;
        PurchasingCost = stock.PurchasingCost;

        OrderNo = orderNo;
        OrderNoHashcode = Helpers.CreateHashCode(orderNo);
        Remark = remark;
        PurchaseId = purchaseId;
        PurchaseTrackingNumber = purchaseTrackingNumber;
        PurchaseTrackingNumberHashcode = Helpers.CreateHashCode(purchaseTrackingNumber);

    }

    /// <summary>
    /// 操作人名称
    /// </summary>
    [Comment("操作人名称")]
    public string UserName { get; set; } = null!;

    /// <summary>
    /// 仓储商品id
    /// </summary>
    [Comment("仓储商品id")]
    public int StockProductId { get; set; }
    
    [ForeignKey(nameof(StockProductId))]
    public StockProduct? StockProduct { get; set; }

    /// <summary>
    /// 商品id
    /// </summary>
    [Comment("商品id")]
    public int ProductId { get; set; }

    /// <summary>
    /// 商品名称
    /// </summary>
    [Comment("商品名称")]
    public string ProductName { get; set; } = null!;

    /// <summary>
    /// 商品变体id
    /// </summary>
    [Comment("商品变体id")]
    public int VariantId { get; set; }

    /// <summary>
    /// 公司名称
    /// </summary>
    [Comment("公司名称")]
    public string CompanyName { get; set; } = null!;

    /// <summary>
    /// 仓库id
    /// </summary>
    [Comment("仓库id")]
    public int WarehouseId { get; set; }
    public int? ShelfId { get; set; }
    
    [ForeignKey(nameof(WarehouseId))]
    public Warehouse? Warehouse { get; set; }

    /// <summary>
    /// 仓库名称
    /// </summary>
    [Comment("仓库名称")]
    public string WarehouseName { get; set; } = null!;

    /// <summary>
    /// 数量
    /// </summary>
    [Comment("数量")]
    public int Quantity { get; set; }

    /// <summary>
    /// 进货单价
    /// </summary>
    [Comment("进货单价"), GraphQLIgnore]
    public Ryu.Data.Money PurchasingCost { get; set; }

    [GraphQLName("StockLogStates")]
    public enum States
    {
        [Description("出库")]
        IN = 1,

        [Description("入库")]
        OUT = 0,
        
        [Description("调拨")]
        Transfer = 2,
    }

    /// <summary>
    /// 操作状态
    /// </summary>
    [Comment("操作状态")]
    public States State { get; set; }

    public enum Shelves
    {
        [Description("搁置")]
        TRUE = 1,

        [Description("正常状态")]
        FALSE = 0
    }

    /// <summary>
    /// 搁置状态:0=正常状态1=已搁置
    /// </summary>
    [Comment("搁置状态:0=正常状态1=已搁置")]
    public Shelves Shelve { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    [Comment("备注")]
    public string Remark { get; set; } = null!;

    /// <summary>
    /// 订单编号
    /// </summary>
    [Comment("订单编号")]
    public string OrderNo { get; set; } = null!;

    /// <summary>
    /// 采购id
    /// </summary>
    [Comment("采购id")]
    public int? PurchaseId { get; set; }

    /// <summary>
    /// 采购运单号
    /// </summary>

    [Comment("采购运单号")]
    public string PurchaseTrackingNumber { get; set; } = null!;
    
    [GraphQLIgnore]
    public ulong PurchaseTrackingNumberHashcode { get; set; }

    /// <summary>
    /// 运单id
    /// </summary>

    [Comment("运单id")]
    public int? WaybillId { get; set; }

    /// <summary>
    /// 运单订单号
    /// </summary>
    [Comment("运单订单号")]
    public string WaybillOrder { get; set; } = null!;

    /// <summary>
    /// 添加公司id
    /// </summary>
    [Comment("添加公司id")]
    public int ExecuteCompanyId { get; set; }

    /// <summary>
    /// 添加用户id
    /// </summary>
    [Comment("添加用户id")]
    public int ExecuteUserId { get; set; }

    /// <summary>
    /// 添加用户名
    /// </summary>
    [Comment("添加用户名")]
    public string ExecuteUserName { get; set; } = null!;

    /// <summary>
    /// 目标仓库id（调拨）
    /// </summary>
    [Comment("目标仓库id(调拨)")]
    public int? ToWarehouseId { get; set; }

    /// <summary>
    /// 目标仓库名称（调拨）
    /// </summary>
    [Comment("目标仓库名称(调拨)")]
    public string ToWarehouseName { get; set; } = null!;

    /// <summary>
    /// 订单编号hashcode 搜索用
    /// </summary>
    [Comment("订单编号hashcode 搜索用"), GraphQLIgnore]
    public ulong OrderNoHashcode { get; set; }
}