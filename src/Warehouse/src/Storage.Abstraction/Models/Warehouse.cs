﻿using ERP.Models.DB.Identity;

namespace ERP.Models.DB.Storage;

/// <summary>
/// 采购管理_采购商表
/// </summary>
[Comment("库存管理_仓库表")]
public class Warehouse : UserModel
{
    public Warehouse()
    {
    }
    
    public Warehouse(string name, string address, string keeperName, string keeperPhone, Types type, User operateUser, bool useShelf, Company? distCompany, string remark): this()
    {
        Name = name;
        Address = address;
        KeeperName = keeperName;
        KeeperPhone = keeperPhone;
        Type = type;
        UseShelf = useShelf;
        Remark = remark;
        DistributionCompanyId = distCompany?.ID;
        DistributionCompanyName = distCompany?.Name ?? string.Empty;
        UserID = operateUser.Id;
        CompanyID = operateUser.CompanyID;
        OEMID = operateUser.OEMID;
        GroupID = operateUser.GroupID;
    }
    
    /// <summary>
    /// 下级公司id
    /// </summary>
    [Comment("下级公司id")]
    public int? DistributionCompanyId { get; set; }
    public Company? DistributionCompany { get; set; }

    /// <summary>
    /// 下级公司名称
    /// </summary>
    [Comment("下级公司名称"), MaxLength(50)]
    public string DistributionCompanyName { get; set; } = null!;

    public enum Types
    {
        [Description("普通")]
        Personal = 1,

        [Description("共享")]
        Shared = 2
    }

    /// <summary>
    /// 仓库类型：1=普通，2=共享
    /// </summary>
    [Comment("仓库类型：1=普通，2=共享")]
    public Types Type { get; set; }

    /// <summary>
    /// 是否强制使用货架。为true时，必须选择货架，不能只选择仓库
    /// </summary>
    public bool UseShelf { get; set; }

    /// <summary>
    /// 仓库名称
    /// </summary>
    [Comment("仓库名称"), MaxLength(50)]
    public string Name { get; set; } = null!;

    /// <summary>
    /// 详细地址
    /// </summary>
    [Comment("详细地址"), MaxLength(255)]
    public string Address { get; set; } = null!;

    /// <summary>
    /// 仓库管理员名称
    /// </summary>
    [Comment("仓库管理员名称"), MaxLength(20)]
    public string KeeperName { get; set; } = null!;

    /// <summary>
    /// 仓库管理员电话
    /// </summary>
    [Comment("仓库管理员电话"), MaxLength(20)]
    public string KeeperPhone { get; set; } = null!;

    /// <summary>
    /// 备注
    /// </summary>
    [Comment("备注")]
    public string Remark { get; set; } = null!;

    public ICollection<Shelf> Shelves { get; set; } = new List<Shelf>();

    public Shelf AddShelf(string name, User createUser,ShelvesStateEnum state)
    {
        var shelf = new Shelf(name, this, createUser,state);
        Shelves.Add(shelf);
        return shelf;
    }

    public void SetDistributionCompany(Company company)
    {
        DistributionCompanyId = company.ID;
        DistributionCompanyName = company.Name;
    }

    public override string ToString()
    {
        return $"{Name}@{ID}{{type:{Type.ToString()}}}";
    }
}