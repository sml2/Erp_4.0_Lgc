﻿using HotChocolate;

namespace ERP.Models.DB.Storage;

[Index(nameof(CreatedUserId), nameof(ProductId)), Comment("库存产品表")]
public class StockProduct : UserModel
{
    public StockProduct()
    {
    }

    public StockProduct(string productName, string url, int createdUserId, string createdUsername, int companyId,
        int groupId, int oemId, string sku, decimal purchasingCost, int totalQuantity = 0, int productId = 0,
        string variantId = "")
    {
        ProductName = productName;
        Url = url;
        UserID = createdUserId;
        CreatedUserId = createdUserId;
        CreatedUsername = createdUsername;
        CompanyID = companyId;
        GroupID = groupId;
        OEMID = oemId;
        TotalQuantity = totalQuantity;
        ProductId = productId;
        VariantId = variantId;
        Sku = sku;
        PurchasingCost = purchasingCost;
    }

    /// <summary>
    /// 商品id
    /// </summary>
    [Comment("商品id")]
    public int? ProductId { get; init; }

    // public Models.Product.Product Product { get; set; }

    /// <summary>
    /// 变体id
    /// </summary>
    [Comment("变体id")]
    public string VariantId { get; init; } = string.Empty;

    /// <summary>
    /// 商品名称
    /// </summary>
    [Comment("商品名称")]
    public string ProductName { get; init; } = null!;

    /// <summary>
    /// 商品图片
    /// </summary>
    [Comment("商品图片")]
    public string Url { get; init; } = null!;

    /// <summary>
    /// 创建者id
    /// </summary>
    [Comment("创建者id")]
    public int CreatedUserId { get; set; }

    /// <summary>
    /// 创建者
    /// </summary>
    [Comment("创建者")]
    public string CreatedUsername { get; init; } = string.Empty;

    /// <summary>
    /// 所有仓库的总数量
    /// </summary>
    [Comment("所有仓库的总数量")]
    public int TotalQuantity { get; set; }

    /// <summary>
    /// 商品Sku
    /// </summary>
    [Comment("商品Sku")]
    public string Sku { get; set; }

    [GraphQLIgnore]
    public Ryu.Data.Money PurchasingCost { get; set; }

    // public enum States
    // {
    //     [Description("需要库存")]
    //     NEED_STOCK = 1,
    //
    //     [Description("不需要库存")]
    //     NOTNEEDSTOCK = 2
    // }

    // /// <summary>
    // /// 是否需要库存:1=需要,2=不需要
    // /// </summary>
    // [Comment("是否需要库存:1=需要,2=不需要")]
    // public States State { get; init; }

    // /// <summary>
    // /// 预设进货单价
    // /// </summary>
    // [Comment("预设进货单价")]
    // public decimal PresetPurchasingCost { get; init; }

    // /// <summary>
    // /// 预设折扣
    // /// </summary>
    // [Comment("预设折扣")]
    // public int PresetDiscount { get; init; }

    public override string ToString()
    {
        return $"{ProductName}@{ID}";
    }

    public void UpdateTotalQuantity(int quantity)
    {
        TotalQuantity += quantity;
    }
}