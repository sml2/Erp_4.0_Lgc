﻿using ERP.Data;
namespace ERP.Storage.Api.Repositories;

public interface IStorageRepository
{
    IUnitOfWork UnitOfWork { get; }
    T Add<T>(T newObject) where T : class;
    void Remove<T>(T needToDelete) where T : class;
}

public class StorageRepository : IStorageRepository
{
    private readonly Base _dbContext;
    public IUnitOfWork UnitOfWork => _dbContext;
    
    public T Add<T>(T newObject) where T : class
    {
        return _dbContext.Set<T>().Add(newObject).Entity;
    }

    public void Remove<T>(T needToDelete) where T : class
    {
        _dbContext.Set<T>().Remove(needToDelete);
    }

    public StorageRepository(Base dbContext)
    {
        _dbContext = dbContext;
    }

}