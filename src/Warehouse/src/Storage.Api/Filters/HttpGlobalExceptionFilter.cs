﻿namespace ERP.Storage.Api.Filters;

public class HttpGlobalExceptionFilter : IExceptionFilter
{
    private readonly ILogger<HttpGlobalExceptionFilter> logger;

    public HttpGlobalExceptionFilter(ILogger<HttpGlobalExceptionFilter> logger)
    {
        this.logger = logger;
    }

    public void OnException(ExceptionContext context)
    {
        if (context.Exception is StorageDomainException ex)
        {
            logger.LogError(new EventId(context.Exception.HResult), ex, null);
            context.Result = new JsonResult(BaseController.Error(context.Exception));
            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.OK;
            context.ExceptionHandled = true;
        }
    }
}
