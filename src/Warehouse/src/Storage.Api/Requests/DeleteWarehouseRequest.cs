﻿namespace ERP.Storage.Api.Requests;

public record DeleteWarehouseRequest(int Id) : IRequest<bool>
{
    [ValidateNever]
    public User User { get; set; } = null!;
}