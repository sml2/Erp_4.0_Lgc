﻿namespace ERP.Storage.Api.Requests;

public class DeleteWarehouseRequestHandler : IRequestHandler<DeleteWarehouseRequest, bool>
{
    private readonly IWarehouseService _warehouseService;
    private readonly IStorageRepository _repository;
    private readonly ILogger<DeleteWarehouseRequestHandler> _logger;
    private readonly IStockService _stockService;

    public DeleteWarehouseRequestHandler(IWarehouseService warehouseService, IStorageRepository repository, ILogger<DeleteWarehouseRequestHandler> logger,
        IStockService stockService)
    {
        _warehouseService = warehouseService;
        _repository = repository;
        _logger = logger;
        _stockService = stockService;
    }

    public async Task<bool> Handle(DeleteWarehouseRequest request, CancellationToken cancellationToken = default)
    {
        var needToDelete = await _warehouseService.GetWarehouseById(request.Id, request.User.CompanyID);
        if (needToDelete is null)
            throw new StorageDomainException("没有该信息");

        var hasStock = await _stockService.HasStockInWarehouse(request.Id);

        if (hasStock)
            throw new StorageDomainException("当前仓库仍有关联库存, 无法删除");
        
        _repository.Remove(needToDelete);
       
        var result = await _repository.UnitOfWork.SaveChangesAsync(cancellationToken);
        
        _logger.LogInformation("User [{User}] delete a warehouse [{Warehouse}]", 
            request.User,
            needToDelete);

        return result > 0;
    }
}