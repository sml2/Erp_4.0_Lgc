﻿namespace ERP.Storage.Api.Requests;

public class DeleteStockProductRequestHandler : IRequestHandler<DeleteStockProductRequest, bool>
{
    private readonly IStockService _stockService;
    private readonly IStorageRepository _repository;
    private readonly ILogger<DeleteStockProductRequestHandler> _logger;

    public DeleteStockProductRequestHandler(IStockService stockService, IStorageRepository repository, ILogger<DeleteStockProductRequestHandler> logger)
    {
        _stockService = stockService;
        _repository = repository;
        _logger = logger;
    }

    public async Task<bool> Handle(DeleteStockProductRequest request, CancellationToken cancellationToken = default)
    {
        var needToDelete = await _stockService.Info(request.User, request.Id);
        if (needToDelete is null)
            throw new StorageDomainException("无法找到该产品信息, 或已被删除");

        var hasStock = await _stockService.HasStockByStockProductId(request.Id);

        if (hasStock)
            throw new StorageDomainException("该产品存在关联库存无法删除", $"该产品[{needToDelete}]存在关联库存无法删除");
        
        _repository.Remove(needToDelete);
        
        var result = await _repository.UnitOfWork.SaveChangesAsync(cancellationToken);
        
        _logger.LogInformation("User [{User}] delete a stock product [{StockProduct}]", 
            request.User,
            needToDelete);

        return result > 0;
    }
}