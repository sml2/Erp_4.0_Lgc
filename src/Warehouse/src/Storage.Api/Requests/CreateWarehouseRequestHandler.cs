﻿namespace ERP.Storage.Api.Requests;

public class CreateWarehouseRequestHandler : IRequestHandler<CreateWarehouseRequest, bool>
{
    private readonly IWarehouseService _warehouseService;
    private readonly IStorageRepository _repository;
    private readonly ILogger<CreateWarehouseRequestHandler> _logger;
    private readonly ICompanyService _companyService;

    public CreateWarehouseRequestHandler(IWarehouseService warehouseService, IStorageRepository repository, ILogger<CreateWarehouseRequestHandler> logger,
        ICompanyService companyService)
    {
        _warehouseService = warehouseService;
        _repository = repository;
        _logger = logger;
        _companyService = companyService;
    }

    public async Task<bool> Handle(CreateWarehouseRequest request, CancellationToken cancellationToken)
    {
        var currentCompanyId = request.User.CompanyID;

        // 检测仓库名称重复
        var row = await _warehouseService.GetWarehouseByName(request.Name, currentCompanyId);
        if (row is not null)
            throw new StorageDomainException("该仓库名已存在");

        // 验证共享仓库数据
        Company? distCompany = null;
        if (request.Type == Warehouse.Types.Shared)
        {
            if (!request.DistributionCompanyId.HasValue)
                throw new StorageDomainException("共享仓库必须选择下级公司");
            distCompany = await _companyService.GetInfoById(request.DistributionCompanyId.Value);
            if (distCompany is null)
            {
                throw new StorageDomainException("共享仓库必须选择下级公司");
            }
        }

        var newWarehouse = new Warehouse(request.Name, request.Address, request.KeeperName, request.KeeperPhone, request.Type, request.User, request.UseShelf, distCompany, request.Remark);

        _repository.Add(newWarehouse);
        var result = await _repository.UnitOfWork.SaveChangesAsync(cancellationToken);
        
        //不强制使用的货架的仓库，默认创建货架
        if (!newWarehouse.UseShelf)
        {
            var shelf = new Shelf(
                $"{newWarehouse.Name}-默认货架1",
                newWarehouse,
                request.User,
                ShelvesStateEnum.Normal,
                distCompany
            );
            _repository.Add(shelf);
            await _repository.UnitOfWork.SaveChangesAsync(cancellationToken);
        }

        _logger.LogInformation("User [{User}] create a warehouse [{Warehouse}]",
            request.User,
            newWarehouse);
        return result > 0;
    }
}