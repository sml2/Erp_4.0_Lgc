﻿namespace ERP.Storage.Api.Requests;

public record StockInRequest(int Id, decimal PurchasingCost, decimal? Discount, int Quantity, int WarehouseId, int? ShelfId, string OrderNo, string Remark) : IRequest<bool>
{
    public int Id { get; set; } = Id;
    public decimal PurchasingCost { get; set; } = PurchasingCost;
    public decimal? Discount { get; set; } = Discount;
    public int Quantity { get; set; } = Quantity;
    public int WarehouseId { get; set; } = WarehouseId;
    public int? ShelfId { get; set; } = ShelfId;
    public string OrderNo { get; set; } = OrderNo;
    public string Remark { get; set; } = Remark;
    public int StockProductId { get; set; }

    [ValidateNever]
    public User User { get; set; } = default!;
}

