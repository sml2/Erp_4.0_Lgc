﻿namespace ERP.Storage.Api.Requests;

public record UpdateShelfRequest(int Id, string Name,ShelvesStateEnum State) : IRequest<bool>
{
    [ValidateNever] public User User { get; set; } = null!;
}