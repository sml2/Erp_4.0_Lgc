﻿namespace ERP.Storage.Api.Requests;

public record DeleteShelfRequest(int Id) : IRequest<bool>
{
    [ValidateNever]
    public User User { get; set; } = default!;
}