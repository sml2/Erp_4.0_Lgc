﻿namespace ERP.Storage.Api.Requests;

public record DeleteStockProductRequest(int Id) : IRequest<bool>
{
    [ValidateNever]
    public User User { get; set; } = null!;
}