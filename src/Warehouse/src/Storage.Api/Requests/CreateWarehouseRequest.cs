﻿namespace ERP.Storage.Api.Requests;

public record CreateWarehouseRequest() : IRequest<bool>
{
    public CreateWarehouseRequest(string name, string keeperName, string address, string keeperPhone, string remark, bool useShelf, Warehouse.Types type = Warehouse.Types.Personal) : this()
    {
        Name = name;
        KeeperName = keeperName;
        Address = address;
        KeeperPhone = keeperPhone;
        Remark = remark;
        UseShelf = useShelf;
        Type = type;
    }
    public string Name { get; set; }=string.Empty;
    public string Address { get; set; } = string.Empty;
    public string KeeperName { get; set; } = string.Empty;
    public string KeeperPhone { get; set; } = string.Empty;
    public string Remark { get; set; } = string.Empty;
    public int? DistributionCompanyId { get; set; }
    public Warehouse.Types Type { get; set; }
    public bool UseShelf { get; set; }

    [ValidateNever]
    public User User { get; set; } = default!;
}