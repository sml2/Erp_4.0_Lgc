﻿namespace ERP.Storage.Api.Requests;

public class CreateShelfRequestHandler : IRequestHandler<CreateShelfRequest, bool>
{
    private readonly IWarehouseService _warehouseService;
    private readonly IShelfService _shelfService;
    private readonly IStorageRepository _repository;
    private readonly ILogger<CreateShelfRequestHandler> _logger;

    public CreateShelfRequestHandler(IWarehouseService warehouseService, IShelfService shelfService, IStorageRepository repository, ILogger<CreateShelfRequestHandler> logger)
    {
        _warehouseService = warehouseService;
        _shelfService = shelfService;
        _repository = repository;
        _logger = logger;
    }

    public async Task<bool> Handle(CreateShelfRequest request, CancellationToken cancellationToken)
    {
        var warehouse = await _warehouseService.Info(request.User, request.WarehouseId);
        if (warehouse is null)
            throw new StorageDomainException("仓库不存在");
        
        var existedShelf = await _shelfService.GetShelfByName(request.Name, request.WarehouseId);

        if (existedShelf is not null)
            throw new StorageDomainException($"货架{request.Name}已存在");

        var shelf = warehouse.AddShelf(request.Name, request.User,request.State);
        
        var result = await _repository.UnitOfWork.SaveChangesAsync(cancellationToken);
        
        _logger.LogInformation("User [{User}] create a shelf [{Shelf}] in warehouse [{Warehouse}]",
            request.User,
            shelf,
            warehouse);
        return result > 0;
    }
}