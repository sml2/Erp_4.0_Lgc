﻿namespace ERP.Storage.Api.Requests;


public record StockTransferRequest
    (int StockProductId, int WarehouseId, int ToWarehouseId, int Quantity, string Remark) : IRequest<bool>
{
    [ValidateNever]
    public User User { get; set; } = null!;
}