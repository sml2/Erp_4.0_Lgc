﻿namespace ERP.Storage.Api.Requests;

public class UpdateShelfRequestHandler : IRequestHandler<UpdateShelfRequest, bool>
{
    private readonly IShelfService _shelfService;
    private readonly ILogger<UpdateShelfRequestHandler> _logger;
    private readonly IStorageRepository _repository;

    public UpdateShelfRequestHandler(IShelfService shelfService, ILogger<UpdateShelfRequestHandler> logger, IStorageRepository repository)
    {
        _shelfService = shelfService;
        _logger = logger;
        _repository = repository;
    }

    public async Task<bool> Handle(UpdateShelfRequest request, CancellationToken cancellationToken)
    {
        
        var shelf = await _shelfService.GetShelfById(request.Id, request.User);

        if (shelf is null)
            throw new StorageDomainException("货架不存在");

        var exist = await _shelfService.GetShelfByName(request.Name, shelf.WarehouseId);
        if (exist is not null && exist.ID != shelf.ID)
            throw new StorageDomainException($"货架[{exist.Name}]存在同名,无法继续添加");

        var oldName = shelf.Name;
        shelf.SetName(request.Name);
        var oldState = shelf.State;
        shelf.SetState(request.State);
        
        
        var result = await _repository.UnitOfWork.SaveChangesAsync(cancellationToken);
        
        _logger.LogInformation("User [{User}] update shelf [{Shelf}] name from [{Old}] to [{New}] and state from [{OldState}] to [{NewState}]",
            request.User,
            shelf,
            oldName,
            request.Name,
            oldState,
            request.State);
        
        return result > 0;
    }
}