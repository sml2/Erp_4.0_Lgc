﻿namespace ERP.Storage.Api.Requests;

public record CreateStockRequest : IRequest<bool>
{
    public CreateStockRequest(string productName, string url, decimal purchasingCost, decimal? discount, int quantity, int warehouseId, string orderNo, string remark, string sku)
    {
        ProductName = productName;
        Url = url;
        PurchasingCost = purchasingCost;
        Discount = discount;
        Quantity = quantity;
        WarehouseId = warehouseId;
        OrderNo = orderNo;
        Remark = remark;
        Sku = sku;
    }

    public string ProductName { get; set; } = string.Empty;
    public string Url { get; set; } = string.Empty;
    public decimal PurchasingCost { get; set; }
    public decimal? Discount { get; set; }
    public int Quantity { get; set; }
    public int WarehouseId { get; set; }
    public int? ShelfId { get; set; }
    public string OrderNo { get; set; } = string.Empty;
    public string Remark { get; set; } = string.Empty;

    public string Sku { get; set; } = string.Empty;

    [ValidateNever]public User User { get; set; } = default!;
}