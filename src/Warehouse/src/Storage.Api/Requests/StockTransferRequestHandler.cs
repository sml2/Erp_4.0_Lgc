﻿namespace ERP.Storage.Api.Requests;

public class StockTransferRequestHandler : IRequestHandler<StockTransferRequest, bool>
{
    private readonly IStockService _stockService;
    private readonly ILogger<StockTransferRequestHandler> _logger;

    public StockTransferRequestHandler(IStockService stockService, ILogger<StockTransferRequestHandler> logger)
    {
        _stockService = stockService;
        _logger = logger;
    }

    public async Task<bool> Handle(StockTransferRequest request, CancellationToken cancellationToken)
    {
        if (request.WarehouseId == request.ToWarehouseId)
            throw new StorageDomainException("调拨仓库不能一致");
        
        var result = await _stockService.Transfer(request.StockProductId, request.WarehouseId, request.ToWarehouseId, request.Quantity, request.User, "手动调拨");
        
        _logger.LogInformation("User [{User}] transfer product @{ProductId} from warehouse @{Warehouse} to @{ToWarehouse} with quantity {Quantity}",
            request.User,
            request.StockProductId,
            request.WarehouseId,
            request.ToWarehouseId,
            request.Quantity);
        
        return result;
    }
}