﻿namespace ERP.Storage.Api.Requests;

public class StockInRequestHandler : IRequestHandler<StockInRequest, bool>
{
    private readonly IStockService _stockService;

    public StockInRequestHandler(IStockService stockService)
    {
        _stockService = stockService;
    }

    public async Task<bool> Handle(StockInRequest request, CancellationToken cancellationToken)
    {
        var result = await _stockService.In(request.StockProductId, request.WarehouseId, request.ShelfId, request.Quantity, request.User, "手动入库");

        return result;
    }
}