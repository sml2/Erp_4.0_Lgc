﻿namespace ERP.Storage.Api.Requests;

public class UpdateWarehouseRequestHandler : IRequestHandler<UpdateWarehouseRequest, bool>
{
    private readonly IWarehouseService _warehouseService;
    private readonly IStorageRepository _repository;
    private readonly ILogger<UpdateWarehouseRequestHandler> _logger;
    private readonly ICompanyService _companyService;

    public UpdateWarehouseRequestHandler(IWarehouseService warehouseService, IStorageRepository repository, ILogger<UpdateWarehouseRequestHandler> logger,
        ICompanyService companyService)
    {
        _warehouseService = warehouseService;
        _repository = repository;
        _logger = logger;
        _companyService = companyService;
    }

    public async Task<bool> Handle(UpdateWarehouseRequest request, CancellationToken cancellationToken)
    {
        var currentCompanyId = request.User.CompanyID;
        var needToUpdate = await _warehouseService.GetWarehouseById(request.Id, currentCompanyId);

        if (needToUpdate == null)
            throw new StorageDomainException("无效信息");
        // 检测仓库名称重复
        var duplicateName = await _warehouseService.GetWarehouseByName(request.Name, currentCompanyId);

        if (duplicateName is not null && duplicateName.ID != needToUpdate.ID)
            throw new StorageDomainException("该仓库已存在");

        // 验证共享仓库数据
        Company? distCompany = null;
        if (request.Type == Warehouse.Types.Shared)
        {
            if (!request.DistributionCompanyId.HasValue)
                throw new StorageDomainException("共享仓库必须选择下级公司");
            distCompany = await _companyService.GetInfoById(request.DistributionCompanyId.Value);
            if (distCompany == null)
                throw new StorageDomainException("共享仓库必须选择下级公司");
        }

        needToUpdate.Name = request.Name;
        needToUpdate.Address = request.Address;
        needToUpdate.KeeperName = request.KeeperName;
        needToUpdate.KeeperPhone = request.KeeperPhone;
        needToUpdate.Remark = request.Remark;
        needToUpdate.Type = request.Type;
        needToUpdate.UseShelf = request.UseShelf;
        if (distCompany is not null)
            needToUpdate.SetDistributionCompany(distCompany);
        
        var result = await _repository.UnitOfWork.SaveChangesAsync(cancellationToken);
        _logger.LogInformation("User [{User}] update warehouse [{Warehouse}] info",
            request.User,
            needToUpdate);
        
        return result > 0;
    }
}