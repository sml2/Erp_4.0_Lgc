﻿namespace ERP.Storage.Api.Requests;

public class DeleteShelfRequestHandler : IRequestHandler<DeleteShelfRequest, bool>
{
    private readonly IShelfService _shelfService;
    private readonly IWarehouseService _warehouseService;
    private readonly IStockService _stockService;
    private readonly IStorageRepository _repository;
    private readonly ILogger<DeleteShelfRequestHandler> _logger;

    public DeleteShelfRequestHandler(IShelfService shelfService, IStorageRepository repository, ILogger<DeleteShelfRequestHandler> logger, IWarehouseService warehouseService, IStockService stockService)
    {
        _shelfService = shelfService;
        _repository = repository;
        _logger = logger;
        _warehouseService = warehouseService;
        _stockService = stockService;
    }

    public async Task<bool> Handle(DeleteShelfRequest request, CancellationToken cancellationToken = default)
    {
        var shelf = await _shelfService.GetShelfById(request.Id, request.User);
        if (shelf is null)
            throw new StorageDomainException("货架不存在");

        var warehouse = await _warehouseService.Info(request.User, shelf.WarehouseId);
        if(warehouse is null)
            throw new StorageDomainException("仓库不存在");
        //不强制使用货架的仓库，必须存在一个默认货架
        if (!warehouse.UseShelf)
        {
            var count = _shelfService.GetShelvesByWarehouseId(shelf.WarehouseId).Result.Count;
            
            if(count <= 1)
                throw new StorageDomainException("不强制使用货架的仓库，必须存在一个默认货架");
        }

        var isExistStock = await _stockService.HasStockInShelf(shelf);
        if(isExistStock)
            throw new StorageDomainException("该货架存在库存，不可删除");
        
        _shelfService.Remove(shelf);
        
        var result = await _repository.UnitOfWork.SaveChangesAsync(cancellationToken);
        
        _logger.LogInformation("User [{User}] delete a shelf [{Shelf}]", 
            request.User,
            shelf);

        return result > 0;
    }
}