﻿namespace ERP.Storage.Api.Requests;

public record CreateShelfRequest(string Name, int WarehouseId, int? DistributionCompanyId,ShelvesStateEnum State) : IRequest<bool>
{
    [ValidateNever]
    public User User { get; set; } = default!;
}