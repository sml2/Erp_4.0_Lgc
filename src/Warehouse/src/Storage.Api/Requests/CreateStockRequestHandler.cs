﻿namespace ERP.Storage.Api.Requests;

public class CreateStockRequestHandler : IRequestHandler<CreateStockRequest, bool>
{
    private readonly IStockService _stockService;
    private readonly IWarehouseService _warehouseService;
    private readonly ILogger<CreateStockRequestHandler> _logger;
    private readonly IShelfService _shelfService;

    public CreateStockRequestHandler(IStockService stockService, IWarehouseService warehouseService,
        ILogger<CreateStockRequestHandler> logger,
        IShelfService shelfService)
    {
        _stockService = stockService;
        _warehouseService = warehouseService;
        _logger = logger;
        _shelfService = shelfService;
    }

    public async Task<bool> Handle(CreateStockRequest request, CancellationToken cancellationToken)
    {
        var warehouse = await _warehouseService.GetWarehouseById(request.WarehouseId, request.User.CompanyID);
        if (warehouse is null)
            throw new StorageDomainException("仓库不存在");
        Shelf? shelf = null;
        if (request.ShelfId.HasValue && request.ShelfId != 0)
        {
            shelf = await _shelfService.GetShelfById(request.ShelfId.Value, request.User);
            if (shelf is null)
                throw new StorageDomainException("货架不存在");
        }

        // if (warehouse.UseShelf && shelf is null)
        if (shelf is null)
            throw new StorageDomainException("使用该仓库必须选择货架");

        var operateUser = request.User;
        //产品是否存在
        var stockProduct = await _stockService.Exists(request.Sku);
        var isCreateStockProduct = stockProduct == null;
        var needToAdd = new StockProduct(request.ProductName, request.Url, operateUser.Id, operateUser.UserName,
            warehouse.CompanyID, warehouse.GroupID, warehouse.OEMID, request.Sku, request.PurchasingCost,
            request.Quantity);

        var result = false;
        if (isCreateStockProduct)
        {
            result = await _stockService.In(needToAdd, shelf, request.Quantity, request.User, "新增产品入库");
            _logger.LogInformation(
                "User [{User}] create a stock product [{StockProduct}] in shelf {Shelf} with quantity [{Quantity}]",
                request.User,
                needToAdd,
                shelf,
                request.Quantity);
        }
        else
        {
            result = await _stockService.In(stockProduct!.ID, request.WarehouseId, request.ShelfId, request.Quantity,
                request.User, "手动入库");
        }


        // if (shelf is not null)
        // {
        //     result = await _stockService.In(needToAdd, shelf, request.Quantity, request.User, "新增产品入库");
        //     _logger.LogInformation("User [{User}] create a stock product [{StockProduct}] in shelf {Shelf} with quantity [{Quantity}]",
        //         request.User,
        //         needToAdd,
        //         shelf,
        //         request.Quantity);
        // }
        // else
        // {
        //     result = await _stockService.In(needToAdd, warehouse, request.Quantity, request.User, "新增产品入库");
        //     _logger.LogInformation("User [{User}] create a stock product [{StockProduct}] in warehouse {Warehouse} with quantity [{Quantity}]",
        //         request.User,
        //         needToAdd,
        //         warehouse,
        //         request.Quantity);
        // }
        return result;
    }
}