﻿namespace ERP.Storage.Api.Controllers;

[Route("api/Storage/[controller]/[action]")]
public class ShelfController : BaseController
{
    private readonly IShelfService _shelfService;
    private readonly IMediator _mediator;
    private readonly UserManager<User> _userManager;

    public ShelfController(IShelfService shelfService, IMediator mediator, UserManager<User> userManager)
    {
        _shelfService = shelfService;
        _mediator = mediator;
        _userManager = userManager;
    }

    [HttpPost]
    public async Task<ResultStruct> Create(CreateShelfRequest request)
    {
        var user = await _userManager.GetUserAsync(User);
        request.User = user;
        var result = await _mediator.Send(request);
        return Success();
    }

    [HttpPost("{id:int}")]
    public async Task<ResultStruct> Delete(int id)
    {
        var user = await _userManager.GetUserAsync(User);
        var request = new DeleteShelfRequest(id) { User = user };
        var result = await _mediator.Send(request);
        return Success();
    }

    [HttpPost("{id:int}")]
    public async Task<ResultStruct> Update(int id, UpdateShelfRequest request)
    {
        var user = await _userManager.GetUserAsync(User);
        request.User = user;
        var result = await _mediator.Send(request);
        return Success();
    }

    [HttpGet]
    public async Task<ResultStruct> List(string? name, int? warehouseId)
    {
        var user = await _userManager.GetUserAsync(User);
        var list = await _shelfService.SearchShelves(user.CompanyID, name, warehouseId);

        return Success(list);
    }
    
    [HttpGet("{id:int}")]
    public async Task<ResultStruct> Info(int id)
    {
        var user = await _userManager.GetUserAsync(User);
        var list = await _shelfService.GetShelfById(id, user);

        return Success(list);
    }

}