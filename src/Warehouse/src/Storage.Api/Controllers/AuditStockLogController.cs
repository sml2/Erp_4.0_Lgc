﻿namespace ERP.Storage.Api.Controllers;

[Route("/api/storage/[controller]/[action]")]
public class AuditStockLogController : BaseController
{
    /// <summary>
    /// 库存列表
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> List([FromServices] IStockService service,[FromServices] UserManager<User> userManager,StockLog.Shelves shelve, int? warehouseId = null, string? orderNo = "", DateTime? startDate = null,
        DateTime? endDate = null)
    {
        var user = await userManager.GetUserAsync(User);
        var list = await service.AuditLogPageList(user, shelve);

        return Success(list);
    }

    public ResultStruct GetInitData()
    {
        return Success(new
        {
            Cols = new List<Column>{
                new Column("productName", "ProductName", "150", true),
                new Column("quantity", "Quantity", "50"),
                new Column("stateText", "InOrOut", "50", template: true),
                new Column("userName", "Applicant", "70"),
                new Column("companyName", "ApplyingCompany", "70"),
                new Column("remark", "Remark", "200", true, true),
                new Column("createdAt", "Create_time", "100"),
            },
            ActionBtn = new List<ActionBtn> {
                new ActionBtn("Audit", "audit", "audit")
            },
        });
    }

}

