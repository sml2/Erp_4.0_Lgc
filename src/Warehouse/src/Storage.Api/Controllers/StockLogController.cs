﻿namespace ERP.Storage.Api.Controllers;

[Route("/api/storage/[controller]/[action]")]
[ApiController]
public class StockLogController : BaseController
{
    private readonly IStockService _stockService;

    public StockLogController(IStockService stockService)
    {
        _stockService = stockService;
    }

    /// <summary>
    /// 库存列表
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> List([FromQuery] StoreLogViewModel request,
        [FromServices] UserManager<User> userManager)
    {
        var user = await userManager.GetUserAsync(User);
        var list = await _stockService.LogPageList(user, request);

        return Success(list);
    }

    public ResultStruct GetInitData()
    {
        return Success(new
        {
            Cols = new List<Column>
            {
                new Column("productName", "ProductName", "150", true),
                new Column("warehouseName", "warehouse.Warehouse", "150"),
                new Column("quantity", "Quantity", "50"),
                new Column("stateText", "InOrOut", "60", template: true),
                new Column("userName", "ExecuteUser", "70"),
                new Column("remark", "Remark", "200", true, true),
                new Column("createdAt", "Create_time", "120"),
            }
        });
    }
}

public record Column
{
    public Column(string key, string label, string width = "", bool popover = false, bool template = false,
        string type = "", string align = "")
    {
        Key = key;
        Label = label;
        Align = align;
        Width = width;
        Popover = popover;
        Template = template;
        Type = type;
    }

    public string Key { get; set; }
    public string Label { get; }
    public string Width { get; }
    public bool Popover { get; }
    public bool Template { get; }
    public string Type { get; }
    public string Align { get; }
}

public record ActionBtn
{
    public ActionBtn(string label, string key, string action, string type = "primary", string desc = "",
        string icon = "", bool link = false)
    {
        Label = label;
        Key = key;
        Action = action;
        Link = link;
        Type = type;
        Desc = desc;
        Icon = icon;
    }

    public string Label { get; }
    public string Key { get; }
    public string Action { get; }
    public string Type { get; }
    public string Desc { get; }
    public string Icon { get; }
    public bool Link { get; }
}

public record SearchItem
{
    public SearchItem(string key, string label, string type = "")
    {
        Key = key;
        Label = label;
        Type = type;
    }

    public string Key { get; }
    public string Label { get; }
    public virtual string Type { get; set; }
}

public record Select : SearchItem
{
    public Select(string key, string label, object data, bool template = false) : base(key, label)
    {
        Data = data;
        Template = template;
    }

    public object Data { get; }
    public bool Template { get; }
    public override string Type => "select";
}

public record HeaderBtn
{
    public HeaderBtn(string key, string label, string action, string icon, string type = "primary")
    {
        Key = key;
        Label = label;
        Action = action;
        Icon = icon;
        Type = type;
    }

    public string Key { get; }
    public string Label { get; }
    public string Action { get; }
    public string Icon { get; }
    public string Type { get; }
}