﻿namespace ERP.Controllers.Storage;

[Route("/api/storage/[controller]/[action]")]
public class StockController : BaseController
{
    private readonly IStockService _stockService;
    private readonly IWarehouseService _warehouseService;
    private readonly UserManager<User> _userManager;
    private readonly IMediator _mediator;

    public StockController(IStockService stockService, IWarehouseService warehouseService,
        UserManager<User> userManager, IMediator mediator)
    {
        _stockService = stockService;
        _warehouseService = warehouseService;
        _userManager = userManager;
        _mediator = mediator;
    }

    /// <summary>
    /// 库存列表
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> List(int? stockProductId, string? name, int? warehouseId, int? shelfId, string? spu)
    {
        var user = await _userManager.GetUserAsync(User);

        var result = await _stockService.StockList(user, stockProductId, name, warehouseId, shelfId, spu);
        return Success(result);
    }

    /// <summary>
    /// 商品入库
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [Permission(Enums.Rule.Config.Storage.STOCK_IN)]
    public async Task<ResultStruct> Create(CreateStockRequest request)
    {
        var user = await _userManager.GetUserAsync(User);
        request.User = user;

        var result = await _mediator.Send(request);
        return result ? Success() : Error();
    }

    [Permission(Enums.Rule.Config.Storage.STOCK_IN)]
    public async Task<ResultStruct> In(StockInRequest request)
    {
        var user = await _userManager.GetUserAsync(User);
        request.User = user;
        var result = await _mediator.Send(request);

        return result ? Success() : Error();
    }

    public record StockOutRequest(int StockProductId, int WarehouseId, int Quantity, string Remark);

    /// <summary>
    /// 商品出库
    /// </summary>
    /// <returns></returns>
    [Permission(Enums.Rule.Config.Storage.STOCK_OUT)]
    public async Task<ResultStruct> Out(StockOutRequest request)
    {
        var user = await _userManager.GetUserAsync(User);
        var result = await _stockService.StockOut(request.StockProductId, request.WarehouseId, request.Quantity, user,
            "手动出库");

        return result ? Success() : Error();
    }


    /// <summary>
    /// 商品转仓/调拨
    /// </summary>
    /// <returns></returns>
    [Permission(Enums.Rule.Config.Storage.STOCK_OUT)]
    public async Task<ResultStruct> Transfer(StockTransferRequest request)
    {
        var user = await _userManager.GetUserAsync(User);
        request.User = user;
        var result = await _mediator.Send(request);

        return result ? Success() : Error();
    }


    /// <summary>
    /// 库存详情
    /// </summary>
    /// <returns></returns>
    [HttpGet("{id:int}")]
    public async Task<ResultStruct> Info(int id)
    {
        var user = await _userManager.GetUserAsync(User);
        var res = await _stockService.Info(user, id);
        return res is not null ? Success(res) : Error("无法找到信息");
    }


    /// <summary>
    /// 获取用户下仓库数据，不分页
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> StockOutWarehouseList(int id)
    {
        var user = await _userManager.GetUserAsync(User);
        var data = await _stockService.StockOutWarehouseList(id, user);

        return Success(data);
    }

    [HttpPost("{id:int}")]
    public async Task<ResultStruct> Delete(int id)
    {
        var user = await _userManager.GetUserAsync(User);
        var request = new DeleteStockProductRequest(id) { User = user };
        var result = await _mediator.Send(request);
        return Success();
    }

    /// <summary>
    /// 初始数据
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetInitData()
    {
        var user = await _userManager.GetUserAsync(User);
        var warehouse = await _warehouseService.AvailableWarehouses(user);

        return Success(
            new
            {
                warehouse,
                Cols = new List<Column>
                {
                    new Column("url", "Image", type: "image",width:"200px"),
                    new Column("productName", "ProductName", popover: true,template:true),
                    new Column("price", "单价", popover: true),
                    new Column("quantity", "Quantity"),
                    new Column("warehouse", "Warehouse", template:true),
                },
                SearchList = new List<SearchItem>
                {
                    new SearchItem("name", "ProductName"),
                    new SearchItem("sku", "商品sku"),
                    new Select("warehouseId", "warehouse.Warehouse", warehouse.Select(w => new
                    {
                        Label = w.Name,
                        Value = w.ID,
                        w.Type,
                    }), true),
                    new Select("shelfId", "货架", warehouse.Select(w => new
                    {
                        Label = w.Name,
                        Value = w.ID,
                        w.Type,
                    }), true),
                },
                ActionBtn = new List<ActionBtn>
                {
                    new ActionBtn("Info", "detail", "show", "info", icon: "InfoFilled"),
                    new ActionBtn("StockIn", "in", "in", "success"),
                    new ActionBtn("StockOut", "out", "out", "warning"),
                    new ActionBtn("Delete", "delete", "delete", "danger"),
                },
                HeaderBtn = new List<HeaderBtn>
                {
                    new HeaderBtn("in", "StockIn", "add", "CirclePlus", "success"),
                    // new HeaderBtn("PurchaseConfirm", "PurchaseConfirm", "showConfirm", "", "warning"),
                    new HeaderBtn("DelMulti", "delete", "batchDelete", "", "danger"),
                    new HeaderBtn("printSkuCode", "打印sku条码", "printSkuCode", "", "danger"),
                    new HeaderBtn("scanIn", "扫码入库", "scanIn", "", "danger"),
                }
            }
        );
    }


    /// <summary>
    /// 订单产品切换仓库
    /// </summary>
    /// <returns></returns>
    public ResultStruct GoodsChangeWarehouse()
    {
        //        $input = request()->post();
        //        $id = $input['id'];
        //        $type = $input['type'];
        //        $editGoods = $input['goods'];
        //        $orderData = self::getDynamicInfo($type, $id);
        //        $goods = json_decode($orderData->goods, true);

        //        if ($this->getSessionObj()->getOrderUnitConfigGoods() === true) {
        //            $unit_config = $orderData['coin'] ?? $this->getSessionObj()->getUnitConfig();
        //        } else {
        //            $unit_config = $this->getSessionObj()->getUnitConfig();
        //        }
        //        DB::beginTransaction();
        //        try {
        //            foreach ($goods as &$good) {

        //                $good['price'] = FromConversion($good['price'], $unit_config);
        //                $good['total'] = FromConversion($good['total'], $unit_config);
        //                if (isset($good['amazon_shipping_money'])) {
        //                    $good['amazon_shipping_money'] = FromConversion(
        //                        $good['amazon_shipping_money'],
        //                        $unit_config
        //                    );
        //                }
        //                if ($good["good_id"] == $editGoods['good_id']) {
        //                    $good["stock_product_id"] = $editGoods["stock_product_id"] ?? 0;
        //                    $good["product_id"] = $editGoods["product_id"] ?? 0;
        //                    $good["variant_id"] = $editGoods["variant_id"] ?? '';
        //                    $good["warehouse_id"] = $editGoods["warehouse_id"] ?? 0;
        //                    $good["warehouse_name"] = $editGoods["warehouse_name"] ?? '';
        //                    $good["tags"] = $editGoods["tags"] ?? [];
        //                }
        //            }
        //            self::setDynamicGoods($type, $id, $goods);
        //            // FIXME: 批量发货为单独表，同步更新可能导致问题
        ////                ShipOrder::query()->where('order_id', $order_id)
        ////                    ->userId($this->sessionData->getUserId())
        ////                    ->companyId($this->sessionData->getCompanyId())
        ////                    ->oemId($this->sessionData->getOemId())
        ////                    ->update(['goods' => $goods]);
        //            DB::commit();
        //            return success("修改成功");
        //        } catch (\Exception $e) {
        //            DB::rollBack();
        //            return error("修改失败", $e->getMessage());
        //        }
        return Success();
    }

    private static object getDynamicInfo( /*$type, $id*/)
    {
        //        if ($type === 'order') {
        //            return (new AmazonOrderService())->getOrderInfoWhereFirst(["goods", "id", 'coin','rate'], $id);
        //        } elseif ($type === 'waybill') {
        //            $row = WayBill::query()->where('id', $id)->first(['product_info', 'id']);
        //            $row->goods = $row->product_info;
        //            return $row;
        //        } else {
        //            return ShipOrder::query()->where('id', $id)->first(['goods', 'id']);
        //        }
        return 1;
    }

    private static object setDynamicGoods( /*$type, $id, $goods*/)
    {
        //        if ($type === 'order') {
        //            AmazonOrder::query()->where('id', $id)->update(["goods" => json_encode($goods)]);
        //        } elseif ($type === 'waybill') {
        //            WayBill::query()->where('id', $id)->update(["product_info" => json_encode($goods)]);
        //        } else {
        //            ShipOrder::query()->where('id', $id)->update(["goods" => json_encode($goods)]);
        //        }
        return 1;
    }

    //    /**
    //     * 根据sku获取产品信息
    //     * @return JsonResponse
    //     */
    public ResultStruct productDetail( /*StockService $service*/)
    {
        //        $input = request()->input();
        //        $id = $input['id'] ?? 0;
        //        $type = $input['type'] ?? '';
        //        $variantId = $input['variant_id'] ?? '';
        //        if (empty($type) || empty($id) || !in_array($type, ['sku', 'productId'])) {
        //            return warning('请选择参数');
        //        }

        //        $func = 'productDetailBy' . ucfirst($type);
        //        $product = self::$func($id);
        //        if (!$product) {
        //            return warning('');
        //        }
        //        if (empty($variantId)) {
        //            $variantId = $product->variants[0] ? $product->variants[0]['hash_code'] : 0;
        //        }
        //        $warehouseList = $service->stockOutWarehouseListByProduct($product->id, $variantId, true);
        //        return success('', [
        //            'product'    => [
        //                'id'        => $product->id,
        //                'variants'  => $product->variants,
        //                'attribute' => $product->attribute,
        //                'images'    => $product->images,
        //                'source'    => $product->source,
        //            ],
        //            'variant_id' => $variantId,
        //            'warehouse'  => $warehouseList
        //        ]);
        return Success();
    }

    private static void productDetailBySku( /*$sku*/)
    {
        //        $productId = decodeSku($sku);
        //        if (!$productId) {
        //            return null;
        //        }
        //        return self::productDetailByProductId($productId);
        productDetailByProductId();
    }

    private static object productDetailByProductId( /*$productId*/)
    {
        //        return \App\Models\Product\Product::query()->where('id', $productId)
        //            ->first(['id', 'variants', 'attribute', 'images', 'source']);
        return 1;
    }
}