﻿namespace ERP.Storage.Api.Controllers;

[Route("/api/storage/[controller]/[action]")]
public class WarehouseController : BaseController
{
    private readonly IWarehouseService _warehouseService;
    private readonly ICompanyService _companyService;
    private readonly UserManager<User> _userManager;
    private readonly IMediator _mediator;

    public WarehouseController(IWarehouseService warehouseService, ICompanyService companyService,
        UserManager<User> userManager, IMediator mediator)
    {
        _warehouseService = warehouseService;
        _companyService = companyService;
        _userManager = userManager;
        _mediator = mediator;
    }

    /// <summary>
    /// 获取权限
    /// </summary>
    /// <returns></returns>
    public ResultStruct GetInitData()
    {
        return Success(new
        {
            Cols = new List<Column>
            {
                new Column("name", "WarehouseName", "100"),
                new Column("type", "Type", "70", template: true, align: "center"),
                new Column("address", "WarehouseAddress", "200", popover: true),
                new Column("keeperName", "ContactUser", "100"),
                new Column("keeperPhone", "ContactUserPhone", "100"),
            },
            ActionBtn = new List<ActionBtn>
            {
                new ActionBtn("Edit", "edit", "edit", link: false),
                new ActionBtn("Copy", "copy", "copyFrom", "success", "warehouse.Msg2", link: false),
                new ActionBtn("Delete", "delete", "delete", "danger", link: false),
            },
            SearchList = new List<SearchItem>
            {
                new SearchItem("name", "WarehouseName")
            },
            headerBtn = new List<HeaderBtn>
            {
                new HeaderBtn("add", "Add", "add", "CirclePlus"),
                new HeaderBtn("delete", "DelMulti", "batchDelete", "Delete", "danger"),
            },
        });
    }


    /// <summary>
    /// 添加仓库
    /// </summary>
    /// <returns></returns>
    [Permission(StorageManager.WarehouseAdd)]
    public async Task<ResultStruct> Create(CreateWarehouseRequest request)
    {
        var user = await _userManager.GetUserAsync(User);
        request.User = user;
        var res = await _mediator.Send(request);

        return res ? Success() : Error();
    }


    /// <summary>
    /// 修改仓库 
    /// </summary>
    /// <returns></returns>
    [Permission(StorageManager.WarehouseEdit)]
    public async Task<ResultStruct> Edit(UpdateWarehouseRequest request)
    {
        var user = await _userManager.GetUserAsync(User);
        request.User = user;

        var res = await _mediator.Send(request);
        return Success();
    }


    /// <summary>
    /// 仓库详情
    /// </summary>
    /// <returns></returns>
    [Route("{id:int}"), Permission(StorageManager.WarehouseDetail), Obsolete("使用graphql")]
    public async Task<ResultStruct> Info(int id)
    {
        var user = await _userManager.GetUserAsync(User);
        var info = await _warehouseService.Info(user, id);
        if (info == null)
        {
            return Error(message: "仓库不存在");
        }

        var dist = await _companyService.GetValidDistributionList(user.CompanyID);
        var typeList = Enum.GetValues(typeof(Warehouse.Types)).Cast<Warehouse.Types>()
            .Select(t => new { value = t, label = Ryu.Extensions.EnumExtension.GetDescription(t) });

        return Success(new
        {
            info,
            dist,
            typeList,
        });
    }

    /// <summary>
    /// 获取下级信息
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetDist()
    {
        var user = await _userManager.GetUserAsync(User);
        var dist = await _companyService.GetValidDistributionList(user.CompanyID);
        var typeList = Enum.GetValues<Warehouse.Types>()
            .Select(t => new { value = t, label = Ryu.Extensions.EnumExtension.GetDescription(t) });

        return Success(new
        {
            dist,
            typeList,
        });
    }

    public record ListQuery(string Name = "", Warehouse.Types? Type = null, int Limit = 10, int Page = 1);

    /// <summary>
    /// 仓库列表
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> List([FromQuery] ListQuery query)
    {
        var user = await _userManager.GetUserAsync(User);
        var res = await _warehouseService.List(user, query.Name, query.Type, query.Limit, query.Page);
        return Success(res);
    }

    public async Task<ResultStruct> All()
    {
        var user = await _userManager.GetUserAsync(User);
        var res = await _warehouseService.AvailableWarehouses(user);
        return Success(res);
    }


    /// <summary>
    /// 删除仓库
    /// </summary>
    /// <returns></returns>
    [HttpPost("{id:int}")]
    public async Task<ResultStruct> Delete(int id)
    {
        var user = await _userManager.GetUserAsync(User);
        var request = new DeleteWarehouseRequest(id) { User = user };

        var res = await _mediator.Send(request);
        return Success();
    }
}