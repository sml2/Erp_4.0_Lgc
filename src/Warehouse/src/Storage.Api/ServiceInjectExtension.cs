﻿using ERP.Data;

namespace Microsoft.Extensions.DependencyInjection;

public static class ServiceInjectExtension
{
    /// <summary>
    /// 添加库存管理模块依赖注入
    /// </summary>
    /// <param name="collection"></param>
    public static void AddStorage<TDbContext>(this IServiceCollection collection) where TDbContext : Base
    {
        collection.AddScoped<IStockService, StockService>(CreateService<StockService>);
        collection.AddScoped<IWarehouseService, WarehouseService>(CreateService<WarehouseService>);
        collection.AddScoped<IShelfService, ShelfService>(CreateService<ShelfService>);

        collection.Configure<MvcOptions>(options =>
        {
            options.Filters.Add(typeof(HttpGlobalExceptionFilter));
        });

        collection.AddMediatR(typeof(CreateShelfRequest).Assembly);
        collection.AddScoped<IStorageRepository, StorageRepository>(CreateService<StorageRepository>);

        T CreateService<T>(IServiceProvider provider)
        {
            var dbContext = provider.GetRequiredService<TDbContext>();
            return ActivatorUtilities.CreateInstance<T>(provider, dbContext);
        }
    }
}