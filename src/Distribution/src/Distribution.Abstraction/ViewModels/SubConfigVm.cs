﻿namespace ERP.Distribution.Abstraction.ViewModels;

public record SubConfigVm(int Id, bool IsShowRate, bool IsNegative, int PurchaseRateType, decimal PurchaseRate, decimal PurchaseRateNum
    , string PurchaseRateUnit, int WaybillRateType, decimal WaybillRate, decimal WaybillRateNum, string WaybillRateUnit
    , bool PublicWalletCanFreeze, decimal FreezeAmountPerWaybill);
    