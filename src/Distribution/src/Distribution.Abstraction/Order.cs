using ERP.Enums.Orders;
using ERP.Models.DB.Identity;

namespace ERP.ViewModels.Distribution;

public class OrderWaitListDto
{
    public int Id { get; }
    public string OrderNo { get; }
    [JsonProperty("receiver_nation")]public string ReceiverNation { get; }
    [JsonProperty("receiver_nation_short")]public string ReceiverNationShort { get; }
    public string Url { get; }
    [JsonProperty("product_num")]public int ProductNum { get; }
    [JsonProperty("pay_money")]public int PayMoney { get; set; }
    public string Coin { get; }
    [JsonProperty("order_state")]public int State { get; }
    public string Progress { get; }=string.Empty; 
    [JsonProperty("company_id")]public int CompanyId { get; }
    [JsonProperty("create_time")]public DateTime CreateTime { get; }
    [JsonProperty("latest_ship_time")]public DateTime? LatestShipTime { get; }
    [JsonProperty("is_batch_ship")]public bool IsBatchShip { get; }
    public decimal Rate { get; }

    public OrderWaitListDto(int id, string? orderNo, string? receiverNation, string? receiverNationShort, string? url, int productNum, string? coin, 
        int state, int companyId, DateTime createTime, DateTime? latestShipTime, bool isBatchShip, decimal rate)
    {
        Id = id;
        OrderNo = orderNo ?? string.Empty;
        ReceiverNation = receiverNation ?? string.Empty;
        ReceiverNationShort = receiverNationShort ?? string.Empty;
        Url = url ?? string.Empty;
        ProductNum = productNum;
        Coin = coin ?? string.Empty;
        State = state;
        CompanyId = companyId;
        CreateTime = createTime;
        LatestShipTime = latestShipTime;
        IsBatchShip = isBatchShip;
        Rate = rate;
    }
}

public class OrderListDto
{
    private readonly string? _customMark;
    public string? Username { get; init; }
    public int Id { get; }
    public string OrderNo { get; }
    public int StoreId { get; }
    public string ReceiverNation { get; }
    public string Url { get; }
    public int ProductNum { get; } 
    public string OrderTotal { get; set; }
    public string Coin { get; }
  
    public string Fee { get; set; }   
    public string Loss { get; set; }   
    public string Refund { get; set; }   
    public string PurchaseFee { get; set; }  
    public string ShippingMoney { get; set; }   
    public string Profit { get; set; }
    public int State { get; }
    public int CompanyId { get; }
    public DateTime CreateTime { get; }
    public DateTime? LatestShipTime { get; }
    public decimal Rate { get; }
    public int PurchaseAudit { get; }
    public ShippingTypes? ShippingType { get; }
    public string ShippingTypeName => ShippingType?.GetName() ?? string.Empty;
    public string Store { get; }
    public PlatformInfo Platform { get; }
    public int PurchaseCompanyId { get; }
    public int WaybillCompanyId { get; }

    public int OrderCompanyId { get; }
    public int? Delivery { get; }
    public string NationShort { get; }
    private readonly object[] _purchaseTrackingNumber;

    public IEnumerable<object> PurchaseTrackingNumber => _purchaseTrackingNumber;
    
   // private readonly object[]? _waybillOrder; 
    
    public IEnumerable<object>? WaybillOrder { get; set; } // => _waybillOrder;

    public OrderListDto(OrderUnitConfig unitInfo, string unit, int id, string? orderNo, int storeId, string? receiverNation, string? url, int productNum,
        MoneyRecordFinancialAffairs orderTotal, string? coin, MoneyRecordFinancialAffairs fee,
        MoneyRecordFinancialAffairs loss, MoneyRecordFinancialAffairs refund, MoneyRecordFinancialAffairs purchaseFee,
        MoneyRecordFinancialAffairs shippingMoney, MoneyRecordFinancialAffairs profit, int state, int companyId,
        DateTime createTime, DateTime? latestShipTime, decimal rate, int purchaseAudit, ShippingTypes? shippingType,
        string store, PlatformInfo platform, int purchaseCompanyId, int waybillCompanyId, int orderCompanyId,
        object[] purchaseTrackingNumber,  string? customMark, string? nationShort, int? delivery)  /*object[] waybillOrder,*/
    {
        var orderUnit = orderTotal.Raw.CurrencyCode;
        _customMark = customMark;
        Id = id;
        OrderNo = orderNo ?? string.Empty;
        StoreId = storeId;
        ReceiverNation = receiverNation ?? string.Empty;
        Url = url ?? string.Empty;
        ProductNum = productNum;
        OrderTotal = Helpers.TransMoney(orderTotal, unitInfo.Total, orderUnit, unit,true);
        Coin = coin ?? string.Empty;
        Fee = Helpers.TransMoney(fee, unitInfo.Fee, orderUnit, unit,true);
        Loss = Helpers.TransMoney(loss, unitInfo.LOSS, orderUnit, unit,true);//订单损耗CNY(基准货币)
        Refund = Helpers.TransMoney(refund, unitInfo.Refund, orderUnit, unit,true); //订单退款金额(基准货币)
        PurchaseFee = Helpers.TransMoney(purchaseFee, unitInfo.Purchase, orderUnit, unit, true);//采购花费(基准货币)
        ShippingMoney = Helpers.TransMoney(shippingMoney, unitInfo.Deliver, orderUnit, unit,true);//运费           

        Profit = Helpers.TransMoneyM(orderTotal.Money - fee.Money - refund.Money - purchaseFee.Money - shippingMoney.Money - loss.Money, orderTotal.Raw.Rate,unitInfo.Profit, orderUnit, unit, true);// Helpers.TransMoney(profit, unitInfo.Profit, unit);//订单获利(基准货币)       
        State = state;
        CompanyId = companyId;
        CreateTime = createTime;
        LatestShipTime = latestShipTime;
        Rate = rate;
        PurchaseAudit = purchaseAudit;
        ShippingType = shippingType;
        Store = store;
        Platform = platform;
        PurchaseCompanyId = purchaseCompanyId;
        WaybillCompanyId = waybillCompanyId;
        OrderCompanyId = orderCompanyId;
        Delivery = delivery;
        NationShort = nationShort ?? string.Empty;
        _purchaseTrackingNumber = purchaseTrackingNumber;      
    }
    public class CustomMarkJson
    {
        public int Id { get; set; }
        public string Color { get; set; } = string.Empty;
        public string Remark { get; set; } = string.Empty;
    }
    public CustomMarkJson? CustomMark => JsonConvert.DeserializeObject<CustomMarkJson>(_customMark ?? "{}");
}

public class OrderActionListDto
{
    public string Value { get; }
    public string Truename { get; }
    public DateTime CreatedAt { get; }

    public OrderActionListDto(string? value, string? truename, DateTime createdAt)
    {
        Value = value ?? string.Empty;
        Truename = truename ?? string.Empty;
        CreatedAt = createdAt;
    }
}

public class OrderRemarkListDto
{
    public string Value { get; }
    public string Truename { get; }
    public DateTime CreatedAt { get; }

    public OrderRemarkListDto(string? value, string? truename, DateTime createdAt)
    {
        Value = value ?? string.Empty;
        Truename = truename ?? string.Empty;
        CreatedAt = createdAt;
    }
}