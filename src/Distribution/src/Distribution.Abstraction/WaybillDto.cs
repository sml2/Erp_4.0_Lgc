﻿using ERP.Data;
using ERP.Enums;
using System.ComponentModel.DataAnnotations;

namespace ERP.Distribution.Abstraction;

public class ParameterDto
{
    public int Id { get; set; }
    public int LogisticsId { get; set; }
    public int Allot { get; set; } = 1;
    public int Charge { get; set; } = 1;
    [Required]
    public PlatformInfo Platform { get; set; } = PlatformInfo.Empty;
    [Required]
    public string Name { get; set; } = string.Empty;
    [Required]
    public string CustomizeName { get; set; }= string.Empty;


}

public class ParameterInfoDto
{
    public int Id { get; set; }
    public int LogisticsId { get; set; }
    public int ParentId { get; set; }
    public int Charge { get; set; }
}