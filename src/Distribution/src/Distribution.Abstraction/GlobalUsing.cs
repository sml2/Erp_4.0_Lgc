﻿global using System.ComponentModel.DataAnnotations;
global using Newtonsoft.Json;
global using Ryu.Data;
global using ERP.Data;
global using ERP.Extensions;
global using ERP.Attributes;
global using ERP.Models.Finance;