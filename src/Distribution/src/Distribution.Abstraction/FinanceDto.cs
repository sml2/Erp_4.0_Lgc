﻿using Ryu.Extensions;
using Sy.Net;

namespace ERP.Distribution.Abstraction;

public class MonthFinanceDto
{
    [JsonProperty("companyId")]public virtual int AboutId { get; set; }
    [JsonMoneyForDefaultUnit]public Money OrderTotal { get; }
    [JsonMoneyForDefaultUnit]public Money OrderTotalFee { get; }

    [JsonMoneyForDefaultUnit]public Money OrderTotalRefund { get; }

    [JsonMoneyForDefaultUnit]public Money PurchaseTotal { get; }

    [JsonMoneyForDefaultUnit]public Money PurchaseTotalRefund { get; }

    [JsonMoneyForDefaultUnit]public Money WaybillTotal { get; }

    [JsonMoneyForDefaultUnit]public Money WaybillTotalRefund { get; }

    [JsonMoneyForDefaultUnit]public Money RechargeTotal { get; }

    [JsonMoneyForDefaultUnit]public Money ExpenseTotal { get; }

    [JsonMoneyForDefaultUnit]
    public Money Profit => OrderTotal - OrderTotalFee - OrderTotalRefund - PurchaseTotal - WaybillTotal;


    public MonthFinanceDto(int aboutId, Money orderTotal, Money orderTotalFee, Money orderTotalRefund, Money purchaseTotal, Money purchaseTotalRefund, Money waybillTotal, Money waybillTotalRefund, Money rechargeTotal, Money expenseTotal)
    {
        AboutId = aboutId;
        OrderTotal = orderTotal;
        OrderTotalFee = orderTotalFee;
        OrderTotalRefund = orderTotalRefund;
        PurchaseTotal = purchaseTotal;
        PurchaseTotalRefund = purchaseTotalRefund;
        WaybillTotal = waybillTotal;
        WaybillTotalRefund = waybillTotalRefund;
        RechargeTotal = rechargeTotal;
        ExpenseTotal = expenseTotal;
    }
}

public class UserFinanceDto : MonthFinanceDto
{
    [JsonMoneyForDefaultUnit]public Money OrderTotalLoss { get; }
    [JsonMoneyForDefaultUnit]public Money SelfRechargeTotal { get; }
    [JsonMoneyForDefaultUnit]public Money SelfExpenseTotal { get; }
    [JsonProperty("userId")]public override int AboutId { get; set; }
    
    public UserFinanceDto(int userId, Money orderTotal, Money orderTotalFee, Money orderTotalRefund, Money orderTotalLoss, Money purchaseTotal,
        Money purchaseTotalRefund, Money waybillTotal, Money waybillTotalRefund, Money rechargeTotal, Money expenseTotal, Money selfRechargeTotal, Money selfExpenseTotal) 
        : base(userId, orderTotal, orderTotalFee, orderTotalRefund, purchaseTotal, purchaseTotalRefund, waybillTotal, waybillTotalRefund, rechargeTotal, expenseTotal)
    {
        OrderTotalLoss = orderTotalLoss;
        SelfRechargeTotal = selfRechargeTotal;
        SelfExpenseTotal = selfExpenseTotal;
    }
}

public class StoreFinanceDto : MonthFinanceDto
{
    [JsonMoneyForDefaultUnit]public Money OrderTotalLoss { get; }
    [JsonProperty("userId")]public override int AboutId { get; set; }
    
    public StoreFinanceDto(int userId, Money orderTotal, Money orderTotalFee, Money orderTotalRefund, Money orderTotalLoss, Money purchaseTotal,
        Money purchaseTotalRefund, Money waybillTotal, Money waybillTotalRefund) 
        : base(userId, orderTotal, orderTotalFee, orderTotalRefund, purchaseTotal, purchaseTotalRefund, waybillTotal, waybillTotalRefund, Money.Zero, Money.Zero)
    {
        OrderTotalLoss = orderTotalLoss;
    }
}

public class GroupFinanceDto : MonthFinanceDto
{
    [JsonMoneyForDefaultUnit]public Money OrderTotalLoss { get; }
    [JsonMoneyForDefaultUnit]public Money SelfRechargeTotal { get; }
    [JsonMoneyForDefaultUnit]public Money SelfExpenseTotal { get; }
    [JsonProperty("userId")]public override int AboutId { get; set; }
    
    public GroupFinanceDto(int userId, Money orderTotal, Money orderTotalFee, Money orderTotalRefund, Money orderTotalLoss, Money purchaseTotal,
        Money purchaseTotalRefund, Money waybillTotal, Money waybillTotalRefund, Money rechargeTotal, Money expenseTotal, Money selfRechargeTotal, Money selfExpenseTotal) 
        : base(userId, orderTotal, orderTotalFee, orderTotalRefund, purchaseTotal, purchaseTotalRefund, waybillTotal, waybillTotalRefund, rechargeTotal, expenseTotal)
    {
        OrderTotalLoss = orderTotalLoss;
        SelfRechargeTotal = selfRechargeTotal;
        SelfExpenseTotal = selfExpenseTotal;
    }
}

public class BillLogDto
{
    public int Id { get; }
    [JsonMoneyForDefaultUnit]public Money Money { get; }
    public string? Balance { get; }
    public string? Unit { get; }
    public BillLog.Types Type { get; }
    public string? Datetime { get; }
    public string? Reason { get; }
    public string? Remark { get; }
    public BillLog.Audits Audit { get; }
    public int CompanyId { get; }
    public BillLog.Modules Module { get; }
    public DateTime CreatedAt { get; }

    public BillLogDto(int id, Money money, BillLog.Types type, DateTime? datetime, string? reason,string? remark,  BillLog.Audits audit, int companyId, BillLog.Modules module, DateTime createdAt,Money? balance = null,string? unit = null)
    {
        Id = id;
        Money = money;
        Type = type;
        Datetime = datetime?.ToString("yyyy-MM-dd");
        Reason = reason;
        Remark = remark;
        Audit = audit;
        CompanyId = companyId;
        Module = module;
        CreatedAt = createdAt;
        if (balance != null && unit !=null)
        {
            Balance = balance?.To(unit).ToFixed();
        }
    }
}