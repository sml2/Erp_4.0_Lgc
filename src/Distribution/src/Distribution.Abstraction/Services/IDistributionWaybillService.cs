﻿using ERP.Enums.Logistics;
using ERP.Extensions;
using ERP.Models.DB.Logistics;
namespace ERP.Distribution.Abstraction.Services;

public interface IDistributionWaybillService
{
    /// <summary>
    /// 上级获取上报运单分页列表
    /// </summary>
    /// <param name="currentCompanyId"></param>
    /// <param name="orderNo"></param>
    /// <param name="express"></param>
    /// <param name="waybillOrder"></param>
    /// <param name="tracking"></param>
    /// <param name="waybillState"></param>
    /// <param name="companyId"></param>
    /// <param name="userId"></param>
    /// <param name="warehouseId"></param>
    /// <returns></returns>
    Task<DbSetExtension.PaginateStruct<Waybill>> GetListPage(int currentCompanyId,string? orderNo, string? express, string? waybillOrder, string? tracking,States? waybillState, 
        int? companyId, int? userId = null, int? warehouseId = null);

    /// <summary>
    /// 下级获取上报运单分页列表
    /// </summary>
    /// <param name="currentCompanyId"></param>
    /// <param name="reportCompanyId"></param>
    /// <param name="orderNo"></param>
    /// <param name="express"></param>
    /// <param name="waybillOrder"></param>
    /// <param name="tracking"></param>
    /// <param name="waybillState"></param>
    /// <param name="storeId"></param>
    /// <param name="storeIds"></param>
    /// <param name="warehouseId"></param>
    /// <returns></returns>
    Task<DbSetExtension.PaginateStruct<Waybill>> GetJuniorListPage(int currentCompanyId, int reportCompanyId,string? orderNo, string? express
        , string? waybillOrder, string? tracking, States? waybillState
        , int? storeId, int[] storeIds, int? warehouseId = null);

    /// <summary>
    /// 获取指定运单详细信息
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    Task<Waybill?> GetInfoById(int id);
    Task<Parameter?> GetLogisticsInfo(int id);

    /// <summary>
    /// 运单手动确认
    /// </summary>
    /// <param name="id"></param>
    /// <param name="billLogId"></param>
    /// <returns></returns>
    Task ConfirmUpdate(int id, int billLogId);
    
    /// <summary>
    /// 获取订单详情展示运单信息
    /// </summary>
    /// <param name="orderId"></param>
    /// <returns></returns>
    Task<List<Waybill>> GetWaybillByOrderId(int orderId);

    /// <summary>
    /// 获取当前公司物流信息
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="name"></param>
    /// <returns></returns>
    Task<DbSetExtension.PaginateStruct<ParameterDto>> GetAllLogistics(int companyId, string? name = "");
    
    /// <summary>
    /// 获取指定公司被分配物流信息
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="pids"></param>
    /// <returns></returns>
    Task<List<ParameterInfoDto>> GetAllotLogistics(int companyId, int[]? pids = null);

    /// <summary>
    /// 根据ids获取指定有效物流信息
    /// </summary>
    /// <param name="currentCompanyId"></param>
    /// <param name="ids"></param>
    /// <returns></returns>
    Task<List<Parameter>> GetLogistics(int currentCompanyId, IEnumerable<int> ids);
    
    /// <summary>
    /// 添加上级分配物流信息
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    Task<bool> AddLogistics(Parameter model);
    
    /// <summary>
    /// 上级修改分配物流扣款方式
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="pid"></param>
    /// <param name="charge"></param>
    /// <returns></returns>
    Task<bool> ChangeChargeType(int companyId, int pid, Charges charge);

    /// <summary>
    /// 删除上级分配指定物流
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="pid"></param>
    /// <returns></returns>
    Task<int> DelLogistics(int companyId, int pid);

    /// <summary>
    /// 更新运单 联动采购追踪号
    /// </summary>
    /// <param name="id"></param>
    /// <param name="purchaseTrackingNumber"></param>
    /// <returns></returns>
    Task UpdatePurchaseTrackingNumber(int id, string purchaseTrackingNumber);
    
    /// <summary>
    /// 获取运单信息
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="waybillOrder"></param>
    /// <returns></returns>
    Task<List<Waybill>> GetWaybill(int companyId, string waybillOrder);
}