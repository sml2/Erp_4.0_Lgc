﻿using ERP.Distribution.Abstraction.ViewModels;
using ERP.Extensions;
using ERP.Models.DB.Users;
using Ryu.Data;

namespace ERP.Distribution.Abstraction.Services;

public interface IDistributionCompanyService
{
    /// <summary>
    /// 获取分销公司分页列表
    /// </summary>
    /// <param name="companyId"></param>
    /// <returns></returns>
    Task<DbSetExtension.PaginateStruct<DistributionCompanyDto>> GetDistributionListPage(int companyId);

    /// <summary>
    /// 获取分销公司费率信息
    /// </summary>
    /// <param name="companyId"></param>
    /// <returns></returns>
    Task<DistributionCompanyDto?> GetDistributionConfig(int companyId);

    /// <summary>
    /// 更新分销公司费率信息
    /// </summary>
    /// <param name="id"></param>
    /// <param name="vm"></param>
    /// <returns></returns>
    Task<bool> UpdateDistributionConfig(int id, SubConfigVm vm);

    /// <summary>
    /// 获取有效下级分销公司数据
    /// </summary>
    /// <param name="companyId"></param>
    /// <returns></returns>
    Task<Dictionary<int, CompanyOnlyNameDto>> GetValidDistributionList(int companyId);

    /// <summary>
    /// 获取分销公司分页钱包列表查询对公
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="currentCompanyId">当前公司id</param>
    /// <returns></returns>
    Task<DbSetExtension.PaginateStruct<CompanyWalletDto>> GetWalletListPage(int currentCompanyId, int? companyId = null);
    
    /// <summary>
    /// 查找以指定公司为上报id的公司数量
    /// </summary>
    /// <returns></returns>
    Task<int> GetReportIdCompanyCount(int companyId);
    
    /// <summary>
    /// 指定钱包充值
    /// </summary>
    Task RechargeWallet(int companyId, Money money);
    
    /// <summary>
    /// 指定钱包支出
    /// </summary>
    Task ExpenseWallet(int companyId, Money money);

    /// <summary>
    /// 获取指定公司信息
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    Task<Company?> GetInfoById(int id);
}