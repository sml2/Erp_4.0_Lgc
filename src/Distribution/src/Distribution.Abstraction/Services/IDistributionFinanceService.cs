﻿using ERP.Models.DB.Identity;
using ERP.Models.DB.Users;

namespace ERP.Distribution.Abstraction.Services;

public interface IDistributionFinanceService
{
    /// <summary>
    /// 获取财务月份统计
    /// </summary>
    /// <param name="companyIds"></param>
    /// <param name="month"></param>
    /// <returns></returns>
    public Task<DbSetExtension.PaginateStruct<MonthFinanceDto>> GetMonthFinance(int[] companyIds, DateOnly? month);

    /// <summary>
    /// 获取上级公司账单审批
    /// </summary>
    /// <param name="type"></param>
    /// <param name="module"></param>
    /// <param name="audit"></param>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public Task<DbSetExtension.PaginateStruct<BillLogDto>> GetBillPage(int currentCompanyId, BillLog.Types? vmType,
        BillLog.Modules? vmModule, BillLog.Audits? vmAudit, int? vmCompanyId, string? unit, List<DateTime>? vmDate,
        string? vmOrderNo, string? vmPurchaseNo, string? vmWaybillNo);

    /// <summary>
    /// 获取账单详细信息
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public ValueTask<Models.Finance.BillLog?> GetBillLogInfo(int id);

    /// <summary>
    /// 更新账单信息
    /// </summary>
    /// <param name="id"></param>
    /// <param name="audit"></param>
    /// <param name="operateUser"></param>
    /// <param name="balance"></param>
    /// <returns></returns>
    public Task<int> UpdateBillLog(int id, Models.Finance.BillLog.Audits audit, Models.DB.Identity.User operateUser,
        Money? balance);

    /// <summary>
    /// 下级公司账单
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="type"></param>
    /// <param name="audit"></param>
    /// <param name="module"></param>
    /// <returns></returns>
    public Task<DbSetExtension.PaginateStruct<BillLogDto>> GetCurrentBillPage(int companyId,
        BillLog.Types? type, BillLog.Audits? audit,
        BillLog.Modules? module);
    
    public Task<DbSetExtension.PaginateStruct<BillLogDto>> GetCurrentBillPageMoreSearch(int companyId,
        BillLog.Types? type, BillLog.Audits? audit,
        BillLog.Modules? module, string? unit, List<DateTime>? date, string? orderNo, string? purchaseNo,
        string? waybillNo,int? storeId);

    /// <summary>
    /// 分销财务上下级钱包充值、支出账单
    /// </summary>
    /// <param name="money"></param>
    /// <param name="type"></param>
    /// <param name="datetime"></param>
    /// <param name="reason"></param>
    /// <param name="remark"></param>
    /// <param name="audit"></param>
    /// <param name="imgAll"></param>
    /// <param name="juniorCompany">下级公司</param>
    /// <param name="superiorCompany">上级公司</param>
    /// <param name="user"></param>
    /// <param name="auditUser"></param>
    /// <returns></returns>
    public Task AddWalletBill(Money money, BillLog.Types type, DateTime datetime, string reason, string remark,
        BillLog.Audits audit, string[] imgAll, Company juniorCompany, Company superiorCompany, User user,
        User? auditUser);

    Task<object?> GetBillPage();
}