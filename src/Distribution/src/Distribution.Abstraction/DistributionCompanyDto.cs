﻿using ERP.Models.DB.Identity;
using ERP.Models.DB.Users;

namespace ERP.Distribution.Abstraction;

public class DistributionCompanyDto
{
    public DistributionCompanyDto()
    {
        
    }
    

    public DistributionCompanyDto(Company m,User u)
    {
        Id = m.ID;
        Name = m.Name;
        Username = u.UserName;
        IsShowRate = m.IsShowRate;
        IsNegative = m.IsNegative;
        PurchaseRateType = m.PurchaseRateType;
        PurchaseRate = m.PurchaseRate;
        PurchaseRateNum = m.PurchaseRateNum;
        WaybillRateType = m.WaybillRateType;
        WaybillRate = m.WaybillRate;
        WaybillRateNum = m.WaybillRateNum;
        PublicWalletCanFreeze = m.PublicWalletCanFreeze;
        FreezeAmountPerWaybill = m.FreezeAmountPerWaybill;
        FreezeAmountPerWaybillCny = m.FreezeAmountPerWaybill.To("CNY");
        CreatedAt = m.CreatedAt;
    }

    public int Id { get; set; }
    [Required]
    public string Name { get; set; } = string.Empty;
    
    public string? Username { get; set; } = string.Empty;
    public bool IsShowRate { get; set; }
    public bool IsNegative { get; set; }
    public int PurchaseRateType { get; set; }
    public int PurchaseRate { get; set; }
    public int PurchaseRateNum { get; set; }
    [Required]
    public string PurchaseRateUnit { get; set; } = string.Empty;
    public int WaybillRateType { get; set; }
    public int WaybillRate { get; set; }
    public int WaybillRateNum { get; set; }
    [Required]
    public string WaybillRateUnit { get; set; } = string.Empty;
    public bool PublicWalletCanFreeze { get; set; }
    
    [JsonIgnore]
    public Money FreezeAmountPerWaybill { get; set; }
    
    [JsonProperty("freezeAmountPerWaybill")]
    public decimal FreezeAmountPerWaybillCny { get; set; }
    public DateTime CreatedAt { get; set; }
}

public class CompanyWalletDto
{
    public CompanyWalletDto(int id, string name, MoneyRecordFinancialAffairs publicWallet,Money freeze,string? username)
    {
        Id = id;
        Name = name;
        PublicWalletRaw = publicWallet;
        FreezeRaw = freeze;
        Username = username;
    }

    public string? Username { get; set; }

    private MoneyRecordFinancialAffairs PublicWalletRaw { get; set; }
    private Money FreezeRaw { get; set; }
    [JsonMoneyForDefaultUnit]public Money Freeze => FreezeRaw.Value;

    public int Id { get; set; }
    public string Name { get; set; }
    [JsonMoneyForDefaultUnit]public Money PublicWallet => PublicWalletRaw.Money;
}

public class CompanyOnlyNameDto
{
    public CompanyOnlyNameDto(Company company, User user)
    {
        Id = company.ID;
        Name = company.Name;
        Username = user.UserName;
    }
    
    public CompanyOnlyNameDto()
    {
    }

    public int Id { get; set; }
    [Required]
    public string Name { get; set; } = string.Empty;
    
    public string Username { get; set; } = string.Empty;
}