﻿using AutoMapper;
using ERP.Data;
using ERP.Distribution.Abstraction;
using ERP.Distribution.Abstraction.Services;
using ERP.Distribution.Services;
using ERP.Models.DB.Logistics;
using ERP.Models.DB.Users;
using Microsoft.Extensions.DependencyInjection;

namespace ERP;

public static class ServiceInjectExtension
{

    public static void AddDistribution<TDbContext>(this IServiceCollection service) where TDbContext : Base
    {
        service.AddScoped<IDistributionCompanyService , CompanyService>(CreateService<CompanyService>);
        service.AddScoped<IDistributionWaybillService , WaybillService>(CreateService<WaybillService>);
        AddMappers(service);
        
        T CreateService<T>(IServiceProvider provider)
        {
            var dbContext = provider.GetRequiredService<TDbContext>();
            return ActivatorUtilities.CreateInstance<T>(provider, dbContext);
        }
    }

    private static void AddMappers(IServiceCollection service)
    {
        service.Configure<MapperConfigurationExpression>(config =>
        {
            // company
            config.CreateMap<Company, DistributionCompanyDto>();
            config.CreateMap<Company, CompanyOnlyNameDto>();
            // waybill
            config.CreateMap<Parameter, ParameterDto>()
                .ForMember(p => p.Name, o => o.MapFrom(p => p.Platform.Name))
                .ForMember(p => p.LogisticsId, o => o.MapFrom(p => p.Platform.Platform));
            config.CreateMap<Parameter, ParameterInfoDto>()
                .ForMember(p => p.LogisticsId, o => o.MapFrom(p => p.Platform.Platform));
        });
    }
}