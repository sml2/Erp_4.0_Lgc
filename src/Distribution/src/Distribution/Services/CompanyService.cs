﻿using System.Data.Common;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using ERP.Distribution.Abstraction.Services;
using ERP.Distribution.Abstraction;
using ERP.Distribution.Abstraction.ViewModels;
using ERP.Extensions;
using ERP.Models.DB.Users;
using ERP.Services;
using Microsoft.EntityFrameworkCore;
using Ryu.Data;
using Ryu.Linq;
using Z.EntityFramework.Plus;
using ERP.Data;
using ERP.Enums.Identity;

namespace ERP.Distribution.Services;
public class CompanyService : BaseService, IDistributionCompanyService
{
    private readonly Base _dbContext;
    private readonly IMapper _mapper;

    public CompanyService(Base dbContext, IMapper mapper)
    {
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    }

    private DbSet<Company> Companies => _dbContext.Set<Company>();
    private IQueryable<Company> CompaniesNt => _dbContext.Set<Company>().AsNoTracking();

    /// <summary>
    /// 获取分销公司分页列表
    /// </summary>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public Task<DbSetExtension.PaginateStruct<DistributionCompanyDto>> GetDistributionListPage(int companyId)
    {
        // return Companies
        //     .Where(c => c.State == Company.States.ENABLE && c.IsDistribution && c.ReportId == companyId && c.ID != companyId)
        //     .ProjectTo<DistributionCompanyDto>(_mapper.ConfigurationProvider)
        //     .ToPaginateAsync();
        return Companies
            .Where(c => c.State == Company.States.ENABLE && c.IsDistribution && c.ReportId == companyId &&
                        c.ID != companyId)
            .Join(_dbContext.Users,c => c.ID,u => u.CompanyID, (company, user) => new
            {
                company,user
            })
            .Where(m => m.user.Role == Role.ADMIN)
            .Select(m => new DistributionCompanyDto(m.company,m.user))
            .ToPaginateAsync();
    }

    /// <summary>
    /// 获取分销公司费率信息
    /// </summary>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public async Task<DistributionCompanyDto?> GetDistributionConfig(int companyId)
    {
        var result = await Companies.Where(c => c.ID == companyId)
            .ProjectTo<DistributionCompanyDto>(_mapper.ConfigurationProvider)
            .FirstOrDefaultAsync();
        if (result is not null)
            result.FreezeAmountPerWaybillCny = result.FreezeAmountPerWaybill.To("CNY");
        return result;
    }

    /// <summary>
    /// 更新分销公司费率信息
    /// </summary>
    /// <param name="id"></param>
    /// <param name="requestData"></param>
    /// <returns></returns>
    public async Task<bool> UpdateDistributionConfig(int id, SubConfigVm requestData)
    {
        
        var model = await _dbContext.Set<Company>().Where(m => m.ID == id).FirstAsync();
        // Companies.Attach(model);
        model.IsShowRate = requestData.IsShowRate;
        model.PurchaseRate = (int)requestData.PurchaseRate;
        model.PurchaseRateType = requestData.PurchaseRateType;
        model.PurchaseRateNum = (int)requestData.PurchaseRateNum;
        // model.PurchaseRateUnit = requestData.PurchaseRateUnit;
        model.WaybillRate = (int)requestData.WaybillRate;
        model.WaybillRateType = requestData.WaybillRateType;
        model.WaybillRateNum = (int)requestData.WaybillRateNum;
        // model.WaybillRateUnit = requestData.WaybillRateUnit;
        model.IsNegative = requestData.IsNegative;

        model.PublicWalletCanFreeze = requestData.PublicWalletCanFreeze;
        model.FreezeAmountPerWaybill = MoneyFactory.Instance.Create(requestData.FreezeAmountPerWaybill, "CNY");
        //            ->state(companyModel::STATE_ENABLE)
        //            ->isDistribution(companyModel::DISTRIBUTION_TRUE)
        //            ->ReportId($this->getSession()->getCompanyId())
        await _dbContext.SaveChangesAsync();
        return true;
    }

    /// <summary>
    /// 获取有效下级分销公司数据
    /// </summary>
    /// <param name="companyId"></param>
    /// <returns></returns>
    public async Task<Dictionary<int, CompanyOnlyNameDto>> GetValidDistributionList(int companyId)
    {
        return (await Companies.Where(c => c.State == Company.States.ENABLE && c.IsDistribution && c.ReportId == companyId
                                           && c.ID != companyId)
                .Join(_dbContext.Users,c => c.ID,u => u.CompanyID, (company, user) => new
                {
                    company,user
                })
                .Where(m => m.user.Role == Role.ADMIN)
                .Select(m => new CompanyOnlyNameDto(m.company,m.user))
                .ToListAsync())
                .KeyBy(c => c.Id);
    }

    public Task<DbSetExtension.PaginateStruct<CompanyWalletDto>> GetWalletListPage(int currentCompanyId, int? companyId = null)
    {
        return CompaniesNt.Where(c => c.State == Company.States.ENABLE && c.IsDistribution && c.ReportId == currentCompanyId && c.ID != currentCompanyId)
            .WhenWhere(companyId.HasValue, c => c.ID == companyId)
            .Join(_dbContext.Users,c => c.ID,u => u.CompanyID, (company, user) => new
            {
                company,user
            })
            .Where(m => m.user.Role == Role.ADMIN)
            .Select(c => new CompanyWalletDto(c.company.ID, c.company.Name, c.company.PublicWallet,c.company.FreezeTotalAmountWaybill,c.user.UserName))
            .ToPaginateAsync();
    }
    
    public Task<int> GetReportIdCompanyCount(int companyId)
    {
        return _dbContext.Set<Company>().Where(c => c.State == Company.States.ENABLE && c.ReportId == companyId && c.ID != companyId)
            .CountAsync();
    }
    
    public async Task RechargeWallet(int companyId, Money money)
    {
        var company = await _dbContext.Set<Company>().Where(c => c.ID == companyId).FirstAsync();
        company.PublicWallet += money;
        await _dbContext.SaveChangesAsync();
    }
    
    public async Task ExpenseWallet(int companyId, Money money)
    {
        var company = await _dbContext.Set<Company>().Where(c => c.ID == companyId).FirstAsync();
        company.PublicWallet -= money;
        await _dbContext.SaveChangesAsync();
    }


    /// <summary>
    /// 指定钱包支出(new)
    /// </summary>
    /// <param name="money"></param>
    /// <param name="reason">模块原因</param>
    /// <param name="module">模块</param>
    /// <param name="company">钱包操作公司</param>
    /// <param name="companyCurrent">当前公祠</param>
    /// <param name="currentUser">当前用户</param>
    /// <param name="groupId"></param>
    /// <param name="remark"></param>
    /// <returns></returns>
    public static int ExpenseWalletNew(Money money, string reason, string module, Company company, Company companyCurrent, Models.DB.Identity.User currentUser, int groupId = 0, string remark = "")
    {
        //    CompanyModel::query()
        //        ->CompanyId($company['id'])
        //        ->decrement('public_wallet', $money);

        //    $billLog = Finance::addBillNew($money, $reason, $module, $company, $companyCurrent,
        //        $currentUser, $group_id, $remark);
        //    return $billLog->id;
        return 1;
    }

    /// <summary>
    /// 获取指定公司信息
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public virtual Task<Company?> GetInfoById(int id)
    {
        return Companies.FirstOrDefaultAsync(c => c.ID == id);
    }
}

