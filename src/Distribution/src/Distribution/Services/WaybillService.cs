using AutoMapper;
using AutoMapper.QueryableExtensions;
using ERP.Distribution.Abstraction;
using ERP.Distribution.Abstraction.Services;
using ERP.Enums.Logistics;
using ERP.Extensions; 
using ERP.Models.DB.Logistics;
using ERP.Models.DB.Storage;
using ERP.Services;
using ERP.Storage.Abstraction.Services;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;
using ERP.Data;
using System.Security.Cryptography.X509Certificates;

namespace ERP.Distribution.Services
{
    public class WaybillService : BaseService, IDistributionWaybillService
    {
        private readonly Base _dbContext;
        private readonly IMapper _mapper;
        private readonly IStockService _stockService;

        public WaybillService(Base dbContext, IMapper mapper, IStockService stockService)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _mapper = mapper;
            _stockService = stockService;
        }

        private DbSet<StockLog> StockLogs => _dbContext.Set<StockLog>();
        private DbSet<Waybill> Waybills => _dbContext.Set<Waybill>();
        private IQueryable<Waybill> WaybillsNt => _dbContext.Set<Waybill>().AsNoTracking();
        private DbSet<Parameter> Parameters => _dbContext.Set<Parameter>();

        /// <summary>
        /// 上级获取上报运单分页列表
        /// </summary>
        /// <param name="currentCompanyId"></param>
        /// <param name="orderNo"></param>
        /// <param name="express"></param>
        /// <param name="waybillOrder"></param>
        /// <param name="tracking"></param>
        /// <param name="waybillState"></param>
        /// <param name="companyId"></param>
        /// <param name="userId"></param>
        /// <param name="warehouseId"></param>
        /// <returns></returns>
        public async Task<DbSetExtension.PaginateStruct<Waybill>> GetListPage(int currentCompanyId,string? orderNo, string? express, string? waybillOrder, string? tracking,States? waybillState, 
            int? companyId, int? userId = null, int? warehouseId = null)
        {
            //    $field = ['id', 'waybill_order', 'express', 'company_id', 'store_id', 'tracking', 'logistic', 'logistic_id', 'carriage_num', 'carriage_unit', 'waybill_state', 'created_at', 'order_id', 'button_name', 'order_no', 'is_verify', 'button_name', 'operate_company_id', 'remark', 'img_url'];
            var ids = new List<int>();
            if (warehouseId.HasValue) {
                ids = await _stockService.GetLogWaybillIdByWarehouse(warehouseId.Value);
            }

            return await Waybills.Where(w => w.WaybillCompanyID == currentCompanyId)
                .WhenWhere(userId.HasValue, w => w.UserID == userId)
                .WhenWhere(companyId.HasValue, w => w.CompanyID == companyId)
                .WhenWhere(waybillState.HasValue, w => w.State == waybillState)
                .WhenWhere(warehouseId.HasValue, w => ids.Contains(w.ID))
                .WhenWhere(!string.IsNullOrEmpty(orderNo), w => w.OrderNo == orderNo)
                .WhenWhere(!string.IsNullOrEmpty(tracking), w => w.Tracking == tracking)
                .WhenWhere(!string.IsNullOrEmpty(express), w => w.Express == express)
                .WhenWhere(!string.IsNullOrEmpty(waybillOrder), w => w.WaybillOrder == waybillOrder)
                .OrderByDescending(o => o.ID)
                .ToPaginateAsync();
        }
        
        public async Task<DbSetExtension.PaginateStruct<Waybill>> GetJuniorListPage(int currentCompanyId, int reportCompanyId,string? orderNo, string? express
            , string? waybillOrder, string? tracking, States? waybillState
            , int? storeId, int[] storeIds, int? warehouseId = null)
        {
            //    $field = ['id', 'waybill_order', 'express', 'company_id', 'store_id', 'tracking', 'logistic', 'logistic_id', 'carriage_num', 'carriage_unit', 'waybill_state', 'created_at', 'order_id', 'button_name', 'order_no', 'is_verify', 'button_name', 'operate_company_id', 'remark', 'img_url'];
            //    $ids = collect();
            //        if ($warehouseId) {
            //        $ids = StockLog::whereIWarehouseId($warehouseId)->pluck('waybill_id')->filter(fn($v) => $v !== 0);
            //        }
            var ids = new List<int>();
            if (warehouseId.HasValue) {
                ids = await _stockService.GetLogWaybillIdByWarehouse(warehouseId.Value);
            }

            return await WaybillsNt.Where(w => w.WaybillCompanyID == reportCompanyId && w.CompanyID == currentCompanyId)
                .WhenWhere(waybillState.HasValue, w => w.State == waybillState)
                .WhenWhere(storeId.HasValue, w => w.StoreId == storeId)
                .WhenWhere(storeIds is { Length: > 0 }, w => storeIds.Contains(w.StoreId))
                .WhenWhere(warehouseId.HasValue, w => ids.Contains(w.ID))
                .WhenWhere(!string.IsNullOrEmpty(orderNo), w => w.OrderNo == orderNo)
                .WhenWhere(!string.IsNullOrEmpty(tracking), w => w.Tracking == tracking)
                .WhenWhere(!string.IsNullOrEmpty(express), w => w.Express == express)
                .WhenWhere(!string.IsNullOrEmpty(waybillOrder), w => w.WaybillOrder == waybillOrder)
                .OrderByDescending(o => o.ID)
                .ToPaginateAsync();
        }
        
        public Task<Waybill?> GetInfoById(int id)
        {
            return Waybills.FirstOrDefaultAsync(w => w.ID == id);
        }
        
        public Task<Parameter?> GetLogisticsInfo(int id)
        {
            return _dbContext.Set<Parameter>().FirstOrDefaultAsync(w => w.ID == id);
        }

        public async Task ConfirmUpdate(int id, int billLogId)
        {
            await Task.CompletedTask;
            throw new NotImplementedException();
            //        return $this->wayBillService->confirmUpdate($id, $bill_log_id);
        }
        
        public Task<List<Waybill>> GetWaybillByOrderId(int orderId)
        {
            //    $field = ['id', 'waybill_order', 'tracking', 'logistic', 'carriage_num', 'tracking', 'deliver_date', 'waybill_state', 'purchase_tracking_number', 'button_name', 'is_verify', 'operate_company_id'];
            return Waybills.Where(w => w.OrderId == orderId)
                .ToListAsync();
        }

        /// <summary>
        /// 获取当前公司物流信息
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<DbSetExtension.PaginateStruct<ParameterDto>> GetAllLogistics(int companyId, string? name = "")
        {
            return await Parameters.AsNoTracking().Where(l => l.CompanyID == companyId && l.Opened)
                .WhenWhere(!string.IsNullOrEmpty(name), l => l.CustomizeName.StartsWith(name!))
                .ProjectTo<ParameterDto>(_mapper.ConfigurationProvider)
                .ToPaginateAsync();
        }

        /// <summary>
        /// 获取指定公司被分配物流信息
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="pids"></param>
        /// <returns></returns>
        public Task<List<ParameterInfoDto>> GetAllotLogistics(int companyId, int[]? pids = null)
        {
            return Parameters.AsNoTracking().Where(l => l.CompanyID == companyId && l.Source == Enums.Logistics.Sources.SUPERIOR && l.IsSysState == true)
                .WhenWhere(pids is { Length: > 0 }, l => pids!.Contains(l.ParentID))
                .ProjectTo<ParameterInfoDto>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }
        
        public Task<List<Parameter>> GetLogistics(int currentCompanyId,IEnumerable<int> ids)
        {
            return Parameters.Where(p => p.CompanyID == currentCompanyId && p.Opened && ids.Contains(p.ID))
                .ToListAsync();
        }

        public async Task<bool> AddLogistics(Parameter model)
        {
            _dbContext.Set<Parameter>().Add(model);
            await _dbContext.SaveChangesAsync();
            return true;
        }
        
        public async Task<bool> ChangeChargeType(int companyId, int pid, Charges charge)
        {
            await _dbContext.Set<Parameter>()
                .Where(p => p.CompanyID == companyId && p.ParentID == pid && p.Source == Sources.SUPERIOR)
                .UpdateFromQueryAsync(p => new { Charge = charge});
            return true;
        }

        /// <summary>
        /// 删除上级分配指定物流
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="pid"></param>
        /// <returns></returns>
        public async Task<int> DelLogistics(int companyId, int pid)
        {
            var list = Parameters.Where(l => l.CompanyID == companyId && l.Source == Sources.SUPERIOR && l.ParentID == pid && l.IsSysState==true).ToList();
            foreach(var item in list)
            {
                item.IsSysState = false;
            }
             _dbContext.Set<Parameter>().UpdateRange(list);
           return await _dbContext.SaveChangesAsync();
            //return Parameters.Where(l => l.CompanyID == companyId && l.Source == Sources.SUPERIOR && l.ParentID == pid)
            //    .DeleteFromQueryAsync();
        }
        
        public async Task UpdatePurchaseTrackingNumber(int id, string purchaseTrackingNumber)
        {
             await Task.CompletedTask;
            //        return wayBillModel::query()->id($id)->update(['purchase_tracking_number' => $purchase_tracking_number])
        }

        
        public Task<List<Waybill>> GetWaybill(int companyId, string express)
        {
            // return _dbContext.Set<Waybill>().Where(w => w.CompanyID == companyId && w.Express == waybillOrder)
                // .ToListAsync();
            return _dbContext.Set<Waybill>().Where(w => w.Express == express)
                .ToListAsync();
        }
    }
}
