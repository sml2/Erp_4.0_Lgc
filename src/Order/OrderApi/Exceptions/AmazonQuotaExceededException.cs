﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace OrderSDK.Exceptions
{

    public class AmazonQuotaExceededException: AmazonException
    {
        public AmazonQuotaExceededException(string msg):base(msg) { }
       
    }

    public class ExceptionResponse
    {
        public string? Content { get; set; }
        public HttpStatusCode? ResponseCode { get; set; }
    }

    public class AmazonSPException : AmazonException
    {
        public ExceptionResponse? Response { get; set; }

        public AmazonSPException(string msg, HttpResponseMessage response) : base(msg)
        {
            if (response != null)
            {
                Response = new ExceptionResponse();
                Response.Content = System.Text.Encoding.UTF8.GetString(response.Content.ReadAsByteArrayAsync().Result);
                Response.ResponseCode = response.StatusCode;
            }
        }

    }

    public class AmazonProcessingReportDeserializeException : AmazonSPException
    {
        public string ReportContent { get; set; }
        public AmazonProcessingReportDeserializeException(string msg, string reportContent, HttpResponseMessage? response = null) : base(msg, response)
        {
            ReportContent = reportContent;
        }
    }

    public class AmazonException : OwnException
    {
        public AmazonException(string msg)
        {

        }
    }


    public class OwnException: System.Exception
    {
        public OwnException()
        {

        }
        public OwnException(string msg)
        {
            Console.WriteLine(msg); 
        }
    }


}
