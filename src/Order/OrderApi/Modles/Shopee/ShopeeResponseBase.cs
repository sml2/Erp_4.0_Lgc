using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee
{
    public class ShopeeResponseBase<T>
    {
        [JsonProperty("error")]
        public string Error { get; set; } = string.Empty;

        [JsonProperty("message")]
        public string Message { get; set; } = string.Empty;

        [JsonProperty("warning")]
        public string Warning { get; set; } = string.Empty;

        [JsonProperty("request_id")]
        public string RequestId { get; set; } = string.Empty;

        [JsonProperty("Response")]
        public T Response { get; set; } = default!;
    }
}