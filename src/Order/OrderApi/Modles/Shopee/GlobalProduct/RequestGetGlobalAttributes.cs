﻿using Newtonsoft.Json;
using OrderSDK.Modles.Shopee.Enums;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.GlobalProduct
{
    public class RequestGetGlobalAttributes : RequestShopee
    {
        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

        public override EShopeeCommonParamType CommonParamType { get; set; }
    = EShopeeCommonParamType.MERCHANT;

        [JsonRequired]
        [JsonProperty("category_id")]
        public long CategoryId { get; set; }

        [JsonProperty("language")]
        public string Language { get; set; } = string.Empty;

        [JsonProperty("need_region_mandatory")]
        public bool NeedRegionMandatory { get; set; }
    }
}
