﻿using Newtonsoft.Json;
using OrderSDK.Modles.Shopee.Enums;
using System.Net.Http;
namespace OrderSDK.Modles.Shopee.GlobalProduct
{
    public class RequestGetGlobalCategory : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/global_product/get_category";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

        public override EShopeeCommonParamType CommonParamType { get; set; }
      = EShopeeCommonParamType.MERCHANT;

        [JsonProperty("language")]
        public string Language { get; set; } = string.Empty;
    }
}
