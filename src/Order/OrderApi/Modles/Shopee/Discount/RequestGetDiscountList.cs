﻿using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using OrderSDK.Modles.Shopee.Enums;

namespace OrderSDK.Modles.Shopee.Discount
{
    public class RequestGetDiscountList : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/discount/get_discount_list";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

        [JsonProperty("discount_status")]
        [EnumDataType(typeof(EShopeeDiscountStatus))]
        [JsonConverter(typeof(StringEnumConverter))]
        public EShopeeDiscountStatus DiscountStatus { get; set; }

        [JsonProperty("page_no")]
        public int PageNo { get; set; }

        [JsonProperty("page_size")]
        public int PageSize { get; set; }
    }
}