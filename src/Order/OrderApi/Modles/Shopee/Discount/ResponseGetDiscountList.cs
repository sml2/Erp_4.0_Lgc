﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using OrderSDK.Modles.Shopee.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace OrderSDK.Modles.Shopee.Discount
{
    public class ResponseGetDiscountList
    {
        [JsonProperty("discount_list")]
        public IEnumerable<_Discount> DiscountList { get; set; } = Enumerable.Empty<_Discount>();

        [JsonProperty("more")]
        public bool More { get; set; }

        public class _Discount
        {
            [JsonProperty("status")]
            [EnumDataType(typeof(EShopeeDiscountStatus))]
            [JsonConverter(typeof(StringEnumConverter))]
            public EShopeeDiscountStatus Status { get; set; }

            [JsonProperty("discount_name")]
            public string DiscountName { get; set; } = string.Empty;

            [JsonProperty("start_time")]
            public long StartTime { get; set; }

            [JsonProperty("end_time")]
            public long EndTime { get; set; }

            [JsonProperty("discount_id")]
            public long DiscountId { get; set; }
        }
    }
}
