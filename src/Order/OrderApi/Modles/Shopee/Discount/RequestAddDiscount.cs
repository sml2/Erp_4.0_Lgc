﻿using System.Net.Http;
using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee.Discount
{
    public class RequestAddDiscount : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/discount/add_discount";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonProperty("discount_name")]
        public string DiscountName { get; set; } = string.Empty;

        [JsonProperty("start_time")]
        public long StartTime { get; set; }

        [JsonProperty("end_time")]
        public long EndTime { get; set; }
    }
}