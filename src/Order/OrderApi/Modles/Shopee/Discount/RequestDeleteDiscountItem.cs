﻿using Newtonsoft.Json;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Discount
{
    public class RequestDeleteDiscountItem : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/discount/delete_discount_item";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonProperty("discount_id")]
        public long DiscountId { get; set; }

        [JsonProperty("item_id")]
        public long ItemId { get; set; }

        [JsonProperty("model_id")]
        public long ModelId { get; set; }
    }
}