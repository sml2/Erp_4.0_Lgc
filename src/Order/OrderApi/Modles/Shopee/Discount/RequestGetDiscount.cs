﻿using Newtonsoft.Json;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Discount
{
    public class RequestGetDiscount : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/discount/get_discount";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

        [JsonProperty("discount_id")]
        public long DiscountId { get; set; }

        [JsonProperty("page_no")]
        public int PageNo { get; set; }

        [JsonProperty("page_size")]
        public int PageSize { get; set; }
    }
}