﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Discount
{
    public class RequestAddDiscountItem : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/discount/add_discount_item";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonProperty("discount_id")]
        public long DiscountId { get; set; }

        [JsonProperty("item_list")]

        public IEnumerable<_Item> ItemList { get; set; } = Enumerable.Empty<_Item>();

        public class _Item
        {
            [JsonProperty("item_id")]

            public long ItemId { get; set; }

            [JsonProperty("model_list", NullValueHandling = NullValueHandling.Ignore)]
            public IEnumerable<_Model> ModelList { get; set; } = Enumerable.Empty<_Model>();

            [JsonProperty("item_promotion_price", NullValueHandling = NullValueHandling.Ignore)]
            public double ItemPromotionPrice { get; set; }

            [JsonProperty("purchase_limit", NullValueHandling = NullValueHandling.Ignore)]
            public int PurchaseLimit { get; set; }

            [JsonProperty("item_promotion_stock", NullValueHandling = NullValueHandling.Ignore)]
            public int ItemPromotionStock { get; set; }

            public class _Model
            {
                [JsonProperty("model_id")]
                public long ModelId { get; set; }

                [JsonProperty("model_promotion_price")]
                public double ModelPromotionPrice { get; set; }

                [JsonProperty("model_promotion_stock", NullValueHandling = NullValueHandling.Ignore)]
                public int ModelPromotionStock { get; set; }
            }
        }
    }
}