﻿using Newtonsoft.Json;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Discount
{
    public class RequestDeleteDiscount : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/discount/delete_discount";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonProperty("discount_id")]
        public long DiscountId { get; set; }
    }
}