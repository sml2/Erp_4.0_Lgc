﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OrderSDK.Modles.Shopee.Discount
{
    public class ResponseGetDiscount
    {
        [JsonProperty("status")]
        public string Status { get; set; } = string.Empty;

        [JsonProperty("discount_name")]
        public string DiscountName { get; set; } = string.Empty;

        [JsonProperty("item_list")]
        public IEnumerable<_Item> ItemList { get; set; } = Enumerable.Empty<_Item>();

        [JsonProperty("start_time")]
        public long StartTime { get; set; }

        [JsonProperty("discount_id")]
        public long DiscountId { get; set; }

        [JsonProperty("end_time")]
        public long EndTime { get; set; }

        [JsonProperty("more")]
        public bool More { get; set; }


        public class _Item
        {
            [JsonProperty("item_promotion_price")]
            public double ItemPromotionPrice { get; set; }

            [JsonProperty("item_name")]
            public string ItemName { get; set; } = string.Empty;

            [JsonProperty("model_list")]
            public IEnumerable<_Model> ModelList { get; set; } = Enumerable.Empty<_Model>();

            [JsonProperty("item_id")]
            public long ItemId { get; set; }

            [JsonProperty("purchase_limit")]
            public int PurchaseLimit { get; set; }

            [JsonProperty("item_original_price")]
            public double ItemOriginalPrice { get; set; }

            [JsonProperty("normal_stock")]
            public int NormalStock { get; set; }

            [JsonProperty("item_inflated_price_of_original_price")]
            public double ItemInflatedPriceOfOriginalPrice { get; set; }

            [JsonProperty("item_inflated_price_of_promotion_price")]
            public double ItemInflatedPriceOfPromotionPrice { get; set; }

            [JsonProperty("item_promotion_stock")]
            public int ItemPromotionStock { get; set; }

            public class _Model
            {
                [JsonProperty("model_name")]
                public string ModelName { get; set; } = string.Empty;

                [JsonProperty("model_normal_stock")]
                public int ModelNormalStock { get; set; }

                [JsonProperty("model_original_price")]
                public double ModelOriginalPrice { get; set; }

                [JsonProperty("model_promotion_price")]
                public double ModelPromotionPrice { get; set; }

                [JsonProperty("model_id")]
                public long ModelId { get; set; }

                [JsonProperty("model_inflated_price_of_original_price")]
                public double ModelInflatedPriceOfOriginalPrice { get; set; }

                [JsonProperty("model_inflated_price_of_promotion_price")]
                public double ModelInflatedPriceOfPromotionPrice { get; set; }

                [JsonProperty("model_promotion_stock")]
                public int ModelPromotionStock { get; set; }
            }
        }
    }
}
