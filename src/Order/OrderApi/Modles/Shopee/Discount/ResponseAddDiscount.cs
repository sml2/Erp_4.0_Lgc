﻿using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee.Discount
{
    public class ResponseAddDiscount
    {
        [JsonProperty("discount_id")]
        public long DiscountId { get; set; }
    }
}
