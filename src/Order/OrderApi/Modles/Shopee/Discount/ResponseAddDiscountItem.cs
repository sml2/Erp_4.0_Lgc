﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OrderSDK.Modles.Shopee.Discount
{
    public class ResponseAddDiscountItem
    {
        [JsonProperty("discount_id")]
        public long DiscountId { get; set; }

        [JsonProperty("count")]
        public int Count { get; set; }

        [JsonProperty("error_list")]
        public IEnumerable<_Error> ErrorList { get; set; } = Enumerable.Empty<_Error>();

        public class _Error
        {
            [JsonProperty("item_id")]
            public long ItemId { get; set; }

            [JsonProperty("model_id")]
            public long ModelId { get; set; }

            [JsonProperty("fail_message")]
            public string FailMessage { get; set; } = string.Empty;

            [JsonProperty("fail_error")]
            public string FailError { get; set; } = string.Empty;
        }

    }
}
