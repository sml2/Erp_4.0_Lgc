using Newtonsoft.Json;
using System.Collections.Generic;

namespace OrderSDK.Modles.Shopee.Shop
{
    public class ResponseGetShopInfo
    {
        [JsonProperty("shop_name")]
        public string ShopName { get; set; } = string.Empty;

        [JsonProperty("region")]
        public string Region { get; set; } = string.Empty;

        [JsonProperty("status")]
        public string Status { get; set; } = string.Empty;

        [JsonProperty("is_cb")]
        public bool IsCb { get; set; }

        [JsonProperty("is_cnsc")]
        public bool IsCNSC { get; set; }

        [JsonProperty("auth_time")]
        public long AuthTime { get; set; }

        [JsonProperty("expire_time")]
        public long ExpireTime { get; set; }

        [JsonProperty("sip_affi_shops")]
        public IEnumerable<_SIPAffiShop> SipAffiShops { get; set; } = Enumerable.Empty<_SIPAffiShop>();

        public class _SIPAffiShop
        {
            [JsonProperty("affi_shop_id")]
            public long AffiShopId { get; set; }

            [JsonProperty("region")]
            public string Region { get; set; } = string.Empty;
        }
    }

}