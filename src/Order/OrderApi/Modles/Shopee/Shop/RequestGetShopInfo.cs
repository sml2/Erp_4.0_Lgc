using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee.Shop
{
    public class RequestGetShopInfo : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/shop/get_shop_info";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

    }
}