using Newtonsoft.Json;
using System.Collections.Generic;

namespace OrderSDK.Modles.Shopee.MediaSpace
{
    public class ResponseUploadImage
    {
        [JsonProperty("image_info")]
        public _ImageInfo ImageInfo { get; set; } = new();

        public class _ImageInfo
        {
            [JsonProperty("image_id")]
            public string ImageId { get; set; } = string.Empty;

            [JsonProperty("image_url_list")]
            public IEnumerable<_ImageUrl> ImageUrlList { get; set; } = Enumerable.Empty<_ImageUrl>();

            public class _ImageUrl
            {
                [JsonProperty("image_url_region")]
                public string ImageUrlRegion { get; set; } = string.Empty;

                [JsonProperty("image_url")]
                public string ImageUrl { get; set; } = string.Empty;
            }
        }
    }
}