using Newtonsoft.Json;
using System;
using System.Net.Http;
using OrderSDK.Modles.Shopee.Enums;
using OrderSDK.Services.Shopee.Validator.Attributes;

namespace OrderSDK.Modles.Shopee.MediaSpace
{
    public class RequestUploadImage : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/media_space/upload_image";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonIgnore]
        public override EShopeeCommonParamType CommonParamType => EShopeeCommonParamType.COMMON;

        [JsonProperty("image")]
        public override string Image
        {
            get { return _Image; }
            set
            {
                _Image = value;
                using (HttpClient client = new HttpClient())
                {
                    ImageByte = client.GetByteArrayAsync(value).Result;
                }
            }
        }

        private string _Image = string.Empty;

        [JsonIgnore]
        [ValidatorMethod(nameof(CheckImageType))]
        public string ImageType { get; set; } = string.Empty;
        public (bool, string) CheckImageType(object o) => (".png.jpg.jpge".Contains(ImageType, StringComparison.CurrentCultureIgnoreCase), $"ImageType[{ImageType}] must be png or jpg or jpge");
        public override byte[] ImageByte { get; set; } = Array.Empty<byte>();
    }

}