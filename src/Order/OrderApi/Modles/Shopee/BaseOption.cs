﻿namespace OrderSDK.Modles.Shopee
{
    public class BaseOption
    {
        //shopee配置项
        public static ShopeeOption ShopeeOption { get; set; } = new ShopeeOption();
    }

    public class ShopeeOption
    {
        public int PartnerId { get; set; }
        public string SecretKey { get; set; } = string.Empty;
        /// <summary>
        ///  true   productEnv，false sandBoxEnv
        /// </summary>
        public bool ApiMode { get; set; }  //EShopeeAPIMode

        public string RedirectUrl { get; set; } = string.Empty;

        public string SandBox_URL { get; set; } = string.Empty;

        public string Production_URL { get; set; } = string.Empty;
    }
}
