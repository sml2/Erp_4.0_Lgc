using Newtonsoft.Json;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Logistic
{
    public class RequestGetChannelList : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/logistics/get_channel_list";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;
    }
}