using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee.Logistic
{

    public class ResponseGetChannelList
    {
        [JsonProperty("logistics_channel_list")]
        public IEnumerable<_LogisticsChannel> LogisticsChannelList { get; set; } = Enumerable.Empty<_LogisticsChannel>();

        public class _LogisticsChannel
        {
            [JsonProperty("logistics_channel_id")]
            public int LogisticsChannelId { get; set; }

            [JsonProperty("preferred")]
            public bool Preferred { get; set; }

            [JsonProperty("logistics_channel_name")]
            public string LogisticsChannelName { get; set; } = string.Empty;

            [JsonProperty("cod_enabled")]
            public bool CodEnabled { get; set; }

            [JsonProperty("enabled")]
            public bool Enabled { get; set; }

            [JsonProperty("fee_type")]
            public string FeeType { get; set; } = string.Empty;

            [JsonProperty("size_list")]
            public IEnumerable<_Size> SizeList { get; set; } = Enumerable.Empty<_Size>();

            [JsonProperty("weight_limit")]
            public _WeightLimit WeightLimit { get; set; } = new();

            [JsonProperty("item_max_dimension")]
            public _ItemMaxDimension ItemMaxDimension { get; set; } = new();

            [JsonProperty("volume_limit")]
            public _VolumeLimit VolumeLimit { get; set; } = new();

            [JsonProperty("logistics_description")]
            public string LogisticsDescription { get; set; } = string.Empty;

            [JsonProperty("force_enabled")]
            public bool ForceEnabled { get; set; }

            [JsonProperty("mask_channel_id")]
            public int MaskChannelId { get; set; }


            public class _Size
            {
                [JsonProperty("size_id")]
                public long SizeId { get; set; }

                [JsonProperty("name")]
                public string Name { get; set; } = string.Empty;

                [JsonProperty("default_price")]
                public double DefaultPrice { get; set; }
            }

            public class _WeightLimit
            {
                [JsonProperty("item_max_weight")]
                public double ItemMaxWeight { get; set; }

                [JsonProperty("item_min_weight")]
                public double ItemMinWeight { get; set; }
            }

            public class _ItemMaxDimension
            {
                [JsonProperty("height")]
                public double Height { get; set; }

                [JsonProperty("width")]
                public double Width { get; set; }

                [JsonProperty("length")]
                public double Length { get; set; }

                [JsonProperty("unit")]
                public string Unit { get; set; } = string.Empty;
            }

            public class _VolumeLimit
            {
                [JsonProperty("item_max_volume")]
                public double ItemMaxVolume { get; set; }

                [JsonProperty("item_min_volume")]
                public double ItemMinVolume { get; set; }
            }
        }

    }
}