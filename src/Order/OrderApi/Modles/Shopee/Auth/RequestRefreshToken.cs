using Newtonsoft.Json;
using OrderSDK.Modles.Shopee.Enums;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Auth
{
    public class RequestRefreshAccessToken : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/auth/access_token/get";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; } = string.Empty;

        [JsonProperty("partner_id")]
        public int PartnerId { get; set; }

        public override EShopeeCommonParamType CommonParamType => EShopeeCommonParamType.COMMON;

    }
}