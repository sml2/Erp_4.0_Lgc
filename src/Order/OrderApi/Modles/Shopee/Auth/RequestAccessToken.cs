using System.Net.Http;
using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee.Auth
{

    public class RequestAccessToken : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/auth/token/get";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonProperty("partner_id")]
        public long PartnerId { get; set; }

        [JsonProperty("main_account_id")]
        public long? MainAccountID { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; } = string.Empty;

        public new string ToPayload()
        {
            string json = JsonConvert.SerializeObject(this);
            return json;
        }
    }
}