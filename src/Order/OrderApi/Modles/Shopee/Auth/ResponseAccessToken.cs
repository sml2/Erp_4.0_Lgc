using System.Collections.Generic;
using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee.Auth
{
    public class ResponseAccessToken
    {
        //{"error":"error_unknown","msg":"Invalid error key:error_unknown","request_id":"411ca13543436876e0991a76f726d21c"}
        [JsonProperty("request_id")]
        public string RequestId { get; set; } = string.Empty;

        [JsonProperty("error")]
        public string Error { get; set; } = string.Empty;

        [JsonProperty("access_token")]
        public string AccessToken { get; set; } = string.Empty;

        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; } = string.Empty;

        [JsonProperty("expire_in")]
        public int ExpireIn { get; set; }

        [JsonProperty("msg")]
        public string Message { get; set; } = string.Empty;

        [JsonProperty("merchant_id")]
        public int MerchantId { get; set; }

        [JsonProperty("shop_id")]
        public int ShopId { get; set; }

        [JsonProperty("merchant_id_list")]
        public List<int> MerchantIdList { get; set; } = Array.Empty<int>().ToList();

        [JsonProperty("shop_id_list")]
        public List<int> ShopIdList { get; set; } = Array.Empty<int>().ToList();
    }
}