﻿using Newtonsoft.Json;
using OrderSDK.Modles.Shopee.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace OrderSDK.Modles.Shopee.Public
{
    public class RequestGetShopsByPartner : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/public/get_shops_by_partner";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

        [JsonIgnore]
        public override EShopeeCommonParamType CommonParamType { get; set; }
      = EShopeeCommonParamType.COMMON;

        [JsonProperty("page_size")]
        public int PageSize { get; set; }

        [JsonProperty("page_no")]
        public int PageNo { get; set; }
    }
}
