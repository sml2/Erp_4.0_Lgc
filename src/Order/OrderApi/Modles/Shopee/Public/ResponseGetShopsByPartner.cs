﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderSDK.Modles.Shopee.Public
{
    public class ResponseGetShopsByPartner
    {
        [JsonProperty("request_id")]
        public string RequestId { get; set; } = string.Empty;

        [JsonProperty("more")]
        public bool More { get; set; }

        [JsonProperty("authed_shop_list")]
        public List<AuthedShopList> AuthedShopList { get; set; } = new();
    }

    public class AuthedShopList
    {
        [JsonProperty("region")]
        public string Region { get; set; } = string.Empty;

        [JsonProperty("shop_id")]
        public int ShopId { get; set; }

        [JsonProperty("auth_time")]
        public long AuthTime { get; set; }

        [JsonProperty("expire_time")]
        public long ExpireTime { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; } = string.Empty;

        [JsonProperty("sip_affi_shop_list")]
        public List<SipAffiShopList> SipAffiShopList { get; set; } = new();

    }

    public class SipAffiShopList
    {
        [JsonProperty("region")]
        public string Region { get; set; } = string.Empty;

        [JsonProperty("affi_shop_id")]
        public int AffiShopId { get; set; }
    }
}
