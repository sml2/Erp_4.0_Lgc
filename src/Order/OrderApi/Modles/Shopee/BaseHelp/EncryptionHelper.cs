﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace OrderSDK.Modles.Shopee.BaseHelp;

public static class EncryptionHelper
{
    /// <summary>
    /// shopee加密方法
    /// </summary>
    /// <param name="key">加盐key</param>
    /// <param name="payload">需要加密的字符串</param>
    /// <returns></returns>
    public static string GenerateHMAC(string key, string payload)
    {
        var hmac_key = Encoding.UTF8.GetBytes(key);
        using (HMACSHA256 sha = new HMACSHA256(hmac_key))
        {
            var bytes = Encoding.UTF8.GetBytes(payload);
            var hash = sha.ComputeHash(bytes);
            return ByteToString(hash);
        }
    }

    /// <summary>
    /// Byte -> string
    /// </summary>
    /// <param name="buff"></param>
    /// <returns></returns>
    public static string ByteToString(byte[] buff)
    {
        string sbinary = "";
        for (int i = 0; i < buff.Length; i++)
        {
            sbinary += buff[i].ToString("x2"); //X2转换为大写的16进制，x2转换为小写的16进制
        }
        return sbinary;
    }
}

