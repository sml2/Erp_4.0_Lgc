﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderSDK.Modles.Shopee.BaseHelp.Enums;

namespace OrderSDK.Modles.Shopee.BaseHelp.Attributes;
public class CurrencyAttribute : Attribute
{
    public ECurrency[] eCurrencies;

    public CurrencyAttribute(params ECurrency[] eCurrencies)
    {
        this.eCurrencies = eCurrencies;
    }

    public bool HasCurrencies(ECurrency currency)
    {
        ECurrency[] array = eCurrencies;
        for (int i = 0; i < array.Length; i++)
        {
            if (array[i] == currency)
            {
                return true;
            }
        }
        return false;
    }
}


