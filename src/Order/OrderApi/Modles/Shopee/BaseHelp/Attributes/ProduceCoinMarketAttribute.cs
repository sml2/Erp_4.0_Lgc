﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderSDK.Modles.Shopee.BaseHelp.Enums;

namespace OrderSDK.Modles.Shopee.BaseHelp.Attributes;

public class ProduceCoinMarketAttribute : Attribute
{
    public ECoinMarket[] _eCoinMarkets;

    public ProduceCoinMarketAttribute(params ECoinMarket[] eCoinMarkets)
    {
        _eCoinMarkets = eCoinMarkets;
    }
}
