﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderSDK.Modles.Shopee.BaseHelp.Enums;

namespace OrderSDK.Modles.Shopee.BaseHelp.Attributes;

public class LanguageAttribute : Attribute
{
    public ELanguage eLanguage;

    public LanguageAttribute(ELanguage eLanguage)
    {
        this.eLanguage = eLanguage;
    }
}

