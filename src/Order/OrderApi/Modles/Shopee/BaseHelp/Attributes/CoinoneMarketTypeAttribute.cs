﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderSDK.Modles.Shopee.BaseHelp.Models;

namespace OrderSDK.Modles.Shopee.BaseHelp.Attributes;
public class CoinoneMarketTypeAttribute : Attribute
{
    public ECoinoneMarketType eMarketType;

    public CoinoneMarketTypeAttribute(ECoinoneMarketType eMarketType)
    {
        this.eMarketType = eMarketType;
    }
}