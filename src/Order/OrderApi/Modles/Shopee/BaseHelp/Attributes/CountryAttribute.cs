﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderSDK.Modles.Shopee.BaseHelp.Enums;

namespace OrderSDK.Modles.Shopee.BaseHelp.Attributes;
public class CountryAttribute : Attribute
{
    public ECountry eCountry;

    public CountryAttribute(ECountry eCountry)
    {
        this.eCountry = eCountry;
    }
}

