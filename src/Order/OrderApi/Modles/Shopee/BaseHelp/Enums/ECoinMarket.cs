﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OrderSDK.Modles.Shopee.BaseHelp.Enums;
//生态市场
public enum ECoinMarket
{
    [EnumMember(Value = "None")]
    NONE,
    [EnumMember(Value = "Coinone")]
    COINONE,
    [EnumMember(Value = "Gopax")]
    GOPAX
}
