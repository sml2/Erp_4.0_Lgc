﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OrderSDK.Modles.Shopee.BaseHelp.Enums;

public enum ELanguage
{
    [EnumMember(Value = "")]
    NONE,
    [EnumMember(Value = "en")]
    EN,
    [EnumMember(Value = "th")]
    TH,
    [EnumMember(Value = "vi")]
    VI,
    [EnumMember(Value = "zh-SG")]
    ZH_SG,
    [EnumMember(Value = "ms")]
    MS,
    [EnumMember(Value = "en-ph")]
    EN_PH,
    [EnumMember(Value = "zh-TW")]
    ZH_TW,
    [EnumMember(Value = "id")]
    ID,
    [EnumMember(Value = "ko")]
    KO
}


