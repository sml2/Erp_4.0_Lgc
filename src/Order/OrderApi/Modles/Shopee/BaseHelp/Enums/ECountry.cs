﻿using OrderSDK.Modles.Shopee.BaseHelp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;


namespace OrderSDK.Modles.Shopee.BaseHelp.Enums;
public enum ECountry
{
    NONE,
    GH,
    GA,
    GY,
    GM,
    GG,
    GP,
    GT,
    GU,
    GD,
    GR,
    GL,
    GN,
    GW,
    NA,
    NR,
    NG,
    AQ,
    SS,
    ZA,
    NL,
    AN,
    NP,
    NO,
    NF,
    NC,
    NZ,
    NU,
    NE,
    NI,
    [Currency(new ECurrency[]
    {
        ECurrency.KRW
    })]
    [Language(ELanguage.KO)]
    KR,
    DK,
    DM,
    DO,
    DE,
    TL,
    LA,
    LR,
    LV,
    [Currency(new ECurrency[]
    {
        ECurrency.RUB
    })]
    RU,
    LB,
    LS,
    RE,
    RO,
    LU,
    RW,
    LY,
    LT,
    LI,
    MG,
    MQ,
    MH,
    YT,
    MO,
    MK,
    MW,
    [Display(Name = "Malaysia")]
    [Currency(new ECurrency[]
    {
        ECurrency.MYR
    })]
    [Language(ELanguage.MS)]
    MY,
    ML,
    IM,
    MX,
    MC,
    MA,
    MU,
    MR,
    MZ,
    ME,
    MS,
    MD,
    MV,
    MT,
    MN,
    [EnumMember(Value = "US")]
    [Currency(new ECurrency[]
    {
        ECurrency.USD
    })]
    US,
    UM,
    VI,
    MM,
    FM,
    VU,
    BH,
    BB,
    VA,
    BS,
    BD,
    BM,
    BJ,
    VE,
    [Display(Name = "Vietnam")]
    [Currency(new ECurrency[]
    {
        ECurrency.VND
    })]
    [Language(ELanguage.VI)]
    VN,
    BE,
    BY,
    BZ,
    BA,
    BW,
    BO,
    BI,
    BF,
    BV,
    BT,
    MP,
    BG,
    BR,
    BN,
    WS,
    SA,
    GS,
    SM,
    ST,
    PM,
    EH,
    SN,
    RS,
    SC,
    LC,
    VC,
    KN,
    SH,
    SO,
    SB,
    SD,
    SR,
    LK,
    SJ,
    SZ,
    SE,
    [Currency(new ECurrency[]
    {
        ECurrency.CHF
    })]
    CH,
    ES,
    SK,
    SI,
    SY,
    SL,
    [Display(Name = "Singapore")]
    [Currency(new ECurrency[]
    {
        ECurrency.SGD
    })]
    [Language(ELanguage.EN)]
    SG,
    AE,
    AW,
    AM,
    AR,
    AS,
    IS,
    HT,
    IE,
    AZ,
    AF,
    AD,
    AL,
    DZ,
    AO,
    AG,
    AI,
    ER,
    EE,
    EC,
    ET,
    SV,
    GB,
    VG,
    IO,
    YE,
    OM,
    AU,
    AT,
    HN,
    AX,
    JO,
    UG,
    UY,
    UZ,
    UA,
    WF,
    IQ,
    IR,
    IL,
    EG,
    IT,
    [Display(Name = "Indonesia")]
    [Currency(new ECurrency[]
    {
        ECurrency.IDR
    })]
    [Language(ELanguage.ID)]
    ID,
    IN,
    [Currency(new ECurrency[]
    {
        ECurrency.JPY
    })]
    JP,
    JM,
    ZM,
    JE,
    GQ,
    KP,
    GE,
    CF,
    [Currency(new ECurrency[]
    {
        ECurrency.TWD
    })]
    [Language(ELanguage.ZH_TW)]
    TW,
    [Currency(new ECurrency[]
    {
        ECurrency.CNY
    })]
    CN,
    DJ,
    GI,
    ZW,
    TD,
    CZ,
    CL,
    CM,
    CV,
    KZ,
    QA,
    KH,
    CA,
    KE,
    KY,
    KM,
    CR,
    CC,
    CI,
    CO,
    CG,
    CD,
    CU,
    KW,
    CK,
    HR,
    CX,
    KG,
    KI,
    CY,
    [Display(Name = "Thailand")]
    [Currency(new ECurrency[]
    {
        ECurrency.THB
    })]
    [Language(ELanguage.TH)]
    TH,
    TJ,
    TZ,
    TC,
    TR,
    TG,
    TK,
    TO,
    TM,
    TV,
    TN,
    TT,
    PA,
    PY,
    PK,
    PG,
    PW,
    PS,
    FO,
    PE,
    PT,
    FK,
    PL,
    PR,
    FR,
    GF,
    TF,
    PF,
    FJ,
    FI,
    [Display(Name = "Philippines")]
    [Currency(new ECurrency[]
    {
        ECurrency.PHP
    })]
    [Language(ELanguage.EN)]
    PH,
    PN,
    HM,
    HU,
    [Currency(new ECurrency[]
    {
        ECurrency.HKD
    })]
    HK
}
