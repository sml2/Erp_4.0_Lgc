﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderSDK.Modles.Shopee.BaseHelp.Attributes;
using OrderSDK.Modles.Shopee.BaseHelp.Enums;

namespace OrderSDK.Modles.Shopee.BaseHelp.Helpers;

internal static class CurrencyHelper
{
    private static bool HasCurrencies(this ECountry e, ECurrency eCurrency)
    {
        return ((CurrencyAttribute)typeof(ECountry).GetMember(e.ToString()).First().GetCustomAttributes(typeof(CurrencyAttribute), inherit: false)
            .First() ?? default!).HasCurrencies(eCurrency);
    }

    private static bool HasCurrencies(this ECountry e, params ECurrency[] eCurrencies)
    {
        CurrencyAttribute currencyAttribute = (CurrencyAttribute)typeof(ECountry).GetMember(e.ToString()).First().GetCustomAttributes(typeof(CurrencyAttribute), inherit: false)
            .First() ?? default!;
        foreach (ECurrency currency in eCurrencies)
        {
            if (!currencyAttribute.HasCurrencies(currency))
            {
                return false;
            }
        }
        return true;
    }

    public static ECurrency[] GetCurrencies(this ECountry e)
    {
        return ((CurrencyAttribute)typeof(ECountry).GetMember(e.ToString()).First().GetCustomAttributes(typeof(CurrencyAttribute), inherit: false)
            .First()).eCurrencies ?? default!;
    }
}


