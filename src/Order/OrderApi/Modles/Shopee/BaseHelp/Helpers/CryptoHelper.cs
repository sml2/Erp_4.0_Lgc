﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace OrderSDK.Modles.Shopee.BaseHelp.Helpers;

public static class CryptoHelper
{
    public static string Base64Encode(string data)
    {
        try
        {
            _ = new byte[data.Length];
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(data));
        }
        catch (Exception ex)
        {
            throw new Exception("Error in Base64Encode: " + ex.Message);
        }
    }

    public static string Base64Decode(string data)
    {
        try
        {
            Decoder decoder = new UTF8Encoding().GetDecoder();
            byte[] array = Convert.FromBase64String(data);
            char[] array2 = new char[decoder.GetCharCount(array, 0, array.Length)];
            decoder.GetChars(array, 0, array.Length, array2, 0);
            return new string(array2);
        }
        catch (Exception ex)
        {
            throw new Exception("Error in Base64Decode: " + ex.Message);
        }
    }

    public static string SHA512Hash(string text, string secretKey)
    {
        StringBuilder stringBuilder = new StringBuilder();
        byte[] bytes = Encoding.UTF8.GetBytes(secretKey);
        byte[] bytes2 = Encoding.UTF8.GetBytes(text);
        using (HMACSHA512 hMACSHA = new HMACSHA512(bytes))
        {
            byte[] array = hMACSHA.ComputeHash(bytes2);
            foreach (byte b in array)
            {
                stringBuilder.Append(b.ToString("x2"));
            }
        }
        return stringBuilder.ToString();
    }
}
