﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OrderSDK.Modles.Shopee.BaseHelp.Helpers;

public static class PropertyHelper
{
    public static void UpdatePropertyFromName<T>(ref T update_to, in object update_from)
    {
        string[] source = new string[0];
        PropertyInfo[] properties = update_from.GetType().GetProperties();
        PropertyInfo[] properties2 = update_to!.GetType().GetProperties();
        foreach (PropertyInfo to_property in properties2)
        {
            if (!Attribute.IsDefined(to_property, typeof(KeyAttribute)) && !source.Contains(to_property.Name))
            {
                IEnumerable<PropertyInfo> source2 = properties.Where((e) => e.Name == to_property.Name);
                if (source2.Any())
                {
                    PropertyInfo propertyInfo = source2.First();
                    to_property.SetValue(update_to, propertyInfo.GetValue(update_from));
                }
            }
        }
    }

    public static string? GetPropertyValueFromAttribute<T>(object obj)
    {
        PropertyInfo propertyInfo = obj.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public).First((e) => Attribute.IsDefined(e, typeof(T))) ?? null!;
        if (propertyInfo != null && obj is not null)
        {
            return propertyInfo.GetValue(obj)!.ToString();
        }
        return null!;
    }
}


