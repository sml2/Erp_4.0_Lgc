﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OrderSDK.Modles.Shopee.BaseHelp.Helpers;

public static class AttributeHelper
{
    public static T? GetAttribute<T>(object obj) where T : Attribute
    {
        MemberInfo? memberInfo = obj.GetType().GetMember(obj.ToString() ?? default!).FirstOrDefault();
        if (memberInfo != null)
        {
            return /*(T)*/memberInfo.GetCustomAttributes(typeof(T), inherit: false).FirstOrDefault() as T;
        }
        return null;
    }
}
