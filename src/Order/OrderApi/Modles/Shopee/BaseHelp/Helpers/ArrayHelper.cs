﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace OrderSDK.Modles.Shopee.BaseHelp.Helpers;
public static class ArrayHelper
{
    public static List<List<T>> Split<T>(this List<T> array, int count)
    {
        List<List<T>> list = new List<List<T>>();
        if (array.Count() > count)
        {
            list.Add(array.GetRange(0, count));
            list.AddRange(Split(array.GetRange(count, array.Count() - count), count));
        }
        else
        {
            list.Add(array);
        }
        return list;
    }

    public static List<T> Merge<T>(this IEnumerable<IEnumerable<T>> array)
    {
        List<T> list = new List<T>();
        foreach (IEnumerable<T> item in array)
        {
            list.AddRange(item);
        }
        return list;
    }
}
