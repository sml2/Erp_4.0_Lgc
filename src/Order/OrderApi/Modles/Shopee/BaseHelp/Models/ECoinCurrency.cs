﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using OrderSDK.Modles.Shopee.BaseHelp.Attributes;
using OrderSDK.Modles.Shopee.BaseHelp.Enums;

namespace OrderSDK.Modles.Shopee.BaseHelp.Models;

public enum ECoinCurrency
{
    [EnumMember(Value = "BTC")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE,
        ECoinMarket.GOPAX
    })]
    [CoinoneMarketType(ECoinoneMarketType.MAIN)]
    BTC,
    [EnumMember(Value = "ETH")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE,
        ECoinMarket.GOPAX
    })]
    [CoinoneMarketType(ECoinoneMarketType.MAIN)]
    ETH,
    [EnumMember(Value = "XRP")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE,
        ECoinMarket.GOPAX
    })]
    [CoinoneMarketType(ECoinoneMarketType.MAIN)]
    XRP,
    [EnumMember(Value = "EOS")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE,
        ECoinMarket.GOPAX
    })]
    [CoinoneMarketType(ECoinoneMarketType.MAIN)]
    EOS,
    [EnumMember(Value = "QTUM")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE,
        ECoinMarket.GOPAX
    })]
    [CoinoneMarketType(ECoinoneMarketType.MAIN)]
    QTUM,
    [EnumMember(Value = "IOTA")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE
    })]
    [CoinoneMarketType(ECoinoneMarketType.MAIN)]
    IOTA,
    [EnumMember(Value = "KLAY")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE
    })]
    [CoinoneMarketType(ECoinoneMarketType.MAIN)]
    KLAY,
    [EnumMember(Value = "BCH")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE
    })]
    [CoinoneMarketType(ECoinoneMarketType.MAIN)]
    BCH,
    [EnumMember(Value = "ETC")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE
    })]
    [CoinoneMarketType(ECoinoneMarketType.MAIN)]
    ETC,
    [EnumMember(Value = "TRX")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE
    })]
    [CoinoneMarketType(ECoinoneMarketType.MAIN)]
    TRX,
    [EnumMember(Value = "NEO")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE
    })]
    [CoinoneMarketType(ECoinoneMarketType.MAIN)]
    NEO,
    [EnumMember(Value = "XLM")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE
    })]
    [CoinoneMarketType(ECoinoneMarketType.MAIN)]
    XLM,
    [EnumMember(Value = "BSV")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE
    })]
    [CoinoneMarketType(ECoinoneMarketType.MAIN)]
    BSV,
    [EnumMember(Value = "ADA")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE
    })]
    [CoinoneMarketType(ECoinoneMarketType.MAIN)]
    ADA,
    [EnumMember(Value = "FIL")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE
    })]
    [CoinoneMarketType(ECoinoneMarketType.MAIN)]
    FIL,
    [EnumMember(Value = "DOT")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE
    })]
    [CoinoneMarketType(ECoinoneMarketType.MAIN)]
    DOT,
    [EnumMember(Value = "LTC")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE
    })]
    [CoinoneMarketType(ECoinoneMarketType.MAIN)]
    LTC,
    [EnumMember(Value = "LINK")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE
    })]
    [CoinoneMarketType(ECoinoneMarketType.MAIN)]
    LINK,
    [EnumMember(Value = "UNI")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE
    })]
    [CoinoneMarketType(ECoinoneMarketType.MAIN)]
    UNI,
    [EnumMember(Value = "ATOM")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE
    })]
    [CoinoneMarketType(ECoinoneMarketType.MAIN)]
    ATOM,
    [EnumMember(Value = "LUNA")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE
    })]
    [CoinoneMarketType(ECoinoneMarketType.MAIN)]
    LUNA,
    [EnumMember(Value = "XTZ")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE
    })]
    [CoinoneMarketType(ECoinoneMarketType.MAIN)]
    XTZ,
    [EnumMember(Value = "SKLAY")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE
    })]
    [CoinoneMarketType(ECoinoneMarketType.MAIN)]
    SKLAY,
    [EnumMember(Value = "POD")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE
    })]
    [CoinoneMarketType(ECoinoneMarketType.GROWTH)]
    POD,
    [EnumMember(Value = "BNA")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE
    })]
    [CoinoneMarketType(ECoinoneMarketType.GROWTH)]
    BNA,
    [EnumMember(Value = "CTC")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.GOPAX
    })]
    CTC,
    [EnumMember(Value = "KRW")]
    [ProduceCoinMarket(new ECoinMarket[]
    {
        ECoinMarket.COINONE,
        ECoinMarket.GOPAX
    })]
    KRW
}

