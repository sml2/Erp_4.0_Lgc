using System.Collections.Generic;
using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee.Product
{

    public class ResponseGetBrandList
    {
        [JsonProperty("brand_list")]
        public IEnumerable<_Brand> BrandList { get; set; } = Enumerable.Empty<_Brand>();

        [JsonProperty("has_next_page")]
        public bool HasNextPage { get; set; }

        [JsonProperty("next_offset")]
        public int NextOffset { get; set; }

        [JsonProperty("is_mandatory")]
        public bool IsMandatory { get; set; }

        [JsonProperty("input_type")]
        public string InputType { get; set; } = string.Empty;



        public class _Brand
        {

            [JsonProperty("brand_id")]
            public int BrandId { get; set; }

            [JsonProperty("original_brand_name")]
            public string OriginalBrandName { get; set; } = string.Empty;

            [JsonProperty("display_brand_name")]
            public string DisplayBrandName { get; set; } = string.Empty;


        }
    }
}