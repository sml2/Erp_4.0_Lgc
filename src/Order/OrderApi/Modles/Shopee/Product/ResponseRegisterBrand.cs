using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee.Product
{
    public class ResponseRegisterBrand
    {
        [JsonProperty("brand_id")]
        public int BrandId { get; set; }

        [JsonProperty("original_brand_name")]
        public string OriginalBrandName { get; set; } = string.Empty;

    }
}