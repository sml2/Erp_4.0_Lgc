using Newtonsoft.Json;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Product
{
    public class RequestCategoryRecommend : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/category_recommend";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

        [JsonRequired]
        [JsonProperty("item_name")]
        public string ItemName { get; set; } = string.Empty;
    }
}