using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Product
{
    public class RequestUpdateSipItemPrice : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/update_sip_item_price";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonRequired]
        [JsonProperty("item_id")]
        public int ItemId { get; set; }

        [JsonProperty("sip_item_price")]
        public IEnumerable<_sipItemPrice> sipItemPrice { get; set; } = Enumerable.Empty<_sipItemPrice>();

        public class _sipItemPrice
        {
            [JsonProperty("model_id")]
            public int ModelId { get; set; }

            [JsonRequired]
            [JsonProperty("sip_item_price")]
            public float SipItemPrice { get; set; }
        }
    }
}