using Newtonsoft.Json;
using System.Collections.Generic;

namespace OrderSDK.Modles.Shopee.Product
{
    public class ResponseGetItemExtraInfo
    {
        [JsonProperty("item_list")]
        public IEnumerable<_itemList> ItemList { get; set; } = Enumerable.Empty<_itemList>();

        public class _itemList
        {
            [JsonProperty("item_id")]
            public int ItemId { get; set; }

            [JsonProperty("sale")]
            public int Sale { get; set; }

            [JsonProperty("views")]
            public int Views { get; set; }

            [JsonProperty("likes")]
            public int Likes { get; set; }

            [JsonProperty("rating_starikes")]
            public float Rating_star { get; set; }

            [JsonProperty("comment_count")]
            public int Comment_count { get; set; }

        }
    }
}