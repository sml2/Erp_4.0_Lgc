using Newtonsoft.Json;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Product
{
    public class RequestBoostItem : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/boost_item";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        /// <summary>
        /// 产品id，长度在1~5之间
        /// </summary>
        [JsonRequired]
        [JsonProperty("item_id_list")]
        public int[] ItemIdList { get; set; } = Array.Empty<int>();
    }
}