using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee.Product
{

    public class RequestRegisterBrand : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/register_brand";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        /// <summary>
        /// 品牌名称，长度小于等于254
        /// </summary>
        [JsonRequired]
        [JsonProperty("original_brand_name")]
        [MaxLength(254)]
        public string OriginalBrandName { get; set; } = string.Empty;

        /// <summary>
        /// 品牌下的分类id，最多可放50个
        /// </summary>
        [JsonRequired]
        [JsonProperty("category_list ")]
        public long[] CategoryList { get; set; } = Array.Empty<long>();

        /// <summary>
        /// 手机端的logo图标id，请输入图片的哈希代码
        /// </summary>
        [JsonProperty("app_logo_image_id")]
        public string AppLogoImageId { get; set; } = string.Empty;


        /// <summary>
        /// 品牌官方网站，长度<=254
        /// </summary>
        [MaxLength(254)]
        [JsonProperty("brand_website")]
        public string BrandWebsite { get; set; } = string.Empty;

        /// <summary>
        /// 品牌描述，长度<=254
        /// </summary>
        [MaxLength(254)]
        [JsonProperty("brand_description")]
        public string BrandDescription { get; set; } = string.Empty;

        /// <summary>
        /// 卖方可添加的注释，长度<=254
        /// </summary>
        [MaxLength(254)]
        [JsonProperty("additional_information")]
        public string AdditionalInformation { get; set; } = string.Empty;

        /// <summary>
        /// PC端的logo图标id，请输入图片的哈希代码
        /// </summary>
        [JsonProperty("pc_logo_image_id")]
        public string PcLogoImageId { get; set; } = string.Empty;

        /// <summary>
        /// 品牌原产国
        /// </summary>
        [JsonRequired]
        [JsonProperty("brand_country ")]
        public string BrandCountry { get; set; } = string.Empty;

        /// <summary>
        /// 这个品牌的产品图片id，最多10个，每个文件长度<=498
        /// </summary>
        [JsonProperty("product_image")]
        public _ProductImage ProductImage { get; set; } = new();


        public class _ProductImage
        {
            /// <summary>
            /// 这个品牌的产品图片id，最多10个，每个文件长度<=498
            /// </summary>
            [JsonProperty("image_id_list")]
            public string[] ImageIdList { get; set; } = Array.Empty<string>();
        }
    }
}