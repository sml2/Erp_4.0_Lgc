﻿using Newtonsoft.Json;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Product
{
    public class RequestGetModelList : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/get_model_list";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

        [JsonRequired]
        [JsonProperty("item_id")]
        public long ItemId { get; set; }
    }
}
