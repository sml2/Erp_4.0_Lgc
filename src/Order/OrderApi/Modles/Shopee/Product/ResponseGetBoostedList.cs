using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee.Product
{
    public class ResponseGetBoostedList
    {
        [JsonProperty("item_list")]
        public _item ItemList { get; set; } = new();


        public class _item
        {
            [JsonProperty("item_id")]
            public int ItemId { get; set; }

            [JsonProperty("cool_down_second")]
            public int CoolDownSecond { get; set; }
        }
    }
}