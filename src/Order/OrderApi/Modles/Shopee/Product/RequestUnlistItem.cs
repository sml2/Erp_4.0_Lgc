﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Product
{
    public class RequestUnlistItem : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/unlist_item";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonRequired]
        [JsonProperty("item_list")]
        public IEnumerable<_Item> ItemList { get; set; } = Enumerable.Empty<_Item>();

        public class _Item
        {
            [JsonRequired]
            [JsonProperty("item_id")]
            public int ItemId { get; set; }

            [JsonRequired]
            [JsonProperty("unlist")]
            public bool UnList { get; set; }
        }
    }
}
