using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee.Product
{
    public class RequestDeleteModel : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/delete_model";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [Required]
        [JsonProperty("item_id")]
        public int ItemId { get; set; }

        [Required]
        [JsonProperty("model_id ")]
        public int ModelId { get; set; }
    }
}