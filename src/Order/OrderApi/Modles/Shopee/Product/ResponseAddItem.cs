using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using OrderSDK.Modles.Shopee.Enums;

namespace OrderSDK.Modles.Shopee.Product
{
    public class ResponseAddItem
    {
        [JsonProperty("description")]
        public string Description { get; set; } = string.Empty;

        [JsonProperty("weight")]
        public int Weight { get; set; }

        [JsonProperty("pre_order")]
        public _PreOrder PreOrder { get; set; } = new();

        [JsonProperty("item_name")]
        public string ItemName { get; set; } = string.Empty;

        [JsonProperty("images")]
        public _Images Images { get; set; } = new();

        [JsonProperty("item_status")]
        [EnumDataType(typeof(EShopeeItemStatus))]
        [JsonConverter(typeof(StringEnumConverter))]
        public EShopeeItemStatus ItemStatus { get; set; }

        [JsonProperty("price_info")]
        public _PriceInfo PriceInfo { get; set; } = new();

        [JsonProperty("logistic_info")]
        public List<_LogisticInfo> LogisticInfo { get; set; } = new();

        [JsonProperty("stock_info")]
        public _StockInfo StockInfo { get; set; } = new();

        [JsonProperty("item_id")]
        public long ItemId { get; set; }

        [JsonProperty("attributes")]
        public List<_Attribute> Attributes { get; set; } = new();

        [JsonProperty("category_id")]
        public long CategoryId { get; set; }

        [JsonProperty("dimension")]
        public _Dimension Dimension { get; set; } = new();

        [JsonProperty("condition")]
        public string Condition { get; set; } = string.Empty;

        [JsonProperty("video_info")]
        public _VideoInfo VideoInfo { get; set; } = new();

        [JsonProperty("wholesale")]
        public _WholeSale WholeSale { get; set; } = new();

        [JsonProperty("brand")]
        public _Brand Brand { get; set; } = new();



        public class _PreOrder
        {
            [JsonProperty("days_to_ship")]
            public int DaysToShip { get; set; }

            [JsonProperty("is_pre_order")]
            public bool IsPreOrder { get; set; }
        }

        public class _Images
        {
            [JsonProperty("image_id_list")]
            public IEnumerable<string> ImageIdList { get; set; } = Enumerable.Empty<string>();

            [JsonProperty("image_url_list")]
            public IEnumerable<string> ImageUrlList { get; set; } = Enumerable.Empty<string>();
        }

        public class _PriceInfo
        {
            [JsonProperty("current_price")]
            public double CurrentPrice { get; set; }

            [JsonProperty("original_price")]
            public double OriginalPrice { get; set; }
        }

        public class _LogisticInfo
        {
            [JsonProperty("size_id")]
            public long SizeId { get; set; }

            [JsonProperty("shipping_fee")]
            public double ShippingFee { get; set; }

            [JsonProperty("enabled")]
            public bool Enabled { get; set; }

            [JsonProperty("logistic_id")]
            public long LogisticId { get; set; }

            [JsonProperty("is_free")]
            public bool IsFree { get; set; }
        }

        public class _StockInfo
        {
            [JsonProperty("normal_stock")]
            public int NormalStock { get; set; }

            [JsonProperty("stock_type")]
            public int StockType { get; set; }

            [JsonProperty("current_stock")]
            public int CurrentStock { get; set; }
        }

        public class _Attribute
        {
            [JsonProperty("attribute_id")]
            public long AttributeId { get; set; }

            [JsonProperty("attribute_value_list")]
            public IEnumerable<_AttributeValue> AttributeValueList { get; set; } = Enumerable.Empty<_AttributeValue>();
        }

        public class _AttributeValue
        {
            [JsonProperty("original_value_name")]
            public string OriginalValueName { get; set; } = string.Empty;

            [JsonProperty("value_id")]
            public long ValueId { get; set; }

            [JsonProperty("value_unit")]
            public string ValueUnit { get; set; } = string.Empty;
        }

        public class _Dimension
        {
            [JsonProperty("package_width")]
            public int PackageWidth { get; set; }

            [JsonProperty("package_length")]
            public int PackageLength { get; set; }

            [JsonProperty("package_height")]
            public int PackageHeight { get; set; }
        }

        public class _VideoInfo
        {
            [JsonProperty("video_url")]
            public string VideoUrl { get; set; } = string.Empty;

            [JsonProperty("thumbnail_url")]
            public string ThumbnailUrl { get; set; } = string.Empty;

            [JsonProperty("duration")]
            public int Duration { get; set; }
        }

        public class _WholeSale
        {
            [JsonProperty("min_count")]
            public int MinCount { get; set; }

            [JsonProperty("max_count")]
            public int MaxCount { get; set; }

            [JsonProperty("unit_price")]
            public double UnitPrice { get; set; }
        }

        public class _Brand
        {
            [JsonProperty("brand_id")]
            public long BrandId { get; set; }

            [JsonProperty("original_brand_name")]
            public string OriginalBrandName { get; set; } = string.Empty;
        }
    }

}