using Newtonsoft.Json;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Product
{
    public class RequestDeleteItem : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/delete_item";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonRequired]
        [JsonProperty("item_id ")]
        public int ItemId { get; set; }
    }
}