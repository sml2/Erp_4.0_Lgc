﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Product
{
    public class RequestUpdateModel : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/update_model";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonRequired]
        [JsonProperty("item_id")]
        public int ItemId { get; set; }

        /// <summary>
        /// 长度：[1,50]
        /// </summary>
        [JsonRequired]
        [JsonProperty("model")]
        public IEnumerable<_Model> Model { get; set; } = Enumerable.Empty<_Model>();


        public class _Model
        {
            [JsonRequired]
            [JsonProperty("model_id")]
            public long ModelId { get; set; }

            [JsonRequired]
            [JsonProperty("model_sku")]
            public string ModelSku { get; set; } = string.Empty;

            [JsonProperty("pre_order")]
            public _PreOrder _PreOrder { get; set; } = new();
        }

        public class _PreOrder
        {
            [JsonRequired]
            [JsonProperty("is_pre_order")]
            public bool isPreOrder { get; set; }

            [JsonRequired]
            [JsonProperty("days_to_ship")]
            public int daysToShip { get; set; }
        }
    }
}
