using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Product
{
    public class RequestInitTierVariation : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/init_tier_variation";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonRequired]
        [JsonProperty("item_id")]
        public int ItemId { get; set; }

        [JsonRequired]
        [JsonProperty("tier_variation")]
        public IEnumerable<_Variation> TierVariation { get; set; } = Enumerable.Empty<_Variation>();

        /// <summary>
        /// 长度1~50之间
        /// </summary>
        [JsonRequired]
        [JsonProperty("model")]
        public IEnumerable<_Model> Model { get; set; } = Enumerable.Empty<_Model>();

        public class _Variation
        {
            [JsonRequired]
            [JsonProperty("name")]
            public string Name { get; set; } = string.Empty;

            [JsonRequired]
            [JsonProperty("option_list")]
            public IEnumerable<_Option> OptionList { get; set; } = Enumerable.Empty<_Option>();


            public class _Option
            {
                [JsonRequired]
                [JsonProperty("option")]
                public string Option { get; set; } = string.Empty;

                [JsonProperty("image")]
                public _Image Image { get; set; } = new();

                public class _Image
                {
                    [JsonRequired]
                    [JsonProperty("image_id")]
                    public string ImageId { get; set; } = string.Empty;
                }
            }

        }

        public class _Model
        {
            [JsonRequired]
            [JsonProperty("tier_index")]
            public int[] TierIndex { get; set; } = Array.Empty<int>();

            [JsonRequired]
            [JsonProperty("normal_stock")]
            public int NormalStock { get; set; }

            [JsonRequired]
            [JsonProperty("original_price")]
            public float OriginalPrice { get; set; }

            [JsonProperty("model_sku")]
            public string ModelSku { get; set; } = string.Empty;
        }
    }

}