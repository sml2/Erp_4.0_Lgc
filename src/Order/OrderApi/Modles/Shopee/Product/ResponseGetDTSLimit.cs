using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee.Product
{

    public class ResponseGetDTSLimit
    {
        [JsonProperty("days_to_ship_limit")]
        public _Limit DaysToShipLimit { get; set; } = new();

        [JsonProperty("non_pre_order_days_to_ship")]
        public int NonPreOrderDaysToShip { get; set; }

        public class _Limit
        {
            [JsonProperty("min_limit")]
            public int MinLimit { get; set; }

            [JsonProperty("max_limit")]
            public int MaxLimit { get; set; }
        }
    }
}