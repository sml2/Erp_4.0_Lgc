﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using OrderSDK.Modles.Shopee.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OrderSDK.Modles.Shopee.Product
{
    public class ResponseGetItemBaseInfo
    {
        public ResponseGetItemBaseInfo()
        {
            ItemList = new List<_Item>();
        }

        [JsonProperty("item_list")]
        public IEnumerable<_Item> ItemList { get; set; }

        public class _Item
        {
            [JsonProperty("item_id")]
            public long ItemId { get; set; }

            [JsonProperty("category_id")]
            public long CategoryId { get; set; }

            [JsonProperty("item_name")]
            public string ItemName { get; set; } = string.Empty;

            [JsonProperty("description")]
            public string Description { get; set; } = string.Empty;

            [JsonProperty("item_sku")]
            public string ItemSku { get; set; } = string.Empty;

            [JsonProperty("create_time")]
            public long CreateTime { get; set; }

            [JsonProperty("update_time")]
            public long UpdateTime { get; set; }

            [JsonProperty("attribute_list")]
            public IEnumerable<_Attribute> AttributeList { get; set; } = Enumerable.Empty<_Attribute>();

            [JsonProperty("price_info")]
            public IEnumerable<_PriceInfo> PriceInfo { get; set; } = Enumerable.Empty<_PriceInfo>();

            [JsonProperty("stock_info")]
            public IEnumerable<_StockInfo> StockInfo { get; set; } = Enumerable.Empty<_StockInfo>();

            [JsonProperty("image")]
            public _Image Image { get; set; } = new();

            [JsonProperty("weight")]
            public double Weight { get; set; }

            [JsonProperty("dimension")]
            public _Dimension Dimension { get; set; } = new();

            [JsonProperty("logistic_info")]
            public IEnumerable<_LogisticInfo> LogisticInfo { get; set; } = Enumerable.Empty<_LogisticInfo>();

            [JsonProperty("pre_order")]
            public _PreOrder PreOrder { get; set; } = new();

            [JsonProperty("wholesales")]
            public IEnumerable<_Wholesale> Wholesale { get; set; } = Enumerable.Empty<_Wholesale>();

            [JsonProperty("condition")]
            public string Condition { get; set; } = string.Empty;

            [JsonProperty("size_chart")]
            public string SizeChart { get; set; } = string.Empty;

            [JsonProperty("item_status")]
            [EnumDataType(typeof(EShopeeItemStatus))]
            [JsonConverter(typeof(StringEnumConverter))]
            public EShopeeItemStatus ItemStatus { get; set; }

            [JsonProperty("has_model")]
            public bool HasModel { get; set; }

            [JsonProperty("promotion_id")]
            public long PromotionId { get; set; }

            [JsonProperty("brand")]
            public _Brand Brand { get; set; } = new();

            [JsonProperty("item_dangerous")]
            public int ItemDangerous { get; set; }



            public IEnumerable<ResponseGetModelList._Model> Models { get; set; } = Enumerable.Empty<ResponseGetModelList._Model>();

            public IEnumerable<ResponseGetModelList._TierVariation> TierVariation { get; set; } = Enumerable.Empty<ResponseGetModelList._TierVariation>();

            public class _Attribute
            {
                [JsonProperty("attribute_id")]
                public long AttributeId { get; set; }

                [JsonProperty("original_attribute_name")]
                public string OriginalAttributeName { get; set; } = string.Empty;

                [JsonProperty("is_mandatory")]
                public bool IsMandatory { get; set; }

                [JsonProperty("attribute_value_list")]
                public IEnumerable<_AttributeValue> AttributeValueList { get; set; } = Enumerable.Empty<_AttributeValue>();

                public class _AttributeValue
                {
                    [JsonProperty("value_id")]
                    public long ValueId { get; set; }

                    [JsonProperty("original_value_name")]
                    public string OriginalValueName { get; set; } = string.Empty;

                    [JsonProperty("value_unit")]
                    public string ValueUnit { get; set; } = string.Empty;
                }
            }


            public class _Brand
            {
                [JsonProperty("brand_id")]
                public long BrandId { get; set; }

                [JsonProperty("original_brand_name")]
                public string OriginalBrandName { get; set; } = string.Empty;
            }

            public class _Dimension
            {
                [JsonProperty("package_length")]
                public int PackageLength { get; set; }

                [JsonProperty("package_width")]
                public int PackageWidth { get; set; }

                [JsonProperty("package_height")]
                public int PackageHeight { get; set; }
            }

            public class _Image
            {
                [JsonProperty("image_url_list")]
                public IEnumerable<string> ImageUrlList { get; set; } = Enumerable.Empty<string>();

                [JsonProperty("image_id_list")]
                public IEnumerable<string> ImageIdList { get; set; } = Enumerable.Empty<string>();
            }

            public class _LogisticInfo
            {
                [JsonProperty("logistic_id")]
                public long LogisticId { get; set; }

                [JsonProperty("logistic_name")]
                public string LogisticName { get; set; } = string.Empty;

                [JsonProperty("enabled")]
                public bool Enabled { get; set; }

                [JsonProperty("shipping_fee")]
                public double ShippingFee { get; set; }

                [JsonProperty("size_id")]
                public long SizeId { get; set; }

                [JsonProperty("is_free")]
                public bool IsFree { get; set; }

                [JsonProperty("estimated_shipping_fee", NullValueHandling = NullValueHandling.Ignore)]
                public double? EstimatedShippingFee { get; set; }


            }

            public class _PreOrder
            {
                [JsonProperty("is_pre_order")]
                public bool IsPreOrder { get; set; }

                [JsonProperty("days_to_ship")]
                public int DaysToShip { get; set; }
            }

            public class _Wholesale
            {
                [JsonProperty("min_count")]
                public int MinCount { get; set; }

                [JsonProperty("max_count")]
                public int MaxCount { get; set; }

                [JsonProperty("unit_price")]
                public double UnitPrice { get; set; }

                [JsonProperty("inflated_price_of_unit_price")]
                public double InflatedPriceOfUnitPrice { get; set; }
            }

            public class _PriceInfo
            {
                [JsonProperty("currency")]
                public string Currency { get; set; } = string.Empty;

                [JsonProperty("original_price")]
                public double OriginalPrice { get; set; }

                [JsonProperty("current_price")]
                public double CurrentPrice { get; set; }

                [JsonProperty("inflated_price_of_original_price")]
                public double InflatedPriceOfOriginalPrice { get; set; }

                [JsonProperty("inflated_price_of_current_price")]
                public double InflatedPriceOfCurrentPrice { get; set; }

                [JsonProperty("sip_item_price")]
                public double SipItemPrice { get; set; }

                [JsonProperty("sip_item_price_source")]
                public string SipItemPriceSource { get; set; } = string.Empty;
            }

            public class _StockInfo
            {
                [JsonProperty("stock_type")]
                public int StockType { get; set; }

                [JsonProperty("stock_location_id")]
                public string StockLocationId { get; set; } = string.Empty;

                [JsonProperty("current_stock")]
                public int CurrentStock { get; set; }

                [JsonProperty("normal_stock")]
                public int NormalStock { get; set; }

                [JsonProperty("reserved_stock")]
                public int ReservedStock { get; set; }
            }
        }

    }
}
