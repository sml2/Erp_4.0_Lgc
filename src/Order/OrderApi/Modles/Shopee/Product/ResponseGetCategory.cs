using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee.Product
{

    public class ResponseGetCategory
    {
        [JsonProperty("category_list")]
        public IEnumerable<_Category> CategoryList { get; set; } = Enumerable.Empty<_Category>();
    }
    public class _Category
    {
        [JsonProperty("category_id")]
        public long? CategoryId { get; set; }

        [JsonProperty("parent_category_id")]
        public long? ParentCategoryId { get; set; }

        [JsonProperty("original_category_name"), Required]
        public string OriginalCategoryName { get; set; } = string.Empty;
        [JsonProperty("display_category_name"), Required]
        public string DisplayCategoryName { get; set; } = string.Empty;

        [JsonProperty("has_children")]
        public bool HasChildren { get; set; }

    }
}