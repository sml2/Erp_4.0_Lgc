﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using OrderSDK.Modles.Shopee.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OrderSDK.Modles.Shopee.Product
{
    public class ResponseGetItemList
    {
        public ResponseGetItemList()
        {
            Item = new List<_Item>();
        }

        [JsonProperty("item")]
        public IEnumerable<_Item> Item { get; set; }

        [JsonProperty("total_count")]
        public int TotalCount { get; set; }

        [JsonProperty("has_next_page")]
        public bool HasNextPage { get; set; }

        [JsonProperty("next_offset")]
        public int NextOffset { get; set; }


        public class _Item
        {
            [JsonProperty("item_id")]
            public int ItemId { get; set; }

            [JsonProperty("item_status")]
            [EnumDataType(typeof(EShopeeItemStatus))]
            [JsonConverter(typeof(StringEnumConverter))]
            public EShopeeItemStatus ItemStatus { get; set; }

            [JsonProperty("update_time")]
            public long UpdateTime { get; set; }
        }
    }
}
