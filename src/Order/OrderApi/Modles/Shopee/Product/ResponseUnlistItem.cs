﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrderSDK.Modles.Shopee.Product
{
    public class ResponseUnlistItem
    {
        [JsonProperty("failure_list")]
        public IEnumerable<_Failure> FailureList { get; set; } = Enumerable.Empty<_Failure>();

        [JsonProperty("success_list")]
        public IEnumerable<_Succes> SuccessList { get; set; } = Enumerable.Empty<_Succes>();



        public class _Failure
        {
            [JsonProperty("item_id")]
            public long ItemId { get; set; }

            [JsonProperty("failed_reason")]
            public string FailedReason { get; set; } = string.Empty;
        }

        public class _Succes
        {
            [JsonProperty("item_id")]
            public long ItemId { get; set; }

            [JsonProperty("unlist")]
            public bool Unlist { get; set; }
        }
    }
}
