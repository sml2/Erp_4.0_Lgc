﻿using Newtonsoft.Json;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Product
{
    public class RequestGetItemBaseInfo : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/get_item_base_info";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

        /// <summary>
        /// int数组内最多50个
        /// </summary>
        [JsonRequired]
        [JsonProperty("item_id_list")]
        public int[] ItemIdList { get; set; } = Array.Empty<int>();

        [JsonProperty("need_tax_info")]
        public bool needTaxInfo { get; set; }

        [JsonProperty("need_complaint_policy")]
        public bool needComplaintPolicy { get; set; }

    }
}
