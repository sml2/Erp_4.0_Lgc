using System.Collections.Generic;
using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee.Product
{

    public class ResponseInitTierVariation
    {
        [JsonProperty("item_id")]
        public long ItemId { get; set; }

        [JsonProperty("tier_variation")]
        public IEnumerable<_TierVariation> TierVariation { get; set; } = Enumerable.Empty<_TierVariation>();

        [JsonProperty("model")]
        public IEnumerable<_Model> Model { get; set; } = Enumerable.Empty<_Model>();


        public class _TierVariation
        {
            [JsonProperty("name")]
            public string Name { get; set; } = string.Empty;

            [JsonProperty("option_list")]
            public IEnumerable<_Option> OptionList { get; set; } = Enumerable.Empty<_Option>();


            public class _Option
            {
                [JsonProperty("image")]
                public _Image Image { get; set; } = new();

                [JsonProperty("option")]
                public string Option { get; set; } = string.Empty;

                public class _Image
                {
                    [JsonProperty("image_url")]
                    public string ImageUrl { get; set; } = string.Empty;
                }
            }

        }


        public class _Model
        {
            [JsonProperty("tier_index")]
            public List<int> TierIndex { get; set; } = new();

            [JsonProperty("model_id")]
            public long ModelId { get; set; }

            [JsonProperty("model_sku")]
            public string ModelSku { get; set; } = string.Empty;

            [JsonProperty("stock_info")]
            public IEnumerable<_StockInfo> StockInfo { get; set; } = Enumerable.Empty<_StockInfo>();

            [JsonProperty("price_info")]
            public IEnumerable<_PriceInfo> PriceInfo { get; set; } = Enumerable.Empty<_PriceInfo>();


            public class _StockInfo
            {
                [JsonProperty("stock_type")]
                public int StockType { get; set; }

                [JsonProperty("normal_stock")]
                public int NormalStock { get; set; }
            }

            public class _PriceInfo
            {
                [JsonProperty("original_price")]
                public double OriginalPrice { get; set; }
            }
        }

    }
}