﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Product
{
    public class RequestUpdatePrice : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/update_price";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonRequired]
        [JsonProperty("item_id")]
        public int ItemId { get; set; }

        /// <summary>
        /// 长度：[1,50]
        /// </summary>
        [JsonRequired]
        [JsonProperty("price_list")]
        public IEnumerable<_Price> PriceList { get; set; } = Enumerable.Empty<_Price>();

        public class _Price
        {
            [JsonProperty("model_id")]
            public long ModelId { get; set; }

            [JsonRequired]
            [JsonProperty("original_price")]
            public double OriginalPrice { get; set; }
        }
    }
}
