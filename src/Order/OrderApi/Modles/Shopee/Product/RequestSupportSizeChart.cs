using Newtonsoft.Json;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Product
{
    public class RequestSupportSizeChart : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/support_size_chart";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

        [JsonRequired]
        [JsonProperty("category_id")]
        public long? CategoryId { get; set; }

    }

}