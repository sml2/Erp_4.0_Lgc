using Newtonsoft.Json;
using System.Collections.Generic;

namespace OrderSDK.Modles.Shopee.Product
{
    public class ResponseReplyComment
    {
        [JsonProperty("result_list")]
        public IEnumerable<_result> ResultList { get; set; } = Enumerable.Empty<_result>();

        public class _result
        {
            [JsonProperty("comment_id")]
            public int CommentId { get; set; }

            [JsonProperty("fail_error")]
            public string FailError { get; set; } = string.Empty;

            [JsonProperty("fail_message")]
            public string FailMessage { get; set; } = string.Empty;
        }
    }
}