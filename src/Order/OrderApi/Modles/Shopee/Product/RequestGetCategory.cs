using Newtonsoft.Json;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Product
{
    public class RequestGetCategory : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/get_category";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

        [JsonProperty("language")]
        public string Language { get; set; } = string.Empty;
    }
}