using Newtonsoft.Json;
using System.Net.Http;
namespace OrderSDK.Modles.Shopee.Product
{

    public class RequestGetItemPromotion : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/get_item_promotion";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

        /// <summary>
        /// 数组最长不超过50
        /// </summary>
        [JsonRequired]
        [JsonProperty("item_id_list ")]
        public int[] ItemIdList { get; set; } = Array.Empty<int>();

    }
}