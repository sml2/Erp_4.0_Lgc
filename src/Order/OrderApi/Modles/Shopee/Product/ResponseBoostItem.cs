using Newtonsoft.Json;
using System.Collections.Generic;

namespace OrderSDK.Modles.Shopee.Product
{
    public class ResponseBoostItem
    {
        [JsonProperty("failure_list")]
        public IEnumerable<_failure_list> FailureList { get; set; } = Enumerable.Empty<_failure_list>();

        [JsonProperty("success_list")]
        public _successList SuccessList { get; set; } = new();


        public class _failure_list
        {
            [JsonProperty("item_id")]
            public int ItemId { get; set; }
            [JsonProperty("failed_reason")]
            public string FailedReason { get; set; } = string.Empty;
        }

        public class _successList
        {
            [JsonProperty("item_id_list")]
            public int[] ItemIdList { get; set; } = Array.Empty<int>();
        }
    }
}