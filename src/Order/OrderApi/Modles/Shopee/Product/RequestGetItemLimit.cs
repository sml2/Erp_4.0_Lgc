using Newtonsoft.Json;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Product
{

    public class RequestGetItemLimit : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/get_item_limit";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

    }
}