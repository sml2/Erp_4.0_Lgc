using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using OrderSDK.Modles.Shopee.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace OrderSDK.Modles.Shopee.Product
{
    public class ResponseUpdateItem
    {
        [JsonRequired]
        [JsonProperty("description")]
        public string Description { get; set; } = string.Empty;

        [JsonRequired]
        [JsonProperty("weight")]
        public int Weight { get; set; }

        [JsonRequired]
        [JsonProperty("pre_order")]
        public _PreOrder PreOrder { get; set; } = new();

        [JsonRequired]
        [JsonProperty("item_name")]
        public string ItemName { get; set; } = string.Empty;

        [JsonRequired]
        [JsonProperty("item_status")]
        [EnumDataType(typeof(EShopeeItemStatus))]
        [JsonConverter(typeof(StringEnumConverter))]
        public EShopeeItemStatus ItemStatus { get; set; }

        [JsonRequired]
        [JsonProperty("images")]
        public _Images Images { get; set; } = new();

        [JsonRequired]
        [JsonProperty("logistic_info")]
        public List<_LogisticInfo> LogisticInfo { get; set; } = new();

        [JsonRequired]
        [JsonProperty("item_id")]
        public long ItemId { get; set; }

        [JsonRequired]
        [JsonProperty("category_id")]
        public long CategoryId { get; set; }

        [JsonRequired]
        [JsonProperty("dimension")]
        public _Dimension Dimension { get; set; } = new();

        [JsonRequired]
        [JsonProperty("condition")]
        public string Condition { get; set; } = string.Empty;

        [JsonProperty("brand")]
        public _Brand Brand { get; set; } = new();


        [JsonProperty("item_dangerous")]
        public int ItemDangerous { get; set; }

        [JsonProperty("tax_info")]
        public _taxInfo TaxInfo { get; set; } = new();

        [JsonProperty("complaint_policy")]
        public _complaint_policy ComplaintPolicy { get; set; } = new();

        public class _PreOrder
        {
            [JsonProperty("days_to_ship")]
            public int DaysToShip { get; set; }

            [JsonProperty("is_pre_order")]
            public bool IsPreOrder { get; set; }
        }

        public class _Images
        {
            [JsonProperty("image_id_list")]
            public IEnumerable<string> ImageIdList { get; set; } = Array.Empty<string>();

            [JsonProperty("image_url_list")]
            public IEnumerable<string> ImageUrlList { get; set; } = Array.Empty<string>();
        }

        public class _LogisticInfo
        {
            [JsonProperty("estimated_shipping_fee")]
            public float EstimatedShippingFee { get; set; }

            [JsonProperty("logistic_name")]
            public string LogisticName { get; set; } = string.Empty;

            [JsonProperty("enabled")]
            public bool Enabled { get; set; }

            [JsonProperty("logistic_id")]
            public long LogisticId { get; set; }

            [JsonProperty("is_free")]
            public bool IsFree { get; set; }
        }

        public class _Dimension
        {
            [JsonProperty("package_width")]
            public int PackageWidth { get; set; }

            [JsonProperty("package_length")]
            public int PackageLength { get; set; }

            [JsonProperty("package_height")]
            public int PackageHeight { get; set; }
        }

        public class _Brand
        {
            [JsonProperty("brand_id")]
            public long BrandId { get; set; }

            [JsonProperty("original_brand_name")]
            public string OriginalBrandName { get; set; } = string.Empty;
        }

        public class _taxInfo
        {
            [JsonProperty("invoice_option")]
            public string InvoiceOption { get; set; } = string.Empty;

            [JsonProperty("vat_rate")]
            public string VatRate { get; set; } = string.Empty;

            [JsonProperty("hs_code")]
            public string HsCode { get; set; } = string.Empty;

            [JsonProperty("tax_code")]
            public string TaxCode { get; set; } = string.Empty;
        }

        public class _complaint_policy
        {
            [JsonProperty("warranty_time")]
            public string WarrantyTime { get; set; } = string.Empty;

            [JsonProperty("exclude_entrepreneur_warranty")]
            public string ExcludeEntrepreneurWarranty { get; set; } = string.Empty;

            [JsonProperty("complaint_address_id")]
            public int ComplaintAddressId { get; set; }

            [JsonProperty("additional_information")]
            public string AdditionalInformation { get; set; } = string.Empty;
        }
    }

}