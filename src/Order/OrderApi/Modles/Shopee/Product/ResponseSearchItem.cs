﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrderSDK.Modles.Shopee.Product
{
    public class ResponseSearchItem
    {
        [JsonProperty("item_id_list")]
        public IEnumerable<int> ItemIdList { get; set; } = Enumerable.Empty<int>();

        [JsonProperty("total_count")]
        public int TotalCount { get; set; }

        [JsonProperty("next_offset")]
        public string NextOffset { get; set; } = string.Empty;
    }
}
