using Newtonsoft.Json;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Product
{

    public class RequestUpdateSizeChart : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/update_size_chart";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonRequired]
        [JsonProperty("item_id")]
        public int itemId { get; set; }

        [JsonRequired]
        [JsonProperty("size_chart")]
        public string sizeChart { get; set; } = string.Empty;

    }
}