﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Product
{
    public class RequestAddModel : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/add_model";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonRequired]
        [JsonProperty("item_id")]
        public long ItemId { get; set; }

        [JsonRequired]
        [JsonProperty("model_list")]
        public IEnumerable<_Model> ModelList { get; set; } = Enumerable.Empty<_Model>();

        public class _Model
        {
            [JsonRequired]
            [JsonProperty("tier_index")]
            public int[] TierIndex { get; set; } = Array.Empty<int>();

            [JsonRequired]
            [JsonProperty("normal_stock")]
            public int NormalStock { get; set; }

            [JsonRequired]
            [JsonProperty("original_price")]
            public float OriginalPrice { get; set; }

            [JsonProperty("model_sku")]
            public string ModelSku { get; set; } = string.Empty;
        }
    }
}
