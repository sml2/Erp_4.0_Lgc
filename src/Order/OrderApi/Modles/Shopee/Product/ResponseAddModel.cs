﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrderSDK.Modles.Shopee.Product
{
    public class ResponseAddModel
    {
        [JsonProperty("model")]
        public List<_Model> Model { get; set; } = new();

        public class _Model
        {
            [JsonProperty("tier_index")]
            public List<int> TierIndex { get; set; } = new();

            [JsonProperty("model_id")]
            public long ModelId { get; set; }

            [JsonProperty("model_sku")]
            public string ModelSku { get; set; } = string.Empty;

            [JsonProperty("stock_info")]
            public IEnumerable<_StockInfo> StockInfo { get; set; } = Enumerable.Empty<_StockInfo>();

            [JsonProperty("price_info")]
            public IEnumerable<_PriceInfo> PriceInfo { get; set; } = Enumerable.Empty<_PriceInfo>();


            public class _StockInfo
            {
                [JsonProperty("stock_type")]
                public long StockType { get; set; }

                [JsonProperty("normal_stock")]
                public long NormalStock { get; set; }
            }

            public class _PriceInfo
            {
                [JsonProperty("original_price")]
                public double OiginalPrice { get; set; }
            }
        }

    }
}
