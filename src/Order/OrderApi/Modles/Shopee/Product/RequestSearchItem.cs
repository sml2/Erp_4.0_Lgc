﻿using Newtonsoft.Json;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Product
{
    public class RequestSearchItem : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/search_item";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

        [JsonProperty("offset")]
        public string Offset { get; set; } = string.Empty;

        [JsonRequired]
        [JsonProperty("page_size")]
        public int PageSize { get; set; }

        [JsonProperty("item_name")]
        public string ItemName { get; set; } = string.Empty;

        /// <summary>
        /// 1:获取缺少requires属性的项。2:获取缺少可选属性的项。
        /// </summary>
        [JsonProperty("attribute_status")]
        public int AttributeStatus { get; set; }
    }
}
