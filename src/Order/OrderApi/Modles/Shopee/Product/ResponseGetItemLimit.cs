using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee.Product
{
    public class ResponseGetItemLimit
    {
        [JsonProperty("price_limit")]
        public _priceLimit PriceLimit { get; set; } = new();

        [JsonProperty("wholesale_price_threshold_percentage")]
        public _wholesalePrice WholesalePrice { get; set; } = new();

        [JsonProperty("stock_limit")]
        public _stockLimit StockLimit { get; set; } = new();

        [JsonProperty("item_name_length_limit")]
        public _itemName ItemNameLength { get; set; } = new();

        [JsonProperty("item_image_count_limit")]
        public _itemImage ItemImageCount { get; set; } = new();

        [JsonProperty("item_description_length_limit")]
        public _itemDescription ItemDescription { get; set; } = new();

        [JsonProperty("tier_variation_name_length_limit")]
        public _tierVariationName TierVariationName { get; set; } = new();

        [JsonProperty("tier_variation_option_length_limit")]
        public _tierVariationOption TierVariationOption { get; set; } = new();

        [JsonProperty("item_count_limit")]
        public _itemCount ItemCountLimit { get; set; } = new();


        public class _priceLimit
        {
            [JsonProperty("min_limit")]
            public float min_limit { get; set; }
            [JsonProperty("max_limit")]
            public float max_limit { get; set; }
        }

        public class _wholesalePrice
        {
            [JsonProperty("min_limit")]
            public int min_limit { get; set; }
            [JsonProperty("max_limit")]
            public int max_limit { get; set; }
        }

        public class _stockLimit
        {
            [JsonProperty("min_limit")]
            public int min_limit { get; set; }
            [JsonProperty("max_limit")]
            public int max_limit { get; set; }
        }

        public class _itemName
        {
            [JsonProperty("min_limit")]
            public int min_limit { get; set; }
            [JsonProperty("max_limit")]
            public int max_limit { get; set; }
        }

        public class _itemImage
        {
            [JsonProperty("min_limit")]
            public int min_limit { get; set; }
            [JsonProperty("max_limit")]
            public int max_limit { get; set; }
        }

        public class _itemDescription
        {
            [JsonProperty("min_limit")]
            public int min_limit { get; set; }
            [JsonProperty("max_limit")]
            public int max_limit { get; set; }
        }

        public class _tierVariationName
        {
            [JsonProperty("min_limit")]
            public int min_limit { get; set; }
            [JsonProperty("max_limit")]
            public int max_limit { get; set; }
        }

        public class _tierVariationOption
        {
            [JsonProperty("min_limit")]
            public int min_limit { get; set; }
            [JsonProperty("max_limit")]
            public int max_limit { get; set; }
        }

        public class _itemCount
        {
            [JsonProperty("max_limit")]
            public int max_limit { get; set; }
        }
    }
}