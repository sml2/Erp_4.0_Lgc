using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using OrderSDK.Modles.Shopee.Enums;

namespace OrderSDK.Modles.Shopee.Product
{
    public class RequestUpdateItem : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/update_item";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;


        [JsonProperty("description")]
        public string Description { get; set; } = string.Empty;

        [JsonProperty("weight")]
        public float Weight { get; set; }

        [JsonProperty("pre_order")]
        public _PreOrder PreOrder { get; set; } = new();

        [JsonProperty("item_name")]
        public string ItemName { get; set; } = string.Empty;

        [JsonProperty("attribute_list")]
        public IEnumerable<_Attribute> AttributeList { get; set; } = Enumerable.Empty<_Attribute>();

        [JsonProperty("image")]
        public new _Image Image { get; set; } = new();

        [JsonProperty("item_sku")]
        public string ItemSku { get; set; } = string.Empty;

        [JsonProperty("item_status")]
        [EnumDataType(typeof(EShopeeItemStatus))]
        [JsonConverter(typeof(StringEnumConverter))]
        public EShopeeItemStatus ItemStatus { get; set; }

        [JsonProperty("logistic_info")]
        public IEnumerable<_LogisticInfo> LogisticInfo { get; set; } = Enumerable.Empty<_LogisticInfo>();

        [JsonProperty("wholesale")]
        public IEnumerable<_Wholesale> Wholesale { get; set; } = Enumerable.Empty<_Wholesale>();

        [JsonRequired]
        [JsonProperty("item_id")]
        public int ItemId { get; set; }

        [JsonProperty("category_id")]
        public long? CategoryId { get; set; }

        [JsonProperty("dimension")]
        public _Dimension Dimension { get; set; } = new();

        [JsonProperty("condition")]
        public string Condition { get; set; } = string.Empty;

        [JsonProperty("video_upload_id")]
        public IEnumerable<string> VideoUploadId { get; set; } = Enumerable.Empty<string>();

        [JsonProperty("brand")]
        public _Brand Brand { get; set; } = new();

        [JsonProperty("item_dangerous")]
        public int ItemDangerous { get; set; } = 0;

        [JsonProperty("tax_info")]
        public _taxInfo TaxInfo { get; set; } = new();

        [JsonProperty("complaint_policy")]
        public _complaint_policy ComplaintPolicy { get; set; } = null!;

        //--------------------------------------------------------------------------------------------------------------------------------
        public class _Dimension
        {
            [JsonRequired]
            [JsonProperty("package_width")]
            public int PackageWidth { get; set; }

            [JsonRequired]
            [JsonProperty("package_height")]
            public int PackageHeight { get; set; }

            [JsonRequired]
            [JsonProperty("package_length")]
            public int PackageLength { get; set; }
        }

        public class _LogisticInfo
        {
            [JsonProperty("size_id")]
            public int SizeId { get; set; }

            [JsonProperty("shipping_fee")]
            public float ShippingFee { get; set; }

            [JsonRequired]
            [JsonProperty("enabled")]
            public bool Enabled { get; set; }

            [JsonRequired]
            [JsonProperty("logistic_id")]
            public int LogisticId { get; set; }

            [JsonProperty("is_free")]
            public bool IsFree { get; set; }
        }

        public class _Attribute
        {
            [JsonRequired]
            [JsonProperty("attribute_id")]
            public long? AttributeId { get; set; }

            [JsonProperty("attribute_value_list")]
            public List<_AttributeValue> AttributeValueList { get; set; } = new();
        }

        public class _AttributeValue
        {
            [JsonRequired]
            [JsonProperty("value_id")]
            public long? ValueId { get; set; }

            [JsonProperty("original_value_name")]
            public string OriginalValueName { get; set; } = string.Empty;

            [JsonProperty("value_unit")]
            public string ValueUnit { get; set; } = string.Empty;
        }

        public class _Image
        {
            [JsonRequired]
            [JsonProperty("image_id_list")]
            public IEnumerable<string> ImageIdList { get; set; } = Enumerable.Empty<string>();
        }

        public class _PreOrder
        {
            [JsonRequired]
            [JsonProperty("is_pre_order")]
            public bool IsPreOrder { get; set; }

            [JsonRequired]
            [JsonProperty("days_to_ship")]
            public int DaysToShip { get; set; } = 0;
        }

        public class _Wholesale
        {
            [JsonRequired]
            [JsonProperty("min_count")]
            public int MinCount { get; set; }

            [JsonRequired]
            [JsonProperty("max_count")]
            public int MaxCount { get; set; }

            [JsonRequired]
            [JsonProperty("unit_price")]
            public float UnitPrice { get; set; }
        }

        public class _Brand
        {
            [JsonProperty("brand_id")]
            public int BrandId { get; set; }

            [JsonProperty("original_brand_name")]
            public string OriginalBrandName { get; set; } = string.Empty;
        }

        public class _taxInfo
        {
            [JsonProperty("invoice_option")]
            public string InvoiceOption { get; set; } = string.Empty;

            [JsonProperty("vat_rate")]
            public string VatRate { get; set; } = string.Empty;

            [JsonRequired]
            [JsonProperty("hs_code")]
            public string HsCode { get; set; } = string.Empty;

            [JsonRequired]
            [JsonProperty("tax_code")]
            public string TaxCode { get; set; } = string.Empty;
        }

        public class _complaint_policy
        {
            [JsonProperty("warranty_time")]
            public string WarrantyTime { get; set; } = string.Empty;

            [JsonProperty("exclude_entrepreneur_warranty")]
            public bool ExcludeEntrepreneurWarranty { get; set; }

            [JsonProperty("complaint_address_id")]
            public int ComplaintAddressId { get; set; }

            [JsonProperty("additional_information")]
            public string AdditionalInformation { get; set; } = string.Empty;
        }
    }
}