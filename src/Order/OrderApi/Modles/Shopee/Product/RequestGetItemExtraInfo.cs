using Newtonsoft.Json;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Product
{
    public class RequestGetItemExtraInfo : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/get_item_extra_info";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

        /// <summary>
        /// 长度：[0~50]
        /// </summary>
        [JsonRequired]
        [JsonProperty("item_id_list")]
        public int[] ItemIdList { get; set; } = Array.Empty<int>();
    }
}