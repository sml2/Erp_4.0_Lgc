using Newtonsoft.Json;
using System.Collections.Generic;

namespace OrderSDK.Modles.Shopee.Product
{
    public class ResponseGetItemPromotion
    {
        [JsonProperty("failure_list")]
        public IEnumerable<_failureList> FailureList { get; set; } = Enumerable.Empty<_failureList>();

        [JsonProperty("success_list")]
        public IEnumerable<_successList> SuccessList { get; set; } = Enumerable.Empty<_successList>();


        public class _failureList
        {
            [JsonProperty("item_id")]
            public int ItemId { get; set; }
            [JsonProperty("failed_reason")]
            public string FailedReason { get; set; } = string.Empty;
        }

        public class _successList
        {
            [JsonProperty("item_id")]
            public int ItemId { get; set; }

            [JsonProperty("promotion")]
            public IEnumerable<promotion> Promotion { get; set; } = Enumerable.Empty<promotion>();
        }

        public class promotion
        {
            [JsonProperty("promotion_type")]
            public string PromotionType { get; set; } = string.Empty;

            [JsonProperty("promotion_id")]
            public int PromotioId { get; set; }

            [JsonProperty("model_id")]
            public int ModelId { get; set; }

            [JsonProperty("start_time")]
            public long StartTime { get; set; }

            [JsonProperty("end_time")]
            public long EndTime { get; set; }

            [JsonProperty("promotion_price_info")]
            public IEnumerable<_promotionPriceInfo> PromotionPriceInfo { get; set; } = Enumerable.Empty<_promotionPriceInfo>();

            [JsonProperty("reserved_stock_info")]
            public IEnumerable<_reservedStockInfo> ReservedStockInfo { get; set; } = Enumerable.Empty<_reservedStockInfo>();


            [JsonProperty("promotion_staging")]
            public string PromotionStaging { get; set; } = string.Empty;
        }

        public class _promotionPriceInfo
        {
            [JsonProperty("promotion_price")]
            public float PromotionPrice { get; set; }
        }

        public class _reservedStockInfo
        {
            [JsonProperty("stock_type")]
            public int StockType { get; set; }

            [JsonProperty("stock_location_id")]
            public string StockLocationId { get; set; } = string.Empty;

            [JsonProperty("reserved_stock")]
            public int ReservedStock { get; set; }

        }
    }
}