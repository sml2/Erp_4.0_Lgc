using Newtonsoft.Json;
using System.Collections.Generic;

namespace OrderSDK.Modles.Shopee.Product
{
    public class ResponseGetRecommendAttribute
    {
        [JsonProperty("attribute_list")]
        public IEnumerable<_attribute_list> attributeList { get; set; } = Enumerable.Empty<_attribute_list>();

        public class _attribute_list
        {
            [JsonProperty("attribute_id")]
            public int attributeId { get; set; }

            [JsonProperty("attribute_value_list")]
            public IEnumerable<_attributeValueList> attributeValueList { get; set; } = Enumerable.Empty<_attributeValueList>();
        }

        public class _attributeValueList
        {
            [JsonProperty("value_id")]
            public int valueId { get; set; }
        }
    }
}