using System;
using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using OrderSDK.Modles.Shopee.Enums;
using OrderSDK.Services.Shopee.Validator.Attributes;

namespace OrderSDK.Modles.Shopee.Product;
public class RequestGetBrandList : RequestShopee
{
    [JsonIgnore]
    public override string EndPoint => "/api/v2/product/get_brand_list";

    [JsonIgnore]
    public override HttpMethod Method => HttpMethod.Get;

    [JsonProperty("offset")]
    public int Offset { get; set; }

    [JsonProperty("page_size"), JsonRequired]
    [ValidatorNumber(1, 100)]
    public int PageSize { get; set; } = 100;

    [JsonRequired, JsonProperty("category_id")]
    public long? CategoryId { get; set; }

    public override int? ShopId { get; set; }


    [JsonProperty("status", Required = Required.Always)]
    //[EnumDataType(typeof(EShopeeBrandStatus))]
    [JsonConverter(typeof(StringEnumConverter))]
    public EShopeeBrandStatus Status { get; set; } = EShopeeBrandStatus.NORMAL;

    public override string Check()
    {
        string s1 = "", s2 = "", s3 = "";
        if (CategoryId == 0)
        {
            s1 = "CategoryId is null;";
        }
        //Status
        if (Status == 0)
        {
            s2 = "Status is null;";
        }
        // PageSize
        if (PageSize == 0 || PageSize > 100)
        {
            s3 = "PageSize 1~100;";
        }
        Tuple<string, string, string> t3 = Tuple.Create(s1, s2, s3);
        return t3.ToString();
    }
}
