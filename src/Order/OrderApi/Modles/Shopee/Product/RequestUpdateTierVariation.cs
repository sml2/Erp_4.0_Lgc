using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Product
{
    public class RequestUpdateTierVariation : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/update_tier_variation";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonProperty("item_id")]
        public int ItemId { get; set; }

        [JsonRequired]
        [JsonProperty("tier_variation")]
        public IEnumerable<_Variation> TierVariation { get; set; } = Enumerable.Empty<_Variation>();

        public class _Variation
        {
            [JsonRequired]
            [JsonProperty("name")]
            public string Name { get; set; } = string.Empty;

            [JsonRequired]
            [JsonProperty("option_list")]
            public IEnumerable<_Option> OptionList { get; set; } = Enumerable.Empty<_Option>();

            public class _Option
            {
                [JsonRequired]
                [JsonProperty("option")]
                public string Option { get; set; } = string.Empty;

                [JsonProperty("image")]
                public _Image Image { get; set; } = new();

                public class _Image
                {
                    [JsonRequired]
                    [JsonProperty("image_id")]
                    public string ImageId { get; set; } = string.Empty;
                }
            }
        }

    }

}