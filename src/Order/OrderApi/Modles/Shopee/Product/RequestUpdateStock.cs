﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
namespace OrderSDK.Modles.Shopee.Product
{
    public class RequestUpdateStock : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/update_stock";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonRequired]
        [JsonProperty("item_id")]
        public long ItemId { get; set; }

        /// <summary>
        /// 长度：[1,50]
        /// </summary>
        [JsonRequired]
        [JsonProperty("stock_list")]
        public IEnumerable<_Stock> StockList { get; set; } = Enumerable.Empty<_Stock>();

        public class _Stock
        {
            [JsonProperty("model_id")]
            public int ModelId { get; set; }

            [JsonRequired]
            [JsonProperty("normal_stock")]
            public int NormalStock { get; set; }
        }
    }
}
