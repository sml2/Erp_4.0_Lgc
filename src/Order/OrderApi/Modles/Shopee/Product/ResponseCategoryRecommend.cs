using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee.Product
{
    public class ResponseCategoryRecommend
    {
        [JsonProperty("category_id")]
        public long[] CategoryId { get; set; } = Array.Empty<long>();
    }
}