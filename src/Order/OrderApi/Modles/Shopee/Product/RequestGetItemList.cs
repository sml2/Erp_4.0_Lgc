﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using OrderSDK.Modles.Shopee.Enums;
using System.ComponentModel.DataAnnotations;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Product;
public class RequestGetItemList : RequestShopee
{
    [JsonIgnore]
    public override string EndPoint => "/api/v2/product/get_item_list";

    [JsonIgnore]
    public override HttpMethod Method => HttpMethod.Get;

    [JsonRequired]
    [JsonProperty("offset")]
    public int Offset { get; set; }

    [JsonRequired]
    [JsonProperty("page_size")]
    public int PageSize { get; set; }

    [JsonProperty("update_time_from")]
    public long UpdateTimeFrom { get; set; }

    [JsonProperty("update_time_to")]
    public long UpdateTimeTo { get; set; }

    [JsonRequired]
    [JsonProperty("item_status")]
    [EnumDataType(typeof(EShopeeItemStatus))]
    [JsonConverter(typeof(StringEnumConverter))]
    public EShopeeItemStatus ItemStatus { get; set; }

}
