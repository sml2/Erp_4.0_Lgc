using Newtonsoft.Json;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Product
{
    public class RequestGetComment : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/get_comment";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

        [JsonProperty("item_id")]
        public int item_id { get; set; }

        [JsonProperty("comment_id")]
        public int Comment_id { get; set; }

        [JsonRequired]
        [JsonProperty("cursor")]
        public string Cursor { get; set; } = string.Empty;

        /// <summary>
        /// 1和100之间
        /// </summary>
        [JsonRequired]
        [JsonProperty("page_size")]
        public int PageSize { get; set; }
    }
}