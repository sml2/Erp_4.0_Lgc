﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrderSDK.Modles.Shopee.Product
{
    public class ResponseUpdateStock
    {
        [JsonProperty("failure_list")]
        public IEnumerable<_Failure> FailureList { get; set; } = Enumerable.Empty<_Failure>();

        [JsonProperty("success_list")]
        public IEnumerable<_Success> SuccessList { get; set; } = Enumerable.Empty<_Success>();

        public class _Failure
        {
            [JsonProperty("model_id")]
            public long ModelId { get; set; }

            [JsonProperty("failed_reason")]
            public string FailedReason { get; set; } = string.Empty;
        }

        public class _Success
        {
            [JsonProperty("model_id")]
            public long ModelId { get; set; }

            [JsonProperty("normal_stock")]
            public int NormalStock { get; set; }
        }
    }
}
