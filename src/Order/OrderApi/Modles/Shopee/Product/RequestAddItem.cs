using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using OrderSDK.Modles.Shopee.Enums;
using OrderSDK.Services.Shopee.Validator.Attributes;

namespace OrderSDK.Modles.Shopee.Product
{
    public class RequestAddItem : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/add_item";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        /// <summary>
        ///产品价格，TH不支持价格的小数点。
        /// </summary>
        [JsonRequired]
        [JsonProperty("original_price")]
        public float OriginalPrice { get; set; }

        [JsonRequired]
        [JsonProperty("description")]
        public string Description { get; set; } = string.Empty;

        [JsonProperty("weight")]
        public float Weight { get; set; }

        [JsonRequired]

        [JsonProperty("item_name")]
        public string ItemName { get; set; } = string.Empty;


        [JsonProperty("item_status")]
        [EnumDataType(typeof(EShopeeItemStatus))]
        [JsonConverter(typeof(StringEnumConverter))]
        public EShopeeItemStatus ItemStatus { get; set; }

        /// <summary>
        /// 产品维度：长、宽、高
        /// </summary>
        [JsonProperty("dimension")]
        public _Dimension Dimension { get; set; } = new();

        /// <summary>
        /// 库存
        /// </summary>
        [JsonRequired]
        [JsonProperty("normal_stock")]
        public int NormalStock { get; set; }

        /// <summary>
        /// 物流通道设置
        /// </summary>
        [JsonRequired]
        [JsonProperty("logistic_info")]
        public IEnumerable<_LogisticInfo> LogisticInfo { get; set; } = Enumerable.Empty<_LogisticInfo>();

        /// <summary>
        /// 产品属性
        /// </summary>
        [JsonProperty("attribute_list")]
        public IEnumerable<_Attribute> AttributeList { get; set; } = Enumerable.Empty<_Attribute>();

        /// <summary>
        /// 产品所属分类id
        /// </summary>
        [JsonRequired]
        [JsonProperty("category_id")]
        [Validator(Rules.NonZero)]
        public long? CategoryId { get; set; }


        [JsonRequired]
        [JsonProperty("image")]
        public new _Image Image { get; set; } = new();

        /// <summary>
        /// 预购订单设置
        /// </summary>
        [JsonProperty("pre_order")]
        public _PreOrder PreOrder { get; set; } = new();

        /// <summary>
        /// 产品SKU标签
        /// </summary>
        [JsonProperty("item_sku")]
        public string ItemSku { get; set; } = string.Empty;

        /// <summary>
        /// 物品是新的还是可使用的
        /// </summary>
        [JsonProperty("condition")]
        public string Condition { get; set; } = string.Empty;

        /// <summary>
        /// 批发环境
        /// </summary>
        [JsonProperty("wholesale")]
        public _Wholesale Wholesale { get; set; } = new();

        [JsonProperty("video_upload_id")]
        public string[] VideoUploadId { get; set; } = Array.Empty<string>();

        [JsonProperty("brand")]
        public _Brand Brand { get; set; } = new();

        /// <summary>
        /// 仅适用于印度尼西亚和马来西亚的本地卖家。是否为危险产品。0表示非危险品，1表示危险品
        /// </summary>
        [JsonProperty("item_dangerous")]
        public int ItemDangerous { get; set; } = 0;

        /// <summary>
        /// 税务信息
        /// </summary>
        [JsonProperty("tax_info")]
        public _taxInfo TaxInfo { get; set; } = new();

        /// <summary>
        /// 产品的投诉政策。仅本地PL卖方需要，否则忽略
        /// </summary>
        [JsonProperty("complaint_policy")]
        public _complaint_policy ComplaintPolicy { get; set; } = new();
        //--------------------------------------------------------------------------------------------------------------------------------


        public class _Dimension
        {
            [JsonRequired]
            [JsonProperty("package_width")]
            public int PackageWidth { get; set; }

            [JsonRequired]
            [JsonProperty("package_height")]
            public int PackageHeight { get; set; }

            [JsonRequired]
            [JsonProperty("package_length")]
            public int PackageLength { get; set; }
        }

        public class _LogisticInfo
        {
            [JsonProperty("size_id")]
            public int SizeId { get; set; }

            [JsonProperty("shipping_fee")]
            public float ShippingFee { get; set; }

            [JsonRequired]
            [JsonProperty("enabled")]
            public bool Enabled { get; set; }

            [JsonProperty("logistic_id")]
            public int LogisticId { get; set; }

            [JsonProperty("is_free")]
            public bool IsFree { get; set; }
        }

        public class _Attribute
        {
            [JsonRequired]
            [JsonProperty("attribute_id")]
            public long? AttributeId { get; set; }

            [JsonProperty("attribute_value_list")]
            public List<_AttributeValue> AttributeValueList { get; set; } = new();
        }


        public class _AttributeValue
        {
            [JsonRequired]
            [JsonProperty("value_id")]
            public long? ValueId { get; set; }

            [JsonProperty("original_value_name")]
            public string OriginalValueName { get; set; } = string.Empty;

            [JsonProperty("value_unit")]
            public string ValueUnit { get; set; } = string.Empty;
        }

        public class _Image
        {
            [JsonProperty("image_id_list")]
            public IEnumerable<string> ImageIdList { get; set; } = Enumerable.Empty<string>();
        }

        public class _PreOrder
        {
            /// <summary>
            /// 是不是预购订单。
            /// </summary>
            [JsonRequired]
            [JsonProperty("is_pre_order")]
            public bool IsPreOrder { get; set; }

            /// <summary>
            /// 预购订单：保证装运订单的天数在7~30之间的天数。非预购订单不填写
            /// </summary>
            [JsonProperty("days_to_ship", NullValueHandling = NullValueHandling.Ignore)]
            public int? DaysToShip { get; set; }
        }

        public class _Wholesale
        {
            [JsonRequired]
            [JsonProperty("min_count")]
            public int MinCount { get; set; }

            [JsonRequired]
            [JsonProperty("max_count")]
            public int MaxCount { get; set; }

            [JsonRequired]
            [JsonProperty("unit_price")]
            public double UnitPrice { get; set; }
        }

        public class _Brand
        {
            [JsonRequired]
            [JsonProperty("brand_id")]
            public long BrandId { get; set; }

            [JsonRequired]
            [JsonProperty("original_brand_name")]
            public string OriginalBrandName { get; set; } = string.Empty;
        }

        public class _taxInfo
        {
            [JsonProperty("invoice_option")]
            public string InvoiceOption { get; set; } = string.Empty;

            [JsonProperty("vat_rate")]
            public string VatRate { get; set; } = string.Empty;

            [JsonRequired]
            [JsonProperty("hs_code")]
            public string HsCode { get; set; } = string.Empty;

            [JsonRequired]
            [JsonProperty("tax_code")]
            public string TaxCode { get; set; } = string.Empty;
        }

        public class _complaint_policy
        {
            [JsonProperty("warranty_time")]
            public string WarrantyTime { get; set; } = string.Empty;

            [JsonProperty("exclude_entrepreneur_warranty")]
            public string ExcludeEntrepreneurWarranty { get; set; } = string.Empty;

            [JsonProperty("complaint_address_id")]
            public int ComplaintAddressId { get; set; }

            [JsonProperty("additional_information")]
            public string AdditionalInformation { get; set; } = string.Empty;
        }



    }
}