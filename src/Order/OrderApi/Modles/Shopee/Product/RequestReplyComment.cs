using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Product
{
    public class RequestReplyComment : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/reply_comment";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        /// <summary>
        /// 长度1~100
        /// </summary>
        [JsonRequired]
        [JsonProperty("comment_list")]
        public IEnumerable<_commentList> CommentList { get; set; } = Enumerable.Empty<_commentList>();

        public class _commentList
        {
            [JsonRequired]
            [JsonProperty("comment_id")]
            public int CommentId { get; set; }

            [JsonRequired]
            [JsonProperty("comment")]
            public string Comment { get; set; } = string.Empty;
        }
    }
}