using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee.Product
{
    public class RequestGetBoostedList : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/get_boosted_list";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

    }
}