using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee.Product
{

    public class ResponseSupportSizeChart
    {
        /// <summary>
        /// 是否可以为此类别设置sizechart
        /// </summary>
        [JsonProperty("support_size_chart")]
        public bool SupportSizeChart { get; set; }
    }
}