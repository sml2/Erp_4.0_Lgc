using Newtonsoft.Json;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Product
{

    public class RequestGetRecommendAttribute : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/get_recommend_attribute";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

        /// <summary>
        /// 产品名称
        /// </summary>
        [JsonProperty("item_name")]
        public string ItemName { get; set; } = string.Empty;

        /// <summary>
        /// 产品封面图像ID
        /// </summary>
        [JsonProperty("cover_image_id")]
        public int CoverImageId { get; set; }

        /// <summary>
        /// 产品分类ID
        /// </summary>
        [JsonProperty("category_id")]
        public long CategoryId { get; set; }
    }
}