using Newtonsoft.Json;
using System.Collections.Generic;

namespace OrderSDK.Modles.Shopee.Product
{
    public class ResponseGetComment
    {
        [JsonProperty("more")]
        public bool More { get; set; }

        [JsonProperty("item_comment_list")]
        public IEnumerable<_itemComment> ItemCommentList { get; set; } = Enumerable.Empty<_itemComment>();

        public class _itemComment
        {
            [JsonProperty("order_sn")]
            public int OrderSn { get; set; }
            [JsonProperty("comment_id")]
            public string CommentId { get; set; } = string.Empty;

            [JsonProperty("comment")]
            public string Comment { get; set; } = string.Empty;

            [JsonProperty("buyer_username")]
            public string BuyerUsername { get; set; } = string.Empty;

            [JsonProperty("item_id")]
            public int ItemId { get; set; }

            [JsonProperty("model_id")]
            public int ModelId { get; set; }

            [JsonProperty("rating_star")]
            public int RatingStar { get; set; }

            [JsonProperty("editable")]
            public string Editable { get; set; } = string.Empty;

            [JsonProperty("hidden")]
            public bool Hidden { get; set; }

            [JsonProperty("create_time")]
            public long CreateTime { get; set; }

            [JsonProperty("comment_reply")]
            public _commentReply CommentReply { get; set; } = new();

            [JsonProperty("next_cursor")]
            public string NextCursor { get; set; } = string.Empty;
        }

        public class _commentReply
        {
            [JsonProperty("reply")]
            public string Reply { get; set; } = string.Empty;

            [JsonProperty("hidden")]
            public bool Hidden { get; set; }
        }
    }
}