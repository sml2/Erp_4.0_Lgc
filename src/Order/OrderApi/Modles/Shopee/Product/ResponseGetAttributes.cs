using System.Collections.Generic;
using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee.Product
{

    public class ResponseGetAttributes
    {
        [JsonProperty("attribute_list")]
        public List<Attribute> AttributeList { get; set; } = new();

        public class Attribute
        {
            [JsonProperty("attribute_id")]
            public long? AttributeId { get; set; }

            [JsonProperty("original_attribute_name")]
            public string OriginalAttributeName { get; set; } = string.Empty;

            [JsonProperty("display_attribute_name")]
            public string DisplayAttributeName { get; set; } = string.Empty;

            [JsonProperty("is_mandatory")]
            public bool IsMandatory { get; set; }

            [JsonProperty("input_validation_type")]
            public string InputValidationType { get; set; } = string.Empty;

            [JsonProperty("format_type")]
            public string FormatType { get; set; } = string.Empty;

            [JsonProperty("date_format_type")]
            public string DateFormatType { get; set; } = string.Empty;

            [JsonProperty("input_type")]
            public string InputType { get; set; } = string.Empty;

            [JsonProperty("attribute_unit")]
            public IEnumerable<string> AttributeUnit { get; set; } = Enumerable.Empty<string>();

            [JsonProperty("attribute_value_list")]
            public IEnumerable<_AttributeValue> AttributeValueList { get; set; } = Enumerable.Empty<_AttributeValue>();


            public class _AttributeValue
            {
                [JsonProperty("value_id")]
                public long? ValueId { get; set; }

                [JsonProperty("original_value_name")]
                public string OriginalValueName { get; set; } = string.Empty;

                [JsonProperty("display_value_name")]
                public string DisplayValueName { get; set; } = string.Empty;

                [JsonProperty("value_unit")]
                public string ValueUnit { get; set; } = string.Empty;

                [JsonProperty("parent_attribute_list")]
                public IEnumerable<_ParentAttribute> ParentAttributeList { get; set; } = Enumerable.Empty<_ParentAttribute>();

                [JsonProperty("parent_brand_list")]
                public IEnumerable<_ParentBrand> ParentBrandList { get; set; } = Enumerable.Empty<_ParentBrand>();

                public class _ParentAttribute
                {
                    [JsonProperty("parent_attribute_id")]
                    public long ParentAttributeId { get; set; }

                    [JsonProperty("parent_value_id")]
                    public long ParentValueId { get; set; }
                }

                public class _ParentBrand
                {
                    [JsonProperty("parent_brand_id")]
                    public long ParentBrandId { get; set; }
                }
            }

        }
    }
}