﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrderSDK.Modles.Shopee.Product
{
    public class ResponseGetModelList
    {
        [JsonProperty("tier_variation")]
        public IEnumerable<_TierVariation> TierVariation { get; set; } = Enumerable.Empty<_TierVariation>();

        [JsonProperty("model")]
        public IEnumerable<_Model> Model { get; set; } = Enumerable.Empty<_Model>();


        public class _TierVariation
        {
            [JsonProperty("option_list")]
            public IEnumerable<_Option> OptionList { get; set; } = Enumerable.Empty<_Option>();

            [JsonProperty("name")]
            public string Name { get; set; } = string.Empty;


            public class _Option
            {
                [JsonProperty("option")]
                public string Option { get; set; } = string.Empty;

                [JsonProperty("image")]
                public _Image Image { get; set; } = new();


                public class _Image
                {
                    [JsonProperty("image_id")]
                    public string ImageId { get; set; } = string.Empty;

                    [JsonProperty("image_url")]
                    public string ImageUrl { get; set; } = string.Empty;
                }
            }
        }

        public class _Model
        {
            [JsonProperty("price_info")]
            public IEnumerable<_PriceInfo> PriceInfo { get; set; } = Enumerable.Empty<_PriceInfo>();

            [JsonProperty("model_id")]
            public int ModelId { get; set; }

            [JsonProperty("stock_info")]
            public IEnumerable<_StockInfo> StockInfo { get; set; } = Enumerable.Empty<_StockInfo>();

            [JsonProperty("tier_index")]
            public IEnumerable<int> TierIndex { get; set; } = Array.Empty<int>();

            [JsonProperty("promotion_id")]
            public long PromotionId { get; set; }

            [JsonProperty("model_sku")]
            public string ModelSku { get; set; } = string.Empty;



            public class _PriceInfo
            {
                [JsonProperty("current_price")]
                public double CurrentPrice { get; set; }

                [JsonProperty("original_price")]
                public double OriginalPrice { get; set; }

                [JsonProperty("inflated_price_of_original_price")]
                public double InflatedPriceOfOriginalPrice { get; set; }

                [JsonProperty("inflated_price_of_current_price")]
                public double InflatedPriceOfCurrentPrice { get; set; }

                [JsonProperty("sip_item_price")]
                public double SipItemPrice { get; set; }

                [JsonProperty("sip_item_price_source")]
                public string SipItemPriceSource { get; set; } = string.Empty;
            }

            public class _StockInfo
            {
                [JsonProperty("normal_stock")]
                public int NormalStock { get; set; }

                [JsonProperty("stock_type")]
                public int StockType { get; set; }

                [JsonProperty("current_stock")]
                public int CurrentStock { get; set; }

                [JsonProperty("reserved_stock")]
                public int ReservedStock { get; set; }
            }
        }


    }
}
