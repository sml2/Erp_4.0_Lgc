using Newtonsoft.Json;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Product
{
    public class RequestGetAttributes : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/product/get_attributes";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;


        [JsonRequired]
        [JsonProperty("category_id")]
        public long? CategoryId { get; set; }

        [JsonProperty("language")]
        public string Language { get; set; } = string.Empty;
    }

}