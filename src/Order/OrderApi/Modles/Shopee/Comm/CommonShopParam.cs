﻿using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee.Comm;
public class CommonShopParam
{
    [JsonProperty("partner_id")]
    public long PartnerId { get; set; }

    [JsonProperty("timestamp")]
    public long Timestamp { get; set; }

    [JsonProperty("access_token")]
    public string AccessToken { get; set; } = string.Empty;

    [JsonProperty("shop_id")]
    public long? ShopId { get; set; }

    [JsonProperty("sign")]
    public string Sign { get; set; } = string.Empty;

    public string ToQueryString()
    {
        string query = ConvertHelper.ConvertToQueryString(this);
        return query;
    }
}

