﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderSDK.Modles.Shopee.Comm;
public class CommonParam
{
    [JsonProperty("sign")]
    public string Sign { get; set; } = string.Empty;

    [JsonProperty("partner_id")]
    public long PartnerId { get; set; }

    [JsonProperty("timestamp")]
    public long Timestamp { get; set; }

    public string ToQueryString()
    {
        string query = ConvertHelper.ConvertToQueryString(this);
        return query;
    }
}
