﻿using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee.Payment
{
    public class ResponseGetEscrowDetail
    {
        /// <summary>
        /// Shopee's unique identifier for an order.
        /// </summary>
        [JsonProperty("order_sn")]
        public string OrderSn { get; set; } = string.Empty;

        /// <summary>
        /// 买方名字
        /// </summary>
        [JsonProperty("buyer_user_name")]
        public string BuyerUserName { get; set; } = string.Empty;

        /// <summary>
        /// 返回序列号的列表
        /// </summary>
        [JsonProperty("return_order_sn_list")]
        public List<string> ReturnOrderSnList { get; set; } = new();

        [JsonProperty("order_income")]
        public orderIncome OrderIncome { get; set; } = new();

        public class orderIncome
        {
            /// <summary>
            /// 卖方预计收到的订单总金额，并将在订单完成前更改。
            /// 对于非cb sip加盟店：代管金额=买家总额+买家折扣+来自买家的代金券+硬币+付款促销-买家交易费-跨境税-佣金费-服务费-卖家交易费
            /// -卖家-现金-后代管税-drc可调退款+最终运费（可为邮寄/非邮寄）。
            /// 对于cb sip加盟店：托管金额=所有Asku结算价格的总和-服务费-佣金费-卖家退货退款-drc可调整退款。
            /// </summary>
            [JsonProperty("escrow_amount")]
            public float EscrowAmount { get; set; }

            /// <summary>
            /// 买方支付的总金额。
            /// </summary>
            [JsonProperty("buyer_total_amount")]
            public float BuyerTotalAmount { get; set; }

            /// <summary>
            /// 任何促销/折扣前商品的原价，以上市货币计算。如果数量超过1，则返回该特定项目的小计。
            /// </summary>
            [JsonProperty("original_price")]
            public float OriginalPrice { get; set; }


            /// <summary>
            /// 特定订单的每项卖家折扣的最终金额。（仅适用于非cb sip加盟店。）
            /// </summary>
            [JsonProperty("seller_discount")]
            public float SellerDiscount { get; set; }


            /// <summary>
            /// 特定订单的每件商品的最终折扣总额。这笔钱将退还给卖方。（仅显示非cb sip关联订单。）
            /// </summary>
            [JsonProperty("shopee_discount")]
            public float ShopeeDiscount { get; set; }

            /// <summary>
            /// 卖方提供的订单凭证的最终价值。（仅适用于非cb sip加盟店。）
            /// </summary>
            [JsonProperty("voucher_from_seller")]
            public float VoucherFromSeller { get; set; }

            /// <summary>
            /// 购物者为订单提供的凭证的最终价值。（仅适用于非cb sip加盟店。）
            /// </summary>
            [JsonProperty("voucher_from_shopee")]
            public float VoucherFromShopee { get; set; }


            /// <summary>
            /// 此值表示买家在结账时消费Shopee硬币时的总抵消金额。（仅适用于非cb sip加盟店。）
            /// </summary>
            [JsonProperty("coins")]
            public float Coins { get; set; }


            /// <summary>
            /// 买方支付的运费。（仅适用于非cb sip加盟店。）
            /// </summary>
            [JsonProperty("buyer_paid_shipping_fee")]
            public float BuyerPaidShippingFee { get; set; }


            /// <summary>
            /// 买方为订单支付的交易费。（仅适用于非cb sip加盟店。）
            /// </summary>
            [JsonProperty("buyer_transaction_fee")]
            public float BuyerTransactionFee { get; set; }


            /// <summary>
            /// 买方因在本国境外购买物品而产生的金额。退款后金额可能会发生变化。（仅适用于非cb sip加盟店。）
            /// </summary>
            [JsonProperty("cross_border_tax")]
            public float CrossBorderTax { get; set; }

            /// <summary>
            /// 通过付款促销抵消的金额。（仅适用于非cb sip加盟店。）
            /// </summary>
            [JsonProperty("payment_promotion")]
            public float PaymentPromotion { get; set; }

            /// <summary>
            /// Shopee平台收取的佣金（如适用）。
            /// </summary>
            [JsonProperty("commission_fee")]
            public float CommissionFee { get; set; }

            /// <summary>
            /// 购物者向卖家收取的额外服务费用。
            /// </summary>
            [JsonProperty("service_fee")]
            public float ServiceFee { get; set; }

            /// <summary>
            /// 卖方为订单支付的交易费。（仅适用于非cb sip加盟店。）
            /// </summary>
            [JsonProperty("seller_transaction_fee")]
            public float SellerTransactionFee { get; set; }


            /// <summary>
            /// 丢失包裹时向卖方赔偿。（仅适用于非cb sip加盟店。）
            /// </summary>
            [JsonProperty("seller_lost_compensation")]
            public float SellerLostCompensation { get; set; }


            /// <summary>
            /// 卖家提供的硬币价值，用于在其店铺购买订单。（仅适用于非cb sip加盟店。）
            /// </summary>
            [JsonProperty("seller_coin_cash_back")]
            public float seller_coin_cash_back { get; set; }


            /// <summary>
            /// 印尼政府对卖家征收的跨境税。（仅适用于非cb sip加盟店。）
            /// </summary>
            [JsonProperty("escrow_tax")]
            public float EscrowTax { get; set; }

            /// <summary>
            /// 卖方作为托管的一部分必须承担的最终调整金额。这个数字可以是负的，也可以是正的。（仅适用于非cb sip加盟店。）
            /// </summary>
            [JsonProperty("final_shipping_fee")]
            public float FinalShippingFee { get; set; }


            /// <summary>
            /// 订单的最终运输成本是正的。非综合物流渠道为0。（仅适用于非cb sip加盟店。）
            /// </summary>
            [JsonProperty("actual_shipping_fee")]
            public float ActualShippingFee { get; set; }


            /// <summary>
            /// 平台运费补贴给卖方。（仅适用于非cb sip加盟店。）
            /// </summary>
            [JsonProperty("shopee_shipping_rebate")]
            public float ShopeeShippingRebate { get; set; }


            /// <summary>
            /// 第三方物流的运费折扣。目前仅适用于ID（仅显示非cb sip加盟店。）
            /// </summary>
            [JsonProperty("shipping_fee_discount_from_3pl")]
            public float ShippingFeeDiscountFrom3pl { get; set; }

            /// <summary>
            /// 卖方定义的运输折扣。（仅适用于非cb sip加盟店。）
            /// </summary>
            [JsonProperty("seller_shipping_discount")]
            public float SellerShippingDiscount { get; set; }

            /// <summary>
            /// 预计运费是Shopee根据特定物流快递标准计算的估计费用。（仅适用于非cb sip加盟店。）
            /// </summary>
            [JsonProperty("estimated_shipping_fee")]
            public float EstimatedShippingFee { get; set; }


            /// <summary>
            /// 卖方提供的凭证代码列表。（仅适用于非cb sip加盟店。）
            /// </summary>
            [JsonProperty("seller_voucher_code")]
            public List<string> SellerVoucherCode { get; set; } = new();


            /// <summary>
            /// Shopee争议解决中心的可调整退款金额。
            /// </summary>
            [JsonProperty("drc_adjustable_refund")]
            public float DrcAdjustableRefund { get; set; }

            /// <summary>
            /// 买方为特定订单中的项目支付的最终金额。（仅适用于非cb sip加盟店。）
            /// </summary>
            [JsonProperty("cost_of_goods_sold")]
            public float CostOfGoodsSold { get; set; }

            /// <summary>
            /// 买方为特定订单中的项目支付的金额。（仅适用于非cb sip加盟店。）
            /// </summary>
            [JsonProperty("original_cost_of_goods_sold")]
            public float OriginalCostOfGoodsSold { get; set; }

            /// <summary>
            /// 特定订单的每件商品购物者折扣的总和。（仅适用于非cb sip加盟店。）
            /// </summary>
            [JsonProperty("original_shopee_discount")]
            public float OriginalShopeeDiscount { get; set; }

            /// <summary>
            /// 部分退货时退还给卖方的金额。
            /// </summary>
            [JsonProperty("seller_return_refund")]
            public float SellerReturnRefund { get; set; }

            public List<Item> items { get; set; } = new();

            public class Item
            {

                /// <summary>
                /// ID of item
                /// </summary>
                [JsonProperty("item_id")]
                public int ItemId { get; set; }

                /// <summary>
                ///商品名称
                /// </summary>
                [JsonProperty("item_name")]
                public string ItemName { get; set; } = string.Empty;


                /// <summary>
                /// 商品SKU
                /// </summary>
                [JsonProperty("item_sku")]
                public string ItemSku { get; set; } = string.Empty;

                /// <summary>
                /// 属于同一项的模型的ID。
                /// </summary>
                [JsonProperty("model_id")]
                public int ModelId { get; set; }

                /// <summary>
                /// 模型的名称
                /// </summary>
                [JsonProperty("model_name")]
                public int ModelName { get; set; }

                /// <summary>
                /// 模型sku
                /// </summary>
                [JsonProperty("model_sku")]
                public string ModelSku { get; set; } = string.Empty;

                /// <summary>
                /// 任何促销/折扣前商品的原价，以上市货币计算。如果数量超过1，则返回该特定项目的小计。
                /// </summary>
                [JsonProperty("original_price")]
                public float OriginalPrice { get; set; }


                /// <summary>
                /// 以上市货币表示的商品折扣后价格。如果数量超过1，则返回该特定项目的小计。如果没有折扣，该价格将与原价相同。
                /// </summary>
                [JsonProperty("discounted_price")]
                public float DiscountedPrice { get; set; }

                /// <summary>
                /// 当买家在结账时消费Shopee硬币时，此项目的抵消。对于捆绑交易项目，该值将返回0。由于技术限制，如果我们不将此字段配置为0，则在捆绑交易情况下，此字段将返回不正确的值。
                /// </summary>
                [JsonProperty("discount_from_coin")]
                public float DiscountFromCoin { get; set; }

                /// <summary>
                /// 买家使用Shopee凭证时该项目的抵销。对于捆绑交易项目，该值将返回0。由于技术限制，如果我们不将此字段配置为0，则在捆绑交易情况下，此字段将返回不正确的值。
                /// </summary>
                [JsonProperty("discount_from_voucher_shopee")]
                public float DiscountFromVoucherShopee { get; set; }

                /// <summary>
                /// 当买方使用卖方特定凭证时，此项的抵销。对于捆绑交易项目，该值将返回0。由于技术限制，如果我们不将此字段配置为0，则在捆绑交易情况下，此字段将返回不正确的值。
                /// </summary>
                [JsonProperty("discount_from_voucher_seller")]
                public float DiscountFromVoucherSeller { get; set; }


                /// <summary>
                /// 项目的类型默认为“”。如果项目是捆绑项目，则值为“捆绑交易”，如果是附加交易项目，则值为“附加交易”
                /// </summary>
                [JsonProperty("activity_type")]
                public string ActivityType { get; set; } = string.Empty;

                /// <summary>
                /// 
                /// </summary>
                [JsonProperty("activity_id")]
                public int ActivityId { get; set; }

                /// <summary>
                /// 添加交易的主项或子项
                /// </summary>
                [JsonProperty("is_main_item")]
                public bool IsMainItem { get; set; }

                /// <summary>
                /// 该值表示同一买家同时从一个清单/物品中购买的相同物品的数量。
                /// </summary>
                [JsonProperty("quantity_purchased")]
                public int QuantityPurchased { get; set; }
            }

            /// <summary>
            /// 卖方预计收到的订单总金额，以原始货币表示，并将在订单完成前更改。第三方托管金额=原价卖方退货佣金费用服务费用可调整退款。（仅显示非cb sip订单。）
            /// </summary>
            [JsonProperty("escrow_amount_pri")]
            public float EscrowAmountPri { get; set; }

            /// <summary>
            /// 买方以主要货币支付的总金额。（仅显示cb sip附属订单。）
            /// </summary>
            [JsonProperty("buyer_total_amount_pri")]
            public float BuyerTotalAmountPri { get; set; }


            /// <summary>
            /// 促销/折扣前商品的原价，以主要货币表示。如果数量超过1，则返回该特定项目的小计。（仅显示非cb sip关联订单。）
            /// </summary>
            [JsonProperty("original_price_pri")]
            public float OriginalPricePri { get; set; }

            /// <summary>
            /// 如果以主要货币部分返还，则返还给卖方的金额。（仅适用于cb sip附属商店。）
            /// </summary>
            [JsonProperty("seller_return_refund_pri")]
            public float SellerReturnRefundPri { get; set; }

            /// <summary>
            /// Shopee平台收取的佣金（如适用），以主要货币计算。（仅适用于cb sip附属商店。）
            /// </summary>
            [JsonProperty("commission_fee_pri")]
            public float CommissionFeePri { get; set; }

            /// <summary>
            /// 购物者以主要货币向卖方收取的额外服务费用。（仅适用于cb sip附属商店。）
            /// </summary>
            [JsonProperty("service_fee_pri")]
            public float ServiceFeePri { get; set; }

            /// <summary>
            /// 从Shopee争议解决中心获得的可调整退款金额，以主要货币表示。（仅适用于cb sip附属商店。）
            /// </summary>
            [JsonProperty("drc_adjustable_refund_pri")]
            public float DrcAdjustablRefundPri { get; set; }


            /// <summary>
            /// 真正的卖家经营的商店所在国的货币。（仅适用于cb sip附属商店。）
            /// </summary>
            [JsonProperty("pri_currency")]
            public string PriCurrency { get; set; } = string.Empty;

            /// <summary>
            /// 开店所在国的货币。（仅适用于cb sip附属商店。）
            /// </summary>
            [JsonProperty("aff_currency")]
            public string AffCurrency { get; set; } = string.Empty;

            /// <summary>
            /// 从主要商店货币到附属商店货币的汇率。
            /// </summary>
            [JsonProperty("exchange_rate")]
            public float ExchangeRate { get; set; }


            /// <summary>
            /// Shopee对退回的订单收取反向运费。该字段的值将为非负。
            /// </summary>
            [JsonProperty("reverse_shipping_fee")]
            public float ReverseShippingFee { get; set; }

            /// <summary>
            /// 下单期间购买的产品保护总金额。（仅适用于cb正常和cb sip主要车间）
            /// </summary>
            [JsonProperty("final_product_protection")]
            public float FinalProductProtection { get; set; }

        }
    }
}
