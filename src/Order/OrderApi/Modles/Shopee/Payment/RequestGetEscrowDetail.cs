﻿using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee.Payment
{
    public class RequestGetEscrowDetail : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/payment/get_escrow_detail";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

        /// <summary>
        /// Shopee's unique identifier for an order.
        /// </summary>
        [JsonRequired]
        [JsonProperty("order_sn ")]
        public string order_sn { get; set; } = string.Empty;
    }
}
