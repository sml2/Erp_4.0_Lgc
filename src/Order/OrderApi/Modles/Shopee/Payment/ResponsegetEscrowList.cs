﻿using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee.Payment
{
    public class ResponsegetEscrowList
    {
        //[JsonProperty("error")]
        //public string Error { get; set; } = string.Empty;

        //[JsonProperty("message")]
        //public string Message { get; set; } = string.Empty;

        //[JsonProperty("request_id")]
        //public string RequestId { get; set; } = string.Empty;

        //[JsonProperty("response")]
        //public Response response { get; set; } = new();

        //public class Response
        //{
        [JsonProperty("escrow_list")]
        public List<escrow> EscrowList { get; set; } = new();

        [JsonProperty("more")]
        public bool more { get; set; }


        public class escrow
        {
            /// <summary>
            /// 订单序号
            /// </summary>
            [JsonProperty("order_sn")]
            public string OrderSn { get; set; } = string.Empty;

            /// <summary>
            /// 结算金额
            /// </summary>
            [JsonProperty("payout_amount")]
            public float PayoutAmount { get; set; }

            /// <summary>
            /// 发布时间
            /// </summary>
            [JsonProperty("escrow_release_time")]
            public long EscrowReleaseTime { get; set; }
        }
    }
}
