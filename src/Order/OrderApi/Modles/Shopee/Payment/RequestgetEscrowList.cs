﻿using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee.Payment
{
    public class RequestgetEscrowList : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/payment/get_escrow_list";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

        /// <summary>
        /// Query start time
        /// </summary>
        [JsonRequired]
        [JsonProperty("release_time_from")]
        public long ReleaseTimeFrom { get; set; }

        /// <summary>
        /// Query end time
        /// </summary>
        [JsonRequired]
        [JsonProperty("release_time_to")]
        public long ReleaseTimeTo { get; set; }

        /// <summary>
        /// 返回的最大页数：100默认值：40
        /// </summary>
        [JsonProperty("page_size")]
        public int PageSize { get; set; }

        /// <summary>
        /// 页码最小值：1默认值：1
        /// </summary>
        [JsonProperty("page_no")]
        public long PageNo { get; set; }
    }
}
