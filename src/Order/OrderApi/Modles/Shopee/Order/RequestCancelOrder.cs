using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Order
{
    public class RequestCancelOrder : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/order/cancel_order";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonProperty("order_sn")]
        public string OrderSn { get; set; } = string.Empty;

        [JsonProperty("cancel_reason")]
        public string CancelReason { get; set; } = string.Empty;

        [JsonProperty("item_list")]
        public IEnumerable<Item> item_list { get; set; } = Enumerable.Empty<Item>();

        public class Item
        {
            [JsonProperty("item_id")]
            public string ItemId { get; set; } = string.Empty;

            [JsonProperty("model_id")]
            public string ModelId { get; set; } = string.Empty;
        }
    }
}