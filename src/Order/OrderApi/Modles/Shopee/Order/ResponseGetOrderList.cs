using Newtonsoft.Json;
using System.Collections.Generic;

namespace OrderSDK.Modles.Shopee.Order
{
    public class ResponseGetOrderList
    {
        [JsonProperty("order_list")]
        public IEnumerable<Order> OrderList { get; set; } = Enumerable.Empty<Order>();

        [JsonProperty("next_cursor")]
        public string NextCursor { get; set; } = string.Empty;

        [JsonProperty("more")]
        public bool More { get; set; }
        public class Order
        {

            [JsonProperty("order_sn")]
            public string OrderSn { get; set; } = string.Empty;

            [JsonProperty("order_status")]
            public string OrderStatus { get; set; } = string.Empty;
        }
    }
}