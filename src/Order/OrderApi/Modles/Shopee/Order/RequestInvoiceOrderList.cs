using Newtonsoft.Json;
using System;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Order
{
    public class RequestInvoiceOrderList : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/order/get_pending_buyer_invoice_order_list";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

        [JsonProperty("cursor")]
        public string Cursor { get; set; } = string.Empty;

        [JsonProperty("page_size ")]
        public ulong page_size { get; set; }
    }
}