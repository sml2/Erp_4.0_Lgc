
using Newtonsoft.Json;


namespace OrderSDK.Modles.Shopee.Order;
public class ResponseGetOrderDetail
{
    [JsonProperty("order_list")]
    public IEnumerable<Order> OrderList { get; set; } = Enumerable.Empty<Order>();

    public class Order
    {
        [JsonProperty("order_sn")]
        public string OrderSn { get; set; } = string.Empty;

        [JsonProperty("region")]
        public string Region { get; set; } = string.Empty;

        [JsonProperty("currency")]
        public string Currency { get; set; } = string.Empty;

        [JsonProperty("cod")]
        public bool Cod { get; set; }

        [JsonProperty("total_amount")]
        public float TotalAmount { get; set; }

        [JsonProperty("order_status")]
        public string OrderStatus { get; set; } = string.Empty;

        [JsonProperty("shipping_carrier")]
        public string ShippingCarrier { get; set; } = string.Empty;

        [JsonProperty("payment_method")]
        public string PaymentMethod { get; set; } = string.Empty;

        [JsonProperty("estimated_shipping_fee")]
        public float EstimatedShippingFee { get; set; }

        [JsonProperty("message_to_seller")]
        public string MessageToSeller { get; set; } = string.Empty;

        [JsonProperty("create_time")]
        public DateTime CreateTime { get; set; }

        [JsonProperty("update_time")]
        public DateTime UpdateTime { get; set; }

        [JsonProperty("days_to_ship")]
        public int DaysToShip { get; set; }

        [JsonProperty("ship_by_date")]
        public int ShipByDate { get; set; }

        [JsonProperty("buyer_user_id")]
        public int BuyerUserId { get; set; }

        [JsonProperty("buyer_username")]
        public string BuyerUserName { get; set; } = string.Empty;

        [JsonProperty("recipient_address")]
        public RecipientAddress RecipientAddress { get; set; } = new();

        [JsonProperty("actual_shipping_fee")]
        public float ActualShippingFee { get; set; }

        [JsonProperty("goods_to_declare")]
        public bool GoodsToDeclare { get; set; }

        [JsonProperty("note")]
        public string Note { get; set; } = string.Empty;

        [JsonProperty("note_update_time")]
        public DateTime NoteUpdateTime { get; set; }

        [JsonProperty("item_list")]
        public IEnumerable<Item> ItemList { get; set; } = Enumerable.Empty<Item>();

        [JsonProperty("pay_time")]
        public DateTime PayTime { get; set; }

        [JsonProperty("dropshipper")]
        public string Dropshipper { get; set; } = string.Empty;

        [JsonProperty("credit_card_number")]
        public string CreditCardNumber { get; set; } = string.Empty;

        [JsonProperty("dropshipper_phone")]
        public string DropshipperPhone { get; set; } = string.Empty;

        [JsonProperty("split_up")]
        public bool SplitUp { get; set; }

        [JsonProperty("buyer_cancel_reason")]
        public string BuyerCancelReason { get; set; } = string.Empty;

        [JsonProperty("cancel_by")]
        public string CancelBy { get; set; } = string.Empty;

        [JsonProperty("cancel_reason")]
        public string CancelReason { get; set; } = string.Empty;

        [JsonProperty("actual_shipping_fee_confirmed")]
        public bool ActualShippingFeeConfirmed { get; set; }

        [JsonProperty("buyer_cpf_id")]
        public string BuyerCpfId { get; set; } = string.Empty;

        [JsonProperty("fulfillment_flag")]
        public string FulfillmentFlag { get; set; } = string.Empty;

        [JsonProperty("pickup_done_time")]
        public DateTime PickupDoneTime { get; set; }

        [JsonProperty("package_list")]
        public List<PackageList> PackageList { get; set; } = new();

        [JsonProperty("invoice_data")]
        public invoice_data invoice_data { get; set; } = new();

        [JsonProperty("checkout_shipping_carrier")]
        public string CheckoutShippingCarrier { get; set; } = string.Empty;

        [JsonProperty("reverse_shipping_fee")]
        public float ReverseShippingFee { get; set; }

    }

    public class RecipientAddress
    {
        [JsonProperty("name")]
        public string Name { get; set; } = string.Empty;

        [JsonProperty("phone")]
        public string Phone { get; set; } = string.Empty;

        [JsonProperty("town")]
        public string Town { get; set; } = string.Empty;

        [JsonProperty("district")]
        public string District { get; set; } = string.Empty;

        [JsonProperty("city")]
        public string City { get; set; } = string.Empty;

        [JsonProperty("state")]
        public string State { get; set; } = string.Empty;

        [JsonProperty("region")]
        public string Region { get; set; } = string.Empty;

        [JsonProperty("zipcode")]
        public string Zipcode { get; set; } = string.Empty;
        [JsonProperty("full_address")]
        public string FullAddress { get; set; } = string.Empty;
    }

    public class Item
    {
        [JsonProperty("item_id")]
        public int ItemId { get; set; }

        [JsonProperty("item_name")]
        public string ItemName { get; set; } = string.Empty;

        [JsonProperty("item_sku")]
        public string ItemSku { get; set; } = string.Empty;

        [JsonProperty("model_id")]
        public int ModelId { get; set; }

        [JsonProperty("model_name")]
        public string ModelName { get; set; } = string.Empty;

        [JsonProperty("model_sku")]
        public string ModelSku { get; set; } = string.Empty;

        [JsonProperty("model_quantity_purchased")]
        public int ModelQuantityPurchased { get; set; }

        [JsonProperty("model_original_price")]
        public float ModelOriginalPrice { get; set; }

        [JsonProperty("model_discounted_price")]
        public float ModelDiscountedPrice { get; set; }

        [JsonProperty("wholesale")]
        public bool Wholesale { get; set; }

        [JsonProperty("weight")]
        public float Weight { get; set; }

        [JsonProperty("add_on_deal")]
        public bool AddOnDeal { get; set; }

        [JsonProperty("main_item")]
        public bool MainItem { get; set; }

        [JsonProperty("add_on_deal_id")]
        public int AddOnDealId { get; set; }

        [JsonProperty("promotion_type")]
        public string PromotionType { get; set; } = string.Empty;

        [JsonProperty("promotion_id")]
        public int PromotionId { get; set; }

        [JsonProperty("order_item_id")]
        public int OrderItemId { get; set; }

        [JsonProperty("promotion_group_id")]
        public int PromotionGroupId { get; set; }

        [JsonProperty("image_info")]
        public ImageInfo ImageInfo { get; set; } = new();

    }

    public class ImageInfo
    {
        public string image_url { get; set; } = string.Empty;
    }

    public class PackageList
    {
        [JsonProperty("package_number")]
        public string PackageNumber { get; set; } = string.Empty;

        [JsonProperty("logistics_status")]
        public string LogisticsStatus { get; set; } = string.Empty;

        [JsonProperty("shipping_carrier")]
        public string ShippingCarrier { get; set; } = string.Empty;

        [JsonProperty("item_list")]
        public List<Cls_item> ItemList { get; set; } = new();
    }

    public class Cls_item
    {

        [JsonProperty("item_id")]
        public int ItemId { get; set; }
        [JsonProperty("model_id")]
        public int ModelId { get; set; }
    }

    public class invoice_data
    {
        [JsonProperty("number")]
        public string Number { get; set; } = string.Empty;

        [JsonProperty("series_number")]
        public string SeriesNumber { get; set; } = string.Empty;

        [JsonProperty("access_key")]
        public string AccessKey { get; set; } = string.Empty;

        [JsonProperty("issue_date")]
        public DateTime IssueDate { get; set; }

        [JsonProperty("total_value")]
        public float TotalValue { get; set; }

        [JsonProperty("products_total_value")]
        public float ProductsTotalValue { get; set; }


        [JsonProperty("tax_code")]
        public string TaxCode { get; set; } = string.Empty;

    }

}
