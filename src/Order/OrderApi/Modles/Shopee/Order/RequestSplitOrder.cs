using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Order
{
    public class RequestSplitOrder : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/order/split_order";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonProperty("order_sn ")]
        public string OrderSn { get; set; } = string.Empty;

        [JsonProperty("package_list ")]
        public IEnumerable<Cls_ItemList> package_list { get; set; } = Enumerable.Empty<Cls_ItemList>();


    }

    public class Cls_ItemList
    {
        [JsonProperty("item_list ")]
        public IEnumerable<Item> ItemList { get; set; } = Enumerable.Empty<Item>();
    }

    public class Item
    {
        [JsonProperty("item_id")]
        public int ItemId { get; set; }

        [JsonProperty("model_id")]
        public int ModelId { get; set; }

        [JsonProperty("order_item_id")]
        public int OrderItemId { get; set; }

        [JsonProperty("promotion_group_id")]
        public int PromotionGroupId { get; set; }
    }
}