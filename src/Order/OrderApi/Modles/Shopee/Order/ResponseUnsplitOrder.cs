using Newtonsoft.Json;

namespace OrderSDK.Modles.Shopee.Order
{
    public class ResponseUnsplitOrder
    {
        [JsonProperty("request_id")]
        public string RequestId { get; set; } = string.Empty;

        [JsonProperty("message")]
        public string Message { get; set; } = string.Empty;

        [JsonProperty("error")]
        public string Error { get; set; } = string.Empty;
    }
}