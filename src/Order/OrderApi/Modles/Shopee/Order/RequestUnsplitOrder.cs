using Newtonsoft.Json;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Order
{
    public class RequestUnsplitOrder : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/order/unsplit_order";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonProperty("order_sn ")]
        public string OrderSn { get; set; } = string.Empty;
    }
}