using Newtonsoft.Json;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Order
{
    public class RequestGetShipmentList : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/order/get_shipment_list";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonProperty("cursor")]
        public string Cursor { get; set; } = string.Empty;

        [JsonProperty("page_size ")]  //20
        public int Page_size { get; set; }
    }
}