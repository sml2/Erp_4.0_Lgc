using Newtonsoft.Json;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Order
{
    public class RequestGetOrderDetail : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/order/get_order_detail";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

        [JsonProperty("order_sn_list ")]
        public string[] OrderSnList { get; set; } = Array.Empty<string>();

        [JsonProperty("response_optional_fields")]
        public string[] ResponseOptionalFields { get; set; } = Array.Empty<string>();
    }
}