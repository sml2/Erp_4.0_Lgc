using Newtonsoft.Json;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Order
{
    public class RequestUploadInvoiceDoc : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/order/upload_invoice_doc";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonProperty("order_sn ")]
        public string OrderSn { get; set; } = string.Empty;

        [JsonProperty("file_type ")]
        public long FileType { get; set; }

        [JsonProperty("file ")]
        public byte[] File { get; set; } = Array.Empty<byte>();
    }
}