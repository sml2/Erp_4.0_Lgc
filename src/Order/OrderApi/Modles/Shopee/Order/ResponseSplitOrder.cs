using Newtonsoft.Json;
using System.Collections.Generic;

namespace OrderSDK.Modles.Shopee.Order
{
    public class ResponseSplitOrder
    {
        [JsonProperty("error")]
        public string Error { get; set; } = string.Empty;

        [JsonProperty("message")]
        public string Message { get; set; } = string.Empty;

        [JsonProperty("request_id")]
        public string RequestId { get; set; } = string.Empty;

        public Response response { get; set; } = new();

        public class Response
        {
            [JsonProperty("order_sn")]
            public string OrderSn { get; set; } = string.Empty;

            [JsonProperty("package_list")]
            public IEnumerable<Package> PackageList { get; set; } = Enumerable.Empty<Package>();
        }

        public class Package
        {
            [JsonProperty("package_number")]
            public string PackageNumber { get; set; } = string.Empty;

            [JsonProperty("item_list")]
            public IEnumerable<Item> ItemList { get; set; } = Enumerable.Empty<Item>();
        }

        public class Item
        {
            [JsonProperty("item_id")]
            public int ItemId { get; set; }

            [JsonProperty("model_id")]
            public int ModelId { get; set; }

            [JsonProperty("order_item_id")]
            public int OrderItemId { get; set; }

            [JsonProperty("promotion_group_id")]
            public int PromotionGroupId { get; set; }
        }
    }
}