using Newtonsoft.Json;
using System.Net.Http;
namespace OrderSDK.Modles.Shopee.Order
{
    public class RequestSetNote : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/order/set_note";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonProperty("order_sn")]
        public string OrderSn { get; set; } = string.Empty;

        [JsonProperty("note")]
        public string Note { get; set; } = string.Empty;
    }
}