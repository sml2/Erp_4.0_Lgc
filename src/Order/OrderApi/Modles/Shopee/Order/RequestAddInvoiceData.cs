using Newtonsoft.Json;
using System;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Order
{
    public class RequestAddInvoiceData : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/order/add_invoice_data";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonProperty("order_sn ")]
        public string OrderSn { get; set; } = string.Empty;

        public Cls_InvoiceData InvoiceData { get; set; } = new();

        public class Cls_InvoiceData
        {
            [JsonProperty("number")]
            public string Number { get; set; } = string.Empty;

            [JsonProperty("series_number")]
            public string SeriesNumber { get; set; } = string.Empty;

            [JsonProperty("access_key")]
            public string AccessKey { get; set; } = string.Empty;


            [JsonProperty("issue_date")]
            public DateTime IssueDate { get; set; }


            [JsonProperty("total_value")]
            public float TotalValue { get; set; }

            [JsonProperty("products_total_value")]
            public float ProductsTotalValue { get; set; }

            [JsonProperty("tax_code")]
            public string TaxCode { get; set; } = string.Empty;
        }
    }
}