using Newtonsoft.Json;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Order
{
    public class RequestHandleBuyerCancellation : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/order/handle_buyer_cancellation";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonProperty("order_sn")]
        public string OrderSn { get; set; } = string.Empty;

        [JsonProperty("operation")]
        public string Operation { get; set; } = string.Empty;
    }
}