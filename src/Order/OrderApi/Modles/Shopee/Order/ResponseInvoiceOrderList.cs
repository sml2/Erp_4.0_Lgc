using Newtonsoft.Json;
using System.Collections.Generic;

namespace OrderSDK.Modles.Shopee.Order
{
    public class ResponseInvoiceOrderList
    {
        [JsonProperty("request_id")]
        public string RequestId { get; set; } = string.Empty;

        [JsonProperty("error")]
        public string error { get; set; } = string.Empty;

        [JsonProperty("message")]
        public string Message { get; set; } = string.Empty;

        public Response response { get; set; } = new();

        public class Response
        {
            [JsonProperty("more")]
            public bool More { get; set; }

            [JsonProperty("next_cursor")]
            public string NextCursor { get; set; } = string.Empty;

            [JsonProperty("order_list")]
            public IEnumerable<Order> OrderList { get; set; } = Enumerable.Empty<Order>();

        }

        public class Order
        {
            [JsonProperty("order_sn")]
            public string OrderSn { get; set; } = string.Empty;
        }

    }
}