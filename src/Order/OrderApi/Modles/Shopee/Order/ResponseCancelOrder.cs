using Newtonsoft.Json;
using System;

namespace OrderSDK.Modles.Shopee.Order
{
    public class ResponseCancelOrder
    {
        [JsonProperty("request_id")]
        public string RequestId { get; set; } = string.Empty;

        [JsonProperty("error")]
        public string Error { get; set; } = string.Empty;

        [JsonProperty("message")]
        public string Message { get; set; } = string.Empty;

        [JsonProperty("response")]
        public Cls_Response Response { get; set; } = new();

        public class Cls_Response
        {
            [JsonProperty("update_time")]
            public DateTime UpdateTime { get; set; }
        }
    }
}