using Newtonsoft.Json;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Order
{

    public class RequestDownloadInvoiceDoc : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/order/download_invoice_doc";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

        [JsonProperty("order_sn ")]
        public string OrderSn { get; set; } = string.Empty;
    }
}