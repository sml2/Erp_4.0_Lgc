using Newtonsoft.Json;
using System.Net.Http;

namespace OrderSDK.Modles.Shopee.Order
{
    public class RequestGetOrderList : RequestShopee
    {
        [JsonIgnore]
        public override string EndPoint => "/api/v2/order/get_order_list";

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;

        [JsonProperty("time_range_field")]
        public string TimeRangeField { get; set; } = string.Empty;

        [JsonProperty("time_from")]
        public long TimeFrom { get; set; }

        [JsonProperty("time_to")]
        public long TimeTo { get; set; }

        [JsonProperty("page_size")]
        public int PageSize { get; set; }

        [JsonProperty("cursor")]
        public string Cursor { get; set; } = string.Empty;

        [JsonProperty("order_status")]
        public string OrderStatus { get; set; } = string.Empty;

        [JsonProperty("response_optional_fields")]
        public string ResponseOptionalFields { get; set; } = string.Empty;
    }
}