using System.Runtime.Serialization;

namespace OrderSDK.Modles.Shopee.Enums
{
    public enum EShopeeBrandStatus
    {
        /// <summary>
        /// 普通品牌
        /// </summary>  
        [EnumMember(Value = "1")]
        NORMAL = 1,
        /// <summary>
        /// 待定品牌
        /// </summary> 
        [EnumMember(Value = "2")]
        PENDING

    }
}