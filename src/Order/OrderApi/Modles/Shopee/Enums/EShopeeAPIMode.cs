using static EnumExtension;

namespace OrderSDK.Modles.Shopee.Enums
{


    public enum EShopeeAPIMode
    {
        [Description("SANDBOX")]
        SANDBOX,
        [Description("PRODUCTION")]
        PRODUCTION
    }

    public enum EShopeeCommonParamType
    {
        COMMON = 1,
        SHOP = 2,
        MERCHANT = 3,
    }
}