﻿using System.Runtime.Serialization;

namespace OrderSDK.Modles.Shopee.Enums
{
    public enum EShopeeItemStatus
    {
        [EnumMember(Value = "NORMAL")]
        NORMAL,
        [EnumMember(Value = "BANNED")]
        BANNED,
        [EnumMember(Value = "DELETED")]
        DELETED,
        [EnumMember(Value = "UNLIST")]
        UNLIST
    }
}
