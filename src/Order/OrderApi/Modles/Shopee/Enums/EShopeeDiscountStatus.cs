﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace OrderSDK.Modles.Shopee.Enums
{
    public enum EShopeeDiscountStatus
    {
        [EnumMember(Value = "upcoming")]
        UPCOMING,
        [EnumMember(Value = "ongoing")]
        ONGOING,
        [EnumMember(Value = "expired")]
        EXPIRED,
        [EnumMember(Value = "all")]
        ALL
    }
}
