using Newtonsoft.Json;
using OrderSDK.Modles.CommonType;
using OrderSDK.Modles.Shopee;
using OrderSDK.Modles.Shopee.Enums;
using OrderSDK.Services.Shopee.Validator.Attributes;
using System.Reflection;
using static OrderSDK.Modles.CommonType.StoreRegion;

namespace OrderSDK.Modles;
public class RequestShopee : RequestBase
{
    public virtual string EndPoint => throw new NotImplementedException();

    [JsonIgnore]
    public virtual Dictionary<string, string> headers { get; set; } = new();
   
    /// <summary>
    /// 参数类型
    /// </summary>
    [JsonIgnore]
    public virtual EShopeeCommonParamType CommonParamType { get; set; }
    = EShopeeCommonParamType.SHOP;

    //[JsonIgnore]
    public  long? MerchantId { get; set; }

    [JsonIgnore]
    [JsonProperty("shop_id")]
    public virtual  int? ShopId { get; set; }

    [JsonIgnore]
    public int storeID { get; set; }

    [JsonIgnore]
    public  ShopeeData? shopeedata { get; set; }

    //[JsonIgnore]
    //public string AccessToken { get; set; } = string.Empty;

    //[JsonIgnore]
    //public string RefreshAccessToken { get; set; } = string.Empty;

    //[JsonIgnore]
    //public int Expires_in { get; set; }

    //[JsonIgnore]
    //public DateTime Time { get; set; }

    ///// <summary>
    ///// 是不是表单数据提交方式：图片，视频，音频
    ///// </summary>
    //[JsonIgnore]
    //public virtual bool IsFormData { get; set; } = false;

    [JsonIgnore]
    public virtual byte[] ImageByte { get; set; }=Array.Empty<byte>();

    [JsonIgnore]
    public virtual string Image { get; set; } = string.Empty;

  

  

    public virtual string Check()
    {
        string msg = string.Empty;
        var props = GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
        foreach (var prop in props)
        {
            foreach (var attr in prop.GetCustomAttributes<AbsValidatorAttribute>(true))
            {
                if (!attr.check(prop.GetValue(this)))
                {
                    msg = $"{prop.Name} check fail because [{attr}]";
                    break;
                }
            }
        }
        return msg;
    }
}
