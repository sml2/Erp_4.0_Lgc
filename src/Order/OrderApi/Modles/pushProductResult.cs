using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderSDK.Modles
{
    public class pushProductResult
    {
        public string? originalRecordNumber { get; set; }
        public string? sku { get; set; }

        public string? errorCode { get; set; }

        public string? errorType { get; set; }
        public string? errorMessage { get; set; }

        public override string ToString()
        {
            return $"sku:{sku}/t,errorCode:{errorCode}/t,errorType:{errorType}/t,errorMessage:{errorMessage}/n";
        }
    }
}
