﻿using OrderSDK.Modles.Amazon.Enums;

namespace OrderSDK.Modles.Amazon.Sellers
{
    public class GetMarketplaceParticipationsRequest : RequestAmazonSP
    {
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.Sellers_GetMarketplaceParticipations;
        public override HttpMethod Method => HttpMethod.Get;

        public override string Url => $"/sellers/v1/marketplaceParticipations";
    }
}
