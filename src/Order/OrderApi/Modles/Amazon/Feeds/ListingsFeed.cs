﻿
using Newtonsoft.Json;

namespace OrderSDK.Modles.Amazon.Feeds
{
    public class ListingFeed<T>
    {
        [JsonProperty("header")]
        public Header _Header { get; set; }

        public class Header
        {
            [JsonProperty("sellerId")]
            public string SellerId { get; set; }
            //"version": "2.0",
            [JsonProperty("version")]
            public string Version { get; set; } = "2.0";
            //"issueLocale": "en_US"
            [JsonProperty("issueLocale")]
            public string IssueLocale { get; set; } = "en_US";
        }
        [JsonProperty("messages")]
        public List<MessageInfo<T>> Messages { get; set; }
    }
    public class MessageInfo<T>
    {
        [JsonProperty("messageId")]
        public int MessageId { get; set; }

        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("operationType")]
        public string OperationType { get; set; }

        [JsonProperty("productType")]
        public string ProductType { get; set; }


        [JsonProperty("requirements")]
        public string Requirements { get; set; }

        [JsonProperty("attributes")]
        public T Attributes { get; set; }  //JObject
    }
}
