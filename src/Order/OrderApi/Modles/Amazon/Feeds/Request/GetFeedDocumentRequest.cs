﻿using OrderSDK.Modles.Amazon.Enums;

namespace OrderSDK.Modles.Amazon.Feeds
{
    public class GetFeedDocumentRequest : RequestAmazonSP
    {
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.Feed_GetFeedDocument;
        public override HttpMethod Method => HttpMethod.Get;

        public override string Url => $"/feeds/2021-06-30/documents/{feedDocumentId}";

        public string feedDocumentId { get; set; }
    }
}
