﻿using OrderSDK.Modles.Amazon.Enums;

namespace OrderSDK.Modles.Amazon.Feeds
{
    public class GetFeedRequest : RequestAmazonSP
    {
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.Feed_CreateFeed;
        public override HttpMethod Method => HttpMethod.Get;

        public override string Url => $"/feeds/2021-06-30/feeds/{feedId}";

        public string feedId { get; set; }
    }

    public class CancelFeedRequest : RequestAmazonSP
    {
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.Feed_CancelFeed;
        public override HttpMethod Method => HttpMethod.Delete;

        public string feedId { get; set; }

        public override string Url => $"/feeds/2021-06-30/feeds/{feedId}";
    }
}
