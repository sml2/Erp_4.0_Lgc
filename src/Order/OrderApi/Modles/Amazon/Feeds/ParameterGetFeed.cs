﻿using OrderSDK.Modles.Amazon.Enums;

namespace OrderSDK.Modles.Amazon.Feeds
{
    public class ParameterGetFeed : RequestAmazonSP
    {
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.Feed_GetFeeds;
        public override HttpMethod Method => HttpMethod.Get;

        public override string Url => $"/feeds/2021-06-30/feeds";

        public IList<FeedType> feedTypes { get; set; }
        public IList<string> marketplaceIds { get; set; }
        public int? pageSize { get; set; }
        public ProcessingStatuses? processingStatuses { get; set; }
        public DateTime? createdSince { get; set; }
        public DateTime? createdUntil { get; set; }
        public string nextToken { get; set; }
    }
}
