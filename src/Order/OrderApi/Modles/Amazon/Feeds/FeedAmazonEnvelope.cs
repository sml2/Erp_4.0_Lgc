﻿using System.Xml.Serialization;
using Newtonsoft.Json;
using OrderSDK.Modles.Amazon.Enums;
using OrderSDK.Modles.Amazon.Feeds.ConstructFeed;
using OrderSDK.Modles.Amazon.Feeds.ConstructFeed.Messages;

namespace OrderSDK.Modles.Amazon.Feeds
{
    [XmlRoot(ElementName = "AmazonEnvelope")]
    [Serializable]
    public class FeedAmazonEnvelope
    {

        [XmlElement(Order = 1)]
        public FeedHeader Header { get; set; }

        [XmlElement(Order = 2)]
        public FeedMessageType? MessageType { get; set; }

        [XmlElement(Order = 3)]
        public bool? PurgeAndReplace { get; set; }
        [XmlIgnore]
        public bool PurgeAndReplaceSpecified { get { return PurgeAndReplace.HasValue; } }

        [XmlElement(ElementName = "Message", Order = 4)]
        public List<BaseMessage> Message;
    }

    [Serializable]
    public class FeedResult
    {
        [JsonProperty("header")]
        public Header header { get; set; }

        [JsonProperty("issues")]
        public List<Issue> issues { get; set; }

        [JsonProperty("summary")]
        public Summary summary { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public class IndexInfo
    {
        public IndexInfo(int index, string sku)
        {
            Index = index;
            Sku = sku;
        }

        public int Index { get; set; }

        public string Sku { get; set; }
    }
}
