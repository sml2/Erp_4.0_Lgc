﻿using System.Xml.Serialization;

namespace OrderSDK.Modles.Amazon.Feeds.ConstructFeed.Messages
{
    public partial class DescriptionData
    {
        public string Title { get; set; }
        public string Brand { get; set; }

        public string Description { get; set; }

        [XmlElement(ElementName = "BulletPoint")]
        public List<string> BulletPoint { get; set; }
        public int? MaxAggregateShipQuantity { get; set; }
        public int? MaxOrderQuantity { get; set; }

        /// <summary>
        /// 生产厂家 必须值
        /// </summary>
        public string Manufacturer { get; set; }
        public string MfrPartNumber { get; set; }
        public string ModelNumber { get; set; }
        [XmlElement(ElementName = "SearchTerms")]
        public List<string> SearchTerms { get; set; }
        //public string ExternalProductInformation { get; set; }
        public string ItemType { get; set; }
        /// <summary>
        /// ToysBaby>>ToysAndGames必有字段
        /// </summary>
        public string TargetAudience { get; set; }
        public string IsGiftWrapAvailable { get; set; }
        public string IsGiftMessageAvailable { get; set; }
        /// <summary>
        /// 必须值
        /// </summary>
        public string RecommendedBrowseNode { get; set; }
        [XmlElement("ItemWeight")]
        public WeightDimension ItemWeight { get; set; }
        public class WeightDimension
        {
            [XmlAttribute(AttributeName = "unitOfMeasure")]
            public string unitOfMeasure { get; set; }
            [XmlText]
            public string WeightValue { get; set; }
        }
        public string CountryOfOrigin { get; set; }
        public bool IsExpirationDatedProduct { get; set; }

    }


}
