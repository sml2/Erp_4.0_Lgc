﻿namespace OrderSDK.Modles.Amazon.Feeds.ConstructFeed.Messages
{
    public partial class OfferMessage
    {
        public string SKU { get; set; }

        public StandardProductID StandardProductID { get; set; }

        public Condition Condition { get; set; }
    }
}
