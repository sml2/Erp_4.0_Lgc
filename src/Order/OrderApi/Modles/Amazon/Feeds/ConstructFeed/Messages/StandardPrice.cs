﻿using System.Xml.Serialization;

namespace OrderSDK.Modles.Amazon.Feeds.ConstructFeed.Messages
{
    public class StandardPrice
    {
        [XmlText]
        public string Value { get; set; }
        [XmlAttribute]
        public string currency { get; set; }
    }
}
