﻿namespace OrderSDK.Modles.Amazon.Feeds.ConstructFeed.Messages
{
    public class StandardProductID
    {
        public string Type { get; set; }

        public string Value { get; set; }
    }
}
