﻿namespace OrderSDK.Modles.Amazon.Feeds.ConstructFeed.Messages
{
    public class PriceMessage
    {
        public string SKU { get; set; }

        public StandardPrice StandardPrice { get; set; }
    }
}
