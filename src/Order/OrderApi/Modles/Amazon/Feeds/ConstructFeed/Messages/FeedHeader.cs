﻿using Newtonsoft.Json;
using System;
using System.Xml.Serialization;

namespace OrderSDK.Modles.Amazon.Feeds.ConstructFeed.Messages
{
    [Serializable]
    public class FeedHeader
    {
        [XmlElement]
        public string DocumentVersion { get; set; }

        [XmlElement]
        public string MerchantIdentifier { get; set; }
    }

    [Serializable]
    public class Header
    {
        [JsonProperty("sellerId")]
        public string SellerId { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }

        [JsonProperty("feedId")]
        public string FeedId { get; set; }
    }

    [Serializable]
    public class Issue
    {
        [JsonProperty("messageId")]
        public int MessageId { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("severity")]
        public string Severity { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }

    [Serializable]
    public class Summary
    {
        [JsonProperty("errors")]
        public int Errors { get; set; }

        [JsonProperty("warnings")]
        public int Warnings { get; set; }

        [JsonProperty("messagesProcessed")]
        public int MessagesProcessed { get; set; }

        [JsonProperty("messagesAccepted")]
        public int MessagesAccepted { get; set; }

        [JsonProperty("messagesInvalid")]
        public int MessagesInvalid { get; set; }
    }
}
