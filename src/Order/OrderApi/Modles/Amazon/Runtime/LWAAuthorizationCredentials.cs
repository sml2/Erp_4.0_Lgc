﻿namespace OrderSDK.Modles.Amazon.Runtime
{
    public class LWAAuthorizationCredentials
    {
        public LWAAuthorizationCredentials()
        {
            Scopes = new List<string>();
        }

        /**
         * LWA Client Id
         */
        public string ClientId { get; set; } = string.Empty;

        /**
         * LWA Client Secret
         */
        public string ClientSecret { get; set; } = string.Empty;

        /**
         * LWA Refresh Token
         */
        public string RefreshToken { get; set; } = string.Empty;

        /**
         * LWA Authorization Server Endpoint
         */
        public Uri Endpoint { get; set; } = default!;

        /**
         * LWA Authorization Scopes
         */
        public List<string> Scopes { get; set; } = Enumerable.Empty<string>().ToList();
    }
}
