
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace OrderSDK.Modles.Amazon.Runtime
{
    public interface IDateHelper
    {
        DateTime GetUtcNow();
    }

    public class SigningDateHelper : IDateHelper
    {
        public DateTime GetUtcNow()
        {
            return DateTime.UtcNow;
        }
    }

    public class AWSSignerHelper
    {
        public const string ISO8601BasicDateTimeFormat = "yyyyMMddTHHmmssZ";
        public const string ISO8601BasicDateFormat = "yyyyMMdd";

        public const string XAmzDateHeaderName = "X-Amz-Date";
        public const string AuthorizationHeaderName = "Authorization";
        public const string CredentialSubHeaderName = "Credential";
        public const string SignatureSubHeaderName = "Signature";
        public const string SignedHeadersSubHeaderName = "SignedHeaders";
        public const string HostHeaderName = "host";

        public const string Scheme = "AWS4";
        public const string Algorithm = "HMAC-SHA256";
        public const string TerminationString = "aws4_request";
        public const string ServiceName = "execute-api";
        public const string Slash = "/";

        private readonly static Regex CompressWhitespaceRegex = new Regex("\\s+");

        public virtual IDateHelper DateHelper { get; set; }

        public AWSSignerHelper()
        {
            DateHelper = new SigningDateHelper();
        }

        /// <summary>
        /// 返回绝对路径的URI编码版本
        /// </summary>
        /// <param name="resource">Resource path(absolute path) from the request</param>
        /// <returns>URI encoded version of absolute path</returns>
        public virtual string ExtractCanonicalURIParameters(string resource)
        {
            string canonicalUri = string.Empty;

            if (string.IsNullOrEmpty(resource))
            {
                canonicalUri = Slash;
            }
            else
            {
                if (!resource.StartsWith(Slash))
                {
                    canonicalUri = Slash;
                }
                //Split path at / into segments
                IEnumerable<string> encodedSegments = resource.Split(new char[] { '/' }, StringSplitOptions.None);

                // Encode twice
                //use Utils.UrlDecode before Encode twice
                encodedSegments = encodedSegments.Select(segment => Utils.UrlEncode(Utils.UrlDecode(segment)));
                encodedSegments = encodedSegments.Select(segment => Utils.UrlEncode(segment));

                canonicalUri += string.Join(Slash, encodedSegments.ToArray());
            }

            return canonicalUri;
        }

        /// <summary>
        /// Returns query parameters in canonical order with URL encoding
        /// </summary>
        /// <param name="request">RestRequest</param>
        /// <returns>Query parameters in canonical order with URL encoding</returns>
        public virtual string ExtractCanonicalQueryString(RequestAmazonSP request)
        {
            if (request.QueryRequest)
            {
                Dictionary<string, string> queryParameters = request.ToQueryString();
                SortedDictionary<string, string> sortedqueryParameters = new SortedDictionary<string, string>(queryParameters);

                StringBuilder canonicalQueryString = new StringBuilder();
                foreach (var key in sortedqueryParameters.Keys)
                {
                    if (canonicalQueryString.Length > 0)
                    {
                        canonicalQueryString.Append("&");
                    }
                    canonicalQueryString.AppendFormat("{0}={1}",
                        Utils.UrlEncode(key),
                        Utils.UrlEncode(sortedqueryParameters[key]));
                }

                return canonicalQueryString.ToString();
            }
            return "";
        }

        /// <summary>
        /// 以规范的顺序返回Http头，所有头名称为小写
        /// </summary>
        /// <param name="request">RestRequest</param>
        /// <returns>Returns Http headers in canonical order</returns>
        public virtual string ExtractCanonicalHeaders(Dictionary<string, string> dictionary)
        {
            SortedDictionary<string, string> sortedHeaders = new SortedDictionary<string, string>(dictionary);

            StringBuilder headerString = new StringBuilder();

            foreach (string headerName in sortedHeaders.Keys)
            {
                if (headerName.Equals("ProxyUrl", StringComparison.CurrentCultureIgnoreCase))
                {
                    continue;
                }
                headerString.AppendFormat("{0}:{1}\n",
                    headerName.ToLower(),
                    CompressWhitespaceRegex.Replace(sortedHeaders[headerName].Trim(), " "));
            }

            return headerString.ToString();
        }

        /// <summary>
        /// 按规范顺序返回Http头的列表（作为字符串）
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public virtual string ExtractSignedHeaders(Dictionary<string, string> dictionary)
        {
            List<string> rawHeaders = dictionary.Select(header => header.Key.Trim().ToLowerInvariant())
                                                        .ToList();
            rawHeaders.Sort(StringComparer.OrdinalIgnoreCase);
            return string.Join(";", rawHeaders);
        }

        /// <summary>
        /// 返回请求体中有效负载的十六进制散列值（使用SHA256）
        /// </summary>
        /// <param name="request">RestRequest</param>
        /// <returns>Hexadecimal hashed value of payload in the body of request</returns>
        public virtual string HashRequestBody(RequestAmazonSP request)
        {
            if (request.BodyRequest)
            {
                //以下stringContent和streamContent 都可以
                //var stream = new MemoryStream(Encoding.UTF8.GetBytes(request.ToPayload()));
                //return Utils.ToHex(Utils.Hash(stream));

                return Utils.ToHex(Utils.Hash(request.ToPayload()));
            }
            return Utils.ToHex(Utils.Hash(""));
        }

        /// <summary>
        /// Builds the string for signing using signing date, hashed canonical request and region
        /// </summary>
        /// <param name="signingDate">Signing Date</param>
        /// <param name="hashedCanonicalRequest">Hashed Canonical Request</param>
        /// <param name="region">Region</param>
        /// <returns>String to be used for signing</returns>
        public virtual string BuildStringToSign(DateTime signingDate, string hashedCanonicalRequest, string region)
        {
            string scope = BuildScope(signingDate, region);
            string stringToSign = string.Format(CultureInfo.InvariantCulture, "{0}-{1}\n{2}\n{3}\n{4}",
                Scheme,
                Algorithm,
                signingDate.ToString(ISO8601BasicDateTimeFormat, CultureInfo.InvariantCulture),
                scope,
                hashedCanonicalRequest);

            return stringToSign;
        }

        /// <summary>
        /// 设置AWS4强制的“x-amz-date”标题，返回将在整个签名过程中使用的日期/时间。
        /// </summary>
        /// <param name="request"></param>
        /// <param name="host"></param>
        /// <returns></returns>
        public virtual DateTime InitializeHeaders(RequestAmazonSP request, string host)
        {
            if (request.Headers.ContainsKey(XAmzDateHeaderName))
            {
                request.Headers.Remove(XAmzDateHeaderName);
            }

            DateTime signingDate = DateHelper.GetUtcNow();

            request.Headers.Add(XAmzDateHeaderName, signingDate.ToString(ISO8601BasicDateTimeFormat,
                CultureInfo.InvariantCulture));

            if (request.Headers.ContainsKey(HostHeaderName))
            {
                request.Headers.Remove(HostHeaderName);
            }
            request.Headers.Add(HostHeaderName, host);
            return signingDate;
        }

        /// <summary>
        /// Calculates AWS4 signature for the string, prepared for signing
        /// </summary>
        /// <param name="stringToSign">String to be signed</param>
        /// <param name="signingDate">Signing Date</param>
        /// <param name="secretKey">Secret Key</param>
        /// <param name="region">Region</param>
        /// <returns>AWS4 Signature</returns>
        public virtual string CalculateSignature(string stringToSign,
                                                 DateTime signingDate,
                                                 string secretKey,
                                                 string region)
        {
            string date = signingDate.ToString(ISO8601BasicDateFormat, CultureInfo.InvariantCulture);
            byte[] kSecret = Encoding.UTF8.GetBytes(Scheme + secretKey);
            byte[] kDate = Utils.GetKeyedHash(kSecret, date);
            byte[] kRegion = Utils.GetKeyedHash(kDate, region);
            byte[] kService = Utils.GetKeyedHash(kRegion, ServiceName);
            byte[] kSigning = Utils.GetKeyedHash(kService, TerminationString);

            // Calculate the signature
            return Utils.ToHex(Utils.GetKeyedHash(kSigning, stringToSign));
        }

        /// <summary>
        /// 以“授权”标题的形式向请求添加签名
        /// </summary>
        /// <param name="restRequest">Request to be signed</param>
        /// <param name="accessKeyId">Access Key Id</param>
        /// <param name="signedHeaders">Signed Headers</param>
        /// <param name="signature">The signature to add</param>
        /// <param name="region">AWS region for the request</param>
        /// <param name="signingDate">Signature date</param>
        public virtual void AddSignature(RequestAmazonSP restRequest,
                                         string accessKeyId,
                                         string signedHeaders,
                                         string signature,
                                         string region,
                                         DateTime signingDate)
        {
            string scope = BuildScope(signingDate, region);
            StringBuilder authorizationHeaderValueBuilder = new StringBuilder();
            authorizationHeaderValueBuilder.AppendFormat("{0}-{1}", Scheme, Algorithm);
            authorizationHeaderValueBuilder.AppendFormat(" {0}={1}/{2},", CredentialSubHeaderName, accessKeyId, scope);
            authorizationHeaderValueBuilder.AppendFormat(" {0}={1},", SignedHeadersSubHeaderName, signedHeaders);
            authorizationHeaderValueBuilder.AppendFormat(" {0}={1}", SignatureSubHeaderName, signature);

            if(restRequest.Headers.ContainsKey(AuthorizationHeaderName))
            {
                restRequest.Headers.Remove(AuthorizationHeaderName);
            }
            restRequest.Headers.Add(AuthorizationHeaderName, authorizationHeaderValueBuilder.ToString());
        }

        private static string BuildScope(DateTime signingDate, string region)
        {
            return string.Format("{0}/{1}/{2}/{3}",
                                 signingDate.ToString(ISO8601BasicDateFormat, CultureInfo.InvariantCulture),
                                 region,
                                 ServiceName,
                                 TerminationString);
        }
    }
}
