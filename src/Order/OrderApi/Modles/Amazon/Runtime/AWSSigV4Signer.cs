﻿using OrderSDK.Modles.Amazon.Authorization;
using System.Text;

namespace OrderSDK.Modles.Amazon.Runtime
{
    public class AWSSigV4Signer

    {
        public virtual AWSSignerHelper AwsSignerHelper { get; set; }
        private AWSAuthenticationCredentials awsCredentials;

        /// <summary>
        /// Constructor for AWSSigV4Signer
        /// </summary>
        /// <param name="awsAuthenticationCredentials">AWS Developer Account Credentials</param>
        public AWSSigV4Signer(AWSAuthenticationCredentials awsAuthenticationCredentials)
        {
            awsCredentials = awsAuthenticationCredentials;
            AwsSignerHelper = new AWSSignerHelper();
        }

        /// <summary>
        /// Signs a Request with AWS Signature Version 4
        /// </summary>
        /// <param name="request">RestRequest which needs to be signed</param>
        /// <param name="host">Request endpoint</param>
        /// <returns>RestRequest with AWS Signature</returns>
        public RequestAmazonSP Sign(RequestAmazonSP request, string host)
        {
            DateTime signingDate = AwsSignerHelper.InitializeHeaders(request, host);
            string signedHeaders = AwsSignerHelper.ExtractSignedHeaders(request.Headers);

            string hashedCanonicalRequest = CreateCanonicalRequest(request, signedHeaders);

            string stringToSign = AwsSignerHelper.BuildStringToSign(signingDate,
                                                                    hashedCanonicalRequest,
                                                                    awsCredentials.Region);

            string signature = AwsSignerHelper.CalculateSignature(stringToSign,
                                                                  signingDate,
                                                                  awsCredentials.SecretKey,
                                                                  awsCredentials.Region);

            AwsSignerHelper.AddSignature(request,
                                         awsCredentials.AccessKeyId,
                                         signedHeaders,
                                         signature,
                                         awsCredentials.Region,
                                         signingDate);

            return request;
        }

        private string CreateCanonicalRequest(RequestAmazonSP request, string signedHeaders)
        {
            var canonicalizedRequest = new StringBuilder();
            //Request Method
            canonicalizedRequest.AppendFormat("{0}\n", request.Method);

            //CanonicalURI
            canonicalizedRequest.AppendFormat("{0}\n", AwsSignerHelper.ExtractCanonicalURIParameters(request.Url));

            //throw new Exception("下两行代码都有问题，首先是查询参数错了，进而推出a[请求地址]和b[查询参数]有问题，其次是第二行c[Header名称大小写]和原程序的不同。可以下断点对比下两边差异");
            //a,b,c解决后，看文档发现这个/authorization/v1/authorizationCode并非商户授权所要的接口，而是“免授权操作”相关的接口，和我们授权业务无关

            //规范查询串
            canonicalizedRequest.AppendFormat("{0}\n", AwsSignerHelper.ExtractCanonicalQueryString(request));

            //规范头
            canonicalizedRequest.AppendFormat("{0}\n", AwsSignerHelper.ExtractCanonicalHeaders(request.Headers));

            //SignedHeaders
            canonicalizedRequest.AppendFormat("{0}\n", signedHeaders);

            // Hash(digest) the payload in the body
            canonicalizedRequest.AppendFormat(AwsSignerHelper.HashRequestBody(request));

            string canonicalRequest = canonicalizedRequest.ToString();

            //Create a digest(hash) of the canonical request
            return Utils.ToHex(Utils.Hash(canonicalRequest));
        }
    }
}
