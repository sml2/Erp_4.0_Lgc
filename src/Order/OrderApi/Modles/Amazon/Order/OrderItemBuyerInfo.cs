/* 
 * Selling Partner API for Orders
 *
 * The Selling Partner API for Orders helps you programmatically retrieve order information. These APIs let you develop fast, flexible, custom applications in areas like order synchronization, order research, and demand-based decision support tools.
 *
 * OpenAPI spec version: v0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Runtime.Serialization;
using System.Text;

namespace OrderSDK.Modles.Amazon.Order
{
    /// <summary>
    /// A single order item&#39;s buyer information.
    /// </summary>
    [DataContract]
    public partial class OrderItemBuyerInfo : IEquatable<OrderItemBuyerInfo>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrderItemBuyerInfo" /> class.
        /// </summary>
        [JsonConstructorAttribute]
        public OrderItemBuyerInfo() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="OrderItemBuyerInfo" /> class.
        /// </summary>
        /// <param name="OrderItemId">An Amazon-defined order item identifier. (required).</param>
        /// <param name="BuyerCustomizedInfo">Buyer information for custom orders from the Amazon Custom program..</param>
        /// <param name="GiftWrapPrice">The gift wrap price of the item..</param>
        /// <param name="GiftWrapTax">The tax on the gift wrap price..</param>
        /// <param name="GiftMessageText">A gift message provided by the buyer..</param>
        /// <param name="GiftWrapLevel">The gift wrap level specified by the buyer..</param>
        public OrderItemBuyerInfo(string OrderItemId = default(string)!, BuyerCustomizedInfoDetail BuyerCustomizedInfo = default(BuyerCustomizedInfoDetail)!, Money GiftWrapPrice = default(Money)!, Money GiftWrapTax = default(Money)!, string GiftMessageText = default(string)!, string GiftWrapLevel = default(string)!)
        {
            // to ensure "OrderItemId" is required (not null)
            if (OrderItemId == null)
            {
                throw new InvalidDataException("OrderItemId is a required property for OrderItemBuyerInfo and cannot be null");
            }
            else
            {
                this.OrderItemId = OrderItemId;
            }
            this.BuyerCustomizedInfo = BuyerCustomizedInfo;
            this.GiftWrapPrice = GiftWrapPrice;
            this.GiftWrapTax = GiftWrapTax;
            this.GiftMessageText = GiftMessageText;
            this.GiftWrapLevel = GiftWrapLevel;
        }

        /// <summary>
        /// An Amazon-defined order item identifier.
        /// </summary>
        /// <value>An Amazon-defined order item identifier.</value>
        [DataMember(Name = "OrderItemId", EmitDefaultValue = false)]
        public string OrderItemId { get; set; } = string.Empty;

        /// <summary>
        /// Buyer information for custom orders from the Amazon Custom program.
        /// </summary>
        /// <value>Buyer information for custom orders from the Amazon Custom program.</value>
        [DataMember(Name = "BuyerCustomizedInfo", EmitDefaultValue = false)]
        public BuyerCustomizedInfoDetail BuyerCustomizedInfo { get; set; } = default!;

        /// <summary>
        /// The gift wrap price of the item.
        /// </summary>
        /// <value>The gift wrap price of the item.</value>
        [DataMember(Name = "GiftWrapPrice", EmitDefaultValue = false)]
        public Money GiftWrapPrice { get; set; } = default!;

        /// <summary>
        /// The tax on the gift wrap price.
        /// </summary>
        /// <value>The tax on the gift wrap price.</value>
        [DataMember(Name = "GiftWrapTax", EmitDefaultValue = false)]
        public Money GiftWrapTax { get; set; } = default!;

        /// <summary>
        /// A gift message provided by the buyer.
        /// </summary>
        /// <value>A gift message provided by the buyer.</value>
        [DataMember(Name = "GiftMessageText", EmitDefaultValue = false)]
        public string GiftMessageText { get; set; } = string.Empty;

        /// <summary>
        /// The gift wrap level specified by the buyer.
        /// </summary>
        /// <value>The gift wrap level specified by the buyer.</value>
        [DataMember(Name = "GiftWrapLevel", EmitDefaultValue = false)]
        public string GiftWrapLevel { get; set; } = string.Empty;

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class OrderItemBuyerInfo {\n");
            sb.Append("  OrderItemId: ").Append(OrderItemId).Append("\n");
            sb.Append("  BuyerCustomizedInfo: ").Append(BuyerCustomizedInfo).Append("\n");
            sb.Append("  GiftWrapPrice: ").Append(GiftWrapPrice).Append("\n");
            sb.Append("  GiftWrapTax: ").Append(GiftWrapTax).Append("\n");
            sb.Append("  GiftMessageText: ").Append(GiftMessageText).Append("\n");
            sb.Append("  GiftWrapLevel: ").Append(GiftWrapLevel).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object? input)
        {
            return Equals(input as OrderItemBuyerInfo);
        }

        /// <summary>
        /// Returns true if OrderItemBuyerInfo instances are equal
        /// </summary>
        /// <param name="input">Instance of OrderItemBuyerInfo to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(OrderItemBuyerInfo? input)
        {
            if (input == null)
                return false;

            return
                (
                    OrderItemId == input.OrderItemId ||
                    OrderItemId != null &&
                    OrderItemId.Equals(input.OrderItemId)
                ) &&
                (
                    BuyerCustomizedInfo == input.BuyerCustomizedInfo ||
                    BuyerCustomizedInfo != null &&
                    BuyerCustomizedInfo.Equals(input.BuyerCustomizedInfo)
                ) &&
                (
                    GiftWrapPrice == input.GiftWrapPrice ||
                    GiftWrapPrice != null &&
                    GiftWrapPrice.Equals(input.GiftWrapPrice)
                ) &&
                (
                    GiftWrapTax == input.GiftWrapTax ||
                    GiftWrapTax != null &&
                    GiftWrapTax.Equals(input.GiftWrapTax)
                ) &&
                (
                    GiftMessageText == input.GiftMessageText ||
                    GiftMessageText != null &&
                    GiftMessageText.Equals(input.GiftMessageText)
                ) &&
                (
                    GiftWrapLevel == input.GiftWrapLevel ||
                    GiftWrapLevel != null &&
                    GiftWrapLevel.Equals(input.GiftWrapLevel)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (OrderItemId != null)
                    hashCode = hashCode * 59 + OrderItemId.GetHashCode();
                if (BuyerCustomizedInfo != null)
                    hashCode = hashCode * 59 + BuyerCustomizedInfo.GetHashCode();
                if (GiftWrapPrice != null)
                    hashCode = hashCode * 59 + GiftWrapPrice.GetHashCode();
                if (GiftWrapTax != null)
                    hashCode = hashCode * 59 + GiftWrapTax.GetHashCode();
                if (GiftMessageText != null)
                    hashCode = hashCode * 59 + GiftMessageText.GetHashCode();
                if (GiftWrapLevel != null)
                    hashCode = hashCode * 59 + GiftWrapLevel.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
