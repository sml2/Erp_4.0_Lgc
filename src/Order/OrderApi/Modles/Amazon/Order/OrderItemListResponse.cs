﻿using OrderSDK.Modles.Amazon.Order;

namespace OrderSDK.Modles.Amazon.Order;

public record OrderItemListResponse
{
    public bool HasNext { get; set; }
    public string NextCycleInfo { get; set; }
    public bool HasOrder { get => orderItems.IsNotEmpty(); }
    public OrderItemList? orderItems { get; set; }

    public OrderItemListResponse(bool hasNext, string nextCycleInfo, OrderItemList? orderItems, string? msg)
    {
        HasNext = hasNext;
        NextCycleInfo = nextCycleInfo;
        this.orderItems = orderItems;
        ErrorMsg = msg;
    }

    public string? ErrorMsg { get; set; }
    public OrderItemListResponse() : this(false, string.Empty, null, null) { }

}