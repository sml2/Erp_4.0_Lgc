﻿using Newtonsoft.Json;
using OrderSDK.Modles.Amazon.Authorization;
using OrderSDK.Modles.Amazon.Enums;
using System.ComponentModel.DataAnnotations;

namespace OrderSDK.Modles.Amazon.Order
{
    public class GetOrderItemsRequest : RequestAmazonSP
    {
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.Order_GetOrderItems;
        public override HttpMethod Method => HttpMethod.Get;

        public override string Url => $"/orders/v0/orders/{orderId}/orderItems";

        [JsonIgnore]
        public string orderId { get; set; } = string.Empty;


        public string NextToken { get; set; } = string.Empty;

        public bool IsNeedRestrictedDataToken { get; set; }
        public CreateRestrictedDataTokenRequest RestrictedDataTokenRequest { get; set; } = default!;
    }
}
