﻿using Newtonsoft.Json;
using OrderSDK.Modles.Amazon.Authorization;
using OrderSDK.Modles.Amazon.Enums;
using System.ComponentModel.DataAnnotations;

namespace OrderSDK.Modles.Amazon.Order
{
    public class GetOrderRequest : RequestAmazonSP
    {
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.Order_GetOrder;
        public override HttpMethod Method => HttpMethod.Get;

        public override string Url => $"/orders/v0/orders/{orderId}";

        [JsonIgnore]
        public string orderId { get; set; } = string.Empty;

        public bool IsNeedRestrictedDataToken { get; set; }
        public CreateRestrictedDataTokenRequest RestrictedDataTokenRequest { get; set; } = default!;
    }
}
