﻿using OrderSDK.Modles.Amazon.Enums;

namespace OrderSDK.Modles.Amazon.Order
{
    public class GetOrderAddressRequest : RequestAmazonSP
    {
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.Order_GetOrderAddress;
        public override HttpMethod Method => HttpMethod.Get;

        public override string Url => $"/orders/v0/orders/{orderId}/address";

        public string orderId { get; set; } = string.Empty;
    }

    public class GetOrderBuyerInfoRequest : RequestAmazonSP
    {
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.Order_GetOrderBuyerInfo;
        public override HttpMethod Method => HttpMethod.Get;

        public override string Url => $"/orders/v0/orders/{orderId}/buyerInfo";

        public string orderId { get; set; } = string.Empty;
    }
    
    public class GetOrderItemsBuyerInfoRequest : RequestAmazonSP
    {
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.Order_GetOrderItemsBuyerInfo;
        public override HttpMethod Method => HttpMethod.Get;

        public override string Url => $"/orders/v0/orders/{orderId}/orderItems/buyerInfo";

        public string orderId { get; set; } = string.Empty;
    }
}
