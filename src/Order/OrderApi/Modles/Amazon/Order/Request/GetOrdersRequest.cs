﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using OrderSDK.Modles.Amazon.Authorization;
using OrderSDK.Modles.Amazon.Enums;
using System.ComponentModel.DataAnnotations;
using static OrderSDK.Modles.Amazon.Order.Order;

namespace OrderSDK.Modles.Amazon.Order
{
    public class GetOrdersRequest : RequestAmazonSP
    {
        [JsonIgnore]
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.Order_GetOrders;

        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

        [JsonIgnore]
        public override string Url => "/orders/v0/orders";

        /// <summary>  
        /// CreatedAfter 和 LastUpdatedAfter  不能都是空的
        /// </summary>
        public string? CreatedAfter { get; set; }

        public string? CreatedBefore { get; set; }

        public string? LastUpdatedAfter { get; set; }

        public string? LastUpdatedBefore { get; set; }


        [JsonProperty(ItemConverterType = typeof(StringEnumConverter))]
        /// <summary>
        /// enum OrderStatuses
        /// </summary>
        public List<OrderStatusEnum> OrderStatuses { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "No more than 50")]
        public List<string> MarketplaceIds { get; set; } = Array.Empty<string>().ToList();

        [JsonProperty(ItemConverterType = typeof(StringEnumConverter))]
        /// <summary>
        /// enum FulfillmentChannel
        /// </summary>
        public List<FulfillmentChannelEnum>? FulfillmentChannels { get; set; }

        [JsonProperty(ItemConverterType = typeof(StringEnumConverter))]
        /// <summary>
        /// enum PaymentMethod 
        /// </summary>
        public List<PaymentMethodEnum>? PaymentMethods { get; set; }

        public string? BuyerEmail { get; set; }

        public string? SellerOrderId { get; set; }

        [MaxLength(100, ErrorMessage = "No more than 100")]
        [MinLength(1, ErrorMessage = "Cannot be less than 1")]
        public int? MaxResultsPerPage { get; set; } = 100;

        [JsonProperty(ItemConverterType = typeof(StringEnumConverter))]
        /// <summary>
        /// enum EasyShipmentStatus
        /// </summary>
        public List<EasyShipmentStatus>? EasyShipShipmentStatuses { get; set; }

        public string? NextToken { get; set; }

        [MaxLength(50, ErrorMessage = "No more than 50")]
        public List<string>? AmazonOrderIds { get; set; }

        public string? ActualFulfillmentSupplySourceId { get; set; }

        public bool? IsISPU { get; set; }

        public string? StoreChainStoreId { get; set; }

        [JsonIgnore]
        public bool IsNeedRestrictedDataToken { get; set; }
        [JsonIgnore]
        public CreateRestrictedDataTokenRequest RestrictedDataTokenRequest { get; set; } = default!;

    }
}
