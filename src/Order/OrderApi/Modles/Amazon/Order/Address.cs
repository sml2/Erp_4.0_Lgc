/* 
 * Selling Partner API for Orders
 *
 * The Selling Partner API for Orders helps you programmatically retrieve order information. These APIs let you develop fast, flexible, custom applications in areas like order synchronization, order research, and demand-based decision support tools.
 *
 * OpenAPI spec version: v0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using static EnumExtension;

namespace OrderSDK.Modles.Amazon.Order
{
    /// <summary>
    /// The shipping address for the order.
    /// </summary>
    [DataContract]
    public partial class Address : IEquatable<Address>, IValidatableObject
    {
        /// <summary>
        /// The address type of the shipping address.
        /// </summary>
        /// <value>The address type of the shipping address.</value>
        [JsonConverter(typeof(StringEnumConverter))]
        public enum AddressTypeEnum
        {

            /// <summary>  
            /// Enum Residential for value: Residential  送货地址是住宅地址
            /// </summary>          
            [EnumMember(Value = "Residential")]
            Residential = 1,

            /// <summary>
            /// Enum Commercial for value: Commercial 送货地址是商业地址
            /// </summary>            
            [EnumMember(Value = "Commercial")]
            Commercial = 2
        }

        /// <summary>
        /// The address type of the shipping address.
        /// </summary>
        /// <value>The address type of the shipping address.</value>
        [DataMember(Name = "AddressType", EmitDefaultValue = false)]
        public AddressTypeEnum? AddressType { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="Address" /> class.
        /// </summary>
        [JsonConstructorAttribute]
        public Address() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="Address" /> class.
        /// </summary>
        /// <param name="Name">The name. (required).</param>
        /// <param name="AddressLine1">The street address..</param>
        /// <param name="AddressLine2">Additional street address information, if required..</param>
        /// <param name="AddressLine3">Additional street address information, if required..</param>
        /// <param name="City">The city .</param>
        /// <param name="County">The county..</param>
        /// <param name="District">The district..</param>
        /// <param name="StateOrRegion">The state or region..</param>
        /// <param name="Municipality">The municipality..</param>
        /// <param name="PostalCode">The postal code..</param>
        /// <param name="CountryCode">The country code. A two-character country code, in ISO 3166-1 alpha-2 format..</param>
        /// <param name="Phone">The phone number. Not returned for Fulfillment by Amazon (FBA) orders..</param>
        /// <param name="AddressType">The address type of the shipping address..</param>
        public Address(string? Name, string AddressLine1 /*= default!*/, string AddressLine2 /*= default!*/, string AddressLine3 /*= default!*/, string City /*= default!*/, string County /*= default!*/, string District /*= default!*/, string StateOrRegion /*= default!*/, string Municipality /*= default!*/, string PostalCode /*= default!*/, string CountryCode /*= default!*/, string Phone /*= default!*/, AddressTypeEnum? AddressType/* = default(AddressTypeEnum)*/)
        {
            // to ensure "Name" is required (not null)
            if (Name == null)
            {
                throw new InvalidDataException("Name is a required property for Address and cannot be null");
            }
            else
            {
                this.Name = Name;
            }
            this.AddressLine1 = AddressLine1;
            this.AddressLine2 = AddressLine2;
            this.AddressLine3 = AddressLine3;
            this.City = City;
            this.County = County;
            this.District = District;
            this.StateOrRegion = StateOrRegion;
            this.Municipality = Municipality;
            this.PostalCode = PostalCode;
            this.CountryCode = CountryCode;
            this.Phone = Phone;
            this.AddressType = AddressType;
        }

        /// <summary>
        /// The name.
        /// </summary>
        /// <value>The name.</value>
        [DataMember(Name = "Name", EmitDefaultValue = false)]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// The street address.
        /// </summary>
        /// <value>The street address.</value>
        [DataMember(Name = "AddressLine1", EmitDefaultValue = false)]
        public string AddressLine1 { get; set; } = string.Empty;

        /// <summary>
        /// Additional street address information, if required.
        /// </summary>
        /// <value>Additional street address information, if required.</value>
        [DataMember(Name = "AddressLine2", EmitDefaultValue = false)]
        public string AddressLine2 { get; set; } = string.Empty;

        /// <summary>
        /// Additional street address information, if required.
        /// </summary>
        /// <value>Additional street address information, if required.</value>
        [DataMember(Name = "AddressLine3", EmitDefaultValue = false)]
        public string AddressLine3 { get; set; } = string.Empty;

        /// <summary>
        /// The city 
        /// </summary>
        /// <value>The city </value>
        [DataMember(Name = "City", EmitDefaultValue = false)]
        public string City { get; set; } = string.Empty;

        /// <summary>
        /// The county.
        /// </summary>
        /// <value>The county.</value>
        [DataMember(Name = "County", EmitDefaultValue = false)]
        public string County { get; set; } = string.Empty;

        /// <summary>
        /// The district.
        /// </summary>
        /// <value>The district.</value>
        [DataMember(Name = "District", EmitDefaultValue = false)]
        public string District { get; set; } = string.Empty;

        /// <summary>
        /// The state or region.
        /// </summary>
        /// <value>The state or region.</value>
        [DataMember(Name = "StateOrRegion", EmitDefaultValue = false)]
        public string StateOrRegion { get; set; } = string.Empty;

        /// <summary>
        /// The municipality.
        /// </summary>
        /// <value>The municipality.</value>
        [DataMember(Name = "Municipality", EmitDefaultValue = false)]
        public string Municipality { get; set; } = string.Empty;

        /// <summary>
        /// The postal code.
        /// </summary>
        /// <value>The postal code.</value>
        [DataMember(Name = "PostalCode", EmitDefaultValue = false)]
        public string PostalCode { get; set; } = string.Empty;

        /// <summary>
        /// The country code. A two-character country code, in ISO 3166-1 alpha-2 format.
        /// </summary>
        /// <value>The country code. A two-character country code, in ISO 3166-1 alpha-2 format.</value>
        [DataMember(Name = "CountryCode", EmitDefaultValue = false)]
        public string CountryCode { get; set; } = string.Empty;

        /// <summary>
        /// The phone number. Not returned for Fulfillment by Amazon (FBA) orders.
        /// </summary>
        /// <value>The phone number. Not returned for Fulfillment by Amazon (FBA) orders.</value>
        [DataMember(Name = "Phone", EmitDefaultValue = false)]
        public string Phone { get; set; } = string.Empty;


        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Address {\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  AddressLine1: ").Append(AddressLine1).Append("\n");
            sb.Append("  AddressLine2: ").Append(AddressLine2).Append("\n");
            sb.Append("  AddressLine3: ").Append(AddressLine3).Append("\n");
            sb.Append("  City: ").Append(City).Append("\n");
            sb.Append("  County: ").Append(County).Append("\n");
            sb.Append("  District: ").Append(District).Append("\n");
            sb.Append("  StateOrRegion: ").Append(StateOrRegion).Append("\n");
            sb.Append("  Municipality: ").Append(Municipality).Append("\n");
            sb.Append("  PostalCode: ").Append(PostalCode).Append("\n");
            sb.Append("  CountryCode: ").Append(CountryCode).Append("\n");
            sb.Append("  Phone: ").Append(Phone).Append("\n");
            sb.Append("  AddressType: ").Append(AddressType).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object? input)
        {
            if (input == null)
                return false;

            return Equals(input as Address);
        }

        /// <summary>
        /// Returns true if Address instances are equal
        /// </summary>
        /// <param name="input">Instance of Address to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Address? input)
        {
            if (input == null)
                return false;

            return
                (
                    Name == input.Name ||
                    Name != null &&
                    Name.Equals(input.Name)
                ) &&
                (
                    AddressLine1 == input.AddressLine1 ||
                    AddressLine1 != null &&
                    AddressLine1.Equals(input.AddressLine1)
                ) &&
                (
                    AddressLine2 == input.AddressLine2 ||
                    AddressLine2 != null &&
                    AddressLine2.Equals(input.AddressLine2)
                ) &&
                (
                    AddressLine3 == input.AddressLine3 ||
                    AddressLine3 != null &&
                    AddressLine3.Equals(input.AddressLine3)
                ) &&
                (
                    City == input.City ||
                    City != null &&
                    City.Equals(input.City)
                ) &&
                (
                    County == input.County ||
                    County != null &&
                    County.Equals(input.County)
                ) &&
                (
                    District == input.District ||
                    District != null &&
                    District.Equals(input.District)
                ) &&
                (
                    StateOrRegion == input.StateOrRegion ||
                    StateOrRegion != null &&
                    StateOrRegion.Equals(input.StateOrRegion)
                ) &&
                (
                    Municipality == input.Municipality ||
                    Municipality != null &&
                    Municipality.Equals(input.Municipality)
                ) &&
                (
                    PostalCode == input.PostalCode ||
                    PostalCode != null &&
                    PostalCode.Equals(input.PostalCode)
                ) &&
                (
                    CountryCode == input.CountryCode ||
                    CountryCode != null &&
                    CountryCode.Equals(input.CountryCode)
                ) &&
                (
                    Phone == input.Phone ||
                    Phone != null &&
                    Phone.Equals(input.Phone)
                ) &&
                (
                    AddressType == input.AddressType ||
                    AddressType != null &&
                    AddressType.Equals(input.AddressType)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (Name != null)
                    hashCode = hashCode * 59 + Name.GetHashCode();
                if (AddressLine1 != null)
                    hashCode = hashCode * 59 + AddressLine1.GetHashCode();
                if (AddressLine2 != null)
                    hashCode = hashCode * 59 + AddressLine2.GetHashCode();
                if (AddressLine3 != null)
                    hashCode = hashCode * 59 + AddressLine3.GetHashCode();
                if (City != null)
                    hashCode = hashCode * 59 + City.GetHashCode();
                if (County != null)
                    hashCode = hashCode * 59 + County.GetHashCode();
                if (District != null)
                    hashCode = hashCode * 59 + District.GetHashCode();
                if (StateOrRegion != null)
                    hashCode = hashCode * 59 + StateOrRegion.GetHashCode();
                if (Municipality != null)
                    hashCode = hashCode * 59 + Municipality.GetHashCode();
                if (PostalCode != null)
                    hashCode = hashCode * 59 + PostalCode.GetHashCode();
                if (CountryCode != null)
                    hashCode = hashCode * 59 + CountryCode.GetHashCode();
                if (Phone != null)
                    hashCode = hashCode * 59 + Phone.GetHashCode();
                if (AddressType != null)
                    hashCode = hashCode * 59 + AddressType.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
