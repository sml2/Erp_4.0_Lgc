﻿using OrderSDK.Modles.Amazon.Enums;

namespace OrderSDK.Modles.Amazon.ShipmentInvoicing
{
    public class ParameterBasedPII : RequestAmazonSP
    {
        //$"{_resourceBaseUrl}/shipments/{shipmentId}"

        public override RateLimitType RateLimitType { get; set; } = RateLimitType.ShipmentInvoicing_GetShipmentDetails;
        public override HttpMethod Method => HttpMethod.Get;

        public override string Url => $"/shipments/{shipmentId}";

        public string shipmentId { get; set; }

        public bool IsNeedRestrictedDataToken { get; set; }
        public CreateRestrictedDataTokenRequest RestrictedDataTokenRequest { get; set; }
    }

    public class CreateRestrictedDataTokenRequest
    {
        public IList<RestrictedResource> restrictedResources { get; set; }

        public string targetApplication { get; set; }

    }

    public class RestrictedResource
    {

        public string method { get; set; }
        public string path { get; set; }
        public IList<string> dataElements { get; set; }


    }
}
