﻿using OrderSDK.Modles.Amazon.Enums;


namespace OrderSDK.Modles.Amazon.ShipmentInvoicing
{
    public class GetInvoiceStatusRequest : RequestAmazonSP
    {
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.ShipmentInvoicing_GetInvoiceStatus;
        public override HttpMethod Method => HttpMethod.Get;

        public override string Url => $"/shipments/{shipmentId}/invoice/status";

        public string shipmentId { get; set; }
    }
}
