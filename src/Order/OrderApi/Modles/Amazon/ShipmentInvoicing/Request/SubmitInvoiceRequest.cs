﻿using OrderSDK.Modles.Amazon.Enums;

namespace OrderSDK.Modles.Amazon.ShipmentInvoicing
{
    public class SubmitInvoiceRequest : RequestAmazonSP
    {
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.ShipmentInvoicing_SubmitInvoice;
        public override HttpMethod Method => HttpMethod.Post;

        public override string Url => $"/shipments/{shipmentId}/invoice";

        public string shipmentId { get; set; }

        public string MarketplaceId { get; set; }
        public string ContentMD5Value { get; set; }
        public byte[] InvoiceContent { get; set; }
    }
}
