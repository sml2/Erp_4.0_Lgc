﻿using OrderSDK.Modles.Amazon.Enums;

namespace OrderSDK.Modles.Amazon.ProductPricing
{
    public class GetPricingRequest : RequestAmazonSP
    {
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.ProductPricing_GetPricing;
        public override HttpMethod Method => HttpMethod.Get;

        public override string Url => $"/products/pricing/v0/price";

        public string MarketplaceId { get; set; }
        public IList<string> Asins { get; set; }
        public IList<string> Skus { get; set; }
        public ItemType ItemType
        {
            get
            {
                if (Asins != null && Asins.Count > 0 && Skus != null && Skus.Count > 0)
                    throw new ArgumentException("Only allowed to fill Asins or Skus you cant have both");
                if (Asins != null && Asins.Count > 0)
                    return ItemType.Asin;
                if (Skus != null && Skus.Count > 0)
                    return ItemType.Sku;

                throw new ArgumentException("You cant request without fill Skus or Asins , you need to fill only of them only");
            }
        }
        public ItemCondition? ItemCondition { get; set; }
        public OfferTypeEnum? OfferType { get; set; } = OfferTypeEnum.B2C;
    }
}
