﻿using OrderSDK.Modles.Amazon.Enums;

namespace OrderSDK.Modles.Amazon.Authorization
{
    public class GetAuthorizationCodeRequest : RequestAmazonSP
    {
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.Authorization_GetAuthorizationCode;
        public override HttpMethod Method => HttpMethod.Get;

        public override TokenDataType TokenDataType { get; set; } = TokenDataType.Grantless;

        public override string Url => "/authorization/v1/authorizationCode";


        public string sellingPartnerId { get; set; } = string.Empty;


        public string developerId { get; set; } = string.Empty;


        public string mwsAuthToken { get; set; } = string.Empty;
    }
}
