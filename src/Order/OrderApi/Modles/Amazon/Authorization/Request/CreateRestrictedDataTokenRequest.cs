﻿using System.Collections.Generic;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace OrderSDK.Modles.Amazon.Authorization
{
    public class CreateRestrictedDataTokenRequest : RequestAmazonSP
    {
        public override HttpMethod Method => HttpMethod.Post;

        public override string Url => $"/tokens/2021-03-01/restrictedDataToken";


        public IList<RestrictedResource> restrictedResources { get; set; } = default!;

        public string targetApplication { get; set; } = string.Empty;
    }
}
