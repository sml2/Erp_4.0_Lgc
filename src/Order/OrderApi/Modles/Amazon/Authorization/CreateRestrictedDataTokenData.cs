﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace OrderSDK.Modles.Amazon.Authorization
{
    public class CreateRestrictedDataTokenData
    {
        public string RestrictedDataToken { get; set; } = string.Empty;
        public int ExpiresIn { get; set; }
    }
}
