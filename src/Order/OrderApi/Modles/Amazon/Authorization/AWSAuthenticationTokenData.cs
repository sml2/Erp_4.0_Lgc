﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace OrderSDK.Modles.Amazon.Authorization
{
    public class AWSAuthenticationTokenData
    {
        public AWSAuthenticationCredentials AWSAuthenticationCredential { get; set; } = default!;
        public string SessionToken { get; set; } = string.Empty;
        public DateTime Expiration { get; set; }


    }

    public class AWSAuthenticationCredentials
    {
        /**
        * AWS IAM User Access Key Id
        */
        public string AccessKeyId { get; set; } = string.Empty;

        /**
        * AWS IAM User Secret Key
        */
        public string SecretKey { get; set; } = string.Empty;

        /**
        * AWS Region
        */
        public string Region { get; set; } = string.Empty;
    }
}
