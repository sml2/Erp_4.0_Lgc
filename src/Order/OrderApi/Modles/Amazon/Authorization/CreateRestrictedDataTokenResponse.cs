﻿namespace OrderSDK.Modles.Amazon.Authorization
{
    public class CreateRestrictedDataTokenResponse
    {
        public string RestrictedDataToken { get; set; } = string.Empty;
        public int ExpiresIn { get; set; }
    }
}
