﻿using System.Collections.Generic;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace OrderSDK.Modles.Amazon.Authorization
{
    public class RestrictedResource
    {
        public string method { get; set; } = string.Empty;
        public string path { get; set; } = string.Empty;
        public IList<string> dataElements { get; set; } = Array.Empty<string>().ToList();
    }

}
