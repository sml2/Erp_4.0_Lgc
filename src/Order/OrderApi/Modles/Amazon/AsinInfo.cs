﻿namespace OrderSDK.Modles.Amazon
{
    public class AsinInfo
    {
        public AsinInfo(string asin, string sellerSku, string? itemName, string? description, string? price, string? quantity, string? imageUrl, string? marketplace, string? productIdType, string? productId)
        {
            Asin = asin;
            SellerSku = sellerSku;
            ItemName = itemName;
            Description = description;
            Price = price;
            Quantity = quantity;
            ImageUrl = imageUrl;
            Marketplace = marketplace;
            ProductIdType = productIdType;
            ProductId = productId;
        }

        public string Asin { get; set; }

        public string SellerSku { get; set; }

        public string? ItemName { get; set; }
        public string? Description { get; set; }

        public string? Price { get; set; }

        public string? Quantity { get; set; }

        public string? ImageUrl { get; set; }
        public string? Marketplace { get; set; }
        public string? ProductIdType { get; set; }

        public string? ProductId { get; set; }
    }
}
