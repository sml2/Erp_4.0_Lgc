﻿using Newtonsoft.Json;
using OrderSDK.Modles.Amazon.Enums;

namespace OrderSDK.Modles.Amazon.Reports
{
    public class getReportScheduleRequest : RequestAmazonSP
    {
        [JsonIgnore]
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.Report_GetReportSchedule;
        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;
        [JsonIgnore]
        public override string Url => $"/reports/2021-06-30/schedules/{reportScheduleId}";

        [JsonIgnore]
        public string reportScheduleId { get; set; }
    }
}
