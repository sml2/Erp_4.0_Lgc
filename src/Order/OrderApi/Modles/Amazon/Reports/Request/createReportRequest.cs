﻿using Newtonsoft.Json;
using OrderSDK.Modles.Amazon.Enums;

namespace OrderSDK.Modles.Amazon.Reports
{
    public class createReportRequest : RequestAmazonSP
    {
        [JsonIgnore]
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.Report_CreateReport;
        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;
        [JsonIgnore]
        public override string Url => $"/reports/2021-06-30/reports";

        public ReportOptions reportOptions { get; set; }
        public ReportTypes reportType { get; set; }
        public DateTime? dataStartTime { get; set; }
        public DateTime? dataEndTime { get; set; }
        public List<string> marketplaceIds { get; set; }
    }
}
