﻿using Newtonsoft.Json;
using OrderSDK.Modles.Amazon.Enums;

namespace OrderSDK.Modles.Amazon.Reports
{
    public class GetReportDocumentRequest : RequestAmazonSP
    {
        [JsonIgnore]
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.Report_GetReportDocument;
        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

        [JsonIgnore]
        public override string Url => $"/reports/2021-06-30/documents/{reportDocumentId}";

        [JsonIgnore]
        public string reportDocumentId { get; set; }
    }
}
