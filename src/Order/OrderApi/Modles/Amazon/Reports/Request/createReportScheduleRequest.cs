﻿using Common.Enums.Products;
using Newtonsoft.Json;
using OrderSDK.Modles.Amazon.Enums;
using static OrderSDK.Modles.Amazon.Reports.CreateReportScheduleSpecification;

namespace OrderSDK.Modles.Amazon.Reports
{
    public class createReportScheduleRequest : RequestAmazonSP
    {
        [JsonIgnore]
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.Report_CreateReportSchedule;
        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Post;
        [JsonIgnore]
        public override string Url => $"/reports/2021-06-30/schedules";

        public ReportOptions reportOptions { get; set; }
        public ReportTypes reportType { get; set; }
        public List<string> marketplaceIds { get; set; }
        public PeriodEnum period { get; set; }
        public DateTime? nextReportCreationTime { get; set; }
    }
}
