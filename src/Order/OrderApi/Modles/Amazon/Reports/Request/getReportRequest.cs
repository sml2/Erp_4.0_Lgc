﻿using Newtonsoft.Json;
using OrderSDK.Modles.Amazon.Enums;

namespace OrderSDK.Modles.Amazon.Reports
{
    public class getReportRequest : RequestAmazonSP
    {
        [JsonIgnore]
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.Report_GetReport;
        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;

        [JsonIgnore]
        public override string Url => $"/reports/2021-06-30/reports/{reportId}";

        [JsonIgnore]
        public string reportId { get; set; }
    }
}
