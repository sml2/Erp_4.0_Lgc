﻿using OrderSDK.Modles.Amazon.Enums;

namespace OrderSDK.Modles.Amazon.CatalogItems
{
    public class ListCatalogCategoriesRequest : RequestAmazonSP
    {
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.CatalogItems_ListCatalogCategories;
        public override HttpMethod Method => HttpMethod.Get;

        public override string Url => $"/catalog/v0/categories";

        public string? ASIN { get; set; }

        public string? SellerSKU { get; set; }

        public string MarketplaceId { get; set; }
    }
}
