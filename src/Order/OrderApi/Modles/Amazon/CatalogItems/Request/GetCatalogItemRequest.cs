﻿using Newtonsoft.Json;
using OrderSDK.Modles.Amazon.Enums;

namespace OrderSDK.Modles.Amazon.CatalogItems
{
    public class GetCatalogItemRequest : RequestAmazonSP
    {
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.CatalogItems_GetCatalogItem;
        public override HttpMethod Method => HttpMethod.Get;

        public override string Url => $"/catalog/v0/items/{asin}";

        [JsonIgnore]
        public string asin { get; set; }

        public string MarketplaceId { get; set; }
    }
}
