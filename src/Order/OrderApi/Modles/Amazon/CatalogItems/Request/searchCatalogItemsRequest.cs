﻿using OrderSDK.Modles.Amazon.Enums;

namespace OrderSDK.Modles.Amazon.CatalogItems
{
    public class searchCatalogItemsRequest : RequestAmazonSP
    {
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.CatalogItems20201201_SearchCatalogItems;
        public override HttpMethod Method => HttpMethod.Get;

        public override string Url => $"/catalog/2022-04-01/items";

        /// <summary>
        /// max count 20
        /// </summary>
        public List<string> identifiers { get; set; }

        public IdentifiersType? identifiersType { get; set; }

        /// <summary>
        /// max count 1
        /// </summary>
        public List<string> marketplaceIds { get; set; }


        public List<enumincludedData> includedData { get; set; }

        public string locale { set; get; }

        public string sellerId { get; set; }


        /// <summary>
        /// max count 20
        /// </summary>
        public List<string> keywords { get; set; }

        public List<string> brandNames { get; set; }

        public List<string> classificationIds { get; set; }

        /// <summary>
        /// max count 20
        /// </summary>
        public int pageSize { get; set; } = 20;

        public string pageToken { get; set; }

        public string keywordsLocale { get; set; }
    }

    public enum IdentifiersType
    {
        ASIN,
        EAN,
        GTIN,
        ISBN,
        JAN,
        MINSAN,
        SKU,
        UPC,
    }

    public enum enumincludedData
    {
        /// <summary>
        /// 一个 JSON 对象，包含以属性名称为键的结构化项目属性数据。 目录商品属性符合适用于产品类型定义的销售伙伴 API 中提供的相关亚马逊产品类型定义。
        /// </summary>
        attributes,

        /// <summary>
        /// 亚马逊目录中商品的尺寸
        /// </summary>
        dimensions,

        /// <summary>
        /// 与亚马逊目录中的商品关联的标识符，例如 UPC 和 EAN 标识符。
        /// </summary>
        identifiers,

        /// <summary>
        /// 亚马逊目录中商品的图片
        /// </summary>
        images,

        /// <summary>
        /// 与亚马逊目录项目关联的产品类型。
        /// </summary>
        productTypes,

        /// <summary>
        /// 亚马逊目录项目的关系详细信息（例如，变体）
        /// </summary>
        relationships,

        /// <summary>
        /// 亚马逊目录商品的销售排名。
        /// </summary>
        salesRanks,

        /// <summary>
        /// 亚马逊目录商品的摘要详细信息。 有关更多详细信息，请参阅亚马逊目录商品的属性。
        /// </summary>
        summaries,

        /// <summary>
        /// 与亚马逊目录商品关联的供应商详细信息。 供应商详细信息仅对供应商可用。
        /// </summary>
        vendorDetails,
    }
}
