﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace OrderSDK.Modles.Amazon.CatalogItems
{
    /// <summary>
    /// GetCatalogItemResponse
    /// </summary>
    [DataContract]
    public partial class ItemSearchResultsResponse : IEquatable<ItemSearchResultsResponse>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GetCatalogItemResponse" /> class.
        /// </summary>
        /// <param name="Payload">The payload for the getCatalogItem operation..</param>
        /// <param name="Errors">One or more unexpected errors occurred during the getCatalogItem operation..</param>
        public ItemSearchResultsResponse(ItemSearchResults Payload = default, ErrorList Errors = default)
        {
            this.Payload = Payload;
            this.Errors = Errors;
        }
        public ItemSearchResultsResponse()
        {
            Payload = default;
            Errors = default;
        }

        /// <summary>
        /// The payload for the getCatalogItem operation.
        /// </summary>
        /// <value>The payload for the getCatalogItem operation.</value>
        [DataMember(Name = "payload", EmitDefaultValue = false)]
        public ItemSearchResults Payload { get; set; }

        /// <summary>
        /// One or more unexpected errors occurred during the getCatalogItem operation.
        /// </summary>
        /// <value>One or more unexpected errors occurred during the getCatalogItem operation.</value>
        [DataMember(Name = "errors", EmitDefaultValue = false)]
        public ErrorList Errors { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class GetCatalogItemResponse {\n");
            sb.Append("  Payload: ").Append(Payload).Append("\n");
            sb.Append("  Errors: ").Append(Errors).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return Equals(input as ItemSearchResultsResponse);
        }

        /// <summary>
        /// Returns true if GetCatalogItemResponse instances are equal
        /// </summary>
        /// <param name="input">Instance of GetCatalogItemResponse to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(ItemSearchResultsResponse input)
        {
            if (input == null)
                return false;

            return
                (
                    Payload == input.Payload ||
                    Payload != null &&
                    Payload.Equals(input.Payload)
                ) &&
                (
                    Errors == input.Errors ||
                    Errors != null &&
                    Errors.Equals(input.Errors)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (Payload != null)
                    hashCode = hashCode * 59 + Payload.GetHashCode();
                if (Errors != null)
                    hashCode = hashCode * 59 + Errors.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

    public class ItemSearchResults
    {
        /// <summary>
        /// 对于基于标识符的搜索，找到的亚马逊目录商品总数。 对于基于关键字的搜索，与搜索查询匹配的亚马逊目录项目的估计总数（无论找到的数量如何，每次请求只会返回不超过页数限制的结果）。
        /// max count 1000
        /// </summary>
        public int numberOfResults { get; set; }

        /// <summary>
        /// 如果可用，返回分页结果所需的 nextToken 和/或 previousToken 值。
        /// </summary>
        public Pagination pagination { get; set; }

        /// <summary>
        /// 基于关键字的搜索的搜索优化。
        /// </summary>
        public Refinements refinements { get; set; }

        /// <summary>
        /// 亚马逊目录中的商品列表。
        /// </summary>
        public List<ItemRes> items { get; set; }
    }


    public class Pagination
    {
        /// <summary>
        /// 可用于获取下一页的令牌
        /// </summary>
        public string? nextToken { get; set; }

        /// <summary>
        /// 可用于获取上一页的令牌
        /// </summary>
        public string? previousToken { get; set; }
    }


    public class Refinements
    {
        /// <summary>
        /// 品牌搜索改进
        /// </summary>
        public List<BrandRefinement> brands { get; set; }

        /// <summary>
        /// 分类搜索细化
        /// </summary>
        public List<ClassificationRefinement> classifications { get; set; }
    }

    public class BrandRefinement
    {
        /// <summary>
        /// 如果应用优化键，估计仍将返回的结果数。
        /// </summary>
        public int numberOfResults { get; set; }

        /// <summary>
        /// 品牌。 用于显示并可用作搜索细化。
        /// </summary>
        public string brandName { get; set; }
    }

    public class ClassificationRefinement
    {
        /// <summary>
        /// 如果应用优化键，估计仍将返回的结果数
        /// </summary>
        public int numberOfResults { get; set; }

        /// <summary>
        /// 分类的显示名称。
        /// </summary>
        public string displayName { get; set; }

        /// <summary>
        /// 可用于搜索细化目的的分类标识符。
        /// </summary>
        public string classificationId { get; set; }
    }

    public class ItemAttributes : Dictionary<string, object>
    {

    }

    public class ItemRes
    {
        /// <summary>
        /// 亚马逊标准识别码 (ASIN) 是亚马逊目录中商品的唯一标识符。
        /// </summary>
        public string asin { get; set; }

        /// <summary>
        /// 一个 JSON 对象，其中包含以属性名称为键的结构化项目属性数据。
        /// 目录项目属性符合产品类型定义的销售伙伴 API 中可用的相关产品类型定义。
        /// </summary>
        public ItemAttributes attributes { get; set; }

        /// <summary>
        /// 与亚马逊市场亚马逊目录中的商品相关联的维度数组。
        /// </summary>
        public ItemDimensions dimensions { get; set; }

        /// <summary>
        /// 与亚马逊目录中的商品关联的标识符，例如 UPC 和 EAN 标识符。
        /// </summary>
        public ItemIdentifiers identifiers { get; set; }

        /// <summary>
        /// 亚马逊目录中商品的图片。
        /// </summary>
        public ItemImages images { get; set; }

        /// <summary>
        /// 与亚马逊目录项目关联的产品类型。
        /// </summary>
        public ItemProductTypes productTypes { get; set; }

        /// <summary>
        /// 亚马逊目录商品的市场关系（例如，变体）
        /// </summary>
        public ItemRelationships relationships { get; set; }

        /// <summary>
        /// 亚马逊目录商品的销售排名。
        /// </summary>
        public ItemSalesRanks salesRanks { get; set; }

        /// <summary>
        /// 亚马逊目录商品的摘要详细信息。
        /// </summary>
        public ItemSummaries summaries { get; set; }

        /// <summary>
        /// 与亚马逊目录商品关联的供应商详细信息。 供应商详细信息仅对供应商可用。
        /// </summary>
        public ItemVendorDetails vendorDetails { get; set; }
    }

    #region  ItemDimensions
    public class ItemDimensions : List<ItemDimensionsByMarketplace>
    {
    }

    public class ItemDimensionsByMarketplace
    {
        public string marketplaceId { get; set; }

        /// <summary>
        /// 亚马逊目录商品的尺寸。
        /// </summary>
        public Dimensions item { set; get; }

        /// <summary>
        /// 亚马逊目录商品在其包装中的尺寸。
        /// </summary>
        public Dimensions package { set; get; }
    }

    public class Dimensions
    {
        /// <summary>
        /// 物品或物品包的高度。
        /// </summary>
        public Dimension height { get; set; }

        /// <summary>
        /// 物品或物品包的长度
        /// </summary>
        public Dimension length { get; set; }

        /// <summary>
        /// 物品或物品包的重量
        /// </summary>
        public Dimension weight { get; set; }

        /// <summary>
        /// 物品或物品包的宽度
        /// </summary>
        public Dimension width { get; set; }
    }

    public class Dimension
    {
        /// <summary>
        /// 单位
        /// </summary>
        public string unit { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        public int value { get; set; }
    }

    #endregion

    #region ItemIdentifiers
    public class ItemIdentifiers : List<ItemIdentifiersByMarketplace>
    {
    }

    public class ItemIdentifiersByMarketplace
    {
        public string marketplaceId { get; set; }

        /// <summary>
        /// 与指定亚马逊商城的亚马逊目录中的商品相关联的标识符。
        /// </summary>
        public List<ItemIdentifier> identifiers { get; set; }
    }

    public class ItemIdentifier
    {
        /// <summary>
        /// 标识符的类型，例如 UPC、EAN 或 ISBN。
        /// </summary>
        public string identifierType { set; get; }

        /// <summary>
        /// 标识符
        /// </summary>
        public string identifier { set; get; }
    }

    #endregion

    #region ItemImages
    public class ItemImages : List<ItemImagesByMarketplace>
    {

    }
    public class ItemImagesByMarketplace
    {
        public string marketplaceId { get; set; }
        public List<ItemImage> images { get; set; }
    }
    public class ItemImage
    {
        /// <summary>
        /// 图像的变体，例如 MAIN 或 PT01。
        /// </summary>
        public enumVariant variant { get; set; }

        /// <summary>
        /// 图片的链接或 URL。
        /// </summary>
        public string link { get; set; }

        /// <summary>
        /// 图像的高度（以像素为单位）。
        /// </summary>
        public int height { get; set; }

        /// <summary>
        /// 图像的宽度（以像素为单位）。
        /// </summary>
        public int width { get; set; }
    }
    public enum enumVariant
    {
        MAIN,
        PT01,
        PT02,
        PT03,
        PT04,
        PT05,
        PT06,
        PT07,
        PT08,
        SWCH,
    }

    #endregion

    #region ItemProductTypes
    public class ItemProductTypes : List<ItemProductTypeByMarketplace>
    {

    }

    public class ItemProductTypeByMarketplace
    {
        public string? marketplaceId { get; set; }

        /// <summary>
        /// 与亚马逊目录项目关联的产品类型的名称。
        /// </summary>
        public string? productType { get; set; }
    }

    #endregion

    #region ItemRelationships
    public class ItemRelationships : List<ItemRelationshipsByMarketplace>
    {
    }
    public class ItemRelationshipsByMarketplace
    {
        public string marketplaceId { get; set; }

        /// <summary>
        /// 项目的关系。
        /// </summary>
        public List<ItemRelationship> relationships { get; set; }
    }

    public class ItemRelationship
    {
        /// <summary>
        /// 作为此商品子项的相关商品的标识符 (ASIN)。
        /// </summary>
        public List<string> childAsins { get; set; }

        /// <summary>
        /// 作为该商品的父项的相关商品的标识符 (ASIN)。
        /// </summary>
        public List<string> parentAsins { get; set; }

        /// <summary>
        /// 对于“VARIATION”关系，表示定义变体系列的亚马逊商品目录属性组合的变体主题。
        /// </summary>
        public ItemVariationTheme variationTheme { get; set; }

        /// <summary>
        /// 关系类型。示例：“变化”
        /// </summary>
        public Type type { get; set; }
    }

    public class ItemVariationTheme
    {
        /// <summary>
        /// 与变体主题关联的亚马逊目录项目属性的名称。
        /// </summary>
        public List<string> attributes { get; set; }

        /// <summary>
        /// 变体主题，指示定义变体系列的亚马逊商品目录属性的组合。示例：“COLOR_NAME/STYLE_NAME”
        /// </summary>
        public string theme { get; set; }
    }

    public enum Type
    {
        VARIATION,
        PACKAGE_HIERARCHY,
    }


    #endregion

    #region ItemSalesRanks

    public class ItemSalesRanks : List<ItemSalesRanksByMarketplace>
    {
    }
    /// <summary>
    /// 指定亚马逊商城的亚马逊目录商品的销售排名。
    /// </summary>
    public class ItemSalesRanksByMarketplace
    {
        public string marketplaceId { get; set; }

        /// <summary>
        /// 亚马逊商城的亚马逊目录商品的销售排名（按分类）。
        /// </summary>
        public List<ItemClassificationSalesRank> classificationRanks { get; set; }

        /// <summary>
        /// 按网站显示组划分的亚马逊商城的亚马逊目录商品的销售排名。
        /// </summary>
        public List<ItemDisplayGroupSalesRank> displayGroupRanks { get; set; }
    }

    public class ItemClassificationSalesRank
    {
        /// <summary>
        /// 与销售排名相关的分类标识符。
        /// </summary>
        public string classificationId { get; set; }

        /// <summary>
        /// 销售排名的标题或名称。
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// 相应的亚马逊零售网站链接或 URL，用于销售排名。
        /// </summary>
        public string link { get; set; }

        /// <summary>
        /// 销售排名值。
        /// </summary>
        public int rank { get; set; }
    }
    public class ItemDisplayGroupSalesRank
    {
        /// <summary>
        /// 与销售排名相关的网站显示组的名称
        /// </summary>
        public string websiteDisplayGroup { get; set; }

        /// <summary>
        /// 销售排名的标题或名称。
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// 相应的亚马逊零售网站链接或 URL，用于销售排名。
        /// </summary>
        public string? link { get; set; }

        /// <summary>
        /// 销售排名值。
        /// </summary>
        public int rank { get; set; }
    }
    #endregion


    #region ItemSummaries

    public class ItemSummaries : List<ItemSummaryByMarketplace>
    {
    }

    public class ItemSummaryByMarketplace
    {
        public string marketplaceId { get; set; }

        /// <summary>
        /// 与亚马逊目录商品关联的品牌名称。
        /// </summary>
        public string brand { get; set; }

        /// <summary>
        /// 与亚马逊目录项目关联的分类（浏览节点）。
        /// </summary>
        public ItemBrowseClassification browseClassification { get; set; }

        /// <summary>
        /// 与亚马逊目录商品关联的颜色名称。
        /// </summary>
        public string color { get; set; }

        /// <summary>
        /// 与亚马逊目录商品关联的分类类型。
        /// </summary>
        public ItemClassification itemClassification { get; set; }

        /// <summary>
        /// 与亚马逊目录商品关联的名称或标题。
        /// </summary>
        public string itemName { get; set; }

        /// <summary>
        /// 与亚马逊目录商品关联的制造商名称。
        /// </summary>
        public string manufacturer { get; set; }

        /// <summary>
        /// 与亚马逊目录商品关联的型号。
        /// </summary>
        public string modelNumber { get; set; }

        /// <summary>
        /// 一个包裹中亚马逊目录商品的数量。
        /// </summary>
        public int packageQuantity { get; set; }

        /// <summary>
        /// 与亚马逊目录商品关联的部件号
        /// </summary>
        public string partNumber { get; set; }

        /// <summary>
        /// 与亚马逊目录商品关联的尺码名称。
        /// </summary>
        public string size { get; set; }

        /// <summary>
        /// 与亚马逊目录商品关联的样式名称。
        /// </summary>
        public string style { get; set; }

        /// <summary>
        /// 与亚马逊目录项目关联的网站显示组的标识符。
        /// </summary>
        public string websiteDisplayGroup { get; set; }

        /// <summary>
        /// 与亚马逊目录项目关联的网站显示组的显示名称。
        /// </summary>
        public string websiteDisplayGroupName { get; set; }
    }


    public class ItemBrowseClassification
    {
        public string displayName { get; set; }

        public string classificationId { get; set; }
    }

    public enum ItemClassification
    {
        BASE_PRODUCT,
        OTHER,
        PRODUCT_BUNDLE,
        VARIATION_PARENT,
    }


    #endregion


    #region ItemVendorDetails

    public class ItemVendorDetails : List<ItemVendorDetailsByMarketplace>
    {
    }

    public class ItemVendorDetailsByMarketplace
    {
        public string marketplaceId { get; set; }

        /// <summary>
        /// 与亚马逊目录商品关联的品牌代码。
        /// </summary>
        public string brandCode { get; set; }

        /// <summary>
        /// 与亚马逊目录商品关联的制造商代码。
        /// </summary>
        public string manufacturerCode { get; set; }

        /// <summary>
        /// 制造商代码的父供应商代码。
        /// </summary>
        public string manufacturerCodeParent { get; set; }

        /// <summary>
        /// 与亚马逊目录项目关联的产品类别。
        /// </summary>
        public ItemVendorDetailsCategory productCategory { get; set; }

        /// <summary>
        /// 与亚马逊目录商品关联的产品组。
        /// </summary>
        public string productGroup { get; set; }

        /// <summary>
        /// 与亚马逊目录项目关联的产品子类别。
        /// </summary>
        public ItemVendorDetailsCategory productSubcategory { get; set; }

        /// <summary>
        /// 与亚马逊目录商品关联的补货类别。
        /// </summary>
        public ReplenishmentCategory replenishmentCategory { get; set; }

    }


    public class ItemVendorDetailsCategory
    {
        public string displayName { get; set; }
        public string value { get; set; }
    }

    public enum ReplenishmentCategory
    {
        ALLOCATED,
        BASIC_REPLENISHMENT,
        IN_SEASON,
        LIMITED_REPLENISHMENT,
        MANUFACTURER_OUT_OF_STOCK,
        NEW_PRODUCT,
        NON_REPLENISHABLE,
        NON_STOCKUPABLE,
        OBSOLETE,
        PLANNED_REPLENISHMENT,
    }

    #endregion
}
