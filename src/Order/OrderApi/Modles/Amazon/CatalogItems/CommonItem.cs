﻿using Newtonsoft.Json;

namespace OrderSDK.Modles.Amazon.CatalogItems
{
    public class CommonItemList : List<CommonItem> { }
    public class CommonItem
    {
        [JsonProperty("marketplace_id")]
        public string MarketplaceId { get; set; }

        [JsonProperty("language_tag")]
        public string LanguageTag { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
