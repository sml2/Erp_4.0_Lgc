﻿
using OrderSDK.Modles.Amazon.Authorization;
using OrderSDK.Modles.Amazon.Enums;
using OrderSDK.Modles.Amazon.Utils;

namespace OrderSDK.Modles.Amazon
{
    /// <summary>
    /// 亚马逊接口凭据
    /// </summary>
    public class AmazonCredential
    {
        public string AccessKey { get; set; } = string.Empty;
        public string SecretKey { get; set; } = string.Empty;
        public string RoleArn { get; set; } = string.Empty;
        public string ClientId { get; set; } = string.Empty;
        public string ClientSecret { get; set; } = string.Empty;
        public bool IsActiveLimitRate { get; set; } = true;
        public int MaxThrottledRetryCount { get; set; } = 4;

        public int MaxTokenRetryCount { get; set; } = 5;
        public string RedirectUri { get; set; } = string.Empty;
        public string BaseApiUrl { get; set; } = string.Empty;

        public ShippingBusiness? ShippingBusiness { get; set; }

        public string? regionName { get; set; }

        public string storeName { get; set; }

        public AmazonCredential(string AccessKey, string SecretKey, string RoleArn, string ClientId, string ClientSecret,
            string RedirectUri, string BaseApiUrl, string? regionName)
        {
            this.AccessKey = AccessKey;
            this.SecretKey = SecretKey;
            this.RoleArn = RoleArn;
            this.ClientId = ClientId;
            this.ClientSecret = ClientSecret;
            this.RedirectUri = RedirectUri;
            this.BaseApiUrl = BaseApiUrl;
            this.regionName = regionName;
        }

        internal Dictionary<RateLimitType, RateLimits> UsagePlansTimings { get; set; } = new RateLimitsDefinitions().RateLimitsTime;
        public void SetAWSAuthenticationTokenData(AWSAuthenticationTokenData tokenData)
        {
            AWSAuthenticationTokenData = tokenData;
        }

        public AWSAuthenticationTokenData GetAWSAuthenticationTokenData()
        {
            if (AWSAuthenticationTokenData != null && AWSAuthenticationTokenData.Expiration.AddSeconds(-60) > DateTime.Now)
                return AWSAuthenticationTokenData;
            else return null!;
        }

        protected AWSAuthenticationTokenData AWSAuthenticationTokenData { get; set; } = default!;

    }
}
