﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using OrderSDK.Modles.Amazon.Enums;

namespace OrderSDK.Modles.Amazon.ListingsItems
{
    public class ListingsItemRequest : RequestAmazonSP
    {
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.ListingsItem_GetListingsItem;
        public override HttpMethod Method => HttpMethod.Get;

        public override string Url => $"/listings/2021-08-01/items/{sellerId}/{sku}";

        [JsonIgnore]
        public string sellerId { get; set; } = string.Empty;

        [JsonIgnore]
        public string sku { get; set; } = string.Empty;


        /// <summary>
        /// A list of MarketplaceId values. Used to select orders that were placed in the specified marketplaces. Max count : 50
        /// </summary>
        public IList<string> marketplaceIds { get; set; }

        /// <summary>
        /// A locale for localization of issues. When not provided, the default language code of the first marketplace is used. Examples: "en_US", "fr_CA", "fr_FR". Localized messages default to "en_US" when a localization is not available in the specified locale.
        /// </summary>
        public string issueLocale { get; set; } = "en_US";

        [JsonProperty(ItemConverterType = typeof(StringEnumConverter))]
        /// <summary>
        /// A comma-delimited list of data sets to include in the response. Default: summaries.
        /// </summary>
        public IList<ListingsIncludedData> includedData { get; set; }
    }
}
