﻿using Newtonsoft.Json;
using OrderSDK.Modles.Amazon.Enums;

using AmazonConstants = OrderSDK.Modles.Amazon.Enums.Constants;

namespace OrderSDK.Modles.Amazon.ListingsItems
{
    public class ParameterPutListingItem : RequestAmazonSP
    {
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.ListingsItem_PutListingsItem;
        public override HttpMethod Method => HttpMethod.Post;

        public override string Url => $"/listings/2021-08-01/items/{SellerId}/{Uri.EscapeDataString(Sku)}";

        public bool Check()
        {
            //if (TestCase == AmazonConstants.TestCase400)
            //    Sku = "BadSKU";
            if (string.IsNullOrWhiteSpace(SellerId))
            {
                throw new InvalidDataException("SellerId is a required property for ParameterPutListingItem and cannot be null");
            }
            if (string.IsNullOrWhiteSpace(Sku))
            {
                throw new InvalidDataException("Sku is a required property for ParameterPutListingItem and cannot be null");
            }
            if (MarketplaceIds == null || !MarketplaceIds.Any())
            {
                throw new InvalidDataException("MarketplaceIds is a required property for ParameterPutListingItem and cannot be null");
            }
            if (_ListingsItemPutRequest == null)
            {
                throw new InvalidDataException("ListingsItemPutRequest is a required property for ParameterPutListingItem and cannot be null");
            }
            if (string.IsNullOrWhiteSpace(_ListingsItemPutRequest.ProductType))
            {
                throw new InvalidDataException("ProductType is a required property for ListingsItemPutRequest and cannot be null");
            }
            if (_ListingsItemPutRequest.Attributes == null)
            {
                throw new InvalidDataException("Attributes is a required property for ListingsItemPutRequest and cannot be null");
            }
            return true;
        }

        [JsonProperty("sellerId")]
        public string SellerId { get; set; }

        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("marketplaceIds")]
        public ICollection<string> MarketplaceIds { get; set; }

        [JsonProperty("issueLocale")]
        public string IssueLocale { get; set; }

        [JsonProperty("listingsItemPutRequest")]
        public ListingsItemPutRequest _ListingsItemPutRequest { get; set; }
    }
}
