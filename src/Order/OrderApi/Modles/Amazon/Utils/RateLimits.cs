using Microsoft.Extensions.Logging;
using OrderSDK.Modles.Amazon.Enums;
using System;
using System.Threading.Tasks;


namespace OrderSDK.Modles.Amazon.Utils
{
    internal class RateLimits
    {
        internal decimal Rate { get; set; }
        internal int Burst { get; set; }
        internal DateTime LastRequest { get; set; }
        internal int RequestsSent { get; set; }

        internal RateLimits(decimal Rate, int Burst)
        {
            this.Rate = Rate;
            this.Burst = Burst;
            LastRequest = DateTime.Now;
            RequestsSent = 0;
        }
        /// <summary>
        /// 获取获取费率周期，单位毫秒
        /// </summary>
        /// <returns></returns>
        private int GetRatePeriodMs() { return (int)(1 / Rate * 1000 / 1); }
        public RateLimits NextRate(RateLimitType rateLimitType, ILogger logger)
        {
            int ratePeriodMs = GetRatePeriodMs();
            LastRequest = DateTime.UtcNow;
            var method = $"【RateLimits ,{rateLimitType,15}】";
            if (RequestsSent < 0)
                RequestsSent = 0;                    
            RequestsSent += 1;           
            if (RequestsSent >= Burst+1)
            {
                Delay(logger,method);
                RequestsSent = 1;
                //var LastRequestTime = LastRequest;
                //while (true)
                //{
                //    LastRequestTime = LastRequestTime.AddMilliseconds(ratePeriodMs);
                //    if (LastRequestTime > DateTime.UtcNow)
                //        break;
                //    else
                //        RequestsSent -= 1;

                //    if (RequestsSent <= 0)
                //    {
                //        RequestsSent = 0;
                //        break;
                //    }
                //}
            }

            //if (RequestsSent >= Burst)
            //{
            //    LastRequest = LastRequest.AddMilliseconds(ratePeriodMs);
            //    while (LastRequest >= DateTime.UtcNow) //.AddMilliseconds(-100)
            //      System.Threading.Tasks.Task.Delay(100).Wait();

            //}

            var RequestsSentTxt = RequestsSent > Burst ? "FULL" : RequestsSent.ToString();
            string output = $"{method}: {DateTime.Now.ToString(),10}\t Request/Burst: {RequestsSentTxt}/{Burst}\t Rate: {Rate}/{ratePeriodMs}ms";


            logger.LogInformation(output);


            return this;
        }

        internal void SetRateLimit(decimal rate)
        {
            Rate = rate;
        }

        internal void Decrement()
        {
            if (RequestsSent > 0) { RequestsSent--; }
            else { RequestsSent = 0; }
        }
        internal void Delay(ILogger logger,string name)
        {
            int time = GetRatePeriodMs() + 40000;
            logger.LogInformation($"【{name}】 Delay:{time}");
            Task.Delay(time).Wait();
            RequestsSent = 1;
        }
    }
}
