﻿using System.IO.Compression;
using System.Security.Cryptography;

namespace OrderSDK.Modles.Amazon.Utils
{
    public class FileTransform
    {
        public static string DecryptString(byte[] key, byte[] iv, byte[] cipherText)
        {
            byte[] buffer = cipherText;

            using (Aes aes = Aes.Create())
            {
                aes.Key = key;
                aes.IV = iv;
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream(buffer))
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader streamReader = new StreamReader(cryptoStream))
                        {
                            return streamReader.ReadToEnd();
                        }
                    }
                }
            }

        }



        public static string Decompress(string fileName)
        {
            FileInfo fileInfo = new FileInfo(fileName);

            using (FileStream originalFileStream = fileInfo.OpenRead())
            {
                string currentFileName = fileInfo.FullName;
                string newFileName = currentFileName.Remove(currentFileName.Length - fileInfo.Extension.Length);

                using (FileStream decompressedFileStream = File.Create(newFileName))
                {
                    using (GZipStream decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress))
                    {
                        decompressionStream.CopyTo(decompressedFileStream);
                        Console.WriteLine($"Decompressed: {fileInfo.Name}");
                    }
                    return decompressedFileStream.Name;
                }
            }
        }


    }
    public class MarketPlace
    {

        public string ID { get; set; }
        public Region Region { get; set; }

        public Country Country { get; set; }

        private MarketPlace(string id, Region region, Country country)
        {
            ID = id;
            Region = region;
            Country = country;
        }

        public static MarketPlace? GetMarketPlaceByID(string id)
        {
            var list = new List<MarketPlace>();
            //NorthAmerica
            list.Add(US); list.Add(Canada); list.Add(Mexico); list.Add(Brazil);
            //Europe
            list.Add(Spain); list.Add(UnitedKingdom); list.Add(France); list.Add(Netherlands);
            list.Add(Germany); list.Add(Italy); list.Add(Sweden); list.Add(Egypt);
            list.Add(Poland); list.Add(Turkey); list.Add(UnitedArabEmirates); list.Add(India);
            list.Add(SaudiArabia);
            //FarEast
            list.Add(Singapore); list.Add(Australia); list.Add(Japan);

            return list.FirstOrDefault(a => a.ID == id);
        }

        public static MarketPlace? GetMarketplaceByCountryCode(string countryCode)
        {
            var list = new List<MarketPlace>();
            //NorthAmerica
            list.Add(US); list.Add(Canada); list.Add(Mexico); list.Add(Brazil);
            //Europe
            list.Add(Spain); list.Add(UnitedKingdom); list.Add(France); list.Add(Netherlands);
            list.Add(Germany); list.Add(Italy); list.Add(Sweden); list.Add(Egypt);
            list.Add(Poland); list.Add(Turkey); list.Add(UnitedArabEmirates); list.Add(India);
            list.Add(SaudiArabia);
            //FarEast
            list.Add(Singapore); list.Add(Australia); list.Add(Japan);

            return list.FirstOrDefault(a => a.Country.Code == countryCode);
        }

        //https://developer-docs.amazon.com/sp-api/docs/marketplace-ids

        //NorthAmerica
        public static MarketPlace US { get { return new MarketPlace("ATVPDKIKX0DER", Region.NorthAmerica, Country.US); } }
        public static MarketPlace Canada { get { return new MarketPlace("A2EUQ1WTGCTBG2", Region.NorthAmerica, Country.CA); } }
        public static MarketPlace Mexico { get { return new MarketPlace("A1AM78C64UM0Y8", Region.NorthAmerica, Country.MX); } }
        public static MarketPlace Brazil { get { return new MarketPlace("A2Q3Y263D00KWC", Region.NorthAmerica, Country.BR); } }

        //Europe
        public static MarketPlace Spain { get { return new MarketPlace("A1RKKUPIHCS9HS", Region.Europe, Country.ES); } }
        public static MarketPlace UnitedKingdom { get { return new MarketPlace("A1F83G8C2ARO7P", Region.Europe, Country.GB); } }
        public static MarketPlace France { get { return new MarketPlace("A13V1IB3VIYZZH", Region.Europe, Country.FR); } }
        public static MarketPlace Netherlands { get { return new MarketPlace("A1805IZSGTT6HS", Region.Europe, Country.NL); } }
        public static MarketPlace Germany { get { return new MarketPlace("A1PA6795UKMFR9", Region.Europe, Country.DE); } }
        public static MarketPlace Italy { get { return new MarketPlace("APJ6JRA9NG5V4", Region.Europe, Country.IT); } }
        public static MarketPlace Sweden { get { return new MarketPlace("A2NODRKZP88ZB9", Region.Europe, Country.SE); } }
        public static MarketPlace Egypt { get { return new MarketPlace("ARBP9OOSHTCHU", Region.Europe, Country.EG); } }
        public static MarketPlace Poland { get { return new MarketPlace("A1C3SOZRARQ6R3", Region.Europe, Country.PL); } }
        public static MarketPlace Turkey { get { return new MarketPlace("A33AVAJ2PDY3EV", Region.Europe, Country.TR); } }
        public static MarketPlace UnitedArabEmirates { get { return new MarketPlace("A2VIGQ35RCS4UG", Region.Europe, Country.AE); } }
        public static MarketPlace India { get { return new MarketPlace("A21TJRUUN4KGV", Region.Europe, Country.IN); } }
        public static MarketPlace SaudiArabia { get { return new MarketPlace("A17E79C6D8DWNP", Region.Europe, Country.SA); } }

        //FarEast
        public static MarketPlace Singapore { get { return new MarketPlace("A19VAU5U5O7RUS", Region.FarEast, Country.SG); } }
        public static MarketPlace Australia { get { return new MarketPlace("A39IBJ37TRP1C6", Region.FarEast, Country.AU); } }
        public static MarketPlace Japan { get { return new MarketPlace("A1VC38T7YXB528", Region.FarEast, Country.JP); } }
    }

    public class Region
    {
        private Region(string regionName, string hostUrl, string sandboxHostUrl)
        {
            RegionName = regionName;
            HostUrl = hostUrl;
            SandboxHostUrl = sandboxHostUrl;
        }
        public string RegionName { get; set; }
        public string HostUrl { get; set; }
        public string SandboxHostUrl { get; set; }


        public static Region NorthAmerica { get { return new Region("us-east-1", "https://sellingpartnerapi-na.amazon.com", "https://sandbox.sellingpartnerapi-na.amazon.com"); } }
        public static Region Europe { get { return new Region("eu-west-1", "https://sellingpartnerapi-eu.amazon.com", "https://sandbox.sellingpartnerapi-eu.amazon.com"); } }
        public static Region FarEast { get { return new Region("us-west-2", "https://sellingpartnerapi-fe.amazon.com", "https://sandbox.sellingpartnerapi-fe.amazon.com"); } }
    }

    public class Country
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string SellercentralURL { get; set; }
        public string VendorCentralURL { get; set; }
        public string AmazonlUrl { get; set; }

        private Country(string code, string name, string domain, string sellercentralUrl, string vendorCentralURL)
        {
            Code = code;
            Name = name;

            SellercentralURL = sellercentralUrl;
            VendorCentralURL = vendorCentralURL;

            AmazonlUrl = $"https://amazon.{domain}";
        }

        public static Country US { get { return new Country("US", "United States of America", "com", "https://sellercentral.amazon.com", "https://vendorcentral.amazon.ca"); } }
        public static Country CA { get { return new Country("CA", "Canada", "ca", "https://sellercentral.amazon.ca", "https://vendorcentral.amazon.ca"); } }
        public static Country MX { get { return new Country("MX", "Mexico", "com.mx", "https://sellercentral.amazon.com.mx", "https://vendorcentral.amazon.com.mx"); } }
        public static Country BR { get { return new Country("BR", "Brazil", "com.br", "https://sellercentral.amazon.com.br", "https://vendorcentral.amazon.com.br"); } }

        public static Country ES { get { return new Country("ES", "Spain", "es", "https://sellercentral-europe.amazon.com", "https://vendorcentral.amazon.es"); } }
        public static Country GB { get { return new Country("GB", "United Kingdom", "co.uk", "https://sellercentral-europe.amazon.com", "https://vendorcentral.amazon.co.uk"); } }
        public static Country FR { get { return new Country("FR", "France", "fr", "https://sellercentral-europe.amazon.com", "https://vendorcentral.amazon.fr"); } }
        public static Country NL { get { return new Country("NL", "Netherlands", "nl", "https://sellercentral.amazon.nl", "https://vendorcentral.amazon.nl"); } }
        public static Country DE { get { return new Country("DE", "Germany", "de", "https://sellercentral-europe.amazon.com", "https://vendorcentral.amazon.de"); } }
        public static Country IT { get { return new Country("IT", "Italy", "it", "https://sellercentral-europe.amazon.com", "https://vendorcentral.amazon.it"); } }
        public static Country SE { get { return new Country("SE", "Sweden", "se", "https://sellercentral.amazon.se", "https://vendorcentral.amazon.se"); } }
        public static Country PL { get { return new Country("PL", "Poland", "pl", "https://sellercentral.amazon.pl", "https://vendorcentral.amazon.pl"); } }
        public static Country EG { get { return new Country("EG", "Egypt", "eg", "https://sellercentral.amazon.eg", "https://vendorcentral.amazon.me"); } }
        public static Country TR { get { return new Country("TR", "Turkey", "com.tr", "https://sellercentral.amazon.com.tr", "https://vendorcentral.amazon.com.tr"); } }
        public static Country AE { get { return new Country("AE", "United Arab Emirates", "ae", "https://sellercentral.amazon.ae", "https://vendorcentral.amazon.me"); } }
        public static Country IN { get { return new Country("IN", "India", "in", "https://sellercentral.amazon.in", "https://www.vendorcentral.in"); } }
        public static Country SA { get { return new Country("SA", "Saudi Arabia", "sa", "https://sellercentral.amazon.sa", "https://vendorcentral.amazon.me"); } }



        public static Country SG { get { return new Country("SG", "Singapore", "sg", "	https://sellercentral.amazon.sg", "https://vendorcentral.amazon.com.sg"); } }
        public static Country AU { get { return new Country("AU", "Australia", "com.au", "https://sellercentral.amazon.com.au", "https://vendorcentral.amazon.com.au"); } }
        public static Country JP { get { return new Country("JP", "Japan", "co.jp", "https://sellercentral.amazon.co.jp", "https://vendorcentral.amazon.co.jp"); } }



    }
}

