﻿using OrderSDK.Modles.Amazon.Order;

namespace OrderSDK.Modles.Amazon.vm;

public  class OrderLists
{
    public OrderLists(string nextToken, OrderList orderList)
    {
        NextToken = nextToken;
        this.orderList = orderList;
    }

    public string NextToken { get; set; }

    public OrderList orderList { get; set; }
}
     
