﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderSDK.Modles.Amazon.vm
{
    public class DeliverProduct
    {
        public DeliverProduct(string amazonOrderID, string fulfillmentDate, string carrierName, string shippingMethod,
            string shipperTrackingNumber, string amazonOrderItemCode, int quantity)
        {
            AmazonOrderID = amazonOrderID;
            FulfillmentDate = fulfillmentDate;
            CarrierName = carrierName;
            ShippingMethod = shippingMethod;
            ShipperTrackingNumber = shipperTrackingNumber;
            AmazonOrderItemCode = amazonOrderItemCode;
            Quantity = quantity;
        }
        public string AmazonOrderID { get; set; }  //订单编号
        public string FulfillmentDate { get; set; }
        public string CarrierName { get; set; }   //承运人名称
        public string ShippingMethod { get; set; }  //货运方式
        public string ShipperTrackingNumber { get; set; }   //运单号
        public string AmazonOrderItemCode { get; set; }
        public int Quantity { get; set; }
        public bool HasAmazonOrderItemCode => false;
    }

    public class ProductDesc
    {
        public string type { get; set; }

        public string version { get; set; }

        public string locale { get; set; }
    }
}
