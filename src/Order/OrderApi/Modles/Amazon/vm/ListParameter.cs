﻿using OrderSDK.Modles.CommonType;

namespace OrderSDK.Modles.Amazon.vm;
public record ListParameter
{
    public StoreRegion store;
    public DateTime Start;
    public DateTime End;
    public string NextToken;   

    public ListParameter(StoreRegion store, DateTime start, DateTime end,string nextToken="")
    {
        this.store = store;
        Start = start;
        End = end;
        NextToken= nextToken;       
    }  
}
     
