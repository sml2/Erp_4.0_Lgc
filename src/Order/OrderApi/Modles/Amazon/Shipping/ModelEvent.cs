/* 
 * Selling Partner API for Shipping
 *
 * Provides programmatic access to Amazon Shipping APIs.
 *
 * OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Runtime.Serialization;
using System.Text;

namespace OrderSDK.Modles.Amazon.Shipping
{
    /// <summary>
    /// An event of a shipment
    /// </summary>
    [DataContract]
    public partial class ModelEvent : IEquatable<ModelEvent>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ModelEvent" /> class.
        /// </summary>
        [JsonConstructorAttribute]
        public ModelEvent() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="ModelEvent" /> class.
        /// </summary>
        /// <param name="EventCode">EventCode (required).</param>
        /// <param name="EventTime">The date and time of an event for a shipment. (required).</param>
        /// <param name="Location">Location.</param>
        public ModelEvent(EventCode EventCode = default(EventCode)!, DateTime? EventTime = default, Location Location = default(Location)!)
        {
            // to ensure "EventCode" is required (not null)
            if (EventCode == null)
            {
                throw new InvalidDataException("EventCode is a required property for ModelEvent and cannot be null");
            }
            else
            {
                this.EventCode = EventCode;
            }
            // to ensure "EventTime" is required (not null)
            if (EventTime == null)
            {
                throw new InvalidDataException("EventTime is a required property for ModelEvent and cannot be null");
            }
            else
            {
                this.EventTime = EventTime;
            }
            this.Location = Location;
        }

        /// <summary>
        /// Gets or Sets EventCode
        /// </summary>
        [DataMember(Name = "eventCode", EmitDefaultValue = false)]
        public EventCode EventCode { get; set; } = default!;

        /// <summary>
        /// The date and time of an event for a shipment.
        /// </summary>
        /// <value>The date and time of an event for a shipment.</value>
        [DataMember(Name = "eventTime", EmitDefaultValue = false)]
        public DateTime? EventTime { get; set; }

        /// <summary>
        /// Gets or Sets Location
        /// </summary>
        [DataMember(Name = "location", EmitDefaultValue = false)]
        public Location Location { get; set; } = default!;

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class ModelEvent {\n");
            sb.Append("  EventCode: ").Append(EventCode).Append("\n");
            sb.Append("  EventTime: ").Append(EventTime).Append("\n");
            sb.Append("  Location: ").Append(Location).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object? input)
        {
            return Equals(input as ModelEvent);
        }

        /// <summary>
        /// Returns true if ModelEvent instances are equal
        /// </summary>
        /// <param name="input">Instance of ModelEvent to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(ModelEvent? input)
        {
            if (input == null)
                return false;

            return
                (
                    EventCode == input.EventCode ||
                    EventCode != null &&
                    EventCode.Equals(input.EventCode)
                ) &&
                (
                    EventTime == input.EventTime ||
                    EventTime != null &&
                    EventTime.Equals(input.EventTime)
                ) &&
                (
                    Location == input.Location ||
                    Location != null &&
                    Location.Equals(input.Location)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (EventCode != null)
                    hashCode = hashCode * 59 + EventCode.GetHashCode();
                if (EventTime != null)
                    hashCode = hashCode * 59 + EventTime.GetHashCode();
                if (Location != null)
                    hashCode = hashCode * 59 + Location.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
