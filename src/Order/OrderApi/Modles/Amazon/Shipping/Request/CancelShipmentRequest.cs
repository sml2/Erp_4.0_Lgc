﻿using OrderSDK.Modles.Amazon.Enums;
using System.ComponentModel.DataAnnotations;

namespace OrderSDK.Modles.Amazon.Shipping
{
    public class CancelShipmentRequest : RequestAmazonSP
    {
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.Shipping_CancelShipment;
        public override HttpMethod Method => HttpMethod.Post;

        public override string Url => $"/shipping/v1/shipments/{shipmentId}/cancel";

        [Required]
        public string shipmentId { get; set; } = string.Empty;
    }
}
