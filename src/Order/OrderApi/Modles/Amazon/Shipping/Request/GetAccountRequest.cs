﻿using OrderSDK.Modles.Amazon.Enums;

namespace OrderSDK.Modles.Amazon.Shipping
{
    public class GetAccountRequest : RequestAmazonSP
    {
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.Shipping_GetRates;
        public override HttpMethod Method => HttpMethod.Get;

        public override string Url => $"/shipping/v1/account";
    }
}
