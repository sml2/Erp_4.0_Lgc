﻿using OrderSDK.Modles.Amazon.Authorization;
using OrderSDK.Modles.Amazon.Enums;
using System.ComponentModel.DataAnnotations;

namespace OrderSDK.Modles.Amazon.Shipping
{
    public class GetShipmentRequest : RequestAmazonSP
    {
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.Shipping_GetShipment;
        public override HttpMethod Method => HttpMethod.Get;

        public override string Url => $"/shipping/v1/shipments/{shipmentId}";

        [Required]
        public string shipmentId { get; set; } = string.Empty;

        public bool IsNeedRestrictedDataToken { get; set; }
        public CreateRestrictedDataTokenRequest RestrictedDataTokenRequest { get; set; } = default!;
    }
}
