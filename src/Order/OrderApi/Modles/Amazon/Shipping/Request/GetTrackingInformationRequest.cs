﻿using OrderSDK.Modles.Amazon.Enums;
using System.ComponentModel.DataAnnotations;

namespace OrderSDK.Modles.Amazon.Shipping
{
    public class GetTrackingInformationRequest : RequestAmazonSP
    {
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.Shipping_GetTrackingInformation;
        public override HttpMethod Method => HttpMethod.Get;

        public override string Url => $"/shipping/v1/tracking/{trackingId}";

        [Required]
        public string trackingId { get; set; } = string.Empty;
    }
}
