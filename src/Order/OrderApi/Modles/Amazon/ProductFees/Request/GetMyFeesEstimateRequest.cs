﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using OrderSDK.Modles.Amazon.Enums;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;


namespace OrderSDK.Modles.Amazon.ProductFees
{
    public partial class GetMyFeesEstimateSRequest : RequestAmazonSP, IList<FeesEstimateByIdRequest>
    {
        public FeesEstimateByIdRequest this[int index] { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public override RateLimitType RateLimitType { get; set; } = RateLimitType.ProductFees_GetMyFeesEstimate;
        public override HttpMethod Method => HttpMethod.Post;

        public override string Url => $"/products/fees/v0/feesEstimate";
        //public FeesEstimateByIdRequest  FeesEstimateByIdRequests { get; set; }

        public int Count => throw new NotImplementedException();

        public bool IsReadOnly => throw new NotImplementedException();

        public void Add(FeesEstimateByIdRequest item)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(FeesEstimateByIdRequest item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(FeesEstimateByIdRequest[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<FeesEstimateByIdRequest> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public int IndexOf(FeesEstimateByIdRequest item)
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, FeesEstimateByIdRequest item)
        {
            throw new NotImplementedException();
        }

        public bool Remove(FeesEstimateByIdRequest item)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }


    public class FeesEstimateByIdRequest
    {
        public string IdValue { get; set; }
        public IdTypeEnum IdType { get; set; }
        public FeesEstimateRequest FeesEstimateRequest { get; set; }
    }


    [JsonConverter(typeof(StringEnumConverter))]
    public enum IdTypeEnum
    {
        /// <summary>
        /// An Amazon Standard Identification Number (ASIN) of a listings item.
        /// </summary>
        [EnumMember(Value = "ASIN")]
        ASIN,

        /// <summary>
        /// A selling partner provided identifier for an Amazon listing.
        /// </summary>
        [EnumMember(Value = "SellerSKU")]
        SellerSKU,
    }
}
