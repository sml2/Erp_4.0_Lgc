﻿
namespace OrderSDK.Modles.Amazon
{
    public class BaseAmazonOption
    {
        public static AmazonProxyOption amazonProxy { get; set; } = new AmazonProxyOption();
    }
    public class AmazonProxyOption
    {
        public string Address { get; set; }
    }
}
