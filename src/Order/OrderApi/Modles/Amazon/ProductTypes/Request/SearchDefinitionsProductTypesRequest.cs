﻿using Newtonsoft.Json;
using OrderSDK.Modles.Amazon.Enums;

namespace OrderSDK.Modles.Amazon.ProductTypes
{
    public class SearchDefinitionsProductTypesRequest : RequestAmazonSP
    {
        [JsonIgnore]
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.ProductTypes_SearchDefinitionsProductTypes;
        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;
        [JsonIgnore]
        public override string Url => $"/definitions/2020-09-01/productTypes";

        public IList<string> keywords { get; set; }
        public IList<string> marketplaceIds { get; set; } = new List<string>();
    }


    public class GetDefinitionsProductTypeRequest : RequestAmazonSP
    {
        [JsonIgnore]
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.ProductTypes_GetDefinitionsProductType;
        [JsonIgnore]
        public override HttpMethod Method => HttpMethod.Get;
        [JsonIgnore]
        public override string Url => $"/definitions/2020-09-01/productTypes/{productType}";

        public string productType { get; set; }

        /// <summary>
        /// 目前，仅支持一个市场
        /// </summary>
        public IList<string> marketplaceIds { get; set; } = new List<string>();
    }

}
