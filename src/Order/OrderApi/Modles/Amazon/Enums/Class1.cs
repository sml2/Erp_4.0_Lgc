﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderSDK.Modles.Amazon.Enums
{
    public enum GetTypes
    {
        Single, Multiple
    }

    public enum IdType
    {
        Asin,
        Sku,
    }
}
