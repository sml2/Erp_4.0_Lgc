﻿using static EnumExtension;

namespace OrderSDK.Modles.Amazon.Enums
{
    public enum BuyerInvoicePreference
    {
        [Description("应向买方开具个人发票")]
        INDIVIDUAL,
        [Description("买方应开具商业发票。可在BuyerTaxInformation结构中获取税务信息。")]
        BUSINESS

    }
}
