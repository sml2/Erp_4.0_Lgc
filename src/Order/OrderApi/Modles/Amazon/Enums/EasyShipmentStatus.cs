﻿using static EnumExtension;

namespace OrderSDK.Modles.Amazon.Enums
{
    public enum EasyShipmentStatus
    {
        [Description("亚马逊还没有从卖家那里拿到包裹")]
        PendingPickUp,
        [Description("卖方取消了取货")]
        LabelCanceled,
        [Description("亚马逊从卖家那里拿到了包裹")]
        Pickeup,
        [Description("包装在原产地履行中心")]
        OriginFC,
        [Description("包裹在目的地履行中心")]
        AtDestinationFC,
        [Description("包裹已送出")]
        OutForDelivery,
        [Description("包装被承运人损坏")]
        Damaged,
        [Description("包装已交付给买方")]
        Delivered,
        [Description("该包裹已被买方拒绝")]
        RejectedByBuyer,
        [Description("包裹无法送达")]
        Undeliverable,
        [Description("包裹未送达买方，已退回卖方")]
        ReturnedToSeller,
        [Description("包裹未交付给买方，正在退还给卖方")]
        ReturningToSeller,
    }
}
