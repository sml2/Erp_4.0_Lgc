﻿namespace OrderSDK.Modles.Amazon.Enums
{
    public enum Environment
    {
        Sandbox, Production
    }



    public enum TokenDataType
    {
        Normal,
        PII,
        Grantless
    }
}
