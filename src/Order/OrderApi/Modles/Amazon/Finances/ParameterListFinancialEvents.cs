﻿using Newtonsoft.Json;
using OrderSDK.Modles.Amazon.Enums;

namespace OrderSDK.Modles.Amazon.Finances
{
    public class ParameterListFinancialEvents : RequestAmazonSP
    {
        public override RateLimitType RateLimitType { get; set; } = RateLimitType.Financial_ListFinancialEventsByOrderId;
        public override HttpMethod Method => HttpMethod.Get;

        public override string Url => $"/finances/v0/orders/{orderId}/financialEvents";

        [JsonIgnore]
        public string orderId { get; set; }


        public int? MaxResultsPerPage { get; set; } = 100;
        public string NextToken { get; set; }

    }
}
