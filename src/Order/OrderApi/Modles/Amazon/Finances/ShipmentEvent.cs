/* 
 * Selling Partner API for Finances
 *
 * The Selling Partner API for Finances helps you obtain financial information relevant to a seller's business. You can obtain financial events for a given order, financial event group, or date range without having to wait until a statement period closes. You can also obtain financial event groups for a given date range.
 *
 * OpenAPI spec version: v0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace OrderSDK.Modles.Amazon.Finances
{
    /// <summary>
    /// A shipment, refund, guarantee claim, or chargeback.
    /// </summary>
    [DataContract]
    public partial class ShipmentEvent : IEquatable<ShipmentEvent>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShipmentEvent" /> class.
        /// </summary>
        /// <param name="AmazonOrderId">An Amazon-defined identifier for an order..</param>
        /// <param name="SellerOrderId">A seller-defined identifier for an order..</param>
        /// <param name="MarketplaceName">The name of the marketplace where the event occurred..</param>
        /// <param name="OrderChargeList">A list of order-level charges. These charges are applicable to Multi-Channel Fulfillment COD orders..</param>
        /// <param name="OrderChargeAdjustmentList">A list of order-level charge adjustments. These adjustments are applicable to Multi-Channel Fulfillment COD orders..</param>
        /// <param name="ShipmentFeeList">A list of shipment-level fees..</param>
        /// <param name="ShipmentFeeAdjustmentList">A list of shipment-level fee adjustments..</param>
        /// <param name="OrderFeeList">A list of order-level fees. These charges are applicable to Multi-Channel Fulfillment orders..</param>
        /// <param name="OrderFeeAdjustmentList">A list of order-level fee adjustments. These adjustments are applicable to Multi-Channel Fulfillment orders..</param>
        /// <param name="DirectPaymentList">A list of transactions where buyers pay Amazon through one of the credit cards offered by Amazon or where buyers pay a seller directly through COD..</param>
        /// <param name="PostedDate">The date and time when the financial event was posted..</param>
        /// <param name="ShipmentItemList">ShipmentItemList.</param>
        /// <param name="ShipmentItemAdjustmentList">A list of shipment item adjustments..</param>
        public ShipmentEvent(string AmazonOrderId = default, string SellerOrderId = default, string MarketplaceName = default, ChargeComponentList OrderChargeList = default, ChargeComponentList OrderChargeAdjustmentList = default, FeeComponentList ShipmentFeeList = default, FeeComponentList ShipmentFeeAdjustmentList = default, FeeComponentList OrderFeeList = default, FeeComponentList OrderFeeAdjustmentList = default, DirectPaymentList DirectPaymentList = default, DateTime? PostedDate = default, ShipmentItemList ShipmentItemList = default, ShipmentItemList ShipmentItemAdjustmentList = default)
        {
            this.AmazonOrderId = AmazonOrderId;
            this.SellerOrderId = SellerOrderId;
            this.MarketplaceName = MarketplaceName;
            this.OrderChargeList = OrderChargeList;
            this.OrderChargeAdjustmentList = OrderChargeAdjustmentList;
            this.ShipmentFeeList = ShipmentFeeList;
            this.ShipmentFeeAdjustmentList = ShipmentFeeAdjustmentList;
            this.OrderFeeList = OrderFeeList;
            this.OrderFeeAdjustmentList = OrderFeeAdjustmentList;
            this.DirectPaymentList = DirectPaymentList;
            this.PostedDate = PostedDate;
            this.ShipmentItemList = ShipmentItemList;
            this.ShipmentItemAdjustmentList = ShipmentItemAdjustmentList;
        }

        /// <summary>
        /// An Amazon-defined identifier for an order.
        /// </summary>
        /// <value>An Amazon-defined identifier for an order.</value>
        [DataMember(Name = "AmazonOrderId", EmitDefaultValue = false)]
        public string AmazonOrderId { get; set; }

        /// <summary>
        /// A seller-defined identifier for an order.
        /// </summary>
        /// <value>A seller-defined identifier for an order.</value>
        [DataMember(Name = "SellerOrderId", EmitDefaultValue = false)]
        public string SellerOrderId { get; set; }

        /// <summary>
        /// The name of the marketplace where the event occurred.
        /// </summary>
        /// <value>The name of the marketplace where the event occurred.</value>
        [DataMember(Name = "MarketplaceName", EmitDefaultValue = false)]
        public string MarketplaceName { get; set; }

        /// <summary>
        /// A list of order-level charges. These charges are applicable to Multi-Channel Fulfillment COD orders.
        /// </summary>
        /// <value>A list of order-level charges. These charges are applicable to Multi-Channel Fulfillment COD orders.</value>
        [DataMember(Name = "OrderChargeList", EmitDefaultValue = false)]
        public ChargeComponentList OrderChargeList { get; set; }

        /// <summary>
        /// A list of order-level charge adjustments. These adjustments are applicable to Multi-Channel Fulfillment COD orders.
        /// </summary>
        /// <value>A list of order-level charge adjustments. These adjustments are applicable to Multi-Channel Fulfillment COD orders.</value>
        [DataMember(Name = "OrderChargeAdjustmentList", EmitDefaultValue = false)]
        public ChargeComponentList OrderChargeAdjustmentList { get; set; }

        /// <summary>
        /// A list of shipment-level fees.
        /// </summary>
        /// <value>A list of shipment-level fees.</value>
        [DataMember(Name = "ShipmentFeeList", EmitDefaultValue = false)]
        public FeeComponentList ShipmentFeeList { get; set; }

        /// <summary>
        /// A list of shipment-level fee adjustments.
        /// </summary>
        /// <value>A list of shipment-level fee adjustments.</value>
        [DataMember(Name = "ShipmentFeeAdjustmentList", EmitDefaultValue = false)]
        public FeeComponentList ShipmentFeeAdjustmentList { get; set; }

        /// <summary>
        /// A list of order-level fees. These charges are applicable to Multi-Channel Fulfillment orders.
        /// </summary>
        /// <value>A list of order-level fees. These charges are applicable to Multi-Channel Fulfillment orders.</value>
        [DataMember(Name = "OrderFeeList", EmitDefaultValue = false)]
        public FeeComponentList OrderFeeList { get; set; }

        /// <summary>
        /// A list of order-level fee adjustments. These adjustments are applicable to Multi-Channel Fulfillment orders.
        /// </summary>
        /// <value>A list of order-level fee adjustments. These adjustments are applicable to Multi-Channel Fulfillment orders.</value>
        [DataMember(Name = "OrderFeeAdjustmentList", EmitDefaultValue = false)]
        public FeeComponentList OrderFeeAdjustmentList { get; set; }

        /// <summary>
        /// A list of transactions where buyers pay Amazon through one of the credit cards offered by Amazon or where buyers pay a seller directly through COD.
        /// </summary>
        /// <value>A list of transactions where buyers pay Amazon through one of the credit cards offered by Amazon or where buyers pay a seller directly through COD.</value>
        [DataMember(Name = "DirectPaymentList", EmitDefaultValue = false)]
        public DirectPaymentList DirectPaymentList { get; set; }

        /// <summary>
        /// The date and time when the financial event was posted.
        /// </summary>
        /// <value>The date and time when the financial event was posted.</value>
        [DataMember(Name = "PostedDate", EmitDefaultValue = false)]
        public DateTime? PostedDate { get; set; }

        /// <summary>
        /// Gets or Sets ShipmentItemList
        /// </summary>
        [DataMember(Name = "ShipmentItemList", EmitDefaultValue = false)]
        public ShipmentItemList ShipmentItemList { get; set; }

        /// <summary>
        /// A list of shipment item adjustments.
        /// </summary>
        /// <value>A list of shipment item adjustments.</value>
        [DataMember(Name = "ShipmentItemAdjustmentList", EmitDefaultValue = false)]
        public ShipmentItemList ShipmentItemAdjustmentList { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class ShipmentEvent {\n");
            sb.Append("  AmazonOrderId: ").Append(AmazonOrderId).Append("\n");
            sb.Append("  SellerOrderId: ").Append(SellerOrderId).Append("\n");
            sb.Append("  MarketplaceName: ").Append(MarketplaceName).Append("\n");
            sb.Append("  OrderChargeList: ").Append(OrderChargeList).Append("\n");
            sb.Append("  OrderChargeAdjustmentList: ").Append(OrderChargeAdjustmentList).Append("\n");
            sb.Append("  ShipmentFeeList: ").Append(ShipmentFeeList).Append("\n");
            sb.Append("  ShipmentFeeAdjustmentList: ").Append(ShipmentFeeAdjustmentList).Append("\n");
            sb.Append("  OrderFeeList: ").Append(OrderFeeList).Append("\n");
            sb.Append("  OrderFeeAdjustmentList: ").Append(OrderFeeAdjustmentList).Append("\n");
            sb.Append("  DirectPaymentList: ").Append(DirectPaymentList).Append("\n");
            sb.Append("  PostedDate: ").Append(PostedDate).Append("\n");
            sb.Append("  ShipmentItemList: ").Append(ShipmentItemList).Append("\n");
            sb.Append("  ShipmentItemAdjustmentList: ").Append(ShipmentItemAdjustmentList).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return Equals(input as ShipmentEvent);
        }

        /// <summary>
        /// Returns true if ShipmentEvent instances are equal
        /// </summary>
        /// <param name="input">Instance of ShipmentEvent to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(ShipmentEvent input)
        {
            if (input == null)
                return false;

            return
                (
                    AmazonOrderId == input.AmazonOrderId ||
                    AmazonOrderId != null &&
                    AmazonOrderId.Equals(input.AmazonOrderId)
                ) &&
                (
                    SellerOrderId == input.SellerOrderId ||
                    SellerOrderId != null &&
                    SellerOrderId.Equals(input.SellerOrderId)
                ) &&
                (
                    MarketplaceName == input.MarketplaceName ||
                    MarketplaceName != null &&
                    MarketplaceName.Equals(input.MarketplaceName)
                ) &&
                (
                    OrderChargeList == input.OrderChargeList ||
                    OrderChargeList != null &&
                    OrderChargeList.Equals(input.OrderChargeList)
                ) &&
                (
                    OrderChargeAdjustmentList == input.OrderChargeAdjustmentList ||
                    OrderChargeAdjustmentList != null &&
                    OrderChargeAdjustmentList.Equals(input.OrderChargeAdjustmentList)
                ) &&
                (
                    ShipmentFeeList == input.ShipmentFeeList ||
                    ShipmentFeeList != null &&
                    ShipmentFeeList.Equals(input.ShipmentFeeList)
                ) &&
                (
                    ShipmentFeeAdjustmentList == input.ShipmentFeeAdjustmentList ||
                    ShipmentFeeAdjustmentList != null &&
                    ShipmentFeeAdjustmentList.Equals(input.ShipmentFeeAdjustmentList)
                ) &&
                (
                    OrderFeeList == input.OrderFeeList ||
                    OrderFeeList != null &&
                    OrderFeeList.Equals(input.OrderFeeList)
                ) &&
                (
                    OrderFeeAdjustmentList == input.OrderFeeAdjustmentList ||
                    OrderFeeAdjustmentList != null &&
                    OrderFeeAdjustmentList.Equals(input.OrderFeeAdjustmentList)
                ) &&
                (
                    DirectPaymentList == input.DirectPaymentList ||
                    DirectPaymentList != null &&
                    DirectPaymentList.Equals(input.DirectPaymentList)
                ) &&
                (
                    PostedDate == input.PostedDate ||
                    PostedDate != null &&
                    PostedDate.Equals(input.PostedDate)
                ) &&
                (
                    ShipmentItemList == input.ShipmentItemList ||
                    ShipmentItemList != null &&
                    ShipmentItemList.Equals(input.ShipmentItemList)
                ) &&
                (
                    ShipmentItemAdjustmentList == input.ShipmentItemAdjustmentList ||
                    ShipmentItemAdjustmentList != null &&
                    ShipmentItemAdjustmentList.Equals(input.ShipmentItemAdjustmentList)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (AmazonOrderId != null)
                    hashCode = hashCode * 59 + AmazonOrderId.GetHashCode();
                if (SellerOrderId != null)
                    hashCode = hashCode * 59 + SellerOrderId.GetHashCode();
                if (MarketplaceName != null)
                    hashCode = hashCode * 59 + MarketplaceName.GetHashCode();
                if (OrderChargeList != null)
                    hashCode = hashCode * 59 + OrderChargeList.GetHashCode();
                if (OrderChargeAdjustmentList != null)
                    hashCode = hashCode * 59 + OrderChargeAdjustmentList.GetHashCode();
                if (ShipmentFeeList != null)
                    hashCode = hashCode * 59 + ShipmentFeeList.GetHashCode();
                if (ShipmentFeeAdjustmentList != null)
                    hashCode = hashCode * 59 + ShipmentFeeAdjustmentList.GetHashCode();
                if (OrderFeeList != null)
                    hashCode = hashCode * 59 + OrderFeeList.GetHashCode();
                if (OrderFeeAdjustmentList != null)
                    hashCode = hashCode * 59 + OrderFeeAdjustmentList.GetHashCode();
                if (DirectPaymentList != null)
                    hashCode = hashCode * 59 + DirectPaymentList.GetHashCode();
                if (PostedDate != null)
                    hashCode = hashCode * 59 + PostedDate.GetHashCode();
                if (ShipmentItemList != null)
                    hashCode = hashCode * 59 + ShipmentItemList.GetHashCode();
                if (ShipmentItemAdjustmentList != null)
                    hashCode = hashCode * 59 + ShipmentItemAdjustmentList.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
