﻿using OrderSDK.Modles.Amazon;
using OrderSDK.Modles.Amazon.Authorization;
using OrderSDK.Services;
using static OrderSDK.Modles.Amazon.Authorization.CacheTokenData;

namespace OrderSDK.Modles.CommonType;

public partial class StoreRegion
{
    public StoreRegion( int id,string name,List<string> marketPlaces,  AmazonToken amazonToken, AmazonCredential _amazonCredential)
    {
        ID = id;
        Name = name;
        MarketPlaces = marketPlaces;      
        this.amazonToken = amazonToken;
        this.amazonCredential = _amazonCredential;
    }

    public AmazonCredential amazonCredential { get; set; }
    public AmazonCredential GetAmazonCredential()
    {
        if (amazonCredential is null)
        {
            throw new ArgumentNullException($"{nameof(amazonCredential)} is null");
        }
        return amazonCredential;
    }
  
    public AmazonToken amazonToken { get; set; }
}

public class AmazonToken
{
    public AmazonToken()
    {

    }

    public AmazonToken(string accessToken, string refreshToken, int expireIn, DateTime date_Created, string tokenType, string sellerID, int? platformDataID, int keyID)
    {
        AccessToken = accessToken;
        RefreshToken = refreshToken;
        ExpireIn = expireIn;
        Date_Created = date_Created;
        TokenType = tokenType;
        SellerID = sellerID;
        PlatformDataID = platformDataID;
        this.keyID = keyID;
    }

    #region token
    public string AccessToken { get; set; } = string.Empty;

    public string RefreshToken { get; set; } = string.Empty;

    public int ExpireIn { get; set; }

    public DateTime Date_Created { get; set; } = DateTime.UtcNow;

    public string TokenType { get; set; }

    public string SellerID { get; set; }

    /// <summary>
    /// 亚马逊市场平台ID
    /// </summary>
    public int? PlatformDataID { get; set; }

    /// <summary>
    /// 亚马逊开发者账号id
    /// </summary>
    public int keyID { get; set; }

    #endregion



    /// <summary>
    /// 构造token对象
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    internal TokenResponse? GetToken()
    {
        TokenResponse? token = new TokenResponse(AccessToken, RefreshToken, TokenType, ExpireIn, Date_Created);

        if (token == null)
            return null!;
        else
        {
            var isExpired = IsTokenExpired(token.expires_in, token.date_Created);
            if (!isExpired)
            {
                return token;
            }
            else return null!;
        }
    }


    /// <summary>
    /// 保存token
    /// </summary>
    /// <param name="tokenDataType"></param>
    /// <param name="token"></param>
    public void SetToken(TokenResponse token, TokenDataType tokenDataType = TokenDataType.Normal)
    {
        AccessToken = token.access_token;
        RefreshToken = token.refresh_token;
        ExpireIn = token.expires_in;
        Date_Created = token.date_Created;
        TokenType = token.token_type;
    }


    public static bool IsTokenExpired(int? expiresIn, DateTime? dateCreated)
    {
        if (dateCreated == null)
            return false;
        else   //返回一个新的 System.TimeSpan 减去指定的日期和时间这个实例的值。
                // 60
            return DateTime.Now.Subtract((DateTime)dateCreated).TotalSeconds > (expiresIn - 300); //Add Margent to a void expaired token
    }
}

