﻿using System.Text.Json.Serialization;

namespace OrderSDK.Modles.CommonType;
public partial class StoreRegion
{
    public class ShopeeData
    {
        public ShopeeData()
        {

        }

        public ShopeeData( string remark)
        {           
            Remark = remark;            
        }

        /// <summary>
        /// shopID,MerchantID
        /// </summary>
        public int? ShopID { get; set; }

        public int? MainAccountId { get; set; }

        public int keyID { get; set; }
        public int PlatformDataID { get; set; } = 0;

        public string AccessToken { get; set; } = string.Empty;

        public string RefreceToken { get; set; } = string.Empty;

        public int Expire_in { get; set; }

        public DateTime? Time { get; set; } 

        public CountryTypes CountryType { get; set; }

        public enum CountryTypes
        {
            Single, Multiple, None
        }

        public string? Remark { get; set; }

        [JsonIgnore]
        public bool flag { get; set; }
       
        public TokenInfo? token
        {
            get
            {
                if (RefreceToken.IsNullOrWhiteSpace()) return null;
                return new TokenInfo(AccessToken, RefreceToken, Expire_in, Time);
            }
        }

        /// 取出token
        /// </summary>
        /// <param name="tokenDataType"></param>
        /// <returns></returns>
        public TokenInfo GetToken()
        {
            if (token == null || token.DateCreated is null)
                return null!;
            else
            {
                var isExpired = IsTokenExpired(token.ExpireIn, token.DateCreated);
                if (!isExpired)
                    return token;
                else return null!;
            }
        }


        /// <summary>
        /// 保存token
        /// </summary>
        /// <param name="tokenDataType"></param>
        /// <param name="token"></param>
        public void SetToken(TokenInfo token)
        {
            AccessToken = token.AccessToken;
            RefreceToken = token.RefreshAccessToken;
            Expire_in = token.ExpireIn;
            Time = token.DateCreated;
        }

        public static bool IsTokenExpired(int? expiresIn, DateTime? dateCreated)
        {
            if (dateCreated == null)
                return false;
            else
                return DateTime.UtcNow.Subtract((DateTime)dateCreated).TotalSeconds > (expiresIn - 60);
        }
    }

    public class TokenInfo
    {       
        public TokenInfo(string accessToken, string refreshAccessToken, int expireIn, DateTime? dateCreated)
        {
            AccessToken = accessToken;
            RefreshAccessToken = refreshAccessToken;
            ExpireIn = expireIn;
            DateCreated = dateCreated;
        }

        public string AccessToken { get; set; } = string.Empty;

        public string RefreshAccessToken { get; set; } = string.Empty;

        public int ExpireIn { get; set; }

        public DateTime? DateCreated { get; set; }
    }
}
