﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using HotChocolate;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using OrderSDK.Modles.Amazon;
using OrderSDK.Modles.Amazon.Authorization;
using OrderSDK.Services;
using static OrderSDK.Modles.Amazon.Authorization.CacheTokenData;

namespace OrderSDK.Modles.CommonType;

[GraphQLName("OrderSDKStoreRegion")]
public partial class StoreRegion
{   
    public int ID { get; set; }

    /// <summary>
    /// 店铺名称
    /// </summary>    
    public string Name { get; set; }
    public List<string> MarketPlaces { get; set; }
     
}

