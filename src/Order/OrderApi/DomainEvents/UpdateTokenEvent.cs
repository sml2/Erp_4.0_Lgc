﻿using MediatR;
using OrderSDK.Modles.Amazon.Authorization;

namespace OrderSDK.DomainEvents;
/// <summary>
/// 订单创建成功事件
/// </summary>
public class UpdateTokenEvent : INotification
{
    public UpdateTokenEvent(int storeId, TokenResponse token, bool flag = true)
    {
        StoreId = storeId;
        Token = token;
        Flag = flag;
    }

    public int StoreId { get; }
    public TokenResponse Token { get; }
    public bool Flag { get; }
}