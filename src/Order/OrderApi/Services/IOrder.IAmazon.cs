using OrderSDK.Modles.Amazon.Authorization;
using OrderSDK.Modles.Amazon.CatalogItems;
using OrderSDK.Modles.Amazon.Feeds;
using OrderSDK.Modles.Amazon.Finances;
using OrderSDK.Modles.Amazon.Order;
using OrderSDK.Modles.Amazon.ProductFees;
using OrderSDK.Modles.Amazon.ProductPricing;
using OrderSDK.Modles.Amazon.ProductTypes;
using OrderSDK.Modles.Amazon.Sellers;
using OrderSDK.Modles.Amazon;
using OrderSDK.Modles.CommonType;
using ListingsItem = OrderSDK.Modles.Amazon.ListingsItems.Item;
using OrderSDK.Modles.Amazon.Order;
using OrderSDK.Modles.Amazon.vm;
using OrderSDK.Services;
using OrderSDK.Modles.Amazon.Enums;
using Newtonsoft.Json.Schema;
using OrderSDK.Modles.Amazon.Reports;
using OrderSDK.Modles;

namespace OrderSDK.Services
{

    public interface IAmazon
    {
        /// <summary>
        /// 订单拉取方式
        /// </summary>
        GetTypes SupportedGetType { get; }

        /// <summary>
        /// 分批次拉取方式中，每次拉取数量
        /// </summary>
        int MaxGet { get; }

        /// <summary>
        /// 单次拉取中，拉取数量
        /// </summary>
        int MaxGets { get; }


        #region 店铺基本操作

        /// <summary>
        /// 获取授权url
        /// </summary>
        /// <param name="orgUrl"></param>
        /// <param name="ApplicationId"></param>
        /// <param name="RedirectUri"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        string GetAuthURL(string orgUrl, string ApplicationId, string RedirectUri, string state);


        /// <summary>
        /// 验证授权
        /// </summary>
        /// <param name="store"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        Task<TokenResponse> VerifyStore(StoreRegion store, string code);



        /// <summary>
        /// 刷新token
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        Task<TokenResponse?> RefreshNormalAccessToken(StoreRegion store);




        /// <summary>
        /// 获取店铺下市场信息
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        Task<(string, MarketplaceParticipationList?)> GetMarketIds(StoreRegion store);


        #endregion



        #region 订单

        /// <summary>
        /// 拉取订单
        /// </summary>
        /// <param name="listParameter"></param>
        /// <returns></returns>
        Task<(string, OrderLists)> ListImp(ListParameter listParameter);

        /// <summary>
        /// 获取订单的基本信息
        /// </summary>
        /// <param name="store"></param>
        /// <param name="OrderNo"></param>
        /// <returns></returns>
        Task<(string, Order?)> GetOrder(StoreRegion store, string OrderNo);

        /// <summary>
        ///  获取订单中产品采购信息  orderItem
        /// </summary>
        /// <param name="store"></param>
        /// <param name="OrderNo"></param>
        /// <returns></returns>

        Task<OrderItemListResponse> GetOrderItem(StoreRegion store, string OrderNo);


        /// <summary>
        /// 获取订单预估费用信息
        /// </summary>
        /// <param name="store"></param>
        /// <param name="marketPlaceId"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        Task<(string, FeesEstimateResult?)> GetFeesEstimate(StoreRegion store, string marketPlaceId, OrderItem item);



        /// <summary>
        /// 获取订单地址
        /// </summary>
        /// <param name="store"></param>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        Task<(string, OrderAddress?)> GetAddress(StoreRegion store, string orderNo);


        /// <summary>
        /// 获取订单购买者信息
        /// </summary>
        /// <param name="store"></param>
        /// <param name="orderNo"></param>
        /// <returns></returns>

        Task<(string, OrderBuyerInfo?)> GetOrderBuyerInfo(StoreRegion store, string orderNo);
        
        Task<(string, OrderItemsBuyerInfoList Payload)> GetOrderItemsBuyerInfo(StoreRegion store, string orderNo);


        /// <summary>
        /// 获取订单费用信息
        /// </summary>
        /// <param name="store"></param>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        Task<(string, List<FinancialEvents>?)> ListFinancialEventsByOrderId(StoreRegion store, string orderNo);


        #endregion



        #region 产品信息

        /// <summary>
        /// 根据asin获取产品属性
        /// </summary>
        /// <param name="ASIN"></param>
        /// <param name="marketPlaceId"></param>
        /// <param name="store"></param>
        /// <returns></returns>
        Task<(string, Item?)> GetCatalogItem(string ASIN, string marketPlaceId, StoreRegion store);


        /// <summary>
        /// 根据Sku，查询产品信息
        /// </summary>
        /// <param name="store"></param>
        /// <param name="marketPlaceId"></param>
        /// <param name="sku"></param>
        /// <returns></returns>
        Task<ListingsItem?> getListingsItem(StoreRegion store, string marketPlaceId, string sku);


        /// <summary>
        /// 获取产品价格
        /// </summary>
        /// <param name="store"></param>
        /// <param name="marketPlaceId"></param>
        /// <param name="ids"></param>
        /// <param name="type"></param>
        /// <returns></returns>

        Task<(string, PriceList?)> GetPricing(StoreRegion store, string marketPlaceId, List<string> ids, IdType type = IdType.Asin);

        /// <summary>
        /// 获取产品信息(包含的信息更多)
        /// </summary>
        /// <param name="store"></param>
        /// <param name="marketPlaceIds"></param>
        /// <param name="ids"></param>
        /// <param name="type"></param>
        /// <param name="vendor"></param>
        /// <returns></returns>

        Task<List<ItemRes>> SearchCatalogItems(StoreRegion store, string marketPlaceId, List<string> ids, IdentifiersType type = IdentifiersType.ASIN, bool vendor = false);


        /// <summary>
        /// 获取指定店铺指定市场下的所有产品
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        Task<ReportDocument?> GetAllAsin(StoreRegion store);




        #endregion

        /// <summary>
        /// 亚马逊目录
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        Task<CategoiesData?> GetReportBowerID(StoreRegion store, string marketPlace);

        /// <summary>
        /// 提交产品
        /// </summary>
        /// <param name="store"></param>
        /// <param name="dataJson"></param>
        /// <param name="MarketPlaceId"></param>
        /// <returns></returns>
        Task<(FeedResult?, string)> SubmitProducts(StoreRegion store, string dataJson, string MarketPlaceId);




        /// <summary>
        /// 更新订单信息到亚马逊
        /// </summary>
        /// <param name="store"></param>
        /// <param name="DP"></param>
        /// <param name="MarketPlaceId"></param>
        /// <returns></returns>
        Task<string> DeliveryGoods(StoreRegion store, List<DeliverProduct> DP, string MarketPlaceId);





        //string AddProductMessage(StoreRegion store, string MarketPlaceId);


        Task<string> UploadExcelUrl(StoreRegion store, string MarketPlaceId, string fileExcelUrl);


        Task<(string, List<pushProductResult>)> UploadExcelBytes(StoreRegion store, string MarketPlaceId, byte[] bytes);

      
        Task<(FeedResult?, string)> GetFeedDetail(string feedID, StoreRegion store);


        /// <summary>
        /// 搜索并返回具有可用定义的亚马逊产品类型列表
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>

        Task<ProductTypeList?> SearchDefinitionsProductTypes(StoreRegion store);

        /// <summary>
        /// 检索 Amazon 产品类型定义
        /// </summary>
        /// <param name="store"></param>
        /// <param name="productType"></param>
        /// <returns></returns>
        Task<ProductTypeDefinition?> GetDefinitionsProductTypes(StoreRegion store, string marketPlace, string productType);


        Task<(JSchema?, ProductDesc)?> GetProductTypeModels(StoreRegion store, string marketPlace, string productTypeName);
    }
}