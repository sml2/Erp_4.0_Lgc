using Microsoft.Extensions.DependencyInjection;
using OrderSDK.Modles.Amazon.Runtime;
using OrderSDK.Services.Amazon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderSDK.Services
{
    public static class OrderApiServiceInjectionExtensions
    {
        public static IServiceCollection AddOrderApi(this IServiceCollection service)
        {
            #region amazon
            service.AddScoped<IAmazon,AmazonService>();
            service.AddScoped<AmazonClient>();           
            service.AddScoped<LWAClient>();
            service.AddScoped<LWAAuthorizationCredentials>();

            #endregion

            #region shopee
            service.AddScoped<IShopee,ShopeeService>();
            service.AddScoped<ShopeeClient>();
            #endregion

            return service;
        }
    }
}
