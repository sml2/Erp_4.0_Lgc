using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OrderSDK.Modles;
using OrderSDK.Modles.CommonType;
using OrderSDK.Modles.Shopee;
using OrderSDK.Modles.Shopee.Auth;
using OrderSDK.Modles.Shopee.BaseHelp;
using OrderSDK.Modles.Shopee.Comm;
using OrderSDK.Modles.Shopee.Enums;
using OrderSDK.Services.Shopee.Extension;
using static OrderSDK.Modles.CommonType.StoreRegion;

namespace OrderSDK.Services;

public class ShopeeClient: Client
{
    public  readonly ShopeeOption shopeeOption;
    private readonly HttpClient HttpClient;
    private readonly ILogger<ShopeeClient> logger;
    public  ShopeeClient(IHttpClientFactory IHttpClientFactory, ILogger<ShopeeClient> _logger)
    {
        if (HttpClient is null)
        {
            HttpClient = IHttpClientFactory.CreateClient(nameof(ShopeeClient));
        }
        logger = _logger;
        shopeeOption = BaseOption.ShopeeOption;
    }   
  

    public CommonParam GetCommonParams(RequestShopee requestBase)
    {
        long timest = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
        string base_string = shopeeOption.PartnerId + requestBase.EndPoint + timest;
        string sign = EncryptionHelper.GenerateHMAC(shopeeOption.SecretKey, base_string);
        CommonParam common = new CommonParam()
        {
            Sign = sign,
            PartnerId = shopeeOption.PartnerId,
            Timestamp = timest

        };
        return common;
    }


    public CommonShopParam GetShopParams(RequestShopee requestBase)
    {
        var key = requestBase.shopeedata!.GetToken();
        long timest = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
        string base_string = shopeeOption.PartnerId + requestBase.EndPoint + timest + key.AccessToken + requestBase.ShopId;
        string sign = EncryptionHelper.GenerateHMAC(shopeeOption.SecretKey, base_string);

        CommonShopParam shopParam = new CommonShopParam()
        {
            PartnerId = shopeeOption.PartnerId,
            ShopId = requestBase.ShopId,
            AccessToken = key.AccessToken,
            Sign = sign,
            Timestamp = timest
        };
        return shopParam;
    }

    public CommonMerchantParam GetMerchantParams(RequestShopee requestBase)
    {
        var key = requestBase.shopeedata!.GetToken();
        long timest = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
        string base_string = shopeeOption.PartnerId + requestBase.EndPoint + timest + key.AccessToken + requestBase.MerchantId;
        string sign = EncryptionHelper.GenerateHMAC(shopeeOption.SecretKey, base_string);
        CommonMerchantParam merchantParam = new CommonMerchantParam()
        {
            PartnerId = shopeeOption.PartnerId,
            MerchantId = requestBase.MerchantId,
            AccessToken = key.AccessToken,
            Sign = sign,
            Timestamp = timest
        };
        return merchantParam;
    }

     

    public async Task<HttpResponseMessage> SendRequest(RequestShopee request)
    {        
        string query = SendRequestParam(request);
        string baseAdress = shopeeOption.ApiMode ? shopeeOption.Production_URL : shopeeOption.SandBox_URL;
        HttpRequestMessage request_message = new HttpRequestMessage(request.Method, $"{ baseAdress+ request.EndPoint}?{query}");
        if (request.Method == HttpMethod.Get)
        {
            var requestStr = request.ToQueryString();
            logger.LogInformation($"Method Name:{request.GetType().Name}", query, requestStr);
            request_message = new HttpRequestMessage(request.Method, $"{shopeeOption.SandBox_URL + request.EndPoint}?{query}&{requestStr}");
            if ("/api/v2/auth/token/get".Equals(request.Method))
            {
                HttpClient http = new HttpClient(new HttpClientHandler() {
                    AllowAutoRedirect = false
                });
                var newresponse =await  http.SendAsync(request_message);
                return newresponse;
            }
            var response =await  HttpClient.SendAsync(request_message);
            return response;
        }
        else
        {
            string payload = request.ToPayload();
            logger.LogInformation($"Method Name:{request.GetType().Name}", query, payload);
            request_message.Content = new StringContent(payload, null, "application/json");
            var response =await HttpClient.SendAsync(request_message);
            return response;
        }
     
    }

    public async Task<HttpResponseMessage> TestSendRequest(RequestShopee request, string json)
    {
        string query = SendRequestParam(request);
        string baseAdress = shopeeOption.ApiMode ? shopeeOption.Production_URL : shopeeOption.SandBox_URL;
        HttpRequestMessage request_message = new HttpRequestMessage(request.Method, $"{baseAdress + request.EndPoint}?{query}");

        string payload = json;
        logger.LogInformation($"Method Name:{request.GetType().Name}", query, payload);
        request_message.Content = new StringContent(payload, null, "application/json");
        var response = await HttpClient.SendAsync(request_message);
        return response;


    }

    public async Task<T>? SendTokenRequestAsync<T>(RequestShopee request)
    {
        T desc = default(T)!;
        try
        {
            var response = await SendRequest(request);
            string content = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
            {
                try
                {
                    desc = JsonConvert.DeserializeObject<T>(content);
                }
                catch (Exception e)
                {
                    logger.LogError($"结果解析失败，解析类型目标类型：{request.GetType().Name},返回的数据为：{0}", content);
                }
            }
        }
        catch (Exception e)
        {
            logger.LogWarning($"http request fail：{e.Message}");
        }
        return desc;
    }

    public async Task<ShopeeResponseBase<T>> SendRequestAsync<T>(RequestShopee request)
    {
        ShopeeResponseBase<T> desc = default(ShopeeResponseBase<T>)!;
        try
        {
            if (request.EndPoint != "/api/v2/auth/token/get" && request.EndPoint != "/api/v2/auth/access_token/get")
            {
                var data = request.shopeedata;
                if (data is null || data.GetToken() is null)
                {
                    await CheckToken(request);
                }
            }
            var response = await SendRequest(request);
            string content = await response.Content.ReadAsStringAsync();
            try
            {
                desc = JsonConvert.DeserializeObject<ShopeeResponseBase<T>>(content) ?? default!;
            }
            catch (Exception e)
            {
                logger.LogError($"结果解析失败，解析类型目标类型：{request.GetType().Name},返回的数据为：{0}", content);
                //return new ResponseResult<T>(new ParseResultError(typeof(T).ToString(),e.Message));
            }
        }
        catch (Exception e)
        {
            logger.LogWarning($"http request fail：{e.Message}");
            // return new ResponseResult<T>(new NetworkRequestError($"{e.Message}"));
        }
        return desc;
    }

    public async Task<ShopeeResponseBase<T>> TestSendRequestAsync<T>(RequestShopee request,string json)
    {
        ShopeeResponseBase<T> desc = default(ShopeeResponseBase<T>)!;
        try
        {
            if (request.EndPoint != "/api/v2/auth/token/get" && request.EndPoint != "/api/v2/auth/access_token/get")
            {
                var data = request.shopeedata;
                if (data is null || data.GetToken() is null)
                {
                    await CheckToken(request);
                }
            }
            var response = await TestSendRequest(request,json);
            string content = await response.Content.ReadAsStringAsync();
            try
            {
                desc = JsonConvert.DeserializeObject<ShopeeResponseBase<T>>(content) ?? default!;
            }
            catch (Exception e)
            {
                logger.LogError($"结果解析失败，解析类型目标类型：{request.GetType().Name},返回的数据为：{0}", content);
                //return new ResponseResult<T>(new ParseResultError(typeof(T).ToString(),e.Message));
            }
        }
        catch (Exception e)
        {
            logger.LogWarning($"http request fail：{e.Message}");
            // return new ResponseResult<T>(new NetworkRequestError($"{e.Message}"));
        }
        return desc;
    }



    public async Task<bool> CheckToken(RequestShopee shopee)
    {
        var key = shopee.shopeedata;
        ResponseAccessToken token = new ResponseAccessToken();
        RequestRefreshAccessToken request = new RequestRefreshAccessToken();
        if (key.CountryType == ShopeeData.CountryTypes.Single)
        {
            request.ShopId = key.ShopID;
            //request.MerchantId = 0;
        }
        else if (key.CountryType == ShopeeData.CountryTypes.Multiple)
        {
            //request.ShopId = 0;
            request.MerchantId = key.MainAccountId;
        }
        request.PartnerId = shopeeOption.PartnerId;
        request.RefreshToken = key.RefreceToken;
        request.shopeedata = key;
        var response = await SendRequest(request);
        string content = await response.Content.ReadAsStringAsync();
        if (response.IsSuccessStatusCode)
        {
            try
            {
                token = JsonConvert.DeserializeObject<ResponseAccessToken>(content) ?? null!;
            }
            catch (Exception e)
            {
                logger.LogError($"结果解析失败，解析类型目标类型：{request.GetType().Name},返回的数据为：{0}", content);
                //return new ResponseShopee<T>(new ParseResultError(typeof(T).ToString(), e.Message));
            }           
        }
        else
        {
            logger.LogWarning($"http request fail,httpStatusCode：{response.StatusCode.ToString()}，errorMessage：{0}", content);
            //return new ResponseShopee<T>(new NetworkRequestError($"{response.StatusCode.ToString()}-{content}"));
        }

        if (token is not null && token.Error.IsNullOrWhiteSpace())
        {
            TokenInfo info = new(token.AccessToken, token.RefreshToken, token.ExpireIn, DateTime.Now);
            key.SetToken(info);          
            return true;
        }
        else
        {
            Console.WriteLine("shopee更新token失败");
            return false;
        }
    }


    /// <summary>
    /// 表单数据提交
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="request"></param>
    /// <returns></returns>
    public async Task<ShopeeResponseBase<T>> SendFormDataRequestAsync<T>(RequestShopee request)
    {
        ShopeeResponseBase<T> desc = default(ShopeeResponseBase<T>)!;
        try
        {
            string query = SendRequestParam(request);
            HttpRequestMessage request_message = new HttpRequestMessage(request.Method, $"{shopeeOption.SandBox_URL + request.EndPoint}?{query}");
            using (MultipartFormDataContent formData = MultipartFormDataContentExtension.CreateMultipartFormDataContent())
            {
                var byteArrayContent = new ByteArrayContent(request.ImageByte);
                formData.Add(byteArrayContent, "image", request.Image);
                request_message.Content = formData;
                logger.LogInformation($"请求方法{request.GetType().Name}", query, formData);
                var response = await HttpClient.SendAsync(request_message);
                string content = await response.Content.ReadAsStringAsync();
                try
                {
                    desc = JsonConvert.DeserializeObject<ShopeeResponseBase<T>>(content)!;
                }
                catch (Exception e)
                {
                    logger.LogError($"结果解析失败，解析类型目标类型：{request.GetType().Name},返回的数据为：{0}", content);
                    //return new ResponseShopee<T>(new ParseResultError(typeof(T).ToString(),e.Message));
                }
            }
        }
        catch (Exception e)
        {
            logger.LogWarning($"http request fail：{e.Message}");
            //return new ResponseShopee<T>(new NetworkRequestError($"{e.Message}"));
        }
        return desc;
    }


    private string SendRequestParam(RequestShopee request)
    {
        string query;
        switch (request.CommonParamType)
        {
            case EShopeeCommonParamType.COMMON:
                query = GetCommonParams(request).ToQueryString();
                break;
            case EShopeeCommonParamType.SHOP:
                query = GetShopParams(request).ToQueryString();
                break;
            case EShopeeCommonParamType.MERCHANT:
                query = GetMerchantParams(request).ToQueryString();
                break;
            default:
                //公共参数传递异常
                throw new ArgumentNullException(request.GetType().Name + ":CommonParams Is Empty");
                //break;
        }
        return query;
    }  
}
