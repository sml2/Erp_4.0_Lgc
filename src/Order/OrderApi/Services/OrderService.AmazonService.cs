
using Microsoft.Extensions.Logging;
using OrderSDK.Modles;
using OrderSDK.Modles.Amazon;
using OrderSDK.Modles.Amazon.CatalogItems;
using OrderSDK.Modles.Amazon.Enums;
using OrderSDK.Modles.Amazon.Finances;
using OrderSDK.Modles.Amazon.ListingsItems;
using OrderSDK.Modles.Amazon.Order;
using static OrderSDK.Modles.Amazon.Order.Order;
using OrderSDK.Modles.Amazon.ProductFees;
using OrderSDK.Modles.Amazon.ProductPricing;
using OrderSDK.Modles.Amazon.Reports;
using OrderSDK.Modles.Amazon.ProductTypes;
using OrderSDK.Modles.Amazon.Sellers;
using OrderSDK.Modles.Amazon.Utils;
using OrderSDK.Modles.CommonType;
using System.Globalization;
using System.Text;
using System.Xml.Serialization;
using static OrderSDK.Modles.Amazon.ListingsItems.ListingsItemPutRequest;
using ListingsItem = OrderSDK.Modles.Amazon.ListingsItems.Item;
using System.IO.Compression;
using System.Net;
using OrderSDK.Modles.Amazon.Feeds;
using Newtonsoft.Json.Linq;
using OrderSDK.Modles.Amazon.Feeds.ConstructFeed.Messages;
using Newtonsoft.Json;
using Newtonsoft.Json.Schema;
using Common;
using OrderSDK.Modles.Amazon.Authorization;
using OrderSDK.Services.Amazon;
using orderMoney = OrderSDK.Modles.Amazon.Order.Money;

using conFeed = OrderSDK.Modles.Amazon.Feeds.ConstructFeed.Messages;
using OrderSDK.Exceptions;
using MediatR;
using OrderSDK.Modles.Amazon.vm;
using Item = OrderSDK.Modles.Amazon.CatalogItems.Item;
using ERP.Enums.Rule.Permission;
using Aop.Api.Domain;

namespace OrderSDK.Services;

public class AmazonService : IAmazon
{
    private readonly AmazonClient client;

    public AmazonCredential amazonCredential;

    public GetTypes SupportedGetType => GetTypes.Single;
    public int MaxGet { get => 10; }

    public int MaxGets { get => 50; }

    private readonly ILogger<AmazonService> logger;

    public AmazonService(AmazonClient amazonSpClient, ILogger<AmazonService> _logger)
    {
        //确保日本的shift-jis可用
        Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        client = amazonSpClient;
        logger = _logger;
        //类库中如何开启生命周期 ？ ServiceCollection扩展方法，继续注入
    }


    #region 店铺基本操作

    /// <summary>
    /// 获取授权链接
    /// </summary>
    /// <param name="store"></param>
    /// <param name="state"></param>
    /// <returns></returns>
    public string GetAuthURL(string orgUrl, string ApplicationId, string RedirectUri, string state)
    {
        // return $"{orgUrl}/apps/authorize/consent?application_id={ApplicationId}&version=beta&state={state}&redirect_uri={RedirectUri}";
        // return $"{orgUrl}/apps/authorize/consent?application_id={ApplicationId}&version=beta&state={state}&redirect_uri={RedirectUri}";
        
        return $"{orgUrl}/apps/authorize/consent?application_id={ApplicationId}&state={state}&version=beta";
    }

    /// <summary>
    /// 验证店铺
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="store"></param>
    /// <param name="code"></param>
    /// <returns></returns>
    public async Task<TokenResponse> VerifyStore(StoreRegion store, string code)
    {
        logger.LogInformation($"$VerifyStore-{store.ID}-{store.Name}");
        var res= await client.GetAccessTokenFromCodeAsync(store.amazonCredential.ClientId, store.amazonCredential.ClientSecret, code, store.amazonCredential.RedirectUri, "authorization_code");
        logger.LogInformation($"$VerifyStore{store.ID}-{store.Name}：{JsonConvert.SerializeObject(res)}");
        return res;
    }

    /// <summary>
    /// 刷新Token
    /// </summary>
    /// <param name="store"></param>
    /// <returns></returns>
    public async Task<TokenResponse?> RefreshNormalAccessToken(StoreRegion store)
    {
        RequestAmazonSP request = new();
        return await client.RefreshNormalTokenAsync(request, store);
    }

    /// <summary>
    /// 核验市场
    /// </summary>
    /// <param name="store"></param>
    /// <returns></returns>
    public async Task<(string, MarketplaceParticipationList?)> GetMarketIds(StoreRegion store)
    {
        GetMarketplaceParticipationsRequest request = new();
        request.QueryRequest = true;
        var res = await client.ExecuteRequestAsync<GetMarketplaceParticipationsResponse>(request, store);
        if (res != null)
        {
            if (res.Errors is null || res.Errors.Count <= 0)
            {
                return ("", res.Payload);
            }
            else
            {
                return (res.Errors.ToErrorMsg(), null);
            }
        }
        return ("", null);
    }

    #endregion





    #region 订单

    /// <summary>
    /// 拉取订单列表
    /// </summary>
    /// <param name="listParameter"></param>
    /// <returns></returns>
    public async Task<(string, OrderLists)> ListImp(ListParameter listParameter)
    {
        logger.LogInformation($"list order,store is {JsonConvert.SerializeObject(listParameter.store)}");

        GetOrdersRequest request = new GetOrdersRequest();
        request.LastUpdatedAfter = listParameter.Start.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ", DateTimeFormatInfo.InvariantInfo);
        request.LastUpdatedBefore = listParameter.End.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ", DateTimeFormatInfo.InvariantInfo);
        request.OrderStatuses = new List<OrderStatusEnum>()
        {   OrderStatusEnum.Unshipped,
            OrderStatusEnum.PartiallyShipped,
            OrderStatusEnum.Shipped,
            OrderStatusEnum.Unfulfillable,
            OrderStatusEnum.Canceled
        };
        request.MarketplaceIds = listParameter.store.MarketPlaces;
        request.QueryRequest = true;
        request.IsNeedRestrictedDataToken = false;
        if (listParameter.NextToken != "")
        {
            request.NextToken = listParameter.NextToken;
        }
        var res =await client.ExecuteRequestAsync<GetOrdersResponse>(request, listParameter.store);
        if (res != null)
        {
            if (res.Errors is null || res.Errors.Count <= 0)
            {
                return ("", new(res.Payload.NextToken,res.Payload.Orders));
            }
            else
            {
                return (res.Errors.ToErrorMsg(), null);
            }
        }
        return ("return null", null);
    }


    /// <summary>
    /// 获取订单的基本信息
    /// </summary>
    /// <param name="store"></param>
    /// <param name="AmazonOrderId"></param>
    /// <returns></returns>
    public async Task<(string, Order?)> GetOrder(StoreRegion store, string OrderNo)
    {
        GetOrderRequest request = new GetOrderRequest();
        request.orderId = OrderNo;
        request.QueryRequest = true;
        var res = await client.ExecuteRequestAsync<GetOrderResponse>(request, store);
        if (res != null)
        {
            if (res.Errors is null || res.Errors.Count <= 0)
            {
                return ("", res.Payload);
            }
            else
            {
                return (res.Errors.ToErrorMsg(), null);
            }
        }
        return ("return null", null);
    }



    /// <summary>
    /// 获取订单中产品采购信息  orderItem
    /// </summary>
    /// <param name="AmazonOrderId"></param>
    /// <param name="store"></param>
    /// <returns></returns>
    public async Task<OrderItemListResponse> GetOrderItem(StoreRegion store, string OrderNo)
    {
        bool HasNext = false;
        string nextToken = "";
        string? msg = "";
        GetOrderItemsRequest request = new GetOrderItemsRequest();
        request.orderId = OrderNo;
        request.QueryRequest = true;
        var orderItemList = new OrderItemList();
        var response = await client.ExecuteRequestAsync<GetOrderItemsResponse>(request, store);
        if (response.IsNotNull())
        {
            if (response!.Payload is not null)
            {
                orderItemList.AddRange(response.Payload.OrderItems);
                nextToken = response.Payload.NextToken;
                while (!string.IsNullOrEmpty(nextToken))
                {
                    var orderItemPayload = GetOrderItemsNextToken(request, store, nextToken);
                    if (orderItemPayload.IsNotNull())
                    {
                        if (orderItemPayload!.Payload is not null)
                        {
                            orderItemList.AddRange(orderItemPayload.Payload.OrderItems);
                            nextToken = orderItemPayload.Payload.NextToken;
                        }
                        else if (response.Errors.Count > 0)
                        {
                            msg = response.Errors.ToErrorMsg();
                            return new OrderItemListResponse(HasNext, nextToken, orderItemList, msg);
                        }
                    }
                    else
                    {
                        return new OrderItemListResponse(HasNext, nextToken, orderItemList, msg);
                    }
                }
            }
            else if (response.Errors.Count > 0)
            {
                msg = response.Errors.ToErrorMsg();
            }
            return new OrderItemListResponse(HasNext, nextToken, orderItemList, msg);
        }
        return new OrderItemListResponse();
    }


    /// <summary>
    /// orderItem Token
    /// </summary>
    /// <param name="request"></param>
    /// <param name="store"></param>
    /// <param name="nextToken"></param>
    /// <returns></returns>
    public GetOrderItemsResponse? GetOrderItemsNextToken(GetOrderItemsRequest request, StoreRegion store, string nextToken)
    {
        request.NextToken = nextToken;
        return client.ExecuteRequestAsync<GetOrderItemsResponse>(request, store).Result;
    }


    /// <summary>
    /// 获取订单预估费用信息
    /// </summary>
    /// <param name="store"></param>
    /// <param name="marketPlaceId"></param>
    /// <param name="model"></param>
    /// <returns></returns>
    public async Task<(string, FeesEstimateResult?)> GetFeesEstimate(StoreRegion store, string marketPlaceId, Modles.Amazon.Order.OrderItem item)
    {
        GetMyFeesEstimateAsinRequest request = new GetMyFeesEstimateAsinRequest();
        FeesEstimateRequest r1 = new();
        r1.MarketplaceId = marketPlaceId;
        r1.IsAmazonFulfilled = false;
        r1.Identifier = item.ASIN;
        request.Asin = item.ASIN;
        r1.PriceToEstimateFees = new PriceToEstimateFees();
        string currencyCode = "";
        if (item.ItemPrice.IsNotNull())
        {
            r1.PriceToEstimateFees.ListingPrice = new orderMoney(item.ItemPrice.CurrencyCode, item.ItemPrice.Amount);
            currencyCode = currencyCode == "" ? item.ItemPrice.CurrencyCode : currencyCode;
        }
        if (item.ShippingPrice.IsNotNull())
        {
            r1.PriceToEstimateFees.Shipping = new orderMoney(item.ShippingPrice.CurrencyCode, item.ShippingPrice.Amount);
            currencyCode = currencyCode == "" ? item.ItemPrice.CurrencyCode : currencyCode;
        }
        //r1.PriceToEstimateFees.Points = new spOrder.ProductFees.Points(0, new OMoneyType(currencyCode, "0"));        
        request.BodyRequest = true;
        request.FeesEstimateRequest = r1;

        if (r1.PriceToEstimateFees.ListingPrice == null && item.ShippingPrice == null)
        {
            return ("price is null,maybe product already take off", null);
        }

        var res = await client.ExecuteRequestAsync<GetMyFeesEstimateResponse>(request, store);
        if (res != null)
        {
            if (res.Errors is null || res.Errors.Count <= 0)
            {
                return ("", res.Payload.FeesEstimateResult);
            }
            else
            {
                return (res.Errors.ToErrorMsg(), null);
            }
        }
        return ("return null", null);
    }



    /// <summary>
    /// 获取订单地址
    /// </summary>
    /// <param name="AmazonOrderId"></param>
    /// <param name="store"></param>
    /// <returns></returns>
    public async Task<(string, OrderAddress?)> GetAddress(StoreRegion store, string orderNo)
    {
        GetOrderAddressRequest request = new GetOrderAddressRequest();
        request.orderId = orderNo;
        request.QueryRequest = true;
        var res = await client.ExecuteRequestAsync<GetOrderAddressResponse>(request, store);
        if (res != null)
        {
            if (res.Errors is null || res.Errors.Count <= 0)
            {
                return ("", res.Payload);
            }
            else
            {
                return (res.Errors.ToErrorMsg(), null);
            }
        }
        return ("return null", null);
    }

    /// <summary> 
    /// 获取订单购买者信息
    /// </summary>
    /// <param name="AmazonOrderId"></param>
    /// <param name="store"></param>
    /// <returns></returns>
    public async Task<(string, OrderBuyerInfo?)> GetOrderBuyerInfo(StoreRegion store, string orderNo)
    {
        GetOrderBuyerInfoRequest request = new();
        request.orderId = orderNo;
        request.QueryRequest = true;
        var res = await client.ExecuteRequestAsync<GetOrderBuyerInfoResponse>(request, store);
        if (res != null)
        {
            if (res.Errors is null || res.Errors.Count <= 0)
            {
                return ("", res.Payload);
            }
            else
            {
                return (res.Errors.ToErrorMsg(), null);
            }
        }
        return ("return null", null);
    }
    
    public async Task<(string, OrderItemsBuyerInfoList Payload)> GetOrderItemsBuyerInfo(StoreRegion store,
        string orderNo)
    {
        GetOrderItemsBuyerInfoRequest request = new();
        request.orderId = orderNo;
        request.QueryRequest = true;
        var res = await client.ExecuteRequestAsync<GetOrderItemsBuyerInfoResponse>(request, store);
        if (res != null)
        {
            if (res.Errors is null || res.Errors.Count <= 0)
            {
                return ("", res.Payload);
            }
            else
            {
                return (res.Errors.ToErrorMsg(), null);
            }
        }
        return ("return null", null);
    }


    /// <summary>
    /// 获取订单费用信息
    /// </summary>
    /// <param name="AmazonOrderId"></param>
    /// <param name="store"></param>
    /// <returns></returns>
    public async Task<(string, List<FinancialEvents>?)> ListFinancialEventsByOrderId(StoreRegion store, string orderNo)
    {
        List<FinancialEvents> financialEventLits = new();
        ParameterListFinancialEvents request = new ParameterListFinancialEvents();
        request.orderId = orderNo;
        request.QueryRequest = true;
        var res = await client.ExecuteRequestAsync<ListFinancialEventsResponse>(request, store);
        if (res != null)
        {
            if (res.Errors is null || res.Errors.Count <= 0)
            {
                financialEventLits.Add(res.Payload.FinancialEvents);
                request.NextToken = res.Payload.NextToken;
                while (!string.IsNullOrWhiteSpace(request.NextToken))
                {
                    var r = await ListFinancialEventsByOrderIdNext(store, request);
                    if (r.IsNotNull())
                    {
                        if (r.Payload is not null)
                        {
                            financialEventLits.Add(r.Payload.FinancialEvents);
                            request.NextToken = r.Payload.NextToken;
                        }
                        else if (r.Errors.Count > 0)
                        {
                            return (r.Errors.ToErrorMsg(), null);
                        }
                    }
                    else
                    {
                        return ("", null);
                    }
                }
                return ("", financialEventLits);
            }
            else
            {
                return (res.Errors.ToErrorMsg(), null);
            }
        }
        return ("return null", null);
    }

    public Task<ListFinancialEventsResponse?> ListFinancialEventsByOrderIdNext(StoreRegion store, ParameterListFinancialEvents request)
    {
        return client.ExecuteRequestAsync<ListFinancialEventsResponse>(request, store);
    }    

    #endregion







    #region 产品信息


    /// <summary>
    /// 根据asin获取产品属性
    /// </summary>
    /// <param name="ASIN"></param>
    /// <param name="marketPlaceId"></param>
    /// <param name="store"></param>
    /// <returns></returns>
    public async Task<(string, Item?)> GetCatalogItem(string ASIN, string marketPlaceId, StoreRegion store)
    {
        GetCatalogItemRequest request = new GetCatalogItemRequest();
        request.asin = ASIN;
        request.MarketplaceId = marketPlaceId;
        request.QueryRequest = true;
        var res = await client.ExecuteRequestAsync<GetCatalogItemResponse>(request, store);
        if (res != null)
        {
            if (res.Errors != null && res.Errors.Count > 0)
            {
                return (res.Errors.ToErrorMsg(), null);
            }
            else
            {
                return ("", res.Payload);
            }
        }
        return ("return null", null);
    }



    /// <summary>
    /// 根据Sku，查询产品信息
    /// </summary>
    /// <param name="marketPlaceId"></param>
    /// <param name="store"></param>
    /// <param name="sku"></param>
    /// <returns></returns>
    public async Task<ListingsItem?> getListingsItem(StoreRegion store, string marketPlaceId, string sku)
    {
        var sellerID = store.amazonToken.SellerID;
        ListingsItemRequest request = new ListingsItemRequest();
        request.QueryRequest = true;
        request.sellerId = sellerID;
        request.marketplaceIds = new List<string>() { marketPlaceId };
        request.sku = sku;
        request.includedData = new List<ListingsIncludedData>() { ListingsIncludedData.Summaries, ListingsIncludedData.Attributes, ListingsIncludedData.Issues, ListingsIncludedData.Offers, ListingsIncludedData.FulfillmentAvailability, ListingsIncludedData.Procurement };
        return await client.ExecuteRequestAsync<ListingsItem>(request, store);
    }



    /// <summary>
    /// 获取产品价格
    /// </summary>
    /// <param name="marketPlaceId"></param>
    /// <param name="store"></param>
    /// <param name="asin">Max count : 20</param>
    /// <returns></returns>
    public async Task<(string, PriceList?)> GetPricing(StoreRegion store, string marketPlaceId, List<string> ids, IdType type = IdType.Asin)
    {
        GetPricingRequest request = new GetPricingRequest();
        request.QueryRequest = true;
        request.MarketplaceId = marketPlaceId;
        if (ids.Count < 0 || ids.Count > 20)
        {
            return ("", null);
        }
        if (type == IdType.Asin)
        {
            request.Asins = ids;
        }
        else
        {
            request.Skus = ids;
        }
        var res = await client.ExecuteRequestAsync<GetPricingResponse>(request, store);
        if (res != null)
        {
            if (res.Errors != null && res.Errors.Count > 0)
            {
                return (res.Errors.ToErrorMsg(), null);
            }
            else
            {
                return ("", res.Payload);
            }
        }
        return ("return null", null);
    }


    /// <summary>
    /// 获取产品信息(包含的信息更多)
    /// </summary>
    /// <param name="marketPlaceId">目前仅支持一个marketplace</param>
    /// <param name="store"></param>
    /// <param name="ids">一次请求最多20个</param>
    /// <returns></returns>
    public async Task<List<ItemRes>> SearchCatalogItems(StoreRegion store, string marketPlaceId, List<string> ids, IdentifiersType type = IdentifiersType.ASIN, bool vendor = false)
    {
        List<ItemRes> items = new();
        searchCatalogItemsRequest request = new searchCatalogItemsRequest();
        request.identifiers = ids;
        request.identifiersType = type;
        request.QueryRequest = true;
        request.marketplaceIds = new List<string>() { marketPlaceId };
        request.includedData = new List<enumincludedData>() { enumincludedData.identifiers, enumincludedData.relationships, enumincludedData.images, enumincludedData.attributes, enumincludedData.productTypes, enumincludedData.summaries, enumincludedData.salesRanks };
        if (vendor)
        {
            request.includedData.Add(enumincludedData.vendorDetails);
        }
        var response = client.ExecuteRequestAsync<ItemSearchResults>(request, store).Result;
        if (response != null)
        {
            items.AddRange(response.items);
            string? nextToken = response.pagination == null ? "" : response.pagination.nextToken;
            while (!string.IsNullOrEmpty(nextToken))
            {
                var result = await SearchCatalogItemsNextToken(request, store, nextToken);
                if (result != null)
                {
                    items.AddRange(result.items);
                    nextToken = result.pagination == null ? "" : result.pagination.nextToken;
                }
            }
        }
        return items;
    }


    /// <summary>
    /// 市场目前仅能输入一个
    /// asin最多20个
    /// </summary>
    /// <param name="marketPlaceId"></param>
    /// <param name="store"></param>
    /// <param name="asin"></param>
    /// <returns></returns>
    public Task<ItemSearchResults?> SearchCatalogItemsNextToken(searchCatalogItemsRequest request, StoreRegion store, string nextToken)
    {
        request.pageToken = nextToken;
        return client.ExecuteRequestAsync<ItemSearchResults>(request, store);
    }





    /// <summary>
    /// 获取指定店铺指定市场下的所有产品
    /// 支持多个市场id
    /// </summary>
    /// <param name="store"></param>
    /// <returns></returns>
    public async Task<ReportDocument?> GetAllAsin(StoreRegion store)
    {       
        var marketIds = store.MarketPlaces;
        var report = await createReport(marketIds, store);
        if (report is not null && report.ReportId.IsNotNull())
        {
            var ReportDocumentId = getReport(report.ReportId, store);
            if (ReportDocumentId is not null)
            {
               return await getReportDocument(ReportDocumentId, store);                
            }
        }
        return null;
    } 

    #endregion



    #region 获取报表信息
    /// <summary>
    /// 市场支持多个
    /// </summary>
    /// <param name="marketPlaceIds"></param>
    /// <param name="store"></param>
    /// <returns></returns>
    public Task<CreateReportResult?> createReport(List<string> marketPlaceIds, StoreRegion store)
    {
        createReportRequest request = new createReportRequest();
        request.BodyRequest = true;
        request.reportType = ReportTypes.GET_MERCHANT_LISTINGS_ALL_DATA;
        request.marketplaceIds = marketPlaceIds;
        var dic = new Dictionary<string, string>();
        request.reportOptions = new ReportOptions();
        request.reportOptions.Add("RootNodesOnly", "false");
        //RootNodesOnly true:报告仅包含来自使用 MarketplaceId 指定的市场（或来自卖家的默认市场，如果未指定 MarketplaceId）的根节点
        //RootNodesOnly false:如果为 false，或者如果 ReportOptions 参数中不包含 RootNodesOnly，则报告的内容取决于 BrowseNodeId 的值。:
        return client.ExecuteRequestAsync<CreateReportResult>(request, store);
    }



    public Task<CreateReportResult?> createBrowseTreeReport(List<string> marketPlaceIds, StoreRegion store, Dictionary<string, string>? keys = null)
    {
        createReportRequest request = new createReportRequest();
        request.BodyRequest = true;
        request.reportType = ReportTypes.GET_XML_BROWSE_TREE_DATA;
        request.marketplaceIds = marketPlaceIds;
        var dic = new Dictionary<string, string>();
        if (keys != null)
        {
            request.reportOptions = new ReportOptions();
            keys.ForEach(k => request.reportOptions.Add(k.Key, k.Value));
            //request.reportOptions.Add("BrowseNodeId", "157097011");
        }
        //RootNodesOnly true:报告仅包含来自使用 MarketplaceId 指定的市场（或来自卖家的默认市场，如果未指定 MarketplaceId）的根节点
        //RootNodesOnly false:如果为 false，或者如果 ReportOptions 参数中不包含 RootNodesOnly，则报告的内容取决于 BrowseNodeId 的值。:
        return client.ExecuteRequestAsync<CreateReportResult>(request, store);
    }


    public string getReport(string reportId, StoreRegion store)
    {
        getReportRequest request = new getReportRequest();
        request.QueryRequest = true;
        request.reportId = reportId;
        var report = client.ExecuteRequestAsync<Report>(request, store).Result;
        if (report.IsNull())
        {
            Console.Write("Getreport error");
            return string.Empty;
        }
        else if (report!.ProcessingStatus == Report.ProcessingStatusEnum.INQUEUE || report.ProcessingStatus == Report.ProcessingStatusEnum.INPROGRESS)
        {
            Thread.Sleep(500);
            //再次请求
            return getReport(report.ReportId, store);
        }
        else if (report.ProcessingStatus == Report.ProcessingStatusEnum.FATAL)
        {
            Console.Write("Report Fatal");
            return string.Empty;
        }
        else if (report.ProcessingStatus == Report.ProcessingStatusEnum.CANCELLED)
        {
            Console.Write("Report Cancelled");
            return string.Empty;
        }
        else
        {
            return report.ReportDocumentId;
        }
    }

    public Task<ReportDocument?> getReportDocument(string reportDocumentId, StoreRegion store)
    {
        GetReportDocumentRequest request = new GetReportDocumentRequest();
        request.QueryRequest = true;
        request.reportDocumentId = reportDocumentId;
        return client.ExecuteRequestAsync<ReportDocument>(request, store);
    }

    #endregion




    /// <summary>
    /// 亚马逊目录
    /// </summary>
    /// <param name="store"></param>
    /// <returns></returns>
    public async Task<CategoiesData?> GetReportBowerID(StoreRegion store,string marketPlace)
    {
        Dictionary<string, string> result = new Dictionary<string, string>();
        var marketIds = new List<string>() { marketPlace };
        var report = await createBrowseTreeReport(marketIds, store);
        if (report is not null && report.ReportId.IsNotNull())
        {
            var ReportDocumentId = getReport(report.ReportId, store);
            if (ReportDocumentId is not null)
            {
                var report1 = await getReportDocument(ReportDocumentId, store);
                if (report1.IsNotNull())
                {
                    try
                    {
                        var Encoding = System.Text.Encoding.Default;
                        bool downLoad = false;
                        //解压gzip，读取stream流
                        //var compressedFileStream = HttpDownload(report1!.Url).Result;
                        bool isCompressionFile = false;
                        bool isEncryptedFile = report1!.EncryptionDetails != null;
                        if (report1.CompressionAlgorithm is ReportDocument.CompressionAlgorithmEnum.GZIP)
                            isCompressionFile = true;

                        var client = new System.Net.WebClient();
                        string fileName = Guid.NewGuid().ToString() + "-" + String.Join(',', marketIds);

                        if (isCompressionFile)
                        {
                            client.Headers[System.Net.HttpRequestHeader.AcceptEncoding] = "gzip";
                            fileName += ".gz";
                        }
                        else fileName += ".txt";

                        string tempFilePath = Path.Combine(Path.GetTempPath() + fileName);

                        if (isEncryptedFile)
                        {
                            //Later will check
                            byte[] rawData = client.DownloadData(report1.Url);
                            byte[] key = Convert.FromBase64String(report1.EncryptionDetails.Key);
                            byte[] iv = Convert.FromBase64String(report1.EncryptionDetails.InitializationVector);
                            var reportData = FileTransform.DecryptString(key, iv, rawData);
                            File.WriteAllText(tempFilePath, reportData);
                        }
                        else
                        {
                            var stream = client.OpenReadTaskAsync(new Uri(report1.Url)).Result;
                            using (Stream s = File.Create(tempFilePath))
                            {
                                stream?.CopyTo(s);
                            }
                        }
                        var path = isCompressionFile ? FileTransform.Decompress(tempFilePath) : tempFilePath;
                        CategoiesData Data = new CategoiesData();
                        var xml = File.ReadAllText(path);
                        var serializer = new XmlSerializer(typeof(CategoiesData));
                        try
                        {
                            using (StringReader reader = new StringReader(xml))
                            {
                                Data = (CategoiesData)serializer.Deserialize(reader);
                                //保存入库
                                return Data;
                            }
                        }
                        catch (Exception e)
                        {
                            logger.LogError($"GetReportTest decompress and read stream error:{e.Message}");
                            throw e;
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("GetReportTest decompress and read stream error");
                        logger.LogError($"GetReportTest decompress and read stream error:{e.Message}");
                        logger.LogError($"{e.StackTrace}");
                    }
                }
            }
        }
        return null;
    }


    #region  测试使用

    /// <summary>
    /// 本地测试用
    /// </summary>
    /// <param name="store"></param>
    public async void GetReportBowerIDTest(StoreRegion store,string marketPlace)
    {
        Dictionary<string, string> result = new Dictionary<string, string>();
        var marketIds = new List<string>() { marketPlace };
        var report = await createBrowseTreeReport(marketIds, store);
        if (report is not null && report.ReportId.IsNotNull())
        {
            var ReportDocumentId = getReport(report.ReportId, store);
            if (ReportDocumentId is not null)
            {
                var report1 = await getReportDocument(ReportDocumentId, store);
                if (report1.IsNotNull())
                {
                    try
                    {
                        var Encoding = System.Text.Encoding.Default;
                        bool downLoad = false;
                        //解压gzip，读取stream流
                        //var compressedFileStream = HttpDownload(report1!.Url).Result;

                        bool isCompressionFile = false;
                        bool isEncryptedFile = report1!.EncryptionDetails != null;

                        if (report1.CompressionAlgorithm is ReportDocument.CompressionAlgorithmEnum.GZIP)
                            isCompressionFile = true;

                        var client = new System.Net.WebClient();
                        string fileName = Guid.NewGuid().ToString();

                        if (isCompressionFile)
                        {
                            client.Headers[System.Net.HttpRequestHeader.AcceptEncoding] = "gzip";
                            fileName += ".gz";
                        }
                        else fileName += ".txt";

                        string tempFilePath = Path.Combine(Path.GetTempPath() + fileName);

                        if (isEncryptedFile)
                        {
                            //Later will check
                            byte[] rawData = client.DownloadData(report1.Url);
                            byte[] key = Convert.FromBase64String(report1.EncryptionDetails.Key);
                            byte[] iv = Convert.FromBase64String(report1.EncryptionDetails.InitializationVector);
                            var reportData = FileTransform.DecryptString(key, iv, rawData);
                            File.WriteAllText(tempFilePath, reportData);
                        }
                        else
                        {
                            var stream = client.OpenReadTaskAsync(new Uri(report1.Url)).Result;
                            using (Stream s = File.Create(tempFilePath))
                            {
                                stream?.CopyTo(s);
                            }
                        }
                        var path = isCompressionFile ? FileTransform.Decompress(tempFilePath) : tempFilePath;
                        CategoiesData Data = new CategoiesData();
                        var xml = File.ReadAllText(path);
                        var serializer = new XmlSerializer(typeof(CategoiesData));
                        try
                        {
                            using (StringReader reader = new StringReader(xml))
                            {
                                Data = (CategoiesData)serializer.Deserialize(reader);
                            }
                        }
                        catch (Exception e)
                        {
                            logger.LogError($"GetReportBowerIDTest error:{e.Message}");
                            throw e;
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("#GetReportBowerIDTest   error");
                        logger.LogError($"GetReportBowerIDTest error:{e.Message}");
                        logger.LogError($"{e.StackTrace}");
                    }
                }
            }
        }
    }


    /// <summary>
    /// 修改产品信息
    /// </summary>
    /// <param name="store"></param>
    /// <param name="attributeJson"></param>
    /// <param name="marketPlaces"></param>
    /// <returns></returns>
    public ListingsItemSubmissionResponse? PutListingsItemAsync(StoreRegion store, string attributeJson, List<string> marketPlaces)
    {
        var sellerID = store.amazonToken.SellerID;
        ParameterPutListingItem request = new ParameterPutListingItem();
        request.SellerId = sellerID;
        request.MarketplaceIds = marketPlaces;
        request.Sku = "78-676";
        request.IssueLocale = "en_CA";
        request.BodyRequest = true;
        request._ListingsItemPutRequest = new ListingsItemPutRequest();
        request._ListingsItemPutRequest.ProductType = "CALENDAR";
        request._ListingsItemPutRequest.Requirements = RequirementsEnum.LISTINGPRODUCTONLY;
        request._ListingsItemPutRequest.Attributes = attributeJson;
        request.Check();
        var response = client.ExecuteRequestAsync<ListingsItemSubmissionResponse>(request, store).Result;
        return response;
    }


    //public string SubmitProductTest(StoreRegion store, string attributesJson, string MarketPlaceId)
    //{
    //    try
    //    {
    //        //构造完整json结构
    //        ListingFeed<JObject> listingFeed = new();
    //        listingFeed._Header = new();
    //        listingFeed._Header.SellerId = store.GetSPAmazon()!.SellerID;
    //        listingFeed.Messages = new();
    //        var Messages = new MessageInfo<JObject>();
    //        Messages.MessageId = 1;
    //        Messages.Sku = "SKU001";
    //        Messages.ProductType = "WATCH";
    //        Messages.Attributes = null;// attributesJson;
    //        Messages.OperationType = OperationType.Update.GetDescription();
    //        Messages.Requirements = RequirementsEnum.LISTINGPRODUCTONLY.GetDescription();
    //        listingFeed.Messages.Add(Messages);

    //        var json = JsonConvert.SerializeObject(listingFeed);
    //        var feedID = SubmitFeed(json, FeedType.JSON_LISTINGS_FEED, store, new List<string>() { MarketPlaceId }, null, ContentType.JSON);
    //        return GetFeedDetailTest(feedID, store);
    //    }
    //    catch (Exception e)
    //    {
    //        logger.LogError($"SubmitProduct fail,storeid:{store.ID},msg:{e.Message}");
    //        return "";
    //    }
    //}


    public async Task<string> GetFeedDetailTest(string feedID, StoreRegion store)
    {
        //第四步：定期轮询 Amazon SQS 队列中的 FEED_PROCESSING_FINISHED 通知事件，该事件在提要处理为 CANCELLED、DONE 或 FATAL 时提供信息。
        var feedOutput = GetFeed(feedID, store);
        if (feedOutput is not null)
        {
            //第五步：调用 getFeedDocument 操作。 使用 feedDocumentId 参数传入上一步的 resultFeedDocumentId 值
            //Amazon 返回 feedDocumentId 值、用于下载 Feed 处理报告的 URL 和压缩算法。
            var outPutResponse = await GetFeedDocument(feedOutput, store);
            if (outPutResponse is not null)
            {
                //下载 Feed 处理报告
                var Report = GetFeedDocumentProcessingReport(outPutResponse).Result;
                if (Report.Item1 is not null)
                {
                    return "";
                }
                else
                {
                    return Report.Item2;
                }
            }
            else
            {
                logger.LogError($"GetFeedDocument fail,storeid:{store.ID}");
                return $"GetFeedDocument fail,storeid:{store.ID}";
            }
        }
        else
        {
            logger.LogError($"GetFeedDocument fail,storeid:{store.ID}");
            return $"GetFeedDocument fail,storeid:{store.ID}";
        }
    }

    #endregion




    #region submit
    /// <summary>
    /// 提交产品
    /// </summary>
    /// <param name="store"></param>
    /// <param name="dataJson"></param>
    /// <param name="MarketPlaceId"></param>
    /// <returns></returns>
    public async Task<(FeedResult?, string)> SubmitProducts(StoreRegion store, string dataJson, string MarketPlaceId)
    {
        string m = "";
        try
        {
            var feedID = await SubmitFeed(dataJson, FeedType.JSON_LISTINGS_FEED, store, new List<string>() { MarketPlaceId }, null, ContentType.JSON);

            if (!string.IsNullOrWhiteSpace(feedID))
            {
                return await GetFeedDetail(feedID, store);
            }
            else
            {
                m = $"SubmitProducts SubmitFeed fail,feedID is null";
                logger.LogError(m);
                return (null, m);
            }
        }
        catch (Exception e)
        {
            m = $"SubmitProducts fail,storeid:{store.ID},msg:{e.Message}";
            logger.LogError(m);
            return (null, m);
        }
    }






    public async Task<string> DeliveryGoods(StoreRegion store, List<DeliverProduct> DP, string MarketPlaceId)
    {
        try
        {
            var sellerID = store.amazonToken.SellerID;
            var createDocument = new ConstructFeedService(sellerID, "1.02");
            var items = new List<conFeed.Item>();
            foreach (var p in DP)
            {
                if (p.HasAmazonOrderItemCode)
                {
                    var item = new conFeed.Item();
                    item.AmazonOrderItemCode = p.AmazonOrderItemCode;
                    item.Quantity = p.Quantity;
                    items.Add(item);
                }
            }
            var list = new List<OrderFulfillmentMessage>();
            list.Add(new OrderFulfillmentMessage()
            {
                AmazonOrderID = DP.Last().AmazonOrderID,
                FulfillmentDate = DP.Last().FulfillmentDate,
                FulfillmentData = new FulfillmentData()
                {
                    //CarrierCode=DP.Last().AmazonOrderItemCode,
                    CarrierName = DP.Last().CarrierName ?? "Other",
                    ShippingMethod = DP.Last().ShippingMethod.IsNullOrWhiteSpace() ? "Standard" : DP.Last().ShippingMethod,
                    ShipperTrackingNumber = DP.Last().ShipperTrackingNumber,
                },
                Item = items.Count == 0 ? null! : items
            });
            createDocument.AddOrderFulfillmentMessage(list);
            var xml = createDocument.GetXML();

            var feedID = await SubmitFeed(xml, FeedType.POST_ORDER_FULFILLMENT_DATA, store, new List<string>() { MarketPlaceId });
            if (string.IsNullOrWhiteSpace(feedID))
            {
                return "SubmitFeed fail,feedID is null";
            }
            
            var processingReport = await GetFeedDeliveryDetail(feedID, store);
            if (processingReport is not null)
            {
                logger.LogInformation("MessagesProcessed=" + processingReport.ProcessingSummary.MessagesProcessed);
                logger.LogInformation("MessagesSuccessful= " + processingReport.ProcessingSummary.MessagesSuccessful);
                logger.LogInformation("MessagesWithError=" + processingReport.ProcessingSummary.MessagesWithError);
                logger.LogInformation("MessagesWithWarning=" + processingReport.ProcessingSummary.MessagesWithWarning);
                logger.LogInformation("processingReport=" + JsonConvert.SerializeObject(processingReport));

                if (processingReport.Result != null && processingReport.Result.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (var itm in processingReport.Result)
                    {
                        if (itm.ResultCode == "Error")
                        {
                            //The XML you submitted is ill-formed at the Amazon Envelope XML level at (or near) line 10, column 42.
                            sb.Append(itm.ResultDescription);
                        }
                    }
                    return sb.ToString();
                }
                return "";
            }
            else
            {
                return $"DeliveryGoods Report fail,storeid:{store.ID}";
            }
        }
        catch (Exception e)
        {
            logger.LogError($"DeliveryGoods fail,storeid:{store.ID},msg:{e.Message}");
            return $"DeliveryGoods fail,storeid:{store.ID},msg:{e.Message}";
        }
    }


    public async Task<string> AddProductMessage(StoreRegion store, string MarketPlaceId)
    {
        try
        {
            var sellerID = store.amazonToken.SellerID;
            var createDocument = new ConstructFeedService(sellerID, "1.02");

            var list = new List<ProductMessage>();
            list.Add(new ProductMessage()
            {
                SKU = "123456-98"
            });
            createDocument.AddProductMessage(list, OperationType.Update);
            var xml = createDocument.GetXML();

            var feedID = await SubmitFeed(xml, FeedType.POST_PRODUCT_DATA, store, new List<string>() { MarketPlaceId });
            if (string.IsNullOrEmpty(feedID))
            {
                return $"AddProductMessage  feedID is null,storeid:{store.ID}";
            }
            var processingReport = await GetFeedDeliveryDetail(feedID!, store);
            if (processingReport is not null)
            {
                Console.WriteLine("MessagesProcessed=" + processingReport.ProcessingSummary.MessagesProcessed);
                Console.WriteLine("MessagesSuccessful= " + processingReport.ProcessingSummary.MessagesSuccessful);
                Console.WriteLine("MessagesWithError=" + processingReport.ProcessingSummary.MessagesWithError);
                Console.WriteLine("MessagesWithWarning=" + processingReport.ProcessingSummary.MessagesWithWarning);

                if (processingReport.Result != null && processingReport.Result.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (var itm in processingReport.Result)
                    {
                        if (itm.ResultCode == "Error")
                        {
                            //The XML you submitted is ill-formed at the Amazon Envelope XML level at (or near) line 10, column 42.
                            sb.Append(itm.ResultDescription);
                        }
                    }
                    return sb.ToString();
                }
                return "";
            }
            else
            {
                return $"AddProductMessage  fail,storeid:{store.ID}";
            }
        }
        catch (Exception e)
        {
            logger.LogError($"AddProductMessage fail,storeid:{store.ID},msg:{e.Message}");
            return $"AddProductMessage fail,storeid:{store.ID},msg:{e.Message}";
        }
    }

    public async Task<string> UploadExcelUrl(StoreRegion store, string MarketPlaceId, string fileExcelUrl)
    {
        try
        {
            var feedID = await SubmitFeed(fileExcelUrl, FeedType.POST_FLAT_FILE_LISTINGS_DATA, store, new List<string>() { MarketPlaceId }, null, ContentType.TXT, ContentFormate.File);
            if (string.IsNullOrEmpty(feedID))
            {
                return $"UploadExcelUrl feedid null,storeid:{store.ID}";
            }
            var processingReport = await GetFeedDetail(feedID, store);
            if (processingReport.Item1 is not null)
            {
                //Console.WriteLine("MessagesProcessed=" + processingReport.ProcessingSummary.MessagesProcessed);
                //Console.WriteLine("MessagesSuccessful= " + processingReport.ProcessingSummary.MessagesSuccessful);
                //Console.WriteLine("MessagesWithError=" + processingReport.ProcessingSummary.MessagesWithError);
                //Console.WriteLine("MessagesWithWarning=" + processingReport.ProcessingSummary.MessagesWithWarning);

                //if (processingReport.Result != null && processingReport.Result.Count > 0)
                //{
                //    StringBuilder sb = new StringBuilder();
                //    foreach (var itm in processingReport.Result)
                //    {
                //        if (itm.ResultCode == "Error")
                //        {
                //            //The XML you submitted is ill-formed at the Amazon Envelope XML level at (or near) line 10, column 42.
                //            sb.Append(itm.ResultDescription);
                //        }
                //    }
                //    return sb.ToString();
                //}
                return "";
            }
            else
            {
                return $"UploadExcelUrl  fail,storeid:{store.ID}";
            }
        }
        catch (Exception e)
        {
            logger.LogError($"UploadExcelUrl fail,storeid:{store.ID},msg:{e.Message}");
            return $"UploadExcelUrl fail,storeid:{store.ID},msg:{e.Message}";
        }
    }

   



    public async Task<string> GetFeedExcelResult(StoreRegion store,string feedID)
    {
        var processingReport = await GetFeedDetail(feedID, store);
        if (processingReport.Item1 is not null)
        {
            //Console.WriteLine("MessagesProcessed=" + processingReport.ProcessingSummary.MessagesProcessed);
            //Console.WriteLine("MessagesSuccessful= " + processingReport.ProcessingSummary.MessagesSuccessful);
            //Console.WriteLine("MessagesWithError=" + processingReport.ProcessingSummary.MessagesWithError);
            //Console.WriteLine("MessagesWithWarning=" + processingReport.ProcessingSummary.MessagesWithWarning);

            //if (processingReport.Result != null && processingReport.Result.Count > 0)
            //{
            //    StringBuilder sb = new StringBuilder();
            //    foreach (var itm in processingReport.Result)
            //    {
            //        if (itm.ResultCode == "Error")
            //        {
            //            //The XML you submitted is ill-formed at the Amazon Envelope XML level at (or near) line 10, column 42.
            //            sb.Append(itm.ResultDescription);
            //        }
            //    }
            //    return sb.ToString();
            //}
            return "";
        }
        else
        {
            return $"GetFeedResult  fail,storeid:{store.ID}";
        }
    }


    #region createfeed 

    /// <summary>
    /// 亚马逊feed接口
    /// </summary>
    /// <param name="XmlContentOrFilePath"></param>
    /// <param name="feedType"></param>
    /// <param name="store"></param>
    /// <param name="marketPlaceIds"></param>
    /// <param name="feedOptions"></param>
    /// <param name="contentType"></param>
    /// <param name="contentFormate"></param>
    /// <returns></returns>
    public async Task<string> SubmitFeed(string XmlContentOrFilePath, FeedType feedType, StoreRegion store, List<string> marketPlaceIds, FeedOptions feedOptions = null, ContentType contentType = ContentType.XML, ContentFormate contentFormate = ContentFormate.AutoDetect)
    {
        logger.LogInformation($"SubmitFeed method param is:{XmlContentOrFilePath}");
        try
        {
            //上传步骤
            //第一步：createFeedDocument ，返回feedDocumentId和URL     
            var feedCreate = await CreateFeedDocument(store, contentType);
            if (feedCreate.IsNotNull())
            {
                var url = feedCreate!.Url;
                //第二步：将您的提要文档内容上传到上一步中的 URL
                //Uploading encoded invoice file
                _ = PostFileDataAsync(feedCreate.Url, XmlContentOrFilePath, contentType, contentFormate).Result;

                //第三步：调用createFeed，传入步骤 1 中的 feedDocumentId 值。指定您希望应用该提要的市场以及任何相关的提要选项。
                CreateFeedSpecification createFeedSpecification = new CreateFeedSpecification()
                {
                    FeedType = feedType.ToString(),
                    InputFeedDocumentId = feedCreate.FeedDocumentId,
                    MarketplaceIds = marketPlaceIds,
                    FeedOptions = feedOptions,
                    BodyRequest = true,
                };
                var FeedId = await CreateFeed(createFeedSpecification, store);
                if (FeedId != null && !string.IsNullOrEmpty(FeedId.FeedId))
                {
                    return FeedId.FeedId;
                }
                logger.LogError($"SubmitFeed error:FeedId is null");
                return "";
            }
            logger.LogError($"SubmitFeed error:CreateFeedDocument is null");
        }
        catch (Exception e)
        {
            logger.LogError($"SubmitFeed Exception:{e.Message}");
            logger.LogError($"SubmitFeed Exception:{e.StackTrace}");
        }
        return "";
    }
   

    public Task<CreateFeedDocumentResult?> CreateFeedDocument(StoreRegion store, ContentType contentType)
    {
        var contxt = LinqHelper.GetEnumMemberValue(contentType);
        var createFeedDocumentSpecification = new CreateFeedDocumentSpecification(contxt);
        createFeedDocumentSpecification.BodyRequest = true;
        return client.ExecuteRequestAsync<CreateFeedDocumentResult>(createFeedDocumentSpecification, store);
    }


    private async Task<string> PostFileDataAsync(string destinationUrl, string contentOrFilePath, ContentType contentType = ContentType.XML, ContentFormate contentFormate = ContentFormate.AutoDetect)
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);

        byte[] bytes = null;
        if (contentFormate == ContentFormate.File)
        {
            bytes = System.IO.File.ReadAllBytes(contentOrFilePath);
        }
        else if (contentFormate == ContentFormate.Text)
        {
            bytes = System.Text.Encoding.UTF8.GetBytes(contentOrFilePath);
        }
        else if (contentFormate == ContentFormate.AutoDetect)
        {
            //if (System.IO.Path.IsPathRooted(contentOrFilePath))
            //{
            //    // The string looks like a file path, so try to read the file
            //    if (System.IO.File.Exists(contentOrFilePath))
            //    {
            //        bytes = System.IO.File.ReadAllBytes(contentOrFilePath);
            //    }
            //    else
            //    {
            //        // The file does not exist, so treat the string as content
            //        bytes = System.Text.Encoding.UTF8.GetBytes(contentOrFilePath);
            //    }
            //}
            //else if (Uri.IsWellFormedUriString(contentOrFilePath, UriKind.RelativeOrAbsolute))
            //{
            //    // The string looks like a URI, so try to parse it as a file URI
            //    var uri = new Uri(contentOrFilePath);
            //    if (uri.IsFile)
            //    {
            //        if (System.IO.File.Exists(uri.LocalPath))
            //        {
            //            bytes = System.IO.File.ReadAllBytes(uri.LocalPath);
            //        }
            //        else
            //        {
            //            // The file does not exist, so treat the string as content
            //            bytes = System.Text.Encoding.UTF8.GetBytes(contentOrFilePath);
            //        }
            //    }
            //    else
            //    {
            //        // The URI is not a file URI, so treat the string as content
            //        bytes = System.Text.Encoding.UTF8.GetBytes(contentOrFilePath);
            //    }
            //}
            //else
            //{
            //    // The string is not a file path or a URI, so treat it as content
            //    bytes = System.Text.Encoding.UTF8.GetBytes(contentOrFilePath);
            //}

            if (IsPathRooted(contentOrFilePath))
            {
                // The string looks like a file path, so try to read the file
                if (System.IO.File.Exists(contentOrFilePath))
                {
                    bytes = System.IO.File.ReadAllBytes(contentOrFilePath);
                }
                else
                    throw new AmazonException("Feed File not exist");
            }
            else
            {
                // The file does not exist, so treat the string as content
                bytes = System.Text.Encoding.UTF8.GetBytes(contentOrFilePath);
            }
        }

        request.ContentType = LinqHelper.GetEnumMemberValue(contentType);
        request.ContentLength = bytes.Length;
        request.Method = "PUT";
        using (Stream requestStream = request.GetRequestStream())
        {
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                string responseStr = await new StreamReader(responseStream).ReadToEndAsync();
                return responseStr;
            }
        }
        return "";
    }

    private bool IsPathRooted(string content)
    {
        if (string.IsNullOrEmpty(content))
            return false;

        if (content.Length > 255 || content.Contains("\n"))
            return false;

        return true;
    }

    public Task<CreateFeedResult?> CreateFeed(CreateFeedSpecification createFeedSpecification, StoreRegion store)
    {
        //第三步：调用createFeed，传入步骤 1 中的 feedDocumentId 值。指定您希望应用该提要的市场以及任何相关的提要选项。       
        return client.ExecuteRequestAsync<CreateFeedResult>(createFeedSpecification, store);
    }

    #endregion

    #region getFeed

   

    public async Task<(FeedResult?, string)> GetFeedDetail(string feedID, StoreRegion store)
    {
        string m = "";
        var feedOutput = GetFeed(feedID, store);
        if (feedOutput is not null)
        {
            var outPutResponse = await GetFeedDocument(feedOutput, store);
            if (outPutResponse is not null)
            {
                return await GetFeedDocumentProcessingReport(outPutResponse);
            }
            else
            {
                m = $"GetFeedDetail is null";
                logger.LogError(m);
            }
        }
        return (null, m);
    }

  

    public async Task<(FeedResult?, string)> GetFeedDocumentProcessingReport(FeedDocument feedDocument)
    {
        string m = "";
        string responseContent = "";
        try
        {
            var stream = await GetStreamFromUrlAsync(feedDocument.Url);
            if (feedDocument.CompressionAlgorithm.HasValue && (feedDocument.CompressionAlgorithm.Value == FeedDocument.CompressionAlgorithmEnum.GZIP))
            {
                stream = new System.IO.Compression.GZipStream(stream, System.IO.Compression.CompressionMode.Decompress);
            }
            var xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(FeedAmazonEnvelope));
            FeedAmazonEnvelope? response = null;
            try
            {
                StreamReader reader = new StreamReader(stream);
                responseContent = reader.ReadToEnd();
                var result = JsonConvert.DeserializeObject<FeedResult>(responseContent);
                return (result, "");
            }
            catch (Exception e)
            {
                m = $"GetFeedDocumentProcessingReport wrong on deserialize report stream,{responseContent}";
                logger.LogError(m);
                return (null, m);
            }
        }
        catch (Exception ex)
        {
            m = $"GetFeedDocumentProcessingReport Exception,{ex.Message}";
            logger.LogError(m);
            logger.LogError($"GetFeedDocumentProcessingReport Exception,{ex.StackTrace}");
        }
        return (null, m);
    }

    public Feed? GetFeed(string feedID, StoreRegion store)
    {
        GetFeedRequest request = new GetFeedRequest();
        request.feedId = feedID;
        request.QueryRequest = true;
        var feedOutput = client.ExecuteRequestAsync<Feed>(request, store).Result;
        if (feedOutput.IsNotNull())
        {
            if (feedOutput!.ProcessingStatus == Feed.ProcessingStatusEnum.DONE)
            {
                return feedOutput;
            }
            else if (feedOutput.ProcessingStatus == Feed.ProcessingStatusEnum.FATAL)
            {
                //出现致命错误
                Console.WriteLine("GetFeed fatal error");
                return null;
            }
            else if (feedOutput.ProcessingStatus == Feed.ProcessingStatusEnum.CANCELLED)
            {
                //被取消了
                Console.WriteLine("GetFeed CANCELLED");
                return null;
            }
            else
            {
                //在排队，或在处理中
                Thread.Sleep(10000);//等待返回结果
                return GetFeed(feedID, store);
            }
        }
        else
        {
            //在排队，或在处理中
            Thread.Sleep(10000);//等待返回结果
            return GetFeed(feedID, store);
        }
    }

  

    public Task<FeedDocument?> GetFeedDocument(Feed feedOutput, StoreRegion store)
    {
        GetFeedDocumentRequest requestdoc = new GetFeedDocumentRequest();
        requestdoc.feedDocumentId = feedOutput.ResultFeedDocumentId;
        requestdoc.QueryRequest = true;
        return client.ExecuteRequestAsync<FeedDocument>(requestdoc, store);
    }

    public async Task<ProcessingReportMessage?> GetFeedDeliveryDetail(string feedID, StoreRegion store)
    {
        string m = "";
        var feedOutput = GetFeed(feedID, store);
        if (feedOutput is not null)
        {
            var outPutResponse = await GetFeedDocument(feedOutput, store);
            if (outPutResponse is not null)
            {
                return await GetFeedDocumentProcessingDeliveryReport(outPutResponse);
            }
            else
            {
                m = $"GetFeedDetail is null";
                logger.LogError(m);
            }
        }
        return null;
    }

    public async Task<ProcessingReportMessage?> GetFeedDocumentProcessingDeliveryReport(FeedDocument feedDocument)
    {
        ProcessingReportMessage? processingReport = null;
        string responseContent;

        var stream = await GetStreamFromUrlAsync(feedDocument.Url);
        if (feedDocument.CompressionAlgorithm.HasValue && (feedDocument.CompressionAlgorithm.Value == FeedDocument.CompressionAlgorithmEnum.GZIP))
        {
            stream = new System.IO.Compression.GZipStream(stream, System.IO.Compression.CompressionMode.Decompress);
        }
        var xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(FeedAmazonEnvelope));
        FeedAmazonEnvelope? response = null;
        try
        {
            response = (FeedAmazonEnvelope)xmlSerializer.Deserialize(stream);
            if (response != null && response.Message != null)
            {
                processingReport = response.Message[0].ProcessingReport;
            }
        }
        catch (Exception e)
        {
            StreamReader reader = new StreamReader(stream);
            responseContent = reader.ReadToEnd();
            throw new AmazonProcessingReportDeserializeException($"GetFeedDocumentProcessingDeliveryReport,error:{e.Message}", responseContent, null);
        }
        return processingReport;
    }

    private static async Task<Stream> GetStreamFromUrlAsync(string url)
    {
        byte[] imageData;

        using (var wc = new System.Net.WebClient())
            imageData = await wc.DownloadDataTaskAsync(new Uri(url));

        return new MemoryStream(imageData);
    }

    #endregion


    #region feedExcel

    public async Task<(string, List<pushProductResult>)> UploadExcelBytes(StoreRegion store, string MarketPlaceId, byte[] bytes)
    {
        try
        {
            logger.LogInformation($"UploadExcelBytes start,storeid:{store.ID},{store.Name}");
            var feedID = await SubmitFileFeed(bytes, FeedType.POST_FLAT_FILE_LISTINGS_DATA, store, new List<string>() { MarketPlaceId }, null, ContentType.TXT, ContentFormate.File);
            if (string.IsNullOrWhiteSpace(feedID))
            {
                return ("FeedIDNull", null);
            }
            return await GetFeedExcelDetail(feedID, store, MarketPlaceId);
        }
        catch (Exception e)
        {
            logger.LogError($"UploadExcelUrl Exception,storeid:{store.ID},msg:{e.Message}");
            logger.LogError($"UploadExcelUrl Exception,msg:{e.StackTrace}");
            return ("ERROR", null);
        }
    }

    public async Task<string> SubmitFileFeed(byte[] bytes, FeedType feedType, StoreRegion store, List<string> marketPlaceIds, FeedOptions feedOptions = null, ContentType contentType = ContentType.XML, ContentFormate contentFormate = ContentFormate.AutoDetect)
    {
        int count = 0;
        try
        {
            //上传步骤
            //第一步：createFeedDocument ，返回feedDocumentId和URL     
            var feedCreate = await CreateFeedDocument(store, contentType);
            if (feedCreate.IsNotNull() && !string.IsNullOrWhiteSpace(feedCreate.Url))
            {
                var url = feedCreate!.Url;
                //第二步：将您的提要文档内容上传到上一步中的 URL
                //Uploading encoded invoice file
                _ = PostFileBytesAsync(feedCreate.Url, bytes, contentType, contentFormate).Result;

                //第三步：调用createFeed，传入步骤 1 中的 feedDocumentId 值。指定您希望应用该提要的市场以及任何相关的提要选项。
                CreateFeedSpecification createFeedSpecification = new CreateFeedSpecification()
                {
                    FeedType = feedType.ToString(),
                    InputFeedDocumentId = feedCreate.FeedDocumentId,
                    MarketplaceIds = marketPlaceIds,
                    FeedOptions = feedOptions,
                    BodyRequest = true,
                };
                while(true)
                {
                    var FeedId = await CreateFeed(createFeedSpecification, store);
                    if (FeedId != null && !string.IsNullOrEmpty(FeedId.FeedId))
                    {
                        return FeedId.FeedId;
                    }
                    else
                    {
                        count++;
                        if (count > 3)

                        {
                            return "";
                        }
                        //延时5分钟再试
                        const int delayTime = 5* 60 * 1000;//10分钟
                        Thread.Sleep(delayTime);//等待返回结果
                       
                    }
                }
               
            }
        }
        catch (Exception e)
        {
            logger.LogError($"SubmitFeed Exception:{e.Message}");
            logger.LogError($"SubmitFeed Exception:{e.StackTrace}");
        }
        return "";
    }



    private async Task<string> PostFileBytesAsync(string destinationUrl, byte[] bytes, ContentType contentType = ContentType.XML, ContentFormate contentFormate = ContentFormate.AutoDetect)
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
        request.ContentType = LinqHelper.GetEnumMemberValue(contentType);
        request.ContentLength = bytes.Length;
        request.Method = "PUT";
        using (Stream requestStream = request.GetRequestStream())
        {
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                string responseStr = await new StreamReader(responseStream).ReadToEndAsync();
                return responseStr;
            }
        }
        return "";
    }


    public async Task<(string, List<pushProductResult>)> GetFeedExcelDetail(string feedID, StoreRegion store,string MarketPlaceId)
    {
        logger.LogInformation($"GetFeedExcelDetail start,storeid:{store.ID},{store.Name}");
        string m = "";
        var feedResult = await GetFeedExcel(feedID, store);
                
        if (!string.IsNullOrWhiteSpace(feedResult.Item1))
        {
            return (feedResult.Item1,null);  //amazon处理有错误
        }
        else
        {
            var outPutResponse = await GetFeedDocument(feedResult.Item2!, store);
            if (outPutResponse is not null)
            {
                logger.LogInformation($"UploadExcelBytes GetFeedUploadBytesDocumentReport,storeid:{store.ID},{store.Name}");
                return await GetFeedUploadBytesDocumentReport(outPutResponse, MarketPlaceId);              
            }
            else
            {
                m = $"ReportERROR";
                return (m, null);  //amazon处理有错误
            }
        }               
    }

    public async Task<(string, Feed?)> GetFeedExcel(string feedID, StoreRegion store)
    {
        try
        {
            const int delayTime = 10 * 60 * 1000;//10分钟
            GetFeedRequest request = new GetFeedRequest();
            request.feedId = feedID;
            request.QueryRequest = true;
            var feedOutput = client.ExecuteRequestAsync<Feed>(request, store).Result;

            if (feedOutput.IsNotNull())
            {
                if (feedOutput!.ProcessingStatus == Feed.ProcessingStatusEnum.DONE)
                {
                    return ("", feedOutput);
                }
                else if (feedOutput.ProcessingStatus == Feed.ProcessingStatusEnum.FATAL)
                {
                    //出现致命错误               
                    return ("FATAL", null);
                }
                else if (feedOutput.ProcessingStatus == Feed.ProcessingStatusEnum.CANCELLED)
                {
                    //被取消了              
                    return ("CANCELLED", null);
                }
                else
                {
                    //在排队，或在处理中               
                    Thread.Sleep(delayTime);//等待返回结果
                    return await GetFeedExcel(feedID, store);
                }
            }
            else
            {
                //在排队，或在处理中
                Thread.Sleep(delayTime);//等待返回结果
                return await GetFeedExcel(feedID, store);
            }
        }
        catch (Exception e)
        {
            logger.LogError($"GetFeedExcel Exception:{e.Message}");
            logger.LogError($"GetFeedExcel Exception:{e.StackTrace}");
            return ("ERROR", null);
        }
    }


    public async Task<(string, List<pushProductResult>)> GetFeedUploadBytesDocumentReport(FeedDocument feedDocument,string MarketPlaceId)
    {        
        List<pushProductResult> results = new List<pushProductResult>();
        string responseContent = "";
        try
        {
            var stream = await GetStreamFromUrlAsync(feedDocument.Url);
            if (feedDocument.CompressionAlgorithm.HasValue && (feedDocument.CompressionAlgorithm.Value == FeedDocument.CompressionAlgorithmEnum.GZIP))
            {
                stream = new System.IO.Compression.GZipStream(stream, System.IO.Compression.CompressionMode.Decompress);
            }
            Encoding encoding = System.Text.Encoding.Default;           
            //墨西哥 ， 巴西 ， 西班牙 ， 法国 ，  荷兰 ， 德国 ， 意大利 ，  瑞典 ， 比利时
             if ("A1AM78C64UM0Y8".Equals(MarketPlaceId) || "A2Q3Y263D00KWC".Equals(MarketPlaceId) || "A1RKKUPIHCS9HS".Equals(MarketPlaceId) || "A13V1IB3VIYZZH".Equals(MarketPlaceId) || "A1805IZSGTT6HS".Equals(MarketPlaceId) ||
                "A1PA6795UKMFR9".Equals(MarketPlaceId) || "APJ6JRA9NG5V4".Equals(MarketPlaceId) || "A2NODRKZP88ZB9".Equals(MarketPlaceId)
               || "AMEN7PMS3EDWL".Equals(MarketPlaceId))
            {
                encoding = System.Text.Encoding.GetEncoding("ISO-8859-1");
            } //波兰
            else if ("A1C3SOZRARQ6R3".Equals(MarketPlaceId))
            {
                encoding = System.Text.Encoding.GetEncoding("ISO-8859-2");
            } //土耳其
            else if ("A33AVAJ2PDY3EV".Equals(MarketPlaceId))
            {
                encoding = System.Text.Encoding.GetEncoding("ISO-8859-9");
            }   //日本
            else if ("A1VC38T7YXB528".Equals(MarketPlaceId))
            {
                encoding = System.Text.Encoding.GetEncoding("Shift-JIS");
            }//埃及
            else if ("ARBP9OOSHTCHU".Equals(MarketPlaceId))
            {
                encoding = System.Text.Encoding.GetEncoding("ISO-8859-6");  //阿拉伯语
            }
             else
            {
                //加拿大A2EUQ1WTGCTBG2， 美国ATVPDKIKX0DER ， 沙特阿拉伯A17E79C6D8DWNP ， 阿联酋A2VIGQ35RCS4UG ， 印度A21TJRUUN4KGV ，  新加坡A19VAU5U5O7RUS ， 澳大利亚A39IBJ37TRP1C6 ， 英国A1F83G8C2ARO7P , 南非AE08WJ6YKNBMC            
                encoding = System.Text.Encoding.Default;
            }
            using (var Reader = new StreamReader(stream, encoding))
            {
                const char Spliter = '\t';
                var HeaderLine = Reader.ReadLine();
                HeaderLine = Reader.ReadLine();
                HeaderLine = Reader.ReadLine();
                HeaderLine = Reader.ReadLine();
                HeaderLine = Reader.ReadLine();
                if (HeaderLine is null)
                {
                    responseContent = Reader.ReadToEnd();
                    logger.LogError($"GetFeedUploadBytesDocumentReport report，HeaderLine is null");
                    throw new ArgumentNullException("# GetFeedUploadBytesDocumentReport HeaderLine is null");
                }
                else
                {
                    int ColNum = 0;
                    int? originalRecordNumberI = null, skuI = null, errorCodeI = null, errorTypeI = null, errorMessageI = null;

                    var hArry = HeaderLine.Split(Spliter);
                    originalRecordNumberI = 0;
                    skuI = 1;
                    errorCodeI = 2;                   
                    errorTypeI = 3;
                    errorMessageI = 4;
                    
                    if (originalRecordNumberI.HasValue)
                    {
                        string? Line;
                        while ((Line = Reader.ReadLine()) is not null)
                        {
                            var Cols = Line.Split(Spliter);                         
                            if (Cols.Count() != hArry.Count())
                            {
                                responseContent = Reader.ReadToEnd();
                                logger.LogError($"GetFeedUploadBytesDocumentReport report，CurColNum ！=ColNum");
                                throw new IndexOutOfRangeException("#GetFeedUploadBytesDocumentReport column number not equal");
                            }
                            else
                            {
                                var number = Cols[originalRecordNumberI.Value];
                                var Sku = Cols[skuI.Value];
                                var code = Cols[errorCodeI.Value];
                                var type = Cols[errorTypeI.Value];
                                var message = Cols[errorMessageI.Value];
                                pushProductResult result = new pushProductResult()
                                {
                                    originalRecordNumber = number,
                                    sku = Sku,
                                    errorCode = code,
                                    errorType = type,
                                    errorMessage = message,
                                };
                                results.Add(result);
                            }
                        }
                    }
                    else
                    {
                        responseContent = Reader.ReadToEnd();
                        logger.LogError($"GetFeedUploadBytesDocumentReport report:originalRecordNumberI not found");
                        throw new InvalidDataException("GetFeedUploadBytesDocumentReport originalRecordNumberI not found");
                    }
                }
            }
            return ("", results);
        }
        catch (Exception ex)
        {            

            logger.LogError($"GetFeedUploadBytesDocumentReport report:{responseContent}");
            logger.LogError($"GetFeedUploadBytesDocumentReport Exception,{ex.Message}");
            logger.LogError($"GetFeedUploadBytesDocumentReport Exception,{ex.StackTrace}");
        }
        return ("ReportERROR", null);
    }



    #endregion


    #endregion






    #region 获取产品类型的schema信息
    /// <summary>
    /// 搜索并返回具有可用定义的亚马逊产品类型列表
    /// marketplaceIds支持多个查询
    /// </summary>
    /// <param name="store"></param>   
    /// <returns></returns>
    public Task<ProductTypeList?> SearchDefinitionsProductTypes(StoreRegion store)
    {
        SearchDefinitionsProductTypesRequest request = new SearchDefinitionsProductTypesRequest();
        request.marketplaceIds = store.MarketPlaces;
        request.QueryRequest = true;
        return client.ExecuteRequestAsync<ProductTypeList>(request, store);
    }

    /// <summary>
    /// 检索 Amazon 产品类型定义
    /// </summary>
    /// <param name="store"></param>
    /// <param name="marketPlace"></param>
    /// <param name="productTypeName"></param>
    /// <returns></returns>
    public async Task<(JSchema?, ProductDesc)?> GetProductTypeModels(StoreRegion store, string marketPlace, string productTypeName)
    {
        try
        {
            var DefinitionsProductTypes = await GetDefinitionsProductTypes(store, marketPlace, productTypeName);
            if (DefinitionsProductTypes is not null && DefinitionsProductTypes.MetaSchema is not null)
            {
                //分析属性组
                var metaSchema = DefinitionsProductTypes.MetaSchema;
                var metaSchemaLink = (JToken)metaSchema.Link;
                var metaSchemaUrl = metaSchemaLink.First.First.ToString();
                var metaSchemaMethod = metaSchemaLink.Last.First.ToString();
                var schema = DefinitionsProductTypes.Schema;
                var SchemaLink = (JToken)schema.Link;
                var SchemaUrl = SchemaLink.First.First.ToString();
                var SchemaMethod = SchemaLink.Last.First.ToString();

                ProductDesc product = new ProductDesc();
                product.type = DefinitionsProductTypes.ProductType;
                product.version = DefinitionsProductTypes.ProductTypeVersion.Version;//可以依据这个来判断是否需要更新属性
                product.locale = DefinitionsProductTypes.Locale;

                var jsonSchema = GetProperties(metaSchemaUrl, SchemaUrl);
                return (jsonSchema, product);
            }
        }
        catch (Exception e)
        {
            logger.LogError($"GetProductTypeModels:{e.Message}");
            logger.LogError($"GetProductTypeModels:{e.StackTrace}");
        }
        return null;
    }



    /// <summary>
    /// 检索 Amazon 产品类型定义
    /// marketplaceIds目前仅支持一个查询
    /// productType:支持按逗号分割，不提供时，默认提供可用产品类型的完整列表
    /// </summary>
    /// <param name="store"></param>
    /// <param name="productType"></param>
    /// <returns></returns>
    public Task<ProductTypeDefinition?> GetDefinitionsProductTypes(StoreRegion store, string MarketPlace, string productType)
    {
        GetDefinitionsProductTypeRequest request = new GetDefinitionsProductTypeRequest();
        if (!string.IsNullOrEmpty(productType))
        {
            request.productType = productType;
        }
        request.marketplaceIds = new List<string>() { MarketPlace };// store.MarketPlaces;
        request.QueryRequest = true;
        return client.ExecuteRequestAsync<ProductTypeDefinition>(request, store);
    }



    public JSchema? GetProperties(string metaSchemaUrl, string SchemaUrl)
    {
        try
        {
            //使用模式的本地副本而不是从 Web 检索它们的模式解析器。
            // Schema resolver that uses local copies of schemas rather than retrieving them from the web.
            var resolver = new JSchemaPreloadedResolver();
            var metaSchemajson = GetJsonFormUrl(metaSchemaUrl);
            var Schemajson = GetJsonFormUrl(SchemaUrl);
            var payload = JObject.Parse(metaSchemajson);
            if (payload is not null)
            {
                //将元模式添加到解析器。
                //Add the meta-schema to the resolver.
                foreach (var v in payload)
                {
                    if ("$id".Equals(v.Key))
                    {
                        //亚马逊产品类型定义元架构的 $id。
                        // $id of the Amazon Product Type Definition Meta-Schema.
                        var schemaId = v.Value.ToString();
                        resolver.Add(new Uri(schemaId), metaSchemajson);
                        //使用解析器和关键字验证器配置读取器设置以在解析元模式实例时使用。
                        //Configure reader settings with resolver and keyword validators to use when parsing instances of the meta-schema.
                        var readerSettings = new JSchemaReaderSettings
                        {
                            Resolver = resolver,
                            Validators = new List<JsonValidator>
                            {
                                //new MaxUniqueItemsKeywordValidator(),
                                //new MaxUtf8ByteLengthKeywordValidator(),
                                //new MinUtf8ByteLengthKeywordValidator()
                            }
                        };
                        return JSchema.Parse(Schemajson, readerSettings);
                    }
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine($"GetProperties error,SchemaUrl:{SchemaUrl}");
        }
        return null;
    }


    public string GetJsonFormUrl(string url)
    {
        using (var client = new WebClient())
        {
            var stream = client.OpenReadTaskAsync(url).Result;

            using (var Reader = new StreamReader(stream))
            {
                return Reader.ReadToEnd();
            }
        }
    }


    public void UpdateDefault(bool isWeight, JSchema jSchema, string name)
    {
        if (jSchema.Items.Count > 0)
        {
            UpdateDefault(isWeight, jSchema.Items[0], "");
        }
        else if (jSchema.Items.Count <= 0 && jSchema.Properties.Count > 0)
        {
            foreach (var properity in jSchema.Properties.Keys)
            {
                var itemP = jSchema.Properties[properity];
                UpdateDefault(isWeight, itemP, properity);
            }
        }
        else if (jSchema.Items.Count <= 0 && jSchema.Properties.Count <= 0)
        {
            if (name == "marketplace_id" || name == "language_tag")
            {
                jSchema.Title = name;
                if (jSchema.AnyOf.Count > 1)
                {
                    jSchema.AnyOf.RemoveAt(0);
                }
            }
            else
            {
                if (jSchema.AnyOf.Count > 1)
                {
                    jSchema.AnyOf.RemoveAt(0);
                    var temp = jSchema.AnyOf[0];
                    if (temp.Enum is not null && temp.Enum.Count > 0)
                    {
                        if (temp.Default is null)
                        {
                            temp.Default = temp.Enum.First();
                            jSchema.Default = temp.Default;
                        }
                    }
                }

                if (jSchema.OneOf.Count > 1)
                {
                    jSchema.OneOf.RemoveAt(1);
                    var temp = jSchema.OneOf[0];
                    //if (temp.Enum is not null && temp.Enum.Count > 0)
                    //{
                    //    if (temp.Default is null)
                    //    {
                    //        temp.Default = temp.Enum.First();
                    //        jSchema.Default = temp.Default;
                    //    }
                    //}
                }
                if (jSchema.Type == JSchemaType.String)
                {
                    if (jSchema.Enum is not null && jSchema.Enum.Count > 0)
                    {
                        if (jSchema.Default is null) jSchema.Default = jSchema.Enum.First();
                    }
                    else if (jSchema.Default is null)
                    {
                        jSchema.Default = " ";
                    }
                }
                else if (jSchema.Type == JSchemaType.Integer || jSchema.Type == JSchemaType.Number)
                {
                    if (jSchema.Default is null)
                    {
                        if (isWeight)
                        {
                            jSchema.Default = 1;
                        }
                        else
                        {
                            jSchema.Default = 0;
                        }
                    }
                }
                else if (jSchema.Type == JSchemaType.Boolean)
                {
                    if (jSchema.Default is null) jSchema.Default = false;
                }
            }
        }
        else
        {
            throw new Exception($"出现特殊情况:{jSchema.Title}");
        }
    }
    #endregion   
}



