﻿using Common;
using OrderSDK.Modles.Amazon.Enums;
using OrderSDK.Modles.Amazon.Feeds;
using OrderSDK.Modles.Amazon.Feeds.ConstructFeed;
using OrderSDK.Modles.Amazon.Feeds.ConstructFeed.Messages;

namespace OrderSDK.Services.Amazon
{
    public class ConstructFeedService
    {

        private FeedAmazonEnvelope envelope = new FeedAmazonEnvelope();
        public ConstructFeedService(string sellerId, string documentVersion, bool? purgeAndReplace = null)
        {
            envelope.Header = new FeedHeader()
            {
                MerchantIdentifier = sellerId,
                DocumentVersion = documentVersion,
            };
            envelope.PurgeAndReplace = purgeAndReplace;
        }

        public void AddPriceMessage(IList<PriceMessage> messages)
        {
            var msgs = new List<BaseMessage>();
            int index = 1;
            foreach (var itm in messages)
            {
                msgs.Add(new BaseMessage()
                {
                    MessageID = index++,
                    Price = itm,
                    OperationType = OperationType.Update
                });
            }
            envelope.Message = msgs;
            envelope.MessageType = FeedMessageType.Price;
        }
        public void AddOfferMessage(IList<OfferMessage> messages, OperationType operationType = OperationType.Update)
        {
            var msgs = new List<BaseMessage>();
            int index = 1;
            foreach (var itm in messages)
            {
                msgs.Add(new BaseMessage()
                {
                    MessageID = index++,
                    Offer = itm,
                    OperationType = operationType
                });
            }
            envelope.Message = msgs;
            envelope.MessageType = FeedMessageType.Product;

        }

        public void AddInventoryMessage(IList<InventoryMessage> messages)
        {
            var msgs = new List<BaseMessage>();
            int index = 1;
            foreach (var itm in messages)
            {
                msgs.Add(new BaseMessage()
                {
                    MessageID = index++,
                    Inventory = itm,
                    OperationType = OperationType.Update
                });
            }
            envelope.Message = msgs;
            envelope.MessageType = FeedMessageType.Inventory;

        }

        public void AddProductMessage(IList<ProductMessage> messages, OperationType operationType = OperationType.Update)
        {
            var msgs = new List<BaseMessage>();
            int index = 1;
            foreach (var itm in messages)
            {
                msgs.Add(new BaseMessage()
                {
                    MessageID = index++,
                    Product = itm,
                    OperationType = operationType
                });
            }
            envelope.Message = msgs;
            envelope.MessageType = FeedMessageType.Product;
        }

        public void AddOrderFulfillmentMessage(IList<OrderFulfillmentMessage> messages, OperationType operationType = OperationType.Update)
        {
            var msgs = new List<BaseMessage>();
            int index = 1;
            foreach (var itm in messages)
            {
                msgs.Add(new BaseMessage()
                {
                    MessageID = index++,
                    OrderFulfillment = itm,
                    OperationType = operationType
                });
            }
            envelope.Message = msgs;
            envelope.MessageType = FeedMessageType.OrderFulfillment;
        }

        public string GetXML()
        {
            return LinqHelper.SerializeObject(envelope);
        }


    }
}
