﻿
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OrderSDK.Modles.Amazon.Authorization;
using OrderSDK.Modles.Amazon.Runtime;
using System.Diagnostics;
using System.Net;
using System.Text;

namespace OrderSDK.Services.Amazon
{
    public class LWAClient
    {
        public const string AccessTokenKey = "AKIAUBCTDHAQUUWEOM4Y";
        public const string JsonMediaType = "application/json; charset=utf-8";


        public LWAAccessTokenRequestMetaBuilder LWAAccessTokenRequestMetaBuilder { get; set; }
        public LWAAuthorizationCredentials LWAAuthorizationCredentials { get; private set; }



        public LWAClient(LWAAuthorizationCredentials lwaAuthorizationCredentials)
        {
            LWAAuthorizationCredentials = lwaAuthorizationCredentials;
            LWAAccessTokenRequestMetaBuilder = new LWAAccessTokenRequestMetaBuilder();
            //RestClient = new HttpClient();
            //LWAAuthorizationCredentials.Endpoint.GetLeftPart(UriPartial.Authority);
        }


        /// <summary>
        /// Retrieves access token from LWA
        /// </summary>
        /// <param name="lwaAccessTokenRequestMeta">LWA AccessTokenRequest metadata</param>
        /// <returns>LWA Access Token</returns>
        public virtual async Task<TokenResponse?> GetAccessTokenAsync(HttpClient client, string name, ILogger logger)
        {
            TokenResponse token = new TokenResponse();
            LWAAccessTokenRequestMeta lwaAccessTokenRequestMeta = LWAAccessTokenRequestMetaBuilder.Build(LWAAuthorizationCredentials);
            string jsonRequestBody = JsonConvert.SerializeObject(lwaAccessTokenRequestMeta);
            try
            {
                HttpResponseMessage response = new();
                string content = "";
                string url = LWAAuthorizationCredentials.Endpoint.AbsolutePath.ToString();
                HttpRequestMessage accessTokenRequest = new HttpRequestMessage(HttpMethod.Post, "http://httpproxyhw.miwaimao.com");
                accessTokenRequest.Content = new StringContent(jsonRequestBody, null, "application/json");
                accessTokenRequest.Headers.Add("ProxyUrl", LWAAuthorizationCredentials.Endpoint.ToString());
                try
                {
                    response = await client.SendAsync(accessTokenRequest);
                    byte[] bytes = await response.Content.ReadAsByteArrayAsync();
                    content = Encoding.UTF8.GetString(bytes);
                    if (response.IsSuccessStatusCode)
                    {
                        try
                        {
                            token = JsonConvert.DeserializeObject<TokenResponse>(content) ?? null!;
                            return token;
                        }
                        catch (Exception e)
                        {
                            logger.LogError($"GetAccessTokenAsync error:storeName:-{name}");
                            logger.LogError(e, e.Message);
                            logger.LogError(e, e.StackTrace);
                            return null!;
                        }
                    }
                    else
                    {
                        logger.LogError($"GetAccessTokenAsync error【{response.StatusCode}】:storeName:-{name} response fail:{content}");
                        return null!;
                    }
                }
                catch (Exception e)
                {
                    logger.LogError("GetAccessTokenAsync:" + e.Message);
                    logger.LogError(e, e.StackTrace);
                }
                finally
                {
                    StringBuilder Info = new();
                    Info.AppendLine($"----GetAccessTokenAsync");
                    Info.AppendLine($"---storeName:-{name}----");
                    Info.Append($"Request[{JsonConvert.SerializeObject(accessTokenRequest)}]:");
                    Info.Append($"Response code:[{response?.StatusCode}]:");
                    Info.Append($"Response Headers:");
                    response?.Headers.ForEach(x => Info.Append($"key:{x.Key},value:{string.Join(',', x.Value)};"));
                    Info.AppendLine($"Response content:{content}");
                    Info.AppendLine("================================");
                    logger.LogInformation(Info.ToString());
                }
            }
            catch (Exception e)
            {
                logger.LogError($"GetAccessTokenAsync error:storeName:-{name},Error getting LWA Access Token");
                logger.LogError(e, e.Message);
                logger.LogError(e, e.StackTrace);
            }
            return null;
        }
    }
}
