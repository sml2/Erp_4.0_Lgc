

using Amazon.SecurityToken;
using Amazon.SecurityToken.Model;
using MediatR;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OrderSDK.DomainEvents;
using OrderSDK.Exceptions;
using OrderSDK.Modles;
using OrderSDK.Modles.Amazon;
using OrderSDK.Modles.Amazon.Authorization;
using OrderSDK.Modles.Amazon.CatalogItems;
using OrderSDK.Modles.Amazon.Enums;
using OrderSDK.Modles.Amazon.Runtime;
using OrderSDK.Modles.CommonType;
using OrderSDK.Services.Amazon;
using System.Diagnostics;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;

namespace OrderSDK.Services
{
    public class AmazonClient : Client
    {              
        public readonly AmazonProxyOption amazonProxy;

        private bool UseProxy = false;
        public AmazonClient(IHttpClientFactory IHttpClientFactory, ILogger<AmazonClient> ILogger, IServiceProvider serviceProvider, IMediator mediator)
        {
            logger = ILogger;
            _mediator = mediator;
            amazonProxy = BaseAmazonOption.amazonProxy;
            //代理
            var con = new ConfigurationBuilder().AddJsonFile("appsettings.json", false, true).Build();
            UseProxy = con.GetSection("UseProxy").Value.Equals("true", StringComparison.CurrentCultureIgnoreCase);

            if (HttpClient is null)
            {
                if (UseProxy)
                {
                    if (amazonProxy is null)
                    {
                        ILogger.LogError("amazonProxy is null");
                        throw new ArgumentNullException(nameof(amazonProxy));
                    }
                    //P31
                    //var webProxy = new WebProxy(amazonProxy.Address);
                    //webProxy.BypassProxyOnLocal = true;
                    //var proxyHttpClientHandler = new HttpClientHandler
                    //{
                    //    Proxy = webProxy,
                    //    UseProxy = true
                    //};
                    //HttpClient httpClient = new HttpClient(proxyHttpClientHandler);                   
                    //HttpClient = httpClient;

                    HttpClient = IHttpClientFactory.CreateClient(nameof(AmazonClient));                    
                }
                else
                {
                    HttpClient= IHttpClientFactory.CreateClient(nameof(AmazonClient));
                }
            }                       
        }

        readonly ILogger logger;
        private readonly IMediator _mediator;
        readonly HttpClient HttpClient;                        
        public static readonly string AccessTokenHeaderName = "x-amz-access-token";  
        public static readonly string SecurityTokenHeaderName = "x-amz-security-token";
        private readonly string RateLimitLimitHeaderName = "x-amzn-RateLimit-Limit";
        //public static readonly string ShippingBusinessIdHeaderName = "x-amzn-shipping-business-id";

        public async Task<T?> ExecuteRequestAsync<T>(RequestAmazonSP request, StoreRegion store) where T : class
        {
            var f = await CreateAuthorizedRequestAsync(request, store);
            if (!f)
            {
                logger.LogInformation($"get token fail");
                return null;
            }
            var tryCount = 0;
            if (store.amazonToken.GetToken() is not null)
            {
                while (true)
                {
                    try
                    {
                        var result = await Send<T>(request, store);
                        if (result.IsNull())
                        {
                            logger.LogInformation($"result is null,Try again!");
                            throw new AmazonQuotaExceededException("result is null,Try again!");
                        }                        
                        else
                        {
                            return result;
                        }
                    }
                    catch (AmazonQuotaExceededException ex)
                    {
                        var amazonCredential = store.GetAmazonCredential();
                        if (tryCount >= amazonCredential.MaxThrottledRetryCount)
                        {
                            logger.LogInformation($"{request.RateLimitType.GetDescription()},make the {tryCount} attempt! msg:{ex.Message}");
                            return null;
                        }                       
                        amazonCredential.UsagePlansTimings[request.RateLimitType].Delay(logger, $"Send {request.RateLimitType,15} ");
                        tryCount++;
                    }
                }
            }
            return null;
        }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="store"></param>
            /// <param name="tokenDataType"></param>
            /// <param name="requestPII"></param>
            /// <returns>int ,0表示tyoken is null；-1：token有error；1 token successfull；</returns>
            /// <exception cref="ArgumentNullException"></exception>
            protected async Task<TokenResponse> RefreshTokenAsync(StoreRegion store, TokenDataType tokenDataType = TokenDataType.Normal,
            CreateRestrictedDataTokenRequest requestPII = null!)
        {
            var key = store.amazonToken;
            var token = key.GetToken();
            if (token == null)  
            {
                if (tokenDataType == TokenDataType.PII) //使用受限数据令牌进行授权
                {                    
                    var pii = await CreateRestrictedDataTokenAsync(requestPII, store);
                    if (pii != null)
                    {
                        token = new TokenResponse(pii.RestrictedDataToken, pii.ExpiresIn);
                    }
                    else
                    {
                        throw new ArgumentNullException(nameof(pii));
                    }
                }
                else
                {
                    token = await RefreshAccessTokenAsync(store, tokenDataType);                                
                }

                if(token != null)
                {
                    if(token.error==null)
                    {
                        key.SetToken(token);
                        //update token
                        await _mediator.Publish(new UpdateTokenEvent(store.ID, token));
                    }
                }               
            }
            return token;
        }      

        /// <summary>
        ///刷新token
        /// </summary>
        /// <param name="request"></param>
        /// <param name="store"></param>
        /// <param name="tokenDataType"></param>
        /// <returns></returns>
        public async Task<TokenResponse?> RefreshNormalTokenAsync(RequestAmazonSP request, StoreRegion store, TokenDataType tokenDataType = TokenDataType.Normal)
        {
            var key = store.amazonToken;
            var token = await RefreshAccessTokenAsync(store, tokenDataType); 
            if(token is not null)
            {                             
                if( string.IsNullOrWhiteSpace(token.error))
                {
                    store.amazonToken.SetToken(token);
                }
                if (token != null)
                {
                    if (token.error == null)
                    {
                        key.SetToken(token);
                        //update token
                        await _mediator.Publish(new UpdateTokenEvent(store.ID, token));
                    }
                }
                return token;
            }
            else
            {               
                return null;
            }           
        }


        public HttpRequestMessage? GetHttpRequestMessage(AmazonCredential amazonCredential,RequestAmazonSP request)
        {
            try
            {
                AWSSignerHelper aWSSignerHelper = new AWSSignerHelper();
                HttpRequestMessage request_message;
                string queryStr = "";
                if (request.QueryRequest)
                {
                    queryStr = "?" + aWSSignerHelper.ExtractCanonicalQueryString(request);
                }
                var url = "https://" + amazonCredential.BaseApiUrl + request.Url + queryStr;
                request_message = new HttpRequestMessage(request.Method, url);// "http://httpproxyhw.miwaimao.com");
                
                if (request.BodyRequest)
                {
                    //以下stream和stringContent 都可以
                    //var stream = new MemoryStream(Encoding.UTF8.GetBytes(request.ToPayload()));
                    //request_message.Content = new StreamContent(stream);// new StringContent(request.ToPayload(),null, request.ContentType);  //, Encoding.UTF8, 
                     request_message.Content =  new StringContent(request.ToPayload(),null, request.ContentType);  //, Encoding.UTF8,              
                }

                if (request.Headers.ContainsKey("ProxyUrl"))
                {
                    request.Headers.Remove("ProxyUrl");
                }
                //request.Headers.Add("ProxyUrl", url);

                return request_message;
            }
            catch (System.Exception e)
            {
                logger.LogError($"GetHttpRequestMessage error:{e.Message}");
                return null;
            }
        }

        /// <summary>
        /// 执行之后，更新limitNumber
        /// </summary>
        /// <param name="response"></param>
        /// <param name="amazonCredential"></param>
        private void UpdateLimitNumber(HttpResponseMessage response, RequestAmazonSP request, AmazonCredential amazonCredential, string name)
        {
            decimal rate = 0;
            var limitHeader = response.Headers.Where(a => a.Key.Contains(RateLimitLimitHeaderName, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
            StringBuilder Info = new();
            Info.AppendLine($"--UpdateLimitNumber--{name}-----");
            Info.Append("limitHeader:");
            Info.AppendLine(JsonConvert.SerializeObject(limitHeader));           
            if (limitHeader.IsNotNull() && limitHeader.Key.IsNotNull())
            {
                var RateLimitValue = limitHeader.Value;
                var s = RateLimitValue.FirstOrDefault();
                decimal.TryParse(s ?? "0", out rate);
            }
            if (amazonCredential.IsActiveLimitRate && request.RateLimitType != RateLimitType.UNSET && rate > 0)
            {
                amazonCredential.UsagePlansTimings[request.RateLimitType].SetRateLimit(rate);
            }
        }

        private void SleepForRateLimit(RequestAmazonSP request,AmazonCredential amazonCredential,string name)
        {
            try
            {               
                StringBuilder Info = new();               
                Info.AppendLine($"--SleepForRateLimit--{name}-----");                                               
                if (amazonCredential.IsActiveLimitRate)
                {
                    if (request.RateLimitType != RateLimitType.UNSET)
                    {                    
                        Info.Append($"RateLimitType:{request.RateLimitType.GetDescription()}");                        
                        var time = amazonCredential.UsagePlansTimings[request.RateLimitType].NextRate(request.RateLimitType,logger);
                    }
                }                
                Info.AppendLine("================================");
                logger.LogInformation(Info.ToString());
            }
            catch (Exception e)
            {
                logger.LogError($"SleepForRateLimit error:{e.Message}");
            }
        }

        public async Task<T?> Send<T>(RequestAmazonSP request, StoreRegion store) where T : class
        {
            try
            {
                string name = $"{store.ID}【{store.Name}】";
                var amazonCredential = store.GetAmazonCredential();                             
                T desc = default(T)!;
                HttpRequestMessage? request_message = GetHttpRequestMessage(amazonCredential, request);
                if (request_message is null)
                {
                    logger.LogWarning($"HttpRequestMessage is null");
                    return null;
                }               
                if (request.Headers.ContainsKey("User-Agent"))
                {
                    request.Headers.Remove("User-Agent");
                }
                request.Headers.Add("User-Agent", $"waimaomvp/build2021 (Language=CN; Host=auth.liulergou.com)");

                if (request.Headers.ContainsKey(AccessTokenHeaderName))
                {
                    request.Headers.Remove(AccessTokenHeaderName);
                }
                request.Headers.Add(AccessTokenHeaderName, store.amazonToken.AccessToken);

                //if (AmazonCredential.ShippingBusiness.HasValue)
                //{
                //    if (request.Headers.ContainsKey(ShippingBusinessIdHeaderName))
                //    {
                //        request.Headers.Remove(ShippingBusinessIdHeaderName);
                //    }
                //    request.Headers.Add(ShippingBusinessIdHeaderName, AmazonCredential.ShippingBusiness.Value.GetEnumMemberValue());
                //}

                await SignWithSTSKeysAndSecurityTokenAsync(request, amazonCredential.BaseApiUrl, amazonCredential);

                if (request.Headers is not null)
                {
                    foreach (var v in request.Headers)
                    {
                        if (v.Key.Contains("Authorization", StringComparison.CurrentCultureIgnoreCase))
                        {
                            var t = v.Value.Split(' ').First();
                            var s = v.Value.Replace($"{v.Value.Split(' ').First()} ", "");
                            request_message.Headers.Authorization = null;
                            request_message.Headers.Authorization = new AuthenticationHeaderValue(v.Value.Split(' ').First(), s);
                        }
                        else
                        {
                            if (!request_message.Headers.Contains(v.Key))
                            {
                                request_message.Headers.Add(v.Key, v.Value);
                            }
                        }
                    }
                }
                var content = string.Empty;
                HttpResponseMessage? response = null;

                try
                {
                    SleepForRateLimit(request, amazonCredential, name);
                    response = await HttpClient.SendAsync(request_message);
                    UpdateLimitNumber(response, request, amazonCredential, name);
                    byte[] bytes = await response.Content.ReadAsByteArrayAsync();
                    content = Encoding.UTF8.GetString(bytes);

                    if(typeof(T)==typeof(GetCatalogItemResponse))
                    {
                        content = content.Replace("{}","''");
                    }
                    if (content.Contains("\"code\": \"QuotaExceeded\""))
                    {
                        return null;
                    }                                                                            
                    desc = JsonConvert.DeserializeObject<T>(content) ?? default!;                                              
                }
                catch (Exception e)
                {
                    logger.LogWarning($"DeserializeObject Fail，Target Type:{typeof(T)},Return Data：{content}，ErrorMessage：{e.Message}");                 
                }
                finally
                {                                      
                    StringBuilder Info = new();
                    var Cls = request.GetType().Name;
                    Info.AppendLine($"----{Cls}");
                    //Info.AppendLine($"----{Cls}{new string('-', Math.Max(28 - Cls.Length, 4))}");
                    Info.AppendLine($"---storeId:-{store.ID}-{store.Name}---");                   
                    Info.AppendLine($"---sellerID:-{store.amazonToken.SellerID}----");
                    Info.AppendLine(request.Url);
                    Info.Append("Request:");
                    Info.AppendLine(JsonConvert.SerializeObject(request));                   
                    Info.Append($"----------------");
                    Info.Append($"Response[{response?.StatusCode}]:");
                    Info.Append($"Response Headers:");
                    response?.Headers.ForEach(x => Info.Append($"key:{x.Key},value:{string.Join(',', x.Value)};"));
                    Info.AppendLine(content);
                    Info.AppendLine("================================");
                    logger.LogInformation(Info.ToString());
                }
                return desc;
            }
            catch (Exception e)
            {
                logger.LogError($"Send error:{e.Message}");
            }
            return null;
        }


        public async Task<TokenResponse> SendLWA(string json, string path)
        {
            TokenResponse token = new TokenResponse();
            var accessTokenRequest = new HttpRequestMessage(HttpMethod.Post, path);
            accessTokenRequest.Content = new StringContent(json, Encoding.UTF8, "application/json; charset=utf-8");
            try
            {
                var response = await HttpClient.SendAsync(accessTokenRequest);
                byte[] bytes = await response.Content.ReadAsByteArrayAsync();
                string content = System.Text.Encoding.UTF8.GetString(bytes);
                if (response.IsSuccessStatusCode)
                {
                    try
                    {
                        token= JsonConvert.DeserializeObject<TokenResponse>(content)??new();                        
                    }
                    catch (Exception e)
                    {
                        logger.LogError($"结果解析失败", e.Message);
                    }
                }
                else
                {
                    logger.LogWarning($"http request fail,httpStatusCode：{response.StatusCode.ToString()}，errorMessage：{0}", content);
                }
            }
            catch (Exception e)
            {
                logger.LogError($"SendLWA", e.Message);
                logger.LogError($"SendLWA", e.StackTrace);
                throw new SystemException("Error getting LWA Access Token", e);
            }
            return token;
        }



        /// <summary>
        /// 异步创建授权请求
        /// </summary>
        /// <param name="AmazonData"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        protected async Task<bool> CreateAuthorizedRequestAsync(RequestAmazonSP request, StoreRegion store)
        {           
            int tryCount = 0;
            TokenResponse token = null;           
            while (true)
            {
                try
                {
                    tryCount++;
                    //判断是否需要使用受限数据令牌进行授权
                    var PiiObject = request as IParameterBasedPII;
                    if (PiiObject != null && PiiObject.IsNeedRestrictedDataToken)
                    {
                        token = await RefreshTokenAsync(store, TokenDataType.PII, PiiObject.RestrictedDataTokenRequest);
                    }
                    else
                    {
                        token = await RefreshTokenAsync(store, request.TokenDataType);
                    }
                    return true;
                }
                catch (AmazonQuotaExceededException ex)
                {
                    var amazonCredential = store.GetAmazonCredential();
                    if (tryCount >= amazonCredential.MaxTokenRetryCount)
                    {                       
                        logger.LogInformation($"RefreshToken:{RateLimitType.Authorization_GetAuthorizationCode.GetDescription()},make the {tryCount} attempt! msg:{ex.Message}");                      
                        return false;
                    }
                    System.Threading.Tasks.Task.Delay(2000).Wait();                                      
                }               
            }           
        }

        public async Task<CreateRestrictedDataTokenResponse?> CreateRestrictedDataTokenAsync(CreateRestrictedDataTokenRequest request, StoreRegion store)
        {                   
            var response = await ExecuteRequestAsync<CreateRestrictedDataTokenResponse>(request,store);
            return response;
        }


        #region tokenService
      
        /// <summary>
        /// 刷新token的方法
        /// </summary>
        /// <param name="credentials"></param>
        /// <param name="tokenDataType"></param>
        /// <returns></returns>
        public async Task<TokenResponse> RefreshAccessTokenAsync(StoreRegion store, TokenDataType tokenDataType = TokenDataType.Normal)
        {
            string name= store.ID+"-"+store.Name;
            try
            {
                var key = store.amazonToken;
                AmazonCredential credentials = store.GetAmazonCredential();
                var lwaCredentials = new LWAAuthorizationCredentials()
                {
                    ClientId = credentials.ClientId,
                    ClientSecret = credentials.ClientSecret,
                    Endpoint = new Uri("https://api.amazon.com/auth/o2/token"),
                    RefreshToken = key.RefreshToken,
                    Scopes = Array.Empty<string>().ToList()
                };
                if (tokenDataType == TokenDataType.Grantless)
                    lwaCredentials.Scopes = new List<string>() { ScopeConstants.ScopeMigrationAPI, ScopeConstants.ScopeNotificationsAPI };

                var Client = new LWAClient(lwaCredentials);
                var accessToken = await Client.GetAccessTokenAsync(HttpClient, name, logger);
                return accessToken;
            }
            catch(Exception e)
            {
                logger.LogError($"Client RefreshAccessTokenAsync error: {e.Message}");
                logger.LogError($"Client RefreshAccessTokenAsync error: {e.StackTrace}");
            }
            return null;
        }

        /// <summary>
        /// 网站授权时，获取AccessToken的方法
        /// </summary>
        /// <param name="ClientId"></param>
        /// <param name="ClientSecret"></param>
        /// <param name="code"></param>
        /// <param name="appRedirectUri"></param>
        /// <param name="grant_type"></param>
        /// <returns></returns>
        public  async Task<TokenResponse> GetAccessTokenFromCodeAsync(string ClientId, string ClientSecret,
            string code, string appRedirectUri, string grant_type = "client_credentials")
        {
            string data = string.Empty;
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://api.amazon.com");
                var byteArray = Encoding.ASCII.GetBytes($"{ClientId}:{ClientSecret}");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                Dictionary<string, string> items = new Dictionary<string, string>();
                items.Add("grant_type", grant_type);
                items.Add("scope", ScopeConstants.ScopeMigrationAPI);
                items.Add("client_id", ClientId);
                items.Add("client_secret", ClientSecret);
                items.Add("code", code);
                items.Add("redirect_uri", appRedirectUri);

                FormUrlEncodedContent formUrlEncodedContent = new FormUrlEncodedContent(items);
                var rs = client.PostAsync("/auth/o2/token", formUrlEncodedContent).Result;
                data = await rs.Content.ReadAsStringAsync();
            }
            return JsonConvert.DeserializeObject<TokenResponse>(data) ?? default!;
        }


        /// <summary>
        /// 获取SecurityToken
        /// </summary>
        /// <param name="request"></param>
        /// <param name="host"></param>
        /// <param name="amazonCredential"></param>
        /// <returns></returns>
        public static async Task<RequestBase> SignWithSTSKeysAndSecurityTokenAsync(RequestAmazonSP request, string host,
            AmazonCredential amazonCredential)
        {
            var dataToken = amazonCredential.GetAWSAuthenticationTokenData();
            //获取当前token          
            if (dataToken == null)
            {
                AssumeRoleResponse response1 = null!;
                try
                {
                    using (var STSClient = new AmazonSecurityTokenServiceClient(amazonCredential.AccessKey, amazonCredential.SecretKey))
                    {
                        var req = new AssumeRoleRequest()
                        {
                            RoleArn = amazonCredential.RoleArn,
                            DurationSeconds = 3600,
                            RoleSessionName = Guid.NewGuid().ToString()
                        };
                        response1 = await STSClient.AssumeRoleAsync(req, new CancellationToken());
                    }
                }
                catch (Exception e)
                {                  
                    throw e;
                }

                //auth step 3
                var awsAuthenticationCredentials = new AWSAuthenticationCredentials
                {
                    AccessKeyId = response1.Credentials.AccessKeyId,
                    SecretKey = response1.Credentials.SecretAccessKey,
                    Region = amazonCredential.regionName!
                };

                amazonCredential.SetAWSAuthenticationTokenData(new AWSAuthenticationTokenData()
                {
                    AWSAuthenticationCredential = awsAuthenticationCredentials,
                    SessionToken = response1.Credentials.SessionToken,
                    Expiration = response1.Credentials.Expiration
                });
                dataToken = amazonCredential.GetAWSAuthenticationTokenData();
            }

            if (request.Headers.ContainsKey(SecurityTokenHeaderName))
            {
                request.Headers.Remove(SecurityTokenHeaderName);
            }
            request.Headers.Add(SecurityTokenHeaderName, dataToken.SessionToken);

            return new AWSSigV4Signer(dataToken.AWSAuthenticationCredential)
                            .Sign(request, amazonCredential.BaseApiUrl);
        }

        #endregion


    }


    public interface IParameterBasedPII
    {
        public bool IsNeedRestrictedDataToken { get; set; }
        public CreateRestrictedDataTokenRequest RestrictedDataTokenRequest { get; set; }
    }

    public class ScopeConstants
    {
        public const string ScopeNotificationsAPI = "sellingpartnerapi::notifications";
        public const string ScopeMigrationAPI = "sellingpartnerapi::migration";
    }


}
