﻿

namespace OrderSDK.Services;

using Microsoft.Extensions.Logging;
using OrderSDK.Modles.CommonType;
using OrderSDK.Modles.Shopee;
using OrderSDK.Modles.Shopee.Auth;
using OrderSDK.Modles.Shopee.BaseHelp;
using OrderSDK.Modles.Shopee.Enums;
using OrderSDK.Modles.Shopee.Order;
using OrderSDK.Modles.Shopee.Payment;
using OrderSDK.Modles.Shopee.Shop;
using OrderSDK.Modles.Amazon.Enums;
using OrderSDK.Modles.Amazon.vm;
using static OrderSDK.Modles.Shopee.Payment.ResponseGetEscrowDetail;
using static OrderSDK.Services.IOrder;

public class ShopeeService :IShopee
{   
    private readonly ShopeeClient shopeeV2Client;
    public  int MaxGets { get => 50; }
    public  GetTypes SupportedGetType => GetTypes.Multiple;
    public readonly ShopeeOption ShopeeOption;


   
    public ShopeeService(ILogger<ShopeeService> logger, ShopeeClient _shopeeV2Client, IServiceProvider serviceProvider ) 
    {
        shopeeV2Client = _shopeeV2Client;       
        ShopeeOption = BaseOption.ShopeeOption;
    }

    public  string GetAuthURL(StoreRegion store, string id)
    {
        string param = "?id=" + id;        
        string apiPath = "/api/v2/shop/auth_partner";
        long timest = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
        string base_string = ShopeeOption.PartnerId + apiPath + timest;
        string sign = EncryptionHelper.GenerateHMAC(ShopeeOption.SecretKey, base_string);
        if (!ShopeeOption.ApiMode)
        {
            return $"{ShopeeOption.SandBox_URL}{apiPath}?partner_id={ShopeeOption.PartnerId}&timestamp={timest}&sign={sign}&redirect={ShopeeOption.RedirectUrl + param}";
        }
        else
        {
            return $"{ShopeeOption.Production_URL}{apiPath}?partner_id={ShopeeOption.PartnerId}&timestamp={timest}&sign={sign}&redirect={ShopeeOption.RedirectUrl + param}";
        }
    }

    //public  async Task<T?> VerifyStore<T>(StoreRegion store, string code) where T : class
    //{
    //    var key = store.GetShopeeData()!;
    //    RequestAccessToken request = new RequestAccessToken();
    //    //$"ShopId-{md.ShopID}-{req.Code}"
    //    string[] astr = code.Split('-');
    //    if ("ShopId".Equals(astr[0]))
    //    {
    //        int.TryParse(astr[1], out int shopid);
    //        request.ShopId = shopid;
    //        //request.MainAccountID = 0;
    //    }
    //    else if ("MainAccountId".Equals(astr[0]))
    //    {
    //        long.TryParse(astr[1], out long mainAccountId);
    //        request.MainAccountID = mainAccountId;
    //        //request.ShopId = 0;
    //    }
    //    else
    //    {
    //        throw new AggregateException("code msg error");
    //    }               
    //    request.Code = astr[2];
    //    request.PartnerId = ShopeeOption.PartnerId;
    //    request.CommonParamType = EShopeeCommonParamType.COMMON;
    //    request.storeID = store.ID;
    //    var result =await  shopeeV2Client.SendTokenRequestAsync<T>(request);
    //    return result as T;
    //}
 

    //public void GetShopInfo(StoreRegion store)
    //{
    //    var key = store.GetShopeeData()!;
    //    RequestGetShopInfo request = new RequestGetShopInfo();
    //    request.storeID = store.ID;
    //    request.ShopId = key.ShopID;
    //    request.shopeedata = key;
    //    var response = shopeeV2Client.SendRequestAsync<ResponseGetShopInfo>(request);
    //}

    //public void Gets(StoreRegion store, string OrderNo)
    //{
    //    string[] ordersnList = new string[] { "201214JASXYXY6" };

    //    var key = store.GetShopeeData()!;        
    //    RequestGetOrderDetail request = new RequestGetOrderDetail();
    //    request.storeID = store.ID;
    //    request.ShopId = key.ShopID;
    //    request.shopeedata = key;
    //    request.OrderSnList = ordersnList;
    //    var b= shopeeV2Client.SendRequestAsync<ResponseGetOrderDetail>(request).Result;        
    //}


    //public  void ListImp(ListParameter listParameter)
    //{
    //    string type = "create_time";//create_time,update_time
    //    List<ResponseGetOrderList.Order> orders = new List<ResponseGetOrderList.Order>();
    //    RequestGetOrderList request = new RequestGetOrderList();
    //    var key = listParameter.store.GetShopeeData()!;
    //    request.storeID = listParameter.store.ID;
    //    request.shopeedata = key;       
    //    request.ShopId = key.ShopID;
    //    request.TimeRangeField = type;
    //    request.TimeFrom = ConvertDateTimeToLong(listParameter.Start);
    //    request.TimeTo = ConvertDateTimeToLong(listParameter.End);
    //    request.PageSize = 20;
        
    //    var response = shopeeV2Client.SendRequestAsync<ResponseGetOrderList>(request).Result;
    //    if (response is not null)
    //    {
    //        if (response.Response is not null)
    //        {
    //            var results = response.Response;
    //            if (results.OrderList is not null && results.OrderList.Count() > 0)
    //            {
    //               // return new(results.More, results.NextCursor, results.OrderList.Select(o => new WaitOrder(o)).ToList());
    //            }
    //        }
    //        else
    //        {
    //            Console.WriteLine(response.Error);
    //        }
    //    }
    //    //return new();
    //}

    //public  void ListNext(StoreRegion store, string nextCycleInfo)
    //{
    //    throw new NotImplementedException();
    //}


    //public void GetOrderList(RequestGetOrderList request, List<ResponseGetOrderList.Order> orders)
    //{
    //    var response = shopeeV2Client.SendRequestAsync<ResponseGetOrderList>(request).Result;
    //    if (response.IsNotNull())
    //    {
    //        if (response.Response is not null)
    //        {
    //            var results = response.Response;
    //            if (results.OrderList.IsNotNull() && results.OrderList.Count() > 0)
    //            {
    //                orders.AddRange(results.OrderList);
    //                if (results.More)  //需要将游标传递过去
    //                {
    //                    request.Cursor = results.NextCursor;
    //                    GetOrderList(request, orders);
    //                }
    //            }
    //        }
    //        else
    //        {
    //            Console.WriteLine(response.Error);
    //        }
    //    }
    //}

  

    //public static long ConvertDateTimeToLong(DateTime dt)
    //{
    //    DateTime dtStart = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
    //    TimeSpan toNow = dt.Subtract(dtStart);
    //    long timeStamp = toNow.Ticks;
    //    timeStamp = long.Parse(timeStamp.ToString().Substring(0, timeStamp.ToString().Length - 4));
    //    return timeStamp;
    //}


    //public bool EscrowList(StoreRegion store, DateTime Start, DateTime End)
    //{
    //    List<ResponsegetEscrowList.escrow> results = new List<ResponsegetEscrowList.escrow>();
    //    RequestgetEscrowList request = new RequestgetEscrowList();
    //    var key = store.GetShopeeData()!;
    //    request.storeID = store.ID;
    //    request.shopeedata = key;
    //    request.ShopId = key.ShopID;
    //    request.ReleaseTimeFrom = ConvertDateTimeToLong(Start);
    //    request.ReleaseTimeTo = ConvertDateTimeToLong(End);
    //    request.PageSize = 20;
    //    var response = shopeeV2Client.SendRequestAsync<ResponsegetEscrowList>(request).Result;
    //    if (response.IsNotNull())
    //    {
    //        if(response.Response.IsNotNull())
    //        {
    //            var result = response.Response;
    //            if (result.more && result.EscrowList.Count() > 0)
    //            {
    //                return false;// (result.more, results.Select(o => new WaitOrder(o)).ToList());
    //            }
    //        }
    //        else
    //        {
    //            Console.WriteLine(response.Error);
    //        }
    //    }
    //    return false;
    //}

    //public void GetEscrowList(RequestGetOrderList request, List<ResponsegetEscrowList.escrow> orders)
    //{
    //    var response = shopeeV2Client.SendRequestAsync<ResponsegetEscrowList>(request).Result;
    //    if (response.IsNotNull() )
    //    {          
    //        if (response.Response.IsNotNull())
    //        {
    //            var results = response.Response;
    //            if (results.EscrowList.IsNotNull() && results.EscrowList.Count() > 0)
    //            {
    //                orders.AddRange(results.EscrowList);
    //                if (results.more)  //没有游标怎么继续取？

                         
    //                {
    //                    // request.Cursor = results.NextCursor;
    //                    GetEscrowList(request, orders);
    //                }
    //            }
    //            else
    //            {
    //                Console.WriteLine(response.Error);
    //            }
    //        }
    //    }
    //}

    //public (bool, orderIncome?) EscrowDetail(StoreRegion store, string order_sn)
    //{
    //    RequestGetEscrowDetail request = new RequestGetEscrowDetail();
    //    var key = store.GetShopeeData()!;
    //    request.storeID = store.ID;
    //    request.shopeedata = key;        
    //    request.ShopId = key.ShopID;
    //    var response = shopeeV2Client.SendRequestAsync<ResponseGetEscrowDetail>(request).Result;
    //    if (response.IsNotNull())
    //    {
    //        if(response.Response.IsNotNull())
    //        {
    //            var result = response.Response;
    //            return (true, result.OrderIncome);
    //        }
           
    //    }
    //    return (false, null);
    //}


   
}
