﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace  OrderSDK.Services.Shopee.Extension
{
    static class MultipartFormDataContentExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <remarks>微软是严格遵循RFC2616，但是有些服务器容器不能识别带双引号的boundary，为了兼容性，暂时去掉</remarks>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static MultipartFormDataContent FormatBoundary(this MultipartFormDataContent instance)
        {
            //boundary="xx" ===>>> boundary=xx
            instance.Headers.ContentType!.Parameters.ToList().ForEach(x => { if (x.Name.Equals("boundary")) { x.Value = x.Value!.Replace("\"", ""); } });
            return instance;
        }

        public static MultipartFormDataContent CreateMultipartFormDataContent() {
            return FormatBoundary(new MultipartFormDataContent());
        }
    }
}
