﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderSDK.Services.Shopee.MyValidator
{
  public abstract class  ValidationAttribute: Attribute
    {
		///// <summary>Gets or sets an error message to associate with a validation control if validation fails.</summary>
		///// <returns>The error message that is associated with the validation control.</returns>
		//public string? ErrorMessage
		//{
		//	get
		//	{
		//		throw null;
		//	}
		//	set
		//	{
		//	}
		//}

		///// <summary>Gets or sets the error message resource name to use in order to look up the <see cref="P:System.ComponentModel.DataAnnotations.ValidationAttribute.ErrorMessageResourceType" /> property value if validation fails.</summary>
		///// <returns>The error message resource that is associated with a validation control.</returns>
		//public string? ErrorMessageResourceName
		//{
		//	get
		//	{
		//		throw null;
		//	}
		//	set
		//	{
		//	}
		//}

		///// <summary>Gets or sets the resource type to use for error-message lookup if validation fails.</summary>
		///// <returns>The type of error message that is associated with a validation control.</returns>
		//[DynamicallyAccessedMembers(DynamicallyAccessedMemberTypes.PublicProperties | DynamicallyAccessedMemberTypes.NonPublicProperties)]
		//public Type? ErrorMessageResourceType
		//{
		//	get
		//	{
		//		throw null;
		//	}
		//	set
		//	{
		//	}
		//}

		///// <summary>Gets the localized validation error message.</summary>
		///// <returns>The localized validation error message.</returns>
		//protected string ErrorMessageString
		//{
		//	[System.Runtime.CompilerServices.NullableContext(1)]
		//	get
		//	{
		//		throw null;
		//	}
		//}

		///// <summary>Gets a value that indicates whether the attribute requires validation context.</summary>
		///// <returns>
		/////   <see langword="true" /> if the attribute requires validation context; otherwise, <see langword="false" />.</returns>
		//public virtual bool RequiresValidationContext
		//{
		//	get
		//	{
		//		throw null;
		//	}
		//}

		///// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DataAnnotations.ValidationAttribute" /> class.</summary>
		//protected ValidationAttribute()
		//{
		//}

		///// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DataAnnotations.ValidationAttribute" /> class by using the function that enables access to validation resources.</summary>
		///// <param name="errorMessageAccessor">The function that enables access to validation resources.</param>
		///// <exception cref="T:System.ArgumentNullException">
		/////   <paramref name="errorMessageAccessor" /> is <see langword="null" />.</exception>
		//[System.Runtime.CompilerServices.NullableContext(1)]
		//protected ValidationAttribute(Func<string> errorMessageAccessor)
		//{
		//}

		///// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DataAnnotations.ValidationAttribute" /> class by using the error message to associate with a validation control.</summary>
		///// <param name="errorMessage">The error message to associate with a validation control.</param>
		//[System.Runtime.CompilerServices.NullableContext(1)]
		//protected ValidationAttribute(string errorMessage)
		//{
		//}

		///// <summary>Applies formatting to an error message, based on the data field where the error occurred.</summary>
		///// <param name="name">The name to include in the formatted message.</param>
		///// <exception cref="T:System.InvalidOperationException">The current attribute is malformed.</exception>
		///// <returns>An instance of the formatted error message.</returns>
		//public virtual string FormatErrorMessage(string name)
		//{
		//	throw null;
		//}

		///// <summary>Checks whether the specified value is valid with respect to the current validation attribute.</summary>
		///// <param name="value">The value to validate.</param>
		///// <param name="validationContext">The context information about the validation operation.</param>
		///// <exception cref="T:System.InvalidOperationException">The current attribute is malformed.</exception>
		///// <exception cref="T:System.ArgumentNullException">
		/////   <paramref name="validationContext" /> is <see langword="null" />.</exception>
		///// <exception cref="T:System.NotImplementedException">
		/////   <see cref="M:System.ComponentModel.DataAnnotations.ValidationAttribute.IsValid(System.Object,System.ComponentModel.DataAnnotations.ValidationContext)" /> has not been implemented by a derived class.</exception>
		///// <returns>An instance of the <see cref="T:System.ComponentModel.DataAnnotations.ValidationResult" /> class.</returns>
		//public ValidationResult? GetValidationResult(object? value, ValidationContext validationContext)
		//{
		//	throw null;
		//}

		///// <summary>Determines whether the specified value of the object is valid.</summary>
		///// <param name="value">The value of the object to validate.</param>
		///// <exception cref="T:System.InvalidOperationException">The current attribute is malformed.</exception>
		///// <exception cref="T:System.NotImplementedException">Neither overload of <see langword="IsValid" /> has been implemented by a derived class.</exception>
		///// <returns>
		/////   <see langword="true" /> if the specified value is valid; otherwise, <see langword="false" />.</returns>
		//public virtual bool IsValid(object? value)
		//{
		//	throw null;
		//}

		///// <summary>Validates the specified value with respect to the current validation attribute.</summary>
		///// <param name="value">The value to validate.</param>
		///// <param name="validationContext">The context information about the validation operation.</param>
		///// <exception cref="T:System.InvalidOperationException">The current attribute is malformed.</exception>
		///// <exception cref="T:System.NotImplementedException">
		/////   <see cref="M:System.ComponentModel.DataAnnotations.ValidationAttribute.IsValid(System.Object,System.ComponentModel.DataAnnotations.ValidationContext)" /> has not been implemented by a derived class.</exception>
		///// <returns>An instance of the <see cref="T:System.ComponentModel.DataAnnotations.ValidationResult" /> class.</returns>
		//protected virtual ValidationResult? IsValid(object? value, ValidationContext validationContext)
		//{
		//	throw null;
		//}

		///// <summary>Validates the specified object.</summary>
		///// <param name="value">The object to validate.</param>
		///// <param name="validationContext">The <see cref="T:System.ComponentModel.DataAnnotations.ValidationContext" /> object that describes the context where the validation checks are performed. This parameter cannot be <see langword="null" />.</param>
		///// <exception cref="T:System.ComponentModel.DataAnnotations.ValidationException">Validation failed.</exception>
		///// <exception cref="T:System.InvalidOperationException">The current attribute is malformed.</exception>
		///// <exception cref="T:System.NotImplementedException">
		/////   <see cref="M:System.ComponentModel.DataAnnotations.ValidationAttribute.IsValid(System.Object,System.ComponentModel.DataAnnotations.ValidationContext)" /> has not been implemented by a derived class.</exception>
		//public void Validate(object? value, ValidationContext validationContext)
		//{
		//}

		///// <summary>Validates the specified object.</summary>
		///// <param name="value">The value of the object to validate.</param>
		///// <param name="name">The name to include in the error message.</param>
		///// <exception cref="T:System.ComponentModel.DataAnnotations.ValidationException">
		/////   <paramref name="value" /> is not valid.</exception>
		///// <exception cref="T:System.InvalidOperationException">The current attribute is malformed.</exception>
		//public void Validate(object? value, string name)
		//{
		//}
	}
}
