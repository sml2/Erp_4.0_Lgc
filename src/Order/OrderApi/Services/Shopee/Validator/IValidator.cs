﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderSDK.Services.Shopee.Validator
{
    interface IValidator
    {
        bool check(object obj);
    }
}
