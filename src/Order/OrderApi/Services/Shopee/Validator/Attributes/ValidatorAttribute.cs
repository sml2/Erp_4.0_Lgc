﻿using System;

namespace OrderSDK.Services.Shopee.Validator.Attributes
{
    public enum Rules
    {
        None, NonZero, NonNull
    }
    class ValidatorAttribute : AbsValidatorAttribute
    {

        public Rules rule;

        public ValidatorAttribute(Rules rule)
        {
            this.rule = rule;
        }

        public override bool check(object? obj)
        {
            switch (rule)
            {
                case Rules.None: 
                    return true;
                case Rules.NonZero:
                    if(obj is string s)
                    {
                        if (double.TryParse(s, out var d))
                        {
                            return d != 0;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else if (obj is double d)
                    {
                        return d != 0;
                    }
                    else if (obj is float f)
                    {
                        return f != 0;
                    }
                    else if (obj is byte b)
                    {
                        return b != 0;
                    }
                    else if (obj is short t)
                    {
                        return t != 0;
                    }
                    else if (obj is int i)
                    {
                        return i != 0;
                    }
                    else if (obj is long l)
                    {
                        return l != 0;
                    }
                    else if (obj is decimal dc)
                    {
                        return dc != 0;
                    }
                    else {
                        return false;
                    }
                case Rules.NonNull:
                    return obj is not null;
                default:
                    throw new ArgumentOutOfRangeException(nameof(rule));
            }
        }
}
}
