﻿using System;

namespace OrderSDK.Services.Shopee.Validator.Attributes
{
    /// <summary>
    /// 自定义验证方法
    /// </summary>
    class ValidatorMethodAttribute : AbsValidatorAttribute
    {
        public string MethodName;
        public string Reason;
        public ValidatorMethodAttribute(string methodName)
        {
            MethodName = methodName;
            Reason = methodName;
        }

        public override bool check(object? obj)
        {
            if (obj is null)
            {
                return false;
            }
            else
            {
                var method = obj.GetType().GetMethod(MethodName);
                object[] ps = { obj };
                if (method is null)
                {
                    throw new ArgumentNullException(MethodName);
                }
                else
                {
                    var r = method.Invoke(obj, ps);

                    if (r is bool rr)
                    {
                        return rr;
                    }
                    else if (r is string ss)
                    {
                        Reason = ss;
                        return ss.Length == 0;
                    }
                    else if (r is (bool, string) || r is (string, bool))
                    {
                        bool result; string reason;
                        if (r is (bool b4, string s4))
                        {
                            result = b4;
                            reason = s4;
                        }
                        else if (r is (string s5, bool b5))
                        {

                            result = b5;
                            reason = s5;
                        }
                        else
                        {
                            throw new BadImageFormatException(nameof(r));
                        }
                        Reason = reason;
                        return result;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
        public override string ToString()
        {
            return Reason ?? $"{MethodName}";
        }
    }
}
