﻿namespace OrderSDK.Services.Shopee.Validator.Attributes
{
    class ValidatorTextAttribute : AbsValidatorAttribute
    {
        public int minlen=default;
        public int maxlen=default;

        public override bool check(object? obj)
        {
            if (obj is string s)
            {
                return s.Length >= minlen && s.Length <= maxlen;
            }
            else {
                return false;
            }
        }
    }
}
