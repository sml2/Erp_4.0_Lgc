﻿using ERP.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderSDK.Services.Shopee.Validator.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
     public abstract class AbsValidatorAttribute : System.Attribute, IValidator
    {
        public abstract bool check(object? obj);
    }

    /// <summary>
    /// 取值范围
    /// </summary>
    public  class ValidatorNumberAttribute : AbsValidatorAttribute
    {
        public bool contains = true;
        public double min;
        public double max;
        public override string ToString()
        {
            return $"val need in {min} to {max}";
        }
        public ValidatorNumberAttribute(double min, double max)
        {
            this.min = min;
            this.max = max;
        }
        public override bool check(object? obj)
        {
            return false;
        }
    }
}
