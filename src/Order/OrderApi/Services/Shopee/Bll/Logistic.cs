﻿
using OrderSDK.Modles.Shopee.Logistic;

namespace OrderSDK.Services.Shopee.Bll;

public class Logistic
{
    private readonly ShopeeClient client;
    public Logistic(ShopeeClient _client)
    {
        client = _client;
    }
    
    public async Task<ResponseGetChannelList?> GetChannelList(RequestGetChannelList request)
    {
        try
        {
            //参数验证，是否必填，格式是否正确
            //xxx
            var response = await client.SendRequestAsync<ResponseGetChannelList>(request);
            return response.Response;
        }
        catch (Exception /*e*/)
        {
            return null;
        }
    }
}
