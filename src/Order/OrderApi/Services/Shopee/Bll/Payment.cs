﻿
using OrderSDK.Modles.Shopee.Payment;

namespace OrderSDK.Services.Shopee.Bll
{
    public class Payment
    {
        private readonly ShopeeClient client;       
        public Payment(ShopeeClient _client)
        {
            client = _client;
        }
     
        public async Task<ResponsegetEscrowList> GetEscrowList(RequestgetEscrowList request)
        {
            var response = await client.SendRequestAsync<ResponsegetEscrowList>(request);
            return response.Response;
        }

        public async Task<ResponseGetEscrowDetail> GetGetEscrowDetail(RequestGetEscrowDetail request)
        {
            var response = await client.SendRequestAsync<ResponseGetEscrowDetail>(request);
            return response.Response;
        }
    }
}
