﻿

using OrderSDK.Modles.Shopee.MediaSpace;

namespace OrderSDK.Services.Shopee.Bll;

public class MediaSpace
{
    private readonly ShopeeClient client;
    public MediaSpace(ShopeeClient _client)
    {
        client = _client;
    }
    public async Task<ResponseUploadImage> UploadImage<ResponseUploadImage>(RequestUploadImage requestUploadImage)
    {
        //参数验证，是否必填，格式是否正确
        //Image file. Max 2.0 MB each. Image format accepted: JPG, JPEG, PNG
        var response = await client.SendFormDataRequestAsync<ResponseUploadImage>(requestUploadImage);
        return response.Response;
    }
}
