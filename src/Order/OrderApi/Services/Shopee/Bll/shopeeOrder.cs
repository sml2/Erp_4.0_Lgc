﻿

using OrderSDK.Modles.Shopee.Order;

namespace OrderSDK.Services.Shopee.Bll;

public class shopeeOrder 
{
    private readonly ShopeeClient client;
    public shopeeOrder(ShopeeClient _client)
    {
        client = _client;
    }
    
    public async  Task<ResponseGetOrderList> GetOrderList(RequestGetOrderList request)
    {
        var response = await client.SendRequestAsync<ResponseGetOrderList>(request);
        return response.Response;
    }

    public async  Task<ResponseGetOrderDetail> GetOrderDetail(RequestGetOrderDetail request)
    {
        var response = await client.SendRequestAsync<ResponseGetOrderDetail>(request);
        return response.Response;
    }

    public ResponseGetShipmentList GetShipmentList(RequestGetShipmentList param)
    {
        throw new NotImplementedException();
    }

    public ResponseSplitOrder SplitOrder(RequestSplitOrder param)
    {
        throw new NotImplementedException();
    }

    public ResponseUnsplitOrder UnsplitOrder(RequestUnsplitOrder param)
    {
        throw new NotImplementedException();
    }

    public ResponseUploadInvoiceDoc UploadInvoiceDoc(RequestUploadInvoiceDoc param)
    {
        throw new NotImplementedException();
    }

    public ResponseDownloadInvoiceDoc DownloadInvoiceDoc(RequestDownloadInvoiceDoc param)
    {
        throw new NotImplementedException();
    }

    public ResponseCancelOrder CancelOrder(RequestCancelOrder param)
    {
        throw new NotImplementedException();
    }


    public ResponseHandleBuyerCancellation HandleBuyerCancellation(RequestHandleBuyerCancellation param)
    {
        throw new NotImplementedException();
    }
    public ResponseAddInvoiceData AddInvoiceData(RequestAddInvoiceData param)
    {

        throw new NotImplementedException();
    }

    public ResponseInvoiceOrderList InvoiceOrderList(RequestInvoiceOrderList param)
    {
        throw new NotImplementedException();
    }

    public ResponseSetNote SetNote(RequestSetNote param)
    {
        throw new NotImplementedException();
    }
}
