﻿using OrderSDK.Modles.Shopee;
using OrderSDK.Modles.Shopee.GlobalProduct;
using OrderSDK.Modles.Shopee.Product;

namespace OrderSDK.Services.Shopee.Bll;

public class GlobalProduct
{
    private readonly ShopeeClient client;
    public GlobalProduct(ShopeeClient _client)
    {
        client = _client;
    }
    
    public async Task<ResponseGetCategory> GetCatagory(RequestGetGlobalCategory request)
    {
        var response = await client.SendRequestAsync<ResponseGetCategory>(request);
        return response.Response;
    }

    public async Task<ResponseGetAttributes> GetAttributes(RequestGetGlobalAttributes request)
    {
        var response = await client.SendRequestAsync<ResponseGetAttributes>(request);
        return response.Response;
    }


    public async Task<ResponseCategoryRecommend> CategoryRecommend(RequestCategoryRecommend request)
    {
        var response = await client.SendRequestAsync<ResponseCategoryRecommend>(request);
        return response.Response;
    }

    public async Task<ResponseGetDTSLimit> GetDtsLimit(RequestGetDTSLimit request)
    {
        var response = await client.SendRequestAsync<ResponseGetDTSLimit>(request);
        return response.Response;
    }


    public async Task<ResponseGetBrandList> GetBrandList(RequestGetBrandList request)
    {
        var response = await client.SendRequestAsync<ResponseGetBrandList>(request);
        return response.Response;
    }

    public async Task<ResponseRegisterBrand> RegisterBrand(RequestRegisterBrand request)
    {
        var response = await client.SendRequestAsync<ResponseRegisterBrand>(request);
        return response.Response;
    }



    public async Task<ResponseGetRecommendAttribute> GetRecommendAttribute(RequestGetRecommendAttribute request)
    {
        var response = await client.SendRequestAsync<ResponseGetRecommendAttribute>(request);
        return response.Response;
    }

    public async Task<ResponseSupportSizeChart> SupportSizeChart(RequestSupportSizeChart request)
    {
        var response = await client.SendRequestAsync<ResponseSupportSizeChart>(request);
        return response.Response;
    }

    public async Task<ResponseUpdateSizeChart> UpdateSizeChart(RequestUpdateSizeChart request)
    {
        var response = await client.SendRequestAsync<ResponseUpdateSizeChart>(request);
        return response.Response;
    }


    public async Task<ResponseAddItem> AddItem(RequestAddItem requestAddItem)
    {
        var response = await client.SendRequestAsync<ResponseAddItem>(requestAddItem);
        return response.Response;
    }

    public async Task<ResponseDeleteItem> DeleteItem(RequestDeleteItem request)
    {
        var response = await client.SendRequestAsync<ResponseDeleteItem>(request);
        return response.Response;
    }

    public async Task<ResponseBoostItem> BoostItem(RequestBoostItem request)
    {

        var response = await client.SendRequestAsync<ResponseBoostItem>(request);
        return response.Response;

    }

    public async Task<ResponseSearchItem> SearchItem(RequestSearchItem request)
    {

        var response = await client.SendRequestAsync<ResponseSearchItem>(request);
        return response.Response;

    }

    public async Task<ResponseGetItemBaseInfo> GetItemBaseInfo(RequestGetItemBaseInfo request)
    {

        var response = await client.SendRequestAsync<ResponseGetItemBaseInfo>(request);
        return response.Response;


    }

    public async Task<ResponseGetItemExtraInfo> GetItemExtraInfo(RequestGetItemExtraInfo request)
    {

        var response = await client.SendRequestAsync<ResponseGetItemExtraInfo>(request);
        return response.Response;

    }

    public async Task<ResponseGetItemLimit> GetItemLimit(RequestGetItemLimit request)
    {

        var response = await client.SendRequestAsync<ResponseGetItemLimit>(request);
        return response.Response;

    }

    public async Task<ResponseGetItemList> GetItemList(RequestGetItemList request)
    {

        var response = await client.SendRequestAsync<ResponseGetItemList>(request);
        return response.Response;

    }

    public async Task<ResponseGetItemPromotion> GetItemPromotion(RequestGetItemPromotion request)
    {

        var response = await client.SendRequestAsync<ResponseGetItemPromotion>(request);
        return response.Response;

    }

    public async Task<ResponseUnlistItem> UnlistItem(RequestUnlistItem request)
    {
        var response = await client.SendRequestAsync<ResponseUnlistItem>(request);
        return response.Response;

    }

    public async Task<ResponseUpdateItem> UpdateItem(RequestUpdateItem request)
    {

        var response = await client.SendRequestAsync<ResponseUpdateItem>(request);
        return response.Response;

    }


    public async Task<ResponseUpdateSipItemPrice> UpdateSipItemPrice(RequestUpdateSipItemPrice request)
    {

        var response = await client.SendRequestAsync<ResponseUpdateSipItemPrice>(request);
        return response.Response;

    }

    public async Task<ResponseUpdateStock> UpdateStock(RequestUpdateStock request)
    {

        var response = await client.SendRequestAsync<ResponseUpdateStock>(request);
        return response.Response;

    }

    public async Task<ResponseUpdatePrice> UpdatePrice(RequestUpdatePrice request)
    {
        var response = await client.SendRequestAsync<ResponseUpdatePrice>(request);
        return response.Response;

    }


    public async Task<ResponseAddModel> AddModel(RequestAddModel requestAddModel)
    {
        var response = await client.SendRequestAsync<ResponseAddModel>(requestAddModel);
        return response.Response;

    }

    public async Task<ResponseDeleteModel> DeleteModel(RequestDeleteModel request)
    {

        var response = await client.SendRequestAsync<ResponseDeleteModel>(request);
        return response.Response;

    }

    public async Task<ResponseGetModelList> GetModelList(RequestGetModelList request)
    {

        var response = await client.SendRequestAsync<ResponseGetModelList>(request);
        return response.Response;

    }

    public async Task<ResponseUpdateModel> UpdateModel(RequestUpdateModel request)
    {
        var response = await client.SendRequestAsync<ResponseUpdateModel>(request);
        return response.Response;

    }

    public async Task<ResponseGetBoostedList> GetBoostedList(RequestGetBoostedList request)
    {

        var response = await client.SendRequestAsync<ResponseGetBoostedList>(request);
        return response.Response;

    }

    public async Task<ResponseGetComment> GetComment(RequestGetComment request)
    {
        var response = await client.SendRequestAsync<ResponseGetComment>(request);
        return response.Response;

    }

    public async Task<ResponseReplyComment> ReplyComment(RequestReplyComment request)
    {
        var response = await client.SendRequestAsync<ResponseReplyComment>(request);
        return response.Response;

    }

    public async Task<ResponseInitTierVariation> InitTierVariation(RequestInitTierVariation request)
    {
        var response = await client.SendRequestAsync<ResponseInitTierVariation>(request);
        return response.Response;

    }

    public async Task<ResponseUpdateTierVariation> UpdateTierVariation(RequestUpdateTierVariation request)
    {
        var response = await client.SendRequestAsync<ResponseUpdateTierVariation>(request);
        return response.Response;

    }
}