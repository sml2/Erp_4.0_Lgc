﻿
using OrderSDK.Modles.Shopee;
using OrderSDK.Modles.Shopee.Product;

namespace OrderSDK.Services.Shopee.Bll;

public class shopeeProduct
{
    private readonly ShopeeClient client;
    public shopeeProduct(ShopeeClient _client)
    {
        client = _client;
    }

    public async Task<ShopeeResponseBase<ResponseGetCategory>> GetCatagory(RequestGetCategory request)
    {
        var response = await client.SendRequestAsync<ResponseGetCategory>(request);
        return response;
    }

    public async Task<ShopeeResponseBase<ResponseCategoryRecommend>> CategoryRecommend(RequestCategoryRecommend request)
    {
        var response = await client.SendRequestAsync<ResponseCategoryRecommend>(request);
        return response;
    }

    public async Task<ShopeeResponseBase<ResponseGetDTSLimit>> GetDtsLimit(RequestGetDTSLimit request)
    {
        var response = await client.SendRequestAsync<ResponseGetDTSLimit>(request);
        return response;
    }


    public async Task<ShopeeResponseBase<ResponseGetBrandList>> GetBrandList(RequestGetBrandList request)
    {
        var response = await client.SendRequestAsync<ResponseGetBrandList>(request);
        return response;
    }

    public async Task<ShopeeResponseBase<ResponseRegisterBrand>> RegisterBrand(RequestRegisterBrand request)
    {
        var response = await client.SendRequestAsync<ResponseRegisterBrand>(request);
        return response;
    }


    public async Task<ShopeeResponseBase<ResponseGetAttributes>> GetAttributes(RequestGetAttributes request)
    {
        var response = await client.SendRequestAsync<ResponseGetAttributes>(request);
        return response;
    }

    public async Task<ShopeeResponseBase<ResponseGetRecommendAttribute>> GetRecommendAttribute(RequestGetRecommendAttribute request)
    {
        var response = await client.SendRequestAsync<ResponseGetRecommendAttribute>(request);
        return response;
    }

    public async Task<ShopeeResponseBase<ResponseSupportSizeChart>> SupportSizeChart(RequestSupportSizeChart request)
    {
        var response = await client.SendRequestAsync<ResponseSupportSizeChart>(request);
        return response;
    }

    public async Task<ShopeeResponseBase<ResponseUpdateSizeChart>> UpdateSizeChart(RequestUpdateSizeChart request)
    {
        var response = await client.SendRequestAsync<ResponseUpdateSizeChart>(request);
        return response;
    }


    public async Task<ShopeeResponseBase<ResponseAddItem>> AddItem(RequestAddItem requestAddItem)
    {
        var response = await client.SendRequestAsync<ResponseAddItem>(requestAddItem);
        return response;
    }

    public async Task<ShopeeResponseBase<ResponseDeleteItem>> DeleteItem(RequestDeleteItem request)
    {
        var response = await client.SendRequestAsync<ResponseDeleteItem>(request);
        return response;
    }

    public async Task<ShopeeResponseBase<ResponseBoostItem>> BoostItem(RequestBoostItem request)
    {

        var response = await client.SendRequestAsync<ResponseBoostItem>(request);
        return response;

    }

    public async Task<ShopeeResponseBase<ResponseSearchItem>> SearchItem(RequestSearchItem request)
    {

        var response = await client.SendRequestAsync<ResponseSearchItem>(request);
        return response;

    }

    public async Task<ShopeeResponseBase<ResponseGetItemBaseInfo>> GetItemBaseInfo(RequestGetItemBaseInfo request)
    {

        var response = await client.SendRequestAsync<ResponseGetItemBaseInfo>(request);
        return response;


    }

    public async Task<ShopeeResponseBase<ResponseGetItemExtraInfo>> GetItemExtraInfo(RequestGetItemExtraInfo request)
    {

        var response = await client.SendRequestAsync<ResponseGetItemExtraInfo>(request);
        return response;

    }

    public async Task<ShopeeResponseBase<ResponseGetItemLimit>> GetItemLimit(RequestGetItemLimit request)
    {

        var response = await client.SendRequestAsync<ResponseGetItemLimit>(request);
        return response;

    }

    public async Task<ShopeeResponseBase<ResponseGetItemList>> GetItemList(RequestGetItemList request)
    {

        var response = await client.SendRequestAsync<ResponseGetItemList>(request);
        return response;

    }

    public async Task<ShopeeResponseBase<ResponseGetItemPromotion>> GetItemPromotion(RequestGetItemPromotion request)
    {

        var response = await client.SendRequestAsync<ResponseGetItemPromotion>(request);
        return response;

    }

    public async Task<ShopeeResponseBase<ResponseUnlistItem>> UnlistItem(RequestUnlistItem request)
    {
        var response = await client.SendRequestAsync<ResponseUnlistItem>(request);
        return response;

    }

    public async Task<ShopeeResponseBase<ResponseUpdateItem>> UpdateItem(RequestUpdateItem request)
    {

        var response = await client.SendRequestAsync<ResponseUpdateItem>(request);
        return response;

    }


    public async Task<ShopeeResponseBase<ResponseUpdateSipItemPrice>> UpdateSipItemPrice(RequestUpdateSipItemPrice request)
    {

        var response = await client.SendRequestAsync<ResponseUpdateSipItemPrice>(request);
        return response;

    }

    public async Task<ShopeeResponseBase<ResponseUpdateStock>> UpdateStock(RequestUpdateStock request)
    {

        var response = await client.SendRequestAsync<ResponseUpdateStock>(request);
        return response;

    }

    public async Task<ShopeeResponseBase<ResponseUpdatePrice>> UpdatePrice(RequestUpdatePrice request)
    {
        var response = await client.SendRequestAsync<ResponseUpdatePrice>(request);
        return response;

    }


    public async Task<ShopeeResponseBase<ResponseAddModel>> AddModel(RequestAddModel requestAddModel)
    {
        var response = await client.SendRequestAsync<ResponseAddModel>(requestAddModel);
        return response;

    }

    public async Task<ShopeeResponseBase<ResponseDeleteModel>> DeleteModel(RequestDeleteModel request)
    {

        var response = await client.SendRequestAsync<ResponseDeleteModel>(request);
        return response;

    }

    public async Task<ShopeeResponseBase<ResponseGetModelList>> GetModelList(RequestGetModelList request)
    {

        var response = await client.SendRequestAsync<ResponseGetModelList>(request);
        return response;

    }

    public async Task<ShopeeResponseBase<ResponseUpdateModel>> UpdateModel(RequestUpdateModel request)
    {
        var response = await client.SendRequestAsync<ResponseUpdateModel>(request);
        return response;

    }

    public async Task<ShopeeResponseBase<ResponseGetBoostedList>> GetBoostedList(RequestGetBoostedList request)
    {

        var response = await client.SendRequestAsync<ResponseGetBoostedList>(request);
        return response;

    }

    public async Task<ShopeeResponseBase<ResponseGetComment>> GetComment(RequestGetComment request)
    {
        var response = await client.SendRequestAsync<ResponseGetComment>(request);
        return response;

    }

    public async Task<ShopeeResponseBase<ResponseReplyComment>> ReplyComment(RequestReplyComment request)
    {
        var response = await client.SendRequestAsync<ResponseReplyComment>(request);
        return response;

    }

    public async Task<ShopeeResponseBase<ResponseInitTierVariation>> InitTierVariation(RequestInitTierVariation request)
    {
        var response = await client.SendRequestAsync<ResponseInitTierVariation>(request);
        return response;

    }

    public async Task<ShopeeResponseBase<ResponseUpdateTierVariation>> UpdateTierVariation(RequestUpdateTierVariation request)
    {
        var response = await client.SendRequestAsync<ResponseUpdateTierVariation>(request);
        return response;

    }
}

