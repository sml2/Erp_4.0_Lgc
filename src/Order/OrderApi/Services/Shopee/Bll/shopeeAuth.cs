﻿using Newtonsoft.Json;
using OrderSDK.Modles.Shopee;
using OrderSDK.Modles.Shopee.Auth;

namespace OrderSDK.Services.Shopee.Bll;
public class shopeeAuth
{
    private readonly ShopeeClient client;
    private readonly ShopeeOption option;
    public shopeeAuth(ShopeeClient _client)
    {
        client = _client;
        option = BaseOption.ShopeeOption;
    }


    public async Task<ResponseAccessToken> GetAccessToken(RequestAccessToken request)
    {
        try
        {
            var response = await client.SendRequest(request);
            bool IsSuccess = response.IsSuccessStatusCode;
            string Code = response.StatusCode.ToString();
            string content = await response.Content.ReadAsStringAsync();
            var desc = JsonConvert.DeserializeObject<ResponseAccessToken>(content);
            return desc!;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            ResponseAccessToken token = new ResponseAccessToken()
            {
                Error = e.Message,
            };
            return token;
        }
    }

    public async Task<ResponseAccessToken> RefreshAccessToken(RequestRefreshAccessToken request)
    {
        try
        {
            var response = await client.SendRequest(request);
            string content = await response.Content.ReadAsStringAsync();
            var desc = JsonConvert.DeserializeObject<ResponseAccessToken>(content);
            return desc!;

        }
        catch (Exception e)
        {
            ResponseAccessToken token = new() { Error=e.Message};
            return token;
        }
        
    }

}
