using ERP.Data.Products;
using ERP.Enums;
using ERP.Enums.Product;

namespace ERP.Models.View.Products.Product;

public class VariantDto
{
    public VariantDto()
    {
    }

    public VariantDto(Variant item)
    {
        var tempIds = new List<Ids>();

        foreach (var i in item.Ids)
        {
            tempIds.Add(new Ids()
            {
                Tag = i.Key,
                Val = i.Value,
            });
        }

        Ids = tempIds;
        Code = item.Code;
        Cost = item.Cost;
        Sale = item.Sale;
        Quantity = item.Quantity;
        Sid = item.Sid;
        Sku = item.Sku;
        Asin = item.Asin;
        State = item.State;
        Images = item.Images;
        UploadState = item.UploadState ?? ProductUploadEnum.NotUploaded;
    }


    [NJP("ids")]
    public List<Ids> Ids { get; set; }

    [NJP("code")]
    public string? Code { get; set; }

    [NJP("cost")]
    public decimal Cost { get; set; }

    [NJP("sale")]
    public decimal Sale { get; set; }

    [NJP("quantity")]
    public int Quantity { get; set; }

    [NJP("sid")]
    public string? Sid { get; set; } = string.Empty;

    [NJP("sku")]
    public string? Sku { get; set; } = string.Empty;

    [NJP("asin")]
    public string? Asin { get; set; } = string.Empty;

    [NJP("state")]
    public VariantStateEnum State { get; set; }

    [NJP("img")]
    public VariantImages Images { get; set; }

    [NJP("basic")]
    public virtual LanguageInfo? Basic { get; set; }

    public ProductUploadEnum UploadState { get; set; } = ProductUploadEnum.NotUploaded;
}