using ERP.Data.Products;
using ERP.Models.DB.Product;

namespace ERP.Models.View.Products.Product;

public class ImageViewModel
{
    public ImageViewModel(){}
    public ImageViewModel(Image image)
    {
        ID = image.ID;
        Path = image.IsNetwork ? image.Url : image.Path!;
        Url = image.Url;
        Size = image.Size;
        Time = image.Time;
        IsNetwork = image.IsNetwork;
        IsEditor = image.IsEditor;
        
    }

    public ImageViewModel(TempImagesModel image)
    {
        ID = image.ID;
        Path = image.Path;
        Size = image.Size;
        Time = image.CreatedAt;
    }

    public int ID { get; set; }
    public string? Path { get; set; }
    public string? Url { get; set; } = string.Empty;
    public DateTime? Time { get; set; }
    public long Size { get; set; }
    public bool IsNetwork { get; set; }
    
    public bool IsEditor { get; set; } = false;
}