using ERP.Data.Products;

namespace ERP.Models.View.Products.Product;

public class ImagesViewModel : List<ImageViewModel>
{
    public ImagesViewModel(){}
    public ImagesViewModel(IEnumerable<Image> images)
    {
        images.ForEach(m =>  Add(new ImageViewModel(m)));
    }
}

public static class ToImagesViewModelExtension
{
    public static ImagesViewModel ToImagesVm(this IEnumerable<Image> images) => new ImagesViewModel(images);

}
