namespace ERP.Models.View.Products.Product;

public class SetProductPriceDto
{
    [NJP("id")]
    public int Id { get; set; }
    
    [NJP("params")]
    public List<ProductPriceParams> @Params { get; set; }
}

public class ProductPriceParams
{
    [NJP("type")]
    public SetProductPriceTypeEnum Type { get; set; }

    [NJP("name")]
    public string Name { get; set; }

    [NJP("mode")]
    public SetProductPriceModeEnum Mode { get; set; }

    [NJP("numberType")]
    public SetProductPriceNumberTypeEnum NumberType { get; set; }

    [NJP("value")]
    public decimal Value { get; set; }
}

public enum SetProductPriceTypeEnum
{
    //成本价格
    Cost = 1,

    //销售价格
    Sale = 2,

    //变体库存

    Quantity = 3
}

public enum SetProductPriceModeEnum
{
    //添加
    Add = 1,

    //减少
    Reduce = 2,

    //覆盖
    Cover = 3
}

public enum SetProductPriceNumberTypeEnum
{
    //数字
    Number = 1,

    //百分比
    Percentage = 2,
}