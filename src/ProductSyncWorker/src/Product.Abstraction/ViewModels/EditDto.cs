using ERP.Data.Products;
using ERP.Data.Products.Expand;
using ERP.Data.Products.Feature;
using Attribute = ERP.Data.Products.Attribute;

namespace ERP.Models.View.Products.Product;

public class EditDto
{
    public int? Id { get; set; }

    [NJP("languages")]
    public Dictionary<Enums.Languages, LanguageInfo> Languages { get; set; }

    [NJP("source")]
    public string? Source { get; set; }

    [NJP("category_id")]
    public int? CategoryId { get; set; }

    [NJP("sub_category_id")]
    public int? SubCategoryId { get; set; }

    [NJP("isImageSync")]
    public bool IsImageSync { get; set; }

    [NJP("quantity")]
    public int? Quantity { get; set; }

    [NJP("attribute")]
    public Dictionary<int, Attribute>? Attributes { get; set; }

    [NJP("attributeTag")]
    public int AttributeTag { get; set; }

    [NJP("isSingle")]
    public bool IsSingle { get; set; }

    [NJP("coin")]
    public string Coin { get; set; }

    [NJP("price")]
    public Prices Prices
    {
        get => _prices;
        set => SetPrices(value);
    }

    protected Prices _prices { get; set; }

    protected virtual void SetPrices(Prices value)
    {
        if (Coin.IsNull())
        {
            throw new InvalidCastException($"coin is null");
        }
        _prices = value;
        _prices.Cost.SetMax(value.Cost.Max, Coin);
        _prices.Cost.SetMin(value.Cost.Min, Coin);
        
        _prices.Sale.SetMax(value.Sale.Max, Coin);
        _prices.Sale.SetMin(value.Sale.Min, Coin);
    }


    [NJP("variants")]
    public List<VariantDto>? Variants { get; set; }

    [NJP("images")]
    public Images? Images { get; set; }

    public Enums.Languages ShowLanguage { get; set; }

    public string? Pid { get; set; }

    [NJP("newCreateImages")]
    public List<int> NewCreateImagesIds { get; set; }

    [NJP("expand")]
    public Expands Expand { get; set; }
    
    
    [NJP("features")]
    public Features Features { get; set; }

    //todo 产品没有修改 无需新增json文件
    [NJP("isChange")]
    public bool IsChange { get; set; }
}

public class LanguageInfo
{
    public string? Title { get; set; }
    public string? Keyword { get; set; }
    public virtual List<Dictionary<string, string>>? Sketch { get; set; }
    public string? Desc { get; set; }
}

public class Ids
{
    [NJP("tag")]
    public int Tag { get; set; }

    [NJP("val")]
    public int Val { get; set; }
}