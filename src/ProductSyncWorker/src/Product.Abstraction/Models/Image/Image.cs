﻿using ERP.Models.DB.Product;
using ERP.Models.View.Products.Product;

namespace ERP.Data.Products;
    public class Image
    {
        public Image()
        {
        }
        public Image(int id, string url):this()
        {
            ID = id;
            Url = url;
        }
        
        public Image(ImageViewModel vm):this()
        {
            ID = vm.ID;
            if (vm.Size == DEFAULT_SIZE)
            {
                Url = vm.Path;
            }
            else
            {
                Path = vm.Path;
            }
            ID = vm.ID;
            Url = vm.Url ?? string.Empty;
            Size = vm.Size;
            Time = vm.Time;
            IsEditor = vm.IsEditor;
        }
        
        public Image(TempImagesModel data)
        {
            if (data.Size == DEFAULT_SIZE)
            {
                Url = data.Path;
            }
            else
            {
                Path = data.Path;
            }
            ID = data.ID;
            Size = data.Size;
            Time = data.CreatedAt;
        }
        
        public Image(ResourceModel data)
        {
            ID = data.ID;
            if (data.Size == DEFAULT_SIZE)
            {
                Url = data.Path;
            }
            else
            {
                Path = data.Path;
            }
            Size = data.Size;
            Time = data.UpdatedAt;
        } 

        public int ID { get; set; }
        public string Url { get; set; } = string.Empty;

        private const int DEFAULT_SIZE = -1;
        [NJI,SJI]
        public bool HasSync
        {
            get => Size != DEFAULT_SIZE;
        }
        
        [NJI,SJI]
        public bool IsNetwork
        {
            get => Size == DEFAULT_SIZE;
        }

        //====================已同步信息====================
        /// <summary>
        /// 占用空间
        /// </summary>
        public long Size { get; set; } = DEFAULT_SIZE;

        /// <summary>
        /// 资源相对路径
        /// </summary>
        public string? Path { get; set; } = null;
        /// <summary>
        /// 同步时间
        /// </summary>
        public DateTime? Time { get; set; } = DateTime.MinValue;

        public Boolean IsEditor { get; set; } = false;
    }
