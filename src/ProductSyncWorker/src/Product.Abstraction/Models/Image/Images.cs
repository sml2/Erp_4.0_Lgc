﻿using ERP.Models.View.Products.Product;

namespace ERP.Data.Products;

public class Images : List<Image>
{
    public void Add(int id, string url) => Add(new(id,url));
    
    public Images(){}

    public Images(IEnumerable<ImageViewModel> images) 
    {
        images.ForEach(m =>  Add(new Image(m)));
    }
    public Images(IEnumerable<Image> images) : base(images)
    {
        
    }
    public Image? Main
    {
        get => this.FirstOrDefault();
    }
    
    public Images(ImagesViewModel images)
    {
        
    }
}

public static class ToImagesExtension
{
    public static Images ToImages(this IEnumerable<Image> images) => new Images(images);
    public static Images ToImages(this IEnumerable<ImageViewModel> images) => new Images(images);
}