﻿namespace ERP.Data.Products;

public class Attributes
{
    
    public void Add(int num ,string key, string[] value,AttributeStateEnum state) => items.Add(num, new(key, value,state));
    public void Add(string key, string[] value) => items.Add(MakeCreateNum(), new(key, value));
    public int MakeCreateNum() => ++CurCreateNum;
    public int CurCreateNum { get; set; }
    public Dictionary<int, Attribute> items { get; set; } = new Dictionary<int, Attribute>();
    public bool IsSingle { get; set; }
}

public class Attribute
{
    public Attribute(){}
    public Attribute(string name, string[] value)
    {
        Name = name;
        Value = value;
        State = AttributeStateEnum.Enable;
    }
    public Attribute(string name, string[] value, AttributeStateEnum state)
    {
        Name = name;
        Value = value;
        State = state;
    }
    public string Name { get; set; }
    public string[] Value { get; set; }
    public AttributeStateEnum State { get; set; }
}

public enum AttributeStateEnum
{
    Disable,
    Enable
}