﻿using ERP.Enums;
using ERP.Models.View.Products.Product;

namespace ERP.Data.Products;

public class Variant : PropertyBase
{
    public Variant()
    {
    }

    public Variant(VariantDto vm, string unit)
    {
        var tempIds = new Dictionary<int, int>();
        foreach (var item in vm.Ids)
        {
            tempIds.Add(item.Tag, item.Val);
        }

        Ids = tempIds;
        Code = vm.Code;
        Cost = MoneyFactory.Instance.Create(vm.Cost, unit);
        Sale = MoneyFactory.Instance.Create(vm.Sale, unit);
        Quantity = vm.Quantity;
        Sid = vm.Sid;
        Sku = vm.Sku;
        State = vm.State;
        Images = vm.Images;
        UploadState = vm.UploadState;
        if (vm.Basic.IsNotNull())
        {
            SetTitle(vm.Basic.Title);
            SetKeyword(vm.Basic.Keyword);
            SetDescription(vm.Basic.Desc);
            var sketches = new List<string>();
            if (vm.Basic.Sketch is not null)
            {
                foreach (var i in vm.Basic.Sketch)
                {
                    sketches.Add(i["value"]);
                }
            }

            SetSketches(sketches);
        }

    }
    public Dictionary<int, int> Ids { get; set; }
    // public string Code { get; set; }= string.Empty;
    public Money Cost { get; set; }
    public Money Sale { get; set; }
    // public int Quantity { get; set; }
    // public string Sid { get; set; } = string.Empty;
    // public string Sku { get; set; } = string.Empty;
    // public string Asin { get; set; } = string.Empty;
    public VariantStateEnum State { get; set; }
    public VariantImages Images { get; set; }
}

public class VariantImages
{
    [NJP("main")]
    public int? Main { get; set; }

    [NJP("affiliate")]
    public List<int>? Affiliate { get; set; }
}

public enum VariantStateEnum
{
    Disable,
    Enable
}