﻿using ERP.Models.View.Products.Product;
using Newtonsoft.Json;

namespace ERP.Data.Products;

[JsonObject(MemberSerialization.Fields)]
public class Variants : List<Variant>
{
    public Attributes Attributes { get; set; } = new();

    public void Add(VariantDto vm,string unit) => Add(new Variant(vm,unit));
}