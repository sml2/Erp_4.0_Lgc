﻿namespace ERP.Data.Products;
    public interface IProperty
    {
        public string Key { get; init; }
        public object? Value { get; set; }
    }
    public interface IProperty<ValueType>: IProperty
{
        public new ValueType? Value { get; set; }
    }
