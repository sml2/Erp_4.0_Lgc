﻿using ERP.Data.Products.Expand;

namespace ERP.Data.Products;

public partial class Product : PropertyBase
{
    public string CurrentVersion = "1.0.1";

    public string Version { get; set; } = "1.0.0";

    public Product()
    {
    }

    public Language Language { get; set; } = new();
    public Variants Variants { get; set; } = new();
    public Images? Images { get; set; } = new();

    // public Image? MainImage { get => Images.Main; }
    public Image? MainImage { get; set; } = new();

    // public Expands Expands { get; set; } = new();

    // public Dictionary<string, string?>? CodeCompare { get; set; } = null;

}