﻿using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using ERP.Enums;
using HotChocolate;

namespace ERP.Models.Product;

[Table("category")]
[Comment("产品管理_分类")]
public class CategoryModel : UserRelated
{
    [Comment("分类名称")]
    public string Name { get; set; } = string.Empty;

    [Comment("菜单级别")]
    public int Level { get; set; }

    [Comment("0 为一级菜单")]
    public int Pid { get; set; }

    [Comment("hash_code")]
    public int HashCode { get; set; }

    [Comment("分销分类 1否2是")]
    public Types Type { get; set; }

    [GraphQLName("CategoryTypes")]
    public enum Types
    {
        [Description("否")]
        TYPE_FALSE = 1,

        [Description("是")]
        TYPE_TRUE
    }
}


//-- ----------------------------
//-- Table structure for erp3_category
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_category`;
//CREATE TABLE `erp3_category`  (
//  `id` bigint unsigned NOT NULL,
//  `i_user_id` bigint unsigned NOT NULL,
//  `i_oem_id` bigint unsigned NOT NULL COMMENT 'oem_id',
//  `i_group_id` bigint unsigned NOT NULL COMMENT '分组id',
//  `i_company_id` bigint unsigned NOT NULL COMMENT '公司id',
//  `d_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '分类名称',
//  `t_level` int (0) NOT NULL DEFAULT 1 COMMENT '菜单级别',
//  `i_pid` bigint(0) NOT NULL DEFAULT 0 COMMENT '0 为一级菜单',
//  `d_hash_code` bigint unsigned NOT NULL,
//  `t_type` tinyint unsigned NOT NULL COMMENT '分销分类 1否2是',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `hash_code`(`d_hash_code`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `pid`(`i_pid`) USING BTREE,
// INDEX `type`(`t_type`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 11159 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '产品管理_分类' ROW_FORMAT = Dynamic;