using ERP.Enums.Product;
using ERP.Extensions;

namespace ERP.Data.Products.Feature;

public class Feature
{
    public Feature(FeatureEnum enumKey, object value)
    {
        EnumKey = enumKey;
        Value = value;
    }

    public Feature()
    {
    }

    public FeatureEnum EnumKey { get; set; }

    public string PropertyKey
    {
        get => EnumKey.GetName();
    }
    
    public string Key 
    {
        get => EnumKey.GetName().ToLower();
    }

    public string Name
    {
        get => EnumKey.GetDescription();
    }
    public object Value { get; set; }
}