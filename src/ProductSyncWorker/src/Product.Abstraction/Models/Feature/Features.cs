using ERP.Enums.Product;

namespace ERP.Data.Products.Feature;

public class Features : List<Feature>
{
    public void Add(FeatureEnum key, object value) => Add(new(key, value));
    public void Add(string key, object value) => Add(new(Enum.Parse<FeatureEnum>(key), value));
}