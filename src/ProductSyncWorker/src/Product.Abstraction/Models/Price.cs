using ERP.Attributes;

namespace ERP.Data.Products;

public class Price
{
    private Money _max { get; set; }
    
    public Money Max
    {
        get => _max;
        set => _max = value;
    }

    public void SetMax(decimal value, string unit) => _max = MoneyFactory.Instance.Create(value, unit);
    public void SetMax(decimal value) => _max = value;

    public decimal GetMax(string unit) => _max.To(unit);

    private Money _min { get; set; }

    public Money Min
    {
        get => _min;
        set => _min = value;
    }

    public void SetMin(decimal value, string unit) => _min = MoneyFactory.Instance.Create(value, unit);
    public void SetMin(decimal value) => _min = value;
    public decimal GetMin(string unit) => _min.To(unit);
}

public class Prices
{
    public Price Cost { get; set; } = new Price();
    public Price Sale { get; set; } = new Price();
}