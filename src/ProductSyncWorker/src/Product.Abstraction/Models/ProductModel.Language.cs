﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;
using ERP.Enums;
using ERP.Enums.Product;
using ERP.Extensions;

namespace ERP.Models.Product;

public partial class ProductModel : UserRelated
{
    public static readonly List<string> LanguageCode = new List<string>()
    {
        "zh", "es", "it", "fr", "de", "ja", "en", "nl",
        "vi", "pt", "ar", "th", "id", "ko", "ms", "tl",
        "pl", "fi", "ru", "ca", "lo", "iw", "ne", "sv",
        "ky", "da", "be", "tr", "hi", "is", "hu", "mn",
        "uk", "ga", "my", "la", "el", "bg", "ta", "lt",
        "no", "ro", "fa", "zh_tw"
    };


    //    //const FLAG_ZH = 1; 
    public bool Haszh() => IsFlagSet(Language, (long)Languages.ZH);
    public void Setzh(bool value) => SetFlag(ref _language, (long)Languages.ZH, value);

    //    //const FLAG_ES = 2;   
    public bool Hases() => IsFlagSet(Language, (long)Languages.ES);
    public void Setes(bool value) => SetFlag(ref _language, (long)Languages.ES, value);

    //    //const FLAG_IT = 4; 
    public bool Hasit() => IsFlagSet(Language, (long)Languages.IT);
    public void Setit(bool value) => SetFlag(ref _language, (long)Languages.IT, value);

    // //const FLAG_FR = 8;                                   // BIT #04 of $flags Has the value 8
    public bool Hasfr() => IsFlagSet(Language, (long)Languages.FR);
    public void setfr(bool value) => SetFlag(ref _language, (long)Languages.FR, value);

    // //const FLAG_DE = 16;                                // BIT #05 of $flags Has the value 16
    public bool Hasde() => IsFlagSet(Language, (long)Languages.DE);
    public void setde(bool value) => SetFlag(ref _language, (long)Languages.DE, value);

    // //const FLAG_JA = 32;                            // BIT #06 of $flags Has the value 32
    public bool Hasja() => IsFlagSet(Language, (long)Languages.JA);
    public void setja(bool value) => SetFlag(ref _language, (long)Languages.JA, value);

    // //const FLAG_EN = 64;                                // BIT #07 of $flags Has the value 64
    public bool Hasen() => IsFlagSet(Language, (long)Languages.EN);
    public void seten(bool value) => SetFlag(ref _language, (long)Languages.EN, value);

    // //const FLAG_NL = 128;                                 // BIT #08 of $flags Has the value 128
    public bool Hasnl() => IsFlagSet(Language, (long)Languages.NL);
    public void setnl(bool value) => SetFlag(ref _language, (long)Languages.NL, value);

    // //const FLAG_VI = 256;                                  // BIT #09 of $flags Has the value 256
    public bool Hasvi() => IsFlagSet(Language, (long)Languages.VI);
    public void setvi(bool value) => SetFlag(ref _language, (long)Languages.VI, value);

    // //const FLAG_PT = 512;                               // BIT #10 of $flags Has the value 512
    public bool Haspt() => IsFlagSet(Language, (long)Languages.PT);
    public void setpt(bool value) => SetFlag(ref _language, (long)Languages.PT, value);

    // //const FLAG_AR = 1024;                               // BIT #11 of $flags Has the value 1024
    public bool Hasar() => IsFlagSet(Language, (long)Languages.AR);
    public void setar(bool value) => SetFlag(ref _language, (long)Languages.AR, value);

    // //const FLAG_TH = 2048;                               // BIT #12 of $flags Has the value 2048
    public bool Hasth() => IsFlagSet(Language, (long)Languages.TH);
    public void setth(bool value) => SetFlag(ref _language, (long)Languages.TH, value);

    // //const FLAG_ID = 4096;                               // BIT #13 of $flags Has the value 4096
    public bool Hasid() => IsFlagSet(Language, (long)Languages.ID);
    public void setid(bool value) => SetFlag(ref _language, (long)Languages.ID, value);

    // //const FLAG_KO = 8192;                               // BIT #14 of $flags Has the value 8192
    public bool Hasko() => IsFlagSet(Language, (long)Languages.KO);
    public void setko(bool value) => SetFlag(ref _language, (long)Languages.KO, value);

    // //const FLAG_MS = 16384;                               // BIT #15 of $flags Has the value 16384
    public bool Hasms() => IsFlagSet(Language, (long)Languages.MS);
    public void setms(bool value) => SetFlag(ref _language, (long)Languages.MS, value);

    // //const FLAG_TL = 32768;                               // BIT #16 of $flags Has the value 32768‬
    public bool Hastl() => IsFlagSet(Language, (long)Languages.TL);
    public void settl(bool value) => SetFlag(ref _language, (long)Languages.TL, value);

    // //const FLAG_PL = 65536;                               // BIT #17 of $flags Has the value 65536
    public bool Haspl() => IsFlagSet(Language, (long)Languages.PL);
    public void setpl(bool value) => SetFlag(ref _language, (long)Languages.PL, value);

    // //const FLAG_FI = 131072;                               // BIT #18 of $flags Has the value 131072
    public bool Hasfi() => IsFlagSet(Language, (long)Languages.FI);
    public void setfi(bool value) => SetFlag(ref _language, (long)Languages.FI, value);

    // //const FLAG_RU = 262144;                               // BIT #19 of $flags Has the value 262144
    public bool Hasru() => IsFlagSet(Language, (long)Languages.RU);
    public void setru(bool value) => SetFlag(ref _language, (long)Languages.RU, value);

    // //const FLAG_CA = 524288;                               // BIT #20 of $flags Has the value 524288
    public bool Hasca() => IsFlagSet(Language, (long)Languages.CA);
    public void setca(bool value) => SetFlag(ref _language, (long)Languages.CA, value);

    // //const FLAG_LO = 1048576;                               // BIT #21 of $flags Has the value 1048576
    public bool Haslo() => IsFlagSet(Language, (long)Languages.LO);
    public void setlo(bool value) => SetFlag(ref _language, (long)Languages.LO, value);

    // //const FLAG_IW = 2097152;                               // BIT #22 of $flags Has the value 2097152
    public bool Hasiw() => IsFlagSet(Language, (long)Languages.IW);
    public void setiw(bool value) => SetFlag(ref _language, (long)Languages.IW, value);

    // //const FLAG_NE = 4194304;                               // BIT #23 of $flags Has the value 4194304
    public bool Hasne() => IsFlagSet(Language, (long)Languages.NE);
    public void setne(bool value) => SetFlag(ref _language, (long)Languages.NE, value);

    // //const FLAG_SV = 8388608;                               // BIT #24 of $flags Has the value 8388608
    public bool Hassv() => IsFlagSet(Language, (long)Languages.SV);
    public void setsv(bool value) => SetFlag(ref _language, (long)Languages.SV, value);

    // //const FLAG_KY = 16777216;                               // BIT #25 of $flags Has the value 16777216
    public bool Hasky() => IsFlagSet(Language, (long)Languages.KY);
    public void setky(bool value) => SetFlag(ref _language, (long)Languages.KY, value);

    // //const FLAG_DA = 33554432;                               // BIT #26 of $flags Has the value 33554432
    public bool Hasda() => IsFlagSet(Language, (long)Languages.DA);
    public void setda(bool value) => SetFlag(ref _language, (long)Languages.DA, value);

    // //const FLAG_BE = 67108864;                               // BIT #27 of $flags Has the value 67108864
    public bool Hasbe() => IsFlagSet(Language, (long)Languages.BE);
    public void setbe(bool value) => SetFlag(ref _language, (long)Languages.BE, value);

    // //const FLAG_TR = 134217728;                               // BIT #28 of $flags Has the value 134217728
    public bool Hastr() => IsFlagSet(Language, (long)Languages.TR);
    public void settr(bool value) => SetFlag(ref _language, (long)Languages.TR, value);

    // //const FLAG_HI = 268435456;                               // BIT #29 of $flags Has the value 268435456
    public bool Hashi() => IsFlagSet(Language, (long)Languages.HI);
    public void sethi(bool value) => SetFlag(ref _language, (long)Languages.HI, value);

    // //const FLAG_IS = 536870912;                               // BIT #30 of $flags Has the value 536870912
    public bool Hasis() => IsFlagSet(Language, (long)Languages.IS);
    public void setis(bool value) => SetFlag(ref _language, (long)Languages.IS, value);

    // //const FLAG_HU = 1073741824;                               // BIT #31 of $flags Has the value 1073741824
    public bool Hashu() => IsFlagSet(Language, (long)Languages.HU);
    public void sethu(bool value) => SetFlag(ref _language, (long)Languages.HU, value);

    //const FLAG_MN = 2147483648;                               // BIT #32 of $flags Has the value 2147483648
    public bool Hasmn() => IsFlagSet(Language, (long)Languages.MN);
    public void setmn(bool value) => SetFlag(ref _language, (long)Languages.MN, value);

    //const FLAG_UK = 4294967296;                               // BIT #33 of $flags Has the value 4294967296
    public bool Hasuk() => IsFlagSet(Language, (long)Languages.UK);
    public void setuk(bool value) => SetFlag(ref _language, (long)Languages.UK, value);

    //const FLAG_GA = 8589934592;                               // BIT #34 of $flags Has the value 8589934592
    public bool Hasga() => IsFlagSet(Language, (long)Languages.GA);
    public void setga(bool value) => SetFlag(ref _language, (long)Languages.GA, value);

    //const FLAG_MY = 17179869184;                               // BIT #35 of $flags Has the value 17179869184
    public bool Hasmy() => IsFlagSet(Language, (long)Languages.MY);
    public void setmy(bool value) => SetFlag(ref _language, (long)Languages.MY, value);

    //const FLAG_LA = 34359738368;                               // BIT #36 of $flags Has the value 34359738368
    public bool Hasla() => IsFlagSet(Language, (long)Languages.LA);
    public void setla(bool value) => SetFlag(ref _language, (long)Languages.LA, value);

    //const FLAG_EL = 68719476736;                               // BIT #37 of $flags Has the value 68719476736
    public bool Hasel() => IsFlagSet(Language, (long)Languages.EL);
    public void setel(bool value) => SetFlag(ref _language, (long)Languages.EL, value);

    //const FLAG_BG = 137438953472;                               // BIT #38 of $flags Has the value 137438953472
    public bool Hasbg() => IsFlagSet(Language, (long)Languages.BG);
    public void setbg(bool value) => SetFlag(ref _language, (long)Languages.BG, value);

    //const FLAG_TA = 274877906944;                               // BIT #39 of $flags Has the value 274877906944
    public bool Hasta() => IsFlagSet(Language, (long)Languages.TA);
    public void setta(bool value) => SetFlag(ref _language, (long)Languages.TA, value);

    //const FLAG_LT = 549755813888;                               // BIT #40 of $flags Has the value 549755813888
    public bool Haslt() => IsFlagSet(Language, (long)Languages.LT);
    public void setlt(bool value) => SetFlag(ref _language, (long)Languages.LT, value);

    //const FLAG_NO = 1099511627776;                               // BIT #41 of $flags Has the value 1099511627776
    public bool Hasno() => IsFlagSet(Language, (long)Languages.NO);
    public void setno(bool value) => SetFlag(ref _language, (long)Languages.NO, value);

    //const FLAG_RO = 2199023255552;                               // BIT #42 of $flags Has the value 2199023255552
    public bool Hasro() => IsFlagSet(Language, (long)Languages.RO);
    public void setro(bool value) => SetFlag(ref _language, (long)Languages.RO, value);

    //const FLAG_FA = 4398046511104;                               // BIT #43 of $flags Has the value 4398046511104
    public bool Hasfa() => IsFlagSet(Language, (long)Languages.FA);

    public void setfa(bool value) => SetFlag(ref _language, (long)Languages.FA, value);

//-----------------------------------44-48
    //const FLAG_ZH_TW = 8796093022208;                               // BIT #44 of $flags Has the value 8796093022208
    public bool HaszhTw() => IsFlagSet(Language, (long)Languages.ZH_TW);
    public void setzhTw(bool value) => SetFlag(ref _language, (long)Languages.ZH_TW, value);

    protected const int BIT_UILANGUAGE = 54; //0000111111000000000000000000000000000000000000000000000000000000

    protected const long
        MASK_UILANGUAGE = 1134907106097364992; // BIT #55-50 of $flags has the value 1134907106097364992

    public long? GetUILanguage() => GetInt(Language, MASK_UILANGUAGE, BIT_UILANGUAGE);

    public void SetUILanguage(int value) => SetInt(ref _language, value, MASK_UILANGUAGE, BIT_UILANGUAGE);


    protected long? GetInt(long value, long mask, int bit) => (value & mask) >> bit;


    protected void SetInt(ref long column, long value, long mask, int bit) => column = (column & (~(long)mask)) ^ (value << (int)bit);

    public void SetLanguage(Languages flag, bool value) => SetFlag(ref _language, (long)flag, value);


    protected void SetFlag(ref long column, long flag, bool value)
    {
        if (value)
        {
            column |= flag;
        }
        else
        {
            column &= ~flag;
        }
    }

    protected bool IsFlagSet(long column, long flag) => (column & flag) == flag;
}