﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Enums.Products;
using ERP.Data.Products;
using ERP.Enums;
using ERP.Enums.Product;
using ERP.Enums.Rule.Config.Internal;
using ERP.Extensions;
using ERP.Models.View.Products.Product;
using HotChocolate;
using Microsoft.AspNetCore.Http;
using ProductItemStruct = ERP.Data.Products.Product;
using Newtonsoft.Json;

namespace ERP.Models.Product;

[Table("product")]
[Comment("产品管理_产品列表'")]
[Index(nameof(HashCode))]
public partial class ProductModel : UserRelated
{
    public ProductModel()
    {
    }


    public ProductModel(int userID, int groupID, int companyID, int oemID)
    {
        UserID = userID;
        GroupID = groupID;
        CompanyID = companyID;
        OEMID = oemID;
    }

    public ProductModel(ISession session)
    {
        UserID = session.GetUserID();
        GroupID = session.GetGroupID();
        CompanyID = session.GetCompanyID();
        OEMID = session.GetOEMID();
    }

    /// <summary>
    /// 分享导入使用
    /// </summary>
    /// <param name="session"></param>
    /// <param name="model"></param>
    public ProductModel(ISession session, ProductModel model)
    {
        UserID = session.GetUserID();
        GroupID = session.GetGroupID();
        CompanyID = session.GetCompanyID();
        OEMID = session.GetOEMID();
        Title = model.Title;
        HashCode = model.HashCode;
        Source = model.Source;
        PlatformId = model.PlatformId;
        CategoryId = model.CategoryId;
        SubCategoryId = model.SubCategoryId;
        Quantity = model.Quantity;
        Prices = model.Prices;
        Pid = model.Pid;
        Sku = model.Sku;
        PidHashCode = model.PidHashCode;
        Type = (long)Types.Default;
        Language = model.Language;
        // Images = model.Images;
        MainImage = model.MainImage;
        ImagesSize = model.ImagesSize;
        IsImageAsync = model.IsImageAsync;
        FilesSize = 0;
    }

    /// <summary>
    /// B2C克隆使用或针对某一种语言进行克隆
    /// </summary>
    /// <param name="session"></param>
    /// <param name="model"></param>
    public ProductModel(ISession session, ProductModel model, ProductItemStruct itemStruct)
    {
        UserID = session.GetUserID();
        GroupID = session.GetGroupID();
        CompanyID = session.GetCompanyID();
        OEMID = session.GetOEMID();
        Title = itemStruct.GetTitle()!;
        HashCode = Helpers.CreateHashCode(Title);
        Source = model.Source;
        PlatformId = model.PlatformId;
        CategoryId = model.CategoryId;
        SubCategoryId = model.SubCategoryId;
        Quantity = model.Quantity;
        Prices = model.Prices;
        Pid = model.Pid;
        PidHashCode = model.PidHashCode;
        Type = (long)Types.Default;
        Language = (long)Types.Default;
        // Images = model.Images;
        MainImage = model.MainImage;
        ImagesSize = model.ImagesSize;
        IsImageAsync = model.IsImageAsync;
        FilesSize = 0;
    }

    [Comment("标题")]
    public string Title { get; set; }

    [Comment("标题的hashCode"), GraphQLIgnore]
    public ulong HashCode { get; set; }


    [Comment("来源")]
    public string? Source { get; set; }

    [Comment("平台")]
    public int? PlatformId { get; set; }

    [Comment("主图"), Column(TypeName = "json")]
    public string? MainImage { get; set; }


    [NotMapped]
    public Image? MainImageObj =>
        this.MainImage is not null ? JsonConvert.DeserializeObject<Image>(this.MainImage) : new Image();

    [Comment("是否存在远程图")]
    public bool IsImageAsync { get; set; }

    [Comment("产品图片大小")]
    public long ImagesSize { get; set; }

    [Comment("任务id")]
    public int? TaskId { get; set; }

    [ForeignKey(nameof(TaskId))]
    public TaskModel? TaskModel { get; set; }

    [Comment("分类id")]
    public int? CategoryId { get; set; }

    [ForeignKey(nameof(CategoryId))]
    public CategoryModel? CategoryModel { get; set; }

    [Comment("子分类id")]
    public int? SubCategoryId { get; set; }

    [ForeignKey(nameof(SubCategoryId))]
    public CategoryModel? SubCategoryModel { get; set; }

    [Comment("分销分类id")]
    public int? DistributionCategoryId { get; set; }

    [ForeignKey(nameof(DistributionCategoryId))]
    public CategoryModel? DistributionCategoryModel { get; set; }

    [Comment("分销子分类id")]
    public int? DistributionSubCategoryId { get; set; }

    [ForeignKey(nameof(DistributionSubCategoryId))]
    public CategoryModel? DistributionSubCategoryModel { get; set; }

    [Comment("库存")]
    public int? Quantity { get; set; }

    [Comment("用户层级的重复次数")]
    public int UserLevelRepeatNum { get; set; } = 0;

    private long _type = (long)Types.Default;

    [Comment("类型")]
    public long Type
    {
        get => _type;
        set => _type = value;
    }

    [Comment("UI冗余价格,{sale:{min:0,max:100},cost:{min:m,max:n}}->m-n,n"), Column(TypeName = "json")]
    //Dictionary<string, Price>
    public string? Prices { get; set; }

    [Comment("Pid")]
    public string? Pid { get; set; } = string.Empty;

    [Comment("Sku")]
    public string? Sku { get; set; } = string.Empty;

    [Comment("采集产品唯一标识Hashcode")]
    public string? PidHashCode { get; set; }


    [Comment("货币单位(采集产品单位)")]
    public string Coin { get; set; } = string.Empty;

    [Comment("汇率(采集产品当时汇率)")]
    public string Rate { get; set; } = string.Empty;

    [Comment("当前版本"), Obsolete("废弃")]
    public long? CurrentVersion { get; set; } = 0;

    [Comment("历史版本"), Obsolete("废弃")]
    //List<long>
    public string? HistoryVersion { get; set; } = string.Empty;

    [Comment("当前版本json文件路径"), Obsolete("废弃")]
    public string? FilePath { get; set; }


    [Comment("当前版本json文件路径hashcode"), Obsolete("废弃"), GraphQLIgnore]
    public ulong? FilePathHashCode { get; set; }

    [Comment("产品文件大小")]
    public long FilesSize { get; set; }

    [Comment("code对应关系")]
    public string? CodeCompare { get; set; } = string.Empty;

    [NotMapped]
    public Dictionary<string, string?>? CodeCompareDic => this.CodeCompare is not null
        ? JsonConvert.DeserializeObject<Dictionary<string, string?>>(this.CodeCompare)
        : null;

    private long _language = (long)Types.Default;


    public long Language
    {
        get => _language;
        set => _language = value;
    }

    [NotMapped]
    public Languages UiLanguage
    {
        get => GetUILanguage() > 0
            ? Enum.Parse<Languages>(ProductModel.LanguageCode[(int)GetUILanguage() - 1], true)
            : Languages.Default;
    }

    [NotMapped]
    public int UiLanguageId => UiLanguage == Languages.Default ? 0 : (int)Math.Log2((long)UiLanguage) + 1;

    [NotMapped]
    public long? Audit => GetAudit();
    // public long? Audit => (Type & (long?)Types.MASK_AUDIT) >> (int?)Types.Audit;

    [NotMapped]
    public object? AuditType { get; set; }

    [NotMapped]
    public object? AuditKey { get; set; }

    [Comment("审核原因(我们提供或客户自定义)")]
    public string? Reason { get; set; }

    public string? Images { get; set; }

    [Comment("扩展字段")]
    public string? Feature { get; set; }

    [NotMapped]
    public Images? Gallery =>
        this.Images is not null ? JsonConvert.DeserializeObject<Images>(this.Images) : new Images();


    // 因产品存在查看范围 如果以公司为维度计算重复次数，存在误差
    // public int? CompanyLevelRepeatNum { get; set; }


    // [Comment("与亚马逊市场亚马逊目录中的商品相关联的维度数组。")]
    // public string Dimensions { get; set; } = "[]";
    //
    //
    // [Comment("属性")]
    // public string Attributes { get; set; } = "[]";
    //
    //
    // [Comment("变体索引")]
    // public string Variants { get; set; } = "[]";
    //
    //
    // [Comment("列表图片edit,download,makemainpic*,delete")]
    // public string Images { get; set; } = string.Empty;


    // [NotMapped]
    // public class LanguagePackage
    // {
    //     public string? Name { get; set; }
    //     public string? KeyWords { get; set; }
    //     public string? Sketch { get; set; }
    //     public string? Description { get; set; }
    // }


    // #region ProductInfo
    //
    // public uint Length;
    // public Dictionary<Compresses, uint> CompressInformation = new();
    // public Compresses Compress;
    //
    // public enum Compresses
    // {
    //     None,
    //     Gzip,
    //     br
    // }
    //
    // private static readonly Version DefaultSerializeVersion = new(1, 0, 0, 0);
    // public Version SerializeVersion = DefaultSerializeVersion;
    //
    // [NotMapped]
    // public Data.Products.Product? Entity
    // {
    //     get; set;
    // }
    // #endregion

    /// <summary>
    /// 使用改价模板修改产品价格
    /// </summary>
    /// <param name="this">产品模型</param>
    /// <param name="productStruct"></param>
    /// <param name="params">改价模板参数</param>
    /// <param name="unit">货币单位</param>
    public void ChangePrices(ProductItemStruct productStruct, List<ProductPriceParams> @params, string unit)
    {
        int allQuantity = 0;
        foreach (var item in @params)
        {
            foreach (var i in productStruct.Variants)
            {
                switch (item.Type)
                {
                    case SetProductPriceTypeEnum.Cost:
                        var cost = i.Cost.To(unit);
                        var newCost = Detection(item, cost);
                        i.Cost = MoneyFactory.Instance.Create(newCost, unit);
                        break;
                    case SetProductPriceTypeEnum.Sale:
                        var sale = i.Sale.To(unit);
                        var newSale = Detection(item, sale);
                        i.Sale = MoneyFactory.Instance.Create(newSale, unit);
                        break;
                    case SetProductPriceTypeEnum.Quantity:
                        i.Quantity = (int)Detection(item, (decimal)i.Quantity);
                        allQuantity += i.Quantity;
                        break;
                }
            }
        }

        var prices = productStruct.GetPrices() ?? new Prices();
        prices.Cost.Min = productStruct.Variants.Min(v => v.Cost.Value);
        prices.Cost.Max = productStruct.Variants.Max(v => v.Cost.Value);
        prices.Sale.Min = productStruct.Variants.Min(v => v.Sale.Value);
        prices.Sale.Max = productStruct.Variants.Max(v => v.Sale.Value);
        productStruct.SetPrices(prices);
        this.Prices = JsonConvert.SerializeObject(prices);

        productStruct.SetQuantity(allQuantity);
        this.Quantity = allQuantity;

        // 改价模板修改单个价格
        static decimal Detection(ProductPriceParams data, decimal number)
        {
            decimal num = 0;
            if (data.Mode == SetProductPriceModeEnum.Add)
            {
                num = data.NumberType == SetProductPriceNumberTypeEnum.Number ? data.Value : number * data.Value / 100;
                num += number;
            }
            else if (data.Mode == SetProductPriceModeEnum.Reduce)
            {
                num = data.NumberType == SetProductPriceNumberTypeEnum.Number ? data.Value : number * data.Value / 100;
                num = number - num;
            }
            else
            {
                num = data.Value;
            }

            return num < 0 ? 0 : num;
        }
    }
}

public static class ProductModelDbSetExtension
{
    public static IQueryable<ProductModel> WhereRange(this IQueryable<ProductModel> query, DataRange_2b range,
        int companyId, int groupId, int userId) =>
        range switch
        {
            DataRange_2b.Company => query.Where(m => m.CompanyID == companyId),
            DataRange_2b.Group => query.Where(m => m.CompanyID == companyId && m.GroupID == groupId),
            _ => query.Where(m => m.CompanyID == companyId && m.UserID == userId),
        };

    /// <summary>
    /// 是否上传中
    /// </summary>
    /// <param name="query"></param>
    /// <returns></returns>
    public static IQueryable<ProductModel> IsOnGoing(this IQueryable<ProductModel> query, bool state) =>
        state
            ? query.Where(m => (m.Type & (long)Types.OnGoing) == (long)Types.OnGoing)
            : query.Where(m => (m.Type & (long)Types.OnGoing) != (long)Types.OnGoing);

    /// <summary>
    /// 是否是导出
    /// </summary>
    /// <param name="query"></param>
    /// <param name="state"></param>
    /// <returns></returns>
    public static IQueryable<ProductModel> IsExport(this IQueryable<ProductModel> query, bool state) =>
        state
            ? query.Where(m => (m.Type & (long)Types.Export) == (long)Types.Export)
            : query.Where(m => (m.Type & (long)Types.Export) != (long)Types.Export);

    /// <summary>
    /// 是否是B2C
    /// </summary>
    /// <param name="query"></param>
    /// <param name="state"></param>
    /// <returns></returns>
    public static IQueryable<ProductModel> IsB2C(this IQueryable<ProductModel> query, bool state) =>
        state
            ? query.Where(m => (m.Type & (long)Types.B2C) == (long)Types.B2C)
            : query.Where(m => (m.Type & (long)Types.B2C) != (long)Types.B2C);

    /// <summary>
    /// 是否是上传
    /// </summary>
    /// <param name="query"></param>
    /// <param name="state"></param>
    /// <returns></returns>
    public static IQueryable<ProductModel> IsUpload(this IQueryable<ProductModel> query, bool state) =>
        state
            ? query.Where(m => (m.Type & (long)Types.Uplaod) == (long)Types.Uplaod)
            : query.Where(m => (m.Type & (long)Types.Uplaod) != (long)Types.Uplaod);

    /// <summary>
    /// 是否回收站
    /// </summary>
    /// <param name="query"></param>
    /// <param name="state"></param>
    /// <returns></returns>
    public static IQueryable<ProductModel> IsRecycle(this IQueryable<ProductModel> query, bool state) =>
        state
            ? query.Where(m => (m.Type & (long)Types.Recycle) == (long)Types.Recycle)
            : query.Where(m => (m.Type & (long)Types.Recycle) != (long)Types.Recycle);

    /// <summary>
    /// 是否分享
    /// </summary>
    /// <param name="query"></param>
    /// <param name="state"></param>
    /// <returns></returns>
    public static IQueryable<ProductModel> IsShare(this IQueryable<ProductModel> query, bool state) =>
        state
            ? query.Where(m => (m.Type & (long)Types.Share) == (long)Types.Share)
            : query.Where(m => (m.Type & (long)Types.Share) != (long)Types.Share);

    /// <summary>
    /// 是否发布过B2C
    /// </summary>
    /// <param name="query"></param>
    /// <param name="state"></param>
    /// <returns></returns>
    public static IQueryable<ProductModel> IsB2cRelease(this IQueryable<ProductModel> query, bool state) => state
        ? query.Where(m => (m.Type & (long)Types.B2CRelease) == (long)Types.B2CRelease)
        : query.Where(m => (m.Type & (long)Types.B2CRelease) != (long)Types.B2CRelease);
    public static IQueryable<ProductModel> Audit(this IQueryable<ProductModel> query, AuditStateEnum auditState)
    {
        var audited = ProductModel.AUDIT_AUDITED;

        switch (auditState)
        {
            case AuditStateEnum.NotReviewed:
                query = (IQueryable<ProductModel>)query.AsEnumerable().Where(m => m.Audit < audited);
                break;
            case AuditStateEnum.Audited:
                query = (IQueryable<ProductModel>)query.AsEnumerable().Where(m => m.Audit >= audited);
                break;
            case AuditStateEnum.DidNotPass:
                query = (IQueryable<ProductModel>)query.AsEnumerable().Where(m => m.Audit > audited);
                break;
        }

        return query;
    }
}