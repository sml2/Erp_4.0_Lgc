﻿using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.Models.Product;

[Comment("产品管理_采集任务")]
public class TaskModel : UserRelated
{
    [Comment("采集任务名称")] public string Name { get; set; } = string.Empty;
    [Comment("平台名称")] public string PlatformName { get; set; } = string.Empty;
    [Comment("唯一标识")] public string HashCode { get; set; } = string.Empty;
    [Comment("平台id")] public int PlatformId { get; set; }

    [Comment("一级分类id")] public int? FirstCategoryId { get; set; } = 0;
    [Comment("二级分类id")] public int? SecondCategoryId { get; set; } = 0;

    [Comment("删除")]
    public bool IsDel { get; set; } = false;
}

//-- ----------------------------
//-- Table structure for erp3_task
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_task`;
//CREATE TABLE `erp3_task`  (
//  `id` bigint unsigned NOT NULL,
//  `d_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '采集任务名称',
//  `d_platform_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '平台名称',
//  `i_platform_id` bigint unsigned NOT NULL COMMENT '平台id',
//  `i_oem_id` bigint unsigned NOT NULL COMMENT 'oem_id',
//  `i_user_id` bigint unsigned NOT NULL COMMENT '用户id',
//  `i_group_id` bigint unsigned NOT NULL COMMENT '分组id',
//  `i_company_id` bigint unsigned NOT NULL COMMENT '公司id',
//  `i_first_category_id` bigint unsigned NULL COMMENT '一级分类id',
//  `i_second_category_id` bigint unsigned NULL COMMENT '二级分类id',
//  `d_hash_code` bigint unsigned NOT NULL COMMENT '唯一标识',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `platform_id`(`i_platform_id`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 139131 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '产品管理_采集任务' ROW_FORMAT = Dynamic;