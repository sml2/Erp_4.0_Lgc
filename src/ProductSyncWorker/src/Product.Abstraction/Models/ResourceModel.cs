﻿using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using ERP.Data.Products;

namespace ERP.Models.DB.Product;

[Index(nameof(OEMID), nameof(UserID))]
[Index(nameof(ProductId))]
public class ResourceModel : UserModel
{
    public ResourceModel()
    {
    }
    
    public ResourceModel(Image m ,int productId)
    {
    }
    
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public new int ID { get; set; }

    public ResourceModel(TempImagesModel m, int productId)
    {
        ID = m.ID;
        Path = m.Path;
        StorageId = m.OssID;
        Type = m.Type;
        Size = m.Size;
        OEMID = m.OEMID;
        CompanyID = m.CompanyID;
        GroupID = m.GroupID;
        UserID = m.UserID;
        ProductId = productId;
    }

    [Comment("图片被产品使用时需填充该字段")]
    public int? ProductId { get; set; }

    [Comment("路径")]
    public string Path { get; set; }

    [Comment("大小（kb）")]
    public long Size { get; set; }

    [Comment("云存储id")]
    public int StorageId { get; set; }

    [Comment("1 oss地址 2 远程地址")]
    public Types Type { get; set; }

    public enum Types
    {
        [Description("oss地址")] Storage = 1,
        [Description("远程地址")] Network
    }
}

//-- ----------------------------
//-- Table structure for erp3_resource
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_resource`;
//CREATE TABLE `erp3_resource`  (
//  `id` bigint unsigned NOT NULL,
//  `d_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'oss路径',
//  `d_size` bigint unsigned NOT NULL COMMENT '图片大小',
//  `c_reuse` int unsigned NOT NULL COMMENT '资源复用次数',
//  `i_user_id` bigint unsigned NOT NULL COMMENT '用户id',
//  `i_oss_id` bigint unsigned NOT NULL COMMENT 'oss资源',
//  `i_oem_id` bigint unsigned NOT NULL COMMENT 'oem_id',
//  `t_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '类型1本地服务器2远程地址',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `oss_id`(`i_oss_id`) USING BTREE,
// INDEX `reuse`(`c_reuse`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 164426944 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户正式资源表' ROW_FORMAT = Dynamic;