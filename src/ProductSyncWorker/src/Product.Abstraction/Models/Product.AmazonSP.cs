﻿namespace ERP.Data.Products;
public partial class Product :PropertyBase
{
    public Dictionary<string,string> Ext { get; set; }
    public Money? ListPrice { get; set; }

    public Money? ShipPrice { get; set; }

    public int? PointsNumber { get; set; }

    public Money? PointPrice { get; set; } 
    
    public string? productType { get; set; }

    public int? storeId { get; set; }
   
    /// <summary>
    /// 创建列表项的日期，采用 ISO 8601 格式
    /// </summary>
    public DateTime? createdDate { get; set; }

    /// <summary>
    /// 商品信息的最后更新日期，采用 ISO 8601 格式
    /// </summary>
    public DateTime? lastUpdatedDate { get; set; }

    public string productID { get; set; }
    public ulong HashCode { get; set; }

    public string Brand { get; set; }

    public string Color { get; set; }

    public string Size { get; set; }

    public string ImageUrl { get; set; }

    public string LanguageTag { get; set; }

    //public Dictionary<string,string> GetKeyValue(string json)
    //{
    //    Dictionary<string, string> attributes = new Dictionary<string, string>();
    //    string attribute = json;
    //    if (attribute.Contains(']'))
    //    {
    //        string key = "", value = "";
    //        foreach (var attr in attribute.Split(']'))
    //        {
    //            key = "";
    //            value = "";
    //            var start = attr.IndexOf("\"");
    //            var end = attr.IndexOf("\": [");
    //            if (start != -1 && end != -1)
    //            {
    //                key = attr.Substring(start+1, end - start-1);
    //            }
    //            //获取value
    //            if (attr.Contains("\"value\": \""))
    //            {
    //                var val = attr.Split("\"value\": \"")[1];
    //                var v = val.IndexOf(',');
    //                value = val.Substring(0, v-1);
    //            }
    //            if (key != "" && value != "")
    //            {
    //                attributes.Add(key, value);
    //            }
    //        }            
    //    }
    //    return attributes;
    //}

    //public List<string> GetValue(List<string> jsonValue)
    //{
    //    List<string> strings = new List<string>();
    //    if (jsonValue.IsNotNull () && jsonValue.Count>0)
    //    {
    //        foreach(var item in jsonValue)
    //        {
    //            string value = "";
    //            string attribute = item;
    //            //获取value
    //            if (attribute.Contains("\"value\": \""))
    //            {
    //                var val = attribute.Split("\"value\": \"")[1];
    //                var v = val.IndexOf(',');
    //                value = val.Substring(0, v);
    //            }
    //            if(value!="")
    //            {
    //                strings.Add(value);
    //            }
    //        }            
    //    }        
    //    return strings;
    //}
}