﻿using Common.Enums.Products;
using ERP.Enums;
using ERP.Enums.Product;
using Ryu.Data;

namespace ERP.Data.Products;

using Newtonsoft.Json;

public abstract class PropertyBase
{
    public Properties Properties { get; set; } = new();
    public PropertiesForLang PropertiesForLang { get; set; } = new();

    public string Pid
    {
        get => Properties.Pid!;
        set => Properties.Pid = value;
    }

    public string? Title
    {
        get => GetTitle();
        set => SetTitle(value);
    }

    public string? Source
    {
        get => Properties.Source;
        set => Properties.Source = value;
    }

    public int Quantity
    {
        get => Properties.Quantity;
        set => Properties.Quantity = value;
    }

    /// <summary>
    /// Sku
    /// </summary>
    public string? Sku
    {
        get => Properties.Sku;
        set => Properties.Sku = value;
    }

    /// <summary>
    /// Asin
    /// </summary>
    public string? Asin
    {
        get => Properties.Asin;
        set => Properties.Asin = value;
    }

    /// <summary>
    /// Sid
    /// </summary>
    public string? Sid
    {
        get => Properties.Sid;
        set => Properties.Sid = value;
    }

    /// <summary>
    /// Code
    /// </summary>
    public string? Code
    {
        get => Properties.Code;
        set => Properties.Code = value;
    }

    /// <summary>
    /// Code
    /// </summary>
    public ProductUploadEnum? UploadState
    {
        get => Properties.UploadState;
        set => Properties.UploadState = value;
    }


    public Platforms? Platform
    {
        get => Properties.Platform;
        set => Properties.Platform = value;
    }

    public string? Carrie
    {
        get => Properties.Carrie;
        set => Properties.Carrie = value;
    }

    public Money? Price
    {
        get => Properties.Price;
        set => Properties.Price = value;
    }

    // public string? Sku
    // {
    //     get => Properties.Sku;
    //     set => Properties.Sku = value;
    // }

    public string? GetTitle() => Properties.Title;
    public void SetTitle(string value) => Properties.Title = value;

    public string? GetKeyword() => Properties.Keyword;
    public void SetKeyword(string? value) => Properties.Keyword = value;

    public string? GetDescription() => Properties.Description;
    public void SetDescription(string? value) => Properties.Description = value;

    public List<string>? GetSketches() => Properties.Sketches;
    public void SetSketches(List<string>? value) => Properties.Sketches = value;

    public string? GetSku() => Properties.Sku;
    public void SetSku(string? value) => Properties.Sku = value;

    public string? GetCode() => Properties.Code;
    public void SetCode(string? value) => Properties.Code = value;

    public string? GetAsin() => Properties.Asin;
    public void SetAsin(string? value) => Properties.Asin = value;


    // public string GetTitle(Enums.Languages lang) => PropertiesForLang[lang].Title;
    // public void SetTitle(Enums.Languages lang, string value) => PropertiesForLang[lang].Title = value;

    // public string? GetKeyword(Enums.Languages lang) => PropertiesForLang[lang].Keyword;
    // public void SetKeyword(Enums.Languages lang, string? value) => PropertiesForLang[lang].Keyword = value;

    // public string? GetDescription(Enums.Languages lang) => PropertiesForLang[lang].Description;
    // public void SetDescription(Enums.Languages lang, string? value) => PropertiesForLang[lang].Description = value;

    // public List<string>? GetSketches(Enums.Languages lang) => PropertiesForLang[lang].Sketches;
    // public void SetSketches(Enums.Languages lang, List<string>? value) => PropertiesForLang[lang].Sketches = value;

    public string? GetSource() => Properties.Source;
    public void SetSource(string? value) => Properties.Source = value;

    [Obsolete("废弃", true)]
    public int? GetCategoryId() => Properties.CategoryId;

    [Obsolete("废弃", true)]
    public void SetCategoryId(int? value) => Properties.CategoryId = value;

    [Obsolete("废弃", true)]
    public int? GetSubCategoryId() => Properties.SubCategoryId;

    [Obsolete("废弃", true)]
    public void SetSubCategoryId(int? value) => Properties.SubCategoryId = value;

    public int GetQuantity() => Properties.Quantity;
    public void SetQuantity(int value) => Properties.Quantity = value;

    public string GetPrice() => JsonConvert.SerializeObject(Price);
    public void SetPrice(string priceInfo) => Properties.SetPrice(priceInfo);
    public void SetPrice(string carrie, double price) => Properties.SetPrice(carrie, price);


    public string? GetCarrie() => Properties.Carrie;
    public string SetCarrie(string value) => Properties.Carrie = value;

    public void SetPrices(Prices value) => Properties.Prices = value;
    public Prices? GetPrices() => Properties.Prices;


    public string? GetPid() => Properties.Pid;
    public void SetPid(string value) => Properties.Pid = value;

    public ulong? GetPidHashCode() => Properties.PidHashCode;

    public void SetPidHashCode() => Properties.PidHashCode =
        Properties.Pid.IsNotNull() ? Helpers.CreateHashCode(Properties.Pid) : null;

    public void SetTypesFlag(long value) => Properties.TypeFlag = value;
    public long GetTypes() => Properties.TypeFlag;

    public void SetLanguageFlag(long value) => Properties.LanguageFlag = value;
    public long GetLanguageFlag() => Properties.LanguageFlag;

    public void SetDescriptionType(DescriptionTypeEnum value) => Properties.DescriptionType = value;
    public DescriptionTypeEnum GetDescriptionType() => Properties.DescriptionType ?? DescriptionTypeEnum.RichText;
}