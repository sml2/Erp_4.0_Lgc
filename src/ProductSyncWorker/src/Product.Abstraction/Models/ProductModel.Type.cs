﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;
using ERP.Enums;
using ERP.Enums.Product;
using ERP.Extensions;

namespace ERP.Models.Product;

public partial class ProductModel : UserRelated
{
    // 未审核
    public const int AUDIT_UNREVIEWED = 0;

    // 已审核
    public const int AUDIT_AUDITED = 2;

    // 侵权
    public const int AUDIT_INFRINGEMENT = 3;

    #region Type_Column

    public void SetCollect(bool value) => SetFlag(ref _type, (long)Types.Collect, value);
    public bool IsCollect() => IsFlagSet(Type, (long)Types.Collect);


    public void SetFromShare(bool value) => SetFlag(ref _type, (long)Types.FromShare, value);
    public void IsFromShare() => IsFlagSet(Type, (long)Types.FromShare);

    public void SetShare(bool value) => SetFlag(ref _type, (long)Types.Share, value);
    public bool IsShare() => IsFlagSet(Type, (long)Types.Share);


    public void SetDistribution(bool value) => SetFlag(ref _type, (long)Types.Distribution, value);
    public bool IsDistribution() => IsFlagSet(Type, (long)Types.Distribution);

    /**
     * const FLAG_IMGSYNC = 16;                                // BIT #05 of $flags has the value 16
    public function hasImageSync():bool{return $this->isFlagSet(self::FLAG_IMGSYNC);}
    public function setImageSync(bool $value){$this->setFlag(self::FLAG_IMGSYNC, $value);}
     */
    public void SetEdited(bool value) => SetFlag(ref _type, (long)Types.Edited, value);

    public bool IsEdited() => IsFlagSet(Type, (long)Types.Edited);

    public void SetRecycle(bool value) => SetFlag(ref _type, (long)Types.Recycle, value);
    public bool IsRecycle() => IsFlagSet(Type, (long)Types.Recycle);

    public void SetLock(bool value) => SetFlag(ref _type, (long)Types.Lock, value);
    public bool IsLock() => IsFlagSet(Type, (long)Types.Lock);

    public void SetRepeat(bool value) => SetFlag(ref _type, (long)Types.Repeat, value);
    public bool IsRepeat() => IsFlagSet(Type, (long)Types.Repeat);


    public void SetOngoing(bool value) => SetFlag(ref _type, (long)Types.OnGoing, value);
    public bool IsOngoing() => IsFlagSet(Type, (long)Types.OnGoing);

    public void SetUpload(bool value) => SetFlag(ref _type, (long)Types.Uplaod, value);
    public bool IsUpload() => IsFlagSet(Type, (long)Types.Uplaod);

    public void SetExport(bool value) => SetFlag(ref _type, (long)Types.Export, value);
    public bool IsExport() => IsFlagSet(Type, (long)Types.Export);

    public void SetAmazonProductFlag(bool value) => SetFlag(ref _type, (long)Types.AmazonProduct, value);
    public bool IsAmazonProductFlag(bool value) => IsFlagSet(Type, (long)Types.AmazonProduct);

    public void SetB2C(bool value) => SetFlag(ref _type, (long)Types.B2C, value);
    public bool IsB2C() => IsFlagSet(Type, (long)Types.B2C);

    public void SetB2CRelease(bool value) => SetFlag(ref _type, (long)Types.B2CRelease, value);
    public bool IsB2CRelease() => IsFlagSet(Type, (long)Types.B2CRelease);

    public void SetAudit(int value) => SetInt(ref _type, value, (long)Types.MASK_AUDIT, (int)Types.Audit);
    public long? GetAudit() => GetInt(Type, (long)Types.MASK_AUDIT, (int)Types.Audit);

    public void SetDelete(bool value) => SetFlag(ref _type, (long)Types.Delete, value);
    public bool IsDelete() => IsFlagSet(Type, (long)Types.Delete);

    #endregion
}