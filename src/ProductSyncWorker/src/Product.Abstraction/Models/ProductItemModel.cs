using System.ComponentModel.DataAnnotations.Schema;
using ERP.Enums;
using ERP.Extensions;
using ERP.Models.DB.Identity;
using ERP.Models.Product;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace ERP.Models.DB.Product;

[Index(nameof(Pid), nameof(LanguageId))]
[Index(nameof(LanguageId))]
public class ProductItemModel : UserRelated
{
    public ProductItemModel()
    {
    }
    

    public ProductItemModel(User user, string coin,int quantity)
    {
        Coin = coin;
        Quantity = quantity;
        UserID = user.Id;
        GroupID = user.GroupID;
        CompanyID = user.CompanyID;
        OEMID = user.OEMID;
    }

    [NotMapped]
    public string Title { get; set; }

    public int Pid { get; set; }

    public string LanguageName { get; set; }

    public int? LanguageId { get; set; }

    [NotMapped]
    public Languages Language => Enum.Parse<Languages>(LanguageName,true);

    [Comment("货币单位(采集产品单位)")]
    public string Coin { get; set; } = string.Empty;

    [Comment("库存")]
    public int Quantity { get; set; }

    [Comment("产品文件大小")]
    public long FilesSize { get; set; }

    [Comment("当前版本json文件路径hashcode")]
    public ulong FilePathHashCode { get; set; }

    [Comment("当前版本json文件路径")]
    public string FilePath { get; set; }

    [Comment("当前版本")]
    public long CurrentVersion { get; set; }

    [Comment("历史版本")]
    //List<long>
    public string HistoryVersion { get; set; }

    [NotMapped]
    public List<long> HistoryVersionList => JsonConvert.DeserializeObject<List<long>>(HistoryVersion)!;
    
}