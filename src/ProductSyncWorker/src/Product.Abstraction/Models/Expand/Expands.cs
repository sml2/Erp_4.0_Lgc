using ERP.Enums.Product;

namespace ERP.Data.Products.Expand;

public class Expands : List<Expand>
{
    public void Add(string key, string name, string value, ExpandEnum type) => Add(new(key, name, value,type));
}