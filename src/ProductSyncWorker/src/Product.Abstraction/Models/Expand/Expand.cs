using ERP.Enums.Product;

namespace ERP.Data.Products.Expand;

public class Expand
{
    public Expand(string key, string name, string value, ExpandEnum type)
    {
        Key = key;
        Name = name;
        Value = value;
        Type = type;
    }

    public Expand()
    {
    }

    public string Key { get; set; }
    public string Name { get; set; }
    public string Value { get; set; }
    public ExpandEnum Type { get; set; }
}