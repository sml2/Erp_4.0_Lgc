﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using ERP.Data.Products;
using ERP.Extensions;
using ERP.Models.Abstract;
using ERP.Models.DB.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.DB.Product;

[Table("temp_images")]
[Comment("产品管理_用户临时资源表")]
public class TempImagesModel : UserModel
{
    public TempImagesModel()
    {
    }

    /// <summary>
    /// 正式图片迁移到临时图片后期删除
    /// </summary>
    /// <param name="m"></param>
    public TempImagesModel(ResourceModel m)
    {
        ID = m.ID;
        Path = m.Path;
        OssID = m.StorageId;
        Type = m.Type;
        Size = m.Size;
        OEMID = m.OEMID;
        CompanyID = m.CompanyID;
        GroupID = m.GroupID;
        UserID = m.UserID;
        CreatedAt = DateTime.MinValue;
        UpdatedAt = DateTime.MinValue;
    }

    public TempImagesModel(string path, Image item, int StorageId, ResourceModel.Types type, ISession session)
    {
        Path = path;
        OssID = StorageId;
        Type = type;
        Size = item.Size;
        UserID = session.GetUserID();
        GroupID = session.GetGroupID();
        CompanyID = session.GetCompanyID();
        OEMID = session.GetOEMID();
    }
    
    public TempImagesModel(int resourceId,string path, Image item, int StorageId, ResourceModel.Types type, ISession session)
    {
        ResourceId = resourceId;
        Path = path;
        OssID = StorageId;
        Type = type;
        Size = item.Size;
        UserID = session.GetUserID();
        GroupID = session.GetGroupID();
        CompanyID = session.GetCompanyID();
        OEMID = session.GetOEMID();
    }

    public TempImagesModel(long size, string path, int StorageId, ResourceModel.Types type, ISession session)
    {
        Path = path;
        OssID = StorageId;
        Type = type;
        Size = size;
        UserID = session.GetUserID();
        GroupID = session.GetGroupID();
        CompanyID = session.GetCompanyID();
        OEMID = session.GetOEMID();
    }

    public TempImagesModel(long size, string path, int OEMID, int CompanyID, int GroupID, int UserID)
    {
        Path = path;
        OssID = 0;
        Type = ResourceModel.Types.Storage;


        this.Size = size;
        this.OEMID = OEMID;
        this.CompanyID = CompanyID;
        this.GroupID = GroupID;
        this.UserID = UserID;
    }

    [Comment("oss路径")]
    public string Path { get; set; } = string.Empty;

    [Comment("大小（kb）")]
    public long Size { get; set; }

    [Comment("产品id")]
    public int OssID { get; set; }

    [Comment("1 oss地址 2 远程地址")]
    public ResourceModel.Types Type { get; set; }
    
    [NotMapped]
    public int ResourceId { get; set; }
}

//------------------------------
//--Table structure for erp3_temp_images
//------------------------------
//DROP TABLE IF EXISTS `erp3_temp_images`;
//CREATE TABLE `erp3_temp_images`  (
//  `id` bigint unsigned NOT NULL,
//  `d_oss_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'oss路径',
//  `d_size` bigint unsigned NOT NULL COMMENT '图片大小',
//  `i_user_id` bigint unsigned NOT NULL COMMENT '用户id',
//  `i_oss_id` bigint unsigned NOT NULL COMMENT 'oss资源',
//  `i_oem_id` bigint unsigned NOT NULL COMMENT 'oem',
//  `i_product_id` bigint unsigned NOT NULL COMMENT '产品id',
//  `t_type` tinyint unsigned NOT NULL COMMENT '1oss 2远程url',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
// PRIMARY KEY(`id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `oss_id`(`i_oss_id`) USING BTREE,
// INDEX `product_id`(`i_product_id`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 164426944 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '产品管理_用户临时资源表' ROW_FORMAT = Dynamic;