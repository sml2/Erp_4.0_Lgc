﻿namespace ERP.Data.Products;

public static class PropertyKey
{
    public const string Title = nameof(Title);
    public const string Keyword = nameof(Keyword);
    public const string Description = nameof(Description);
    public const string Sketches = nameof(Sketches);
    public const string CategoryId = nameof(CategoryId);
    public const string SubCategoryId = nameof(SubCategoryId);
    public const string IsImageSync = nameof(IsImageSync);
    public const string Prices = nameof(Prices);
    public const string Price = nameof(Price);
    public const string Image = nameof(Image);
    public const string Sku = nameof(Sku);
    public const string Code = nameof(Code);
    public const string Asin = nameof(Asin);
    public const string Sid = nameof(Sid);
    public const string UploadState = nameof(UploadState);


    #region 属性

    /// <summary>
    /// 存在电池
    /// </summary>
    public const string Battery = nameof(Battery);

    /// <summary>
    /// 颜色
    /// </summary>
    public const string Color = nameof(Color);

    /// <summary>
    /// 尺码
    /// </summary>
    public const string Size = nameof(Size);

    /// <summary>
    /// 重量
    /// </summary>
    public const string Weight = nameof(Weight);

    /// <summary>
    /// 长度（cm）
    /// </summary>
    public const string Length = nameof(Length);

    /// <summary>
    /// 宽度（cm）
    /// </summary>
    public const string Width = nameof(Width);

    /// <summary>
    /// 高度（cm）
    /// </summary>
    public const string Height = nameof(Height);

    /// <summary>
    /// 原产国
    /// </summary>
    public const string Origin = nameof(Origin);

    /// <summary>
    /// 品牌
    /// </summary>
    public const string Brand = nameof(Brand);

    /// <summary>
    /// 制造商
    /// </summary>
    public const string Facturer = nameof(Facturer);

    /// <summary>
    /// 厂商编号
    /// </summary>
    public const string Number = nameof(Number);

    /// <summary>
    /// 材料
    /// </summary>
    public const string Material = nameof(Material);

    /// <summary>
    /// 珠宝类型
    /// </summary>
    public const string Gem = nameof(Gem);

    /// <summary>
    /// 金属材质
    /// </summary>
    public const string Metal = nameof(Metal);

    /// <summary>
    /// 包装材料
    /// </summary>
    public const string Package = nameof(Package);

    /// <summary>
    /// 生产日期
    /// </summary>
    public const string Produced = nameof(Produced);

    /// <summary>
    /// 有效期（月）
    /// </summary>
    public const string Validity = nameof(Validity);

    /// <summary>
    /// 推荐浏览节点(亚马逊相关)
    /// </summary>
    public const string RecommendedBrowseNodes = nameof(RecommendedBrowseNodes);

    /// <summary>
    /// 自定义
    /// </summary>
    public const string Custom = nameof(Custom);


    public const string SourceId = nameof(SourceId);
    public const string SourceItemId = nameof(SourceItemId);

    #endregion


    /// <summary>
    /// 来源URL
    /// </summary>
    public const string Source = nameof(Source);

    /// <summary>
    /// 源平台
    /// </summary>
    public const string Platform = nameof(Platform);

    /// <summary>
    /// 在源平台的唯一标识
    /// </summary>
    public const string PlatformID = nameof(PlatformID);

    /// <summary>
    /// 库存
    /// </summary>
    public const string Quantity = nameof(Quantity);

    /// <summary>
    /// 采集货币单位
    /// </summary>
    public const string Carrie = nameof(Carrie);

    public static string Pid = nameof(Pid);

    public static string PidHashCode = nameof(PidHashCode);


    public static string TypeFlag = nameof(TypeFlag);

    public static string LanguageFlag = nameof(LanguageFlag);

    public static string DescriptionType = nameof(DescriptionType);
}