﻿
namespace ERP.Exceptions.Product
{
    public class GeneralException : ProductException
    {
        public GeneralException(string message) : base(message)
        {
        }
    }
}
