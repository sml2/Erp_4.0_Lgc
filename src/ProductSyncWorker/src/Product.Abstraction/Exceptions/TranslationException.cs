namespace ERP.Exceptions.Product;

public class TranslationException : ProductException
{
    public TranslationException(string message) : base(message)
    {
    }
}