namespace ERP.Exceptions.Product;

public class SavedException : ProductException
{
    public SavedException(string message) : base(message)
    {
        
    }
}