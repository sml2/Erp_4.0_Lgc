namespace ERP.Exceptions.Upload;

public class UploadException  : Exception
{
    public UploadException(string message) : base(message)
    {
    }
}