﻿namespace ERP.Exceptions.Product;

public class ProductException :  Exception// StorageDomainException
{
    public ProductException(string message) : base(message)
    {
    }
}

public class EanUpcNotEnoughException : ProductException
{
    public EanUpcNotEnoughException(string mode) : base($"当前{mode}数量不足")
    {
    }
}