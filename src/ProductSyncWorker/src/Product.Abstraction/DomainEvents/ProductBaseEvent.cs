using ERP.Models.Product;
using MediatR;

namespace ERP.DomainEvents.Product;

public class ProductBaseEvent : INotification
{
    public ProductBaseEvent(ProductModel product)
    {
        Product = product;
    }

    public ProductModel Product { get; }
}