using ERP.Models.Product;

namespace ERP.DomainEvents.Product;

public class ProductRecoveryEvent : ProductBaseEvent
{
    public bool IsInRecovery;

    public ProductRecoveryEvent(ProductModel product, bool isInRecovery) : base(product)
    {
        IsInRecovery = isInRecovery;
    }
}