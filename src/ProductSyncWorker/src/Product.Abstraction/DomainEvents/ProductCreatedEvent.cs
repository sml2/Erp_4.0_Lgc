using ERP.Models.Product;

namespace ERP.DomainEvents.Product;

public class ProductCreatedEvent : ProductBaseEvent
{
    public ProductCreatedEvent(ProductModel product) : base(product)
    {
        
    }
}