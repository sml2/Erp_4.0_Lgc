using ERP.Models.Product;

namespace ERP.DomainEvents.Product;

public class ProductDeleteEvent : ProductBaseEvent
{
    public ProductDeleteEvent(ProductModel product) : base(product)
    {
        
    }
}