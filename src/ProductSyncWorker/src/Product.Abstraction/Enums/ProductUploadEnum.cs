namespace ERP.Enums.Product;

public enum ProductUploadEnum
{
    /// <summary>
    /// 未上传
    /// </summary>
    NotUploaded,
    
    /// <summary>
    /// 已上传
    /// </summary>
    Uploaded
}