namespace ERP.Enums.Product;

public enum FeatureEnum
{
    None = 0,

    /// <summary>
    /// 存在电池
    /// </summary>
    [EnumExtension.DescriptionAttribute("存在电池")]
    Battery,

    /// <summary>
    /// 颜色
    /// </summary>
    [EnumExtension.DescriptionAttribute("颜色")]
    Color,

    /// <summary>
    /// 尺码
    /// </summary>
    [EnumExtension.DescriptionAttribute("尺码")]
    Size,

    /// <summary>
    /// 重量
    /// </summary>
    [EnumExtension.DescriptionAttribute("重量")]
    Weight,

    /// <summary>
    /// 长度（cm）
    /// </summary>
    [EnumExtension.DescriptionAttribute("长度")]
    Length,

    /// <summary>
    /// 宽度（cm）
    /// </summary>
    [EnumExtension.DescriptionAttribute("宽度")]
    Width,

    /// <summary>
    /// 高度（cm）
    /// </summary>
    [EnumExtension.DescriptionAttribute("高度")]
    Height,

    /// <summary>
    /// 原产国
    /// </summary>
    [EnumExtension.DescriptionAttribute("原产国")]
    Origin,

    /// <summary>
    /// 品牌
    /// </summary>
    [EnumExtension.DescriptionAttribute("品牌")]
    Brand,

    /// <summary>
    /// 制造商
    /// </summary>
    [EnumExtension.DescriptionAttribute("制造商")]
    Facturer,

    /// <summary>
    /// 厂商编号
    /// </summary>
    [EnumExtension.DescriptionAttribute("厂商编号")]
    Number,

    /// <summary>
    /// 材料
    /// </summary>
    [EnumExtension.DescriptionAttribute("材料")]
    Material,

    /// <summary>
    /// 珠宝类型
    /// </summary>
    [EnumExtension.DescriptionAttribute("珠宝类型")]
    Gem,

    /// <summary>
    /// 金属材质
    /// </summary>
    [EnumExtension.DescriptionAttribute("金属材质")]
    Metal,

    /// <summary>
    /// 包装材料
    /// </summary>
    [EnumExtension.DescriptionAttribute("包装材料")]
    Package,

    /// <summary>
    /// 生产日期
    /// </summary>
    [EnumExtension.DescriptionAttribute("生产日期")]
    Produced,

    /// <summary>
    /// 有效期（月）
    /// </summary>
    [EnumExtension.DescriptionAttribute("有效期")]
    Validity,

    /// <summary>
    /// 推荐浏览节点(亚马逊相关)
    /// </summary>
    [EnumExtension.DescriptionAttribute("推荐浏览节点(亚马逊相关)")]
    RecommendedBrowseNodes,
    
    
    /// <summary>
    /// B2C
    /// </summary>
    [EnumExtension.DescriptionAttribute("B2C")]
    B2C,
    
    /// <summary>
    /// B2cOriginalPid b2c克隆产品的原产品ID
    /// </summary>
    [EnumExtension.DescriptionAttribute("B2cOriginalPid")]
    B2cOriginalPid,
}