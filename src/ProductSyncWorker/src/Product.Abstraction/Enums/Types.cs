using ERP.Enums.Rule;

namespace ERP.Enums.Product;

[Flags]
public enum Types : long
{
    Default = 4611686018427387904,

    //采集产品
    Collect = Flag.FlAG_BIT_01,

    //来自分享
    FromShare = Flag.FlAG_BIT_02,

    //分享产品
    Share = Flag.FlAG_BIT_03,

    //分销产品
    Distribution = Flag.FlAG_BIT_04,

    //是否修改过
    Edited = Flag.FlAG_BIT_06,

    //回收站
    Recycle = Flag.FlAG_BIT_07,

    //lock
    Lock = Flag.FlAG_BIT_08,

    //重复
    Repeat = Flag.FlAG_BIT_10,

    //上传中
    OnGoing = Flag.FlAG_BIT_11,

    //上传产品
    Uplaod = Flag.FlAG_BIT_12,

    //导出产品
    Export = Flag.FlAG_BIT_13,

    /// <summary>
    /// 从amazon接口拿来的产品信息
    /// </summary>
    AmazonProduct = Flag.FlAG_BIT_14,
    
    /// <summary>
    /// 标记删除
    /// </summary>
    Delete = Flag.FlAG_BIT_15,

    //b2c
    B2C = Flag.FlAG_BIT_43,

    //B2CRelease
    B2CRelease = Flag.FlAG_BIT_44,

    //Audit
    Audit = 54L,
    MASK_AUDIT = 4593671619917905920,

    // //Upload
    // Upload = Flag.FlAG_BIT_04,
    //
    // //自增产品
    // Manual = Flag.FlAG_BIT_09,
    //
    // //审核产品
    // Audit = Flag.FlAG_BIT_10,
    //
    //
    //
    // //回收站
    // Recycle = (long)1 << 63,
}


/*
 <?php
namespace App\Models\Product\Biter;

use App\BitwiseFlag;

class Type extends BitwiseFlag
{
    protected  function getFieldName(): string
    {
        return 'b_type';
    }

    const FLAG_COLLECT = 1;                            // BIT #01 of $flags has the value 1
    public function isUserAdd():bool{return !$this->IsCollct();}
    public function setUserAdd(bool $value){$this->setCollct(!$value);}
    public function isCollct():bool{return $this->isFlagSet(self::FLAG_COLLECT);}
    public function setCollct(bool $value){$this->setFlag(self::FLAG_COLLECT, $value);}

    const FLAG_FOMESHARE = 2;                                 // BIT #02 of $flags has the value 2
    public function isFromShare():bool{return $this->isFlagSet(self::FLAG_FOMESHARE);}
    public function setFromShare(bool $value){$this->setFlag(self::FLAG_FOMESHARE, $value);}

    const FLAG_SHARE = 4;                                     // BIT #03 of $flags has the value 4
    public function isShare():bool{return $this->isFlagSet(self::FLAG_SHARE);}
    public function setShare(bool $value){$this->setFlag(self::FLAG_SHARE, $value);}

    const FLAG_DISTRIBUTION = 8;                                   // BIT #04 of $flags has the value 8
    public function isDistribution():bool{return $this->isFlagSet(self::FLAG_DISTRIBUTION);}
    public function setDistribution(bool $value){$this->setFlag(self::FLAG_DISTRIBUTION, $value);}
//    public function isShareRange():bool{return $this->isFlagSet(self::FLAG_DISTRIBUTION);}
//    public function setShareRange(bool $value){$this->setFlag(self::FLAG_DISTRIBUTION, $value);}

    const FLAG_IMGSYNC = 16;                                // BIT #05 of $flags has the value 16
    public function hasImageSync():bool{return $this->isFlagSet(self::FLAG_IMGSYNC);}
    public function setImageSync(bool $value){$this->setFlag(self::FLAG_IMGSYNC, $value);}

    const FLAG_EDITED = 32;                            // BIT #06 of $flags has the value 32
    public function hasEdited():bool{return $this->isFlagSet(self::FLAG_EDITED);}
    public function setEdited(bool $value){$this->setFlag(self::FLAG_EDITED, $value);}

     const FLAG_RECYCLE = 64;                                // BIT #07 of $flags has the value 64
     public function isRecycle():bool{return $this->isFlagSet(self::FLAG_RECYCLE);}
    public function setRecycle(bool $value){$this->setFlag(self::FLAG_RECYCLE, $value);}
    public function edtRecycle(bool $value):array {return $this->edtFlag(self::FLAG_RECYCLE, $value);}

     const FLAG_LOCK = 128;                                 // BIT #08 of $flags has the value 128
     public function isLock():bool{return $this->isFlagSet(self::FLAG_LOCK);}
     public function setLock(bool $value){$this->setFlag(self::FLAG_LOCK, $value);}
//
    // 是否外部分享
//     const FLAG_SHARE_RANGE = 256;                                  // BIT #09 of $flags has the value 256
//     public function isShareRange():bool{return $this->isFlagSet(self::FLAG_SHARE_RANGE);}
//     public function setShareRange(bool $value){$this->setFlag(self::FLAG_SHARE_RANGE, $value);}

     const FLAG_TITLE_REPEAT = 512;                                  // BIT #09 of $flags has the value 256
     public function isRepeat():bool{return $this->isFlagSet(self::FLAG_TITLE_REPEAT);}
     public function setRepeat(bool $value){$this->setFlag(self::FLAG_TITLE_REPEAT, $value);}
     public function editRepeat(bool $value):array {return $this->edtFlag(self::FLAG_TITLE_REPEAT, $value);}

     const FLAG_IS_ONGOING = 1024;                               // BIT #11 of $flags has the value 1024
     public function isOngoing():bool{return $this->isFlagSet(self::FLAG_IS_ONGOING);}
     public function setOngoing(bool $value){$this->setFlag(self::FLAG_IS_ONGOING, $value);}

     const FLAG_IS_UPLOAD = 2048;                               // BIT #12 of $flags has the value 2048
     public function hasUpload():bool{return $this->isFlagSet(self::FLAG_IS_UPLOAD);}
     public function setUpload(bool $value){$this->setFlag(self::FLAG_IS_UPLOAD, $value);}

     const FLAG_IS_EXPORT = 4096;                               // BIT #13 of $flags has the value 4096
     public function hasExport():bool{return $this->isFlagSet(self::FLAG_IS_EXPORT);}
     public function setExport(bool $value){$this->setFlag(self::FLAG_IS_EXPORT, $value);}

//     const FLAG_KO = 8192;                               // BIT #14 of $flags has the value 8192
//     public function hasko():bool{return $this->isFlagSet(self::FLAG_KO);}
//     public function setko(bool $value){$this->setFlag(self::FLAG_KO, $value);}

//     const FLAG_MS = 16384;                               // BIT #15 of $flags has the value 16384
//     public function hasms():bool{return $this->isFlagSet(self::FLAG_MS);}
//     public function setms(bool $value){$this->setFlag(self::FLAG_MS, $value);}

//     const FLAG_TL = 32768;                               // BIT #16 of $flags has the value 32768‬
//     public function hastl():bool{return $this->isFlagSet(self::FLAG_TL);}
//     public function settl(bool $value){$this->setFlag(self::FLAG_TL, $value);}

//     const FLAG_PL = 65536;                               // BIT #17 of $flags has the value 65536
//     public function haspl():bool{return $this->isFlagSet(self::FLAG_PL);}
//     public function setpl(bool $value){$this->setFlag(self::FLAG_PL, $value);}

//     const FLAG_FI = 131072;                               // BIT #18 of $flags has the value 131072
//     public function hasfi():bool{return $this->isFlagSet(self::FLAG_FI);}
//     public function setfi(bool $value){$this->setFlag(self::FLAG_FI, $value);}

//     const FLAG_RU = 262144;                               // BIT #19 of $flags has the value 262144
//     public function hasru():bool{return $this->isFlagSet(self::FLAG_RU);}
//     public function setru(bool $value){$this->setFlag(self::FLAG_RU, $value);}

//     const FLAG_CA = 524288;                               // BIT #20 of $flags has the value 524288
//     public function hasca():bool{return $this->isFlagSet(self::FLAG_CA);}
//     public function setca(bool $value){$this->setFlag(self::FLAG_CA, $value);}

//     const FLAG_LO = 1048576;                               // BIT #21 of $flags has the value 1048576
//     public function haslo():bool{return $this->isFlagSet(self::FLAG_LO);}
//     public function setlo(bool $value){$this->setFlag(self::FLAG_LO, $value);}

//     const FLAG_IW = 2097152;                               // BIT #22 of $flags has the value 2097152
//     public function hasiw():bool{return $this->isFlagSet(self::FLAG_IW);}
//     public function setiw(bool $value){$this->setFlag(self::FLAG_IW, $value);}

//     const FLAG_NE = 4194304;                               // BIT #23 of $flags has the value 4194304
//     public function hasne():bool{return $this->isFlagSet(self::FLAG_NE);}
//     public function setne(bool $value){$this->setFlag(self::FLAG_NE, $value);}

//     const FLAG_SV = 8388608;                               // BIT #24 of $flags has the value 8388608
//     public function hassv():bool{return $this->isFlagSet(self::FLAG_SV);}
//     public function setsv(bool $value){$this->setFlag(self::FLAG_SV, $value);}

//     const FLAG_KY = 16777216;                               // BIT #25 of $flags has the value 16777216
//     public function hasky():bool{return $this->isFlagSet(self::FLAG_KY);}
//     public function setky(bool $value){$this->setFlag(self::FLAG_KY, $value);}

//     const FLAG_DA = 33554432;                               // BIT #26 of $flags has the value 33554432
//     public function hasda():bool{return $this->isFlagSet(self::FLAG_DA);}
//     public function setda(bool $value){$this->setFlag(self::FLAG_DA, $value);}

//     const FLAG_BE = 67108864;                               // BIT #27 of $flags has the value 67108864
//     public function hasbe():bool{return $this->isFlagSet(self::FLAG_BE);}
//     public function setbe(bool $value){$this->setFlag(self::FLAG_BE, $value);}

//     const FLAG_TR = 134217728;                               // BIT #28 of $flags has the value 134217728
//     public function hastr():bool{return $this->isFlagSet(self::FLAG_TR);}
//     public function settr(bool $value){$this->setFlag(self::FLAG_TR, $value);}

//     const FLAG_HI = 268435456;                               // BIT #29 of $flags has the value 268435456
//     public function hashi():bool{return $this->isFlagSet(self::FLAG_HI);}
//     public function sethi(bool $value){$this->setFlag(self::FLAG_HI, $value);}

//     const FLAG_IS = 536870912;                               // BIT #30 of $flags has the value 536870912
//     public function hasis():bool{return $this->isFlagSet(self::FLAG_IS);}
//     public function setis(bool $value){$this->setFlag(self::FLAG_IS, $value);}

//     const FLAG_HU = 1073741824;                               // BIT #31 of $flags has the value 1073741824
//     public function hashu():bool{return $this->isFlagSet(self::FLAG_HU);}
//     public function sethu(bool $value){$this->setFlag(self::FLAG_HU, $value);}

//     const FLAG_MN = 2147483648;                               // BIT #32 of $flags has the value 2147483648
//     public function hasmn():bool{return $this->isFlagSet(self::FLAG_MN);}
//     public function setmn(bool $value){$this->setFlag(self::FLAG_MN, $value);}

//     const FLAG_UK = 4294967296;                               // BIT #33 of $flags has the value 4294967296
//     public function hasuk():bool{return $this->isFlagSet(self::FLAG_UK);}
//     public function setuk(bool $value){$this->setFlag(self::FLAG_UK, $value);}

//     const FLAG_GA = 8589934592;                               // BIT #34 of $flags has the value 8589934592
//     public function hasga():bool{return $this->isFlagSet(self::FLAG_GA);}
//     public function setga(bool $value){$this->setFlag(self::FLAG_GA, $value);}

//     const FLAG_MY = 17179869184;                               // BIT #35 of $flags has the value 17179869184
//     public function hasmy():bool{return $this->isFlagSet(self::FLAG_MY);}
//     public function setmy(bool $value){$this->setFlag(self::FLAG_MY, $value);}

//     const FLAG_LA = 34359738368;                               // BIT #36 of $flags has the value 34359738368
//     public function hasla():bool{return $this->isFlagSet(self::FLAG_LA);}
//     public function setla(bool $value){$this->setFlag(self::FLAG_LA, $value);}

//     const FLAG_EL = 68719476736;                               // BIT #37 of $flags has the value 68719476736
//     public function hasel():bool{return $this->isFlagSet(self::FLAG_EL);}
//     public function setel(bool $value){$this->setFlag(self::FLAG_EL, $value);}

//     const FLAG_BG = 137438953472;                               // BIT #38 of $flags has the value 137438953472
//     public function hasbg():bool{return $this->isFlagSet(self::FLAG_BG);}
//     public function setbg(bool $value){$this->setFlag(self::FLAG_BG, $value);}

//     const FLAG_TA = 274877906944;                               // BIT #39 of $flags has the value 274877906944
//     public function hasta():bool{return $this->isFlagSet(self::FLAG_TA);}
//     public function setta(bool $value){$this->setFlag(self::FLAG_TA, $value);}

//     const FLAG_LT = 549755813888;                               // BIT #40 of $flags has the value 549755813888
//     public function haslt():bool{return $this->isFlagSet(self::FLAG_LT);}
//     public function setlt(bool $value){$this->setFlag(self::FLAG_LT, $value);}

//     const FLAG_NO = 1099511627776;                               // BIT #41 of $flags has the value 1099511627776
//     public function hasno():bool{return $this->isFlagSet(self::FLAG_NO);}
//     public function setno(bool $value){$this->setFlag(self::FLAG_NO, $value);}

//     const FLAG_RO = 2199023255552;                               // BIT #42 of $flags has the value 2199023255552
//     public function hasro():bool{return $this->isFlagSet(self::FLAG_RO);}
//     public function setro(bool $value){$this->setFlag(self::FLAG_RO, $value);}

     const FLAG_IS_B2C = 4398046511104;                               // BIT #43 of $flags has the value 4398046511104
     public function hasB2C():bool{return $this->isFlagSet(self::FLAG_IS_B2C);}
     public function setB2C(bool $value){$this->setFlag(self::FLAG_IS_B2C, $value);}

     const FLAG_IS_B2C_RELEASE = 8796093022208;                               // BIT #44 of $flags has the value 8796093022208
     public function hasB2CRelease():bool{return $this->isFlagSet(self::FLAG_IS_B2C_RELEASE);}
     public function setB2CRelease(bool $value){$this->setFlag(self::FLAG_IS_B2C_RELEASE, $value);}
// //-----------------------------------
//     const BIT_CLIENTLANGUAGE = 48;                      //0000000000111111000000000000000000000000000000000000000000000000
//     const MASK_CLIENTLANGUAGE = 17732923532771328;           // BIT #49-54 of $flags has the value 17732923532771328
//     public function getClientLanguage():int{return $this->getInt(self::MASK_CLIENTLANGUAGE,self::BIT_CLIENTLANGUAGE);}
//     public function setClientLanguage(int $value){$this->setInt($value, self::BIT_CLIENTLANGUAGE);}

    const BIT_AUDIT = 54;                          //0011111111000000000000000000000000000000000000000000000000000000
    const MASK_AUDIT = 4593671619917905920;           // BIT #55-62 of $flags has the value 4593671619917905920
    public function getAudit():int{return $this->getInt(self::MASK_AUDIT,self::BIT_AUDIT);}
    public function setAudit(int $value){$this->setInt($value, self::MASK_AUDIT,self::BIT_AUDIT);}

    // const FLAG_PPP = 1152921504606846976;           // BIT #61 of $flags has the value 1152921504606846976
    // public function hasPPP():bool{return $this->isFlagSet(self::FLAG_PPP);}
    // public function setPPP(bool $value){$this->setFlag(self::FLAG_PPP, $value);}

    // const FLAG_TRANSLATING = 2305843009213693952;           // BIT #62 of $flags has the value 2305843009213693952
    // public function hasTranslating():bool{return $this->isFlagSet(self::FLAG_TRANSLATING);}
    // public function setTranslating(bool $value){$this->setFlag(self::FLAG_TRANSLATING, $value);}
    public function __construct(int $v = 0)
    {
        parent::__construct($v);
    }

}


 */