namespace ERP.Enums.Product;

public enum ExpandEnum
{
   /// <summary>
   /// 特定
   /// </summary>
   Specific,
   /// <summary>
   /// 单选框
   /// </summary>
   Radio = 1,
   /// <summary>
   /// 数字输入框
   /// </summary>
   Number = 2,
   /// <summary>
   /// 字符串输入框
   /// </summary>
   String = 3,
   /// <summary>
   /// 日期
   /// </summary>
   Date = 4,
   
}