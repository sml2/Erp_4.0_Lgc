using ERP.Data;
using ERP.DomainEvents.Product;
using ERP.EventHandlers.Product;
using ERP.Interface;
using ERP.ProductSyncWorker;
using ERP.Services.DB.ERP3;
using ERP.Services.DB.Statistics;
using ERP.Services.FileStorage;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NLog.Web;

var host = Host.CreateDefaultBuilder(args)
    .ConfigureLogging(builder => builder.ClearProviders())
    .UseNLog()
    .ConfigureAppConfiguration(builder => builder.AddEnvironmentVariables("ERP_"))
    .ConfigureServices((context, services) =>
    {
        
        services.AddHostedService<MigrateProductBackground>();
        // services.AddHostedService<MigrateSyncExportBackground>();
        services.AddScoped<ProductSyncService>();
        services.AddScoped<Statistic>();
        services.AddMediatR(typeof(ProductCreatedEvent).Assembly, typeof(ProductCreatedReportEventHandler).Assembly);
        services.AddSingleton<IOemProvider, NullOemProvider>();
        
        var configuration = context.Configuration;
        configuration.GetSection("Disk").Bind(FileSystemFactory.diskConfig);
        AddDbContext(services, configuration);
    })
    .Build();


host.Run();

static void AddDbContext(IServiceCollection services, IConfiguration configuration)
{
    var connectionStr = configuration.GetConnectionString("mysql");
    if (string.IsNullOrWhiteSpace(connectionStr))
        throw new Exception("没有配置mysql数据库连接字符串");

    services.AddDbContext<DBContext>(options =>
    {
        options.UseMySql(connectionStr, ServerVersion.AutoDetect(connectionStr)).EnableSensitiveDataLogging().EnableDetailedErrors();
    });
        
    var connectionStr3 = configuration.GetConnectionString("mysql3");
    if (string.IsNullOrWhiteSpace(connectionStr3))
        throw new Exception("没有配置mysql3.0的数据库连接字符串");
    
    services.AddDbContext<DBContext3>(options =>
        options.UseMySql(connectionStr3, ServerVersion.AutoDetect(connectionStr3)).EnableSensitiveDataLogging()
            .EnableDetailedErrors());
}
