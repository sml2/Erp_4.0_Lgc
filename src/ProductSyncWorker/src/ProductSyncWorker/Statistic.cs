using ERP.Enums.Statistics;
using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ERP.Models.DB.Statistics;

using Extensions;

[Index(nameof(AboutID))]
[Index(nameof(Validity))]
public class Statistic : BaseModel
{
    public Statistic()
    {
    }
    /// <summary>
    /// user_day|296        user_month|232          user_year|168       user_total|104
    /// userGroup_day|280   userGroup_month|216     userGroup_year|152  userGroup_total|88
    /// company_day|272     company_month|208       company_year|144    company_total|80
    ///264,200,136,72
    /// oem_day|264         oem_month|200           oem_year|136        oem_total|72
    /// store_day|288       store_month|224         store_year|160      store_total|96
    /// </summary>
    /// <param name="aboutID"></param>
    /// <param name="role"></param>
    /// <param name="EffectiveTimeRange"></param>
    /// <param name="cur"></param>
    /// <exception cref="ArgumentOutOfRangeException"></exception>
    public Statistic(int aboutID, Types role, Types EffectiveTimeRange, DateTime cur)
    {
        AboutID = aboutID;
        AboutType = role | EffectiveTimeRange;
        Validity = EffectiveTimeRange switch
        {
            Types.DAY => cur.LastDayTick(),
            Types.MONTH => cur.LastMonthTick(),
            Types.YEAR => cur.LastYearTick(),
            Types.TOTAL => DateTime.MaxValue,
            _ => throw new ArgumentOutOfRangeException(nameof(EffectiveTimeRange), EffectiveTimeRange, "无效参数")
        };
    }

    #region 公司管理

    [Comment("新增公司数")]
    public int CompanyInsert { get; set; }

    [Comment("删除公司数")]
    public int CompanyDelete { get; set; }

    [Comment("当前公司数")]
    public int CompanyCount { get; set; }

    [Comment("禁用公司数")]
    public int CompanyReject { get; set; }

    #endregion

    #region 员工管理

    [Comment("新增员工数")]
    public int EmployeeInsert { get; set; }

    [Comment("删除员工数")]
    public int EmployeeDelete { get; set; }

    [Comment("当前员工数")]
    public int EmployeeCount { get; set; }

    [Comment("禁用员工数")]
    public int EmployeeReject { get; set; }

    #endregion

    #region 开户管理

    [Comment("新增用户数")]
    public int UserInsert { get; set; }

    [Comment("删除用户数")]
    public int UserDelete { get; set; }


    [Comment("新增用户数-当前公司开通用户数(添加即汇总)")]
    public int UserCount { get; set; }

    [Comment("禁用用户数")]
    public int UserReject { get; set; }

    #endregion

    #region 产品管理

    [Comment("抠图API计费")]
    public int AiMatting { get; set; }

    [Comment("翻译次数")]
    public int ApiTranslate { get; set; }

    [Comment("产品新增语种数量")]
    public int ApiTranslateLanguage { get; set; }

    [Comment("产品翻译次数")]
    public int ApiTranslateProduct { get; set; }

    [Comment("新增硬盘开销")]
    public int DiskAdd { get; set; }

    [Comment("释放硬盘开销")]
    public int DiskDel { get; set; }

    [Comment("当前硬盘开销")]
    public int DiskSize { get; set; }

    [Comment("新增产品数")]
    public int ProductInsert { get; set; }

    [Comment("删除产品数")]
    public int ProductDelete { get; set; }

    /// <summary>
    /// 当前公司产品数(添加即汇总)
    /// </summary>
    [Comment("当前产品数-当前公司产品数(添加即汇总)")]
    public int ProductCount { get; set; }

    [Comment("回收站产品数")]
    public int ProductRecovery { get; set; }

    #endregion

    #region 店铺管理

    [Comment("新增上传任务数")]
    public int TaskInsert { get; set; }

    [Comment("删除上传任务数")]
    public int TaskDelete { get; set; }

    [Comment("当前上传任务数")]
    public int TaskCount { get; set; }

    [Comment("新增店铺数")]
    public int StoreInsert { get; set; }

    [Comment("删除店铺数")]
    public int StoreDelete { get; set; }


    /// <summary>
    /// 当前公司店铺数(添加即汇总)
    /// </summary>
    [Comment("当前店铺数 - 当前公司店铺数(添加即汇总)")]
    public int StoreCount { get; set; }

    #endregion

    #region 订单管理

    [Comment("新增订单数")]
    public int OrderInsert { get; set; }

    [Comment("删除订单数")]
    public int OrderDelete { get; set; }


    /// <summary>
    /// 当前公司订单数(添加即汇总)
    /// </summary>
    [Comment("当前订单数-当前公司订单数(添加即汇总)")]
    public int OrderCount { get; set; }

    #endregion

    #region 采购管理

    [Comment("新增采购数")]
    public int PurchaseInsert { get; set; }

    [Comment("删除采购数")]
    public int PurchaseDelete { get; set; }

    /// <summary>
    /// 当前公司采购数(添加即汇总)
    /// </summary>
    [Comment("当前采购数-当前公司采购数(添加即汇总)")]
    public int PurchaseCount { get; set; }

    #endregion

    #region 运单管理

    [Comment("新增运单数")]
    public int WaybillInsert { get; set; }

    [Comment("删除运单数")]
    public int WaybillDelete { get; set; }

    /// <summary>
    /// 当前公司运单数(添加即汇总)
    /// </summary>
    [Comment("当前公司运单数(添加即汇总)")]
    public int WaybillCount { get; set; }

    #endregion


    [Comment("对应类型主键ID")]
    public int AboutID { get; set; }

    [Comment("类型主键")]
    public Types AboutType { get; set; }

    #region 下级上报

    /// <summary>
    /// 下级上报开通公司数(添加即汇总)
    /// </summary>

    [Comment("下级上报开通公司数(添加即汇总)")]
    public int ReportCompanyCount { get; set; }

    /// <summary>
    /// 下级上报开通公司数(添加即汇总)
    /// </summary>

    [Comment("下级上报开通员工数(添加即汇总)")]
    public int ReportEmployeeCount { get; set; }


    /// <summary>
    /// 下级上报开通员工数(添加即汇总)
    /// </summary>

    [Comment("下级上报开通员工数(添加即汇总)")]
    public int ReportUserCount { get; set; }

    /// <summary>
    /// 下级上报产品数(添加即汇总)
    /// </summary>

    [Comment("下级上报产品数(添加即汇总)")]
    public int ReportProductCount { get; set; }

    /// <summary>
    /// 下级上报店铺数(添加即汇总)
    /// </summary>

    [Comment("下级上报店铺数(添加即汇总)")]
    public int ReportStoreCount { get; set; }

    /// <summary>
    /// 下级上报订单数(添加即汇总)
    /// </summary>

    [Comment("下级上报订单数(添加即汇总)")]
    public int ReportOrderCount { get; set; }

    /// <summary>
    /// 下级上报采购数(添加即汇总)
    /// </summary>

    [Comment("下级上报采购数(添加即汇总)")]
    public int ReportPurchaseCount { get; set; }

    /// <summary>
    /// 下级上报运单数(添加即汇总)
    /// </summary>

    [Comment("下级上报运单数(添加即汇总)")]
    public int ReportWaybillCount { get; set; }

    #endregion

    #region 下级分销

    /// <summary>
    /// 下级上报分销订单数(添加即汇总)
    /// </summary>

    [Comment("下级上报分销订单数(添加即汇总)")]
    public int DistributionOrderCount { get; set; }

    /// <summary>
    /// 下级上报分销采购数(添加即汇总)
    /// </summary>

    [Comment("下级上报分销采购数(添加即汇总)")]
    public int DistributionPurchaseCount { get; set; }

    /// <summary>
    /// 下级上报分销运单数(添加即汇总)
    /// </summary>

    [Comment("下级上报分销运单数(添加即汇总)")]
    public int DistributionWaybillCount { get; set; }

    #endregion


    /// <summary>
    /// 订单总价汇总(基准货币 完结 所属公司 汇总)
    /// </summary>

    [Comment("订单总价汇总(基准货币 完结 所属公司 汇总)")]
    public Money OrderTotal { get; set; }

    /// <summary>
    /// 订单手续费汇总(基准货币 完结 所属公司 汇总)
    /// </summary>

    [Comment("订单手续费汇总(基准货币 完结 所属公司 汇总)")]
    public Money OrderTotalFee { get; set; }

    /// <summary>
    /// 订单退款汇总(基准货币 完结 所属公司 汇总)
    /// </summary>

    [Comment("订单退款汇总(基准货币 完结 所属公司 汇总)")]
    public Money OrderTotalRefund { get; set; }

    /// <summary>
    /// 订单损耗CNY(基准货币 完结 所属公司 汇总)
    /// </summary>

    [Comment("订单损耗CNY(基准货币 完结 所属公司 汇总)")]
    public Money OrderTotalLoss { get; set; }

    /// <summary>
    /// 采购总价汇总(基准货币 完结 所属公司 汇总)
    /// </summary>

    [Comment("采购总价汇总(基准货币 完结 所属公司 汇总)")]
    public Money PurchaseTotal { get; set; }

    /// <summary>
    /// 采购退款汇总(基准货币 完结 所属公司 汇总)
    /// </summary>

    [Comment("采购退款汇总(基准货币 完结 所属公司 汇总)")]
    public Money PurchaseTotalRefund { get; set; }

    /// <summary>
    /// 运费总价汇总(基准货币 完结 所属公司 汇总)
    /// </summary>

    [Comment("运费总价汇总(基准货币 完结 所属公司 汇总)")]
    public Money WaybillTotal { get; set; }

    /// <summary>
    /// 运费退款汇总(基准货币 完结 所属公司 汇总)
    /// </summary>

    [Comment("运费退款汇总(基准货币 完结 所属公司 汇总)")]
    public Money WaybillTotalRefund { get; set; }

    /// <summary>
    /// 自用钱包充值汇总(基准货币 完结 所属公司 汇总)
    /// </summary>

    [Comment("自用钱包充值汇总(基准货币 完结 所属公司 汇总)")]
    public Money SelfRechargeTotal { get; set; }

    /// <summary>
    /// 自用钱包支出汇总(基准货币 完结 所属公司 汇总)
    /// </summary>

    [Comment("自用钱包支出汇总(基准货币 完结 所属公司 汇总)")]
    public Money SelfExpenseTotal { get; set; }

    /// <summary>
    /// 钱包充值汇总(基准货币 完结 所属公司 汇总)
    /// </summary>

    [Comment("钱包充值汇总(基准货币 完结 所属公司 汇总)")]
    public Money RechargeTotal { get; set; }

    /// <summary>
    /// 钱包支出汇总(基准货币 完结 所属公司 汇总)
    /// </summary>

    [Comment("钱包支出汇总(基准货币 完结 所属公司 汇总)")]
    public Money ExpenseTotal { get; set; }

    public bool NeedUpdate { get; internal set; } = true;
    public DateTime Validity { get; internal set; }

    #region MyRegion

    //public function getOrderTotalAttribute($order_total)
    //{
    //    return ToConversion($order_total, $this->getSession()->getUnitConfig());
    //}

    //public function getOrderTotalFeeAttribute($order_total_fee)
    //{
    //    return ToConversion($order_total_fee, $this->getSession()->getUnitConfig());
    //}

    //public function getOrderTotalRefundAttribute($order_total_refund)
    //{
    //    return ToConversion($order_total_refund, $this->getSession()->getUnitConfig());
    //}
    //public function getOrderTotalLossAttribute($order_total_loss)
    //{
    //    return ToConversion($order_total_loss, $this->getSession()->getUnitConfig());
    //}

    //public function getPurchaseTotalAttribute($purchase_total)
    //{
    //    return ToConversion($purchase_total, $this->getSession()->getUnitConfig());
    //}

    //public function getPurchaseTotalRefundAttribute($purchase_total_refund)
    //{
    //    return ToConversion($purchase_total_refund, $this->getSession()->getUnitConfig());
    //}

    //public function getWaybillTotalAttribute($waybill_total)
    //{
    //    return ToConversion($waybill_total, $this->getSession()->getUnitConfig());
    //}

    //public function getWaybillTotalRefundAttribute($waybill_total_refund)
    //{
    //    return ToConversion($waybill_total_refund, $this->getSession()->getUnitConfig());
    //}

    //public function getSelfRechargeTotalAttribute($self_recharge_total)
    //{
    //    return ToConversion($self_recharge_total, $this->getSession()->getUnitConfig());
    //}

    //public function getSelfExpenseTotalAttribute($self_expense_total)
    //{
    //    return ToConversion($self_expense_total, $this->getSession()->getUnitConfig());
    //}

    //public function getRechargeTotalAttribute($recharge_total)
    //{
    //    return ToConversion($recharge_total, $this->getSession()->getUnitConfig());
    //}

    //public function getExpenseTotalAttribute($expense_total)
    //{
    //    return ToConversion($expense_total, $this->getSession()->getUnitConfig());
    //}

    //public function scopeCompanyId(Builder $query, $company_id)
    //{
    //    if ($company_id) {
    //        return $query->where('company_id', '=', $company_id);
    //    }
    //}

    //public function scopeOemId(Builder $query, $oem_id)
    //{
    //    if ($oem_id) {
    //        return $query->where('oem_id', '=', $oem_id);
    //    }

    //}

    //public function scopeInCompanyId(Builder $query, $company_ids)
    //{
    //    if (checkArr($company_ids))
    //    {
    //        return $query->whereIn('company_id', $company_ids);
    //    }
    //    elseif($company_ids) {
    //        return $query->where('company_id', '=', $company_ids);
    //    }
    //}

    #endregion
}