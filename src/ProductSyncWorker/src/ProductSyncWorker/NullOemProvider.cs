﻿using ERP.Interface;
using ERP.Models.Setting;

namespace ERP.ProductSyncWorker;

internal class NullOemProvider : IOemProvider
{
    public OEM? OEM => null;
}