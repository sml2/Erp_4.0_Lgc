using ERP.DomainEvents.Product;
using ERP.Services.DB.Statistics;
using ERP.ViewModels.Statistics;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace ERP.EventHandlers.Product;

public class ProductCreatedReportEventHandler : INotificationHandler<ProductCreatedEvent>
{
    private readonly Statistic _statisticService;
    private ILogger<ProductCreatedReportEventHandler> _logger;

    public ProductCreatedReportEventHandler(Statistic statisticService,
        ILogger<ProductCreatedReportEventHandler> logger)
    {
        _statisticService = statisticService;
        _logger = logger;
    }

    public async Task Handle(ProductCreatedEvent notification, CancellationToken cancellationToken)
    {
        var model = notification.Product;
        _logger.LogInformation("产品创建触发计数 | ID:[${Id}]", model.ID);

        #region 统计-新增产品数量

        await _statisticService.InsertOrCount(new DataBelongDto(model), Statistic.NumColumnEnum.ProductInsert,
            Statistic.NumColumnEnum.ProductCount);

        #endregion
    }
}