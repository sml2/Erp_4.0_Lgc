﻿#!/usr/bin/bash
# build.sh [version]
# version 镜像版本，默认latest,指定版本号最后的tag名称为`v版本号`

tags='-t swr.cn-east-3.myhuaweicloud.com/erp4/product-sync-worker:latest'
if [ $1 ]; then
    tags="$tags -t swr.cn-east-3.myhuaweicloud.com/erp4/product-sync-worker:$1"
fi
echo $tags
docker build . -f src/ProductSyncWorker/src/ProductSyncWorker/Dockerfile  $tags