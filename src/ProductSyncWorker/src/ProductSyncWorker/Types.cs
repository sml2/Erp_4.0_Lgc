namespace ERP.Enums.Statistics;
[Flags]
public enum Types:int
{
    //0-2
    ZERO,ONE,TWO,THREE,FOUR,FIVE,SIX,SEVEN,
    //3-5
    OEM = ONE << 3,
    Company = TWO << 3,
    UserGroup = THREE << 3,
    Store = FOUR << 3,
    User = FIVE << 3,
    Role = SEVEN << 3,
    //6-8
    TOTAL = ONE << 6,
    YEAR = TWO << 6,
    MONTH = THREE << 6,
    DAY = FOUR << 6,
    Effective = SEVEN << 6,
}