using ERP.Extensions;
using Microsoft.EntityFrameworkCore;
using ERP.Data;

namespace ERP.Services.DB.Statistics;

using Enums.Statistics;
using Models.DB.Identity;
using Interface;
using Models.DB.Users;
using Models.Setting;
using Model = Models.DB.Statistics.Statistic;

public partial class Statistic
{
    private readonly DBContext _dbContext;


    public Statistic(DBContext dbContext)
    {
        _dbContext = dbContext;
    }

    private readonly Types[] AllEffectiveTimeRanges = new[] { Types.TOTAL, Types.YEAR, Types.MONTH, Types.DAY };

    private async System.Threading.Tasks.Task CheckOrAdd(int ID, IStatistic IStatistic, Types types)
    {
        var Now = DateTime.Now;
        var Date = Now.Date;
        if (IStatistic.LastCreateStatistic < Date) //是否跨天
        {
            var EffectiveTimeRanges = await _dbContext.Statistic
                .Where(s =>
                    s.AboutID == ID
                    && s.Validity >= Now
                    && (s.AboutType & Types.Role) == types
                )
                .Select(s => s.AboutType & Types.Effective).ToListAsync();
            var NeedAddEffectiveTimeRanges = AllEffectiveTimeRanges.Where(s => !EffectiveTimeRanges.Contains(s));
            await _dbContext.Statistic.AddRangeAsync(
                NeedAddEffectiveTimeRanges.Select(t => new Model(ID, types, t, Date)));
            IStatistic.LastCreateStatistic = Now;

            // var entry = _dbContext.Entry(IStatistic);
            // entry.State = EntityState.Unchanged;
            // entry.Property(nameof(IStatistic.LastCreateStatistic)).IsModified = true;
            await _dbContext.SaveChangesAsync();
        }
    }

    public Task CheckOrAdd(OEM m) => CheckOrAdd(m.ID, m, Types.OEM);
    public Task CheckOrAdd(Company m) => CheckOrAdd(m.ID, m, Types.Company);
    public Task CheckOrAdd(Group m) => CheckOrAdd(m.ID, m, Types.UserGroup);
    public Task CheckOrAdd(User m) => CheckOrAdd(m.ID, m, Types.User);

    public async Task<Model> GetStatisticInfo(int? aboutId, Types role, Types effective)
    {
        if (!aboutId.HasValue)
        {
            throw new Exception($"AboutId Is Null");
        }

        if (effective == Types.DAY)
        {
            switch (role)
            {
                case Types.User:
                    await CheckOrAdd(await _dbContext.User.FindAsync(aboutId));
                    break;
                case Types.UserGroup:
                    await CheckOrAdd(await _dbContext.UserGroup.FindAsync(aboutId));
                    break;
                case Types.Company:
                    await CheckOrAdd(await _dbContext.Company.FindAsync(aboutId));
                    break;
                case Types.OEM:
                    await CheckOrAdd(await _dbContext.OEM.FindAsync(aboutId));
                    break;
                case Types.Store:
                    // await CheckOrAdd(await _dbContext.StoreRegion.FindAsync(aboutId));
                    break;
            }
        }

        var begin = DateTime.Now.BeginDayTick();
        var last = DateTime.Now.LastDayTick();

        var info = await _dbContext.Statistic
            .Where(m =>
                m.AboutID == aboutId
                && (m.AboutType & Types.Effective) == effective
                && (m.AboutType & Types.Role) == role
                && (m.Validity >= begin && m.Validity <= last)
            )
            .FirstOrDefaultAsync();

        if (info == null)
        {
            throw new Exception($"找不到相关数据#role[{role.GetName()}] | effective[{effective.GetName()}]");
        }

        return info;
    }

}