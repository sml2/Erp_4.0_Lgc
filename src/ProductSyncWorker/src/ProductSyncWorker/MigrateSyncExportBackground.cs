﻿using ERP.Data;
using ERP.Models.DB.Identity;
using ERP.Models.Product;
using ERP.ProductSyncWorker.Models;
using ERP.Services.DB.ERP3;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace ERP.ProductSyncWorker;

/// <summary>
/// 迁移erp3产品后台
/// </summary>
public class MigrateSyncExportBackground : BackgroundService
{
    private readonly IServiceProvider _provider;
    private readonly ILogger<MigrateSyncExportBackground> _logger;

    public MigrateSyncExportBackground(IServiceProvider provider, ILogger<MigrateSyncExportBackground> logger)
    {
        _provider = provider;
        _logger = logger;
    }

    private record Transfer(Erp3User From, User To);

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        var scope = _provider.CreateScope().ServiceProvider;
        var dbContext = scope.GetRequiredService<DBContext>();
        var dbContext3 = scope.GetRequiredService<DBContext3>();
        var usernames = new string[]
        {
            // "13798999097",
            // "13631261703",
            // "13532224747",
            // "13555640767",
            // "lyq",
        };
        var transfers = new Queue<Transfer>();
        foreach (var username in usernames)
        {
            var erp3User = await dbContext3.Erp3Users.Where(u => u.d_username == username).SingleAsync(stoppingToken);
            var user = await dbContext.Users.Where(u => u.UserName == username).SingleAsync(stoppingToken);
            transfers.Enqueue(new Transfer(erp3User, user));
        }
        while (!stoppingToken.IsCancellationRequested && transfers.TryDequeue(out var outTransfer))
        {
            var transfer = outTransfer;
            var oldSyncExports = await dbContext3.Erp3SyncExports
                .Where(m => m.i_oem_id == transfer.From.u_oem_id && m.i_company_id == transfer.From.i_company_id &&
                            m.i_group_id == transfer.From.i_group_id).ToArrayAsync(stoppingToken);
            if (!oldSyncExports.Any())
            {
                _logger.LogInformation("用户[{Username}]没有导出模板任务", transfer.From.d_username);
                continue;
            }

            var newSyncExports = oldSyncExports.Select(o => new SyncExport()
            {
                Name = o.d_name,
                Amount = 0,
                Param = o.u_param,
                Append = o.u_append,
                Sort = o.d_sort,
                ExportSign = (int)o.u_export_sign,
                PlatformId = (int)o.i_platform_id,
                CreatedAt = o.created_at ?? DateTime.Now,
                UpdatedAt = o.updated_at ?? DateTime.Now,
                UserID = transfer.To.Id,
                GroupID = transfer.To.GroupID,
                CompanyID = transfer.To.CompanyID,
                OEMID = transfer.To.OEMID,
                LanguageId = o.i_language_id
            }).ToArray();
            dbContext.SyncExports.AddRange(newSyncExports);
            var count = await dbContext.SaveChangesAsync(stoppingToken);
            if (count == newSyncExports.Length)
            {
                _logger.LogInformation("用户[{Username}]的所有导出模板任务迁移成功, 共{N}条", transfer.From.d_username, count);
            }
            else
            {
                _logger.LogError("用户[{Username}]的部分导出模板任务迁移成功, 共{N}条, 失败{EN}条", transfer.From.d_username, count, newSyncExports.Length - count);
            }

        }
    }
}