﻿using ERP.Extensions;
using ERP.Interface;
using ERP.Interface.Rule;
using ERP.Models.DB.Identity;
using ERP.Models.DB.Product;
using ERP.Models.DB.Statistics;
using ERP.Models.DB.Users;
using ERP.Models.Product;
using ERP.Models.Setting;
using ERP.ProductSyncWorker.Models;
using ERP.Services.Rule;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace ERP.Data;

public class DBContext : Base
{
    public DbSet<OEM> OEM { get; set; } = null!;
    public DbSet<User> User { get; set; } = null!;
    public DbSet<Group> UserGroup { get; set; } = null!;
    public DbSet<Company> Company { get; set; } = null!;
    public DbSet<TaskModel> Task { get; set; } = null!;
    public DbSet<ProductModel> Product { get; set; } = null!;
    public DbSet<ProductItemModel> ProductItem { get; set; } = null!;
    public DbSet<TempImagesModel> TempImages { get; set; } = null!;
    public DbSet<ResourceModel> Resource { get; set; } = null!;
    public DbSet<ProductMigrationTask> ProductMigrationTasks { get; set; } = null!;
    public DbSet<Statistic> Statistic { get; set; } = null!;
    public DbSet<SyncExport> SyncExports { get; set; } = null!;

    public DBContext(DbContextOptions<DBContext> options, IOemProvider oemProvider) : base(options, oemProvider)
    {
        this.HandleDataChanged();
    }
}