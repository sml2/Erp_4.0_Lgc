using ERP.Models.DB.Logistics;
using ERP.Models.DB.Users;
using ERP.Models.Finance;
using ERP.Models.Product;

namespace ERP.ViewModels.Statistics;

public class DataBelongDto
{
    public DataBelongDto(ProductModel m)
    {
        UserId = m.UserID;
        if (m.GroupID != 1)
        {
            GroupId = m.GroupID;
        }
        CompanyId = m.CompanyID;
        OemId = m.OEMID;
    }

    public int? UserId { get; set; }
    public int? GroupId { get; set; }
    public int? CompanyId { get; set; }
    public int OemId { get; set; }
}