﻿using ERP.Data;
using ERP.Models.Product;
using ERP.Services.DB.ERP3;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace ERP.ProductSyncWorker;

/// <summary>
/// 迁移erp3产品后台
/// </summary>
public class MigrateProductBackground : BackgroundService
{
    private readonly IServiceProvider _provider;
    private readonly ILogger<MigrateProductBackground> _logger;

    public MigrateProductBackground(IServiceProvider provider, ILogger<MigrateProductBackground> logger)
    {
        _provider = provider;
        _logger = logger;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        var scope = _provider.CreateScope().ServiceProvider;
        var dbContext = scope.GetRequiredService<DBContext>();
        var dbContext3 = scope.GetRequiredService<DBContext3>();
        var productSyncService = scope.GetRequiredService<ProductSyncService>();
        while (!stoppingToken.IsCancellationRequested)
        {
            var task = await dbContext.ProductMigrationTasks
                .Where(m => m.State < ProductMigrationTask.TaskState.Completed).FirstOrDefaultAsync(stoppingToken);
            if (task is null)
            {
                _logger.LogInformation("暂时没有迁移任务");
                await Task.Delay(10_000, stoppingToken);
                continue;
            }

            var user = await dbContext.User.Where(u => u.Id == task.UserID).FirstOrDefaultAsync(stoppingToken);
            if (user is null)
            {
                task.State = ProductMigrationTask.TaskState.Completed;
                await dbContext.SaveChangesAsync(stoppingToken);
                _logger.LogError("迁移任务#{Id}无法找到关联用户#{UserId}", task.ID, task.UserID);
                continue;
            }
            _logger.LogInformation("准备执行迁移任务#{Id}", task.ID);
            if (task.State == ProductMigrationTask.TaskState.Queueing)
            {
                var totalCount = await dbContext3.erp3_products.WhereByTask(task).CountAsync(stoppingToken);
                if (totalCount == 0)
                {
                    task.State = ProductMigrationTask.TaskState.Completed;
                    await dbContext.SaveChangesAsync(stoppingToken);
                    continue;
                }
                
                task.TotalCount = totalCount;
                task.State = ProductMigrationTask.TaskState.Migrating;
                await dbContext.SaveChangesAsync(stoppingToken);
                _logger.LogInformation("迁移任务#{Id}的产品数量为{Count}", task.ID, totalCount);
            }

            const int batch = 10;
            var successProductIds = JsonConvert.DeserializeObject<Dictionary<ulong, ulong>>(task.SuccessProducts) ??
                                    new Dictionary<ulong, ulong>();
            var oldProducts = await dbContext3.erp3_products.WhereByTask(task).Skip(task.CompletedCount).Take(batch).ToArrayAsync(stoppingToken);
            while (oldProducts.Any())
            {
                _logger.LogInformation("开始执行迁移任务#{Id}", task.ID);
                foreach (var oldProduct in oldProducts)
                {
                    if (stoppingToken.IsCancellationRequested)
                        break;
                    
                    try
                    {
                        _logger.LogInformation("任务#{Id}开始迁移产品#{ProductId}", task.ID, oldProduct.id);
                        var id = await productSyncService.Handle(oldProduct, task, user);
                        successProductIds.TryAdd(oldProduct.id, (ulong)id);
                        task.SuccessProducts = JsonConvert.SerializeObject(successProductIds);
                        _logger.LogInformation("任务#{Id}迁移产品#{ProductId}成功", task.ID, oldProduct.id);
                    }
                    catch (Exception e)
                    {
                        _logger.LogError(e, "迁移任务#{TaskId}迁移产品#{ProductId}时报错", task.ID, oldProduct.id);
                        task.FailedCount++;
                        var failedPids = JsonConvert.DeserializeObject<List<ulong>>(task.FailedProductIds)!;
                        failedPids.Add(oldProduct.id);
                        task.FailedProductIds = JsonConvert.SerializeObject(failedPids);
                    }
                    task.CompletedCount++;
                    await dbContext.SaveChangesAsync(default);
                }
                
                oldProducts = await dbContext3.erp3_products.WhereByTask(task).Skip(task.CompletedCount).Take(batch).ToArrayAsync(stoppingToken);
            }

            if (task.CompletedCount < task.TotalCount)
            {
                // 如果第一次获取总量后客户删除产品, 该迁移将无法完成, 此处获取一下总数
                var oldCount = await dbContext3.erp3_products.WhereByTask(task).CountAsync(default);
                task.TotalCount = oldCount;
            }
            if (task.CompletedCount >= task.TotalCount)
            {
                task.State = ProductMigrationTask.TaskState.Completed;
                await dbContext.SaveChangesAsync(default);
                _logger.LogInformation("迁移任务#{Id}完成", task.ID);
            }
            
        }
    }
}