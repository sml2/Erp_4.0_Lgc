﻿global using NJS = Newtonsoft.Json.JsonSerializer;
global using SJS = System.Text.Json.JsonSerializer;

global using NJP = Newtonsoft.Json.JsonPropertyAttribute;
global using NJI = Newtonsoft.Json.JsonIgnoreAttribute;
global using SJI = System.Text.Json.Serialization.JsonIgnoreAttribute;
global using Ryu.Data;
