﻿using System.ComponentModel.DataAnnotations.Schema;
using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;

namespace ERP.ProductSyncWorker.Models;

/// <summary>
/// 任务集
/// </summary>
[Index(nameof(ExportSign))]
[Index(nameof(LanguageId))]
[Index(nameof(PlatformId))]
[Index(nameof(Sort))]
[Comment("产品导出_导出任务集")]
[Table("SyncExport")]
public class SyncExport : UserModel
{
    public SyncExport()
    {
    }
    

    // public SyncExport(AddSyncExportDto req)
    // {
    //     Name = req.Name;
    //     Amount = 0;
    //     Param = Newtonsoft.Json.JsonConvert.SerializeObject(req.Params);
    //     Append = Newtonsoft.Json.JsonConvert.SerializeObject(req.Append);
    //     Sort = DateTime.Now.GetTimeStamp();
    //     ExportSign = req.Format;
    //     PlatformId = req.PlatformId;
    //     CreatedAt = DateTime.Now;
    //     UpdatedAt = DateTime.Now;
    // }

    /// <summary>
    /// 导出产品任务集名称
    /// </summary>
    [Comment("导出产品任务集名称")]
    public string? Name { get; set; }

    /// <summary>
    /// 导出任务集产品数量
    /// </summary>
    [Comment("导出任务集产品数量")]
    public int Amount { get; set; }

    /// <summary>
    /// 排序字段
    /// </summary>
    [Comment("排序字段")]
    public int Sort { get; set; }

    /// <summary>
    /// 导出条件参数json（头部）
    /// </summary>
    [Comment("导出条件参数json（头部）")]
    public string? Param { get; set; }

    /// <summary>
    /// 导出条件附加参数json
    /// </summary>
    [Comment("导出条件附加参数json")]
    public string? Append { get; set; }

    /// <summary>
    /// 产品语言id
    /// </summary>
    [Comment("产品语言id")]
    public int LanguageId { get; set; }

    /// <summary>
    /// 导出模板sign
    /// </summary>
    [Comment("导出模板sign")]
    public int ExportSign { get; set; }

    /// <summary>
    /// 导出模板平台id
    /// </summary>
    [Comment("导出模板平台id")]
    public int PlatformId { get; set; }
}
