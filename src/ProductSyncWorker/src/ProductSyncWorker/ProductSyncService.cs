using ERP.Data;
using ERP.Data.Products;
using ERP.DomainEvents.Product;
using ERP.Enums;
using ERP.Exceptions.Upload;
using ERP.Models.DB.Erp3;
using ERP.Models.DB.Identity;
using ERP.Models.DB.Product;
using ERP.Models.Product;
using ERP.Product.Abstraction.Exceptions;
using ERP.Services.Upload;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Z.EntityFramework.Plus;

namespace ERP.Services.DB.ERP3;

internal class ProductSyncService
{
    private readonly DBContext _dbContext;
    private readonly ILogger<ProductSyncService> _logger;
    private readonly IHostEnvironment _environment;
    private UploadService _uploadService;
    private readonly IMediator _mediator;

    public ProductSyncService(DBContext dbContext, ILogger<ProductSyncService> logger,
        IHostEnvironment environment, IMediator mediator)
    {
        _dbContext = dbContext;
        _logger = logger;
        _environment = environment;
        _mediator = mediator;
    }

    public async Task<int> Handle(Erp3Product vm, ProductMigrationTask task, User user)
    {
        var oem = await _dbContext.OEM.Include(m => m.Oss).FirstOrDefaultAsync(m => m.ID == user.OEMID);
        if (oem is null || oem.Oss is null)
        {
            throw new NotFoundException("找不到Oem相关数据#1");
        }

        _uploadService = new UploadService(oem.Oss, _environment);
        var company = await _dbContext.Company.SingleOrDefaultAsync(m => m.ID == user.CompanyID);

        if (company is null)
        {
            throw new NotFoundException("找不到公司相关数据#2");
        }

        //任务
        var taskId = 0;

        var taskName = string.Empty;
        if (task.Type == ProductMigrationTask.TaskType.DateRange)
        {
            var range = task.GetDateRange();

            var s = range.Start.ToString("yyyy-MM-dd");
            var e = range.End.ToString("yyyy-MM-dd");
            taskName = $"{s}~{e}";
        }
        else
        {
            var t = task.CreatedAt.ToString("yyyy-MM-dd");
            taskName = $"{t}-勾选";
        }

        var taskHashCode = Helpers.CreateHashCode(taskName).ToString();
        var taskInfo = await _dbContext.Task
            .Where(m => m.CompanyID == user.CompanyID
                        && m.UserID == user.Id
                        && m.HashCode == taskHashCode
            ).FirstOrDefaultAsync();


        //初始化产品
        var productModel = vm.ToProduct(user);

        //是否采集
        ulong collectBiter = 1;
        var isCollect = (vm.b_type & collectBiter) == collectBiter;
        productModel.SetCollect(isCollect);

        //设置审核状态
        productModel.SetAudit((int)company.ProductAudit);

        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            if (taskInfo is not null)
            {
                taskId = taskInfo.ID;
            }
            else
            {
                var taskModel = new TaskModel()
                {
                    Name = taskName,
                    PlatformName = "迁移",
                    PlatformId = 0,
                    OEMID = user.OEMID,
                    CompanyID = user.CompanyID,
                    GroupID = user.GroupID,
                    UserID = user.Id,
                    HashCode = taskHashCode,
                    IsDel = false
                };

                await _dbContext.Task.AddAsync(taskModel);

                await _dbContext.SaveChangesAsync();
                taskId = taskModel.ID;
            }

            productModel.TaskId = taskId;


            //处理图片 
            var copySuccessImages = new Data.Products.Images();
            var imageComparison = new Dictionary<int, int>();
            var images = vm.Images;
            var allTempImages = new List<TempImagesModel>();
            //是否远程
            var isImageSync = false;
            if (images != null && images.Count > 0)
            {
                var storageId = oem.OssId;
                foreach (var image in images)
                {
                    var tempImageModel = new TempImagesModel();
                    if (image.IsNetwork)
                    {
                        isImageSync = true;
                        tempImageModel = image.ToTempImagesModel(image.Path, storageId,
                            ResourceModel.Types.Network,
                            user);
                    }
                    else
                    {
                        var path = await _uploadService.GetImagePath(image.Path, productModel);
                        var copyState = await _uploadService.CopyObject(image.Path!, path, "erp4res");
                        if (!copyState)
                        {
                            throw new UploadException("资源复制失败#3");
                        }

                        tempImageModel = image.ToTempImagesModel(path, storageId,
                            ResourceModel.Types.Storage,
                            user);
                    }

                    await _dbContext.TempImages.AddAsync(tempImageModel);
                    await _dbContext.SaveChangesAsync();
                    imageComparison.Add(image.Id, tempImageModel.ID);
                    copySuccessImages.Add(new Image(tempImageModel));
                    allTempImages.Add(tempImageModel);
                }
            }

            productModel.Images = JsonConvert.SerializeObject(copySuccessImages);
            if (copySuccessImages.Count > 0)
            {
                productModel.MainImage = JsonConvert.SerializeObject(copySuccessImages.First());
            }

            productModel.ImagesSize = copySuccessImages.Sum(m => m.Size);
            productModel.IsImageAsync = isImageSync;
            //新增主产品
            await _dbContext.Product.AddAsync(productModel);
            await _dbContext.SaveChangesAsync();

            //语种
            List<ProductItemModel> itemModels = new List<ProductItemModel>();
            var updatedAt = DateTime.Now;
            var version = updatedAt.GetTimeStampMilliseconds();

            var languages = vm.Languages;

            //添加默认语种
            languages.Add("default", new Erp3ProductLanguage()
            {
                D = vm.u_lp_d,
                K = vm.u_lp_k,
                N = vm.u_lp_n,
                S = vm.Sketch,
                Map = null,
            });

            foreach (var (key, item) in languages)
            {
                var language = Enum.Parse<Languages>(key, true);
                var languageId = language == Languages.Default ? 0 : (int)Math.Log2((long)language) + 1;

                //设置产品语言
                if (language != Languages.Default)
                {
                    productModel.SetLanguage(language, true);
                }

                var newItemModel = new ProductItemModel(user, vm.d_coin, (int)vm.u_quantity);
                var historyVersion = new List<long>();
                historyVersion.Add(version);

                newItemModel.Pid = productModel.ID;
                newItemModel.LanguageName = language.GetDescriptionByKey("Code");
                newItemModel.LanguageId = languageId;
                newItemModel.CurrentVersion = version;
                newItemModel.HistoryVersion = Newtonsoft.Json.JsonConvert.SerializeObject(historyVersion);

                //产品结构
                Data.Products.Product productStruct =
                    ToBeServiceStruct(vm, item, copySuccessImages, imageComparison, key);

                await InitItemVersion(productStruct, productModel, newItemModel, ++version, user);
                itemModels.Add(newItemModel);
                productModel.FilesSize += newItemModel.FilesSize;
                productModel.Quantity += productStruct.GetQuantity();
            }

            productModel.UpdatedAt = updatedAt;
            productModel.MainImage = copySuccessImages.Count > 0
                ? JsonConvert.SerializeObject(copySuccessImages.First())
                : null;
            await _dbContext.ProductItem.AddRangeAsync(itemModels);
            await _dbContext.Product.SingleUpdateAsync(productModel);

            //移动图片
            await _dbContext.Resource
                .AddRangeAsync(allTempImages.Select(m => new ResourceModel(m, productModel.ID)).ToList());
            var tempImagesIds = allTempImages.Select(m => m.ID).ToList();
            await _dbContext.TempImages
                .Where(m => tempImagesIds.Contains(m.ID))
                .DeleteAsync();

            await _dbContext.SaveChangesAsync();

            #region 新增产品数/当前产品数

            var productCreatedEvent = new ProductCreatedEvent(productModel);
            await _mediator.Publish(productCreatedEvent);

            #endregion

            //设置重复次数
            await SetUserLevelRepeatNum(productModel.HashCode, user);
            await transaction.CommitAsync();
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            _logger.LogError(e, e.Message);
            throw;
        }


        return productModel.ID;
    }

    private Data.Products.Product ToBeServiceStruct(Erp3Product model, Erp3ProductLanguage item,
        Data.Products.Images images, Dictionary<int, int> imageComparison, string language)
    {
        var erp3ProductVariants = model.Variants;
        Data.Products.Product product = new();
        product.SetSource(model.d_source);
        product.SetQuantity(erp3ProductVariants!.Sum(m => m.Quantity));
        product.SetPid(model.d_pid);

        //Price
        var prices = new Price();

        //Images
        product.Images = images;
        prices.SetMax(erp3ProductVariants.Max(m => m.Cost));
        prices.SetMin(erp3ProductVariants.Max(m => m.Cost));
        product.SetPrices(new Prices()
        {
            Cost = prices,
            Sale = prices,
        });
        product.SetCarrie(model.d_coin);

        //Basic
        product.SetTitle(item.N);
        product.SetKeyword(item.K);
        product.SetDescription(item.D);
        product.SetSketches(!string.IsNullOrEmpty(item.S.First()) && !string.IsNullOrWhiteSpace(item.S.First())
            ? item.S
            : new List<string>());

        //Variants

        var variants = new Variants();

        foreach (var v in erp3ProductVariants!)
        {
            //处理图片
            if (v.Img.Main.HasValue)
            {
                v.Img.Main = imageComparison.TryGetValue(v.Img.Main.Value, out var value)
                    ? value
                    : null;
            }

            if (v.Img.Affiliate.Count > 0)
            {
                var affiliate = new List<int>();
                var oldAffiliate = v.Img.Affiliate;
                foreach (int i in oldAffiliate)
                {
                    if (imageComparison.TryGetValue(i, out var value))
                    {
                        affiliate.Add(value);
                    }
                }

                v.Img.Affiliate = affiliate;
            }

            variants.Add(v.ToVariant(language));
        }

        product.Variants = variants;


        //Attribute
        Data.Products.Attributes productAttributes = new Data.Products.Attributes();

        var erp3Attribute = model.Attribute;
        foreach (var i in erp3Attribute)
        {
            productAttributes.Add(i.Tag, i.Name, i.Value.ToArray(), AttributeStateEnum.Enable);
        }

        var tag = erp3Attribute.Max(m => m.Tag);
        productAttributes.CurCreateNum = tag == 0 ? 1 : tag;
        product.Variants.Attributes = productAttributes;
        return product;
    }

    private async Task InitItemVersion(ERP.Data.Products.Product productStruct,
        ProductModel model, ProductItemModel itemModel, long version, User user)
    {
        var jsonProduct = JsonConvert.SerializeObject(productStruct, new JsonSerializerSettings()
        {
            TypeNameHandling = TypeNameHandling.All
        });

        var jsonFilePath = await _uploadService.UploadStructFile(jsonProduct, version, model.ID, user);
        itemModel.CurrentVersion = version;
        itemModel.HistoryVersion = JsonConvert.SerializeObject(new List<long>() { version });
        itemModel.FilePath = jsonFilePath;
        itemModel.FilePathHashCode = Helpers.CreateHashCode(jsonFilePath);
        itemModel.FilesSize = jsonProduct.Length;
    }

    public async Task SetUserLevelRepeatNum(ulong hashcode, User user)
    {
        var count = await _dbContext.Product
            .Where(m => m.HashCode == hashcode && m.UserID == user.Id)
            .CountAsync();

        if (count == 1)
        {
            count = 0;
        }

//, UpdatedAt = DateTime.Now
        await _dbContext.Product.Where(m => m.HashCode == hashcode && m.UserID == user.Id)
            .UpdateAsync(m => new { UserLevelRepeatNum = count });
    }
}