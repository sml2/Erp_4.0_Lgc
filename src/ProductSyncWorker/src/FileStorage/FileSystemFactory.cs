using ERP.Services.FileStorage.Models;

namespace ERP.Services.FileStorage;

public class FileSystemFactory
{
    public static DiskManager diskConfig { get; set; } = new DiskManager();

    public static AdapterInterface Storage(string key)
    {
        FileSystems? config = GetConfig(key);

        AdapterInterface? fileStorage = default!;
        if (config is not null)
        {
            if (config.Driver?.ToUpper() == "OBS")
            {
                fileStorage = new ObsAdapter(config.AccessKey, config.SecretKey, config.Endpoint, config.Bucket,
                    config.BucketDomain,
                    config.UseSsl,
                    config.IsCName, config.Prefix);
            }
        }

        return fileStorage;
    }

    public static AdapterInterface Storage(FileSystems? config)
    {
        AdapterInterface? fileStorage = default!;
        if (config is not null)
        {
            if (config.Driver?.ToUpper() == "OBS")
            {
                fileStorage = new ObsAdapter(config.AccessKey, config.SecretKey, config.Endpoint, config.Bucket,
                    config.BucketDomain,
                    config.UseSsl,
                    config.IsCName, config.Prefix);
            }
        }

        return fileStorage;
    }

    protected static FileSystems? GetConfig(string key)
    {
        var sourceSystem = key;
        key = key.ToUpper();


        FileSystems? config = (FileSystems?)diskConfig.GetType().GetProperty(key)?.GetValue(diskConfig, null);


        if (config == null)
        {
            config = new FileSystems();
        }

        if (config.Driver == null)
        {
            throw new InvalidCastException($"Disk [{sourceSystem}] does not have a configured driver.");
        }


        DiskManager diskManager = new DiskManager();

        var managerProperty = diskManager.GetType().GetProperty(key);

        if (managerProperty == null)
        {
            throw new InvalidCastException($"Driver [{sourceSystem}] is not supported.");
        }

        return config;
    }
}