using System.Text;
using OBS;
using OBS.Model;
using static System.TimeZone;

namespace ERP.Services.FileStorage;

public class ObsAdapter : AbstractAdapter
{
    protected string? AccessKeyId;
    protected string? AccessKeySecret;
    protected string? Endpoint;
    protected string? Bucket;
    protected string? BucketDomain;
    protected ObsClient Client = default!;
    protected bool UseSsl;
    protected bool IsCName;


    public ObsAdapter(string? accessKeyId, string? accessKeySecret, string? endpoint, string? bucket,
        string? bucketDomain,
        bool isCName,
        bool useSsl,
        string? pathPrefix = null)
    {
        AccessKeyId = accessKeyId ?? string.Empty;
        AccessKeySecret = accessKeySecret ?? string.Empty;
        Endpoint = endpoint ?? string.Empty;
        Bucket = bucket;
        BucketDomain = bucketDomain;
        IsCName = isCName;
        UseSsl = useSsl;
        PathPrefixAttr = pathPrefix;
        InitClient();
    }

    /// <summary>
    /// init obs client.
    /// </summary>
    protected void InitClient()
    {
        if (Client is null)
        {
            Client = new ObsClient(AccessKeyId, AccessKeySecret, Endpoint);
        }
    }


    /// <summary>
    /// Check whether a file exists.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public override async Task<bool> Has(string path)
    {
        HeadObjectRequest request = new HeadObjectRequest()
        {
            BucketName = Bucket,
            ObjectKey = ApplyPathPrefix(path)
        };

        return Client.HeadObject(request);
    }

    /// <summary>
    /// copy a file.
    /// </summary>
    /// <param name="path"></param>
    /// <param name="newPath"></param>
    /// <returns></returns>
    public override async Task<bool> Copy(string path, string newPath)
    {
        try
        {
            CopyObjectRequest request = new CopyObjectRequest()
            {
                SourceBucketName = Bucket,
                SourceObjectKey = ApplyPathPrefix(path),
                BucketName = Bucket,
                ObjectKey = ApplyPathPrefix(newPath)
            };

            CopyObjectResponse response = Client.CopyObject(request);
            // Console.WriteLine("Copy object response: {0}", response.StatusCode);
            return true;
        }
        catch (ObsException ex)
        {
            Console.WriteLine("ErrorCode: {0}", ex.ErrorCode);
            Console.WriteLine("ErrorMessage: {0}", ex.ErrorMessage);
            return false;
        }
    }

    public override async Task<bool> CopyObject(string path, string newPath, string sourceBucketName)
    {
        try
        {
            CopyObjectRequest request = new CopyObjectRequest()
            {
                SourceBucketName = sourceBucketName,
                SourceObjectKey = ApplyPathPrefix(path),
                BucketName = Bucket,
                ObjectKey = ApplyPathPrefix(newPath)
            };

            CopyObjectResponse response = Client.CopyObject(request);
            // Console.WriteLine("Copy object response: {0}", response.StatusCode);
            return true;
        }
        catch (ObsException ex)
        {
            Console.WriteLine("ErrorCode: {0}", ex.ErrorCode);
            Console.WriteLine("ErrorMessage: {0}", ex.ErrorMessage);
            return false;
        }
    }

    /// <summary>
    /// delete a file.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public override async Task<bool> Delete(string path)
    {
        try
        {
            DeleteObjectRequest request = new DeleteObjectRequest()
            {
                BucketName = Bucket,
                ObjectKey = ApplyPathPrefix(path),
            };
            DeleteObjectResponse response = Client.DeleteObject(request);
            // Console.WriteLine("Delete object response: {0}", response.StatusCode);
        }
        catch (ObsException ex)
        {
            Console.WriteLine("ErrorCode: {0}", ex.ErrorCode);
            Console.WriteLine("ErrorMessage: {0}", ex.ErrorMessage);
            throw new ObsException(ex.Message);
        }

        return !await Has(path);
    }

    public override async Task<bool> BatchDeleteAsync(List<string> paths, bool quiet)
    {
        try
        {
            DeleteObjectsRequest request = new DeleteObjectsRequest()
            {
                BucketName = Bucket,
                Quiet = quiet,
            };

            foreach (var path in paths)
            {
                request.AddKey(ApplyPathPrefix(path));
            }

            DeleteObjectsResponse response = Client.DeleteObjects(request);
            // Console.WriteLine("Delete objects response: {0}", response.StatusCode);
            return await Task.FromResult(true);
        }
        catch (ObsException ex)
        {
            Console.WriteLine("ErrorCode: {0}", ex.ErrorCode);
            Console.WriteLine("ErrorMessage: {0}", ex.ErrorMessage);
            throw new ObsException(ex.Message);
        }

        return await Task.FromResult(false);
    }

    /// <summary>
    /// visibility
    /// </summary>
    /// <param name="path"></param>
    /// <param name="visibility"></param>
    /// <returns></returns>
    public override async Task<bool> SetVisibility(string path, AdapterInterface.VisibilityEnum visibility)
    {
        CannedAclEnum acl = (visibility == AdapterInterface.VisibilityEnum.PUBLIC)
            ? CannedAclEnum.PublicRead
            : CannedAclEnum.Private;
        try
        {
            PutObjectRequest request = new PutObjectRequest
            {
                BucketName = Bucket,
                ObjectKey = ApplyPathPrefix(path),
                CannedAcl = acl,
            };
            PutObjectResponse response = Client.PutObject(request);
            // Console.WriteLine("Set object ac response: {0}", response.StatusCode);
            return true;
        }
        catch (ObsException ex)
        {
            Console.WriteLine("ErrorCode: {0}", ex.ErrorCode);
            Console.WriteLine("ErrorMessage: {0}", ex.ErrorMessage);
            return false;
        }
    }

    /// <summary>
    /// get meta data.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public override GetObjectMetadataResponse GetMetadata(string path)
    {
        GetObjectMetadataRequest request = new GetObjectMetadataRequest()
        {
            BucketName = Bucket,
            ObjectKey = ApplyPathPrefix(path)
        };

        return Client.GetObjectMetadata(request);
    }

    public override async Task<string> GetUrl(string path)
    {
        return $"{NormalizeHost()}{ApplyPathPrefix(path).TrimStart('/')}";
    }


    public override async Task<long> GetSize(string path)
    {
        return GetMetadata(path).ContentLength;
    }

    public override async Task<string> GetMimetype(string path)
    {
        return GetMetadata(path).ContentType;
    }

    /// <summary>
    /// get timestamp
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public override async Task<long> GetTimestamp(string path)
    {
        long timestamp = 0;
        var lastModified = GetMetadata(path).LastModified;
        if (lastModified != null)
        {
            var startTime = CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            timestamp = (long)(lastModified.Value - startTime).TotalSeconds; // 相差毫秒数
        }

        return timestamp;
    }

    /// <summary>
    /// write a file.
    /// </summary>
    /// <param name="name"></param>
    /// <param name="path"></param>
    /// <param name="contents"></param>
    /// <param name="config"></param>
    /// <returns></returns>
    public override async Task<bool> Put(string path, string contents, object? config)
    {
        path = ApplyPathPrefix(path);
        try
        {
            PutObjectRequest request = new PutObjectRequest()
            {
                BucketName = Bucket, //待传入目标桶名
                ObjectKey = ApplyPathPrefix(path), //待传入对象名(不是本地文件名，是文件上传到桶里后展现的对象名)
                InputStream = new MemoryStream(Encoding.UTF8.GetBytes(contents))
            };
            PutObjectResponse response = Client.PutObject(request);
            Console.WriteLine("put object response: {0}", response.StatusCode);
            return true;
        }
        catch (ObsException ex)
        {
            Console.WriteLine("ErrorCode: {0}", ex.ErrorCode);
            Console.WriteLine("ErrorMessage: {0}", ex.ErrorMessage);
            return false;
        }
    }

    /// <summary>
    /// Write a new file using a stream.
    /// </summary>
    /// <param name="path"></param>
    /// <param name="contents"></param>
    /// <param name="config"></param>
    /// <returns></returns>
    public override async Task<bool> PutStream(string path, Stream contents, object? config)
    {
        try
        {
            PutObjectRequest request = new PutObjectRequest()
            {
                BucketName = Bucket,
                ObjectKey = ApplyPathPrefix(path),
                InputStream = contents,
            };
            PutObjectResponse response = Client.PutObject(request);
            return true;
            // Console.WriteLine("put object response: {0}", response.StatusCode);
        }
        catch (ObsException ex)
        {
            Console.WriteLine("ErrorCode: {0}", ex.ErrorCode);
            Console.WriteLine("ErrorMessage: {0}", ex.ErrorMessage);
            return false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    protected GetObjectResponse GetObject(string path)
    {
        try
        {
            GetObjectRequest request = new GetObjectRequest()
            {
                BucketName = Bucket,
                ObjectKey = ApplyPathPrefix(path),
            };
            GetObjectResponse response = Client.GetObject(request);
            // Console.WriteLine("Get object response: {0}", response.StatusCode);
            return response;
        }
        catch (ObsException ex)
        {
            Console.WriteLine("ErrorCode: {0}", ex.ErrorCode);
            Console.WriteLine("ErrorMessage: {0}", ex.ErrorMessage);
            return new GetObjectResponse();
        }
    }


    /// <summary>
    /// read a file stream.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public override Task<Stream> Read(string path)
    {
        return Task.FromResult(GetObject(path).OutputStream);
    }

    public override async Task<List<string>> ListObjects(string path)
    {
        try
        {
            ListObjectsRequest request = new ListObjectsRequest()
            {
                BucketName = Bucket,
                Prefix = ApplyPathPrefix(path),
            };


            ListObjectsResponse response = Client.ListObjects(request);
            // Console.WriteLine("Get object response: {0}", response.StatusCode);
            return response.ObsObjects.Select(m =>m.ObjectKey).ToList();
        }
        catch (ObsException ex)
        {
            Console.WriteLine("ErrorCode: {0}", ex.ErrorCode);
            Console.WriteLine("ErrorMessage: {0}", ex.ErrorMessage);
            return new List<string>();
        }
    }

    /// <summary>
    /// normalize Host.
    /// </summary>
    /// <returns></returns>
    protected string NormalizeHost()
    {
        string domain;

        if (IsCName && !string.IsNullOrEmpty(BucketDomain))
        {
            domain = BucketDomain!;
        }
        else
        {
            domain = $"{Bucket}.{Endpoint}";
        }

        if (UseSsl)
        {
            domain = $"https://{domain}";
        }
        else
        {
            domain = $"http://{domain}";
        }

        return domain.TrimEnd('/') + '/';
    }
}