
using OBS.Model;

namespace ERP.Services.FileStorage;

public abstract class AbstractAdapter : AdapterInterface
{
    protected string? PathPrefix;

    protected string? PathPrefixAttr
    {
        get => PathPrefix;
        set
        {
            if (value is null)
            {
                PathPrefix = null;
            }
            else
            {
                PathPrefix = value?.TrimEnd('/') + PathSeparator;
            }
        }
    }

    protected string PathSeparator { get; set; } = "/";


    /// <summary>
    /// Prefix a path.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public string ApplyPathPrefix(string path)
    {
        return PathPrefix + path.TrimStart('/');
    }

    /// <summary>
    /// Remove a path prefix.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public string removePathPrefix(string path)
    {
        var str = PathPrefix;
        if (str is not null)
        {
            return path.Substring(str.Length);
        }

        return path;
    }

    /// <summary>
    /// Check whether a file exists.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public abstract Task<bool> Has(string path);

    /// <summary>
    /// Get resource url.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public abstract Task<string> GetUrl(string path);

    /// <summary>
    /// get the size of file.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public abstract Task<long> GetSize(string path);

    /// <summary>
    /// get mime type.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public abstract Task<string> GetMimetype(string path);

    /// <summary>
    /// get timestamp.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public abstract Task<long> GetTimestamp(string path);

    /// <summary>
    ///  write a file.
    /// </summary>
    /// <param name="name"></param>
    /// <param name="path"></param>
    /// <param name="contents"></param>
    /// <param name="config"></param>
    /// <returns></returns>
    public abstract Task<bool> Put(string path, string contents, object? config);

    /// <summary>
    /// Write a new file using a stream.
    /// </summary>
    /// <param name="name"></param>
    /// <param name="path"></param>
    /// <param name="contents"></param>
    /// <param name="config"></param>
    /// <returns></returns>
    public abstract Task<bool> PutStream(string path, Stream contents, object? config);

    /// <summary>
    /// copy a file.
    /// </summary>
    /// <param name="path"></param>
    /// <param name="newPath"></param>
    /// <returns></returns>
    public abstract Task<bool> Copy(string path, string newPath);

    /// <summary>
    /// remove a file.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public abstract Task<bool> Delete(string path);

    /// <summary>
    /// batch remove a file.
    /// </summary>
    /// <param name="paths"></param>
    /// <returns></returns>
    public abstract Task<bool> BatchDeleteAsync(List<string> paths, bool quiet);

    /// <summary>
    /// read a file.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public abstract Task<Stream> Read(string path);

    /// <summary>
    ///  visibility.
    /// </summary>
    /// <param name="path"></param>
    /// <param name="visibility"></param>
    /// <returns></returns>
    public abstract Task<bool> SetVisibility(string path, AdapterInterface.VisibilityEnum visibility);

    /// <summary>
    /// get meta data.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public abstract object GetMetadata(string path);

    public abstract Task<bool> CopyObject(string path, string newPath, string sourceBucketName);
    public abstract Task<List<string>> ListObjects(string path);
}