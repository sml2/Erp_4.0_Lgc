using OBS.Model;

namespace ERP.Services.FileStorage;

public interface AdapterInterface
{
    public enum VisibilityEnum
    {
        PUBLIC,
        PRIVATE
    }

    public Task<bool> Has(string path);

    public object GetMetadata(string path);

    public Task<string> GetUrl(string path);

    public Task<long> GetSize(string path);

    public Task<string> GetMimetype(string path);

    public Task<long> GetTimestamp(string path);


    public Task<bool> Put(string path, string contents, object? config);

    public Task<bool> PutStream(string path, Stream contents, object? config);

    public Task<bool> Copy(string path, string newPath);

    public Task<bool> Delete(string path);
    public Task<bool> BatchDeleteAsync(List<string> paths,bool quiet);

    public Task<Stream> Read(string path);

    public Task<bool> SetVisibility(string path, VisibilityEnum visibility);
    public Task<bool> CopyObject(string path, string newPath,string sourceBucketName);
    public Task<List<string>> ListObjects(string path);
}