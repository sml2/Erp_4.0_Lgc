using ERP.Models.Setting;

namespace ERP.Services.FileStorage.Models;

public class FileSystems
{
   
    public FileSystems()
    {

    }

    public FileSystems(FileBucket model)
    {
        Driver = model.Type == FileBucket.StorageEnum.Obs ? "obs" : "oss";
        AccessKey = model.AccessKey;
        SecretKey = model.SecretKey;
        Endpoint = model.EndpointDomain;
        Bucket = model.Bucket;
        IsCName = model.IsCName;
        BucketDomain = model.BucketDomain;
        UseSsl = model.UseSsl;
    }

    public string? BucketDomain { get; set; }
    public string? Driver { get; set; }
    public string? AccessKey { get; set; }
    public string? SecretKey { get; set; }
    public string? Endpoint { get; set; }
    public string? Bucket { get; set; }
    public bool IsCName { get; set; } = false;

    public string? Prefix { get; set; }
    public bool UseSsl { get; set; } = false;
}