﻿using ERP.Models.DB.Erp3;
using Microsoft.EntityFrameworkCore;
using ProductSync.Abstraction.Models;

namespace ERP.Data;

public class DBContext3 : DbContext
{

    public DBContext3(DbContextOptions<DBContext3> options) : base(options)
    {
    }
    
    
    public virtual DbSet<Erp3Product> erp3_products { get; set; } = null!;
    public virtual DbSet<Erp3MigrationTask> MigrationTasks { get; set; } = null!;
    
    public virtual DbSet<Erp3SyncExport> Erp3SyncExports { get; set; } = null!;
    public virtual DbSet<Erp3User> Erp3Users { get; set; } = null!;
    
}
