﻿using System.ComponentModel.DataAnnotations.Schema;
using ERP.Data.Products;
using ERP.Enums.Product;
using ERP.Models.DB.Identity;
using ERP.Models.DB.Product;
using ERP.Models.Product;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ERP.Models.DB.Erp3
{
    /// <summary>
    /// 产品管理_产品列表
    /// </summary>
    [Table("erp3_product")]
    public class Erp3Product
    {
        public ulong id { get; set; }

        /// <summary>
        /// oem_id
        /// </summary>
        public ulong i_oem_id { get; set; }

        /// <summary>
        /// 公司id
        /// </summary>
        public ulong i_company_id { get; set; }

        /// <summary>
        /// 用户id
        /// </summary>
        public ulong i_user_id { get; set; }

        /// <summary>
        /// 分组id
        /// </summary>
        public ulong i_group_id { get; set; }

        /// <summary>
        /// 任务id
        /// </summary>
        public ulong i_task_id { get; set; }

        /// <summary>
        /// 分类id
        /// </summary>
        public ulong i_category_id { get; set; }

        /// <summary>
        /// 子分类id
        /// </summary>
        public ulong i_sub_category_id { get; set; }

        /// <summary>
        /// 分销分类id
        /// </summary>
        public ulong i_distribution_category_id { get; set; }

        /// <summary>
        /// 分销子分类id
        /// </summary>
        public ulong i_distribution_sub_category_id { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string u_lp_n { get; set; } = null!;

        /// <summary>
        /// 关键字
        /// </summary>
        public string u_lp_k { get; set; } = null!;

        /// <summary>
        /// 卖点
        /// </summary>
        public string u_lp_s { get; set; } = null!;

        public List<string> Sketch => JsonConvert.DeserializeObject<List<string>>(u_lp_s)!;

        /// <summary>
        /// 描述
        /// </summary>
        public string u_lp_d { get; set; } = null!;

        /// <summary>
        /// 标题hashCode
        /// </summary>
        public ulong d_hash_code { get; set; }

        /// <summary>
        /// 翻译后的lps:{CN:{lp},EN:{lp}}
        /// </summary>
        public string d_languages { get; set; } = null!;

        [NotMapped]
        public Dictionary<string, Erp3ProductLanguage> Languages =>
            d_languages != "[]"
                ? JsonConvert.DeserializeObject<Dictionary<string, Erp3ProductLanguage>>(d_languages) ??
                  new Dictionary<string, Erp3ProductLanguage>()
                : new Dictionary<string, Erp3ProductLanguage>();

        /// <summary>
        /// 0 翻译中，1-4默认显示语种，5-8客户端上传语种，9-15保留,16-31已翻译语言
        /// </summary>
        public ulong b_language { get; set; }

        /// <summary>
        /// 0 采集或自增，1来自分享，2是否分享，3是否同步图片，4编辑过，5-8审核状态，9-15保留字段
        /// </summary>
        public ulong b_type { get; set; }

        /// <summary>
        /// 审核原因(我们提供或客户自定义)
        /// </summary>
        public string d_reason { get; set; } = null!;

        /// <summary>
        /// UI冗余价格,{sale:{min:0,max:100},cost:{min:m,max:n}}-&gt;m-n,n
        /// </summary>
        public string? u_price { get; set; }

        [NotMapped]
        public Erp3ProductPrices? Price =>
            u_price != null ? JsonConvert.DeserializeObject<Erp3ProductPrices>(u_price) : null;

        /// <summary>
        /// 库存
        /// </summary>
        public uint u_quantity { get; set; }

        /// <summary>
        /// 来源url
        /// </summary>
        public string? d_source { get; set; }

        /// <summary>
        /// 平台id
        /// </summary>
        public ulong i_platform_id { get; set; }

        /// <summary>
        /// 平台名称
        /// </summary>
        public string? u_platform { get; set; }

        /// <summary>
        /// 扩展属性spu,长cm,宽cm,高cm,重g,product_type
        /// </summary>
        public string? d_ext { get; set; }

        /// <summary>
        /// 属性
        /// </summary>
        public string d_attribute { get; set; } = null!;

        [NotMapped]
        public List<Erp3ProductAttribute> Attribute =>
            JsonConvert.DeserializeObject<List<Erp3ProductAttribute>>(d_attribute)!;

        /// <summary>
        /// 变体索引
        /// </summary>
        public string d_variants { get; set; } = null!;

        [NotMapped]
        public Erp3ProductVariants Variants
        {
            get
            {
                var tmp = JToken.Parse(d_variants);
                if (tmp.Type != JTokenType.Array)
                    return new Erp3ProductVariants();

                foreach (var variant in tmp.Values<JObject>())
                {
                    var lps = variant!.Property(nameof(Erp3ProductVariant.Lps), StringComparison.OrdinalIgnoreCase);
                    if (lps is null)
                        continue;

                    if (lps is { Value.Type: JTokenType.Array })
                    {
                        lps.Value = new JObject();
                        continue;
                    }

                    var objLps = (JObject)lps.Value;
                    var needToRemoveProperties = objLps.Properties().Where(p => p is { Value.Type: JTokenType.Array })
                        .Select(p => p.Name).ToArray();
                    foreach (var property in needToRemoveProperties) objLps.Remove(property);
                }

                return tmp.ToObject<Erp3ProductVariants>()!;
            }
        }


        /// <summary>
        /// 图片索引[{ID,url,size,oss}]
        /// </summary>
        public string? d_images { get; set; }

        [NotMapped]
        public Erp3ProductImages? Images =>
            d_images != null ? JsonConvert.DeserializeObject<Erp3ProductImages>(d_images) : null;


        /// <summary>
        /// 列表图片edit,download,makemainpic*,delete
        /// </summary>
        public string u_image { get; set; } = null!;

        /// <summary>
        /// 采集产品唯一标识
        /// </summary>
        public string d_pid { get; set; } = null!;

        /// <summary>
        /// 采集产品唯一标识Hashcode
        /// </summary>
        public ulong d_pid_hash_code { get; set; }

        /// <summary>
        /// 产品图片大小
        /// </summary>
        public long c_size { get; set; }

        /// <summary>
        /// 货币单位(采集产品单位)
        /// </summary>
        public string d_coin { get; set; } = null!;

        /// <summary>
        /// 汇率(采集产品当时汇率)
        /// </summary>
        public decimal d_rate { get; set; }

        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }

        public ProductModel ToProduct(User user)
        {
            return new ProductModel()
            {
                UserID = user.Id,
                GroupID = user.GroupID,
                CompanyID = user.CompanyID,
                OEMID = user.OEMID,
                Title = u_lp_n,
                HashCode = d_hash_code,
                Source = d_source,
                PlatformId = (int)i_platform_id,
                // Quantity = (int)u_quantity,
                Prices = JsonConvert.SerializeObject(Price),
                Pid = d_pid,
                PidHashCode = d_pid_hash_code.ToString(),
                Type = (long)Types.Default,
                Language = (long)Types.Default,
                FilesSize = 0,
            };
        }
    }
}

#region 翻译

public class Erp3ProductLanguage
{
    public string D { get; set; }
    public string K { get; set; }
    public string N { get; set; }
    public List<string> S { get; set; }
    public Dictionary<string, string>? Map { get; set; }
}

#endregion

#region 属性

public class Erp3ProductAttribute
{
    public int Tag { get; set; }
    public string Name { get; set; }
    public List<string> Value { get; set; }
}

#endregion

#region 价格

public class Erp3ProductPrices
{
    public Erp3ProductPrice Cost { get; set; }
    public Erp3ProductPrice Sale { get; set; }
}

public class Erp3ProductPrice
{
    public decimal? Max { get; set; }
    public decimal? Min { get; set; }
}

#endregion

#region 图片

public class Erp3ProductImage
{
    private const long NetworkSize = 102400;

    public int Id { get; set; }
    public string Path { get; set; }
    public long Size { get; set; }
    public bool IsNetwork => Size == NetworkSize;

    public TempImagesModel ToTempImagesModel(string path, int storageId,
        ResourceModel.Types type, User user)
    {
        return new TempImagesModel()
        {
            Path = path,
            OssID = storageId,
            Type = type,
            Size = IsNetwork ? -1 : Size,
            UserID = user.Id,
            GroupID = user.GroupID,
            CompanyID = user.CompanyID,
            OEMID = user.OEMID,
        };
    }
}

public class Erp3ProductImages : List<Erp3ProductImage>
{
}

#endregion

#region 变体

public class Erp3ProductVariants : List<Erp3ProductVariant>
{
}

public class Erp3ProductVariant
{
    public List<Erp3ProductIds> Ids { get; set; }
    public Erp3ProductImg Img { get; set; }
    public string Sid { get; set; } = string.Empty;
    public string? Code { get; set; }
    public decimal Cost { get; set; }
    public decimal Sale { get; set; }
    public int State { get; set; }

    public int Quantity { get; set; }

    public Dictionary<string, Erp3ProductVariantLps>? Lps { get; set; }

    [JsonProperty("hash_code")] public string HasHCode { get; set; }

    public Variant ToVariant(string language)
    {
        var result = new Variant();
        var tempIds = new Dictionary<int, int>();
        foreach (var item in Ids)
        {
            tempIds.Add(item.Tag, item.Val);
        }

        result.Ids = tempIds;
        result.Code = Code;
        result.Cost = Cost;
        result.Sale = Sale;
        result.Quantity = Quantity;
        result.Sid = Sid;
        result.State = Enum.Parse<VariantStateEnum>(State.ToString());
        result.Images = new VariantImages()
        {
            Main = Img.Main,
            Affiliate = Img.Affiliate
        };
        if (Lps != null && Lps.Keys.Contains(language))
        {
            var basic = Lps[language];
            result.SetTitle(basic.N);
            result.SetKeyword(basic.K);
            result.SetDescription(basic.D);
            if (!string.IsNullOrEmpty(basic.S.First()) && !string.IsNullOrWhiteSpace(basic.S.First()))
            {
                result.SetSketches(basic.S);
            }
        }

        return result;
    }
}

public class Erp3ProductVariantLps
{
    public string D { get; set; }
    public string K { get; set; }
    public string N { get; set; }
    public List<string> S { get; set; }
}

public class Erp3ProductIds
{
    public int Tag { get; set; }
    public int Val { get; set; }
}

public class Erp3ProductImg
{
    public int? Main { get; set; }
    public List<int> Affiliate { get; set; }
}

#endregion