﻿using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using ERP.Models.DB.Erp3;
using Newtonsoft.Json;

namespace ERP.Models.Product;

[Comment("产品管理_erp3产品迁移任务")]
[Index(nameof(Key), IsUnique = true)]
[Index(nameof(State))]
public class ProductMigrationTask : UserModel
{
    [Comment("口令")] public Guid Key { get; set; }
    [Comment("迁移类型")] public TaskType Type { get; set; }
    [Comment("范围")] public string Range { get; set; } = string.Empty;
    [Comment("待迁移产品总数")] public int TotalCount { get; set; }

    [Comment("已完成数量")] public int CompletedCount { get; set; }
    [Comment("失败数量")] public int FailedCount { get; set; }
    public int SuccessCount => CompletedCount - FailedCount;

    [Comment("失败产品id")] public string FailedProductIds { get; set; } = "[]";
    
    [Comment("成功产品对应id")] public string SuccessProducts { get; set; } = "{}";
    

    [Comment("迁移状态")] public TaskState State { get; set; }

    [Comment("用户可视范围")] public DataVisible UserVisible { get; set; }

    public ulong Erp3UserId { get; set; }
    public ulong Erp3GroupId { get; set; }
    public ulong Erp3CompanyId { get; set; }
    public ulong Erp3OemId { get; set; }

    public enum TaskType
    {
        /// <summary>
        /// 勾选迁移
        /// </summary>
        Selected,

        /// <summary>
        /// 日期范围迁移
        /// </summary>
        DateRange,
    }

    public enum TaskState
    {
        Queueing,
        Migrating,
        Completed,
    }

    public enum DataVisible
    {
        Group = 1,
        User,
        Company
    }

    public record DateRange(DateTime Start, DateTime End);

    public ulong[] GetPids() => JsonConvert.DeserializeObject<ulong[]>(Range)!;

    public DateRange GetDateRange()
    {
        var dates = JsonConvert.DeserializeObject<DateTime[]>(Range, new JsonSerializerSettings()
        {
            DateParseHandling = DateParseHandling.DateTimeOffset,
            DateTimeZoneHandling = DateTimeZoneHandling.Local,
        })!;
        return new DateRange(dates[0].Date, dates[1]);
    }

    public static ProductMigrationTask FromErp3Task(Erp3MigrationTask task) => new ProductMigrationTask()
    {
        Type = task.d_type == 1 ? TaskType.Selected : TaskType.DateRange,
        Range = task.d_range,
        UserVisible = (DataVisible)task.d_user_visible,
        Erp3UserId = task.i_user_id,
        Erp3GroupId = task.i_group_id,
        Erp3CompanyId = task.i_company_id,
        Erp3OemId = task.i_oem_id,
    };
}

public static class ProductMigrationTaskDbExtensions
{
    public static IQueryable<Erp3Product> WhereByTask(this IQueryable<Erp3Product> query, ProductMigrationTask task)
    {
        if (task.Type == ProductMigrationTask.TaskType.Selected)
        {
            var pids = task.GetPids();
            return query.Where(p => pids.Contains(p.id));
        }

        if (task.Type == ProductMigrationTask.TaskType.DateRange)
        {
            // 过滤不需要的产品
            const ulong filteredBit = 1024 // onGoing
                                      | 4096 // export
                                      | 4398046511104UL // b2c
                                      | 64; // recycle
            var dateRange = task.GetDateRange();
            return (task.UserVisible switch
            {
                ProductMigrationTask.DataVisible.Group => query.Where(p => p.i_group_id == task.Erp3GroupId),
                ProductMigrationTask.DataVisible.Company => query.Where(p => p.i_company_id == task.Erp3CompanyId),
                _ => query.Where(p => p.i_user_id == task.Erp3UserId),
            }).Where(p =>
                (p.b_type & filteredBit) == 0 && p.created_at >= dateRange.Start && p.created_at <= dateRange.End);
        }

        return query;
    }
}