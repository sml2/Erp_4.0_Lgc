﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP
{
    /// <summary>
    /// 用户管理_用户表
    /// </summary>
    [Table("erp3_user")]
    public partial class Erp3User
    {
        public ulong id { get; set; }
        /// <summary>
        /// 用户名(唯一)
        /// </summary>
        public string d_username { get; set; } = null!;
        /// <summary>
        /// 姓名
        /// </summary>
        public string d_truename { get; set; } = null!;
        /// <summary>
        /// 密码encrypt
        /// </summary>
        public string d_password { get; set; } = null!;
        /// <summary>
        /// 手机
        /// </summary>
        public string d_mobile { get; set; } = null!;
        /// <summary>
        /// 邮箱
        /// </summary>
        public string d_email { get; set; } = null!;
        /// <summary>
        /// 手机号码认证 1未认证2已认证
        /// </summary>
        public byte t_attestation { get; set; }
        /// <summary>
        /// 用户软件UI展示货币单位
        /// </summary>
        public string d_unit_config { get; set; } = null!;
        /// <summary>
        /// 订单价格显示配置
        /// </summary>
        public ulong d_order_unit_config { get; set; }
        /// <summary>
        /// 订单汇率配置项 1 拉取时亚马逊汇率 2实时汇率
        /// </summary>
        public uint d_order_rate_config { get; set; }
        /// <summary>
        /// 权限组id
        /// </summary>
        public uint i_rule_id { get; set; }
        /// <summary>
        /// 用户组id
        /// </summary>
        public uint i_group_id { get; set; }
        /// <summary>
        /// 父级用户id
        /// </summary>
        public uint i_pid { get; set; }
        /// <summary>
        /// 用户状态1正常2禁止
        /// </summary>
        public byte t_state { get; set; }
        /// <summary>
        /// 创建用户过程
        /// </summary>
        public string? i_pids { get; set; }
        /// <summary>
        /// 用户类型1独立账号2子账号
        /// </summary>
        public byte t_concierge { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int d_sort { get; set; }
        /// <summary>
        /// 深度
        /// </summary>
        public byte d_level { get; set; }
        /// <summary>
        /// OEM
        /// </summary>
        public uint u_oem_id { get; set; }
        /// <summary>
        /// 公司id
        /// </summary>
        public uint i_company_id { get; set; }
        /// <summary>
        /// 创建公司id
        /// </summary>
        public uint i_p_company_id { get; set; }
        /// <summary>
        /// 是否分销1否2是
        /// </summary>
        public byte u_is_distribution { get; set; }
        /// <summary>
        /// 首次web登录
        /// </summary>
        public DateTime? d_first_login_web { get; set; }
        /// <summary>
        /// 首次exe登录
        /// </summary>
        public DateTime? d_first_login_exe { get; set; }
        /// <summary>
        /// 导出excel图片后加随机数1否2是
        /// </summary>
        public byte t_product_img_config { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string d_avatar { get; set; } = null!;
        /// <summary>
        /// 主题id
        /// </summary>
        public uint i_theme_id { get; set; }
        /// <summary>
        /// 登录时间
        /// </summary>
        public DateTime? login_time { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
    }
}
