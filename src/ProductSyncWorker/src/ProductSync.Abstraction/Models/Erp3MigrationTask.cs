﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.Models.DB.Erp3;

[Table("erp3_migration_task")]
public partial class Erp3MigrationTask
{
    public int d_user_visible { get; set; }
    public int id { get; set; }
    public int d_type { get; set; }
    public string d_range { get; set; } = null!;
    public string d_key { get; set; } = null!;
    public ulong i_user_id { get; set; }
    public ulong i_group_id { get; set; }
    public ulong i_company_id { get; set; }
    public ulong i_oem_id { get; set; }
    public DateTime created_at { get; set; }
    public DateTime? updated_at { get; set; }
        
}