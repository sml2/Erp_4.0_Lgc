﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProductSync.Abstraction.Models;

/// <summary>
/// 产品导出_导出任务集
/// </summary>
[Table("erp3_sync_export")]
public partial class Erp3SyncExport
{
    public ulong id { get; set; }
    /// <summary>
    /// 导出产品任务集名称
    /// </summary>
    public string? d_name { get; set; }
    /// <summary>
    /// 导出任务集产品数量
    /// </summary>
    public uint d_amount { get; set; }
    /// <summary>
    /// 排序字段
    /// </summary>
    public int d_sort { get; set; }
    /// <summary>
    /// 导出条件参数json（头部）
    /// </summary>
    public string u_param { get; set; } = null!;
    /// <summary>
    /// 导出条件附加参数json
    /// </summary>
    public string u_append { get; set; } = null!;
    /// <summary>
    /// 产品语言id
    /// </summary>
    public int i_language_id { get; set; }
    /// <summary>
    /// 导出模板sign
    /// </summary>
    public uint u_export_sign { get; set; }
    /// <summary>
    /// 导出模板平台id
    /// </summary>
    public uint i_platform_id { get; set; }
    /// <summary>
    /// 用户id
    /// </summary>
    public uint i_user_id { get; set; }
    /// <summary>
    /// 创建用户组id
    /// </summary>
    public int i_group_id { get; set; }
    /// <summary>
    /// 所属公司id
    /// </summary>
    public uint i_company_id { get; set; }
    /// <summary>
    /// 当前Oem id
    /// </summary>
    public uint i_oem_id { get; set; }
    public DateTime? created_at { get; set; }
    public DateTime? updated_at { get; set; }
}