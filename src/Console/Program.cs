// See https://aka.ms/new-console-template for more information

using System.Diagnostics;
using System.Dynamic;
using ERP.Data;
using ERP.Enums.Identity;
using ERP.Enums.Statistics;
using ERP.Extensions;
using ERP.Models.DB.Statistics;
using ERP.Models.DB.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Hosting.Internal;
using Newtonsoft.Json;
using Z.EntityFramework.Plus;
using ProductType = ERP.Enums.Product.Types;

Console.WriteLine("Reset Count");


var jsonPath = "appsettings.Development.json";
var serviceCollection = new ServiceCollection();
var configurationBuilder = new ConfigurationBuilder().AddJsonFile(jsonPath);
serviceCollection.AddSingleton<IHostEnvironment, HostingEnvironment>();
var configuration = configurationBuilder.Build();
serviceCollection.AddServices(configuration);
var sp = serviceCollection.BuildServiceProvider().CreateScope().ServiceProvider;
var dbContext = sp.GetRequiredService<DBContext>();

// return;

//处理公司层级计数 获取ids数据
var companies = await dbContext.Company.ToListAsync();
var dicCompanies = companies.ToDictionary(m => m.ID, m => m);
var companyIds = companies.Select(m => m.ID).ToList();
await ClearCount(companyIds, Types.Company);
var companyData = await GetCountData(companyIds, Types.Company, Types.DAY);
await UpdateCompanyCount(companyData,dicCompanies);
//
//处理用户组层级
// var groupIds = await dbContext.UserGroup.Select(m => m.ID).ToListAsync();
// await ClearCount(groupIds, Types.UserGroup);
// var groupData = await GetCountData(groupIds, Types.UserGroup, Types.DAY);
// await UpdateGroupCount(groupData);
//
// //处理用户层级
var userIds = await dbContext.User.Select(m => m.Id).ToListAsync();
await ClearCount(userIds, Types.User); 
var userData = await GetCountData(userIds, Types.User, Types.DAY);
await UpdateUserCount(userData);

//处理店铺层级
// var storeIds = await dbContext.StoreRegion.Select(m => m.ID).ToListAsync();
// await ClearCount(storeIds, Types.Store);
// var storeData = await GetCountData(storeIds, Types.Store, Types.DAY);
// await UpdateStoreCount(storeData);


#region 清空每日统计数据

async Task ClearCount(List<int> aboutIds, Types type)
{
    var restCompanyStatistic = new ExpandoObject();
    restCompanyStatistic.TryAdd("CompanyInsert", 0);
    restCompanyStatistic.TryAdd("CompanyDelete", 0);
    restCompanyStatistic.TryAdd("CompanyCount", 0);
    restCompanyStatistic.TryAdd("CompanyReject", 0);
    restCompanyStatistic.TryAdd("EmployeeInsert", 0);
    restCompanyStatistic.TryAdd("EmployeeDelete", 0);
    restCompanyStatistic.TryAdd("EmployeeCount", 0);
    restCompanyStatistic.TryAdd("EmployeeReject", 0);
    restCompanyStatistic.TryAdd("UserInsert", 0);
    restCompanyStatistic.TryAdd("UserDelete", 0);
    restCompanyStatistic.TryAdd("UserCount", 0);
    restCompanyStatistic.TryAdd("UserReject", 0);
    restCompanyStatistic.TryAdd("TaskInsert", 0);
    restCompanyStatistic.TryAdd("TaskDelete", 0);
    restCompanyStatistic.TryAdd("TaskCount", 0);
    restCompanyStatistic.TryAdd("StoreInsert", 0);
    restCompanyStatistic.TryAdd("StoreDelete", 0);
    restCompanyStatistic.TryAdd("StoreCount", 0);
    restCompanyStatistic.TryAdd("OrderInsert", 0);
    restCompanyStatistic.TryAdd("OrderDelete", 0);
    restCompanyStatistic.TryAdd("OrderCount", 0);
    restCompanyStatistic.TryAdd("PurchaseInsert", 0);
    restCompanyStatistic.TryAdd("PurchaseDelete", 0);
    restCompanyStatistic.TryAdd("PurchaseCount", 0);
    restCompanyStatistic.TryAdd("WaybillInsert", 0);
    restCompanyStatistic.TryAdd("WaybillDelete", 0);
    restCompanyStatistic.TryAdd("WaybillCount", 0);
    restCompanyStatistic.TryAdd("ProductInsert", 0);
    restCompanyStatistic.TryAdd("ProductDelete", 0);
    restCompanyStatistic.TryAdd("ProductCount", 0);
    restCompanyStatistic.TryAdd("ProductRecovery", 0);
    
    restCompanyStatistic.TryAdd("ReportCompanyCount", 0);
    restCompanyStatistic.TryAdd("ReportEmployeeCount", 0);
    restCompanyStatistic.TryAdd("ReportUserCount", 0);
    restCompanyStatistic.TryAdd("ReportProductCount", 0);
    restCompanyStatistic.TryAdd("ReportStoreCount", 0);
    restCompanyStatistic.TryAdd("ReportOrderCount", 0);
    restCompanyStatistic.TryAdd("ReportPurchaseCount", 0);
    restCompanyStatistic.TryAdd("ReportWaybillCount", 0);
    
    // superior. = item.CompanyCount;
    // superior. = item.EmployeeCount;
    // superior. = item.UserCount;
    // superior. = item.ProductCount;
    // superior. = item.StoreCount;
    // superior. = item.OrderCount;
    // superior. = item.PurchaseCount;
    // superior. = item.WaybillCount;

    await dbContext.Statistic.Where(m =>
            aboutIds.Contains(m.AboutID) && (m.AboutType & Types.Role) == type)
        .UpdateAsync(restCompanyStatistic);
}

#endregion

#region 获取操作数据

async Task<List<Statistic>> GetCountData(List<int> aboutIds, Types role, Types effective)
{
    List<Statistic> data = new();
    foreach (var cid in aboutIds)
    {
        var now = DateTime.Now;
        var start = Convert.ToDateTime(now.ToString("yyyy-MM-dd 00:00:00"));
        var end = Convert.ToDateTime(now.ToString("yyyy-MM-dd 23:59:59"));
        var item = await dbContext.Statistic
            .Where(s =>
                s.AboutID == cid
                && (s.AboutType & Types.Role) == role //role Types.Company
                && (s.AboutType & Types.Effective) == effective //effective Types.DAY
                && s.CreatedAt >= start
                && s.CreatedAt <= end
            ).FirstOrDefaultAsync();
        if (item == null)
        {
            item = new Statistic(cid, role, Types.DAY, now);
            await dbContext.Statistic.AddAsync(item);
            await dbContext.SaveChangesAsync();
        }

        data.Add(item);
    }

    return data;
}

#endregion

#region 更新公司计数

async Task UpdateCompanyCount(List<Statistic> countData, Dictionary<int, Company> dictionary)
{
    //Employee
    var users = await dbContext.User.Where(m => m.Role == Role.Employee)
        .GroupBy(m => m.CompanyID)
        .Select(m => new
        {
            KeyId = m.Key,
            Count = m.Count(),
        }).ToDictionaryAsync(m => m.KeyId, m => m);

    //Company
    var companys = await dbContext.Company.GroupBy(m => m.ParentCompanyId).Select(m => new
    {
        KeyId = m.Key,
        Count = m.Count(),
    }).ToDictionaryAsync(m => m.KeyId, m => m);

//Task

    var tasks = await dbContext.Task.GroupBy(m => m.CompanyID).Select(m => new
    {
        KeyId = m.Key,
        Count = m.Count(),
    }).ToDictionaryAsync(m => m.KeyId, m => m);

//Store
    var stores = await dbContext.StoreRegion.Where(m => !m.IsDel).GroupBy(m => m.CompanyID).Select(
        m =>
            new
            {
                KeyId = m.Key,
                Count = m.Count(),
            }).ToDictionaryAsync(m => m.KeyId, m => m);

//Order
    var orders = await dbContext.Order.GroupBy(m => m.CompanyID).Select(m =>
        new
        {
            KeyId = m.Key,
            Count = m.Count(),
        }).ToDictionaryAsync(m => m.KeyId, m => m);

//Purchase
    var purchases = await dbContext.Purchase.GroupBy(m => m.CompanyID).Select(m =>
        new
        {
            KeyId = m.Key,
            Count = m.Count(),
        }).ToDictionaryAsync(m => m.KeyId, m => m);

//Waybill
    var waybills = await dbContext.Waybill.GroupBy(m => m.CompanyID).Select(m =>
        new
        {
            KeyId = m.Key,
            Count = m.Count(),
        }).ToDictionaryAsync(m => m.KeyId, m => m);

//Product
    var products = await dbContext.Product.Where(m =>
        (m.Type & (long)ProductType.OnGoing) != (long)ProductType.OnGoing
        && (m.Type & (long)ProductType.Export) != (long)ProductType.Export
        && (m.Type & (long)ProductType.B2C) != (long)ProductType.B2C
        && (m.Type & (long)ProductType.Uplaod) != (long)ProductType.Uplaod
    ).GroupBy(m => m.CompanyID).Select(m => new
    {
        KeyId = m.Key,
        ProductCount = m.Count(m => (m.Type & (long)ProductType.Recycle) != (long)ProductType.Recycle),
        ProductRecovery = m.Count(m => (m.Type & (long)ProductType.Recycle) == (long)ProductType.Recycle),
    }).ToDictionaryAsync(m => m.KeyId, m => m);

    var dicCountData = countData.ToDictionary(m => m.AboutID, m => m);
    foreach (var item in countData)
    {
        if (users.Keys.Contains(item.AboutID))
            item.EmployeeCount = users[item.AboutID].Count; //Employee

        if (companys.Keys.Contains(item.AboutID))
        {
            item.UserCount = companys[item.AboutID].Count; //User
            item.CompanyCount = companys[item.AboutID].Count; //Company
        }

        if (tasks.Keys.Contains(item.AboutID))
            item.TaskCount = tasks[item.AboutID].Count; //Task

        if (stores.Keys.Contains(item.AboutID))
            item.StoreCount = stores[item.AboutID].Count; //Store


        if (orders.Keys.Contains(item.AboutID))
            item.OrderCount = orders[item.AboutID].Count; //Order 


        if (purchases.Keys.Contains(item.AboutID))
            item.PurchaseCount = purchases[item.AboutID].Count; //Purchase

        if (waybills.Keys.Contains(item.AboutID))
            item.WaybillCount = waybills[item.AboutID].Count; //Wabyill

        if (products.Keys.Contains(item.AboutID))
        {
            item.ProductCount = products[item.AboutID].ProductCount;
            item.ProductRecovery = products[item.AboutID].ProductRecovery;
        }
        
        //多级上报
        if (dictionary.Keys.Contains(item.AboutID))
        {
            var c = dictionary[item.AboutID];
            
            var reportIds = JsonConvert.DeserializeObject<List<int>>(c.Pids);
            if (reportIds is not null && reportIds.Count > 0)
            {
                foreach (var pid in reportIds)
                {
                    if (dicCountData.Keys.Contains(pid))
                    {
                        var superior = dicCountData[pid];
                        superior.ReportCompanyCount = item.CompanyCount;
                        superior.ReportEmployeeCount = item.EmployeeCount;
                        superior.ReportUserCount = item.UserCount;
                        superior.ReportProductCount = item.ProductCount;
                        superior.ReportStoreCount = item.StoreCount;
                        superior.ReportOrderCount = item.OrderCount;
                        superior.ReportPurchaseCount = item.PurchaseCount;
                        superior.ReportWaybillCount = item.WaybillCount;
                    }
                }
            }
        }
    }
    
   

    dbContext.Statistic.UpdateRange(countData);
    await dbContext.SaveChangesAsync();
}

#endregion

#region 更新用户组计数

async Task UpdateGroupCount(List<Statistic> countData)
{
    //Employee
    var users = await dbContext.User
        .GroupBy(m => m.GroupID)
        .Select(m => new
        {
            KeyId = m.Key,
            UserCount = m.Count(m => m.Role == Role.ADMIN),
            EmployeeCount = m.Count(m => m.Role == Role.Employee),
        }).ToDictionaryAsync(m => m.KeyId, m => m);

    //Task
    var tasks = await dbContext.Task.GroupBy(m => m.GroupID).Select(m => new
    {
        KeyId = m.Key,
        Count = m.Count(),
    }).ToDictionaryAsync(m => m.KeyId, m => m);

    //Store
    var stores = await dbContext.StoreRegion.Where(m => !m.IsDel).GroupBy(m => m.GroupID).Select(
        m =>
            new
            {
                KeyId = m.Key,
                Count = m.Count(),
            }).ToDictionaryAsync(m => m.KeyId, m => m);

    //Order
    var orders = await dbContext.Order.GroupBy(m => m.GroupID).Select(m =>
        new
        {
            KeyId = m.Key,
            Count = m.Count(),
        }).ToDictionaryAsync(m => m.KeyId, m => m);

//Purchase
    var purchases = await dbContext.Purchase.GroupBy(m => m.GroupID).Select(m =>
        new
        {
            KeyId = m.Key,
            Count = m.Count(),
        }).ToDictionaryAsync(m => m.KeyId, m => m);

    //Waybill
    var waybills = await dbContext.Waybill.GroupBy(m => m.GroupID).Select(m =>
        new
        {
            KeyId = m.Key,
            Count = m.Count(),
        }).ToDictionaryAsync(m => m.KeyId, m => m);

    //Product
    var products = await dbContext.Product.Where(m =>
        (m.Type & (long)ProductType.OnGoing) != (long)ProductType.OnGoing
        && (m.Type & (long)ProductType.Export) != (long)ProductType.Export
        && (m.Type & (long)ProductType.B2C) != (long)ProductType.B2C
        && (m.Type & (long)ProductType.Uplaod) != (long)ProductType.Uplaod
    ).GroupBy(m => m.GroupID).Select(m => new
    {
        KeyId = m.Key,
        ProductCount = m.Count(m => (m.Type & (long)ProductType.Recycle) != (long)ProductType.Recycle),
        ProductRecovery = m.Count(m => (m.Type & (long)ProductType.Recycle) == (long)ProductType.Recycle),
    }).ToDictionaryAsync(m => m.KeyId, m => m);

    foreach (var item in countData)
    {
        if (users.Keys.Contains(item.AboutID))
        {
            item.EmployeeCount = users[item.AboutID].EmployeeCount; //Employee
            item.UserCount = users[item.AboutID].UserCount; //User
        }


        if (tasks.Keys.Contains(item.AboutID))
            item.TaskCount = tasks[item.AboutID].Count; //Task

        if (stores.Keys.Contains(item.AboutID))
            item.StoreCount = stores[item.AboutID].Count; //Store


        if (orders.Keys.Contains(item.AboutID))
            item.OrderCount = orders[item.AboutID].Count; //Order 


        if (purchases.Keys.Contains(item.AboutID))
            item.PurchaseCount = purchases[item.AboutID].Count; //Purchase

        if (waybills.Keys.Contains(item.AboutID))
            item.WaybillCount = waybills[item.AboutID].Count; //Wabyill

        if (products.Keys.Contains(item.AboutID))
        {
            item.ProductCount = products[item.AboutID].ProductCount;
            item.ProductRecovery = products[item.AboutID].ProductRecovery;
        }
    }

    dbContext.Statistic.UpdateRange(countData);
    await dbContext.SaveChangesAsync();
}

#endregion

#region 更新用户计数

async Task UpdateUserCount(List<Statistic> countData)
{
    //Employee
    var users = await dbContext.User
        .GroupBy(m => m.Pid)
        .Select(m => new
        {
            KeyId = m.Key,
            UserCount = m.Count(m => m.Role == Role.ADMIN),
            EmployeeCount = m.Count(m => m.Role == Role.Employee),
        }).ToDictionaryAsync(m => m.KeyId, m => m);

    //Task
    var tasks = await dbContext.Task.GroupBy(m => m.UserID).Select(m => new
    {
        KeyId = m.Key,
        Count = m.Count(),
    }).ToDictionaryAsync(m => m.KeyId, m => m);

    //Store
    var stores = await dbContext.StoreRegion.Where(m => !m.IsDel).GroupBy(m => m.UserID).Select(
        m =>
            new
            {
                KeyId = m.Key,
                Count = m.Count(),
            }).ToDictionaryAsync(m => m.KeyId, m => m);

    //Order
    var orders = await dbContext.Order.GroupBy(m => m.UserID).Select(m =>
        new
        {
            KeyId = m.Key,
            Count = m.Count(),
        }).ToDictionaryAsync(m => m.KeyId, m => m);

//Purchase
    var purchases = await dbContext.Purchase.GroupBy(m => m.UserID).Select(m =>
        new
        {
            KeyId = m.Key,
            Count = m.Count(),
        }).ToDictionaryAsync(m => m.KeyId, m => m);

    //Waybill
    var waybills = await dbContext.Waybill.GroupBy(m => m.UserID).Select(m =>
        new
        {
            KeyId = m.Key,
            Count = m.Count(),
        }).ToDictionaryAsync(m => m.KeyId, m => m);

    //Product
    var products = await dbContext.Product.Where(m =>
        (m.Type & (long)ProductType.OnGoing) != (long)ProductType.OnGoing
        && (m.Type & (long)ProductType.Export) != (long)ProductType.Export
        && (m.Type & (long)ProductType.B2C) != (long)ProductType.B2C
        && (m.Type & (long)ProductType.Uplaod) != (long)ProductType.Uplaod
    ).GroupBy(m => m.UserID).Select(m => new
    {
        KeyId = m.Key,
        ProductCount = m.Count(m => (m.Type & (long)ProductType.Recycle) != (long)ProductType.Recycle),
        ProductRecovery = m.Count(m => (m.Type & (long)ProductType.Recycle) == (long)ProductType.Recycle),
    }).ToDictionaryAsync(m => m.KeyId, m => m);

    foreach (var item in countData)
    {
        if (users.Keys.Contains(item.AboutID))
        {
            item.EmployeeCount = users[item.AboutID].EmployeeCount; //Employee
            item.UserCount = users[item.AboutID].UserCount; //User
            item.CompanyCount = users[item.AboutID].UserCount; //Company
        }


        if (tasks.Keys.Contains(item.AboutID))
            item.TaskCount = tasks[item.AboutID].Count; //Task

        if (stores.Keys.Contains(item.AboutID))
            item.StoreCount = stores[item.AboutID].Count; //Store


        if (orders.Keys.Contains(item.AboutID))
            item.OrderCount = orders[item.AboutID].Count; //Order 


        if (purchases.Keys.Contains(item.AboutID))
            item.PurchaseCount = purchases[item.AboutID].Count; //Purchase

        if (waybills.Keys.Contains(item.AboutID))
            item.WaybillCount = waybills[item.AboutID].Count; //Wabyill

        if (products.Keys.Contains(item.AboutID))
        {
            item.ProductCount = products[item.AboutID].ProductCount;
            item.ProductRecovery = products[item.AboutID].ProductRecovery;
        }
    }

    dbContext.Statistic.UpdateRange(countData);
    await dbContext.SaveChangesAsync();
}

#endregion

#region 更新店铺计数

async Task UpdateStoreCount(List<Statistic> countData)
{
    //Order
    var orders = await dbContext.Order.GroupBy(m => m.StoreId).Select(m =>
        new
        {
            KeyId = m.Key,
            Count = m.Count(),
        }).ToDictionaryAsync(m => m.KeyId, m => m);

//Purchase
    var purchases = await dbContext.Purchase.GroupBy(m => m.StoreId).Select(m =>
        new
        {
            KeyId = m.Key,
            Count = m.Count(),
        }).ToDictionaryAsync(m => m.KeyId, m => m);

    //Waybill
    var waybills = await dbContext.Waybill.GroupBy(m => m.StoreId).Select(m =>
        new
        {
            KeyId = m.Key,
            Count = m.Count(),
        }).ToDictionaryAsync(m => m.KeyId, m => m);

    foreach (var item in countData)
    {
        if (orders.Keys.Contains(item.AboutID))
            item.OrderCount = orders[item.AboutID].Count; //Order 

        if (purchases.Keys.Contains(item.AboutID))
            item.PurchaseCount = purchases[item.AboutID].Count; //Purchase

        if (waybills.Keys.Contains(item.AboutID))
            item.WaybillCount = waybills[item.AboutID].Count; //Wabyill
    }

    dbContext.Statistic.UpdateRange(countData);
    await dbContext.SaveChangesAsync();
}

#endregion

Console.WriteLine("Success");