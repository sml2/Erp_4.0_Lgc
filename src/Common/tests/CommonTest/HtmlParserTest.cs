using Common.Html;

namespace CommonTest;

public class HtmlParserTest
{
    [Fact]
    public void TestHtml()
    {
        var html = @"<p  class=""text"" >test
<p>inner test</p>
</p>";

        var result = HtmlParser.Parse(html).Nodes;
        Assert.Single(result);
        var node = (Tag)result[0];
        Assert.Equal("p", node.TagName);
        Assert.Equal("text", node.Attributes["class"]);
        Assert.Equal("test\n", ((TextNode)node.Children[0]).Content);
        Assert.Equal("inner test", ((TextNode)((Tag)node.Children[1]).Children[0]).Content);
    }
    
    [Theory]
    [InlineData(@"<img src=""http://1.png"" />")]
    [InlineData(@"<img src=""http://1.png"" >")]
    [InlineData(@"<img src=""http://1.png""/>")]
    public void TestSelfClosing(string html)
    {

        var result = HtmlParser.Parse(html).Nodes;
        Assert.Single(result);
        var node = (Tag)result[0];
        Assert.Equal("img", node.TagName);
        Assert.True(node.SelfClosing);
    }
    
    [Theory]
    [InlineData(@"<p>第一行
</p>
<p>第三行</p>
<p>前缀<span>中间</span>后缀</p>",
        "第一行\n\n第三行\n前缀\n中间\n后缀",
        "1st line\n\n3rd line\nprefix\nmiddle\nsuffix\n",
        "<p>1st line\n</p><p>3rd line</p><p>prefix<span>middle</span>suffix</p>")]
    public void TestReplaceContent(string html, string expectedText, string replace, string expectedReplaceText)
    {
        var result = HtmlParser.Parse(html);
        Assert.Equal(expectedText ,result.GetContent());
        
        result.ReplaceContent(replace);
        Assert.Equal(expectedReplaceText, result.ToString());
    }
    
    [Fact]
    public void TestReplaceContent_RemoveEndWhitespaceLines()
    {
        var html = "<p>第一行</p><p> </p><p>第三行\n&nbsp;\n</p><p></p><p> </p><p>\n</p><p>&nbsp;</p>\n";
        var result = HtmlParser.Parse(html);
        var setting = new GetContentSetting(){Unescape = true, RemoveWhiteSpaceEndLines = true};
        Assert.Equal("第一行\n \n第三行" ,result.GetContent(setting));
        
        result.ReplaceContent("1st line\n \n3rd line\n", setting);
        Assert.Equal("<p>1st line</p><p> </p><p>3rd line</p><p></p><p> </p><p>\n</p><p>&nbsp;</p>", result.ToString());
        // Assert.Equal("<p>1st line</p><p> </p><p>3rd line</p>", result.ToString(setting));
    }
    
    [Fact]
    public void TestReplaceContent_RemoveStartWhitespaceLines()
    {
        var html = "<p><img src=\"test.png\" /></p><p> </p><p> \n&nbsp;\n第一行</p><p> </p><p>第三行\n&nbsp;\n</p>";
        var result = HtmlParser.Parse(html);
        var setting = new GetContentSetting(){Unescape = true, RemoveWhiteSpaceEndLines = true, RemoveWhiteSpaceStartLines = true};
        Assert.Equal("第一行\n \n第三行" ,result.GetContent(setting));
        
        result.ReplaceContent("1st line\n \n3rd line\n", setting);
        Assert.Equal("<p><img src=\"test.png\" /></p><p> </p><p>1st line</p><p> </p><p>3rd line</p>", result.ToString());
    }
    
    [Fact]
    public void TestParseContent_WithArrowChar()
    {
        var html = @"Clothes length: short (40cm<clothes length≤50cm)";
        var result = HtmlParser.Parse(html);
        Assert.Single(result.Nodes);
    }
    
    [Theory]
    [InlineData("<p>Care Instructions:Hand wash only",  "Care Instructions:Hand wash only")]
    [InlineData("<p><p>Care Instructions:Hand wash only",  "Care Instructions:Hand wash only")]
    [InlineData("<p><p>Care Instructions:Hand wash only</",  "Care Instructions:Hand wash only")]
    // [InlineData("<p><p>Care Instructions:Hand wash only</p",  "Care Instructions:Hand wash only")]
    public void TestGetContent(string html, string expected)
    {
        var result = HtmlParser.Parse(html);
        Assert.Equal(expected,result.GetContent());
    }
}