﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using ERP.Models.ProductNodeType.Enum;
using ERP.Enums;

namespace ERP.Models.ProductNodeType
{
    public class AttrValueRelation
    {
        public AttrValueRelation()
        {

        }

        public AttrValueRelation(ProductNodeTypeAttribute attributeModel, Value valueModel,long? valueId, Platforms enumPlatform,long cid)
        {
            Attribute = attributeModel;
            Value = valueModel;
            ValueId = valueId;
            Platform = enumPlatform;
            CId = cid;            
         }
        public AttrValueRelation(long attrId, Value valueModel, long? valueId, Platforms enumPlatform,long? oldID,long cid)
        {
            AttrId = attrId;
            Value = valueModel;
            ValueId = valueId;
            Platform = enumPlatform;
            OldVId = oldID;
            CId = cid;
        }

        public long Id { get; set; }

        [Comment("属性表主键ID")]
        public long AttrId { get; set; }

        [ForeignKey("AttrId")]
        public ProductNodeTypeAttribute? Attribute { get; set; }

        [Comment("值ID")]
        public long? ValueId { get; set; }

        [Comment("属性值的主键ID")]
        public long VId { get; set; }

        [ForeignKey("VId")]
        public Value Value { get; set; } = default!;

        [Comment("使用状态")]
        /// <summary>
        /// 使用状态
        /// </summary>
        public EnumStatus Status { set; get; } = EnumStatus.ADD;

        [Comment("旧的属性值的主键ID")]
        public long? OldVId { get; set; }

        [Comment("新的属性值的主键ID")]
        public long? NewVId { get; set; }

        [ForeignKey("NewVId")]
        public Value NewValue { get; set; } = default!;

        [Comment("执行操作时间")]
        public DateTime? ExcuteTime { get; set; } = DateTime.Now;

        [Comment("分类目录表的主键ID")]
        public long CId { set; get; }

        [Comment("所属平台")]
        /// <summary>
        /// 所属平台
        /// </summary>
        public Platforms Platform { get; set; }
    }
}
