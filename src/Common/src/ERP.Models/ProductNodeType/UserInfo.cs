﻿
using Microsoft.EntityFrameworkCore;
using System;

namespace ERP.Models.ProductNodeType;
public class UserInfo
{
    public int ID { get; set; }

    [Comment("店铺ID")]
    public int ShopId { get; set; }

    [Comment("token")]
    public string AccessToken { get; set; } = string.Empty;

    [Comment("有效期")]
    public long ExpireIn { get; set; }

    [Comment("获取时间")]
    public DateTime GetTime { get; set; } = DateTime.Now;

    [Comment("刷新token")]
    public string RefreshAccessToken { get; set; } = string.Empty;

    [Comment("店铺ID列表")]
    public string? ShopIdList { get; set; } 

    [Comment("商人ID列表")]
    public string? MainAccountIdList { get; set; }
}
