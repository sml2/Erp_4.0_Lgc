﻿using Microsoft.EntityFrameworkCore;
using ERP.Models.ProductNodeType.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using ERP.Enums;

namespace ERP.Models.ProductNodeType
{
    [Index(nameof(AmzRSRID), nameof(AmzFormRawID))]
    public class ProductNodeTypeAttribute
    {
        public ProductNodeTypeAttribute()
        {

        }
        public ProductNodeTypeAttribute(string oname, string dname, EnumInputType input, EnumFormatType formatType, 
            EnumDateFormatType dateFormatType,EnumType type,
            string? units, string? attrValues, int? formrawID, int? rsrid, Platforms platform,
            string? definition, string? acceptValues, string? example,long? attributeId)
        {
            OriginalName = oname;
            DisplayName = dname;
            InputType = input;
            FormatType = formatType;
            DateFormatType = dateFormatType;
            Type = type;
            AttributeUnit = units ?? "";
            AttributeValuesList = attrValues ?? "";
            AmzFormRawID = formrawID;
            AmzRSRID = rsrid;
            Platform = platform;
            DefinitionAndUse = definition;
            AcceptedValues = acceptValues;
            Example = example;
            AddTime = DateTime.Now;
            AttributeId = attributeId;
        }
        public long Id { get; set; }     

        [Comment("字段名称")]
        /// <summary>
        /// 字段名称
        /// </summary>
        public string OriginalName { get; set; } = string.Empty;

        [Comment("展示名称")]
        /// <summary>
        /// 展示名称
        /// </summary>
        public string DisplayName { get; set; } = string.Empty;

        [Comment("输入类型")]
        /// <summary>
        /// 输入类型
        /// </summary>
        public EnumInputType InputType { get; set; }

        [Comment("输出格式")]
        /// <summary>
        /// 输出格式
        /// </summary>
        public EnumFormatType FormatType { get; set; }

        [Comment("日期格式类型")]
        /// <summary>
        /// 日期格式类型
        /// </summary>
        public EnumDateFormatType DateFormatType { get; set; }

        [Comment("显示类型")]
        /// <summary>
        /// 显示类型
        /// </summary>
        public EnumType Type { get; set; }

        [Comment("值的单位")]
        public string AttributeUnit { get; set; } = string.Empty;

        [Comment("属性下所有的值")]
        public string AttributeValuesList { get; set; } = string.Empty;

        [Comment("所属平台")]
        /// <summary>
        /// 所属平台
        /// </summary>
        public Platforms Platform { get; set; }

        [Comment("添加时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        [Comment("amazon来源")]
        /// <summary>
        /// amazon来源
        /// </summary>
        public int? AmzFormRawID { get; set; }

        [Comment("amazon来源")]
        /// <summary>
        /// amazon来源
        /// </summary>
        public int? AmzRSRID { get; set; }

        [Comment("属性ID")]
        public long? AttributeId { set; get; }

        [InverseProperty("attribute")]
        public List<CategoryAttrRelation> attributes { get; set; } = new();

        [InverseProperty("NewAttribute")]
        public List<CategoryAttrRelation> Newattributes { get; set; } = new();
        
        public List<AttrValueRelation> AttrValueRelations { get; set; } = new();

        [Comment("定义和使用")]
        public string? DefinitionAndUse { get; set; }

        [Comment("接受的值描述")]
        public string? AcceptedValues { get; set; }

        [Comment("例子")]
        public string? Example { get; set; }
    }    
}
