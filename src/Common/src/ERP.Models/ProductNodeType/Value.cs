﻿using Microsoft.EntityFrameworkCore;
using ERP.Models.ProductNodeType.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using ERP.Enums;

namespace ERP.Models.ProductNodeType
{
    public class ValueShow
    {
        public long Id { get; set; }

        [Comment("字段名称")]
        public string OriginalName { get; set; } = string.Empty;

        [Comment("展示名称")]
        public string DisplayName { get; set; } = string.Empty;
    }

    [Index(nameof(AmzRSRID), nameof(AmzFormRawID))]
    public class Value
    {
        public Value()
        {

        }

        public Value(string oname,string dname, Platforms platform,long? valueId)
        {
            OriginalName = oname;
            DisplayName = dname;
            Platform = platform;
            ValueId = valueId;
        }

        public Value(string oname, string dname, Platforms platform,int formID,int rID)
        {
            OriginalName = oname;
            DisplayName = dname;
            Platform = platform;
            AmzFormRawID = formID;
            AmzRSRID = rID;
        }
        public long Id { get; set; }     

        [Comment("字段名称")]
        public string OriginalName { get; set; }=string.Empty;

        [Comment("展示名称")]
        public string DisplayName { get; set; } = string.Empty;

        [Comment("所属平台")]
        /// <summary>
        /// 所属平台
        /// </summary>
        public Platforms Platform { get; set; }

        [Comment("添加时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;
     
        [Comment("ProductRawID")]
        public int AmzFormRawID { get; set; }

        [Comment("HtmlID")]
        public int? AmzRSRID { get; set; }

        [Comment("值ID")]
        public long? ValueId { get; set; }

        [InverseProperty("Value")]
        List<AttrValueRelation> Values { get; set; } = new();

        [InverseProperty("NewValue")]
        List<AttrValueRelation> NewValues { get; set; } = new();
    }
}
