using ERP.Models.ProductNodeType.Enum;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.Models.ProductNodeType.Amazon;

public partial class RealShip
{
    public RealShip() { }
    public RealShip(RealShipRaw realShipRaw, NodeRaw nodeRaw, ProductRaw productRaw,string name)
    {
        RealShipRaw = realShipRaw;
        Area = realShipRaw.Area!;
        NodeRaw = nodeRaw;
        ProductRaw = productRaw;
        Name = name;
    }
    public int ID { get; set; }

    [Comment("RealShipRawID")]
    public RealShipRaw RealShipRaw { get; set; } = null!;

    [Comment("NodeRawID")]
    public NodeRaw? NodeRaw { get; set; }

    [Comment("ProductRawID")]
    public ProductRaw? ProductRaw { get; set; }

    [Comment("所属地区、国家")]
    public string Area { get; set; }=string.Empty;

    [Comment("区分是直接关系，还是间接引用的其他的")]   
    public EnumSourceTypes Type { get; set; }

    [Comment("模板名称")]
    public string Name { get; set; } = string.Empty;
}
