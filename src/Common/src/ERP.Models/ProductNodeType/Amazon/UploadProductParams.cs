using ERP.Enums;

namespace ERP.Models.ProductNodeType.Amazon
{
    public class UploadProductParams
    {      
        public List<int> Pids { get; set; }
        public string PathNodes { get; set; }
        public string PathName { get; set; }
        public string MarketPlace { get; set; }
        public Languages   Lang { get; set; }
        public string Unit { get; set; }
        public string ProductType { get; set; }
        public byte[] Bytes { get; set; }      
    }
}
