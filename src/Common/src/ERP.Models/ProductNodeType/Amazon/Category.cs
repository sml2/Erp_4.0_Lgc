using ERP.Enums;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.ProductNodeType.Amazon
{
    public class Category
    {
        public Category()
        {

        }

        public Category(string oname, string dname, Platforms enumPlatform, long categoryId, long? parentCategoryId, string productType,
            string marketPlace, bool hasChild, string? path, bool isRoot,bool flag=false)
        {
            UniqueHashCode = Helpers.CreateHashCode(marketPlace + categoryId.ToString()+ path);

            //Platform+OriginalName+DisplayName+CategoryParentId+ProductType+MarketPlaceID+HasChild+IsRoot+PathNodes
            var text = $"{enumPlatform.GetDescription()}{oname}{dname}{parentCategoryId}{productType}{marketPlace}{hasChild}{isRoot}{path}";
            RowMd5 = Helpers.Md5(text);
            OriginalName = oname;
            DisplayName = dname.IsNullOrWhiteSpace() ? oname : dname;
            Platform = enumPlatform;
            CategoryId = categoryId;
            CategoryParentId = parentCategoryId;
            ProductType = productType;
            MarketPlaceID = marketPlace;
            HasChild = hasChild;
            PathNodes = path;
            IsRoot = isRoot;
            Flag = flag;
        }
        public long Id { get; set; }

        /// <summary>
        /// platform+CategoryId+paths
        /// </summary>
        public ulong UniqueHashCode { set; get; }

        //Platform+OriginalName+DisplayName+CategoryParentId+ProductType+MarketPlaceID+HasChild+IsRoot+PathNodes
        public string RowMd5 { get; set; }

        [Comment("所属平台")]
        /// <summary>
        /// 所属平台
        /// </summary>
        public Platforms Platform { get; set; }

        [Comment("字段名称")]
        public string OriginalName { set; get; } = string.Empty;

        [Comment("展示名称")]
        public string DisplayName { set; get; } = string.Empty;


        [Comment("分类目录ID")]
        public long CategoryId { set; get; }

        [Comment("所属父目录ID")]
        public long? CategoryParentId { set; get; }

        [Comment("所属产品类型")]
        public string ProductType { set; get; }

        [Comment("市场/国家")]
        public string MarketPlaceID { get; set; }

        [Comment("是否有子目录节点")]
        public bool HasChild { get; set; }

        [Comment("是否是根节点")]
        public bool IsRoot { get; set; }

        [Comment("节点路径")]
        public string? PathNodes { get; set; }

        [Comment("目录记录状态")]
        public CategoryState State { get; set; } = CategoryState.InUsed;

        [Comment("与productType的对应状态")]
        public bool Flag { get; set; } = false;

        [Comment("添加时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        [Comment("中文翻译")]
        public string Is8n { get; set; } = "";
    }

    public enum CategoryState
    {
        None,
        InUsed, //在用的
        Expired,  //过期的
        Deleted,  //删除的
    }
}
