﻿using System.ComponentModel.DataAnnotations;

namespace ERP.Models.ProductNodeType.Enum
{
    public enum EnumIsMandatory
    {        
        //未命中一下任何类型，出现异常
        NONE=0,
        //必填
        REQUIRED = 1,
        //可选
        OPTIONAL = 2,
        //建议
        DESIRED = 3,      
        DISABLED=4,
    }
}
