﻿namespace ERP.Models.ProductNodeType.Enum
{
    public enum EnumType
    {
        TEXT_FILED=1,  //文本填写框
        SELECT_COMBO_BOX=2, //单选下拉框
        MULTIPLE_SELECT_COMBO_BOX=3, //多选下拉框
        CHECKBOX=4, //checkbox选择      
        RADIO=5,    // radio
        DATETIME=6, //日期类型
        None=0,
    }
}
