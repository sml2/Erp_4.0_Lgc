﻿namespace ERP.Models.ProductNodeType
{
    public class CategoryItem
    {
       public long? CategoryId { get; set; }
       public string OriginalNname { get; set; } = string.Empty;
        public long? CategoryParentId { get; set; }
        public bool  Flag { get; set; }
        public bool ChangeFlag { get; set; }
    }
}