using Microsoft.EntityFrameworkCore;
using ERP.Models.ProductNodeType.Enum;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ERP.Models.DB;
using ERP.Enums;

namespace ERP.Models.ProductNodeType
{
    public class AreaCategoryRelation
    {
        public AreaCategoryRelation()
        {

        }

        public AreaCategoryRelation(int areaId,ProductNodeTypeCategory category,long? categoryId,long? parentCategoryId,
            EnumStatus enumStatus, long oldCId,bool hasChildren, Platforms enumPlatform)
        {
            AId = areaId;
            Category = category;
            CategoryId = categoryId;
            CategoryParentId = parentCategoryId;
            Platform = enumPlatform;
            HasChildren = hasChildren;
            Status = enumStatus;
            OldCId = oldCId;
        }
        public long Id { get; set; }

        [Comment("地区ID")]
        public int AId { get; set; }

        [ForeignKey("AId")]
        public PlatformData? area { get; set; }

        [Comment("分类目录表主键ID")]
        public long CId { set; get; }

        [ForeignKey("CId")]
        public ProductNodeTypeCategory Category { get; set; } = null!;

        [Comment("分类目录ID")]
        public long? CategoryId { set; get; }

        [Comment("所属父目录ID")]
        public long? CategoryParentId { set; get; }

        [Comment("状态")]
        public EnumStatus Status { set; get; } = EnumStatus.ADD;
            
        [Comment("旧的分类目录表主键ID")]
        public long? OldCId { get; set; }

        [Comment("新的分类目录表主键ID")]
        public long? NewCId { get; set; }

        [ForeignKey("NewCId")]
        public ProductNodeTypeCategory? NewCategory { get; set; }

        [Comment("更新操作时间")]
        public DateTime? ExcuteTime { get; set; } = DateTime.Now;

        [Comment("所属平台")]
        /// <summary>
        /// 所属平台
        /// </summary>
        public Platforms Platform { get; set; }

        [Comment("是否有子目录")]
        public bool HasChildren { set; get; }

        [Comment("shopee属性数据是否进行过解析")]
        public bool Flag { get; set; } = false;

        [Comment("shopee属性数据是否进行过更新解析")]
        public bool ChangeFlag { get; set; } = false;

        [Comment("归档数据")]
        public string? Datas { get; set; }

    }
}
