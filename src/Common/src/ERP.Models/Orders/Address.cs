//using System;
//using ERP.Models.Abstract;
//using Microsoft.EntityFrameworkCore;

//namespace ERP.Models.Orders;
////https://docs.developer.amazonservices.com/en_US/orders-2013-09-01/Orders_Datatypes.html#Order
///// <summary>
///// 收件人信息 
///// </summary>
//public class Address : UserModel
//{
//    public static void Set(Order order, Address address) => Set(address, order);
//    public static void Set(Address address, Order? order)
//    {
//        address.OrderInternal = order;
//        if (order != null)
//        {
//            order.ReceiverInternal = address;
//            address.Set(order);
//        }
//    }
//#pragma warning disable CS8618 // 在退出构造函数时，不可为 null 的字段必须包含非 null 值。请考虑声明为可以为 null。
//    public Address() { }
//#pragma warning restore CS8618 // 在退出构造函数时，不可为 null 的字段必须包含非 null 值。请考虑声明为可以为 null。
//    public int OrderID { get; set; }
//    private Order OrderInternal;
//    /// <summary>
//    /// 订单
//    /// </summary>
//    public Order Order { get => OrderInternal; set => Set(this, value); }
//    /// <summary>
//    /// 国家id(数据字典nation 主键)
//    /// </summary>
//    [Comment("国家id")]
//    public int? NationId { get; set; }

//    /// <summary>
//    /// 收件人名称
//    /// </summary>
//    [Comment("收件人名称")]
//    public string Name { get; set; } = string.Empty;

//    /// <summary>
//    /// 收件人的电话号码
//    /// </summary>
//    [Comment("收件人的电话号码")]
//    public string? Phone { get; set; }

//    /// <summary>
//    /// 收件人的邮政编码
//    /// </summary>
//    [Comment("收件人的邮政编码")]
//    public string? Zip { get; set; }

//    ///// <summary>
//    ///// 收货人所在州/省
//    ///// </summary>
//    [Comment("收货人所在州/省")]
//    public string? Province { get; set; }

//    /// <summary>
//    /// 收件人地址的地区。
//    /// </summary>
//    [Comment("收件人地址的县")]
//    public string? County { get; set; }

//    /// <summary>
//    /// 收件人地址的地区。
//    /// </summary>
//    [Comment("收件人地址的区")]
//    public string? District { get; set; }

//    /// <summary>
//    /// 收件人地址所在城市。
//    /// </summary>
//    [Comment("收件人地址所在城市")]
//    public string? City { get; set; }

//    /// <summary>
//    /// 收货人所在国家
//    /// </summary>
//    [Comment("收货人所在国家")]
//    public string? Nation { get; set; }

//    /// <summary>
//    /// 表示收件人所在地区的两位数代码
//    /// </summary>
//    [Comment("表示收件人所在地区的两位数代码")]
//    public string? NationShort { get; set; }

//    /// <summary>
//    /// 收货人详细地址1
//    /// </summary>
//    [Comment("收货人详细地址1")]
//    public string? Address1 { get; set; }

//    /// <summary>
//    /// 收货人详细地址2
//    /// </summary>
//    [Comment("收货人详细地址2")]
//    public string? Address2 { get; set; }

//    /// <summary>
//    /// 收货人详细地址3
//    /// </summary>
//    [Comment("收货人详细地址3")]
//    public string? Address3 { get; set; }

//    /// <summary>
//    /// 收货人邮箱
//    /// </summary>
//    [Comment("收货人邮箱")]
//    public string? Email { get; set; }

//}

