//using ERP.Data;
//using ERP.Models.Abstract;
//using Microsoft.EntityFrameworkCore;

//namespace ERP.Models.Orders;

//[Index(nameof(SourceType), nameof(SourceId))]
//[Index(nameof(DistributedType), nameof(DistributedId))]
//[Comment("被分配订单关联表")]
//public class OrderDistributedLink : UserModel
//{
//    /// <summary>
//    /// 来源id类型
//    /// </summary>
//    [Comment("来源id类型")]
//    public SourceIdType SourceType { get; set; }

//    /// <summary>
//    /// 来源id
//    /// </summary>
//    [Comment("来源id")]
//    public int SourceId { get; set; }

//    /// <summary>
//    /// 来源id类型
//    /// </summary>
//    [Comment("被分配id类型")]
//    public DistributedIdType DistributedType { get; set; }

//    /// <summary>
//    /// 被分配id
//    /// </summary>
//    [Comment("被分配id")]
//    public int DistributedId { get; set; }

//    /// <summary>
//    /// 订单分配类型
//    /// </summary>
//    public enum SourceIdType
//    {
//        OrderId,

//        /// <summary>
//        /// 分销公司
//        /// </summary>
//        DistributionCompanyId,
//        DistributionOrderId,
//    }

//    /// <summary>
//    /// 被分配的id表示类型
//    /// </summary>
//    public enum DistributedIdType
//    {
//        UserId,
//        GroupId,
//    }
//}
