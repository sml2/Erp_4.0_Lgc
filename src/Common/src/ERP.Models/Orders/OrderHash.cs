//using ERP.Models.Abstract;
//using Newtonsoft.Json;

//namespace ERP.Models.Orders;

//public class OrderHash : BaseModel
//{
//    public OrderHash() { }
//    public OrderHash(int orderId, string value, int num) => Set(orderId, value, num);
//    private void Set(int orderId, string value, int num)
//    {
//        //if (orderId <= 0) throw new Exception($"{nameof(OrderHash)} orderId不能<=0");
//        //if (value.IsNullOrWhiteSpace()) throw new Exception($"{nameof(OrderHash)} value不能为空");
//        Value = value;
//        Hash = Helpers.CreateHashCode(value);
//        OrderIds = JsonConvert.SerializeObject(new List<int> { orderId });
//        Number += num;
//    }

//    /// <summary>
//    /// sku或者goodId
//    /// </summary>
//    public string Value { get; set; } = string.Empty;
//    /// <summary>
//    /// value 的 hash
//    /// </summary>
//    public ulong Hash { get; set; }
//    /// <summary>
//    /// List<int>
//    /// </summary>
//    public string OrderIds { get; set; } = string.Empty;
//    /// <summary>
//    /// 产品销量
//    /// </summary>
//    public int Number { get; set; }

//    public List<int> GetOrderIds()
//    {
//        return JsonConvert.DeserializeObject<List<int>>(OrderIds) ?? new();
//    }
//    public void Update(int orderId, int num)
//    {
//        var ids = JsonConvert.DeserializeObject<List<int>>(OrderIds);
//        if (!(ids ??= new List<int>()).Contains(orderId))
//        {
//            ids.Add(orderId);
//        }
//        Number += num;
//        OrderIds = JsonConvert.SerializeObject(ids);
//    }
//}
