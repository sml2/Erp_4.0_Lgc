//using ERP.Models.Abstract;

//using Microsoft.EntityFrameworkCore;
//using System;
//using System.ComponentModel.DataAnnotations;

//namespace ERP.Models.Orders;

//[Index(nameof(OrderId), nameof(OrderNo), nameof(PlatformId), nameof(State), nameof(StoreId))]
//public class Refund : UserModel
//{
//    /// <summary>
//    /// 订单id
//    /// </summary>

//    [Comment("订单id")]
//    public int OrderId { get; set; }//订单id

//    /// <summary>
//    /// 订单编号
//    /// </summary>

//    [Comment("订单编号")]
//    public string? OrderNo { get; set; }//订单编号

//    /// <summary>
//    /// 订单图片地址
//    /// </summary>

//    [Comment("订单图片地址")]
//    public string? ImgUrl { get; set; }//订单图片地址

//    /// <summary>
//    /// 卖家店铺id
//    /// </summary>

//    [Comment("卖家店铺id")]
//    public int StoreId { get; set; }//卖家店铺id（平台-商店）

//    /// <summary>
//    /// 订单所在国家
//    /// </summary>

//    [Comment("订单所在国家")]
//    public string? Nation { get; set; }//订单所在国家

//    /// <summary>
//    /// 货币单位
//    /// </summary>

//    [Comment("货币单位")]
//    public string? Coin { get; set; }//货币单位

//    /// <summary>
//    /// 订单总价
//    /// </summary>

//    [Comment("订单总价")]
//    public decimal OrderMoney { get; set; }//订单总价(基准货币)

//    /// <summary>
//    /// 订单修改退款金额
//    /// </summary>

//    [Comment("订单修改退款金额(基准货币)")]
//    public decimal Value { get; set; }//订单修改退款金额(基准货币)

//    /// <summary>
//    /// 订单详情url
//    /// </summary>

//    [Comment("订单详情url")]
//    public string? InfoUrl { get; set; }//订单详情url

//    /// <summary>
//    /// 平台英文标识amazon
//    /// </summary>

//    [Comment("平台英文标识amazon")]
//    public string? Platfrom { get; set; }//平台英文标识amazon

//    /// <summary>
//    /// 所属平台id
//    /// </summary>

//    [Comment("所属平台id")]
//    public int PlatformId { get; set; }//所属平台id

//    /// <summary>
//    /// 订单拉取用户姓名
//    /// </summary>

//    [Comment("订单拉取用户姓名")]
//    public string? Truename { get; set; }//订单拉取用户姓名

//    /// <summary>
//    /// 退款操作用户id
//    /// </summary>

//    [Comment("退款操作用户id")]
//    public int OperateUserId { get; set; }//退款操作用户id

//    /// <summary>
//    /// 退款操作用户姓名
//    /// </summary>

//    [Comment("退款操作用户姓名")]
//    public string? OperateTruename { get; set; }//退款操作用户姓名

//    /// <summary>
//    /// 审核操作用户id
//    /// </summary>

//    [Comment("审核操作用户id")]
//    public int AuditUserId { get; set; }//审核操作用户id

//    /// <summary>
//    /// 审核操作用户姓名
//    /// </summary>

//    [Comment("审核操作用户姓名")]
//    public string? AuditTruename { get; set; }//审核操作用户姓名

//    /// <summary>
//    /// 上报公司id
//    /// </summary>

//    [Comment("上报公司id")]
//    public int ParentCompanyId { get; set; }//上报公司id

//    /// <summary>
//    /// 退款账单id
//    /// </summary>

//    [Comment("退款账单id")]
//    public int RefundBill { get; set; }//退款账单id

//    /// <summary>
//    /// 退款状态
//    /// </summary>

//    [Comment("退款状态")]
//    public RefundState State { get; set; }//退款状态1申请2通过3拒绝4失效5无需审核

//    public enum RefundState
//    {
//     // 申请
//         STATUS_APPLICATION,
//        // 通过
//         STATUS_BY,
//        // 拒绝
//         STATUS_REFUSE,
//        // 失效
//         STATUS_INVALID,
//        // 无需审核
//         STATUS_NOREVIEWREQUIRED 
//    }

//}


////-- ----------------------------
////-- Table structure for erp3_order_refund
////-- ----------------------------
////DROP TABLE IF EXISTS `erp3_order_refund`;
////CREATE TABLE `erp3_order_refund`  (
////  `id` bigint unsigned NOT NULL,
////  `i_user_id` int unsigned NOT NULL COMMENT '添加备注用户id',
////  `i_group_id` int unsigned NOT NULL COMMENT '当前用户组id',
////  `i_company_id` int unsigned NOT NULL COMMENT '订单所在公司id',
////  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
////  `i_order_id` int unsigned NOT NULL COMMENT '订单id',
////  `d_order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '订单编号',
////  `u_img_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '订单图片地址',
////  `i_store_id` int unsigned NOT NULL COMMENT '卖家店铺id（平台-商店）',
////  `d_nation` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '订单所在国家',
////  `d_coin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '货币单位',
////  `d_order_money` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '订单总价(基准货币)',
////  `d_order_refund` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '订单退款金额(基准货币)',
////  `d_refund` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '订单修改退款金额(基准货币)',
////  `d_info_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '订单详情url',
////  `d_platfrom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '平台英文标识amazon',
////  `i_platform_id` int unsigned NOT NULL COMMENT '所属平台id',
////  `d_truename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '订单拉取用户姓名',
////  `d_operate_user_id` int unsigned NOT NULL COMMENT '退款操作用户id',
////  `d_operate_truename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '退款操作用户姓名',
////  `d_audit_user_id` int unsigned NOT NULL COMMENT '审核操作用户id',
////  `d_audit_truename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '审核操作用户姓名',
////  `i_parent_company_id` int unsigned NOT NULL COMMENT '上报公司id',
////  `i_refund_bill` int unsigned NOT NULL COMMENT '退款账单id',
////  `t_state` tinyint unsigned NOT NULL COMMENT '退款状态1申请2通过3拒绝4失效5无需审核',
////  `created_at` timestamp(0) NULL DEFAULT NULL,
////  `updated_at` timestamp(0) NULL DEFAULT NULL,
////  PRIMARY KEY(`id`) USING BTREE,
//// INDEX `company_id`(`i_company_id`) USING BTREE,
//// INDEX `group_id`(`i_group_id`) USING BTREE,
//// INDEX `oem_id`(`i_oem_id`) USING BTREE,
//// INDEX `order_id`(`i_order_id`) USING BTREE,
//// INDEX `order_no`(`d_order_no`) USING BTREE,
//// INDEX `platform_id`(`i_platform_id`) USING BTREE,
//// INDEX `platfrom`(`d_platfrom`) USING BTREE,
//// INDEX `state`(`t_state`) USING BTREE,
//// INDEX `store_id`(`i_store_id`) USING BTREE,
//// INDEX `user_id`(`i_user_id`) USING BTREE
////) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '订单管理_退款管理' ROW_FORMAT = Dynamic;