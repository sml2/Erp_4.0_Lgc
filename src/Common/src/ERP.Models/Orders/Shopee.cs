
//using Newtonsoft.Json;

//namespace ERP.Models.Orders;

//public class Shopee
//{
//    /// <summary>
//    /// 订单编号，shopee唯一标识字段
//    /// </summary>
//    [JsonProperty("order_sn")]
//    public string OrderSn { get; set; } = string.Empty;

//    /// <summary>
//    /// 下订单的地区
//    /// </summary>
//    [JsonProperty("region")]
//    public string Region { get; set; } = string.Empty;

//    /// <summary>
//    /// 订单支付的货币单位的三位数代码
//    /// </summary>
//    [JsonProperty("currency")]
//    public string Currency { get; set; } = string.Empty;

//    /// <summary>
//    /// 此值指示订单是否为COD（货到付款）订单
//    /// </summary>
//    [JsonProperty("cod")]
//    public bool Cod { get; set; }

//    /// <summary>
//    /// 买方为订单支付的总金额。该金额包括项目的总销售价格、买方承担的运输成本；如适用，可通过Shopee促销进行抵消。此值仅在买方完成订单付款后返回
//    /// </summary>
//    [JsonProperty("total_amount")]
//    public float TotalAmount { get; set; }

//    /// <summary>
//    /// 定义订单当前状态的枚举类型
//    /// </summary>
//    [JsonProperty("order_status")]
//    public string OrderStatus { get; set; } = string.Empty;

//    /// <summary>
//    /// 买方为订单选择的物流服务提供商，以交付项目。
//    /// </summary>
//    [JsonProperty("shipping_carrier")]
//    public string ShippingCarrier { get; set; } = string.Empty;

//    /// <summary>
//    /// 买方为订单选择的付款方式。
//    /// </summary>
//    [JsonProperty("payment_method")]
//    public string PaymentMethod { get; set; } = string.Empty;

//    /// <summary>
//    /// 预计运费是Shopee根据特定物流快递标准计算的估计费用。
//    /// </summary>
//    [JsonProperty("estimated_shipping_fee")]
//    public float EstimatedShippingFee { get; set; }

//    /// <summary>
//    /// 默认情况下返回。给卖家的信息。
//    /// </summary>
//    [JsonProperty("message_to_seller")]
//    public string MessageToSeller { get; set; } = string.Empty;

//    /// <summary>
//    /// 默认情况下返回。指示创建订单的日期和时间的时间戳。
//    /// </summary>
//    [JsonProperty("create_time")]
//    public long CreateTime { get; set; }

//    /// <summary>
//    /// 默认情况下返回。时间戳，指示订单价值最后一次发生变化的时间，例如订单状态从“已支付”更改为“已完成”。
//    /// </summary>
//    [JsonProperty("update_time")]
//    public long UpdateTime { get; set; }

//    /// <summary>
//    /// 默认情况下返回。卖方在Shopee上列出物品时设置的装运准备时间。
//    /// </summary>
//    [JsonProperty("days_to_ship")]
//    public int DaysToShip { get; set; }

//    /// <summary>
//    /// 默认情况下返回。寄出包裹的最后期限。
//    /// </summary>
//    [JsonProperty("ship_by_date")]
//    public int ShipByDate { get; set; }

//    /// <summary>
//    /// 此订单的买家的用户id
//    /// </summary>
//    [JsonProperty("buyer_user_id")]
//    public int BuyerUserId { get; set; }

//    /// <summary>
//    /// 买方名称
//    /// </summary>
//    [JsonProperty("buyer_username")]
//    public string BuyerUserName { get; set; } = string.Empty;


//    /// <summary>
//    /// 包含收件人地址的详细明细。
//    /// </summary>
//    [JsonProperty("recipient_address")]
//    public RecipientAddress RecipientAddress { get; set; } = new();


//    /// <summary>
//    /// 订单的实际运费（如果可从外部物流合作伙伴处获得）
//    /// </summary>
//    [JsonProperty("actual_shipping_fee")]
//    public float ActualShippingFee { get; set; }


//    /// <summary>
//    /// 仅适用于跨境订单。此值表示订单是否包含需要在海关申报的货物。“T”表示真实，并将在装运标签上标记为“T”；
//    /// “F”表示错误，并将在装运标签上标记为“P”。此值仅在生成订单跟踪号后才准确，请在检索跟踪号后捕获此值。
//    /// </summary>
//    [JsonProperty("goods_to_declare")]
//    public bool GoodsToDeclare { get; set; }

//    /// <summary>
//    /// 注释
//    /// </summary>
//    [JsonProperty("note")]
//    public string Note { get; set; } = string.Empty;

//    /// <summary>
//    /// 更新注释的时间。
//    /// </summary>
//    [JsonProperty("note_update_time")]
//    public long NoteUpdateTime { get; set; }

//    /// <summary>
//    /// 商品list
//    /// </summary>
//    [JsonProperty("item_list")]
//    public IEnumerable<Item> ItemList { get; set; } = Enumerable.Empty<Item>();

//    /// <summary>
//    /// 订单状态从未付款更新为已付款的时间。订单尚未付款时，此值为空。
//    /// </summary>
//    [JsonProperty("pay_time")]
//    public long PayTime { get; set; }

//    /// <summary>
//    /// 仅限印尼订单。托运人的姓名
//    /// </summary>
//    [JsonProperty("dropshipper")]
//    public string Dropshipper { get; set; } = string.Empty;

//    /// <summary>
//    /// dropshipper的电话号码
//    /// </summary>
//    [JsonProperty("dropshipper_phone")]
//    public string DropshipperPhone { get; set; } = string.Empty;

//    ///// <summary>
//    ///// 
//    ///// </summary>
//    //[JsonProperty("credit_card_number")]
//    //public string CreditCardNumber { get; set; }


//    /// <summary>
//    /// 指示此订单是否拆分为fullfil订单（forder）级别。如果是“真的”，请调用GetForderInfo。
//    /// </summary>
//    [JsonProperty("split_up")]
//    public bool SplitUp { get; set; }

//    /// <summary>
//    /// 买方取消原因，可以为空
//    /// </summary>
//    [JsonProperty("buyer_cancel_reason")]
//    public string BuyerCancelReason { get; set; } = string.Empty;

//    /// <summary>
//    /// 可以是买方、卖方、系统或Ops中的一个
//    /// </summary>
//    [JsonProperty("cancel_by")]
//    public string CancelBy { get; set; } = string.Empty;

//    /// <summary>
//    /// 获取买方、卖方和系统取消的原因
//    /// </summary>
//    [JsonProperty("cancel_reason")]
//    public string CancelReason { get; set; } = string.Empty;

//    /// <summary>
//    /// 判断是否确认实际装运费用
//    /// </summary>
//    [JsonProperty("actual_shipping_fee_confirmed")]
//    public bool ActualShippingFeeConfirmed { get; set; }

//    /// <summary>
//    /// 用于税务和发票目的的买方CPF编号。仅限巴西订单。
//    /// </summary>
//    [JsonProperty("buyer_cpf_id")]
//    public string BuyerCpfId { get; set; } = string.Empty;

//    /// <summary>
//    /// 使用此字段表示订单是由购物者或卖家完成的。适用值：买方履行、cb卖方履行、本地卖方履行。
//    /// </summary>
//    [JsonProperty("fulfillment_flag")]
//    public string FulfillmentFlag { get; set; } = string.Empty;

//    /// <summary>
//    /// 完成时的时间戳
//    /// </summary>
//    [JsonProperty("pickup_done_time")]
//    public long PickupDoneTime { get; set; }

//    /// <summary>
//    /// 包裹信息list
//    /// </summary>
//    [JsonProperty("package_list")]
//    public List<PackageList> PackageList { get; set; } = new();

//    /// <summary>
//    /// 订单的发票数据
//    /// </summary>
//    [JsonProperty("invoice_data")]
//    public invoice_data invoice_data { get; set; } = new();

//    /// <summary>
//    /// 对于非屏蔽订单，指买方为订单选择的物流服务提供商，以交付项目。对于蒙版订单，买方为订单选择的物流服务类型，以交付项目。
//    /// </summary>
//    [JsonProperty("checkout_shipping_carrier")]
//    public string CheckoutShippingCarrier { get; set; } = string.Empty;

//    /// <summary>
//    /// Shopee对退回的订单收取反向运费。此字段的值将为非负值。
//    /// </summary>
//    [JsonProperty("reverse_shipping_fee")]
//    public float ReverseShippingFee { get; set; }



//    public static DateTime ConvertLongToDateTime(long d)
//    {
//        DateTime dtStart = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
//        long lTime = long.Parse(d + "0000");
//        TimeSpan toNow = new TimeSpan(lTime);
//        DateTime dtResult = dtStart.Add(toNow);
//        return dtResult;
//    }
//}

//public class RecipientAddress
//{
//    /// <summary>
//    /// 收件人的地址名称。
//    /// </summary>
//    [JsonProperty("name")]
//    public string Name { get; set; } = string.Empty;

//    /// <summary>
//    /// 下单时输入收件人的电话号码。
//    /// </summary>
//    [JsonProperty("phone")]
//    public string Phone { get; set; } = string.Empty;

//    /// <summary>
//    /// 收件人地址所在的城镇。是否有城镇将取决于地区和/或国家。
//    /// </summary>
//    [JsonProperty("town")]
//    public string Town { get; set; } = string.Empty;

//    /// <summary>
//    /// 收件人地址的地区。是否有地区将取决于地区和/或国家。
//    /// </summary>
//    [JsonProperty("district")]
//    public string District { get; set; } = string.Empty;

//    /// <summary>
//    /// 收件人地址所在城市。是否有城市将取决于地区和/或国家。
//    /// </summary>
//    [JsonProperty("city")]
//    public string City { get; set; } = string.Empty;

//    /// <summary>
//    /// 收件人地址所在的州/省。是否存在州/省将取决于地区和/或国家。
//    /// </summary>
//    [JsonProperty("state")]
//    public string State { get; set; } = string.Empty;

//    /// <summary>
//    /// 表示收件人所在地区的两位数代码。
//    /// </summary>
//    [JsonProperty("region")]
//    public string Region { get; set; } = string.Empty;

//    /// <summary>
//    /// 收件人的邮政编码。
//    /// </summary>
//    [JsonProperty("zipcode")]
//    public string Zipcode { get; set; } = string.Empty;

//    /// <summary>
//    /// 收件人的完整地址，包括国家、州、甚至街道等。
//    /// </summary>
//    [JsonProperty("full_address")]
//    public string FullAddress { get; set; } = string.Empty;
//}

//public class Item
//{
//    /// <summary>
//    /// 商品的唯一标识符。
//    /// </summary>
//    [JsonProperty("item_id")]
//    public long ItemId { get; set; }


//    /// <summary>
//    /// 商品名称
//    /// </summary>
//    [JsonProperty("item_name")]
//    public string ItemName { get; set; } = string.Empty;

//    /// <summary>
//    /// 商品的SKU
//    /// </summary>
//    [JsonProperty("item_sku")]
//    public string ItemSku { get; set; } = string.Empty;

//    /// <summary>
//    /// 变体id
//    /// </summary>
//    [JsonProperty("model_id")]
//    public int ModelId { get; set; }

//    /// <summary>
//    /// 变体名称
//    /// </summary>
//    [JsonProperty("model_name")]
//    public string ModelName { get; set; } = string.Empty;

//    /// <summary>
//    /// 变体sku
//    /// </summary>
//    [JsonProperty("model_sku")]
//    public string ModelSku { get; set; } = string.Empty;

//    /// <summary>
//    /// 某个变体下购买的数量
//    /// </summary>
//    [JsonProperty("model_quantity_purchased")]
//    public int ModelQuantityPurchased { get; set; }

//    /// <summary>
//    /// 
//    /// </summary>
//    [JsonProperty("model_original_price")]
//    public float ModelOriginalPrice { get; set; }

//    /// <summary>
//    /// 折扣后的价格
//    /// </summary>
//    [JsonProperty("model_discounted_price")]
//    public float ModelDiscountedPrice { get; set; }

//    /// <summary>
//    /// 买方是否以批发价购买订单项目
//    /// </summary>
//    [JsonProperty("wholesale")]
//    public bool Wholesale { get; set; }

//    /// <summary>
//    /// 物品的重量
//    /// </summary>
//    [JsonProperty("weight")]
//    public float Weight { get; set; }

//    /// <summary>
//    /// 此项目是否属于附加交易
//    /// </summary>
//    [JsonProperty("add_on_deal")]
//    public bool AddOnDeal { get; set; }

//    /// <summary>
//    /// 此项是主项还是子项。True表示主项，false表示子项
//    /// </summary>
//    [JsonProperty("main_item")]
//    public bool MainItem { get; set; }


//    /// <summary>
//    /// 用于区分购物车中的项目组和订单的唯一ID
//    /// </summary>
//    [JsonProperty("add_on_deal_id")]
//    public int AddOnDealId { get; set; }

//    /// <summary>
//    /// 可用类型：产品促销、快闪销售、分组、捆绑交易、在交易主界面上添加、在交易子界面上添加
//    /// </summary>
//    [JsonProperty("promotion_type")]
//    public string PromotionType { get; set; } = string.Empty;

//    /// <summary>
//    /// 促销活动的ID
//    /// </summary>
//    [JsonProperty("promotion_id")]
//    public int PromotionId { get; set; }

//    /// <summary>
//    /// 订单项的标识
//    /// </summary>
//    [JsonProperty("order_item_id")]
//    public int OrderItemId { get; set; }

//    /// <summary>
//    /// 产品促销的识别
//    /// </summary>
//    [JsonProperty("promotion_group_id")]
//    public int PromotionGroupId { get; set; }

//    /// <summary>
//    /// 产品的图像信息
//    /// </summary>
//    [JsonProperty("image_info")]
//    public ImageInfo ImageInfo { get; set; } = new();

//}

//public class ImageInfo
//{
//    public string image_url { get; set; } = string.Empty;
//}

//public class PackageList
//{
//    /// <summary>
//    /// 订单下包裹的Shopee唯一标识符。
//    /// </summary>
//    [JsonProperty("package_number")]
//    public string PackageNumber { get; set; } = string.Empty;

//    /// <summary>
//    /// 订单的Shopee物流状态。适用值：请参阅数据定义LogisticsStatus。
//    /// </summary>
//    [JsonProperty("logistics_status")]
//    public string LogisticsStatus { get; set; } = string.Empty;

//    /// <summary>
//    /// 买方为订单选择的物流服务提供商，以交付项目。
//    /// </summary>
//    [JsonProperty("shipping_carrier")]
//    public string ShippingCarrier { get; set; } = string.Empty;

//    /// <summary>
//    /// 包括内商品信息
//    /// </summary>
//    [JsonProperty("item_list")]
//    public List<Cls_item> ItemList { get; set; } = new();
//}

//public class Cls_item
//{
//    /// <summary>
//    /// 商品的id
//    /// </summary>
//    [JsonProperty("item_id")]
//    public int ItemId { get; set; }

//    /// <summary>
//    /// 商品变体的id
//    /// </summary>
//    [JsonProperty("model_id")]
//    public int ModelId { get; set; }
//}

//public class invoice_data
//{
//    /// <summary>
//    /// 发票号码。数字应该是9位
//    /// </summary>
//    [JsonProperty("number")]
//    public string Number { get; set; } = string.Empty;

//    /// <summary>
//    /// 发票的序列号。序列号应为3位数字
//    /// </summary>
//    [JsonProperty("series_number")]
//    public string SeriesNumber { get; set; } = string.Empty;

//    /// <summary>
//    /// 发票的访问密钥。访问密钥应为44位数字。
//    /// </summary>
//    [JsonProperty("access_key")]
//    public string AccessKey { get; set; } = string.Empty;

//    /// <summary>
//    /// 发票的签发日期。发行日期应晚于订单付款日期
//    /// </summary>
//    [JsonProperty("issue_date")]
//    public long IssueDate { get; set; }

//    /// <summary>
//    /// 发票的总价值
//    /// </summary>
//    [JsonProperty("total_value")]
//    public float TotalValue { get; set; }

//    /// <summary>
//    /// 产品发票的总价值
//    /// </summary>
//    [JsonProperty("products_total_value")]
//    public float ProductsTotalValue { get; set; }

//    /// <summary>
//    /// 发票的税务代码。税号应为4位数字。
//    /// </summary>
//    [JsonProperty("tax_code")]
//    public string TaxCode { get; set; } = string.Empty;

//}
