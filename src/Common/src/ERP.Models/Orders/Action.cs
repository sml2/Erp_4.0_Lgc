//using ERP.Models.Abstract;

//using Microsoft.EntityFrameworkCore;

//using System.ComponentModel.DataAnnotations;
//using ERP.Enums.Orders;
//using ERP.Models.DB.Identity;

//namespace ERP.Models.Orders;

//[Index(nameof(OrderId))]
//public class Action : UserModel
//{
//    public Action() { }
//    public Action(string action, int orderId, int userId, int groupId, int companyId, int oemId, string userName, string trueName, States state)
//    {
//        OrderId = orderId;
//        Value = action;
//        UserID = userId;
//        GroupID = groupId;
//        CompanyID = companyId;
//        OEMID = oemId;
//        UserName = userName;
//        Truename = trueName;
//        OrderStatus = state;
//        OrderStatusText = EnumExtension.GetDescription(state);
//    }
//    public Action(string action, User user, Order order)
//        : this(action, order.ID, user.ID, user.GroupID, user.CompanyID, user.OEMID, user.UserName, user.TrueName, order.State)
//    { }
//    /// <summary>
//    /// 订单id
//    /// </summary>

//    [Comment("订单id")]
//    public int OrderId { get; set; }

//    /// <summary>
//    /// 动作内容
//    /// </summary>

//    [Comment("动作内容")]
//    public string? Value { get; set; }

//    /// <summary>
//    /// 操作人
//    /// </summary>

//    [Comment("操作人")]
//    public string? UserName { get; set; }

//    /// <summary>
//    /// 操作用户姓名
//    /// </summary>

//    [Comment("操作用户姓名")]
//    public string? Truename { get; set; }

//    /// <summary>
//    /// 订单状态
//    /// </summary>

//    [Comment("订单状态")]
//    public States OrderStatus { get; set; }

//    /// <summary>
//    /// 订单状态名称
//    /// </summary>

//    [Comment("订单状态名称")]
//    public string? OrderStatusText { get; set; }
//}


////-- ----------------------------
////-- Table structure for erp3_amazon_order_action
////-- ----------------------------
////DROP TABLE IF EXISTS `erp3_amazon_order_action`;
////CREATE TABLE `erp3_amazon_order_action`  (
////  `id` bigint unsigned NOT NULL,
////  `i_user_id` int unsigned NOT NULL COMMENT '操作人id',
////  `i_group_id` int unsigned NOT NULL COMMENT '当前用户组id',
////  `i_company_id` int unsigned NOT NULL COMMENT '当前拉取用户所在公司id',
////  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
////  `i_order_id` int unsigned NOT NULL COMMENT '订单id',
////  `d_action` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '动作内容',
////  `d_user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作人',
////  `d_truename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作用户姓名',
////  `t_order_status` int unsigned NOT NULL COMMENT '订单状态',
////  `d_order_status_text` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '订单状态名称',
////  `created_at` timestamp(0) NULL DEFAULT NULL,
////  `updated_at` timestamp(0) NULL DEFAULT NULL,
////  PRIMARY KEY(`id`) USING BTREE,
//// INDEX `company_id`(`i_company_id`) USING BTREE,
//// INDEX `group_id`(`i_group_id`) USING BTREE,
//// INDEX `oem_id`(`i_oem_id`) USING BTREE,
//// INDEX `order_id`(`i_order_id`) USING BTREE,
//// INDEX `user_id`(`i_user_id`) USING BTREE
////) ENGINE = InnoDB AUTO_INCREMENT = 1414919 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '亚马逊订单_订单操作日志表' ROW_FORMAT = Dynamic;