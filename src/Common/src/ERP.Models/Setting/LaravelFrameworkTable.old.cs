﻿//-- ----------------------------
//-- Table structure for erp3_failed_jobs
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_failed_jobs`;
//CREATE TABLE `erp3_failed_jobs`  (
//  `id` bigint unsigned NOT NULL,
//  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
//  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
//  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
//  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
//  `failed_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
//  PRIMARY KEY (`id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '失败任务队列' ROW_FORMAT = Dynamic;
//-- ----------------------------
//-- Table structure for erp3_migrations
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_migrations`;
//CREATE TABLE `erp3_migrations`  (
//  `id` int unsigned NOT NULL,
//  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
//  `batch` int(0) NOT NULL,
//  PRIMARY KEY (`id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 76 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;