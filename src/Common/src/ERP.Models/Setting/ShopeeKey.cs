using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.Setting;

public class ShopeeKey : BaseModel
{
    [Comment("名称")]
    public string Name { get; set; } = string.Empty;

    public int PartnerId { get; set; }

    public string SecretKey { get; set; } = string.Empty;

    public bool ApiMode { get; set; } = false;

    [Comment("系统地址")]
    public string RedirectUri { get; set; } = string.Empty;

    [Comment("备注")]
    public string Remark { get; set; } = string.Empty;
}
