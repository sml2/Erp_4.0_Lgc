using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.Setting;

public class AmazonSPKey : BaseModel
{
    [Comment("名称")]
    public string Name { get; set; } = string.Empty;

    public string AccessKey { get; set; } = string.Empty;

    public string SecretKey { get; set; } = string.Empty;

    [Comment("角色")]
    public string RoleArn { get; set; } = string.Empty;
    [Comment("ClientId")]
    public string ClientId { get; set; } = string.Empty;

    [Comment("ClientSecret")]
    public string ClientSecret { get; set; } = string.Empty;

    [Comment("系统地址")]
    public string RedirectUri { get; set; } = string.Empty;

    [Comment("ApplicationId")]
    public string ApplicationId { get; set; } = string.Empty;

    [Comment("备注")]
    public string Remark { get; set; } = string.Empty;
}
