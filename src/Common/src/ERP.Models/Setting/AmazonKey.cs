using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.Setting;
/// <summary>
/// 亚马逊开发者密钥
/// </summary>
public class AmazonKey : BaseModel
{
    [Comment("标题")]
    public string Title { get; set; } = string.Empty;

    [Comment("编号")]
    public string CodeNumber { get; set; } = string.Empty;

    [Comment("名称")]
    public string Name { get; set; } = string.Empty;

    public string AccessKey { get; set; } = string.Empty;

    public string SecretKey { get; set; } = string.Empty;

    [Comment("备注")]
    public string Remark { get; set; } = string.Empty;
}
