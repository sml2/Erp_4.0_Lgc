using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.Models.Setting
{
    public class Unit : BaseModel
    {
        public Unit() { }
        public Unit(int id, string name, string sign, decimal rate, DateTime refreceTime)
        {
            ID = id;
            Name = name;
            Sign = sign;
            Rate = rate;
            RefreceTime = refreceTime;
        }

        [Comment("名称")]
        public string Name { get; set; } = string.Empty;

        [Comment("简称")]
        public string Sign { get; set; } = string.Empty;

        [Column(TypeName = "decimal(22,6)"), Comment("汇率")]
        public decimal Rate { get; set; }
        /// <summary>
        /// 汇率服务器刷新日期
        /// </summary>
        [Comment("汇率服务器刷新日期")]
        public DateTime RefreceTime { get; set; }
        public override string ToString()
        {
            return $"Name[{Sign}]->{Rate}";
        }
    }
}
