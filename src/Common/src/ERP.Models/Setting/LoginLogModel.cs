using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ERP.Extensions;
using ERP.Models.DB.Users;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using ERP.Models.Users;

namespace ERP.Models.Setting;

public class LoginLogModel : BaseModel
{
    public LoginLogModel()
    {
    }

    public LoginLogModel(ISession sessin, DateTime now)
    {
        OemId = sessin.GetOEMID();
        UserId = sessin.GetUserID();
        Username = sessin.GetUserName();
        CreatedAt = now;
        UpdatedAt = now;
    }

    public LoginLogModel(UserOnlineModel userOnline, DateTime now)
    {
        OemId = userOnline.OemId;
        UserId = userOnline.UserId;
        Username = userOnline.Username;
        LoginIp = userOnline.LoginIP;
        CreatedAt = now;
        UpdatedAt = now;
        Ext = JsonConvert.SerializeObject(userOnline);
    }

    [Comment("OemId")]
    public int OemId { get; set; }

    [Comment("用户id")]
    public int UserId { get; set; }

    [Comment("用户名")]
    public string Username { get; set; }

    public enum Types
    {
        [Description("登录")] Login = 1,

        [Description("正常退出")] NormalExit = 2,

        [Description("超时登出")] TimeoutLogout = 3,

        [Description("被挤下线")] SqueezedOffline = 4,

        [Description("密码修改下线")] PasswordModificationOffline = 5,

        [Description("权限变更下线")] PermissionChangeOffline = 6,

        [Description("账户禁用下线")] AccountDisableOffline = 7,

        [Description("异常退出")] ErrorNormalExit = 8,
    }


    [Comment("1登录,2正常退出,3超时登出,4被挤下线,5密码修改下线,6权限变更下线,7账户禁用下线")]
    public Types Type { get; set; }

    [Comment("登录ip")]
    public string? LoginIp { get; set; }

    // [Comment("最后操作时间")]
    // public DateTime? LastTime { get; set; }

    [Comment("备注")]
    public string? Remark { get; set; }


    [Comment("拓展字段")]
    public string? Ext { get; set; }
}

//-- ----------------------------
//-- Table structure for erp3_login_log
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_login_log`;
//CREATE TABLE `erp3_login_log`  (
//  `id` bigint unsigned NOT NULL,
//  `i_user_id` bigint unsigned NOT NULL COMMENT '用户id',
//  `d_username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '用户名',
//  `i_oem_id` bigint unsigned NOT NULL COMMENT 'Oemid',
//  `t_type` bigint unsigned NOT NULL COMMENT '1登录,2正常退出,3超时登出,4被挤下线,5密码修改下线,6权限变更下线,7账户禁用下线',
//  `d_login_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '登录IP',
//  `d_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '备注',
//  `d_ext` json NULL COMMENT '拓展字段,{LastTime}',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 655684 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户管理_登录日志' ROW_FORMAT = Dynamic;