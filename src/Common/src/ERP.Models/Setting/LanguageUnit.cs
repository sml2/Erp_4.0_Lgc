using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.Models.Setting
{
    [Table("language_unit"),
     Index(nameof(LanguageId), nameof(UnitId), nameof(Type), nameof(UserId), nameof(CompanyId), nameof(OemId))]
    public class LanguageUnit : BaseModel
    {
        [Key]
        public new int ID { get; set; }

        [Comment("语言ID")]
        public int LanguageId { get; set; }

        [Comment("货币单位ID")]
        public int UnitId { get; set; }

        [Comment("1系统 2公司 3用户")]
        public int Type { get; set; }

        [Comment("用户id")]
        public int UserId { get; set; }

        [Comment("公司Id")]
        public int CompanyId { get; set; }

        [Comment("Oem id")]
        public int OemId { get; set; }

        [Comment("排序")]
        public int Sort { get; set; } = 0;
    }
}


//------------------------------
//--Table structure for erp3_language_unit
//------------------------------
//DROP TABLE IF EXISTS `erp3_language_unit`;
//CREATE TABLE `erp3_language_unit`  (
//  `id` bigint unsigned NOT NULL,
//  `i_language_id` int(0) NOT NULL DEFAULT 0 COMMENT '语言id',
//  `i_unit_id` int(0) NOT NULL DEFAULT 0 COMMENT '货币单位id',
//  `t_type` tinyint(0) NOT NULL DEFAULT 1 COMMENT '1系统 2公司 3用户',
//  `i_user_id` int(0) NOT NULL DEFAULT 0 COMMENT '用户id',
//  `i_company_id` int(0) NOT NULL DEFAULT 0 COMMENT '所属公司id',
//  `i_oem_id` int(0) NOT NULL DEFAULT 0 COMMENT '当前Oem id',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
// PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `language_id`(`i_language_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `type`(`t_type`) USING BTREE,
// INDEX `unit_id`(`i_unit_id`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统设置_语言货币配置' ROW_FORMAT = Dynamic;