﻿using System;
namespace ERP.Models.DB.Setting;
//------------------------------
//--Table structure for erp3_unit
//------------------------------
//DROP TABLE IF EXISTS `erp3_unit`;
//CREATE TABLE `erp3_unit`  (
//  `id` bigint unsigned NOT NULL,
//  `d_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '名称',
//  `d_sign` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '简写',
//  `d_rate` decimal(22, 6) NOT NULL DEFAULT 0.000000 COMMENT '转换基准 10000/汇率',
//  `rate_date` date NOT NULL COMMENT '获取汇率服务器日期',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
// PRIMARY KEY(`id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 171 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '数据字典_货币单位' ROW_FORMAT = Dynamic;


