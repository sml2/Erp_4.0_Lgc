using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.Models.Setting
{
    //邮箱配置项
    [Table("email_config"), Index(nameof(PopSSL), nameof(SmtpSSL), nameof(State), nameof(Sort))]
    [Comment("邮箱管理_系统预设邮箱")]
    public class EmailConfig : BaseModel
    {
        [Key]
        public new int ID { get; set; }

        [Comment("邮箱名称")]
        public string Name { get; set; } = string.Empty;

        [Comment("邮箱域名")]
        public string Domain { get; set; } = string.Empty;

        [Comment("pop地址")]
        public string Pop { get; set; } = string.Empty;

        [Comment("smtp地址")]
        public string Smtp { get; set; } = string.Empty;

        [Comment("pop端口")]
        public int PopPort { get; set; }

        [Comment("1启用 2关闭")]
        public StateEnum PopSSL { get; set; } = StateEnum.Open;

        [Comment("smtp端口")]
        public int SmtpPort { get; set; }

        [Comment("1启用 2关闭")]
        public StateEnum SmtpSSL { get; set; } = StateEnum.Open;

        [Comment("排序")]
        public int Sort { get; set; } = 0;

        [Comment("1启用 2禁用")]
        public StateEnum State { get; set; }
    }
}


//------------------------------
//--Table structure for erp3_email_config
//------------------------------
//DROP TABLE IF EXISTS `erp3_email_config`;
//CREATE TABLE `erp3_email_config`  (
//  `id` bigint unsigned NOT NULL,
//  `d_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '邮箱名称',
//  `d_domain` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '邮箱域名',
//  `d_pop` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'pop地址',
//  `d_smtp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'stmp地址',
//  `d_pop_port` int unsigned NOT NULL COMMENT 'pop端口',
//  `d_smtp_port` int unsigned NOT NULL COMMENT 'smtp端口',
//  `t_pop_ssl` tinyint unsigned NOT NULL COMMENT '1启用 2关闭',
//  `t_smtp_ssl` tinyint unsigned NOT NULL COMMENT '1启用 2关闭',
//  `d_sort` bigint(0) NOT NULL DEFAULT 0 COMMENT '排序',
//  `t_state` tinyint unsigned NOT NULL COMMENT '1启用 2禁用',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
// PRIMARY KEY(`id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '邮箱管理_系统预设邮箱' ROW_FORMAT = Dynamic;