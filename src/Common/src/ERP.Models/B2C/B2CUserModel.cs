using ERP.Enums.Identity;
using ERP.Extensions;
using ERP.Models.Abstract;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using User = ERP.Models.DB.Identity.User;

namespace ERP.Models.B2C;

public class B2CUserModel : BaseModel
{
    
    public B2CUserModel()
    {}
    public B2CUserModel(User user, ISession session)
    {
        UserId = user.Id;
        CompanyId = user.CompanyID;
        Username = user.UserName;
        Truename = user.TrueName;
        DomainNum = 0;
        SelectionNum = 0;
        OperatorUserId = session.GetUserID();
        OperatorUsername = session.GetUserName();
        OperatorTruename = session.GetTrueName();
        Status = StateEnum.Open;
    }
    
    [Comment("b2c用户id")]
    public int UserId { get; set; }

    [Comment("b2c公司id")]
    public int CompanyId { get; set; }

    [Comment("b2c用户名")]
    public string Username { get; set; }

    [Comment("b2c用户姓名")]
    public string Truename { get; set; }

    [Comment("域名数量")]
    public int DomainNum { get; set; }

    [Comment("产品库数量")]
    public int SelectionNum { get; set; }

    [Comment("操作人Id")]
    public int OperatorUserId { get; set; }

    [Comment("操作人用户名")]
    public string OperatorUsername { get; set; }

    [Comment("操作人姓名")]
    public string OperatorTruename { get; set; }

    [Comment("状态")]
    public StateEnum Status { get; set; } = StateEnum.Open;

}