using System.ComponentModel.DataAnnotations.Schema;
using ERP.Extensions;
using ERP.Models.Abstract;
using Microsoft.AspNetCore.Http;

namespace ERP.Models.B2C;

public class DomainModel : UserModel
{
    public DomainModel()
    {
    }

    public void Update(string name, string url, string domain)
    {
        Name = name;
        Url = url;
        Domain = domain;
        UpdatedAt = DateTime.Now;
    }

    public string Name { get; set; }
    public string Url { get; set; }
    public string Domain { get; set; }
    
    /// <summary>
    /// 是否 Situne平台
    /// </summary>
    [NotMapped]
    public bool IsSitune
    {
        get => Domain.Contains("situne");
    }
}