using ERP.Models.Product;
using ERP.Models.Template;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models;

public partial class BaseDBContext
{
    public DbSet<ProductMarkDeletionModel> ProductMarkDeletion { get; set; } = null!;
    public DbSet<ProductValidityModel> ProductValidity { get; set; } = null!;
    public DbSet<IllegalWordModel> IllegalWord { get; set; } = null!;
    public DbSet<CategoryModel> Category { get; set; } = null!;
    public DbSet<ToSellModel> ToSell { get; set; } = null!;
    public DbSet<TaskModel> Task { get; set; } = null!;
    
    #region template
    public DbSet<ExtTemplateModel> ExtTemplate { get; set; } = null!;
    public DbSet<KeywordTemplateModel> KeywordTemplate { get; set; } = null!;
    public DbSet<ChangePriceTemplateModel> ChangePriceTemplate { get; set; } = null!;
    public DbSet<SketchTemplateModel> SketchTemplate { get; set; } = null!;
    #endregion
}