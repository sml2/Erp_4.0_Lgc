using Microsoft.EntityFrameworkCore;

using ERP.Models.Setting;
using ERP.Models.Abstract;
using ERP.Models.ProductNodeType;
using ERP.Enums;

namespace ERP.Models;
public partial class PlatformData : BaseModel
{
    public PlatformData() { }

    public int? NationId { get; set; }

    [Comment("所属国家")]
    /// <summary>
    /// 所属平台
    /// </summary>
    public Nation? Nation { get; set; }

    [Comment("所属平台")]
    /// <summary>
    /// 所属平台
    /// </summary>
    public Platforms Platform { get; set; }

    [Comment("平台数据")]
    public string Data { get; set; }=string.Empty;
    
    public void SetData<T>(T data)
    {
        //Data = System.Text.Json.JsonSerializer.Serialize(data);
        Data = Newtonsoft.Json.JsonConvert.SerializeObject(data);
    }
    /// <summary>
    /// 安全转换，但可能为空
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="maker"></param>
    /// <returns></returns>
    private T? GetDataSafe<T>(Action<T>? maker = null)
    {
        var data = System.Text.Json.JsonSerializer.Deserialize<T>(Data);
        if (maker is not null && data is not null) maker(data);
        return data;
    }   
    /// <summary>
    /// 非安全转换
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="maker"></param>
    /// <returns></returns>
    /// <exception cref="NullReferenceException"></exception>
    private T GetData<T>(Action<T>? maker = null)
    {
        var data = GetDataSafe<T>(maker);
        if (data is not null)
        {
            return data;
        }
        else
        {
            throw new NullReferenceException();
        }
    }
    public AmazonData? GetAmazonData() => GetDataSafe<AmazonData>();
    public LogisticsData? GetLogisticsData() => GetDataSafe<LogisticsData>();
    public AmazonDataWithID? GetAmazonDataWithID() => GetDataSafe<AmazonDataWithID>(d => d.ID = ID);
    public ShopeeData? GetShopeeDataSafe() => GetDataSafe<ShopeeData>();
    public ShopeeData GetShopeeData() => GetData<ShopeeData>();
    public SFCData? GetSFCData() => GetDataSafe<SFCData>();

    public List<AreaCategoryRelation> AreaCategoryRelations { get; set; } = new();
    public override string ToString()
    {
        return $"ID[{ID}] Platform[{Platform}] Nation[{Nation}]";
    }
}

public class ErrorDatas
{
    public ErrorDatas()
    {

    }      

    public ErrorDatas(Platforms platform, int? amzFormRawID, int? amzRSRID, string message)
    {
        Platform = platform;
        AmzFormRawID = amzFormRawID;
        AmzRSRID = amzRSRID;
        Message = message;
    }      
    public int ID { get; set; }
    public Platforms Platform { get; set; }
    public int? AmzFormRawID { get; set; }
    public int? AmzRSRID { get; set; }
    public string Message { get; set; } = string.Empty;

    public DateTime Datetime { get; set; } = DateTime.Now;
}
