using System.ComponentModel.DataAnnotations.Schema;
using ERP.Extensions;
using ERP.Models.Product;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.Template;

[Table("change_price_template")]
[Index(nameof(NationShort))]
[Comment("产品管理_改价模板")]
public class ChangePriceTemplateModel : UserRelated
{
    public ChangePriceTemplateModel()
    {
    }

    public ChangePriceTemplateModel(ISession session)
    {
        UserID = session.GetUserID();
        GroupID = session.GetGroupID();
        CompanyID = session.GetCompanyID();
        OEMID = session.GetOEMID();
    }
    
    [Comment("名称")]
    public string Name { get; set; }

    [Comment("值"), Column(TypeName = "text")]
    public string Value { get; set; } = string.Empty;

    [Comment("国家名称")]
    public string NationName { get; set; } = string.Empty;

    [Comment("国家简称")]
    public string NationShort { get; set; } = string.Empty;
}