using System.ComponentModel.DataAnnotations.Schema;
using ERP.Enums;
using ERP.Models.Product;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.Template;

[Table("sketch_template")]
[Comment("产品管理_卖点模板")]
public class SketchTemplateModel : UserRelated
{
    [Comment("卖点模板名称")]
    public string Name { get; set; } = string.Empty;

    [Comment("卖点模板内容")]
    public string Content { get; set; } = string.Empty;

    [Comment("卖点模板内容")]
    public string Language { get; set; }

    [Comment("语言简码")]
    public string LanguageSign { get; set; }

    [NotMapped]
    public Languages LanguageEnum => Enum.Parse<Languages>(LanguageSign, true);
    
    [NotMapped]
    public int LanguageId => LanguageEnum == Languages.Default ? 0 : (int)Math.Log2((long)LanguageEnum) + 1;
}


//-- ----------------------------
//-- Table structure for erp3_sketch_template
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_sketch_template`;
//CREATE TABLE `erp3_sketch_template`  (
//  `id` bigint unsigned NOT NULL,
//  `d_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '卖点模板名称',
//  `d_content` json NOT NULL COMMENT '卖点模板内容',
//  `i_oem_id` bigint(0) NOT NULL COMMENT 'oem_id',
//  `i_user_id` bigint(0) NOT NULL COMMENT '用户id',
//  `i_group_id` bigint(0) NOT NULL DEFAULT 0 COMMENT '分组id',
//  `i_company_id` bigint(0) NOT NULL DEFAULT 0 COMMENT '公司id',
//  `i_language_sign` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default' COMMENT '语言简码',
//  `u_language` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '默认' COMMENT '语言简码',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `language_sign`(`i_language_sign`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 12030 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '产品管理_卖点模板' ROW_FORMAT = Dynamic;