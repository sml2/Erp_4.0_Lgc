using System.ComponentModel.DataAnnotations.Schema;
using ERP.Enums;
using ERP.Extensions;
using ERP.Models.Product;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.Template;

[Table("keyword_template")]
[Comment("产品管理_关键词")]
public class KeywordTemplateModel : UserRelated
{
    public KeywordTemplateModel()
    {
    }
    
    public KeywordTemplateModel(ISession session)
    {
        UserID = session.GetUserID();
        GroupID = session.GetGroupID();
        CompanyID = session.GetCompanyID();
        OEMID = session.GetOEMID();
    }

    public KeywordTemplateModel(ISession session, string name, string value, string language)
    {
        UserID = session.GetUserID();
        GroupID = session.GetGroupID();
        CompanyID = session.GetCompanyID();
        OEMID = session.GetOEMID();
        Name = name;
        Value = value;
        LanguageSign = language;
        Name = name;
        Language = LanguageSign == Languages.Default.ToString().ToLower()
            ? "默认"
            : Enum.Parse<Languages>(language).GetDescriptionByKey("Name")!;
    }

    [Comment("关键词模板名称")]
    public string Name { get; set; } = string.Empty;

    [Comment("关键词")]
    public string Value { get; set; } = string.Empty;

    [Comment("使用计数")]
    public int Count { get; set; }

    [Comment("语言简码")]
    public string LanguageSign { get; set; }

    [Comment("语言简码")]
    public string Language { get; set; } = string.Empty;

    [NotMapped]
    public Languages LanguageEnum => Enum.Parse<Languages>(LanguageSign, true);

    [NotMapped]
    public int LanguageId => LanguageEnum == Languages.Default ? 0 : (int)Math.Log2((long)LanguageEnum) + 1;
}


//-- ----------------------------
//-- Table structure for erp3_keyword_template
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_keyword_template`;
//CREATE TABLE `erp3_keyword_template`  (
//  `id` bigint unsigned NOT NULL,
//  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '关键词模板名称',
//  `d_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '关键词',
//  `i_oem_id` bigint(0) NOT NULL COMMENT 'oem_id',
//  `i_user_id` bigint(0) NOT NULL,
//  `i_group_id` bigint unsigned NOT NULL COMMENT '分组id',
//  `i_company_id` bigint unsigned NOT NULL COMMENT '公司id',
//  `c_count` bigint(0) NOT NULL DEFAULT 0 COMMENT '使用计数',
//  `i_language_sign` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default' COMMENT '语言简码',
//  `u_language` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '默认' COMMENT '语言简码',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `language_sign`(`i_language_sign`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 12934 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '产品管理_关键词' ROW_FORMAT = Dynamic;