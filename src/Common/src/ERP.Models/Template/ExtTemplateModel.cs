using System.ComponentModel.DataAnnotations.Schema;
using ERP.Extensions;
using ERP.Models.Product;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace ERP.Models.Template;

[Table("ext_template")]
[Comment("产品管理_属性模板")]
public class ExtTemplateModel : UserRelated
{
    // public ExtTemplateModel(ExpandEditDto req, ISession session)
    // {
    //     Name = req.Name;
    //     Content = JsonConvert.SerializeObject(req.Content);
    //     UserID = session.GetUserID();
    //     GroupID = session.GetGroupID();
    //     CompanyID = session.GetCompanyID();
    //     OEMID = session.GetOEMID();
    // }
    


    public ExtTemplateModel()
    {
    }

    [Comment("属性模板名称")]
    public string Name { get; set; } = string.Empty;

    [Comment("属性模板内容")]
    public string Content { get; set; } = string.Empty;
}


//-- ----------------------------
//-- Table structure for erp3_ext_template
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_ext_template`;
//CREATE TABLE `erp3_ext_template`  (
//  `id` bigint unsigned NOT NULL,
//  `d_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '属性模板名称',
//  `d_content` json NOT NULL COMMENT '属性模板内容',
//  `i_oem_id` bigint(0) NOT NULL COMMENT 'oem_id',
//  `i_user_id` bigint(0) NOT NULL COMMENT '用户id',
//  `i_group_id` bigint(0) NOT NULL DEFAULT 0 COMMENT '分组id',
//  `i_company_id` bigint(0) NOT NULL DEFAULT 0 COMMENT '公司id',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 803 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '产品管理_属性模板' ROW_FORMAT = Dynamic;