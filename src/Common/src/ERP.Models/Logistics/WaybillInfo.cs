namespace ERP.Models.Logistics;

public partial class ProductInfoItem
{
    public GoodInfo GoodInfos { get; set; } = new();
    public string Tag { get; set; } = "[]";
    public int ProductId { get; set; }//商品id
    public int VariantId { get; set; }//变体ID
    public int WarehouseId { get; set; }//仓库id
    public int stockOutNum { get; set; }
    public string warehouseName { get; set; } = string.Empty;//仓库名称
    public int StockProductId { get; set; } 

    //[{"num": 1, "pid": 0, "url": "https://m.media-amazon.com/images/I/41JL1E7-8PL._SL75_.jpg", "asin": "B0859S8HZK", 
    //    "name": "Miwaimao Free Hand Washing Flat Mop Lazy 360 Rotating Magic Mop With Squeezing Strong Water Absorption Floor Cleaner Household Cleaning,2pcs rag green", 
    //    "unit": "USD", "price": 104.72, "total": 213.29, "good_id": "B0859S8HZK", "sendnum": 1, "delivery": 0, "from_url": "https://www.amazon.com/dp/B0859S8HZK",
    //    "send_num": 1, "seller_sku": "PzQtE74d-17382396853335136028-6", "amazon_shipping_money": 108.58}]

}

public class GoodInfo
{
    public string goodId { get; set; } = string.Empty;
    public string Name { get; set; } = string.Empty;
    public int Num { get; set; }
    public string? Asin { get; set; }
    /// <summary>
    /// 单位
    /// </summary>
    public string Unit { get; set; }=string.Empty;
    /// <summary>
    /// 单价
    /// </summary>
    public double Price { get; set; }
    public double Total { get; set; }
    /// <summary>
    /// 已发货数量
    /// </summary>
    public int Delivery { get; set; }
    /// <summary>
    /// 待发货数量
    /// </summary>
    public int SendNum { get; set; }
    public string? Url { get; set; }
    public string? Sku { get; set; }
}