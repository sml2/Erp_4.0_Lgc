//using ERP.Enums;
//using ERP.Models.Abstract;
//using Microsoft.EntityFrameworkCore;
//using System.ComponentModel.DataAnnotations.Schema;
//using static ERP.Models.Orders.Order;

//namespace ERP.Models.Logistics
//{
//    public class ShipOrder : UserModel
//    {
//        /// <summary>
//        /// 申报总价
//        /// </summary>

//        [Comment("申报总价")]
//        public Money Declare { get; set; }

//        /// <summary>
//        /// 申报货币单位
//        /// </summary>

//        [Comment("申报货币单位")]
//        public string DeclareUnit { get; set; } = string.Empty;

//        /// <summary>
//        /// 中文申报名称
//        /// </summary>

//        [Comment("中文申报名称")]
//        public string? Declare_zh { get; set; }

//        /// <summary>
//        /// 英文申报名称
//        /// </summary>

//        [Comment("英文申报名称")]
//        public string? Declare_en { get; set; }

//        /// <summary>
//        /// 海关编码
//        /// </summary>

//        [Comment("海关编码")]
//        public string? CustomsCode { get; set; }

//        /// <summary>
//        /// 商品描述
//        /// </summary>

//        [Comment("商品描述")]
//        public string? Description { get; set; }

//        /// <summary>
//        /// 自定义标签
//        /// </summary>

//        [Comment("自定义标签")]
//        public string? Tag { get; set; }

//        /// <summary>
//        /// 存在电池
//        /// </summary>
        
//        //ERP3.0 存在电池1否2是  old：d_battery` tinyint(0) 
//        [Comment("存在电池'")]
//        public bool Battery { get; set; }

//        /// <summary>
//        /// 长
//        /// </summary>

//        [Comment("长(cm)")]
//        public int Length { get; set; }

//        /// <summary>
//        /// 宽
//        /// </summary>

//        [Comment("宽(cm)")]
//        public int Width { get; set; }

//        /// <summary>
//        /// 高
//        /// </summary>

//        [Comment("高(cm)")]
//        public int Height { get; set; }

//        /// <summary>
//        /// 重量
//        /// </summary>

//        [Comment("重量(g)")]
//        public int Weight { get; set; }

//        /// <summary>
//        /// 订单商品
//        /// </summary>

//        [Comment("订单商品")]
//        //[Column(TypeName = "json")]
//        public string? Goods { get; set; }

//        /// <summary>
//        /// 订单编号
//        /// </summary>

//        //  U
//        [Comment("订单编号")]
//        public string? OrderNo { get; set; }

//        /// <summary>
//        /// 订单收件人国家简码
//        /// </summary>

//        [Comment("订单收件人国家简码")]
//        public string? NationShort { get; set; }

//        /// <summary>
//        /// 订单收件人国家
//        /// </summary>

//        [Comment("订单收件人国家")]
//        public string? Nation { get; set; }

//        /// <summary>
//        /// 收件人地址
//        /// </summary>

//        [Comment("收件人地址")]
//        [Column(TypeName = "json")]
//        public string? Address { get; set; }

//        /// <summary>
//        /// 订单ID
//        /// </summary>
//        [Comment("订单ID")]
//        public int OrderId { get; set; }
//        /// <summary>
//        /// 绑定发货模板ID
//        /// </summary>
//        [Comment("绑定发货模版id")]
//        public int ShipTemplateId { get; set; }
//        /// <summary>
//        /// 所属平台ID
//        /// </summary>
//        [Comment("所属平台Id")]
//        public Platforms PlatformId { get; set; }
//    }
//}



////------------------------------
////--Table structure for erp3_ship_order
////------------------------------
////DROP TABLE IF EXISTS `erp3_ship_order`;
////CREATE TABLE `erp3_ship_order`  (
////  `id` bigint unsigned NOT NULL,
////  `d_declare` decimal(22, 6) NOT NULL DEFAULT 0.000000 COMMENT '申报总价',
////  `d_declare_unit` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '申报货币单位',
////  `d_declare_zh` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '中文申报名称',
////  `d_declare_en` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '英文申报名称',
////  `d_customs_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '海关编码',
////  `d_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '商品描述',
////  `d_tag` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '自定义标签',
////  `d_battery` tinyint(0) NOT NULL DEFAULT 1 COMMENT '存在电池1否2是',
////  `d_length` int(0) NOT NULL DEFAULT 0 COMMENT '长(cm)',
////  `d_width` int(0) NOT NULL DEFAULT 0 COMMENT '宽(cm)',
////  `d_height` int(0) NOT NULL DEFAULT 0 COMMENT '高(cm)',
////  `d_weight` int(0) NOT NULL DEFAULT 0 COMMENT '重量(g)',
////  `d_goods` json NOT NULL COMMENT '订单商品',
////  `i_order_id` int(0) NOT NULL DEFAULT 0 COMMENT '订单id',
////  `u_order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '订单编号',
////  `u_nation_short` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '订单收件人国家简码',
////  `u_nation` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '订单收件人国家',
////  `u_address` json NOT NULL COMMENT '收件人地址',
////  `i_ship_template_id` int(0) NOT NULL DEFAULT 0 COMMENT '绑定发货模版id',
////  `i_platform_id` int unsigned NOT NULL COMMENT '所属平台id',
////  `i_user_id` int(0) NOT NULL DEFAULT 0 COMMENT '用户id',
////  `i_group_id` int(0) NOT NULL DEFAULT 0 COMMENT '创建用户组id',
////  `i_company_id` int(0) NOT NULL DEFAULT 0 COMMENT '所属公司id',
////  `i_oem_id` int(0) NOT NULL DEFAULT 0 COMMENT '当前Oem id',
////  `created_at` timestamp(0) NULL DEFAULT NULL,
////  `updated_at` timestamp(0) NULL DEFAULT NULL,
//// PRIMARY KEY(`id`) USING BTREE,
//// INDEX `company_id`(`i_company_id`) USING BTREE,
//// INDEX `group_id`(`i_group_id`) USING BTREE,
//// INDEX `oem_id`(`i_oem_id`) USING BTREE,
//// INDEX `order_id`(`i_order_id`) USING BTREE,
//// INDEX `platform_id`(`i_platform_id`) USING BTREE,
//// INDEX `user_id`(`i_user_id`) USING BTREE
////) ENGINE = InnoDB AUTO_INCREMENT = 16425 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '订单管理_待发货订单' ROW_FORMAT = Dynamic;
