using ERP.Models.Abstract;
using ERP.Models.Setting;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.Models.Logistics
{
    /// <summary>
    /// 运费估算模板表
    /// </summary>
    public class ShipRatesEstimationTemplate:UserModel 
    {
        [Comment("名称")]
        public string Name {  get; set; } = string.Empty;
        //* json
        //     * weight_start 起始重量
        //     * weight_end 结束重量
        //     * min_weight 首重
        //     * min_cost 首重费用
        //     * continued_weight 续重单位重量
        //     * continued_cost   续重单位费用
        //     * registration_cost 挂号费
        [Comment("数据")]
        //[Column(TypeName = "json")]
        public string Data { get; set; } = string.Empty;
        [Comment("排序")]
        public int Order {  get; set; }
        [Comment("模式")]
        public int Mode {  get; set; }
        private List<T>? GetData<T>()
        {
            return JsonConvert.DeserializeObject<List<T>>(Data);
        }
        internal List<Datas>? GetTemplateData() => GetData<Datas>();
        //internal void AddData(string Short, TempData tempData)
        //{
        //    List <Datas>? datas = GetTemplateData();
        //    Datas data = new();
        //    data.Short = Short;
        //    data.data = tempData;
        //    datas?.Add(data);
        //    //data.Short = Short;
        //    //data.data = tempData;
        //}
    }

    public class Datas
    {
        public string Short { get; set; }=string.Empty;
        public TempData data { get; set; } = default!;
        
    }
    public class TempData
    {
        public decimal continuedUnitCost { get; set; }
        public decimal continuedUnitWeight { get; set; }
        public decimal endWeight { get; set; }
        public decimal minCost { get; set; }
        public decimal minWeight { get; set; }
        public decimal registrationCost { get; set; }
        public decimal startWeight { get; set; }
    }
}


