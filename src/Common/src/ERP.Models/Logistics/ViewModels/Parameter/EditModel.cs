using ERP.Enums;
using System.ComponentModel.DataAnnotations;

namespace ERP.Models.Logistics.ViewModels.Parameter
{
    public class EditModel 
    {
        [Required]
        public string CustomizeName { get; set; } = string.Empty;
        // TODO: 命名规范
        public Platforms LogisticsID { get; set; }
        public string? Remark { get; set; }
        [Required]
        public object Data { get; set; } = new();
    }
}
