using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;

namespace ERP.Models.Stores;

[Index(nameof(TasksId))]
public class MwsTasksSession
{
    /// <summary>
    /// 任务id
    /// </summary>

    [Comment("任务id")]
    public int TasksId { get; set; }

    /// <summary>
    /// 任务更新状态
    /// </summary>

    [Comment("任务更新状态")]
    public int State { get; set; }
}

//-- ----------------------------
//-- Table structure for erp3_mws_tasks_session
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_mws_tasks_session`;
//CREATE TABLE `erp3_mws_tasks_session`  (
//  `id` bigint unsigned NOT NULL,
//  `i_tasks_id` int unsigned NOT NULL COMMENT '任务id',
//  `i_user_id` int unsigned NOT NULL COMMENT '用户id',
//  `i_group_id` int unsigned NOT NULL COMMENT '当前用户组id',
//  `i_company_id` int unsigned NOT NULL COMMENT '店铺公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `d_index` tinyint unsigned NOT NULL COMMENT '任务更新字段',
//  `d_state` tinyint unsigned NOT NULL COMMENT '任务更新状态',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `tasks_id`(`i_tasks_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 16083 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '监听Session日志' ROW_FORMAT = Dynamic;