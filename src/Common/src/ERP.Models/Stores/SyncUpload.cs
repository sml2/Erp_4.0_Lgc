using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;

namespace ERP.Models.Stores;

[Index(nameof(StoreId))]
public class SyncUpload : UserModel
{
    /// <summary>
    /// 任务数量
    /// </summary>

    [Comment("任务数量")]
    public int StoreId { get; set; }

    /// <summary>
    /// 任务数量
    /// </summary>

    [Comment("任务数量")]
    public int Count { get; set; }

    /// <summary>
    /// 正在操作者id
    /// </summary>

    [Comment("正在操作者id")]
    public int OperateId { get; set; }

    public enum States
    {
        [Description("进行中")]
        FALSE = 1,

        [Description("已完成")]
        TRUE = 2
    }

    /// <summary>
    /// 状态 1 进行中 2 已完成
    /// </summary>

    [Comment("状态 1 进行中 2 已完成")]
    public States State { get; set; }

    /// <summary>
    /// 报错信息
    /// </summary>

    [Comment("报错信息")]
    public int ErrorInfo { get; set; }

    /// <summary>
    /// Index归档
    /// </summary>

    [Comment("Index归档")]
    public int ErrorIndex { get; set; }

    /// <summary>
    /// 产品session_id
    /// </summary>

    [Comment("产品session_id")]
    public int ProductId { get; set; }

    /// <summary>
    /// 变体session_id
    /// </summary>

    [Comment("变体session_id")]
    public int VariantId { get; set; }

    /// <summary>
    /// 价格session_id
    /// </summary>

    [Comment("价格session_id")]
    public int PricingId { get; set; }

    /// <summary>
    /// 仓库session_id
    /// </summary>

    [Comment("仓库session_id")]
    public int InventoryId { get; set; }

    /// <summary>
    /// 图片session_id
    /// </summary>

    [Comment("图片session_id")]
    public int ImageId { get; set; }

    /// <summary>
    /// 关系session_id
    /// </summary>

    [Comment("关系session_id")]
    public int RelationshipId { get; set; }

    /// <summary>
    /// 产品上传状态
    /// </summary>

    [Comment("产品上传状态")]
    public int ProductState { get; set; }

    /// <summary>
    /// 变体上传状态
    /// </summary>

    [Comment("变体上传状态")]
    public int VariantState { get; set; }

    /// <summary>
    /// 价格上传状态
    /// </summary>

    [Comment("价格上传状态")]
    public int PricingState { get; set; }

    /// <summary>
    /// 库存上传状态
    /// </summary>

    [Comment("库存上传状态")]
    public int InventoryState { get; set; }

    /// <summary>
    /// 图片上传状态
    /// </summary>

    [Comment("图片上传状态")]
    public int ImageState { get; set; }

    /// <summary>
    /// 关系上传
    /// </summary>

    [Comment("关系上传")]
    public int RelationshipState { get; set; }
}

//-- ----------------------------
//-- Table structure for erp3_sync_upload
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_sync_upload`;
//CREATE TABLE `erp3_sync_upload`  (
//  `id` bigint unsigned NOT NULL,
//  `i_store_id` int unsigned NOT NULL COMMENT '店铺id',
//  `c_count` int unsigned NOT NULL COMMENT '任务数量',
//  `i_operate_id` int unsigned NULL COMMENT '正在操作者id',
//  `i_user_id` int unsigned NOT NULL COMMENT '用户id',
//  `i_group_id` int unsigned NOT NULL COMMENT '当前用户组id',
//  `i_company_id` int unsigned NOT NULL COMMENT '公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `t_state` int unsigned NOT NULL COMMENT '状态 1 进行中 2 已完成',
//  `d_error_info` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '报错信息',
//  `d_error_index` int (0) NULL DEFAULT NULL COMMENT 'Index归档',
//  `d_product_id` bigint unsigned NULL COMMENT '产品session_id',
//  `d_variant_id` bigint unsigned NULL COMMENT '变体session_id',
//  `d_pricing_id` bigint unsigned NULL COMMENT '价格session_id',
//  `d_inventory_id` bigint unsigned NULL COMMENT '仓库session_id',
//  `d_image_id` bigint unsigned NULL COMMENT '图片session_id',
//  `d_relationship_id` bigint unsigned NULL COMMENT '关系session_id',
//  `t_product_state` int unsigned NOT NULL COMMENT '产品上传状态',
//  `t_variant_state` int unsigned NOT NULL COMMENT '变体上传状态',
//  `t_pricing_state` int unsigned NOT NULL COMMENT '价格上传状态',
//  `t_inventory_state` int unsigned NOT NULL COMMENT '库存上传状态',
//  `t_image_state` int unsigned NOT NULL COMMENT '图片上传状态',
//  `t_relationship_state` int unsigned NOT NULL COMMENT '关系上传',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `store_id`(`i_store_id`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 110046 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '上传同步操作表' ROW_FORMAT = Dynamic;
