using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;

namespace ERP.Models.Stores;

public class MwsTasks : UserModel
{
    /// <summary>
    /// 店铺id
    /// </summary>

    [Comment("店铺id")]
    public int Sid { get; set; }

    /// <summary>
    /// 任务数量
    /// </summary>

    [Comment("任务数量")]
    public int Count { get; set; }

    /// <summary>
    /// 正在操作者id
    /// </summary>

    [Comment("正在操作者id")]
    public int OperateId { get; set; }

    /// <summary>
    /// 产品
    /// </summary>

    [Comment("产品")]
    public string? SessionProduct { get; set; }

    /// <summary>
    /// 变型
    /// </summary>

    [Comment("变型")]
    public string? SessionVariant { get; set; }

    /// <summary>
    /// 定价
    /// </summary>

    [Comment("定价")]
    public int SessionPricing { get; set; }

    /// <summary>
    /// 库存
    /// </summary>

    [Comment("库存")]
    public string? SessionInventory { get; set; }

    /// <summary>
    /// 图像
    /// </summary>

    [Comment("图像")]
    public string? SessionImage { get; set; }

    /// <summary>
    /// 关系
    /// </summary>

    [Comment("关系")]
    public string? SessionRelationship { get; set; }

    public enum States
    {
        [Description("进行中")]
        DOING = 1,

        [Description("已完成")]
        DONE = 2
    }

    /// <summary>
    /// 状态 1 进行中 2 已完成
    /// </summary>

    [Comment("状态 1 进行中 2 已完成")]
    public States state { get; set; }
}

//-- ----------------------------
//-- Table structure for erp3_mws_tasks
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_mws_tasks`;
//CREATE TABLE `erp3_mws_tasks`  (
//  `id` bigint unsigned NOT NULL,
//  `i_sid` int unsigned NOT NULL COMMENT '店铺id',
//  `c_count` int unsigned NOT NULL COMMENT '任务数量',
//  `i_operate_id` int unsigned NULL COMMENT '正在操作者id',
//  `i_user_id` int unsigned NOT NULL COMMENT '用户id',
//  `i_group_id` int unsigned NOT NULL COMMENT '当前用户组id',
//  `i_company_id` int unsigned NOT NULL COMMENT '公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `d_session_product` json NULL COMMENT '产品',
//  `d_session_variant` json NULL COMMENT '变型',
//  `d_session_pricing` json NULL COMMENT '定价',
//  `d_session_inventory` json NULL COMMENT '库存',
//  `d_session_image` json NULL COMMENT '图像',
//  `d_session_relationship` json NULL COMMENT '关系',
//  `t_state` int unsigned NOT NULL COMMENT '状态 1 进行中 2 已完成',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `sid`(`i_sid`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 2970 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = 'Mws_商品上传亚马逊相同店铺打包任务表' ROW_FORMAT = Dynamic;
