namespace ERP.Models.Stores;
using Interface;


public partial class StoreRegion : IStatistic
{
    public DateTime LastCreateStatistic { get; set; }
}

public partial class StoreMarket : IStatistic
{
    public DateTime LastCreateStatistic { get; set; }
}
