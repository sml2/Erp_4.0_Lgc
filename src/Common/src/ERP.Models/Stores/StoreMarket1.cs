namespace ERP.Models.Stores;

using Models.DB.Identity;
using Extensions;
using Interface;
using Models.Abstract;
using Models.DB.Users;
using Models.Setting;
using EID = Enums.ID;
using Microsoft.AspNetCore.Http;

public partial class StoreMarket : UserModel
{
#pragma warning disable CS8618 // 在退出构造函数时，不可为 null 的字段必须包含非 null 值。请考虑声明为可以为 null。
    public StoreMarket() : base() { }
    public StoreMarket(User? user, Group? userGroup, Company? company, OEM? oem) : base(user, userGroup, company, oem) { }

    public StoreMarket(int UID, int groupID, int companyID, int oemID) : base(UID, groupID, companyID, oemID) { }

    public StoreMarket(ISessionProvider sessionProvider) : base(sessionProvider) { }
    public StoreMarket(ISession session) : base(session) { }
#pragma warning restore CS8618 // 在退出构造函数时，不可为 null 的字段必须包含非 null 值。请考虑声明为可以为 null。
    /// <summary>
    /// Seed
    /// </summary>
    /// <param name="id"></param>
    /// <param name="u"></param>
    /// <param name="g"></param>
    /// <param name="c"></param>
    /// <param name="o"></param>
    /// <param name="modes"></param>
    /// <param name="continent"></param>
    /// <param name="data"></param>
    /// <param name="flag"></param>
    /// <param name="hash"></param>
    /// <param name="name"></param>
    /// <param name="nid"></param>
    /// <param name="n"></param>
    /// <param name="ns"></param>
    /// <param name="platform"></param>
    /// <param name="unitid"></param>
    /// <param name="unitname"></param>
    /// <param name="unitsign"></param>
    public StoreMarket(int id, Enums.Identity.User u, EID.Group g, EID.Company c, EID.OEM o,
        int nid, string n, string ns,
        int unitid, string unitname, string unitsign) : this(u.GetID(), g.GetID(), c.GetID(), o.GetID())
    {
        ID = id;
        State = StateEnum.Open;
        NationId = nid;
        NationShort = ns;
        NationName = n;
        UnitID = unitid;
        UnitName = unitname;
        UnitSign = unitsign;
    }
}
