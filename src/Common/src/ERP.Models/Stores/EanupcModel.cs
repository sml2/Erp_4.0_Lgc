using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;

namespace ERP.Models.Stores;

[Index(nameof(Value), IsUnique = true)]
public class EanupcModel : UserModel
{
    public enum TypeEnum
    {
        [Description("ean")]
        EAN = 1,

        [Description("upc")]
        UPC = 2
    }

    /// <summary>
    /// 类型1ean2upc
    /// </summary>

    [Comment("类型1ean2upc")]
    public TypeEnum Type { get; set; }

    /// <summary>
    /// 码值
    /// </summary>
    [Comment("码值")]
    public string Value { get; set; } = string.Empty;

    public int HashCode { get; set; }

    public enum SelectModeEnum
    {
        //模糊查询
        Vague = 1,
        //精确查询
        Accurate
    }

    public enum RangeEnum
    {
        Group = 1,
        User,
        Company
    }
}

