using Microsoft.EntityFrameworkCore;
using ERP.Models.Setting;
using HotChocolate;

namespace ERP.Models.Stores;

[Index(nameof(NationId)), Index(nameof(State))]

public partial class StoreMarket
{
    /// <summary>
    /// StoreRegion中的UniqueHashCode，作为market的主键
    /// </summary>
    [Comment("UniqueHashCode"), GraphQLIgnore]
    public ulong UniqueHashCode { get; set; }

    public int? platFormId { get; set; }

    /// <summary>
    /// 国家id
    /// </summary>
    [Comment("id")]
    public int? NationId { get; set; }
    public Nation? Nation { get; set; }
    /// <summary>
    /// 国家简码
    /// </summary>
    [Comment("国家简码")]
    public string NationShort { get; set; } = string.Empty;

    /// <summary>
    /// 国家名称
    /// </summary>
    [Comment("国家名称")]
    public string NationName { get; set; } = string.Empty;

    /// <summary>
    /// 状态1启用2禁用
    /// </summary>
    [Comment("状态1启用2禁用")]
    public StateEnum State { get; set; } = StateEnum.Open;

    /// <summary>
    /// 货币
    /// </summary>
    public int? UnitID { get; set; }

    /// <summary>
    /// 货币
    /// </summary>
    public Unit? Unit { get; set; }

    /// <summary>
    /// 货币名称
    /// </summary>
    [Comment("货币名称")]
    public string? UnitName { get; set; }

    /// <summary>
    /// 货币名称
    /// </summary>
    [Comment("货币符号")]
    public string? UnitSign { get; set; }

    [Comment("市场")]
    public string Marketplace { get; set; }
}

