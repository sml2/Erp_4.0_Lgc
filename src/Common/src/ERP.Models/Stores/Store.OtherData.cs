using ERP.Interface;
using System.Globalization;

namespace ERP.Models.Stores;

public partial class StoreRegion
{

    public class OtherData
    {
        public string Note { get; set; } = string.Empty;
    }
}
