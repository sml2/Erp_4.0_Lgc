using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.Stores
{

    [Index(nameof(Asin))]
    public class AsinProduct : BaseModel
    {
        public AsinProduct()
        {

        }
        public AsinProduct(string sellerid, string sku, string asin, int storeId, bool isMainSku, bool isActive = true)
        {
            SellerID = sellerid;
            Sku = sku;
            Asin = asin;
            StoreId = storeId;
            PidHashCode = GetPidHashCode(sellerid, sku);
            IsMainSku = isMainSku;
            IsActive = isActive;
        }

        /// <summary>
        /// 拉取订单时，顺带拉取的产品属性，不修改syncTime
        /// </summary>
        /// <param name="sellerid"></param>
        /// <param name="sku"></param>
        /// <param name="asin"></param>
        /// <param name="storeId"></param>
        /// <param name="brand"></param>
        /// <param name="size"></param>
        /// <param name="color"></param>
        /// <param name="imageUrl"></param>
        /// <param name="isActive"></param>
        public AsinProduct(string sellerid, string sku, string asin, int storeId, string brand, string size, string color, string imageUrl,
            bool isActive = true)
        {
            SellerID = sellerid;
            Sku = sku;
            Asin = asin;
            StoreId = storeId;
            PidHashCode = GetPidHashCode(sellerid, sku);
            Brand = brand;
            Size = size;
            ImageUrl = imageUrl;
            Color = color;
            IsActive = isActive;
        }

        /// <summary>
        /// 自动拉取产品，用于修改产品属性
        /// </summary>
        /// <param name="pidHashCode"></param>
        /// <param name="brand"></param>
        /// <param name="size"></param>
        /// <param name="color"></param>
        /// <param name="imageUrl"></param>
        public AsinProduct(ulong pidHashCode, int storeID, string brand, string size, string color, string imageUrl, string languageTag)
        {
            PidHashCode = pidHashCode;
            StoreId = storeID;
            Brand = brand;
            Size = size;
            ImageUrl = imageUrl;
            Color = color;
            LanguageTag = languageTag;
            SyncTime = DateTime.Now;
        }

        public string Asin { get; set; }

        public string Sku { get; set; }

        public string SellerID { get; set; }

        /// <summary>
        /// SellerID+sku
        /// </summary>
        public ulong PidHashCode { get; set; }

        public int StoreId { get; set; }

        public bool IsActive { get; set; } = true;

        public DateTime? SyncTime { get; set; }

        /// <summary>
        /// 属性json，key-value结构
        /// </summary>
        //  public string AttributeJson { get; set; }

        public string? Brand { get; set; }

        public string? Color { get; set; }

        public string? Size { get; set; }

        public string? ImageUrl { get; set; }

        public string? LanguageTag { get; set; }

        public bool IsMainSku { get; set; } = false;
        public bool Pull { get; set; } = false;

        public ulong GetPidHashCode(string sellerID, string sku)
        {
            if (!sellerID.IsNullOrWhiteSpace() && !sku.IsNullOrWhiteSpace())
            {
                return Helpers.CreateHashCode(sellerID + sku);
            }
            else
            {
                Console.WriteLine($"sellerID:{sellerID},sku:{sku}  get error");
                return 0;
            }
        }
    }
}
