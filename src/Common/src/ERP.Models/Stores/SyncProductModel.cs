using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ERP.Enums.Rule.Config.Internal;
using ERP.Extensions;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using ERP.Data.Products;

namespace ERP.Models.Stores;

[Index(nameof(CategoryId), nameof(Pid), nameof(SourceId), nameof(SubCategoryId))]
public class SyncProductModel : UserModel
{
    public SyncProductModel()
    {
    }

    public SyncProductModel(ISession session)
    {
        UserID = session.GetUserID();
        GroupID = session.GetGroupID();
        CompanyID = session.GetCompanyID();
        OEMID = session.GetOEMID();
    }

    /// <summary>
    /// [镜像]产品id
    /// </summary>

    [Comment("[镜像]产品id")]
    public int Pid { get; set; }

    /// <summary>
    /// [原始]产品id
    /// </summary>

    [Comment("[原始]产品id")]
    public int SourceId { get; set; }

    [Comment("[原始]产品ItemId")]
    public int SourceItemId { get; set; }

    /// <summary>
    /// 导出产品名称
    /// </summary>

    [Comment("导出产品名称")]
    public string? ProductName { get; set; }

    /// <summary>
    /// 产品图片
    /// </summary>

    [Comment("产品图片")]
    public string? ImgUrl { get; set; }

    [NotMapped]
    public Image? Image => ImgUrl is not null ? JsonConvert.DeserializeObject<Image>(ImgUrl) : null;

    /// <summary>
    /// 产品SKU
    /// </summary>

    [Comment("产品SKU")]
    public string Sku { get; set; } = string.Empty;

    public enum SkuTypes
    {
        [Description("有前缀")] TYPE_TRUE = 1,

        [Description("无前缀")] FALSE = 2
    }

    /// <summary>
    /// 1 有前缀（有前缀可以关联产品） 2 无前缀(无法关联产品)
    /// </summary>

    [Comment("1 有前缀（有前缀可以关联产品） 2 无前缀(无法关联产品)")]
    public SkuTypes SkuType { get; set; }

    public enum SkuSuffixs
    {
        [Description("SKU追加属性信息（更改产品变体属性名会更改上传sku）")]
        TRUE = 1,

        [Description("不追加属性信息")] FALSE = 2
    }

    /// <summary>
    /// 1 SKU追加属性信息（更改产品变体属性名会更改上传sku） 2 不追加属性信息
    /// </summary>

    [Comment("1 SKU追加属性信息（更改产品变体属性名会更改上传sku） 2 不追加属性信息")]
    public SkuSuffixs SkuSuffix { get; set; }

    public enum TitleSuffixs
    {
        [Description("产品标题追加属性信息")] TRUE = 1,

        [Description("不追加属性信息")] FALSE = 2
    }

    /// <summary>
    /// 1 产品标题追加属性信息 2 不追加属性信息
    /// </summary>

    [Description("1 产品标题追加属性信息 2 不追加属性信息")]
    public TitleSuffixs TitleSuffix { get; set; }

    public enum ProductTypes
    {
        [Description("上传")] Upload = 1,

        [Description("导出")] Export = 2,

        [Description("上传导出")] UploadAndExport = 3
    }

    /// <summary>
    /// 使用类型1上传，2导出，3上传导出
    /// </summary>

    [Comment("使用类型1上传，2导出，3上传导出")]
    public ProductTypes ProductType { get; set; }

    /// <summary>
    /// 分类id
    /// </summary>

    [Comment("分类id")]
    public int? CategoryId { get; set; }

    /// <summary>
    /// 子分类id
    /// </summary>

    [Comment("子分类id")]
    public int? SubCategoryId { get; set; }

    /// <summary>
    /// 变体是否能修改
    /// </summary>

    [Comment("变体是否能修改")]
    public bool AlreadyUpload { get; set; }

    /// <summary>
    /// 产品是否能修改
    /// </summary>

    [Comment("产品是否能修改")]
    public bool Uploading { get; set; }

    /// <summary>
    /// 分类
    /// </summary>

    [Comment("分类")]
    public string? TypeData { get; set; }

    /// <summary>
    /// 分类id
    /// </summary>

    [Comment("分类id")]
    public string? TypeDataId { get; set; }

    public enum EanupcStatuss
    {
        [Description("没")] NOTHING = 0,

        [Description("部分")] PORTION = 1,

        [Description("全部")] ALL = 2
    }

    /// <summary>
    /// EANupc分配情况 0(没),1(部分),2(全部)
    /// </summary>

    [Comment("EANupc分配情况 0(没),1(部分),2(全部)")]
    public EanupcStatuss EanupcStatus { get; set; }
}

public static class SyncProductModelDbSetExtension
{
    public static IQueryable<SyncProductModel> WhereRange(this IQueryable<SyncProductModel> query, DataRange_2b? range, int companyId, int groupId, int userId) =>
        range switch
        {
            DataRange_2b.Company => query.Where(m => m.CompanyID == companyId),
            DataRange_2b.Group => query.Where(m => m.CompanyID == companyId && m.GroupID == groupId),
            _ => query.Where(m => m.CompanyID == companyId && m.UserID == userId),
        };
}

//-- ----------------------------
//-- Table structure for erp3_sync_product
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_sync_product`;
//CREATE TABLE `erp3_sync_product`  (
//  `id` bigint unsigned NOT NULL,
//  `i_pid` int unsigned NOT NULL COMMENT '[镜像]产品id',
//  `i_source_id` int unsigned NOT NULL COMMENT '[原始]产品id',
//  `u_product_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '导出产品名称',
//  `u_img_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '产品图片',
//  `d_sku` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '产品SKU',
//  `t_sku_type` tinyint unsigned NOT NULL COMMENT '1 有前缀（有前缀可以关联产品） 2 无前缀(无法关联产品)',
//  `t_sku_suffix` tinyint unsigned NOT NULL COMMENT '1 SKU追加属性信息（更改产品变体属性名会更改上传sku） 2 不追加属性信息',
//  `t_title_suffix` tinyint unsigned NOT NULL COMMENT '1 产品标题追加属性信息 2 不追加属性信息',
//  `t_product_type` bigint unsigned NOT NULL COMMENT '使用类型1上传，2导出，3上传导出',
//  `u_category_id` bigint unsigned NOT NULL COMMENT '分类id',
//  `u_sub_category_id` bigint unsigned NOT NULL COMMENT '子分类id',
//  `t_already_upload` tinyint unsigned NOT NULL COMMENT '变体是否能修改 1 是 2 否',
//  `t_uploading` tinyint unsigned NOT NULL COMMENT '产品是否能修改    1 是 2 否',
//  `d_type_data` json NULL COMMENT '分类',
//  `d_type_data_id` json NULL COMMENT '分类id',
//  `i_user_id` int unsigned NOT NULL COMMENT '用户id',
//  `i_group_id` int unsigned NOT NULL COMMENT '当前用户组id',
//  `i_company_id` int unsigned NOT NULL COMMENT '当前公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `t_eanupc_status` tinyint unsigned NOT NULL COMMENT 'EANupc分配情况 0(没),1(部分),2(全部)',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `category_id`(`u_category_id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `pid`(`i_pid`) USING BTREE,
// INDEX `source_id`(`i_source_id`) USING BTREE,
// INDEX `sub_category_id`(`u_sub_category_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 18234963 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;