using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using ERP.Enums;
using ERP.Models.Setting;
using ERP.Models.DB.Users;
using ERP.Data;
using HotChocolate;

namespace ERP.Models.Stores;

[Index(nameof(Continent)), Index(nameof(HashCode)), Index(nameof(IsVerify)), Index(nameof(Sort)), Index(nameof(LastAllotTime)), Index(nameof(LastAutoUpdateTime)), Index(nameof(State)), Index(nameof(Platform))]

public partial class StoreRegion
{
    /// <summary>
    /// 唯一的hashcode(经济区+Seller)，作为market子表的主键
    /// </summary>
    [Comment("UniqueHashCode"), GraphQLIgnore]
    public ulong UniqueHashCode { get; set; }

    /// <summary>
    /// SellerID hashcode
    /// </summary>
    [Comment("hashcode"), GraphQLIgnore]
    public ulong HashCode { get; set; }

    /// <summary>
    /// user-store组
    /// </summary>
    [Comment("user-store组")]
    public int? BelongsGroupID { get; set; }
    public Group? BelongsGroup { get; set; }


    /// <summary>
    /// 店铺名称
    /// </summary>
    [Comment("店铺名称")]
    public string Name { get; set; } = null!;


    [Comment("添加店铺状态")]
    public AddModes AddMode { get; set; }
    public enum AddModes
    {
        None, Single, Multiple
    }


    /// <summary>
    /// 店铺验证1已认证2未认证
    /// </summary>

    [Comment("店铺验证1已认证2未认证")]
    public IsVerifyEnum IsVerify { get; set; } = IsVerifyEnum.Uncertified;

    public enum IsVerifyEnum
    {
        Certified = 1,
        Uncertified
    }

    public int TokenCount { get; set; }

    /// <summary>
    /// 是否删除
    /// </summary>
    [Comment("是否删除")]
    public bool IsDel { get; set; } = false;

    /// <summary>
    /// 状态1启用2禁用
    /// </summary>
    [Comment("状态1启用2禁用")]
    public StateEnum State { get; set; } = StateEnum.Open;


    public enum Flags
    {
        [Description("是")]
        YES = 2,

        [Description("否")]
        NO = 1
    }

    /// <summary>
    /// 逻辑删除，跟随国家禁用状态1有效2失效
    /// </summary>
    [Comment("逻辑删除，跟随国家禁用状态1有效2失效")]
    public Flags Flag { get; set; } = Flags.NO;

    /// <summary>
    /// 排序
    /// </summary>
    [Comment("排序")]
    public int Sort { get; set; }

    /// <summary>
    /// 货币
    /// </summary>
    public int? UnitID { get; set; }

    /// <summary>
    /// 货币
    /// </summary>
    public Unit? Unit { get; set; }

    [NotMapped]
    public int Creator { get; set; } = 0;

    /// <summary>
    /// 货币名称
    /// </summary>

    [Comment("货币名称")]
    public string? UnitName { get; set; }

    /// <summary>
    /// 货币名称
    /// </summary>
    [Comment("货币符号")]
    public string? UnitSign { get; set; }

    public void SetUnitData(Unit? unit)
    {
        if (unit is null)
        {
            UnitID = default;
            //Unit = default;
            UnitName = string.Empty;
            UnitSign = string.Empty;
        }
        else
        {
            UnitID = unit.ID;
            //Unit = unit;
            UnitName = unit.Name;
            UnitSign = unit.Sign;
        }
    }

    /// <summary>
    /// 所属大洲
    /// </summary>
    [Comment("所属大洲")]
    public Continents Continent { get; set; }

    /// <summary>
    /// 最后一个有效订单时间(上报最终状态即更新)
    /// </summary>

    [Comment("最后一个有效订单时间(上报最终状态即更新)")]
    public DateTime LastShipTime { get; set; }

    /// <summary>
    /// 自动拉取最后一个有效订单时间(自动拉取上报最终状态即更新)
    /// </summary>

    [Comment("自动拉取最后一个有效订单时间(自动拉取上报最终状态即更新)")]
    public DateTime LastAutoShipTime { get; set; }

    /// <summary>
    /// 最后被分配拉取订单时间(自动拉取获取店铺更新)
    /// </summary>

    [Comment("最后被分配拉取订单时间(自动拉取获取店铺更新)")]
    public DateTime LastAllotTime { get; set; }

    /// <summary>
    /// 最后更新订单状态时间
    /// </summary>

    [Comment("最后更新订单状态时间")]
    public DateTime LastAutoUpdateTime { get; set; }


    [Comment("平台id")]
    public Platforms Platform { get { return _plat; } set { _plat = value; } }


    private Platforms _plat;

    [NotMapped]
    public PlatformInfo PlatformInfo { get { return new ERP.Data.PlatformInfo(_plat); } }

    //public FixEnum Fix { get; set; }

    //public enum FixEnum
    //{
    //    //同步
    //    InOfSync = 1,
    //    //不同步
    //    OutOfSync = 2
    //}

    /// <summary>
    /// json参数
    /// </summary>
    [Comment("json参数")]
    public string Data { get; set; } = string.Empty;

    private T? GetData<T>()
    {
        try
        {
            return System.Text.Json.JsonSerializer.Deserialize<T>(Data);
        }
        catch
        {
            return default(T);
        }
    }

    [NotMapped]
    public List<int?> PlatFormIds { get; set; }

    [NotMapped]
    public List<string> MarketPlaces { get; set; }

    [NotMapped]
    public List<int?> NationIds { get; set; }

    //internal void InjectCache(Services.Caches.AmazonKey amazonCache, Services.Caches.PlatformData platformData)
    //{
    //    this.amazonCache = amazonCache;
    //    this.platformData = platformData;
    //}
    //private Services.Caches.AmazonKey amazonCache;
    //private Services.Caches.PlatformData platformData;
    //  internal AmazonData? GetAmazonData() => GetData<AmazonData>()?.AddCache(amazonCache, platformData, PlatFormIds);
    public ShopeeData? GetShopeeData() => GetData<ShopeeData>();
    public OtherData? GetOtherData() => GetData<OtherData>();
    public SPAmazonData? GetSPAmazon() => GetData<SPAmazonData>();
}

