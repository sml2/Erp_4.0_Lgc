﻿using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;
using ERP.Enums;
using ERP.Models.DB.Product;
using ERP.Models.Stores;

namespace ERP.Models.DB.Stores;

/// <summary>
/// 上传任务表, 用于批量上传聚合产品数据
/// </summary>
public class SyncUploadTask : UserModel
{
    /// <summary>
    /// 任务名称
    /// </summary>
    [MaxLength(30)]
    public string Name { get; set; } = null!;
    public int StoreId { get; set; }
    public StoreRegion Store { get; set; } = null!;
    
    /// <summary>
    /// 语言
    /// </summary>
    [Comment("语言")]
    public Languages Language { get; set; }
    
    [Comment("上传状态")]
    public UploadResult State { get; set; }
    
    [Comment("产品数量")]
    public int Amount { get; set; }
    
    /// <summary>
    /// 上传任务所属平台
    /// </summary>
    public Platforms Platform { get; set; }

    /// <summary>
    /// 区分平台任务的扩展信息, 只给前端展示用
    /// </summary>
    public string PlatformData { get; set; } = null!;

    public enum EanupcStatuses
    {
        [Description("没")]
        Nothing = 0,
        [Description("部分")]
        Portion = 1,
        [Description("全部")]
        All = 2
    }

    /// <summary>
    /// EANupc分配情况 0(没),1(部分),2(全部)
    /// </summary>
    [Comment("EANupc分配情况 0(没),1(部分),2(全部)")]
    public EanupcStatuses EanupcStatus { get; set; }

    public bool Locked { get; set; }

    public ICollection<ProductUpload> ProductUploads { get; set; } = new List<ProductUpload>();

    public void MarkFailed(string msg)
    {
        State = UploadResult.UploadFail;
    }
}

/// <summary>
/// 给前端展示区分上传任务的平台字段
/// </summary>
public class PlatformExtData
{

    #region 亚马逊相关

    /// <summary>
    /// 节点编号
    /// </summary>
    public string? BrowserNodeNames { get; set; }

    public string? MarketPlace { get; set; }

    #endregion
}