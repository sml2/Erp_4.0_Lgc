using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;

namespace ERP.Models.Stores;

[Index(nameof(UploadId))]
public class SyncAliveSession : UserModel
{
    /// <summary>
    /// 上传id
    /// </summary>

    [Comment("上传id")]
    public int UploadId { get; set; }

    /// <summary>
    /// 任务更新字段索引
    /// </summary>

    [Comment("任务更新字段索引")]
    public int Index { get; set; }

    /// <summary>
    /// 任务更新字段
    /// </summary>

    [Comment("任务更新字段")]
    public string? IndexText { get; set; }

    /// <summary>
    /// 任务更新状态索引
    /// </summary>

    [Comment("任务更新状态索引")]
    public int State { get; set; }

    /// <summary>
    /// 任务更新状态
    /// </summary>

    [Comment("任务更新状态")]
    public string? StateText { get; set; }

    /// <summary>
    /// 本次任务运行api
    /// </summary>

    [Comment("本次任务运行api")]
    public string? InvokeApi { get; set; }

    /// <summary>
    /// 服务器老任务状态索引
    /// </summary>

    [Comment("服务器老任务状态索引")]
    public int ServerOldState { get; set; }

    /// <summary>
    /// 客户端老任务状态索引
    /// </summary>

    [Comment("客户端老任务状态索引")]
    public int ClientOldState { get; set; }

    /// <summary>
    /// 计算更新数量
    /// </summary>

    [Comment("计算更新数量")]
    public int Count { get; set; }

    /// <summary>
    /// 最后操作人uid
    /// </summary>

    [Comment("最后操作人uid")]
    public int LastOperateId { get; set; }
}

//-- ----------------------------
//-- Table structure for erp3_sync_alive_session
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_sync_alive_session`;
//CREATE TABLE `erp3_sync_alive_session`  (
//  `id` bigint unsigned NOT NULL,
//  `i_upload_id` int unsigned NOT NULL COMMENT '上传id',
//  `i_user_id` int unsigned NOT NULL COMMENT '用户id',
//  `i_group_id` int unsigned NOT NULL COMMENT '当前用户组id',
//  `i_company_id` int unsigned NOT NULL COMMENT '店铺公司id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `d_index` tinyint unsigned NOT NULL COMMENT '任务更新字段索引',
//  `d_index_text` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '任务更新字段',
//  `d_state` tinyint unsigned NOT NULL COMMENT '任务更新状态索引',
//  `d_state_text` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '任务更新状态',
//  `d_invoke_api` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '本次任务运行api',
//  `d_server_old_state` tinyint unsigned NULL COMMENT '服务器老任务状态索引',
//  `d_client_old_state` tinyint unsigned NULL COMMENT '客户端老任务状态索引',
//  `d_count` tinyint unsigned NULL COMMENT '计算更新数量',
//  `i_last_operate_id` bigint unsigned NULL COMMENT '最后操作人uid',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `upload_id`(`i_upload_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 503509 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '监听Session日志,跟踪session状态,帮助客户端解决逻辑可能出现的问题' ROW_FORMAT = Dynamic;