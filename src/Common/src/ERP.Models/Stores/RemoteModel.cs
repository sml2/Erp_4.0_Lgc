using ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;

using System.ComponentModel;

using System.ComponentModel.DataAnnotations;

namespace ERP.Models.Stores;

[Index(nameof(Sort))]
public class RemoteModel : UserModel
{
    /// <summary>
    /// 管理名称
    /// </summary>

    [Comment("管理名称")]
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// VPS地址
    /// </summary>

    [Comment("VPS地址")]
    public string Address { get; set; } = string.Empty;

    /// <summary>
    /// VPS用户
    /// </summary>

    [Comment("VPS用户")]
    public new string User { get; set; } = string.Empty;

    /// <summary>
    /// VPS密码
    /// </summary>

    [Comment("VPS密码")]
    public string Password { get; set; } = string.Empty;

    /// <summary>
    /// 排序字段
    /// </summary>

    [Comment("排序字段")]
    public int Sort { get; set; } = 0;
}

//-- ----------------------------
//-- Table structure for erp3_remote
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_remote`;
//CREATE TABLE `erp3_remote`  (
//  `id` bigint unsigned NOT NULL,
//  `d_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '管理名称',
//  `d_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'VPS地址',
//  `d_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'VPS用户',
//  `d_password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'VPS密码',
//  `i_user_id` int unsigned NOT NULL COMMENT '创建用户id',
//  `i_group_id` int unsigned NOT NULL COMMENT '创建用户组id',
//  `i_oem_id` int unsigned NOT NULL COMMENT 'oemid',
//  `i_company_id` int unsigned NOT NULL COMMENT '公司id',
//  `d_sort` int (0) NOT NULL DEFAULT 0 COMMENT '排序字段',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `d_sort`(`d_sort`) USING BTREE,
// INDEX `i_company_id`(`i_company_id`) USING BTREE,
// INDEX `i_group_id`(`i_group_id`) USING BTREE,
// INDEX `i_oem_id`(`i_oem_id`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 1151 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '店铺管理_远程管理' ROW_FORMAT = Dynamic;