

namespace ERP.Models.Stores;

using System.ComponentModel.DataAnnotations.Schema;
using OrderSDK.Modles.Amazon;
using OrderSDK.Modles.CommonType;
using amazonStore = OrderSDK.Modles.CommonType.StoreRegion;
public enum Chooses
{
    //第三方应用程序
    BINDMODE_THIRDPART = 1,
    BELONGS_DISTRIBUTION,
    BINDMODE_MWSDEV
}


public partial class StoreRegion
{

    [NotMapped]
    public AmazonCredential? amazonCredential { get; set; }

    [NotMapped]
    public AmazonToken? amazonToken { get; set; }

    [NotMapped]
    public amazonStore? amazonStore { get; set; }

    public class SPAmazonData
    {
        public SPAmazonData()
        {

        }
        public bool SetData(string sellerId, int? platformDataID, int keyID)
        {
            bool change = false;
            if (!SellerID.Equals(sellerId) || !PlatformDataID.Equals(platformDataID) || !this.keyID.Equals(keyID))
            {
                change = true;
            }

            SellerID = sellerId;
            PlatformDataID = platformDataID;
            this.keyID = keyID;
            return change;
        }
        public SPAmazonData(string sellerID, int? platformDataID, int keyID)
        {
            SellerID = sellerID;
            PlatformDataID = platformDataID;
            this.keyID = keyID;
        }

        public string AccessToken { get; set; } = string.Empty;

        public string RefreshToken { get; set; } = string.Empty;

        public int ExpireIn { get; set; }

        public DateTime Date_Created { get; set; } = DateTime.UtcNow;

        public string TokenType { get; set; }

        public string SellerID { get; set; }

        public int? PlatformDataID { get; set; }

        public int keyID { get; set; }
    }
}