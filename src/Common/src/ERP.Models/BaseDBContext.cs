using ERP.Data;
using ERP.Interface;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models;

public partial class BaseDBContext : Base
{
    public BaseDBContext(DbContextOptions options, IOemProvider oemProvider) : base(options, oemProvider)
    {
    }
}