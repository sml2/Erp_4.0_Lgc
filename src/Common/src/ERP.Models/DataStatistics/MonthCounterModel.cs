using System.ComponentModel.DataAnnotations;
using HotChocolate;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.DataStatistics;

public class MonthCounterModel
{
    [Comment("表索引")]
    [Key, GraphQLName("id")]
    public int ID { get; set; }
    
    [Comment("日活总数")]
    public int UserTotal { get; set; }
    
    [Comment("Oem")]
    public int OemId { get; set; }
    
    [Comment("创建时间")]
    public DateTime CreatedAt { get; set; } = DateTime.Now;

    [Comment("更新时间")]
    public DateTime UpdatedAt { get; set; } = DateTime.Now;
}