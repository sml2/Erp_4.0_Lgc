using System.ComponentModel.DataAnnotations;
using ERP.Models.Abstract;
using HotChocolate;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.DataStatistics;

public class StatisticsChartModel
{
    [Comment("表索引")]
    [Key, GraphQLName("id")]
    public int ID { get; set; }

    /// <summary>
    /// 产品总数
    /// </summary>

    [Comment("产品总数")]
    public int ProductTotal { get; set; }

    /// <summary>
    /// 订单总数
    /// </summary>

    [Comment("订单总数")]
    public int OrderTotal { get; set; }

    /// <summary>
    /// 采购总数
    /// </summary>

    [Comment("采购总数")]
    public int PurchaseTotal { get; set; }

    /// <summary>
    /// 运单总数
    /// </summary>

    [Comment("运单总数")]
    public int WaybillTotal { get; set; }

    /// <summary>
    /// oem总数
    /// </summary>

    [Comment("oem总数")]
    public int OemTotal { get; set; }

    /// <summary>
    /// 公司总数
    /// </summary>

    [Comment("公司总数")]
    public int CompanyTotal { get; set; }

    /// <summary>
    /// 用户总数
    /// </summary>

    [Comment("用户总数")]
    public int UserTotal { get; set; }

    /// <summary>
    /// 邮箱总数
    /// </summary>

    [Comment("邮箱总数")]
    public int MailboxTotal { get; set; }

    /// <summary>
    /// 邮件总数
    /// </summary>

    [Comment("邮件总数")]
    public int EmailTotal { get; set; }

    /// <summary>
    /// 日活总数
    /// </summary>

    [Comment("日活总数")]
    public int ActivityTotal { get; set; }

    /// <summary>
    /// 库存总数
    /// </summary>

    [Comment("库存总数")]
    public int QuantityTotal { get; set; }

    /// <summary>
    /// 开户总数
    /// </summary>

    [Comment("开户总数")]
    public int OpenTotal { get; set; }

    /// <summary>
    /// 店铺总数
    /// </summary>

    [Comment("店铺总数")]
    public int StoreTotal { get; set; }

    [Comment("创建时间")]
    public DateTime CreatedAt { get; set; } = DateTime.Now;

    [Comment("更新时间")]
    public DateTime UpdatedAt { get; set; } = DateTime.Now;
}