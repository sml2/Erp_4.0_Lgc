using ERP.Models.B2C;
using ERP.Models.Product;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models;

public partial class BaseDBContext
{
    public DbSet<DomainModel> Domain { get; set; } = null!;
    public DbSet<B2CUserModel> B2CUser { get; set; } = null!;
}