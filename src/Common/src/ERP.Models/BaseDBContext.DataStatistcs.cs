using ERP.Models.DataStatistics;
using ERP.Models.Product;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models;

public partial class BaseDBContext
{
    public DbSet<StatisticsChartModel> StatisticsChart { get; set; } = null!;
    public DbSet<MonthCounterModel> MonthCounter { get; set; } = null!;
}