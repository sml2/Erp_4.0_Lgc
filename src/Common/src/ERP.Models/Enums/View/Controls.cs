﻿namespace ERP.Enums.View
{
    public enum Controls
    {
        /// <summary>
        /// 单行输入框
        /// </summary>
        Input,
        Text = Input,//是否必填+[plain,url,mail,password_eye,password]

        /// <summary>
        /// 多行输入框
        /// </summary>
        TextArea,
        TextRich = TextArea,//是否必填

        /// <summary>
        /// 单选框
        /// </summary>
        Radio,
        RadioBox = Radio,//ValueDictionary

        /// <summary>
        /// 下拉框
        /// </summary>
        Select,
        ComboBox = Select,//ValueDictionary

        /// <summary>
        /// 多选框
        /// </summary>
        Check = Select,
        CheckBox = Select,//ValueDictionary

        /// <summary>
        /// 整型数值
        /// </summary>
        Number,//Min,Max,精度
    }
}
