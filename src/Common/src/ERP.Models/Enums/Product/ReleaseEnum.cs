namespace ERP.Enums.Product;

public enum ReleaseEnum
{
    /// <summary>
    /// 未发布
    /// </summary>
    NoRelease,
    /// <summary>
    /// 已发布
    /// </summary>
    Release
}