﻿using System.ComponentModel;

namespace ERP.Enums.Orders;
public enum IsMains
{
    [Description("副订单")]
    VICE = 1,

    [Description("主订单")]
    THELORD = 2
}