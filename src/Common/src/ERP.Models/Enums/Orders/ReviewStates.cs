﻿using System.ComponentModel;

namespace ERP.Enums.Orders;
public enum ReviewStates
{
    [Description("未评价")]
    NOTEVALUATED = 0,

    [Description("已评价")]
    REVIEWED = 1,

    [Description("已追评")]
    ADDTO = 2
}