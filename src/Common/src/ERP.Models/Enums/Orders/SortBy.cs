﻿using System.ComponentModel;

namespace ERP.Enums.Orders;
public enum SortBy
{
    [Description("亚马逊订单生成时间(降序)")]
    CreateAtDESC =1,
    [Description("亚马逊订单生成时间(升序)")]
    CreateAtASC,
    [Description("订单拉取时间(降序)")]
    PullTimeDesc,
    [Description("订单拉取时间(升序)")]
    PullTimeAsc,
    [Description("最迟发货时间(降序)")]
    LastSendDesc,
    [Description("最迟发货时间(升序)")]
    LastSendAsc,
}
