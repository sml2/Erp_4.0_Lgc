﻿using System.ComponentModel;

namespace ERP.Enums.Orders;
public enum EntryModes
{
    None,
    [Description("表单录入")]
    WebForm = 1,
    [Description("API手动拉取")]
    APIManual,
    [Description("API自动拉取")]
    APIAutomatic
}