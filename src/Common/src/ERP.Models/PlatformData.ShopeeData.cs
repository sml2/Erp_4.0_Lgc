using ERP.Models.ProductNodeType.Enum;

namespace ERP.Models;
public partial class PlatformData
{
    public class ShopeeData
    {
        //[Comment("地区名称")]
        public string AreaName { get; set; } = string.Empty;
        //[Comment("地区简称")]
        public string ShorterForm { get; set; } = string.Empty;
        public int ShopId { get; set; }

        public EnumShopType? ShopType { get; set; }

        public int KeyID { get; set; }
    }
}
