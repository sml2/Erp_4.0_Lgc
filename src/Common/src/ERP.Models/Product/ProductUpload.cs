using ERP.Enums;
using ERP.Models.DB.Stores;
using ERP.Models.Product;
using ERP.Models.Stores;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;
using static EnumExtension;

namespace ERP.Models.DB.Product
{

    [Table("ProductUpload")]
    [Comment("产品上传记录")]
    public class ProductUpload : UserRelated
    {
        public ProductUpload()
        {
            
        }
        public ProductUpload(ProductModel pmodel, StoreRegion store , UploadResult result, string pathNodes, string pathName, Platforms? platform,string marketplace, Languages languages, string productType,string unit, int userid, int groupID, int companyID, int oemID)
        {
            Result = result;
            Product = pmodel;
            this.Store = store;
            PathNodes = pathNodes;
            PathName = pathName;
            MarketPlace= marketplace;
            ProductType= productType;
            Unit= unit;
            Platform = platform;
            this.Languages = languages;
            base.UserID = userid;
            base.GroupID = groupID;
            base.CompanyID = companyID;
            base.OEMID = oemID;
        }

        public int ID { get; set; }
        
        public UploadResult Result { get; set; } = UploadResult.None;


        [Comment("具体错误信息")]
        public string Message { get; set; } = "[]";

        public int ProductId { get; set; }
        /// <summary>
        /// 产品ID
        /// </summary>      
        public ProductModel Product { get; set; }
        
        /// <summary>
        /// 所属上传任务id
        /// </summary>
        public int SyncUploadTaskId { get; set; }
        [JsonIgnore]
        public SyncUploadTask SyncUploadTask { get; set; }

        /// <summary>
        /// 店铺id
        /// </summary>
        [Comment("店铺id")]
        public int StoreId { get; set; }

        [Comment("店铺ID")]        

        public StoreRegion Store { get; set; }

        [Comment("上传平台节点路径")]
        public string PathNodes { get; set; }

        [Comment("上传平台节点路径名称")]
        public string PathName { get; set; }

        [Comment("平台")]
        public Platforms? Platform { get; set; }

        public string MarketPlace { get; set; }
         
        public Languages Languages { get; set; }

        public string LanguageSign { get { return Languages.GetDescription(); } }

        public string ProductType { get; set; }

        public string Unit { get; set; }
        /// <summary>
        /// [镜像]产品id
        /// </summary>
        [Comment("[镜像]产品id")]
        public int Spid { get; set; }

        /// <summary>
        /// 产品名称
        /// </summary>
        [Comment("产品名称")]
        public string? ProductName { get; set; }
        public int? ChangePriceId { get; set; }
        /// <summary>
        /// 导出表格参数
        /// </summary>
        public string Params { get; set; } = null!;
        /// <summary>
        /// 导出表格扩展字段
        /// </summary>
        public string Ext { get; set; } = null!;
    }
    public enum UploadResult
    {
        [Description("")]
        None,
        [Description("待刊登")]
        NeedToUpload,       
        [Description("刊登中")]
        Uploading,
        [Description("刊登失败")]
        UploadFail,
        [Description("刊登成功")]
        UploadSuccess,       
    }

    //[Table("ProductUploadJson")]
    //[Comment("产品上传json结果")]
    //public class ProductUploadJson
    //{
    //    public ProductUploadJson()
    //    {

    //    }

    //    public ProductUploadJson(string productJson, uploadResult result, string message, int pid, string sku)
    //    {
    //        ProductJson = productJson;
    //        Result = result;
    //        Message = message;
    //        ProductID = pid;
    //        Sku = sku;
    //    }
    //    public ProductUploadJson(string productJson, uploadResult result, string message, bool isMain, int pid, string sku)
    //    {
    //        ProductJson = productJson;
    //        Result = result;
    //        Message = message;
    //        IsMain = isMain;
    //        ProductID = pid;
    //        Sku = sku;
    //     }

    //    public int ID { get; set; }

    //    [Comment("上传产品内容")]
    //    public string ProductJson { get; set; } = "[]";

    //    public uploadResult Result { get; set; } = uploadResult.None;

    //    [Comment("具体错误信息")]
    //    public string Message { get; set; }

    //    public bool IsMain { get; set; }

    //    /// <summary>
    //    /// 产品ID
    //    /// </summary>
    //    public int ProductID { get; set; }
    //    public ProductModel product { get; set; } = default!;

    //    public string Sku { get; set; }
    //}


    //[Table("ProductUploadLog")]
    //[Comment("产品上传日志")]
    //public class ProductUploadLog : UserRelated
    //{
    //    public ProductUploadLog()
    //    {

    //    }
    //    public ProductUploadLog(int Pid, string action, string userName,int userid,int groupID,int companyID,int oemID)
    //    {
    //        this.Pid = Pid;
    //        Action = action;
    //        UserName = userName;
    //        base.UserID= userid;
    //        base.GroupID = groupID;
    //        base.CompanyID = companyID;
    //        base.OEMID= oemID;
    //    }

    //    public int ID { get; set; }

    //    public int Pid { set; get; }

    //    public string Action { set; get; }

    //    public string UserName { get; set; }
    //}    



}
