using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.Product;

public class ProductMarkDeletionModel : UserModel
{
    public ProductMarkDeletionModel()
    {
    }

    // public ProductMarkDeletionModel(ProductModel model)
    // {
    //     Pid = model.ID;
    //     PCreatedAt = model.CreatedAt;
    //     UserID = model.UserID;
    //     GroupID = model.GroupID;
    //     CompanyID = model.CompanyID;
    // }

    public int? OEMID { get; set; }

    [Comment("产品Id")]
    public int Pid { get; set; }

    [Comment("脚本触发时间")]
    public DateTime TriggerTime { get; set; }

    [Comment("产品创建时间")]
    public DateTime PCreatedAt { get; set; }
}