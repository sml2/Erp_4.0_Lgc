using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.Product;

public class ProductValidityModel : BaseModel
{
    public ProductValidityModel()
    {
    }

    public int UserId { get; set; }

    public int CompanyId { get; set; }

    [Comment("此次延长时间对应费用")]
    public decimal? Cost { get; set; }

    [Comment("到期时间")]
    public DateTime Validity { get; set; }

    [Comment("备注")]
    public string? Remark { get; set; }

    [Comment("添加人")]
    public int AddUserId { get; set; }

    public string AddTrueName { get; set; }
}