namespace ERP.Models.Product;

public class DistributionProduct : UserRelated
{
    public object GetPriceAttribute(object val)
    {
        //    if (isset($this->attributes['price'])) {
        //        $rate = $this->getUiCoinAttribute();
        //        $price = json_decode($this->attributes['price'], true);
        //        array_walk_recursive($price, static function (&$v) use ($rate) {
        //            $v = ToConversion($v, $rate);
        //        });
        //    } else {
        //        $price = [
        //            'cost' => ['max' => 0, 'min' => 0],
        //            'sale' => ['max' => 0, 'min' => 0]
        //        ];
        //    }
        var t = new{ max = 0, min = 0 };
        var r = new{ cost = t, sale = t};
        return r;
    }

    //public function getUiCoinAttribute()
    //{
    //    return $this->getSession()->getUnitConfig();
    //}

    //public function getRecycleAttribute(): bool
    //{
    //    if (isset($this->attributes['type'])) {
    //        $typeBiter = new Type($this->attributes['type']);
    //        return $typeBiter->isRecycle();
    //    }
    //    return false;
    //}

    //public function getShareAttribute(): bool
    //{
    //    if (isset($this->attributes['type'])) {
    //        $typeBiter = new Type($this->attributes['type']);
    //        return $typeBiter->isShare();
    //    }
    //    return false;
    //}

    //public function getVariantsAttribute()
    //{
    //    $rate = $this->getUiCoinAttribute();
    //    $variants = collect();
    //    if (isset($this->attributes['variants'])) {
    //        $variants = collect(json_decode($this->attributes['variants'], true))
    //            ->map(function($v) use ($rate) {
    //                $v['cost'] = ToConversion($v['cost'], $rate);
    //                $v['sale'] = ToConversion($v['sale'], $rate);
    //                $v['quantity'] = (int)$v['quantity'];
    //                if (isset($v['lps'])) {
    //                    foreach ($v['lps'] as &$val) {
    //                        if ($val && isset($val['s'])) {
    //                            $val['s'] = collect($val['s'])->map(
    //                                fn($item) => is_string($item) ? ['value' => $item] : $item
    //                            );
    //                        }
    //                    }
    //                }
    //                return $v;
    //            });
    //    }
    //    // 是否单变体
    //    if ($variants->count() === 1) {
    //        //去重变体附图
    //        $variants = $variants->map(static function($v) {
    //            $v['img']['affiliate'] = array_unique($v['img']['affiliate']);
    //            return $v;
    //        });
    //    }
    //    return $variants;
    //}

    //public function getUiLanguagesAttribute()
    //{
    //    $languages = json_decode($this->attributes['languages'], true) ?: [];
    //    $lp = ['s'];
    //    foreach ($lp as $val) {
    //        foreach ($languages as $k => $v) {
    //            $languages[$k][$val] = collect($v[$val])->map(
    //                fn ($item) => ['value' => $item]
    //            );
    //        }
    //    }

    //    $uiLanguage = 'default';
    //    $languages[$uiLanguage] = [
    //        'n' => $this->lp_n,
    //        's' => collect($this->lp_s)->map(
    //            fn ($item) => ['value' => $item]
    //        ),
    //        'k' => $this->lp_k,
    //        'd' => $this->lp_d,
    //    ];

    //    return $languages;
    //}
}