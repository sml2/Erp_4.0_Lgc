using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.Product;

[Comment("抠图图片样本集")]
public class MattingTrainModel : BaseModel
{
    /// <summary>
    /// 旧图oss网址
    /// </summary>

    [Comment("旧图oss网址")]
    public string? OldImg { get; set; }
    /// <summary>
    /// 新图oss网址
    /// </summary>

    [Comment("新图oss网址")]
    public string? NewImg { get; set; }
}

//-- ----------------------------
//-- Table structure for erp3_matting_train
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_matting_train`;
//CREATE TABLE `erp3_matting_train`  (
//  `id` bigint unsigned NOT NULL,
//  `d_old_img` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '旧图oss网址',
//  `d_new_img` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '新图oss网址',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY (`id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 9298 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '抠图图片样本集' ROW_FORMAT = Dynamic;