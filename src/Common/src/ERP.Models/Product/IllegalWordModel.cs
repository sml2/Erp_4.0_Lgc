using System.ComponentModel.DataAnnotations.Schema;
using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.Product;

[Table("illegal_word")]
[Comment("产品管理_违禁词表")]
public class IllegalWordModel : UserModel
{
    [Comment("违禁词")]
    public string Value { get; set; } = string.Empty;
    [Comment("HashCode")]
    public string HashCode { get; set; } = string.Empty;
    [Comment("违禁词等级 1为OEM管理员设置")]
    public int Level { get; set; }
}

//-- ----------------------------
//-- Table structure for erp3_illegal_word
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_illegal_word`;
//CREATE TABLE `erp3_illegal_word`  (
//  `id` bigint unsigned NOT NULL,
//  `d_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '违禁词',
//  `i_oem_id` bigint unsigned NOT NULL COMMENT 'oem_id',
//  `i_user_id` bigint unsigned NOT NULL COMMENT '创建用户id',
//  `i_group_id` bigint unsigned NOT NULL COMMENT '分组id',
//  `i_company_id` bigint unsigned NOT NULL COMMENT '所属公司id',
//  `t_level` tinyint unsigned NOT NULL COMMENT '违禁词等级 1为OEM管理员设置',
//  `d_hash_code` bigint unsigned NOT NULL,
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `hash_code`(`d_hash_code`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 52270 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '产品管理_违禁词表' ROW_FORMAT = Dynamic;