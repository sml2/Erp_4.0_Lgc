using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.Product;

[Index(nameof(PlatformId), nameof(StoreId))]
public class ToSellTaskModel : UserModel
{
    /// <summary>
    /// 产品上传session_state
    /// </summary>

    [Comment("产品上传session_state")]
    public int UploadSessionState { get; set; }

    /// <summary>
    /// 产品上传亚马逊返回 upload_session_id
    /// </summary>

    [Comment("产品上传亚马逊返回 upload_session_id")]
    public string? UploadSessionId { get; set; }

    /// <summary>
    /// 价格上传session_state
    /// </summary>

    [Comment("价格上传session_state")]
    public int PriceSessionState { get; set; }

    /// <summary>
    /// 价格上传亚马逊返回 price_session_id
    /// </summary>

    [Comment("价格上传亚马逊返回 price_session_id")]
    public int PriceSessionId { get; set; }

    /// <summary>
    /// 库存上传stock_session_state
    /// </summary>

    [Comment("库存上传stock_session_state")]
    public int StockSessionState { get; set; }

    /// <summary>
    /// 库存上传亚马逊返回 stock_session_id
    /// </summary>

    [Comment("库存上传亚马逊返回 stock_session_id")]
    public int StockSessionId { get; set; }

    /// <summary>
    /// 最终结果 result:成功 true失败 false ，message:返回附属信息
    /// </summary>

    [Comment("最终结果 result:成功 true失败 false ，message:返回附属信息")]
    public int EndResult { get; set; }

    /// <summary>
    /// 任务集执行是否结束
    /// </summary>

    [Comment("任务集执行是否结束")]
    public bool IsFinish { get; set; }

    /// <summary>
    /// 上传跟卖产品店铺id（平台-商店）
    /// </summary>

    [Comment("上传跟卖产品店铺id（平台-商店）")]
    public int StoreId { get; set; }

    /// <summary>
    /// 所属平台id
    /// </summary>

    [Comment("所属平台id")]
    public int PlatformId { get; set; }

    /// <summary>
    /// 任务分配用户id
    /// </summary>

    [Comment("任务分配用户id")]
    public int OperateUserId { get; set; }
}

//-- ----------------------------
//-- Table structure for erp3_to_sell_task
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_to_sell_task`;
//CREATE TABLE `erp3_to_sell_task`  (
//  `id` bigint unsigned NOT NULL,
//  `d_upload_session_state` tinyint unsigned NOT NULL COMMENT '产品上传session_state',
//  `d_upload_session_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '产品上传亚马逊返回 upload_session_id',
//  `d_price_session_state` tinyint unsigned NOT NULL COMMENT '价格上传session_state',
//  `d_price_session_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '价格上传亚马逊返回 price_session_id',
//  `d_stock_session_state` tinyint unsigned NOT NULL COMMENT '库存上传stock_session_state',
//  `d_stock_session_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '库存上传亚马逊返回 stock_session_id',
//  `d_end_result` json NOT NULL COMMENT '最终结果 result:成功 true失败 false ，message:返回附属信息',
//  `t_is_finish` tinyint unsigned NOT NULL COMMENT '任务集执行是否结束 1否2是 ',
//  `i_store_id` int unsigned NOT NULL COMMENT '上传跟卖产品店铺id（平台-商店）',
//  `i_platform_id` int unsigned NOT NULL COMMENT '所属平台id',
//  `i_operate_user_id` int (0) NOT NULL DEFAULT 0 COMMENT '任务分配用户id',
//  `i_user_id` int (0) NOT NULL DEFAULT 0 COMMENT '创建用户id',
//  `i_group_id` int (0) NOT NULL DEFAULT 0 COMMENT '创建用户组id',
//  `i_company_id` int (0) NOT NULL DEFAULT 0 COMMENT '公司id',
//  `i_oem_id` int (0) NOT NULL DEFAULT 0 COMMENT 'oem_id',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `platform_id`(`i_platform_id`) USING BTREE,
// INDEX `store_id`(`i_store_id`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 1103 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '产品管理_跟卖产品上传任务集' ROW_FORMAT = Dynamic;