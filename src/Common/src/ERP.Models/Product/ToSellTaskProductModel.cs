using System.ComponentModel;
using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.Product;

[Index(nameof(EndResult), nameof(HashCode), nameof(Pid), nameof(PriceResult), nameof(ProductResult), nameof(SellerId), nameof(StockResult), nameof(StoreId))]
public class ToSellTaskProductModel : UserModel
{
    /// <summary>
    /// 产品原店铺marketplace
    /// </summary>

    [Comment("产品原店铺marketplace")]
    public string? Marketplace { get; set; }

    /// <summary>
    /// 产品上传SKU
    /// </summary>

    [Comment("产品上传SKU")]
    public string? Sku { get; set; }

    /// <summary>
    /// asin码（亚马逊唯一编号，访问商品）
    /// </summary>

    [Comment("asin码（亚马逊唯一编号，访问商品）")]
    public string Asin { get; set; }

    /// <summary>
    /// 商品价格(填写价格)
    /// </summary>

    [Comment("商品价格(填写价格)")]
    public decimal Price { get; set; }

    /// <summary>
    /// 商品价格(基准单位)
    /// </summary>

    [Comment("商品价格(基准单位)")]
    public decimal BasePrice { get; set; }

    /// <summary>
    /// 产品货币单位
    /// </summary>

    [Comment("产品货币单位")]
    public int Unit { get; set; }

    /// <summary>
    /// 汇率(更改时汇率)
    /// </summary>

    [Comment("汇率(更改时汇率)")]
    public int Rate { get; set; }

    /// <summary>
    /// 产品上传默认库存
    /// </summary>

    [Comment("产品上传默认库存")]
    public int Stock { get; set; }

    /// <summary>
    /// 备货时间
    /// </summary>

    [Comment("备货时间")]
    public int StockUp { get; set; }

    /// <summary>
    /// 产品状态  New, UsedLikeNew, UsedVeryGood, UsedGood, UsedAcceptable, CollectibleLikeNew, CollectibleVeryGood, CollectibleGood, CollectibleAcceptable, Refurbished, Club
    /// </summary>

    [Comment("产品状态  New, UsedLikeNew, UsedVeryGood, UsedGood, UsedAcceptable, CollectibleLikeNew, CollectibleVeryGood, CollectibleGood, CollectibleAcceptable, Refurbished, Club")]
    public string? ProductState { get; set; }

    /// <summary>
    /// 跟卖产品id
    /// </summary>

    [Comment("跟卖产品id")]
    public int Pid { get; set; }

    /// <summary>
    /// 【asin+sku+marketplace+seller_id】 hashCode
    /// </summary>

    [Comment("【asin+sku+marketplace+seller_id】 hashCode")]
    public int HashCode { get; set; }

  
    public enum ProductResults
    {
        [Description("待同步")]
        ONE = 1,

        [Description("上传完成")]
        TWO = 2,

        [Description("成功")]
        THREE = 3,

        [Description("失败")]
        FOUR = 4
    }

    /// <summary>
    /// 上传状态 1待同步2上传完成3成功4失败
    /// </summary>

    [Comment("上传状态 1待同步2上传完成3成功4失败")]
    public ProductResults ProductResult { get; set; }

    /// <summary>
    /// 产品上传错误信息
    /// </summary>

    [Comment("产品上传错误信息")]
    public string? ProductReason { get; set; }

    public enum PriceResults
    {
        [Description("待同步")]
        ONE = 1,

        [Description("上传完成")]
        TWO = 2,

        [Description("成功")]
        THREE = 3,

        [Description("失败")]
        FOUR = 4
    }

    /// <summary>
    /// 上传状态 1待同步2上传完成3成功4失败
    /// </summary>

    [Comment("上传状态 1待同步2上传完成3成功4失败")]
    public PriceResults PriceResult { get; set; }

    /// <summary>
    /// 价格上传错误信息
    /// </summary>

    [Comment("价格上传错误信息")]
    public string? PriceReason { get; set; }

    public enum StockResults
    {
        [Description("待同步")]
        ONE = 1,

        [Description("上传完成")]
        TWO = 2,

        [Description("成功")]
        THREE = 3,

        [Description("失败")]
        FOUR = 4
    }

    /// <summary>
    /// 上传状态 1待同步2上传完成3成功4失败
    /// </summary>

    [Comment("上传状态 1待同步2上传完成3成功4失败")]
    public StockResults StockResult { get; set; }

    /// <summary>
    /// 库存上传错误信息
    /// </summary>

    [Comment("库存上传错误信息")]
    public int StockReason { get; set; }

    public enum EndResults
    {
        [Description("成功")]
        TRUE = 1,

        [Description("失败")]
        FALSE = 2
    }

    /// <summary>
    /// 最终结果
    /// </summary>

    [Comment("最终结果")]
    public EndResults EndResult { get; set; }

    /// <summary>
    /// 最终结果
    /// </summary>

    [Comment("最终结果")]
    public int OtherInfo { get; set; }

    /// <summary>
    /// 最后一次上传产品参数快照(sku,price,unit,rate,stock,product_state)
    /// </summary>

    [Comment("最后一次上传产品参数快照(sku,price,unit,rate,stock,product_state)")]
    public int LastFinishData { get; set; }

    public enum UploadStates
    {
        [Description("待同步")]
        ONE = 1,

        [Description("同步中")]
        TWO = 2,

        [Description("成功")]
        THREE = 3,
    }

    /// <summary>
    /// 上传产品状态 1待同步 2同步中 3完成
    /// </summary>

    [Comment("上传产品状态 1待同步 2同步中 3完成")]
    public UploadStates UploadState { get; set; }

    public enum Types
    {
        [Description("上架")]
        UPLOAD = 1,

        [Description("更新")]
        UPDATE = 2,

        [Description("删除")]
        DELETE = 3
    }

    /// <summary>
    /// 上传产品状态  1上传(上架) 2更新（同步） 3删除（下架）
    /// </summary>

    [Comment("上传产品状态  1上传(上架) 2更新（同步） 3删除（下架）")]
    public Types Type { get; set; }

    public enum DataStates
    {
        [Description("未变更")]
        FALSE = 1,

        [Description("已变更")]
        TRUE = 2
    }

    /// <summary>
    /// 数据状态 1未变更 2已变更
    /// </summary>

    [Comment("数据状态 1未变更 2已变更")]
    public DataStates DataState { get; set; }

    /// <summary>
    /// 上传跟卖产品店铺id（平台-商店）
    /// </summary>

    [Comment("上传跟卖产品店铺id（平台-商店）")]
    public int StoreId { get; set; }

    /// <summary>
    /// seller_id
    /// </summary>

    [Comment("seller_id")]
    public int SellerId { get; set; }

    /// <summary>
    /// 拉取用户用户姓名
    /// </summary>

    [Comment("拉取用户用户姓名")]
    public string? TrueName { get; set; }
}