using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.Product;

[Index(nameof(TaskId), nameof(TaskProductId))]
public class ToSellTaskProductRelationModel : UserModel
{
    /// <summary>
    /// 跟卖产品上传任务集id
    /// </summary>

    [Comment("跟卖产品上传任务集id")]
    public int TaskId { get; set; }

    /// <summary>
    /// 跟卖产品上传任务下任务产品id
    /// </summary>

    [Comment("跟卖产品上传任务下任务产品id")]
    public int TaskProductId { get; set; }
}

//-- ----------------------------
//-- Table structure for erp3_to_sell_task_product_relation
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_to_sell_task_product_relation`;
//CREATE TABLE `erp3_to_sell_task_product_relation`  (
//  `id` bigint unsigned NOT NULL,
//  `i_task_id` int (0) NOT NULL DEFAULT 0 COMMENT '跟卖产品上传任务集id',
//  `i_task_product_id` int (0) NOT NULL DEFAULT 0 COMMENT '跟卖产品上传任务下任务产品id',
//  `i_user_id` int (0) NOT NULL DEFAULT 0 COMMENT '创建用户id',
//  `i_group_id` int (0) NOT NULL DEFAULT 0 COMMENT '创建用户组id',
//  `i_company_id` int (0) NOT NULL DEFAULT 0 COMMENT '公司id',
//  `i_oem_id` int (0) NOT NULL DEFAULT 0 COMMENT 'oem_id',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `task_id`(`i_task_id`) USING BTREE,
// INDEX `task_product_id`(`i_task_product_id`) USING BTREE,
// INDEX `user_id`(`i_user_id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 10396 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '产品管理_跟卖产品上传任务集关联上传产品信息' ROW_FORMAT = Dynamic;