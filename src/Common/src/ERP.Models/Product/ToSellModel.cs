using System.ComponentModel;
using ERP.Data;
using ERP.Models.Abstract;
using ERP.Models.Stores;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.Product;

[Index(nameof(HashCode))]
public class ToSellModel : UserModel
{
    /// <summary>
    /// asin+storeid
    /// </summary>
    [Comment("标题 hashCode")]
    public ulong HashCode { get; set; }


    /// <summary>
    /// 产品标题
    /// </summary>
    [Comment("产品标题")]
    public string? Title { get; set; }

    /// <summary>
    /// asin码（亚马逊唯一编号，访问商品）
    /// </summary>
    [Comment("asin码")]
    public string Asin { get; set; }


    /// <summary>
    /// Sku
    /// </summary>
    [Comment("Sku")]
    public string? Sku { get; set; }

    /// <summary>
    /// 货币
    /// </summary>
    [Comment("货币")]
    public string Currency { get; set; }

    /// <summary>
    /// ProductType产品类型
    /// </summary>
    [Comment("ProductType")]
    public string ProductType { get; set; }

    /// <summary>
    /// 备货时间
    /// </summary>
    [Comment("StockDays")]
    public int? StockDays { get; set; }

    /// <summary>
    /// 产品图片地址
    /// </summary>
    [Comment("产品图片地址")]
    public string? ImageUrl { get; set; }

    /// <summary>
    /// 原产品所属marketplace
    /// </summary>
    [Comment("产品原店铺marketplace")]
    public string AsinMarketplace { get; set; } = string.Empty;   

    /// <summary>
    /// 商品最低价(基准单位)
    /// </summary>
    [Comment("商品最低价(基准单位)")]
    public MoneyRecord UnitPrice { get; set; } = MoneyRecord.Empty;


    /// <summary>
    /// 运费
    /// </summary>
    [Comment("运费")]
    public MoneyRecord ShippingPrice { get; set; } = MoneyRecord.Empty;


    /// <summary>
    /// 库存
    /// </summary>
    [Comment("库存")]
    public int Quantity { get; set; }

    /// <summary>
    /// 品牌
    /// </summary>
    [Comment("品牌")]
    public string? Brand { get; set; }

    /// <summary>
    /// 制造商
    /// </summary>
    [Comment("制造商")]
    public string? Manufacturer { get; set; }
       

    ///// <summary>
    ///// 所属平台id
    ///// </summary>
    //[Comment("所属平台id")]
    //public Platforms Plateform { get; set; }

    /// <summary>
    /// 拉取产品卖家店铺id（平台-商店）
    /// </summary>

    [Comment("拉取产品卖家店铺id（平台-商店）")]
    public StoreRegion storeRegion { get; set; }


    /// <summary>
    /// 操作结果信息
    /// </summary>

    [Comment("操作结果信息")]
    public string Msg { get; set; }

    /// <summary>
    /// 操作结果信息
    /// </summary>

    [Comment("同步时间")]
    public DateTime? SyncTime { get; set; }


    [Comment("同步标记，只要同步成功过，就是1，否则就是0")]
    public int Sync { get; set; } = 0;

    [Comment("产品状态")]
    public string? ProductState { get; set; }

    /// <summary>
    /// 产品状态
    /// </summary>

    [Comment("产品状态")]
    public HijackingResults ProductResult { get; set; }
}

public enum HijackingResults
{
    [Description("暂无状态")]
    None =0,
    [Description("待同步")]
    WaitSynchronize = 1,

    [Description("同步中")]
    Synchronize,

    [Description("同步失败")]
    SynchronizeFail,

    [Description("已上架")]
    PutOn,

    [Description("已下架")]
    TakeOff
}


//[Index(nameof(ToSellModelID))]
//public class HijackingAction : UserModel
//{
  
//    /// <summary>
//    /// 订单id
//    /// </summary>

//    [Comment("跟卖id")]
//    public int ToSellModelID { get; set; }

//    /// <summary>
//    /// 动作内容
//    /// </summary>

//    [Comment("动作内容")]
//    public string? Message { get; set; }

//    /// <summary>
//    /// 操作人
//    /// </summary>

//    [Comment("操作人")]
//    public string? UserName { get; set; }

//    /// <summary>
//    /// 操作用户姓名
//    /// </summary>

//    [Comment("操作用户姓名")]
//    public string? Truename { get; set; }
   
//}