using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ERP.Models.Stores;

namespace ERP.Models.Host
{
    /// <summary>
    /// 任务队列调度策略
    /// </summary>
    public interface IOrderWork
    {

        /// <summary>
        /// 下次评估时间
        /// </summary>
        DateTime NextEvaluate { get; set; }
        /// <summary>
        /// 当前拉取状态
        /// </summary>
        PullProgresses PullProgress { get; set; }
        /// <summary>
        /// 完整周期数
        /// </summary>
        int FullCycle { get; set; }
        /// <summary>
        /// 拉取次数
        /// </summary>
        int PullCount { get; set; }
        /// <summary>
        /// 最后自动拉取订单时间
        /// </summary>
        DateTime LastPullStart { get; set; }
        /// <summary>
        /// 拉取订单范围结束时间（待用，等待GetOrder，GetOrderNext执行完成后赋值<see cref="QueryEndPayload"/>）
        /// </summary>
        DateTime QueryEndPayload { get; set; }
        /// <summary>
        /// <see cref="PullProgresses.Pulling">拉取订单范围开始时间</see> 
        /// <see cref="PullProgresses.Pulled">拉取订单范围结束时间</see> 
        /// </summary>
        DateTime QueryEnd { get; set; }
        /// <summary>
        /// 最后自动拉取耗时
        /// </summary>
        int LastDuration { get; set; }
        /// <summary>
        /// 订单拉取间隔秒数
        /// </summary>
        int IntervalSeconds { get; set; }
        /// <summary>
        /// 下次拉取订单时间
        /// </summary>
        DateTime NextPull { get; set; }
        /// <summary>
        /// 最后一条订单时间
        /// </summary>
        DateTime LastOrderTime { get; set; }
        /// <summary>
        /// 下次拉取参数
        /// </summary>
        string? NextCycleInfo { get; set; }
        StoreRegion.IsVerifyEnum IsVerify { get; set; }
    }
    public enum PullProgresses
    {
        Never,
        Pulling,
        Pulled
    }
}
