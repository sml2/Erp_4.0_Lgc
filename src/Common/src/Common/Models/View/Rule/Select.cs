﻿using ERP.Interface.Rule;

namespace ERP.Models.View.Rule;

public class Select : Authorization.Rule.Component.Select, IComponentView
{
    public Select(string key, string name, long value, Dictionary<int,string> data) : base(key, name, value, data)
    {
    }
}

