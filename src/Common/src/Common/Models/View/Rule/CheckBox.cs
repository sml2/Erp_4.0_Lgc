﻿using ERP.Interface.Rule;

namespace ERP.Models.View.Rule;

public class CheckBox : Authorization.Rule.Component.CheckBox, IComponentView
{
    public CheckBox(string key, string name, object value) : base(key, name, value)
    {
    }

    public CheckBox(string key, string name, object value, string type) : base(key, name, value, type)
    {
    }
}

