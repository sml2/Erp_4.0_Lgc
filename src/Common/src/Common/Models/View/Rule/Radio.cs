﻿using ERP.Interface.Rule;

namespace ERP.Models.View.Rule;

public class Radio : Authorization.Rule.Component.Radio, IComponentView
{
    public Radio(string key, string name, long value, Dictionary<int,string> data) : base(key, name, value, data)
    {
    }
}

