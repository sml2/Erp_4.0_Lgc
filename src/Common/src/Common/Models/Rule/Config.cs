﻿using System;
using System.Collections;
using System.Collections.Generic;
using ERP.Interface;
using ERP.Interface.Rule;
using ERP.Models.DB.Users;

namespace ERP.Models.Rule;

public class Config : IEnumerable<Config.Item>
{
    private int menuID;
    private List<Item> RawItems = new();
    private IEnumerable<Item> Items=Enumerable.Empty<Item>();


    public Config(int id)
    {
        menuID = id;
    }
    public void Add(string key, long value, IComponentData data)
    {
        RawItems.Add(new(key, value, data));
        Items = RawItems.OrderBy(i => i.Data.Sorter);
    }

    public class Item : IHas,To
    {
        public Item(string key, long value, IComponentData data)
        {
            this.key = key;
            Value = value;
            Data = data;
        }
        private string key { get; }
        public string Key { get=> $"{key}.{nameof(DB.Rule.Config<Enums.Rule.Flag>.Visible)}"; }

        public long Value { get; }

        public IComponentData Data { get; }
        public IConfigVisitor ToVal() => new ConfigVisitor($"{key}.{nameof(DB.Rule.Config<Enums.Rule.Flag>.Editable)}", Helpers.GetLastBit(Value), Value);

    }


    public IEnumerator<Item> GetEnumerator() => Items.GetEnumerator();

    IEnumerator IEnumerable.GetEnumerator() => Items.GetEnumerator();

    public IEnumerable<IComponentView> RenderConfigItems(Func<IHas, bool> has, Func<IConfigVisitor, long> val ) => Items.Where(i => has(i)).Select(i => i.Data.CastView(val(i.ToVal())));
}

