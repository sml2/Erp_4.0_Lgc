﻿using ERP.Interface;
using ERP.Models.Setting;
using ERP.Models.DB.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.Abstract;

using Models.DB.Users;
using Extensions;
[Index(nameof(GroupID))]
public abstract class GroupModel : CompanyModel
{

    public GroupModel() : base() { }
    public GroupModel(Group? userGroup, Company? company, OEM? oem) : base(company, oem) => Group = userGroup;
    public GroupModel(int? groupID, int? companyID, int? oemID) : base(companyID, oemID) => SetGroupID(groupID);
    public GroupModel(ISessionProvider sessionProvider) : this(sessionProvider.Session) { }
    public GroupModel(ISession? session) : base(session) => SetGroupID(session?.GetGroupID());
    public GroupModel(GroupModel g) : base(g) => SetGroupID(g.GroupID);
    public void SetGroupID(int? groupID) => GroupID = groupID.GetValueOrDefault();
    public void Set(GroupModel groupModel)
    {
        if (groupModel.GroupID == 0)
            throw new InvalidDataException($"{groupModel.GroupID} can not be zero");
        SetGroupID(groupModel.GroupID);
        base.Set(groupModel);
    }
    public override string ToString()
    {
        return $"GID={GroupID},{base.ToString()}";
    }
    public int GroupID { get; set; }
    
    [GraphQLIgnore]
    public Group? Group { get; set; }
}