﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.Models.Abstract;

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Extensions;
public abstract class BaseModel : IHasTimestamps
{
    [Comment("表索引")]
    [Key, GraphQLName("id")]
    public int ID { get; set; }
#if !SQILTE
    [Column(TypeName = "Timestamp")]
#endif
    [Comment("创建时间")]
    public virtual DateTime CreatedAt { get; set; } = DateTime.Now;
#if !SQILTE
    [Column(TypeName = "Timestamp")]
#endif
    [Comment("更新时间")]
    public virtual DateTime UpdatedAt { get; set; } = DateTime.Now;

    [NotMapped, GraphQLIgnore]
    public string UpdatedAtTicks => UpdatedAt.Ticks.ToString();

    public enum StateEnum
    {
        //Desc()
        None = 0,
        [Description("启用")]
        Open,
        [Description("禁用")]
        Close,
        [Description("未授权")]
        Disabled, 
        [Description("管理员操作禁用，下级用户无权开启")]
        AdminToDisable = 5
    }
}
