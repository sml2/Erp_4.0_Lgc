﻿using ERP.Models.DB.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
namespace ERP.Models.Abstract;
using Extensions;
using Interface;
using Models.Setting;
using ENUM = Enums.ID.Company;
using ENUMID = Enums.ID;
using Model = Company;

[Index(nameof(CompanyID))]
public abstract class CompanyModel : OEMModel
{
    public CompanyModel() : base() { }
    public CompanyModel(int? companyID) => SetCompanyID(companyID);
    public CompanyModel(ENUM c) => SetCompanyID(c);
    public CompanyModel(ENUM c, ENUMID.OEM o) : base(o) => SetCompanyID(c);
    public CompanyModel(Model? company, OEM? oem) : base(oem) => Company = company;
    public CompanyModel(int? companyID, int? oemID) : base(oemID) => SetCompanyID(companyID);
    public CompanyModel(ISessionProvider sessionProvider) : this(sessionProvider.Session) { }
    public CompanyModel(ISession? session) : base(session) => SetCompanyID(session?.GetCompanyID());
    public CompanyModel(CompanyModel c) : base(c) => SetCompanyID(c.CompanyID);
    public void SetCompanyID(int? companyID) => CompanyID = companyID.GetValueOrDefault();
    private void SetCompanyID(ENUM c) => SetCompanyID(c.GetID());
    public void Set(CompanyModel companyModel)
    {
        if (companyModel.CompanyID == 0)
            throw new InvalidDataException($"{companyModel.CompanyID} can not be zero");
        SetCompanyID(companyModel.CompanyID);
        base.Set(companyModel);
    }
    public override string ToString()
    {
        return $"CID={CompanyID},{base.ToString()}";
    }
    public int CompanyID { get; set; }
    [GraphQLIgnore]
    public Model? Company { get; set; }
}
