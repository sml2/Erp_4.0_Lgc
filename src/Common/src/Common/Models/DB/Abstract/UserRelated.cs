﻿using ERP.Models.DB.Identity;
using ERP.Models.Abstract;
using ERP.Models.DB.Users;
using ERP.Models.Setting;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.Models.Product;

public abstract class UserRelated : BaseModel
{
    public int UserID { get; set; }

    [ForeignKey(nameof(UserID))]
    public User User { get; set; } = default!;

    public int GroupID { get; set; }

    [ForeignKey(nameof(GroupID))]
    public Group UserGroupModel { get; set; } = default!;

    public int CompanyID { get; set; }

    [ForeignKey(nameof(CompanyID))]
    public Company Company { get; set; } = default!;

    public int OEMID { get; set; }

    [ForeignKey(nameof(OEMID))]
    public OEM OEM { get; set; } = default!;
}