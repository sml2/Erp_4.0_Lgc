﻿
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
namespace ERP.Models.Abstract;
using Interface;
using Models.Setting;
using Extensions;
using ENUM = Enums.ID.OEM;
[Index(nameof(OEMID))]
public abstract class OEMModel : BaseModel
{
    public OEMModel() { }
    public OEMModel(OEM? oem) => OEM = oem;
    public OEMModel(int? id) => OEMID = id.GetValueOrDefault();
    public OEMModel(ENUM o) : this(o.GetID()) { }
    public OEMModel(ISessionProvider sessionProvider) : this(sessionProvider.Session) { }
    public OEMModel(ISession? session) : this(session?.GetOEMID()) { }
    public OEMModel(OEMModel o) : this(o.OEMID) { }
    public void Set(OEMModel OEMModel)
    {
        if (OEMModel.OEMID == 0)
            throw new InvalidDataException($"{OEMModel.OEMID} can not be zero");
        OEMID = OEMModel.OEMID;
    }
    public override string ToString() => $"OID={OEMID},{base.ToString()}";

    public int OEMID { get; set; }
    [GraphQLIgnore]
    public OEM? OEM { get; set; }
}
