
namespace ERP.Models.Abstract;

using Microsoft.EntityFrameworkCore;
using Extensions;
using Interface;
using Models.DB.Identity;
using Models.DB.Users;
using Models.Setting;

[Index(nameof(UserID))]
public abstract class UserModel : GroupModel
{
    public UserModel() : base() { }
    public UserModel(User? user, Group? userGroup, Company? company, OEM? oem) : base(userGroup,company, oem) => User = user;
    public UserModel(int? UID,int? groupID, int? companyID, int? oemID) : base(groupID,companyID, oemID) => SetUserID(UID);
    public UserModel(ISessionProvider sessionProvider) : this(sessionProvider.Session) { }
    public UserModel(ISession? session) : base(session) => SetUserID(session?.GetUserID());
    public UserModel(UserModel u) : base(u) => SetUserID(u.UserID);
    public void SetUserID(int? UID) => UserID = UID.GetValueOrDefault();
    public void Set(UserModel userModel) {
        if (userModel.UserID == 0)
            throw new InvalidDataException($"{userModel.UserID} can not be zero");
        SetUserID(userModel.UserID);
        base.Set(userModel);
    }
    public override string ToString()=>$"UID={UserID},{base.ToString()}";
    public int UserID {  get; set; }
    public User? User { get; set; }
}