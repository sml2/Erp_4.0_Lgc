﻿using System.ComponentModel.DataAnnotations.Schema;
using ERP.Interface.Rule;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace ERP.Models.DB.Users;

using Abstract;
using Authorization;
using Enums.Rule;
using ERP.Attributes.Rule;
using ERP.Data;
using ERP.Enums.Identity;
using Extensions;
using Models.DB.Rule;
using Config = Enums.Rule.Config;
using Menu = Enums.Rule.Menu;
using Permission = Enums.Rule.Permission;
using RULE = Attributes.Rule.RuleAttribute;

[Comment("业务角色模板")]
[Index(nameof(Sort)), Index(nameof(State))]
public class RuleTemplate : CompanyModel
{
    public RuleTemplate()
    {
    }

    public RuleTemplate(IOrderedEnumerable<ERP.Models.DB.Setting.Menu> list)
    {
        var ul = Convert.ToInt64(Flag.BASE);
        foreach (var item in list)
        {
            this.SetValue(item.Module, ul);
        }
    }

    public RuleTemplate(Flag flag)
    {
        throw new NotImplementedException();
    }

    /// <summary>
    /// 名称
    /// </summary>
    [Comment("名称")]
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// 备注信息
    /// </summary>
    [Comment("备注信息")]
    public string? Remark { get; set; }

    /// <summary>
    /// 状态1启用2禁用
    /// </summary>
    [Comment("状态1启用2禁用")]
    public StateEnum State { get; set; }

    /// <summary>
    /// 排序字段
    /// </summary>
    [Comment("排序字段")]
    public int Sort { get; set; }


    /// <summary>
    /// 用于User创建时生成用户权限
    /// </summary>
    /// <returns></returns>
    private Dictionary<string, long> GetData()
    {
        Dictionary<string, long> dic = new();
        foreach (var Propertie in GetType().GetProperties())
        {
            if (Propertie is not null)
            {
                var Attribute = Propertie.GetCustomAttributes(typeof(RULE), false).FirstOrDefault() as RULE;
                var Value = Propertie.GetValue(this);
                if (Attribute is not null && Value is not null)
                {
                    var PropName = Propertie.Name;
                    switch (Attribute.Type)
                    {
                        case Types.MENU:
                        case Types.PERMISSION:
                            dic.Add(PropName, Convert.ToInt64(Value));
                            break;
                        case Types.CONFIG:
                            var VVisitor = $"{PropName}.{nameof(IConfig.Visible)}";
                            var EVisitor = $"{PropName}.{nameof(IConfig.Editable)}";
                            dic.Add(VVisitor, this.GetValue(VVisitor));
                            dic.Add(EVisitor, this.GetValue(EVisitor));
                            break;
                        default:
                            throw new ArgumentOutOfRangeException(nameof(Attribute.Type));
                    }
                }
            }
        }

        return dic;
    }

    public IEnumerable<Claim> GetClaims(int OemID)
    {
        List<Claim> lst = new();
        var items = GetData();
        foreach (var item in items)
        {
            lst.Add(new Claim(item.Key, item.Value) { OemId = OemID });
        }

        return lst;
    }

    #region "Rule = Permission + Config" //=========================================

    /// <summary>
    /// 模块 迁移至每个模块的第一个位
    /// </summary>
    //[Comment("模块")] public long Modules { get; set; }

    /// <summary>
    /// 电商平台[权限]
    /// </summary>
    [Comment("电商平台[权限]"), RULE(Types.PERMISSION), Seed(Role.DEV | Role.SU)]
    public Permission.ElectronicBusiness ElectronicBusiness { get; set; }

    /// <summary>
    /// 物流平台[权限]
    /// </summary>
    [Comment("物流平台[权限]"), RULE(Types.PERMISSION), Seed(Role.DEV | Role.SU)]
    public Permission.Logistics Logistics { get; set; }

    /// <summary>
    /// 导出模板[权限]
    /// </summary>
    [Comment("导出模板[权限]"), RULE(Types.PERMISSION), Seed(Role.DEV | Role.SU)]
    public Permission.Export Export { get; set; }

    /// <summary>
    /// 分销[菜单]
    /// </summary>
    [Comment("分销[菜单]"), RULE(Types.MENU), Seed(Role.DEV | Role.SU)]
    public Menu.Distribution DistributionMenu { get; set; }

    /// <summary>
    /// 分销[配置项]
    /// </summary>
    [Comment("分销"), RULE(Types.CONFIG), Seed(Role.DEV | Role.SU)]
    public Config<Config.Distribution> Distribution { get; set; } = null!;

    /// <summary>
    /// 财务[菜单]
    /// </summary>
    [Comment("财务[菜单]"), RULE(Types.MENU), Seed(Role.DEV | Role.SU)]
    public Menu.Finance FinanceMenu { get; set; }

    /// <summary>
    /// 财务[配置项]
    /// </summary>
    [Comment("财务[配置项]"), RULE(Types.CONFIG), Seed(Role.DEV | Role.SU)]
    public Config<Config.Finance> Finance { get; set; } = null!;

    /// <summary>
    /// 消息,工单[菜单]
    /// </summary>
    [Comment("消息[菜单]"), RULE(Types.MENU), Seed(Role.DEV | Role.SU)]
    public Menu.Message MessageMenu { get; set; }

    /// <summary>
    /// 消息,工单[配置项]
    /// </summary>
    [Comment("消息[配置项]"), RULE(Types.CONFIG), Seed(Role.DEV | Role.SU)]
    public Config<Config.Message> Message { get; set; } = null!;

    /// <summary>
    /// 订单[菜单]
    /// </summary>
    [Comment("订单[菜单]"), RULE(Types.MENU), Seed(Role.DEV | Role.SU)]
    public Menu.Order OrderMenu { get; set; }

    /// <summary>
    /// 订单[配置项]
    /// </summary>
    [Comment("订单[配置项]"), RULE(Types.CONFIG), Seed(Role.DEV | Role.SU)]
    public Config<Config.Order> Order { get; set; } = null!;

    /// <summary>
    /// 产品[菜单]
    /// </summary>
    [Comment("产品[菜单]"), RULE(Types.MENU), Seed(Role.DEV | Role.SU)]
    public Menu.Product ProductMenu { get; set; }

    /// <summary>
    /// 产品编辑本身[配置项]
    /// </summary>
    [Comment("产品编辑本身[配置项]"), RULE(Types.CONFIG), Seed(Role.DEV | Role.SU)]
    public Config<Config.Product> Product { get; set; } = null!;

    /// <summary>
    /// 产品周边相关[配置项]
    /// </summary>
    [Comment("产品周边相关[配置项]"), RULE(Types.CONFIG), Seed(Role.DEV | Role.SU)]
    public Config<Config.ProductAbout> ProductAbout { get; set; } = null!;

    /// <summary>
    /// 采购[菜单]
    /// </summary>
    [Comment("采购[菜单]"), RULE(Types.MENU), Seed(Role.DEV | Role.SU)]
    public Menu.Purchase PurchaseMenu { get; set; }

    /// <summary>
    /// 采购[配置项]
    /// </summary>
    [Comment("采购[配置项]"), RULE(Types.CONFIG), Seed(Role.DEV | Role.SU)]
    public Config<Config.Purchase> Purchase { get; set; } = null!;

    /// <summary>
    /// 采购[菜单]
    /// </summary>
    [Comment("运维[菜单]"), RULE(Types.MENU), Seed(Role.DEV | Role.SU)]
    public Menu.Operations OperationsMenu { get; set; }

    /// <summary>
    /// 采购[配置项]
    /// </summary>
    [Comment("运维[配置项]"), RULE(Types.CONFIG), Seed(Role.DEV | Role.SU)]
    public Config<Config.Operations> Operations { get; set; }

    /// <summary>
    /// 设置[菜单]
    /// </summary>
    [Comment("设置[菜单]"), RULE(Types.MENU), Seed(Role.DEV | Role.SU)]
    public Menu.Setting SettingMenu { get; set; }

    /// <summary>
    /// 设置[配置项]
    /// </summary>
    [Comment("设置[配置项]"), RULE(Types.CONFIG), Seed(Role.DEV | Role.SU)]
    public Config<Config.Setting> Setting { get; set; } = null!;

    /// <summary>
    /// 仓储[菜单]
    /// </summary>
    [Comment("仓储[菜单]"), RULE(Types.MENU), Seed(Role.DEV | Role.SU)]
    public Menu.Storage StorageMenu { get; set; }

    /// <summary>
    /// 仓储[配置项]
    /// </summary>
    [Comment("仓储[配置项]"), RULE(Types.CONFIG), Seed(Role.DEV | Role.SU)]
    public Config<Config.Storage> Storage { get; set; } = null!;

    /// <summary>
    /// 店铺[菜单]
    /// </summary>
    [Comment("店铺[菜单]"), RULE(Types.MENU), Seed(Role.DEV | Role.SU)]
    public Menu.Store StoreMenu { get; set; }

    /// <summary>
    /// 店铺[配置项]
    /// </summary>
    [Comment("店铺[配置项]"), RULE(Types.CONFIG), Seed(Role.DEV | Role.SU)]
    public Config<Config.Store> Store { get; set; } = null!;

    /// <summary>
    /// 用户[菜单]
    /// </summary>
    [Comment("用户[菜单]"), RULE(Types.MENU), Seed(Role.DEV | Role.SU)]
    public Menu.User UserMenu { get; set; }

    /// <summary>
    /// 用户[配置项]
    /// </summary>
    [Comment("用户[配置项]"), RULE(Types.CONFIG), Seed(Role.DEV | Role.SU)]
    public Config<Config.User> User { get; set; } = null!;

    /// <summary>
    /// 运单[菜单]
    /// </summary>
    [Comment("店铺[菜单]"), RULE(Types.MENU), Seed(Role.DEV | Role.SU)]
    public Menu.Waybill WaybillMenu { get; set; }

    /// <summary>
    /// 运单[配置项]
    /// </summary>
    [Comment("运单[配置项]"), RULE(Types.CONFIG), Seed(Role.DEV | Role.SU)]
    public Config<Config.Waybill> Waybill { get; set; } = null!;

    /// <summary>
    /// B2C[菜单]
    /// </summary>
    [Comment("B2C[菜单]"), RULE(Types.MENU), Seed(Role.DEV | Role.SU)]
    public Menu.B2C B2CMenu { get; set; }

    [Comment("B2C[配置项]"), RULE(Types.CONFIG), Seed(Role.DEV | Role.SU)]
    public Config<Config.B2C> B2C { get; set; } = null!;

    /// <summary>
    /// 运营工具[菜单]
    /// </summary>
    [Comment("运营工具[菜单]"), RULE(Types.MENU), Seed(Role.DEV | Role.SU)]
    public Menu.Tools  ToolsMenu { get; set; }

    [Comment("智能跟卖[配置项]"), RULE(Types.CONFIG), Seed(Role.DEV | Role.SU)]
    public Config<Config.Tools> Tools { get; set; } = null!;

    #endregion
}