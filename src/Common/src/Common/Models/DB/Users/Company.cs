using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Enums.Products;
using ERP.Attributes;

namespace ERP.Models.DB.Users;
using Abstract;
using Interface;
using Extensions;
using EMUM = Enums.ID.Company;
using EnumOEM = Enums.ID.OEM;
using ERPEnumExtension = Extensions.EnumExtension;
using Newtonsoft.Json;

[Index(nameof(Name), nameof(CreatIpHashCode), nameof(ParentCompanyId), nameof(Validity), nameof(ReferrerId), nameof(ReferrerKwSign), nameof(ReportId), nameof(ShowOemId), nameof(State), nameof(IsAgency))]
public class Company : OEMModel, IStatistic
{
    public Company() { }
    public Company(EMUM c, EnumOEM o, EMUM p) : base(o)
    {
        ID = c.GetID();
        Name = c.GetDescription();
        ParentCompanyId = p.GetID();
    }

    /// <summary>
    /// 父级公司id
    /// </summary>
    [Comment("父级公司id")]
    public int ParentCompanyId { get; set; }

    /// <summary>
    /// 公司名称
    /// </summary>
    [Comment("公司名称")]
    public string Name { get; set; } = string.Empty;


    /// <summary>
    /// 分销上报公司id
    /// </summary>
    [Comment("分销上报公司id")]
    public int ReportId { get; set; }

    /// <summary>
    /// 上报公司上级所有公司ids
    /// </summary>
    [Comment("上报公司上级所有公司ids")]
    public string ReportPids { get; set; } = string.Empty;
    
    [Column(TypeName = "varchar(255)"), Comment("创建公司过程")]
    
    public string Pids { get; set; } = string.Empty;

    /// <summary>
    /// 代理标识 1是 2否
    /// </summary>
    [Comment("代理标识 1是 0否")]
    public bool IsAgency { get; set; } = false;

    public enum States
    {
        [Description("正常")]
        ENABLE = 1,

        [Description("禁止")]
        DISABLE = 2,
        
        [Description("管理员操作禁用，下级用户无权开启")]
        AdminToDisable = 5
    }

    /// <summary>
    /// 公司状态1正常2禁止
    /// </summary>
    [Comment("公司状态1正常2禁止")]
    public States State { get; set; }

    /// <summary>
    /// 是否允许修改公司
    /// </summary>
    [Comment("是否允许修改公司名1是2否")]
    public bool IsChangeCompany { get; set; }

    /// <summary>
    /// 是否允许店铺无限
    /// </summary>
    [Comment("是否允许店铺无限")]
    public bool IsOpenStore { get; set; }

    /// <summary>
    /// 允许开店数量 
    /// </summary>
    [Comment("允许开店数量")]
    public int OpenStoreNum { get; set; }

    /// <summary>
    /// 是否允许子账号无限开
    /// </summary>
    [Comment("是否允许子账号无限开")]
    public bool IsOpenUser { get; set; }

    /// <summary>
    /// 允许子账号开通数量
    /// </summary>
    [Comment("允许子账号开通数量")]
    public int OpenUserNum { get; set; }

    /// <summary>
    /// 是否子公司展示费率
    /// </summary>
    [Comment("是否子公司展示费率")]
    public bool IsShowRate { get; set; }

    /// <summary>
    /// 余额(基准货币) ???
    /// </summary>
    [Comment("余额(基准货币)")]
    public decimal Balance { get; set; }
    
    [NotMapped, GraphQLIgnore]
    public Money BalanceMoney
    {
        get => new Money(Balance);
    }

    [NotMapped, GraphQLIgnore]
    public decimal BalanceStr => BalanceMoney.To("CNY");


    /// <summary>
    /// 对账钱包(基准货币) ???
    /// </summary>
    [Comment("对账钱包(基准货币)"), GraphQLIgnore]
    public MoneyRecordFinancialAffairs PublicWallet { get; set; } = MoneyRecordFinancialAffairs.Empty;
    
    [JsonMoneyForDefaultUnit,NotMapped, GraphQLIgnore]
    public Money PublicWalletUi => PublicWallet.Money;

    /// <summary>
    /// 自用钱包(基准货币) ???
    /// </summary>
    [Comment("自用钱包(基准货币)"), GraphQLIgnore]
    public MoneyRecordFinancialAffairs PrivateWallet { get; set; } = MoneyRecordFinancialAffairs.Empty;

    /// <summary>
    /// 是否分销
    /// </summary>
    [Comment("是否分销")]
    public bool IsDistribution { get; set; }

    [GraphQLName("CompanyTypes")]
    public enum Types
    {
        [Description("业务用户")]
        ONE = 1,

        [Description("注册用户")]
        TWO = 2,

        // [Description("个人版用户(月付)")]
        // THREE = 3,
        //
        // [Description("个人版用户(年付)")]
        // FOUR = 4,
        //
        // [Description("团队版用户")]
        // FIVE = 5,
        //
        // [Description("企业版用户")]
        // SIX = 6
    }

    /// <summary>
    /// 账户类型 1业务用户 2注册免费用户 3个人版用户(月付) 4个人版用户(年付) 5团队版用户 6企业版用户
    /// </summary>
    [Comment("账户类型 1业务用户")]
    public Types Type { get; set; }

    public enum Sources
    {
        [Description("普通用户")]
        ONE = 1,

        [Description("积分用户")]
        TWO = 2
    }

    /// <summary>
    /// 用户来源(1普通用户 2积分用户)
    /// </summary>
    [Comment("用户来源(1普通用户 2积分用户)")]
    public Sources Source { get; set; }

    /// <summary>
    /// 推荐人公司id
    /// </summary>
    [Comment("推荐人公司id")]
    public int ReferrerId { get; set; }

    /// <summary>
    /// 推荐人数
    /// </summary>
    [Comment("推荐人数")]
    public int ReferrerNum { get; set; }

    /// <summary>
    /// 返现比例
    /// </summary>
    [Comment("返现比例")]
    public int Discount { get; set; }

    /// <summary>
    /// 积分
    /// </summary>
    [Comment("积分")]
    public int Integral { get; set; }

    /// <summary>
    /// 推荐关键字代码
    /// </summary>
    [Comment("推荐关键字代码")]
    public string? ReferrerKwSign { get; set; }

    /// <summary>
    /// 推荐关键字
    /// </summary>
    [Comment("推荐关键字")]
    public int ReferrerKwName { get; set; }

    /// <summary>
    /// 首次充值时间
    /// </summary>
    [Comment("首次充值时间")]
    public DateTime? FirstRechargeDate { get; set; }

    /// <summary>
    /// 首次充值是否完成
    /// </summary>
    [Comment("首次充值是否完成")]
    public bool FirstRecharge { get; set; }

    /// <summary>
    /// 是否贴牌注册二代套餐用户
    /// </summary>
    [Comment("是否贴牌注册二代套餐用户")]
    public bool SecondPackageOem { get; set; }

    /// <summary>
    /// 对公钱包是否为负, true 可为负， false 不可为负
    /// <remarks>仅针对对公钱包，个人钱包不需要此字段判断</remarks>
    /// </summary>
    [Comment("对公钱包是否为负")]
    public bool IsNegative { get; set; }

    public enum DistributionPurchases
    {
        [Description("全部上报")]
        TRUE = 1,

        [Description("选择上报")]
        PORTION = 2,

        [Description("部分上报")]
        FALSE = 3
    }

    /// <summary>
    /// 分销公司采购上报类型 1全部上报 2选择上报 3不上报
    /// </summary>
    [Comment("分销公司采购上报类型 1全部上报 2选择上报 3不上报")]
    public DistributionPurchases DistributionPurchase { get; set; }

    public enum DistributionWaybills
    {
        [Description("全部上报")]
        TRUE = 1,

        [Description("选择上报")]
        PORTION = 2,

        [Description("部分上报")]
        FALSE = 3
    }

    /// <summary>
    /// 分销公司物流上报类型 1全部上报 2选择上报 3不上报
    /// </summary>
    [Comment("分销公司物流上报类型 1全部上报 2选择上报 3不上报")]
    public DistributionWaybills DistributionWaybill { get; set; }

    public enum DistributionOrders
    {
        [Description("全部上报")]
        TRUE = 1,

        [Description("选择上报")]
        PORTION = 2,

        [Description("部分上报")]
        FALSE = 3
    }

    /// <summary>
    /// 分销公司物流上报类型 1全部上报 2选择上报 3不上报
    /// </summary>
    [Comment("分销公司合并订单上报类型 1全部上报 2选择上报 3不上报")]
    public DistributionOrders DistributionOrder { get; set; }

    /// <summary>
    /// UI展示 OEM
    /// </summary>
    [Comment("UI展示 OEM")]
    public int ShowOemId { get; set; }
    
    /// <summary>
    /// 账户到期时间
    /// </summary>
    [Comment("账户到期时间")]
    public DateTime Validity { get; set; }

    // /// <summary>
    // /// 真实有效期时间id
    // /// </summary>
    // [Comment("真实有效期时间id")]
    // public int RealityValidityId { get; set; }
    //
    // /// <summary>
    // /// 真实到期时间戳
    // /// </summary>
    // [Comment("真实到期时间戳")]
    // public DateTime RealityValidity { get; set; }
    //
    // /// <summary>
    // /// 展示有效期时间id
    // /// </summary>
    // [Comment("展示有效期时间id")]
    // public int ShowValidityId { get; set; }
    // /// <summary>
    // /// 展示到期时间
    // /// </summary>
    // [Comment("展示到期时间戳")]
    // public DateTime ShowValidity { get; set; }
    /// <summary>
    /// 展示内容
    /// </summary>
    [GraphQLIgnore]
    public string ShowValidityInfo { get => Validity == DateTime.MaxValue ? "无限制" : Validity.ToString("yyyy-MM-dd"); }//"infinite"



    /// <summary>
    /// 采购手续费率
    [Comment("采购手续费率")]
    public int PurchaseRate { get; set; }

    /// <summary>
    /// 采购手续费类型1百分比2固定数值
    /// </summary>
    [Comment("采购手续费类型1百分比2固定数值")]
    public int PurchaseRateType { get; set; } = 1;

    [Comment("采购手续费固定值")]
    public int PurchaseRateNum { get; set; }

    /// <summary>
    /// 采购手续费固定值货币单位(暂时舍弃)
    /// </summary>
    [Comment("采购手续费固定值货币单位(暂时舍弃)")]
    public int PurchaseRateUnit { get; set; }

    /// <summary>
    /// 运费手续费率
    /// </summary>
    [Comment("运费手续费率")]
    public int WaybillRate { get; set; }

    /// <summary>
    /// 运费手续费类型1百分比2固定数值
    /// </summary>
    [Comment("运费手续费类型1百分比2固定数值")]
    public int WaybillRateType { get; set; } = 1;

    /// <summary>
    /// 运费手续费固定值
    /// </summary>
    [Comment("运费手续费固定值")]
    public int WaybillRateNum { get; set; }

    /// <summary>
    /// 运费手续费固定值货币单位(暂时舍弃)
    /// </summary>
    [Comment("运费手续费固定值货币单位(暂时舍弃)")]
    public int WaybillRateUnit { get; set; }

    public enum ProductAudits
    {
        [Description("产品需要审核")]
        YES = 1,

        [Description("产品不需要审核")]
        NO = 2
    }

    /// <summary>
    /// 产品审核(1是2否)
    /// </summary>
    [Comment("产品审核(1是2否)")]
    public ProductAudits ProductAudit { get; set; } = ProductAudits.NO;
    
    public enum ProductTranslationEnum
    {
        [Description("免费接口")]
        Free = 1,

        [Description("付费接口")]
        Pay = 2
    }

    /// <summary>
    /// 产品翻译(1免费接口2付费接口)
    /// </summary>
    [Comment("产品翻译(1是2否)")]
    public ProductTranslationEnum ProductTranslation { get; set; } = ProductTranslationEnum.Free;

    public DescriptionTypeEnum ProductEditor { get; set; } = DescriptionTypeEnum.PlainText;

    public enum PurchaseAudits
    {
        [Description("采购需要审核")]
        YES = 1,

        [Description("采购不需要审核")]
        NO = 2
    }

    /// <summary>
    /// 采购审核(1是2否)
    /// </summary>
    [Comment("采购审核(1是2否)")]
    public PurchaseAudits PurchaseAudit { get; set; } = PurchaseAudits.NO;

    public enum WaybillAudits
    {
        [Description("物流需要审核")]
        YES = 1,

        [Description("物流不需要审核")]
        NO = 2
    }

    /// <summary>
    /// 物流审核(1是2否)
    /// </summary>
    [Comment("物流审核(1是2否)")]
    public WaybillAudits WaybillAudit { get; set; } = WaybillAudits.NO;

    public enum BillAudits
    {
        [Description("账单需要审核")]
        YES = 1,

        [Description("账单不需要审核")]
        NO = 2
    }

    /// <summary>
    /// 账单审核(1是2否)
    /// </summary>
    [Comment("账单审核(1是2否)")]
    public BillAudits BillAudit { get; set; } = BillAudits.YES;

    public enum RefundAudits
    {
        [Description("退款需要审核")]
        YES = 1,

        [Description("退款不需要审核")]
        NO = 2
    }

    /// <summary>
    /// 订单退款审核(1是2否)
    /// </summary>
    [Comment("订单退款审核(1是2否)")]
    public RefundAudits RefundAudit { get; set; } = RefundAudits.YES;

    /// <summary>
    /// 仓库审核(1是2否)
    /// </summary>
    [Comment("仓库审核(1是2否)")]
    public int StorageAudit { get; set; } = 2;

    public enum WaybillConfirms
    {
        [Description("运单需要审核")]
        YES = 1,

        [Description("运单不需要审核")]
        NO = 2
    }

    /// <summary>
    /// 运单自动确认(1是2否)
    /// </summary>
    [Comment("运单自动确认(1是2否)")]
    public WaybillConfirms WaybillConfirm { get; set; } = WaybillConfirms.YES;

    public enum AllowPhones
    {
        [Description("不允许员工修改手机号")]
        NO = 0,
        [Description("允许员工修改手机号")]
        YES = 1
    }

    /// <summary>
    /// 允许员工修改手机号(1是2否)
    /// </summary>
    [Comment("允许员工修改手机号(1是0否)")]
    public AllowPhones AllowPhone { get; set; } = AllowPhones.YES;

    public enum LoginPhones
    {
        [Description("需要登录手机验证")]
        YES = 1,

        [Description("不需要登录手机验证")]
        NO = 2
    }

    /// <summary>
    /// 登录手机验证(1是2否)
    /// </summary>
    [Comment("登录手机验证(1是2否)")]
    public LoginPhones LoginPhone { get; set; } = LoginPhones.NO;

    /// <summary>
    /// 主题id
    /// </summary>
    [Comment("主题id")]
    public int ThemeId { get; set; }

    /// <summary>
    /// 创建ip
    /// </summary>
    [Comment("创建ip")]
    public string? CreatIp { get; set; }

    /// <summary>
    /// 创建ip hashcode
    /// </summary>
    [Comment("创建ip hashcode"), GraphQLIgnore]
    public ulong CreatIpHashCode { get; set; }

    public enum ExtendTips
    {
        /** @var int  */
        [Description("推荐提醒关闭")]
        FALSE = 1,

        [Description("推荐提醒弹窗 ")]
        TRUE = 2
    }


    /// <summary>
    /// 推荐提醒1关闭2弹窗
    /// </summary>
    [Comment("推荐提醒1关闭2弹窗")]
    public ExtendTips ExtendTip { get; set; } = ExtendTips.TRUE;

    /// <summary>
    /// 扣费额度
    /// </summary>
    [Comment("扣费额度")]
    public string? PriceConfig { get; set; }

    /// <summary>
    /// 配置默认下级扣费额度
    /// </summary>
    [Comment("配置默认下级扣费额度")]
    public string? DefaultPriceConfig { get; set; }
    public DateTime LastCreateStatistic { get; set; }
    
    /// <summary>
    /// 对公钱包是否可冻结
    /// </summary>
    [Comment("对公钱包是否可冻结")]
    public bool PublicWalletCanFreeze { get; set; }
    
    /// <summary>
    /// 每笔运单默认冻结金额
    /// </summary>
    [Comment("每笔运单默认冻结金额"), GraphQLIgnore]
    public Money FreezeAmountPerWaybill { get; set; }
    
    [Comment("运单冻结总金额"), GraphQLIgnore]
    public Money FreezeTotalAmountWaybill { get; set; }

    public int GetOpenStoreNum(int count)
    {
        int num=0;
        if (!IsOpenStore)
        {
             num = OpenStoreNum - count;
            if (num <= 0)
            {
                return 0;
            }
        }
        return num;
        //默认一次性可开通店铺100
        //var num = 100;
        //if (!IsOpenStore)
        //{
        //    //if (OpenStoreNum == 0)
        //    //{
        //    //    return 0;
        //    //}

        //    //当前公司下店铺数
        //    num = OpenStoreNum - count;
        //    if (num <= 0)
        //    {
        //        return 0;
        //    }
        //}

        //return num;
    }

    /// <summary>
    /// 判断对公钱包是否足够
    /// </summary>
    /// <param name="money"></param>
    /// <returns></returns>
    public bool CheckPublicWalletEnough(decimal money)
    {
        return !(!IsNegative && PublicWallet.Money >= money);
    }

    //, Types.THREE, Types.FOUR, Types.FIVE, Types.SIX
    private static readonly Types[] TypesSupports = { Types.ONE, Types.TWO };



    // 获取所有账户类型
    public static Dictionary<int, string> GetAllTypes()
    {
        return TypesSupports.ToDictionary(type => (int)type, type => type.GetDescription());
    }
}
