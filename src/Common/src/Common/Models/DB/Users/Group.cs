﻿
using ERP.Interface;
using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.DB.Users;
using Extensions;
using ENUM=Enums.ID.Group;
using Enums.ID;
[Index(nameof(OEMID))]
[Index(nameof(Sort))]
[Index(nameof(State))]
public class Group : CompanyModel, IStatistic
{
    public Group() { }
    public Group(ENUM g, Enums.ID.Company c, OEM o):base(c,o)
    {
        ID = g.GetID();
        Name = g.GetDescription();
    }

    [Comment("名称")]
    public string Name { get; set; } = string.Empty;

    [Comment("状态1启用2禁用")]
    public StateEnum State { get; set; } = StateEnum.Open;
    public DateTime LastCreateStatistic { get; set; }
    [Comment("排序字段")]
    // public int Sort { get; set; } =  -DateTime.Now.GetTimeStamp();
    public int Sort { get; set; } =  -(new DateTime(2021, 11, 19, 13, 38, 0)).GetTimeStamp();

}


//-- ----------------------------
//-- Table structure for erp3_group
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_group`;
//CREATE TABLE `erp3_group`  (
//  `id` bigint unsigned NOT NULL,
//  `d_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '名称',
//  `t_state` tinyint unsigned NOT NULL COMMENT '状态1启用2禁用',
//  `d_sort` int (0) NOT NULL DEFAULT 0 COMMENT '排序字段',
//  `i_oem_id` int unsigned NOT NULL COMMENT 'OEM ID',
//  `i_company_id` bigint unsigned NOT NULL COMMENT '公司ID',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `sort`(`d_sort`) USING BTREE,
// INDEX `state`(`t_state`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 11566 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户管理_用户组' ROW_FORMAT = Dynamic;