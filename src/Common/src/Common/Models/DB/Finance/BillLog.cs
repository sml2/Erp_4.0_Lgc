using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ERP.Enums.Identity;
using ERP.Models.DB.Users;
using ERP.Models.Setting;
using Ryu.Data;
using User = ERP.Models.DB.Identity.User;

namespace ERP.Models.Finance;

[Index(nameof(Audit), nameof(Belong), nameof(Module), nameof(OperateCompanyId), nameof(StoreId), nameof(Type))]
public class BillLog : UserModel
{
    public BillLog()
    {
    }

    public BillLog(Money money, Types type, DateTime? datetime, string? remark, Audits audit, int userId,string? truename,
        int? auditUserId, string? auditTruename,int companyId, int groupId,int oemId, int creatCompanyId, int operateCompanyId, string? operateCompanyName,
        Modules module, int storeId, string? info)
    {
        Money = money;
        Type = type;
        Datetime = datetime;
        Remark = remark;
        Audit = audit;
        UserID = userId;
        Truename = truename;
        AuditUserId = auditUserId;
        AuditTruename = auditTruename;
        CompanyID = companyId;
        GroupID = groupId;
        OEMID = oemId;
        CreatCompanyId = creatCompanyId;
        OperateCompanyId = operateCompanyId;
        OperateCompanyName = operateCompanyName;
        Module = module;
        StoreId = storeId;
        Info = info;
    }

    /// <summary>
    /// 变动金额
    /// </summary>

    [Comment("变动金额")]
    public Money Money { get; set; }
    
    [Comment("余额")]
    public Money? Balance { get; set; }

    public enum Belongs
    {
        [Description("自用")]
        SELFUSE = 1,

        [Description("对公")]
        RIGHT = 2
    }

    /// <summary>
    /// 所属类型
    /// </summary>

    [Comment("所属类型")]
    public Belongs Belong { get; set; } = Belongs.SELFUSE;

    public enum Types
    {
        [Description("充值")]
        Recharge = 1,

        [Description("支出")]
        Deduction = 2
    }

    /// <summary>
    /// 充值扣款
    /// </summary>

    [Comment("充值扣款")]
    public Types Type { get; set; } = Types.Deduction;

    /// <summary>
    /// 账单时间
    /// </summary>

    [Comment("账单时间")]
    public DateTime? Datetime { get; set; }

    /// <summary>
    /// 账单原因
    /// </summary>

    [Comment("账单原因")]
    public string? Reason { get; set; }

    /// <summary>
    /// 备注
    /// </summary>

    [Comment("备注")]
    public string? Remark { get; set; }

    public enum Audits
    {
        [Description("待审核")]
        PendingReview = 1,

        [Description("已审核")]
        Audited = 2,

        [Description("未通过")]
        DidNotPass = 3,

        [Description("无需审核")]
        NoReviewRequired = 4
    }

    /// <summary>
    /// 审核状态
    /// </summary>

    [Comment("审核状态")]
    public Audits Audit { get; set; } // Audits核状态1待审核2已审核3未通过4无需审核

    /// <summary>
    /// 创建用户姓名
    /// </summary>

    [Comment("用户姓名")]
    public string? Truename { get; set; }

    /// <summary>
    /// 审核用户id
    /// </summary>

    [Comment("审核用户id")]
    public int? AuditUserId { get; set; }

    /// <summary>
    /// 审核用户姓名
    /// </summary>

    [Comment("审核用户姓名")]
    public string? AuditTruename { get; set; }

    /// </summary>

    [Comment("上报公司id")]
    public int ReportCompanyId { get; set; }

    /// <summary>
    /// 创建数据公司id
    /// </summary>

    [Comment("创建数据公司id")]
    public int CreatCompanyId { get; set; } //

    /// <summary>
    /// 操作公司id
    /// </summary>

    [Comment("'操作公司id")]
    public int OperateCompanyId { get; set; }

    /// <summary>
    /// 操作公司名称
    /// </summary>

    [Comment("操作公司名称")]
    public string? OperateCompanyName { get; set; }

    /// <summary>
    /// 上传凭证id
    /// </summary>

    [Comment("上传凭证id（连接）")]
    public string? Resource { get; set; }

    /// <summary>
    /// 资源url地址
    /// </summary>

    [Comment("资源url地址")]
    public string? ImgAll { get; set; }

    public enum Modules
    {
        [Description("通用")]
        INTERCHANGEABLE = 1,

        [Description("采购")]
        PURCHASE = 2,

        [Description("运单")]
        WAYBILL = 3,

        // [Description("订单退款")]
        // ORDER = 4,

        [Description("在线支付")]
        ONLINEPAY = 5,

        // [Description("订单手续费")]
        // FEE = 6,

        [Description("订单成本损失")]
        LOSS = 7
    }

    /// <summary>
    /// 账单类型
    /// </summary>

    [Comment("账单类型")]
    public Modules Module { get; set; }
    
    [Comment("数据id")]
    public int? ModuleId { get; set; }

    /// <summary>
    /// 卖家店铺id
    /// </summary>
    [Comment("卖家店铺id")]
    public int? StoreId { get; set; }

    /// <summary>
    /// 详情地址
    /// </summary>
    [Comment("详情地址")]
    public string? Info { get; set; }
    
    public IWallet GetWallet(Company company, User operateUser)
    {
        if (Belong == BillLog.Belongs.SELFUSE)
            return new SelfWallet(company, operateUser);

        return new PublicWallet(company, operateUser);
    }
}


//-- ----------------------------
//-- Table structure for erp3_bill_log
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_bill_log`;
//CREATE TABLE `erp3_bill_log`  (
//  `id` bigint unsigned NOT NULL,
//  `i_user_id` int unsigned NOT NULL COMMENT '用户id',
//  `i_company_id` int unsigned NOT NULL COMMENT '所属公司id(被扣款充值公司id)',
//  `i_group_id` int unsigned NOT NULL COMMENT '当前用户组id',
//  `i_oem_id` int unsigned NOT NULL COMMENT '当前Oem id',
//  `d_money` decimal (22, 6) NOT NULL DEFAULT 0.000000 COMMENT '变动金额',
//  `t_belong` tinyint unsigned NOT NULL COMMENT '1自用2对公',
//  `t_type` tinyint unsigned NOT NULL COMMENT '1充值2扣款',
//  `d_datetime` date NULL DEFAULT NULL COMMENT '账单时间',
//  `d_reason` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '账单原因',
//  `d_remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '备注',
//  `t_audit` tinyint unsigned NOT NULL COMMENT '审核状态1待审核2已审核3未通过4无需审核',
//  `u_truename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '创建用户姓名',
//  `i_audit_user_id` int unsigned NOT NULL COMMENT '审核用户id',
//  `u_audit_truename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '审核用户姓名',
//  `i_report_company_id` int unsigned NOT NULL COMMENT '上报公司id',
//  `i_creat_company_id` int unsigned NOT NULL COMMENT '创建数据公司id',
//  `i_operate_company_id` int unsigned NOT NULL COMMENT '操作公司id',
//  `u_operate_company_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作公司名称',
//  `i_resource` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '上传凭证id（，连接）',
//  `u_img_all` json NULL COMMENT '资源url地址',
//  `t_module` int unsigned NOT NULL COMMENT '使用类型1通用2采购3运单4订单退款5在线支付6订单手续费',
//  `i_module_id` int unsigned NOT NULL COMMENT '数据id',
//  `i_store_id` int unsigned NOT NULL COMMENT '卖家店铺id（平台-商店）',
//  `d_info_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '详情地址',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// INDEX `audit`(`t_audit`) USING BTREE,
// INDEX `belong`(`t_belong`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `module`(`t_module`) USING BTREE,
// INDEX `oem_id`(`i_oem_id`) USING BTREE,
// INDEX `operate_company_id`(`i_operate_company_id`) USING BTREE,
// INDEX `store_id`(`i_store_id`) USING BTREE,
// INDEX `type`(`t_type`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 81967 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '钱包管理_账单记录' ROW_FORMAT = Dynamic;