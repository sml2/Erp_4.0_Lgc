using ERP.Models.DB.Identity;
using ERP.Models.DB.Users;

namespace ERP.Models.Finance;

public class FreezeWaybill : AWallet
{
    private readonly Company _company;
    private readonly User _operateUser;
    private readonly Money _freeze;
    private readonly Money _totalFreeze;

    public FreezeWaybill(Company company, User operateUser)
    {
        _company = company;
        _operateUser = operateUser;
        _freeze = company.FreezeAmountPerWaybill;
        _totalFreeze = company.FreezeTotalAmountWaybill;
    }

    public Money GetFreeze()
    {
        return _freeze;
    }


    public override bool IsEnough(decimal money, out string message)
    {
        var wallet = _company.PublicWallet;
        var allowNegative = _company.IsNegative;
        var walletGreaterThanMoney = wallet.Money > (_freeze + _totalFreeze);

    var isEnough = allowNegative || walletGreaterThanMoney;
        message = isEnough ? string.Empty
            : _company.ID == _operateUser.CompanyID ? "当前余额不足" : "当前余额不足，请先提醒对方充值钱包金额";
                
        return isEnough;
    }


    public override BillLog.Belongs GetBelong() => BillLog.Belongs.RIGHT;

    public override Task Expense(decimal money)
    {
        _company.PublicWallet -= money;
        _company.FreezeTotalAmountWaybill += money;
        return Task.CompletedTask;
    }

    public override Task Recharge(decimal money)
    {
        _company.PublicWallet += money;
        _company.FreezeTotalAmountWaybill -= money;
        return Task.CompletedTask;
    }

    public override BillLog AddLog(decimal money, bool isRecharge, BillLog.Modules modules, string reason, string remark = "",
        int? storeId = null,
        int? moduleId = null)
    {
        var now = DateTime.Now;
        var billLog = new BillLog()
        {
            Money = money,
            Type = isRecharge ? BillLog.Types.Recharge : BillLog.Types.Deduction,
            Datetime = now,
            Remark = remark,
            Audit = BillLog.Audits.NoReviewRequired,
            UserID = _operateUser.Id,
            Truename = _operateUser.TrueName,
            AuditUserId = _operateUser.Id,
            AuditTruename = _operateUser.TrueName,
            CompanyID = _company.ID,
            GroupID = _operateUser.GroupID,
            OEMID = _company.OEMID,
            CreatCompanyId = _operateUser.CompanyID,
            OperateCompanyId = _operateUser.CompanyID,
            OperateCompanyName = _operateUser.Company.Name,
            Module = modules,
            StoreId = storeId ?? 0,
            Reason = reason,
            ReportCompanyId = _company.ReportId,
            Balance = _company.PublicWallet.Money,
            Belong = GetBelong(),
            ModuleId= moduleId ?? 0
        };
        return billLog;
    }
}