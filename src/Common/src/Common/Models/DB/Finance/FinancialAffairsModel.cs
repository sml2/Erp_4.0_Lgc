using ERP.Enums.Finance;
using ERP.Models.Abstract;
using ERP.Models.DB.Identity;
using ERP.Models.DB.Users;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.Finance;

[Index(nameof(SourceID))]
[Index(nameof(OEMID))]
[Index(nameof(CompanyID))]
[Index(nameof(GroupID))]
[Index(nameof(UserID))]
[Index(nameof(TableID),nameof(TableField))]
[Index(nameof(EffectiveTime))]
[Index(nameof(FailureTime))]
public class FinancialAffairsModel : CompanyModel
{
    public FinancialAffairsModel()
    {
    }

    public FinancialAffairsModel(MoneyRecord money, TableFieldEnum field, int? indexId, int? sourceId, string? reason,
        string? remark)
    {
        Value = money;
        TableField = field;
        TableID = indexId;
        Reason = reason;
        Remark = remark;
        SourceID = sourceId;
    }

    [Comment("金额")]
    public MoneyRecord Value { get; set; } = MoneyRecord.Empty;

    [Comment("生效时间")]
    public DateTime EffectiveTime { get; set; }

    [Comment("失效时间")]
    public DateTime FailureTime { get; set; } = DateTime.MaxValue;

    // [Comment("数据表名称")]
    // public string Table { get; set; }

    /// <summary>
    /// 业务表的ID
    /// </summary>
    /// <!--
    /// “问‘这个字段有什么意义’”本身没有意义
    /// 有意义是的是“‘探索’他有什么有意义”这件事就很有意义
    /// 问这个问题就如同本表继承UserModel -- 继承UserModel有意义嘛，写这条注释时本表尚未继承UserModel？
    /// 下意识的讲几条
    /// 1、运维:对运维有帮助，可以避免根据去业务表再进行一次 SELECT * FROM XTable Where Y = ID;【运维场景是可急迫的，财务数据是繁多且严谨的】
    /// 2、运算:可降低事务密集时SQL的时间复杂度，典型的用空间换时间的操作
    /// 3、调试:在一定程度上对财务数据数据进行闭环，相较于其他散落在各业务表的数据，本字段有数据直观说明事务走到了最后一步。
    /// 4、开发:增加了开发成本，居然多写了两行代码（SetID+SaveChange）！
    /// 5、运行:增加了一个>一个TTL的执行周期(10ms)
    /// 6、存储:增加存储空间(4Bytes)
    /// -->
    [Comment("数据表主键ID")]
    public int? TableID { get; set; } = 0;


    public TableFieldEnum TableField { get; set; }

    [Comment("原因")]
    public string? Reason { get; set; }

    [Comment("备注")]
    public string? Remark { get; set; }

    [Comment("原数据ID")]
    public int? SourceID { get; set; } = 0;

    public int? GroupID { get; set; }
    public Group? Group { get; set; }
    
    public int? UserID { get; set; }
    public User? User { get; set; }
}