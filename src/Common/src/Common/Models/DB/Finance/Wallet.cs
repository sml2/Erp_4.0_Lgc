using ERP.Models.DB.Identity;
using ERP.Models.Abstract;
using ERP.Models.DB.Users;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.Finance;

/// <summary>
/// 钱包接口
/// </summary>
public interface IWallet
{
    /// <summary>
    /// 判断钱包是否够扣
    /// </summary>
    /// <param name="money">金额基数</param>
    /// <returns>true足够 | false不够</returns>
    public bool IsEnough(decimal money);
    
    /// <inheritdoc cref="IsEnough(decimal)"/>
    /// <param name="message">钱包不够扣的原因, 够扣时为空</param>
    public bool IsEnough(decimal money, out string message);

    /// <summary>
    /// 获取钱包所属
    /// </summary>
    /// <returns></returns>
    public BillLog.Belongs GetBelong();

    /// <summary>
    /// 钱包支出
    /// </summary>
    /// <param name="money"></param>
    /// <returns></returns>
    public Task Expense(decimal money);
    
    /// <summary>
    /// 钱包充值
    /// </summary>
    /// <param name="money"></param>
    /// <returns></returns>
    public Task Recharge(decimal money);

    /// <summary>
    /// 添加财务日志
    /// </summary>
    /// <param name="money"></param>
    /// <param name="isRecharge">true充值 | false支出</param>
    /// <param name="reason"></param>
    /// <param name="modules"></param>
    /// <param name="remark"></param>
    /// <param name="storeId"></param>
    /// <param name="moduleId"></param>
    /// <returns></returns>
    public BillLog AddLog(decimal money, bool isRecharge, BillLog.Modules modules, string reason, string remark = "", int? storeId = null,int? moduleId = null);
}

public abstract class AWallet : IWallet
{
    private IWallet _walletImplementation;
    public bool IsEnough(decimal money) => IsEnough(money, out _);
    
    public abstract bool IsEnough(decimal money, out string message);

    public abstract BillLog.Belongs GetBelong();
    public abstract Task Expense(decimal money);
    public abstract Task Recharge(decimal money);
    public abstract BillLog AddLog(decimal money, bool isRecharge, BillLog.Modules modules, string reason, string remark = "", int? storeId = null,int? moduleId = null);
}

/// <summary>
/// 个人钱包
/// </summary>
public sealed class SelfWallet : AWallet
{
    private readonly Company _company;
    private readonly User _operateUser;

    public SelfWallet(Company company, User operateUser)
    {
        _company = company;
        _operateUser = operateUser;
    }
    
    public override bool IsEnough(decimal money, out string message)
    {
        message = string.Empty;
        return true; // 永远够扣
    }

    public override BillLog.Belongs GetBelong() => BillLog.Belongs.SELFUSE;
    public override Task Expense(decimal money)
    {
        _company.PrivateWallet -= money;
        return Task.CompletedTask;
    }

    public override Task Recharge(decimal money)
    {
        _company.PrivateWallet += money;
        return Task.CompletedTask;
    }

    public override BillLog AddLog(decimal money, bool isRecharge, BillLog.Modules modules, string reason, string remark = "", int? storeId = null,int? moduleId = null)
    {
        var now = DateTime.Now;
        var billLog = new BillLog()
        {
            Money = money,
            Type = isRecharge ? BillLog.Types.Recharge : BillLog.Types.Deduction,
            Datetime = now,
            Remark = remark,
            Audit = BillLog.Audits.NoReviewRequired,
            UserID = _operateUser.Id,
            Truename = _operateUser.TrueName,
            AuditUserId = _operateUser.Id,
            AuditTruename = _operateUser.TrueName,
            CompanyID = _company.ID,
            GroupID = _operateUser.GroupID,
            OEMID = _company.OEMID,
            CreatCompanyId = _operateUser.CompanyID,
            OperateCompanyId = _operateUser.CompanyID,
            OperateCompanyName = _operateUser.Company.Name,
            Module = modules,
            StoreId = storeId ?? 0,
            Reason = reason,
            ReportCompanyId = _company.ReportId,
            Balance = _company.PrivateWallet.Money
        };
        return billLog;
    }
}

/// <summary>
/// 对公钱包
/// </summary>
public sealed class PublicWallet : AWallet
{
    private readonly Company _company;
    private readonly User _operateUser;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="company">钱包所属公司</param>
    /// <param name="operateUser">当前操作人</param>
    public PublicWallet(Company company, User operateUser)
    {
        _company = company;
        _operateUser = operateUser;
    }
    
    public override bool IsEnough(decimal money, out string message)
    {
        var wallet = _company.PublicWallet;
        var allowNegative = _company.IsNegative;
        var walletGreaterThanMoney = wallet.Money >= money;
        var isEnough = allowNegative || walletGreaterThanMoney;

        message = isEnough ? string.Empty
            : _company.ID == _operateUser.CompanyID ? "当前余额不足" : "当前余额不足，请先提醒对方充值钱包金额";
        
        return isEnough;
    }

    public override BillLog.Belongs GetBelong() => BillLog.Belongs.RIGHT;
    public override Task Expense(decimal money)
    {
        _company.PublicWallet -= money;
        return Task.CompletedTask;
    }

    public override Task Recharge(decimal money)
    {
        _company.PublicWallet += money;
        return Task.CompletedTask;
    }
    
    public override BillLog AddLog(decimal money, bool isRecharge, BillLog.Modules modules, string reason, string remark = "", int? storeId = null,int? moduleId = null)
    {
        var now = DateTime.Now;
        var billLog = new BillLog()
        {
            Money = money,
            Type = isRecharge ? BillLog.Types.Recharge : BillLog.Types.Deduction,
            Datetime = now,
            Remark = remark,
            Audit = BillLog.Audits.NoReviewRequired,
            UserID = _operateUser.Id,
            Truename = _operateUser.TrueName,
            AuditUserId = _operateUser.Id,
            AuditTruename = _operateUser.TrueName,
            CompanyID = _company.ID,
            GroupID = _operateUser.GroupID,
            OEMID = _company.OEMID,
            CreatCompanyId = _operateUser.CompanyID,
            OperateCompanyId = _operateUser.CompanyID,
            OperateCompanyName = _operateUser.Company.Name,
            Module = modules,
            StoreId = storeId ?? 0,
            Reason = reason,
            ReportCompanyId = _company.ReportId,
            Balance = _company.PublicWallet.Money,
            Belong = GetBelong()
        };
        return billLog;
    }
}