using System.ComponentModel.DataAnnotations.Schema;
using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.Setting;

[Index(nameof(State))]
public class OemDomainModel : BaseModel
{

    [Comment("域名")]
    public string Value { get; set; }=string.Empty;

    [Comment("状态")]
    public StateEnum State { get; set; } = StateEnum.Open;

    [Comment("hash_code")]
    public ulong HashCode { get; set; }

    public int OemId { get; set; }

    [ForeignKey(nameof(OemId))]
    public OEM Oem { get; set; } = null!;
}