﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace ERP.Models.DB.Setting;
[Keyless]
public class MetaSt
{
    public string Title { get; set; } = string.Empty;
    [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
    public string Icon { get; set; } = string.Empty;
    [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
    [NotMapped]
    public List<string> Roles { get; set; } = new();
    [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
    public bool? AlwaysShow { get; set; }
    [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
    public bool? Hidden { get; set; }

    [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
    public bool? NoCache { get; set; }
    [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
    public bool? KeepAlive { get; set; }
}
