﻿
using ERP.Interface;
using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace ERP.Models.Setting;
using ENUM = Enums.ID.OEM;
using Extensions;

public class ERP1Version
{
    public DateTime DateTime { get; }

    public ERP1Version(int year, int month, int day, int v1, int v2, int v3)
    {
        var ClientID = v1 + v2 + v3;
        var h = ClientID;
        if (ClientID > 23)
            h = 23;
        ClientID -= h;
        var m = ClientID;
        if (ClientID > 59)
            m = 59;
        ClientID -= m;
        var s = ClientID;
        DateTime = new(year,month,day,h,m,s);
    }
}

public class OEM : BaseModel, IStatistic
{
    public OEM() { }
    
    public OEM(ENUM o, string domain, States state, int DId, int OId, int RId, int TId, DateTime time, string icon = "", string logo = "", string alipay = "")
    {
        ID = o.GetID();
        Name = o.GetDescription();
        Title = Name;
        Detail = Name;
        Domain = JsonConvert.SerializeObject(new List<string>() { domain });//"erp.waimaomvp.com"
        State = state;// OEM.States.Expired_V1,
        Alipay = alipay;
        DatabaseId = DId;// 1,
        OssId = OId;// 1,
        RedisId = RId;//1,
        ThemeId = TId;// 1,
        UrlIcon = icon;
        UrlLogo = logo;
        CreatedAt = time;
        UpdatedAt = time;
    }

    public OEM(ENUM o, string domain, States state, int DId, int OId, int RId, int TId, ERP1Version time
        ):this(o,domain,state,DId,OId,RId,TId,time.DateTime)
    {
    }

    public DateTime LastCreateStatistic { get; set; }
    public int? ThemeId { get; set; }
    public Theme? Theme { get; set; }
    public int? RedisId { get; set; }
    public Redis? Redis { get; set; }
    public int OssId { get; set; }
    public FileBucket? Oss { get; set; }
    public int? DatabaseId { get; set; }
    public DataBase? Database { get; set; }
    public States State { get; set; }
    
    [GraphQLName("OemStates")]
    public enum States
    {
        [Description("正常可用")]
        ENABLE = 1,
        [Description("失效(被禁用)")]
        DISABLE,
        [Description("过期(0.x系统迁移)")]
        Expired_V0,
        [Description("过期(1.x系统迁移)")]
        Expired_V1,
        [Description("过期(2.x系统迁移)")]
        Expired_V2,
    }

    public Styles Style { get; set; } = Styles.SingleLineDisplay;

    public enum Styles
    {
        SingleLineDisplay = 1,
        TowLineDisplay

    }
    [Comment("名称")]
    public string Name { get; set; } = string.Empty;
    [Comment("名称")]
    public string Title { get; set; } = string.Empty;
    [Comment("详情介绍")]
    public string Detail { get; set; } = string.Empty;
    [Comment("阿里即时支付信息")]
    public string Alipay { get; set; } = string.Empty;
    [Comment("Ico图片url")]
    public string? UrlIcon { get; set; } = string.Empty;
    [Comment("Logo图片url")]
    public string? UrlLogo { get; set; } = string.Empty;
    [Comment("域名(不带http)")]
    public string Domain { get; set; } = string.Empty;

    public string? LoginPage { get; set; } = LoginPageEnum.UniversalLogin.ToString();
    
    public enum LoginPageEnum
    {
        [Description("通用登录页")]
        UniversalLogin,
        [Description("米境通登录页")]
        MjkLogin,
        [Description("永合国际登录页")]
        YhLogin,
        [Description("嘉杰登录页")]
        JiajieLogin
    }

    [NotMapped, GraphQLIgnore]
    public List<ulong>? HashCodes
    {
        get
        {
            var domains = JsonConvert.DeserializeObject<List<string>>(Domain);
            if (domains == null)
            {
                return null;
            }

            return domains.Select(m => Helpers.CreateHashCode(m)).ToList();
        }
    }

}



//-- ----------------------------
//-- Table structure for erp3_oem
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_oem`;
//CREATE TABLE `erp3_oem`  (
//  `id` bigint unsigned NOT NULL,
//  `d_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'oem名称',
//  `u_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '左侧菜单显示',
//  `t_style` tinyint unsigned NOT NULL COMMENT '左上角文字样式 1单行 2双行',
//  `d_domain` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '域名(不带http)',
//  `t_state` tinyint unsigned NOT NULL COMMENT '1正常 2失效',
//  `d_oem_detail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'oem详情介绍',
//  `d_alipay` json NULL COMMENT '阿里即时支付信息',
//  `d_icon_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'ico图片url',
//  `d_logo_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'logo图片url',
//  `i_theme_id` bigint unsigned NOT NULL COMMENT '主题id',
//  `i_rds_id` bigint unsigned NULL COMMENT 'rds',
//  `i_oss_id` bigint unsigned NULL COMMENT 'oss',
//  `i_redis_id` bigint unsigned NULL COMMENT 'redis',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 76 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统管理_OEM' ROW_FORMAT = Dynamic;
