﻿using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;

namespace ERP.Models.Setting;

public class FileBucket : BaseModel
{
    [Comment("名称")]
    public string Name { get; set; } = string.Empty;

    [Comment("块/桶 名")]
    public string Bucket { get; set; } = string.Empty;

    [Comment("连通标识")]
    public string AccessKey { get; set; } = string.Empty;

    [Comment("密钥")]
    public string SecretKey { get; set; } = string.Empty;

    [Comment("外网地址")]
    public string EndpointDomain { get; set; } = string.Empty;


    [Comment("外网访问地址(自定义绑定的资源访问域名)")]
    public string BucketDomain { get; set; } = string.Empty;

    [Comment("1 阿里oss 2 华为云obs")]
    public StorageEnum Type { get; set; } = StorageEnum.Obs;

    public bool IsCName { get; set; } = false;
    public bool UseSsl { get; set; } = false;

    public enum StorageEnum
    {
        Oss = 1,
        Obs
    }
}


//-- ----------------------------
//-- Table structure for erp3_resource_oss
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_resource_oss`;
//CREATE TABLE `erp3_resource_oss`  (
//  `id` bigint unsigned NOT NULL,
//  `d_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'oss名称',
//  `d_bucket` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'bucket名称',
//  `d_access_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'oss_access_key',
//  `d_secret_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'oss_sercet_key',
//  `d_endpoint_domain` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'EndPoint外网地址',
//  `d_endpoint_intranet_domain` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'EndPoint内网地址',
//  `d_bucket_domain` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Bucket外网地址',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  `type` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1oss 2obs',
//  PRIMARY KEY(`id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = 'oss资源' ROW_FORMAT = Dynamic;