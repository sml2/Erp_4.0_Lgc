﻿//-- ----------------------------
//-- Table structure for erp3_resource_rds
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_resource_rds`;
//CREATE TABLE `erp3_resource_rds`  (
//  `id` bigint unsigned NOT NULL,
//  `d_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'rds名称',
//  `d_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '主机地址',
//  `d_port` int unsigned NOT NULL COMMENT '端口',
//  `d_database` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '库名',
//  `d_prefix` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '前缀',
//  `d_username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户名',
//  `d_password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '密码',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = 'rds资源' ROW_FORMAT = Dynamic;