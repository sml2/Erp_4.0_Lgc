﻿
using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace ERP.Models.Setting;
public class Redis : BaseModel
{
    public Versions Version { get; set; }
    public enum Versions
    {
        Redis4,
        Redis5
    }
    [Required(ErrorMessage = "请填写名称")]
    [MaxLength(255, ErrorMessage = "名称最多为255位")]
    [Comment("名称")]
    public string Name { get; set; } = string.Empty;

    [Required(ErrorMessage = "请填写主机地址")]
    [MaxLength(255, ErrorMessage = "主机地址最多为255位")]
    [Comment("主机地址")]
    public string Host { get; set; } = string.Empty;

    [Required(ErrorMessage = "请填写端口")]
    [Comment("端口")]
    public int Port { get; set; }

    [Comment("用户名")]
    public string? Username { get; set; }

    [Comment("密码")]
    public string? Password { get; set; }
}



//-- ----------------------------
//-- Table structure for erp3_resource_redis
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_resource_redis`;
//CREATE TABLE `erp3_resource_redis`  (
//  `id` bigint unsigned NOT NULL,
//  `d_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'redis名称',
//  `d_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '地址',
//  `d_port` int unsigned NOT NULL COMMENT '端口',
//  `d_username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户名',
//  `d_password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '密码',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = 'redis资源' ROW_FORMAT = Dynamic;
