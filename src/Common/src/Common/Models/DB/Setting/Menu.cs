using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace ERP.Models.DB.Setting;
using Extensions;
using ENUMID = Enums.ID.Menu;
public class Menu : BaseModel, Interface.Rule.IHas
{
    public Menu()
    {
    }

    public Menu(string ModuleEnumTypeName, Enum PermissionFlag)
    {
        Module = ModuleEnumTypeName;
        this.PermissionFlag = Convert.ToInt64(PermissionFlag);
    }
    public Menu(Enum PermissionFlag) : this($"{PermissionFlag.GetType().Name}Menu", PermissionFlag)
    {
    }
    public Menu(Enum PermissionFlag, ENUMID eid, ENUMID peid = ENUMID.None) : this(PermissionFlag)
    {
        ID = eid.GetID();
        if (peid != ENUMID.None)
            ParentId = peid.GetID();
    }
    [Column(TypeName = "varchar(50)")]
    public string Module { get; set; } = string.Empty;

    public long PermissionFlag { get; set; }

    [Column(TypeName = "varchar(50)")]
    public string Path { get; set; } = string.Empty;

    [Column(TypeName = "varchar(50)")]
    [SJI(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [NJP(NullValueHandling = NullValueHandling.Ignore)]
    public string? Redirect { get; set; }

    [Column(TypeName = "varchar(50)")]
    [SJI(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [NJP(NullValueHandling = NullValueHandling.Ignore)]
    public string Component { get; set; } = string.Empty;

    [SJI(Condition = JsonIgnoreCondition.Always)]
    [NJP(NullValueHandling = NullValueHandling.Include)]
    [Column(TypeName = "json")]
    public string MetaJson { get; set; } = string.Empty;

    public Menu Clone()
    {
        var m = (Menu)MemberwiseClone();
        if(Children is not null)
            m.Children = Children.Select(c => c.Clone()).ToList();
        return m;
    }

    [Column(TypeName = "varchar(50)"), Comment("介绍，描述")]
    public string? Desc { get; set; }

    [Column(TypeName = "int(11)"), Comment("排序字段 从小到大")]
    public long Sort { get; set; } = 0;

    [Comment("Hidden")]
    public bool Hidden { get; set; }

    [Comment("Hidden")]
    public bool AlwaysShow { get; set; }

    [NotMapped]
    public string Key => Module;
    [NotMapped]
    public long Value => PermissionFlag;

    [NotMapped]
    public MetaSt Meta
    {
        get
        {
            var json = SJS.Deserialize<MetaSt>(MetaJson);
            if (json != null)
                json.AlwaysShow = ParentId == null;

            return json??default!;
        }
        set => MetaJson = SJS.Serialize(value);
    }

    public int? ParentId { get; set; }

    [SJI, NJI]
    public Menu Parent { get; set; } = null!;

    [InverseProperty(nameof(Parent))]
    [SJI(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [NJP(NullValueHandling = NullValueHandling.Ignore)]
    public List<Menu> Children { get; set; } = null!;

    [Column(TypeName = "varchar(50)")]
    [SJI(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public string Name { get; set; } = string.Empty;


}