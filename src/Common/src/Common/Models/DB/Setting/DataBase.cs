﻿
using ERP.Models.Abstract;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace ERP.Models.Setting;
public class DataBase : BaseModel
{
    public Providers Provider { get; set; }
    public enum Providers
    {
        @private,
        AliCloud_Rds,
        HuaWeiCloud_Rds,
        AmazonCloud_DataBase,
        MicroSoftAzure
    }
    [Comment("数据库驱动")]
    public Drivers Driver { get; set; }
    public enum Drivers
    {
        SQLite1,
        SQLite2,
        SQLite3,
        SQLServer2008,
        SQLServer2010,
        SQLServer2016,
        Oracle09,
        Oracle10,
        Oracle11,
        MySQL5,
        MySQL7,
        MySQL8
    }
    [Required(ErrorMessage = "请填写名称")]
    [MaxLength(255, ErrorMessage = "名称最多为255位")]
    [Comment("名称")]
    public string Name { get; set; } = string.Empty;

    [Required(ErrorMessage = "请填写主机地址")]
    [MaxLength(255, ErrorMessage = "主机地址最多为255位")]
    [Comment("主机地址")]
    public string Host { get; set; } = string.Empty;

    [Required(ErrorMessage = "请填写端口")]
    [Comment("端口")]
    public int Port { get; set; }

    [Required(ErrorMessage = "请填写库名")]
    [MaxLength(255, ErrorMessage = "库名最多为255位")]
    [Comment("库名")]
    public string Database { get; set; } = string.Empty;

    [Required(ErrorMessage = "请填写用户名")]
    [MaxLength(255, ErrorMessage = "用户名最多为255位")]
    [Comment("用户名")]
    public string Username { get; set; } = string.Empty;

    [Required(ErrorMessage = "请填写密码")]
    [MaxLength(255, ErrorMessage = "密码最多为255位")]
    [Comment("密码")]
    public string Password { get; set; } = string.Empty;

    [Required(ErrorMessage = "请填写表前缀")]
    [MaxLength(255, ErrorMessage = "表前缀最多为255位")]
    [Comment("表前缀")]
    public string Prefix { get; set; } = string.Empty;
}


