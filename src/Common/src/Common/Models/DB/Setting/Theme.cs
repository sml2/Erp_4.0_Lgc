﻿
using ERP.Models.Abstract;

namespace ERP.Models.Setting;
public class Theme:BaseModel
{
}


//-- ----------------------------
//-- Table structure for erp3_theme
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_theme`;
//CREATE TABLE `erp3_theme`  (
//  `id` bigint unsigned NOT NULL,
//  `i_oem_id` int unsigned NOT NULL COMMENT 'OEM id',
//  `d_menu_text_color` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '菜单字体颜色',
//  `d_menu_active_text_color` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '子菜单选中字体颜色',
//  `d_sub_menu_active_text_color` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '主菜单选中字体颜色',
//  `d_menu_bg_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '菜单背景图',
//  `d_menu_bg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '菜单背景色',
//  `d_menu_hover_color` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '主菜单鼠标悬停颜色',
//  `d_sub_menu_bg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '子菜单背景色',
//  `d_sub_menu_hover_color` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '子菜单鼠标悬浮颜色',
//  `d_logo_bg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'logo背景色',
//  `d_navbar_bg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '头部导航栏颜色',
//  `d_title_color` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '左上角文字颜色',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 58 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = 'OEM管理_oem主题' ROW_FORMAT = Dynamic;
