﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using MyClaims = ERP.Authorization.Claim;

namespace ERP.Models.DB.Identity;

using Extensions;
[Comment("用户-模块权限")]
public class UserClaim : IdentityUserClaim<int>, Interface.ICanInitializeFromClaim
{
    public UserClaim() { }
    public UserClaim(int id, Enums.Identity.User user, MyClaims myClaims)
    {
        Id = id;
        UserId = user.GetID();
        InitializeFromClaim(myClaims);
    }
    
    public UserClaim(int userId, MyClaims myClaims)
    {
        UserId = userId;
        InitializeFromClaim(myClaims);
    }
    public int OemId { get; set; }
    public string BinaryStr { get; set; } = string.Empty;
    public string HexStr { get; set; } = string.Empty;
    public string Base64 { get; set; } = string.Empty;

    // TODO: mysql不能用
    // The property 'UserClaim.ClaimValue' could not be mapped because it is of type 'string', which is not a supported primitive type or a valid        // entity type. Either explicitly map this property, or ignore it using the '[NotMapped]' attribute or by using 'EntityTypeBuilder.Ignore' in        //  'OnModelCreating'.

    // [Column(TypeName = "bigint")]
    //public override string ClaimValue { get; set; }

    public override void InitializeFromClaim(Claim claim)
    {
        base.InitializeFromClaim(claim);
        claim.Initialize(this);
    }
}
