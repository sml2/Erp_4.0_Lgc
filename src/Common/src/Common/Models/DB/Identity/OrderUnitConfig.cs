using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace ERP.Models.DB.Identity;

/// <summary>
/// 订单货币配置
/// <para>true 代表使用订单货币</para>
/// <para>false 代表使用默认展示货币</para>
/// </summary>
[Owned]
public class OrderUnitConfig
{
     
    public OrderUnitConfig()
    {
    }
    
    public OrderUnitConfig(bool total, bool goods, bool fee, bool refund, bool deliver, bool purchase, bool profit, bool productPrice)
    {
        Total = total;
        Goods = goods;
        Fee = fee;
        Refund = refund;
        Deliver = deliver;
        Purchase = purchase;
        Profit = profit;
        ProductPrice = productPrice;
    }

    public static OrderUnitConfig Default => new OrderUnitConfig();

    /// <summary>
    /// 订单总价
    /// </summary>
    public bool Total { get; set; }

    /// <summary>
    /// 平台佣金手续费
    /// </summary>
    public bool Fee { get; set; }

    /// <summary>
    /// 订单退款
    /// </summary>
    public bool Refund { get; set; }

    /// <summary>
    /// 国际运费
    /// </summary>
    public bool Deliver { get; set; }

    /// <summary>
    /// 采购价格
    /// </summary>
    public bool Purchase { get; set; }

    /// <summary>
    /// 订单获利
    /// </summary>
    public bool Profit { get; set; }

    /// <summary>
    /// 订单商品价格
    /// </summary>
    public bool ProductPrice { get; set; }

    public bool Goods { get; set; }

    public bool LOSS { get; set; }

}