﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace ERP.Models.DB.Identity;

[Owned]
public class ProductConfig
{
    public static ProductConfig Default => new ProductConfig();
    
    /// <summary>
    /// 产品展示货币
    /// </summary>
    /// <returns>
    /// <para>true 采集货币</para>
    /// <para>false 通用货币</para>
    /// </returns>
    public bool ShowUnit { get; set; }
    
    /// <summary>
    /// sku是否添加前缀
    /// </summary>
    public bool SkuPrefix { get; set; } = true;
    
    /// <summary>
    /// sku是否追加属性信息
    /// </summary>
    public bool SkuSuffix { get; set; }
    
    /// <summary>
    /// 标题是否追加属性信息
    /// </summary>
    public bool TitleSuffix { get; set; }
    
    /// <summary>
    /// 导出图片地址
    /// </summary>
    /// <returns>
    /// <para>false 固定地址</para>
    /// <para>true 添加随机后缀</para>
    /// </returns>
    public bool ExportImgUrl { get; set; }

    /// <summary>
    /// 自动分配EAN/UPC, 在将产品添加到导出时自动分配EAN/UPC, 分配类型根据<see cref="ProduceCodeType"/>判断.<br />
    /// EAN/UPC码不够时不进行分配, 同时不会使添加导出产品任务失败, 只会有警告提示.
    /// </summary>
    /// <returns>
    /// <para>true 启用</para>
    /// <para>false 关闭</para>
    /// </returns>
    public bool AutoAllotProduceCode { get; set; }

    /// <summary>
    /// 自动分配的产品码类型
    /// </summary>
    /// <returns>
    /// EAN | UPC
    /// </returns>
    [MaxLength(5)]
    public string ProduceCodeType { get; set; } = "EAN";
}