﻿using System;
using ERP.Extensions;
using Newtonsoft.Json;
namespace ERP.Models.DB.Identity;
using ENUM = Enums.ID;
/// <summary>
/// ERP2.0跟卖系统用户
/// </summary>
public class ERP2User
{
    /// <summary>
    /// ef6.0.5不支持子类直接存入，要所以采用包装的写法
    /// 主要是防止污染User类
    /// </summary>
    public User User = new();
    const int ERP2BaseID = 222222;
    public ERP2User(int id, ENUM.OEM o) 
    {
        User.ID = ERP2BaseID + id;
        User.OEMID = o.GetID();
        User.CompanyID = ERP.Enums.ID.Company.Empty.GetID();
        User.FormRuleTemplateID = 1;
        User.GroupID = Enums.ID.Group.DEFAUTE.GetID();
        User.ConcurrencyStamp = $"ERP2[{User.ID}]";
    }
    private  void AddRemark(string key,string value) => User.Remark += $"{key}={value}\r\n";
    public long VALIDITY { set => AddRemark(nameof(VALIDITY),value.ToString()); }
    public string USERNAME { set => User.UserName = value; }
    public string TRUENAME { set => User.TrueName = value; }
    public string PASSWORD { set => User.PasswordHash = value; }
    public string MOBILE {
        set
        {
            if (value is not null)
                if (value.IsNumeric() && value.Length == 11)
                    User.Mobile = value;
                else
                    AddRemark(nameof(MOBILE), value);
        }
    }

    public string EMAIL
    {
        set
        {
            if (value is not null)
                if (value.Contains("@"))
                    User.Email = value;
                    //User.EmailConfirmed = true;
                else
                    AddRemark(nameof(EMAIL), value);
        }
    }
    public int STATUS { set => User.State = value == 1 ? Abstract.BaseModel.StateEnum.Open : Abstract.BaseModel.StateEnum.Close ; }
    /// <summary>
    /// ef 6.0.5 无法区分带Kind的DateTime,却还傻不拉几的无脑当成Local判别,而且0秒还不给用
    /// </summary>
    public long TIMESTAMP { set => User.CreatedAt = User.UpdatedAt = Mod_GlobalFunction.GetTimeFromStamp(value.ToString()); }
    public int PID { set => User.Pid = ERP2BaseID + value; }
    public int DEPT_ID { set => AddRemark(nameof(DEPT_ID), value.ToString()); }
    public string FROM { set => User.Pids = JsonConvert.SerializeObject(value.Split(',').Select(s=> Convert.ToInt32(s) + ERP2BaseID)); }
    public int CONCIERGE { set => User.Role =  value == 1 ? Enums.Identity.Role.ADMIN : Enums.Identity.Role.Employee; }
    public int ODX { set => User.Sort = value; }
    public int LEVEL { set => User.Level = value; }
    public int TYPE { set => AddRemark(nameof(TYPE), value.ToString()); }
    public int CAPACITY_ID { set => AddRemark(nameof(CAPACITY_ID), value.ToString()); }
    public int IS_SHARE { set => AddRemark(nameof(IS_SHARE), value.ToString()); }
    public int AUTH_NUM { set => AddRemark(nameof(AUTH_NUM), value.ToString()); }
    public int UPGRADE { set => AddRemark(nameof(UPGRADE), value.ToString()); }
    public int PRODUCT_NUM { set => AddRemark(nameof(PRODUCT_NUM), value.ToString()); }
    public int ORDER_NUM { set => AddRemark(nameof(ORDER_NUM), value.ToString()); }
    public int PURCHASE_NUM { set => AddRemark(nameof(PURCHASE_NUM), value.ToString()); }
    public double ORDER_TOTAL { set => AddRemark(nameof(ORDER_TOTAL), value.ToString()); }
    public double SERVICE_TOTAL { set => AddRemark(nameof(SERVICE_TOTAL), value.ToString()); }
    public double PURCHASE_TOTAL { set => AddRemark(nameof(PURCHASE_TOTAL), value.ToString()); }
    public double SELF_RECHARGE_TOTAL { set => AddRemark(nameof(SELF_RECHARGE_TOTAL), value.ToString()); }
    public int RAISE { set => AddRemark(nameof(RAISE), value.ToString()); }
    public double BALANCE { set => AddRemark(nameof(BALANCE), value.ToString()); }
    public int TRANSLATION_UNIT { set => AddRemark(nameof(TRANSLATION_UNIT), value.ToString()); }
    public int TRANSLATION_PRICE { set => AddRemark(nameof(TRANSLATION_PRICE), value.ToString()); }
    public int COMPANY_ID { set => AddRemark(nameof(COMPANY_ID), value.ToString()); }
    public int EXPORT_IMG { set => AddRemark(nameof(EXPORT_IMG), value.ToString()); }
}

