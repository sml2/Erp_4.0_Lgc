﻿using Microsoft.AspNetCore.Identity;
namespace ERP.Models.DB.Identity;

using Extensions;
using System.ComponentModel.DataAnnotations.Schema;

public class UserRole : IdentityUserRole<int>
{
    public UserRole() { }
    public UserRole(Enums.Identity.User user, Enums.ID.Role role)
    {
        UserId = user.GetID();
        RoleId = role.GetID();
    }
    public override int UserId { get; set; }
    //public User User { get; set; }
    public override int RoleId { get; set; }
    //public Role Role { get; set; }
}
