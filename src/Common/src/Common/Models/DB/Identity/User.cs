﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.ComponentModel;
using ERP.Models.DB.Users;

namespace ERP.Models.DB.Identity;
using ENUM = Enums.ID;
using Interface;
using Models.Setting;
using Extensions;
using static Models.Abstract.BaseModel;
using System.Linq;

[Table("user")]
[Comment("用户管理_用户")]
public class User : IdentityUser<int>, IHasTimestamps, IStatistic
{
    public User()
    {
        ProductConfig = ProductConfig.Default;
        OrderUnitConfig = OrderUnitConfig.Default;
    }
    public User(Enums.Identity.User u, ENUM.Group g, ENUM.Company c, ENUM.OEM o, ENUM.Company cc, string phone) : this()
    {
        ID = u.GetID();
        UserName = $"{u}";
        NormalizedUserName = UserName.ToUpper();
        TrueName = u.GetDescription();
        GroupID = g.GetID();
        CompanyID = c.GetID();
        OEMID = o.GetID();
        CreateCompanyId = cc.GetID();
        PhoneNumber = phone;
        PhoneNumberConfirmed = !string.IsNullOrWhiteSpace(phone);
        ConcurrencyStamp = $"ERP4[{ID}]";
    }

    [NotMapped]
    public int ID { get => Id; set => Id = value; }

    [Column(TypeName = "varchar(255)"), Comment("姓名")]
    public string TrueName { get; set; } = string.Empty;

    [NotMapped]
    public string DisplayName
    {
        get => NormalizedUserName;
        set => NormalizedUserName = value;
    }
    /// <summary>
    /// 明文密码 方便运维
    /// </summary>
    [JsonIgnore, GraphQLIgnore, Comment("明文密码 方便运维")]
    public string Password { get; set; } = string.Empty;

    [JsonIgnore, GraphQLIgnore]
    public override string? PasswordHash { get; set; }

    // PhoneNumberConfirmed 代替 Attestation 
    //[Column(TypeName = "tinyint"), Comment("手机号码认证 1未认证2已认证")]
    //public Attestations Attestation { get; set; } = Attestations.DISABLE;

    //public enum Attestations
    //{
    //    None,
    //    DISABLE,
    //    ENABLE
    //}

    [NotMapped]
    public string Mobile { get => PhoneNumber; set => PhoneNumber = value; }
    [Column(TypeName = "varchar(255)"), Comment("邮箱")]
    public override string Email { get; set; } = string.Empty;
    /// <summary>
    /// 业务角色模板来源
    /// </summary>
    [Column(TypeName = "int(11)"), Comment("业务角色模板来源")]
    public int FormRuleTemplateID { get; set; }
    [GraphQLIgnore]
    public RuleTemplate FormRuleTemplate { get; set; } = null!;


    [Comment("用户软件UI展示货币单位")]
    public string UnitConfig { get; set; } = "CNY";

    [Comment("订单价格显示配置"), GraphQLIgnore]
    public OrderUnitConfig OrderUnitConfig { get; set; }

    [Comment("产品配置"), GraphQLIgnore]
    public ProductConfig ProductConfig { get; set; }

    [Column(TypeName = "tinyint"), Comment("订单汇率配置项 1 拉取时亚马逊汇率 2实时汇率")]
    public OrderRateConfigs OrderRateConfig { get; set; }
    public enum OrderRateConfigs
    {
        None,
        [Description("拉取时亚马逊汇率")]
        AMAZON,
        [Description("实时汇率")]
        REALTIME,
    }

    /// <summary>
    /// 归档用户角色，业务实际使用关联表
    /// </summary>
    [Comment("归档用户角色，业务实际使用关联表")] public Enums.Identity.Role Role { get; set; } = Enums.Identity.Role.NONE;

    [Column(TypeName = "int(11)"), Comment("父级用户ID")]
    public int Pid { get; set; }

    [Column(TypeName = "varchar(255)"), Comment("创建用户过程")]
    public string Pids { get; set; } = string.Empty;
    public StateEnum State { get; set; } = StateEnum.Open;
    public bool Enabled { get => State == StateEnum.Open; set => State = value ? StateEnum.Open : StateEnum.Close; }
    [Column(TypeName = "int(11)"), Comment("层级深度")]
    public int Level { get; set; }

    [Column(TypeName = "varchar(255)"), Comment("头像")]
    public string Avatar { get; set; } = string.Empty;

    [Column(TypeName = "int(11)"), Comment("主题Id")]
    public int? ThemeId { get; set; }

    [Column(TypeName = "int(11)"), Comment("排序字段")]
    public long Sort { get; set; }

    [Column(TypeName = "bigint(20)"), Comment("用户名 hashCode")]
    public long HashCode { get; set; }

    [Column(TypeName = "int(11)"), Comment("公司ID")]
    public int CompanyID { get; set; }

    [Column(TypeName = "int(11)"), Comment("创建公司id")]
    public int CreateCompanyId { get; set; }

    [Comment("是否分销")]
    public bool IsDistribution { get; set; }

    [Column(TypeName = "int(11)"), Comment("用户组ID")]
    public int GroupID { get; set; } = 0;
    //public Group? Group { get; set; } = default!;

    /// <summary>
    /// user-store组
    /// </summary>
    [Comment("该用户创建者所属的用户组")]
    public int? OpenUserGroupID { get; set; }
    [GraphQLIgnore]
    public Group? BelongsGroup { get; set; }

    [GraphQLIgnore]
    public Company Company { get; set; } = default!;

    [Column(TypeName = "int(11)"), Comment("贴牌OEM ID(分区id)")]
    public int OEMID { get; set; } = -1;
    [GraphQLIgnore]
    public OEM OEM { get; set; } = default!;

    [Column(TypeName = "Timestamp"), Comment("首次WEB登录")]
    public DateTime? FirstLoginWeb { get; set; } = null;

    [Column(TypeName = "Timestamp"), Comment("首次EXE登录")]
    public DateTime? FirstLoginExe { get; set; } = null;

    [Column(TypeName = "Timestamp"), Comment("登录时间")]
    public DateTime? LoginTime { get; set; } = null;

    [Column(TypeName = "Timestamp")]
    public DateTime CreatedAt { get; set; }

    [Column(TypeName = "Timestamp")]
    public DateTime UpdatedAt { get; set; }

    public DateTime LastCreateStatistic { get; set; }

    public string? Remark { get; set; }

    /// <summary>
    /// 自定义标记模板
    /// </summary>
    [Comment("自定义标记模板:10个")]
    public string CustomMarkTemplate { get; set; } = string.Empty;
    public List<CustomMarkJson> GetCustomMarkTemplate()
    {
        return JsonConvert.DeserializeObject<List<CustomMarkJson>>(CustomMarkTemplate) ?? new();
    }
    public void SetCustomMarkTemplate(List<CustomMarkJson> lc)
    {
        var temp = GetCustomMarkTemplate();
        lc.ForEach(item =>
        {
            var cm = temp.FirstOrDefault(a => a.Id == item.Id);
            if (cm == null)
            {
                temp.Add(item);
            }
            else
            {
                temp.Remove(cm);
                temp.Add(item);
            }
        });
        CustomMarkTemplate = JsonConvert.SerializeObject(temp);
    }

    public class CustomMarkJson
    {
        public int Id { get; set; }
        public string Color { get; set; } = string.Empty;
        public string Remark { get; set; } = string.Empty;
    }
    public override string ToString()
    {
        return $"{UserName}@{Id}";
    }
}


//-- ----------------------------
//-- Table structure for erp3_user
//-- ----------------------------
//DROP TABLE IF EXISTS `erp3_user`;
//CREATE TABLE `erp3_user`  (
//  `id` bigint unsigned NOT NULL,
//  `d_username` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户名(唯一)',
//  `d_truename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '姓名',
//  `d_password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '密码encrypt',
//  `d_mobile` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '手机',
//  `d_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '邮箱',
//  `t_attestation` tinyint unsigned NOT NULL COMMENT '手机号码认证 1未认证2已认证',
//  `d_unit_config` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'CNY' COMMENT '用户软件UI展示货币单位',
//  `d_order_unit_config` bigint unsigned NOT NULL COMMENT '订单价格显示配置',
//  `d_order_rate_config` int unsigned NOT NULL COMMENT '订单汇率配置项 1 拉取时亚马逊汇率 2实时汇率',
//  `i_rule_id` int unsigned NOT NULL COMMENT '权限组id',
//  `i_group_id` int unsigned NOT NULL COMMENT '用户组id',
//  `i_pid` int unsigned NOT NULL COMMENT '父级用户id',
//  `t_state` tinyint unsigned NOT NULL COMMENT '用户状态1正常2禁止',
//  `i_pids` json NULL COMMENT '创建用户过程',
//  `t_concierge` tinyint unsigned NOT NULL COMMENT '用户类型1独立账号2子账号',
//  `d_sort` int (0) NOT NULL DEFAULT 0 COMMENT '排序',
//  `d_level` tinyint unsigned NOT NULL COMMENT '深度',
//  `u_oem_id` int unsigned NOT NULL COMMENT 'OEM',
//  `i_company_id` int unsigned NOT NULL COMMENT '公司id',
//  `i_p_company_id` int unsigned NOT NULL COMMENT '创建公司id',
//  `u_is_distribution` tinyint unsigned NOT NULL COMMENT '是否分销1否2是',
//  `d_first_login_web` datetime(0) NULL DEFAULT NULL COMMENT '首次web登录',
//  `d_first_login_exe` datetime(0) NULL DEFAULT NULL COMMENT '首次exe登录',
//  `t_product_img_config` tinyint unsigned NOT NULL COMMENT '导出excel图片后加随机数1否2是',
//  `d_avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '头像',
//  `i_theme_id` int unsigned NOT NULL COMMENT '主题id',
//  `login_time` timestamp(0) NULL DEFAULT NULL COMMENT '登录时间',
//  `created_at` timestamp(0) NULL DEFAULT NULL,
//  `updated_at` timestamp(0) NULL DEFAULT NULL,
//  PRIMARY KEY(`id`) USING BTREE,
// UNIQUE INDEX `username`(`d_username`) USING BTREE,
// INDEX `company_id`(`i_company_id`) USING BTREE,
// INDEX `group_id`(`i_group_id`) USING BTREE,
// INDEX `mobile`(`d_mobile`) USING BTREE,
// INDEX `oem_id`(`u_oem_id`) USING BTREE,
// INDEX `p_company_id`(`i_p_company_id`) USING BTREE,
// INDEX `pid`(`i_pid`) USING BTREE,
// INDEX `rule_id`(`i_rule_id`) USING BTREE,
// INDEX `sort`(`d_sort`) USING BTREE,
//  INDEX `state`(`t_state`) USING BTREE
//) ENGINE = InnoDB AUTO_INCREMENT = 12373 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户管理_用户表' ROW_FORMAT = Dynamic;