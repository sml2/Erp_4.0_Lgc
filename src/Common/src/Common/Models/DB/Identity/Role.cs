﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ERP.Models.DB.Identity;
using Extensions;
using RoleID = Enums.ID.Role;
[Table("role")]
[Comment("角色")]
public class Role : IdentityRole<int>, IHasTimestamps
{
    public Role() { }
    public Role(RoleID id)
    {
        DisplayName = id.GetDescription();
        Name = id.GetName();
        NormalizedName = Name.ToUpper();
        Id = id.GetID();
    }

    //1超管2管理员3公司管理员4员工(无用户管理)
    [Comment("展示名称"), Required]
    public string DisplayName { get; set; } = string.Empty;

    [Column(TypeName = "Timestamp")]
    public DateTime CreatedAt { get; set; }

    [Column(TypeName = "Timestamp")]
    public DateTime UpdatedAt { get; set; }
}