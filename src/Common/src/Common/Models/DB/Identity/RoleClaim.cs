﻿using System.Security.Claims;
using ERP.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
namespace ERP.Models.DB.Identity;
using MyClaims = Authorization.Claim;
[Comment("系统角色权限")]
public class RoleClaim : IdentityRoleClaim<int>, Interface.ICanInitializeFromClaim
{
    public RoleClaim() { }
    public RoleClaim(int id, Enums.Identity.Role role, MyClaims myClaims)
    {
        Id = id;
        RoleId = role.GetID();
        InitializeFromClaim(myClaims);
    }

    public int OemId { get; set; }
    public string BinaryStr { get; set; } = string.Empty;
    public string HexStr { get; set; } = string.Empty;
    public string Base64 { get; set; } = string.Empty;
    public override void InitializeFromClaim(Claim claim)
    {
        base.InitializeFromClaim(claim);
        claim.Initialize(this);
    }
}
