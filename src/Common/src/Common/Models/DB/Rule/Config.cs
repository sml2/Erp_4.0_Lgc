﻿using ERP.Interface.Rule;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.Models.DB.Rule;
/// <summary>
/// 此结构可以Rule下的Permission和Config都可用,不过目前只用在Config
/// </summary>

[Owned]
public record Config<T>: IConfig where T : struct, Enum
{
    public T Visible { get; set; }//CanEditConfig
    public T Editable { get; set; }//Value

    long IConfig.Visible { get => Convert.ToInt64(Visible); set => Visible = GetData(value); }
    long IConfig.Editable { get => Convert.ToInt64(Editable); set => Editable = GetData(value); }
    private T GetData(long value)=> Enum.Parse<T>(value.ToString());

    //public int RuleTemplateID { get; set; }
}