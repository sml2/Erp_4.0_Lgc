﻿using System.Diagnostics;
using System.Net;
using System.Text;

namespace Common.Html;

public class Document
{
    public Document(List<Node> nodes)
    {
        Nodes = nodes;
    }

    public List<Node> Nodes { get; }

    /// <summary>
    /// 获取html中的所有文本
    /// </summary>
    /// <returns></returns>
    public string GetContent(GetContentSetting? setting = null)
    {
        var sb = new StringBuilder();
        GetContentCore(Nodes, setting ?? new GetContentSetting(), sb);
        return sb.ToString();
    }

    private static int GetContentCore(List<Node> nodes, GetContentSetting setting, StringBuilder? sb = null)
    {
        using var iter = IterTextNode(nodes, setting).GetEnumerator();
        if (!iter.MoveNext())
            return 0;
        var lineNumber = 0;
        var isFirst = true;

        var firstNode = iter.Current;
        while (firstNode != null)
        {
            var secondNode = iter.MoveNext() ? iter.Current : null;

            
            if (secondNode is not null)
            {
                var content = setting.Unescape ? WebUtility.HtmlDecode(firstNode.Content) : firstNode.Content;
                content = FormatString(content, isFirst && setting.RemoveWhiteSpaceStartLines, false);
                sb?.Append(content);
                lineNumber += content.Count(c => c == '\n');
                sb?.Append('\n');
                lineNumber++;
                isFirst = false;
            }
            else
            {
                var content = setting.Unescape ? WebUtility.HtmlDecode(firstNode.Content) : firstNode.Content;
                content = FormatString(content,isFirst && setting.RemoveWhiteSpaceStartLines, setting.RemoveWhiteSpaceEndLines);
                sb?.Append(content);
                lineNumber += content.Count(c => c == '\n');
                lineNumber++;
            }
            
            firstNode = secondNode;
        }
        return lineNumber;



    }
    
    static string FormatString(string str, bool trimStart, bool trimEnd)
    {
        var pointer = 0;
        if (trimStart)
            str = str[SkipWhitespaceLine()..];
        do
        {
            var length = SkipWhitespaceLine();
            if (pointer + length >= str.Length)
            {
                return trimEnd ? str[..pointer] : str;
            }

            pointer++;
        } while (pointer < str.Length);

        return str;

        int SkipWhitespaceLine()
        {
            var innerPointer = pointer;

            while (innerPointer < str.Length)
            {
                if (str[innerPointer] == ' ' || str[innerPointer] == 160 || str[innerPointer] == '\n')
                    innerPointer++;
                else if (str[innerPointer] == '&' && str[innerPointer + 1] == 'n' && str[innerPointer + 2] == 'b' && str[innerPointer + 3] == 's' &&
                         str[innerPointer + 4] == 'p' && str[innerPointer + 5] == ';')
                    innerPointer += 6;
                else
                    break;
            }

            return innerPointer - pointer;
        }
    }
    
    /// <summary>
    /// 迭代document中的所有TextNode
    /// </summary>
    /// <param name="nodes"></param>
    /// <returns></returns>
    private static IEnumerable<TextNode> IterTextNode(List<Node> nodes, GetContentSetting setting)
    {
        var pointer = 0;
        var iter = IterAllTextNode(nodes).ToArray();
        
        var whitespaceNodes = ReadWhitespaceNodes().ToArray();
        if (!setting.RemoveWhiteSpaceStartLines)
        {
            foreach (var whitespaceNode in whitespaceNodes)
            {
                yield return whitespaceNode;
            }
        }
        while (IsNotEof())
        {
            whitespaceNodes = ReadWhitespaceNodes().ToArray();
            
            if (IsNotEof())
            {
                foreach (var whitespaceNode in whitespaceNodes)
                {
                    yield return whitespaceNode;
                }

                yield return iter[pointer];
                pointer++;
            }
            else
            {
                if (!setting.RemoveWhiteSpaceEndLines) 
                    foreach (var whitespaceNode in whitespaceNodes)
                    {
                        yield return whitespaceNode;
                    }
                yield break;
            }
                
        }

        bool IsNotEof() => pointer < iter.Length;

        IEnumerable<TextNode> ReadWhitespaceNodes()
        {
            while (IsNotEof() && IsWhitespaceNode(iter[pointer], true))
            {
                yield return iter[pointer];
                pointer++;
            }
        }

        static bool IsWhitespaceNode(TextNode node, bool unescape)
        {
            return string.IsNullOrWhiteSpace((unescape
                ? WebUtility.HtmlDecode(node.Content)
                : node.Content).Replace("\n", ""));
        }
    }
    
    /// <summary>
    /// 迭代document中的所有TextNode
    /// </summary>
    /// <param name="nodes"></param>
    /// <returns></returns>
    private static IEnumerable<TextNode> IterAllTextNode(List<Node> nodes)
    {
        foreach (var node in nodes)
        {
            if (node is TextNode text)
                yield return text;
            if (node is Tag tag)
                foreach (var item in IterAllTextNode(tag.Children))
                    yield return item;
        }
    }

    public Document ReplaceContent(string content) => ReplaceContent(content, new GetContentSetting());

    /// <summary>
    /// 替换html中的所有文本
    /// </summary>
    /// <param name="content"></param>
    /// <param name="setting"></param>
    public Document ReplaceContent(string content, GetContentSetting setting)
    {
        var split = content.Split('\n');
        var textNodes = IterTextNode(Nodes, setting).ToArray();
        var totalLineNumber = GetContentCore(Nodes, setting);

        if (split.Length == totalLineNumber + 1 && split[^1] == string.Empty)
            split = split.SkipLast(1).ToArray();
        
        Debug.Assert(split.Length == totalLineNumber);

        var index = 0;
        for (var i = 0; i < textNodes.Length; i++)
        {
            var textNode = textNodes[i];
            var text = textNode.Content;
            // 只有一个
            if (textNodes.Length == 1)
                text = FormatString(textNode.Content, setting.RemoveWhiteSpaceStartLines,
                    setting.RemoveWhiteSpaceEndLines);
            // 第一个
            else if (i == 0)
                text = FormatString(textNode.Content, setting.RemoveWhiteSpaceStartLines,false);
            // 最后一个
            else if (i == textNodes.Length - 1)
                text = FormatString(textNode.Content, false, setting.RemoveWhiteSpaceEndLines);

            var length = text.Count(c => c == '\n') + 1;
            textNode.Content = string.Join('\n', split, index, length);
            index += length;
        }

        return this;
    }

    public override string ToString() => ToString(new GetContentSetting());
    
    public string ToString(GetContentSetting setting)
    {
        var sb = new StringBuilder();
        foreach (var node in Nodes)
        {
            AppendNode(sb, node);
        }

        return sb.ToString();
        
        void AppendNode(StringBuilder buffer, Node node)
        {
            if (node is TextNode text)
                buffer.Append(text.Content);

            if (node is Tag tag)
            {
                buffer.Append('<');
                buffer.Append(tag.TagName);

                foreach (var attr in tag.Attributes)
                    AppendAttribute(buffer, attr);
                if (tag.SelfClosing)
                {
                    buffer.Append(" />");
                    return;
                }
                buffer.Append('>');

                foreach (var child in tag.Children)
                {
                    AppendNode(buffer, child);
                }
                buffer.Append($"</{tag.TagName}>");
            }
        }

        void AppendAttribute(StringBuilder buffer, KeyValuePair<string, string?> attr)
        {
            buffer.Append(' ');
            buffer.Append(attr.Key);
            if (attr.Value is not null)
            {
                buffer.Append("=\"");
                buffer.Append(attr.Value);
                buffer.Append('"');
            }
        }
    }
}

public class GetContentSetting
{
    /// <summary>
    /// 反转义html字符串
    /// </summary>
    public bool Unescape { get; set; }
    
    /// <summary>
    /// 删除末尾空白行, 包括空行/全是空格行
    /// </summary>
    public bool RemoveWhiteSpaceEndLines { get; set; }
    
    /// <summary>
    /// 删除开头空白行, 包括空行/全是空格行
    /// </summary>
    public bool RemoveWhiteSpaceStartLines { get; set; }
}