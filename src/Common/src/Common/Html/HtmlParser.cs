﻿using System.Diagnostics;

namespace Common.Html;

public class HtmlParser
{
    private HtmlParser(string src)
    {
        Src = src;
    }
    
    public static Document Parse(string src)
    {
        var nodes = new HtmlParser(src.Replace("\r\n", "\n")).ParseNodes();
        return new Document(nodes);
    }

    private List<Node> ParseNodes()
    {
        var nodes = new List<Node>();
        SkipString(c => c is ' ' or '\n');
        while (!IsEof())
        {
            var node = NextNode(nodes.Count == 0 ? null : nodes[^1]);
            if (node is not null)
                nodes.Add(node);
        }

        return nodes;
    }

    /// <summary>
    /// 字符串
    /// </summary>
    public string Src { get; }
    
    /// <summary>
    /// 当前游标
    /// </summary>
    private int _pointer;

    private char PeekChar() => Src[_pointer];
    
    private char NextChar() => Src[_pointer++];

    private bool IsEof() => _pointer >= Src.Length;

    private string NextString(Func<char, bool> func)
    {
        var start = _pointer;
        while (!IsEof() && func(PeekChar()))
        {
            _pointer++;
        }

        return Src.Substring(start, _pointer - start);
    }

    private void Skip(int num) => _pointer += num;
    private void SkipString(Func<char, bool> func)
    {
        while (!IsEof() && func(PeekChar()))
        {
            _pointer++;
        }
    }

    private Node NextContent()
    {
        return new TextNode(NextString(c => c != '<'));
    }

    private static string[] SelfClosingTags = new[] { "img", "br" };
    private Node? NextTag()
    {
        var ch = NextChar();
        Debug.Assert(ch == '<');
        var tagName = NextString(c => IsNotWhitespace(c) && c != '>' && c != '/');
        SkipString(IsWhitespace);

        var attributes = new Dictionary<string, string?>();
        while (PeekChar() != '/' && PeekChar() != '>')
        {
            var kv = NextAttr();
            if (IsEof() || attributes.ContainsKey(kv.Item1))
                return null;
            attributes.Add(kv.Item1, kv.Item2);
            SkipString(IsWhitespace);
        }

        ch = NextChar();
        var children = new List<Node>();
        
        // self closing tag
        if (ch == '/' && PeekChar() == '>')
        {
            _pointer++;
            return new Tag(tagName, attributes, children, true);
        }
        
        Debug.Assert(ch == '>');
        
        if (SelfClosingTags.Any(t => t.Equals(tagName, StringComparison.OrdinalIgnoreCase)))
            return new Tag(tagName, attributes, children, true);

        while (!IsEof() && !(PeekChar() == '<' && Src[_pointer+1] == '/'))
        {
            var node = NextNode();
            if (node is not null)
                children.Add(node);
        }

        _pointer += 2;
        if (!IsEof())
        {
            var closingTagName = NextString(c => IsNotWhitespace(c) && c != '>');
            Debug.Assert(tagName == closingTagName);
            ch = NextChar();
            Debug.Assert(ch == '>');
        }

        return new Tag(tagName, attributes, children);
    }

    private (string, string?) NextAttr()
    {
        var name = NextString(c => IsNotWhitespace(c) && c != '=');
        string? value = null;
        if (!IsEof() && PeekChar() == '=')
        {
            Skip(1);
            var ch = NextChar();
            Debug.Assert(ch == '"');
            value = NextString(c => c != '"');
            Skip(1);
        }
        
        return (name, value);
    }
    private Node? NextNode(Node? brotherNode = null)
    {
        if (IsEof())
            return null;
        
        if (PeekChar() == '<')
        {
            while (!IsEof())
            {
                var startPtr = _pointer;
                var tag = NextTag();
                if (tag is not null)
                {
                    SkipString(c => c is ' ' or '\n');
                    return tag;
                }

                var content = Src[startPtr.._pointer];
                var nextContent = NextString(c => c != '<');
                if (brotherNode is TextNode textNode)
                    textNode.Content += content + nextContent;
                else 
                    return new TextNode(content + nextContent);
            }

            return null;
        }
        return NextContent();
    }

    private static bool IsWhitespace(char c) => c == ' ';
    private static bool IsNotWhitespace(char c) => c != ' ';
}

public class Tag : Node
{
    public Tag(string tagName, Dictionary<string, string?> attributes, List<Node> children, bool selfClosing = false)
    {
        TagName = tagName;
        Attributes = attributes;
        Children = children;
        SelfClosing = selfClosing;
    }

    public string TagName { get; set; }
    public bool SelfClosing { get; set; }
    public Dictionary<string, string?> Attributes { get; set; }
    public List<Node> Children { get; set; }
}

public class TextNode : Node
{
    public TextNode(string content)
    {
        Content = content;
    }

    public string Content { get; set; }
}

public class Node
{
    
}