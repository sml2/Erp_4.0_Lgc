﻿namespace ERP;

public static class Constants
{
    /// <summary>
    /// 超级管理员账号id
    /// </summary>
    public const int SuperId = 1;
    
    /// <summary>
    /// 小米开放注册用户 OEM
    /// </summary>
    public const int OemId = 1;
}