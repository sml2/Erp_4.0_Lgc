﻿using System.Reflection;
using ERP.Models.DB.Users;
using System.Security.Claims;
using ERP.Attributes.Rule;
using ERP.Enums.Rule.Config.Internal;
using ERP.Interface;
using ERP.Interface.Rule;
using ERP.Models.DB.Identity;
using Role = ERP.Enums.Identity.Role;

namespace ERP.Extensions
{
    public static class Rule
    {
        public static bool Has(this IEnumerable<UserClaim> userClaims, IHas hr)
        {
            return userClaims.Any(clm => clm.ClaimType.Equals(hr.Key) && Convert.ToBoolean(hr.Value & (Convert.ToInt64(clm.ClaimValue))));
        }
        
        public static bool Has<TEnum>(this IEnumerable<UserClaim> User, TEnum @enum) where TEnum : Enum=> User.Has(nameof(TEnum), Convert.ToInt64(@enum));

        public static bool Has(this IEnumerable<UserClaim> User, string EnumType, long value)
        {
            //严格模式
            //return User.HasClaim(clm => clm.Type.Equals(EnumType) && (value & Convert.ToInt64(clm.Value))== value);
            //宽松模式 容错 提高兼容性
            return User.Any(clm => clm.ClaimType.Equals(EnumType) && Convert.ToBoolean(value & (Convert.ToInt64(clm.ClaimValue)))) ;
        }
        public static bool Has(this RuleTemplate tpl, IHas hr) => Convert.ToBoolean(Convert.ToInt64(tpl.GetValue(hr.Key)) & hr.Value);
        public static long Has(this RuleTemplate tpl, IConfigVisitor icv) => (Convert.ToInt64(tpl.GetValue(icv.Key)) & icv.Value) >> icv.bit;
        public static bool Has(this IList<Claim> claims, IHas hr) => Convert.ToBoolean(Convert.ToInt64(claims.FirstOrDefault(c => c.Type.Equals(hr.Key))?.Value) & hr.Value);
        public static long Has(this IList<Claim> claims, IConfigVisitor icv) => (Convert.ToInt64(claims.FirstOrDefault(c => c.Type.Equals(icv.Key))?.Value) & icv.Value)  >> icv.bit;
        public static bool Has(this ClaimsPrincipal User, string EnumType, long value)
        {
            //严格模式
            //return User.HasClaim(clm => clm.Type.Equals(EnumType) && (value & Convert.ToInt64(clm.Value))== value);
            //宽松模式 容错 提高兼容性
            return User.HasClaim(clm => clm.Type.Equals(EnumType) && Convert.ToBoolean(value & (Convert.ToInt64(clm.Value)))) ;
        }
        public static bool Has(this ClaimsPrincipal User, IHas hr) => User.Has(hr.Key, hr.Value);

        public static bool Has<TEnum>(this ClaimsPrincipal User, TEnum @enum) where TEnum : Enum
        {
            var type = typeof(TEnum);
            var propName = type.Namespace!.EndsWith("Config")
                ? $"{type.Name}.{nameof(IConfig.Visible)}"
                : $"{type.Name}Menu";
            return User.Has(propName, Convert.ToInt64(@enum));
        }

        public static bool Has(this ClaimsPrincipal User, string EnumType, Enum value) => User.Has(EnumType, Convert.ToInt64(value));
        public static bool HasEditable(this ClaimsPrincipal User, string EnumType, Enum value) => User.Has($"{EnumType}.{nameof(IConfig.Editable)}", Convert.ToInt64(value));
        public static bool IsInRole(this ClaimsPrincipal User, Enums.Identity.Role role) => User.IsInRole(Enum.GetName(role)!);
        public static Dictionary<string, Dictionary<string, bool>> GetRules(this ClaimsPrincipal User, IManager rm) => rm.GetRules(User);

        public static long GetMenuValue(this ClaimsPrincipal user, Enum value) =>
            GetPermissionValue(user, "Menu", value);
        public static long GetVisibleValue(this ClaimsPrincipal user, Enum value) =>
            GetPermissionValue(user, "Visible", value);
        public static long GetEditableValue(this ClaimsPrincipal user, Enum value) =>
            GetPermissionValue(user, "Editable", value);

        public static DataRange_2b GetProductModuleRange(this ClaimsPrincipal principal, Enum config, User user)
        {
            var range = (DataRange_2b)principal.GetEditableValue(config);
            if (user.Role == Role.ADMIN || user.Role == Role.DEV)
            {
                range = DataRange_2b.Company;
            }
            return range;
        }
        
        /// <summary>
        /// 从claims中获取权限值
        /// </summary>
        /// <param name="user"></param>
        /// <param name="ruleType"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private static long GetPermissionValue(ClaimsPrincipal user, string ruleType, Enum value)
        {
            var key = $"{value.GetType().Name}{(ruleType == "Menu" ? "" : ".")}{ruleType}";
            var permission = user.Claims.FirstOrDefault(c => c.Type == key)?.Value ?? "0";
            var longPerm = Convert.ToInt64(permission);
            var longValue = Convert.ToInt64(value);
            int? shift = null;
            var type = value.GetType();
            var name = type.GetEnumName(value);
            if (!string.IsNullOrEmpty(name))
            {
                var field = type.GetField(name);
                if (field is not null)
                {
                    var select = (ERP.Authorization.Rule.Component.Select?)field.GetCustomAttribute(typeof(ERP.Authorization.Rule.Component.Select), true);
                    shift = select?.Shift ?? null;
                }
            }

            return shift == null ? 0 : (longPerm & longValue) >> shift.Value;
        }
        
        public static long GetValue(this RuleTemplate tpl, string key)
        {
            if (key.Contains('.'))
            {
                var s = key.Split('.');
                var c = (IConfig)tpl.GetPropOrFieldValue(s[0]);
                return s[1].Equals(nameof(Models.DB.Rule.Config<Enums.Rule.Flag>.Editable)) ? c.Editable : c.Visible;
            }
            return Convert.ToInt64(tpl.GetPropOrFieldValue(key));
        }
        public static void SetValue(this RuleTemplate tpl, string key, long value)
        {
            if (key.Contains('.'))
            {
                var s = key.Split('.');
                var c = (IConfig)tpl.GetPropOrFieldValue(s[0]);
                if (s[1].Equals(nameof(IConfig.Editable)))
                    c.Editable = value;
                else
                    c.Visible = value;
            }
            else 
                tpl.SetPropOrFieldValue(key, value);
        }


        public static string? GetSessionId(this ClaimsPrincipal user)
        {
            return user.Claims.LastOrDefault(c => c.Type == "SessionId")?.Value;
        }
    }
}
