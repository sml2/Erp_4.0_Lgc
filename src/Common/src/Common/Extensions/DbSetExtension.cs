﻿using System.Linq.Expressions;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;

namespace ERP.Extensions;
public static class DbSetExtension
{

    public static async Task<PaginateStruct<T>> ToPaginateAsync<T>(this IQueryable<T> dbSet, int page = -1,
        int limit = -1)
    {
        var count = await dbSet.CountAsync();
        var (pageSize, currentPage) = PaginatorProperties.Instance.CalcLimitAndPage(limit, page, count);
        
        if (count == 0)
        {
            return new PaginateStruct<T>() { CurrentPage = 1, PageSize = pageSize, Items = new List<T>() };
        }
        
        return new PaginateStruct<T>()
        {
            Total = count,
            CurrentPage = currentPage,
            PageSize = pageSize,
            Items = await dbSet.Skip((currentPage - 1) * pageSize).Take(pageSize).ToListAsync()
        };
    }
    
    public static async Task<PaginateStruct<TResult, G>> ToPaginateAsync<T, G, TResult>(this IQueryable<T> dbSet, Expression<Func<IGrouping<int, T>, G>> group,
        Expression<Func<T, TResult>> selector, int page = -1, int limit = -1)
    {
        var result = await dbSet.GroupBy(g => 1).Select(group).FirstAsync();
        var count = await dbSet.CountAsync();
        var (pageSize, currentPage) = PaginatorProperties.Instance.CalcLimitAndPage(limit, page, count);
        
        if (count == 0)
        {
            return new PaginateStruct<TResult, G>() { CurrentPage = 1, PageSize = pageSize, Items = new List<TResult>() };
        }
        
        return new PaginateStruct<TResult, G>()
        {
            Total = count,
            CurrentPage = currentPage,
            PageSize = pageSize,
            Group = result,
            Items = await dbSet.Select(selector).Skip((currentPage - 1) * pageSize).Take(pageSize).ToListAsync()
        };
    }

    public static Task<PaginateStruct<T>> ToFakePaginateAsync<T>(this IQueryable<T> dbSet, HttpRequest request, int ratio = 0)
        where T : class
    {
        if (request == null) throw new ArgumentNullException(nameof(request));

        request.TryGetValue("page", out var page);
        request.TryGetValue("limit", out var limit);

        var intPage = Convert.ToInt32(page);
        intPage = intPage < 1 ? 1 : intPage;
        var intLimit = Convert.ToInt32(limit);
        return dbSet.ToFakePaginateAsync(intPage, intLimit, ratio);
    }

    public static async Task<PaginateStruct<T>> ToFakePaginateAsync<T>(this IQueryable<T> dbSet, int page = 1,
        int limit = 20, int ratio = 0) where T : class
    {
        if (ratio <= 0)
        {
            return await dbSet.ToPaginateAsync(page, limit);
        }
        var count = await dbSet.CountAsync();
        PaginateStruct<T> result;
        if (count / limit > page - 1)
        {
            result = await dbSet.ToPaginateAsync(page, limit);
        }
        else
        {
            limit = limit < 10 ? 10 : limit;
            var maxPage = (int)Math.Ceiling((double)count / limit);
            var currentPage = page > maxPage ? maxPage : (page < 1 ? 1 : page);
            result = new PaginateStruct<T>()
            {
                Total = count,
                CurrentPage = currentPage,
                PageSize = limit,
                Items = await dbSet.Skip((currentPage - 1) * limit).Take(limit).ToListAsync()
            };
        }

        result.Total = count * (ratio + 1);
        return result;
    }

    public static PaginateStruct<T> ToPaginate<T>(this IEnumerable<T> dbSet, int page = 1,
        int limit = 20) where T : class
    {
        var enumerable = dbSet.ToList();
        var count = enumerable.Count;
        var (pageSize, currentPage) = PaginatorProperties.Instance.CalcLimitAndPage(limit, page, count);
        if (count == 0)
        {
            return new PaginateStruct<T>() { CurrentPage = 1, PageSize = pageSize, Items = new List<T>() };
        }


        return new PaginateStruct<T>()
        {
            Total = count,
            CurrentPage = currentPage,
            PageSize = pageSize,
            Items = enumerable.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList()
        };
    }

    public class PaginateStruct<T>
    {
        public int Total { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; init; }
        public List<T> Items { get; init; }=new List<T>();

        public PaginateStruct<TO> Transform<TO>(Func<T, TO> trans) where TO : class
        {
            return new()
            {
                Total = Total,
                CurrentPage = CurrentPage,
                PageSize = PageSize,
                Items = Items.Select(trans).ToList(),
            };
        }
    }

    public class PaginateStruct<T, G> : PaginateStruct<T>
    {
        public G Group { get; set; }
        
        public PaginateStruct<TO, G> Transform<TO>(Func<T, TO> trans) where TO : class
        {
            return new()
            {
                Total = Total,
                CurrentPage = CurrentPage,
                PageSize = PageSize,
                Items = Items.Select(trans).ToList(),
                Group = Group
            };
        }
    }


    //public static void SoftRemove<TEntity>(this IQueryable<TEntity> dbSet, TEntity entity)
    //    where TEntity : ISoftDelete
    //{
    //    entity.IsDeleted = true;
    //}

    //public static void Restore<TEntity>(this IQueryable<TEntity> dbSet, TEntity entity) where TEntity : ISoftDelete
    //{
    //    entity.IsDeleted = false;
    //}

    //public static void ForceRemove<TEntity>(this DbSet<TEntity> dbSet, TEntity entity)
    //    where TEntity : class, ISoftDelete
    //{
    //    dbSet.Remove(entity);
    //}

    /// <summary>
    /// 根据当条件为真时添加where语句
    /// </summary>
    /// <param name="dbSet"></param>
    /// <param name="condition"></param>
    /// <param name="predicate"></param>
    /// <typeparam name="TSource"></typeparam>
    /// <returns></returns>
    public static IQueryable<TSource> WhenWhere<TSource>(this IQueryable<TSource> dbSet, bool condition,
        Expression<Func<TSource, bool>> predicate) where TSource : class
    {
        return condition ? dbSet.Where(predicate) : dbSet;
    }
    
    public static IQueryable<TSource> WhenWhere<TSource>(this IQueryable<TSource> dbSet, bool condition,
        Func<IQueryable<TSource>, IQueryable<TSource>> predicate) where TSource : class
    {
        return condition ? predicate(dbSet) : dbSet;
    }



    public static IQueryable<TSource> OrderByDynamic<TSource>(this IQueryable<TSource> dbSet, bool condition,
        Action<IQueryable<TSource>> predicate) where TSource : class
    {
        if (condition) { predicate(dbSet); }
        return dbSet;
    }

    public static IEnumerable<TSource> WhenWhere<TSource>(this IEnumerable<TSource> dbSet, bool condition,
        Func<TSource, bool> predicate) where TSource : class
    {
        return condition ? dbSet.Where(predicate) : dbSet;
    }

    private static Dictionary<Type, object> _primaryKeyDic = new();

    public static IQueryable<TSource> WhereIn<TSource, TId>(this IQueryable<TSource> dbSet, TId[] ids)
        where TSource : class
    {
        Func<TSource, TId> lam;
        if (!_primaryKeyDic.ContainsKey(typeof(TSource)) && dbSet is DbSet<TSource> DBSet)
        {
            var key = DBSet.EntityType.FindPrimaryKey()!.Properties.First().Name;
            var property = typeof(TSource).GetProperty(key);
            var paramSource = Expression.Parameter(typeof(TSource));
            var val = Expression.Call(Expression.Constant(property), nameof(property.GetValue), null, paramSource);
            var intVal = Expression.Call(null, typeof(Convert).GetMethod(nameof(Convert.ToInt32), new[] { typeof(object) })!, val);
            
            var res = Expression.Lambda<Func<TSource, TId>>(intVal, paramSource);
            lam = res.Compile();
            _primaryKeyDic.Add(typeof(TSource), lam);
        }
        else
        {
            lam = (Func<TSource, TId>)_primaryKeyDic[typeof(TSource)];
        }
        return dbSet.Where(m => ids.Contains(lam(m)));
    }

    /// <summary>
    /// 去掉重复的key
    /// </summary>
    /// <param name="source"></param>
    /// <param name="keySelector"></param>
    /// <typeparam name="TSource"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    /// <returns></returns>
    public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
    {
        var seenKeys = new HashSet<TKey>();
        foreach (var element in source)
        {
            if (seenKeys.Add(keySelector(element)))
            {
                yield return element;
            }
        }
    }
    
    public static IEnumerable<TSource> Shuffle<TSource>(this IEnumerable<TSource> sources)
    {
        return sources.OrderBy(m => Random.Shared.Next());
    }

    public static IQueryable<TSource> Increment<TSource>(this IQueryable<TSource> sources, Func<TSource, int> selector, int num = 1) where TSource : class
    {
        return sources;
    }

    public static IQueryable<TSource> Decrement<TSource>(this IQueryable<TSource> sources, Func<TSource, int> selector, int num = 1) where TSource : class
    {
        return sources;
    }

    //public static IQueryable<TSource> FromCache<TSource>(this IQueryable<TSource> sources, Func<TSource, int> selector, int num = 1) where TSource : class
    //{
    //    return sources;
    //}
}

public class PaginatorOption
{
    public static readonly PaginatorOption Default = new();
    
    /// <summary>
    /// 分页数量
    /// </summary>
    public int DefaultLimit = 20;

    /// <summary>
    /// 请求query中标识当前页数的key
    /// </summary>
    public string PageKey = "page";
    
    /// <summary>
    /// 请求query中标识分页数量的key
    /// </summary>
    public string LimitKey = "limit";
}

public class PaginatorProperties
{
    private readonly IHttpContextAccessor _accessor;
    private readonly PaginatorOption _options;
    
    private PaginatorProperties(IHttpContextAccessor accessor, IOptions<PaginatorOption> options)
    {
        _accessor = accessor;
        _options = options.Value;
    }

    private static PaginatorProperties? _instance;
    public static PaginatorProperties Instance => _instance ?? throw new ArgumentNullException(nameof(_instance), "没有使用UsePaginator方法");

    public static void CreateInstance(IHttpContextAccessor accessor, IOptions<PaginatorOption> options)
    {
        _instance = new PaginatorProperties(accessor, options);
    }

    /// <summary>
    /// 当前请求的页数
    /// </summary>
    public int Page => _accessor.HttpContext!.Request.TryGetValue(_options.PageKey, out var page) && int.TryParse(page, out var v) ?  v : 1;
    
    /// <summary>
    /// 当前请求的分页大小
    /// </summary>
    public int Limit => _accessor.HttpContext!.Request.TryGetValue(_options.LimitKey, out var limit) && int.TryParse(limit, out var v) ?  v : _options.DefaultLimit;

    public (int limit, int page) CalcLimitAndPage(int? maybeLimit, int? maybePage, int totalCount)
    {
        var limit = maybeLimit is > 0 ? maybeLimit.Value : Limit;
        var maxPage = (int)Math.Ceiling((double)totalCount / limit);
        var currentPage = maybePage is > 0 ? maybePage.Value : Page;
        currentPage = currentPage > maxPage && maxPage > 0 ? maxPage : currentPage;
        return (limit, currentPage);
    }
}

public static class PaginatorExtension
{
    public static void UsePaginator(this IApplicationBuilder app)
    {
        app.UsePaginator(_ => { });
    }
    
    public static void UsePaginator(this IApplicationBuilder app, Action<PaginatorOption> action)
    {
        var option = Options.Create(PaginatorOption.Default);
        action(option.Value);
        PaginatorProperties.CreateInstance(app.ApplicationServices.GetRequiredService<IHttpContextAccessor>(), option);
    }
}
