﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;
using ERP.Models.Finance;

namespace ERP.Extensions;
public static class DateTimeExtension
{
    public static DateTime BeginDayTick(this DateTime t) => t.Date;
    // public static DateTime LastDayTick(this DateTime t) => t.Date.AddDays(1).AddSeconds(-1);
    public static DateTime LastDayTick(this DateTime t) => t.Date.AddDays(1).AddTicks(-1);
    public static DateTime BeginMonthTick(this DateTime t) => t.AddDays(1 - t.Day).Date;
    public static DateTime LastMonthTick(this DateTime t) => t.AddDays(1 - t.Day).AddMonths(1).AddDays(-1).LastDayTick();
    public static DateTime BeginYearTick(this DateTime t) => t.AddMonths(1 - t.Month).AddDays(1 - t.Day).Date;
    public static DateTime LastYearTick(this DateTime t) => t.AddMonths(1 - t.Month).AddYears(1).AddMonths(-1).LastMonthTick();
}
