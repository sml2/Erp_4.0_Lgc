﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;
using ERP.Attributes;

namespace ERP.Extensions;

public static class EnumExtension
{
    public static string GetDescription(this Enum @enum, string @default = "")
    {
        var enumType = @enum.GetType();
        var value = @enum.GetNumber();
        var fieldInfo = enumType.GetField(Enum.GetName(enumType, value)!);
        if (fieldInfo!.GetCustomAttribute(typeof(DescriptionAttribute), false) is DescriptionAttribute descriptionAttribute)
        {
            return descriptionAttribute.Description;
        }
        return !string.IsNullOrEmpty(@default) ? @default : @enum.ToString();
    }

    public static string GetTranslationCode(this Enum @enum,string @default)
    {
        var enumType = @enum.GetType();
        var value = @enum.GetLong();
        var fieldInfo = enumType.GetField(Enum.GetName(enumType, value)!);
        if (fieldInfo!.GetCustomAttribute(typeof(TranslationAttribute), false) is TranslationAttribute translationAttribute)
        {
            return !string.IsNullOrEmpty(translationAttribute.Code) ? translationAttribute.Code : @default;
        }

        return @default;
    }
    public static int GetID(this Enum @enum) => @enum.GetNumber();
    public static int GetNumber(this Enum @enum) => int.Parse($"{@enum:d}");
    public static long GetLong(this Enum @enum) => long.Parse($"{@enum:d}");
    public static ulong GetULong(this Enum @enum) => ulong.Parse($"{@enum:d}");

    public static string GetName<TEnum>(this TEnum @enum) where TEnum : struct, Enum => Enum.GetName(@enum)!;

    public static Dictionary<int, string> ToDictionaryForUI(this IEnumerable<Enum> enums) => enums.ToDictionary(x => x.GetNumber(), x => Enum.GetName(x.GetType(), x)!);
    public static Dictionary<int, string> ToDictionaryForUIByDescription(this IEnumerable<Enum> enums) => enums.ToDictionary(x => x.GetNumber(), x => x.GetDescription());
    public static Dictionary<string, string> ToDictionaryValueWithDescription(this IEnumerable<Enum> enums) => enums.ToDictionary(x => Enum.GetName(x.GetType(), x)!, x => x.GetDescription());
}
public static class Enum<TEnum> where TEnum : struct, Enum, IConvertible
{
    public static Dictionary<int, string> ToDictionaryForUI() => Enum.GetValues(typeof(TEnum)).Cast<int>().ToDictionary(x => x, x => Enum.GetName(typeof(TEnum), x)!);
    public static Dictionary<int, string> ToDictionaryForUIByDescription() => Enum.GetValues(typeof(TEnum)).Cast<Enum>().ToDictionaryForUIByDescription();
    public static Dictionary<string, string> ToDictionaryValueWithDescription() => Enum.GetValues(typeof(TEnum)).Cast<Enum>().ToDictionaryValueWithDescription();
}