﻿using ERP.Models.DB.Users;
using System.Diagnostics;
using Common.Enums.Products;
using static ERP.Models.DB.Users.Company;
using ERP.Models.DB.Identity;
using Exception = ERP.Exceptions.Exception;

namespace ERP.Extensions
{
    public static class ISessionExtension
    {
        [DebuggerStepThrough()]
        public static string GetStringWithoutNullable(this ISession ISession, string key)
        {
            var v = ISession.GetString(key);
            if (v is not null) return v;
            throw new Exception("Object reference not set to an instance of an object");
        }

       [DebuggerStepThrough()]
        public static int GetInt32WithoutNullable(this ISession ISession, string key)
        {
            var v = ISession.GetInt32(key);
            if (v.HasValue) { return v.Value; }
            throw new Exception("Object reference not set to an instance of an object");
            //throw new NullReferenceException();  无法捕捉NullReferenceException
        }
        
        [DebuggerStepThrough()]
        public static bool GetBooleanWithoutNullable(this ISession ISession, string key)
        {
            var v = ISession.GetString(key);
            if (v is not null)
            {
                return Convert.ToBoolean(v);
            }
            return false;
        }

        const string UserID = nameof(UserID);
        private static void SetUserID(this ISession ISession, int id) => ISession.SetInt32(UserID, id);
        public static int GetUserID(this ISession ISession) => ISession.GetInt32WithoutNullable(UserID);
        
        const string FormRuleTemplateID = nameof(FormRuleTemplateID);
        private static void SetFormRuleTemplateID(this ISession ISession, int id) => ISession.SetInt32(FormRuleTemplateID, id);
        public static int GetFormRuleTemplateID(this ISession ISession) => ISession.GetInt32WithoutNullable(FormRuleTemplateID);

        const string GroupID = nameof(GroupID);
        private static void SetGroupID(this ISession ISession, int id) => ISession.SetInt32(GroupID, id);
        public static int GetGroupID(this ISession ISession) => ISession.GetInt32WithoutNullable(GroupID);

        const string CompanyID = nameof(CompanyID);
        private static void SetCompanyID(this ISession ISession, int id) => ISession.SetInt32(CompanyID, id);
        public static int GetCompanyID(this ISession ISession) => ISession.GetInt32WithoutNullable(CompanyID);

        //Role
        const string Role = nameof(Role);
        public static void SetRole(this ISession ISession, Enums.Identity.Role role) => ISession.SetInt32(Role, (int)role);
        public static Enums.Identity.Role GetRole(this ISession ISession) => (Enums.Identity.Role)ISession.GetInt32WithoutNullable(Role);
        public static bool Is(this ISession ISession, Enums.Identity.Role role) => ISession.GetRole() == role;

        const string OEMID = nameof(OEMID);
        private static void SetOEMID(this ISession ISession, int id) => ISession.SetInt32(OEMID, id);
        public static int GetOEMID(this ISession ISession) => ISession.GetInt32WithoutNullable(OEMID);

        const string UserName = nameof(UserName);
        private static void SetUserName(this ISession ISession, string name) => ISession.SetString(UserName, name);
        public static string GetUserName(this ISession ISession) => ISession.GetStringWithoutNullable(UserName);

        const string TrueName = nameof(TrueName);
        private static void SetTrueName(this ISession ISession, string name) => ISession.SetString(TrueName, name);
        public static string GetTrueName(this ISession ISession) => ISession.GetStringWithoutNullable(TrueName);

        const string DisplayName = nameof(DisplayName);
        private static void SetDisplayName(this ISession ISession, string name) => ISession.SetString(DisplayName, name);
        public static string GetDisplayName(this ISession ISession) => ISession.GetStringWithoutNullable(DisplayName);

        const string Email = nameof(Email);
        private static void SetEmail(this ISession ISession, string email) => ISession.SetString(Email, email);
        public static string GetEmail(this ISession ISession) => ISession.GetStringWithoutNullable(Email);

        const string PhoneNumber = nameof(PhoneNumber);
        private static void SetPhoneNumber(this ISession ISession, string value) => ISession.SetString(PhoneNumber, value);
        public static string GetPhoneNumber(this ISession ISession) => ISession.GetStringWithoutNullable(PhoneNumber);
        
        const string PhoneNumberConfirmed = nameof(PhoneNumberConfirmed);
        private static void SetPhoneNumberConfirmed(this ISession ISession, string value) => ISession.SetString(PhoneNumberConfirmed, value);
        public static bool GetPhoneNumberConfirmed(this ISession ISession) => ISession.GetBooleanWithoutNullable(PhoneNumberConfirmed);

        const string CreatedAt = nameof(CreatedAt);
        private static void SetCreatedAt(this ISession ISession, string value) => ISession.SetString(CreatedAt, value);
        public static string GetCreatedAt(this ISession ISession) => ISession.GetStringWithoutNullable(CreatedAt);
        //level
        const string Level = nameof(Level);
        private static void SetLevel(this ISession ISession, int value) => ISession.SetInt32(Level, value);
        public static int GetLevel(this ISession ISession) => ISession.GetInt32WithoutNullable(Level);

        const string UserPids = nameof(UserPids);
        private static void SetUserPids(this ISession ISession, string pids) => ISession.SetString(UserPids, pids);
        public static string GetUserPids(this ISession ISession) => ISession.GetStringWithoutNullable(UserPids);
        
        const string CompanyPids = nameof(CompanyPids);
        private static void SetCompanyPids(this ISession ISession, string pids) => ISession.SetString(CompanyPids, pids);
        public static string GetCompanyPids(this ISession ISession) => ISession.GetStringWithoutNullable(CompanyPids);
        
        
        const string ProductTranslation = nameof(ProductTranslation);
        private static void SetProductTranslation(this ISession ISession, ProductTranslationEnum pt) => ISession.SetInt32(ProductTranslation, pt.GetNumber());
        public static ProductTranslationEnum GetProductTranslation(this ISession ISession) => (ProductTranslationEnum)ISession.GetInt32WithoutNullable(ProductTranslation);
        
        const string ProductEditor = nameof(ProductEditor);
        private static void SetProductEditor(this ISession ISession, DescriptionTypeEnum pe) => ISession.SetInt32(ProductEditor, pe.GetNumber());
        public static DescriptionTypeEnum GetProductEditor(this ISession ISession) => (DescriptionTypeEnum)ISession.GetInt32WithoutNullable(ProductEditor);
        
        

        //unit
        const string UnitConfig = nameof(UnitConfig);
        public static void SetUnitConfig(this ISession ISession, string unit) => ISession.SetString(UnitConfig, unit);
        public static string GetUnitConfig(this ISession ISession) => ISession.GetStringWithoutNullable(UnitConfig);
        
        //productShowUnit
        const string ProductShowUnit = nameof(ProductShowUnit);
        public static void SetProductShowUnit(this ISession ISession, string state) => ISession.SetString(ProductShowUnit, state);
        public static bool GetProductShowUnit(this ISession ISession) => ISession.GetBooleanWithoutNullable(ProductShowUnit);

        #region company
        const string IsDistribution = nameof(IsDistribution);
        public static void SetIsDistribution(this ISession ISession, bool value) => ISession.SetInt32(IsDistribution, Convert.ToInt32(value));
        public static bool GetIsDistribution(this ISession ISession) => Convert.ToBoolean( ISession.GetInt32WithoutNullable(IsDistribution));

        const string ShowOemId = nameof(ShowOemId);
        private static void SetShowOemId(this ISession ISession, int value) => ISession.SetInt32(ShowOemId, value);
        public static int GetShowOemId(this ISession ISession) => ISession.GetInt32WithoutNullable(ShowOemId);

        const string Source = nameof(Source);
        private static void SetSource(this ISession ISession, Sources value) => ISession.SetInt32(Source, (int)value);
        public static Sources GetSource(this ISession ISession) => (Sources)ISession.GetInt32WithoutNullable(Source);

        const string RealityValidityId = nameof(RealityValidityId);
        private static void SetRealityValidityId(this ISession ISession, int value) => ISession.SetInt32(RealityValidityId, value);
        public static int GetRealityValidityId(this ISession ISession) => ISession.GetInt32WithoutNullable(RealityValidityId);

        const string ShowValidityId = nameof(ShowValidityId);
        private static void SetShowValidityId(this ISession ISession, int value) => ISession.SetInt32(ShowValidityId, value);
        public static int GetShowValidityId(this ISession ISession) => ISession.GetInt32WithoutNullable(ShowValidityId);

        const string ShowValidity = nameof(ShowValidity);
        private static void SetShowValidity(this ISession ISession, string value) => ISession.SetString(ShowValidity, value.ToString());
        public static DateTime GetShowValidity(this ISession ISession) => Convert.ToDateTime(ISession.GetStringWithoutNullable(ShowValidity));
        
        const string ReportId = nameof(ReportId);
        private static void SetReportId(this ISession ISession, int value) => ISession.SetInt32(ReportId, value);
        public static int GetReportId(this ISession ISession) => ISession.GetInt32WithoutNullable(ReportId);

        const string ReportPids = nameof(ReportPids);
        private static void SetReportPids(this ISession ISession, string value) => ISession.SetString(ReportPids, value);
        public static string GetReportPids(this ISession ISession) => ISession.GetStringWithoutNullable(ReportPids);

        #endregion



        //OrderRate
        const string OrderRate = nameof(OrderRate);
        public static void SetOrderRate(this ISession ISession, string rate) => ISession.SetString(OrderRate, rate);
        public static string GetOrderRate(this ISession ISession) => ISession.GetStringWithoutNullable(OrderRate);

        //OrderPulling
        const string OrderPulling= nameof(OrderPulling);
        public static void SetOrderPulling(this ISession ISession, Guid? doing) {
            if (doing is not null) {
                ISession.SetString(OrderPulling, doing.ToString()!);
            }
            else {
                ISession.Remove(OrderPulling);
            }
        }
        public static Guid GetOrderPulling(this ISession ISession)
        {
            var value = ISession.GetString(OrderPulling);
            return value == null ? Guid.Empty : Guid.Parse(value);
        }
        
        //WaybillPulling
        
        const string WaybillFeePulling= nameof(WaybillFeePulling);
        
        public static Guid GetWaybillFeePulling(this ISession ISession)
        {
            var value = ISession.GetString(WaybillFeePulling);
            return value == null ? Guid.Empty : Guid.Parse(value);
        }
        
        public static void SetWaybillFeePulling(this ISession ISession, Guid? doing) {
            if (doing is not null) {
                ISession.SetString(WaybillFeePulling, doing.ToString()!);
            }
            else {
                ISession.Remove(WaybillFeePulling);
            }
        }


        //pushAmazonProduct
        const string AmazonProduct = nameof(AmazonProduct);
        public static void SetAmazonProduct(this ISession ISession, Guid? doing)
        {
            if (doing is not null)
            {
                ISession.SetString(AmazonProduct, doing.ToString()!);
            }
            else
            {
                ISession.Remove(AmazonProduct);
            }
        }
        public static Guid GetAmazonProduct(this ISession ISession)
        {
            var value = ISession.GetString(AmazonProduct);
            return value == null ? Guid.Empty : Guid.Parse(value);
        }


        //SyncOrder 
        const string SyncOrder = nameof(SyncOrder);

        const string sync = "SyncOrder";
        public static void SetSyncOrder(this ISession ISession,string orderNo, Guid? doing)
        {
            if (doing is not null)
            {
                ISession.SetString(GetUpdateOrder(sync, orderNo), doing.ToString()!);
            }
            else
            {
                ISession.Remove(GetUpdateOrder(sync, orderNo));
            }
        }
        public static Guid GetSyncOrder(this ISession ISession,string orderNo)
        {
            var value = ISession.GetString(GetUpdateOrder(sync, orderNo));
            return value == null ? Guid.Empty : Guid.Parse(value);
        }




        //UpdateOrder 
        const string UpdateOrder = nameof(UpdateOrder);

        public static  string GetUpdateOrder(string flag,string orderNo)
        {
            return  flag+ orderNo;
        }

        const string update = "UpdateOrder";

        public static void SetUpdateOrder(this ISession ISession,string orderNo, Guid? doing)
        {
            
            if (doing is not null)
            {
                ISession.SetString(GetUpdateOrder(update,orderNo), doing.ToString()!);
            }
            else
            {
                ISession.Remove(GetUpdateOrder(update,orderNo));
            }
        }


        public static Guid GetUpdateOrder(this ISession ISession,string orderNo)
        {
            var value = ISession.GetString(GetUpdateOrder(update,orderNo));
            return value == null ? Guid.Empty : Guid.Parse(value);
        }

        //GetProductByAsins
        const string AsinProduct = nameof(AsinProduct);
        public static void SetAsinProduct(this ISession ISession, Guid? doing)
        {
            if (doing is not null)
            {
                ISession.SetString(AsinProduct, doing.ToString()!);
            }
            else
            {
                ISession.Remove(AsinProduct);
            }
        }
        public static Guid GetAsinProduct(this ISession ISession)
        {
            var value = ISession.GetString(AsinProduct);
            return value == null ? Guid.Empty : Guid.Parse(value);
        }


        public static void SetUser(this ISession ISession, User u)
        {
            ISession.SetUserID(u.Id);
            ISession.SetFormRuleTemplateID(u.FormRuleTemplateID);
            ISession.SetGroupID(u.GroupID);
            ISession.SetCompanyID(u.CompanyID);
            ISession.SetRole(u.Role);
            ISession.SetOEMID(u.OEMID);
            ISession.SetUserName(u.UserName == null ? "" : u.UserName);
            ISession.SetTrueName(u.TrueName == null ? "" : u.TrueName);
            ISession.SetDisplayName(u.DisplayName == null ? "" : u.DisplayName);
            ISession.SetEmail(u.Email == null ? "" : u.Email);
            ISession.SetPhoneNumber(u.PhoneNumber == null ? "" : u.PhoneNumber);
            ISession.SetCreatedAt(u.CreatedAt.ToString("yyyy-MM-DD HH:mm:ss"));
            ISession.SetLevel(1);
            ISession.SetUnitConfig(u.UnitConfig);
            ISession.SetProductShowUnit(u.ProductConfig.ShowUnit.ToString());
            ISession.SetUserPids(u.Pids ?? "[]");
        }

        public static void SetCompany(this ISession ISession, Company c)
        {
            ISession.SetIsDistribution(c.IsDistribution);
            ISession.SetShowOemId(c.ShowOemId);
            ISession.SetShowValidity(c.ShowValidityInfo);
            ISession.SetSource(c.Source);
            ISession.SetReportId(c.ReportId);
            ISession.SetReportPids(c.ReportPids);
            ISession.SetCompanyPids(c.Pids);
            ISession.SetCompanyFuncItem(c);
        }
        
        public static void SetCompanyFuncItem(this ISession ISession,Company c)
        {
            //产品翻译
            var pt = c.ProductTranslation;
            if (pt == 0)
                pt = ProductTranslationEnum.Free;
            ISession.SetProductTranslation(pt);
            
            //产品描述编辑器
            var pe = c.ProductEditor;
            if (pe == 0)
                pe = DescriptionTypeEnum.PlainText;
            ISession.SetProductEditor(pe);
        }

        public static void SetMobile(this ISession ISession,string mobile)
        {
            ISession.SetPhoneNumber(mobile);
            ISession.SetPhoneNumberConfirmed("true"); 
        }
    }
}
