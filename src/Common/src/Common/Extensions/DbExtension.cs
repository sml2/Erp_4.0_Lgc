﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace ERP.Extensions;
public static class DbExtension
{
#pragma warning disable CS8622 // 参数类型中引用类型的为 Null 性与目标委托不匹配(可能是由于为 Null 性特性)。
    public static void HandleDataChanged(this DbContext dbContext)
    {
        dbContext.ChangeTracker.StateChanged += UpdateTimestamps;
        dbContext.ChangeTracker.Tracked += UpdateTimestamps;
    }

    public static void RemoveDataChanged(this DbContext dbContext)
    {
        dbContext.ChangeTracker.StateChanged -= UpdateTimestamps;
        dbContext.ChangeTracker.Tracked -= UpdateTimestamps;

    }
#pragma warning restore CS8622 // 参数类型中引用类型的为 Null 性与目标委托不匹配(可能是由于为 Null 性特性)。
    private static void UpdateTimestamps(object sender, EntityEntryEventArgs e)
    {
        if (e.Entry.Entity is IHasTimestamps entityWithTimestamps)
        {
            switch (e.Entry.State)
            {
                case EntityState.Deleted:
                    //entityWithTimestamps.Deleted = DateTime.UtcNow;
                    Console.WriteLine($"Stamped for delete: {e.Entry.Entity}");
                    break;
                case EntityState.Modified:
                    entityWithTimestamps.UpdatedAt = DateTime.Now;
                    break;
                case EntityState.Added:
                    entityWithTimestamps.CreatedAt = DateTime.Now;
                    entityWithTimestamps.UpdatedAt = DateTime.Now;
                    break;
            }
        }
    }


#pragma warning disable EF1001 // Internal EF Core API usage.
    public static ModelBuilder RemoveForeignKeys(this ModelBuilder modelBuilder)
    {
        var entityTypes = modelBuilder.Model.GetEntityTypes().ToList();

        for (int i = 0; i < entityTypes.Count(); i++)
        {
            var entityType = entityTypes[i];

            var references = entityType.GetDeclaredReferencingForeignKeys().ToList();

            using (((Model)entityType.Model).Builder.Metadata.ConventionDispatcher.DelayConventions())
            {
                foreach (var reference in references)
                {
                    reference.DeclaringEntityType.RemoveForeignKey(reference);
                }
            }
        }
        return modelBuilder;
    }
#pragma warning restore EF1001 // Internal EF Core API usage.
}

public interface IHasTimestamps
{
    public DateTime CreatedAt { get; set; }

    public DateTime UpdatedAt { get; set; }
}