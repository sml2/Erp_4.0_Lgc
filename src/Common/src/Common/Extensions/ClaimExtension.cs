﻿using System.Security.Claims;
namespace ERP.Extensions;
using ICIFC = Interface.ICanInitializeFromClaim;
using ErpClaims = Authorization.Claim;
public static class ClaimExtension
{
    public static void Initialize(this Claim me, ICIFC Target)
    {
        var mclm = me as ErpClaims;
        if (mclm == null)
        {
            throw new InvalidCastException($"Please User {typeof(ErpClaims)} instead {typeof(Claim)}");
        }
        else
        {
            Target.OemId = mclm.OemId;
            Target.BinaryStr = Convert.ToString((long)mclm.NumberValue, 2);
            Target.HexStr = Convert.ToString((long)mclm.NumberValue, 16);
            Target.Base64 = Convert.ToBase64String(BitConverter.GetBytes(mclm.NumberValue));
        }
    }
}
