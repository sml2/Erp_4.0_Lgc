﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ERP.Interface;
public interface ICanInitializeFromClaim {
    [Column("oem_id"), Comment("贴牌OEM ID(分区id)"), Required]
    public int OemId { get; set; }
    public string BinaryStr { get; set; } 
    public string HexStr { get; set; } 
    public string Base64 { get; set; }
}
