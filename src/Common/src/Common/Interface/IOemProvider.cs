﻿namespace ERP.Interface;
using Model = Models.Setting.OEM;
public interface IOemProvider
{
    Model? OEM { get; }
}

