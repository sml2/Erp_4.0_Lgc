﻿using System.Security.Claims;
using ERP.Enums.Identity;
using ERP.Models.Rule;

namespace ERP.Interface.Rule
{
    public interface IManager
    {
        Config? Get(int id);
        Config? Get<TP>(TP id);
        SortedList<string, long> GetSeed(Role r);
        Dictionary<string, Dictionary<string, bool>> GetRules(ClaimsPrincipal user);
    }
}