﻿namespace ERP.Interface.Rule;
public  interface IConfig
{
    public long Visible { get; set; }
    public long Editable { get; set; }
}
