﻿namespace ERP.Interface.Rule;

public interface IComponentData: IMenuSorter,ISetKey
{
    string AuthLabel { get; }
    IComponentView CastView(long val);
}

public interface ISetKey
{
    //原本Key用于在ERP3识别Menu信息，在ERP4中被用作直接表示配置项名及占用位 "xxxx.127"
    void SetKey(string key);
}