﻿using System;
namespace ERP.Interface.Rule
{
    public interface IMenuSorter
    {
        public int MenuID { get; }
        public int Sorter { get; }
    }
}

