﻿using System;
namespace ERP.Interface.Rule;

public interface IHas
{
    string Key { get; }
    long Value { get; }
}
public interface To {
    public IConfigVisitor ToVal();
}
public interface IConfigVisitor: IHas
{
    new string Key { get; }
    byte bit { get; }
}

public class ConfigVisitor : IConfigVisitor
{
    public ConfigVisitor(string key, byte bit, long value)
    {
        Key = key;
        this.bit = bit;
        Value = value;
    }

    public string Key { get; }

    public byte bit { get; }

    public long Value { get; }
}
