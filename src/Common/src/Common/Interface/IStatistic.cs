using Microsoft.EntityFrameworkCore;

namespace ERP.Interface;

public interface IStatistic
{
    /// <summary>
    /// 最后创建统计记录时间
    /// </summary>
    [Comment("最后创建统计记录时间")]
    DateTime LastCreateStatistic { get; set; }
}