﻿namespace ERP.Interface
{
    public interface ISoftDelete
    {
        public bool IsDeleted { get; set; }
    }
}