﻿using ERP.Models.DB.Users;

namespace ERP.Interface.Services;

public record CompanyDto(int Id, string Name);

public interface ICompanyService
{
    public Task<Company> GetInfoById(int id);

    /// <summary>
    /// 获取正常使用的下级公司
    /// </summary>
    /// <returns></returns>
    Task<List<CompanyDto>> GetValidDistributionList(int companyId);
    
    /// <summary>
    /// 获取正常使用的下级公司Query
    /// </summary>
    /// <returns></returns>
    IQueryable<Company> GetValidDistributionListQuery(int companyId);
}