﻿using Microsoft.AspNetCore.Http;

namespace ERP.Interface;
public interface ISessionProvider
{
    public ISession? Session { get; }
}
