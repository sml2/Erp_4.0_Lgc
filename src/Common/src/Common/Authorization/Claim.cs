﻿namespace ERP.Authorization;
using Enums.Rule.Config;
using Enums.Rule;

public enum ClaimModes { VALUE, FULL, XOR, FULLSUB = XOR }

public class Claim : System.Security.Claims.Claim
{
    public long NumberValue;
    public int OemId { get; set; }

    public Claim(string type, long value) : base(type, value.ToString("D"))
    {
        NumberValue = value;
    }
    public Claim(string type, Enum value, ClaimModes claimModes) : base(type, InitValue(claimModes, value).ToString("D"))
    {
        NumberValue = InitValue(claimModes, value);
    }
    public Claim(string type, Enum value1, Enum value2) : base(type, InitValue(value1, value2).ToString("D"))
    {
        NumberValue = InitValue(value1, value2);
    }
    public Claim(string type, Enum value) : this(type, value, ClaimModes.VALUE) { }
    public Claim(string type) : this(type, Flag.FULL) { }

    private static long InitValue(Enum @enuma, Enum @enumb)
    {
        return Convert.ToInt64(enuma) ^ Convert.ToInt64(enumb);
    }
    
    private static long InitValue(ClaimModes claimModes, Enum @enum)
    {
        switch (claimModes)
        {
            case ClaimModes.FULL:
                return Convert.ToInt64(Flag.FULL);
            case ClaimModes.FULLSUB:
                return Convert.ToInt64(Flag.FULL) ^ Convert.ToInt64(@enum);
            case ClaimModes.VALUE:
                return Convert.ToInt64(@enum);
            default: throw new ArgumentOutOfRangeException(nameof(claimModes), claimModes, "Error claimModes");
        }
    }
}
public class ClaimMenu<T> : Claim
{
    public ClaimMenu() : base($"{typeof(T).Name}Menu") { }
}

public class ClaimMenu : Claim
{
    public ClaimMenu(Enum type) : base($"{type.GetType().Name}Menu") { }
}
public class ClaimConfig<T> : Claim
{
    public ClaimConfig() : base($"{typeof(T).Name}.{nameof(Models.DB.Rule.Config<Flag>.Visible)}") { }
}

public class ClaimConfig : Claim
{
    public ClaimConfig(Enum type) : base($"{type.GetType().Name}.{nameof(Models.DB.Rule.Config<Flag>.Visible)}") { }
}