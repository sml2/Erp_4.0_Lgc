﻿namespace ERP.Authorization.Rule.Component;

public class Radio : CheckBoxWithData
{
    public Radio(string key, string name, long value, Dictionary<int,string> data) : base(key, name, value, data, nameof(Radio))
    {
    }
}

