﻿using ERP.Interface.Rule;

namespace ERP.Authorization.Rule.Component;
using Attributes.Rule;
public class CheckBox : ComponentAttribute,ISetKey
{
    public string key;
    public string name;
    public object value;
    public string type;
    public CheckBox(string key, string name, object value) : this(key, name, value, nameof(CheckBox))
    {
    }
    public CheckBox(string key, string name, object value, string type)
    {
        this.key = key;
        this.name = name;
        this.value = value;
        this.type = type;
    }

    public void SetKey(string key) => this.key = key;

}

