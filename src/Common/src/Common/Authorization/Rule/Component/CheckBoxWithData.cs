﻿namespace ERP.Authorization.Rule.Component;

public class CheckBoxWithData:CheckBox
{
    public Dictionary<int,string> data;
    public CheckBoxWithData(string key, string name, long value, Dictionary<int, string> data,string type) : base(key, name, value, type) => this.data = data;
}

