﻿namespace ERP.Authorization.Rule.Component;
public class Select : CheckBoxWithData
{
    
    public int Shift { get; set; }
    
    public Select(string key, string name, long value, Dictionary<int,string> data) : base(key, name, value, data, nameof(Select))
    {
    }
}