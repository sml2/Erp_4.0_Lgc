using System.ComponentModel;

namespace ERP.Enums.Finance;

public enum TableFieldEnum
{
    #region 订单

    [Description("订单-运费")]
    OrderShippingMoney = 1,

    [Description("订单-订单获利")]
    OrderProfit,

    [Description("订单-订单总价")]
    OrderTotal,

    [Description("订单-手续费")]
    OrderFee,

    [Description("订单-退款金额")]
    OrderRefund,

    [Description("订单-订单损耗")]
    OrderLoss,

    [Description("订单-采购花费")]
    OrderPurchaseFee,

    #endregion

    #region 采购

    [Description("采购-采购总价")]
    PurchaseTotal,

    [Description("采购-订单商品单价")]
    PurchaseOrderGoodPrice,

    [Description("采购-商品单价")]
    PurchaseGoodPrice,
    
    [Description("采购-商品总价")]
    PurchaseGoodTotal,

    [Description("采购-其他费用")]
    PurchaseOther,

    #endregion

    #region 运单

    [Description("运单-最终运费")]
    WaybillTotal,

    [Description("运单-基础运费")]
    WaybillCarriageBase,

    [Description("运单-其他费用")]
    WaybillCarriageOther,

    [Description("运单-运费手续费")]
    WaybillRate,

    #endregion


    #region 钱包
    [Description("自用")]
    PrivateWallet,
    
    [Description("对公")]
    PublicWallet

    #endregion
    
    
}