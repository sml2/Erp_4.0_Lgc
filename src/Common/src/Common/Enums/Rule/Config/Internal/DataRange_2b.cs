﻿using System.ComponentModel;

namespace ERP.Enums.Rule.Config.Internal;
public enum DataRange_2b
{
    [Description("请选择")]
    None,
    [Description("小组")]
    Group,
    [Description("仅个人")]
    User,
    [Description("公司所有")]
    Company
}