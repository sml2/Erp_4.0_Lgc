﻿using System.ComponentModel;

namespace ERP.Enums.Rule.Config.Internal;
public enum StoreRange_1b
{
    /// <summary>
    /// 公司全部
    /// </summary>
    [Description("公司全部")]
    CompanyAll,
    /// <summary>
    /// 已经分配
    /// </summary>
    [Description("已经分配")]
    Authorization
}