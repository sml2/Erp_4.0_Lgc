﻿using System.ComponentModel;

namespace ERP.Enums.Rule.Config.Internal;
public enum Confirm_2b
{
    UNKNOW,
    /// <summary>
    /// 不显示
    /// </summary>
    [Description("不显示")]
    None,
    /// <summary>
    /// 内部数据
    /// </summary>
    [Description("内部数据")]
    Internal,
    /// <summary>
    /// 全部数据
    /// </summary>
    [Description("全部数据")]
    ALL
}