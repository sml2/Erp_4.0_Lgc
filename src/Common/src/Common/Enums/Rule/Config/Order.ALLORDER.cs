﻿using System.ComponentModel;

namespace ERP.Enums.Rule.Config;
public enum Order_ALLORDER
{
    [Description("公司全部")]
    ALL,
    [Description("已经分配")]
    Some
}