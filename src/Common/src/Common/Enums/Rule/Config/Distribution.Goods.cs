﻿using System.ComponentModel;

namespace ERP.Enums.Rule.Config;
public enum Distribution_GOODS
{
    [Description("显示")]
    Show,
    [Description("不显示")]
    Hide
}