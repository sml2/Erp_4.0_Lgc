﻿using ERP.Enums.Rule.Config.Internal;
using ERP.Attributes.Rule;

namespace ERP.Enums.Rule.Config;
public enum Waybill:long
{
    /// <summary>
    /// 待发货列表--运单添加
    /// </summary>
    [CheckBox(ID.Menu.Shipped, 1, "WaybillAdd", "添加运单", true)]
    WAYBILL_ADD = Flag.FlAG_BIT_01,
    /// <summary>
    /// 待发货列表--收件人信息
    /// </summary>
    [CheckBox(ID.Menu.Shipped, 2, "WaybillReceiverInfo", "收货人信息", true)]
    WAYBILL_RECEIVER_INFO = Flag.FlAG_BIT_02,

    /// <summary>
    /// 自由切换上报信息
    /// </summary>
    [CheckBox(ID.Menu.Shipped, 1, "SwitchDistribution", "切换上报信息", true)]
    SWITCH_DISTRIBUTION = Flag.FlAG_BIT_03,
    //采购确认
    [CheckBox(ID.Menu.Shipped, 2, "PurchaseConfirm", "采购确认", true)]
    DISTRIBUTION_PURCHASE_CONFIRM = Flag.FlAG_BIT_04,
    [Select<Confirm_2b>(ID.Menu.Shipped, 3, "PurchaseConfirmRange", "采购确认", Confirm_2b.ALL, new[] { Confirm_2b.None, Confirm_2b.Internal, Confirm_2b.ALL })]
    PURCHASE_CONFIRM_RANGE = Flag.FlAG_BIT_05 | Flag.FlAG_BIT_06,

    /// <summary>
    /// 寄件人信息管理--寄件人编辑 
    /// </summary>
    [CheckBox(ID.Menu.WaybillList, 4, "WaybillDownload", "下载", true)]
    WAYBILL_DOWNLOAD = Flag.FlAG_BIT_07,
    /// <summary>
    /// 运单列表--批量发货
    /// </summary>
    [CheckBox(ID.Menu.WaybillList, 5, "WaybillSend", "批量发货", true)]
    WAYBILL_SEND = Flag.FlAG_BIT_08,
    /// <summary>
    /// 运单列表--跟踪
    /// </summary>
    [CheckBox(ID.Menu.WaybillList, 6, "WaybillFollow", "跟踪", true)]
    WAYBILL_FOLLOW = Flag.FlAG_BIT_09,
    ///<summary>
    /// 运单列表--运单详情
    /// </summary>
    [CheckBox(ID.Menu.WaybillList, 7, "WaybillInfo", "详情", true)]
    WAYBILL_INFO = Flag.FlAG_BIT_10,
    /// <summary>
    /// 运单列表--订单信息
    /// </summary>
    [CheckBox(ID.Menu.WaybillList, 8, "WaybillOrder", "订单信息", true)]
    WAYBILL_ORDER = Flag.FlAG_BIT_11,
    /// <summary>
    /// 运单列表--运单确认
    /// </summary>
    [CheckBox(ID.Menu.WaybillList, 9, "WaybillConfirm", "运单确认", true)]
    WAYBILL_CONFIRM = Flag.FlAG_BIT_12,
    /// <summary>
    /// 运单列表--运单编辑
    /// </summary>
    [CheckBox(ID.Menu.WaybillList,10, "WaybillEdit", "运单编辑", true)]
    WAYBILL_EDIT = Flag.FlAG_BIT_13,
    /// <summary>
    /// 亚马逊订单 可查看店铺
    /// </summary>
    [Select<StoreRange_1b>(ID.Menu.WaybillList, 11, "Store", "可查看店铺", StoreRange_1b.Authorization, new[] { StoreRange_1b.Authorization, StoreRange_1b.CompanyAll })]
    STORE_RANGE = Flag.FlAG_BIT_14,
    /// <summary>
    /// 寄件人信息管理--寄件人编辑
    /// </summary>
    [CheckBox(ID.Menu.Sender, 1, "WaybillSenderAdd", "添加寄件人", true)]
    WAYBILL_SENDER_ADD = Flag.FlAG_BIT_15,
    /// <summary>
    /// 寄件人信息管理--寄件人添加
    /// </summary>
    [CheckBox(ID.Menu.Sender, 2, "WaybillSenderEdit", "编辑", true)]
    WAYBILL_SENDER_EDIT = Flag.FlAG_BIT_16,
    /// <summary>
    /// 寄件人信息管理--寄件人删除
    /// </summary>
    [CheckBox(ID.Menu.Sender, 3, "WaybillSenderDelete", "删除", true)]
    WAYBILL_SENDER_DELETE = Flag.FlAG_BIT_17,
    /// <summary>
    /// 店铺管理--分配用户组
    /// </summary>
    [CheckBox(ID.Menu.LogisticsManage, 1, "LogisticAdd", "添加物流", true)]
    LOGISTIC_ADD = Flag.FlAG_BIT_18,
    /// <summary>
    /// 店铺管理--分配用户组
    /// </summary>
    [CheckBox(ID.Menu.LogisticsManage, 2, "LogisticEdit", "编辑", true)]
    LOGISTIC_EDIT = Flag.FlAG_BIT_19,
    /// <summary>
    /// 店铺管理--分配用户组
    /// </summary>
    [CheckBox(ID.Menu.LogisticsManage, 3, "LogisticDelete", "删除", true)]
    LOGISTIC_DELETE = Flag.FlAG_BIT_20,
}