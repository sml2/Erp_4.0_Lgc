﻿using ERP.Attributes.Rule;

namespace ERP.Enums.Rule.Config;

[Flags]
public enum User : long
{
    #region 推荐用户 1,2,3

    /// <summary>
    /// 新增注册用户
    /// </summary>
    [CheckBox(ID.Menu.Referer, 1, "RefererAdd", "新增注册用户", true)]
    RefererAdd = Flag.FlAG_BIT_01,

    /// <summary>
    /// 推荐充值
    /// </summary>
    [CheckBox(ID.Menu.Referer, 2, "RefererRecharge", "充值", true)]
    RefererRecharge = Flag.FlAG_BIT_02,
    // /// <summary>
    // /// 购买套餐
    // /// </summary>
    // [CheckBox(ID.Menu.Referer, 3, "RefererBuyPackage", "购买套餐", true)]
    // REFERER_BUY_PACKAGE = Flag.FlAG_BIT_03,

    #endregion

    #region 角色 6,7,8,9,11,24

    //权限组配置项
    /// <summary>
    /// 权限组添加
    /// </summary>
    [CheckBox(ID.Menu.Rule, 1, "RuleAdd", "角色添加", true)]
    RuleAdd = Flag.FlAG_BIT_06,

    /// <summary>
    /// 权限组编辑
    /// </summary>
    [CheckBox(ID.Menu.Rule, 2, "RuleEdit", "角色编辑", true)]
    RuleEdit = Flag.FlAG_BIT_07,

    /// <summary>
    /// 权限克隆
    /// </summary>
    [CheckBox(ID.Menu.Rule, 3, "RuleCopy", "角色克隆", true)]
    RuleCopy = Flag.FlAG_BIT_08,

    /// <summary>
    /// 权限配置
    /// </summary>
    [CheckBox(ID.Menu.Rule, 4, "RuleConfig", "权限配置", true)]
    RuleConfig = Flag.FlAG_BIT_09,

    /// <summary>
    /// 权限配置
    /// </summary>
    [CheckBox(ID.Menu.Rule, 4, "RuleConfigItem", "权限配置项", true)]
    RuleConfigItem = Flag.FlAG_BIT_24,
    
    /// <summary>
    /// 权限组删除
    /// </summary>
    [CheckBox(ID.Menu.Rule, 6, "RuleDel", "角色删除", true)]
    RuleDel = Flag.FlAG_BIT_11,
    #endregion

    #region 独立账号 4,15,16,17,18,19,20,25,26,29,30,33,34,35

    /// <summary>
    /// 独立账户重置密码
    /// </summary>
    [CheckBox(ID.Menu.Own, 2, "UserResetPassword", "重置密码", true)]
    OwnUserResetPassword = Flag.FlAG_BIT_04,

    /// <summary>
    /// 独立账户添加
    /// </summary>
    [CheckBox(ID.Menu.Own, 1, "OwnUserAdd", "添加", true)]
    OwnUserAdd = Flag.FlAG_BIT_15,

    /// <summary>
    /// 独立账户编辑
    /// </summary>
    [CheckBox(ID.Menu.Own, 2, "OwnUserEdit", "编辑", true)]
    OwnUserEdit = Flag.FlAG_BIT_16,

    /// <summary>
    /// 独立账户 公司配置
    /// </summary>
    [CheckBox(ID.Menu.Own, 3, "OwnCompanyConfig", "公司配置", true)]
    OwnCompanyConfig = Flag.FlAG_BIT_17,

    /// <summary>
    /// 独立账户续期
    /// </summary>
    [CheckBox(ID.Menu.Own, 5, "OwnUserRenewal", "续期", true)]
    OwnUserRenewal = Flag.FlAG_BIT_18,

    /// <summary>
    /// 独立账户充值
    /// </summary>
    [CheckBox(ID.Menu.Own, 6, "OwnUserRecharge", "充值", true)]
    OwnUserRecharge = Flag.FlAG_BIT_19,

    /// <summary>
    /// 独立账户 操作日志
    /// </summary>
    [CheckBox(ID.Menu.Own, 7, "OwnUserLog", "操作日志", true)]
    OwnUserLog = Flag.FlAG_BIT_20,

    /// <summary>
    /// 独立账户 导出余额账单
    /// </summary>
    [CheckBox(ID.Menu.Own, 7, "OwnExportBill", "导出余额账单", true)]
    OwnExportBill = Flag.FlAG_BIT_25,

    /// <summary>
    /// 独立账户 余额账单
    /// </summary>
    [CheckBox(ID.Menu.Own, 7, "OwnBalance", "余额账单", true)]
    OwnBalance = Flag.FlAG_BIT_26,
    
    /// <summary>
    /// 独立账户 权限配置
    /// </summary>
    [CheckBox(ID.Menu.Own, 7, "OwnRuleConfig", "权限配置", true)]
    OwnRuleConfig = Flag.FlAG_BIT_29,
    /// <summary>
    /// 独立账户 权限配置项
    /// </summary>
    [CheckBox(ID.Menu.Own, 7, "OwnRuleConfigItem", "权限配置项", true)]
    OwnRuleConfigItem = Flag.FlAG_BIT_30,
    /// <summary>
    /// 独立账户 查看资源
    /// </summary>
    [CheckBox(ID.Menu.Own, 8, "OwnPackageItem", "查看资源", true)]
    OwnPackageItem = Flag.FlAG_BIT_33,
    /// <summary>
    /// 独立账户 赠送资源
    /// </summary>
    [CheckBox(ID.Menu.Own, 9, "OwnGiveResource", "赠送资源", true)]
    OwnGiveResource = Flag.FlAG_BIT_34,
        
    /// <summary>
    /// 独立账户 产品有效期延长
    /// </summary>
    [CheckBox(ID.Menu.Own, 9, "SetProductValidity", "产品有效期延长", true)]
    SetProductValidity = Flag.FlAG_BIT_35,
    #endregion

    #region 员工账户 05，21,22,23,31,32

    /// <summary>
    /// 员工账户重置密码
    /// </summary>
    [CheckBox(ID.Menu.Child, 4, "OwnUserResetPassword", "重置密码", true)]
    UserResetPassword = Flag.FlAG_BIT_05,


    //员工账号配置
    /// <summary>
    /// 员工账户添加
    /// </summary>
    [CheckBox(ID.Menu.Child, 1, "UserAdd", "添加", true)]
    UserAdd = Flag.FlAG_BIT_21,

    /// <summary>
    /// 员工账户编辑
    /// </summary>
    [CheckBox(ID.Menu.Child, 3, "UserEdit", "编辑", true)]
    UserEdit = Flag.FlAG_BIT_22,

    /// <summary>
    /// 员工账户 操作日志
    /// </summary>
    [CheckBox(ID.Menu.Child, 4, "UserLog", "操作日志", true)]
    UserLog = Flag.FlAG_BIT_23,
    
    /// <summary>
    /// 员工账户 店铺
    /// </summary>
    [CheckBox(ID.Menu.Child, 4, "UserStores", "绑定店铺", true)]
    UserStores = Flag.FlAG_BIT_31,
    
    /// <summary>
    /// 员工账户 店铺
    /// </summary>
    [CheckBox(ID.Menu.Child, 4, "UserRuleConfig", "权限配置项", true)]
    UserRuleConfig = Flag.FlAG_BIT_32,

    #endregion

    #region 用户分组 12，13，14，27，28

    //用户组配置项
    /// <summary>
    /// 用户组添加
    /// </summary>
    [CheckBox(ID.Menu.Group, 1, "GroupAdd", "分组添加", true)]
    GroupAdd = Flag.FlAG_BIT_12,

    /// <summary>
    /// 用户组编辑
    /// </summary>
    [CheckBox(ID.Menu.Group, 2, "GroupEdit", "分组编辑", true)]
    GroupEdit = Flag.FlAG_BIT_13,

    /// <summary>
    /// 用户组删除
    /// </summary>
    [CheckBox(ID.Menu.Group, 3, "GroupDel", "分组删除", true)]
    GroupDel = Flag.FlAG_BIT_14,

    /// <summary>
    /// 编辑组下用户
    /// </summary>
    [CheckBox(ID.Menu.Group, 3, "GroupUsers", "编辑组下用户", true)]
    GroupUsers = Flag.FlAG_BIT_27,

    /// <summary>
    /// 编辑组下用户
    /// </summary>
    [CheckBox(ID.Menu.Group, 3, "GroupStores", "编辑组下店铺", true)]
    GroupStores = Flag.FlAG_BIT_28,

    #endregion
}