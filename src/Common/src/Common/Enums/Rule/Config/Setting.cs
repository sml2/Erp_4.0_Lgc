﻿using ERP.Attributes.Rule;

namespace ERP.Enums.Rule.Config;

public enum Setting:long
{
    //rule['integralBill']
    /// <summary>
    /// 积分账单 
    /// </summary>
    // [CheckBox(ID.Menu.IntegralBalance, 1, "IntegralBill", "积分账单", true)]
    // INTEGRAL_BILL=Flag.FlAG_BIT_01,
    /// <summary>
    /// 积分转换
    /// </summary>
    // [CheckBox(ID.Menu.IntegralBalance, 2, "IntegralTransform", "积分转换", true)]
    // INTEGRAL_TRANSFORM=Flag.FlAG_BIT_02,
    //rule['balanceBill']
    /// <summary>
    /// 余额账单
    /// </summary>
    [CheckBox(ID.Menu.IntegralBalance, 3, "BalanceBill", "余额账单", true)]
    BALANCE_BILL=Flag.FlAG_BIT_03,
    //rule['rechargeBill']
    /// <summary>
    /// 充值账单
    /// </summary>
    [CheckBox(ID.Menu.IntegralBalance, 4, "RechargeBill", "充值账单", true)]
    RECHARGE_BILL=Flag.FlAG_BIT_04,
    /// <summary>
    /// 余额充值
    /// </summary>
    [CheckBox(ID.Menu.IntegralBalance, 5, "BalanceRecharge", "在线充值", true)]
    BALANCE_RECHARGE=Flag.FlAG_BIT_05,

    /// <summary>
    /// 个人数据
    /// </summary>
    [CheckBox(ID.Menu.Business, 1, "StatisticsPerson", "个人数据", true)]
    STATISTICS_PERSON= Flag.FlAG_BIT_06,
    /// <summary>
    /// 公司统计
    /// </summary>
    [CheckBox(ID.Menu.Business, 2, "StatisticsCurrent", "公司统计", true)]
    STATISTICS_CURRENT= Flag.FlAG_BIT_07,
    /// <summary>
    /// 员工统计
    /// </summary>
    [CheckBox(ID.Menu.Business, 3, "StatisticsUser", "员工统计", true)]
    STATISTICS_USER = Flag.FlAG_BIT_08,
    /// <summary>
    /// 用户组统计
    /// </summary>
    [CheckBox(ID.Menu.Business, 4, "StatisticsGroup", "用户组统计", true)]
    STATISTICS_GROUP = Flag.FlAG_BIT_09,
    /// <summary>
    /// 下属公司统计
    /// </summary>
    [CheckBox(ID.Menu.Business, 5, "StatisticsCompany", "下属公司统计", true)]
    STATISTICS_COMPANY = Flag.FlAG_BIT_10,
    /// <summary>
    /// 业务统计-->资源统计
    /// </summary> 
    // [CheckBox(ID.Menu.Business, 6, "StatisticsResource", "资源统计", true)]
    // STATISTICS_RESOURCE = Flag.FlAG_BIT_11,


    /// <summary>
    /// 在线用户
    /// </summary>
    [CheckBox(ID.Menu.Trend, 1, "OnlineUser", "在线用户", true)]
    ONLINEUSER = Flag.FlAG_BIT_12,
    /// <summary>
    /// 开户统计
    /// </summary>
    [CheckBox(ID.Menu.Trend, 2, "Account", "开户统计", true)]
    ACCOUNT = Flag.FlAG_BIT_13,
    /// <summary>
    /// 开户统计
    /// </summary>
    [CheckBox(ID.Menu.Trend, 3, "Expenses", "积分统计", true)]
    EXPENSES = Flag.FlAG_BIT_15,
    /// <summary>
    /// 邮件趋势
    /// </summary>
    [CheckBox(ID.Menu.Trend, 4, "EmailChart", "邮件趋势", true)]
    /// <summary>
    /// 数据趋势
    /// </summary>
    EMAILCHART = Flag.FlAG_BIT_16,
    /// <summary>
    /// 数据趋势
    /// </summary>
    [CheckBox(ID.Menu.Trend, 5, "EssentialChart", "数据趋势", true)]
    ESSENTIALCHART = Flag.FlAG_BIT_17,
    /// <summary>
    /// 模版分配
    /// </summary>
    [CheckBox(ID.Menu.ExportTemplate, 1, "AllotExport", "模版分配", true)]
    ALLOT_EXPORT = Flag.FlAG_BIT_18,
}