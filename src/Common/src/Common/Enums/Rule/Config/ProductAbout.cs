﻿using ERP.Enums.Rule.Config.Internal;
using ERP.Attributes.Rule;

namespace ERP.Enums.Rule.Config;

public enum ProductAbout : long
{
    /// <summary>
    /// 同步店铺操作
    /// Menu:<see cref="ID.Menu.ProductManager"/>
    /// </summary>
    [CheckBox(ID.Menu.ProductManager, 1, "OperateOrderMessage", "同步店铺操作", true)] // return null; 3.0
    ProductRankTool = Flag.FlAG_BIT_01,

    /// <summary>
    /// 亚马逊上传结果
    /// </summary>
    [CheckBox(ID.Menu.ProductManager, 2, "AmazonSearch", "亚马逊上传结果", true)] // return null; 3.0
    AmazonSearch = Flag.FlAG_BIT_02,

    //ATTRIBUTE 属性替换
    /// <summary>
    /// 搜索功能
    /// </summary>
    [CheckBox(ID.Menu.ProductManager, 3, "AttributeSearch", "搜索功能", true)] // return null; 3.0
    AttributeSearch = Flag.FlAG_BIT_03,

    //Template
    /// <summary>
    /// 查看范围
    /// </summary>
    // [Select<DataRange_2b>(ID.Menu.Template, 1, "TemplateRange", "可查看范围", DataRange_2b.Group, new[] { DataRange_2b.User, DataRange_2b.Group, DataRange_2b.Company })]
    [Select<DataRange_2b>(ID.Menu.Template, 1, "TemplateRange", "可查看范围", DataRange_2b.Group,
        new[] { DataRange_2b.None, DataRange_2b.User, DataRange_2b.Group, DataRange_2b.Company },Shift = 3)]
    TemplateRange = Flag.FlAG_BIT_04 | Flag.FlAG_BIT_05,

    /// <summary>
    /// 属性模板
    /// </summary>
    // [CheckBox(ID.Menu.Template, 2, "ExtTemplate", "属性模板", true)]
    // TEMPLATE_EXT = Flag.FlAG_BIT_06,

    /// <summary>
    /// 卖点模板
    /// </summary>
    [CheckBox(ID.Menu.Template, 3, "SketchTemplate", "卖点模板", true)]
    TemplateSketch = Flag.FlAG_BIT_07,

    /// <summary>
    /// 关键词模板
    /// </summary>
    [CheckBox(ID.Menu.Template, 4, "KeywordTemplate", "关键词模板", true)]
    TemplateKeyword = Flag.FlAG_BIT_08,

    /// <summary>
    /// 搜索功能
    /// </summary>
    [CheckBox(ID.Menu.Template, 5, "TemplateSearch", "搜索功能", true)]
    TemplateSearch = Flag.FlAG_BIT_09,

    ////ToSell
    ///// <summary>
    ///// 拉取跟卖产品
    ///// </summary>
    //[CheckBox(ID.Menu.ToSell, 1, "ToSellPull", "拉取跟卖产品", true)]
    //ToSellPull = Flag.FlAG_BIT_10,

    ///// <summary>
    ///// 上传跟卖产品
    ///// </summary>
    //[CheckBox(ID.Menu.ToSell, 2, "ToSellUpload", "上传跟卖产品", true)]
    //ToSellUpload = Flag.FlAG_BIT_11,

    ///// <summary>
    ///// 删除跟卖产品
    ///// </summary>
    //[CheckBox(ID.Menu.ToSell, 3, "ToSellDelete", "删除跟卖产品", true)]
    //ToSellDelete = Flag.FlAG_BIT_12,

    //Category
    /// <summary>
    /// 查看范围
    /// </summary>
    [Select<DataRange_2b>(ID.Menu.Category, 1, "CategoryRange", "可查看范围", DataRange_2b.Group,
        new[] { DataRange_2b.None, DataRange_2b.User, DataRange_2b.Group, DataRange_2b.Company },Shift = 12)]
    CategoryRange = Flag.FlAG_BIT_13 | Flag.FlAG_BIT_14,

    /// <summary>
    /// 添加分类
    /// </summary>
    [CheckBox(ID.Menu.Category, 2, "AddCategory", "添加分类", true)]
    CategoryAdd = Flag.FlAG_BIT_15,

    /// <summary>
    /// 修改分类
    /// </summary>
    [CheckBox(ID.Menu.Category, 3, "EditCategory", "修改分类", true)]
    CategoryEdit = Flag.FlAG_BIT_16,

    /// <summary>
    /// 删除分类
    /// </summary>
    [CheckBox(ID.Menu.Category, 4, "DeleteCategory", "删除分类", true)]
    CategoryDelete = Flag.FlAG_BIT_17,

    // Task
    /// <summary>
    ///  查看范围
    /// </summary>
    [Select<DataRange_2b>(ID.Menu.Task, 1, "TaskRange", "可查看范围", DataRange_2b.Group,
        new[] { DataRange_2b.None, DataRange_2b.User, DataRange_2b.Group, DataRange_2b.Company },Shift = 17)]
    TaskRange = Flag.FlAG_BIT_18 | Flag.FlAG_BIT_19,

    /// <summary>
    /// 修改采集任务
    /// </summary>
    [CheckBox(ID.Menu.Task, 2, "EditTask", "修改采集任务", true)]
    TaskEdit = Flag.FlAG_BIT_20,

    /// <summary>
    /// 删除采集任务
    /// </summary>
    [CheckBox(ID.Menu.Task, 3, "DeleteTask", "删除采集任务", true)]
    TaskDelete = Flag.FlAG_BIT_21,

    /// <summary>
    /// 查看范围
    /// Menu:<see cref="ID.Menu.IllegalWord"/>
    /// </summary>
    [Select<DataRange_2b>(ID.Menu.IllegalWord, 1, "IllegalwordRange", "可查看范围", DataRange_2b.Group,
        new[] { DataRange_2b.None, DataRange_2b.User, DataRange_2b.Group, DataRange_2b.Company },Shift = 21)]
    IllegalWordRange = Flag.FlAG_BIT_22 | Flag.FlAG_BIT_23,

    /// <summary>
    /// 搜索功能
    /// Menu:<see cref="ID.Menu.IllegalWord"/>
    /// </summary>
    [CheckBox(ID.Menu.IllegalWord, 2, "IllegalWordSearch", "搜索功能", true)]
    IllegalWordSearch = Flag.FlAG_BIT_24,

    /// <summary>
    /// 修改违禁词
    /// Menu:<see cref="ID.Menu.IllegalWord"/>
    /// </summary>
    [CheckBox(ID.Menu.IllegalWord, 4, "IllegalWordEdit", "修改违禁词", true)]
    IllegalWordEdit = Flag.FlAG_BIT_25,


    /// <summary>
    /// 关键词导入
    /// Menu:<see cref="ID.Menu.KeywordTool"/>
    /// </summary>
    [CheckBox(ID.Menu.KeywordTool, 1, "KeywordToolImport", "关键词导入", true)]
    IllegalWordImport = Flag.FlAG_BIT_26,

    /// <summary>
    /// 修改违禁词
    /// Menu:<see cref="ID.Menu.IllegalWord"/>
    /// </summary>
    [CheckBox(ID.Menu.IllegalWord, 3, "IllegalWordAdd", "添加违禁词", true)]
    IllegalWordAdd = Flag.FlAG_BIT_27,

    /// <summary>
    /// 删除违禁词
    /// Menu:<see cref="ID.Menu.IllegalWord"/>
    /// </summary>
    [CheckBox(ID.Menu.IllegalWord, 4, "IllegalWordDel", "删除违禁词", true)]
    IllegalWordDelete = Flag.FlAG_BIT_28,
    
    /// <summary>
    /// 关键词模板
    /// </summary>
    [CheckBox(ID.Menu.Template, 4, "ChangePriceTemplate", "改价模板", true)]
    TemplateChangePrice = Flag.FlAG_BIT_29,
}