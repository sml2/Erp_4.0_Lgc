﻿using ERP.Attributes.Rule;

namespace ERP.Enums.Rule.Config;
public enum Finance:long
{
    /// <summary>
    /// 订单详情
    /// Menu:<see cref="ID.Menu.AmazonOrderStatistics"/>
    /// </summary>
    [CheckBox(ID.Menu.AmazonOrderStatistics, 1, "AmazonOrderStatisticDetails", "订单详情", true)]
    AMAZON_ORDER_STATISTIC_DETAILS = Flag.FlAG_BIT_01,
    //FinanceAudit
    /// <summary>
    /// 财务审核
    /// Menu:<see cref="ID.Menu.FinanceAudit"/>
    /// </summary>
    [CheckBox(ID.Menu.FinanceAudit, 1, "Approval", "财务审核", true)]
    APPROVAL = Flag.FlAG_BIT_02,

    /// <summary>
    /// 账单导出
    /// Menu:<see cref="ID.Menu.BillLog"/>
    /// </summary>
    [CheckBox(ID.Menu.BillLog, 1, "BillExport", "账单导出", true)]
    BILLEXPORT = Flag.FlAG_BIT_03,
    /// <summary>
    /// 充值
    /// </summary>
    [CheckBox(ID.Menu.BillLog, 2, "BillRecharge", "充值", true)]
    BILL_RECHARGE = Flag.FlAG_BIT_04,

    /// <summary>
    /// 支出
    /// </summary>
    [CheckBox(ID.Menu.BillLog, 3, "BillDisburse", "支出", true)]
    BILL_DISBURSE = Flag.FlAG_BIT_05,
    /// <summary>
    /// 详情
    /// </summary>
    [CheckBox(ID.Menu.BillLog, 4, "BillDetails", "账单详情", true)]
    BILL_DETAILS = Flag.FlAG_BIT_06,

    /// <summary>
    /// 利润计算
    /// Menu:<see cref="ID.Menu.Statistics"/>
    /// </summary>
    [CheckBox(ID.Menu.Statistics, 1, "ProfitCalculation", "利润计算", true)]
    HASPROFITCALCULATION = Flag.FlAG_BIT_07,
    /// <summary>
    /// 钱包统计
    /// </summary>
    [CheckBox(ID.Menu.Statistics, 2, "WalletStatistics", "钱包统计", true)]
    WALLET_STATISTICS = Flag.FlAG_BIT_08
}
