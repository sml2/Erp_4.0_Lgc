﻿using ERP.Enums.Rule.Config.Internal;
using ERP.Attributes.Rule;

namespace ERP.Enums.Rule.Config;
public enum Storage:long
{
    /// <summary>
    /// 采购确认
    /// Munu:<see cref="ID.Menu.Warehouse"/>
    /// </summary>
    [CheckBox(ID.Menu.Warehouse, 1, "WarehouseAdd", "仓库添加", true)]
    WAREHOUSE_ADD=Flag.FlAG_BIT_01,
    /// <summary>
    /// 采购确认
    /// </summary>
    [CheckBox(ID.Menu.Warehouse, 2, "WarehouseEdit", "仓库编辑", true)]
    WAREHOUSE_EDIT = Flag.FlAG_BIT_02,
    /// <summary>
    /// 采购确认
    /// </summary>
    [CheckBox(ID.Menu.Warehouse, 3, "WarehouseDetail", "仓库详情", true)]
    WAREHOUSE_DETAIL = Flag.FlAG_BIT_03,
    /// <summary>
    /// 采购确认
    /// </summary>
    [CheckBox(ID.Menu.Stock, 1, "StockOut", "出库", true)]
    STOCK_OUT = Flag.FlAG_BIT_04,
    /// <summary>
    /// 采购确认
    /// </summary>
    [CheckBox(ID.Menu.Stock, 2, "StockIn", "入库", true)]
    STOCK_IN = Flag.FlAG_BIT_05,

    //IF(distribution)
    /// <summary>
    /// 采购确认
    /// </summary>
    [CheckBox(ID.Menu.Stock, 3, "PurchaseConfirm", "采购确认", true)]
    PURCHASE_CONFIRM = Flag.FlAG_BIT_06,
    /// <summary>
    /// 采购确认[1 => '不显示', 2 => '内部数据', 3 => '全部数据']
    /// </summary>
    [Select<Confirm_2b>(ID.Menu.Stock, 4, "PurchaseConfirmRange", "采购确认", Confirm_2b.ALL, new[] { Confirm_2b.None, Confirm_2b.Internal, Confirm_2b.ALL })]
    PURCHASE_CONFIRM_RANGE = Flag.FlAG_BIT_07| Flag.FlAG_BIT_08
}