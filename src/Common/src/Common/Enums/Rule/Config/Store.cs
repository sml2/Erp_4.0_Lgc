﻿using ERP.Enums.Rule.Config.Internal;
using ERP.Attributes.Rule;

namespace ERP.Enums.Rule.Config;

public enum Store:long
{
    // <summary>
    /// 待上传列表
    /// </summary>
    [CheckBox(ID.Menu.UploadProgress, 2, "ToBeUploaded", "待上传列表", true)]
    TOBEUPLOADED=Flag.FlAG_BIT_01,
    /// <summary>
    /// 上传商品--查看进度
    /// </summary>
    [CheckBox(ID.Menu.UploadProgress, 2, "ProgressPlan", "查看进度", true)]
    Progress_Plan = Flag.FlAG_BIT_02,
    /// <summary>
    /// 上传商品--上传按钮
    /// </summary>
    [CheckBox(ID.Menu.UploadProgress, 3, "ProgressUpload", "上传商品", true)]
    Progress_Upload = Flag.FlAG_BIT_03,
    /// <summary>
    /// 上传商品--分配EAN/UPC
    /// </summary>
    [CheckBox(ID.Menu.UploadProgress, 4, "ProgressDistribution", "分配EAN/UPC", true)]
    Progress_Distribution = Flag.FlAG_BIT_04,
    /// <summary>
    /// 上传商品--编辑上传商品
    /// </summary>
    [CheckBox(ID.Menu.UploadProgress, 5, "ProgressEdit", "编辑上传商品", true)]
    Progress_Edit = Flag.FlAG_BIT_05,
    /// <summary>
    /// 上传商品--添加上传商品
    /// </summary>
    [CheckBox(ID.Menu.UploadProgress, 6, "ProgressAdd", "添加上传商品", true)]
    Progress_Add = Flag.FlAG_BIT_06,
    /// <summary>
    /// 删除sync产品
    /// </summary>
    [CheckBox(ID.Menu.UploadProgress, 7, "SyncProDel", "删除产品列表", true)]
    DEL_SYNCPRO = Flag.FlAG_BIT_07,


    /*-------------节点模板-----------*/
    /// <summary>
    /// 模板更新范围 1 => '组内数据', 0 => '公司数据'
    /// </summary>
    [Radio<DataRange_2b>(ID.Menu.CategoryNodeDefault, 1, "CategoryNodeRange", "模板更新范围", DataRange_2b.Group, new[] { DataRange_2b.User, DataRange_2b.Company })]
    CATEGORY_NODE_RANGE =Flag.FlAG_BIT_08,
    /// <summary>
    /// 新增节点模板
    /// </summary>
    [CheckBox(ID.Menu.CategoryNodeDefault, 2, "CategoryNodeDefaultAdd", "模板添加", true)]
    ADD_CATEGORYNODEDEFAULT = Flag.FlAG_BIT_09,
    /// <summary>
    /// 编辑节点模板
    /// </summary>
    [CheckBox(ID.Menu.CategoryNodeDefault, 3, "CategoryNodeDefaultEdit", "模板编辑", true)]
    EDIT_CATEGORYNODEDEFAULT = Flag.FlAG_BIT_10,
    /// <summary>
    /// 删除节点模板
    /// </summary>
    [CheckBox(ID.Menu.CategoryNodeDefault, 4, "CategoryNodeDefaultDelete", "模板删除", true)]
    DEL_CATEGORYNODEDEFAULT = Flag.FlAG_BIT_11,

    /**上传导出--删除产品**/
    /// <summary>
    /// 编辑导出任务
    /// </summary>
    [CheckBox(ID.Menu.ExportProgress, 7, "EditTasks", "编辑导出任务", true)]
    EDIT_TASKS = Flag.FlAG_BIT_12,
    /// <summary>
    /// 删除导出任务
    /// </summary>
    [CheckBox(ID.Menu.ExportProgress, 7, "DelTasks", "删除导出任务", true)]
    DEL_TASKS = Flag.FlAG_BIT_13,
    /// <summary>
    /// 编辑导出商品
    /// </summary>
    [CheckBox(ID.Menu.ExportProgress, 7, "EditTask", "编辑导出商品", true)]
    EDIT_TASK = Flag.FlAG_BIT_14,
    /// <summary>
    /// 分配EAN/UPC
    /// </summary>
    [CheckBox(ID.Menu.ExportProgress, 7, "AllotEanUpc", "分配EAN/UPC", true)]
    ALLOT_EAN_UPC = Flag.FlAG_BIT_15,
    /// <summary>
    /// 删除导出商品
    /// </summary>
    [CheckBox(ID.Menu.ExportProgress, 7, "DelTask", "删除导出商品", true)]
    DEL_TASK = Flag.FlAG_BIT_16,
    // /// <summary>
    // /// 上传商品
    // /// </summary>
    // [CheckBox(ID.Menu.Eanupc, 1, "EanupcRange", "查看范围", true)]
    // EANUPC_RANGE = Flag.FlAG_BIT_17,
    /// <summary>
    /// EAN/UPC--一键生成
    /// </summary>
    [CheckBox(ID.Menu.Eanupc, 2, "CodeCreate", "一键生成", true)]
    CODE_CREATE = Flag.FlAG_BIT_18,
    /// <summary>
    /// EAN/UPC--删除
    /// </summary>
    [CheckBox(ID.Menu.Eanupc, 3, "CodeDel", "删除", true)]
    CODE_DEL = Flag.FlAG_BIT_19,
    /// <summary>
    /// 下载批量导入模板
    /// </summary>
    [CheckBox(ID.Menu.Eanupc, 4, "LeadMode", "下载批量导入模板", true)]
    LEAD_MODE = Flag.FlAG_BIT_20,
    /// <summary>
    /// 导入 EAN/UPC
    /// </summary>
    [CheckBox(ID.Menu.Eanupc, 5, "LeadIn", "导入 EAN/UPC", true)]
    LEAD_IN = Flag.FlAG_BIT_21,
    /// <summary>
    /// 远程管理--新增
    /// </summary>
    [CheckBox(ID.Menu.Remote, 1, "RemoteAdd", "远程添加", true)]
    FLAG_REMOTE_ADD = Flag.FlAG_BIT_22,
    /// <summary>
    /// 店铺管理--编辑
    /// </summary>
    [CheckBox(ID.Menu.Remote, 2, "RemoteEdit", "远程编辑", true)]
    REMOTE_EDIT = Flag.FlAG_BIT_23,
    /// <summary>
    /// 店铺管理--删除
    /// </summary>
    [CheckBox(ID.Menu.Remote, 3, "RemoteDelete", "远程删除", true)]
    REMOTE_DELETE = Flag.FlAG_BIT_24,
    /// <summary>
    /// 店铺管理--打开远程
    /// </summary>
    [CheckBox(ID.Menu.Remote, 4, "RemoteOpen", "打开远程", true)]
    REMOTE_OPEN = Flag.FlAG_BIT_25,
    /// <summary>
    /// 可查看店铺
    /// </summary>
    [Radio<DataRange_2b>(ID.Menu.AmazonStore, 1, "StoreList", "可查看店铺", DataRange_2b.Group, new[] { DataRange_2b.User, DataRange_2b.Company })]
    AMAZON_STORE = Flag.FlAG_BIT_26| Flag.FlAG_BIT_27,
    /// <summary>
    /// 店铺管理--店铺添加
    /// </summary>
    [CheckBox(ID.Menu.AmazonStore, 2, "StoreAdd", "添加店铺", true)]
    STORE_ADD = Flag.FlAG_BIT_28,
    /// <summary>
    /// 店铺管理--店铺编辑
    /// </summary>
    [CheckBox(ID.Menu.AmazonStore, 3, "StoreEdit", "编辑店铺", true)]
    STORE_EDIT = Flag.FlAG_BIT_29,
    /// <summary>
    /// 店铺管理--店铺删除
    /// </summary>
    [CheckBox(ID.Menu.AmazonStore, 4, "StoreDelete", "删除店铺", true)]
    STORE_DELETE = Flag.FlAG_BIT_30,
    /// <summary>
    /// 店铺管理--分配用户组
    /// </summary>
    [CheckBox(ID.Menu.AmazonStore, 5, "StoreGroup", "分配用户组", true)]
    STORE_GROUP = Flag.FlAG_BIT_31,

    /// <summary>
    /// 店铺管理--店铺添加
    /// </summary>
    [CheckBox(ID.Menu.MyStore, 1, "MYStoreAdd", "添加店铺", true)]
    MYSTORE_ADD = Flag.FlAG_BIT_32,
    /// <summary>
    /// 店铺管理--店铺编辑
    /// </summary>
    [CheckBox(ID.Menu.MyStore, 2, "MYStoreEdit", "编辑店铺", true)]
    MYSTORE_EDIT = Flag.FlAG_BIT_33,
    /// <summary>
    /// 店铺管理--店铺删除
    /// </summary>
    [CheckBox(ID.Menu.MyStore, 3, "MYStoreDelete", "删除店铺", true)]
    MYSTORE_DELETE = Flag.FlAG_BIT_34,
    /// <summary>
    /// 店铺管理--分配用户组
    /// </summary>
    [CheckBox(ID.Menu.MyStore, 4, "MYStoreGroup", "分配用户组", true)]
    MYSTORE_GROUP = Flag.FlAG_BIT_35,

    
    /// <summary>
    /// 可查看范围
    /// </summary>
    [Select<DataRange_2b>(ID.Menu.UploadProgress, 1, "UploadProgressRange", "导出产品模板查看范围", DataRange_2b.User,
        new[] { DataRange_2b.None, DataRange_2b.User, DataRange_2b.Group, DataRange_2b.Company },Shift = 35)]
    UploadProgressRange = Flag.FlAG_BIT_36 | Flag.FlAG_BIT_37,
    
    [Select<DataRange_2b>(ID.Menu.Eanupc, 1, "EanupcRange", "EAN/UPC查看范围", DataRange_2b.User,
        new[] { DataRange_2b.None, DataRange_2b.User, DataRange_2b.Group, DataRange_2b.Company },Shift = 37)]
    EanUpcRange = Flag.FlAG_BIT_38 | Flag.FlAG_BIT_39 ,
    
}