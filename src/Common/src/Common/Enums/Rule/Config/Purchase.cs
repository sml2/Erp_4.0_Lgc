﻿using ERP.Enums.Rule.Config.Internal;
using ERP.Attributes.Rule;

namespace ERP.Enums.Rule.Config;

public enum Purchase:long
{

    //PurchaseList
    /// <summary>
    /// 自由切换上报信息
    /// </summary>
    [CheckBox(ID.Menu.Purchase, 1, "SwitchDistribution", "切换上报信息", true)]
    SWITCH_DISTRIBUTION = Flag.FlAG_BIT_01,
    /// <summary>
    /// 采购确认
    /// </summary>
    [CheckBox(ID.Menu.Purchase, 2, "PurchaseConfirm", "采购确认", true)]
    PURCHASE_CONFIRM = Flag.FlAG_BIT_02,
    /// <summary>
    /// 采购确认
    /// </summary>
    [Select<Confirm_2b>(ID.Menu.Purchase, 3, "PurchaseConfirmRange", "采购确认", Confirm_2b.ALL, new[] { Confirm_2b.None, Confirm_2b.Internal, Confirm_2b.ALL })]
    PURCHASE_CONFIRM_RANGE = Flag.FlAG_BIT_03| Flag.FlAG_BIT_04,
    /// <summary>
    /// 亚马逊订单 可查看店铺
    /// </summary>
    [Select<StoreRange_1b>(ID.Menu.PurchaseDistribution, 4, "PurchaseStore", "可查看店铺", StoreRange_1b.Authorization, new[] { StoreRange_1b.Authorization, StoreRange_1b.CompanyAll })]
    STORE_RANGE = Flag.FlAG_BIT_05,
    /// <summary>
    /// 无需找货
    /// </summary>
    [CheckBox(ID.Menu.Purchase, 5, "PurchaseNoFindProduct", "无需找货", true)]
    PURCHASE_NO_FIND_PRODUCT = Flag.FlAG_BIT_06,
    /// <summary>
    /// 修改
    /// </summary>
    [CheckBox(ID.Menu.Purchase, 6, "PurchaseEdit", "修改", true)]
    PURCHASE_EDIT = Flag.FlAG_BIT_07,
    /// <summary>
    /// 自定义添加采购
    /// </summary>
    [CheckBox(ID.Menu.Purchase, 7, "PurchaseAddCustomPurchase", "自定义添加采购", true)]
    PURCHASE_ADD_CUSTOM_PURCHASE = Flag.FlAG_BIT_08,   
    /// <summary>
    /// 修改
    /// </summary>
    [CheckBox(ID.Menu.Purchase, 8, "PurchaseEditInfo", "修改", true)]
    PURCHASE_EDIT_INFO = Flag.FlAG_BIT_09,
    /// <summary>
    /// 提交采购信息
    /// </summary>
    [CheckBox(ID.Menu.Purchase, 9, "PurchaseSubPurchase", "提交采购信息", true)]
    PURCHASE_SUB_PURCHASE = Flag.FlAG_BIT_10,
    /// <summary>
    /// 提交采购信息
    /// </summary>
    [CheckBox(ID.Menu.WaitForFind, 1, "AffirmFindProductAdd", "添加找货", true)]
    AFFIRM_FIND_PRODUCT_ADD = Flag.FlAG_BIT_11,
    /// <summary>
    /// 提交采购信息
    /// </summary>
    [CheckBox(ID.Menu.WaitForFind, 2, "AffirmFindProductEdit", "编辑找货", true)]
    AFFIRM_FIND_PRODUCT_EDIT = Flag.FlAG_BIT_12,
    /// <summary>
    /// 提交采购信息
    /// </summary>
    [CheckBox(ID.Menu.WaitForFind, 3, "AffirmFindProductAffirm", "确认找货", true)]
    AFFIRM_FIND_PRODUCT_AFFIRM = Flag.FlAG_BIT_13,

    /// <summary>
    /// 
    /// </summary>
    [CheckBox(ID.Menu.Supplier, 3, "SupplierAdd", "添加", true)]
    SUPPLIER_ADD = Flag.FlAG_BIT_14,
    /// <summary>
    /// 
    /// </summary>
    [CheckBox(ID.Menu.Supplier, 2, "SupplierEdit", "编辑", true)]
    SUPPLIER_EDIT = Flag.FlAG_BIT_15,
    /// <summary>
    /// 
    /// </summary>
    [CheckBox(ID.Menu.Supplier, 1, "SupplierDelete", "删除", true)]
    SUPPLIER_DELETE = Flag.FlAG_BIT_16,


    /// <summary>
    /// 搜索功能
    /// </summary>
    [CheckBox(ID.Menu.MyPurchase,1, "MyPurchaseListSearch", "搜索功能", true)]
    MYPURCHASELISTSEARCH = Flag.FlAG_BIT_17,

}