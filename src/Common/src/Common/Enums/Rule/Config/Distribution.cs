﻿using ERP.Enums.Rule.Config.Internal;
using ERP.Attributes.Rule;

namespace ERP.Enums.Rule.Config;
public enum Distribution : long
{
    /// <summary>
    /// 上级订单  真假发货
    /// </summary>
    [CheckBox(ID.Menu.OrderDistribution, 7, "OrderDelivery", "真假发货[上级]", true)]
    ORDER_DELIVERY = Flag.FlAG_BIT_01,
    /// <summary>
    /// 上级订单  修改订单收货地址
    /// </summary>
    [CheckBox(ID.Menu.OrderDistribution, 6, "OrderShipping", "修改收货地址[上级]", true)]
    ORDER_SHIPPING = Flag.FlAG_BIT_02,
    /// <summary>
    /// 上级订单  采购确认
    /// </summary>
    ORDER_CONFIRM = Flag.FlAG_BIT_03,
    /// <summary>
    /// 分销订单详情更改订单进度
    /// </summary>
    [CheckBox(ID.Menu.OrderDistribution, 8, "OrderProgress", "更改订单进度[上级]", true)]
    ORDER_PROGRESS = Flag.FlAG_BIT_04,


    //分销产品
    //上级
    /// <summary>
    /// 分销产品绑定分类
    /// </summary>
    [CheckBox(ID.Menu.ProductDistribution, 1, "BindingCategory", "绑定分类[上级]", true)]
    BINDING_CATEGORY = Flag.FlAG_BIT_05,
    /// <summary>
    /// 分销产品分类
    /// </summary>
    [CheckBox(ID.Menu.ProductDistribution, 2, "CategoryDistribution", "分销产品分类[上级]", true)]
    CATEGORY = Flag.FlAG_BIT_06,
    /// <summary>
    /// 取消分销
    /// </summary>
    [CheckBox(ID.Menu.ProductDistribution, 3, "CancelDistribution", "取消分销[上级]", true)]
    CANCEL_DISTRIBUTION = Flag.FlAG_BIT_07,
    //下级
    /// <summary>
    /// 分销购物车
    /// </summary>
    [CheckBox(ID.Menu.ProductDistribution, 5, "Cart", "购物车[下级]", true)]
    CART = Flag.FlAG_BIT_08,
    /// <summary>
    /// 添加购物车
    /// </summary>
    [CheckBox(ID.Menu.ProductDistribution, 4, "CartAdd", "购买[下级]", true)]
    CART_ADD = Flag.FlAG_BIT_09,
    //公共
    /// <summary>
    /// 产品详情
    /// </summary>
    [CheckBox(ID.Menu.ProductDistribution, 4, "ProductDetail", "产品详情", true)]
    PRODUCT_DETAIL = Flag.FlAG_BIT_10,

    //分销财务
    //上级
    /// <summary>
    /// 钱包列表
    /// </summary>
    [CheckBox(ID.Menu.FinanceDistribution, 5, "WaletList", "钱包列表[上级]", true)]
    WALET_LIST = Flag.FlAG_BIT_11,
    /// <summary>
    /// 审批列表
    /// </summary>
    [CheckBox(ID.Menu.FinanceDistribution, 6, "ApprovalList", "审批列表[上级]", true)]
    APPROVAL_LIST = Flag.FlAG_BIT_12,
    /// <summary>
    /// 在线支付配置
    /// </summary>
    [CheckBox(ID.Menu.FinanceDistribution, 7, "OnlinePaymentConfig", "在线支付配置列表[上级]", true)]
    ONLINE_PAYMENT_CONFIG = Flag.FlAG_BIT_13,
    /// <summary>
    /// 钱包账单
    /// </summary>
    [CheckBox(ID.Menu.FinanceDistribution, 8, "WalletBill", "钱包账单[上级]", true)]
    WALLET_BILL = Flag.FlAG_BIT_14,
    /// <summary>
    /// 审批
    /// </summary>
    [CheckBox(ID.Menu.FinanceDistribution, 9, "Approval", "账单审批[上级]", true)]
    APPROVAL = Flag.FlAG_BIT_15,
    /// <summary>
    /// 配置在线支付设置
    /// </summary>
    [CheckBox(ID.Menu.FinanceDistribution, 10, "OnlinePaymentEdit", "在线支付设置[上级]", true)]
    ONLINE_PAYMENT_EDIT = Flag.FlAG_BIT_16,
    //下级
    /// <summary>
    /// 公司账单
    /// </summary>
    [CheckBox(ID.Menu.FinanceDistribution, 11, "CompanyBill", "公司账单[下级]", true)]
    COMPANY_BILL = Flag.FlAG_BIT_17,
    /// <summary>
    /// 待审批列表
    /// </summary>
    [CheckBox(ID.Menu.FinanceDistribution, 12, "WaitApprovalList", "待审批列表[下级]", true)]
    WAIT_APPROVAL_LIST = Flag.FlAG_BIT_18,
    /// <summary>
    /// 在线支付列表
    /// </summary>
    [CheckBox(ID.Menu.FinanceDistribution, 13, "OnlinePaymentList", "在线支付列表[下级]", true)]
    ONLINE_PAYMENT_LIST = Flag.FlAG_BIT_19,
    /// <summary>
    /// 员工财务统计
    /// </summary>
    [CheckBox(ID.Menu.FinanceDistribution, 14, "UserStatistics", "用户财务统计[下级]", true)]
    USER_STATISTICS = Flag.FlAG_BIT_20,
    /// <summary>
    /// 用户组财务统计
    /// </summary>
    [CheckBox(ID.Menu.FinanceDistribution, 15, "GroupStatistics", "用户组财务统计[下级]", true)]
    GROUP_STATISTICS = Flag.FlAG_BIT_21,
    /// <summary>
    /// 店铺财务统计
    /// </summary>
    [CheckBox(ID.Menu.FinanceDistribution, 16, "StoreStatistics", "店铺财务统计[下级]", true)]
    STORE_STATISTICS = Flag.FlAG_BIT_22,
    /// <summary>
    /// 在线支付
    /// </summary>
    [CheckBox(ID.Menu.FinanceDistribution, 17, "OnlinePayment", "在线支付[下级]", true)]
    ONLINE_PAYMENT = Flag.FlAG_BIT_23,
    //公共
    /// <summary>
    /// 财务统计
    /// </summary>
    [CheckBox(ID.Menu.FinanceDistribution, 1, "FinancialStatistics", "财务统计", true)]
    FINANCIAL_STATISTICS = Flag.FlAG_BIT_24,
    /// <summary>
    /// 充值
    /// </summary>
    [CheckBox(ID.Menu.FinanceDistribution, 2, "Recharge", "充值", true)]
    RECHARGE = Flag.FlAG_BIT_25,
    /// <summary>
    /// 支出
    /// </summary>
    [CheckBox(ID.Menu.FinanceDistribution, 3, "Expend", "支出", true)]
    EXPEND = Flag.FlAG_BIT_26,
    /// <summary>
    /// 账单详情
    /// </summary>
    [CheckBox(ID.Menu.FinanceDistribution, 4, "BillDetail", "账单详情", true)]
    BILL_DETAIL = Flag.FlAG_BIT_27,

    //分销运单
    //下级
    /// <summary>
    /// 退回
    /// </summary>
    [CheckBox(ID.Menu.WaybillDistribution, 3, "BackWaybill", "发货上报退回[下级]", true)]
    BACK_WAYBILL = Flag.FlAG_BIT_28,
    /// <summary>
    /// 可查看店铺 1 => '公司全部', 0 => '分配店铺'
    /// </summary>
    [Radio<StoreRange_1b>(ID.Menu.WaybillDistribution, 4, "WaybillStore", "可查看店铺[下级]", StoreRange_1b.Authorization, new[] { StoreRange_1b.Authorization, StoreRange_1b.CompanyAll })]
    STORE_WAYBILL = Flag.FlAG_BIT_29,
    //上级
    /// <summary>
    /// 我的运单
    /// </summary>
    [CheckBox(ID.Menu.WaybillDistribution, 5, "MyWaybill", "我的运单", true)]
    MY_WAYBILL = Flag.FlAG_BIT_30,
    /// <summary>
    /// 上级运单 采购确认
    /// </summary>
    [Radio<Confirm_2b>(ID.Menu.WaybillDistribution, 6, "WaybillConfirm", "物流确认[上级]", Confirm_2b.ALL, new[] { Confirm_2b.None, Confirm_2b.Internal, Confirm_2b.ALL })]
    WAYBILL_CONFIRM_RANGE = Flag.FlAG_BIT_31 | Flag.FlAG_BIT_32,
    /// <summary>
    /// 发货
    /// </summary>
    [CheckBox(ID.Menu.WaybillDistribution, 7, "DeliverWaybill", "发货[上级]", true)]
    DELIVER_WAYBILL = Flag.FlAG_BIT_33,
    /// <summary>
    /// 编辑运单
    /// </summary>
    [CheckBox(ID.Menu.WaybillDistribution, 8, "EditWaybill", "编辑运单[上级]", true)]
    EDIT_WAYBILL = Flag.FlAG_BIT_34,
    //公共
    /// <summary>
    /// 待发货订单
    /// </summary>
    [CheckBox(ID.Menu.WaybillDistribution, 1, "WaitWaybillOrder", "待发货订单", true)]
    WAYBILL_ORDER = Flag.FlAG_BIT_35,
    /// <summary>
    /// 运单列表
    /// </summary>
    [CheckBox(ID.Menu.WaybillDistribution, 2, "WaybillList", "运单列表", true)]
    WAYBILL_LIST = Flag.FlAG_BIT_36,
    /// <summary>
    /// 运单跟踪
    /// </summary>
    [CheckBox(ID.Menu.WaybillDistribution, 9, "TraceWaybill", "运单跟踪", true)]
    TRACE_WAYBILL = Flag.FlAG_BIT_37,
    /// <summary>
    /// 运单详情
    /// </summary>
    [CheckBox(ID.Menu.WaybillDistribution, 11, "ViewWaybill", "运单详情", true)]
    VIEW_WAYBILL = Flag.FlAG_BIT_38,
    /// <summary>
    /// 确认运单
    /// </summary>
    [CheckBox(ID.Menu.WaybillDistribution, 10, "ConfirmWaybill", "确认运单", true)]
    CONFIRM_WAYBILL = Flag.FlAG_BIT_39,
    /// <summary>
    /// 订单详情
    /// </summary>
    [CheckBox(ID.Menu.WaybillDistribution, 12, "OrderDetailWaybill", "订单详情", true)]
    ORDER_DETAIL_WAYBILL = Flag.FlAG_BIT_40,//AMAZON_DETAIL_WAYBILL

    //分销采购
    //下级
    /// <summary>
    /// 退回
    /// </summary>
    [CheckBox(ID.Menu.PurchaseDistribution, 5, "PurchaseBack", "采购上报退回[下级]", true)]
    BACK_PURCHASE = Flag.FlAG_BIT_41,
    /// <summary>
    /// 可查看店铺 1 => '公司全部', 0 => '用户组内'
    /// </summary>
    [Radio<StoreRange_1b>(ID.Menu.PurchaseDistribution, 6, "PurchaseStore", "可查看店铺[下级]", StoreRange_1b.Authorization, new[] { StoreRange_1b.Authorization, StoreRange_1b.CompanyAll })]
    PURCHASE_STORE = Flag.FlAG_BIT_42,
    //上级
    /// <summary>
    /// 我的采购
    /// </summary>
    [CheckBox(ID.Menu.PurchaseDistribution, 7, "MyPurchase", "我的采购", true)]
    MY_PURCHASE = Flag.FlAG_BIT_43 | Flag.FlAG_BIT_44,
    /// <summary>
    /// 上级采购  采购确认
    /// </summary>
    [Radio<Confirm_2b>(ID.Menu.PurchaseDistribution, 8, "PurchaseConfirm", "采购确认[上级]", Confirm_2b.ALL, new[] { Confirm_2b.None, Confirm_2b.Internal, Confirm_2b.ALL })]
    PURCHASE_CONFIRM_RANGE = Flag.FlAG_BIT_45,
    /// <summary>
    /// 新增采购
    /// </summary>
    [CheckBox(ID.Menu.PurchaseDistribution, 9, "AddPurchase", "新增采购[上级]", true)]
    ADD_PURCHASE = Flag.FlAG_BIT_46,
    /// <summary>
    /// 编辑采购
    /// </summary>
    [CheckBox(ID.Menu.PurchaseDistribution, 10, "EditPurchase", "编辑采购[上级]", true)]
    EDIT_PURCHASE = Flag.FlAG_BIT_47,
    //公共
    /// <summary>
    /// 采购详情
    /// </summary>
    [CheckBox(ID.Menu.PurchaseDistribution, 11, "ViewPurchase", "采购详情", true)]
    VIEW_PURCHASE = Flag.FlAG_BIT_48,
    /// <summary>
    /// 订单详情
    /// </summary>
    [CheckBox(ID.Menu.PurchaseDistribution, 12, "DetailPurchase", "订单详情", true)]
    ORDER_DETAIL_PURCHASE = Flag.FlAG_BIT_49,//AMAZON_DETAIL_PURCHASE
    /// <summary>
    /// 待采购订单
    /// </summary>
    [CheckBox(ID.Menu.PurchaseDistribution, 1, "WaitOrder", "待采购订单", true)]
    WAIT_ORDER = Flag.FlAG_BIT_50,
    /// <summary>
    /// 采购中列表
    /// </summary>
    [CheckBox(ID.Menu.PurchaseDistribution, 2, "PurchaseAlready", "采购中列表", true)]
    PURCHASE_ALREADY = Flag.FlAG_BIT_51,
    /// <summary>
    /// 采购列表
    /// </summary>
    [CheckBox(ID.Menu.PurchaseDistribution, 3, "PurchaseList", "采购列表", true)]
    PURCHASE_LIST = Flag.FlAG_BIT_52,

    //分销订单
    /// <summary>
    /// 订单详情
    /// </summary>
    [CheckBox(ID.Menu.OrderDistribution, 4, "Detail", "订单详情", true)]
    ORDER_DETAIL = Flag.FlAG_BIT_53,//AMAZON_DETAIL
    /// <summary>
    /// 列表商品信息
    /// </summary>
    [Radio<Distribution_GOODS>(ID.Menu.OrderDistribution, 3, "Goods", "列表商品信息", Distribution_GOODS.Show, new[] { Distribution_GOODS.Show, Distribution_GOODS.Hide })]
    ORDER_GOODINFO = Flag.FlAG_BIT_54,//AMAZON_GOOD
    /// <summary>
    /// 店铺订单 可查看店铺 1 => '公司全部', 0 => '已分配店铺'
    /// </summary>
    [Radio<StoreRange_1b>(ID.Menu.OrderDistribution, 2, "StoreRange", "可查看店铺[下级]", StoreRange_1b.Authorization, new[] { StoreRange_1b.Authorization, StoreRange_1b.CompanyAll })]
    STORERANGE = Flag.FlAG_BIT_55,//AMAZON_STORE

    //分销公司
    /// <summary>
    /// 费率设置
    /// </summary>
    [CheckBox(ID.Menu.CompanyDistribution, 1, "Rate", "费率设置", true)]
    COMPANY_RATE = Flag.FlAG_BIT_56,

    /// <summary>
    /// 上级查看下级亚马逊订单汇总
    /// </summary>
    [CheckBox(ID.Menu.OrderDistribution, 1, "OrderStatistics", "查看下级订单汇总[上级]", true)]
    ORDER_STATISTICS = Flag.FlAG_BIT_57,//AMAZONORDER_STATISTICS
    /// <summary>
    /// 上级订单 采购确认
    /// </summary>
    [Radio<Confirm_2b>(ID.Menu.OrderDistribution, 5, "OrderConfirm", "采购确认[上级]", Confirm_2b.ALL, new[] { Confirm_2b.None, Confirm_2b.Internal, Confirm_2b.ALL })]
    ORDER_CONFIRM_RANGE = Flag.FlAG_BIT_58,
}
