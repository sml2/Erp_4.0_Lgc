﻿using ERP.Attributes.Rule;

namespace ERP.Enums.Rule.Config;

public enum Message:long
{
    /// <summary>
    /// 新增工单问题类型
    /// Menu:<see cref="ID.Menu.JobProblemMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.JobProblemMessage, 1, "AddJobProblem", "新增工单问题类型", true)]
    ADD_JOB_PROBLEM = Flag.FlAG_BIT_01,
    /// <summary>
    /// 工单问题类型添加
    /// Menu:<see cref="ID.Menu.JobProblemMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.JobProblemMessage, 2, "JobProblemAdd", "工单问题类型添加", true)]
    JOB_PROBLEM_ADD = Flag.FlAG_BIT_02,
    /// <summary>
    /// 工单问题类型修改
    /// Menu:<see cref="ID.Menu.JobProblemMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.JobProblemMessage, 3, "JobProblemEdit", "工单问题类型修改", true)]
    JOB_PROBLEM_EDIT = Flag.FlAG_BIT_03,
    /// <summary>
    /// 工单问题类型删除
    /// Menu:<see cref="ID.Menu.JobProblemMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.JobProblemMessage, 4, "JobProblemDel", "工单问题类型删除", true)]
    JOB_PROBLEM_DEL = Flag.FlAG_BIT_04,

    /// <summary>
    /// 工单进度类型添加
    /// Menu:<see cref="ID.Menu.JobProgressMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.JobProgressMessage, 1, "JobProgressAdd", "工单进度类型添加", true)]
    JOB_PROGRESS_ADD = Flag.FlAG_BIT_05,
    /// <summary>
    /// 工单进度类型修改
    /// Menu:<see cref="ID.Menu.JobProgressMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.JobProgressMessage, 2, "JobProgressEdit", "工单进度类型修改", true)]
    JOB_PROGRESS_EDIT = Flag.FlAG_BIT_06,
    /// <summary>
    /// 工单进度类型删除
    /// Menu:<see cref="ID.Menu.JobProgressMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.JobProgressMessage, 3, "JobProgressDel", "工单进度类型删除", true)]
    JOB_PROGRESS_DEL = Flag.FlAG_BIT_07,

    /// <summary>
    /// 我的工单添加
    /// Menu:<see cref="ID.Menu.JobMyMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.JobMyMessage, 1, "JobMyAdd", "我的工单添加", true)]
    JOB_MY_ADD = Flag.FlAG_BIT_08,
    /// <summary>
    /// 我的工单查看
    /// Menu:<see cref="ID.Menu.JobMyMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.JobMyMessage, 2, "JobMyView", "我的工单查看", true)]
    JOB_MY_VIEW = Flag.FlAG_BIT_09,
    /// <summary>
    /// 我的工单撤销
    /// Menu:<see cref="ID.Menu.JobMyMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.JobMyMessage, 3, "JobMyRevoke", "我的工单撤销", true)]
    JOB_MY_REVOKE = Flag.FlAG_BIT_10,

    /// <summary>
    /// 待处理工单处理
    /// Menu:<see cref="ID.Menu.JobPendingMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.JobPendingMessage, 1, "JobPendingProcessing", "待处理工单处理", true)]
    JOB_PENDING_PROCESSING = Flag.FlAG_BIT_11,
    /// <summary>
    /// 待处理工单完成
    /// Menu:<see cref="ID.Menu.JobPendingMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.JobPendingMessage, 2, "JobPendingComplete", "待处理工单完成", true)]
    JOB_PENDING_COMPLETE = Flag.FlAG_BIT_12,

    /// <summary>
    /// 客服邮箱设置 添加邮箱
    /// Menu:<see cref="ID.Menu.EmailListMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.EmailListMessage, 1, "EmailListAdd", "添加邮箱", true)]
    EMAIL_LIST_ADD = Flag.FlAG_BIT_13,
    /// <summary>
    /// 客服邮箱设置 发送邮件
    /// Menu:<see cref="ID.Menu.EmailListMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.EmailListMessage, 2, "EmailListSend", "发送邮件", true)]
    EMAIL_LIST_SEND = Flag.FlAG_BIT_14,
    /// <summary>
    /// 客服邮箱设置 删除邮箱
    /// Menu:<see cref="ID.Menu.EmailListMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.EmailListMessage, 3, "EmailListDel", "删除邮箱", true)]
    EMAIL_LIST_DEL = Flag.FlAG_BIT_15,
    /// <summary>
    /// 客服邮箱设置 编辑邮箱
    /// Menu:<see cref="ID.Menu.EmailListMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.EmailListMessage, 4, "EmailListEdit", "编辑邮箱", true)]
    EMAIL_LIST_EDIT = Flag.FlAG_BIT_16,
    /// <summary>
    /// 客服邮箱设置 重置邮箱
    /// Menu:<see cref="ID.Menu.EmailListMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.EmailListMessage, 5, "EmailListReset", "重置邮箱", true)]
    EMAIL_LIST_RESET = Flag.FlAG_BIT_17,
    ///<summary>
    /// 客服邮箱设置 修改使用状态
    /// Menu:<see cref="ID.Menu.EmailListMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.EmailListMessage, 6, "EmailListChangeState", "修改使用状态", true)]
    LIST_CHANGE_STATE = Flag.FlAG_BIT_18,
    /// <summary>
    /// 客服邮箱设置 同步邮件
    /// Menu:<see cref="ID.Menu.EmailListMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.EmailListMessage, 7, "EmailListSynchronous", "同步邮件", true)]
    EMAIL_LIST_SYNCHRONOUS = Flag.FlAG_BIT_19,

    /// <summary>
    /// 收件箱 删除
    /// Menu:<see cref="ID.Menu.EmailCollectionMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.EmailCollectionMessage, 1, "EmailCollectionDel", "收件箱删除", true)]
    EMAIL_COLLECTION_DEL = Flag.FlAG_BIT_20,
    /// <summary>
    /// 收件箱 导入
    /// Menu:<see cref="ID.Menu.EmailCollectionMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.EmailCollectionMessage, 2, "EmailCollectionImport", "收件箱导入", true)]
    EMAIL_COLLECTION_IMPORT = Flag.FlAG_BIT_21,
    /// <summary>
    /// 收件箱 详情
    /// Menu:<see cref="ID.Menu.EmailCollectionMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.EmailCollectionMessage, 3, "EmailCollectionDetails", "收件箱详情", true)]
    EMAIL_COLLECTION_DETAILS = Flag.FlAG_BIT_22,
    /// <summary>
    /// 收件箱 回复
    /// Menu:<see cref="ID.Menu.EmailCollectionMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.EmailCollectionMessage, 4, "EmailCollectionReply", "收件箱回复", true)]
    EMAIL_COLLECTION_REPLY = Flag.FlAG_BIT_23,

    /// <summary>
    /// 邮件营销 添加
    /// Menu:<see cref="ID.Menu.EmailMassMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.EmailMassMessage, 1, "EmailMassAdd", "添加", true)]
    EMAIL_MASS_ADD = Flag.FlAG_BIT_24,
    /// <summary>
    /// 邮件营销 邮件群发
    /// Menu:<see cref="ID.Menu.EmailMassMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.EmailMassMessage, 2, "EmailMassSend", "邮件群发", true)]
    EMAIL_MASS_SEND = Flag.FlAG_BIT_25,
    /// <summary>
    /// 邮件营销 删除
    /// Menu:<see cref="ID.Menu.EmailMassMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.EmailMassMessage, 3, "EmailMassDel", "删除", true)]
    EMAIL_MASS_DEL = Flag.FlAG_BIT_26,
    /// <summary>
    /// 邮件营销 编辑
    /// Menu:<see cref="ID.Menu.EmailMassMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.EmailMassMessage, 4, "EmailMassEdit", "编辑", true)]
    EMAIL_MASS_EDIT = Flag.FlAG_BIT_27,

    /// <summary>
    /// 发送历史 发送邮件
    /// Menu:<see cref="ID.Menu.EmailSendMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.EmailSendMessage, 1, "EmailSendSending", "发送邮件", true)]
    EMAIL_SEND_SENDING = Flag.FlAG_BIT_28,
    /// <summary>
    /// 发送历史 删除邮件
    /// Menu:<see cref="ID.Menu.EmailSendMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.EmailSendMessage, 2, "EmailSendDel", "删除", true)]
    EMAIL_SEND_DEL = Flag.FlAG_BIT_29,
    /// <summary>
    /// 发送历史 导入群发邮箱
    /// Menu:<see cref="ID.Menu.EmailSendMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.EmailSendMessage, 3, "EmailSendImport", "导入群发邮箱", true)]
    EMAIL_SEND_IMPORT = Flag.FlAG_BIT_30,
    /// <summary>
    /// 发送历史 详情
    /// Menu:<see cref="ID.Menu.EmailSendMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.EmailSendMessage, 4, "EmailSendDetails", "详情", true)]
    EMAIL_SEND_DETAILS = Flag.FlAG_BIT_31,

    /// <summary>
    /// 反馈意见 详情
    /// Menu:<see cref="ID.Menu.FeedBackMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.FeedBackMessage, 1, "FeedBackDetails", "详情", true)]
    FEED_BACK_DETAILS = Flag.FlAG_BIT_32,
    /// <summary>
    /// 反馈意见 完成
    /// Menu:<see cref="ID.Menu.FeedBackMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.FeedBackMessage, 2, "FeedBackComplete", "完成", true)]
    FEED_BACK_COMPLETE = Flag.FlAG_BIT_33,
    /// <summary>
    /// 反馈意见 撤销
    /// Menu:<see cref="ID.Menu.FeedBackMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.FeedBackMessage, 3, "FeedBackRevoke", "撤销", true)]
    FEED_BACK_REVOKE = Flag.FlAG_BIT_34,

    /// <summary>
    /// 反馈处理 详情
    /// Menu:<see cref="ID.Menu.ProductDistribution"/>
    /// </summary>
    [CheckBox(ID.Menu.ProductDistribution, 1, "ProcessingFeedBackDetails", "详情", true)]
    PROCESSING_FEED_BACK_DETAILS = Flag.FlAG_BIT_35,
    /// <summary>
    /// 反馈处理 完成
    /// Menu:<see cref="ID.Menu.ProductDistribution"/>
    /// </summary>
    [CheckBox(ID.Menu.ProductDistribution, 2, "ProcessingFeedBackComplete", "完成", true)]
    PROCESSING_FEED_BACK_COMPLETE = Flag.FlAG_BIT_36,
    /// <summary>
    /// 反馈处理 删除
    /// Menu:<see cref="ID.Menu.ProductDistribution"/>
    /// </summary>
    [CheckBox(ID.Menu.ProductDistribution, 3, "ProcessingFeedBackDel", "删除", true)]
    PROCESSING_FEED_BACK_DEL = Flag.FlAG_BIT_37,

    /// <summary>
    /// 通知公告 新建公告
    /// Menu:<see cref="ID.Menu.NoticeMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.NoticeMessage, 1, "NoticeAdd", "新建公告", true)]
    NOTICE_ADD = Flag.FlAG_BIT_38,
    /// <summary>
    /// 通知公告 预览
    /// Menu:<see cref="ID.Menu.NoticeMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.NoticeMessage, 2, "NoticePreview", "预览", true)]
    NOTICE_PREVIEW = Flag.FlAG_BIT_39,
    /// <summary>
    /// 通知公告 编辑
    /// Menu:<see cref="ID.Menu.NoticeMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.NoticeMessage, 3, "NoticeEdit", "编辑", true)]
    NOTICE_EDIT = Flag.FlAG_BIT_40,
    /// <summary>
    /// 通知公告 删除
    /// Menu:<see cref="ID.Menu.NoticeMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.NoticeMessage, 4, "NoticeDel", "删除", true)]
    NOTICE_DEL = Flag.FlAG_BIT_41,
    /// <summary>
    /// 通知公告 系统公告
    /// Menu:<see cref="ID.Menu.NoticeMessage"/>
    /// </summary>
    [CheckBox(ID.Menu.NoticeMessage, 5, "NoticeAll", "系统公告", true)]
    NOTICE_ALL = Flag.FlAG_BIT_42,
}
