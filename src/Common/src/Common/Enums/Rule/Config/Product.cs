﻿using ERP.Enums.Rule.Config.Internal;
using ERP.Attributes.Rule;

namespace ERP.Enums.Rule.Config;

public enum Product : long
{
    /// <summary>
    /// 可查看范围
    /// </summary>
    [Select<DataRange_2b>(ID.Menu.Multiple, 1, "ProductRange", "查看范围[自增产品]", DataRange_2b.User,
        new[] { DataRange_2b.None, DataRange_2b.User, DataRange_2b.Group, DataRange_2b.Company },Shift = 0)]
    ProductRange = Flag.FlAG_BIT_01 | Flag.FlAG_BIT_02,

    /// <summary>
    /// 采集产品
    /// </summary>
    [CheckBox(ID.Menu.Multiple, 2, "CollectProduct", "采集产品", true)]
    CollectProduct = Flag.FlAG_BIT_03,

    /// <summary>
    /// 自增产品
    /// </summary>
    [CheckBox(ID.Menu.Multiple, 3, "ManualProduct", "自增产品", true)]
    ManualProduct = Flag.FlAG_BIT_04,

    /// <summary>
    /// 审核产品
    /// </summary>
    [CheckBox(ID.Menu.Multiple, 4, "AuditProduct", "审核产品", true)]
    AuditProduct = Flag.FlAG_BIT_05,

    /// <summary>
    /// 分享产品
    /// </summary>
    [CheckBox(ID.Menu.Multiple, 5, "ShareProduct", "分享产品", true)]
    ShareProduct = Flag.FlAG_BIT_06,

    /// <summary>
    /// 产品回收站
    /// </summary>
    [CheckBox(ID.Menu.Multiple, 6, "RecycleProduct", "产品回收站", true)]
    RecycleProduct = Flag.FlAG_BIT_07,

    /// <summary>
    /// 搜索功能
    /// </summary>
    [CheckBox(ID.Menu.Multiple, 7, "ProductSearch", "搜索功能", true)]
    ProductSearch = Flag.FlAG_BIT_08,

    /// <summary>
    /// 产品新增
    /// </summary>
    [CheckBox(ID.Menu.Multiple, 8, "ProductAdd", "产品新增", true)]
    ProductAdd = Flag.FlAG_BIT_09,

    /// <summary>
    /// 产品修改
    /// </summary>
    [CheckBox(ID.Menu.Multiple, 9, "ProductEdit", "产品修改", true)]
    ProductEdit = Flag.FlAG_BIT_10,

    /// <summary>
    /// 产品删除
    /// </summary>
    [CheckBox(ID.Menu.Multiple, 10, "ProductDelete", "产品删除", true)]
    ProductDelete = Flag.FlAG_BIT_11,

    // /// <summary>
    // /// 产品审核
    // /// </summary>
    // [CheckBox(ID.Menu.Multiple, 11, "ProductAudit", "产品审核", true)]
    // PRODUCT_AUDIT = Flag.FlAG_BIT_12,

    /// <summary>
    /// 产品标题检测
    /// </summary>
    [CheckBox(ID.Menu.Multiple, 12, "ProductCheckTitle", "产品标题检测", true)]
    ProductCheckTitle = Flag.FlAG_BIT_13,

    /// <summary>
    /// 产品标题检测
    /// </summary>
    [CheckBox(ID.Menu.Multiple, 13, "ProductClone", "产品克隆", true)]
    ProductClone = Flag.FlAG_BIT_14,

    /// <summary>
    /// 产品翻译
    /// </summary>
    [CheckBox(ID.Menu.Multiple, 14, "ProductTranslate", "产品翻译", true)]
    ProductTranslate = Flag.FlAG_BIT_15,

    /// <summary>
    /// 产品添加上传
    /// </summary>
    [CheckBox(ID.Menu.Multiple, 15, "ProductAddUpload", "产品添加上传", true)]
    ProductAddUpload = Flag.FlAG_BIT_16,

    /// <summary>
    /// 产品分享
    /// </summary>
    [CheckBox(ID.Menu.Multiple, 16, "ProductShare", "产品分享", true)]
    ProductShare = Flag.FlAG_BIT_17,

    /// <summary>
    /// 下载图片
    /// </summary>
    [CheckBox(ID.Menu.Multiple, 17, "ProductDownLoadImages", "下载图片", true)]
    ProductDownloadImages = Flag.FlAG_BIT_18,

    /// <summary>
    /// 以图找货
    /// </summary>
    [CheckBox(ID.Menu.Multiple, 18, "ProductFindByImage", "以图找货", true)]
    ProductFindByImage = Flag.FlAG_BIT_19,

    /// <summary>
    /// 产品真删除
    /// </summary>
    [CheckBox(ID.Menu.Multiple, 19, "ProductTrueDelete", "产品真删除", true)]
    ProductTrueDelete = Flag.FlAG_BIT_20,

    /// <summary>
    /// 产品分享导入
    /// </summary>
    [CheckBox(ID.Menu.Multiple, 20, "ProductShareImport", "产品分享导入", true)]
    ProductShareImport = Flag.FlAG_BIT_21,

    /// <summary>
    /// 产品分享导入
    /// </summary>
    // [CheckBox(ID.Menu.Multiple, 21, "ProductImport", "产品分享导入", true)]
    // PRODUCT_IMPORT= Flag.FlAG_BIT_22,

    /// <summary>
    /// 同步图片
    /// </summary>
    [CheckBox(ID.Menu.Multiple, 22, "ProductSyncImages", "同步图片", true)]
    ProductSyncImages = Flag.FlAG_BIT_23,

    /// <summary>
    /// 填充数据
    /// </summary>
    [CheckBox(ID.Menu.Multiple, 23, "ProductFillBasic", "填充数据", true)]
    ProductFillBasic = Flag.FlAG_BIT_24,

    /// <summary>
    /// 是否显示金额
    /// </summary>
    // [CheckBox(ID.Menu.Multiple, 24, "ProductIsShowPrices", "是否显示金额", true)]
    // PRODUCT_IS_SHOW_PRICES = Flag.FlAG_BIT_25,

    /// <summary>
    /// 绑定分类
    /// </summary>
    [CheckBox(ID.Menu.Multiple, 25, "ProductBindCategory", "绑定分类", true)]
    ProductBindCategory = Flag.FlAG_BIT_26,


    /// <summary>
    /// 产品导出
    /// </summary>
    [CheckBox(ID.Menu.Multiple, 26, "ProductExprot", "产品导出", true)]
    ProductExport = Flag.FlAG_BIT_27,

    /// <summary>
    /// 产品随机填充
    /// </summary>
    [CheckBox(ID.Menu.Multiple, 27, "ProductRandomFill", "产品随机填充", true)]
    ProductRandomFill = Flag.FlAG_BIT_28,

    /// <summary>
    /// 产品改价
    /// </summary>
    [CheckBox(ID.Menu.Multiple, 28, "ProductChangePrices", "产品改价", true)]
    ProductChangePrices = Flag.FlAG_BIT_29,
    /// <summary>
    /// 产品改价
    /// </summary>
    [CheckBox(ID.Menu.Multiple, 29, "SelectionClone", "选品克隆", true)]
    SelectionClone = Flag.FlAG_BIT_30,

    // /// <summary>
    // /// 产品是否展示图片
    // /// </summary>
    // [CheckBox(ID.Menu.Multiple, 29, "ProductIsShowImage", "产品是否展示图片", true)]
    // PRODUCT_IS_SHOW_IMAGE = Flag.FlAG_BIT_30,

    // /// <summary>
    // /// 产品是否展示库存
    // /// </summary>
    // [CheckBox(ID.Menu.Multiple, 30, "ProductIsShowQuantity", "产品是否展示库存", true)]
    // PRODUCT_IS_SHOW_QUANTITY = Flag.FlAG_BIT_31,

    /// <summary>
    /// 产品历史版本
    /// </summary>
    // [CheckBox(ID.Menu.Multiple, 31, "ProductHistory", "产品历史版本", true)]
    // PRODUCT_HISTORY= Flag.FlAG_BIT_32,

    /// <summary>
    /// 产品历史版本
    /// </summary>
    // [CheckBox(ID.Menu.Multiple, 32, "ProductRollbackHistory", "产品回滚历史版本", true)]
    // PRODUCT_ROLLBACK_HISTORY= Flag.FlAG_BIT_33,
}