﻿using ERP.Attributes.Rule;

namespace ERP.Enums.Rule.Config;
using Extensions;
[Flags]
public enum Order : long
{
    //待合并订单列表
    /// <summary>
    /// 合并订单
    /// </summary>
    [CheckBox(ID.Menu.MergePending, 1, "ConsolidatedOrder", "合并订单", true)]
    CONSOLIDATED_ORDER = Flag.FlAG_BIT_01,
    /// <summary>
    /// 清除待合并列表
    /// </summary>
    [CheckBox(ID.Menu.MergePending, 2, "ConsolidatedOrderClear", "清空待合并订单", true)]
    CONSOLIDATED_ORDER_CLEAR = Flag.FlAG_BIT_02,
    /// <summary>
    /// 设置主订单
    /// </summary>
    [CheckBox(ID.Menu.MergePending, 3, "SetMainOrder", "设置主订单", true)]
    SET_MAIN_ORDER = Flag.FlAG_BIT_03,
    /// <summary>
    /// 移除
    /// </summary>
    [CheckBox(ID.Menu.MergePending, 4, "RemoveMergePending", "移除", true)]
    REMOVE_MERGE_PENDING = Flag.FlAG_BIT_04,

    //合并订单
    /// <summary>
    /// 移除
    /// </summary>
    [CheckBox(ID.Menu.Merge, 1, "RemoveMerge", "移除", true)]
    MERGED_ORDER_REMOV = Flag.FlAG_BIT_05,

    //亚马逊订单
    /// <summary>
    /// 导入邮箱
    /// </summary>
    //[CheckBox(ID.Menu.Order, 4, "ImportEmail", "导入邮箱", true)]
    IMPORT_EMAIL = Flag.FlAG_BIT_06,
    /// <summary>
    /// 可查看店铺 
    /// </summary>
    [Radio<Order_ALLORDER>(ID.Menu.Order, 2, "RuleEdit", "可查看店铺", Order_ALLORDER.Some, new[] { Order_ALLORDER.Some, Order_ALLORDER.ALL })]
    ALL_ORDER = Flag.FlAG_BIT_07,
    /// <summary>
    /// 拉取订单(手动)
    /// </summary>
    [CheckBox(ID.Menu.Order, 5, "PullTheOrder", "拉取订单", true)]
    PULL_THE_ORDER = Flag.FlAG_BIT_08,
    /// <summary>
    /// 订单详情
    /// </summary>
    [CheckBox(ID.Menu.Order, 7, "Detail", "订单详情", true)]
    DETAIL = Flag.FlAG_BIT_09,
    /// <summary>
    /// 添加合并订单
    /// </summary>
    [CheckBox(ID.Menu.Order, 8, "AddMergeList", "添加合并订单", true)]
    ADD_MERGE_LIST = Flag.FlAG_BIT_10,
    /// <summary>
    /// 修改手续费
    /// </summary>
    //[CheckBox(ID.Menu.Order, 9, "EditOrderFee", "修改手续费", true)]
    EDIT_ORDER_FEE = Flag.FlAG_BIT_11,
    /// <summary>
    /// 修改退款金额
    /// </summary>
    //[CheckBox(ID.Menu.Order, 10, "EditOrderRefund", "修改退款金额", true)]
    EDIT_ORDER_REFUND = Flag.FlAG_BIT_12,
    /// <summary>
    /// 同步店铺操作
    /// </summary>
    [CheckBox(ID.Menu.Order, 11, "OperateOrderMessage", "同步店铺操作", true)]
    OPERATE_ORDER_MESSAGE = Flag.FlAG_BIT_13,
    /// <summary>
    /// 修改订单状态
    /// </summary>
    [CheckBox(ID.Menu.Order, 12, "EditOrderProgress", "修改订单状态", true)]
    EDIT_ORDER_PROGRESS = Flag.FlAG_BIT_14,
    /// <summary>
    /// 报备发货
    /// </summary>
    [CheckBox(ID.Menu.Order, 13, "PrepareForShipment", "报备发货", true)]
    PREPARE_FOR_SHIPMENT = Flag.FlAG_BIT_15,
    /// <summary>
    /// 修改待发货数
    /// </summary>
    [CheckBox(ID.Menu.Order, 14, "EditPendingShipment", "修改待发货数", true)]
    EDIT_PENDING_SHIPMENT = Flag.FlAG_BIT_16,
    /// <summary>
    /// 添加采购
    /// </summary>
    [CheckBox(ID.Menu.Order, 15, "AddPurchase", "添加采购", true)]
    ADD_PURCHASE = Flag.FlAG_BIT_17,
    /// <summary>
    /// 编辑采购信息
    /// </summary>
    [CheckBox(ID.Menu.Order, 16, "EditPurchase", "编辑采购信息", true)]
    EDIT_PURCHASE = Flag.FlAG_BIT_18,
    /// <summary>
    /// 添加运单
    /// </summary>
    [CheckBox(ID.Menu.Order, 17, "AddConsignmentNote", "添加运单", true)]
    ADD_CONSIGNMENT_NOTE = Flag.FlAG_BIT_19,
    /// <summary>
    /// 修改收货信息
    /// </summary>
    [CheckBox(ID.Menu.Order, 18, "EditReceiptInformation", "修改收货信息", true)]
    EDIT_RECEIPT_INFORMATION = Flag.FlAG_BIT_20,
    /// <summary>
    /// 添加备注
    /// </summary>
    [CheckBox(ID.Menu.Order, 19, "AddRemark", "添加备注", true)]
    ADD_REMARK = Flag.FlAG_BIT_21,
    /// <summary>
    /// 手动添加订单
    /// </summary>
    [CheckBox(ID.Menu.Order, 6, "AddSelfOrder", "手动添加订单", true)]
    ADD_SELF_ORDER = Flag.FlAG_BIT_22,
    /// <summary>
    /// 采购确认
    /// </summary>
    [CheckBox(ID.Menu.Order, 1, "UserLog", "操作日志", true)]
    PURCHASE_CONFIRM = Flag.FlAG_BIT_23,
    /// <summary>
    /// 分销采购确认
    /// </summary>
    //[CheckBox(ID.Menu.Order, 1, "UserLog", "操作日志", true)]
    PURCHASE_CONFIRM_RANGE = Flag.FlAG_BIT_24 | Flag.FlAG_BIT_25,
    /// <summary>
    /// 批量修改订单进度
    /// </summary>
    [CheckBox(ID.Menu.Order, 3, "ChangeOrderProgress", "批量修改订单进度", true)]
    BATCH_CHANGE_ORDER_PROGRESS = Flag.FlAG_BIT_26,
    /// <summary>
    /// 订单列表是否显示利润
    /// </summary>
    [CheckBox(ID.Menu.Order, 20, "OrderListShowProfit", "订单列表显示利润", true)]
    OEDER_LIST_SHOW_PROFIT = Flag.FlAG_BIT_27,

    //订单退款审核
    /// <summary>
    /// 接受
    /// </summary>
    [CheckBox(ID.Menu.Order, 1, "Adopt", "通过", true)]
    ADOPT = Flag.FlAG_BIT_28,
    /// <summary>
    /// 拒绝
    /// </summary>
    [CheckBox(ID.Menu.Order, 4, "Refuse", "拒绝", true)]
    REFUSE = Flag.FlAG_BIT_29,
}
