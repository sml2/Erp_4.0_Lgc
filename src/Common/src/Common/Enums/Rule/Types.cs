﻿using System;
namespace ERP.Enums.Rule;

public enum Types : short
{
    PERMISSION = 1 << 0,
    CONFIG = 1 << 1,
    MENU = PERMISSION | CONFIG,
}

