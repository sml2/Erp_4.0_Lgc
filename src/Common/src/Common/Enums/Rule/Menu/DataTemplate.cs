namespace ERP.Enums.Rule.Menu;

public enum DataTemplate : long
{
    Module = Flag.FlAG_BIT_01,

    // Attribute = Flag.FlAG_BIT_02,

    /// <summary>
    /// 卖点
    /// </summary>
    Sketches = Flag.FlAG_BIT_03,

    /// <summary>
    /// 关键字
    /// </summary>
    Keywords = Flag.FlAG_BIT_04,

    /// <summary>
    /// 报关信息
    /// </summary>
    CustomsDeclaration = Flag.FlAG_BIT_05,

    /// <summary>
    /// 运费预估
    /// </summary>
    FreightEstimation = Flag.FlAG_BIT_06
}