namespace ERP.Enums.Rule.Menu;

public enum Tools : long
{
    Module = Flag.FlAG_BIT_01,
    Domain = Flag.FlAG_BIT_02,
    Selection = Flag.FlAG_BIT_03,
    Library = Flag.FlAG_BIT_04,
}