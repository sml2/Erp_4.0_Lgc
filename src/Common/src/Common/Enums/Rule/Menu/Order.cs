﻿using ERP.Attributes.Rule;
using ERP.Enums.Identity;

namespace ERP.Enums.Rule.Menu;
public enum Order: long
{
    Module = Flag.FlAG_BIT_01,
    Order = Flag.FlAG_BIT_02,
    MergePending = Flag.FlAG_BIT_03,
    Merge = Flag.FlAG_BIT_04,
    Refund = Flag.FlAG_BIT_05,
    OrderSalesRanking = Flag.FlAG_BIT_06,
    DistributedOrder = Flag.FlAG_BIT_07,
}
