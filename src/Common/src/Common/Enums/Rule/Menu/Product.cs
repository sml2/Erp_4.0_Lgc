﻿namespace ERP.Enums.Rule.Menu;
public enum Product : long
{
    Module = Flag.FlAG_BIT_01,
    Template = Flag.FlAG_BIT_02,
    Category = Flag.FlAG_BIT_03,
    Task = Flag.FlAG_BIT_04,
    Multiple = Flag.FlAG_BIT_05,
    IllegalWord = Flag.FlAG_BIT_06,
    InvalidProductQuery = Flag.FlAG_BIT_07,
    KeywordTool = Flag.FlAG_BIT_08,
    AsinKeywords = Flag.FlAG_BIT_09,
    ToSell = Flag.FlAG_BIT_10,
    ImageList = Flag.FlAG_BIT_11,
}
