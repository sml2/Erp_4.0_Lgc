﻿namespace ERP.Enums.Rule.Menu;
public enum Storage : long
{
    Module = Flag.FlAG_BIT_01,
    Warehouse = Flag.FlAG_BIT_02,
    Stock = Flag.FlAG_BIT_03,
    StockLog = Flag.FlAG_BIT_04,
    AuditStockLog = Flag.FlAG_BIT_05,
    Shelf = Flag.FlAG_BIT_06,
    WarehouseAdd = Flag.FlAG_BIT_07,
    WarehouseEdit = Flag.FlAG_BIT_08,
    WarehouseDetail = Flag.FlAG_BIT_09,
    StockIn = Flag.FlAG_BIT_10,
    StockOut = Flag.FlAG_BIT_11,
}
