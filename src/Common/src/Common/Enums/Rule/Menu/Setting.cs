﻿using ERP.Attributes.Rule;
using ERP.Enums.Identity;

namespace ERP.Enums.Rule.Menu;

[Flags]
public enum Setting : long
{
    Module = Flag.FlAG_BIT_01,
    Company = Flag.FlAG_BIT_02,
    IntegralBalance = Flag.FlAG_BIT_03,
    Business = Flag.FlAG_BIT_04,
    Information = Flag.FlAG_BIT_05,
    Operation = Flag.FlAG_BIT_06,
    ExportTemplate = Flag.FlAG_BIT_07,
    ProductNodeType = Flag.FlAG_BIT_08,
    Oem = Flag.FlAG_BIT_09,
    Dictionary = Flag.FlAG_BIT_10,
    Resources = Flag.FlAG_BIT_11,
    Trend = Flag.FlAG_BIT_12,
    IntegralTransform = Flag.FlAG_BIT_13,
    IntegralBill = Flag.FlAG_BIT_14,
    BalanceRecharge = Flag.FlAG_BIT_15,
    ResourcePackage = Flag.FlAG_BIT_16,

}
