﻿namespace ERP.Enums.Rule.Menu;
public enum Purchase : long
{
    Module = Flag.FlAG_BIT_01,
    Purchase = Flag.FlAG_BIT_02,
    WaitForFind = Flag.FlAG_BIT_03,
    MyPurchase = Flag.FlAG_BIT_04,
    Supplier = Flag.FlAG_BIT_05,
    PurchaseAddCustomPurchase = Flag.FlAG_BIT_06,
    PurchaseEdit = Flag.FlAG_BIT_07,
    PurchaseConfirm = Flag.FlAG_BIT_08,
}
