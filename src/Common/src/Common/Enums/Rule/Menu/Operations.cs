﻿using ERP.Attributes.Rule;
using ERP.Enums.Identity;

namespace ERP.Enums.Rule.Menu;

public enum Operations : long
{
    Module = Flag.FlAG_BIT_01,
    TaskLogOperation = Flag.FlAG_BIT_02
}
