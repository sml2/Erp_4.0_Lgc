﻿namespace ERP.Enums.Rule.Menu;
public enum Store : long
{
    Module = Flag.FlAG_BIT_01,
    Remote = Flag.FlAG_BIT_02,
    AmazonStore = Flag.FlAG_BIT_03,
    MyStore = Flag.FlAG_BIT_04,
    UploadProgress = Flag.FlAG_BIT_05,
    Eanupc = Flag.FlAG_BIT_06,
    CategoryNodeDefault = Flag.FlAG_BIT_07,
    UploadShop = Flag.FlAG_BIT_08,
}
