namespace ERP.Enums.Rule.Menu;
public enum Distribution : long
{
    Module = Flag.FlAG_BIT_01,

    Company = Flag.FlAG_BIT_02,
    /// <summary>
    /// 分销产品
    /// </summary>
    Product = Flag.FlAG_BIT_03,
    Order = Flag.FlAG_BIT_04,
    MergeOrder= Flag.FlAG_BIT_15,
    Purchase = Flag.FlAG_BIT_05,
    Waybill = Flag.FlAG_BIT_06,
    Finance = Flag.FlAG_BIT_07,
    DistributedOrder = Flag.FlAG_BIT_14,

    //分销功能宣传页面
    Flyer_Company = Flag.FlAG_BIT_08,
    Flyer_Product = Flag.FlAG_BIT_09,
    Flyer_Order = Flag.FlAG_BIT_10,
    Flyer_Purchase = Flag.FlAG_BIT_11,
    Flyer_Waybill = Flag.FlAG_BIT_12,
    Flyer_Finance = Flag.FlAG_BIT_13,
}
