﻿namespace ERP.Enums.Rule.Menu;
public enum Finance : long
{
    Module = Flag.FlAG_BIT_01,
    Statistics = Flag.FlAG_BIT_02,
    BillLog = Flag.FlAG_BIT_03,
    FinanceAudit = Flag.FlAG_BIT_04,
    AmazonOrderStatistics = Flag.FlAG_BIT_05,
}
