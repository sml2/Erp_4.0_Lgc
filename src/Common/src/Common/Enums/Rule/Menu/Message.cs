﻿using ERP.Attributes.Rule;
using ERP.Enums.Identity;

namespace ERP.Enums.Rule.Menu;

public enum Message : long
{
    Module = Flag.FlAG_BIT_01,
    JobProblemMessage = Flag.FlAG_BIT_02,
    JobProgressMessage = Flag.FlAG_BIT_03,
    JobMyMessage = Flag.FlAG_BIT_04,
    JobPendingMessage = Flag.FlAG_BIT_05,
    EmailListMessage = Flag.FlAG_BIT_06,
    EmailCollectionMessage = Flag.FlAG_BIT_07,
    EmailMassMessage = Flag.FlAG_BIT_08,
    EmailSendMessage = Flag.FlAG_BIT_09,
    FeedBackMessage = Flag.FlAG_BIT_10,
    ProcessingFeedBackMessage = Flag.FlAG_BIT_11,
    NoticeMessage = Flag.FlAG_BIT_12,
    Menu7 = Flag.FlAG_BIT_60,
    Menu8 = Flag.FlAG_BIT_61,
    Menu9 = Flag.FlAG_BIT_62,
}