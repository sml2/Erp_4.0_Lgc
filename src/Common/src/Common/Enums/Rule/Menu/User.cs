﻿using ERP.Attributes.Rule;
using ERP.Enums.Identity;

namespace ERP.Enums.Rule.Menu;
[Flags]
public enum User : long
{
    Module = Flag.FlAG_BIT_01,
    Referer = Flag.FlAG_BIT_02,
    Rule = Flag.FlAG_BIT_03,
    Own = Flag.FlAG_BIT_04,
    Group = Flag.FlAG_BIT_05,
    Child = Flag.FlAG_BIT_06,
}
