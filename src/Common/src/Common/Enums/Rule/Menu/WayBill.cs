﻿namespace ERP.Enums.Rule.Menu;
public enum Waybill : long
{
    Module = Flag.FlAG_BIT_01,
    LogisticsManage = Flag.FlAG_BIT_02,
    Sender = Flag.FlAG_BIT_03,
    ShipTemplate = Flag.FlAG_BIT_04,
    ShipRatesEstimationTemplate = Flag.FlAG_BIT_05,
    Shipped = Flag.FlAG_BIT_06,
    WaybillList = Flag.FlAG_BIT_07,
    WaybillDownload = Flag.FlAG_BIT_08,
    WaybillEdit = Flag.FlAG_BIT_09,
    LogisticAdd = Flag.FlAG_BIT_10,
    WaybillConfirm = Flag.FlAG_BIT_11,
}
