﻿using ERP.Attributes;

namespace ERP.Enums;

[Flags]
public enum Languages : long
{
    [EnumExtension.DescriptionAttribute("Name", "默认")]
    [EnumExtension.DescriptionAttribute("Code", "default")]
    Default = 0,
    [EnumExtension.DescriptionAttribute("Name", "中文")]
    [EnumExtension.DescriptionAttribute("Code", "zh")]
    [Translation("cn")]
    @ZH = 1L << 0,
    [EnumExtension.DescriptionAttribute("Name", "西班牙语")]
    [EnumExtension.DescriptionAttribute("Code", "es")]
    [Translation]
    @ES = 1L << 1,
    [EnumExtension.DescriptionAttribute("Name", "意大利语")]
    [EnumExtension.DescriptionAttribute("Code", "it")]
    [Translation]
    @IT = 1L << 2,
    [EnumExtension.DescriptionAttribute("Name", "法语")]
    [EnumExtension.DescriptionAttribute("Code", "fr")]
    [Translation]
    @FR = 1L << 3,
    [EnumExtension.DescriptionAttribute("Name", "德语")]
    [EnumExtension.DescriptionAttribute("Code", "de")]
    [Translation]
    @DE = 1L << 4,
    [EnumExtension.DescriptionAttribute("Name", "日语")]
    [EnumExtension.DescriptionAttribute("Code", "ja")]
    [Translation]
    @JA = 1L << 5,
    [EnumExtension.DescriptionAttribute("Name", "英语")]
    [EnumExtension.DescriptionAttribute("Code", "en")]
    [Translation]
    @EN = 1L << 6,
    [EnumExtension.DescriptionAttribute("Name", "荷兰语")]
    [EnumExtension.DescriptionAttribute("Code", "nl")]
    [Translation]
    @NL = 1L << 7,
    [EnumExtension.DescriptionAttribute("Name", "越南语")]
    [EnumExtension.DescriptionAttribute("Code", "vi")]
    [Translation]
    @VI = 1L << 8,
    [EnumExtension.DescriptionAttribute("Name", "葡萄牙语")]
    [EnumExtension.DescriptionAttribute("Code", "pt")]
    [Translation]
    @PT = 1L << 9,
    [EnumExtension.DescriptionAttribute("Name", "阿拉伯语")]
    [EnumExtension.DescriptionAttribute("Code", "ar")]
    [Translation]
    @AR = 1L << 10,
    [EnumExtension.DescriptionAttribute("Name", "泰语")]
    [EnumExtension.DescriptionAttribute("Code", "th")]
    [Translation]
    @TH = 1L << 11,
    [EnumExtension.DescriptionAttribute("Name", "印尼语")]
    [EnumExtension.DescriptionAttribute("Code", "id")]
    [Translation]
    @ID = 1L << 12,
    [EnumExtension.DescriptionAttribute("Name", "韩语")]
    [EnumExtension.DescriptionAttribute("Code", "ko")]
    [Translation]
    @KO = 1L << 13,
    [EnumExtension.DescriptionAttribute("Name", "马来语")]
    [EnumExtension.DescriptionAttribute("Code", "ms")]
    [Translation]
    @MS = 1L << 14,
    [EnumExtension.DescriptionAttribute("Name", "菲律宾语")]
    [EnumExtension.DescriptionAttribute("Code", "tl")]
    [Translation]
    @TL = 1L << 15,
    [EnumExtension.DescriptionAttribute("Name", "波兰语")]
    [EnumExtension.DescriptionAttribute("Code", "pl")]
    [Translation]
    @PL = 1L << 16,
    [EnumExtension.DescriptionAttribute("Name", "芬兰语")]
    [EnumExtension.DescriptionAttribute("Code", "fi")]
    [Translation]
    @FI = 1L << 17,
    [EnumExtension.DescriptionAttribute("Name", "俄语")]
    [EnumExtension.DescriptionAttribute("Code", "ru")]
    [Translation]
    @RU = 1L << 18,
    [EnumExtension.DescriptionAttribute("Name", "加泰罗尼亚语")]
    [EnumExtension.DescriptionAttribute("Code", "ca")]
    @CA = 1L << 19,
    [EnumExtension.DescriptionAttribute("Name", "老挝语")]
    [EnumExtension.DescriptionAttribute("Code", "lo")]
    @LO = 1L << 20,
    [EnumExtension.DescriptionAttribute("Name", "希伯来语")]
    [EnumExtension.DescriptionAttribute("Code", "iw")]
    [Translation("he")]
    @IW = 1L << 21,
    [EnumExtension.DescriptionAttribute("Name", "尼泊尔语")]
    [EnumExtension.DescriptionAttribute("Code", "ne")]
    @NE = 1L << 22,
    [EnumExtension.DescriptionAttribute("Name", "瑞典语")]
    [EnumExtension.DescriptionAttribute("Code", "sv")]
    [Translation]
    @SV = 1L << 23,
    [EnumExtension.DescriptionAttribute("Name", "吉尔吉斯语")]
    [EnumExtension.DescriptionAttribute("Code", "ky")]
    @KY = 1L << 24,
    [EnumExtension.DescriptionAttribute("Name", "丹麦语")]
    [EnumExtension.DescriptionAttribute("Code", "da")]
    @DA = 1L << 25,
    [EnumExtension.DescriptionAttribute("Name", "白俄罗斯语")]
    [EnumExtension.DescriptionAttribute("Code", "be")]
    @BE = 1L << 26,
    [EnumExtension.DescriptionAttribute("Name", "土耳其语")]
    [EnumExtension.DescriptionAttribute("Code", "tr")]
    [Translation]
    @TR = 1L << 27,
    [EnumExtension.DescriptionAttribute("Name", "印地语")]
    [EnumExtension.DescriptionAttribute("Code", "hi")]
    [Translation]
    @HI = 1L << 28,
    [EnumExtension.DescriptionAttribute("Name", "冰岛语")]
    [EnumExtension.DescriptionAttribute("Code", "is")]
    @IS = 1L << 29,
    [EnumExtension.DescriptionAttribute("Name", "匈牙利语")]
    [EnumExtension.DescriptionAttribute("Code", "hu")]
    [Translation]
    @HU = 1L << 30,
    [EnumExtension.DescriptionAttribute("Name", "蒙古语")]
    [EnumExtension.DescriptionAttribute("Code", "mn")]
    @MN = 1L << 31,
    [EnumExtension.DescriptionAttribute("Name", "乌克兰语")]
    [EnumExtension.DescriptionAttribute("Code", "uk")]
    [Translation]
    @UK = 1L << 32,
    [EnumExtension.DescriptionAttribute("Name", "爱尔兰语")]
    [EnumExtension.DescriptionAttribute("Code", "ga")]
    @GA = 1L << 33,
    [EnumExtension.DescriptionAttribute("Name", "缅甸语")]
    [EnumExtension.DescriptionAttribute("Code", "my")]
    @MY = 1L << 34,
    [EnumExtension.DescriptionAttribute("Name", "拉丁语")]
    [EnumExtension.DescriptionAttribute("Code", "la")]
    @LA = 1L << 35,
    [EnumExtension.DescriptionAttribute("Name", "希腊语")]
    [EnumExtension.DescriptionAttribute("Code", "el")]
    [Translation]
    @EL = 1L << 36,
    [EnumExtension.DescriptionAttribute("Name", "保加利亚语")]
    [EnumExtension.DescriptionAttribute("Code", "bg")]
    [Translation]
    @BG = 1L << 37,
    [EnumExtension.DescriptionAttribute("Name", "泰米尔语")]
    [EnumExtension.DescriptionAttribute("Code", "ta")]
    @TA = 1L << 38,
    [EnumExtension.DescriptionAttribute("Name", "立陶宛语")]
    [EnumExtension.DescriptionAttribute("Code", "lt")]
    @LT = 1L << 39,
    [EnumExtension.DescriptionAttribute("Name", "挪威语")]
    [EnumExtension.DescriptionAttribute("Code", "no")]
    @NO = 1L << 40,
    [EnumExtension.DescriptionAttribute("Name", "罗马尼亚语")]
    [EnumExtension.DescriptionAttribute("Code", "ro")]
    [Translation]
    @RO = 1L << 41,
    [EnumExtension.DescriptionAttribute("Name", "波斯语")]
    [EnumExtension.DescriptionAttribute("Code", "fa")]
    [Translation]
    @FA = 1L << 42,
    [EnumExtension.DescriptionAttribute("Name", "繁體中文")]
    [EnumExtension.DescriptionAttribute("Code", "zh_tw")]
    ZH_TW = 1L << 43
}