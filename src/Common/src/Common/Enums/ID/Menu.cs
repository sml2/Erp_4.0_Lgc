using System.ComponentModel;

namespace ERP.Enums.ID;
public enum Menu : int
{
    [Description("未添加")]
    None = 0,
    //Setting
    SettingManager = 100,
    Company = 110,
    IntegralBalance = 120,
    Business = 130,
    Information = 140,
    Oem = 141,
    Resources = 142,
    Dictionary = 143,
    Trend = 144,
    Operation = 150,
    ExportTemplate = 160,
    ResourcePackage = 161,
    ProductNodeType = 170,
    Tasklog=171,


    //Store
    StoreManager = 200,
    Remote = 210,
    AmazonStore = 220,
    MyStore = 230,
    UploadProgress = 240,
    UploadShop = 241,
    Eanupc = 250,
    CategoryNodeDefault = 260,
    ExportProgress = 270,

    //User
    UserManager = 300,
    Referer = 310,
    Rule = 320,
    Own = 330,
    Group = 340,
    Child = 350,

    //Product
    ProductManager = 400,
    Template = 410,
    Category = 420,
    Task = 430,
    Multiple = 440,
    IllegalWord = 450,
    InvalidProductQuery = 460,
    KeywordTool = 470,
    AsinKeywords = 480,
    ImageList = 481,
    //ToSell = 490,

    //Storage
    StorageManager = 500,
    Warehouse = 510,
    Stock = 520,
    StockLog = 530,
    AuditStockLog = 540,
    Shelf = 550,

    //Order
    OrderManager = 600,
    Order = 610,
    DistributedOrder = 615,
    MergePending = 620,
    Merge = 630,
    OrderSalesRanking = 640,

    //Purchase
    PurchaseManager = 700,
    Purchase = 710,
    WaitForFind = 720,
    MyPurchase = 730,
    Supplier = 740,

    //WayBillManager
    WayBillManager = 800,
    LogisticsManage = 810,
    Sender = 820,
    ShipTemplate = 830,
    ShipRatesEstimationTemplate = 840,
    Shipped = 850,
    WaybillList = 860,

    //Finance
    FinanceManager = 900,
    Statistics = 910,
    BillLog = 920,
    FinanceAudit = 930,
    AmazonOrderStatistics = 940,

    //Distribution
    DistributionManager = 1000,
    CompanyDistribution = 1010,
    Flyer_CompanyDistribution = 1011,
    ProductDistribution = 1020,
    Flyer_ProductDistribution = 1021,
    OrderDistribution = 1030,
    DistributedOrderDistribution = 1032,
    Flyer_OrderDistribution = 1031,
    PurchaseDistribution = 1040,
    Flyer_PurchaseDistribution = 1041,
    WaybillDistribution = 1050,
    Flyer_WaybillDistribution = 1051,
    FinanceDistribution = 1060,
    Flyer_FinanceDistribution = 1061,
    MergeOrderDistribution = 1070,
    Flyer_MergeOrderDistribution = 1071,

    //Job
    JobManager = 1100,
    JobRule = 1110,
    JobOwn = 1120,

    //Message
    MessageManager = 1200,
    JobProblemMessage = 1210,
    JobProgressMessage = 1220,
    JobMyMessage = 1230,
    JobPendingMessage = 1240,
    EmailListMessage = 1250,
    EmailCollectionMessage = 1260,
    EmailMassMessage = 1270,
    EmailSendMessage = 1280,
    FeedBackMessage = 1290,
    ProcessingFeedBackMessage = 1300,
    NoticeMessage = 1310,


    //运维
    OperationsManager =1400,
    TaskLogOperation = 1410,
    
    //B2C
    B2CManager = 1500,
    B2CDomain = 1510,
    B2CSelection = 1520,
    B2CLibrary = 1530,

    ToolsManager=1600,
    SmartHijacking=1610,
}