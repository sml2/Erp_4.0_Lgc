﻿using System.ComponentModel;
namespace ERP.Enums.ID;
public enum Role
{
    None,
    [Description("超管")] SU ,
    [Description("开发者")] DEV ,

    [Description("管理员")] ADMIN ,
    [Description("员工")] Employee ,
}