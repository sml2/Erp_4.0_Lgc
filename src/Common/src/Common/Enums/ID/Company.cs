﻿using System.ComponentModel;

namespace ERP.Enums.ID;
public enum Company
{
    None,
    [Description("占位")]
    Empty,
    [Description("小米")]
    XiaoMi,
    // [Description("永合")]
    // YongHe,
    // [Description("锋鸟")]
    // FengNiao,
}

