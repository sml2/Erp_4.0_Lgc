namespace ERP.Enums.ID;

public enum UserValidity
{
    None,

    [EnumExtension.DescriptionAttribute("默认过期时间")]
    FiveYear,
    [EnumExtension.DescriptionAttribute("永合")]
    YhOneYear,
    [EnumExtension.DescriptionAttribute("锋鸟")]
    FnOneYear,
}