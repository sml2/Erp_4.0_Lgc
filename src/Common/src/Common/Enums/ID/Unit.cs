using System.ComponentModel;

namespace ERP.Enums.ID;

public enum Unit
{
    None,

    [Description("美元")]
    USD,

    [Description("欧元")]
    EUR,

    [Description("人民币")]
    CNY,

    [Description("日元")]
    JPY,

    [Description("港币")]
    HKD,

    [Description("韩元")]
    KRW,

    [Description("卢布")]
    RUB,

    [Description("英镑")]
    GBP,

    [Description("新加坡元")]
    SGD,

    [Description("新台币")]
    TWD,

    [Description("加拿大元")]
    CAD,

    [Description("澳大利亚元")]
    AUD,

    [Description("巴西雷亚尔")]
    BRL,

    [Description("印度卢比")]
    INR,

    [Description("瑞士法郎")]
    CHF,

    [Description("泰国铢")]
    THB,

    [Description("澳门元")]
    MOP,

    [Description("新西兰元")]
    NZD,

    [Description("南非兰特")]
    ZAR,

    [Description("瑞典克朗")]
    SEK,

    [Description("印尼卢比")]
    IDR,

    [Description("墨西哥比索")]
    MXN,

    [Description("阿根廷比索")]
    ARS,

    [Description("林吉特")]
    MYR,

    [Description("阿曼里亚尔")]
    OMR,

    [Description("埃及镑")]
    EGP,

    [Description("爱尔兰镑")]
    IEP,

    [Description("奥地利先令")]
    ATS,

    [Description("巴基斯坦卢比")]
    PKR,

    [Description("巴拉圭瓜拉尼")]
    PYG,

    [Description("巴林第纳尔")]
    BHD,

    [Description("巴拿马巴尔博亚")]
    PAB,

    [Description("百慕大元")]
    BMD,

    [Description("保加利亚列弗")]
    BGN,

    [Description("比利时法郎")]
    BEF,

    [Description("冰岛克朗")]
    ISK,

    [Description("波兰兹罗提")]
    PLN,

    [Description("玻利维亚诺")]
    BOB,

    [Description("博茨瓦纳普拉")]
    BWP,

    [Description("丹麦克朗")]
    DKK,

    [Description("德国马克")]
    DEM,

    [Description("法国法郎")]
    FRF,

    [Description("菲律宾比索")]
    PHP,

    [Description("芬兰马克")]
    FIM,

    [Description("哥伦比亚比索")]
    COP,

    [Description("古巴比索")]
    CUP,

    [Description("哈萨克斯坦坚戈")]
    KZT,

    [Description("荷兰盾")]
    ANG,

    [Description("加纳塞地")]
    GHC,

    [Description("捷克克朗")]
    CZK,

    [Description("津巴布韦元")]
    ZWD,

    [Description("卡塔尔里亚尔")]
    QAR,

    [Description("科威特第纳尔")]
    KWD,

    [Description("克罗地亚库纳")]
    HRK,

    [Description("肯尼亚先令")]
    KES,

    [Description("拉脱维亚拉图")]
    LVL,

    [Description("老挝基普")]
    LAK,

    [Description("黎巴嫩镑")]
    LBP,

    [Description("立陶宛立特")]
    LTL,

    [Description("罗马尼亚列伊")]
    RON,

    [Description("毛里求斯卢比")]
    MUR,

    [Description("蒙古图格里克")]
    MNT,

    [Description("孟加拉塔卡")]
    BDT,

    [Description("秘鲁新索尔")]
    PEN,

    [Description("缅甸缅元")]
    BUK,

    [Description("摩洛哥迪拉姆")]
    MAD,

    [Description("挪威克朗")]
    NOK,

    [Description("葡萄牙埃斯库多")]
    PTE,

    [Description("沙特里亚尔")]
    SAR,

    [Description("斯里兰卡卢比")]
    LKR,

    [Description("索马里先令")]
    SOS,

    [Description("坦桑尼亚先令")]
    TZS,

    [Description("突尼斯第纳尔")]
    TND,

    [Description("土耳其里拉")]
    TRY,

    [Description("危地马拉格查尔")]
    GTQ,

    [Description("委内瑞拉博利瓦")]
    VEB,

    [Description("乌拉圭比索")]
    UYU,

    [Description("西班牙比塞塔")]
    ESP,

    [Description("希腊德拉克马")]
    GRD,

    [Description("匈牙利福林")]
    HUF,

    [Description("牙买加元")]
    JMD,

    [Description("以色列谢克尔")]
    ILS,

    [Description("意大利里拉")]
    ITL,

    [Description("约旦第纳尔")]
    JOD,

    [Description("越南盾")]
    VND,

    [Description("智利比索")]
    CLP,

    [Description("巴布亚新几内亚基那")]
    PGK,

    [Description("朝鲜圆")]
    KPW,

    [Description("莱索托洛提")]
    LSL,

    [Description("利比亚第纳尔")]
    LYD,

    [Description("卢旺达法郎")]
    RWF,

    [Description("缅甸元")]
    MMK,

    [Description("毛里塔尼亚乌吉亚")]
    MRO,

    [Description("马拉维克瓦查")]
    MWK,

    [Description("尼加拉瓜科多巴")]
    NIO,

    [Description("尼泊尔卢比")]
    NPR,

    [Description("所罗门群岛元")]
    SBD,

    [Description("塞舌尔法郎")]
    SCR,

    [Description("文莱元")]
    BND,

    [Description("叙利亚镑")]
    SYP,

    [Description("阿尔及利亚第纳尔")]
    DZD,

    [Description("阿联酋迪拉姆")]
    AED,

    [Description("巴巴多斯元")]
    BBD,

    [Description("阿富汗尼")]
    AFN,

    [Description("阿尔巴尼亚勒克")]
    ALL,

    [Description("亚美尼亚德拉姆")]
    AMD,

    [Description("安哥拉宽扎")]
    AOA,

    [Description("阿鲁巴盾弗罗林")]
    AWG,

    [Description("阿塞拜疆新马纳特")]
    AZN,

    [Description("波斯尼亚马尔卡")]
    BAM,

    [Description("布隆迪法郎")]
    BIF,

    [Description("巴哈马元")]
    BSD,

    [Description("不丹努扎姆")]
    BTN,

    [Description("白俄罗斯卢布")]
    BYR,

    [Description("伯利兹美元")]
    BZD,

    [Description("刚果法郎")]
    CDF,

    [Description("哥斯达黎加科朗")]
    CRC,

    [Description("古巴可兑换比索")]
    CUC,

    [Description("佛得角埃斯库多")]
    CVE,

    [Description("吉布提法郎")]
    DJF,

    [Description("多明尼加比索")]
    DOP,

    [Description("尼日利亚奈拉")]
    NGN,

    [Description("厄立特里亚纳克法")]
    ERN,

    [Description("埃塞俄比亚比尔")]
    ETB,

    [Description("斐济元")]
    FJD,

    [Description("福克兰镑")]
    FKP,

    [Description("格鲁吉亚拉里")]
    GEL,

    [Description("直布罗陀镑")]
    GIP,

    [Description("冈比亚达拉西")]
    GMD,

    [Description("几内亚法郎")]
    GNF,

    [Description("圭亚那元")]
    GYD,

    [Description("洪都拉斯伦皮拉")]
    HNL,

    [Description("海地古德")]
    HTG,

    [Description("伊拉克第纳尔")]
    IQD,

    [Description("伊朗里亚尔")]
    IRR,

    [Description("吉尔吉斯斯坦索姆")]
    KGS,

    [Description("柬埔寨瑞尔")]
    KHR,

    [Description("科摩罗法郎")]
    KMF,

    [Description("开曼群岛元")]
    KYD,

    [Description("利比里亚元")]
    LRD,

    [Description("摩尔多瓦列伊")]
    MDL,

    [Description("马尔加什阿里亚")]
    MGA,

    [Description("马其顿第纳尔")]
    MKD,

    [Description("马尔代夫拉菲亚")]
    MVR,

    [Description("新莫桑比克梅蒂卡尔")]
    MZN,

    [Description("纳米比亚元")]
    NAD,

    [Description("塞尔维亚第纳尔")]
    RSD,

    [Description("苏丹镑")]
    SDG,

    [Description("圣圣赫勒拿镑")]
    SHP,

    [Description("塞拉利昂利昂")]
    SLL,

    [Description("苏里南元")]
    SRD,

    [Description("圣多美多布拉")]
    STD,

    [Description("斯威士兰里兰吉尼")]
    SZL,

    [Description("塔吉克斯坦索莫尼")]
    TJS,

    [Description("土库曼斯坦马纳特")]
    TMT,

    [Description("汤加潘加")]
    TOP,

    [Description("特立尼达多巴哥元")]
    TTD,

    [Description("乌克兰格里夫纳")]
    UAH,

    [Description("乌干达先令")]
    UGX,

    [Description("乌兹别克斯坦苏姆")]
    UZS,

    [Description("委内瑞拉玻利瓦尔")]
    VEF,

    [Description("瓦努阿图瓦图")]
    VUV,

    [Description("西萨摩亚塔拉")]
    WST,

    [Description("中非金融合作法郎")]
    XAF,

    [Description("东加勒比元")]
    XCD,

    [Description("西非法郎")]
    XOF,

    [Description("法属波利尼西亚法郎")]
    XPF,

    [Description("也门里亚尔")]
    YER,

    [Description("赞比亚克瓦查")]
    ZMW,

    [Description("萨尔瓦多科朗")]
    SVC
}