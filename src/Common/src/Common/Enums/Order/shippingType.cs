﻿namespace ERP.Enums.Orders;
using System.ComponentModel;
/// <summary>
/// 订单流程
/// </summary>
[Flags]
public enum BITS : ulong
{
    ZERO, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN,
    [Description("未知")]
    Unknown = ZERO,
    None = Unknown,
    Null = Unknown,

    //0-3
    [Description("异常订单")]
    Progresses_Error = ONE << 3,                             //0000000000000000000000000000000000000000000000000000000000001000
    [Description("异常订单-取消")]
    Progresses_Error_Cancel = Progresses_Error,                         //0000000000000000000000000000000000000000000000000000000000001000
    [Description("异常订单-Reason1")]
    Progresses_Error_Reason1 = Progresses_Error | ONE,                     //0000000000000000000000000000000000000000000000000000000000001001
    [Description("异常订单-未付款")]
    Progresses_Error_Unpaid = Progresses_Error | TWO,                    //0000000000000000000000000000000000000000000000000000000000001010
    [Description("异常订单-Reason2")]
    Progresses_Error_Program = Progresses_Error | THREE,                    //0000000000000000000000000000000000000000000000000000000000001011
    [Description("异常订单-Reason3")]
    Progresses_Error_Reason3 = Progresses_Error | FOUR,                    //0000000000000000000000000000000000000000000000000000000000001100
    [Description("异常订单-Reason4")]
    Progresses_Error_Reason4 = Progresses_Error | FIVE,                    //0000000000000000000000000000000000000000000000000000000000001101
    [Description("异常订单-Reason5")]
    Progresses_Error_Reason5 = Progresses_Error | SIX,                    //0000000000000000000000000000000000000000000000000000000000001110
    [Description("异常订单-用户自己放入")]
    Progresses_Error_UserAction = Progresses_Error | SEVEN,                    //0000000000000000000000000000000000000000000000000000000000001111
    Progresses_Error_BitMask = (ONE << 4) - 1, //移位优先级<加减 1111

    //4-5
    [Description("仓库发货")]
    Progresses_Shipment = TWO << 4,                                       //0000000000000000000000000000000000000000000000000000000000100000
    [Description("仓库发货-成功")]
    Progresses_Shipment_Success = Progresses_Shipment,                  //0000000000000000000000000000000000000000000000000000000000100000
    [Description("仓库发货-异常")]
    Progresses_Shipment_Error = Progresses_Shipment | ONE << 4,           //0000000000000000000000000000000000000000000000000000000000110000
    Progresses_Shipment_BitMask = Progresses_Shipment_Error,

    //6-7
    [Description("打印面单")] Progresses_PrintLabel = TWO << 6,                              //0000000000000000000000000000000000000000000000000000000010000000
    [Description("打印面单-缺货")] Progresses_PrintLabel_OUTOFSTOCK = Progresses_PrintLabel, //0000000000000000000000000000000000000000000000000000000011000000
    [Description("打印面单-有货")] Progresses_PrintLabel_INSTOCK = THREE << 6,               //0000000000000000000000000000000000000000000000000000000011000000
    Progresses_PrintLabel_BitMask = Progresses_PrintLabel_INSTOCK,

    //8
    [Description("采购或运单申报")]
    Progresses_PurchaseORreport = ONE << 8,                               //0000000000000000000000000000000000000000000000000000000100000000

    //9
    [Description("未处理")] // 512
    Progresses_Unprocessed = ONE << 9,                                    //0000000000000000000000000000000000000000000000000000001000000000

    //10 已完成 20220615 Define 董建方[UI不展示,但是全部订单有]
    [Description("已完成")] // 1024
    Progresses_Complete = ONE << 10,                                    //0000000000000000000000000000000000000000000000000000001000000000

    [Description("订单进度位掩码")]
    Progresses_BitMask = (ONE << 20) - 1,
    //20
    //[Description("已签收")] // 2048 仅和用户操作标记相关 20220615 Define 董建方[暂不提供撤销操作,只能单向标记,不关联Progresses变更]
    //Complete新 = ONE << 20,                                   
    //21-24
    [Description("上传发票")]
    Waybill_HasUploadInvoice = ONE << 21,
    [Description("同步运单到店铺(虚拟发货)")]
    Waybill_HasSync = ONE << 22,
    [Description("打印面单")]
    Waybill_HasPrintLabel = ONE << 23,

    ShippingType_Mask = ONE << 24,
    [Description("货运方式")]
    ShippingType_FBA = ShippingType_Mask,
    ShippingType_MFN = ZERO,

    //14-20






}

public enum ShippingTypes : ulong
{
    Mask = BITS.ShippingType_Mask,
    [Description("电商平台配送")]
    AFN = BITS.ShippingType_FBA,
    [Description("卖家自行配送")]
    MFN = BITS.ShippingType_MFN
}