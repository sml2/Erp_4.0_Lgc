﻿using System.ComponentModel;

namespace ERP.Enums.Identity;
public enum User
{
    BASE = 444444,
    [Description("超管")]
    SU,
    [Description("小米")]
    XIAOMI,
    [Description("刘玉琦")]
    LYQ,
    [Description("安东煜")]
    ADY,
    [Description("孙勇")]
    SYY,
    [Description("许其杰")]
    XQJ,
    [Description("永合")]
    SunAdmin,
    [Description("蜂鸟")]
    FengNiao,
}

