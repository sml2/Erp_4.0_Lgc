﻿using System.ComponentModel;
namespace ERP.Enums.Identity;
using IDS = ID.Role;
[Flags]
public enum Role 
{
    NONE,
    /// <summary>
    /// 超管
    /// </summary>
    [Description("超管")] SU = 1 << (IDS.SU - 1),

    /// <summary>
    /// 开发者
    /// </summary>
    [Description("开发者")] DEV = 1 << (IDS.DEV - 1),

    /// <summary>
    /// 管理员 原Concierge=OWN，表示有自己的CompanyConfig
    /// </summary>
    [Description("管理员")] ADMIN = 1 << (IDS.ADMIN - 1),

    /// <summary>
    /// 员工
    /// </summary>
    [Description("员工")] Employee = 1 << (IDS.Employee - 1),
}