namespace Common.Enums.ResourcePackage;

public enum PackageSellStateEnum
{
    /// <summary>
    /// 上架
    /// </summary>
    PutOnTheShelf = 1,

    /// <summary>
    /// 下架
    /// </summary>
    OffShelf = 2
}