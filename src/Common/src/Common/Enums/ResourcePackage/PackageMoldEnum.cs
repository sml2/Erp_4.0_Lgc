namespace Common.Enums.ResourcePackage;

public enum PackageMoldEnum
{
    /// <summary>
    /// 自购
    /// </summary>
    Self = 1,

    /// <summary>
    /// 赠送
    /// </summary>
    Present = 2
}