namespace Common.Enums.ResourcePackage;

public enum PackageTypeEnum
{
    /// <summary>
    /// 抠图
    /// </summary>
    [EnumExtension.DescriptionAttribute("智能抠图")]
    Matting = 1,

    /// <summary>
    /// 翻译
    /// </summary>
    [EnumExtension.DescriptionAttribute("文本翻译")]
    Translation = 2,

    /// <summary>
    /// 图片翻译
    /// </summary>
    [EnumExtension.DescriptionAttribute("图片翻译")]
    ImgTranslation = 3
}