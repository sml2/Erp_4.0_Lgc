namespace Common.Enums.ResourcePackage;

public enum PackageUseStateEnum
{
    /// <summary>
    /// 已生效
    /// </summary>
    Effective = 1,

    /// <summary>
    /// 已用完
    /// </summary>
    UsedUp = 2
}