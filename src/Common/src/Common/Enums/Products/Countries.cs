using System.ComponentModel;

namespace Common.Enums.Products;

public enum Countries
{
    [EnumExtension.DescriptionAttribute("美国"),
     EnumExtension.DescriptionAttribute("SearchRaw", "site:amazon.com /dp/ \"currently unavailable\" {0}"),
     EnumExtension.DescriptionAttribute("SearchReplace", "Amazon.com"),
     EnumExtension.DescriptionAttribute("geo_name", "United States")]
    美国,

    [EnumExtension.DescriptionAttribute("法国"),
     EnumExtension.DescriptionAttribute("SearchRaw", "site:amazon.fr /dp/ \"Actuellement indisponible\" {0}"),
     EnumExtension.DescriptionAttribute("SearchReplace", "Amazon.fr"),
     EnumExtension.DescriptionAttribute("geo_name", "Alsace")]
    法国,

    [EnumExtension.DescriptionAttribute("西班牙"),
     EnumExtension.DescriptionAttribute("SearchRaw", "site:amazon.es /dp/ \"Agotado temporalmente\" {0}"),
     EnumExtension.DescriptionAttribute("SearchReplace", "Amazon.es"),
     EnumExtension.DescriptionAttribute("geo_name", "Aragon")]
    西班牙,

    [EnumExtension.DescriptionAttribute("意大利"),
     EnumExtension.DescriptionAttribute("SearchRaw", "site:amazon.it /dp/ \"Non disponibile\" {0}"),
     EnumExtension.DescriptionAttribute("SearchReplace", "Amazon.it"),
     EnumExtension.DescriptionAttribute("geo_name", "Italy")]
    意大利,

    [EnumExtension.DescriptionAttribute("英国"),
     EnumExtension.DescriptionAttribute("SearchRaw", "site:amazon.co.uk /dp/ \"Currently unavailable\" {0}"),
     EnumExtension.DescriptionAttribute("SearchReplace", "Amazon.co.uk"),
     EnumExtension.DescriptionAttribute("geo_name", "United Kingdom")]
    英国,

    //'20211129'nicht verfügbar
    [EnumExtension.DescriptionAttribute("德国"),
     EnumExtension.DescriptionAttribute("SearchRaw", "site:amazon.de /dp/ \"Derzeit nicht verfügbar\" {0}"),
     EnumExtension.DescriptionAttribute("SearchReplace", "Amazon.de"),
     EnumExtension.DescriptionAttribute("geo_name", "Bavaria")]
    德国,

    //'20211129'Momenteel niet op voorraad
    [EnumExtension.DescriptionAttribute("荷兰"),
     EnumExtension.DescriptionAttribute("SearchRaw", "site:amazon.nl /dp/ \"Momenteel niet verkrijgbaar\" {0}"
     ),
     EnumExtension.DescriptionAttribute("SearchReplace", "Amazon.nl"),
     EnumExtension.DescriptionAttribute("geo_name", "Netherlands")]
    荷兰,

    //'20211129'För närvarande inte i lager
    [EnumExtension.DescriptionAttribute("瑞典"),
     EnumExtension.DescriptionAttribute("SearchRaw", "site:amazon.se /dp/ \"För närvarande inte tillgänglig\" {0}"
     ),
     EnumExtension.DescriptionAttribute("SearchReplace", "Amazon.se"),
     EnumExtension.DescriptionAttribute("geo_name", "Sweden")]
    瑞典,

    [EnumExtension.DescriptionAttribute("墨西哥"),
     EnumExtension.DescriptionAttribute("SearchRaw", "site:amazon.com.mx /dp/ \"No disponible por el momento\" {0}"),
     EnumExtension.DescriptionAttribute("SearchReplace", "Amazon.com.mx"),
     EnumExtension.DescriptionAttribute("geo_name", "Mexico")]
    墨西哥,

    [EnumExtension.DescriptionAttribute("加拿大"),
     EnumExtension.DescriptionAttribute("SearchRaw", "site:amazon.ca /dp/ \"Currently unavailable\" {0}"),
     EnumExtension.DescriptionAttribute("SearchReplace", "Amazon.ca"),
     EnumExtension.DescriptionAttribute("geo_name", "Canada")]
    加拿大,

    [EnumExtension.DescriptionAttribute("日本"),
     EnumExtension.DescriptionAttribute("SearchRaw", "site:amazon.co.jp /dp/ \"現在在庫切れ\" {0}"),
     EnumExtension.DescriptionAttribute("SearchReplace", "Amazon.co.jp"),
     EnumExtension.DescriptionAttribute("geo_name", "Japan")]
    日本,

    [EnumExtension.DescriptionAttribute("澳大利亚"),
     EnumExtension.DescriptionAttribute("SearchRaw", "site:amazon.com.au /dp/ \"Currently unavailable\" {0}"),
     EnumExtension.DescriptionAttribute("SearchReplace", "Amazon.com.au"),
     EnumExtension.DescriptionAttribute("geo_name", "Australia")]
    澳大利亚,

    //'20211129'इससमय अपर्याप्त है।,
    [EnumExtension.DescriptionAttribute("印度"),
     EnumExtension.DescriptionAttribute("SearchRaw", "site:amazon.in /dp/ \"इस समय उपलब्ध नहीं है\" {0}"),
     EnumExtension.DescriptionAttribute("SearchReplace", "Amazon.in"),
     EnumExtension.DescriptionAttribute("geo_name", "India")]
    印度
}