namespace Common.Enums.Products.ImageTran;

public enum SourceLanguageEnum
{
    [EnumExtension.DescriptionAttribute("Name", "中文(简体)")]
    [EnumExtension.DescriptionAttribute("Code", "CHS")]
    CHS,
    [EnumExtension.DescriptionAttribute("Name", "中文(繁體)")]
    [EnumExtension.DescriptionAttribute("Code", "CHT")]
    CHT,
    
    [EnumExtension.DescriptionAttribute("Name", "英文")]
    [EnumExtension.DescriptionAttribute("Code", "ENG")]
    ENG,
    
    [EnumExtension.DescriptionAttribute("Name", "日文")]
    [EnumExtension.DescriptionAttribute("Code", "JPN")]
    JPN,
}