namespace Common.Enums.Products.ImageTran;

public enum TargetLanguageEnum
{
    [EnumExtension.DescriptionAttribute("Name", "中文(简体)")]
    [EnumExtension.DescriptionAttribute("Code", "CHS")]
    CHS,
    
    [EnumExtension.DescriptionAttribute("Name", "中文(繁體)")]
    [EnumExtension.DescriptionAttribute("Code", "CHT")]
    CHT,
    
    [EnumExtension.DescriptionAttribute("Name", "阿拉伯语")]
    [EnumExtension.DescriptionAttribute("Code", "AR")]
    AR,
    
    [EnumExtension.DescriptionAttribute("Name", "捷克语")]
    [EnumExtension.DescriptionAttribute("Code", "CSY")]
    CSY,
    
    [EnumExtension.DescriptionAttribute("Name", "德语")]
    [EnumExtension.DescriptionAttribute("Code", "DEU")]
    DEU,
        
    [EnumExtension.DescriptionAttribute("Name", "英语")]
    [EnumExtension.DescriptionAttribute("Code", "ENG")]
    ENG,
    
    [EnumExtension.DescriptionAttribute("Name", "西班牙语")]
    [EnumExtension.DescriptionAttribute("Code", "ESP")]
    ESP,
    
    [EnumExtension.DescriptionAttribute("Name", "法语")]
    [EnumExtension.DescriptionAttribute("Code", "FRA")]
    FRA,
        
    [EnumExtension.DescriptionAttribute("Name", "匈牙利语")]
    [EnumExtension.DescriptionAttribute("Code", "HUN")]
    HUN,
    
    [EnumExtension.DescriptionAttribute("Name", "印尼语")]
    [EnumExtension.DescriptionAttribute("Code", "ID")]
    ID,
    
    [EnumExtension.DescriptionAttribute("Name", "意大利语")]
    [EnumExtension.DescriptionAttribute("Code", "ITA")]
    ITA,
    
    [EnumExtension.DescriptionAttribute("Name", "日本语")]
    [EnumExtension.DescriptionAttribute("Code", "JPN")]
    JPN,
        
    [EnumExtension.DescriptionAttribute("Name", "爪哇语")]
    [EnumExtension.DescriptionAttribute("Code", "JW")]
    JW,
            
    [EnumExtension.DescriptionAttribute("Name", "韩国语")]
    [EnumExtension.DescriptionAttribute("Code", "KOR")]
    KOR,
                
    [EnumExtension.DescriptionAttribute("Name", "马来语")]
    [EnumExtension.DescriptionAttribute("Code", "MS")]
    MS,
    
    [EnumExtension.DescriptionAttribute("Name", "缅甸语")]
    [EnumExtension.DescriptionAttribute("Code", "MY")]
    MY,
    
    [EnumExtension.DescriptionAttribute("Name", "荷兰语")]
    [EnumExtension.DescriptionAttribute("Code", "NLD")]
    NLD,
    
    [EnumExtension.DescriptionAttribute("Name", "波兰语")]
    [EnumExtension.DescriptionAttribute("Code", "PLK")]
    PLK,
    
    [EnumExtension.DescriptionAttribute("Name", "葡萄牙语")]
    [EnumExtension.DescriptionAttribute("Code", "PTB")]
    PTB,
    
    [EnumExtension.DescriptionAttribute("Name", "罗马尼亚语")]
    [EnumExtension.DescriptionAttribute("Code", "ROM")]
    ROM,
    
    [EnumExtension.DescriptionAttribute("Name", "俄语")]
    [EnumExtension.DescriptionAttribute("Code", "RUS")]
    RUS,
    
    [EnumExtension.DescriptionAttribute("Name", "泰语")]
    [EnumExtension.DescriptionAttribute("Code", "TH")]
    TH,
    
    [EnumExtension.DescriptionAttribute("Name", "他加禄语")]
    [EnumExtension.DescriptionAttribute("Code", "TL")]
    TL,
    
    [EnumExtension.DescriptionAttribute("Name", "土耳其语")]
    [EnumExtension.DescriptionAttribute("Code", "TRK")]
    TRK,
    
    [EnumExtension.DescriptionAttribute("Name", "越南语")]
    [EnumExtension.DescriptionAttribute("Code", "VIN")]
    VIN,
    
}
