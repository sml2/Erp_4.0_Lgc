using System.ComponentModel;

namespace Common.Enums.Products;

public enum MarketplaceIds
{
    [EnumExtension.DescriptionAttribute("ATVPDKIKX0DER")]
    [EnumExtension.DescriptionAttribute("Domain", "https://sellercentral.amazon.com")]
    UNKNOWN,

    [EnumExtension.DescriptionAttribute("A39IBJ37TRP1C6")]
    [EnumExtension.DescriptionAttribute("Domain", "https://sellercentral.amazon.com.au")]
    澳大利亚,

    [EnumExtension.DescriptionAttribute("A2Q3Y263D00KWC")]
    [EnumExtension.DescriptionAttribute("Domain", "https://sellercentral.amazon.com.br")]
    巴西,

    [EnumExtension.DescriptionAttribute("A2EUQ1WTGCTBG2")]
    [EnumExtension.DescriptionAttribute("Domain", "https://sellercentral.amazon.ca")]
    加拿大,

    [EnumExtension.DescriptionAttribute("AAHKV2X7AFYLW")]
    [EnumExtension.DescriptionAttribute("Domain", "https://sellercentral.amazon.cn")]
    中国大陆,

    [EnumExtension.DescriptionAttribute("A1PA6795UKMFR9")]
    [EnumExtension.DescriptionAttribute("Domain", "https://sellercentral.amazon.de")]
    德国,

    [EnumExtension.DescriptionAttribute("A1RKKUPIHCS9HS")]
    [EnumExtension.DescriptionAttribute("Domain", "https://sellercentral.amazon.es")]
    西班牙,

    [EnumExtension.DescriptionAttribute("A13V1IB3VIYZZH")]
    [EnumExtension.DescriptionAttribute("Domain", "https://sellercentral.amazon.fr")]
    法国,

    [EnumExtension.DescriptionAttribute("A21TJRUUN4KGV")]
    [EnumExtension.DescriptionAttribute("Domain", "https://sellercentral.amazon.in")]
    印度,

    [EnumExtension.DescriptionAttribute("APJ6JRA9NG5V4")]
    [EnumExtension.DescriptionAttribute("Domain", "https://sellercentral.amazon.it")]
    意大利,

    [EnumExtension.DescriptionAttribute("A1VC38T7YXB528")]
    [EnumExtension.DescriptionAttribute("Domain", "https://sellercentral.amazon.co.jp")]
    日本,

    [EnumExtension.DescriptionAttribute("A1AM78C64UM0Y8")]
    [EnumExtension.DescriptionAttribute("Domain", "https://sellercentral.amazon.com.mx")]
    墨西哥,

    [EnumExtension.DescriptionAttribute("A1805IZSGTT6HS")]
    [EnumExtension.DescriptionAttribute("Domain", "https://sellercentral.amazon.nl")]
    荷兰,

    [EnumExtension.DescriptionAttribute("A33AVAJ2PDY3EV")]
    [EnumExtension.DescriptionAttribute("Domain", "https://sellercentral.amazon.com.tr")]
    土耳其,

    [EnumExtension.DescriptionAttribute("ATVPDKIKX0DER")]
    [EnumExtension.DescriptionAttribute("Domain", "https://sellercentral.amazon.com")]
    美国,

    [EnumExtension.DescriptionAttribute("A2VIGQ35RCS4UG")]
    [EnumExtension.DescriptionAttribute("Domain", "https://sellercentral.amazon.ae")]
    阿拉伯联合酋长国,

    [EnumExtension.DescriptionAttribute("A1F83G8C2ARO7P")]
    [EnumExtension.DescriptionAttribute("Domain", "https://sellercentral.amazon.co.uk")]
    英国,

    [EnumExtension.DescriptionAttribute("A19VAU5U5O7RUS")]
    [EnumExtension.DescriptionAttribute("Domain", "https://sellercentral.amazon.sg")]
    新加坡,

    [EnumExtension.DescriptionAttribute("A2NODRKZP88ZB9")]
    [EnumExtension.DescriptionAttribute("Domain", "https://sellercentral.amazon.es")]
    瑞典,

    [EnumExtension.DescriptionAttribute("A17E79C6D8DWNP")]
    [EnumExtension.DescriptionAttribute("Domain", "https://sellercentral.amazon.sa")]
    沙特阿拉伯
}