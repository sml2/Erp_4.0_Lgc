namespace Common.Enums.Products;

public enum DescriptionTypeEnum
{
    PlainText = 1,
    RichText = 2
}