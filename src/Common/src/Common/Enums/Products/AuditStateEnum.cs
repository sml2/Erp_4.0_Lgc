namespace Common.Enums.Products;

public enum AuditStateEnum
{
    //未审核
    NotReviewed = 1,
    //已审核
    Audited,
    //未通过
    DidNotPass,
}