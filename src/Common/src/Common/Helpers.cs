using System.IO;
using System.Net;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using Microsoft.AspNetCore.StaticFiles;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ryu.String;

namespace ERP;

public static class Helpers
{
    /// <summary>
    /// 通用循环打印异常信息
    /// </summary>
    /// <param name="exp">异常</param>
    /// <returns></returns>
    public static string ExpHandle(Exception exp)
    {
        var deep = 0;
        StringBuilder sb = new(exp.Message);
        sb.AppendLine();
        while (exp.InnerException is not null)
        {
            //缩进深度
            deep++;
            //空格缩进
            sb.Append(new string(' ', 4 * deep));

            exp = exp.InnerException;
            sb.AppendLine(exp.Message);
        }

        return sb.ToString();
    }

    //public static double RoundData(double d)
    //{
    //    return Math.Round(d, 4);
    //}

    public static double RoundData(double d, int length=4)
    {       
        return Math.Round(d, length);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="d"></param>
    /// <param name="divisorNumber"></param>
    /// <param name="length"></param>
    /// <returns></returns>
    public static decimal RoundData(decimal d, int length=2)
    {      
        return Math.Round(d, length);
    }
    /// <summary>
    /// k=>kg 单位转换
    /// </summary>
    /// <param name="d"></param>
    /// <param name="length"></param>
    /// <returns></returns>
    public static float RoundData(float d, int length =2)
    {
        if (d == 0) return 0;
        return Convert.ToSingle(Math.Round(d, length));
    }

    public static string GetOrderNo(string OrderNo, int Send, int Fail)
    {
        int SendNum = Send + Fail;
        if (SendNum > 0)
        {
            var n = SendNum - 1;
            if (n == 0)
            {
                OrderNo = $"{OrderNo}";
            }
            else if (n > 0)
            {
                OrderNo = $"{OrderNo}-{n}";
            }
            else
            {
                throw new Exception("计数器出现异常");
            }
        }
        else
        {
            throw new Exception("计数器出现异常");
        }

        return OrderNo;
    }

    public static string GetOldOrderNo(string CustomerOrderNumber)
    {
        if (CustomerOrderNumber.Contains('-') && CustomerOrderNumber.Split('-').Length == 4)
        {
            var array = CustomerOrderNumber.Split('-');
            return array[0] + "-" + array[1] + "-" + array[2];
        }

        return CustomerOrderNumber;
    }

    /// <summary>
    /// 针对亚马逊费用：orderTotal。orderFee，orderReturn
    /// </summary>
    /// <param name="money"></param>
    /// <param name="flag"></param>
    /// <param name="code">订单所在国货币原值</param>
    /// <param name="unit">个人设置通用货币值</param>
    /// <param name="showUnit"></param>
    /// <returns></returns>
    public static string TransMoney(MoneyRecord money, bool flag, string code, string unit, bool showUnit = true)
    {
        try
        {
            decimal m = 0;
            if (string.IsNullOrWhiteSpace(code)) return "0.00";
            string currentUnit = flag ? code : unit;
            if (flag)
            {
                m = money.Raw.Amount;
            }
            else
            {
                m = money.Money.To(currentUnit);
            }
            if (showUnit)
            {
                return m.ToString("#0.00") + " " + currentUnit;
            }
            else
            {
                return m.ToString("#0.00");
            }
        }
        catch
        {
            throw new Exception($"TransMoney Exception ,code:{code}");
        }
    }

    public static string TransMoneyM(Money money, decimal rate, bool flag, string code, string unit, bool showUnit = true)
    {
        try
        {
            decimal m = 0;
            if (string.IsNullOrWhiteSpace(code)) return "0.00";
            string currentUnit = flag ? code : unit;
            if (flag)
            {
                if (rate == 0)
                { m = 0; }
                else
                {
                    m = Math.Round(money.Value / rate, 2); //按照原来的汇率计算
                }
            }
            else
            {
                m = money.To(currentUnit);  //转别的单位，按照最新汇率计算
            }
            if (showUnit)
            {
                return m.ToString("#0.00") + " " + currentUnit;
            }
            else
            {
                return m.ToString("#0.00");
            }
        }
        catch
        {
            throw new Exception($"TransMoney Exception ,code:{code}");
        }
    }
    

    /// <summary>
    /// 为了兼容错误利润数据
    /// </summary>
    /// <param name="money"></param>
    /// <param name="flag"></param>
    /// <param name="code"></param>
    /// <param name="unit"></param>
    /// <param name="showUnit"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    public static string TransMoneyP(MoneyRecord money, bool flag, string code, string unit, bool showUnit = true)
    {
        try
        {
            decimal m = 0;
            if (string.IsNullOrWhiteSpace(code)) return "0.00";
            string currentUnit = flag ? code : unit;
            if (flag)
            {
                int length = money.Raw.Amount.ToString().Split('.')[1].Length;
                if (length > 2)
                {
                    if (money.Raw.Rate == 0)
                    {
                        m = 0;
                    }
                    else
                    {
                        m = money.Raw.Amount / money.Raw.Rate;
                    }
                }
                else
                {
                    m = money.Raw.Amount;
                }               
            }
            else
            {
                int length = money.Raw.Amount.ToString().Split('.')[1].Length;
                if (length > 2)
                {                   
                    Money money1 = new Ryu.Data.Money(money.Raw.Amount);
                    m = money1.To(currentUnit);
                }
                else
                {
                    if (money.Raw.Rate == 0)
                    {
                        m = 0;
                    }
                    else
                    {
                        Money money1 = new Ryu.Data.Money(money.Raw.Amount, money.Raw.Rate);
                        m = money1.To(currentUnit);
                    }                   
                }                               
            }
            if (showUnit)
            {
                return m.ToString("#0.00") + " " + currentUnit;
            }
            else
            {
                return m.ToString("#0.00");
            }
        }
        catch
        {
            throw new Exception($"TransMoney Exception ,code:{code}");
        }
    }

    /// <summary>
    /// 计算，按选择的货币值显示
    /// </summary>
    /// <param name="money"></param>
    /// <param name="flag"></param>
    /// <param name="code"></param>
    /// <param name="unit"></param>
    /// <param name="showUnit"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    public static string TransMoney(Money money, bool flag, string code, string unit, bool showUnit = true)
    {
        try
        {
            decimal m = 0;
            if (string.IsNullOrWhiteSpace(code)) return "0.00";
            string currentUnit = flag ? code : unit;
            m = money.To(currentUnit);
            if (showUnit)
            {
                return m.ToString("#0.00") + " " + currentUnit;
            }
            else
            {
                return m.ToString("#0.00");
            }
        }
        catch
        {
            throw new Exception($"TransMoney Exception ,code:{code}");
        }
    }

    /// <summary>
    /// 针对sum求和，按默认的货币值转换
    /// </summary>
    /// <param name="money"></param>
    /// <param name="unit"></param>
    /// <returns></returns>
    public static Money TransMoney(Money money, string unit)
    {
        try
        {
            return money.To(unit);
        }
        catch
        {
            throw new Exception($"TransMoney Exception ,unit:{unit}");
        }
    }


    /// <summary>
    /// 针对系统purchaseFee，Loss，shippmentFee
    /// 这类费用，在系统中，肯定有CurrencyCode
    /// </summary>
    /// <param name="money"></param>
    /// <param name="flag"></param>
    /// <param name="orderUnit"></param>
    /// <param name="showUnit"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    public static string TransMoney(MoneyRecord money, bool flag, string orderUnit, bool showUnit = false)
    {
        if (money.Raw.CurrencyCode == "")
        {
            return "0.00";
        }
        else
        {
            if (flag)
            {
                string unit = "";
                if (showUnit)
                {
                    unit = money.Raw.CurrencyCode;
                }
                return money.Raw.Amount.ToString("0.00") + unit;  
            }
            else
            {
                return money.ToString(orderUnit, showUnit);
            }
        }
    }

  
    public static string GetRandomString()
    {
        Random random = new Random();
        return random.Next(100, 10000).ToString() + DateTime.Now.ToString("yyyyMMddTHHmmssZ");
    }

    /// <summary>
    /// 字符串摘要为bigint
    /// </summary>
    /// <param name="str">要进行摘要的字符串</param>
    /// <returns>为兼容更多语言,最终结果采用<see cref="ulong"/>类型</returns>
    public static ulong CreateHashCode(string str)
    {
        //var hash = MD5.HashData(Encoding.UTF8.GetBytes(str));

        //var temp = new byte[8];
        //Array.Copy(hash.Reverse().ToArray(), 4, temp, 0, 7);
        //var b4 = (byte)(hash[4] >> 1);
        ////b4 &= 0x7F;
        //temp[7] = b4;
        //return BitConverter.ToUInt64(temp);
        return str.GetFixedHashCode();
    }

    public static byte[] GetMd5(string text)
    {
        byte[] bytes = System.Text.Encoding.Default.GetBytes(text);
        byte[] b = MD5.HashData(bytes);
        return b;
    }

    public static string Md5(string text)
    {
        var b = GetMd5(text);
        StringBuilder sb = new StringBuilder(32);
        for (int i = 0; i < b.Length; i++)
        {
            sb.Append(b[i].ToString("X2"));
        }

        return sb.ToString();
    }

    public static string Md5Lower(string text)
    {
        var b = GetMd5(text);
        StringBuilder sb = new StringBuilder(32);
        for (int i = 0; i < b.Length; i++)
        {
            sb.Append(b[i].ToString("x2"));
        }

        return sb.ToString();
    }

    public static string EnCodeSku(int ID, string? Code)
    {
        string Key = "P" + From10to62(ID);
        string SucMd5 = CreateHashCode(Key).ToString().Substring(0, 3);
        return Key + SucMd5 + "-" + Code; //MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(Code));
    }

    public static string SkuCode()
    {
        return From10to62(CreateHashCode(Guid.NewGuid().ToString()));
    }

    public static string From10to62(int dec) => From10to62((ulong)dec);

    /// <summary>
    /// 10进制转62进制
    /// </summary>
    /// <param name="dec">十进制</param>
    /// <returns>string 62进制</returns>
    public static string From10to62(ulong dec)
    {
        string dict = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        string result = "";
        try
        {
            do
            {
                result = dict[(int)(dec % 62)] + result;
                dec = Convert.ToUInt64(dec / 62);
            } while (dec != 0);
        }
        catch
        {
            result = "0";
        }

        return result;
    }

    /// <summary>
    /// 62进制转10进制
    /// </summary>
    /// <param name="str">62进制</param>
    /// <returns>int 十进制</returns>
    public static int From62to10(string str)
    {
        string dict = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var len = str.Length;
        int dec = 0;
        for (int i = 0; i < len; i++)
        {
            //找到对应字典的下标.
            int pos = dict.IndexOf(str[i]);
            dec += (pos * (int)Math.Pow(62, (len - i - 1)));
        }

        return dec;
    }

    public static T GetParamType<T>(string parameter)
    {
        return JsonConvert.DeserializeObject<T>(parameter) ?? default!;
    }


    /// <summary>
    /// 将金额转换后存储数据库
    /// </summary>
    /// <param name="money">当前需要转换的金额</param>
    /// <param name="unit">当前币种</param>
    /// <param name="rate"></param>
    /// <returns></returns>
    public static decimal FromConversion(decimal money, string unit, decimal? rate = null)
    {
        if (money == 0)
        {
            return money;
        }

        decimal temp;
        if (rate.HasValue)
        {
            temp = rate.Value;
        }
        else
        {
            temp = unit == "CNY" ? 1 : 0; /*Cache::instance()->unitSingle()->get($unit)['rate'];*/
        }

        //return number_format(bcmul($money, $rate, 6), 6, '.', '');
        return money * temp;
        //return 0;
    }

    /**
 * 将数据库存储的金额转换在UI显示
 * @param float  $money 数据库中存储的金额数据
 * @param string $unit  需要转换成的币种
 * @param null   $rate
 * @return float|int 对应币种UI展示用的金额数据
 */
    public static decimal ToConversion(decimal money, string unit, decimal? rate = null)
    {
        if (money == 0)
        {
            return money;
        }

        decimal temp;
        if (rate.HasValue)
        {
            temp = rate.Value;
        }
        else
        {
            temp = unit == "CNY" ? 1 : 0; /*Cache::instance()->unitSingle()->get($unit)['rate'];*/
        }

        //return number_format(bcmul($money, $rate, 6), 6, '.', '');
        return money * temp;
    }

    public static int GetTimeStamp(this DateTime dateTime) =>
        (int)dateTime.ToUniversalTime().Subtract(DateTime.UnixEpoch).TotalSeconds;

    public static long GetTimeStampMilliseconds(this DateTime dateTime)
    {
        TimeSpan ts = dateTime.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0, 0);
        return Convert.ToInt64(ts.TotalMilliseconds);
    }

    public static long GetTimeStamp()
    {
        TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
        return Convert.ToInt64(ts.TotalMilliseconds);
    }

    public static string GetSign()
    {
        string sbinary = "";
        byte[] buff = Encoding.UTF8.GetBytes(DateTime.Now.ToString());
        for (int i = 0; i < buff.Length; i++)
        {
            sbinary += buff[i].ToString("X2"); //X2转换为大写的16进制，x2转换为小写的16进制
        }

        return sbinary;
    }

    public static object? XmlDeSerializer(string xml, Type type)
    {
        object? response = new object();
        try
        {
            using (StringReader sr = new StringReader(xml))
            {
                XmlSerializer serializer = new XmlSerializer(type);
                response = serializer.Deserialize(sr) as object;
            }
        }
        catch
        {
            response = null;
        }

        return response;
    }

    public static string Obj2Xml(Type type, object obj)
    {
        MemoryStream Stream = new MemoryStream();
        XmlSerializer xml = new XmlSerializer(type);
        try
        {
            //序列化对象
            xml.Serialize(Stream, obj);
        }
        catch (InvalidOperationException)
        {
            throw;
        }

        Stream.Position = 0;
        StreamReader sr = new StreamReader(Stream);
        string str = sr.ReadToEnd();

        sr.Dispose();
        Stream.Dispose();

        return str;
    }


    public static string Serialize(object obj)
    {
        StringWriter stringWriter = new StringWriter();
        XmlSerializer xmlSerializer = new XmlSerializer(obj.GetType());
        xmlSerializer.Serialize(stringWriter, RuntimeHelpers.GetObjectValue(obj));
        StringBuilder stringBuilder = stringWriter.GetStringBuilder();
        stringWriter.Close();
        return stringBuilder.ToString();
    }


    /// <summary>
    /// 十六进制转二进制
    /// </summary>
    /// <param name="hs"></param>
    /// <param name="encode"></param>
    /// <returns></returns>
    public static byte[] HexStringToString(string hs)
    {
        string strTemp;
        byte[] b = new byte[hs.Length / 2];
        for (int i = 0; i < hs.Length / 2; i++)
        {
            strTemp = hs.Substring(i * 2, 2);
            b[i] = Convert.ToByte(strTemp, 16);
        }

        return b;
    }


    public static byte[] GetBytes(Stream stream)
    {
        byte[] bytes = new byte[stream.Length];
        stream.Read(bytes, 0, bytes.Length);
        // 设置当前流的位置为流的开始 
        stream.Seek(0, SeekOrigin.Begin);
        return bytes;
    }

    public static T ByteToStructure<T>(byte[] bytes)
    {
        object structure = null;
        int size = Marshal.SizeOf(typeof(T));
        IntPtr allocIntPtr = Marshal.AllocHGlobal(size);
        try
        {
            Marshal.Copy(bytes, 0, allocIntPtr, size);
            structure = Marshal.PtrToStructure(allocIntPtr, typeof(T));
        }
        finally
        {
            Marshal.FreeHGlobal(allocIntPtr);
        }
        return (T)structure;
    }

    /// <summary>
    /// 获取金额转换积分数值 1CNY = 10积分 / 1000基准货币数值=1积分
    /// </summary>
    /// <returns></returns>
    public static int GetIntegralFromBalance(decimal money)
    {
        return (int)(money / 1000);
    }

    /// <summary>
    /// 此处需要用Log或者In函数取得位运算的最低位所在的index
    /// </summary>
    /// <param name="ul"></param>
    /// <param name="i"></param>
    /// <returns></returns>
    public static byte GetLastBit(ulong ul, byte i = 0) => (ul & 1) != 0 ? i : GetLastBit(ul >> 1, (byte)(i + 1));

    public static byte GetLastBit(long ul, byte i = 0) => (ul & 1) != 0 ? i : GetLastBit(ul >> 1, (byte)(i + 1));

    public static string GetRandStr(int n)

    {
        string str = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder stringBuilder = new StringBuilder();
        Random rd = new Random();
        for (int i = 0; i < n; i++)
        {
            stringBuilder.Append(str.Substring(rd.Next(0, str.Length), 1));
        }

        return stringBuilder.ToString();
    }
    
    /// <summary>
    /// 删除最后结尾的一个逗号
    /// </summary>
    public static string DelLastComma(string str)
    {
        return str.Substring(0, str.LastIndexOf(","));
    }
    
    public static string DelComma(string str)
    {
        if (str.EndsWith(","))
        {
            str = str.Substring(0, str.LastIndexOf(",", StringComparison.Ordinal));;
        }

        if (str.StartsWith(","))
        {
            str = str.Substring(1);
        }
        return str;
    }

    /// <summary>
    /// 更新所有换行符为指定符号
    /// </summary>
    /// <param name="str"></param>
    /// <param name="spliter"></param>
    /// <returns></returns>
    public static string TransitionBr(string str, string spliter ="\r\n")
    {
        str = str.Trim();
        if (str == "")
            return "";
        var br = "<br/>";
        
        if (str.IndexOf("\r\n", StringComparison.Ordinal) != -1)
            str = str.Replace("\r\n", br);
        
        if (str.IndexOf("\n", StringComparison.Ordinal) != -1)
            str = str.Replace("\n", br);
        
        if (str.IndexOf("\r", StringComparison.Ordinal) != -1)
            str = str.Replace("\r", br);
        
        str = Regex.Replace(str, "<br[ /]{0,3}>", spliter, RegexOptions.IgnoreCase);
        return str;
    }

    public static string GetCurrency(string marketplace)
    {
        switch (marketplace)
        {
            case "A2EUQ1WTGCTBG2":  //Canada
                return "CAD";

            case "ATVPDKIKX0DER": //United States of America
                return "USD";

            case "A1AM78C64UM0Y8"://Mexico
                return "MXN";

            case "A2Q3Y263D00KWC":  //Brazil
                return "BRL";


            case "A1RKKUPIHCS9HS":  //Spain
                return "EUR";

            case "A1F83G8C2ARO7P":  //United Kingdom
                return "GBP";

            case "A13V1IB3VIYZZH":  //France
                return "EUR";

            case "AMEN7PMS3EDWL":  //Belgium
                return "BEF";

            case "A1805IZSGTT6HS":  //Netherlands
                return "EUR";

            case "A1PA6795UKMFR9":  //Germany
                return "EUR";

            case "APJ6JRA9NG5V4":  //Italy
                return "EUR";

            case "A2NODRKZP88ZB9":  //Sweden
                return "EUR";

            case "A1C3SOZRARQ6R3":  //Poland
                return "EUR";

            case "ARBP9OOSHTCHU":  //Egypt
                return "EGP";

            case "A33AVAJ2PDY3EV":  //Turkey
                return "TRY";

            case "A17E79C6D8DWNP":  //Saudi Arabia
                return "SAR";

            case "A2VIGQ35RCS4UG":  //United Arab Emirates
                return "AED";

            case "A21TJRUUN4KGV":  //India
                return "INR";


            case "A19VAU5U5O7RUS":  //Singapore
                return "SGD";

            case "A39IBJ37TRP1C6":  //Australia
                return "AUD";

            case "A1VC38T7YXB528":  //Japan
                return "JPY";
            default: return "";
        }
    }


    public static string GetAmazonLang(string langSign, string marketplace)
    {
        if (langSign == "ar")  //阿拉伯语系
        {
            if (marketplace == "A17E79C6D8DWNP" || marketplace == "A2VIGQ35RCS4UG")
            { return "ar_AE"; }
        }

        switch (marketplace)
        {
            case "A2EUQ1WTGCTBG2":  //Canada
                return "en_CA";

            case "ATVPDKIKX0DER": //United States of America
                return "en_US";

            case "A1AM78C64UM0Y8"://Mexico
                return "es_MX";

            case "A2Q3Y263D00KWC":  //Brazil
                return "pt_BR";


            case "A1RKKUPIHCS9HS":  //Spain
                return "es_ES";

            case "A1F83G8C2ARO7P":  //United Kingdom
                return "en_GB";

            case "A13V1IB3VIYZZH":  //France
                return "fr_FR";

            case "AMEN7PMS3EDWL":  //Belgium
                return "";

            case "A1805IZSGTT6HS":  //Netherlands
                return "nl_NL";

            case "A1PA6795UKMFR9":  //Germany
                return "de_DE";

            case "APJ6JRA9NG5V4":  //Italy
                return "it_IT";

            case "A2NODRKZP88ZB9":  //Sweden
                return "sv_SE";

            case "A1C3SOZRARQ6R3":  //Poland
                return "pl_PL";

            case "ARBP9OOSHTCHU":  //Egypt
                return "";

            case "A33AVAJ2PDY3EV":  //Turkey
                return "tr_TR";

            case "A17E79C6D8DWNP":  //Saudi Arabia
                return "en_AE";

            case "A2VIGQ35RCS4UG":  //United Arab Emirates
                return "en_AE";

            case "A21TJRUUN4KGV":  //India
                return "en_IN";


            case "A19VAU5U5O7RUS":  //Singapore
                return "en_SG";

            case "A39IBJ37TRP1C6":  //Australia
                return "en_AU";

            case "A1VC38T7YXB528":  //Japan
                return "ja_JP";
        }
        return "en_US";
    }


}