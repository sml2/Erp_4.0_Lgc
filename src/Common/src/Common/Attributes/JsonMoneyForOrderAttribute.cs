﻿using ERP.Extensions;
using ERP.Interface;
using Ryu.Money.Json;

namespace ERP.Attributes;

public class JsonMoneyForOrderAttribute : JsonMoneyAttribute
{
    public JsonMoneyForOrderAttribute() : base(typeof(OrderUnitResolver))
    {
        
    }
}

public class OrderUnitResolver : IUnitResolver
{
    private readonly IHttpContextAccessor _httpContextAccessor;

    public OrderUnitResolver(IHttpContextAccessor httpContextAccessor)
    {
        _httpContextAccessor = httpContextAccessor;
    }
    
    public string GetUnit(string defaultUnit, string? dependsOnUnit) => _httpContextAccessor.HttpContext?.Session?.GetUnitConfig() ?? defaultUnit;
}