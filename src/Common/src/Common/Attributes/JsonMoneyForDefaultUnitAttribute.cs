﻿using ERP.Extensions;
using ERP.Interface;
using Ryu.Money.Json;

namespace ERP.Attributes;

public class JsonMoneyForDefaultUnitAttribute : JsonMoneyAttribute
{
    public JsonMoneyForDefaultUnitAttribute(bool showUnit = true) : base(typeof(DefaultUnitResolver))
    {
        if (!showUnit)
        {
            Format = "{0:0.00}";
        }
    }
}

public class DefaultUnitResolver : IUnitResolver
{
    private readonly IHttpContextAccessor _httpContextAccessor;

    public DefaultUnitResolver(IHttpContextAccessor httpContextAccessor)
    {
        _httpContextAccessor = httpContextAccessor;
    }
    
    public string GetUnit(string defaultUnit, string? dependsOnUnit) => _httpContextAccessor.HttpContext?.Session?.GetUnitConfig() ?? defaultUnit;
}