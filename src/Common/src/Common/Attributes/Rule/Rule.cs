﻿namespace ERP.Attributes.Rule;
using Enums.Rule;
/// <summary>
/// 用于标记权限模板的模块字段
/// </summary>
[AttributeUsage(AttributeTargets.Property)]
public class RuleAttribute : Attribute
{
    public RuleAttribute(Types type)
    {
        Type = type;
    }
    public Types  Type { get; }
}
