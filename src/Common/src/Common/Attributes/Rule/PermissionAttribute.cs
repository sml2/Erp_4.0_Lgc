﻿using Microsoft.AspNetCore.Authorization;
using ERP.Enums.Rule.Config;

namespace ERP.Authorization.Rule;

public class PermissionAttribute: AuthorizeAttribute
{
    public Enum Enum { get;protected set; }
    public string Type { get; protected set; }
    public PermissionAttribute(string Type, Enum @enum) : base(nameof(PermissionAttribute)) { 
        this.Type = Type;
        Enum = @enum;
    }
    public PermissionAttribute(User p) : this(nameof(User), p){}
    public PermissionAttribute(Setting p) : this(nameof(Setting), p){}
    public PermissionAttribute(Store p) : this(nameof(Store), p){}
    public PermissionAttribute(Product p) : this(nameof(Product), p){}
    public PermissionAttribute(ProductAbout p) : this(nameof(ProductAbout), p){}
    public PermissionAttribute(Storage p) : this(nameof(Storage), p){}
    public PermissionAttribute(Order p) : this(nameof(Order), p){}
    public PermissionAttribute(Purchase p) : this(nameof(Purchase), p){}
    public PermissionAttribute(Waybill p) : this(nameof(Waybill), p){}
    public PermissionAttribute(Finance p) : this(nameof(Finance), p){}
    public PermissionAttribute(Distribution p) : this(nameof(Distribution), p){}
    public PermissionAttribute(Message p) : this(nameof(Message), p){}
    public PermissionAttribute(Enums.Rule.Menu.Storage p) : this(nameof(Message), p){}
}
public class PermissionAttribute<T> : PermissionAttribute where T:Enum
{   
    //C#11以前 泛型不可以作为注解使用 System.Enum 或 PermissionAttribute<TEnum> 
    //C#11以后 虽然泛型可以作为注解，但是V17.2的编译器还不能够自动推导泛型
    //等C#12
    public PermissionAttribute(T @enum) : base(@enum.GetType().Name, @enum){ }
}