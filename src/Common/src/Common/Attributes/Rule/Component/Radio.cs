﻿namespace ERP.Attributes.Rule;

using Enums.ID;
using Extensions;
using Interface.Rule;

public class RadioAttribute<T> : Authorization.Rule.Component.Radio, IComponentData where T : Enum
{
    public int MenuID { get; }
    public int Sorter { get; }
    public string AuthLabel => name;
    public RadioAttribute(Menu menu, int sort, string key, string name, T value, T[] data) : base(key, name, value.GetLong(), data.Cast<Enum>().ToDictionaryForUIByDescription())
    {
        MenuID = menu.GetID();
        Sorter = sort;
    }

    public IComponentView CastView(long val) => new Models.View.Rule.Radio(key, name, val, data);
}

