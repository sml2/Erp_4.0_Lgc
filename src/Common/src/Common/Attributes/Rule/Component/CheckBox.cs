﻿namespace ERP.Attributes.Rule;

using Enums.ID;
using Extensions;
using Interface.Rule;
using System;

public class CheckBoxAttribute : Authorization.Rule.Component.CheckBox, IComponentData
{
    public int MenuID { get; }
    public int Sorter { get; }
    public string AuthLabel => name;

    public CheckBoxAttribute(Menu menu,int sort,string key, string name, bool value) : base(key, name, value)
    {
        MenuID = menu.GetID();
        Sorter = sort;
    }

    public CheckBoxAttribute(Menu menu, int sort, string key, string name, bool value, string type) : base(key, name, value, type)
    {
        MenuID = menu.GetID();
        Sorter = sort;
    }

    public IComponentView CastView(long val) => new Models.View.Rule.CheckBox(key, name, val != 0);
}

