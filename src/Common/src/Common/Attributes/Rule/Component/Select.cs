﻿namespace ERP.Attributes.Rule;

using Enums.ID;
using Extensions;
using Interface.Rule;

public class SelectAttribute<T> : Authorization.Rule.Component.Select, IComponentData where T : Enum
{
    public int MenuID { get; }
    public int Sorter { get; }
    public string AuthLabel => name;

    public SelectAttribute(Menu menu, int sort, string key, string name, T value, T[] data) : base(key, name, value.GetLong(), data.Cast<Enum>().ToDictionaryForUIByDescription())
    {
        MenuID = menu.GetID();
        Sorter = sort;
    }

    public IComponentView CastView(long val) => new Models.View.Rule.Select(key, name, val, data);
}

