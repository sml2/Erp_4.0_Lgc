﻿namespace ERP.Attributes.Rule;
using ERP.Enums.Identity;

/// <summary>
/// 用于初始化权限数据
/// </summary>
[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Enum)]
public class SeedAttribute : Attribute
{
    public SeedAttribute(Role role)
    {
        Role = role;
    }
    public Role Role { get; }
}
