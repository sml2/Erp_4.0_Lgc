namespace ERP.Attributes;

public class TranslationAttribute : Attribute
{
    public string? Code;

    public TranslationAttribute(string? code = null)
    {
        if (!string.IsNullOrEmpty(code))
        {
            Code = code;
        }
    }

}