﻿
using ERP.Data;
using Microsoft.Extensions.Caching.Memory;

namespace ERP.Services;

public class BaseService
{
    protected int SuperId = Constants.SuperId;

    public static ReturnStruct ReturnArr(bool state = true, string msg = "", object? data = null)
    {
        return new() { State = state, Msg = msg, Data = data };
    }
    public struct ReturnStruct
    {
        public bool State { get; set; }
        public string Msg { get; set; }
        public object? Data { get; set; }
    }

    public ReturnStruct<T> ReturnArrT<T>(bool state = true, string msg = "", T? data = default)
    {
        return new() { State = state, Msg = msg, Data = data };
    }

    public struct ReturnStruct<T>
    {
        public bool State { get; set; }
        public string Msg { get; set; }
        public T? Data { get; set; }
    }

    /// <summary>
    /// ServiceCollectionExtension    Class
    /// </summary>
    public class HTTPBaseService//没有按需实例化Service，额外增加了不必要的开销     
    {
        // public Microsoft.AspNetCore.Identity.UserManager<User> UserManager { get; set; }
        public HttpContext HttpContext { get; set; } = null!;
        public HttpRequest Request => HttpContext.Request;
        //  public User CurrentUser { get; set; }
        public IMemoryCache Cache { get; set; } = null!;
        public Base DbContext { get; set; } = null!;
        public string Error { get; protected set; } = string.Empty;
    }
}

