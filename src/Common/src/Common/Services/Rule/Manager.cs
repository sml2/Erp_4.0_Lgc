﻿using System.Reflection;
using System.Security.Claims;
using ERP.Attributes.Rule;
using ERP.Authorization;
using ERP.Authorization.Rule.Component;
using ERP.Enums.Identity;
using ERP.Enums.Rule;
using ERP.Interface.Rule;
using Microsoft.Extensions.Logging;

namespace ERP.Services.Rule;

using RULE = Attributes.Rule.RuleAttribute;
using Model = Models.Rule.Config;

public abstract class Manager<T> : IManager
{
    private readonly ILogger Logger;
    private readonly Dictionary<int, Model> Configs = new();

    private readonly List<Type> PermissionTypes = new();
    private readonly List<Type> ConfigTypes = new();
    private readonly List<Type> MenuTypes = new();

    private readonly Dictionary<Role, SortedList<string, long>> RoleSeed = new()
        { { Role.SU, new() }, { Role.DEV, new() }, { Role.ADMIN, new() }, { Role.Employee, new() } };

    private void AddSeed(MemberInfo memberInfo, string type) =>
        AddSeed(memberInfo, type, (long)Flag.FULL);

    private void AddSeed(MemberInfo memberInfo, string type, long value) =>
        AddSeed(memberInfo, type, (long)Flag.FULL, value);

    private void AddSeed(MemberInfo memberInfo, string type, long mask, long value)
    {
        if (memberInfo.GetCustomAttribute(typeof(SeedAttribute), false) is SeedAttribute seed)
        {
            var Role = seed.Role;
            foreach (var (k, v) in RoleSeed)
            {
                if (Role.HasFlag(k))
                {
                    if (v.TryGetValue(type, out var old))
                    {
                        old &= ~mask;
                        old ^= value << Helpers.GetLastBit((ulong)mask);
                        v[type] = old;
                    }
                    else
                    {
                        v.Add(type, value);
                    }
                }
            }
        }
    }

    public SortedList<string, long> GetSeed(Role r) => RoleSeed[r];

    /// <summary>
    /// 权限名称, flag缓存
    /// </summary>
    private readonly Dictionary<string, Dictionary<string, long>> Flags = new();

    public Manager(ILogger<Manager<T>> logger)
    {
        foreach (var PropertyInfo in typeof(T).GetProperties())
        {
            if (PropertyInfo!.GetCustomAttribute(typeof(RULE), false) is RULE Rule)
            {
                var PropertyType = PropertyInfo.PropertyType;
                var PropertyName = PropertyInfo.Name;
                if (Rule.Type.HasFlag(Enums.Rule.Types.PERMISSION))
                {
                    if (PropertyType.IsEnum)
                    {
                        var EnumType = PropertyType;
                        var EnumTypeName = EnumType.Name;

                        AddSeed(PropertyInfo, PropertyName);

                        PermissionTypes.Add(EnumType);

                        var flags = new Dictionary<string, long>();
                        foreach (var @enum in Enum.GetValues(EnumType))
                        {
                            var EnumName = EnumType.GetEnumName(@enum); // Enum.GetName(EnumType, value);
                            var fieldInfo = EnumType.GetField(EnumName!)!;
                            var EnumNumber = Convert.ToInt64(@enum);
                            var ul = Convert.ToInt64(@enum);
                            flags.Add(Enum.GetName(EnumType, @enum)!, EnumNumber);
                            AddSeed(fieldInfo, PropertyName, EnumNumber);
                        }

                        Flags.Add(EnumTypeName, flags);

                        if (Rule.Type.HasFlag(Enums.Rule.Types.MENU))
                        {
                            MenuTypes.Add(EnumType);
                        }
                    }
                    else
                    {
                        logger.LogError("RuleAttribute错误的声明在{Type}的{PropertyInfo}字段上，需检查代码,确保为特定类型(Enum,Owned)",
                            typeof(T).Name, PropertyInfo.Name);
                    }
                }
                else if (Rule.Type.HasFlag(Enums.Rule.Types.CONFIG))
                {
                    if (PropertyType.IsGenericType &&
                        PropertyType.GetGenericTypeDefinition().Equals(typeof(Models.DB.Rule.Config<>)))
                    {
                        var GenericType = PropertyType.GetGenericArguments().First();
                        if (GenericType.IsEnum)
                        {
                            var SeedType_Visible = $"{PropertyName}.{nameof(Models.DB.Rule.Config<Flag>.Visible)}";
                            var SeedType_Editable = $"{PropertyName}.{nameof(Models.DB.Rule.Config<Flag>.Editable)}";
                            AddSeed(PropertyInfo, SeedType_Visible);
                            AddSeed(PropertyInfo, SeedType_Editable);
                            var EnumType = GenericType;
                            var flags = new Dictionary<string, long>();
                            foreach (var value in EnumType.GetEnumValues())
                            {
                                var EnumName = EnumType.GetEnumName(value); // Enum.GetName(EnumType, value);
                                var fieldInfo = EnumType.GetField(EnumName!)!;
                                var EnumNumber = Convert.ToInt64(value);
                                AddSeed(fieldInfo, SeedType_Visible, EnumNumber);
                                var ComponentAttribute =
                                    fieldInfo!.GetCustomAttribute(typeof(ComponentAttribute), false);
                                var key = Enum.GetName(EnumType, value)!;
                                if (ComponentAttribute is IComponentData data && ComponentAttribute is CheckBox cb)
                                {
                                    if (!Configs.TryGetValue(data.MenuID, out Model? Config))
                                        Configs.Add(data.MenuID, Config = new(data.MenuID));
                                    Config.Add(PropertyName, EnumNumber, data);
                                    key = cb.key;
                                    data.SetKey($"{PropertyName}.{EnumNumber}");
                                    //填充默认迁移时Config值
                                    var @default = cb.value;
                                    var defaultValue = 0;
                                    if (@default is bool b)
                                        defaultValue = b ? 1 : 0;
                                    else if (@default is int i)
                                        defaultValue = i;
                                    AddSeed(PropertyInfo, SeedType_Editable, EnumNumber, defaultValue);
                                }

                                flags.Add(key, EnumNumber);

                                //flags.Add(Enum.GetName(EnumType, @enum)!, Convert.ToInt64(@enum));
                            }

                            Flags.Add($"{PropertyName}.{nameof(Models.DB.Rule.Config<Flag>.Editable)}", flags);
                            ConfigTypes.Add(EnumType);
                        }
                        else
                        {
                            logger.LogError(
                                "RuleAttribute错误的声明在{Type}的{PropertyInfo}字段上，定义Models.DB.Rule.Config的范型应为枚举类型，需检查代码",
                                typeof(T).Name, PropertyInfo.Name);
                        }
                    }
                    else
                    {
                        logger.LogError("当Rule为CONFIG类型时，字段应为Models.DB.Rule.Config类型");
                    }
                }
                else
                {
                    logger.LogError("不能识别Rule类型[Value={v:d},Name ={N}]", Rule.Type, Rule.Type);
                }
            }
        }

        Logger = logger;
    }

    public abstract Model? Get<TP>(TP id);

    public Model? Get(int id)
    {
        if (Configs.TryGetValue(id, out var config))
        {
            return config;
        }
        else
        {
            Logger.LogInformation("未发现MenuID为[{ID}]的配置项", id);
            return null;
        }
    }

    public Dictionary<string, Dictionary<string, bool>> GetRules(ClaimsPrincipal user)
    {
        var result = new Dictionary<string, Dictionary<string, bool>>();
        foreach (var claim in user.Claims)
        {
            if (Flags.TryGetValue(claim.Type, out var flags))
            {
                if (result.ContainsKey(SetName(claim.Type)))
                    continue;
                var items = new Dictionary<string, bool>();
                foreach (var (name, value) in flags)
                {
                    if (items.ContainsKey(name))
                        continue;
                    var hasFlag = false;
                    try
                    {
                        hasFlag = (long.Parse(claim.Value) & value) == value;
                    }
                    catch
                    {
                        Logger.LogError($"{claim.Type} 权限转换long出错");
                    }

                    items.Add(name, hasFlag);
                }

                result.Add(SetName(claim.Type), items);
            }
        }

        return result;

        string SetName(string name) => name.EndsWith(".Editable") ? name[..^9] : name;
    }
}