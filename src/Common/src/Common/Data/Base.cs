﻿using ERP.Models.Abstract;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
namespace ERP.Data;

using Models.DB.Identity;
using Extensions;
using Interface;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Newtonsoft.Json;
using System.Linq.Expressions;

public class Base : IdentityDbContext<User, Role, int, UserClaim, UserRole, UserLogin, RoleClaim, UserToken>, IUnitOfWork
{
    private readonly IOemProvider _oemProvider;
    /* PM
         切换迁移环境
            $env:ASPNETCORE_ENVIRONMENT = ""
            $env:ASPNETCORE_ENVIRONMENT = "Dev_SQLite"
            # 指定$env:ASPNETCORE_ENVIRONMENT或$env:DOTNET_ENVIRONMENT都可以达到切换效果
            $env:DOTNET_ENVIRONMENT = ""
            $env:DOTNET_ENVIRONMENT = "Dev_SQLite"
            # 保险起见最好连续赋值
            # ps
            $env:ASPNETCORE_ENVIRONMENT = $env:DOTNET_ENVIRONMENT = ""                              #默认测试环境
            $env:ASPNETCORE_ENVIRONMENT = $env:DOTNET_ENVIRONMENT = "Development.SunYong"           #Dev_SQLite环境
            $env:ASPNETCORE_ENVIRONMENT = $env:DOTNET_ENVIRONMENT = "Development.SunYongHome"       #SunYongHome环境
            $env:ASPNETCORE_ENVIRONMENT = $env:DOTNET_ENVIRONMENT = "Development.SunYongCompany"    #SunYongCompany环境
            $env:ASPNETCORE_ENVIRONMENT = $env:DOTNET_ENVIRONMENT = "Development.ZhuYY"             #ZhuYY环境
            $env:ASPNETCORE_ENVIRONMENT = $env:DOTNET_ENVIRONMENT = "Development.XuQijie"           #Xu环境
            # zsh
            export ASPNETCORE_ENVIRONMENT="Development.SunYongCompany" 
            export DOTNET_ENVIRONMENT=%ASPNETCORE_ENVIRONMENT%
         控制台
         https://docs.microsoft.com/zh-cn/ef/core/cli/powershell#add-migration
         添加迁移文件/初始化
          Add-Migration -OutputDir Migrations\Debug\MySQL -Name <String>                #Default
          Add-Migration -OutputDir Migrations\Debug\SQLite [name]                       #Dev_SQLite
          Add-Migration -OutputDir Migrations\Debug\SQLite init                         #Dev_SQLite
         回滚迁移
          Remove-Migration                                                        #会依赖数据库记录进行,无需指定输出目录？
         更新数据库/执行迁移/没有数据库会自动创建
          Update-Database
         删库
          Drop-Database
         生成SQL脚本，来手动迁移
          Script-Migration -Context Label
         获取帮助
          get-help entityframework

        dotnet ef tools
         安装 dotnet tool install --global dotnet-ef
         https://docs.microsoft.com/zh-cn/ef/core/cli/dotnet
         添加迁移
         Win:
          dotnet ef migrations add -o Migrations\MySQL <String>
          dotnet ef migrations add -o Migrations\Debug\SQLite <String>
          dotnet ef migrations add -o Migrations\Debug\SQLite init
         Unix:
          dotnet ef migrations add -o Migrations/Debug/SQLite <String>
          dotnet ef migrations add -o Migrations/Debug/SQLite init
         更新数据库
          dotnet ef database update
    */


    public Base(DbContextOptions options, IOemProvider oemProvider) : base(options)
    {
        _oemProvider = oemProvider;
        // Console.WriteLine("MyDbContext(DbContextOptions<MyDbContext> options)");
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // Console.WriteLine("OnModelCreating");
        base.OnModelCreating(modelBuilder);
        // Console.WriteLine("OnModelCreating base over");

        // modelBuilder.Entity<EmailConfigGroup>().HasMany(g => g.EmailConfigs)
        //     .WithOne(e => e.Group).OnDelete(DeleteBehavior.SetNull);
        modelBuilder.Entity<User>().HasQueryFilter(user => !user.ConcurrencyStamp.StartsWith("ERP2"));
        foreach (var entityType in modelBuilder.Model.GetEntityTypes())
        {
            var clrType = entityType.ClrType;
            if (clrType.IsAssignableTo(typeof(OEMModel)))
            {
                var oem = _oemProvider.OEM;
                if (oem is not null)
                {
                    Expression<Func<OEMModel, bool>> filter = model => model.OEMID == oem.ID;
                    modelBuilder.Entity(clrType).HasQueryFilter(filter);
                }
            }

            if (!clrType.IsAssignableTo(typeof(ISoftDelete))) continue;
            var parameter = Expression.Parameter(clrType);
            var ownedModelType = parameter.Type;
            var ownedAttribute = Attribute.GetCustomAttribute(ownedModelType, typeof(OwnedAttribute));
            if (ownedAttribute == null)
            {
                var propertyMethodInfo = typeof(EF).GetMethod("Property")!.MakeGenericMethod(typeof(bool));
                var isDeletedProperty =
                    Expression.Call(propertyMethodInfo, parameter, Expression.Constant("IsDeleted"));
                var compareExpression = Expression.MakeBinary(ExpressionType.Equal, isDeletedProperty,
                    Expression.Constant(false));
                var lambda = Expression.Lambda(compareExpression, parameter);
                modelBuilder.Entity(clrType).HasQueryFilter(lambda);
            }
        }
        modelBuilder.Entity<User>().ToTable(nameof(User));
        //不需要删除外键
        //modelBuilder.RemoveForeignKeys();
        //Console.WriteLine("OnModelCreating Over");
    }

    protected override void ConfigureConventions(ModelConfigurationBuilder configurationBuilder)
    {
        base.ConfigureConventions(configurationBuilder);
        configurationBuilder.UseMoney();

        configurationBuilder.Properties<MoneyMeta>()
            .HaveConversion<JsonValueConverter>();
        configurationBuilder.DefaultTypeMapping<MoneyMeta>()
            .HasConversion<JsonValueConverter>();
    }

    public override void Dispose()
    {
        this.RemoveDataChanged();
        base.Dispose();
        // Console.WriteLine("MyDbContext.Dispose()");
    }
}

public class JsonValueConverter : ValueConverter<MoneyMeta, string>
{
    /// <summary>
    /// 
    /// </summary>
    public JsonValueConverter()
        : base(MoneyMate => JsonConvert.SerializeObject(MoneyMate), s => JsonConvert.DeserializeObject<MoneyMeta>(s) ?? MoneyMeta.Empty)
    {

    }
}

public interface IUnitOfWork
{
    public Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
}