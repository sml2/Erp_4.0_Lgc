﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
namespace ERP.Data;
using Extensions;
public record OwnSeed
{
    private readonly string TableName;
    public readonly Type ClrType;
    public readonly string Property;
    private readonly string IDName;
    public readonly List<object> Data;

    public OwnSeed(string tableName, Type clrType, string property, string IDName)
    {
        TableName = tableName;
        ClrType = clrType;
        Property = property;
        this.IDName = IDName;
        Data = new();
    }
    //构造带ID的Seed中带Own的数据实体
    public void AddData(object item, int ID) => Data.Add(GetIDCalssConstructor(item.GetType(), TableName, IDName)(ID, item));
    private delegate object Constructor(int id, object item);
    private static Dictionary<string, Constructor> DynamicClasses = new();
    private static ModuleBuilder? moduleBuilder;
    private static Constructor GetIDCalssConstructor(Type tRaw, string Table, string IDName)
    {
        var TypeName = $"{tRaw.AssemblyQualifiedName}_{Table}";
        if (DynamicClasses.TryGetValue(TypeName, out var targer))
            return targer;
        else
        {
            var ParentType = typeof(object);//tRaw
            var ParentConstructor = ParentType.GetConstructor(Type.EmptyTypes);
            if (ParentConstructor is null)
                throw new NullReferenceException($"ParentType has no Constructor of EmptyTypes");
            var RawProperties = tRaw.GetProperties(BindingFlags.Instance | BindingFlags.Public);
            if (RawProperties is null)
                throw new NullReferenceException($"{RawProperties} has no Constructor of EmptyTypes");
            if (moduleBuilder is null)
            {
                AssemblyName DynamicName = new($"SeedDynamicClasses");
                var assemblyBuilder = AssemblyBuilder.DefineDynamicAssembly(DynamicName, AssemblyBuilderAccess.Run);
                moduleBuilder = assemblyBuilder.DefineDynamicModule(DynamicName.Name!);
            }
            var typeBuilder = moduleBuilder.DefineType(TypeName, TypeAttributes.Public, ParentType);
            (Type[], FieldBuilder) MakeProperty(string Name, Type PropertyType)
            {
                Type[] PropertyTypeArr = new[] { PropertyType };
                var fbb = typeBuilder.DefineField($"m{Name}", PropertyType, FieldAttributes.Private);
                var mas = MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig;

                var Geter = typeBuilder.DefineMethod($"get_{Name}", mas, PropertyType, Type.EmptyTypes);
                ILGenerator GeterIL = Geter.GetILGenerator();
                GeterIL.Emit(OpCodes.Ldarg_0);
                GeterIL.Emit(OpCodes.Ldfld, fbb);
                GeterIL.Emit(OpCodes.Ret);

                var SetID = typeBuilder.DefineMethod($"set_{Name}", mas, null, PropertyTypeArr);
                ILGenerator SeterIL = SetID.GetILGenerator();
                SeterIL.Emit(OpCodes.Ldarg_0);
                SeterIL.Emit(OpCodes.Ldarg_1);
                SeterIL.Emit(OpCodes.Stfld, fbb);
                SeterIL.Emit(OpCodes.Ret);

                var ptb = typeBuilder.DefineProperty(Name, PropertyAttributes.HasDefault, PropertyType, null);
                ptb.SetGetMethod(Geter);
                ptb.SetSetMethod(SetID);
                return (PropertyTypeArr, fbb);
            }
            (var IDPropertyTypeArr, var fbID) = MakeProperty($"{Table}{IDName}", typeof(int));
            RawProperties.ForEach(p => MakeProperty(p.Name, p.PropertyType));

            ConstructorBuilder ctorID = typeBuilder.DefineConstructor(MethodAttributes.Public, CallingConventions.Standard, IDPropertyTypeArr);
            ILGenerator ctor1IL = ctorID.GetILGenerator();
            ctor1IL.Emit(OpCodes.Ldarg_0);
            ctor1IL.Emit(OpCodes.Call, ParentConstructor);
            ctor1IL.Emit(OpCodes.Ldarg_0);
            ctor1IL.Emit(OpCodes.Ldarg_1);
            ctor1IL.Emit(OpCodes.Stfld, fbID);
            ctor1IL.Emit(OpCodes.Ret);

            var NewType = typeBuilder.CreateType();
            if (NewType is null)
                throw new NullReferenceException($"{typeBuilder.AssemblyQualifiedName} can not be created");
            var Constructor = NewType.GetConstructor(IDPropertyTypeArr);
            if (Constructor is null)
                throw new NullReferenceException($"{Constructor} not found");
            Debug.Assert(!ctorID.Equals(Constructor));

            var Raw2New = RawProperties.ToDictionary(p => p, p => NewType.GetProperty(p.Name)!); //原属性与目标属性对应关系，缓存好供使用，降低反射开销

            //构建复制实例数据的委托
            object cd(int id, object item)
            {
                var o = Constructor.Invoke(new object[] { id });
                Raw2New.ForEach(kv => kv.Value.SetValue(o, kv.Key.GetValue(item)));
                return o;
            }

            DynamicClasses.Add(TypeName, cd);
            return cd;
        }
    }
}