using Microsoft.EntityFrameworkCore;
namespace ERP.Data;
/// <summary>
/// 货币及其元数据
/// </summary>
[Owned]
public record MoneyRecord
{
    //不可以共用一个实体去保存
    //https://github.com/dotnet/efcore/issues/24614
    //https://github.com/dotnet/efcore/issues/20474
    public static MoneyRecord Empty => new();

    public MoneyRecord() { }
    public MoneyRecord(decimal v) : this(v, MoneyMeta.Empty) { }
    public MoneyRecord(Money money, MoneyMeta raw)
    {
        Money = money;
        Raw = raw;
    }
    /// <summary>
    /// 基准货币
    /// </summary>
    public Money Money { get; init; } = Money.Zero;
    /// <summary>
    /// 原始数据
    /// </summary>
    public MoneyMeta Raw { get; init; } = MoneyMeta.Empty;

    public static MoneyRecord operator +(MoneyRecord mr, decimal v) => new(mr.Money + v);
    public static MoneyRecord operator +(MoneyRecord mr, decimal? v) => mr + v.GetValueOrDefault();
    public static MoneyRecord operator +(MoneyRecord mr, MoneyRecord mrOther) => mr with { Money = mr.Money + mrOther.Money };
    public static MoneyRecord operator -(MoneyRecord mr, decimal v) => new(mr.Money - v);
    public static MoneyRecord operator -(MoneyRecord mr, decimal? v) => mr - v.GetValueOrDefault();
    public static MoneyRecord operator -(MoneyRecord mr, MoneyRecord mrOther) => mr with { Money = mr.Money - mrOther.Money };
    public static implicit operator MoneyRecord(decimal v) => new(v);

    public override string? ToString()
    {
        return Money.ToString();
    }
    public string ToString(string unitSign) => ToString(unitSign, true);
    public string ToString(string unitSign, bool showUnit)
    {
        return $"{Money.To(unitSign):0.00}{(showUnit ? $" {unitSign}" : "")}";
    }
    public string? ToString(bool showUnit = true)
    {
        return Money.ToString();
    }   
}