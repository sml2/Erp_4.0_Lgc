using ERP.Models.Finance;
using Microsoft.EntityFrameworkCore;

namespace ERP.Data;
/// <summary>
/// 货币 元数据 财务当前归档
/// Money + MoneyMate + FinancialAffairs
/// </summary>
[Owned]
public record MoneyRecordFinancialAffairs : MoneyRecord
{
    public static new MoneyRecordFinancialAffairs Empty => new();
    public MoneyRecordFinancialAffairs()
    {
    }
    public MoneyRecordFinancialAffairs(decimal v) : base(v)
    {
    }
    public MoneyRecordFinancialAffairs(Money money, MoneyMeta raw) : base(money, raw)
    {
    }
    public MoneyRecordFinancialAffairs(Money money, MoneyMeta raw, int fid) : base(money, raw)
    {
        FinancialAffairsID = fid;
    }
    public MoneyRecordFinancialAffairs(Money money, MoneyMeta raw, FinancialAffairsModel financialAffairs) : base(money, raw)
    {
        FinancialAffairs = financialAffairs;
    }
    public MoneyRecordFinancialAffairs(MoneyRecord original) : base(original)
    {
    }
    public MoneyRecordFinancialAffairs(MoneyRecord original, int fid) : base(original)
    {
        FinancialAffairsID = fid;
    }
    public MoneyRecordFinancialAffairs(MoneyRecord original, FinancialAffairsModel  financialAffairs) : base(original)
    {
        FinancialAffairs = financialAffairs;
    }
    
    public int? FinancialAffairsID { get; set; }
    public FinancialAffairsModel? FinancialAffairs { get; set; }

    public static MoneyRecordFinancialAffairs operator +(MoneyRecordFinancialAffairs mr, decimal v)
    {
        var result =new MoneyRecordFinancialAffairs(mr.Money + v);
        result.FinancialAffairsID = mr.FinancialAffairsID;
        return result;
    }

    public static MoneyRecordFinancialAffairs operator -(MoneyRecordFinancialAffairs mr, decimal v)
    {
        var result =new MoneyRecordFinancialAffairs(mr.Money - v);
        result.FinancialAffairsID = mr.FinancialAffairsID;
        return result;
    }

    public static MoneyRecordFinancialAffairs operator +(MoneyRecordFinancialAffairs mr, decimal? v) => mr + v.GetValueOrDefault();
    public static MoneyRecordFinancialAffairs operator -(MoneyRecordFinancialAffairs mr, decimal? v) => mr - v.GetValueOrDefault();

    public static implicit operator MoneyRecordFinancialAffairs(decimal v) => new(v);
    public static implicit operator MoneyRecordFinancialAffairs(Money v) => new(v);


    public MoneyRecordFinancialAffairs Next(MoneyRecord original)
    {
        var result = new MoneyRecordFinancialAffairs(original);
        result.FinancialAffairsID = FinancialAffairsID;
        return result;
    }

    public MoneyRecordFinancialAffairs Next(Money v)
    {
        var result = new MoneyRecordFinancialAffairs(v);
        result.FinancialAffairsID = FinancialAffairsID;
        return result;
    }
    
    public MoneyRecordFinancialAffairs Next(decimal v)
    {
        var result = new MoneyRecordFinancialAffairs(v);
        result.FinancialAffairsID = FinancialAffairsID;
        return result;
    }
    
    public MoneyRecordFinancialAffairs Next(string currencyCode, decimal amount)
    {
        var result = new MoneyMeta(currencyCode, amount).ToMoneyRecordFinancialAffairs();
        result.FinancialAffairsID = FinancialAffairsID;
        return result;
    }
    public MoneyRecordFinancialAffairs Next(string currencyCode, double amount)
    {
        var result = new MoneyMeta(currencyCode, amount).ToMoneyRecordFinancialAffairs();
        result.FinancialAffairsID = FinancialAffairsID;
        return result;
    }
    public MoneyRecordFinancialAffairs Next(int fid)
    {
        return this with {FinancialAffairsID = fid};
    }
    
    public MoneyRecordFinancialAffairs Next(FinancialAffairsModel m)
    {
        return this with {FinancialAffairs = m,FinancialAffairsID = m.ID};
    }
}