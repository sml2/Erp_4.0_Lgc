﻿namespace ERP.Data;
/// <summary>
/// 货币原数据
/// </summary>
public record MoneyMeta
{
    /// <summary>
    /// 转换成归档数据
    /// </summary>
    public MoneyRecord ToRecode() => new(Result, this);
    /// <summary>
    /// 转换成归档财务
    /// </summary>
    public MoneyRecordFinancialAffairs ToMoneyRecordFinancialAffairs() => new(ToRecode());
    /// <summary>
    /// 单位
    /// </summary>
    public string CurrencyCode { get; init; }
    /// <summary>
    /// 货币数值
    /// </summary>
    public decimal Amount { get; init; }
    /// <summary>
    /// 原始数据
    /// </summary>
    public string Raw { get; init; }
    /// <summary>
    /// 结果
    /// </summary>
    public Money Result { get; init; }
    public decimal Rate { get; init; }
    public bool Success { get; init; }
    /// <summary>
    /// 来源类型
    /// </summary>
    public FromTypes FromType { get; init; }
    public static readonly MoneyMeta Empty = new();

    public enum FromTypes
    {
        Empty,
        Decimal,
        Float, Double,
        Byte, Short, Int, Long,
        SByte, UShort, UInt, ULong,
        StringValid, StringInvalid,
    }
    
    private MoneyMeta(string code, string raw, FromTypes from) { CurrencyCode = code; Raw = raw; FromType = from; }
    private MoneyMeta(string code, decimal amount, string raw, FromTypes from)
    {
        CurrencyCode = code;
        Amount = amount;
        Raw = raw;
        if (((FromType = from) != FromTypes.StringInvalid) && (Success = Money.TryParse($"{Amount}{CurrencyCode}", out var Result, out var Rate)))
        {
            this.Result = Result;
            this.Rate = Rate;
        }
        else
        {
            this.Result = Money.Zero;
            this.Rate = default;
        }
    }
    private MoneyMeta(string code, string raw, FromTypes from, decimal amount) : this(code, amount, raw, from) { }
    public MoneyMeta() : this(string.Empty, 0, string.Empty, FromTypes.Empty) { }
    public MoneyMeta(string currencyCode, decimal amount) : this(currencyCode, amount, amount.ToString(), FromTypes.Decimal) { }
    public MoneyMeta(string currencyCode, byte amount) : this(currencyCode, amount, amount.ToString(), FromTypes.Byte) { }
    public MoneyMeta(string currencyCode, sbyte amount) : this(currencyCode, amount, amount.ToString(), FromTypes.SByte) { }
    public MoneyMeta(string currencyCode, short amount) : this(currencyCode, amount, amount.ToString(), FromTypes.Short) { }
    public MoneyMeta(string currencyCode, ushort amount) : this(currencyCode, amount, amount.ToString(), FromTypes.UShort) { }
    public MoneyMeta(string currencyCode, int amount) : this(currencyCode, amount, amount.ToString(), FromTypes.Int) { }
    public MoneyMeta(string currencyCode, uint amount) : this(currencyCode, amount, amount.ToString(), FromTypes.UInt) { }
    public MoneyMeta(string currencyCode, long amount) : this(currencyCode, amount, amount.ToString(), FromTypes.Long) { }
    public MoneyMeta(string currencyCode, ulong amount) : this(currencyCode, amount, amount.ToString(), FromTypes.ULong) { }
    public MoneyMeta(string currencyCode, float amount) : this(currencyCode, Convert.ToDecimal(amount), amount.ToString(), FromTypes.Float) { }
    public MoneyMeta(string currencyCode, double amount) : this(currencyCode, Convert.ToDecimal(amount), amount.ToString(), FromTypes.Double) { }
    public MoneyMeta(string currencyCode, string amount) : this(currencyCode, amount, decimal.TryParse(amount, out var v) ? FromTypes.StringValid : FromTypes.StringInvalid, v) { }
    public static implicit operator MoneyRecord(MoneyMeta Mate) => Mate.ToRecode();
    public static implicit operator MoneyRecordFinancialAffairs(MoneyMeta Mate) => Mate.ToMoneyRecordFinancialAffairs();
    public static implicit operator Money(MoneyMeta Mate) => new(Mate.Amount);
    public static implicit operator decimal(MoneyMeta Mate) => Mate.Amount;
}