﻿//基准货币
global using Money = Ryu.Data.Money;
global using MoneyFactory = Ryu.Data.MoneyFactory;
global using MoneyRateProvider = Ryu.Data.RateProvider;
//货币元数据归档
global using MoneyMeta = ERP.Data.MoneyMeta;
//货币差值
// global using MoneyDelta = ERP.Data.MoneyDelta;
//基准货币及货币元数据
//MoneyRecord = Money + MoneyMate
global using MoneyRecord = ERP.Data.MoneyRecord;
//基准货币+货币元数据+财务当前归档
global using MoneyRecordFinancialAffairs = ERP.Data.MoneyRecordFinancialAffairs;
// global using MoneyFinancialAffairs = ERP.Data.MoneyFinancialAffairs;
