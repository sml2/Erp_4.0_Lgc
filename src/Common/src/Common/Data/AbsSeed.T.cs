﻿using System.Diagnostics;
using System.Reflection;
using System.Reflection.Emit;
using Microsoft.EntityFrameworkCore;

namespace ERP.Data;

public abstract class AbsSeed<TRecode, TSeedFactory>: AbsSeed
    where TRecode : class
    where TSeedFactory : AbsSeed<TRecode, TSeedFactory>, new()
{
    protected int curNum = 1;
    protected int NumAdd(bool add) => add ? curNum++ : curNum;
    protected int Sorter { get => NumAdd(false); }
    protected int Indexer { get => NumAdd(true); }

    protected static readonly DateTime Now = new DateTime(2021, 11, 19, 13, 38, 0);

    public static TRecode[] GetModels()
    {
        var Fac = new TSeedFactory();
        //不需要处理最后一个可能为空的情形
        //new[] {m,} 不会使返回数组最后一个多一个null
        //return Fac.Create().Where(x => x is not null).ToArray();
        return Fac.Create();
    }
    public abstract TRecode[] Create();

    public static void Seed(ModelBuilder builder)
    {
        var Models = GetModels();
        List<OwnSeed> OwnSeed = new();
        foreach (var fk in builder.Entity<TRecode>().Metadata.GetReferencingForeignKeys().Where(fk => fk.IsOwnership))
        {
            var p = fk.PrincipalToDependent;
            var clrType = p!.ClrType;
            var PropertyInfo = p.PropertyInfo;
            if (PropertyInfo is null)
                throw new NullReferenceException(nameof(PropertyInfo));
            var TableType = p.DeclaringEntityType;
            var TableName = TableType.ClrType.Name;
            if (TableName is null)
                throw new NullReferenceException(nameof(TableName));
            var IDGeter = TableType.GetKeys().First().Properties[0].PropertyInfo;
            if (IDGeter is null)
                throw new NullReferenceException(nameof(TableName));
            if (clrType.GetCustomAttributes(typeof(OwnedAttribute), true).Length > 0)
            {
                OwnSeed ows = new(TableName, clrType, p.Name, IDGeter.Name);
                foreach (var m in Models)
                {
                    var item = PropertyInfo.GetValue(m);
                    if (item is null)
                        throw new NullReferenceException(nameof(item));
                    var Idobj = IDGeter.GetValue(m);
                    if (Idobj is null)
                        throw new NullReferenceException(nameof(Idobj));
                    ows.AddData(item, (int)Idobj);
                    PropertyInfo.SetValue(m, null);
                }
                OwnSeed.Add(ows);
            }
        }

        var EntityTypeBuilder = builder.Entity<TRecode>();

        EntityTypeBuilder.HasData(Models);

        //Data中的数据类型不可直接继承自Own类，HasData内部会判断相同类时会抛出异常
        //if (ClrType != datum.GetType() && ClrType.IsAssignableFrom(datum.GetType())) throw new InvalidOperationException(CoreStrings.SeedDatumDerivedType(DisplayName(), datum.GetType().ShortDisplayName()));
        //Microsoft.EntityFrameworkCore.Metadata.Internal.EntityType..HasData(IEnumerable<object> data)
        //Microsoft.EntityFrameworkCore.Metadata.Internal.InternalEntityTypeBuilder.HasData(IEnumerable<object> data, ConfigurationSource configurationSource)
        //Microsoft.EntityFrameworkCore.Metadata.Builders.OwnedNavigationBuilder.HasData(IEnumerable<object> data)
        OwnSeed.Where(ows => ows.Data.Count > 0).ForEach(ows => EntityTypeBuilder.OwnsOne(ows.ClrType, ows.Property).HasData(ows.Data));
    }

    
}
