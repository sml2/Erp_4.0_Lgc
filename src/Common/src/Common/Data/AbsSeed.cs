﻿using ERP.Interface.Rule;
using ERP.Services.Rule;

namespace ERP.Data;

public abstract class AbsSeed {
    public static void SetStatic(IServiceProvider serviceProvider)
    {
        ServiceProvider = serviceProvider;
    }
    protected static IServiceProvider ServiceProvider { get; set; } = null!;
}
