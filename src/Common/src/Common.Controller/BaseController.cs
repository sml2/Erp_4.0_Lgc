﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Security.Claims;
using FluentResults;
using Microsoft.VisualBasic;

namespace ERP.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        protected int superId = Constants.SuperId;
        protected IActionResult Json(object? data = null)
        {
            return new JsonResult(data);
        }
        const string DefaultSuccessTitle = "成功";
        internal const string DefaultSuccessMessage = "执行完成";
        const string DefaultWarningTitle = "警告";
        const string DefaultWarningMessage = "出现问题";
        const string DefaultErrorTitle = "报错";
        internal const string DefaultErrorMessage = "发生异常";
        protected ResultStruct Success()
        {
            return new(Codes.None | Codes.Success, DefaultSuccessTitle, DefaultSuccessMessage, null);
        }
        protected ResultStruct Success(object? data)
        {
            return new(Codes.None | Codes.Success, DefaultSuccessTitle, DefaultSuccessMessage,data);
        }
        public static ResultStruct Success(object data, string message = DefaultSuccessMessage)
        {
            return new(Codes.None | Codes.Success, DefaultSuccessTitle, message, data);
        }
        protected ResultStruct Success(string message, string title = DefaultSuccessTitle)
        {
            return new(Codes.None | Codes.Success, title, message, null);
        }
        protected ResultStruct Warning()
        {
            return new(Codes.None | Codes.Warning, DefaultWarningTitle, DefaultWarningMessage, null);
        }
        protected ResultStruct Warning(object data, string message = DefaultWarningMessage)
        {
            return new(Codes.None | Codes.Warning, DefaultWarningTitle, message, data);
        }
        protected ResultStruct Warning(string message, string title = DefaultWarningTitle)
        {
            return new(Codes.None | Codes.Warning, title, message, null);
        }
        protected ResultStruct Error()
        {
            return new(Codes.None | Codes.Error, DefaultErrorTitle, DefaultErrorMessage, null);
        }
        public static ResultStruct Error(object data, string message = DefaultErrorMessage)
        {
            return new(Codes.None | Codes.Error, DefaultErrorTitle, message, data);
        }
        protected static ResultStruct Error(string message, string title = DefaultErrorTitle)
        {
            return new(Codes.None | Codes.Error, title, message, null);
        }
        public static ResultStruct Error(Exception e)
        {
            return Error(e.Message);
        }
    }

    [Flags]
    public enum Codes
    {
        //01
        ZERO,
        ONE,
        TWO,
        THREE,
        FOUR,
        FIVE,
        SIX,
        SEVEN,

        //01 WinType
        None = ONE,
        Notify = TWO,
        Message = THREE,
        //23 State
        Success = ONE << 2,
        Warning = TWO << 2,
        Error = THREE << 2,
        //456 Validate
        V1 = ONE << 4,
        V2 = TWO << 4,
        V3 = THREE << 4,
        V4 = FOUR << 4,
        V5 = FIVE << 4,
        VMax = SEVEN << 4,
        //789 Login
        Login1 = ONE << 7,
        Login2 = TWO << 7,
        Login3 = THREE << 7,
        Login4 = FOUR << 7,
        Login5 = FIVE << 7,
        Login6 = SIX << 7,
        LoginMax = SEVEN << 7
    }

    public struct ResultStruct
    {
        public ResultStruct(Codes code, string title, string message, object? data)
        {
            Title = title;
            Message = message;
            Data = data ?? new { };
            Code = code;
        }

        public string Title { get; set; }
        public string Message { get; set; }
        public object? Data { get; set; }
        public Codes Code { get; set; }
    }

    public static class ResultToResultStructExtension
    {
        public static ResultStruct ToResultStruct(this Result result)
        {
            return result.IsSuccess
                ? BaseController.Success(null!,
                    result.Successes.FirstOrDefault()?.Message ?? BaseController.DefaultSuccessMessage)
                : BaseController.Error(null!,
                    result.Errors.FirstOrDefault()?.Message ?? BaseController.DefaultErrorMessage);
        }
        
        public static ResultStruct ToResultStruct<TValue>(this Result<TValue> result)
        {
            return result.IsSuccess
                ? BaseController.Success(result.Value!,
                    result.Successes.FirstOrDefault()?.Message ?? BaseController.DefaultSuccessMessage)
                : BaseController.Error(null!,
                    result.Errors.FirstOrDefault()?.Message ?? BaseController.DefaultErrorMessage);
        }
    }
}
