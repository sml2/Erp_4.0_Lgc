﻿namespace ERP.Export.ExportCommon;

public interface IParameterItem
{
     string primary_category { get; set; }
     string model { get; set; }
     string brand { get; set; }
     string color_family { get; set; }
     string package_content { get; set; }
     string package_length { get; set; }
     string package_width { get; set; }
     string package_height { get; set; }
     string package_weight { get; set; }
     string taxes { get; set; }
}
