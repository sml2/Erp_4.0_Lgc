﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ERP.Export.ExportCommon;

public class CommonParameter
{
    public List<Custom_attribute>? custom_attribute { get; set; }

    [JsonExtensionData]
    private IDictionary<string, JToken> _additionalData = new Dictionary<string, JToken>();

    public JToken this[string key]
    {
        get => _additionalData[key];
        set => _additionalData[key] = value;
    }

    public class Custom_attribute
    {
        public string? key { get; set; }
        public string? value { get; set; }
    }
}
