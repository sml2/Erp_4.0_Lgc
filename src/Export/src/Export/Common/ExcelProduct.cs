﻿namespace ERP.Export.ExportCommon;

public class ExcelProduct
{
    public ExcelProduct(string name, string? description, List<string> bulletPoint, string? searchTerms, string color, string size, string sku, string? mainImage, List<string>? images, int quantity, string price, string cost, string produceCode, string? fields, string source)
    {
        Name = name;
        Description = description ?? string.Empty;
        BulletPoint = bulletPoint ?? new();
        SearchTerms = searchTerms ?? string.Empty;
        Color = color;
        Size = size;
        Sku = sku;
        MainImage = mainImage;
        Images = images ?? Enumerable.Empty<string>().ToList();
        Quantity = quantity;
        Price = price;
        Cost = cost;
        ProduceCode = produceCode;
        Fields = fields;
        Source = source;
    }
    public string Name { get; set; }
    public string Description { get; set; }
    public List<string> BulletPoint { get; set; }
    public string SearchTerms { get; set; }
    public string Color { get; set; }
    public string Size { get; set; }
    public string Sku { get; set; }
    public string? MainImage { get; set; }
    public List<string> Images { get; set; }
    public List<string> DescriptionImages { get; set; }
    public int Quantity { get; set; }
    public string Price { get; set; }
    public string Cost { get; set; }
    public string ProduceCode { get; set; }
    public string? Fields { get; set; }
    public string Source { get; set; }
    public string OtherAttribute { get; set; } = string.Empty;

}
