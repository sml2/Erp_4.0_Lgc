using System.Net.Http;
using System.Text;
using ERP.Exceptions.Export;
using ERP.Export.Templates.Adapter;
using ERP.Models.Export;
using FluentResults;
using Newtonsoft.Json;
using NPOI.OpenXml4Net.OPC;
using NPOI.SS.UserModel;
using NPOI.XSSF.Streaming;
using NPOI.XSSF.UserModel;
using Sy.Security;

namespace ERP.Export.ExportCommon;

using ERP.Export.Templates;
using System.Linq;

public class ProductExportHelper
{
    private readonly IWebHostEnvironment _env;
    private readonly ILogger<ProductExportHelper> _logger;


    public ProductExportHelper(IWebHostEnvironment env, ILogger<ProductExportHelper> logger)
    {
        _env = env;
        _logger = logger;
    }

    /// <summary>
    /// 根据表单名称读取表头
    /// </summary>
    /// <param name="Name"></param>
    /// <returns></returns>
    public async Task<Dictionary<string, List<List<string>>>> GetHead(TemplateType Name)
    {
        Dictionary<string, List<List<string>>> DicHeaders = new Dictionary<string, List<List<string>>>();
        string FilePath =
            Path.Combine(
                _env.EnvironmentName.StartsWith("Development")
                    ? Path.GetDirectoryName(Environment.ProcessPath)!
                    : _env.ContentRootPath, "Export", "Templates", "Config");
        string FileName = Path.Combine(FilePath, $"{Name}.xltx");
        try
        {
            if (!File.Exists(FileName))
            {
                _logger.LogError("模板文件{FileName}不存在", FileName);
                return null!;
            }

            IWorkbook workbook = null!;
            using (var fs = new FileStream(FileName, FileMode.Open, FileAccess.Read))
            {
                workbook = new XSSFWorkbook(fs);
            }

            for (int SheetIndex = 0; SheetIndex < workbook.NumberOfSheets; SheetIndex++)
            {
                List<List<string>> Headers = new List<List<string>>();
                ISheet sheet = workbook.GetSheetAt(SheetIndex);
                string SheetName = sheet.SheetName;
                var rows = sheet.GetRowEnumerator();
                while (rows.MoveNext())
                {
                    List<string> Header = new();
                    IRow row = (rows.Current as IRow)!;
                    bool IsAdd = false;
                    for (int i = 0; i < row.LastCellNum; i++)
                    {
                        string TempH = row.GetCell(i)?.ToString() ?? "";
                        if (TempH.Length > 0) IsAdd = true;
                        Header.Add(TempH);
                    }

                    if (IsAdd) Headers.Add(Header);
                }

                if (Headers is not null) DicHeaders.Add(SheetName, Headers);
            }

            return DicHeaders;
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "文件{FileName}异常", FileName);
            return null!;
        }
    }

    /// <summary>
    /// 获取导出文件的后缀
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public static string GetExportFileExtension(TemplateType type) => type switch
    {
        TemplateType.Hktvmall or TemplateType.GlobalSources => "zip",
        TemplateType.ShopifyCSV or TemplateType.OriginEbay => "csv",
        _ => "xlsx",
    };

    /// <summary>
    /// 创建导出适配器
    /// </summary>
    /// <param name="type"></param>
    /// <param name="sp"></param>
    /// <returns></returns>
    public static IExportAdapter CreateExportAdapter(TemplateType type, IServiceProvider sp) => type switch
    {
        TemplateType.ShopifyCSV or TemplateType.OriginEbay => new CsvExportAdapter(),
        _ => new ExcelExportAdapter(new SXSSFWorkbook(new XSSFWorkbook()), sp.GetRequiredService<ILogger<ExcelExportAdapter>>()),
    };

    /// <summary>
    /// 创建导出适配器
    /// </summary>
    /// <param name="type"></param>
    /// <param name="sp"></param>
    /// <param name="path"></param>
    /// <param name="state"></param>
    /// <returns></returns>
    public static Result<IExportAdapter> RestoreExportAdapter(TemplateType type, IServiceProvider sp, string path, string? state)
    {
        if (!File.Exists(path))
            return Result.Fail($"临时文件[{path}]丢失");

        
        if (type == TemplateType.ShopifyCSV)
            return new CsvExportAdapter(path);
        
        if (string.IsNullOrEmpty(state))
            return Result.Fail("断点信息丢失");
        
        var openStream = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.Read);
        var workbook = new SXSSFWorkbook(new XSSFWorkbook(OPCPackage.Open(openStream)));

        var rowIndex = JsonConvert.DeserializeObject<int[]>(state);
        if (rowIndex is null || rowIndex.Length != workbook.NumberOfSheets)
            return Result.Fail("读取临时表格异常");
        
        Dictionary<ISheet, int> sheets = new();
        for (int i = 0; i < rowIndex.Length; i++)
        {
            sheets.Add(workbook.GetSheetAt(i), rowIndex[i]);
        }
        return new ExcelExportAdapter(workbook, sp.GetRequiredService<ILogger<ExcelExportAdapter>>(), sheets);
    }


    /// <summary>
    /// 获取客户自定义标题和值
    /// </summary>
    /// <param name="Parameter"></param>
    /// <returns></returns>
    public List<List<string>> GetCustomAttribute(string Parameter)
    {
        List<List<string>> OutPut = new();
        List<string> CusKey = new();
        List<string> CusValue = new();
        var Item = ToDeserialize<CommonParameter>(Parameter);
        if (Item is not null && Item.custom_attribute is not null && Item.custom_attribute.Count() > 0)
        {
            foreach (var p in Item.custom_attribute)
            {
                if (p.key.IsNotWhiteSpace() && p.value.IsNotWhiteSpace())
                {
                    CusKey.Add(p.key);
                    CusValue.Add(p.value);
                }
            }

            OutPut.Add(CusKey);
            OutPut.Add(CusValue);
        }

        return OutPut;
    }

    /// <summary>
    /// 根据描述规则获取描述
    /// </summary>
    /// <param name="Description"></param>
    /// <param name="Sketch"></param>
    /// <param name="KeyWord"></param>
    /// <param name="Mode"></param>
    /// <returns></returns>
    public string GetDescription(string? Description, string Sketch, string? KeyWord,
        ExcleRole.DescriptionType Mode, bool DescriptionAppendImage, List<string>? Images)
    {
        var ProDescription = Description;
        if (ProDescription.IsNullOrEmpty())
        {
            if (Mode == ExcleRole.DescriptionType.Keyword) ProDescription = KeyWord;
            else if (Mode == ExcleRole.DescriptionType.Sketch) ProDescription = Sketch;
            else if (Mode == ExcleRole.DescriptionType.Keyword_Sketch)
            {
                if (KeyWord.IsNullOrEmpty()) ProDescription = Sketch;
                else ProDescription = KeyWord;
            }
            else if (Mode == ExcleRole.DescriptionType.Sketch_Keyword)
            {
                if (Sketch.IsNullOrEmpty()) ProDescription = KeyWord;
                else ProDescription = Sketch;
            }
        }

        if (DescriptionAppendImage && Images.IsNotNull() && Images.Count > 0)
        {
            List<string> TempImage = new();
            Images.ForEach((r) => TempImage.Add($"<img src=\"{r}\">"));
            ProDescription += string.Join("<br>", TempImage);
        }

        return ProDescription!;
    }

    /// <summary>
    /// 获取卖点
    /// </summary>
    /// <param name="sketch"></param>
    /// <param name="keyword"></param>
    /// <param name="description"></param>
    /// <param name="mode"></param>
    /// <returns></returns>
    public List<string> GetBulletPoint(List<string> sketch, string? keyword, string? description,
        ExcleRole.SketchType mode)
    {
        var bulletPoint = sketch;
        if (bulletPoint.IsNull() || bulletPoint.Count == 0)
        {
            if (mode == ExcleRole.SketchType.Keyword) bulletPoint = new List<string>() { keyword! };
            else if (mode == ExcleRole.SketchType.Description) bulletPoint = new List<string>() { description! };
            else if (mode == ExcleRole.SketchType.Keyword_Description)
            {
                if (keyword.IsNullOrEmpty()) bulletPoint = new List<string>() { description! };
                else bulletPoint = new List<string>() { keyword };
            }
            else if (mode == ExcleRole.SketchType.Description_Keyword)
            {
                if (description.IsNullOrEmpty()) bulletPoint = new List<string>() { keyword! };
                else bulletPoint = new() { description };
            }
        }

        bulletPoint = bulletPoint.Take(5).ToList();
        var count = bulletPoint.Count;
        if (count < 5)
        {
            for (int i = 1; i <= 5 - count; i++)
            {
                bulletPoint.Add("");
            }
        }

        return bulletPoint;
    }

    /// <summary>
    /// 获取关键字
    /// </summary>
    /// <param name="Sketch"></param>old List<string>
    /// <param name="Keyword"></param>
    /// <param name="Description"></param>
    /// <param name="Mode"></param>
    /// <returns></returns>
    public List<string> GetKeywords(List<string> Sketch, string? Keyword, string? Description,
        ExcleRole.KeywordType Mode)
    {
        List<string> Keywords;
        if (Keyword.IsNotWhiteSpace())
        {
            Keywords = new List<string> { Keyword }; //BulletPoint[SearchTerms] ? BulletPoint.Values[Description]
        }
        else
        {
            Keywords = new List<string>();

            if (Mode == ExcleRole.KeywordType.Sketch)
                Keywords = Sketch;

            else if (Mode == ExcleRole.KeywordType.Description) Keywords = new List<string>() { Description! };
            else if (Mode == ExcleRole.KeywordType.Sketch_Description)
            {
                if (Sketch.IsNull() || Sketch.Count == 0) Keywords = new List<string>() { Description! };
                else Keywords = Sketch;
            }
            else if (Mode == ExcleRole.KeywordType.Description_Sketch)
            {
                if (Description.IsNullOrEmpty())
                    Keywords = Sketch;
                else Keywords = new List<string>() { Description };
            }
        }

        Keywords = Keywords.Take(5).ToList();
        int Count = Keywords.Count;
        if (Count < 5)
        {
            for (int i = 1; i <= 5 - Count; i++)
            {
                Keywords.Add("");
            }
        }

        return Keywords;
    }

    public void SetCommonImage(List<string?> Pro, List<Dictionary<string, string>> BulletPoint, int ExportCount)
    {
        if (BulletPoint is null)
        {
            SetCommonCell(Pro, "", ExportCount);
        }
        else
        {
            var TempImages = BulletPoint.Take(ExportCount).ToList();
            TempImages.ForEach(x => x.ForEach(y => Pro.Add($"{(y.Key, y.Value)}")));
            int Count = TempImages.Count;
            if (Count < ExportCount)
            {
                SetCommonCell(Pro, "", ExportCount - Count);
            }
        }
    }

    public static void SetCommonImage(List<string?> pro, List<string>? images, int exportCount)
    {
        if (images is null || images.Count <= 0)
        {
            SetCommonCell(pro, "", exportCount);
        }
        else
        {
            var tempImages = images.Take(exportCount).ToList();
            pro.AddRange(tempImages);
            var count = tempImages.Count;
            if (count < exportCount)
            {
                SetCommonCell(pro, "", exportCount - count);
            }
        }
    }

    public static void SetCommonCell(List<string?> pro, string data, int count)
    {
        pro.AddRange(Enumerable.Repeat(data, count));
    }

    public static void SetLazadaPrice(List<string?> Pro, string ExcelPrice, List<Unit> Rate)
    {
        double CNY = 0, SGD = 0, MYR = 0, IDR = 0, PHP = 0, THB = 0, VND = 0;
        if (ExcelPrice.IsNullOrEmpty()) ExcelPrice = "0";
        double Price = Convert.ToDouble(ExcelPrice);
        if (Rate is not null && Rate.Count > 0)
        {
            foreach (var r in Rate)
            {
                if (r.Sign == "CNY") CNY = (Price / Convert.ToDouble(r.Rate));
                if (r.Sign == "SGD") SGD = (Price / Convert.ToDouble(r.Rate));
                if (r.Sign == "MYR") MYR = (Price / Convert.ToDouble(r.Rate));
                if (r.Sign == "IDR") IDR = (Price / Convert.ToDouble(r.Rate));
                if (r.Sign == "PHP") PHP = (Price / Convert.ToDouble(r.Rate));
                if (r.Sign == "THB") THB = (Price / Convert.ToDouble(r.Rate));
                if (r.Sign == "VND") VND = (Price / Convert.ToDouble(r.Rate));
            }
        }

        //14 各国价格的位置
        Pro.Add(CNY.ToString("0.00"));
        Pro.Add(SGD.ToString("0.00"));
        Pro.Add(MYR.ToString("0.00"));
        Pro.Add(IDR.ToString("0.00"));
        Pro.Add(PHP.ToString("0.00"));
        Pro.Add(THB.ToString("0.00"));
        Pro.Add(VND.ToString("0.00"));
        Pro.Add((CNY - 1).ToString("0.00"));
        Pro.Add((SGD - 1).ToString("0.00"));
        Pro.Add((MYR - 1).ToString("0.00"));
        Pro.Add((IDR - 1).ToString("0.00"));
        Pro.Add((PHP - 1).ToString("0.00"));
        Pro.Add((THB - 1).ToString("0.00"));
        Pro.Add((VND - 1).ToString("0.00"));
    }

    public void GetLazadaCommonCell(List<string?> Pro, int NullCount, ExcelProduct Data, IParameterItem Item,
        ExcleRole role, List<Unit> Rate)
    {
        //描述
        string Description = GetDescription(Data.Description, string.Join(";", Data.BulletPoint),
            Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, Data.Images);
        //卖点
        string BulletPoint = "";
        var BPLst = GetBulletPoint(Data.BulletPoint, Data.SearchTerms, Data.Description, role.SketchMode);
        BPLst.ForEach((r) =>
        {
            if (BulletPoint.Length > 0) BulletPoint += ";";
            if (r.IsNotWhiteSpace()) BulletPoint += r;
        });
        SetCommonCell(Pro, "", NullCount);
        Pro.Add(Data.Name);
        Pro.Add(Description);
        Pro.Add(BulletPoint);
        SetCommonCell(Pro, "", 5);
        Pro.Add(Data.Sku);
        Pro.Add(Data.Sku);
        Pro.Add(Item.package_content);
        Pro.Add(Item.package_length);
        Pro.Add(Item.package_width);
        Pro.Add(Item.package_height);
        Pro.Add(Item.package_weight);
        Pro.Add(Data.MainImage);
        SetCommonImage(Pro, Data.Images, 7);
        Pro.Add(Data.Quantity.ToString());
        SetLazadaPrice(Pro, Data.Price, Rate);
        Pro.Add(Item.taxes);
    }

    public static readonly string[] Sizes = new string[]
    {
        "サイズ", "tamaño de las bandas", "talla", "taglia", "size", "尺寸", "尺码", "size name", "lantern size", "tamaño",
        "大きさ", "tamaño del zapato", "material", "band width", "素材", "ring size", "longitud del envoltorio",
        "stretched length", "compatible models", "number of tiers", "belt length", "lunch box capacity",
        "number of compartments", "kid size", "bands size", "cup size", "christmas hat size", "rozmiar", "chima", "사이즈",
        "größe", "la taille", "dimensione", "размер", "tamanho", "size_name", "length", "body color", "wattage",
        "ball diameter", "christmas bell size", "christmas tree height", "尺寸规格"
    };

    public readonly string[] Colors = new string[]
    {
        "颜色", "color", "cor", "colour", "color name", "眼色", "颜", "色", "couleur", "farbe", "colore", "xim", "koloro",
        "색깔", "kolor", "9", "顏色", "color，サイズ", "colour name", "색상", "renk", "Цвет", "צבע", "اللون", "metal color",
        "band color", "handle color", "カラー", "nombre del color"
    };

    public ExcelProduct GetCommonData(ProductsItem p, Dictionary<string, string[]> Item, string Source,
        string Sku, TemplateType TemplateType = TemplateType.BasicData)
    {
        string Color = GetAttribute(p.Values, Item, Colors), Size = GetAttribute(p.Values, Item, Sizes);
        var OtherAttr = GetOtherAttribute(p.Values, Item);
        if (OtherAttr.IsNotNull() && OtherAttr.Length > 0)
        {
            if (Color.IsNullOrEmpty() && Size.IsNullOrEmpty())
            {
                if (OtherAttr.Contains(","))
                {
                    Color = OtherAttr.Before(",");
                    Size = OtherAttr.After(",");
                }
                else Color = OtherAttr;
            }
            else if (Color.IsNullOrEmpty()) Color = OtherAttr;
            else if (Size.IsNullOrEmpty()) Size = OtherAttr;
            else
            {
                if (TemplateType != TemplateType.ZYingThree) Color = $"{Color} {OtherAttr}";
            }
        }

        // 只有系统的默认变体需要过滤，带有颜色尺寸的单变体不过滤
        if (Sku == p.Sku && TemplateType == TemplateType.Amazon
                         && Item.Count == 1 && Item.FirstOrDefault() is var first
                         && first.Value.Length == 1 && first.Value[0] == "default")
        {
            Color = "";
            Size = "";
        }

        ExcelProduct ChildPro = new ExcelProduct(p.Name, p.Description, p.BulletPoint, p.SearchTerms, Color, Size,
            p.Sku,
            p.MainImage, p.Images ?? new List<string>(), p.Quantity, p.Price, p.Cost, p.ProduceCode, null, Source);
        ChildPro.OtherAttribute = OtherAttr;
        ChildPro.DescriptionImages = p.DescriptionImages;
        return ChildPro;
    }

    public string GetAttribute(List<int>? ids, Dictionary<string, string[]> attributes, string[] possibleAttr)
    {
        if (ids is null || ids.Count != attributes.Count)
            return string.Empty;

        for (var i = 0; i < attributes.Count; i++)
        {
            var attribute = attributes.ElementAt(i);
            var key = attribute.Key.ToLower();
            if (attribute.Value.Length > ids[i] && possibleAttr.Contains(key))
            {
                return attribute.Value[ids[i]];
            }
        }

        return string.Empty;
    }

    /// <summary>
    /// 获取所有的属性值
    /// </summary>
    /// <param name="ids"></param>
    /// <param name="attributes"></param>
    /// <returns></returns>
    private string GetOtherAttribute(List<int> ids, Dictionary<string, string[]> attributes)
    {
        if (ids is not { Count: > 0 })
            return string.Empty;

        if (ids.Count != attributes.Count)
            throw new ExportException("变体属性缺失");

        var result = new StringBuilder();
        for (var i = 0; i < attributes.Count; i++)
        {
            var attribute = attributes.ElementAt(i);
            var key = attribute.Key.ToLower();
            if (attribute.Value.Length <= ids[i] || Colors.Contains(key) || Sizes.Contains(key))
                continue;

            if (result.Length > 0) result.Append(',');
            result.Append(attribute.Value[ids[i]]);
        }

        return result.ToString();
    }

    public List<string> GetAttributes(Dictionary<string, string[]> Item, string[] Attri)
    {
        List<string> Attrs = new();
        Item.ForEach((r) =>
        {
            var Key = r.Key.ToLower();
            if (Attri.Contains(Key))
            {
                Attrs = r.Value.ToList();
                return;
            }
        });
        return Attrs;
    }

    public List<string> GetAllAttributes(List<int> IDs, Dictionary<string, string[]> Item)
    {
        List<string> Value = new();
        if (IDs.IsNotNull() && IDs.Count > 0)
        {
            int i = 0;
            Item.ForEach((r) =>
            {
                var Key = r.Key.ToLower();
                if (r.Value.Length - 1 >= IDs[i]) Value.Add(r.Value[IDs[i]]);
                i++;
            });
        }

        return Value;
    }

    public async Task<string> GetMainSalePrice(Prices? Price, List<Unit> Rate)
    {
        return await Task.FromResult((Price is not null && Price.Sale.Max.Value > 0 && Rate.Count > 0)
            ? (Price.Sale.Max.Value / Rate[0].Rate).ToString("F")
            : "0");
    }

    public string GetMainCostPrice(Prices? Price, List<Unit> Rate)
    {
        return (Price is not null && Price.Sale.Max.Value > 0 && Rate.Count > 0)
            ? (Price.Sale.Max.Value / Rate[0].Rate).ToString("F")
            : "0";
    }

    public string GetDataPrice(string Price, List<Unit> Rate, string multiple)
    {
        if (decimal.TryParse(multiple, out var result))
        {
            return GetDataPrice(Price, Rate, result);
        }
        else
        {
            return GetDataPrice(Price, Rate);
        }
    }

    public string GetDataPrice(Money price, string unit)
    {
        return price.To(unit).ToString("0.00");
    }

    public static string GetDataPrice(string price, List<Unit> rate, decimal multiple = 1)
    {
        var result = GetDataPriceDecimal(price, rate, multiple);
        return result.IsSuccess ? result.Value.ToString("0.00") : "";
    }
    
    public static Result<decimal> GetDataPriceDecimal(string price, List<Unit> rate, decimal multiple = 1)
    {
        if (string.IsNullOrWhiteSpace(price))
            return Result.Fail("price为空");

        return Result.Try(() =>
        {
            var priceValue = Convert.ToDecimal(price);
            if (rate.IsNotNull() && rate.Count > 0) priceValue /= Convert.ToDecimal(rate[0].Rate);
            priceValue *= multiple;
            return priceValue;
        });
    }
    
    public static async Task DownloadAsync(string uri, string savePath)
    {
        using var http = new HttpClient();
        using var res = await http.GetAsync(uri);
        await using var str = await res.Content.ReadAsStreamAsync();
        var dir = savePath.Substring(0, savePath.LastIndexOf('/'));
        Directory.CreateDirectory(dir);
        await using var file = System.IO.File.Create(savePath);
        str.Seek(0L, SeekOrigin.Begin);
        await str.CopyToAsync(file);
    }

    public static T? ToDeserialize<T>(string str) => JsonConvert.DeserializeObject<T>(str);
    public static string ToSerialize(object obj) => JsonConvert.SerializeObject(obj);
}