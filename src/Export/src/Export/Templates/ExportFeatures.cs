﻿namespace ERP.Export.Templates;

/// <summary>
/// 导出配置项
/// </summary>
public class ExportFeatures
{
    /// <summary>
    /// 是否填充amazon表格中的b2c相关字段
    /// </summary>
    public bool ExportAmazonB2CEnabled { get; set; }
}