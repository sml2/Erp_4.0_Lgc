﻿using ERP.Export.ExportCommon;
using ERP.Models.Export;
using ERP.Export.Templates;
using System.ComponentModel.DataAnnotations;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class LazadaBeauty : ExcelBase<LazadaBeauty, LazadaBeauty.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.Lazada_Beauty;

    public override string Name => "Lazada Beauty表";
    public LazadaBeauty(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }

    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
    {
        List<List<string>> Product = new List<List<string>>();
        ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
        if (Item is null)
        {
            _Logger.LogInformation($"{Name} GetData ParameterItem Is Null");
            return null!;
        }
        string Sku = Data.Sku;
        if (Data.Products.IsNull() || Data.Products.Count == 0)
        {
            string Price = "0";
            string Cost = "0";
            if (Data.Price is not null && Data.Price.Sale.Max.Value > 0) Price = Data.Price.Sale.Max.Value.ToString();//sale
            if (Data.Price is not null && Data.Price.Cost.Max.Value > 0) Price = Data.Price.Sale.Max.Value.ToString();//cost
            ExcelProduct MainPro = new ExcelProduct(Data.Name, Data.Description, Data.BulletPoint, Data.SearchTerms, "", "", Sku, "", null, Data.Quantity, Price, Cost, "", null, Data.Source);
            var MainExcelPro = GetProduct(MainPro, Item, role, Rate);
            if (MainExcelPro.IsNotNull()) Product.Add(MainExcelPro);
        }
        else
        {
            //子产品
            foreach (var p in Data.Products)
            {
                ExcelProduct ChildPro = _Common.GetCommonData(p, Data.Attribute, Data.Source, Sku);
                var ChileExcelPro = GetProduct(ChildPro, Item, role, Rate);
                if (ChileExcelPro.IsNotNull()) Product.Add(ChileExcelPro);
            }
        }
        var Dic = new Dictionary<int, List<List<string>>>();
        Dic.Add(0, Product);
        return Dic;
    }
    private List<string> GetProduct(ExcelProduct Data, ParameterItem Item, ExcleRole role, List<Unit> Rate)
    {
        List<string> Pro = new List<string>();
        Pro.Add(Item.primary_category);
        Pro.Add(Item.model);
        Pro.Add(Item.brand);
        string Color = Data.Color;
        if (Color.IsNullOrEmpty()) Color = Item.color_family;
        Pro.Add(Color);
        _Common.GetLazadaCommonCell(Pro, 45, Data, Item, role, Rate);
        ProductExportHelper.SetCommonCell(Pro, "", 2);
        Pro.Add("Others");
        Pro.Add(Item.waterproof_function);
        ProductExportHelper.SetCommonCell(Pro, "", 3);
        Pro.Add(Item.fragrance_family);
        Pro.Add(Item.fragrance_family);
        Pro.Add(Item.units);
        return Pro;
    }

    public class ParameterItem : IParameterItem //: JsonObject<ParameterItem>, IParameterItem
    {
        [Required]
        public string primary_category { get; set; } = null!;

        public string model { get; set; }

        public string brand { get; set; }

        public string color_family { get; set; }

        public string package_content { get; set; }

        public string package_length { get; set; }

        public string package_width { get; set; }

        public string package_height { get; set; }

        public string package_weight { get; set; }

        public string taxes { get; set; }

        public string waterproof_function { get; set; }
        public string fragrance_family { get; set; }
        public string units { get; set; }
    }
}
