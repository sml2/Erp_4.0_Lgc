﻿using ERP.Export.ExportCommon;
using ERP.Export.Templates;
using ERP.Models.Export;
using System.ComponentModel.DataAnnotations;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class Catch : ExcelBase<Catch, Catch.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.Catch;
    public override string Name => "Catch导出模板";


    public Catch(IServiceProvider serviceProvider) : base(serviceProvider)
    {
    }

    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem data, ExcleRole role,
        string parameter, List<Unit> rate)
    {
        var product = new List<List<string?>>();
        var item = ProductExportHelper.ToDeserialize<ParameterItem>(parameter);
        if (item is null)
        {
            _Logger.LogInformation("{Name}ParameterItem is Null", Name);
            return null!;
        }

        string sku = data.Sku;
        // sizeChartImage留空
        // var sizeChartImage = data.ProductImages.LastOrDefault();
        var sizeChartImage = "";

        // if (data.Products.Count == 1 && data.Products[0].Sku == sku)
        // {
        // }
        // else
        // {
        //     var images = data.Products.First().Images;
        //     var pro = new ExcelProduct(data.Name, data.Description, data.BulletPoint, data.SearchTerms, "", "",
        //         sku, images?.FirstOrDefault(), images, data.Quantity, "", "", string.Empty, null, string.Empty);
        //     var mainPro = GetProduct(pro, item, role, rate, sku, sizeChartImage, true);
        //     if (mainPro.IsNotNull()) product.Add(mainPro);
        // }

        //子产品
        if (data.Products.Count > 0)
        {
            var products = new List<ExcelProduct>();
            var skuMap = new Dictionary<string, string>();
            foreach (var p in data.Products)
            {
                var childPro =
                    _Common.GetCommonData(p, data.Attribute, data.Source, sku, TemplateType.Catch);
                products.Add(childPro);
            }

            // 将变体根据颜色进行排序，并且给每种颜色赋予一个新的sku
            if (!string.IsNullOrEmpty(products.First().Size))
            {
                products = products.OrderBy(p => p.Color, StringComparer.Ordinal).ToList();
                var index = 1;
                foreach (var pro in products)
                {
                    if (skuMap.TryAdd(pro.Color, $"{sku}-{index:0000}"))
                        index++;
                }
            }

            foreach (var childPro in products)
            {
                // 当size有值时才需要父sku
                var needSku = !string.IsNullOrEmpty(childPro.Size);
                var chileExcelPro = GetProduct(childPro, item, role, rate,
                    needSku ? (skuMap.TryGetValue(childPro.Color, out var colorSku) ? colorSku : childPro.Color) : "",
                    sizeChartImage, false);
                if (chileExcelPro.IsNotNull()) product.Add(chileExcelPro);
            }
        }

        var dic = new Dictionary<int, List<List<string?>>>();
        dic.Add(0, product);
        return dic;
    }

    private List<string?> GetProduct(ExcelProduct data, ParameterItem item, ExcleRole role,
        List<Unit> rate, string sku, string? sizeChartImage, bool isMain)
    {
        //描述
        string description = _Common.GetDescription(data.Description, string.Join(";", data.BulletPoint),
            data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, data.Images);
        //卖点
        var bulletPoint = _Common.GetBulletPoint(data.BulletPoint, data.SearchTerms, data.Description, role.SketchMode);
        //关键字
        var keyword = _Common.GetKeywords(data.BulletPoint, data.SearchTerms, data.Description, role.KeywordMode)
            .Where(s => !string.IsNullOrWhiteSpace(s)).ToList();
        //主产品
        var pro = new List<string?>();
        pro.Add(item.Category);
        pro.Add(data.Sku);
        pro.Add($"{data.Name}{(string.IsNullOrEmpty(data.Color) ? "" : $"-{data.Color}")}");
        pro.Add(data.ProduceCode); //UPC Value
        var produceType = "";
        if (data.ProduceCode is { Length: 12 }) produceType = "UPC";
        else if (data.ProduceCode is { Length: 13 }) produceType = "EAN";
        pro.Add(produceType); //UPC EAN
        pro.Add(description);
        pro.Add(item.Brand);
        pro.Add("New");
        pro.Add("1");
        pro.Add(data.Color);
        pro.Add(string.Join('|', keyword));
        pro.Add(item.Gender);
        pro.Add(item.Material);
        pro.Add(sku);
        pro.Add(data.Color);
        pro.Add(data.Size);
        pro.Add(sizeChartImage);
        pro.Add(data.MainImage);
        ProductExportHelper.SetCommonImage(pro, data.Images, 9);
        ProductExportHelper.SetCommonCell(pro, "", 10);
        pro.Add(item.Weight);
        pro.Add(item.WeightUnit);
        pro.Add(item.Width);
        pro.Add(item.WidthUnit);
        pro.Add(item.Length);
        pro.Add(item.LengthUnit);
        pro.Add(item.Height);
        pro.Add(item.HeightUnit);
        pro.Add(item.ModelNumber);
        pro.Add(item.Season);
        pro.Add(""); // adult
        pro.Add(""); // Restriction
        pro.Add(""); // Commercial use
        pro.Add(""); // Gift Type
        pro.Add("No"); // Contains Button Cell Batteries
        pro.Add(""); // Clearance
        pro.Add(""); // Clearance Stream
        pro.Add(""); // Bedding Size
        pro.Add(""); // Electrical Equipment Level
        pro.Add(""); // Equipment Number
        pro.Add(""); // Cushion Type
        pro.Add(""); // Rug Shape
        pro.Add(""); // Framed
        pro.Add(""); // Art Subject
        pro.Add(""); // Cookware Features
        pro.Add(""); // Pillow Size
        pro.Add(""); // Number of Pieces
        pro.Add(""); // Knife Type
        pro.Add(""); // Knife Accessory Type
        pro.Add(""); // Cutlery Type
        pro.Add(""); // Dinner Set Type
        pro.Add(""); // Plate Type
        pro.Add(""); // Coffee & Tea Set Type
        pro.Add(data.Sku);
        pro.Add(data.Sku);
        pro.Add("SHOP_SKU");
        pro.Add("");
        pro.Add("");
        string price = ProductExportHelper.GetDataPrice(data.Price, rate);
        pro.Add(price); //Standard Price
        pro.Add("");
        if (isMain) pro.Add("");
        else pro.Add(data.Quantity.ToString());
        pro.Add(""); // Minimum Quantity Alert
        pro.Add("New");
        pro.Add("");
        pro.Add("");
        pro.Add("FREE");
        pro.Add("");
        pro.Add("");
        pro.Add("");
        pro.Add(item.LeadTimeToShip);
        pro.Add("");
        pro.Add("1");
        pro.Add("");
        pro.Add("true"); // Club Catch eligible
        pro.Add("10"); // tax-au
        pro.Add("");
        pro.Add(item.LocationCountry);
        pro.Add(item.ShippedFromPostCode);

        return pro;
    }

    public class ParameterItem : CommonParameter
    {
        public string? unit { get; set; } = "AUD";

        [Required] public string Category { get; set; } = null!;

        public string? Brand { get; set; } = "unbranded";
        public string? Gender { get; set; }
        public string? Material { get; set; }
        public string? Weight { get; set; }
        public string? WeightUnit { get; set; }
        public string? Width { get; set; }
        public string? WidthUnit { get; set; }
        public string? Length { get; set; }
        public string? LengthUnit { get; set; }
        public string? Height { get; set; }
        public string? HeightUnit { get; set; }
        public string? LeadTimeToShip { get; set; }
        public string? LocationCountry { get; set; }
        public string? ShippedFromPostCode { get; set; }
        public string? ModelNumber { get; set; }
        public string? Season { get; set; }
    }
}