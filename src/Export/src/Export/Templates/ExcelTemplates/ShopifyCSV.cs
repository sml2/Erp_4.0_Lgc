﻿using ERP.Export.ExportCommon;
using ERP.Models.Export;
using ERP.Export.Templates;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class ShopifyCSV : ExcelBase<ShopifyCSV, ShopifyCSV.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.ShopifyCSV;
    public override string Name => "Shopify CSV表";

    public ShopifyCSV(IServiceProvider serviceProvider) : base(serviceProvider)
    {
    }

    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem data, ExcleRole role,
        string parameter, List<Unit> rate)
    {
        var item = ProductExportHelper.ToDeserialize<ParameterItem>(parameter);
        if (item is null)
        {
            _Logger.LogInformation("{Name} GetData Method ParameterItem Is Null", Name);
            return null!;
        }

        var product = new List<List<string>>();
        var sku = data.Sku;

        if (data.Products.IsNull() || data.Products.Count == 0)
        {
            var price = await _Common.GetMainSalePrice(data.Price, rate);
            var cost = _Common.GetMainCostPrice(data.Price, rate);
            var mainPro = new ExcelProduct(data.Name, data.Description, data.BulletPoint, data.SearchTerms, "", "", sku,
                "", null, data.Quantity, price, cost, "", null, data.Source);
            var mainExcelPro = GetProduct(mainPro, item, rate, role, 1, true);
            if (mainExcelPro.IsNotNull()) product.Add(mainExcelPro);
        }
        else
        {
            var i = 0;
            foreach (var products in SplitProduct(data.Attribute, data.Products))
            {
                var tempSku = sku;
                var name = data.Name;
                if (i > 0)
                {
                    tempSku = $"{sku}-{i}";
                    foreach (var p in products)
                    {
                        p.Name = $"{p.Name} {i}";
                    }

                    name = $"{name} {i}";
                }

                var imgIndex = 1;
                var allImages = FillSplitProduct(products, role, rate, tempSku, item, ref imgIndex, product,
                    data.Attribute, data.Source);
                AppendImages(name, ref imgIndex, product, allImages);
                i++;
            }
        }

        var dic = new Dictionary<int, List<List<string>>>();
        dic.Add(0, product);
        return dic;
    }

    private IEnumerable<ProductsItem[]> SplitProduct(Dictionary<string, string[]> attributes,
        List<ProductsItem> products)
    {
        if (products.Count <= 100)
        {
            yield return products.ToArray();
            yield break;
        }

        if (attributes.Count == 1)
        {
            foreach (var item in products.Chunk(100))
            {
                yield return item;
            }
        }

        // 计算出每个属性分成每批有多少个值
        int[] CalcBatches(Dictionary<string, string[]> innerAttr)
        {
            var orders = new int[innerAttr.Count];
            for (int i = 0; i < innerAttr.Count; i++)
            {
                var attribute = innerAttr.ElementAt(i);
                if (!ProductExportHelper.Sizes.Contains(attribute.Key))
                {
                    orders[i] = i;
                    continue;
                }

                for (int j = i; j > 0; j--)
                {
                    orders[j] = orders[j - 1];
                }

                orders[0] = i;
            }
            
            var batches = new int[innerAttr.Count];
            var total = 100m;
            for (int i = 0; i < innerAttr.Count; i++)
            {
                var attribute = innerAttr.ElementAt(orders[i]);
                batches[orders[i]] = Math.Min((int)total, attribute.Value.Length);
                total = Math.Max(Math.Floor(total / attribute.Value.Length), 1);
            }

            return batches;
        }

        var batches = CalcBatches(attributes);

        IEnumerable<List<List<int>>> ChunkRecursive(Dictionary<string, string[]> attr, int[] bats, int start = 0)
        {
            var count = attr.ElementAt(start).Value.Length;
            var round = Math.Ceiling((decimal)count / bats[start]);
            foreach (var chunk in Enumerable.Range(0, count).Chunk(bats[start]))
            {
                var result = new List<List<int>>() { chunk.ToList() };
                if (start >= bats.Length - 1)
                {
                    yield return result;
                    continue;
                }

                foreach (var append in ChunkRecursive(attr, bats, start + 1))
                {
                    var clone = new List<List<int>>();
                    clone.AddRange(result);
                    clone.AddRange(append);
                    yield return clone;
                }
            }
        }

        foreach (var chunk in ChunkRecursive(attributes, batches))
        {
            var result = products.AsEnumerable();
            for (int i = 0; i < chunk.Count; i++)
            {
                var items = chunk[i];
                var i1 = i;
                result = result.Where(m => items.Contains(m.Values[i1]));
            }

            yield return result.ToArray();
        }
    }

    private List<string> FillSplitProduct(ProductsItem[] products, ExcleRole role, List<Unit> rate, string sku,
        ParameterItem item,
        ref int imgIndex, List<List<string>> product, Dictionary<string, string[]> attributes, string source)
    {
        var allImages = new List<string>();
        for (var index = 0; index < products.Length; index++)
        {
            var p = products[index];
            var childPro = _Common.GetCommonData(p, attributes, source, sku);
            var chileExcelPro = GetProduct(childPro, item, rate, role, imgIndex, index == 0);
            if (chileExcelPro.IsNotNull()) product.Add(chileExcelPro);
            if (p.Images.IsNotNull() && p.Images.Count > 0)
            {
                foreach (var img in p.Images)
                {
                    if (!allImages.Contains(img)) allImages.Add(img);
                }

                allImages = allImages.Distinct().ToList();
            }

            imgIndex++;
        }

        return allImages;
    }

    private static void AppendImages(string name, ref int imgIndex, List<List<string>> product, List<string> allImages)
    {
        foreach (var img in allImages)
        {
            List<string> proImg = new List<string>();
            proImg.Add(name.Replace(" ", "-"));
            ProductExportHelper.SetCommonCell(proImg, "", 24);
            proImg.Add(img);
            proImg.Add(imgIndex.ToString());
            product.Add(proImg);
            imgIndex++;
        }
    }

    private List<string> GetProduct(ExcelProduct data, ParameterItem item, List<Unit> rate, ExcleRole role, int index,
        bool isMain)
    {
        //描述
        var description = _Common.GetDescription(data.Description, string.Join(";", data.BulletPoint),
                data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, data.Images)
            .Replace("\r\n", "<br>")
            .Replace("\n", "<br>");
        description = RemoveImgTag(description);
        description += string.Join("", data.Images.Select(i => $"<img src=\"{i}\" />"));
        //卖点
        var bulletPoint = _Common.GetBulletPoint(data.BulletPoint, data.SearchTerms, data.Description, role.SketchMode);
        //关键字
        var keyword = _Common.GetKeywords(data.BulletPoint, data.SearchTerms, data.Description, role.KeywordMode);
        var pro = new List<string>();
        pro.Add(data.Name.Replace(" ", "-"));
        pro.Add(isMain ? data.Name : string.Empty);
        pro.Add(isMain ? description : string.Empty);
        pro.Add(isMain ? item.vendor : string.Empty);
        pro.Add(isMain ? item.ProductCategory : string.Empty);
        pro.Add(isMain ? item.protype : string.Empty);
        pro.Add(isMain ? string.Join(" ", keyword) : string.Empty);
        pro.Add(isMain ? "TRUE" : string.Empty);
        Dictionary<string, string> attrDic = new Dictionary<string, string>();
        if (data.Color.IsNotWhiteSpace()) attrDic.Add("Color", data.Color);
        if (data.Size.IsNotWhiteSpace()) attrDic.Add("Size", data.Size);
        attrDic.ForEach((r) =>
        {
            pro.Add(isMain ? r.Key : string.Empty);
            pro.Add(r.Value);
        });
        if (attrDic.Count < 3)
        {
            for (var i = 1; i <= 3 - attrDic.Count; i++)
            {
                ProductExportHelper.SetCommonCell(pro, "", 2);
            }
        }

        pro.Add(data.Sku);
        pro.Add("0.2");
        pro.Add("shopify");
        pro.Add("2000");
        pro.Add("continue");
        pro.Add("manual");
        var price = ProductExportHelper.GetDataPriceDecimal(data.Price, rate);
        pro.Add(price.ValueOrDefault.ToString("0.00"));
        pro.Add((price.ValueOrDefault / 0.8m).ToString("0.00"));
        pro.Add("TRUE");
        pro.Add("TRUE");
        pro.Add("");
        pro.Add(data.MainImage ?? string.Empty); //
        pro.Add(index.ToString());
        pro.Add("");
        pro.Add(isMain ? "FALSE" : string.Empty);
        ProductExportHelper.SetCommonCell(pro, "", 15);
        pro.Add(data.MainImage ?? string.Empty);
        pro.Add("kg");
        ProductExportHelper.SetCommonCell(pro, "", 4);
        pro.Add(isMain ? "active" : string.Empty); // status active draft archived
        return pro;
    }

    private static string RemoveImgTag(string html)
    {
        var result = new StringBuilder();
        var ptr = 0;
        while (true)
        {
            var start = html.IndexOf("<img ", ptr, StringComparison.OrdinalIgnoreCase);
            if (start == -1)
            {
                result.Append(html.AsSpan(ptr));
                break;
            }

            result.Append(html.AsSpan(ptr, start - ptr));
            var end = html.IndexOf(">", start, StringComparison.Ordinal);
            if (end == -1)
                break; // 缺少结束标签。应该不会执行到这里。
            ptr = end + 1;
        }

        return result.ToString();
    }

    public class ParameterItem
    {
        public string unit { get; set; } = null!;
        [Required] public string vendor { get; set; } = null!;
        public string ProductCategory { get; set; } = null!;
        [Required] public string protype { get; set; } = null!;
    }
}