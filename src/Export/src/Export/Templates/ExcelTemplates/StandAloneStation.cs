﻿using ERP.Export.ExportCommon;
using ERP.Models.Export;
using ERP.Export.Templates;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class StandAloneStation : ExcelBase<StandAloneStation, StandAloneStation.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.StandAloneStation;
    public override string Name => "独立站表格";

    public StandAloneStation(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }
    
    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
    {
        List<List<string>> Product = new List<List<string>>();
        ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
        if (Item.IsNull())
        {
            _Logger.LogInformation($"{Name} GetData Method ParameterItem Is Null");
            return null;
        }
        string Sku = Data.Sku;
        //
        //描述
        List<string> AllImages = new List<string>();
        foreach (var pro in Data.Products)
        {
            if (!AllImages.Contains(pro.MainImage)) AllImages.Add(pro.MainImage);
            pro.Images.ForEach((img) =>
            {
                if (!AllImages.Contains(img)) AllImages.Add(img);
            });
        }
        string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, AllImages);
        //卖点
        var BulletPoint = _Common.GetBulletPoint(Data.BulletPoint, Data.SearchTerms, Data.Description, role.SketchMode).Where((r) => r.IsNotWhiteSpace()).ToList();
        //关键字
        var Keyword = _Common.GetKeywords(Data.BulletPoint, Data.SearchTerms, Data.Description, role.KeywordMode).Where((r) => r.IsNotWhiteSpace()).ToList();
        //主产品
        List<List<string>> Pro = new List<List<string>>();
        Pro.Add(new List<string> { "" });
        Pro.Add(new List<string> { Data.Name });
        Pro.Add(new List<string> { Item.ProductCategory });
        Pro.Add(new List<string> { Item.ProductCategories });
        Pro.Add(new List<string> { Item.ProductNumber });
        Pro.Add(new List<string> { Data.Sku });
        string Cost = _Common.GetMainCostPrice(Data.Price, Rate);
        Pro.Add(new List<string> { Cost });
        string Price = await _Common.GetMainSalePrice(Data.Price, Rate);
        Pro.Add(new List<string> { Price });
        Pro.Add(new List<string> { Cost });
        Pro.Add(new List<string> { Item.CustomAddress });
        Pro.Add(AllImages);
        var Attributes = Data.Attribute;
        Pro.Add(Attributes.Keys.ToList());
        List<string> AllAttr = new List<string>();
        foreach (var item in Attributes)
        {
            AllAttr.Add($"{item.Key}[{string.Join(",", item.Value)}]");
        }
        Pro.Add(AllAttr);
        Pro.Add(Attributes.Keys.ToList());
        Pro.Add(AllAttr);
        List<string> Attr = new List<string>();
        List<string> ProSku = new List<string>();
        List<string> IsMarkup = new List<string>();
        List<string> ProPrice = new List<string>();
        List<string> inventory = new List<string>();
        List<string> AttributeWeights = new List<string>();
        List<string> ProImages = new List<string>();
        if (Data.Products.IsNotNull() && Data.Products.Count > 0)
        {
            foreach (var p in Data.Products)
            {
                string AttrStr = "";
                for (int i = 0; i < p.Values.Count - 1; i++)
                {
                    AttrStr += $"{Attributes.Keys.ToList()[i]}[{Attributes.Values.ToList()[i][p.Values[i]]}]";
                }
                Attr.Add(AttrStr);
                ProSku.Add(p.Sku);
                IsMarkup.Add(Item.Markup);
                ProPrice.Add(ProductExportHelper.GetDataPrice(p.Price, Rate));
                inventory.Add(p.Quantity.ToString());
                AttributeWeights.Add(Item.AttributeWeight);
                ProImages.Add(string.Join(",", p.Images));
            }
        }
        Pro.Add(Attr);
        Pro.Add(ProSku);
        Pro.Add(IsMarkup);
        Pro.Add(ProPrice);
        Pro.Add(inventory);
        Pro.Add(AttributeWeights);
        Pro.Add(ProImages);
        Pro.Add(new List<string> { Item.PropertyPriceSwitch });
        Pro.Add(new List<string> { Item.AttributeCombinationSwitch });
        Pro.Add(new List<string> { Item.Weight });
        Pro.Add(new List<string> { Item.Volume });
        Pro.Add(new List<string> { Item.Bulky });
        Pro.Add(new List<string> { Item.MOQ });
        Pro.Add(new List<string> { Item.MaximumPurchaseAmount });
        Pro.Add(new List<string> { Item.TotalInventory });
        Pro.Add(new List<string> { Item.OutofStock });
        Pro.Add(new List<string> { Item.OffShelf });
        Pro.Add(new List<string> { Item.Scheduled });
        Pro.Add(new List<string> { Item.TimedListingTime });
        Pro.Add(new List<string> { Item.FreeShipping });
        Pro.Add(new List<string> { Item.NewProduct });
        Pro.Add(new List<string> { Item.Selling });
        Pro.Add(new List<string> { Item.BestSelling });
        Pro.Add(new List<string> { Item.HomePageDisplay });
        Pro.Add(new List<string> { Data.Name });
        Pro.Add(new List<string> { string.Join(",", Keyword) });
        Pro.Add(new List<string> { Description });
        Pro.Add(new List<string> { Description });
        Pro.Add(new List<string> { Description });
        Pro.Add(new List<string> { Item.Sorting });
        Pro.Add(new List<string> { Data.Source });
        bool HasData = false;
        int Index = 0;
        do
        {
            HasData = false;
            List<string> Line = new List<string>();
            for (int i = 0; i <= Pro.Count - 1; i++)
            {
                var Value = Pro[i];
                if (Value.Count - 1 >= Index && Value[Index].IsNotWhiteSpace())
                {
                    Line.Add(Value[Index]);
                    HasData = true;
                }
                else Line.Add("");
            }
            if (HasData) Product.Add(Line);
            Index++;
        } while (HasData);
        var Dic = new Dictionary<int, List<List<string>>>();
        Dic.Add(0, Product);
        return Dic;
    }
    public class ParameterItem //: JsonObject<ParameterItem>
    {
        /// <summary>
        /// 产品分类
        /// </summary>
        public string ProductCategory { get; set; }
        /// <summary>
        /// 产品多分类
        /// </summary>
        public string ProductCategories { get; set; }
        /// <summary>
        /// 产品编号
        /// </summary>
        public string ProductNumber { get; set; }
        /// <summary>
        /// 自定义地址
        /// </summary>
        public string CustomAddress { get; set; }
        /// <summary>
        /// 加价
        /// </summary>
        public string Markup { get; set; }
        /// <summary>
        /// 重量
        /// </summary>
        public string AttributeWeight { get; set; }
        /// <summary>
        /// 属性价格开关
        /// </summary>
        public string PropertyPriceSwitch { get; set; }
        /// <summary>
        /// 属性组合开关
        /// </summary>
        public string AttributeCombinationSwitch { get; set; }
        /// <summary>
        /// 重量
        /// </summary>
        public string Weight { get; set; }
        /// <summary>
        /// 体积
        /// </summary>
        public string Volume { get; set; }
        /// <summary>
        /// 体积重
        /// </summary>
        public string Bulky { get; set; }
        /// <summary>
        /// 起订量
        /// </summary>
        public string MOQ { get; set; }
        /// <summary>
        /// 最大购买量
        /// </summary>
        public string MaximumPurchaseAmount { get; set; }
        /// <summary>
        /// 总库存
        /// </summary>
        public string TotalInventory { get; set; }
        /// <summary>
        /// 脱销状态
        /// </summary>
        public string OutofStock { get; set; }
        /// <summary>
        /// 下架
        /// </summary>
        public string OffShelf { get; set; }
        /// <summary>
        /// 定时上架
        /// </summary>
        public string Scheduled { get; set; }
        /// <summary>
        /// 定时上架时间
        /// </summary>
        public string TimedListingTime { get; set; }
        /// <summary>
        /// 免运费
        /// </summary>
        public string FreeShipping { get; set; }
        /// <summary>
        /// 新品
        /// </summary>
        public string NewProduct { get; set; }
        /// <summary>
        /// 热卖
        /// </summary>
        public string Selling { get; set; }
        /// <summary>
        /// 畅销
        /// </summary>
        public string BestSelling { get; set; }
        /// <summary>
        /// 首页显示
        /// </summary>
        public string HomePageDisplay { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public string Sorting { get; set; }
    }
}
