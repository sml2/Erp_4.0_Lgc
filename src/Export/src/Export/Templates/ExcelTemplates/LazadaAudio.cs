﻿using ERP.Export.ExportCommon;
using ERP.Models.Export;
using ERP.Export.Templates;
using System.ComponentModel.DataAnnotations;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class LazadaAudio : ExcelBase<LazadaAudio>
{
    public override TemplateType TemplateType => TemplateType.Lazada_Audio;
    public override string Name => "Lazada Audio表";
    public LazadaAudio(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }

    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
    {
        var Product = new List<List<string?>>();
        ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
        if (Item.IsNull())
        {
            _Logger.LogInformation($"{Name} GetData ParameterItem Is Null");
            return null!;
        }
        string Sku = Data.Sku;
        if (Data.Products.IsNull() || Data.Products.Count == 0)
        {
            string Price = "0";
            string Cost = "0";
            if (Data.Price is not null && Data.Price.Sale.Max.Value > 0) Price = Data.Price.Sale.Max.Value.ToString();//sale
            if (Data.Price is not null && Data.Price.Cost.Max.Value > 0) Price = Data.Price.Sale.Max.Value.ToString();//cost
            ExcelProduct MainPro = new ExcelProduct(Data.Name, Data.Description, Data.BulletPoint, Data.SearchTerms, "", "", Sku, "", null, Data.Quantity, Price, Cost, "", null, Data.Source);
            var MainExcelPro = GetProduct(MainPro, Item, role, Rate);
            if (MainExcelPro.IsNotNull()) Product.Add(MainExcelPro);
        }
        else
        {
            //子产品
            foreach (var p in Data.Products)
            {
                ExcelProduct ChildPro = _Common.GetCommonData(p, Data.Attribute, Data.Source, Sku);
                var ChileExcelPro = GetProduct(ChildPro, Item, role, Rate);
                if (ChileExcelPro.IsNotNull()) Product.Add(ChileExcelPro);
            }
        }
        Dictionary<int, List<List<string?>>>? Dic = new Dictionary<int, List<List<string?>>>();
        Dic.Add(0, Product);
        return Dic;
    }
    private List<string?> GetProduct(ExcelProduct Data, ParameterItem Item, ExcleRole role, List<Unit> Rate)
    {
        var Pro = new List<string?>();
        Pro.Add(Item.primary_category);
        Pro.Add(Item.model);
        Pro.Add(Item.brand);
        string Color = Data.Color;
        if (Color.IsNullOrEmpty()) Color = Item.color_family;
        Pro.Add(Color);
        _Common.GetLazadaCommonCell(Pro, 39, Data, Item, role, Rate);
        Pro.Add("");
        Pro.Add(Item.cable_connection);
        return Pro;
    }
    public class ParameterItem : IParameterItem//: JsonObject<ParameterItem>, IParameterItem
    {
        [Required]
        public string primary_category { get; set; } = null!;

        public string? model { get; set; }

        public string? brand { get; set; }

        public string? color_family { get; set; }

        public string? package_content { get; set; }

        public string? package_length { get; set; }

        public string? package_width { get; set; }

        public string? package_height { get; set; }

        public string? package_weight { get; set; }

        public string? taxes { get; set; }

        public string? cable_connection { get; set; }
    }
}
