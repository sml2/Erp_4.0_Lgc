﻿using System.ComponentModel.DataAnnotations;
using ERP.Export.ExportCommon;
using ERP.Export.Templates;
using ERP.Export.V4_Export.Interfaces;
using ERP.Models.Export;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace ERP.Export.V4_Export.ExcelTemplates;

public abstract class ExcelBase<ITMode> : ITemplate
{
    protected readonly ProductExportHelper _Common;
    protected readonly ILogger<ITMode> _Logger;
    public virtual TemplateType TemplateType { get; set; }
    public abstract string Name { get; }
    public virtual Dictionary<int, int> StartRowIndex { get; set; }
    public ExcelBase(IServiceProvider serviceProvider)
    {
        _Common = serviceProvider.GetRequiredService<ProductExportHelper>();
        var loggerFactory = serviceProvider.GetRequiredService<ILoggerFactory>();
        _Logger = loggerFactory.CreateLogger<ITMode>();
    }

    public virtual Task<bool> Verify(string parameter)
    {
        throw new NotImplementedException();
    }

    public virtual Task<Dictionary<string, List<List<string>>>> GetHead(string parameter) => _Common.GetHead(TemplateType);
    
    public virtual Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem data, ExcleRole role, string parameter, List<Unit> rate)
    {
        throw new NotImplementedException();
    }
    public virtual Task<Dictionary<int, List<List<string?>>>?> GetOhterData(DataItem data, ExcleRole role, string parameter, List<Unit> rate)
    {
        throw new NotImplementedException();
    }

    public virtual bool TryValidate(string parameter, out List<ValidationResult> results)
    {
        results = new List<ValidationResult>();

        var obj = JsonConvert.DeserializeObject(parameter);
        if (obj is null)
        {
            results.Add(new ValidationResult("参数不能为null"));
            return false;
        }

        return true;
    }
}


public abstract class ExcelBase<TTemplate, TParameter> : ExcelBase<TTemplate>
{
    protected ExcelBase(IServiceProvider serviceProvider) : base(serviceProvider)
    {
    }


    public override bool TryValidate(string parameter, out List<ValidationResult> results)
    {
        results = new List<ValidationResult>();
        var obj = JsonConvert.DeserializeObject<TParameter>(parameter);
        if (obj is null)
        {
            results.Add(new ValidationResult("参数不能为null"));
            return false;
        }

        var context = new ValidationContext(obj);

        if (!Validator.TryValidateObject(obj, context, results))
        {
            return false;
        }

        return true;
    }
}