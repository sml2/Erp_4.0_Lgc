﻿using ERP.Export.ExportCommon;
using ERP.Models.Export;
using ERP.Export.Templates;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class DianXiaoMi : ExcelBase<DianXiaoMi, DianXiaoMi.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.Dianxiaomi;
    public override string Name => "店小秘";

    public DianXiaoMi(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }

    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
    {
        var Product = new List<List<string?>>();
        ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
        if (Item is null)
        {
            _Logger.LogInformation($"{Name} GetData ParameterItem Is Null");
            return null!;
        }
        string Sku = Data.Sku;
        //子产品
        List<string> Colors = new List<string>() { "白色", "黄色", "红色", "酒红色", "紫色", "浅灰色", "绿色", "蓝色", "深蓝色", "黑色", "浅绿色", "浅黄色", "紫罗兰", "桔色", "透明", "多色", "褐色", "深灰色", "深紫色", "粉红色", "巧克力色", "深卡其布色", "军绿色", "天蓝色" };
        int Index = 1;
        int CIndex = 0;
        foreach (var p in Data.Products)
        {
            if (CIndex > Colors.Count - 1) CIndex = 0;
            string Color = Colors[CIndex];
            string Size = _Common.GetAttribute(p.Values, Data.Attribute, ProductExportHelper.Sizes);
            ExcelProduct ChildPro = new ExcelProduct(p.Name, p.Description, p.BulletPoint, p.SearchTerms, Color, Size, p.Sku, p.MainImage, p.Images, p.Quantity, p.Price.ToString(), p.Cost.ToString(), p.ProduceCode, null, Data.Source);
            var ChileExcelPro = await GetProduct(ChildPro, Item, role, Rate, Index);
            if (ChileExcelPro.IsNotNull()) Product.Add(ChileExcelPro);
            Index++; CIndex++;
        }
        Dictionary<int, List<List<string?>>>? Dic = new Dictionary<int, List<List<string?>>>();
        Dic.Add(0, Product);
        return Dic;
    }
    private Task<List<string?>> GetProduct(ExcelProduct Data, ParameterItem Item, ExcleRole role, List<Unit> Rate, int Index)
    {
        //描述
        string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, Data.Images);
        //主产品
        var Pro = new List<string?>();
        Pro.Add(Item.category);
        Pro.Add(Data.Sku);
        Pro.Add(Data.Color);
        Pro.Add(Index.ToString());//颜色自定义名称(纯数字)
        Pro.Add(Data.MainImage);//Main Image
        Pro.Add(Data.Size);//Size
        Pro.Add("");
        string Price = ProductExportHelper.GetDataPrice(Data.Price, Rate);
        Pro.Add(Price);//Standard Price
        Pro.Add(Data.Quantity.ToString());
        Pro.Add(Data.ProduceCode);//UPC Value
        Pro.Add("件/个");
        Pro.Add("单件出售");
        ProductExportHelper.SetCommonCell(Pro, "", 3);
        Pro.Add(Data.Name);
        ProductExportHelper.SetCommonImage(Pro, Data.Images, 6);
        Pro.Add("支付减库存");
        Pro.Add("7");
        Pro.Add("30");
        Pro.Add(Description);
        Pro.Add(Item.weight);
        Pro.Add(Item.length);
        Pro.Add(Item.width);
        Pro.Add(Item.height);
        Pro.Add(Item.template);
        Pro.Add(Item.feetemp);
        Pro.Add(Item.servicetemp);
        Pro.Add(Item.proteam);
        Pro.Add(Data.Source);
        return Task.FromResult(Pro);
    }
    public class ParameterItem //: JsonObject<ParameterItem>
    {
        public string? category { get; set; }

        public string? weight { get; set; }

        public string? length { get; set; }

        public string? width { get; set; }

        public string? height { get; set; }

        public string? template { get; set; }

        public string? feetemp { get; set; }

        public string? servicetemp { get; set; }

        public string? proteam { get; set; }

        public string? des { get; set; }
    }
}
