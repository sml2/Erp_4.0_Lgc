﻿using ERP.Export.ExportCommon;
using ERP.Models.Export;
using ERP.Export.Templates;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class JapanYahoo : ExcelBase<JapanYahoo, JapanYahoo.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.JapanYahoo;
    public override string Name => "日本雅虎";

    public JapanYahoo(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }

    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
    {
        ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
        if (Item.IsNull())
        {
            _Logger.LogInformation($"{Name} GetData ParameterItem Is Null");
            return null!;
        }
        var Product = new List<List<string?>>();
        //描述
        string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, null);
        //卖点
        var BulletPoint = _Common.GetBulletPoint(Data.BulletPoint, Data.SearchTerms, Data.Description, role.SketchMode);
        //关键字
        var Keyword = _Common.GetKeywords(Data.BulletPoint, Data.SearchTerms, Data.Description, role.KeywordMode);
        string Sku = Data.Sku;
        var MainPro = new List<string?>();
        MainPro.Add(Item.path);
        MainPro.Add(Data.Name);
        MainPro.Add(Sku);
        ExportCommon.ProductExportHelper.SetCommonCell(MainPro, "", 2);
        string Price = await _Common.GetMainSalePrice(Data.Price, Rate);
        MainPro.Add(Price);
        MainPro.Add(Price);
        //属性值
        string AttrValue = ProductExportHelper.ToSerialize(Data.Attribute);

        //Data.Attribute.Values.ForEach((r) =>
        //{
        //    if (AttrValue.Length > 0) AttrValue += " ";
        //     AttrValue += string.Join(" ", r);
        //});
        MainPro.Add(AttrValue);
        MainPro.Add(string.Join(" ", Keyword).TrimEnd());
        string caption = "";
        Data.Products.ForEach((r) => caption += r.MainImage + "\r\n");
        MainPro.Add(caption);
        MainPro.Add("");
        MainPro.Add(string.Join(" ", BulletPoint).TrimEnd());
        ExportCommon.ProductExportHelper.SetCommonCell(MainPro, "", 8);
        MainPro.Add(Item.pointcode);
        MainPro.Add(Description);
        MainPro.Add(Description);
        MainPro.Add("");
        DateTime Now = DateTime.Now;
        MainPro.Add(Now.ToString("yyMMdd"));
        MainPro.Add(Now.AddDays(10).ToString("yyMMddHH"));
        ExportCommon.ProductExportHelper.SetCommonCell(MainPro, "", 7);
        MainPro.Add(Item.delivery);
        MainPro.Add(Item.astkcode);
        MainPro.Add(Item.condition);
        MainPro.Add(Item.productcategory);
        ExportCommon.ProductExportHelper.SetCommonCell(MainPro, "", 12);//spec
        MainPro.Add(caption);
        ExportCommon.ProductExportHelper.SetCommonCell(MainPro, "", 2);
        MainPro.Add(Item.leadtimeinstock);
        MainPro.Add(Item.leadtimeoutstock);
        MainPro.Add(Item.keepstock);
        Product.Add(MainPro);
        Dictionary<int, List<List<string?>>>? Dic = new Dictionary<int, List<List<string?>>>();
        Dic.Add(0, Product);
        return Dic;
    }
    public class ParameterItem //: JsonObject<ParameterItem>
    {
        public string? path { get; set; }

        public string? pointcode { get; set; }

        public string? delivery { get; set; }

        public string? astkcode { get; set; }

        public string? condition { get; set; }

        public string? productcategory { get; set; }

        public string? leadtimeinstock { get; set; }

        public string? leadtimeoutstock { get; set; }

        public string? keepstock { get; set; }
    }
}
