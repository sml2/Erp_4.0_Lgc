﻿using ERP.Export.ExportCommon;
using ERP.Export.Templates;
using ERP.Models.Export;

namespace ERP.Export.V4_Export.ExcelTemplates
{
    public class LazadaMobilesTablets : ExcelBase<LazadaMobilesTablets>
    {
        public override TemplateType TemplateType => TemplateType.Lazada_Mobiles_Tablets;

        public override string Name => "Lazada Mobiles & Tablets表";

        public LazadaMobilesTablets(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }
        public override async Task<bool> Verify(string Parameter)
        {
            ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
            if (Item.IsNull())
            {
                _Logger.LogInformation($"{Name} Verify Method ParameterItem Is Null");
                return false;
            }
            if (Item.primary_category.IsNullOrEmpty())
            {
                _Logger.LogInformation($"{Name} Verify Method ParameterItem  Primary Category is Empty!");
                return false;
            }
            return await Task.FromResult(true);
        }

        public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
        {
            List<List<string>> Product = new List<List<string>>();
            ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
            if (Item.IsNull())
            {
                _Logger.LogInformation($"{Name} GetData Method ParameterItem Is Null");
                return null;
            }
            string Sku = Data.Sku;
            if (Data.Products.IsNull() || Data.Products.Count == 0)
            {
                string Price = "0";
                string Cost = "0";
                if (Data.Price is not null && Data.Price.Sale.Max.Value > 0) Price = Data.Price.Sale.Max.Value.ToString();//sale
                if (Data.Price is not null && Data.Price.Cost.Max.Value > 0) Price = Data.Price.Sale.Max.Value.ToString();//cost
                ExcelProduct MainPro = new ExcelProduct(Data.Name, Data.Description, Data.BulletPoint, Data.SearchTerms, "", "", Sku, "", null, Data.Quantity, Price, Cost, "", null, Data.Source);
                var MainExcelPro = GetProduct(MainPro, Item, Rate, role);
                if (MainExcelPro.IsNotNull()) Product.Add(MainExcelPro);
            }
            else
            {
                //子产品
                foreach (var p in Data.Products)
                {
                    ExcelProduct ChildPro = _Common.GetCommonData(p, Data.Attribute, Data.Source, Sku);
                    var ChileExcelPro = GetProduct(ChildPro, Item, Rate, role);
                    if (ChileExcelPro.IsNotNull()) Product.Add(ChileExcelPro);
                }
            }
            var Dic = new Dictionary<int, List<List<string>>>();
            Dic.Add(0, Product);
            return Dic;
        }
        private List<string> GetProduct(ExcelProduct Data, ParameterItem Item, List<Unit> Rate, ExcleRole role)
        {
            List<string> Pro = new List<string>();
            Pro.Add(Item.primary_category);
            Pro.Add(Item.model);
            Pro.Add(Item.brand);
            Pro.Add(Data.Color);
            _Common.GetLazadaCommonCell(Pro, 48, Data, Item, role, Rate);
            Pro.Add("");
            Pro.Add(Item.storage_capacity_new);
            ProductExportHelper.SetCommonCell(Pro, "", 2);
            Pro.Add(Item.compatibility_by_model);
            Pro.Add(Item.cable_length);
            Pro.Add(Item.cable_connection);
            Pro.Add(Item.type_screen_guard);
            Pro.Add(Item.material_screen_guard);
            Pro.Add(Item.powerbank_capacity);
            return Pro;
        }
        public class ParameterItem : IParameterItem //: JsonObject<ParameterItem>, IParameterItem
        {
            public string primary_category { get; set; }

            public string model { get; set; }

            public string brand { get; set; }

            public string color_family { get; set; }

            public string package_content { get; set; }

            public string package_length { get; set; }

            public string package_width { get; set; }

            public string package_height { get; set; }

            public string package_weight { get; set; }

            public string taxes { get; set; }

            public string storage_capacity_new { get; set; }

            public string compatibility_by_model { get; set; }

            public string cable_length { get; set; }

            public string cable_connection { get; set; }

            public string type_screen_guard { get; set; }

            public string material_screen_guard { get; set; }

            public string powerbank_capacity { get; set; }

        }
    }
}
