﻿using ERP.Export.ExportCommon;
using ERP.Export.Templates;
using ERP.Models.Export;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class Ymmbh : ExcelBase<Ymmbh, Ymmbh.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.Ymmbh;
    public override string Name => "Ymmbh";

    public Ymmbh(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }

    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
    {
        List<List<string>> Product = new List<List<string>>();
        ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
        if (Item is null)
        {
            _Logger.LogInformation($"{Name} ParameterItem Is Null");
            //Log(I18N.Instance.Export_Log3);
            return null!;
        }
        string Sku = Data.Sku;
        if (Data.Products.IsNotNull() && Data.Products.Count > 0)
        {
            foreach (var p in Data.Products)
            {
                ExcelProduct ChildPro = _Common.GetCommonData(p, Data.Attribute, Data.Source, Sku, TemplateType);
                var ChileExcelPro = await GetProduct(ChildPro, Item, Rate, role, Sku);
                if (ChileExcelPro.IsNotNull()) Product.Add(ChileExcelPro);
            }
        }
        var Dic = new Dictionary<int, List<List<string>>>();
        Dic.Add(0, Product);
        return Dic;
    }
    public async Task<List<string>> GetProduct(ExcelProduct Data, ParameterItem Item, List<Unit> Rate, ExcleRole role, string Sku)
    {
        //描述
        string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, Data.Images);
        //卖点
        var BulletPoint = _Common.GetBulletPoint(Data.BulletPoint, Data.SearchTerms, Data.Description, role.SketchMode);
        //关键字
        var Keyword = _Common.GetKeywords(Data.BulletPoint, Data.SearchTerms, Data.Description, role.KeywordMode);
        List<string> Pro = new List<string>();
        Pro.Add(Sku);
        Pro.Add(Data.Sku);
        Pro.Add(Item.Operation);
        Pro.Add(Data.Color);
        Pro.Add(Data.Size);
        Pro.Add(Item.Brand);
        Pro.Add(Item.Category);
        Pro.Add(Item.CnName);
        Pro.Add(Item.EnName);
        Pro.Add(Data.Quantity.ToString());
        Pro.Add(Rate[0].Sign);
        string Cost = ProductExportHelper.GetDataPrice(Data.Cost, Rate);
        Pro.Add(Cost);
        Pro.Add(Item.ShipFee);
        Pro.Add(Item.RegistrationTemplate);
        string Price = ProductExportHelper.GetDataPrice(Data.Price, Rate);
        Pro.Add(Price);
        Pro.Add(Item.Weight);
        Pro.Add(Item.PackageSize);
        Pro.Add(Item.ForPeople);
        Pro.Add(Item.Material);
        Pro.Add(Item.PackageMaterial);
        Pro.Add(Item.Metal);
        Pro.Add(Item.Jewelry);
        Pro.Add(role.Language.ToString());
        Pro.Add(Data.Name);
        Pro.Add(string.Join(",", Keyword));
        BulletPoint.ForEach((r) => { Pro.Add(r); });
        Pro.Add(Description);
        if (Data.Images.IsNotNull()) Pro.Add(string.Join("|", Data.Images));
        else Pro.Add(Data.MainImage);
        Pro.Add(Data.Source);
        return Pro;
    }
    public class ParameterItem //: JsonObject<ParameterItem>
    {
        public string Operation { get; set; }
        public string Brand { get; set; }
        public string Category { get; set; }
        public string CnName { get; set; }
        public string EnName { get; set; }
        public string ShipFee { get; set; }
        /// <summary>
        /// 挂号模板
        /// </summary>
        public string RegistrationTemplate { get; set; }
        /// <summary>
        /// 毛重(g)
        /// </summary>
        public string Weight { get; set; }
        /// <summary>
        /// 包装尺寸
        /// </summary>
        public string PackageSize { get; set; }
        /// <summary>
        /// 适用人群
        /// </summary>
        public string ForPeople { get; set; }
        public string Material { get; set; }
        public string PackageMaterial { get; set; }
        public string Metal { get; set; }
        public string Jewelry { get; set; }
        public string Language { get; set; }
    }
}
