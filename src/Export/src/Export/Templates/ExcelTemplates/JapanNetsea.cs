﻿using System.ComponentModel.DataAnnotations;
using ERP.Export.ExportCommon;
using ERP.Export.Templates;
using ERP.Models.Export;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class JapanNetsea : ExcelBase<JapanNetsea>
{
    public override TemplateType TemplateType => TemplateType.JapanNetsea;
    public override string Name => "日本netsea表";

    public JapanNetsea(IServiceProvider _ServiceProvider) : base(_ServiceProvider)
    {
    }

    public override bool TryValidate(string parameter, out List<ValidationResult> results)
    {
        results = new List<ValidationResult>();
        return true;
    }

    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem data, ExcleRole role,
        string parameter, List<Unit> rate)
    {
        var products = new List<List<string?>>();
        var mainSku = data.Sku;

        //子产品
        if (data.Products.IsNotNull() && data.Products.Count > 0)
        {
            for (var index = 0; index < data.Products.Count; index++)
            {
                var p = data.Products[index];
                var childPro = _Common.GetCommonData(p, data.Attribute, data.Source, mainSku);
                var chileExcelPro = await GetProduct(childPro, role, false, rate, mainSku, (index + 1).ToString(),
                    data.RawPid);
                if (chileExcelPro.IsNotNull()) products.Add(chileExcelPro);
            }
        }
        else // 只有主产品
        {
            var price = await _Common.GetMainSalePrice(data.Price, rate);
            var cost = _Common.GetMainCostPrice(data.Price, rate);
            var pro = new ExcelProduct(data.Name, data.Description, data.BulletPoint, data.SearchTerms, "", "", mainSku,
                "",
                null, data.Quantity, price, cost, "", null, data.Source);
            var mainPro = await GetProduct(pro, role, true, rate, mainSku);
            if (mainPro.IsNotNull()) products.Add(mainPro);
        }

        return new Dictionary<int, List<List<string?>>> { { 0, products } };
    }

    private Task<List<string?>> GetProduct(ExcelProduct data, ExcleRole role, bool isMain, List<Unit> rate,
        string mainSku, string? index = null, string? pid = null)
    {
        //描述
        string description = _Common.GetDescription(data.Description, string.Join(";", data.BulletPoint),
            data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, data.Images);
        //卖点
        var bulletPoint = _Common.GetBulletPoint(data.BulletPoint, data.SearchTerms, data.Description, role.SketchMode);
        //关键字
        var keyword = _Common.GetKeywords(data.BulletPoint, data.SearchTerms, data.Description, role.KeywordMode);
        //主产品
        var pro = new List<string?>();
        pro.Add(mainSku);
        pro.Add(data.Name);
        ProductExportHelper.SetCommonCell(pro, "", 8);
        string attrValue = data.Size;
        if (attrValue.IsNotWhiteSpace()) attrValue += ",";
        attrValue += data.Color;
        pro.Add(isMain ? "" : attrValue);
        ProductExportHelper.SetCommonCell(pro, "", 5);
        bulletPoint.ForEach((r) => pro.Add(r));
        var images = new List<string?>();
        images.Add(data.MainImage);
        images.AddRange(data.Images);
        int count = images.Count;
        if (count < 10) ProductExportHelper.SetCommonCell(images, "", 10 - count);

        foreach (var (image, i) in images.Select((img, i) => (img, i)).Take(10))
        {
            pro.Add(image);
            pro.Add(!isMain && i == 0 ? pid : string.Empty); // 画像1填入产品Pid
        }

        pro.Add(!isMain ? index : ""); // 变体顺序，从1开始
        pro.Add(data.Sku);
        var colorSize = string.Join(", ", new[] { data.Color, data.Size }
            .Where(str => !string.IsNullOrEmpty(str)));
        pro.Add(string.IsNullOrEmpty(colorSize) ? data.Name : $"{data.Name} ({colorSize})"); // 内訳, 格式为: 标题 (颜色, 尺寸)
        string price = data.Price.ToString();
        if (!isMain) price = ProductExportHelper.GetDataPrice(price, rate);
        pro.Add(price);
        pro.Add("");
        pro.Add(price);
        pro.Add(data.Quantity.ToString());
        pro.Add(data.Quantity.ToString());
        pro.Add("N");
        pro.Add("N");
        pro.Add("Y");
        pro.Add("T");
        pro.Add("");
        pro.AddRange(keyword.Take(3));
        ProductExportHelper.SetCommonCell(pro, "", 10);
        pro.Add(description);
        return Task.FromResult(pro);
    }
}