﻿using ERP.Export.ExportCommon;
using ERP.Models.Export;
using ERP.Export.Templates;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class ERPebay : ExcelBase<ERPebay, ERPebay.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.ERPebay;
    public override string Name => "船长ERPebay表";

    public ERPebay(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }

    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
    {
        ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
        if (Item is null)
        {
            _Logger.LogInformation($"{Name} GetData ParameterItem Is Null");
            return null;
        }
        var Product = new List<List<string?>>();
        string Sku = Data.Sku;
        if (Data.Products.IsNull() || Data.Products.Count == 0)
        {
            string Price = await _Common.GetMainSalePrice(Data.Price, Rate);
            string Cost = _Common.GetMainCostPrice(Data.Price, Rate);
            ExcelProduct MainPro = new ExcelProduct(Data.Name, Data.Description, Data.BulletPoint, Data.SearchTerms, "", "", Sku, "", null, Data.Quantity, Price, Cost, "", null, Data.Source);
            var MainExcelPro = await GetProduct(MainPro, Item, Rate, role, Sku, true);
            if (MainExcelPro.IsNotNull()) Product.Add(MainExcelPro);
        }
        else
        {
            foreach (var p in Data.Products)
            {
                ExcelProduct ChildPro = _Common.GetCommonData(p, Data.Attribute, Data.Source, Sku);
                var ChildExcelPro = await GetProduct(ChildPro, Item, Rate, role, Sku, false);
                if (ChildExcelPro.IsNotNull()) Product.Add(ChildExcelPro);
            }
        }
        Dictionary<int, List<List<string?>>>? Dic = new Dictionary<int, List<List<string?>>>();
        Dic.Add(0, Product);
        return Dic;
    }
    private Task<List<string?>> GetProduct(ExcelProduct Data, ParameterItem Item, List<Unit> Rate, ExcleRole role, string Sku, bool IsMain)
    {
        //描述
        string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, Data.Images);
        //卖点
        var BulletPoint = _Common.GetBulletPoint(Data.BulletPoint, Data.SearchTerms, Data.Description, role.SketchMode);
        //关键字
        var Keyword = _Common.GetKeywords(Data.BulletPoint, Data.SearchTerms, Data.Description, role.KeywordMode);
        var Pro = new List<string?>();
        Pro.Add(Data.Sku);
        Pro.Add(Item.eBayUserID);
        Pro.Add(Data.Sku);
        Pro.Add(Data.Name);
        Pro.Add("");
        Pro.Add(Item.DescriptionTitle);
        Pro.Add(string.Join(" ", BulletPoint));
        Pro.Add(Description);
        ProductExportHelper.SetCommonCell(Pro, "", 2);
        Pro.Add(Item.SellerDescription);
        Pro.Add(Item.Control_MaxActiveListing);
        Pro.Add(Data.Sku);
        string Price = Data.Price;
        if (!IsMain) Price = ProductExportHelper.GetDataPrice(Data.Price, Rate);
        Pro.Add(Price);
        Pro.Add(Data.Quantity.ToString());
        Dictionary<string, string> AttrDic = new Dictionary<string, string>();
        if (Data.Color.IsNotWhiteSpace()) AttrDic.Add("Color", Data.Color);
        if (Data.Size.IsNotWhiteSpace()) AttrDic.Add("Size", Data.Size);
        AttrDic.ForEach((r) =>
        {
            Pro.Add(r.Key);
            Pro.Add(r.Value);
        });
        if (AttrDic.Count < 5)
        {
            for (int i = 1; i <= 5 - AttrDic.Count; i++)
            {
                ProductExportHelper.SetCommonCell(Pro, "", 2);
            }
        }
        if (AttrDic.Count > 1) Pro.Add(AttrDic.Keys.ToArray()[0]);
        else Pro.Add("");
        ProductExportHelper.SetCommonImage(Pro, Data.Images, 12);
        ProductExportHelper.SetCommonCell(Pro, "", 2);
        Pro.Add(Data.ProduceCode);
        ProductExportHelper.SetCommonCell(Pro, "", 4);
        if (Rate.Count > 0) Pro.Add(Rate[0].Sign);
        else Pro.Add("");
        ProductExportHelper.SetCommonCell(Pro, "", 4);
        Pro.Add(Price);
        Pro.Add("");
        Pro.Add(Item.Duration);
        Pro.Add("Variation");
        Pro.Add(Item.eBaySiteID);
        Pro.Add(Item.CategoryID1);
        Pro.Add("");
        Pro.Add(Item.CategoryName1);
        ProductExportHelper.SetCommonCell(Pro, "", 16);
        Pro.Add(Item.ListTemplateType);
        Pro.Add(Data.Name);
        Pro.Add("");
        Pro.Add(Item.ListTemplateID);
        ProductExportHelper.SetCommonImage(Pro, Data.Images, 16);
        ProductExportHelper.SetCommonImage(Pro, Data.Images, 12);
        ProductExportHelper.SetCommonCell(Pro, "", 52);
        Pro.Add(Item.StoreCategoryID1);
        Pro.Add("");
        Pro.Add(Item.StoreCategoryName1);
        Pro.Add("");
        Pro.Add(Sku);
        ProductExportHelper.SetCommonCell(Pro, "", 78);
        Pro.Add(Data.MainImage);
        string ShowImg = "";
        if (Data.Images.Count > 0) ShowImg = Data.Images[0];
        Pro.Add(ShowImg);
        return Task.FromResult(Pro);
    }
    public class ParameterItem //: JsonObject<ParameterItem>
    {
        public string? eBayUserID { get; set; }

        public string? DescriptionTitle { get; set; }

        public string? SellerDescription { get; set; }

        public string? Control_MaxActiveListing { get; set; }

        public string? V_Prod_UPC { get; set; }

        public string? Duration { get; set; }

        public string? eBaySiteID { get; set; }

        public string? CategoryID1 { get; set; }

        public string? CategoryName1 { get; set; }

        public string? ListTemplateType { get; set; }

        public string? ListTemplateID { get; set; }

        public string? StoreCategoryID1 { get; set; }

        public string? StoreCategoryName1 { get; set; }
    }
}
