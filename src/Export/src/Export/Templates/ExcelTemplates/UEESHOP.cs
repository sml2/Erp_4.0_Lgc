﻿using ERP.Export.ExportCommon;
using ERP.Models.Export;
using ERP.Export.Templates;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class UEESHOP : ExcelBase<UEESHOP, UEESHOP.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.UEESHOP;
    public override string Name => "UEESHOP表";

    public UEESHOP(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }

    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
    {
        List<List<string>> Product = new List<List<string>>();
        ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
        if (Item is null)
        {
            _Logger.LogInformation($"{Name} GetData Method ParameterItem Is Null");
            return null!;
        }
        string Sku = Data.Sku;
        //子产品
        if (Data.Products.IsNotNull() && Data.Products.Count > 0)
        {
            foreach (var p in Data.Products)
            {
                ExcelProduct ChildPro = _Common.GetCommonData(p, Data.Attribute, Data.Source, Sku, TemplateType);
                var ChileExcelPro = await GetProduct(ChildPro, Item, role, Rate, Sku, Parameter, false);
                if (ChileExcelPro.IsNotNull()) Product.Add(ChileExcelPro);
            }
        }
        var Dic = new Dictionary<int, List<List<string>>>();
        Dic.Add(0, Product);
        return Dic;
    }
    private async Task<List<string>> GetProduct(ExcelProduct Data, ParameterItem Item, ExcleRole role, List<Unit> Rate, string Sku, string Parameter, bool IsMain)
    {
        //描述
        string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, Data.Images);
        //卖点
        var BulletPoint = _Common.GetBulletPoint(Data.BulletPoint, Data.SearchTerms, Data.Description, role.SketchMode).Where((r) => r.IsNotWhiteSpace()).ToList();
        //关键字
        var Keyword = _Common.GetKeywords(Data.BulletPoint, Data.SearchTerms, Data.Description, role.KeywordMode).Where((r) => r.IsNotWhiteSpace()).ToList();
        //主产品
        List<string> Pro = new List<string>();
        Pro.Add(Data.Source);
        Pro.Add(Data.Name);
        Pro.Add(Item.category);
        Pro.Add(Sku);
        string Cost = ProductExportHelper.GetDataPrice(Data.Cost, Rate);
        Pro.Add(Cost);
        string Price = ProductExportHelper.GetDataPrice(Data.Price, Rate);
        Pro.Add(Price);//Standard Price
        Pro.Add(Data.MainImage);
        if (Data.Color.IsNotWhiteSpace() && Data.Size.IsNotWhiteSpace())
        {
            Pro.Add("Color");
            Pro.Add(Data.Color);
            Pro.Add("Size");
            Pro.Add(Data.Size);
        }
        else if (Data.Color.IsNotWhiteSpace())
        {
            Pro.Add("Color");
            Pro.Add(Data.Color);
            ProductExportHelper.SetCommonCell(Pro, "", 2);
        }
        else if (Data.Color.IsNotWhiteSpace())
        {
            Pro.Add("Size");
            Pro.Add(Data.Size);
            ProductExportHelper.SetCommonCell(Pro, "", 2);
        }
        else
        {
            ProductExportHelper.SetCommonCell(Pro, "", 4);
        }
        Pro.Add(Data.Sku);
        Pro.Add(Cost);
        Pro.Add(Price);
        Pro.Add(Data.Quantity.ToString());
        Pro.Add(Item.attribute_weight);
        Pro.Add(Data.MainImage);
        Pro.Add(Item.multiple_specifications);
        Pro.Add(Item.attribute_weight);
        Pro.Add(Data.Quantity.ToString());
        Pro.Add(Item.status);
        Pro.Add(Item.track_inventory);
        Pro.Add(Data.Name);
        Pro.Add(string.Join(",", Keyword));
        Pro.Add(string.Join(",", BulletPoint));
        Pro.Add(Item.brief_introduction);
        Pro.Add(Description);
        return Pro;
    }
    public class ParameterItem//:JsonObject<ParameterItem>
    {
        public string category { get; set; }
        public string attribute_weight { get; set; }
        public string multiple_specifications { get; set; }
        public string status { get; set; }
        public string track_inventory { get; set; }
        public string brief_introduction { get; set; }
    }
}
