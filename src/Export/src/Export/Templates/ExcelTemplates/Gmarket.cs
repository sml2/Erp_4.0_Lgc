﻿using System.ComponentModel.DataAnnotations;
using ERP.Export.ExportCommon;
using ERP.Export.Templates;
using ERP.Models.Export;

namespace ERP.Export.V4_Export.ExcelTemplates
{
    public class Gmarket : ExcelBase<Gmarket, Gmarket.ParameterItem>
    {
        public override TemplateType TemplateType => TemplateType.Gmarket;

        public override string Name => "Gmarket表";

        public Gmarket(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }

        public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
        {
            ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
            if (Item is null)
            {
                _Logger.LogInformation($"{Name} ParameterItem Is Null");
                //Log(I18N.Instance.Export_Log3);
                return null;
            }
            //描述
            string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, null);
            //卖点
            var BulletPoint = _Common.GetBulletPoint(Data.BulletPoint, Data.SearchTerms, Data.Description, role.SketchMode);
            var Product = new List<List<string?>>();
            string Sku = Data.Sku;
            var MainPro = new List<string?>();
            MainPro.Add(Item.origin_number);
            MainPro.Add(Item.seller_manage_code);
            MainPro.Add(Item.folder_name);
            MainPro.Add(Item.cat_number);
            MainPro.Add(Data.Name);
            string Price = await _Common.GetMainSalePrice(Data.Price, Rate);
            MainPro.Add(Price);
            MainPro.Add(Data.Quantity.ToString());
            MainPro.Add(Item.max_number);
            MainPro.Add(Item.min_number);
            MainPro.Add(Item.origin);
            MainPro.Add(Item.auction_source_type);
            MainPro.Add(Item.manufacturer);
            MainPro.Add(Item.importer);
            List<string> AllImage = new List<string>();
            if (Data.ShowImage.IsNotWhiteSpace()) AllImage.Add(Data.ShowImage);
            Data.Products.ForEach((r) =>
            {
                if (r.MainImage.IsNotWhiteSpace()) AllImage.Add(r.MainImage);
            });
            ExportCommon.ProductExportHelper.SetCommonImage(MainPro, AllImage, 6);
            MainPro.Add(Description);
            MainPro.Add(Item.esm_config_info);
            MainPro.Add(Item.esm_promotion_info);
            MainPro.Add(Item.option_type);
            MainPro.Add("주문\r\n확인");
            string Attributes = "";
            int i = 1;
            Data.Products.ForEach((r) =>
            {
                if (Attributes.Length > 0) Attributes += "\r\n";
                List<string> TempAttr = _Common.GetAllAttributes(r.Values, Data.Attribute);
                if (TempAttr.Count > 0)
                {
                    Attributes += $"0{i}_{string.Join(" ", TempAttr)}*주문확인**0*100*Y";
                    i++;
                }
            });
            MainPro.Add(Attributes);
            MainPro.Add(Item.optional_stock);
            MainPro.Add(Item.composite_type);
            MainPro.Add(Item.additional_option);
            MainPro.Add(Item.config_info);
            //=========
            MainPro.Add(Item.is_tax);
            MainPro.Add(Item.age_limit);
            MainPro.Add(Item.product_date);
            MainPro.Add(Item.product_status);
            MainPro.Add(Item.effective_date);
            MainPro.Add(Item.public_relations_text);
            string Cost = _Common.GetMainCostPrice(Data.Price, Rate);
            MainPro.Add(Cost);
            MainPro.Add(Price);
            MainPro.Add(Item.is_deduction);
            MainPro.Add(Item.series_code);
            MainPro.Add("Y");
            ExportCommon.ProductExportHelper.SetCommonCell(MainPro, "", 29);
            MainPro.Add(Item.error_msg);
            Product.Add(MainPro);
            Dictionary<int, List<List<string?>>>? Dic = new Dictionary<int, List<List<string?>>>();
            Dic.Add(0, Product);
            return Dic;
        }
        public class ParameterItem //: JsonObject<ParameterItem>
        {
            public string? unit { get; set; }

            public string? brand_name { get; set; }

            public string? manufacturer { get; set; }

            public string? category { get; set; }

            public string? origin { get; set; }

            public string? statue { get; set; }

            public string? taxes { get; set; }

            public string? freight { get; set; }
            [Required]
            public string origin_number { get; set; } = null!;
            [Required]
            public string cat_number { get; set; } = null!;

            public string? folder_name { get; set; }

            public string? seller_manage_code { get; set; }

            public string? max_number { get; set; }

            public string? min_number { get; set; }

            public string? auction_source_type { get; set; }

            public string? importer { get; set; }

            public string? esm_config_info { get; set; }

            public string? esm_promotion_info { get; set; }

            public string? option_type { get; set; }

            public string? composite_type { get; set; }

            public string? optional_stock { get; set; }

            public string? additional_option { get; set; }

            public string? config_info { get; set; }

            public string? model_name { get; set; }

            public string? is_tax { get; set; }

            public string? age_limit { get; set; }

            public string? product_date { get; set; }

            public string? product_status { get; set; }

            public string? effective_date { get; set; }

            public string? public_relations_text { get; set; }

            public string? is_deduction { get; set; }
            [Required]
            public string series_code { get; set; } = null!;

            public string? error_msg { get; set; }
        }
    }
}
