﻿using ERP.Export.ExportCommon;
using ERP.Export.Templates;
using ERP.Models.Export;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class Bol : ExcelBase<Bol, Bol.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.Bol;
    public override string Name => "Bol导出模板";


    public Bol(IServiceProvider serviceProvider) : base(serviceProvider)
    {
    }

    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem data, ExcleRole role,
        string parameter, List<Unit> rate)
    {
        var product = new List<List<string?>>();
        var item = ProductExportHelper.ToDeserialize<ParameterItem>(parameter);
        if (item is null)
        {
            _Logger.LogInformation("{Name}ParameterItem is Null", Name);
            return null!;
        }

        var sku = data.Sku;
        
        //子产品
        foreach (var p in data.Products)
        {
            var childPro =
                 _Common.GetCommonData(p, data.Attribute, data.Source, sku, TemplateType.Bol);
            
            var chileExcelPro = GetProduct(childPro, item, role, rate, sku);
            if (chileExcelPro.IsNotNull()) product.Add(chileExcelPro);
        }

        var dic = new Dictionary<int, List<List<string?>>>();
        dic.Add(0, product);
        return dic;
    }

    private List<string?> GetProduct(ExcelProduct data, ParameterItem item, ExcleRole role, List<Unit> rate, string sku)
    {
        //描述
        var description = _Common.GetDescription(data.Description, string.Join(";", data.BulletPoint),
            data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, data.Images);
        //卖点
        var bulletPoint = _Common.GetBulletPoint(data.BulletPoint, data.SearchTerms, data.Description, role.SketchMode);
        //关键字
        var keyword = _Common.GetKeywords(data.BulletPoint, data.SearchTerms, data.Description, role.KeywordMode);
        //主产品
        var pro = new List<string?>();
        pro.Add("");
        pro.Add(data.ProduceCode);
        pro.Add("nieuw");
        pro.Add("");
        pro.Add(data.Quantity.ToString());
        var price = ProductExportHelper.GetDataPrice(data.Price, rate);
        pro.Add(price);
        pro.Add("1-8d");
        pro.Add("verkoper");
        pro.Add(""); // Te koop
        pro.Add(data.Name);
        pro.Add(description);
        return pro;
    }

    public class ParameterItem : CommonParameter
    {
    }
}