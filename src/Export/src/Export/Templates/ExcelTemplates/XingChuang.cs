﻿using ERP.Export.ExportCommon;
using ERP.Models.Export;
using ERP.Export.Templates;

namespace ERP.Export.V4_Export.ExcelTemplates
{
    public class XingChuang : ExcelBase<XingChuang, XingChuang.ParameterItem>
    {
        public override TemplateType TemplateType => TemplateType.XingChuang;

        public override string Name => "XingChuang";

        public XingChuang(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }

        public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
        {
            List<List<string>> Product = new List<List<string>>();
            ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
            if (Item is null)
            {
                _Logger.LogInformation($"{Name} GetData Method ParameterItem Is Null");
                return null;
            }
            string Sku = Data.Sku;
            if (Data.Products.IsNotNull() && Data.Products.Count > 0)
            {
                foreach (var p in Data.Products)
                {
                    ExcelProduct ChildPro = _Common.GetCommonData(p, Data.Attribute, Data.Source, Sku, TemplateType);
                    var ChileExcelPro = await GetProduct(ChildPro, Item, Rate, role);
                    if (ChileExcelPro.IsNotNull()) Product.Add(ChileExcelPro);
                }
            }
            var Dic = new Dictionary<int, List<List<string>>>();
            Dic.Add(0, Product);
            return Dic;
        }
        public async Task<List<string>> GetProduct(ExcelProduct Data, ParameterItem Item, List<Unit> Rate, ExcleRole role)
        {
            //描述
            string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, Data.Images);
            List<string> Pro = new List<string>();
            Pro.Add(Item.ProductCode);
            Pro.Add(Item.ProductBrand);
            Pro.Add(Data.Name);
            Pro.Add(Data.Name);
            Pro.Add(Description);
            Pro.Add(Description);
            Pro.Add(Item.OptimalAgeGroup);
            Pro.Add(Item.Material);
            Pro.Add(Item.Shape);
            Pro.Add(Item.Sex);
            Pro.Add(Data.Color);
            Pro.Add(Item.Style);
            Pro.Add(Data.Sku);
            Pro.Add(Item.GTIN);
            Pro.Add(Item.Length);
            Pro.Add(Item.Width);
            Pro.Add(Item.Height);
            Pro.Add(Item.Weight);
            string Price = ProductExportHelper.GetDataPrice(Data.Price, Rate);
            Pro.Add(Price);
            //Pro.Add(Item.EstimatedIncome);
            Pro.Add(Rate[0].Sign);
            Pro.Add(Data.Quantity.ToString());
            Pro.Add(Data.MainImage);
            ProductExportHelper.SetCommonImage(Pro, Data.Images, 5);
            ProductExportHelper.SetCommonImage(Pro, Data.Images, 5);
            Pro.Add(Item.InstallationMethod);
            string LW = "";
            if (Item.Length.IsNotWhiteSpace() && Item.Width.IsNotWhiteSpace()) LW = $"{Item.Length}*{Item.Width}";
            Pro.Add(LW);
            Pro.Add(Data.Quantity.ToString());
            Pro.Add(Item.Touch);
            Pro.Add(Item.Type);
            Pro.Add(Item.Effect);
            Pro.Add(Item.BeautyNouns);
            Pro.Add(Item.Specification);
            Pro.Add(Item.Features);
            Pro.Add(Item.TypesOfHairdressingTools);
            Pro.Add(Item.DimmingType);
            Pro.Add(Item.WhetherItContainsCore);
            Pro.Add(Item.PopularElements);
            Pro.Add(Data.Size);
            Pro.Add(Item.StorageObject);
            Pro.Add(Item.HomeTextileCharacteristics);
            Pro.Add(Item.StorageFeatures);
            Pro.Add(Data.Size);
            Pro.Add(Item.MovementType);
            Pro.Add(Item.WaterproofDegree);
            Pro.Add(Item.StrapMaterial);
            Pro.Add(Item.DialShape);
            Pro.Add(Item.Seasons);
            Pro.Add(Item.IsWired);
            Pro.Add(Item.PlugType);
            Pro.Add(Item.ApplicableNetworkType);
            Pro.Add(Item.InstrumentalNouns);
            Pro.Add(Item.PowerSupplyMethod);
            Pro.Add(Item.Power);
            Pro.Add(Item.Gear);
            string LWH = "";
            if (Item.Length.IsNotWhiteSpace() && Item.Width.IsNotWhiteSpace() && Item.Height.IsNotWhiteSpace()) LWH = $"{Item.Length}*{Item.Width}*{Item.Height}";
            Pro.Add(LWH);
            Pro.Add(Item.Length);
            Pro.Add(Item.Width);
            Pro.Add(Item.Height);
            Pro.Add(Item.PrinciplesOfMosquitoControl);
            Pro.Add(Item.Scent);
            Pro.Add(Item.PhysicalForm);
            Pro.Add(Item.FeaturesOfCleaningTools);
            Pro.Add(Item.Capacity);
            Pro.Add(Item.NumberOfLayers);
            Pro.Add(Item.Voltage);
            Pro.Add(Item.Fabric);
            Pro.Add(Item.ApplicablePregnancyStage);
            Pro.Add(Item.Thickness);
            Pro.Add(Item.Pattern);
            Pro.Add(Item.Waist);
            Pro.Add(Item.PantsLengthType);
            Pro.Add(Item.ClosedWay);
            Pro.Add(Item.InternalStructure);
            Pro.Add(Item.ShoeStyle);
            Pro.Add(Item.ToeStyle);
            Pro.Add(Item.UpperMaterial);
            Pro.Add(Item.FullBust);
            Pro.Add(Item.ShoulderWidth);
            Pro.Add(Item.WaistCircumference);
            Pro.Add(Item.HipCircumference);
            Pro.Add(Item.ClothesLength);
            Pro.Add(Item.SleeveLength);
            Pro.Add(Item.ClothingNouns);
            Pro.Add(Item.PantsLength);
            Pro.Add(Item.SkirtLength);
            Pro.Add(Item.SkirtLengthType);
            Pro.Add(Item.MainIngredientContent);
            Pro.Add(Item.SocksType);
            Pro.Add(Item.LightSourceColor);
            Pro.Add(Item.IsFolded);
            Pro.Add(Item.Stuffing);
            Pro.Add(Item.ForPeople);
            Pro.Add(Item.ConstructionSituation);
            Pro.Add(Item.TentStructure);
            Pro.Add(Item.ApplicableNumber);
            Pro.Add(Item.DIYUsageScenarios);
            Pro.Add(Item.ApplicableSpace);
            Pro.Add(Item.RatedPower);
            Pro.Add(Item.SmallAppliancesNouns);
            Pro.Add(Item.InterfaceType);
            Pro.Add(Item.Combination);
            Pro.Add(Item.Texture);
            Pro.Add(Item.FitnessEquipmentClassification);
            Pro.Add(Item.FitnessEffect);
            Pro.Add(Item.ApplicableParts);
            Pro.Add(Item.Diameter);
            Pro.Add(Item.ApplicableDanceType);
            Pro.Add(Item.SportsSeries);
            Pro.Add(Item.Reminder);
            Pro.Add(Item.IsWearHat);
            Pro.Add(Item.ScreenSize);
            Pro.Add(Item.ProductTypeName);
            Pro.Add(Item.IsWireControl);
            Pro.Add(Item.TheWayOfWearHeadset);
            Pro.Add(Item.ApplicableModel);
            Pro.Add(Item.BrushHeadMaterial);
            Pro.Add(Item.Function);
            Pro.Add(Item.TypesOfPhotographicEquipment);
            Pro.Add(Item.HeadphoneAccessoryType);
            Pro.Add(Item.DigitalNouns);
            Pro.Add(Item.ApplicableBedSize);
            Pro.Add(Item.PopularStyles);
            Pro.Add(Item.PersonalCareElectricalNouns);
            Pro.Add(Item.IsDetachable);
            Pro.Add(Item.MinimumMealSize);
            Pro.Add(Item.TimeSetting);
            Pro.Add(Item.MaximumMealSize);
            Pro.Add(Item.IsBrinkingFountain);
            Pro.Add(Item.PowerSource);
            Pro.Add(Item.FabricFunction);
            Pro.Add(Item.CleaningMethod);
            Pro.Add(Item.Elasticity);
            Pro.Add(Item.StyleTouch);
            Pro.Add(Item.SleeveType);
            Pro.Add(Item.Detail);
            Pro.Add(Item.Craft);
            Pro.Add(Item.ClothingFunctionality);
            Pro.Add(Item.PlacketType);
            Pro.Add(Item.PrintingTheme);
            Pro.Add(Item.PantsType);
            Pro.Add(Item.FrameMmaterial);
            Pro.Add(Item.PartsClassification);
            Pro.Add(Item.IsAuto);
            Pro.Add(Item.ApplicablePartsOfInteriorStickers);
            Pro.Add(Item.SurfaceMaterial);
            Pro.Add(Item.AbilityTraining);
            Pro.Add(Item.EarlyEducationDirectionClassification);
            Pro.Add(Item.ModelAssemblyType);
            Pro.Add(Item.ToyType);
            Pro.Add(Item.ProtectionFunction);
            Pro.Add(Item.WaistType);
            Pro.Add(Item.StarRelated);
            Pro.Add(Item.ThePrincipleOfEarphoneSound);
            Pro.Add(Item.HeadphoneCategory);
            Pro.Add(Item.Kind);
            Pro.Add(Item.ApplicableScene);
            Pro.Add(Item.Suitable);
            Pro.Add(LW);
            Pro.Add(LWH);
            Pro.Add(Item.Height);
            Pro.Add(Item.ScopeOfApplication);
            Pro.Add(Item.ProductCategory);
            Pro.Add(Item.Height);
            Pro.Add(Item.HeelHeight);
            Pro.Add(Item.SuitableModels);
            Pro.Add(Item.KeyboardInterfaceType);
            Pro.Add(Item.InternalLength);
            Pro.Add(Item.PowerSupply);
            Pro.Add(LWH);
            Pro.Add("");
            Pro.Add(Item.InsulationCupStyle);
            Pro.Add(Item.FreshkeepingBagCategory);
            Pro.Add(Item.FunctionLuggageType);
            Pro.Add(Item.PlacementMethod);
            Pro.Add(Item.CarryingMethod);
            Pro.Add(Item.DisplayMethod);
            Pro.Add(Item.ApplicableMobilePhoneModels);
            Pro.Add(Item.LoadBearing);
            Pro.Add(Item.Popular);
            Pro.Add(Item.OuterShape);
            Pro.Add(Item.WaytoControl);
            Pro.Add(Item.ChildBagType);
            Pro.Add(Item.WaterproofPerformance);
            Pro.Add(Item.FashionDesignOfEarrings);
            Pro.Add(Item.ApplicableMusicType);
            Pro.Add(Item.Function);
            return Pro;
        }
        public class ParameterItem //: JsonObject<ParameterItem>
        {
            /// <summary>
            /// 商品货号
            /// </summary>
            public string ProductCode { get; set; }
            /// <summary>
            /// 商品品牌
            /// </summary>
            public string ProductBrand { get; set; }
            /// <summary>
            /// 最适年龄段
            /// </summary>
            public string OptimalAgeGroup { get; set; }
            /// <summary>
            /// 材质
            /// </summary>
            public string Material { get; set; }
            /// <summary>
            /// 形状
            /// </summary>
            public string Shape { get; set; }
            /// <summary>
            /// 适用性别
            /// </summary>
            public string Sex { get; set; }
            /// <summary>
            /// 款式
            /// </summary>
            public string Style { get; set; }
            /// <summary>
            /// GTIN编码
            /// </summary>
            public string GTIN { get; set; }
            /// <summary>
            /// 长度(cm)
            /// </summary>
            public string Length { get; set; }
            /// <summary>
            /// 包装宽度(cm)
            /// </summary>
            public string Width { get; set; }
            /// <summary>
            /// 包装高度(cm)
            /// </summary>
            public string Height { get; set; }
            /// <summary>
            /// 重量(g)
            /// </summary>
            public string Weight { get; set; }
            /// <summary>
            /// 预估收入
            /// </summary>
            public string EstimatedIncome { get; set; }
            /// <summary>
            /// 安装方式
            /// </summary>
            public string InstallationMethod { get; set; }
            /// <summary>
            /// 风格
            /// </summary>
            public string Touch { get; set; }
            /// <summary>
            /// 类型
            /// </summary>
            public string Type { get; set; }
            /// <summary>
            /// 功效
            /// </summary>
            public string Effect { get; set; }
            /// <summary>
            /// 美妆名词
            /// </summary>
            public string BeautyNouns { get; set; }
            /// <summary>
            /// 规格
            /// </summary>
            public string Specification { get; set; }
            /// <summary>
            /// 功能特点
            /// </summary>
            public string Features { get; set; }
            /// <summary>
            /// 美发工具类型
            /// </summary>
            public string TypesOfHairdressingTools { get; set; }
            /// <summary>
            /// 调光类型
            /// </summary>
            public string DimmingType { get; set; }
            /// <summary>
            /// 是否含芯
            /// </summary>
            public string WhetherItContainsCore { get; set; }
            /// <summary>
            /// 流行元素
            /// </summary>
            public string PopularElements { get; set; }
            /// <summary>
            /// 收纳对象
            /// </summary>
            public string StorageObject { get; set; }
            /// <summary>
            /// 家纺特点
            /// </summary>
            public string HomeTextileCharacteristics { get; set; }
            /// <summary>
            /// 收纳特征
            /// </summary>
            public string StorageFeatures { get; set; }
            /// <summary>
            /// 机芯类型
            /// </summary>
            public string MovementType { get; set; }
            /// <summary>
            /// 防水程度
            /// </summary>
            public string WaterproofDegree { get; set; }
            /// <summary>
            /// 表带材质
            /// </summary>
            public string StrapMaterial { get; set; }
            /// <summary>
            /// 表盘形状
            /// </summary>
            public string DialShape { get; set; }
            /// <summary>
            /// 季节
            /// </summary>
            public string Seasons { get; set; }
            /// <summary>
            /// 是否有线
            /// </summary>
            public string IsWired { get; set; }
            /// <summary>
            /// 插头类型
            /// </summary>
            public string PlugType { get; set; }
            /// <summary>
            /// 适用网络类型
            /// </summary>
            public string ApplicableNetworkType { get; set; }
            /// <summary>
            /// 工具名词
            /// </summary>
            public string InstrumentalNouns { get; set; }
            /// <summary>
            /// 电源方式
            /// </summary>
            public string PowerSupplyMethod { get; set; }
            /// <summary>
            /// 功率
            /// </summary>
            public string Power { get; set; }
            /// <summary>
            /// 档位
            /// </summary>
            public string Gear { get; set; }
            /// <summary>
            /// 灭蚊原理
            /// </summary>
            public string PrinciplesOfMosquitoControl { get; set; }
            /// <summary>
            /// 香味
            /// </summary>
            public string Scent { get; set; }
            /// <summary>
            /// 物理形态
            /// </summary>
            public string PhysicalForm { get; set; }
            /// <summary>
            /// 清洁工具特点
            /// </summary>
            public string FeaturesOfCleaningTools { get; set; }
            /// <summary>
            /// 容量(毫升)
            /// </summary>
            public string Capacity { get; set; }
            /// <summary>
            /// 层数
            /// </summary>
            public string NumberOfLayers { get; set; }
            /// <summary>
            /// 电压
            /// </summary>
            public string Voltage { get; set; }
            /// <summary>
            /// 面料
            /// </summary>
            public string Fabric { get; set; }
            /// <summary>
            /// 适用孕期阶段
            /// </summary>
            public string ApplicablePregnancyStage { get; set; }
            /// <summary>
            /// 厚薄程度
            /// </summary>
            public string Thickness { get; set; }
            /// <summary>
            /// 图案
            /// </summary>
            public string Pattern { get; set; }
            /// <summary>
            /// 腰围(下装)(厘米)
            /// </summary>
            public string Waist { get; set; }
            /// <summary>
            /// 裤长类型
            /// </summary>
            public string PantsLengthType { get; set; }
            /// <summary>
            /// 闭合方式
            /// </summary>
            public string ClosedWay { get; set; }
            /// <summary>
            /// 内部结构
            /// </summary>
            public string InternalStructure { get; set; }
            /// <summary>
            /// 鞋子款式
            /// </summary>
            public string ShoeStyle { get; set; }
            /// <summary>
            /// 鞋头款式
            /// </summary>
            public string ToeStyle { get; set; }
            /// <summary>
            /// 鞋面材质
            /// </summary>
            public string UpperMaterial { get; set; }
            /// <summary>
            /// 全胸围(厘米)
            /// </summary>
            public string FullBust { get; set; }
            /// <summary>
            /// 肩宽(厘米)	
            /// </summary>
            public string ShoulderWidth { get; set; }
            /// <summary>
            /// 腰围(厘米)
            /// </summary>
            public string WaistCircumference { get; set; }
            /// <summary>
            /// 臀围(厘米)
            /// </summary>
            public string HipCircumference { get; set; }
            /// <summary>
            /// 衣长(厘米)
            /// </summary>
            public string ClothesLength { get; set; }
            /// <summary>
            /// 袖长(厘米)
            /// </summary>
            public string SleeveLength { get; set; }
            /// <summary>
            /// 服装类名词
            /// </summary>
            public string ClothingNouns { get; set; }
            /// <summary>
            /// 裤长(厘米)
            /// </summary>
            public string PantsLength { get; set; }
            /// <summary>
            /// 裙长(厘米)
            /// </summary>
            public string SkirtLength { get; set; }
            /// <summary>
            /// 裙长类型
            /// </summary>
            public string SkirtLengthType { get; set; }
            /// <summary>
            /// 主要成分含量
            /// </summary>
            public string MainIngredientContent { get; set; }
            /// <summary>
            /// 袜子类型
            /// </summary>
            public string SocksType { get; set; }
            /// <summary>
            /// 光源颜色
            /// </summary>
            public string LightSourceColor { get; set; }
            /// <summary>
            /// 是否可折叠
            /// </summary>
            public string IsFolded { get; set; }
            /// <summary>
            /// 填充物
            /// </summary>
            public string Stuffing { get; set; }
            /// <summary>
            /// 适用人群
            /// </summary>
            public string ForPeople { get; set; }
            /// <summary>
            /// 搭建情况
            /// </summary>
            public string ConstructionSituation { get; set; }
            /// <summary>
            /// 帐篷结构
            /// </summary>
            public string TentStructure { get; set; }
            /// <summary>
            /// 适用人数
            /// </summary>
            public string ApplicableNumber { get; set; }
            /// <summary>
            /// DIY使用场景
            /// </summary>
            public string DIYUsageScenarios { get; set; }
            /// <summary>
            /// 适用空间
            /// </summary>
            public string ApplicableSpace { get; set; }
            /// <summary>
            /// 额定功率(瓦)
            /// </summary>
            public string RatedPower { get; set; }
            /// <summary>
            /// 小家电名词
            /// </summary>
            public string SmallAppliancesNouns { get; set; }
            /// <summary>
            /// 接口类型
            /// </summary>
            public string InterfaceType { get; set; }
            /// <summary>
            /// 组合形式
            /// </summary>
            public string Combination { get; set; }
            /// <summary>
            /// 质地
            /// </summary>
            public string Texture { get; set; }
            /// <summary>
            /// 健身器材分类
            /// </summary>
            public string FitnessEquipmentClassification { get; set; }
            /// <summary>
            /// 健身效果
            /// </summary>
            public string FitnessEffect { get; set; }
            /// <summary>
            /// 适用部位
            /// </summary>
            public string ApplicableParts { get; set; }
            /// <summary>
            /// 直径
            /// </summary>
            public string Diameter { get; set; }
            /// <summary>
            /// 适用舞蹈类型
            /// </summary>
            public string ApplicableDanceType { get; set; }
            /// <summary>
            /// 运动系列
            /// </summary>
            public string SportsSeries { get; set; }
            /// <summary>
            /// 提醒语
            /// </summary>
            public string Reminder { get; set; }
            /// <summary>
            /// 是否带帽子
            /// </summary>
            public string IsWearHat { get; set; }
            /// <summary>
            /// 屏幕尺寸(英寸)
            /// </summary>
            public string ScreenSize { get; set; }
            /// <summary>
            /// 商品类型名称
            /// </summary>
            public string ProductTypeName { get; set; }
            /// <summary>
            /// 是否线控
            /// </summary>
            public string IsWireControl { get; set; }
            /// <summary>
            /// 耳机佩戴方式
            /// </summary>
            public string TheWayOfWearHeadset { get; set; }
            /// <summary>
            /// 适用型号
            /// </summary>
            public string ApplicableModel { get; set; }
            /// <summary>
            /// 刷头材质
            /// </summary>
            public string BrushHeadMaterial { get; set; }
            /// <summary>
            /// 摄影器材种类
            /// </summary>
            public string TypesOfPhotographicEquipment { get; set; }
            /// <summary>
            /// 耳机配件类型
            /// </summary>
            public string HeadphoneAccessoryType { get; set; }
            /// <summary>
            /// 数码类名词
            /// </summary>
            public string DigitalNouns { get; set; }
            /// <summary>
            /// 适用床尺寸
            /// </summary>
            public string ApplicableBedSize { get; set; }
            /// <summary>
            /// 流行款式
            /// </summary>
            public string PopularStyles { get; set; }
            /// <summary>
            /// 个人护理电器名词
            /// </summary>
            public string PersonalCareElectricalNouns { get; set; }
            /// <summary>
            /// 是否可拆
            /// </summary>
            public string IsDetachable { get; set; }
            /// <summary>
            /// 最小出餐量
            /// </summary>
            public string MinimumMealSize { get; set; }
            /// <summary>
            /// 时间设定
            /// </summary>
            public string TimeSetting { get; set; }
            /// <summary>
            /// 最大出餐量
            /// </summary>
            public string MaximumMealSize { get; set; }
            /// <summary>
            /// 是否带饮水器
            /// </summary>
            public string IsBrinkingFountain { get; set; }
            /// <summary>
            /// 动力来源
            /// </summary>
            public string PowerSource { get; set; }
            /// <summary>
            /// 面料功能
            /// </summary>
            public string FabricFunction { get; set; }
            /// <summary>
            /// 清洗方式
            /// </summary>
            public string CleaningMethod { get; set; }
            /// <summary>
            /// 弹性
            /// </summary>
            public string Elasticity { get; set; }
            /// <summary>
            /// 款型
            /// </summary>
            public string StyleTouch { get; set; }
            /// <summary>
            /// 袖型
            /// </summary>
            public string SleeveType { get; set; }
            /// <summary>
            /// 细节
            /// </summary>
            public string Detail { get; set; }
            /// <summary>
            /// 工艺
            /// </summary>
            public string Craft { get; set; }
            /// <summary>
            /// 服装功能性
            /// </summary>
            public string ClothingFunctionality { get; set; }
            /// <summary>
            /// 门襟类型
            /// </summary>
            public string PlacketType { get; set; }
            /// <summary>
            /// 印花主题
            /// </summary>
            public string PrintingTheme { get; set; }
            /// <summary>
            /// 裤型
            /// </summary>
            public string PantsType { get; set; }
            /// <summary>
            /// 镜架材质
            /// </summary>
            public string FrameMmaterial { get; set; }
            /// <summary>
            /// 零配件分类
            /// </summary>
            public string PartsClassification { get; set; }
            /// <summary>
            /// 是否自动
            /// </summary>
            public string IsAuto { get; set; }
            /// <summary>
            /// 内饰贴纸适用部位
            /// </summary>
            public string ApplicablePartsOfInteriorStickers { get; set; }
            /// <summary>
            /// 表层材质
            /// </summary>
            public string SurfaceMaterial { get; set; }
            /// <summary>
            /// 能力培养
            /// </summary>
            public string AbilityTraining { get; set; }
            /// <summary>
            /// 早教方向分类
            /// </summary>
            public string EarlyEducationDirectionClassification { get; set; }
            /// <summary>
            /// 模型拼装类型
            /// </summary>
            public string ModelAssemblyType { get; set; }
            /// <summary>
            /// 玩具类型
            /// </summary>
            public string ToyType { get; set; }
            /// <summary>
            /// 防护功能
            /// </summary>
            public string ProtectionFunction { get; set; }
            /// <summary>
            /// 腰型
            /// </summary>
            public string WaistType { get; set; }
            /// <summary>
            /// 明星相关
            /// </summary>
            public string StarRelated { get; set; }
            /// <summary>
            /// 耳机发声原理
            /// </summary>
            public string ThePrincipleOfEarphoneSound { get; set; }
            /// <summary>
            /// 耳机类别
            /// </summary>
            public string HeadphoneCategory { get; set; }
            /// <summary>
            /// 种类
            /// </summary>
            public string Kind { get; set; }
            /// <summary>
            /// 适用场景
            /// </summary>
            public string ApplicableScene { get; set; }
            /// <summary>
            /// 适用对象
            /// </summary>
            public string Suitable { get; set; }
            /// <summary>
            /// 适用范围
            /// </summary>
            public string ScopeOfApplication { get; set; }
            /// <summary>
            /// 产品类别
            /// </summary>
            public string ProductCategory { get; set; }
            /// <summary>
            /// 后跟高度
            /// </summary>
            public string HeelHeight { get; set; }
            /// <summary>
            /// 适合车型
            /// </summary>
            public string SuitableModels { get; set; }
            /// <summary>
            /// 键盘接口类型
            /// </summary>
            public string KeyboardInterfaceType { get; set; }
            /// <summary>
            /// 内长(厘米)
            /// </summary>
            public string InternalLength { get; set; }
            /// <summary>
            /// 供电方式
            /// </summary>
            public string PowerSupply { get; set; }
            /// <summary>
            /// 保温杯样式
            /// </summary>
            public string InsulationCupStyle { get; set; }
            /// <summary>
            /// 保鲜袋类别
            /// </summary>
            public string FreshkeepingBagCategory { get; set; }
            /// <summary>
            /// 功能箱包类型
            /// </summary>
            public string FunctionLuggageType { get; set; }
            /// <summary>
            /// 放置方式
            /// </summary>
            public string PlacementMethod { get; set; }
            /// <summary>
            /// 携带方式
            /// </summary>
            public string CarryingMethod { get; set; }
            /// <summary>
            /// 显示方式
            /// </summary>
            public string DisplayMethod { get; set; }
            /// <summary>
            /// 适用手机机型
            /// </summary>
            public string ApplicableMobilePhoneModels { get; set; }
            /// <summary>
            /// 承重
            /// </summary>
            public string LoadBearing { get; set; }
            /// <summary>
            /// 流行
            /// </summary>
            public string Popular { get; set; }
            /// <summary>
            /// 外形
            /// </summary>
            public string OuterShape { get; set; }
            /// <summary>
            /// 控制方式
            /// </summary>
            public string WaytoControl { get; set; }
            /// <summary>
            /// 儿童包类型
            /// </summary>
            public string ChildBagType { get; set; }
            /// <summary>
            /// 防水性能
            /// </summary>
            public string WaterproofPerformance { get; set; }
            /// <summary>
            /// 耳饰时尚设计
            /// </summary>
            public string FashionDesignOfEarrings { get; set; }
            /// <summary>
            /// 适用音乐类型
            /// </summary>
            public string ApplicableMusicType { get; set; }
            /// <summary>
            /// 功能
            /// </summary>
            public string Function { get; set; }
        }
    }
}
