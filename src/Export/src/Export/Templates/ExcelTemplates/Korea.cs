﻿using ERP.Export.ExportCommon;
using ERP.Models.Export;
using ERP.Export.Templates;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class Korea : ExcelBase<Korea, Korea.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.Korea;
    public override string Name => "Korea";

    public Korea(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }

    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
    {
        ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
        if (Item.IsNull())
        {
            _Logger.LogInformation($"{Name} GetData ParameterItem Is Null");
            return null;
        }
        var Product = new List<List<string?>>();
        string Sku = Data.Sku;
        //描述
        string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, null);
        //卖点
        var BulletPoint = _Common.GetBulletPoint(Data.BulletPoint, Data.SearchTerms, Data.Description, role.SketchMode);
        //关键字
        var Keyword = _Common.GetKeywords(Data.BulletPoint, Data.SearchTerms, Data.Description, role.KeywordMode);
        var Pro = new List<string?>();
        Pro.Add(Item.Original_number);
        Pro.Add(Sku);
        Pro.Add("");
        Pro.Add(Item.Category_No);
        Pro.Add(Data.Name);
        string Price = await _Common.GetMainSalePrice(Data.Price, Rate);
        Pro.Add(Price);
        Pro.Add("3000");
        ExportCommon.ProductExportHelper.SetCommonCell(Pro, "", 2);
        Pro.Add(Item.origin);
        ExportCommon.ProductExportHelper.SetCommonCell(Pro, "", 3);
        Pro.Add(Data.ShowImage);
        var Images = new List<string>();
        Data.Products?.ForEach((r) => Images.Add(r.MainImage));
        ExportCommon.ProductExportHelper.SetCommonImage(Pro, Images, 5);
        Pro.Add(Description);
        ExportCommon.ProductExportHelper.SetCommonCell(Pro, "", 2);
        if (Data.Products.IsNull() || Data.Products.Count == 0) ExportCommon.ProductExportHelper.SetCommonCell(Pro, "", 3);
        else
        {
            Pro.Add("조합형");
            string AttrName = "";
            string AttrDetail = "";
            if (Data.Attribute.IsNotNull())
            {
                AttrName = string.Join("\r\n", Data.Attribute.Keys);
                Data.Attribute.ForEach((r) =>
                {
                    if (AttrDetail.Length > 0) AttrDetail += "*";
                    AttrDetail += r.Value.Last();
                });
            }
            Pro.Add(AttrName);
            var Image = Data.ShowImage;
            Pro.Add($"{AttrDetail}**{Price}*{Data.Quantity}*{Image}");
        }
        Pro.Add("N");
        ExportCommon.ProductExportHelper.SetCommonCell(Pro, "", 18);
        Pro.Add("35");
        Pro.Add("Y");
        ExportCommon.ProductExportHelper.SetCommonCell(Pro, "", 30);
        Product.Add(Pro);

        Dictionary<int, List<List<string?>>>? Dic = new Dictionary<int, List<List<string?>>>();
        Dic.Add(0, Product);
        return Dic;
    }
    public class ParameterItem //: JsonObject<ParameterItem>
    {
        public string? unit { get; set; }

        public string? Original_number { get; set; }

        public string? Category_No { get; set; }

        public string? origin { get; set; }
    }
}
