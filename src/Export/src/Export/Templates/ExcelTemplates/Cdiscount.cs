﻿using ERP.Export.ExportCommon;
using ERP.Models.Export;
using ERP.Export.Templates;

namespace ERP.Export.V4_Export.ExcelTemplates
{
    public class Cdiscount : ExcelBase<Cdiscount, Cdiscount.ParameterItem>
    {
        public override TemplateType TemplateType => TemplateType.Cdiscount;
        public override string Name => "Cdiscount";
        public override Dictionary<int, int> StartRowIndex => new Dictionary<int, int>() { { 0, 3 } };

        public Cdiscount(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }
        
        public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
        {
            Dictionary<int, List<List<string?>>>? Dic = new Dictionary<int, List<List<string?>>>();
            ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
            if (Item is null)
            {
                _Logger.LogInformation($"{Name} GetData Method ParameterItem Is Null");
                return null!;
            }
            var Product = new List<List<string?>>();
            string Sku = Data.Sku;
            if (Data.Products.IsNull() || Data.Products.Count == 0)
            {
                string Price = await _Common.GetMainSalePrice(Data.Price, Rate);
                string Cost = _Common.GetMainCostPrice(Data.Price, Rate);
                ExcelProduct MainPro = new ExcelProduct(Data.Name, Data.Description, Data.BulletPoint, Data.SearchTerms, "", "", Sku, Data.ShowImage, null, Data.Quantity, Price, Cost, "", null, Data.Source);
                var ChileExcelPro = await GetProduct(MainPro, Item, role, Sku, true);
                if (ChileExcelPro.IsNotNull()) Product.Add(ChileExcelPro);
            }
            else
            {
                foreach (var p in Data.Products)
                {
                    ExcelProduct ChildPro = _Common.GetCommonData(p, Data.Attribute, Data.Source, Sku);
                    var ChileExcelPro = await GetProduct(ChildPro, Item, role, Sku, false);
                    if (ChileExcelPro.IsNotNull()) Product.Add(ChileExcelPro);
                }
            }
            Dic.Add(0, Product);
            return Dic;
        }
        private Task<List<string?>> GetProduct(ExcelProduct Data, ParameterItem Item, ExcleRole role, string Sku, bool IsMain)
        {
            //描述
            string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, Data.Images);
            //卖点
            var BulletPoint = _Common.GetBulletPoint(Data.BulletPoint, Data.SearchTerms, Data.Description, role.SketchMode);
            //关键字
            var Keyword = _Common.GetKeywords(Data.BulletPoint, Data.SearchTerms, Data.Description, role.KeywordMode);
            var Pro = new List<string?>();
            Pro.Add(Data.Sku);
            Pro.Add(Data.ProduceCode);
            Pro.Add(Item.brand_name);
            if (IsMain) Pro.Add("Standard");
            else Pro.Add("Variant");
            Pro.Add(Item.category_code);
            string Title = Data.Name;
            if (Title.Length > 27) Title = Title.Substring(0, 27) + "...";
            Pro.Add(Title);
            string LongTitle = Data.Name + "\r\n" + string.Join(" ", Keyword).TrimEnd();
            if (LongTitle.Length > 132) LongTitle = LongTitle.Substring(0, 132);
            Pro.Add(LongTitle);
            string Sketch = string.Join("\r\n", BulletPoint);
            Pro.Add(Sketch);
            Pro.Add(Data.MainImage);
            if (IsMain) ProductExportHelper.SetCommonCell(Pro, "", 3);
            else
            {
                Pro.Add(Sku);
                Pro.Add(Data.Size);
                Pro.Add(Data.Color);
            }
            Pro.Add(Description);
            ProductExportHelper.SetCommonImage(Pro, Data.Images, 3);
            ProductExportHelper.SetCommonCell(Pro, "", 2);
            ProductExportHelper.SetCommonCell(Pro, "", 4);
            //if (Item.length== "0") Pro.Add("");
            //else Pro.Add(Item.length);
            //if (Item.width == "0") Pro.Add("");
            //else Pro.Add(Item.width);
            //if (Item.height == "0") Pro.Add("");
            //else Pro.Add(Item.height);
            //if (Item.weight == "0") Pro.Add("");
            //else Pro.Add(Item.weight);
            Pro.Add(Data.Color);
            Pro.Add(Item.genre);
            Pro.Add(Item.Public);
            Pro.Add(Item.sports);
            Pro.Add(Item.avertissement);
            return Task.FromResult(Pro);
        }
        public override async Task<Dictionary<int, List<List<string?>>>?> GetOhterData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
        {
            ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
            if (Item.IsNull())
            {
                _Logger.LogInformation($"{Name} GetOhterData Method ParameterItem Is Null");
                return null!;
            }
            Dictionary<int, List<List<string?>>>? Dic = new Dictionary<int, List<List<string?>>>();
            var Pros = new List<List<string?>>();
            string Sku = Data.Sku;
            if (Data.Products.IsNull() || Data.Products.Count == 0)
            {
                var Pro = new List<string?>();
                Pro.Add(Sku);
                string EAN = "";
                if (Data.Products.IsNotNull() && Data.Products.Count == 1) EAN = Data.Products[0].ProduceCode;
                Pro.Add(EAN);
                Pro.Add(Data.Quantity.ToString());
                double Price = Convert.ToDouble(_Common.GetMainSalePrice(Data.Price, Rate)) + Item.VAT;
                Pro.Add(Price.ToString());
                Pros.Add(Pro);
            }
            else
            {
                foreach (var p in Data.Products)
                {
                    var Pro = new List<string?>();
                    Pro.Add(p.Sku);
                    Pro.Add(p.ProduceCode);
                    Pro.Add(p.Quantity.ToString());
                    double Price = Convert.ToDouble(ProductExportHelper.GetDataPrice(p.Price, Rate)) + Item.VAT;
                    Pro.Add(Price.ToString());
                    Pros.Add(Pro);
                }
            }
            Dic.Add(0, Pros);
            return await Task.FromResult(Dic);
        }


        public class ParameterItem //: JsonObject<ParameterItem>
        {
            public string? brand_name { get; set; }

            public string? category_code { get; set; }

            public string? licence { get; set; }
            /// <summary>
            /// 增值税（价格表：输入的价格包括增值税）
            /// </summary>
            public double VAT { get; set; }
            //public string length { get; set; }
            //public string width { get; set; }
            //public string height { get; set; }
            //public string weight { get; set; }
            public string? principale { get; set; }
            public string? genre { get; set; }
            public string? Public { get; set; }
            public string? sports { get; set; }
            public string? avertissement { get; set; }
        }
    }
}
