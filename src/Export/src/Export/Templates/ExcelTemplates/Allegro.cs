﻿using ERP.Export.ExportCommon;
using ERP.Export.Templates;
using ERP.Models.Export;
using Microsoft.EntityFrameworkCore;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class Allegro : ExcelBase<Allegro, Allegro.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.Allegro;
    public override string Name => "Allegro";
    public Allegro(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }
    
    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
    {

        var Product = new List<List<string?>>();
        ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
        if (Item is null)
        {
            _Logger.LogInformation($"{Name} GetData Method: ParameterItem Is Null");
            //Log(I18N.Instance.Export_Log3);
            return null!;
        }
        var Sku = Data.Sku;


        if (Data.Products is not null)
        {
            foreach (var p in Data.Products)
            {
                string Description = _Common.GetDescription(p.Description, string.Join(";", p.BulletPoint), p.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, p.Images);
                //卖点
                var BulletPoint = _Common.GetBulletPoint(p.BulletPoint, p.SearchTerms, p.Description, role.SketchMode).Where((r) => r.IsNotWhiteSpace()).ToList();
                //关键字      BulletPoint[SearchTerms] ? BulletPoint.Values[Description]
                var Keyword = _Common.GetKeywords(p.BulletPoint, p.SearchTerms, p.Description, role.KeywordMode).Where((r) => r.IsNotWhiteSpace()).ToList();
                ExcelProduct ChildPro = _Common.GetCommonData(p, Data.Attribute, Data.Source, Sku);
                var Pro = new List<string?>();
                ProductExportHelper.SetCommonCell(Pro, "", 7);
                Pro.Add(ChildPro.ProduceCode);
                Pro.Add(Item.main_category);
                Pro.Add(Item.leaf_category);
                Pro.Add(Item.seller_sku);
                Pro.Add(ChildPro.Quantity.ToString());
                string Price = await _Common.GetMainSalePrice(Data.Price, Rate);
                Pro.Add(Price);
                Pro.Add(ChildPro.Name);
                Pro.Add(string.Join("|", ChildPro.Images));
                Pro.Add(Description);
                Pro.Add(Item.shipping_price_list);
                Pro.Add(Item.dispatch_time);
                Pro.Add(Item.country);
                Pro.Add(Item.province);
                Pro.Add(Item.postal_code);
                Pro.Add(Item.city);
                Pro.Add(Item.invoice);
                Pro.Add(Item.offer_subject);
                Pro.Add(Item.vat_rate);
                Pro.Add(Item.base_of_vat_exemption);
                Pro.Add(Item.returns_terms);
                Pro.Add(Item.complaints_terms);
                Pro.Add(Item.warranties);
                Pro.Add(Item.state_dict);
                Pro.Add(Item.original_manufacturer_s_packaging_dict);
                Pro.Add(Item.number_of_pieces_on_offer_szt_text);
                Pro.Add(Item.packaging_status_dict);
                Pro.Add(Item.gender_dict);
                Pro.Add(Item.manufacturer_code_text);
                Pro.Add(Item.brand_dict);
                Pro.Add(ChildPro.Color);
                Pro.Add(ChildPro.Size);
                Pro.Add(Item.main_pattern_dict);
                Pro.Add(Item.cut_dict);
                Pro.Add(Item.neckline_dict);
                Pro.Add(Item.sleeve_dict);
                Pro.Add(Item.main_fabric_dict);
                Pro.Add(Item.additional_features_dict);
                Pro.Add(Item.model_text);
                Pro.Add(Item.height_cm_text);
                Pro.Add(Item.kind_dict);
                Pro.Add(Item.width_long_side_cm_text);
                Pro.Add(Item.depth_short_side_cm_text);
                Pro.Add(Item.main_material_dict);
                Pro.Add(Item.main_color_dict);
                Pro.Add(Item.material_dict);
                Pro.Add(Item.bracelet_length_mm_text);
                Pro.Add(Item.total_length_cm_text);
                Product.Add(Pro);
            }
        }
        var Dic = new Dictionary<int, List<List<string?>>>();
        Dic.Add(0, Product);
        return Dic;
    }
    public class ParameterItem
    {
        public string? main_category { get; set; }
        public string? leaf_category { get; set; }
        public string? seller_sku { get; set; }
        public string? shipping_price_list { get; set; }
        public string? dispatch_time { get; set; }
        public string? country { get; set; }
        public string? province { get; set; }
        public string? postal_code { get; set; }
        public string? city { get; set; }
        public string? invoice { get; set; }
        public string? offer_subject { get; set; }
        public string? vat_rate { get; set; }
        public string? base_of_vat_exemption { get; set; }
        public string? returns_terms { get; set; }
        public string? complaints_terms { get; set; }
        public string? warranties { get; set; }
        public string? state_dict { get; set; }
        public string? original_manufacturer_s_packaging_dict { get; set; }
        public string? number_of_pieces_on_offer_szt_text { get; set; }
        public string? packaging_status_dict { get; set; }
        public string? gender_dict { get; set; }
        public string? manufacturer_code_text { get; set; }
        public string? brand_dict { get; set; }
        public string? main_pattern_dict { get; set; }
        public string? cut_dict { get; set; }
        public string? neckline_dict { get; set; }
        public string? sleeve_dict { get; set; }
        public string? main_fabric_dict { get; set; }
        public string? additional_features_dict { get; set; }
        public string? model_text { get; set; }
        public string? height_cm_text { get; set; }
        public string? kind_dict { get; set; }
        public string? width_long_side_cm_text { get; set; }
        public string? depth_short_side_cm_text { get; set; }
        public string? main_material_dict { get; set; }
        public string? main_color_dict { get; set; }
        public string? material_dict { get; set; }
        public string? bracelet_length_mm_text { get; set; }
        public string? total_length_cm_text { get; set; }
    }
}
