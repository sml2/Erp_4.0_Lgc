﻿using ERP.Export.ExportCommon;
using ERP.Models.Export;
using ERP.Export.Templates;
using System.ComponentModel.DataAnnotations;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class Shopee : ExcelBase<Shopee, Shopee.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.Shopee;
    public override string Name => "Shopee";

    public Shopee(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }
    
    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
    {
        ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
        if (Item is null)
        {
            _Logger.LogInformation($"{Name} GetData Method ParameterItem Is Null");
            return null!;
        }
        List<List<string>> Product = new List<List<string>>();
        string Sku = Data.Sku;
        if (Data.Products.IsNull() || Data.Products.Count == 0)
        {
            string Price = await _Common.GetMainSalePrice(Data.Price, Rate);
            string Cost = _Common.GetMainCostPrice(Data.Price, Rate);
            ExcelProduct MainPro = new ExcelProduct(Data.Name, Data.Description, Data.BulletPoint, Data.SearchTerms, "", "", Sku, Data.ShowImage, null, Data.Quantity, Price, Cost, "", null, Data.Source);
            var MainExcelPro = await GetProduct(MainPro, Item, Rate, role, Sku, true);
            if (MainExcelPro.IsNotNull()) Product.Add(MainExcelPro);
        }
        else
        {
            foreach (var p in Data.Products)
            {
                ExcelProduct ChildPro = _Common.GetCommonData(p, Data.Attribute, Data.Source, Sku);
                var ChileExcelPro = await GetProduct(ChildPro, Item, Rate, role, Sku, false);
                if (ChileExcelPro.IsNotNull()) Product.Add(ChileExcelPro);
            }
        }
        var Dic = new Dictionary<int, List<List<string>>>();
        Dic.Add(0, new List<List<string>>());
        Dic.Add(1, Product);
        return Dic;
    }
    private async Task<List<string>> GetProduct(ExcelProduct Data, ParameterItem Item, List<Unit> Rate, ExcleRole role, string Sku, bool IsMain)
    {
        //描述
        string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, Data.Images);
        //卖点
        var BulletPoint = _Common.GetBulletPoint(Data.BulletPoint, Data.SearchTerms, Data.Description, role.SketchMode);
        //关键字
        var Keyword = _Common.GetKeywords(Data.BulletPoint, Data.SearchTerms, Data.Description, role.KeywordMode);
        List<string> Pro = new List<string>();
        Pro.Add(Item.Category);
        Pro.Add(Data.Name);
        Pro.Add(Description);
        Pro.Add("");
        Pro.Add(Sku);
        Dictionary<string, string> Dic = new Dictionary<string, string>();
        if (Data.Color.IsNotWhiteSpace()) Dic.Add("Color", Data.Color);
        if (Data.Size.IsNotWhiteSpace()) Dic.Add("Size", Data.Size);
        int Index = 0;
        if (Dic.Count > 0)
        {
            Dic.ForEach((r) =>
            {
                Pro.Add(r.Key);
                Pro.Add(r.Value);
                if (Index == 0) Pro.Add(Data.MainImage);
                Index++;
            });
        }
        for (int i = 1; i <= 2 - Dic.Count; i++)
        {
            ProductExportHelper.SetCommonCell(Pro, "", 2);
            if (Index == 0) Pro.Add(Data.MainImage);
            Index++;
        }
        string Price = "";
        if (IsMain) Price = Data.Price;
        else Price = ProductExportHelper.GetDataPrice(Data.Price, Rate);
        Pro.Add(Price);
        Pro.Add(Data.Quantity.ToString());
        Pro.Add(Data.Sku);
        Pro.Add(Data.MainImage);
        ProductExportHelper.SetCommonImage(Pro, Data.Images, 8);
        Pro.Add(Item.Weight);
        Pro.Add(Item.Length);
        Pro.Add(Item.Width);
        Pro.Add(Item.Height);
        Pro.Add(Item.Logistics);
        Pro.Add(Item.Singpost);
        Pro.Add(Item.Shipping);
        Pro.Add(Item.DTS);
        return Pro;
    }
    public class ParameterItem //: JsonObject<ParameterItem>
    {
        public string unit { get; set; }
        [Required]
        public string Category { get; set; } = null!;

        public string Variation { get; set; }
        [Required]
        public string Weight { get; set; } = null!;

        public string Height { get; set; }

        public string Length { get; set; }

        public string Width { get; set; }

        public string Logistics { get; set; }

        public string Singpost { get; set; }

        public string Shipping { get; set; }

        public string DTS { get; set; }
    }
}
