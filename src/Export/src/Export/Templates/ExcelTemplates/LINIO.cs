﻿using ERP.Models.Export;
using ERP.Export.Templates;
using System.ComponentModel.DataAnnotations;
using ERP.Export.ExportCommon;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class LINIO : ExcelBase<LINIO, LINIO.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.LINIO;

    public override string Name => "LINIO";

    public LINIO(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }

    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
    {
        ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
        if (Item is null)
        {
            _Logger.LogInformation($"{Name} GetData Method ParameterItem Is Null");
            return null!;
        }
        List<List<string>> Product = new List<List<string>>();
        string Sku = Data.Sku;
        //描述
        string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, null);
        //卖点
        var BulletPoint = _Common.GetBulletPoint(Data.BulletPoint, Data.SearchTerms, Data.Description, role.SketchMode);
        //关键字
        var Keyword = _Common.GetKeywords(Data.BulletPoint, Data.SearchTerms, Data.Description, role.KeywordMode);
        List<string> Pro = new List<string>();
        Pro.Add(Data.Name);
        ExportCommon.ProductExportHelper.SetCommonCell(Pro, "", 2);
        string Sketch = "";
        BulletPoint.ForEach((r) => Sketch += $"<li>{r}</li>");
        Pro.Add(Sketch);
        string Price = await _Common.GetMainSalePrice(Data.Price, Rate);
        Pro.Add(Price);
        Pro.Add(Price);
        Pro.Add("");
        Pro.Add(Description);
        Pro.Add(Item.PackageContent);
        Pro.Add(Item.Brand);
        Pro.Add(Item.Packageweight.ToString());
        Pro.Add($"{Item.length}*{Item.width}*{Item.height}");
        Pro.Add(Sku);
        var Color = _Common.GetAttributes(Data.Attribute, _Common.Colors);
        var Size = _Common.GetAttributes(Data.Attribute, ProductExportHelper.Sizes);
        Pro.Add(Color.Count > 0 ? Color.Last() : "");
        Pro.Add("");
        Pro.Add(Data.ShowImage);
        Pro.Add(Data.Source);
        Pro.Add(Data.ShowImage);
        Pro.Add(string.Join(",", Color));
        Pro.Add(string.Join(",", Size));
        ExportCommon.ProductExportHelper.SetCommonCell(Pro, "", 18);
        Pro.Add(Item.Category);
        string ProduceCode = "";
        if (Data.Products.IsNotNull() && Data.Products.Count > 0) ProduceCode = Data.Products[0].ProduceCode;
        Pro.Add(ProduceCode);
        string ProductType = "";
        if (ProduceCode.Length == 12) ProductType = "UPC";
        else if (ProduceCode.Length == 13) ProductType = "EAN";
        Pro.Add(ProductType);
        Product.Add(Pro);
        var Dic = new Dictionary<int, List<List<string>>>();
        Dic.Add(0, Product);
        return Dic;
    }
    public class ParameterItem //: JsonObject<ParameterItem>
    {
        public string unit { get; set; }
        [Required]
        public string Brand { get; set; } = null!;

        public string Category { get; set; }

        public int Packageweight { get; set; }

        public int height { get; set; }

        public int length { get; set; }

        public int width { get; set; }

        public string PackageContent { get; set; }
    }
}
