﻿using ERP.Export.ExportCommon;
using ERP.Models.Export;
using ERP.Export.V4_Export.Interfaces;
using Newtonsoft.Json;
using System.Text;
using common = ERP.Export.ExportCommon.ProductExportHelper;
using ERP.Export.Templates;

namespace ERP.Export.V4_Export.ExcelTemplates
{
    public class LazadaMotherBaby : ExcelBase<LazadaMotherBaby>
    {
        public override TemplateType TemplateType => TemplateType.Lazada_Mother_Baby;

        public override string Name => "Lazada Mother & Baby表";

        public LazadaMotherBaby(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }
        public override async Task<bool> Verify(string Parameter)
        {
            ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
            if (Item.IsNull())
            {
                _Logger.LogInformation($"{Name} Verify Method ParameterItem Is Null");
                return false;
            }
            if (Item.primary_category.IsNullOrEmpty())
            {
                _Logger.LogInformation($"{Name} Verify Method ParameterItem  Primary_Category is Empty!");
                return false;
            }
            return true;
        }

        public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
        {
            List<List<string>> Product = new List<List<string>>();
            ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
            if (Item is null)
            {
                _Logger.LogInformation($"{Name} ParameterItem Is Null");
                //Log(I18N.Instance.Export_Log3);
                return null!;
            }
            string Sku = Data.Sku;
            if (Data.Products.IsNull() || Data.Products.Count == 0)
            {
                string Price = "0", Cost = "0";
                if (Data.Price is not null && Data.Price.Sale.Max.Value > 0) Price = Data.Price.Sale.Max.Value.ToString();//sale
                if (Data.Price is not null && Data.Price.Cost.Max.Value > 0) Price = Data.Price.Cost.Max.Value.ToString();//cost
                ExcelProduct MainPro = new ExcelProduct(Data.Name, Data.Description, Data.BulletPoint, Data.SearchTerms, "", "", Sku, "", null, Data.Quantity, Price, Cost, "", null, Data.Source);
                var MainExcelPro = GetProduct(MainPro, Item, Rate, role);
                if (MainExcelPro is not null) Product.Add(MainExcelPro);
            }
            else
            {
                //子产品
                foreach (var p in Data.Products)
                {
                    ExcelProduct ChildPro = _Common.GetCommonData(p, Data.Attribute, Data.Source, Sku);
                    var ChileExcelPro = GetProduct(ChildPro, Item, Rate, role);
                    if (ChileExcelPro.IsNotNull()) Product.Add(ChileExcelPro);
                }
            }
            var Dic = new Dictionary<int, List<List<string>>>();
            Dic.Add(0, Product);
            return Dic;
        }
        private List<string> GetProduct(ExcelProduct Data, ParameterItem Item, List<Unit> Rate, ExcleRole role)
        {
            List<string> Pro = new List<string>();
            Pro.Add(Item.primary_category);
            Pro.Add(Item.brand);
            Pro.Add(Data.Color);
            Pro.Add(Item.model);
            common.SetCommonCell(Pro, "", 27);
            Pro.Add(Item.breast_pump_type);
            common.SetCommonCell(Pro, "", 2);
            Pro.Add(Item.tray);
            Pro.Add(Item.foldable);
            common.SetCommonCell(Pro, "", 17);
            Pro.Add(Data.Size);
            _Common.GetLazadaCommonCell(Pro, 30, Data, Item, role, Rate);
            Pro.Add(Item.baby_scent);
            Pro.Add("");
            Pro.Add(Item.volume);
            Pro.Add(Item.baby_flavor);
            Pro.Add(Item.volumetric_capacity);
            Pro.Add(Item.number_of_tins);
            Pro.Add(Item.milk_Formula_Flavor);
            Pro.Add(Item.volume_ml);
            Pro.Add(Item.weight);
            Pro.Add(Item.weight);
            Pro.Add(Item.size_diaper);
            Pro.Add(Item.diaper_pack_size);
            Pro.Add(Item.size_baby_clothing);
            Pro.Add(Item.size_baby_shoes);
            return Pro;
        }
        public class ParameterItem : IParameterItem//: JsonObject<ParameterItem>, 
        {
            public string primary_category { get; set; }

            public string model { get; set; }

            public string brand { get; set; }

            public string color_family { get; set; }

            public string package_content { get; set; }

            public string package_length { get; set; }

            public string package_width { get; set; }

            public string package_height { get; set; }

            public string package_weight { get; set; }

            public string taxes { get; set; }

            public string breast_pump_type { get; set; }

            public string tray { get; set; }

            public string foldable { get; set; }

            public string baby_scent { get; set; }

            public string volume { get; set; }

            public string baby_flavor { get; set; }

            public string volumetric_capacity { get; set; }

            public string number_of_tins { get; set; }

            public string milk_Formula_Flavor { get; set; }

            public string volume_ml { get; set; }

            public string weight { get; set; }

            public string size_diaper { get; set; }

            public string diaper_pack_size { get; set; }

            public string size_baby_clothing { get; set; }

            public string size_baby_shoes { get; set; }

        }
    }
}
