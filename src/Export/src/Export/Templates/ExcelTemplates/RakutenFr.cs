﻿using ERP.Export.ExportCommon;
using ERP.Export.Templates;
using ERP.Models.Export;
using Newtonsoft.Json;

namespace ERP.Export.V4_Export.ExcelTemplates;

using System;
using System.Collections.Generic;

public class Rakuten_Fr : ExcelBase<Rakuten_Fr, Rakuten_Fr.ParameterItems>
    {
        public override TemplateType TemplateType => TemplateType.Rakuten_Fr;

        public override string Name => "Rakuten_Fr";
        
        public Rakuten_Fr(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
        {
            ParameterItems Item = JsonConvert.DeserializeObject<ParameterItems>(Parameter);
            List<List<string>> Product = new List<List<string>>();
            string Sku = Data.Sku;
            //子产品
            if (Data.Products.IsNotNull() && Data.Products.Count > 0)
            {   
                foreach (var p in Data.Products)
                {
                    ExcelProduct ChildPro = _Common.GetCommonData(p, Data.Attribute, Data.Source, Sku);
                    var ChileExcelPro = GetProduct(ChildPro,Item, role,  Rate, Sku, Parameter);
                  
                    if (ChileExcelPro.IsNotNull()) Product.Add(ChileExcelPro);
                }
            }
            var Dic = new Dictionary<int, List<List<string>>>();
            Dic.Add(0, Product);
            return Dic;
        }
        private List<string?> GetProduct(ExcelProduct Data,ParameterItems Item, ExcleRole role, List<Unit> Rate, string Sku, string Parameter)
        {
            //描述
            string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, Data.Images);
            //卖点
            var BulletPoint = _Common.GetBulletPoint(Data.BulletPoint, Data.SearchTerms, Data.Description, role.SketchMode);
            //关键字
            var Keyword = _Common.GetKeywords(Data.BulletPoint, Data.SearchTerms, Data.Description, role.KeywordMode);
            //主产品
            List<string> Pro = new List<string>();
            string Price = string.Empty;
            //父SKU    4GdTBEWxuwk
            Sy.String sku = Sku;
            var sku_streamline = sku.Split("-")[0];

            Pro.Add(sku_streamline);   // ""
            //子SKU    4GdTBEWxuwk-0
            // Sy.String _Sku = Data.Sku;
            // int count = Data.Sku.Split('-').Length-1;
            // if (count > 1)
            // {
            //     string sku =  $"{_Sku.Split("-")[1].ToString()}-{_Sku.Split("-")[2].ToString()}";
            //     Pro.Add(sku);
            // }
            // else   Pro.Add(Data.Sku);  //"-0"
            
            // 客户需求：系统生成的子sku上传平台时无法关联为同一个产品
            //  子Sku构成： 主Sku+颜色+尺寸   鞋-Rosa-EUR 35
            if (Data.Color == "default")
            {
                if (Data.Size.IsNullOrEmpty())
                {
                    Pro.Add($"{sku_streamline}"); // 无变体产品
                }
            }
            else if (Data.Size.IsNullOrEmpty())  
            {
                Pro.Add($"{sku_streamline}_{Data.Color}"); // 单属性_ 颜色
            }
            else if ( Data.Color.IsNullOrEmpty())
            {
                Pro.Add($"{sku_streamline}_{Data.Size}");  // 单属性_ 尺码
            }
            else
            {
                Pro.Add($"{sku_streamline}_{Data.Size}_{Data.Color}");
            }

            // var sku = new StringBuilder("Sku");
            //
            // if (Data.Color != "default")
            // {
            //     if (Data.Color.IsNotEmpty())
            //     {
            //         sku.Append($"-{Data.Color}");
            //     }
            //     
            //     if (Data.Size.IsNotEmpty())
            //     {
            //         sku.Append($"-{Data.Size}");
            //     }
            // }
            //
            // Pro.Add(sku.ToString());
            
            //  title
            Pro.Add(Data.Name.Replace("@",""));
            //  category code   
            Pro.Add(Item.category_code);
            //  brand  
            Pro.Add("Générique");
            // short description 
            // 删掉肉眼可见的换行符，使用"<br />";不要在你的标题和描述中加入任何@字符。
            Pro.Add(Description.Replace("\r\n", "<br />").Replace("\n", "<br />").Replace("@",""));
            // URL Images (separated by # if many)
            // 无变体产品，Data.Images下为0，使用主图
            // https://fr.shopping.rakuten.com/offer/shop/5570536358/x.html
            if (Data.Images.Count == 0)  Pro.Add(Data.MainImage);
            else Pro.Add(string.Join("#", Data.Images));
            // price in euros 
            Price = ProductExportHelper.GetDataPrice(Data.Price, Rate); 
            Pro.Add(Price);
            // condition  
            Pro.Add("N");
            // quantity   
            Pro.Add(Data.Quantity.ToString());
            // advert comment 
            //  删掉肉眼可见的换行符，使用"<br />"
            Pro.Add("Nouveau<br />Expédition depuis la Chine; Livraison sous  6-13 jours ouvrables. Le transport professionnel est bien protégé.");
            // packaging weight in grammes (integer value)
            Pro.Add("");
            // Is it a refurbished product ?
            Pro.Add("");
            // RRP (recommended retail price in euros)
            Pro.Add("");
            // variation size
            Pro.Add(Data.Size);
            // variation Color
            Pro.Add(Data.Color);
            //  public    
            Pro.Add(Item._public);
            //  area of use   
            Pro.Add("Ville");
            //  unique variant barcode     EAN?
            Pro.Add(Data.ProduceCode);
            //  Promotion code
            Pro.Add("");
            // Custom Advert Description / Rich content in html
            Pro.Add("");
           
            
            return Pro;
        }
        public class ParameterItems : CommonParameter
        {
            public string category_code { get; set; }
            
            public string _public { get; set; }
        }
    }

