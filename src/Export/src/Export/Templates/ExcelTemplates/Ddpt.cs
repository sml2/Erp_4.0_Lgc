﻿using ERP.Export.ExportCommon;
using ERP.Models.Export;
using ERP.Export.Templates;
using System.ComponentModel.DataAnnotations;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class Ddpt : ExcelBase<Ddpt, Ddpt.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.Ddpt;
    public override string Name => "DDPT表";

    public Ddpt(IServiceProvider serviceProvider) : base(serviceProvider) { }

    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem data, ExcleRole role, string parameter, List<Unit> rate)
    {
        var item = ProductExportHelper.ToDeserialize<ParameterItem>(parameter);
        if (item is null)
        {
            _Logger.LogInformation("{Name} GetData Method ParameterItem Is Null", Name);
            return null!;
        }
        var product = new List<List<string>>();
        var sku = data.Sku;
        
        var price = await _Common.GetMainSalePrice(data.Price, rate);
        var cost = _Common.GetMainCostPrice(data.Price, rate);
        var mainPro = new ExcelProduct(data.Name, data.Description, data.BulletPoint, data.SearchTerms, "", "", sku, data.ShowImage, null, data.Quantity, price, cost, "", null, data.Source);
        var isSingle = data.Products.IsNull() || data.Products.Count <= 1;
        if (isSingle)
        {
            mainPro.Images = data.Products?.SelectMany(i => i.Images ?? new List<string>()).ToList() ?? new List<string>();
        }
        var mainExcelPro = GetProduct(mainPro, item, rate, role, sku, data.Products.IsNull() || data.Products.Count <= 1, true);
        if (mainExcelPro.IsNotNull()) product.Add(mainExcelPro);

        if (!isSingle)
        {
            for (var index = 0; index < data.Products!.Count; index++)
            {
                var p = data.Products[index];
                var childPro = _Common.GetCommonData(p, data.Attribute, data.Source, sku);
                var chileExcelPro = GetProduct(childPro, item, rate, role, sku, false, false);
                if (chileExcelPro.IsNotNull()) product.Add(chileExcelPro);
            }
        }
        var dic = new Dictionary<int, List<List<string>>>();
        dic.Add(0, product);
        return dic;
    }
    private List<string> GetProduct(ExcelProduct data, ParameterItem item, List<Unit> rate, ExcleRole role, string sku, bool isSingle, bool isMain)
    {
        //描述
        var description = _Common.GetDescription(data.Description, string.Join(";", data.BulletPoint),
            data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, data.Images);
        description = description.Replace("\r\n", "<br>\r\n").Replace("\n", "<br>\n");
        //卖点
        var bulletPoint = _Common.GetBulletPoint(data.BulletPoint, data.SearchTerms, data.Description, role.SketchMode);
        //关键字
        var keyword = _Common.GetKeywords(data.BulletPoint, data.SearchTerms, data.Description, role.KeywordMode);
        var pro = new List<string>();
        ProductExportHelper.SetCommonImage(pro, item.Categories, 6);
        pro.Add(data.Sku);
        pro.Add(item.Brand ?? string.Empty);
        pro.Add("EAN");
        pro.Add(data.ProduceCode);
        pro.Add(data.Source);
        ProductExportHelper.SetCommonCell(pro, "", 4);
        pro.Add(data.Name);
        pro.Add(isMain && !isSingle ? "": data.Quantity.ToString());
        pro.Add(data.MainImage ?? string.Empty);
        ProductExportHelper.SetCommonImage(pro, data.Images, 8);
        pro.Add(isSingle ? string.Empty : isMain ? "Parent" : "Child");
        pro.Add(isSingle || isMain ? string.Empty : sku);
        pro.Add(isSingle ? string.Empty : "variation");
        pro.Add(description);
        ProductExportHelper.SetCommonImage(pro, bulletPoint, 5);
        pro.Add(string.Join(',', keyword));
        pro.Add(data.Size);
        pro.Add(data.Color);
        ProductExportHelper.SetCommonCell(pro, "", 3);
        pro.Add(isSingle || !isMain ? item.MaxOrderQuantity : string.Empty);
        ProductExportHelper.SetCommonCell(pro, "", 3);
        if (isSingle || !isMain)
        {
            var cost = ProductExportHelper.GetDataPriceDecimal(data.Cost, rate);
            var price = ProductExportHelper.GetDataPrice(data.Price, rate);
            pro.Add(cost.IsSuccess ? (cost.Value * 2).ToString("0.00") : "");
            pro.Add(price);
            pro.Add(item.SaleStartDate);
            pro.Add(item.SaleEndDate);
        }
        else
        {
            ProductExportHelper.SetCommonCell(pro, "", 4);
        }
        pro.Add(item.Weight);
        pro.Add(item.Length);
        pro.Add(item.Width);
        pro.Add(item.Height);
        pro.Add(item.DimensionsUnitOfMeasure);
        pro.Add(item.WeightUnitOfMeasure);
        pro.Add("");
        pro.Add(item.PackageHeight);
        pro.Add(item.PackageWidth);
        pro.Add(item.PackageLength);
        pro.Add(item.PackageWeight);
        pro.Add(item.PackageWeightUnitOfMeasure);
        pro.Add(item.PackageDimensionsUnitOfMeasure);
        pro.Add(item.ShippingTemplate);
        pro.Add(item.DeliveryFrom);
        return pro;
    }
    public class ParameterItem
    {

        public string unit { get; set; } = null!;

        public List<string> Categories { get; set; } = null!;
        public string? Brand { get; set; }
        public string MaxOrderQuantity { get; set; } = "2";
        public string SaleStartDate { get; set; } = null!;
        public string SaleEndDate { get; set; } = null!;
        public string Weight { get; set; } = null!;
        public string Length { get; set; } = null!;
        public string Width { get; set; } = null!;
        public string Height { get; set; } = null!;
        public string DimensionsUnitOfMeasure { get; set; } = null!;
        public string WeightUnitOfMeasure { get; set; } = null!;
        public string PackageHeight { get; set; } = null!;
        public string PackageWidth { get; set; } = null!;
        public string PackageLength { get; set; } = null!;
        public string PackageWeight { get; set; } = null!;
        public string PackageWeightUnitOfMeasure { get; set; } = null!;
        public string PackageDimensionsUnitOfMeasure { get; set; } = null!;
        public string ShippingTemplate { get; set; } = null!;
        public string DeliveryFrom { get; set; } = null!;
    }
}
