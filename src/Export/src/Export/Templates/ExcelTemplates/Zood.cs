﻿using ERP.Export.ExportCommon;
using ERP.Models.Export;
using ERP.Export.Templates;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class Zood : ExcelBase<Zood, Zood.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.Zood;
    public override string Name => "zood模板";

    public Zood(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }

    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
    {
        List<List<string>> Product = new List<List<string>>();
        ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
        if (Item.IsNull())
        {
            _Logger.LogInformation($"{Name} GetData Method ParameterItem Is Null");
            return null;
        }
        var Sku = Data.Sku;
        var Attributes = Data.Attribute;
        if (Data.Products.IsNotNull() && Data.Products.Count > 0)
        {
            foreach (var p in Data.Products)
            {
                string Description = _Common.GetDescription(p.Description, string.Join(";", p.BulletPoint), p.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, p.Images);
                //卖点
                var BulletPoint = _Common.GetBulletPoint(p.BulletPoint, p.SearchTerms, p.Description, role.SketchMode).Where((r) => r.IsNotWhiteSpace()).ToList();
                //关键字
                var Keyword = _Common.GetKeywords(p.BulletPoint, p.SearchTerms, p.Description, role.KeywordMode).Where((r) => r.IsNotWhiteSpace()).ToList();

                List<string> Pro = new List<string>();
                Pro.Add(Sku);
                Pro.Add(p.Sku);
                Pro.Add(p.Name);
                Pro.Add(Item.CategoryID);
                Pro.Add(Item.Brand);
                string AttrStrName = "", AttrStrValue = "";
                for (int i = 0; i <= p.Values.Count - 1; i++)
                {
                    if (AttrStrName.Length > 0) AttrStrName += "||";
                    if (AttrStrValue.Length > 0) AttrStrValue += "||";
                    AttrStrName += Attributes.Keys.ToList()[i];
                    AttrStrValue += Attributes.Values.ToList()[i][p.Values[i]];
                }
                Pro.Add(AttrStrName);
                Pro.Add(p.MainImage);
                Pro.Add(AttrStrValue);
                //Price
                Pro.Add(ProductExportHelper.GetDataPrice(p.Cost, Rate, Item.Multiple));
                //*MSRP
                Pro.Add(ProductExportHelper.GetDataPrice(p.Cost, Rate, Item.Multiple * Item.MSRP));
                Pro.Add(Rate[0].Sign);
                Pro.Add(p.Quantity.ToString());
                Pro.Add(string.Join("||", Keyword));
                Pro.Add(string.Join("||", BulletPoint));
                Pro.Add(Description);
                Pro.Add(p.MainImage);
                Pro.Add(Item.SizeGuideUrl);
                Pro.Add(Item.hasBattery);
                Pro.Add(Item.hasPowder);
                Pro.Add(Item.hasLiquid);
                Pro.Add(Item.declaredValue);
                Pro.Add(Item.hsCode);
                Pro.Add(Item.packageHeight);
                Pro.Add(Item.packageLenght);
                Pro.Add(Item.packageWidth);
                Pro.Add(Item.packageWeight);
                ExportCommon.ProductExportHelper.SetCommonImage(Pro, p.Images, 8);
                Pro.Add(Item.MarketCode);
                Pro.Add(Item.Shippingfee);
                Product.Add(Pro);
            }
        }
        var Dic = new Dictionary<int, List<List<string>>>
        {
            { 0, Product }
        };
        return Dic;
    }
    public class ParameterItem //: JsonObject<ParameterItem>
    {
        public string CategoryID { get; set; }
        public string Brand { get; set; }
        public string Shippingfee { get; set; }
        //价格倍数 20211104新增
        public decimal Multiple { get; set; }
        public decimal MSRP { get; set; }
        public string SizeGuideUrl { get; set; }
        public string hasBattery { get; set; }
        public string hasPowder { get; set; }
        public string hasLiquid { get; set; }
        public string declaredValue { get; set; }
        public string hsCode { get; set; }
        public string packageHeight { get; set; }
        public string packageLenght { get; set; }
        public string packageWidth { get; set; }
        public string packageWeight { get; set; }
        public string MarketCode { get; set; }
    }
}
