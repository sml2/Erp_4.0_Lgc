﻿using ERP.Export.ExportCommon;
using ERP.Models.Export;
using ERP.Export.Templates;
using System.ComponentModel.DataAnnotations;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class EbayTemplate : ExcelBase<EbayTemplate, EbayTemplate.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.EbayTemplate;
    public override string Name => "FileExchangeBaseTemplate";

    public EbayTemplate(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }

    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
    {
        ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
        if (Item.IsNull())
        {
            //Log(I18N.Instance.Export_Log3);
            _Logger.LogInformation($"{Name} GetData ParameterItem Is Null");
            return null!;
        }
        var Product = new List<List<string?>>();
        string Sku = Data.Sku;
        string Price = await _Common.GetMainSalePrice(Data.Price, Rate);
        string Cost = _Common.GetMainCostPrice(Data.Price, Rate);
        ExcelProduct MainPro = new ExcelProduct(Data.Name, Data.Description, Data.BulletPoint, Data.SearchTerms, "", "", Sku, "", null, Data.Quantity, Price, Cost, "", null, Data.Source);
        var MainExcelPro = await GetProduct(MainPro, Item, role);
        if (MainExcelPro.IsNotNull()) Product.Add(MainExcelPro);
        Dictionary<int, List<List<string?>>>? Dic = new Dictionary<int, List<List<string?>>>();
        Dic.Add(0, Product);
        return Dic;
    }
    private Task<List<string?>> GetProduct(ExcelProduct Data, ParameterItem Item, ExcleRole role)
    {
        //描述
        string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, Data.Images);
        var Pro = new List<string?>();
        Pro.Add(Item.Action);
        Pro.Add(Item.Category);
        Pro.Add(Data.Name);
        Pro.Add(Item.Subtitle);
        Pro.Add(Description);
        Pro.Add(Item.ConditionID);
        Pro.Add(Data.MainImage);
        Pro.Add(Data.Quantity.ToString());
        Pro.Add(Item.Format);
        Pro.Add(Data.Price);
        Pro.Add(Data.Cost);
        Pro.Add(Item.Duration);
        Pro.Add(Item.ImmediatePayRequired);
        Pro.Add(Item.Location);
        Pro.Add(Item.GalleryType);
        Pro.Add(Item.PayPalAccepted);
        Pro.Add(Item.PayPalEmailAddress);
        Pro.Add(Item.PaymentInstructions);
        Pro.Add(Item.StoreCategory);
        Pro.Add(Item.ShippingDiscountProfileID);
        Pro.Add(Item.DomesticRateTable);
        Pro.Add(Item.ShippingType);
        Pro.Add(Item.ShippingService1Option);
        Pro.Add(Item.ShippingService1Cost);
        Pro.Add(Item.ShippingService1Priority);
        Pro.Add(Item.ShippingService1ShippingSurcharge);
        Pro.Add(Item.ShippingService2Option);
        Pro.Add(Item.ShippingService2Cost);
        Pro.Add(Item.ShippingService2Priority);
        Pro.Add(Item.ShippingService2ShippingSurcharge);
        Pro.Add(Item.DispatchTimeMax);
        Pro.Add(Item.CustomLabel);
        Pro.Add(Item.ReturnsAcceptedOption);
        Pro.Add(Item.RefundOption);
        Pro.Add(Item.ReturnsWithinOption);
        Pro.Add(Item.ShippingCostPaidByOption);
        Pro.Add(Item.AdditionalDetails);
        Pro.Add(Item.ShippingProfileName);
        Pro.Add(Item.ReturnProfileName);
        Pro.Add(Item.PaymentProfileName);
        return Task.FromResult(Pro);
    }
    public class ParameterItem //: JsonObject<ParameterItem>
    {
        public string? unit { get; set; }
        public string? language { get; set; }
        [Required]
        public string Action { get; set; } = null!;
        [Required]
        public string Category { get; set; } = null!;
        public string? Subtitle { get; set; }
        [Required]
        public string ConditionID { get; set; } = null!;
        [Required]
        public string Format { get; set; } = null!;
        [Required]
        public string Duration { get; set; } = null!;
        public string? ImmediatePayRequired { get; set; }
        [Required]
        public string Location { get; set; } = null!;
        public string? GalleryType { get; set; }
        public string? PayPalAccepted { get; set; }
        public string? PayPalEmailAddress { get; set; }
        public string? PaymentInstructions { get; set; }
        public string? StoreCategory { get; set; }
        public string? ShippingDiscountProfileID { get; set; }
        public string? DomesticRateTable { get; set; }
        public string? ShippingType { get; set; }
        public string? ShippingService1Option { get; set; }
        public string? ShippingService1Cost { get; set; }
        public string? ShippingService1Priority { get; set; }
        public string? ShippingService1ShippingSurcharge { get; set; }
        public string? ShippingService2Option { get; set; }
        public string? ShippingService2Cost { get; set; }
        public string? ShippingService2Priority { get; set; }
        public string? ShippingService2ShippingSurcharge { get; set; }
        public string? DispatchTimeMax { get; set; }
        public string? CustomLabel { get; set; }
        public string? ReturnsAcceptedOption { get; set; }
        public string? RefundOption { get; set; }
        public string? ReturnsWithinOption { get; set; }
        public string? ShippingCostPaidByOption { get; set; }
        public string? AdditionalDetails { get; set; }
        public string? ShippingProfileName { get; set; }
        public string? ReturnProfileName { get; set; }
        public string? PaymentProfileName { get; set; }
    }
}
