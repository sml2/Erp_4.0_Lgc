﻿using ERP.Export.ExportCommon;
using ERP.Export.Templates;
using ERP.Models.Export;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class YunHe : ExcelBase<YunHe, YunHe.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.YunHe;

    public override string Name => "云盒导出模板";

    public YunHe(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }
    
    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
    {
        List<List<string>> Product = new List<List<string>>();
        ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
        if (Item.IsNull())
        {
            _Logger.LogInformation($"{Name} GetData Method ParameterItem Is Null");
            return null;
        }
        string Sku = Data.Sku;
        string MainImage = "";
        List<string> Images = new List<string>();
        if (Data.Products.IsNotNull() && Data.Products.Count > 0)
        {
            if (role.MainProImgSetting == ExcleRole.MainProImgType.FirstVariantImage)
            {
                MainImage = Data.Products.First().MainImage;
                Images = Data.Products.First().Images;
            }
            else if (role.MainProImgSetting == ExcleRole.MainProImgType.LastVariantImage)
            {
                MainImage = Data.Products.Last().MainImage;
                Images = Data.Products.Last().Images;
            }
            else if (role.MainProImgSetting == ExcleRole.MainProImgType.EveryFirstImage)
            {
                List<string> NewImage = new List<string>();
                Data.Products.ForEach((r) => { if (r.Images.IsNotNull() && r.Images.Count > 0) NewImage.Add(r.Images.First()); });
                if (NewImage.Count > 0)
                {
                    MainImage = NewImage.First();
                    Images = NewImage.Skip(1).ToList();
                }
            }
        }
        ExcelProduct Pro = new ExcelProduct(Data.Name, Data.Description, Data.BulletPoint, Data.SearchTerms, "", "", Sku, MainImage, Images, Data.Quantity, "", "", "", null, "");
        //变体属性ColorSize
        string Attribute = "";
        Data.Attribute.ForEach((r) =>
        {
            var Key = r.Key.ToLower();
            if (_Common.Colors.Contains(Key)) Attribute += "Color";
            if (ProductExportHelper.Sizes.Contains(Key)) Attribute += "Size";
        });
        if (Attribute.IsNullOrEmpty() && Data.Attribute.Count == 1) Attribute = "Color";
        if (Data.Attribute.Count > 1) Attribute = "ColorSize";

        if (Data.Products.Count == 1 && Data.Products[0].Sku == Sku)
        {
            //无变体
            Attribute = "";
        }
        else
        {
            //20211211 去掉主产品行,需求来自djf飞书
            //var MainPro = GetProduct(Pro, Item, role, Attribute, Rate, Sku, Parameter, true);
            //if (MainPro.IsNotNull()) Product.Add(MainPro);
        }
        //子产品
        if (Data.Products.IsNotNull() && Data.Products.Count > 0)
        {
            foreach (var p in Data.Products)
            {
                ExcelProduct ChildPro = _Common.GetCommonData(p, Data.Attribute, Data.Source, Sku);
                var ChileExcelPro = await GetProduct(ChildPro, Item, role, Attribute, Rate, Sku, Parameter, false);
                if (ChileExcelPro.IsNotNull()) Product.Add(ChileExcelPro);
            }
        }
        var Dic = new Dictionary<int, List<List<string>>>();
        Dic.Add(0, Product);
        return Dic;
    }
    private async Task<List<string>> GetProduct(ExcelProduct Data, ParameterItem Item, ExcleRole role, string Attribute, List<Unit> Rate, string Sku, string Parameter, bool IsMain)
    {
        //描述
        string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, Data.Images);
        //卖点
        var BulletPoint = _Common.GetBulletPoint(Data.BulletPoint, Data.SearchTerms, Data.Description, role.SketchMode);
        //关键字
        var Keyword = _Common.GetKeywords(Data.BulletPoint, Data.SearchTerms, Data.Description, role.KeywordMode);
        //主产品
        List<string> Pro = new List<string>();
        //父SKU
        //产品父sku编码，此项值必填，若同属一个产品，保证该项值相同
        Pro.Add(Sku);
        //SKU
        //产品sku，每一条产品sku信息的编码
        Pro.Add(Data.Sku);
        //操作
        //该属性值无实际意义，导入均为新增操作，可不填写
        Pro.Add("新增");
        //若颜色尺码均有值，变种主题处理为ColorSize；若都没有值，则当做单品处理
        //颜色
        //产品变种主题，只填写颜色，变种主题为Color
        Pro.Add(Data.Color);
        //尺码
        //只填写尺码，变种主题为Size；
        Pro.Add(Data.Size);
        //品牌
        //产品品牌属性，非必填。若为空，系统自动填充为N/A
        Pro.Add(Item.Brand.IsNotWhiteSpace() ? Item.Brand : "N/A");
        //分类
        //产品分类，在导入时需要重新进行选择。导入的产品分类则为导入时选择的分类
        Pro.Add(Item.Category);
        //中文简称
        //产品的中文简称
        Pro.Add(Data.Name);
        //英文简称
        //产品的英文简称
        Pro.Add(Data.Name);
        // 海关编码  -new 表格内留空
        // 产品海关编码
        Pro.Add("");
        // 申报品名  -new 表格内留空
        Pro.Add("");
        //库存
        //产品库存数
        //if (IsMain) Pro.Add("");
        //else Pro.Add(Data.Quantity.ToString());
        Pro.Add(Data.Quantity.ToString());
        //币种
        //产品价格的币种，例如 人民币填写CNY、美元填写USD
        //Pro.Add(Item.Unit);
        Pro.Add(Rate.FirstOrDefault()?.Sign ?? "Base");
        //成本价
        //产品成本价格
        string Price = ProductExportHelper.GetDataPrice(Data.Price, Rate);
        Pro.Add(Price);
        //运费
        //产品产生的运费
        Pro.Add(Item.LogisticsPrice);
        //挂号模板
        //可不填写
        Pro.Add(Item.RegistrationTemplate);
        //分销价
        //若为分销产品，须填写分销价格
        Pro.Add(Item.DistributionPrice);
        //毛重(克)
        //产品重量，单位以“克”计算
        Pro.Add(Item.Weight);
        // 报关属性 -new 表格内留空
        // 多个报关属性用英文逗号隔开
        Pro.Add("");
        // 分销国家 -new 表格内留空
        // 多个分销国家用英文逗号隔开
        Pro.Add("");
        //产品附加属性、在刊登亚马逊时使用，可忽略
        //包装尺寸
        Pro.Add(Item.Dimension);
        // 23.04.18 只是UI换名，字段延用之前的
        //包装毛重  - new
        Pro.Add(Item.ForPeople);
        //产品尺寸  - new
        Pro.Add(Item.Material);
        //产品毛重  - new
        Pro.Add(Item.PackageMaterial);
        //产品材质  - new
        Pro.Add(Item.Metal);
        //产品用途   -new
        Pro.Add(Item.Jewelry);

        //语言
        //产品描述信息语言，可供填写的值（中文、英语、法语、德语、意大利、西班牙、荷兰语、日语、瑞典语、阿拉伯语、波兰语），按照给出的值填写、防止出错
        Pro.Add(Item.LanguageCN);
        //标题
        //产品标题、长度不超过200字符
        Pro.Add(Data.Name);

        //关键字
        //产品关键词，长度不超过250字符 
        Pro.Add(string.Join(" ", Keyword));

        //要点1 要点2 要点3 要点4 要点5
        //产品要点、最多五条，每条不超过500字符
        ProductExportHelper.SetCommonImage(Pro, BulletPoint, 5);

        //简介
        //产品介绍信息
        //Pro.Add(Data.Description);
        Pro.Add(Description.Replace("\r\n", "</br>").Replace("\n", "</br>").Replace("<br>", "</br>"));
        //产品图1 产品图2 产品图3 产品图4 产品图5 产品图6 产品图7 产品图8 产品图9 产品图10 产品图11 产品图12 
        //产品图片，最多12条，每一列填写一个产品图地址，系统会将父SKU相同的产品图进行合并，单一行产品图则作为sku图片
        Pro.Add(Data.MainImage);
        ProductExportHelper.SetCommonImage(Pro, Data.Images, 11);
        //参考网址
        //产品来源参考网址
        Pro.Add(Data.Source);
        return Pro;
    }
    public class ParameterItem : CommonParameter
    {
        public string Unit { get; set; }
        public string Brand { get; set; }
        public string Category { get; set; }
        public string LanguageCN { get; set; }
        public string LogisticsPrice { get; set; }
        public string DistributionPrice { get; set; }
        public string RegistrationTemplate { get; set; }
        public string Material { get; set; }
        public string Weight { get; set; }
        public string Dimension { get; set; }
        public string ForPeople { get; set; }
        public string PackageMaterial { get; set; }
        public string Metal { get; internal set; }
        public string Jewelry { get; internal set; }
    }
}
