﻿using ERP.Export.ExportCommon;
using ERP.Export.Templates;
using ERP.Models.Export;
using Ryu.Extensions;
using System.Security.Cryptography;
using System.Text;

namespace ERP.Export.V4_Export.ExcelTemplates
{
    public class LazadaSingleCountry : ExcelBase<LazadaSingleCountry>
    {
        public override TemplateType TemplateType => TemplateType.Lazada_SingleCountry;

        public override string Name => "Lazada单国家通用表";

        public LazadaSingleCountry(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }
        public override async Task<bool> Verify(string Parameter)
        {
            ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
            if (Item.IsNull())
            {
                _Logger.LogInformation($"{Name} Verify Method ParameterItem Is Null");
                return false;
            }
            if (Item.Brand.IsNullOrEmpty())
            {
                _Logger.LogInformation($"{Name} Verify Method ParameterItem [{nameof(Item.Brand)}]");
                return false;
            }
            return await Task.FromResult(true);
        }

        public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
        {
            List<List<string>> Product = new List<List<string>>();
            ParameterItem? ParameterItem = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
            if (ParameterItem.IsNull())
            {
                _Logger.LogInformation($"{Name} GetData Method ParameterItem Is Null");
                return null;
            }
            string Sku = Data.Sku;
            var GN = Sku.IsNotWhiteSpace() ? Sku : Data.ProductID;
            if (GN.IsNullOrWhiteSpace() || GN.Equals("") || GN.Equals("0"))
            {
                if (Data.Source.IsNotWhiteSpace())
                {
                    var bytes = MD5.HashData(Encoding.UTF8.GetBytes(Data.Source));

                    GN = bytes.Aggregate(new StringBuilder(), (sb, b) => sb.AppendFormat("{x2}", b)).ToString();
                }
                else
                {
                    GN = $"{DateTime.Now.GetTimeStamp()}";
                }
            }
            //子产品
            foreach (var p in Data.Products)
            {
                ExcelProduct ChildPro = _Common.GetCommonData(p, Data.Attribute, Data.Source, Sku);


                var ChileExcelPro = await GetProduct(ChildPro, ParameterItem, role, Rate, GN);
                if (ChileExcelPro.IsNotNull()) Product.Add(ChileExcelPro);
            }
            var Dic = new Dictionary<int, List<List<string>>>();
            Dic.Add(0, Product);
            return Dic;
        }
        private async Task<List<string>> GetProduct(ExcelProduct Data, ParameterItem ParameterItem, ExcleRole role, List<Unit> Rate, string GroupNo)
        {
            //描述
            string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, Data.Images);
            //卖点
            string BulletPoint = string.Join(";", _Common.GetBulletPoint(Data.BulletPoint, Data.SearchTerms, Data.Description, role.SketchMode));
            List<string> Pro = new List<string>();
            //Group No
            Pro.Add(GroupNo);
            //catId	
            Pro.Add("");
            //*Product Name
            Pro.Add(Data.Name);
            //*Product Images1	Product Images2	Product Images3	Product Images4	Product Images5	Product Images6	Product Images7	Product Images8
            ProductExportHelper.SetCommonImage(Pro, Data.Images, 8);
            //Showcase_image1:1	originalLocalName	currencyCode	Video URL
            ProductExportHelper.SetCommonCell(Pro, "", 4);
            //*Brand
            Pro.Add(ParameterItem.Brand);
            //Material
            Pro.Add(ParameterItem.Material);
            //Model
            Pro.Add(ParameterItem.Model);
            //Cutlery Type
            Pro.Add(ParameterItem.CutleryType);
            //Settings
            Pro.Add(ParameterItem.Settings);
            //Long Description (Lorikeet)
            Pro.Add(Description);
            //Product Description (Malay)
            Pro.Add(BulletPoint);
            //What's in the box
            Pro.Add(ParameterItem.Box);
            //Warranty Policy
            Pro.Add(ParameterItem.WarrantyPolicy);
            //Warranty Period
            Pro.Add(ParameterItem.WarrantyPeriod);
            //Warranty Type
            Pro.Add(ParameterItem.WarrantyType);
            //*Package Weight (kg)
            Pro.Add(ParameterItem.PackageWeight);
            //*Package Dimensions (cm)-Length (cm)
            Pro.Add(ParameterItem.PackageLength);
            //*Package Dimensions (cm)-Width (cm)
            Pro.Add(ParameterItem.PackageWidth);
            //*Package Dimensions (cm)-Height (cm)
            Pro.Add(ParameterItem.PackageHeight);
            //Dangerous Goods
            Pro.Add(ParameterItem.DangerousGoods);
            //Color Family
            var cf = $"{Data.Color}{Data.Size}{Data.OtherAttribute}";
            Pro.Add(cf.Length == 0 ? ParameterItem.ColorFamily : cf);
            //props	Images1	Images2	Images3	Images4	Images5	Images6	Images7	Images8
            ProductExportHelper.SetCommonCell(Pro, "", 9);
            //Color thumbnail
            Pro.Add(ParameterItem.ColorThumbnail);
            //Taxes
            Pro.Add(ParameterItem.Taxes);
            //*Price
            string Price = ProductExportHelper.GetDataPrice(Data.Price, Rate);
            Pro.Add(Price);
            //SellerSKU
            Pro.Add(Data.Sku);
            //SpecialPrice	SpecialPrice Start	SpecialPrice End
            ProductExportHelper.SetCommonCell(Pro, "", 3);
            //*Quantity
            Pro.Add($"{Math.Max(Data.Quantity, 50)}");
            return Pro;
        }

        public class ParameterItem //: JsonObject<ParameterItem>
        {
            //Basic Information
            public string GroupNo { get; set; }
            public string catId { get; set; }
            //Product Attributes
            public string Brand { get; set; }//*
            public string Material { get; set; }//**
            public string Model { get; set; }
            public string CutleryType { get; set; }
            public string Settings { get; set; }
            //Product Description
            public string Box { get; set; }
            //Service
            public string WarrantyPolicy { get; set; }
            public string WarrantyPeriod { get; set; }
            public string WarrantyType { get; set; }
            //Delivery
            public string PackageWeight { get; set; }//*
            public string PackageLength { get; set; }//*
            public string PackageWidth { get; set; }//*
            public string PackageHeight { get; set; }//*
            public string DangerousGoods { get; set; }
            //sku
            public string ColorFamily { get; set; }
            public string Props { get; set; }
            public string ColorThumbnail { get; set; }
            public string Taxes { get; set; }
        }
    }
}
