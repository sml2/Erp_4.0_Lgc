﻿using ERP.Export.ExportCommon;
using ERP.Export.Templates;
using ERP.Models.Export;

namespace ERP.Export.V4_Export.ExcelTemplates
{
    public class LazadaBeddingBath : ExcelBase<LazadaBeddingBath>
    {
        public override TemplateType TemplateType => TemplateType.Lazada_Bedding_Bath;
        public override string Name => "Lazada Bedding & Bath表";
        public LazadaBeddingBath(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }
        public override async Task<bool> Verify(string Parameter)
        {
            ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
            if (Item is null)
            {
                _Logger.LogInformation($"{Name} GetData ParameterItem Is Null");
                return false;
            }
            if (Item.primary_category.IsNullOrEmpty())
            {
                _Logger.LogInformation($"{Name} GetData ParameterItem Is Null");
                return false;
            }
            return true;
        }

        public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
        {
            List<List<string>> Product = new List<List<string>>();
            ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
            if (Item is null)
            {
                _Logger.LogInformation($"{Name} GetData ParameterItem Is Null");
                return null!;
            }
            string Sku = Data.Sku;
            if (Data.Products.IsNull() || Data.Products.Count == 0)
            {
                string Price = "0";
                string Cost = "0";
                if (Data.Price is not null && Data.Price.Sale.Max.Value > 0) Price = Data.Price.Sale.Max.Value.ToString();//sale
                if (Data.Price is not null && Data.Price.Cost.Max.Value > 0) Price = Data.Price.Sale.Max.Value.ToString();//cost
                ExcelProduct MainPro = new ExcelProduct(Data.Name, Data.Description, Data.BulletPoint, Data.SearchTerms, "", "", Sku, "", null, Data.Quantity, Price, Cost, "", null, Data.Source);
                var MainExcelPro = GetProduct(MainPro, Item, role, Rate);
                if (MainExcelPro.IsNotNull()) Product.Add(MainExcelPro);
            }
            else
            {
                //子产品
                foreach (var p in Data.Products)
                {
                    ExcelProduct ChildPro = _Common.GetCommonData(p, Data.Attribute, Data.Source, Sku);
                    var ChileExcelPro = GetProduct(ChildPro, Item, role, Rate);
                    if (ChileExcelPro.IsNotNull()) Product.Add(ChileExcelPro);
                }
            }
            var Dic = new Dictionary<int, List<List<string>>>();
            Dic.Add(0, Product);
            return Dic;
        }
        private List<string> GetProduct(ExcelProduct Data, ParameterItem Item, ExcleRole role, List<Unit> Rate)
        {
            List<string> Pro = new List<string>();
            Pro.Add(Item.primary_category);
            Pro.Add(Item.model);
            Pro.Add(Item.brand);
            string Color = Data.Color;
            if (Color.IsNullOrEmpty()) Color = Item.color_family;
            Pro.Add(Color);
            ProductExportHelper.SetCommonCell(Pro, "", 19);
            Pro.Add(Item.linen_fabric);
            Pro.Add(Item.thread_count);
            Pro.Add("");
            Pro.Add(Item.filling);
            Pro.Add("");
            Pro.Add(Item.bed_accessory_type);
            _Common.GetLazadaCommonCell(Pro, 0, Data, Item, role, Rate);
            Pro.Add("");
            string Size = Data.Size;
            if (Size.IsNullOrEmpty()) Size = Item.bedding_size_2;
            Pro.Add(Size);
            Pro.Add(Item.Bedding_Pattern);
            return Pro;
        }

        public class ParameterItem : IParameterItem //: JsonObject<ParameterItem>, IParameterItem
        {
            public string primary_category { get; set; }

            public string model { get; set; }

            public string brand { get; set; }

            public string color_family { get; set; }

            public string package_content { get; set; }

            public string package_length { get; set; }

            public string package_width { get; set; }

            public string package_height { get; set; }

            public string package_weight { get; set; }

            public string taxes { get; set; }

            public string linen_fabric { get; set; }
            public string thread_count { get; set; }
            public string filling { get; set; }
            public string bed_accessory_type { get; set; }
            public string bedding_size_2 { get; set; }
            public string Bedding_Pattern { get; set; }
        }
    }
}
