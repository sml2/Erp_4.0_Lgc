﻿using ERP.Export.ExportCommon;
using ERP.Models.Export;
using ERP.Export.V4_Export.Interfaces;
using Newtonsoft.Json;
using System.Text;
using common = ERP.Export.ExportCommon.ProductExportHelper;
using ERP.Export.Templates;

namespace ERP.Export.V4_Export.ExcelTemplates
{
    public class LazadaSingleCountryOLD : ExcelBase<LazadaSingleCountryOLD>
    {
        public override TemplateType TemplateType => TemplateType.Lazada_SingleCountry_OLD;

        public override string Name => "Lazada单国家通用表";

        public LazadaSingleCountryOLD(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }
        public override async Task<bool> Verify(string Parameter)
        {
            ParameterItem Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
            if (Item.IsNull())
            {
                //Log(I18N.Instance.Export_Log3);
                return false;
            }
            if (Item.primary_category.IsNullOrEmpty())
            {
                //Log($"{I18N.Instance.Export_Log4}Primary Category is Empty!");
                return false;
            }
            return true;
        }

        public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
        {
            List<List<string>> Product = new List<List<string>>();
            ParameterItem ParameterItem = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
            if (ParameterItem.IsNull())
            {
                //Log(I18N.Instance.Export_Log3);
                return null;
            }
            string Sku = Data.Sku;
            //子产品
            foreach (var p in Data.Products)
            {
                ExcelProduct ChildPro = _Common.GetCommonData(p, Data.Attribute, Data.Source, Sku);
                var ChileExcelPro = await GetProduct(ChildPro, ParameterItem, role, Rate);
                if (ChileExcelPro.IsNotNull()) Product.Add(ChileExcelPro);
            }
            var Dic = new Dictionary<int, List<List<string>>>();
            Dic.Add(0, Product);
            return Dic;
        }
        private async Task<List<string>> GetProduct(ExcelProduct Data, ParameterItem ParameterItem, ExcleRole role, List<Unit> Rate)
        {
            //描述
            string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, Data.Images);
            //卖点
            string BulletPoint = string.Join(";", _Common.GetBulletPoint(Data.BulletPoint, Data.SearchTerms, Data.Description, role.SketchMode));
            List<string> Pro = new List<string>();
            Pro.Add(ParameterItem.primary_category);
            Pro.Add(ParameterItem.brand);
            Pro.Add(ParameterItem.model);
            Pro.Add(Data.Color);
            Pro.Add(Data.Name);
            Pro.Add(Description);
            Pro.Add(BulletPoint);
            string Price = ProductExportHelper.GetDataPrice(Data.Price, Rate);
            Pro.Add(Price);
            common.SetCommonCell(Pro, "", 3);
            Pro.Add(Data.Sku);
            Pro.Add(Data.Sku);
            Pro.Add(ParameterItem.package_content);
            Pro.Add(ParameterItem.package_length);
            Pro.Add(ParameterItem.package_width);
            Pro.Add(ParameterItem.package_height);
            Pro.Add(ParameterItem.package_weight);
            Pro.Add(Data.MainImage);
            common.SetCommonImage(Pro, Data.Images, 7);
            Pro.Add(Data.Size);
            Pro.Add(ParameterItem.warranty_type);
            Pro.Add(ParameterItem.bedding_size_2);
            Pro.Add(ParameterItem.size_baby_clothing);
            Pro.Add(ParameterItem.lens_color);
            Pro.Add(ParameterItem.eyewear_size);
            Pro.Add(ParameterItem.frame_color);
            Pro.Add(ParameterItem.weight_of_metal);
            Pro.Add(ParameterItem.chain_size);
            Pro.Add(ParameterItem.ring_size);
            Pro.Add(ParameterItem.bangle_size);
            Pro.Add(ParameterItem.watch_strap_color);
            Pro.Add(ParameterItem.clothing_size);
            Pro.Add(ParameterItem.glove_size);
            Pro.Add(ParameterItem.shoes_size);
            Pro.Add(ParameterItem.material_shaft);
            return Pro;
        }

        public class ParameterItem //: JsonObject<ParameterItem>
        {
            public string unit { get; set; }

            public string primary_category { get; set; }

            public string model { get; set; }

            public string brand { get; set; }

            public string color_family { get; set; }

            public string package_content { get; set; }

            public string package_length { get; set; }

            public string package_width { get; set; }

            public string package_height { get; set; }

            public string package_weight { get; set; }

            public string warranty_type { get; set; }

            public string bedding_size_2 { get; set; }

            public string size_baby_clothing { get; set; }

            public string lens_color { get; set; }

            public string eyewear_size { get; set; }

            public string frame_color { get; set; }

            public string weight_of_metal { get; set; }

            public string chain_size { get; set; }

            public string ring_size { get; set; }

            public string bangle_size { get; set; }

            public string watch_strap_color { get; set; }

            public string clothing_size { get; set; }

            public string glove_size { get; set; }

            public string shoes_size { get; set; }

            public string material_shaft { get; set; }

            public string color { get; set; }

            public string size { get; set; }
        }
    }
}
