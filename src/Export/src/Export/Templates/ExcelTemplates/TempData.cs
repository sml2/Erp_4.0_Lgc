﻿using System.ComponentModel.DataAnnotations;
using ERP.Export.ExportCommon;
using ERP.Export.Templates;
using ERP.Models.Export;

namespace ERP.Export.V4_Export.ExcelTemplates
{
    public class TempData : ExcelBase<TempData, TempData.ParameterItem>
    {
        public override TemplateType TemplateType => TemplateType.TempData;

        public override string Name => "17678004554表";

        public TempData(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }
        
        public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
        {
            ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
            if (Item.IsNull())
            {
                //Log(I18N.Instance.Export_Log3);
                return null;
            }
            List<List<string>> Product = new List<List<string>>();
            string Sku = Data.Sku;
            //string Price = _Common.GetMainSalePrice(Data, Rate);
            //string Cost = _Common.GetMainCostPrice(Data, Rate);
            ////处理无变体的时候
            //if (Data.Products.IsNotNull() && Data.Products.Count == 1 && Data.Products[0].Sku == Data.Sku) { }
            //else
            //{
            //    ExcelProduct MainPro = new ExcelProduct(Data.Name, Data.Description, Data.BulletPoint, Data.SearchTerms, "", "", Sku, "", null, Data.Quantity, Price, Cost, "", null, Data.Source);
            //    var MainExcelPro = GetProduct(MainPro, Item, Rate, role);
            //    if (MainExcelPro.IsNotNull()) Product.Add(MainExcelPro);
            //}
            if (Data.Products.IsNotNull() && Data.Products.Count > 0)
            {
                //获取所有图片
                List<string> Images = new List<string>();
                Data.Products.ForEach((r) =>
                {
                    if (r.MainImage.IsNotNull()) Images.Add(r.MainImage + ".jpg");
                    if (r.Images.IsNotNull() && r.Images.Count > 0)
                    {
                        r.Images.ForEach((img) =>
                        {
                            if (img.IsNotWhiteSpace()) Images.Add(img + ".jpg");
                        });
                    }
                });
                foreach (var p in Data.Products)
                {
                    ExcelProduct ChildPro = _Common.GetCommonData(p, Data.Attribute, Data.Source, Sku, TemplateType);
                    var ChileExcelPro = await GetProduct(ChildPro, Item, Rate, Sku, Images, role);
                    if (ChileExcelPro.IsNotNull()) Product.Add(ChileExcelPro);
                }
            }
            var Dic = new Dictionary<int, List<List<string>>>();
            Dic.Add(0, Product);
            return Dic;
        }
        private async Task<List<string>> GetProduct(ExcelProduct Data, ParameterItem Item, List<Unit> Rate, string Sku, List<string> Images, ExcleRole role)
        {
            //描述
            string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, null);
            List<string> Pro = new List<string>();
            #region 老版本模板
            //if (IsMain) Pro.Add("");
            //else Pro.Add(Data.Sku);
            //Pro.Add(Sku);
            //Pro.Add(Data.Name);
            //Pro.Add(Description);
            //Pro.Add(Item.primary_category);
            //Pro.Add(Item.material);
            //List<string> Images = new List<string>();
            //if (Data.MainImage.IsNotWhiteSpace()) Images.Add(Data.MainImage + ".jpg");
            //if (Data.Images.IsNotNull())
            //{
            //    Data.Images.ForEach((img) =>
            //    {
            //        if (img.IsNotWhiteSpace()) Images.Add(img + ".jpg");
            //    });
            //}
            //_Common.SetCommonImage(Pro, Images, 5);
            //Pro.Add(Data.Color);
            //Pro.Add(Data.Size);
            //string Price = _Common.GetDataPrice(Data.Price, Rate);
            //Pro.Add(Price);
            //Pro.Add(Item.package_length);
            //Pro.Add(Item.package_width);
            //Pro.Add(Item.package_height);
            //Pro.Add(Item.package_weight);
            #endregion
            #region 新版本模板 20201105
            Pro.Add(Item.primary_category);
            Pro.Add(Item.seller);
            Pro.Add(Data.Name);
            Pro.Add(Description);
            Pro.Add("");
            Pro.Add("");
            Pro.Add(Item.category);
            Pro.Add(Item.season);
            Pro.Add(Item.brand);
            Pro.Add(Data.SearchTerms);
            Pro.Add(Item.material);
            ProductExportHelper.SetCommonImage(Pro, Images, 5);
            Pro.Add(Item.associated);
            Pro.Add(Data.Color);
            Pro.Add(Data.Size);
            string Price = ProductExportHelper.GetDataPrice(Data.Price, Rate);
            Pro.Add(Price);
            if (Rate.IsNotNull() && Rate.Count > 0) Pro.Add(Rate[0].Sign);
            else Pro.Add("USD");
            Pro.Add(Data.Sku);
            Pro.Add(Data.Quantity.ToString());
            Pro.Add(Item.package_length);
            Pro.Add(Item.package_width);
            Pro.Add(Item.package_height);
            Pro.Add(Item.package_weight);
            Pro.Add(Data.MainImage + ".jpg");
            #endregion
            return Pro;
        }
        public class ParameterItem //: JsonObject<ParameterItem>
        {
            public string unit { get; set; }
            [Required]
            public string primary_category { get; set; } = null!;

            public string material { get; set; }

            public string package_length { get; set; }

            public string package_width { get; set; }

            public string package_height { get; set; }

            public string package_weight { get; set; }
            public string associated { get; set; }
            public string category { get; set; }
            public string season { get; set; }
            public string brand { get; set; }
            public string seller { get; set; }
        }
    }
}
