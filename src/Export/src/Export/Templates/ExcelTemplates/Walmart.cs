﻿using ERP.Export.ExportCommon;
using ERP.Export.Templates;
using ERP.Models.Export;

namespace ERP.Export.V4_Export.ExcelTemplates
{
    public class Walmart : ExcelBase<Walmart, Walmart.ParameterItem>
    {
        public override TemplateType TemplateType => TemplateType.Walmart;

        public override string Name => "沃尔玛变体模板";

        public Walmart(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }

        public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem data, ExcleRole role, string parameter, List<Unit> rate)
        {
            var product = new List<List<string?>>();
            ParameterItem? item = ProductExportHelper.ToDeserialize<ParameterItem>(parameter);
            if (item.IsNull())
            {
                //Log(I18N.Instance.Export_Log3);
                _Logger.LogInformation($"{Name} GetData Method ParameterItem Is Null");
                return null!;
            }
            var sku = data.Sku;
            var attributes = data.Attribute;
            if (data.Products.IsNotNull() && data.Products.Count > 0)
            {
                foreach (var p in data.Products)
                {
                    string description = _Common.GetDescription(p.Description, string.Join(";", p.BulletPoint), p.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, p.Images);
                    //卖点
                    var bulletPoint = _Common.GetBulletPoint(p.BulletPoint, p.SearchTerms, p.Description, role.SketchMode).Where((r) => r.IsNotWhiteSpace()).ToList();
                    //关键字
                    var keyword = _Common.GetKeywords(p.BulletPoint, p.SearchTerms, p.Description, role.KeywordMode).Where((r) => r.IsNotWhiteSpace()).ToList();

                    var pro = new List<string?>();
                    ExcelProduct tempPro = _Common.GetCommonData(p, data.Attribute, data.Source, sku, TemplateType.Walmart);
                    ProductExportHelper.SetCommonCell(pro, "", 3);
                    pro.Add(tempPro.Sku);
                    string produceType = tempPro.ProduceCode.Length switch
                    {
                        12 => "UPC",
                        13 => "EAN",
                        _ => "",
                    };
                    pro.Add(produceType);//UPC EAN
                    pro.Add(tempPro.ProduceCode);
                    pro.Add(tempPro.Name);
                    string brand = "";
                    if (p.Fields.IsNotNull())
                    {
                        foreach (var temp in p.Fields)
                        {
                            if (temp.Name == "brand" && temp.Value.IsNotWhiteSpace()) brand = temp.Value; //Key  Value
                        }
                    }

                    if (string.IsNullOrEmpty(brand) && !string.IsNullOrWhiteSpace(item.Brand))
                        brand = item.Brand;
                    
                    pro.Add(brand);
                    string price = ProductExportHelper.GetDataPrice(tempPro.Price, rate);
                    pro.Add(price);
                    pro.Add(item.ShippingWeight);
                    pro.Add(description);
                    ProductExportHelper.SetCommonImage(pro, bulletPoint, 6);
                    pro.Add(tempPro.MainImage);
                    ProductExportHelper.SetCommonImage(pro, tempPro.Images, 7);
                    pro.Add(item.California_Prop_65_Warning_Text);
                    pro.Add(item.Gender);
                    pro.Add(tempPro.Size);
                    pro.Add(item.Age_Group);
                    pro.Add(tempPro.Color);
                    pro.Add(item.Variant_Group_ID);
                    pro.Add(item.VariantAttributeName);
                    pro.Add(item.AttributeName);
                    pro.Add(item.Is_Primary_Variant);
                    pro.Add(item.Swatch_Variant_Attribute);
                    pro.Add(tempPro.MainImage);
                    product.Add(pro);
                }
            }
            var dic = new Dictionary<int, List<List<string?>>>();
            dic.Add(0, product);
            return dic;
        }
        public class ParameterItem //: JsonObject<ParameterItem>
        {
            public string? ShippingWeight { get; set; }
            public string? California_Prop_65_Warning_Text { get; set; }
            public string? Gender { get; set; }
            public string? Age_Group { get; set; }
            public string? Variant_Group_ID { get; set; }
            public string? VariantAttributeName { get; set; }
            public string? AttributeName { get; set; }
            public string? Is_Primary_Variant { get; set; }
            public string? Swatch_Variant_Attribute { get; set; }
            public string? Brand { get; set; }
        }
    }
}
