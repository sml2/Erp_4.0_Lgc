﻿using ERP.Export.ExportCommon;
using ERP.Models.Export;
using ERP.Export.Templates;
using System.ComponentModel.DataAnnotations;
using Ryu.Extensions;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class NopCommerce : ExcelBase<NopCommerce, NopCommerce.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.NopCommerce;
    public override string Name => "NopCommerce";

    public NopCommerce(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }
    
    private List<string> GetHead2 => new List<string>() { "", "", "AttributeId", "AttributeName", "AttributeTextPrompt", "AttributeIsRequired", "AttributeControlType", "AttributeDisplayOrder", "ProductAttributeValueId", "ValueName", "AttributeValueType", "AssociatedProductId", "ColorSquaresRgb", "ImageSquaresPictureId", "PriceAdjustment", "PriceAdjustmentUsePercentage", "WeightAdjustment", "Cost", "CustomerEntersQty", "Quantity", "IsPreSelected", "DisplayOrder", "PictureId" };
    private List<string> GetHead3 => new List<string> { "", "", "AttributeType", "SpecificationAttribute", "CustomValue", "SpecificationAttributeOptionId", "AllowFiltering", "ShowOnProductPage", "DisplayOrder" };
    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
    {
        List<List<string>> Product = new List<List<string>>();
        ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
        if (Item.IsNull())
        {
            _Logger.LogInformation($"{Name} GetData Method ParameterItem Is Null");
            return null;
        }
        string Sku = Data.Sku;
        string Price = await _Common.GetMainSalePrice(Data.Price, Rate);
        string Cost = _Common.GetMainCostPrice(Data.Price, Rate);
        ExcelProduct Pro = new ExcelProduct(Data.Name, Data.Description, Data.BulletPoint, Data.SearchTerms, "", "", Sku, "", null, Data.Quantity, Price, Cost, "", null, "");
        List<string> Images = new List<string>();
        if (Data.Products.IsNotNull() && Data.Products.Count > 0)
        {
            Images.Add(Data.Products[0].MainImage);
            Images.AddRange(Data.Products[0].Images);
        }
        else Images.Add(Data.ShowImage);
        var MainPro = await GetProduct(Pro, Item, role, Images, Data.ProductID);
        if (MainPro.IsNotNull()) Product.Add(MainPro);
        //属性(有变体的情况)
        if (Data.Attribute.IsNotNull() && Data.Attribute.Count > 0)
        {
            //属性行标题
            Product.Add(GetHead2);
            List<string> Color = new List<string>();
            List<string> Size = new List<string>();
            Dictionary<string, List<string>> Temp = new Dictionary<string, List<string>>();
            //填充属性
            Data.Attribute.ForEach((attr) =>
            {
                var TempKey = attr.Key.ToLower();
                if (_Common.Colors.Contains(TempKey)) Color.AddRange(attr.Value);
                else if (ProductExportHelper.Sizes.Contains(TempKey)) Size.AddRange(attr.Value);
                else Temp.Add(TempKey, attr.Value.ToList());
            });
            if (Color.Count == 0 && Temp.Count > 0)
            {
                Color.AddRange(Temp.Values.ToList()[0]);
                Temp.Remove(Temp.Keys.ToList()[0]);
            }
            if (Size.Count == 0 && Temp.Count > 0)
            {
                Size.AddRange(Temp.Values.ToList()[0]);
                Temp.Remove(Temp.Keys.ToList()[0]);
            }
            var AttrImage = await GetImage(Data);
            if (Color.Count > 0) Product.AddRange(GetAttribute(Color, "Color", Item, AttrImage));
            if (Size.Count > 0) Product.AddRange(GetAttribute(Size, "Size", Item, AttrImage));
            if (Color.Count > 0 || Size.Count > 0) Product.Add(GetHead3);
            //获取Size需要填写的值
            if (Size.Count > 0)
            {
                List<string> NeedSize = new List<string>();
                var TempSizeKey = ShowSize.Keys.ToList();
                var TempSizeValue = ShowSize.Values.ToList();
                Size.ForEach((r) =>
                {
                    if (TempSizeKey.Contains(r))
                    {
                        if (!NeedSize.Contains(ShowSize[r])) NeedSize.Add(ShowSize[r]);
                    }
                    else if (TempSizeValue.Contains(r) && !NeedSize.Contains(r))
                    {
                        NeedSize.Add(r);
                    }
                });
                if (NeedSize.Count > 0) Product.AddRange(GetNeedAttribute(NeedSize, "Size"));
            }
            //获取Coloe需要填写的值
            if (Color.Count > 0)
            {
                List<string> NeedColor = new List<string>();
                var TempColorKey = ShowColor.Keys.ToList();
                var TempColorValue = ShowColor.Values.ToList();
                Color.ForEach((r) =>
                {
                    if (TempColorKey.Contains(r))
                    {
                        if (!NeedColor.Contains(ShowColor[r])) NeedColor.Add(ShowColor[r]);
                    }
                    else if (TempColorValue.Contains(r) && !NeedColor.Contains(r))
                    {
                        NeedColor.Add(r);
                    }
                });
                if (NeedColor.Count > 0) Product.AddRange(GetNeedAttribute(NeedColor, "Color"));
            }
        }
        var Dic = new Dictionary<int, List<List<string>>>();
        Dic.Add(0, Product);
        return Dic;
    }
    private async Task<Dictionary<string, string>> GetImage(DataItem Data)
    {
        Dictionary<string, string> AttrImage = new Dictionary<string, string>();
        foreach (var p in Data.Products)
        {
            ExcelProduct ChildPro = _Common.GetCommonData(p, Data.Attribute, Data.Source, Data.Sku, TemplateType);
            if (ChildPro.Color.IsNotWhiteSpace() && !AttrImage.ContainsKey(ChildPro.Color)) AttrImage.Add(ChildPro.Color, ChildPro.MainImage);
        }
        return AttrImage;
    }
    private Dictionary<string, string> ShowSize = new Dictionary<string, string>()
    {
        {"XXXS" , "17"},
        {"XXS" , "18"},
        {"XS" , "19"},
        {"S" , "20"},
        {"M" , "21"},
        {"L" , "22"},
        {"XL" , "23"},
        {"XXL" , "24"},
        {"XXXL" , "25"},
    };
    private Dictionary<string, string> ShowColor = new Dictionary<string, string>()
    {
        {"Grey" , "14"},
        {"Red" , "15"},
        {"Blue" , "16"},
        {"Black" , "26"},
        {"Yellow" , "27"},
        {"White" , "28"},
    };
    private async Task<List<string>> GetProduct(ExcelProduct Data, ParameterItem Item, ExcleRole role, List<string> Image, string PID)
    {
        //描述
        string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, Data.Images);
        //主产品
        List<string> Pro = new List<string>();
        Pro.Add(Item.ProductId);
        Pro.Add("Simple Product");
        Pro.Add(Item.ParentGroupedProductId);
        Pro.Add("True");
        Pro.Add(Data.Name);
        Pro.Add(Description);
        Pro.Add(Description);
        Pro.Add(Item.Vendor);
        Pro.Add("Simple product");
        Pro.Add("FALSE");
        Pro.Add("0");
        Pro.Add("");
        Pro.Add("");
        Pro.Add("");
        Pro.Add($"1{DateTime.Now.GetTimeStamp(true)}");//随机14位数字
        Pro.Add("True");
        Pro.Add("FALSE");
        Pro.Add(PID);
        Pro.Add(Data.Source);
        Pro.Add(Item.Gtin);
        Pro.Add("FALSE");
        Pro.Add("Virtual");
        Pro.Add("");
        Pro.Add("FALSE");
        Pro.Add(Item.RequiredProductIds);
        Pro.Add("FALSE");
        Pro.Add("FALSE");
        Pro.Add("0");
        Pro.Add("True");
        Pro.Add("10");
        Pro.Add("When Order Is Paid");
        Pro.Add("FALSE");
        Pro.Add("0");
        Pro.Add("FALSE");
        Pro.Add(Item.UserAgreementText);
        Pro.Add("FALSE");
        Pro.Add("100");
        Pro.Add("Days");
        Pro.Add("10");
        Pro.Add("FALSE");
        Pro.Add("1");
        Pro.Add("Days");
        Pro.Add("True");
        Pro.Add("FALSE");
        Pro.Add("FALSE");
        Pro.Add("0");
        Pro.Add("");
        Pro.Add("FALSE");
        Pro.Add("");
        Pro.Add("FALSE");
        Pro.Add("Manage Stock");
        Pro.Add("");
        Pro.Add("FALSE");
        Pro.Add("0");
        Pro.Add(Data.Quantity.ToString());
        Pro.Add("FALSE");
        Pro.Add("FALSE");
        Pro.Add("0");
        Pro.Add("Nothing");
        Pro.Add("1");
        Pro.Add("No Backorders");
        Pro.Add("FALSE");
        Pro.Add("1");
        Pro.Add("10000");
        Pro.Add(Item.AllowedQuantities);
        Pro.Add("FALSE");
        Pro.Add("FALSE");
        Pro.Add("FALSE");
        Pro.Add("FALSE");
        Pro.Add("FALSE");
        Pro.Add("");
        Pro.Add("FALSE");
        Pro.Add(Data.Price);
        Pro.Add(Data.Price);
        Pro.Add(Data.Cost);
        Pro.Add("FALSE");
        Pro.Add("0");
        Pro.Add("1000");
        Pro.Add("FALSE");
        Pro.Add("0");
        Pro.Add("ounce(s)");
        Pro.Add("0");
        Pro.Add("ounce(s)");
        Pro.Add(Item.MarkAsNew);
        Pro.Add(Item.MarkAsNewStartDateTimeUtc);
        Pro.Add(Item.MarkAsNewEndDateTimeUtc);
        Pro.Add(Item.Weight);
        Pro.Add(Item.Length);
        Pro.Add(Item.Width);
        Pro.Add(Item.Height);
        Pro.Add(Item.Categories);
        Pro.Add(Item.Manufacturers);
        Pro.Add(Item.ProductTags);
        ProductExportHelper.SetCommonImage(Pro, Image, 3);
        return Pro;
    }
    private List<List<string>> GetAttribute(List<string> Data, string Attribute, ParameterItem Item, Dictionary<string, string> AttrImage)
    {
        List<List<string>> Rows = new List<List<string>>();
        foreach (var value in Data)
        {
            List<string> Pro = new List<string>();
            string ID = Attribute == "Color" ? "1" : "8";
            string AttributeControlType = Attribute == "Color" ? "Image Squares" : "Dropdown List";
            Pro.Add("");
            Pro.Add("");
            Pro.Add(ID);
            Pro.Add(Attribute);
            Pro.Add("");
            Pro.Add("TRUE");
            Pro.Add(AttributeControlType);
            Pro.Add("0");
            Pro.Add("");
            Pro.Add(value);
            Pro.Add("Simple");
            Pro.Add("0");
            string TempImage = "";
            if (AttrImage.ContainsKey(value)) TempImage = AttrImage[value];
            Pro.Add(TempImage);//变体主图URL
            Pro.Add("0");
            Pro.Add(Item.PriceAdjustment);
            Pro.Add("FALSE");
            Pro.Add("0");
            Pro.Add("0");
            Pro.Add("FALSE");
            Pro.Add(Item.Quantity);
            Pro.Add("FALSE");
            Pro.Add("0");
            Pro.Add("0");
            Rows.Add(Pro);
        }
        return Rows;
    }
    private List<List<string>> GetNeedAttribute(List<string> AttriValue, string Attri)
    {
        List<List<string>> Rows = new List<List<string>>();
        foreach (var Temp in AttriValue)
        {
            List<string> Row = new List<string>();
            ProductExportHelper.SetCommonCell(Row, "", 2);
            Row.Add("Option");
            Row.Add(Attri);
            Row.Add("");
            Row.Add(Temp);
            Row.Add("FALSE");
            Row.Add("TRUE");
            Row.Add("0");
            Rows.Add(Row);
        }
        return Rows;
    }
    public class ParameterItem //: JsonObject<ParameterItem>
    {
        public string ProductId { get; set; }
        //public string ProductType { get; set; }
        public string ParentGroupedProductId { get; set; }
        //public string VisibleIndividually { get; set; }
        public string Vendor { get; set; }
        //public string ProductTemplate { get; set; }
        //public string ShowOnHomepage { get; set; }
        //public string DisplayOrder { get; set; }
        //public string MetaKeywords { get; set; }
        //public string MetaDescription { get; set; }
        //public string MetaTitle { get; set; }
        //public string AllowCustomerReviews { get; set; }
        //public string Published { get; set; }
        public string ManufacturerPartNumber { get; set; }
        public string Gtin { get; set; }
        //public string IsGiftCard { get; set; }
        //public string GiftCardType { get; set; }
        //public string OverriddenGiftCardAmount { get; set; }
        //public string RequireOtherProducts { get; set; }
        public string RequiredProductIds { get; set; }
        //public string AutomaticallyAddRequiredProducts { get; set; }
        //public string IsDownload { get; set; }
        //public string DownloadId { get; set; }
        //public string UnlimitedDownloads { get; set; }
        //public string MaxNumberOfDownloads { get; set; }
        //public string DownloadActivationType { get; set; }
        //public string HasSampleDownload { get; set; }
        //public string SampleDownloadId { get; set; }
        //public string HasUserAgreement { get; set; }
        public string UserAgreementText { get; set; }
        //public string IsRecurring { get; set; }
        //public string RecurringCycleLength { get; set; }
        //public string RecurringCyclePeriod { get; set; }
        //public string RecurringTotalCycles { get; set; }
        //public string IsRental { get; set; }
        //public string RentalPriceLength { get; set; }
        //public string RentalPricePeriod { get; set; }
        //public string IsShipEnabled { get; set; }
        //public string IsFreeShipping { get; set; }
        //public string ShipSeparately { get; set; }
        //public string AdditionalShippingCharge { get; set; }
        //public string DeliveryDate { get; set; }
        //public string IsTaxExempt { get; set; }
        //public string TaxCategory { get; set; }
        //public string IsTelecommunicationsOrBroadcastingOrElectronicServices { get; set; }
        //public string ManageInventoryMethod { get; set; }
        //public string ProductAvailabilityRange { get; set; }
        //public string UseMultipleWarehouses { get; set; }
        //public string WarehouseId { get; set; }
        //public string DisplayStockAvailability { get; set; }
        //public string DisplayStockQuantity { get; set; }
        //public string MinStockQuantity { get; set; }
        //public string LowStockActivity { get; set; }
        //public string NotifyAdminForQuantityBelow { get; set; }
        //public string BackorderMode { get; set; }
        //public string AllowBackInStockSubscriptions { get; set; }
        //public string OrderMinimumQuantity { get; set; }
        //public string OrderMaximumQuantity { get; set; }
        public string AllowedQuantities { get; set; }
        //public string AllowAddingOnlyExistingAttributeCombinations { get; set; }
        //public string NotReturnable { get; set; }
        //public string DisableBuyButton { get; set; }
        //public string DisableWishlistButton { get; set; }
        //public string AvailableForPreOrder { get; set; }
        //public string PreOrderAvailabilityStartDateTimeUtc { get; set; }
        //public string CallForPrice { get; set; }
        //public string Price { get; set; }
        //public string OldPrice { get; set; }
        //public string ProductCost { get; set; }
        //public string CustomerEntersPrice { get; set; }
        //public string MinimumCustomerEnteredPrice { get; set; }
        //public string MaximumCustomerEnteredPrice { get; set; }
        //public string BasepriceEnabled { get; set; }
        //public string BasepriceAmount { get; set; }
        //public string BasepriceUnit { get; set; }
        //public string BasepriceBaseAmount { get; set; }
        //public string BasepriceBaseUnit { get; set; }
        public string MarkAsNew { get; set; }
        public string MarkAsNewStartDateTimeUtc { get; set; }
        public string MarkAsNewEndDateTimeUtc { get; set; }
        public string Weight { get; set; }
        public string Length { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        [Required]
        public string Categories { get; set; } = null!;
        public string Manufacturers { get; set; }
        public string ProductTags { get; set; }
        //属性字段
        //public string AttributeTextPrompt { get; set; }
        //public string AttributeIsRequired { get; set; }
        //public string AttributeDisplayOrder { get; set; }
        //public string ProductAttributeValueId { get; set; }
        //public string AssociatedProductId { get; set; }
        //public string ImageSquaresPictureId { get; set; }
        public string PriceAdjustment { get; set; }
        //public string PriceAdjustmentUsePercentage { get; set; }
        //public string WeightAdjustment { get; set; }
        //public string Cost { get; set; }
        //public string CustomerEntersQty { get; set; }
        public string Quantity { get; set; }
        //public string IsPreSelected { get; set; }
        //public string PictureId { get; set; }
    }
}
