﻿using ERP.Export.ExportCommon;
using ERP.Export.Templates;
using ERP.Models.Export;
using System.ComponentModel.DataAnnotations;
using Sy.Security;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class GlobalSources : ExcelBase<GlobalSources, GlobalSources.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.GlobalSources;
    public override string Name => "GlobalSources导出模板";


    public GlobalSources(IServiceProvider serviceProvider) : base(serviceProvider)
    {
    }

    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem data, ExcleRole role,
        string parameter, List<Unit> rate)
    {
        var product = new List<List<string?>>();
        var item = ProductExportHelper.ToDeserialize<ParameterItem>(parameter);
        if (item is null)
        {
            _Logger.LogInformation("{Name}ParameterItem is Null", Name);
            return null!;
        }

        //子产品
        foreach (var p in data.Products)
        {
            var childPro = _Common.GetCommonData(p, data.Attribute, data.Source, data.Sku, TemplateType);
            var chileExcelPro = GetProduct(childPro, item, role, rate);
            if (chileExcelPro.IsNotNull())
            {
                product.Add(chileExcelPro);
                if (role.TaskId.HasValue)
                    await DownloadImages(role.TaskId.Value, data.Sku, childPro.Images.Prepend(childPro.MainImage!).Distinct().ToList());
            }
        }

        var dic = new Dictionary<int, List<List<string?>>>();
        dic.Add(0, product);
        return dic;
    }

    private List<string?> GetProduct(ExcelProduct data, ParameterItem item, ExcleRole role,
        List<Unit> rate)
    {
        //描述
        var description = _Common.GetDescription(data.Description, string.Join(";", data.BulletPoint),
            data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, data.Images);
        //卖点
        var bulletPoint = _Common.GetBulletPoint(data.BulletPoint, data.SearchTerms, data.Description, role.SketchMode);
        //关键字
        var keyword = _Common.GetKeywords(data.BulletPoint, data.SearchTerms, data.Description, role.KeywordMode);
        //主产品
        var pro = new List<string?>();
        pro.Add(item.ProductCategory);
        pro.Add(data.Name);
        pro.Add(string.Join(',', keyword));
        pro.Add(data.Name);
        ProductExportHelper.SetCommonCell(pro, "", 2);
        pro.Add(data.Sku);
        pro.Add(description);
        pro.Add(item.MinimumOrderQuantity);
        pro.Add(item.UnitOfMeasure);
        var minPrice = ProductExportHelper.GetDataPriceDecimal(data.Cost, rate);
        pro.Add(minPrice.IsSuccess ? minPrice.Value.ToString("0.00") : "");
        var maxPrice = ProductExportHelper.GetDataPriceDecimal(data.Price, rate);
        pro.Add(maxPrice.IsSuccess ? maxPrice.Value.ToString("0.00") : "");
        pro.Add(item.FobPort);
        pro.Add(item.ShortestOrderLeadTime);
        pro.Add(item.NormalOrderLeadTime);
        pro.Add(item.PaymentMethod); // Payment Method

        // Sample Policy
        pro.Add(item.SampleAvailability);
        pro.Add(item.DirectSale); // Direct Sale
        pro.Add(item.SelectSamplePolicy); // Select Sample Policy
        pro.Add("");
        pro.Add("");
        pro.Add("");
        pro.Add("");

        // Additional Product Information
        pro.Add(item.SmallOrdersAccepted);
        ProductExportHelper.SetCommonCell(pro, "", 14);
        pro.Add(item.BrandName);
        pro.Add(item.Origin);

        // Additional Product Information (Main Export Market(s))
        pro.Add("Yes");
        pro.Add("Yes");
        pro.Add("Yes");
        pro.Add("Yes");
        pro.Add("Yes");
        pro.Add("Yes");
        pro.Add("Yes");

        return pro;
    }

    private async Task DownloadImages(int taskId, string sku, List<string> images)
    {
        foreach (var image in images)
        {
            if (string.IsNullOrEmpty(image))
                continue;

            var hash = MD5.Encrypt(image);
            var filename = $"{sku}/{hash}.jpg";
            var tempPath = Path.Combine("temp", $"{taskId}_ext", filename);
            if (!File.Exists(tempPath))
            {
                await ProductExportHelper.DownloadAsync(image, tempPath);
            }
        }
    }

    public class ParameterItem : CommonParameter
    {
        public string? Unit { get; set; } = "USD";

        [Required] public string ProductCategory { get; set; } = null!;
        [Required] public string MinimumOrderQuantity { get; set; } = "99";
        [Required] public string UnitOfMeasure { get; set; } = null!;
        public string? FobPort { get; set; } = "Shenzhen";
        public string? BrandName { get; set; }
        public string? Origin { get; set; } = "China";
        public string? ShortestOrderLeadTime { get; set; } = "14";
        public string? NormalOrderLeadTime { get; set; } = "38";
        public string? PaymentMethod { get; set; } = "Telegraphic Transfer (TT,T/T)";
        public string? SampleAvailability { get; set; } = "Yes";
        public string? DirectSale { get; set; } = "No";
        public string? SelectSamplePolicy { get; set; } = "We do not offer free samples, but will reimburse the buyer once an order is confirmed";

        public string SmallOrdersAccepted { get; set; } = "Yes";
    }
}