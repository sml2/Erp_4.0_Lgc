﻿using ERP.Models.Export;
using ERP.Export.Templates;
using System.ComponentModel.DataAnnotations;
using ERP.Export.ExportCommon;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class QuickMaxvic : ExcelBase<QuickMaxvic>
{
    public override TemplateType TemplateType => TemplateType.QuickMaxvic;
    public override string Name => "QuickmaxvicBulkUpload";

    public QuickMaxvic(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }
    public override bool TryValidate(string parameter, out List<ValidationResult> results)
    {
        results = new List<ValidationResult>();
        return true;
    }

    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
    {
        List<List<string>> Product = new List<List<string>>();
        string Sku = Data.Sku;
        List<string> MainPro = new List<string>();
        MainPro.Add(Data.Name);
        MainPro.Add(Sku);
        int Quantity = 0;
        List<string> PageImages = new List<string>();
        if (Data.Products.IsNotNull() && Data.Products.Count > 0)
        {
            Data.Products.ForEach((r) =>
            {
                Quantity += r.Quantity;
                if (!PageImages.Contains(r.MainImage)) PageImages.Add(r.MainImage);
                r.Images.ForEach((Img) =>
                {
                    if (!PageImages.Contains(Img)) PageImages.Add(Img);
                });
            });
        }
        //描述
        string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, PageImages);
        var TargetNewline = "\r\n";
        Description = Description.Replace("<br>", TargetNewline).Replace("<br />", TargetNewline).Replace("<br/>", TargetNewline);
        //卖点
        var BulletPoint = _Common.GetBulletPoint(Data.BulletPoint, Data.SearchTerms, Data.Description, role.SketchMode);
        //关键字
        var Keyword = _Common.GetKeywords(Data.BulletPoint, Data.SearchTerms, Data.Description, role.KeywordMode);
        MainPro.Add(Description);
        MainPro.Add(Description);
        MainPro.Add(string.Join(",", Keyword));
        string Price = await _Common.GetMainSalePrice(Data.Price, Rate);
        MainPro.Add(Price);
        MainPro.Add(Quantity.ToString());
        MainPro.Add(Data.Source);
        List<string> Colors = new List<string>();
        List<string> Sizes = new List<string>();
        Data.Attribute.ForEach((r) =>
        {
            if (_Common.Colors.Contains(r.Key.ToLower())) Colors = r.Value.ToList();
            if (ProductExportHelper.Sizes.Contains(r.Key.ToLower())) Sizes = r.Value.ToList();
        });
        MainPro.Add(string.Join(",", Colors));
        MainPro.Add(string.Join(",", Sizes));
        ExportCommon.ProductExportHelper.SetCommonImage(MainPro, PageImages, 5);
        Product.Add(MainPro);
        var Dic = new Dictionary<int, List<List<string>>>();
        Dic.Add(0, Product);
        return Dic;
    }
}
