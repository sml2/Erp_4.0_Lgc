﻿using ERP.Export.ExportCommon;
using ERP.Export.Templates;
using ERP.Models.Export;

namespace ERP.Export.V4_Export.ExcelTemplates
{
    public class Fecshop : ExcelBase<Fecshop, Fecshop.ParameterItem>
    {
        public override TemplateType TemplateType => TemplateType.Fecshop;

        public override string Name => "Fecshop表";

        public Fecshop(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }

        public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
        {
            ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
            if (Item.IsNull())
            {
                _Logger.LogInformation($"{Name} GetData ParameterItem Is Null");
                return null;
            }
            var Product = new List<List<string?>>();
            string Sku = Data.Sku;
            if (Data.Products.IsNull() || Data.Products.Count == 0)
            {
                string Price = await _Common.GetMainSalePrice(Data.Price, Rate);
                string Cost = _Common.GetMainCostPrice(Data.Price, Rate);
                ExcelProduct MainPro = new ExcelProduct(Data.Name, Data.Description, Data.BulletPoint, Data.SearchTerms, "", "", Sku, "", null, Data.Quantity, Price, Cost, "", null, Data.Source);
                var MainExcelPro = await GetProduct(MainPro, Item, Rate, role, true);
                if (MainExcelPro.IsNotNull()) Product.Add(MainExcelPro);
            }
            else
            {
                foreach (var p in Data.Products)
                {
                    ExcelProduct ChildPro = _Common.GetCommonData(p, Data.Attribute, Data.Source, Sku);
                    var ChileExcelPro = await GetProduct(ChildPro, Item, Rate, role, true);
                    if (ChileExcelPro.IsNotNull()) Product.Add(ChileExcelPro);
                }
            }
            Dictionary<int, List<List<string?>>>? Dic = new Dictionary<int, List<List<string?>>>();
            Dic.Add(0, Product);
            return Dic;
        }
        private Task<List<string?>> GetProduct(ExcelProduct Data, ParameterItem Item, List<Unit> Rate, ExcleRole role, bool IsMain)
        {
            //描述
            string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, Data.Images);
            //关键字
            var Keyword = _Common.GetKeywords(Data.BulletPoint, Data.SearchTerms, Data.Description, role.KeywordMode);
            var Pro = new List<string?>();
            Pro.Add(Data.Name);
            Pro.Add("");
            Pro.Add(Data.Sku);
            Pro.Add(Data.Sku);
            Pro.Add(Item.category_ids);
            Pro.Add(Item.weight);
            Pro.Add("1");
            Pro.Add(Data.Source);
            Pro.Add(Data.Quantity.ToString());
            Pro.Add("1");
            Pro.Add(Item.remark);
            string Price = Data.Price;
            string Cost = Data.Cost;
            if (!IsMain)
            {
                Cost = ProductExportHelper.GetDataPrice(Data.Cost, Rate);
                Price = ProductExportHelper.GetDataPrice(Data.Price, Rate);
            }
            Pro.Add(Cost);
            Pro.Add(Price);
            Pro.Add(Price);
            Pro.Add("");
            Pro.Add(Data.Name);
            Pro.Add(string.Join(" ", Keyword));
            Pro.Add("");
            Pro.Add(Description);
            Pro.Add(Description);
            Pro.Add(Data.MainImage);
            Pro.Add(string.Join(",", Data.Images));
            Dictionary<string, string> Dic = new Dictionary<string, string>();
            if (Data.Color.IsNotWhiteSpace()) Dic.Add("Color", Data.Color);
            if (Data.Size.IsNotWhiteSpace()) Dic.Add("Size", Data.Size);
            string? AttrValue = ProductExportHelper.ToSerialize(Dic);
            if (Dic.Count == 0) AttrValue = Item.group_attr_name;
            //if (Data.Color.IsNotWhiteSpace()) AttrValue += $"\"Color\":\"{Data.Color}\"";
            //if (AttrValue.Length > 0) AttrValue += ",";
            //if (Data.Size.IsNotWhiteSpace()) AttrValue += $"\"Size\":\"{Data.Size}\"";
            //if (AttrValue.IsNullOrEmpty()) AttrValue = Item.group_attr_name;
            Pro.Add(AttrValue);
            Pro.Add("");
            return Task.FromResult(Pro);
        }
        public class ParameterItem //: JsonObject<ParameterItem>
        {
            public int is_trans { get; set; }
            public string? category_ids { get; set; }
            public string? weight { get; set; }
            public string? group_attr_name { get; set; }
            public string? remark { get; set; }
        }
    }
}
