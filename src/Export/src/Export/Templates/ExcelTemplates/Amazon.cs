﻿using ERP.Export.ExportCommon;
using ERP.Export.Templates;
using ERP.Models.Export;
using System.Text;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using Microsoft.Extensions.Options;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class Amazon : ExcelBase<Amazon, Amazon.ParameterItem>
{
    private readonly IOptions<ExportFeatures> _features;
    public override TemplateType TemplateType => TemplateType.Amazon;
    public override string Name => "Amazon通用导出模板";


    public Amazon(IServiceProvider serviceProvider, IOptions<ExportFeatures> features) : base(serviceProvider)
    {
        _features = features;
    }

    public override async Task<Dictionary<string, List<List<string>>>> GetHead(string parameter)
    {
        var head = await _Common.GetHead(TemplateType);
        var outPut = _Common.GetCustomAttribute(parameter);
        if (outPut.Count == 2 && head.IsNotNull() && head.Count > 0)
        {
            var pageOneHead = head.Values.ToList()[0];
            if (pageOneHead.Count > 1) pageOneHead[1].AddRange(outPut[0]);
            if (pageOneHead.Count > 2) pageOneHead[2].AddRange(outPut[0]); //加key()
        }

        return head;
    }

    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role,
        string Parameter, List<Unit> Rate)
    {
        var Product = new List<List<string?>>();
        ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
        if (Item is null)
        {
            _Logger.LogInformation("{Name}ParameterItem is Null", Name);
            return null!;
        }

        string Sku = Data.Sku;
        string? MainImage = "";
        List<string>? Images = new List<string>();
        if (Data.Products.IsNotNull() && Data.Products.Count > 0)
        {
            if (role.MainProImgSetting == ExcleRole.MainProImgType.FirstVariantImage)
            {
                MainImage = Data.Products.First().MainImage;
                Images = Data.Products.First().Images;
            }
            else if (role.MainProImgSetting == ExcleRole.MainProImgType.LastVariantImage)
            {
                MainImage = Data.Products.Last().MainImage;
                Images = Data.Products.Last().Images;
            }
            else if (role.MainProImgSetting == ExcleRole.MainProImgType.EveryFirstImage)
            {
                List<string> NewImage = new List<string>();
                Data.Products.ForEach((r) =>
                {
                    if (r.Images.IsNotNull() && r.Images.Count > 0) NewImage.Add(r.Images.First());
                });
                if (NewImage.Count > 0)
                {
                    MainImage = NewImage.First();
                    Images = NewImage.Skip(1).ToList();
                }
            }
        }

        ExcelProduct Pro = new ExcelProduct(Data.Name, Data.Description, Data.BulletPoint, Data.SearchTerms, "", "",
            Sku, MainImage, Images, Data.Quantity, "", "", string.Empty, null, string.Empty);
        //变体属性ColorSize
        string Attribute = "";
        Data.Attribute.ForEach((r) =>
        {
            var Key = r.Key.ToLower();
            if (_Common.Colors.Contains(Key)) Attribute += "Color";
            if (ProductExportHelper.Sizes.Contains(Key)) Attribute += "Size";
        });
        if (Attribute.IsNullOrEmpty() && Data.Attribute.Count == 1) Attribute = "Color";
        if (Data.Attribute.Count > 1) Attribute = "ColorSize";

        if (Data.Products.Count == 1 && Data.Products[0].Sku == Sku)
        {
            //无变体
            Attribute = "";
        }
        else
        {
            var MainPro = GetProduct(Pro, Item, role, Attribute, Rate, Sku, Parameter, true);
            if (MainPro.IsNotNull()) Product.Add(MainPro);
        }

        //子产品
        if (Data.Products.Count > 0)
        {
            foreach (var p in Data.Products)
            {
                ExcelProduct ChildPro =
                     _Common.GetCommonData(p, Data.Attribute, Data.Source, Sku, TemplateType.Amazon);
                var ChileExcelPro = GetProduct(ChildPro, Item, role, Attribute, Rate, Sku, Parameter, false);
                if (ChileExcelPro.IsNotNull()) Product.Add(ChileExcelPro);
            }
        }

        var Dic = new Dictionary<int, List<List<string?>>>();
        Dic.Add(0, Product);
        return Dic;
    }

    private List<string?> GetProduct(ExcelProduct Data, ParameterItem Item, ExcleRole role, string Attribute,
        List<Unit> Rate, string Sku, string Parameter, bool IsMain)
    {
        //描述
        string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint),
            Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, Data.Images);
        //卖点
        var BulletPoint = _Common.GetBulletPoint(Data.BulletPoint, Data.SearchTerms, Data.Description, role.SketchMode);
        //关键字
        var Keyword = _Common.GetKeywords(Data.BulletPoint, Data.SearchTerms, Data.Description, role.KeywordMode);
        //主产品
        var Pro = new List<string?>();
        Pro.Add(Item.feed_product_type);
        Pro.Add(Data.Sku);
        Pro.Add(Item.brand_name);
        Pro.Add(Data.Name);
        Pro.Add(Data.ProduceCode); //UPC Value
        string ProduceType = "";
        if (Data.ProduceCode is { Length: 12 }) ProduceType = "UPC";
        else if (Data.ProduceCode is { Length: 13 }) ProduceType = "EAN";
        Pro.Add(ProduceType); //UPC EAN
        Pro.Add(Item.item_type);
        Pro.Add(Item.outer_material_type);
        ProductExportHelper.SetCommonCell(Pro, "", 4);
        Pro.Add(Data.Color);
        Pro.Add(Data.Color); //Color Map
        Pro.Add(Data.Size); //Size
        Pro.Add(Item.department_name); //Department
        Pro.Add(Data.Size); //Size
        string Price = ProductExportHelper.GetDataPrice(Data.Price, Rate);
        Pro.Add(Price); //Standard Price
        if (IsMain) Pro.Add("");
        else Pro.Add(Data.Quantity.ToString());
        Pro.Add(Data.MainImage); //Main Image
        Pro.Add(Item.material_composition);
        ProductExportHelper.SetCommonCell(Pro, "", 9);
        Pro.Add(Item.hard_disk_size_unit_of_measure);
        Pro.Add(Item.computer_cpu_speed);
        Pro.Add(Item.computer_cpu_type);
        Pro.Add(Item.hardware_platform);
        Pro.Add(Item.computer_cpu_manufacturer);
        Pro.Add(Item.operating_system);
        Pro.Add(Item.bbfc_rating);
        Pro.Add(Item.binding);
        Pro.Add(Item.metal_type);
        Pro.Add(Item.setting_type);
        Pro.Add(Item.gem_type);
        Pro.Add(Data.MainImage); //Swatch Image URL
        ProductExportHelper.SetCommonImage(Pro, Data.Images, 8);
        if (Attribute.IsNotWhiteSpace() && Attribute.Length > 0)
        {
            if (IsMain) Pro.Add("parent");
            else Pro.Add("child");
        }
        else Pro.Add(""); //base-product

        if (!IsMain)
        {
            if (Attribute.IsNotWhiteSpace() && Attribute.Length > 0)
            {
                Pro.Add(Sku); //Parent SKU
                Pro.Add("Variation"); //Relationship Type
            }
            else
            {
                Pro.Add("");
                Pro.Add("");
            }
        }
        else
        {
            Pro.Add("");
            Pro.Add("");
        }

        Pro.Add(Attribute); //Variation Theme
        Pro.Add("Update");
        Pro.Add(Item.recommended_browse_nodes);
        Pro.Add(Description);
        Pro.Add("");
        Pro.Add(Item.model);
        ProductExportHelper.SetCommonCell(Pro, "", 5); //Inner Material Type
        Pro.Add(Item.part_number);
        Pro.Add(Item.manufacturer);
        BulletPoint.ForEach((r) => { Pro.Add(r); });
        Keyword.ForEach((r) => { Pro.Add(r); });
        ProductExportHelper.SetCommonCell(Pro, "", 9);
        Pro.Add(Item.neck_style);
        ProductExportHelper.SetCommonCell(Pro, "", 8);
        Pro.Add(Item.style_name);
        Pro.Add("");
        if (Attribute.IsNotWhiteSpace() && IsMain)
        {
            ProductExportHelper.SetCommonCell(Pro, "", 23);
            Pro.Add(Item.country_of_origin);
            ProductExportHelper.SetCommonCell(Pro, "", 58);
        }
        else
        {
            if (double.TryParse(Item.website_shipping_weight, out var weight) && weight > 0)
            {
                Pro.Add((weight / 1000).ToString());
                Pro.Add("KG");
            }
            else
                Pro.AddRange(Enumerable.Repeat(string.Empty, 2));

            ProductExportHelper.SetCommonCell(Pro, "", 4);
            Pro.Add(Item.item_length_longer_edge);
            Pro.Add(Item.length_longer_edge_unit_of_measure);
            Pro.Add(Item.item_width_shorter_edge);
            Pro.Add(Item.width_shorter_edge_unit_of_measure);
            // fufillment
            Pro.Add(Item.fufillment_center_id);
            ProductExportHelper.SetCommonCell(Pro, "", 12);
            Pro.Add(Item.country_of_origin);
            ProductExportHelper.SetCommonCell(Pro, "", 38);
            Pro.Add(Item.condition_type);
            Pro.Add(Price);
            Pro.Add("");
            Pro.Add(Item.fulfillment_latency);
            Pro.Add(DateTime.Today.ToString("yyyy/MM/dd"));
            Pro.Add(Rate.First().Sign);
            ProductExportHelper.SetCommonCell(Pro, "", 8);
            Pro.Add(Item.number_of_items);
            ProductExportHelper.SetCommonCell(Pro, "", 3);
            Pro.Add(Item.MaxOrderQuantity);
            Pro.Add(Item.merchant_shipping_group_name);
        }

        ProductExportHelper.SetCommonCell(Pro, "", 4);
        Pro.Add(Item.pattern_type);
        ProductExportHelper.SetCommonCell(Pro, "", 2);
        Pro.Add(Item.unit_count);
        Pro.Add(Item.unit_count_type);
        Pro.Add(Item.season);
        Pro.Add(Item.target_gender);
        Pro.Add(Item.is_adult_product);
        Pro.Add(Item.footwear_size_system ?? "");
        Pro.Add(Item.footwear_age_group ?? "");
        Pro.Add(Item.footwear_size_class ?? "");
        Pro.Add(Item.footwear_width ?? "");
        Pro.Add(Item.footwear_size ?? "");
        Pro.Add(Item.age_range_description ?? "");
        Pro.Add(IsMain ? "" : Item.is_expiration_dated_product ?? "");
        Pro.Add(Item.EnableApparelSize ? Data.Size : string.Empty);
        Pro.Add(Item.item_form);
        Pro.Add(Item.net_content_length_unit_of_measure);
        Pro.Add(Item.net_content_count_unit_of_measure);
        Pro.Add(Item.net_content_volume_unit_of_measure);
        Pro.Add(Item.net_content_count);
        Pro.Add(Item.net_content_area);
        Pro.Add(Item.net_content_weight);
        Pro.Add(Item.net_content_weight_unit_of_measure);
        Pro.Add(Item.net_content_area_unit_of_measure);
        Pro.Add(Item.net_content_volume);
        Pro.Add(Item.net_content_length);
        //Pro.Add(IsMain ? "" : (Item.fufillment_center_id ?? ""));

        if (_features.Value.ExportAmazonB2CEnabled)
        {
            var businessPrice = !string.IsNullOrEmpty(Item.BusinessPrice) ? Item.BusinessPrice : Price;
            if (decimal.TryParse(businessPrice, out var decPrice))
                businessPrice = (decPrice / 100 * 95).ToString("0.00");
            Pro.Add(businessPrice);
            Pro.Add(!string.IsNullOrEmpty(businessPrice)
                ? (!string.IsNullOrEmpty(Item.QuantityPriceType) ? Item.QuantityPriceType : "percent")
                : "");

            int needToFilled;
            var bounds = Item.QuantityBounds is { Count: > 0 }
                ? Item.QuantityBounds
                : new List<ParameterItem.QuantityBound>() { new(5, "10") };
            if (string.IsNullOrWhiteSpace(Price))
            {
                needToFilled = 5;
                bounds = new List<ParameterItem.QuantityBound>(0);
            }
            else
            {
                needToFilled = bounds.Count > 5 ? 0 : 5 - bounds.Count;
            }

            foreach (var bound in bounds)
            {
                Pro.Add(bound.QuantityLowerBound.ToString());
                Pro.Add(bound.QuantityPrice);
            }

            Pro.AddRange(Enumerable.Repeat(string.Empty, needToFilled * 2));
            Pro.Add(string.Empty); //national_stock_number
            Pro.Add(string.Empty); // unspsc_code
            Pro.Add(Item.PricingAction);
        }
        else
        {
            Pro.AddRange(Enumerable.Repeat(string.Empty, 15));
        }

        List<List<string>> OutPut = _Common.GetCustomAttribute(Parameter);
        if (OutPut.IsNotNull() && OutPut.Count == 2)
        {
            //加自定义Value
            if (IsMain) ProductExportHelper.SetCommonCell(Pro, "", OutPut.Count);
            else Pro.AddRange(OutPut[1]);
        }

        return Pro;
    }

    public class ParameterItem : CommonParameter
    {
        public string? unit { get; set; }

        [Required] public string brand_name { get; set; } = null!;
        [Required] public string manufacturer { get; set; } = null!;
        [Required] public string feed_product_type { get; set; } = null!;

        /// <summary>
        /// 是否将size字段填充到apparel_size
        /// </summary>
        public bool EnableApparelSize { get; set; } = true;

        public string? recommended_browse_nodes { get; set; }
        public string? website_shipping_weight { get; set; }
        public string? item_type { get; set; }
        public string? department_name { get; set; }
        public string? merchant_shipping_group_name { get; set; }
        public string? material_composition { get; set; }
        public string? outer_material_type { get; set; }
        public string? fulfillment_latency { get; set; }
        public string? condition_type { get; set; }
        public string? country_of_origin { get; set; }
        public string? neck_style { get; set; }
        public string? style_name { get; set; }
        public string? model { get; set; }
        public string? part_number { get; set; }
        public string? metal_type { get; set; }
        public string? setting_type { get; set; }
        public string? gem_type { get; set; }
        public string? hard_disk_size_unit_of_measure { get; set; }
        public string? computer_cpu_speed { get; set; }
        public string? computer_cpu_type { get; set; }
        public string? hardware_platform { get; set; }
        public string? computer_cpu_manufacturer { get; set; }
        public string? operating_system { get; set; }
        public string? bbfc_rating { get; set; }
        public string? binding { get; set; }
        public string? pattern_type { get; set; }
        public string? unit_count { get; set; }
        public string? unit_count_type { get; set; }
        public string? season { get; set; }
        public string? target_gender { get; set; }
        public string? is_adult_product { get; set; }
        public string? footwear_size_system { get; set; }
        public string? footwear_age_group { get; set; }
        public string? footwear_size_class { get; set; }
        public string? footwear_width { get; set; }
        public string? footwear_size { get; set; }
        public string? age_range_description { get; set; }
        public string? is_expiration_dated_product { get; set; }
        public string? fufillment_center_id { get; set; }
        public string? number_of_items { get; set; }
        public string? item_form { get; set; }
        public string? net_content_length_unit_of_measure { get; set; }
        public string? net_content_count_unit_of_measure { get; set; }
        public string? net_content_volume_unit_of_measure { get; set; }
        public string? net_content_count { get; set; }
        public string? net_content_area { get; set; }
        public string? net_content_weight { get; set; }
        public string? net_content_weight_unit_of_measure { get; set; }
        public string? net_content_area_unit_of_measure { get; set; }
        public string? net_content_volume { get; set; }
        public string? net_content_length { get; set; }

        public string? item_length_longer_edge { get; set; }
        public string? length_longer_edge_unit_of_measure { get; set; }
        public string? item_width_shorter_edge { get; set; }
        public string? width_shorter_edge_unit_of_measure { get; set; }
        #region b2b

        /// <summary>
        /// <para>所有b2b字段可在<see href="https://images-na.ssl-images-amazon.com/images/G/31/rainier/help/Flat.File.PriceInventory_b2b._CB1198675309_.xls">此处</see>下载表格查看</para>
        /// <para>该价格提供给商家进货使用, 单位是usd. 如果该字段不为空, 值应该小于或等于产品价格.值应该大于0.不需要包含千位分隔符和货币单位.如果提供了<see cref="QuantityBound.QuantityPrice" />字段,则该字段必填.</para>
        /// <para><b>原文: </b>The price at which the merchant offers the product to Verified Business customers, expressed in U.S. dollars. If price exists, business-price should be less than or equal to price, as business-price represents a discount for business customers. The price should be greater than 0. Do not include thousands separators or currency symbols. This field is required if quantity prices are provided. </para>
        /// </summary>
        public string BusinessPrice { get; set; } = string.Empty;

        /// <summary>
        /// <para>值为<c>fixed</c>或<c>percent</c>. 使用<c>fixed</c>时, <see cref="QuantityBound.QuantityPrice" />填写固定美元价格. 当使用<c>percent</c>时, <see cref="QuantityBound.QuantityPrice" />为扣除百分比的价格.</para>
        /// <para><b>原文: </b>Either fixed or percent. When fixed is selected, quantity prices are expressed in U.S. dollars. When percent is selected, quantity prices are calculated as a percent off the business price.</para>
        /// </summary>
        /// <value>fixed | percent</value>
        public string QuantityPriceType { get; set; } = string.Empty;

        /// <summary>
        /// <para>
        /// 执行b2b价格的操作. 目前只支持删除<see cref="BusinessPrice" />.<br />
        /// 当你想删除以前提交的<see cref="BusinessPrice" />和所有<see cref="QuantityBound.QuantityPrice" />时, 设置值为<c>delete business_price</c>. 如果留空,什么操作也不执行.
        /// </para>
        /// <para><b>原文: </b>"Specifies the actions that need to be performed on the price attributes of this SKU. The only action currently supported by the system is deleting the business_price.<br />
        /// Set this field to ""delete business_price"" when you want to remove a previously submitted business_price and any quantity discounts.If left blank, no action will be performed and the contents of this column will be ignored."
        /// </para>
        /// </summary>
        /// <value><c>delete business_price</c></value>
        public string PricingAction { get; set; } = string.Empty;

        /// <summary>
        /// 价格区间数组
        /// </summary>
        public List<QuantityBound> QuantityBounds { get; set; } = new();

        public string MaxOrderQuantity { get; set; } = string.Empty;

        public class QuantityBound
        {
            public QuantityBound(int quantityLowerBound, string quantityPrice)
            {
                QuantityLowerBound = quantityLowerBound;
                QuantityPrice = quantityPrice;
            }

            /// <summary>
            /// <para>产品数量阈值. 当商家客户购买的数量大于等于该阈值时,会使用对应的<see cref="QuantityPrice" />结算. 例如, 当quantity-price1为50,
            /// 购买数量大于等于50时会使用quantity-price1.卖家最多设置5个数量阈值, 并且每一个都要提供quantity-price.每一个数量阈值都应该大于前一个数量阈值.
            /// </para>
            /// <para>
            /// <b>原文: </b>Quantity thresholds, expressed in units. Business customers who purchase at or above a quantity-lower-bound will be given the corresponding quantity-price.
            /// For example, if lower-bound1 is set to 50, quantity-price1 will apply to a purchase of 50 or more units.
            /// Sellers can specify up to 5 lower-bounds and must provide a quantity-price for each lower bound. Each lower-bound must be higher than the previous one.
            /// </para>
            /// </summary>
            public int QuantityLowerBound { get; set; }

            /// <summary>
            /// <para>数量价格, 根据<see cref="ExcelProduct.QuantityPriceType" />的不同, 分别代表美元价格(值199.99 代表 $199.99), 或按百分比打折(percent off)(值为7代表扣减7%即原价的93%). 该值必填. 每一个价格都应该小于上一个(如果使用百分比, 值应该比上一个大)</para>
            /// <para><b>原文: </b>Quantity prices, expressed either as U.S. dollars (199.99 = $199.99) or as a percent off the business-price (7= 7% off business-price), depending on the chosen quantity-price type. Must provide a lower-bound for each quantity-price. Each quantity-price must be lower than the previous one.</para>
            /// </summary>
            public string QuantityPrice { get; set; }
        }

        #endregion
    }
}