﻿using ERP.Export.ExportCommon;
using ERP.Export.Templates;
using ERP.Models.Export;

namespace ERP.Export.V4_Export.ExcelTemplates
{
    public class Fruugo : ExcelBase<Fruugo, Fruugo.ParameterItem>
    {
        public override TemplateType TemplateType => TemplateType.Fruugo;

        public override string Name => "Fruugo导出模板";

        public Fruugo(IServiceProvider serviceProvider) : base(serviceProvider) { }
        
        public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem data, ExcleRole role, string parameter, List<Unit> rate)
        {
            var product = new List<List<string?>>();
            ParameterItem? item = ProductExportHelper.ToDeserialize<ParameterItem>(parameter);
            if (item is null)
            {
                //Log(I18N.Instance.Export_Log3);
                _Logger.LogInformation("{Name} GetData ParameterItem Is Null", Name);
                return null!;
            }
            string sku = data.Sku;
            string? mainImage = "";
            List<string>? images = new List<string>();
            if (data.Products.IsNotNull() && data.Products.Count > 0)
            {
                if (role.MainProImgSetting == ExcleRole.MainProImgType.FirstVariantImage)
                {
                    mainImage = data.Products.First().MainImage;
                    images = data.Products.First().Images;
                }
                else if (role.MainProImgSetting == ExcleRole.MainProImgType.LastVariantImage)
                {
                    mainImage = data.Products.Last().MainImage;
                    images = data.Products.Last().Images;
                }
                else if (role.MainProImgSetting == ExcleRole.MainProImgType.EveryFirstImage)
                {
                    List<string> newImage = new List<string>();
                    data.Products.ForEach((r) => { if (r.Images.IsNotNull() && r.Images.Count > 0) newImage.Add(r.Images.First()); });
                    if (newImage.Count > 0)
                    {
                        mainImage = newImage.First();
                        images = newImage.Skip(1).ToList();
                    }
                }
            }
            ExcelProduct pro = new ExcelProduct(data.Name, data.Description, data.BulletPoint, data.SearchTerms, "", "", sku, mainImage, images, data.Quantity, "", "", "", null, "");
            //变体属性ColorSize
            string attribute = "";
            data.Attribute.ForEach((r) =>
            {
                var key = r.Key.ToLower();
                if (_Common.Colors.Contains(key)) attribute += "Color";
                if (ProductExportHelper.Sizes.Contains(key)) attribute += "Size";
            });
            if (attribute.IsNullOrEmpty() && data.Attribute.Values.Count() == 1) attribute = "Color";
            if (data.Attribute.Count > 1) attribute = "ColorSize";

            if (data.Products.Count == 1 && data.Products[0].Sku == sku)
            {
                //无变体
                attribute = "";
            }
            else
            {
                //20211211 去掉主产品行,需求来自djf飞书
                //var MainPro = GetProduct(Pro, Item, role, Attribute, Rate, Sku, Parameter, true);
                //if (MainPro.IsNotNull()) Product.Add(MainPro);
            }
            //子产品
            if (data.Products.IsNotNull() && data.Products.Count > 0)
            {
                string pcode = string.Empty;
                bool isFirst = true;
                foreach (var p in data.Products)
                {
                    if (isFirst) pcode = p.ProduceCode;
                    ExcelProduct childPro = _Common.GetCommonData(p, data.Attribute, data.Source, sku);
                    var chileExcelPro = await GetProduct(childPro, item, role, attribute, rate, sku, parameter, isFirst, pcode);
                    isFirst = false;
                    if (chileExcelPro.IsNotNull()) product.Add(chileExcelPro);
                }
            }
            Dictionary<int, List<List<string?>>>? dic = new Dictionary<int, List<List<string?>>>();
            dic.Add(0, product);
            return dic;
        }
        private Task<List<string?>> GetProduct(ExcelProduct data, ParameterItem item, ExcleRole role, string attribute, List<Unit> rate, string sku, string parameter, bool isMain, string pcode)
        {
            //描述
            string description = _Common.GetDescription(data.Description, string.Join(";", data.BulletPoint), data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, data.Images);
            //卖点
            var bulletPoint = _Common.GetBulletPoint(data.BulletPoint, data.SearchTerms, data.Description, role.SketchMode);
            //关键字
            var keyword = _Common.GetKeywords(data.BulletPoint, data.SearchTerms, data.Description, role.KeywordMode);
            //主产品
            var pro = new List<string?>();

            // var skuSplit = Data.Sku.Split('-');
            // Pro.Add(skuSplit.Length - 1 > 1 ? $"{skuSplit[1]}-{skuSplit[2]}" : Data.Sku);
            pro.Add(""); //SKU, 示例文件为空值
            pro.Add(""); //STATUS“
            
            pro.Add(data.ProduceCode); //BARCODE, 必填，纯数字，采用EAN、UPC 
            //UPC
            //与BARCODE 数据一致
            pro.Add(data.ProduceCode);
            //EAN
            pro.Add("");
            //VENDOR CODE 0
            //供应商代码 客户自己填的
            pro.Add(item.VendorCode);
            //VENDOR SKU 0
            pro.Add(data.Sku);
            pro.Add("0.01"); // VENDOR COST 0 (€)
            pro.Add(""); // SHIP COST 0 (€)
            pro.Add(""); // CUSTOMS DUTY 0 (€)

            pro.Add(data.Name); //中文简称 产品的中文简称
            //英文简称
            //产品的英文简称
            pro.Add("");
            //简介
            //产品介绍信息
            pro.Add(description.Replace("\r\n", "<br>").Replace("\n", "<br>").Replace("</br>", "<br>"));

            pro.Add(item.Length);
            pro.Add(item.Width);
            pro.Add(item.Height);
            //毛重(克)
            //产品重量，单位以“克”计算
            // 从产品属性中获取
            pro.Add(item.Weight);
            // string msrp = ProductExportHelper.GetDataPrice(data.Price, rate); // 对应ERP 销售价格 cost字段
            pro.Add(string.Empty); // MSRP (€) 留空
            pro.Add("");
            
            //分类
            //产品分类，在导入时需要重新进行选择。导入的产品分类则为导入时选择的分类
            pro.Add(item.Category);
            pro.Add(string.Empty); // ORIGIN OF PRODUCT
            pro.Add("100000"); // TARIFF CODE, 默认填100000
            pro.Add(string.Empty); // UN NUMBER
            pro.Add(isMain ? "" : pcode);
            pro.Add(data.MainImage); //产品主图 用户新表格无MainImg列（将Img0当作主图）
            //产品图1 产品图2 产品图3 产品图4 产品图5 
            ProductExportHelper.SetCommonImage(pro, data.Images, 4);

            pro.Add(data.Sku);

            pro.Add(data.Name);
            pro.Add(description.Replace("\r\n", "<br>").Replace("\n", "<br>").Replace("</br>", "<br>"));

            //END CUSTOMER PRICE (£)
            string price = ProductExportHelper.GetDataPrice(data.Cost, rate);//对应ERP 成本价格 price字段
            pro.Add(price);
            pro.Add("0.01"); // TAX PERCENT (%)   *  客户表头为0.01   税收百分比  

            //LOGISTICS			
            pro.Add("");  //PACKAGING CODE 0     *客户表格 为空
            pro.Add(""); // PACKAGING RANGE 0   *客户表格 为空
            pro.Add("STD");
            pro.Add("0");

            pro.Add(item.MISCCategory);

            //尺码
            //只填写尺码，变种主题为Size；
            pro.Add(data.Size);
            //颜色
            //产品变种主题，只填写颜色，变种主题为Color
            pro.Add(data.Color);
            // 尺码颜色都存在 | 尺码加颜色
            // 无尺码有颜色   | 颜色
            // 有尺码无颜色   | 尺码
            // 无尺码无颜色   |  空
            pro.Add("");
            //属性*9 不知道*4
            ProductExportHelper.SetCommonCell(pro, "", 13);
            //品牌
            //产品品牌属性
            pro.Add(item.Brand);
            //RESTOCK DATE
            pro.Add("");
            //LEAD TIME
            pro.Add("2");
            //PACKAGE WEIGHT
            pro.Add("");
            //DISCOUNT PRICE (WITHOUTVAT)
            pro.Add("");
            pro.Add("");
            pro.Add("");
            pro.Add("");
            var includedAttributes = new List<string>();
            if (data.Size.IsNotEmpty())
            {
                includedAttributes.Add(nameof(data.Size));
            }
            if(data.Color.IsNotEmpty())
            {
                includedAttributes.Add(nameof(data.Color));
            }
            pro.Add(string.Join(",", includedAttributes));
            return Task.FromResult(pro);
        }
        public class ParameterItem : CommonParameter
        {
            public string VendorCode { get; set; } = string.Empty; //供应商代码
            public string VendorCost { get; set; } = "0.01";//供应商折扣 字符串类型//* 客户表格 为空 
            public string ShipCost { get; set; } = "0"; //物流折扣 数值类型//* 客户表格 为0.01  
            public string CustomsDuty { get; set; } = "0"; //关税 数值类型 // *  客户表格 为空
            public string Length { get; set; } = "1"; //长CM 数值类型  **** 
            public string Width { get; set; } = "1"; //宽CM 数值类型 ****
            public string Height { get; set; } = "1"; //高CM 数值类型 ****
            public string Weight { get; set; } = "200"; //重量G 数值类型 ****
            public string MSRP { get; set; } = "0"; //MSRP 数值类型 //* MSRP建议零售价   Fruugo产品无建议零售价
            public string Category { get; set; } = "Everything Else";//分类 字符串类型 ****
            //分类下拉框（既能手输，也可以选择）
            //Apparel & Accessories > Clothing > Underwear & Socks > Bra Accessories > Bra Straps & Extenders > Womens
            //Toys & Games > Games > Dexterity Games
            //Home & Garden > Kitchen & Dining > Cookware & Bakeware > Cookware > Cookware Sets
            //Hardware > Building Consumables > Hardware Tape
            //Hardware > Tools > Pipe & Tube Cleaners
            public string MISCCategory { get; set; } = "Toys & Games > Games > Dexterity Games";//分类节点 字符串类型 ****
            public string Brand { get; set; } = "SUNRAIN";//品牌 字符串类型 ****
            public string Discount { get; set; } = "0.77";//折扣 数值 0-1；
            //可以用时间段选择器
            public string DiscountStart { get; set; } = "2022-2-23";//折扣开始时间 默认取当天
            public string DiscountEND { get; set; } = "2025-2-23";//折扣结束时间  默认取三年后今天
        }
    }
}
