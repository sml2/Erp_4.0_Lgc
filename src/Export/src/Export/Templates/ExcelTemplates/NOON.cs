﻿using ERP.Export.ExportCommon;
using ERP.Models.Export;
using ERP.Export.Templates;
using System.ComponentModel.DataAnnotations;

namespace ERP.Export.V4_Export.ExcelTemplates
{
    public class NOON : ExcelBase<NOON, NOON.ParameterItem>
    {
        public override TemplateType TemplateType => TemplateType.NOON;
        public override string Name => "NOON";

        public NOON(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }


        public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
        {
            List<List<string>> Product = new List<List<string>>();
            ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
            if (Item.IsNull())
            {
                _Logger.LogInformation($"{Name} GetData Method ParameterItem Is Null");
                return null;
            }
            string Sku = Data.Sku;
            string Price = await _Common.GetMainSalePrice(Data.Price, Rate);
            ExcelProduct Pro = new ExcelProduct(Data.Name, Data.Description, Data.BulletPoint, Data.SearchTerms, "", "", Sku, "", null, Data.Quantity, Price, "", "", null, "");
            var MainPro = await GetProduct(Pro, Item, role, Rate, Sku, true);
            if (MainPro.IsNotNull()) Product.Add(MainPro);
            //子产品
            if (Data.Products.IsNotNull() && Data.Products.Count > 0)
            {
                foreach (var p in Data.Products)
                {
                    ExcelProduct ChildPro = _Common.GetCommonData(p, Data.Attribute, Data.Source, Sku);
                    var ChileExcelPro = await GetProduct(ChildPro, Item, role, Rate, Sku, false);
                    if (ChileExcelPro.IsNotNull()) Product.Add(ChileExcelPro);
                }
            }
            var Dic = new Dictionary<int, List<List<string>>>();
            Dic.Add(0, Product);
            return Dic;
        }
        private async Task<List<string>> GetProduct(ExcelProduct Data, ParameterItem Item, ExcleRole role, List<Unit> Rate, string Sku, bool IsMain)
        {
            //描述
            string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, null);
            //卖点
            var BulletPoint = _Common.GetBulletPoint(Data.BulletPoint, Data.SearchTerms, Data.Description, role.SketchMode);
            List<string> Pro = new List<string>();
            if (IsMain) ProductExportHelper.SetCommonCell(Pro, "", 2);
            else
            {
                Pro.Add(Data.ProduceCode);
                Pro.Add(Data.Sku);
            }
            Pro.Add(Sku);
            Pro.Add(Item.brand);
            Pro.Add(Data.Name);
            Pro.Add(Item.country_origin);
            Pro.Add(Item.model_number);
            Pro.Add(Item.model_name);
            string Color = Data.Color;
            if (Color.IsNullOrEmpty()) Color = Item.colour_family;
            if (IsMain) Pro.Add("");
            else Pro.Add(Color);
            Pro.Add(Item.colour_family);
            string Size = Data.Size;
            if (Size.IsNullOrEmpty()) Size = Item.size;
            if (IsMain) ProductExportHelper.SetCommonCell(Pro, "", 2);
            else
            {
                Pro.Add(Size);
                Pro.Add(Item.size_unit);
            }
            Pro.Add(Item.base_material);
            Pro.Add(Item.secondary_material);
            Pro.Add(Item.material_finish);
            Pro.Add(Item.care_instructions);
            Pro.Add(Item.grade);
            Pro.Add(Item.item_conditon);
            Pro.Add(Item.shipping_destination);
            Pro.Add(Item.product_length);
            Pro.Add(Item.product_length_unit);
            Pro.Add(Item.product_height);
            Pro.Add(Item.product_height_unit);
            Pro.Add(Item.product_widthdepth);
            Pro.Add(Item.product_widthdepth_unit);
            Pro.Add(Item.product_weight);
            Pro.Add(Item.product_weight_unit);
            Pro.Add(Data.Quantity.ToString());
            Pro.Add(Item.shower_head_type);
            Pro.Add(Item.dispenser_type);
            Pro.Add(Item.spray_settings);
            Pro.Add(Item.assembly_required);
            //Data.BulletPoint
            if (BulletPoint.IsNull() || BulletPoint.Count == 0)
            {
                BulletPoint.Add(Item.bullet1);
                BulletPoint.Add(Item.bullet2);
                BulletPoint.Add(Item.bullet3);
                BulletPoint.Add(Item.bullet4);
                BulletPoint.Add(Item.bullet5);
            }
            ProductExportHelper.SetCommonImage(Pro, Data.BulletPoint, 5);
            Pro.Add(Item.set_includes);
            //url
            if (IsMain) ProductExportHelper.SetCommonCell(Pro, "", 7);
            else
            {
                List<string> Images = new List<string>();
                Images.Add(Data.MainImage);
                Images.AddRange(Data.Images);
                ProductExportHelper.SetCommonImage(Pro, Images, 7);
            }
            Pro.Add(Item.shipping_length);
            Pro.Add(Item.shipping_length_unit);
            Pro.Add(Item.shipping_height);
            Pro.Add(Item.shipping_height_unit);
            Pro.Add(Item.shipping_widthdepth);
            Pro.Add(Item.shipping_widthdepth_unit);
            Pro.Add(Item.shipping_weight);
            Pro.Add(Item.shipping_weight_unit);
            string Price = Data.Price;
            if (!IsMain) Price = ProductExportHelper.GetDataPrice(Data.Price, Rate);
            Pro.Add(Price);
            Pro.Add(Rate[0].Sign);
            Pro.Add(Price);
            Pro.Add(Rate[0].Sign);
            Pro.Add(Price);
            Pro.Add(Rate[0].Sign);
            Pro.Add(Item.hs_code);
            Pro.Add(Item.family);
            Pro.Add(Item.product_type);
            Pro.Add(Item.product_subtype);
            Pro.Add(Description);
            return Pro;
        }
        public class ParameterItem //: JsonObject<ParameterItem>
        {
            [Required]
            public string brand { get; set; } = null!;

            public string country_origin { get; set; }
            [Required]
            public string model_number { get; set; } = null!;
            [Required]
            public string model_name { get; set; } = null!;

            public string colour_family { get; set; }

            public string size { get; set; }

            public string size_unit { get; set; }

            public string base_material { get; set; }

            public string secondary_material { get; set; }

            public string material_finish { get; set; }

            public string care_instructions { get; set; }

            public string grade { get; set; }

            public string item_conditon { get; set; }

            public string shipping_destination { get; set; }

            public string product_length { get; set; }

            public string product_length_unit { get; set; }

            public string product_height { get; set; }

            public string product_height_unit { get; set; }

            public string product_widthdepth { get; set; }

            public string product_widthdepth_unit { get; set; }

            public string product_weight { get; set; }

            public string product_weight_unit { get; set; }

            public string shower_head_type { get; set; }

            public string dispenser_type { get; set; }

            public string spray_settings { get; set; }
            [Required]
            public string assembly_required { get; set; } = null!;

            public string bullet1 { get; set; }

            public string bullet2 { get; set; }

            public string bullet3 { get; set; }

            public string bullet4 { get; set; }

            public string bullet5 { get; set; }

            public string set_includes { get; set; }

            public string shipping_length { get; set; }

            public string shipping_length_unit { get; set; }

            public string shipping_height { get; set; }

            public string shipping_height_unit { get; set; }

            public string shipping_widthdepth { get; set; }

            public string shipping_widthdepth_unit { get; set; }

            public string shipping_weight { get; set; }

            public string shipping_weight_unit { get; set; }

            public string hs_code { get; set; }

            public string family { get; set; }
            [Required]
            public string product_type { get; set; } = null!;
            [Required]
            public string product_subtype { get; set; } = null!;
        }
    }
}
