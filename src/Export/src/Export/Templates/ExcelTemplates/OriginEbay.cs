﻿using ERP.Export.ExportCommon;
using ERP.Models.Export;
using ERP.Export.Templates;
using System.ComponentModel.DataAnnotations;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class OriginEbay : ExcelBase<OriginEbay, OriginEbay.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.OriginEbay;
    public override string Name => "Ebay原始表";

    public OriginEbay(IServiceProvider serviceProvider) : base(serviceProvider)
    {
    }

    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem data, ExcleRole role,
        string parameter, List<Unit> rate)
    {
        var item = ProductExportHelper.ToDeserialize<ParameterItem>(parameter);
        if (item.IsNull())
        {
            _Logger.LogInformation("{Name} GetData ParameterItem Is Null", Name);
            return null!;
        }

        var product = new List<List<string?>>();
        var sku = data.Sku;
        var price = await _Common.GetMainSalePrice(data.Price, rate);
        var cost = _Common.GetMainCostPrice(data.Price, rate);
        var mainPro = new ExcelProduct(data.Name, data.Description, data.BulletPoint, data.SearchTerms, "", "", sku, "",
            data.ProductImages, data.Quantity, price, cost, "", null, data.Source);

        if (data.Attribute.Count == 1 && data.Attribute.First().Value.Length == 1 && data.Attribute.First().Value[0] == "default")
        {
            
            var mainExcelPro = GetProduct(mainPro, item, role, new Dictionary<string, string[]>(), true, true, rate);
            product.Add(mainExcelPro);
        }
        else
        {
            var existsColors = new HashSet<string>();
            for (var index = 0; index < data.Products.Count; index++)
            {
                var productItem = data.Products[index];
                var excelPro = _Common.GetCommonData(productItem, data.Attribute, data.Source, sku, TemplateType);
                
                // 产品主图使用第一个变体的图片
                if (index == 0)
                {
                    mainPro.MainImage = excelPro.MainImage;
                    mainPro.Images = excelPro.Images;
                    var mainExcelPro = GetProduct(mainPro, item, role, data.Attribute, false, true, rate);
                    product.Add(mainExcelPro);
                }
                var attribute = data.Attribute
                    .Select((kv, i) => new { Key = kv.Key, Value = kv.Value[productItem.Values[i]] })
                    .ToDictionary(x => x.Key, x => new[] { x.Value });
                var pro = GetProduct(excelPro, item, role, attribute, false, false, rate, existsColors);
                product.Add(pro);
            }
        }
        var dic = new Dictionary<int, List<List<string?>>>();
        dic.Add(0, product);
        return dic;
    }

    private List<string?> GetProduct(ExcelProduct data, ParameterItem item, ExcleRole role,
        Dictionary<string, string[]> attribute, bool isSingle, bool isMain, List<Unit> rate, HashSet<string>? existsColors = null)
    {
        //描述
        string description = _Common.GetDescription(data.Description, string.Join(";", data.BulletPoint),
            data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, data.Images);
        var pro = new List<string?>
        {
            isMain ? item.Action : "",
            data.Sku
        };
        if (isMain)
        {
            pro.Add(item.Category);
            pro.Add(item.StoreCategory);
            pro.Add(data.Name);
            pro.Add(item.Subtitle);
            pro.Add("");
            pro.Add(string.Join('|', attribute.Select(kv => $"{kv.Key}={string.Join(';', kv.Value)}")));
            pro.Add(item.ConditionID);
            pro.Add(item.Brand);
            pro.Add("Does not apply");
            pro.Add(""); // type
            pro.Add(""); // model
            pro.Add(""); // California Prop 65 Warning
            pro.Add(""); // Country/Region of Manufacture
            pro.Add(""); // Unit Quantity
            pro.Add(""); // Unit Type
            pro.Add(""); // Expiration Date
            pro.Add(""); // Material
            pro.Add(""); // Number of Items
            pro.Add(""); // Color
            pro.Add(""); // Item Height
            pro.Add(""); // Features
            pro.Add(""); // Item Width
            pro.Add(""); // Item Length
            pro.Add(""); // Department
            pro.Add(""); // Item Weight
            pro.Add(""); // Compatible Brand
            pro.Add(""); // Compatible Model
            pro.Add(""); // Product Line
            pro.Add(""); // Style
            pro.Add(""); // Minimum Height
            pro.Add(""); // Maximum Height
            pro.Add(""); // Minimum Width
            pro.Add(""); // Maximum Width
            pro.Add(""); // Maximum Length
            pro.Add(""); // Minimum Length
            pro.Add(""); // Item Diameter
            pro.Add(""); // Room
            pro.Add(""); // Finish
            pro.Add(""); // Pattern
            pro.Add(""); // Mounting
            pro.Add(""); // Personalized
            pro.Add(""); // Manufacturer Warranty
            pro.Add(""); // Tabletop Thickness
            pro.Add(""); // Care Instructions
            pro.Add(""); // Suitable For
            pro.Add(""); // Theme
            pro.Add(""); // Personalize
            pro.Add(""); // Original/Reproduction
            pro.Add(""); // Shape
            pro.Add(""); // C:Era
            pro.Add(""); // C:Personalization Instructions
            pro.Add(""); // C:Upholstery Fabric
            pro.Add(string.Join('|', MapImages(data).Take(12))); // PicURL
            pro.Add(""); // GalleryType
            pro.Add(""); // VideoID
            pro.Add(description);
            pro.Add(item.Format); // Valid entry: Auction or FixedPrice
            pro.Add(item.Duration);
            pro.Add(isSingle ? "" : data.Price);
            pro.Add(""); // BuyItNowPrice
            pro.Add(isSingle ? "" : data.Quantity.ToString());
            pro.Add(item.ImmediatePayRequired);
            pro.Add(item.Location);
            pro.Add(item.ShippingType);
            pro.Add(item.ShippingService1Option);
            pro.Add(item.ShippingService1Cost);
            pro.Add(item.ShippingService2Option);
            pro.Add(item.ShippingService2Cost);
            pro.Add(item.DispatchTimeMax);
            pro.Add(item.PromotionalShippingDiscount);
            pro.Add(item.ShippingDiscountProfileID);
            pro.Add(item.ReturnsAcceptedOption);
            pro.Add(item.ReturnsWithinOption);
            pro.Add(item.RefundOption);
            pro.Add(item.ShippingCostPaidByOption);
            pro.Add(item.AdditionalDetails);
            pro.Add(item.TakeBackPolicyID);
            pro.Add(item.RegionalTakeBackPolicies);
            pro.Add(item.ProductCompliancePolicyID);
            pro.Add(item.RegionalProductCompliancePolicies);
            pro.Add(item.EcoParticipationFee);
        }
        else
        {
            ProductExportHelper.SetCommonCell(pro, "", 4);
            pro.Add("Variation");
            pro.Add(string.Join('|', attribute.Select(kv => $"{kv.Key}={kv.Value[0]}")));
            ProductExportHelper.SetCommonCell(pro, "", 46);
            if (!string.IsNullOrEmpty(data.Color))
            {
                pro.Add(existsColors?.Add(data.Color) is true ? $"{data.Color}={string.Join('|', MapImages(data).Take(12))}" : ""); // PicURL
            }
            else
            {
                pro.Add(string.Join('|', MapImages(data).Take(12))); // PicURL
            }
            ProductExportHelper.SetCommonCell(pro, "", 5);
            var price = ProductExportHelper.GetDataPrice(data.Price, rate);
            pro.Add(price);
            pro.Add(""); // BuyItNowPrice
            pro.Add(data.Quantity.ToString());
        }

        return pro;

        static IEnumerable<string> MapImages(ExcelProduct product)
        {
            var hash = new HashSet<string>();

            if (!string.IsNullOrEmpty(product.MainImage))
            {
                hash.Add(product.MainImage);
                yield return product.MainImage;
            }

            foreach (var image in product.Images.Where(image => !string.IsNullOrEmpty(image) && hash.Add(image)))
            {
                yield return image;
            }
        }
    }

    public class ParameterItem //: JsonObject<ParameterItem>
    {
        public string? TakeBackPolicyID { get; set; }
        public string? unit { get; set; }
        public string? language { get; set; }
        [Required] public string Action { get; set; } = null!;
        [Required] public string Category { get; set; } = null!;
        public string? Subtitle { get; set; }
        [Required] public string ConditionID { get; set; } = null!;
        [Required] public string Format { get; set; } = null!;
        [Required] public string Duration { get; set; } = null!;
        public string? ImmediatePayRequired { get; set; }
        [Required] public string Location { get; set; } = null!;
        public string? StoreCategory { get; set; }
        public string? ShippingDiscountProfileID { get; set; }
        public string? ShippingType { get; set; }
        public string? ShippingService1Option { get; set; }
        public string? ShippingService1Cost { get; set; }
        public string? ShippingService2Option { get; set; }
        public string? ShippingService2Cost { get; set; }
        public string? DispatchTimeMax { get; set; }
        public string? CustomLabel { get; set; }
        public string? ReturnsAcceptedOption { get; set; }
        public string? RefundOption { get; set; }
        public string? ReturnsWithinOption { get; set; }
        public string? ShippingCostPaidByOption { get; set; }
        public string? AdditionalDetails { get; set; }
        public string? Brand { get; set; }
        public string? PromotionalShippingDiscount { get; set; }
        public string? RegionalTakeBackPolicies { get; set; }
        public string? ProductCompliancePolicyID { get; set; }
        public string? RegionalProductCompliancePolicies { get; set; }
        public string? EcoParticipationFee { get; set; }
    }
}