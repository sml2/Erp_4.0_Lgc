﻿using ERP.Models.Export;
using ERP.Export.Templates;
using System.ComponentModel.DataAnnotations;
using ERP.Export.ExportCommon;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class Coupang : ExcelBase<Coupang, Coupang.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.Coupang;
    public override string Name => "Coupang表";

    public Coupang(IServiceProvider serviceProvider) : base(serviceProvider)
    {
    }

    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem data, ExcleRole role,
        string parameter, List<Unit> rate)
    {
        ParameterItem? item = ProductExportHelper.ToDeserialize<ParameterItem>(parameter);
        if (item is null)
        {
            _Logger.LogInformation("{Name} GetData ParameterItem Is Null", Name);
            return null!;
        }

        var products = new List<List<string?>>();

        foreach (var p in data.Products)
        {
            products.Add(FillProduct(data, item, p, role, rate));
        }

        return await Task.FromResult(new Dictionary<int, List<List<string?>>> { { 0, products } });
    }

    private List<string?> FillProduct(DataItem data, ParameterItem parameter, ProductsItem product, ExcleRole role,
        List<Unit> rate)
    {
        //关键字
        var keywords = _Common
            .GetKeywords(product.BulletPoint, product.SearchTerms, product.Description, role.KeywordMode)
            .Where((r) => r.IsNotWhiteSpace()).ToList();
        //卖点
        var bulletPoint = _Common.GetBulletPoint(product.BulletPoint, product.SearchTerms, product.Description,
            role.SketchMode);
        var result = new List<string?>();
        result.Add(parameter.Category);
        result.Add(product.Name);
        result.Add(parameter.SalesStartDate);
        result.Add(parameter.SalesEndDate);
        result.Add("새상품");
        result.Add("새상품");
        result.Add(parameter.brand);
        result.Add(parameter.manufacturer);
        var keyword = "";
        foreach (var item in keywords)
        {
            if (keyword.Length + item.Length < 20)
            {
                if (keyword.Length > 0) keyword += ",";
                keyword += item;
            }
        }

        result.Add(keyword);
        var attriValue = GetAttribute(product.Values, data.Attribute);

        attriValue.Take(5).ForEach((r) =>
        {
            result.Add(r.Key);
            result.Add(r.Value);
        });
        if (attriValue.Count <= 5)
        {
            ProductExportHelper.SetCommonCell(result, "", (6 - attriValue.Count) * 2);
        }
        else
        {
            var temp = attriValue.Skip(5).ToDictionary(r => r.Key, r => r.Value);
            result.Add(string.Join(",", temp.Keys));
            result.Add(string.Join(",", temp.Values));
        }

        var options = parameter.Option.Take(20).ToList();
        foreach (var item in options)
        {
            result.Add(item.OptionType);
            result.Add(item.OptionValue);
        }

        if (options.Count <= 20) ProductExportHelper.SetCommonCell(result, "", (20 - options.Count) * 2);

        //被10整除
        string price = ProductExportHelper.GetDataPrice(product.Price, rate);
        int tempPrice = Convert.ToInt32(Convert.ToDouble(price) / 10); //(TempPrice * 10).ToString()
        result.Add((tempPrice * 10).ToString());
        result.Add("");
        string cost = ProductExportHelper.GetDataPrice(product.Cost, rate);
        int tempCost = Convert.ToInt32(Convert.ToDouble(cost) / 10);
        result.Add((tempCost * 10).ToString());
        result.Add(product.Quantity.ToString());
        result.Add(parameter.DeliveryTime);
        result.Add(parameter.BuyCount);
        result.Add(parameter.BuyDay);
        result.Add(parameter.IsAdult);
        result.Add(parameter.Tax);
        result.Add(parameter.ParallelImport);
        result.Add(parameter.Overseas_Purchasing_Agency);
        result.Add(parameter.Company_Product_Code);
        result.Add(parameter.Model_Number);
        result.Add(parameter.Barcode);
        result.Add(parameter.AuthenticationReportType1);
        result.Add(parameter.AuthenticationReportValue1);
        result.Add(parameter.AuthenticationReportExtraValue1);
        result.Add(parameter.AuthenticationReportSecondExtraValue1);
        result.Add(parameter.AuthenticationReportType2);
        result.Add(parameter.AuthenticationReportValue2);
        result.Add(parameter.AuthenticationReportExtraValue2);
        result.Add(parameter.AuthenticationReportSecondExtraValue2);
        result.Add(parameter.AuthenticationReportType3);
        result.Add(parameter.AuthenticationReportValue3);
        result.Add(parameter.AuthenticationReportExtraValue3);
        result.Add(parameter.AuthenticationReportSecondExtraValue3);
        result.Add(parameter.AdditionalOrderMessage);
        result.Add(parameter.ProductNoticeInformation);
        ProductExportHelper.SetCommonImage(result, bulletPoint, 14);
        var mainImage = !string.IsNullOrWhiteSpace(product.MainImage) ? product.MainImage : product.Images?.FirstOrDefault();
        result.Add(mainImage);
        result.Add(mainImage);
        result.Add(string.Join(",", product.Images?.Take(9) ?? Enumerable.Empty<string>()));
        result.Add("");
        result.Add(""); // 重复图像
        result.Add(""); // 图像质量
        result.Add(parameter.RequiredDocumentValue1);
        result.Add(parameter.RequiredDocumentValue2);
        result.Add(parameter.RequiredDocumentValue3);
        result.Add(parameter.RequiredDocumentValue4);
        result.Add(parameter.RequiredDocumentValue5);
        result.Add(parameter.RequiredDocumentValue6);
        result.Add(parameter.RequiredDocumentValue7);
        return result;

        Dictionary<string, string> GetAttribute(List<int> values, Dictionary<string, string[]> attribute)
        {
            Dictionary<string, string> kv = new Dictionary<string, string>();
            try
            {
                for (int i = 0; i < values.Count - 1; i++)
                {
                    string key = attribute.Keys.ToList()[i];
                    string value = attribute.Values.ToList()[i][values[i]];
                    kv.Add(key, value);
                }
            }
            catch
            {
            }

            return kv;
        }
    }

    public class ParameterItem //: JsonObject<ParameterItem>
    {
        [Required] public string Category { get; set; } = null!;

        /// <summary>
        /// 日期格式必须yyyy-mm-dd
        /// </summary>
        [Required]
        public string SalesStartDate { get; set; } = null!;

        /// <summary>
        /// 日期格式必须yyyy-mm-dd
        /// </summary>
        [Required]
        public string SalesEndDate { get; set; } = null!;

        /// <summary>
        /// 如果没品牌填店铺名(UI提示)
        /// </summary>
        [Required]
        public string brand { get; set; } = null!;

        /// <summary>
        /// 如果没制造商填店铺名
        /// </summary>
        [Required]
        public string manufacturer { get; set; } = null!;

        #region 键值对

        public List<OptionItem> Option { get; set; } = new List<OptionItem>();

        public class OptionItem
        {
            public string? OptionType { get; set; }
            public string? OptionValue { get; set; }
        }

        #endregion

        /// <summary>
        /// 销售代理费
        /// </summary>
        public string? SalesAgencyFee { get; set; }

        /// <summary>
        /// 折扣价(这个字段需要改,以前我理解成折扣率了!)
        /// 注意:价格必须被整10结尾，不能大于销售价
        /// </summary>
        public string? Discount { get; set; }

        /// <summary>
        /// 1-99数字
        /// </summary>
        public string? DeliveryTime { get; set; }

        /// <summary>
        /// 每人最多可购买的数量
        /// </summary>
        public string? BuyCount { get; set; }

        /// <summary>
        /// 最大购买数量期（天）
        /// </summary>
        public string? BuyDay { get; set; }

        /// <summary>
        /// Bool :Y/N
        /// </summary>
        public string? IsAdult { get; set; }

        /// <summary>
        /// Y/N
        /// </summary>
        public string? Tax { get; set; }

        /// <summary>
        /// Y/N
        /// </summary>
        public string? ParallelImport { get; set; }

        /// <summary>
        /// Y/N
        /// </summary>
        public string? Overseas_Purchasing_Agency { get; set; }

        public string? Company_Product_Code { get; set; }
        public string? Model_Number { get; set; }
        public string? Barcode { get; set; }
        public string? AuthenticationReportType1 { get; set; }
        public string? AuthenticationReportValue1 { get; set; }
        public string? AuthenticationReportType2 { get; set; }
        public string? AuthenticationReportValue2 { get; set; }
        public string? AuthenticationReportType3 { get; set; }
        public string? AuthenticationReportValue3 { get; set; }
        public string? AdditionalOrderMessage { get; set; }

        /// <summary>
        /// 必填
        /// </summary>
        [Required]
        public string? ProductNoticeInformation { get; set; } = null!;

        //这些字段干掉-填的数据为卖点
        //public string? ProductNoticeInformationValue1 { get; set; }
        //public string? ProductNoticeInformationValue2 { get; set; }
        //public string? ProductNoticeInformationValue3 { get; set; }
        //public string? ProductNoticeInformationValue4 { get; set; }
        //public string? ProductNoticeInformationValue5 { get; set; }
        //public string? ProductNoticeInformationValue6 { get; set; }
        //public string? ProductNoticeInformationValue7 { get; set; }
        //public string? ProductNoticeInformationValue8 { get; set; }
        //public string? ProductNoticeInformationValue9 { get; set; }
        //public string? ProductNoticeInformationValue10 { get; set; }
        //public string? ProductNoticeInformationValue11 { get; set; }
        //public string? ProductNoticeInformationValue12 { get; set; }
        //public string? ProductNoticeInformationValue13 { get; set; }
        //public string? ProductNoticeInformationValue14 { get; set; }
        public string? DuplicateImage { get; set; }
        public string? ImageQuality { get; set; }

        /// <summary>
        /// 所需凭证
        /// </summary>
        public string? RequiredDocumentValue1 { get; set; }

        public string? RequiredDocumentValue2 { get; set; }
        public string? RequiredDocumentValue3 { get; set; }
        public string? RequiredDocumentValue4 { get; set; }
        public string? RequiredDocumentValue5 { get; set; }
        public string? RequiredDocumentValue6 { get; set; }
        public string? RequiredDocumentValue7 { get; set; }
        public string? AuthenticationReportExtraValue1 { get; set; }
        public string? AuthenticationReportSecondExtraValue1 { get; set; }
        public string? AuthenticationReportExtraValue2 { get; set; }
        public string? AuthenticationReportSecondExtraValue2 { get; set; }
        public string? AuthenticationReportExtraValue3 { get; set; }
        public string? AuthenticationReportSecondExtraValue3 { get; set; }
    }
}