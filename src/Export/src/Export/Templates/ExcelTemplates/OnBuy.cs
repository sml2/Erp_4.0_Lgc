﻿using ERP.Export.ExportCommon;
using ERP.Models.Export;
using ERP.Export.Templates;
using System.ComponentModel.DataAnnotations;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class OnBuy : ExcelBase<OnBuy, OnBuy.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.OnBuy;
    public override string Name => "Product_Create_Template";

    public OnBuy(IServiceProvider serviceProvider) : base(serviceProvider) { }
    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
    {
        List<List<string>> Product = new List<List<string>>();
        ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
        if (Item.IsNull())
        {
            _Logger.LogInformation($"{Name} GetData Method ParameterItem Is Null");
            return null;
        }
        //迁移服务器导出需要注意，需要重新探究SKU生成 归档的逻辑，以及不确定本平台为啥采用ProductID字段而非原生的SKU
        //当前会有重复，导致上传报错
        string Sku = Data.Sku;//ProductID   原客户端使用ProductID  现在迁移到服务器先使用原生Sku     可能以前这个平台就没有Sku从而使用ProductID
        ExcelProduct Pro = new ExcelProduct(Data.Name, Data.Description, Data.BulletPoint, Data.SearchTerms, "", "",
            Sku, Data.ShowImage, Data.Products.Where(p => p.Images is not null).SelectMany(p => p.Images!).ToList(),
            Data.Quantity, "", "", "", null, "");
        if (Data.Products.Count == 1 && Data.Products[0].Sku == Sku) //无变体
        {
        }
        else
        {
            var MainPro = await GetProduct(Pro, Item, role, Rate, Sku, true);
            if (MainPro.IsNotNull()) Product.Add(MainPro);
        }
        //子产品
        if (Data.Products.IsNotNull() && Data.Products.Count > 0)
        {
            foreach (var p in Data.Products)
            {
                ExcelProduct childPro = _Common.GetCommonData(p, Data.Attribute, Data.Source, Sku, TemplateType);
                var ChileExcelPro = await GetProduct(childPro, Item, role, Rate, Sku, false);
                if (ChileExcelPro.IsNotNull()) Product.Add(ChileExcelPro);
            }
        }
        var Dic = new Dictionary<int, List<List<string>>>();
        Dic.Add(0, Product);
        return Dic;
    }
    private async Task<List<string>> GetProduct(ExcelProduct Data, ParameterItem Item, ExcleRole role, List<Unit> Rate, string Sku, bool IsMain)
    {
        //描述
        string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, Data.Images);
        //卖点
        var BulletPoint = _Common.GetBulletPoint(Data.BulletPoint, Data.SearchTerms, Data.Description, role.SketchMode);
        List<string> Pro = new List<string>();
        Pro.Add(Data.Sku);
        Pro.Add(Data.Name);
        Pro.Add(Description);
        Pro.Add(Data.MainImage);
        Pro.Add(Item.Brand);
        Pro.Add(Item.Category);
        Pro.Add(Item.Condition);
        Pro.Add(Data.ProduceCode);
        string Price = ProductExportHelper.GetDataPrice(Data.Price, Rate);
        if (IsMain)
        {
            ProductExportHelper.SetCommonCell(Pro, "", 11);
        }
        else
        {
            Pro.Add(Price);//Standard Price
            Pro.Add(Data.Quantity.ToString());
            Pro.Add(Item.Handling_Time);
            Pro.Add(Item.Shipping_Template_Id);
            Pro.Add(Item.Shipping_Weight_KG);
            Pro.Add(Item.Warranty);
            Pro.Add(Item.Free_Returns);
            Pro.Add(Item.ASIN);
            Pro.Add(Item.MPN);
            Pro.Add(Item.RRP);
            Pro.Add(Data.Sku == Sku ? string.Empty : Sku);
        }
        
        if (IsMain)
        {
            ProductExportHelper.SetCommonCell(Pro, "", 4);
        }
        else
        {
            if (Data.Size.IsNotWhiteSpace() && Data.Color.IsNotWhiteSpace())
            {
                Pro.Add("Colour");
                Pro.Add(Data.Color);
                Pro.Add("Size");
                Pro.Add(Data.Size);
            }
            else if (Data.Size.IsNullOrEmpty() && Data.Color.IsNullOrEmpty())
            {
                ProductExportHelper.SetCommonCell(Pro, "", 4);
            }
            else
            {
                string VarName = "", VarValue = "";
                if (Data.Size.IsNotWhiteSpace())
                {
                    VarName = "Size";
                    VarValue = Data.Size;
                }
                else if (Data.Color.IsNotWhiteSpace())
                {
                    VarName = "Colour";
                    VarValue = Data.Color;
                }
                Pro.Add(VarName);
                Pro.Add(VarValue);
                ProductExportHelper.SetCommonCell(Pro, "", 2);
            }
        }

        if (IsMain)
        {
            ProductExportHelper.SetCommonCell(Pro, "", 7);
        }
        else
        {
            Pro.Add(Data.Size);
            Pro.Add(Data.Color);
            ProductExportHelper.SetCommonImage(Pro, BulletPoint, 5);
        }
        ProductExportHelper.SetCommonImage(Pro, Data.Images, 10);
        if (IsMain)
        {
            ProductExportHelper.SetCommonCell(Pro, "", 42);
            return Pro;
        }
        Pro.Add(Item.Data_One_Name);
        Pro.Add(Item.Data_One_Value);
        Pro.Add(Item.Data_Two_Name);
        Pro.Add(Item.Data_Two_Value);
        Pro.Add(Item.Data_Three_Name);
        Pro.Add(Item.Data_Three_Value);
        Pro.Add(Item.Data_Four_name);
        Pro.Add(Item.Data_Four_Value);
        Pro.Add(Item.Data_Five_Name);
        Pro.Add(Item.Data_Five_Value);
        Pro.Add(Item.Data_Six_Name);
        Pro.Add(Item.Data_Six_Value);
        Pro.Add(Item.Data_Seven_Name);
        Pro.Add(Item.Data_Seven_Value);
        Pro.Add(Item.Data_Eight_Name);
        Pro.Add(Item.Data_Eight_Value);
        Pro.Add(Item.Data_Nine_Name);
        Pro.Add(Item.Data_Nine_Value);
        Pro.Add(Item.Data_Ten_Name);
        Pro.Add(Item.Data_Ten_Value);
        Pro.Add(Item.Data_One_Group);
        Pro.Add(Item.Data_Two_Group);
        Pro.Add(Item.Data_Three_Group);
        Pro.Add(Item.Data_Four_Group);
        Pro.Add(Item.Data_Five_Group);
        Pro.Add(Item.Data_Six_Group);
        Pro.Add(Item.Data_Seven_Group);
        Pro.Add(Item.Data_Eight_Group);
        Pro.Add(Item.Data_Nine_Group);
        Pro.Add(Item.Data_Ten_Group);
        Pro.Add(Item.Video_One);
        Pro.Add(Item.Video_One_Name);
        Pro.Add(Item.Video_Two);
        Pro.Add(Item.Video_Two_Name);
        Pro.Add(Item.Attachment_One);
        Pro.Add(Item.Attachment_One_Name);
        Pro.Add(Item.Attachment_Two);
        Pro.Add(Item.Attachment_Two_Name);
        Pro.Add(Item.Condition_Note_One);
        Pro.Add(Item.Condition_Note_Two);
        Pro.Add(Item.Condition_Note_Three);
        Pro.Add(Item.Condition_Note_Four);
        Pro.Add(Item.Condition_Note_Five);
        return Pro;
    }
    public class ParameterItem //: JsonObject<ParameterItem>
    {
        [Required]
        public string Brand { get; set; } = null!;
        [Required]
        public string Category { get; set; } = null!;
        [Required]
        public string Condition { get; set; } = null!;
        public string Handling_Time { get; set; }
        public string Shipping_Template_Id { get; set; }
        public string Shipping_Weight_KG { get; set; }
        public string Warranty { get; set; }
        public string Free_Returns { get; set; }
        public string ASIN { get; set; }
        public string MPN { get; set; }
        public string RRP { get; set; }
        //public string Parent_Group { get; set; }
        /// <summary>
        /// 可选参
        /// </summary>
        public string Data_One_Name { get; set; }
        public string Data_One_Value { get; set; }
        public string Data_Two_Name { get; set; }
        public string Data_Two_Value { get; set; }
        public string Data_Three_Name { get; set; }
        public string Data_Three_Value { get; set; }
        public string Data_Four_name { get; set; }
        public string Data_Four_Value { get; set; }
        public string Data_Five_Name { get; set; }
        public string Data_Five_Value { get; set; }
        public string Data_Six_Name { get; set; }
        public string Data_Six_Value { get; set; }
        public string Data_Seven_Name { get; set; }
        public string Data_Seven_Value { get; set; }
        public string Data_Eight_Name { get; set; }
        public string Data_Eight_Value { get; set; }
        public string Data_Nine_Name { get; set; }
        public string Data_Nine_Value { get; set; }
        public string Data_Ten_Name { get; set; }
        public string Data_Ten_Value { get; set; }
        public string Data_One_Group { get; set; }
        public string Data_Two_Group { get; set; }
        public string Data_Three_Group { get; set; }
        public string Data_Four_Group { get; set; }
        public string Data_Five_Group { get; set; }
        public string Data_Six_Group { get; set; }
        public string Data_Seven_Group { get; set; }
        public string Data_Eight_Group { get; set; }
        public string Data_Nine_Group { get; set; }
        public string Data_Ten_Group { get; set; }
        public string Video_One { get; set; }
        public string Video_One_Name { get; set; }
        public string Video_Two { get; set; }
        public string Video_Two_Name { get; set; }
        public string Attachment_One { get; set; }
        public string Attachment_One_Name { get; set; }
        public string Attachment_Two { get; set; }
        public string Attachment_Two_Name { get; set; }
        public string Condition_Note_One { get; set; }
        public string Condition_Note_Two { get; set; }
        public string Condition_Note_Three { get; set; }
        public string Condition_Note_Four { get; set; }
        public string Condition_Note_Five { get; set; }
    }
}
