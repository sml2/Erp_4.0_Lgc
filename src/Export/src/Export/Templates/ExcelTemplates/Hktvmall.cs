﻿using ERP.Export.ExportCommon;
using ERP.Export.Templates;
using ERP.Models.Export;
using System.ComponentModel.DataAnnotations;
using Common.Html;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class Hktvmall : ExcelBase<Hktvmall, Hktvmall.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.Hktvmall;
    public override string Name => "Hktvmall导出模板";


    public Hktvmall(IServiceProvider serviceProvider) : base(serviceProvider)
    {
    }

    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem data, ExcleRole role,
        string parameter, List<Unit> rate)
    {
        var product = new List<List<string?>>();
        var item = ProductExportHelper.ToDeserialize<ParameterItem>(parameter);
        if (item is null)
        {
            _Logger.LogInformation("{Name}ParameterItem is Null", Name);
            return null!;
        }

        var sku = data.Sku;
        
        //子产品
        foreach (var p in data.Products)
        {
            var childPro = _Common.GetCommonData(p, data.Attribute, data.Source, sku, TemplateType);
            
            var chileExcelPro = GetProduct((p as HktvmallProductItem)!, childPro, item, role, rate, p.Sku);
            if (chileExcelPro.IsNotNull()) product.Add(chileExcelPro);
        }

        var dic = new Dictionary<int, List<List<string?>>>();
        dic.Add(0, product);
        return dic;
    }

    private List<string?> GetProduct(HktvmallProductItem productItem,ExcelProduct data, ParameterItem item, ExcleRole role, List<Unit> rate, string sku)
    {
        //描述
        var description = _Common.GetDescription(data.Description, string.Join(";", data.BulletPoint),
            data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, data.Images);
        var descriptionEn = _Common.GetDescription(productItem.DescriptionEn, string.Join(";", data.BulletPoint),
            data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, data.Images);
        description = Sy.Text.Html.RemoveTag(description, new[] { "img" }).TrimEnd(Environment.NewLine.ToArray());
        descriptionEn = Sy.Text.Html.RemoveTag(descriptionEn, new[] { "img" }).TrimEnd(Environment.NewLine.ToArray());
        //卖点
        var bulletPoint = _Common.GetBulletPoint(data.BulletPoint, data.SearchTerms, data.Description, role.SketchMode);
        var bulletPointEn = _Common.GetBulletPoint(productItem.BulletPointEn, data.SearchTerms, productItem.DescriptionEn, role.SketchMode);
        //关键字
        var keyword = _Common.GetKeywords(data.BulletPoint, data.SearchTerms, data.Description, role.KeywordMode);
        //主产品
        var pro = new List<string?>();
        pro.Add(sku);
        pro.Add(data.Sku);
        pro.Add(item.ProductTypeCode);
        pro.Add(item.PrimaryCategoryCode);
        pro.Add(item.Brand);
        pro.Add(item.ProductReadyMethod);
        ProductExportHelper.SetCommonCell(pro, "", 2);
        pro.Add(item.Warehouse);
        pro.Add(""); // Term Name 留空
        pro.Add(data.Sku == sku ? "Yes" : "No");
        pro.Add(productItem.NameEn); // 英文
        pro.Add(data.Name);
        ProductExportHelper.SetCommonCell(pro, "", 2);
        pro.Add(descriptionEn.Length > 500 ? descriptionEn[..500] : descriptionEn); // 英文
        pro.Add(description.Length > 500 ? description[..500] : description);
        ProductExportHelper.SetCommonCell(pro, "", 2);
        pro.Add(string.Join("<br>", bulletPointEn.Where(b => !string.IsNullOrWhiteSpace(b)))); // 英文
        pro.Add(string.Join("<br>", bulletPoint.Where(b => !string.IsNullOrWhiteSpace(b))));
        pro.Add(data.MainImage);
        pro.Add(""); // main video
        pro.Add(string.Join(",", data.Images));
        pro.Add(string.Join(",", data.Images));
        // pro.Add(string.Join(",", data.DescriptionImages)); // 描述图片
        pro.Add(data.MainImage);
        ProductExportHelper.SetCommonCell(pro, "", 3 * 5); // video link
        pro.Add(item.ManufacturedCountry); 
        pro.Add("");  // Colour Families
        pro.Add("");  // Color (Eng)
        pro.Add("");
        pro.Add("");  // Size System
        pro.Add("");  // Size
        pro.Add("HKD : HKD");
        pro.Add("");
        
        var price = ProductExportHelper.GetDataPrice(data.Price, rate);
        pro.Add(price);
        pro.Add(""); // selling price
        pro.Add("");
        pro.Add("");
        pro.Add(""); // user max
        pro.Add("RED");
        pro.Add("");
        pro.Add("");
        pro.Add(""); // Packing Spec (Eng)
        pro.Add("");
        pro.Add(""); // Packing Height
        pro.Add("");
        pro.Add("");
        pro.Add("");
        pro.Add(""); // Weight
        pro.Add("");
        pro.Add("F : 小心輕放");
        
        ProductExportHelper.SetCommonCell(pro, "", 40);
        
        pro.Add("0");
        pro.Add("14 : 14");
        pro.Add("MS : Mon-Sat");
        pro.Add("AM/PM");
        
        ProductExportHelper.SetCommonCell(pro, "", 7);

        return pro;
    }

    public class ParameterItem : CommonParameter
    {
        public string unit { get; set; } = "HKD";
        [Required] public string ProductTypeCode { get; set; } = null!;
        [Required] public string PrimaryCategoryCode { get; set; } = null!;
        [Required] public string Brand { get; set; } = "#N/A";

        public string ProductReadyMethod { get; set; } = "M : Merchant Delivery";
        public string? Warehouse { get; set; } = "B0387001-9";
        public string ManufacturedCountry { get; set; } = "N/A";
    }
}