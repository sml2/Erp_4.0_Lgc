﻿using ERP.Export.ExportCommon;
using ERP.Models.Export;
using ERP.Export.V4_Export.Interfaces;
using Newtonsoft.Json;
using System.Text;
using common = ERP.Export.ExportCommon.ProductExportHelper;
using ERP.Export.Templates;

namespace ERP.Export.V4_Export.ExcelTemplates
{
    public class LazadaSmallAppliances : ExcelBase<LazadaSmallAppliances>
    {
        public override TemplateType TemplateType => TemplateType.Lazada_Small_Appliances;

        public override string Name => "Lazada Small Appliances表";

        public LazadaSmallAppliances(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }
        public override async Task<bool> Verify(string Parameter)
        {
            ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
            if (Item.IsNull())
            {
                _Logger.LogInformation($"{Name} Verify Method ParameterItem Is Null");
                return false;
            }
            if (Item.primary_category.IsNullOrEmpty())
            {
                _Logger.LogInformation($"{Name} Verify Method ParameterItem Category is Empty!");
                return false;
            }
            return true;
        }

        public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
        {
            List<List<string>> Product = new List<List<string>>();
            ParameterItem Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
            if (Item.IsNull())
            {
                _Logger.LogInformation($"{Name} Verify Method ParameterItem Is Null");
                return null;
            }
            string Sku = Data.Sku;
            if (Data.Products.IsNull() || Data.Products.Count == 0)
            {
                string Price = "0";
                string Cost = "0";
                if (Data.Price is not null && Data.Price.Sale.Max.Value > 0) Price = Data.Price.Sale.Max.Value.ToString();//sale
                if (Data.Price is not null && Data.Price.Cost.Max.Value > 0) Price = Data.Price.Sale.Max.Value.ToString();//cost
                ExcelProduct MainPro = new ExcelProduct(Data.Name, Data.Description, Data.BulletPoint, Data.SearchTerms, "", "", Sku, "", null, Data.Quantity, Price, Cost, "", null, Data.Source);
                var MainExcelPro = GetProduct(MainPro, Item, Rate, role);
                if (MainExcelPro.IsNotNull()) Product.Add(MainExcelPro);
            }
            else
            {
                //子产品
                foreach (var p in Data.Products)
                {
                    ExcelProduct ChildPro = _Common.GetCommonData(p, Data.Attribute, Data.Source, Sku);
                    var ChileExcelPro = GetProduct(ChildPro, Item, Rate, role);
                    if (ChileExcelPro.IsNotNull()) Product.Add(ChileExcelPro);
                }
            }
            var Dic = new Dictionary<int, List<List<string>>>();
            Dic.Add(0, Product);
            return Dic;
        }
        private List<string> GetProduct(ExcelProduct Data, ParameterItem Item, List<Unit> Rate, ExcleRole role)
        {
            List<string> Pro = new List<string>();
            Pro.Add(Item.primary_category);
            Pro.Add(Item.model);
            Pro.Add(Item.brand);
            Pro.Add(Data.Color);
            Pro.Add("");
            Pro.Add(Data.Size);
            _Common.GetLazadaCommonCell(Pro, 59, Data, Item, role, Rate);
            Pro.Add("");
            return Pro;
        }
        public class ParameterItem : IParameterItem //: JsonObject<ParameterItem>, IParameterItem
        {
            public string unit { get; set; }

            public string primary_category { get; set; }

            public string model { get; set; }

            public string brand { get; set; }

            public string color_family { get; set; }

            public string package_content { get; set; }

            public string package_length { get; set; }

            public string package_width { get; set; }

            public string package_height { get; set; }

            public string package_weight { get; set; }

            public string taxes { get; set; }

        }
    }
}
