﻿using ERP.Export.ExportCommon;
using ERP.Models.Export;
using ERP.Export.Templates;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class ZYingThress : ExcelBase<ZYingThress, ZYingThress.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.ZYingThree;
    public override string Name => "智赢Excel(多变体)";

    public ZYingThress(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }

    public override async Task<Dictionary<string, List<List<string>>>> GetHead(string Parameter)
    {
        //智赢的表多一列
        var Temp = await _Common.GetHead(TemplateType.ZYingExcel);
        Temp.ForEach((r) => r.Value[0].Insert(5, "其他属性"));
        return Temp;
    }
    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
    {
        ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
        if (Item is null)
        {
            //Log(I18N.Instance.Export_Log3);
            return null;
        }
        List<List<string>> Product = new List<List<string>>();
        string Sku = Data.Sku;
        if (Data.Products.IsNull() || Data.Products.Count == 0 || Data.Products.Count == 1 && Data.Products[0].Sku == Sku)
        {
            //无变体
            string Price = await _Common.GetMainSalePrice(Data.Price, Rate);
            string Cost = _Common.GetMainCostPrice(Data.Price, Rate);
            List<string> Images = null;
            if (Data.Products.IsNotNull() && Data.Products.Count > 0)
            {
                Images = Data.Products[0].Images;
            }
            int Quantity = Data.Quantity;
            if (Data.Products.IsNotNull() && Data.Products.Count > 0) Quantity = Data.Products[0].Quantity;
            ExcelProduct MainPro = new ExcelProduct(Data.Name, Data.Description, Data.BulletPoint, Data.SearchTerms, "", "", Sku, Data.ShowImage, Images, Quantity, Price, Cost, "", null, Data.Source);
            MainPro.OtherAttribute = "";
            var MainExcelPro = await GetProduct(MainPro, Item, Rate, role, Sku, true);
            if (MainExcelPro.IsNotNull()) Product.Add(MainExcelPro);
        }
        else
        {
            foreach (var p in Data.Products)
            {
                ExcelProduct ChildPro = _Common.GetCommonData(p, Data.Attribute, Data.Source, Sku, TemplateType);
                var ChileExcelPro = await GetProduct(ChildPro, Item, Rate, role, Sku, false);
                if (ChileExcelPro.IsNotNull()) Product.Add(ChileExcelPro);
            }
        }
        var Dic = new Dictionary<int, List<List<string>>>();
        Dic.Add(0, Product);
        return Dic;
    }
    private async Task<List<string>> GetProduct(ExcelProduct Data, ParameterItem Item, List<Unit> Rate, ExcleRole role, string Sku, bool IsMain)
    {
        //描述
        string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, Data.Images);
        //卖点
        var BulletPoint = _Common.GetBulletPoint(Data.BulletPoint, Data.SearchTerms, Data.Description, role.SketchMode);
        //关键字
        var Keyword = _Common.GetKeywords(Data.BulletPoint, Data.SearchTerms, Data.Description, role.KeywordMode);
        List<string> Pro = new List<string>();
        Pro.Add(Sku);
        Pro.Add(Data.Sku);
        Pro.Add(Item.is_Adult.ToString());
        Pro.Add(Data.Color);
        Pro.Add(Data.Size);
        Pro.Add(Item.BrandName);
        Pro.Add(Item.Category);
        Pro.Add(Data.Name);
        Pro.Add(Data.Name);
        Pro.Add(Data.Quantity.ToString());
        Pro.Add(Rate[0].Sign);
        string Cost = Data.Cost;
        if (!IsMain) Cost = ProductExportHelper.GetDataPrice(Data.Cost, Rate);
        string Price = Data.Price;
        if (!IsMain) Price = ProductExportHelper.GetDataPrice(Data.Price, Rate);
        Pro.Add(Cost);
        Pro.Add(Item.Fee);
        Pro.Add(Item.TemplateID);
        Pro.Add(Item.HsCode);
        Pro.Add(Item.DeclaredValue);
        Pro.Add(Price);
        Pro.Add(Item.Weight);
        Pro.Add(Item.Package_Dimensions);
        Pro.Add(Item.ForPeople);
        Pro.Add(Item.Material);
        Pro.Add(Item.PackageMaterial);
        Pro.Add(Item.Metal);
        Pro.Add(Item.Jewelry);
        Pro.Add("");
        Pro.Add(Data.Name);
        Pro.Add(string.Join(" ", Keyword));
        BulletPoint.ForEach((r) => { Pro.Add(r); });
        Pro.Add(Description);
        List<string> Images = new List<string>();
        if (Data.MainImage.IsNotWhiteSpace()) Images.Add(Data.MainImage);
        if (Data.Images.IsNotNull()) Images.AddRange(Data.Images);
        Pro.Add(string.Join(" | ", Images));
        Pro.Add(Data.Source);
        Pro.Add(Item.SecurityLevel);
        Pro.Add(Item.ProductLevel);
        return Pro;
    }
    public class ParameterItem //: JsonObject<ParameterItem>
    {
        /// <summary>
        /// 成人
        /// </summary>
        public bool is_Adult { get; set; }
        /// <summary>
        /// 分类
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// 品牌
        /// </summary>
        public string BrandName { get; set; }
        /// <summary>
        /// 运费
        /// </summary>
        public string Fee { get; set; }
        /// <summary>
        /// 挂号模板
        /// </summary>
        public string TemplateID { get; set; }
        /// <summary>
        /// 海关编码
        /// </summary>
        public string HsCode { get; set; }
        /// <summary>
        /// 申报价(美元)
        /// </summary>
        public string DeclaredValue { get; set; }
        /// <summary>
        /// 毛重(克)
        /// </summary>
        public string Weight { get; set; }
        /// <summary>
        /// 包装尺寸
        /// </summary>
        public string Package_Dimensions { get; set; }
        /// <summary>
        /// 适用人群
        /// </summary>
        public string ForPeople { get; set; }
        /// <summary>
        /// 材料
        /// </summary>
        public string Material { get; set; }
        /// <summary>
        /// 包装材料
        /// </summary>
        public string PackageMaterial { get; set; }
        /// <summary>
        /// 金属
        /// </summary>
        public string Metal { get; set; }
        /// <summary>
        /// 珠宝
        /// </summary>
        public string Jewelry { get; set; }
        /// <summary>
        /// 安全等级
        /// </summary>
        public string SecurityLevel { get; set; }
        /// <summary>
        /// 产品级别
        /// </summary>
        public string ProductLevel { get; set; }
    }
}
