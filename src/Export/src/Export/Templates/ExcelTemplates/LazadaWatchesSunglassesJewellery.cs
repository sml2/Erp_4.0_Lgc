﻿using ERP.Export.ExportCommon;
using ERP.Export.Templates;
using ERP.Models.Export;

namespace ERP.Export.V4_Export.ExcelTemplates
{
    public class LazadaWatchesSunglassesJewellery : ExcelBase<LazadaWatchesSunglassesJewellery>
    {
        public override TemplateType TemplateType => TemplateType.Lazada_Watches_Sunglasses_Jewellery;

        public override string Name => "Lazada Watches Sunglasses Jewellery表";

        public LazadaWatchesSunglassesJewellery(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }
        public override async Task<bool> Verify(string Parameter)
        {
            ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
            if (Item.IsNull())
            {
                _Logger.LogInformation($"{Name} Verify Method ParameterItem Is Null");
                return false;
            }
            if (Item.primary_category.IsNullOrEmpty())
            {
                _Logger.LogInformation($"{Name} Verify Method ParameterItem In primary_category Null");
                return false;
            }
            await Task.CompletedTask;
            return true;
        }

        public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
        {
            List<List<string>> Product = new List<List<string>>();
            ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
            if (Item.IsNull())
            {
                _Logger.LogInformation($"{Name} GetData Method ParameterItem Is Null");
                return null;
            }
            string Sku = Data.Sku;
            if (Data.Products.IsNull() || Data.Products.Count == 0)
            {
                string Price = "0";
                string Cost = "0";
                if (Data.Price is not null && Data.Price.Sale.Max.Value > 0) Price = Data.Price.Sale.Max.Value.ToString();//sale
                if (Data.Price is not null && Data.Price.Cost.Max.Value > 0) Price = Data.Price.Sale.Max.Value.ToString();//cost
                ExcelProduct MainPro = new ExcelProduct(Data.Name, Data.Description, Data.BulletPoint, Data.SearchTerms, "", "", Sku, "", null, Data.Quantity, Price, Cost, "", null, Data.Source);
                var MainExcelPro = GetProduct(MainPro, Item, role, Rate);
                if (MainExcelPro.IsNotNull()) Product.Add(MainExcelPro);
            }
            else
            {
                //子产品
                foreach (var p in Data.Products)
                {
                    ExcelProduct ChildPro = _Common.GetCommonData(p, Data.Attribute, Data.Source, Sku);
                    var ChileExcelPro = GetProduct(ChildPro, Item, role, Rate);
                    if (ChileExcelPro.IsNotNull()) Product.Add(ChileExcelPro);
                }
            }
            var Dic = new Dictionary<int, List<List<string>>>();
            Dic.Add(0, Product);
            return Dic;
        }
        private List<string> GetProduct(ExcelProduct Data, ParameterItem Item, ExcleRole role, List<Unit> Rate)
        {
            List<string> Pro = new List<string>();
            Pro.Add(Item.primary_category);
            Pro.Add(Item.model);
            Pro.Add(Item.brand);
            Pro.Add(Data.Color);
            _Common.GetLazadaCommonCell(Pro, 32, Data, Item, role, Rate);
            Pro.Add(Item.eyewear_size);
            Pro.Add(Item.frame_color);
            Pro.Add(Item.lens_color);
            Pro.Add(Item.power_lenses);
            Pro.Add(Item.Lens_Axis);
            Pro.Add("");
            Pro.Add(Item.chain_size);
            Pro.Add(Item.Lens_Index);
            Pro.Add(Item.weight_of_metal);
            Pro.Add(Item.ring_size);
            Pro.Add(Item.earring_size);
            Pro.Add(Item.bracelet_size);
            Pro.Add(Item.watch_strap_color);
            return Pro;
        }

        public class ParameterItem : IParameterItem //: JsonObject<ParameterItem>, IParameterItem
        {
            public string primary_category { get; set; }

            public string model { get; set; }

            public string brand { get; set; }

            public string color_family { get; set; }

            public string package_content { get; set; }

            public string package_length { get; set; }

            public string package_width { get; set; }

            public string package_height { get; set; }

            public string package_weight { get; set; }

            public string taxes { get; set; }

            public string eyewear_size { get; set; }

            public string frame_color { get; set; }

            public string lens_color { get; set; }

            public string power_lenses { get; set; }

            public string Lens_Axis { get; set; }

            public string chain_size { get; set; }

            public string Lens_Index { get; set; }

            public string weight_of_metal { get; set; }

            public string ring_size { get; set; }

            public string earring_size { get; set; }

            public string bracelet_size { get; set; }

            public string watch_strap_color { get; set; }
        }
    }
}
