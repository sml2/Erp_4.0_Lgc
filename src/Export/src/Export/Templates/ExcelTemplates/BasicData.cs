﻿using ERP.Models.Export;
using ERP.Export.Templates;
using System.ComponentModel.DataAnnotations;
using ERP.Export.ExportCommon;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class BasicData : ExcelBase<BasicData>
{
    public override TemplateType TemplateType => TemplateType.BasicData;
    public override string Name => "备份基础数据";

    public BasicData(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }
    public override bool TryValidate(string parameter, out List<ValidationResult> results)
    {
        results = new List<ValidationResult>();
        return true;
    }
    
    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
    {
        var Product = new List<List<string?>>();
        string Sku = Data.Sku;
        var MainPro = new List<string?>();
        MainPro.Add(Data.Name);
        MainPro.Add(Sku);
        string Price = await _Common.GetMainSalePrice(Data.Price, Rate);
        MainPro.Add(Price);
        MainPro.Add(Data.Source);
        MainPro.Add(Data.ProductID);
        Product.Add(MainPro);
        //存在变体的情况下
        if (!(Data.Products.Count == 0 && Data.Products[0].Sku == Data.Sku))
        {
            foreach (var Pro in Data.Products)
            {
                var ChildPro = new List<string?>();
                ChildPro.Add(Pro.Name);
                ChildPro.Add(Sku);
                string ProPrice = ProductExportHelper.GetDataPrice(Pro.Price, Rate);
                ChildPro.Add(ProPrice);
                ChildPro.Add(Data.Source);
                ChildPro.Add(Pro.SID);
                Product.Add(ChildPro);
            }
        }
        var Dic = new Dictionary<int, List<List<string?>>>();
        Dic.Add(0, Product);
        return Dic;
    }
}
