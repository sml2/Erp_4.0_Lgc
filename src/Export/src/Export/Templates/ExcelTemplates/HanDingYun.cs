﻿using ERP.Models.Export;
using ERP.Export.Templates;
using System.ComponentModel.DataAnnotations;
using ERP.Export.ExportCommon;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class HanDingYun : ExcelBase<HanDingYun>
{
    public override TemplateType TemplateType => TemplateType.HanDingYun;
    public override string Name => "汉顶云";
    public HanDingYun(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }
    public override bool TryValidate(string parameter, out List<ValidationResult> results)
    {
        results = new List<ValidationResult>();
        return true;
    }

    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
    {
        Dictionary<int, List<List<string?>>>? Dic = new Dictionary<int, List<List<string?>>>();
        //Sheet2
        var Product1 = new List<List<string?>>();
        string Sku = Data.Sku;
        //描述
        string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, null);
        var Sheet1 = new List<string?>();
        Sheet1.Add(Sku);
        Sheet1.Add("");
        Sheet1.Add(Data.Name);
        Sheet1.Add(Data.Name);
        Sheet1.Add(Data.ShowImage);
        List<string> AdditionImg = new List<string>();
        Data.Products.ForEach((r) =>
        {
            if (r.MainImage.IsNotWhiteSpace()) AdditionImg.Add(r.MainImage);
        });
        Sheet1.Add(string.Join("|", AdditionImg));
        string Price = await _Common.GetMainSalePrice(Data.Price, Rate);
        Sheet1.Add(Price);
        Sheet1.Add("");
        Sheet1.Add(Data.Quantity.ToString());
        Sheet1.Add("");//weight
        Sheet1.Add("1");
        Sheet1.Add("Accessories");
        ExportCommon.ProductExportHelper.SetCommonCell(Sheet1, "", 2);
        Sheet1.Add(Description);
        Sheet1.Add(Description);
        Product1.Add(Sheet1);
        Dic.Add(0, Product1);
        //Sheet2
        var Product2 = new List<List<string?>>();
        var Sheet2 = new List<string?>();
        var Color = _Common.GetAttributes(Data.Attribute, _Common.Colors);
        if (Color.IsNotNull() && Color.Count > 0)
        {
            Sheet2.Add(Sku);
            Sheet2.Add(string.Join("||", Color));
        }
        Product2.Add(Sheet2);
        Dic.Add(1, Product2);
        //Sheet3
        var Product3 = new List<List<string?>>();
        var Sheet3 = new List<string?>();
        var Size = _Common.GetAttributes(Data.Attribute, ProductExportHelper.Sizes);
        if (Size.IsNotNull() && Size.Count > 0)
        {
            Sheet3.Add(Sku);
            Sheet3.Add(string.Join("||", Size));
        }
        Product3.Add(Sheet3);
        Dic.Add(2, Product3);
        return Dic;
    }
}
