﻿using ERP.Models.Export;
using ERP.Export.Templates;
using System.ComponentModel.DataAnnotations;
using ERP.Export.ExportCommon;

namespace ERP.Export.V4_Export.ExcelTemplates;

public class QuTian : ExcelBase<QuTian, QuTian.ParameterItem>
{
    public override TemplateType TemplateType => TemplateType.QuTian;
    public override string Name => "QuTian";

    public QuTian(IServiceProvider _ServiceProvider) : base(_ServiceProvider) { }
    
    public override async Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string Parameter, List<Unit> Rate)
    {
        List<List<string>> Product = new List<List<string>>();
        ParameterItem? Item = ProductExportHelper.ToDeserialize<ParameterItem>(Parameter);
        if (Item.IsNull())
        {
            _Logger.LogInformation($"{Name} GetData Method ParameterItem Is Null");
            return null;
        }
        var Sku = Data.Sku;
        var Attributes = Data.Attribute;
        List<string> AllImages = new List<string>();
        if (Data.Products.IsNotNull() && Data.Products.Count > 0)
        {
            Data.Products.ForEach((r) =>
            {
                if (!AllImages.Contains(r.MainImage)) AllImages.Add(r.MainImage);
                if (r.Images.IsNotNull() && r.Images.Count > 0)
                {
                    r.Images.ForEach((Img) =>
                    {
                        if (!AllImages.Contains(Img)) AllImages.Add(Img);
                    });
                }
            });
        }
        //描述
        string Description = _Common.GetDescription(Data.Description, string.Join(";", Data.BulletPoint), Data.SearchTerms, role.DescriptionMode, role.DescriptionAppendImage, AllImages);
        //卖点
        var BulletPoint = _Common.GetBulletPoint(Data.BulletPoint, Data.SearchTerms, Data.Description, role.SketchMode);
        //关键字
        var Keyword = _Common.GetKeywords(Data.BulletPoint, Data.SearchTerms, Data.Description, role.KeywordMode);
        //主产品
        List<string> Pro = new List<string>();
        Pro.Add("");//Item.item_number
        Pro.Add(Item.seller_unique_item_id);
        Pro.Add(Item.category_number);
        Pro.Add(Item.brand_number);
        Pro.Add(Data.Name);
        Pro.Add(Item.item_promotion_name);
        Pro.Add(Item.item_status);
        Pro.Add(Item.end_date);
        string Price = await _Common.GetMainSalePrice(Data.Price, Rate);
        Pro.Add(Price);
        string Cost = _Common.GetMainCostPrice(Data.Price, Rate).ToString();
        Pro.Add(Cost);
        var Quantity = 0;
        if (Data.Products.IsNotNull() && Data.Products.Count > 0) Quantity = Data.Products.First().Quantity;
        Pro.Add(Quantity.ToString());
        //  - 纵行区分字符(||*) / 横行区分字符($$)
        //选项名称1||*详细名称1||*价格1||*选项代码1$$选项名1||*选项详情名2||*价格2||*选项代码2
        //库存名1||*库存详情名称1||*价格1||*数量1||*库存选项代码1$$库存名1||*库存详情名2||*价格2||*数量2||*库存选项代码2
        //颜色||*黑色||*0||*100||*testSellerCode-CH3200-black$$颜色||*红色||*0||*100||*testSellerCode-CH3200-red
        string InventoryInfo = "";
        List<string> InventoryCoverImage = new List<string>();
        List<string> OneAttrLst = new List<string>();
        string SpliterJPG = ".jpg";
        Func<string, string> GetImage = (OrignalImage) =>
        {
            if (OrignalImage.Contains(SpliterJPG)) OrignalImage = OrignalImage.Before(SpliterJPG);
            return $"{OrignalImage}{SpliterJPG}";
        };
        List<string> Images = new List<string>();
        int Code = 1;
        Data.Products.ForEach((v) =>
        {
            string TempInventoryCoverImage = "";
            if (InventoryInfo.Length > 0) InventoryInfo += "$$";
            //图片
            if (v.MainImage.IsNotWhiteSpace()) Images.Add(v.MainImage);
            v.Images.ForEach((img) =>
            {
                if (!Images.Contains(img)) Images.Add(img);
            });
            string VPrice = ProductExportHelper.GetDataPrice(v.Price, Rate);
            //属性
            int index = 0;
            v.Values.ForEach((r) =>
            {
                if (index > 0) InventoryInfo += "||*";
                string AttName = Data.Attribute.Keys.ToList()[index];
                string AttValue = Data.Attribute.Values.ToList()[index][r];
                InventoryInfo += $"{AttName}||*{AttValue}";
                if (TempInventoryCoverImage.Length == 0) TempInventoryCoverImage += $"{AttName}||*{AttValue}";
                index++;
            });
            InventoryInfo += $"||*0||*{v.Quantity}||*code{Code}";
            if (!OneAttrLst.Contains(TempInventoryCoverImage))
            {
                OneAttrLst.Add(TempInventoryCoverImage);
                TempInventoryCoverImage += $"||*{v.MainImage}";
                InventoryCoverImage.Add(TempInventoryCoverImage);
            }
            Code += 1;
        });
        Pro.Add(InventoryInfo);
        Pro.Add(Item.additional_option_info);
        Pro.Add(Item.additional_option_text);
        Pro.Add(Data.ShowImage);

        Pro.Add(string.Join("$$\r\n", Images.Take(5)));
        Pro.Add(Item.video_url);
        Pro.Add($"{string.Join("$$\r\n", InventoryCoverImage)}$$");
        Pro.Add(Item.image_additional_option_info);
        Pro.Add(Item.header_html);
        Pro.Add(Item.footer_html);
        Pro.Add(Description);
        Pro.Add(Item.Shipping_number);
        Pro.Add(Item.option_number);
        Pro.Add(Item.available_shipping_date);
        Pro.Add(Item.desired_shipping_date);
        var KeywordStr = string.Join("$$", Keyword.Distinct());
        if (KeywordStr.Length > 30) KeywordStr = KeywordStr.Substring(0, 30);
        if (KeywordStr.Contains("$$")) KeywordStr = KeywordStr.Before("$$");
        Pro.Add(KeywordStr);
        Pro.Add(Item.item_condition_type);
        Pro.Add(Item.origin_type);
        Pro.Add(Item.origin_region_id);
        Pro.Add(Item.origin_country_id);
        Pro.Add(Item.origin_others);
        Pro.Add(Item.medication_type);
        Pro.Add(Item.item_weight);
        Pro.Add(Item.item_material);
        Pro.Add(Item.model_name);
        //string ProduceType = "";
        //if (TempPro.ProduceCode.Length == 12) ProduceType = "UPC代码";
        //else if (TempPro.ProduceCode.Length == 13) ProduceType = "EAN代码";
        //Pro.Add(ProduceType);//UPC EAN
        //Pro.Add(TempPro.ProduceCode);
        ExportCommon.ProductExportHelper.SetCommonCell(Pro, "", 2);
        Pro.Add(Item.manufacture_date);
        Pro.Add(Item.expiration_date_type);
        Pro.Add(Item.expiration_date_MFD);
        Pro.Add(Item.expiration_date_PAO);
        Pro.Add(Item.expiration_date_EXP);
        Pro.Add(Item.under18s_display);
        Pro.Add(Item.AS_info);
        Pro.Add(Item.buy_limit_type);
        Pro.Add(Item.buy_limit_date);
        Pro.Add(Item.buy_limit_qty);
        Product.Add(Pro);
        var Dic = new Dictionary<int, List<List<string>>>
        {
            { 0, Product }
        };
        return Dic;
    }
    public class ParameterItem //: JsonObject<ParameterItem>
    {
        public string item_number { get; set; }
        public string seller_unique_item_id { get; set; }
        public string category_number { get; set; }
        public string brand_number { get; set; }
        public string item_promotion_name { get; set; }
        [Required]
        public string item_status { get; set; } = null!;
        [Required]
        public string end_date { get; set; } = null!;
        public string additional_option_info { get; set; }
        public string additional_option_text { get; set; }
        public string video_url { get; set; }
        public string image_additional_option_info { get; set; }
        public string header_html { get; set; }
        public string footer_html { get; set; }
        [Required]
        public string Shipping_number { get; set; } = null!;
        public string option_number { get; set; }
        [Required]
        public string available_shipping_date { get; set; } = null!;
        public string desired_shipping_date { get; set; }
        [Required]
        public string item_condition_type { get; set; } = null!;
        [Required]
        public string origin_type { get; set; } = null!;
        public string origin_region_id { get; set; }
        public string origin_country_id { get; set; }
        public string origin_others { get; set; }
        public string external_product_id { get; set; }
        public string medication_type { get; set; }
        public string item_weight { get; set; }
        public string item_material { get; set; }
        public string model_name { get; set; }
        public string manufacture_date { get; set; }
        public string expiration_date_type { get; set; }
        public string expiration_date_MFD { get; set; }
        public string expiration_date_PAO { get; set; }
        public string expiration_date_EXP { get; set; }
        public string under18s_display { get; set; }
        public string AS_info { get; set; }
        public string buy_limit_type { get; set; }
        public string buy_limit_date { get; set; }
        public string buy_limit_qty { get; set; }

        public List<option_infoItem> option_info { get; set; }
        public class option_infoItem
        {
            public string option_name { get; set; }
            public string detail_name { get; set; }
            public string option_price { get; set; }
            public string option_code { get; set; }
        }
    }
}
