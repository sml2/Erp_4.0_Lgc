﻿using ERP.Exceptions.Export;
using ERP.Export.V4_Export.Interfaces;

namespace ERP.Export.Templates;

public class ExcelTemplateFactory
{
    public ExcelTemplateFactory()
    {
    }

    private readonly Dictionary<TemplateType, ITemplate> _templates = new();

    /// <summary>
    /// 注册模板
    /// </summary>
    /// <param name="template"></param>
    public void Register(ITemplate template)
    {
        if (!_templates.TryAdd(template.TemplateType, template))
            throw new ExportException($"模板[{template.TemplateType}]重复注册!");
    }

    public ITemplate? this[TemplateType templateType]
    {
        get
        {
            if (_templates.TryGetValue(templateType, out var value))
                return value;
            return default;
            // throw new ExportException($"不支持的模板类型{templateType}!");
        }
    }
}
