﻿using System.ComponentModel.DataAnnotations;
using ERP.Export.Templates;
using ERP.Models.Export;

namespace ERP.Export.V4_Export.Interfaces;

public interface ITemplate
{
    TemplateType TemplateType { get; }
    string Name { get; }
    /// <summary>
    /// 每个Sheet开始填写产品数据的index
    /// </summary>
    /// <returns></returns>
    Dictionary<int, int> StartRowIndex { get; }

    /// <summary>
    /// 验证模板参数.
    /// TODO: 修改所有返回值为非Task
    /// </summary>
    /// <param name="parameter">参数json</param>
    /// <returns></returns>
    Task<bool> Verify(string parameter);

    /// <summary>
    /// 验证模板参数.
    /// </summary>
    /// <param name="parameter">参数json</param>
    /// <param name="results">验证错误结果, 验证通过时Count为0</param>
    /// <returns>true代表验证通过</returns>
    public bool TryValidate(string parameter, out List<ValidationResult> results);

    Task<Dictionary<int, List<List<string?>>>?> GetOhterData(DataItem Data, ExcleRole role, string Parameter, List<Unit> rate/*, LogSub Log*/);

    /// <summary>
    /// 获取模板的表头
    /// </summary>
    /// <param name="parameter">参数json</param>
    /// <returns></returns>
    Task<Dictionary<string, List<List<string>>>> GetHead(string parameter);
    Task<Dictionary<int, List<List<string?>>>?> GetData(DataItem Data, ExcleRole role, string parameter, List<Unit> rate);
}
