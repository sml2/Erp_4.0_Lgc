﻿using Newtonsoft.Json;
using NPOI.SS.UserModel;
using NPOI.XSSF.Streaming;

namespace ERP.Export.Templates.Adapter;

/// <summary>
/// 导出生成表格适配器
/// </summary>
internal class ExcelExportAdapter : IExportAdapter
{
    protected IWorkbook Workbook;
    private readonly ILogger<ExcelExportAdapter> _logger;
    protected Dictionary<ISheet, int> Sheets;
    private bool _disposedValue;

    public ExcelExportAdapter(IWorkbook workbook, ILogger<ExcelExportAdapter> logger)
    {
        Workbook = workbook;
        _logger = logger;
        Sheets = new Dictionary<ISheet, int>();
    }
    
    public ExcelExportAdapter(IWorkbook workbook, ILogger<ExcelExportAdapter> logger, Dictionary<ISheet, int> sheets)
    {
        Workbook = workbook;
        _logger = logger;
        Sheets = sheets;
    }

    public virtual void AddHeaders(Dictionary<string, List<List<string>>> headers)
    {
        foreach (var (sheetName, value) in headers)
        {
            var rowIndex2 = 0;
            var sheet2 = Workbook.CreateSheet(sheetName);
            rowIndex2 += FullData(sheet2, rowIndex2, value);
            Sheets.Add(sheet2, rowIndex2);
        }
    }

    public void AddProduct(Dictionary<int, List<List<string>>> product)
    {
        
        for (int index = 0; index < product.Count; index++)
        {
            var sheet = Sheets.ElementAt(index);
            int rowIndex = sheet.Value;
            rowIndex += FullData(sheet.Key, sheet.Value, product[index]);
            Sheets[sheet.Key] = rowIndex;
        }
    }
    
    private int FullData(ISheet sheet, int startRow, List<List<string>> data)
    {
        var rowNum = data.Count;
        var exportNum = 0;
        for (var i = 0; i <= rowNum - 1; i++)
        {
            var row = sheet.CreateRow(startRow + i);
            var rowData = data[i];
            var colNum = rowData.Count;
            var canExport = true;
            // 检测当前数据中是否有超过最大字符的数据,存在则跳过导出
            foreach (var item in rowData)
            {
                if (item.IsNotNull() && item.Length > 32767)
                {
                    canExport = false;
                    _logger.LogWarning("超过最大字符的数据{Unknown}{Substring}", rowData[1], item.Substring(0, 50));
                    break;
                }
            }
            // 可导出
            if (canExport)
            {
                exportNum += 1;
                for (var colIndex = 0; colIndex <= colNum - 1; colIndex++)
                {
                    row.CreateCell(colIndex).SetCellValue(rowData[colIndex]);
                }
            }
        }

        return exportNum;
    }

    public void SaveToPath(string path)
    {
        using var fs = File.OpenWrite(path);
        Workbook.Write(fs);
    }

    public string GetState()
    {
        return JsonConvert.SerializeObject(Sheets.Values.ToArray());
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!_disposedValue)
        {
            if (disposing)
            {
                Workbook.Close();
                Sheets.Clear();
                // 删除临时文件
                ((SXSSFWorkbook)Workbook).Dispose();
            }
            
            _disposedValue = true;
        }
    }

    public void Dispose()
    {
        // 不要更改此代码。请将清理代码放入“Dispose(bool disposing)”方法中
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
}