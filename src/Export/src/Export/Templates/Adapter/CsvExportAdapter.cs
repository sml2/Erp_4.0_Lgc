﻿using System.Text;

namespace ERP.Export.Templates.Adapter;

internal class CsvExportAdapter : IExportAdapter
{
    private readonly StringBuilder _output;
    private readonly Stream _stream;
    private bool _disposed;
    private readonly string _tmpPath;

    public CsvExportAdapter()
    {
        _output = new StringBuilder();
        _tmpPath = Path.Combine(Path.GetTempPath(), $"export_{DateTime.Now.ToString("yyyyMMdd-HHmmss")}.csv");
        _stream = new FileStream(_tmpPath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read);
    }

    public CsvExportAdapter(string path) : this()
    {
        using var fs = File.OpenRead(path);
        fs.CopyTo(_stream);
    }

    public void AddHeaders(Dictionary<string, List<List<string>>> headers)
    {
        CheckDisposed();
        headers.First().Value.ForEach((r) => { _output.AppendLine(string.Join(",", r.Select(MapField))); });
    }

    public void AddProduct(Dictionary<int, List<List<string>>> product)
    {
        CheckDisposed();
        product.FirstOrDefault().Value.ForEach((r) => { _output.AppendLine(string.Join(",", r.Select(MapField))); });

        if (_output.Length > 1 * 1024 * 1024)
        {
            var buffer = Encoding.UTF8.GetBytes(_output.ToString());
            _stream.Write(buffer, 0, buffer.Length);
            _output.Clear();
        }
    }

    private static string MapField(string origin)
    {
        if (string.IsNullOrEmpty(origin))
            return origin;

        var sb = new StringBuilder(origin);
        if (origin.Contains('\n'))
            sb.Replace("\n", "\\n");
        
        // 中文符号会导致乱码
        var charList = new List<char>{'（', '）', '’', '，', '“', '”'};
        var mapList = new List<char>{'(', ')', '\'', ',', '"', '"'};
        for (var i = 0; i < charList.Count; i++)
            if (origin.Contains(charList[i]))
                sb.Replace(charList[i], mapList[i]);

        if (origin.Contains(',') || origin.Contains('"'))
        {
            sb.Replace("\"", "\"\"");
            sb.Insert(0, "\"");
            sb.Append('"');
        }

        return sb.ToString();
    }

    public void SaveToPath(string path)
    {
        CheckDisposed();
        var buffer = Encoding.UTF8.GetBytes(_output.ToString());
        _stream.Write(buffer, 0, buffer.Length);
        _stream.Flush();
        _output.Clear();

        if (Path.GetFullPath(path) != Path.GetFullPath(_tmpPath))
            File.Copy(_tmpPath, path, true);
    }

    public string GetState() => string.Empty;

    private void CheckDisposed()
    {
        if (_disposed)
            throw new ObjectDisposedException(nameof(CsvExportAdapter));
    }

    public void Dispose()
    {
        _disposed = true;
        _output.Clear();
        _stream.Close();
        _stream.Dispose();
        File.Delete(_tmpPath);
        GC.SuppressFinalize(this);
    }
}