﻿namespace ERP.Export.Templates.Adapter;

public interface IExportAdapter : IDisposable
{
    /// <summary>
    /// 添加表头
    /// </summary>
    /// <param name="headers"></param>
    public void AddHeaders(Dictionary<string, List<List<string>>> headers);

    /// <summary>
    /// 添加单条产品数据
    /// </summary>
    /// <param name="product"></param>
    public void AddProduct(Dictionary<int, List<List<string>>> product);
    
    /// <summary>
    /// 保存文件至目录
    /// </summary>
    /// <param name="path"></param>
    public void SaveToPath(string path);

    /// <summary>
    /// 获取断点状态, 用于继续导出
    /// </summary>
    /// <returns></returns>
    public string GetState();
}