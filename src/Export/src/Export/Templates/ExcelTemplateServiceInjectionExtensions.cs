﻿using ERP.Export.Templates;
using ERP.Export.V4_Export.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Microsoft.Extensions.DependencyInjection;

public static class ExcelTemplateServiceInjectionExtensions
{
    public static IExcelServiceCollection AddExcelTemplate(this IServiceCollection service, IConfiguration configuration)
    {
        service.Configure<ExportFeatures>(configuration.GetSection("Features"));
        
        service.AddSingleton<ExcelTemplateFactory>(sp =>
        {
            var instance = new ExcelTemplateFactory();
            foreach (var template in sp.GetServices<ITemplate>())
            {
                instance.Register(template);
            }
            return instance;
        });

        return new ExcelServiceCollection(service);
    }
    
    
    private class ExcelServiceCollection : IExcelServiceCollection
    {
        private readonly IServiceCollection _service;
        
        public ExcelServiceCollection(IServiceCollection service)
        {
            _service = service;
        }

        /// <summary>
        /// 注册模板类
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IExcelServiceCollection RegisterTemplate<T>() where T : class, ITemplate
        {
            _service.AddTransient<ITemplate, T>();
            return this;
        }
    }
}

/// <summary>
/// 导出表格服务容器
/// </summary>
public interface IExcelServiceCollection
{
    /// <summary>
    /// 注册模板类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public IExcelServiceCollection RegisterTemplate<T>() where T : class, ITemplate;
}