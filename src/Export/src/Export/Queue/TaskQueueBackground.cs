﻿using System.Threading;
using MediatR;
using Microsoft.Extensions.Hosting;
using Unit = MediatR.Unit;

namespace ERP.Export.Queue;

public class TaskQueueBackground : BackgroundService
{
    private readonly IServiceProvider _serviceProvider;
    private readonly ILogger<TaskQueueBackground> _logger;

    public TaskQueueBackground(IServiceProvider serviceProvider, ILogger<TaskQueueBackground> logger)
    {
        _serviceProvider = serviceProvider;
        _logger = logger;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        var handlers = GetServices(_serviceProvider,
            type => typeof(IRequestHandler<,>).MakeGenericType(type, typeof(Unit)));
        var queues = GetServices(_serviceProvider, type => typeof(ITaskQueue<>).MakeGenericType(type));
        _logger.LogInformation("任务队列等待延时中");
        await Task.Delay(20_000, stoppingToken);
        _logger.LogInformation("任务队列启动");
        foreach (var (key, handler) in handlers)
        {
            await InvokeAsync(handler, nameof(IQueueHandler<IQueueItem>.OnStart), new[] { queues[key], stoppingToken });
        }

        Task.WaitAll(
            queues.Select(q =>
                Task.Run(
                    () => InvokeAsync(q.Value, nameof(ITaskQueue<IQueueItem>.WaitAsync),
                        new object[] { stoppingToken }), default)).ToArray(),
            CancellationToken.None);

        static Dictionary<Type, object> GetServices(IServiceProvider serviceProvider, Func<Type, Type> makeType)
        {
            return TaskQueueDependencyInjectionExtensions.QueueItems
                .Select(item => (item, serviceProvider.GetService(makeType(item))!))
                .ToDictionary(x => x.item, x => x.Item2);
        }

        static async Task InvokeAsync(object obj, string methodName, object[] param)
        {
            var method = obj.GetType().GetMethod(methodName)!;
            var task = (Task)method.Invoke(obj, param)!;
            await task.ConfigureAwait(false);
        }
    }
}