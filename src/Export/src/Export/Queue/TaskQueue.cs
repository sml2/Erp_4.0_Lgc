﻿using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using MediatR;
using Prometheus;

namespace ERP.Export.Queue;

public class TaskQueue<T> : ITaskQueue<T> where T : IQueueItem
{
    private readonly IMediator _mediator;
    private readonly ConcurrentQueue<T> _queue;
    private readonly ConcurrentQueue<int> _idleQueue;
    private readonly Task?[] _workingTasks;
    private readonly Task _mainTask;
    private CancellationTokenSource _stoppingTs = new CancellationTokenSource();
    private TaskCompletionSource _are = new TaskCompletionSource();
    private static readonly Gauge Progressing  = Metrics.CreateGauge("erp_task_queue_in_progress", "任务队列中正在执行的任务数量", "queue");


    public TaskQueue(IMediator mediator,int capacity)
    {
        _mediator = mediator;
        _queue = new ConcurrentQueue<T>();
        _idleQueue = new ConcurrentQueue<int>(Enumerable.Range(0, capacity));
        _workingTasks = new Task?[capacity];
        _mainTask = Task.Run(MainTaskCore);
    }


    private Task MainTaskCore()
    {
        while (!_stoppingTs.IsCancellationRequested)
        {
            while (!_stoppingTs.IsCancellationRequested && _queue.TryDequeue(out var item))
            {
                if (!_idleQueue.TryDequeue(out var num))
                {
                    // 没有空闲任务时等待任意一个任务完成
                    Task.WaitAny(_workingTasks!);
                    Debug.Assert(_idleQueue.TryDequeue(out num)); // 必定为真
                }
                _workingTasks[num]?.Dispose();
                _workingTasks[num] = Task.Run(() => RunWorkAsync(num, item));
            }

            _are = new TaskCompletionSource();
            // 队列中没有任务时, 挂起1分钟
            Task.WaitAny(Task.Delay(60_000, _stoppingTs.Token), Task.Run(() => _are.Task, _stoppingTs.Token));
        }

        return Task.CompletedTask;
    }

    private async Task RunWorkAsync(int num, T item)
    {
        using (Progressing.WithLabels(typeof(T).Name).TrackInProgress())
        {
            await _mediator.Send(item, _stoppingTs.Token);
            _idleQueue.Enqueue(num);
        }
    }
    
    public void Dispose()
    {
        _stoppingTs.Dispose();
    }

    public void Enqueue(T item)
    {
        _queue.Enqueue(item);
        _are.TrySetResult();
    }

    public async Task WaitAsync(CancellationToken stoppingToken)
    {
        _stoppingTs = CancellationTokenSource.CreateLinkedTokenSource(stoppingToken, _stoppingTs.Token);
        await _mainTask;
    }
}