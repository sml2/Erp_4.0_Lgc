﻿using System.Threading;
using MediatR;

namespace ERP.Export.Queue;

public interface IQueueItem : IRequest
{
    
}

public interface IQueueHandler<T> : IRequestHandler<T> where T : IQueueItem
{
    /// <summary>
    /// 在项目启动时,初始化队列事件
    /// </summary>
    /// <param name="queue"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task OnStart(ITaskQueue<T> queue, CancellationToken cancellationToken);
}