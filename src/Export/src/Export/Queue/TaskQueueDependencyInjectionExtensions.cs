﻿using ERP.Export.Queue;
using MediatR;

namespace Microsoft.Extensions.DependencyInjection;

public static class TaskQueueDependencyInjectionExtensions
{
    public static List<Type> QueueItems = new List<Type>();
    /// <summary>
    /// 添加后台任务队列
    /// </summary>
    /// <param name="services"></param>
    /// <typeparam name="TItem"></typeparam>
    /// <typeparam name="THandler"></typeparam>
    /// <returns></returns>
    public static IServiceCollection AddTaskQueue<TItem, THandler>(this IServiceCollection services)
        where TItem : IQueueItem where THandler : IQueueHandler<TItem>
    {
        return services.AddTaskQueue<TItem, THandler>(10);
    }

    /// <summary>
    /// 添加后台任务队列
    /// </summary>
    /// <param name="services"></param>
    /// <param name="maxTaskNum">同时执行任务的最大数量</param>
    /// <typeparam name="TItem"></typeparam>
    /// <typeparam name="THandler"></typeparam>
    /// <returns></returns>
    public static IServiceCollection AddTaskQueue<TItem, THandler>(this IServiceCollection services, int maxTaskNum)
        where TItem : IQueueItem where THandler : IQueueHandler<TItem>
    {
        QueueItems.Add(typeof(TItem));
        services.AddHostedService<TaskQueueBackground>();
        return services.AddSingleton<ITaskQueue<TItem>, TaskQueue<TItem>>(sp =>
                ActivatorUtilities.CreateInstance<TaskQueue<TItem>>(sp, maxTaskNum));
    }
}