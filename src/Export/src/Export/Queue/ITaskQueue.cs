﻿using System.Diagnostics.CodeAnalysis;
using System.Threading;

namespace ERP.Export.Queue;

public interface ITaskQueue<in T> : IDisposable where T : IQueueItem
{
    public void Enqueue(T item);

    /// <summary>
    /// 等待队列任务执行完成
    /// </summary>
    /// <param name="stoppingToken"></param>
    /// <returns></returns>
    internal Task WaitAsync(CancellationToken stoppingToken);
}