

namespace ERP.Exceptions.Export;

public class ExportException : Exception //StorageDomainException
{
    public ExportException(string message) : base(message)
    {
    }
}