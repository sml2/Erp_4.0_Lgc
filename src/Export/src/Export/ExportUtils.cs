﻿using System.Text;
using ERP.Export.Templates;

namespace ERP.Export;

public static class ExportUtils
{
    /// <summary>
    /// 去除文本中的html标签, 保留amazon的br标签
    /// </summary>
    /// <param name="description"></param>
    /// <param name="shouldRemove"></param>
    /// <param name="templateType"></param>
    /// <returns></returns>
    public static string RemoveHtmlTag(string? description, bool shouldRemove, TemplateType templateType)
    {
        if (string.IsNullOrWhiteSpace(description))
            return string.Empty;
        if (!shouldRemove && templateType != TemplateType.Amazon)
            return description;

        var reserves = new List<string>() { "img" };
        if (templateType == TemplateType.Amazon)
            reserves.Add("br");

        if (shouldRemove)
            description = RemoveTag(description, new[]
            {
                "script",
                "style"
            }, reserves.ToArray());

        if (templateType == TemplateType.Amazon)
            description = description.Replace("\r\n", "<br>").Replace("\n", "<br>");

        return description;
    }
    
    /// <summary>
    /// 设置保留节点
    /// </summary>
    /// <param name="html"></param>
    /// <param name="removeTags"></param>
    /// <param name="reservedTags">保留节点</param>
    /// <returns></returns>
    public static string RemoveTag(string html, string[] removeTags, string[] reservedTags)
    {
        if (html.IsNullOrEmpty())
            return string.Empty;

        var removeTagLst = removeTags?.Select((string s) => s.ToLower()).ToList();
        var reservedTagLst = reservedTags?.Select((string s) => s.ToLower()).ToList();
        var lastIndex = 0;
        var curIndex = 0;
        var curTag = "";
        var lastTag = "";
        var length = html.Length;
        var stringBuilder = new StringBuilder();
        while (curIndex < length)
        {
            curIndex = html.IndexOf("<", curIndex);
            if (curIndex != -1)
            {
                if (curTag is not ("" or "p") && !(curTag == "/p" && lastTag == "br"))
                    stringBuilder.AppendLine();
                stringBuilder.Append(html.AsSpan(lastIndex, curIndex - lastIndex));
                lastIndex = curIndex;
                lastTag = curTag;
            }

            var isLast = curIndex >= length - 1;
            if (curIndex != -1 && !isLast)
            {
                var tagIndexA = html.IndexOf(" ", curIndex);
                var tagIndexB = html.IndexOf(">", curIndex);
                var tagIndex = ((tagIndexA == -1)
                    ? tagIndexB
                    : ((tagIndexB != -1) ? Math.Min(tagIndexA, tagIndexB) : tagIndexA));
                var tagLen = tagIndex - curIndex - 1;
                if (tagIndex == -1 || curIndex + 2 + tagLen > length)
                {
                    break;
                }

                curTag = html.Substring(curIndex + 1, tagLen).ToLower();
                curIndex += tagLen + 1;
                var needReserved = reservedTagLst != null && reservedTagLst.Count != 0 &&
                                   reservedTagLst.Contains(curTag);
                var needRemove = removeTagLst == null || removeTagLst.Count == 0 || removeTagLst.Contains(curTag);
                if (needRemove || needReserved)
                {
                    var endTag = $"{"</"}{curTag}>"; // </div>
                    var endTagStartIndex =
                        html.IndexOf(endTag, curIndex,
                            StringComparison
                                .OrdinalIgnoreCase); // -1表示不包含尾节点 Then</input>==>形如：<input type = "hidden" id="userDeviceType" value="" />
                    if (needReserved)
                    {
                        if (endTagStartIndex == -1)
                        {
                            var reservedIndex = html.IndexOf(">", lastIndex);
                            curIndex = ((reservedIndex != -1)
                                ? reservedIndex // 找到保留的结尾部分
                                : (length - 1)); // html不完整的情况 < img src = "" 没有结束标志
                        }
                        else
                        {
                            curIndex = endTagStartIndex + endTag.Length; // </   xxx        >
                        }

                        continue;
                    }

                    // 删除字段(数组删除的是包含在该元素节点中的所有内容) 形如：<script>...</script>
                    if (endTagStartIndex == -1)
                    {
                        break;
                    }

                    curIndex = endTagStartIndex + endTag.Length; // </   xxx        >
                    lastIndex = curIndex;
                }
                else
                {
                    var startTagEndIndex = html.IndexOf(">", curIndex);
                    if (startTagEndIndex == -1)
                    {
                        break;
                    }

                    curIndex = startTagEndIndex + 1;
                    lastIndex = curIndex;
                }

                continue;
            }

            if (!isLast)
            {
                stringBuilder.AppendLine(html.Substring(lastIndex));
            }

            break;
        }

        return stringBuilder.ToString();
    }
}