﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Export
{
    internal static class ObjectExtensions
    {
        public static bool IsNull([NotNullWhen(false)] this object? obj) => obj == null;
        public static bool IsNotNull([NotNullWhen(true)] this object? obj) => obj != null;
        public static bool IsNotWhiteSpace([NotNullWhen(true)]this string? str) => !string.IsNullOrWhiteSpace(str);
        public static bool IsNullOrWhiteSpace([NotNullWhen(false)] this string? str) => string.IsNullOrWhiteSpace(str);
        public static bool IsNullOrEmpty([NotNullWhen(false)] this string? str) => string.IsNullOrEmpty(str);
        public static bool IsNotEmpty([NotNullWhen(true)] this string? str) => !string.IsNullOrEmpty(str);

        public static void ForEach<T>(this IEnumerable<T> dic, Action<T> action)
        {
            foreach (var kvp in dic)
            {
                action(kvp);
            }
        }
    }
}
