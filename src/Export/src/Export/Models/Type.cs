﻿namespace ERP.Models.Export;

public enum Type
{
    Default = 0,
    None = Default,
    DoubleSheet = 1, // 双表

    DoubleSheetWithImage = 11, // 双表 并且需要处理图片

    ThreeSheetWithImage = 21, // 三表 并且需要处理图片
}
