﻿namespace ERP.Export.Models;

public class CellData
{
    public int ID { get; set; }
    public bool State { get; set; }
    public DataItem? Data { get; set; }
    public string CodeInfo { get; set; } = null!;
}
public class DataItem
{
    public int PID { get; set; }
    public string Name { get; set; } = null!;
    public string Source { get; set; } = null!;
    public string Sku { get; set; } = null!;
    public string? ShowImage { get; set; }
    public int Quantity { get; set; }
    public Prices Price { get; set; } = null!;
    public string? Description { get; set; }
    public List<string> BulletPoint { get; set; } = null!;
    public string? SearchTerms { get; set; }
    public string? Category { get; set; }
    public Dictionary<string, string[]> Attribute { get; set; } = null!;
    public List<ProductsItem> Products { get; set; } = null!;
    public string? ProductID { get; set; }
    
    /// <summary>
    /// 采集下来的pid,可在前端修改
    /// </summary>
    public string? RawPid { get; set; }

    public List<string> ProductImages { get; set; }
}

public class ProductsItem
{
    public string SID { get; set; } = null!;
    public string Name { get; set; } = null!;
    public string? Description { get; set; }

    /// <summary>
    /// 卖点
    /// </summary>
    public List<string> BulletPoint { get; set; } = new List<string>();
    public string? SearchTerms { get; set; }

    public List<int> Values { get; set; } = null!;
    public string Sku { get; set; } = null!;

    public string? MainImage { get; set; }

    public List<string>? Images { get; set; }
    public List<string> DescriptionImages { get; set; }
    public int Quantity { get; set; }

    public string Price { get; set; } = null!;

    public string Cost { get; set; } = null!;

    public string ProduceCode { get; set; } = null!;

    public Expands Fields { get; set; } = null!;
}

public class Price
{
    private Money _max { get; set; }

    public Money Max
    {
        get => _max;
        set => _max = value;
    }

    public void SetMax(decimal value, string unit) => _max = MoneyFactory.Instance.Create(value, unit);

    public decimal GetMax(string unit) => _max.To(unit);

    private Money _min { get; set; }

    public Money Min
    {
        get => _min;
        set => _min = value;
    }

    public void SetMin(decimal value, string unit) => _min = MoneyFactory.Instance.Create(value, unit);
    public decimal GetMin(string unit) => _min.To(unit);
}

public class Prices
{
    public Price Cost { get; set; } = new Price();
    public Price Sale { get; set; } = new Price();
}

public class Expands : List<Expand>
{
    public void Add(string key, string name, string value, ExpandEnum type) => Add(new(key, name, value, type));
}

public class Expand
{
    public Expand(string key, string name, string value, ExpandEnum type)
    {
        Key = key;
        Name = name;
        Value = value;
        Type = type;
    }

    public string Key { get; set; }
    public string Name { get; set; }
    public string Value { get; set; }
    public ExpandEnum Type { get; set; }
}

public enum ExpandEnum
{
    /// <summary>
    /// 特定
    /// </summary>
    Specific,
    /// <summary>
    /// 单选框
    /// </summary>
    Radio = 1,
    /// <summary>
    /// 数字输入框
    /// </summary>
    Number = 2,
    /// <summary>
    /// 字符串输入框
    /// </summary>
    String = 3,
    /// <summary>
    /// 日期
    /// </summary>
    Date = 4,

}



