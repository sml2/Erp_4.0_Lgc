﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Runtime.Serialization;

namespace ERP.Models.Export;

public class ExcleRole
{
    public ExcleRole()
    {
        _additionalData = new Dictionary<string, JToken>();
    }
    public ExcleRole(Languages language, bool descriptionAppendImage, KeywordType keywordMode,
        SketchType sketchMode, DescriptionType descriptionMode, MainProImgType mainProImgSetting) : this()
    {
        Language = language;
        DescriptionAppendImage = descriptionAppendImage;
        KeywordMode = keywordMode;
        SketchMode = sketchMode;
        DescriptionMode = descriptionMode;
        MainProImgSetting = mainProImgSetting;
    }
    public Languages Language { get; set; }
    public bool DescriptionAppendImage { get; set; }
    // # 随机一个数字
    // @ 随机一个字母
    // * 随机一个字符(数字或字母)
    public KeywordType KeywordMode { get; set; }
    public SketchType SketchMode { get; set; }
    public DescriptionType DescriptionMode { get; set; }
    public MainProImgType MainProImgSetting { get; set; }
    public enum DescriptionType
    {
        None, // 无需填充

        Keyword, // 关键字填充

        Sketch, // 卖点填充

        Keyword_Sketch, // 优先关键字，后卖点填充

        Sketch_Keyword, // 优先卖点，后关键字填充
    }
    public enum SketchType
    {
        None, // 无需填充

        Keyword,// 关键字填充

        Description, // 描述填充

        Keyword_Description,// 优先关键字，后描述填充

        Description_Keyword, // 优先描述，后关键字填充
    }
    public enum KeywordType
    {
        None, // 无需填充

        Sketch, // 卖点填充

        Description, // 描述填充

        Sketch_Description,// 优先卖点，后描述填充

        Description_Sketch, // 优先描述，后卖点填充
    }




    /// <summary>
    /// 这个决定主产品填充的图片：
    /// 1.第一个变体图片：主产品填充第一个变体的所有图片（主图对应主图，附图对应附体）
    /// 2.最后一个变体图片：（同上取最后一个变体的数据）
    /// 3.所有变体主图：取所有变体的主图对象，这个对象的第一个图做主产品主图，其余为附图
    /// </summary>
    public enum MainProImgType
    {
        NoImage, // 无需追加

        FirstVariantImage, // 第一个变体图片

        LastVariantImage, // 最后一个变体图片
        EveryFirstImage, // 所有变体主图
    }

    [JsonExtensionData]
    private IDictionary<string, JToken> _additionalData;

    public JToken? this[string key] => _additionalData.TryGetValue(key, out var value) ? value : null;
    [JsonIgnore]
    public int? TaskId
    {
        get => this["TaskId"]?.Value<int>();
        set => _additionalData["TaskId"] = value;
    }
}
