﻿namespace ERP.Export.Models;

public class HktvmallProductItem : ProductsItem
{
    public HktvmallProductItem(ProductsItem item)
    {
        SID = item.SID;
        Name = item.Name;
        Description = item.Description;
        BulletPoint = item.BulletPoint;
        SearchTerms = item.SearchTerms;
        Values = item.Values;
        Sku = item.Sku;
        MainImage = item.MainImage;
        Images = item.Images;
        DescriptionImages = item.DescriptionImages;
        Quantity = item.Quantity;
        Price = item.Price;
        Cost = item.Cost;
        ProduceCode = item.ProduceCode;
        Fields = item.Fields;
    }

    public string NameEn { get; set; } = null!;
    public string DescriptionEn { get; set; } = null!;
    public List<string> BulletPointEn { get; set; }
}