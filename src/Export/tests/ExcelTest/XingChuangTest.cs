namespace ExcelTest;

public class XingChuangTest : Setup
{

    private readonly XingChuang _basicData;
    private readonly XingChuang.ParameterItem _defaultParam = new()
    {
    };

    public XingChuangTest()
    {
        var logger = Mock.Of<ILogger<XingChuang>>();
        _basicData = new(ServerProvider.Object);
    }

    [Fact]
    public void TestValidate()
    {
        // action
        var result = _basicData.TryValidate(JsonConvert.SerializeObject(new { }), out var results);

        // assert
        Assert.True(result);

    }


    [Fact]
    public async Task TestGetHead()
    {
        // action
        var result = await _basicData.GetHead(JsonConvert.SerializeObject(_defaultParam));

        // assert
        Assert.NotNull(result);
    }

    [Fact]
    public async Task TestGetData()
    {
        // action
        var result = await _basicData.GetData(DefaultDataItem, DefaultRole, JsonConvert.SerializeObject(_defaultParam), Rates);

        // assert
        Assert.NotNull(result);
        Assert.True(2 == result[0].Count);

    }
}