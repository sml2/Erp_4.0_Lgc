namespace ExcelTest;

public class QuickMaxvicTest : Setup
{

    private readonly QuickMaxvic _basicData;

    public QuickMaxvicTest()
    {
        var logger = Mock.Of<ILogger<QuickMaxvic>>();
        _basicData = new(ServerProvider.Object);
    }

    [Fact]
    public void TestValidate()
    {
        // action
        var result = _basicData.TryValidate(JsonConvert.SerializeObject(new { }), out var results);

        // assert
        Assert.True(result);

    }


    [Fact]
    public async Task TestGetHead()
    {
        // action
        var result = await _basicData.GetHead("");

        // assert
        Assert.NotNull(result);
    }

    [Fact]
    public async Task TestGetData()
    {
        // action
        var result = await _basicData.GetData(DefaultDataItem, DefaultRole, "", Rates);

        // assert
        Assert.NotNull(result);
        Assert.True(1 == result[0].Count);

    }
}