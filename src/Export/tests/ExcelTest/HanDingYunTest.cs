namespace ExcelTest;

public class HanDingYunTest : Setup
{

    private readonly HanDingYun _basicData;

    public HanDingYunTest()
    {
        var logger = Mock.Of<ILogger<HanDingYun>>();
        _basicData = new(ServerProvider.Object);
    }


    [Fact]
    public void TestValidate()
    {
        // action
        var result = _basicData.TryValidate(JsonConvert.SerializeObject(new { }), out var results);

        // assert
        Assert.True(result);

    }


    [Fact]
    public async Task TestGetHead()
    {
        // action
        var result = await _basicData.GetHead("");

        // assert
        Assert.NotNull(result);
    }

    [Fact]
    public async Task TestGetData()
    {
        // action
        var result = await _basicData.GetData(DefaultDataItem, DefaultRole, "", Rates);

        // assert
        Assert.NotNull(result);
        Assert.Equal(3, result.Count);
        Assert.True(1 == result[0].Count);

    }
}