namespace ExcelTest;

public class FruugoTest : Setup
{

    private readonly Fruugo _basicData;
    private readonly Fruugo.ParameterItem _defaultParam = new()
    {
    };

    public FruugoTest()
    {
        var logger = Mock.Of<ILogger<Fruugo>>();
        _basicData = new(ServerProvider.Object);
    }


    [Fact]
    public void TestValidate()
    {
        // action
        var result = _basicData.TryValidate(JsonConvert.SerializeObject(new { }), out var results);

        // assert
        Assert.True(result);

    }


    [Fact]
    public async Task TestGetHead()
    {
        // action
        var result = await _basicData.GetHead(JsonConvert.SerializeObject(_defaultParam));

        // assert
        Assert.NotNull(result);
    }

    [Fact]
    public async Task TestGetData()
    {
        // action
        var result = await _basicData.GetData(DefaultDataItem, DefaultRole, JsonConvert.SerializeObject(_defaultParam), Rates);

        // assert
        Assert.NotNull(result);
        Assert.True(2 == result[0].Count);
        var resultFirst = result[0][1];
        var i = 0;
        Assert.Equal("", resultFirst[i++]);
        Assert.Equal("", resultFirst[i++]);
        i += 5;
        Assert.Equal("0.01", resultFirst[i++]); 
        i += 9;
        Assert.Equal("", resultFirst[i++]); // 18
        i += 3;
        Assert.Equal("100000", resultFirst[i++]); // 22
        Assert.Equal("", resultFirst[i++]); // 23
    }
}