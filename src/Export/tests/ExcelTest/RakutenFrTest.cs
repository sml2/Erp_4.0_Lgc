namespace ExcelTest;

public class RakutenFrTest : Setup
{
    public readonly Rakuten_Fr.ParameterItems ParamDefault = new()
    {
    };
    private readonly Rakuten_Fr _rakutenFr;

    public RakutenFrTest()
    {
        var logger = Mock.Of<ILogger<Rakuten_Fr>>();
        _rakutenFr = new Rakuten_Fr(ServerProvider.Object);
    }

    [Fact]
    public void TestValidate()
    {
        // action
        var result= _rakutenFr.TryValidate(JsonConvert.SerializeObject(ParamDefault), out var results);

        // assert
        Assert.True(result);

    }

    [Fact]
    public async Task TestGetHead()
    {
        var logger = Mock.Of<ILogger<Rakuten_Fr>>();

        IDictionary<string, List<List<string>>> result = await _rakutenFr.GetHead("");
        Assert.Contains("Votre fichier", result);
    }

    [Fact]
    public async Task TestGetData()
    {
        // setup

        var data = DefaultDataItem;

        // action
        var result = await _rakutenFr.GetData(data, DefaultRole, JsonConvert.SerializeObject(ParamDefault), Rates);

        // assert
        Assert.NotNull(result);
        Assert.True(2 == result[0].Count);
    }
}