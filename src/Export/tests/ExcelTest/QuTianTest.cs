namespace ExcelTest;

public class QuTianTest : Setup
{

    private readonly QuTian _basicData;
    private readonly QuTian.ParameterItem _defaultParam = new()
    {
        item_status = "fakeitem_status",
        end_date = "fakeend_date",
        Shipping_number = "fakeShipping_number",
        available_shipping_date = "fakeavailable_shipping_date",
        item_condition_type = "fakeitem_condition_type",
        origin_type = "fakeorigin_type",
    };

    public QuTianTest()
    {
        var logger = Mock.Of<ILogger<QuTian>>();
        _basicData = new(ServerProvider.Object);
    }

    [Fact]
    public void TestValidate()
    {
        // action
        var result = _basicData.TryValidate(JsonConvert.SerializeObject(new { }), out var results);
        // assert
        ValidatorHelper.HasError(results, "item_status", "end_date", "Shipping_number", "available_shipping_date", "item_condition_type", "origin_type");

        // 验证成功
        // action
        result = _basicData.TryValidate(JsonConvert.SerializeObject(_defaultParam), out results);

        // assert
        Assert.True(result);

    }


    [Fact]
    public async Task TestGetHead()
    {
        // action
        var result = await _basicData.GetHead(JsonConvert.SerializeObject(_defaultParam));

        // assert
        Assert.NotNull(result);
    }

    [Fact]
    public async Task TestGetData()
    {
        // action
        var result = await _basicData.GetData(DefaultDataItem, DefaultRole, JsonConvert.SerializeObject(_defaultParam), Rates);

        // assert
        Assert.NotNull(result);
        Assert.True(1 == result[0].Count);

    }
}