using CatchExcel = ERP.Export.V4_Export.ExcelTemplates.Catch;
namespace ExcelTest;

public class CatchTest : Setup
{
    public readonly CatchExcel.ParameterItem ParamDefault = new()
    {
        Category = "category",
    };
    private readonly CatchExcel _catch;

    public CatchTest()
    {
        var logger = Mock.Of<ILogger<CatchExcel>>();
        _catch = new CatchExcel(ServerProvider.Object);
    }

    [Fact]
    public void TestValidate()
    {
        // action
        var result = _catch.TryValidate(JsonConvert.SerializeObject(new { }), out var results);
        // assert
        ValidatorHelper.HasError(results, "Category");

        // 验证成功
        // action
        result = _catch.TryValidate(JsonConvert.SerializeObject(ParamDefault), out results);

        // assert
        Assert.True(result);

    }

    [Fact]
    public async Task TestGetHead()
    {
        var logger = Mock.Of<ILogger<CatchExcel>>();

        IDictionary<string, List<List<string>>> result = await _catch.GetHead("");
        Assert.Contains("Data", result);
    }

    [Fact]
    public async Task TestGetData()
    {
        // setup

        var data = DefaultDataItem;

        // action
        var result = await _catch.GetData(data, DefaultRole, JsonConvert.SerializeObject(ParamDefault), Rates);

        // assert
        Assert.NotNull(result);
        Assert.True(2 == result[0].Count);

        var resultFirst = result[0][0]; // 第一个变体
        var first = data.Products[0];
        var i = 0;
        Assert.Equal(ParamDefault.Category, resultFirst[i++]);
        Assert.Equal("fakeSku1", resultFirst[i++]);
        Assert.Equal($"{first.Name}-fakeColor1", resultFirst[i++]);
        Assert.Equal(first.ProduceCode, resultFirst[i++]);
        Assert.Equal("", resultFirst[i++]);
        Assert.Equal(first.Description, resultFirst[i++]);
        Assert.Equal(ParamDefault.Brand, resultFirst[i++]);
        i += 2;
        Assert.Equal("fakeColor1", resultFirst[i++]);
        Assert.Equal("fakeDescription1", resultFirst[i++]);
        Assert.Equal(ParamDefault.Gender, resultFirst[i++]);
        Assert.Equal(ParamDefault.Material, resultFirst[i++]);
        Assert.Equal("fakeSku-0001", resultFirst[i++]);
        Assert.Equal("fakeColor1", resultFirst[i++]);
        Assert.Equal("fakeSize1", resultFirst[i++]);
        i += 21;
        Assert.Equal(ParamDefault.Weight, resultFirst[i++]);
        Assert.Equal(ParamDefault.WeightUnit, resultFirst[i++]);
        Assert.Equal(ParamDefault.Width, resultFirst[i++]);
        Assert.Equal(ParamDefault.WidthUnit, resultFirst[i++]);
        Assert.Equal(ParamDefault.Length, resultFirst[i++]);
        Assert.Equal(ParamDefault.LengthUnit, resultFirst[i++]);
        Assert.Equal(ParamDefault.Height, resultFirst[i++]);
        Assert.Equal(ParamDefault.HeightUnit, resultFirst[i++]);
        Assert.Equal(ParamDefault.ModelNumber, resultFirst[i++]);
        Assert.Equal(ParamDefault.Season, resultFirst[i++]);
        i += 23;
        Assert.Equal("fakeSku1", resultFirst[i++]);
        Assert.Equal("fakeSku1", resultFirst[i++]);
        i += 3;
        Assert.Equal("1.00", resultFirst[i++]);
        i += 1;
        Assert.Equal(first.Quantity.ToString(), resultFirst[i++]);
        i += 8;
        Assert.Equal(ParamDefault.LeadTimeToShip, resultFirst[i++]);
        i += 6;
        Assert.Equal(ParamDefault.LocationCountry, resultFirst[i++]);
        Assert.Equal(ParamDefault.ShippedFromPostCode, resultFirst[i++]);
    }

}