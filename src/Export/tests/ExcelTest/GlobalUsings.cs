﻿// Global using directives

global using ERP.Export.ExportCommon;
global using ERP.Export.V4_Export.ExcelTemplates;
global using ERP.Export.Models;
global using ERP.Interface;
global using Microsoft.AspNetCore.Hosting;
global using Microsoft.Extensions.DependencyInjection;
global using Microsoft.Extensions.Logging;
global using Xunit;
global using Moq;
global using Ryu.Data;
global using Newtonsoft.Json;
global using ERP.Enums;
global using ERP.Models.Export;
global using System.ComponentModel.DataAnnotations;