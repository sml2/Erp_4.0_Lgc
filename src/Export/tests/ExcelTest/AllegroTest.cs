namespace ExcelTest;

public class AllegroTest : Setup
{

    private Allegro.ParameterItem DefaultParam = new Allegro.ParameterItem();
    private readonly Allegro _allegro;

    public AllegroTest()
    {
        var logger = Mock.Of<ILogger<Allegro>>();
        _allegro = new Allegro(ServerProvider.Object);
    }

    [Fact]
    public void TestValidate()
    {
        // action
        var result = _allegro.TryValidate(JsonConvert.SerializeObject(new { }), out var results);

        // assert
        Assert.True(true);
    }


    [Fact]
    public async Task TestGetHead()
    {
        // action
        var result = await _allegro.GetHead(JsonConvert.SerializeObject(DefaultParam));

        // assert
        Assert.NotNull(result);
    }

    [Fact]
    public async Task TestGetData()
    {
        // action
        var result = await _allegro.GetData(DefaultDataItem, DefaultRole, JsonConvert.SerializeObject(DefaultParam), Rates);

        // assert
        Assert.NotNull(result);
        Assert.True(2 == result[0].Count);

        var firstResult = result[0][0];
        var first = DefaultDataItem.Products[0];
        var i = 0;

        i += 7;
        Assert.Equal(first.ProduceCode, firstResult[i++]);
        Assert.Equal(DefaultParam.main_category, firstResult[i++]);
        Assert.Equal(DefaultParam.leaf_category, firstResult[i++]);
        Assert.Equal(DefaultParam.seller_sku, firstResult[i++]);
        Assert.Equal(first.Quantity.ToString(), firstResult[i++]);
        Assert.Equal("1.00", firstResult[i++]);
        Assert.Equal(first.Name, firstResult[i++]);
        Assert.Equal("", firstResult[i++]);
        Assert.Equal("fakeDescription1", firstResult[i++]);
        Assert.Equal(DefaultParam.shipping_price_list, firstResult[i++]);
        Assert.Equal(DefaultParam.dispatch_time, firstResult[i++]);
        Assert.Equal(DefaultParam.country, firstResult[i++]);
        Assert.Equal(DefaultParam.province, firstResult[i++]);
        Assert.Equal(DefaultParam.postal_code, firstResult[i++]);
        Assert.Equal(DefaultParam.city, firstResult[i++]);
        Assert.Equal(DefaultParam.invoice, firstResult[i++]);
        Assert.Equal(DefaultParam.offer_subject, firstResult[i++]);
        Assert.Equal(DefaultParam.vat_rate, firstResult[i++]);
        Assert.Equal(DefaultParam.base_of_vat_exemption, firstResult[i++]);
        Assert.Equal(DefaultParam.returns_terms, firstResult[i++]);
        Assert.Equal(DefaultParam.complaints_terms, firstResult[i++]);
        Assert.Equal(DefaultParam.warranties, firstResult[i++]);
        Assert.Equal(DefaultParam.state_dict, firstResult[i++]);
        Assert.Equal(DefaultParam.original_manufacturer_s_packaging_dict, firstResult[i++]);
        Assert.Equal(DefaultParam.number_of_pieces_on_offer_szt_text, firstResult[i++]);
        Assert.Equal(DefaultParam.packaging_status_dict, firstResult[i++]);
        Assert.Equal(DefaultParam.gender_dict, firstResult[i++]);
        Assert.Equal(DefaultParam.manufacturer_code_text, firstResult[i++]);
        Assert.Equal(DefaultParam.brand_dict, firstResult[i++]);
        Assert.Equal("fakeColor1", firstResult[i++]);
        Assert.Equal("fakeSize1", firstResult[i++]);
        Assert.Equal(DefaultParam.main_pattern_dict, firstResult[i++]);
        Assert.Equal(DefaultParam.cut_dict, firstResult[i++]);
        Assert.Equal(DefaultParam.neckline_dict, firstResult[i++]);
        Assert.Equal(DefaultParam.sleeve_dict, firstResult[i++]);
        Assert.Equal(DefaultParam.main_fabric_dict, firstResult[i++]);
        Assert.Equal(DefaultParam.additional_features_dict, firstResult[i++]);
        Assert.Equal(DefaultParam.model_text, firstResult[i++]);
        Assert.Equal(DefaultParam.height_cm_text, firstResult[i++]);
        Assert.Equal(DefaultParam.kind_dict, firstResult[i++]);
        Assert.Equal(DefaultParam.width_long_side_cm_text, firstResult[i++]);
        Assert.Equal(DefaultParam.depth_short_side_cm_text, firstResult[i++]);
        Assert.Equal(DefaultParam.main_material_dict, firstResult[i++]);
        Assert.Equal(DefaultParam.main_color_dict, firstResult[i++]);
        Assert.Equal(DefaultParam.material_dict, firstResult[i++]);
        Assert.Equal(DefaultParam.bracelet_length_mm_text, firstResult[i++]);
        Assert.Equal(DefaultParam.total_length_cm_text, firstResult[i++]);
    }
}