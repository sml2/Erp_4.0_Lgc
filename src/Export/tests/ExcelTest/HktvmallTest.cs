namespace ExcelTest;

public class HktvmallTest : Setup
{
    public readonly Hktvmall.ParameterItem ParamDefault = new()
    {
        PrimaryCategoryCode = "PrimaryCategoryCode",
        ProductTypeCode = "ProductTypeCode",
        Brand = "brand",
        ManufacturedCountry = "manufacturer",
    };
    private readonly Hktvmall _hktvmall;

    public HktvmallTest()
    {
        _hktvmall = new (ServerProvider.Object);
    }

    [Fact]
    public void TestValidate()
    {
        // action
        var result = _hktvmall.TryValidate(JsonConvert.SerializeObject(new { }), out var results);
        // assert
        ValidatorHelper.HasError(results, "PrimaryCategoryCode", "ProductTypeCode");

        // 验证成功
        // action
        result = _hktvmall.TryValidate(JsonConvert.SerializeObject(ParamDefault), out results);

        // assert
        Assert.True(result);

    }

    [Fact]
    public async Task TestGetHead()
    {
        IDictionary<string, List<List<string>>> result = await _hktvmall.GetHead("");
        Assert.Contains("Product Template", result);
    }

    [Fact]
    public async Task TestGetData()
    {
        // setup

        var data = DefaultDataItem;
        var enName = "enName";
        var enDescription = "enDescription";
        data.Products = data.Products.Select(p => (ProductsItem)new HktvmallProductItem(p)
        {
            NameEn = enName,
            DescriptionEn = enDescription,
        }).ToList();
        // action
        var result = await _hktvmall.GetData(data, DefaultRole, JsonConvert.SerializeObject(ParamDefault), Rates);

        // assert
        Assert.NotNull(result);
        Assert.True(2 == result![0].Count);

        var resultFirst = result[0][0]; // 第一个变体
        var first = data.Products[0];
        var i = 0;
        Assert.Equal(first.Sku, resultFirst[i++]);
        Assert.Equal(first.Sku, resultFirst[i++]);
        Assert.Equal(ParamDefault.ProductTypeCode, resultFirst[i++]);
        Assert.Equal(ParamDefault.PrimaryCategoryCode, resultFirst[i++]);
        Assert.Equal(ParamDefault.Brand, resultFirst[i++]);
        Assert.Equal(ParamDefault.ProductReadyMethod, resultFirst[i++]);
        i += 2;
        Assert.Equal(ParamDefault.Warehouse, resultFirst[i++]);
        Assert.Equal("", resultFirst[i++]);
        Assert.Equal("Yes", resultFirst[i++]);
        Assert.Equal(enName, resultFirst[i++]);
        Assert.Equal(first.Name, resultFirst[i++]);
        i += 2;
        Assert.Equal(enDescription, resultFirst[i++]);
        Assert.Equal(first.Description, resultFirst[i++]);
        i += 2;
        Assert.Equal("enDescription", resultFirst[i++]);
        Assert.Equal("fakeBulletPoint1", resultFirst[i++]);
        Assert.Equal(first.MainImage, resultFirst[i++]);
        Assert.Equal("", resultFirst[i++]);
        Assert.Equal(string.Join(",", first.Images!.Skip(1)), resultFirst[i++]);
        Assert.Equal(string.Join(",", first.Images!.Skip(1)), resultFirst[i++]);
        i += 16;
        Assert.Equal(ParamDefault.ManufacturedCountry, resultFirst[i++]);
        Assert.Equal("", resultFirst[i++]);
        Assert.Equal("", resultFirst[i++]);
        i += 1;
        Assert.Equal("", resultFirst[i++]);
        Assert.Equal("", resultFirst[i++]);
        Assert.Equal("HKD : HKD", resultFirst[i++]);
        Assert.Equal("", resultFirst[i++]);
        Assert.Equal("1.00", resultFirst[i++]);
        i += 4;
        Assert.Equal("RED", resultFirst[i++]);
        i += 10;
        Assert.Equal("F : 小心輕放", resultFirst[i++]);
        i += 40;
        Assert.Equal("0", resultFirst[i++]);
        Assert.Equal("14 : 14", resultFirst[i++]);
        Assert.Equal("MS : Mon-Sat", resultFirst[i++]);
        Assert.Equal("AM/PM", resultFirst[i++]);
        i += 7;
    }
}