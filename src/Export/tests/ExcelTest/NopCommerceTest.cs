namespace ExcelTest;

public class NopCommerceTest : Setup
{

    private readonly NopCommerce _basicData;
    private readonly NopCommerce.ParameterItem _defaultParam = new()
    {
        Categories = "fakeCategories",
    };

    public NopCommerceTest()
    {
        var logger = Mock.Of<ILogger<NopCommerce>>();
        _basicData = new(ServerProvider.Object);
    }

    [Fact]
    public void TestValidate()
    {
        // action
        var result = _basicData.TryValidate(JsonConvert.SerializeObject(new { }), out var results);
        // assert
        ValidatorHelper.HasError(results, "Categories");

        // 验证成功
        // action
        result = _basicData.TryValidate(JsonConvert.SerializeObject(_defaultParam), out results);

        // assert
        Assert.True(result);

    }


    [Fact]
    public async Task TestGetHead()
    {
        // action
        var result = await _basicData.GetHead(JsonConvert.SerializeObject(_defaultParam));

        // assert
        Assert.NotNull(result);
    }

    [Fact]
    public async Task TestGetData()
    {
        // action
        var result = await _basicData.GetData(DefaultDataItem, DefaultRole, JsonConvert.SerializeObject(_defaultParam), Rates);

        // assert
        Assert.NotNull(result);
        Assert.True(6 == result[0].Count);

    }
}