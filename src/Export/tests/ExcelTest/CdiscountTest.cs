namespace ExcelTest;

public class CdiscountTest : Setup
{

    private readonly Cdiscount _basicData;
    private readonly Cdiscount.ParameterItem _defaultParam = new Cdiscount.ParameterItem()
    {

    };

    public CdiscountTest()
    {
        var logger = Mock.Of<ILogger<Cdiscount>>();
        _basicData = new Cdiscount(ServerProvider.Object);
    }


    [Fact]
    public void TestValidate()
    {
        // action
        var result = _basicData.TryValidate(JsonConvert.SerializeObject(new { }), out var results);

        // assert
        Assert.True(result);

    }


    [Fact]
    public async Task TestGetHead()
    {
        // action
        var result = await _basicData.GetHead(JsonConvert.SerializeObject(_defaultParam));

        // assert
        Assert.NotNull(result);
    }

    [Fact]
    public async Task TestGetData()
    {
        // action
        var result = await _basicData.GetData(DefaultDataItem, DefaultRole, JsonConvert.SerializeObject(_defaultParam), Rates);

        // assert
        Assert.NotNull(result);
        Assert.True(2 == result[0].Count);

    }
}