﻿namespace ExcelTest;

public class GlobalSourcesTest : Setup
{
    public readonly GlobalSources.ParameterItem ParamDefault = new()
    {
        ProductCategory = "PrimaryCategoryCode",
        MinimumOrderQuantity = "10",
        UnitOfMeasure = "unit",
        BrandName = "brand",
    };
    private readonly GlobalSources _globalSources;

    public GlobalSourcesTest()
    {
        _globalSources = new (ServerProvider.Object);
    }

    [Fact]
    public void TestValidate()
    {
        // action
        var result = _globalSources.TryValidate(JsonConvert.SerializeObject(new { }), out var results);
        // assert
        ValidatorHelper.HasError(results, "ProductCategory", "UnitOfMeasure");

        // 验证成功
        // action
        result = _globalSources.TryValidate(JsonConvert.SerializeObject(ParamDefault), out results);

        // assert
        Assert.True(result);

    }

    [Fact]
    public async Task TestGetHead()
    {
        IDictionary<string, List<List<string>>> result = await _globalSources.GetHead("");
        Assert.Contains("Products Batch Upload Template", result);
    }

    [Fact]
    public async Task TestGetData()
    {
        // setup

        var data = DefaultDataItem;
        // action
        var result = await _globalSources.GetData(data, DefaultRole, JsonConvert.SerializeObject(ParamDefault), Rates);

        // assert
        Assert.NotNull(result);
        Assert.True(2 == result![0].Count);

        var resultFirst = result[0][0]; // 第一个变体
        var first = data.Products[0];
        var i = 0;
        Assert.Equal(ParamDefault.ProductCategory, resultFirst[i++]);
        Assert.Equal(first.Name, resultFirst[i++]);
    }
}