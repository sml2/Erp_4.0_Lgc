namespace ExcelTest;

public class FecshopTest : Setup
{

    private readonly Fecshop _basicData;
    private readonly Fecshop.ParameterItem _defaultParam = new()
    {
    };

    public FecshopTest()
    {
        var logger = Mock.Of<ILogger<Fecshop>>();
        _basicData = new(ServerProvider.Object);
    }



    [Fact]
    public void TestValidate()
    {
        // action
        var result = _basicData.TryValidate(JsonConvert.SerializeObject(new { }), out var results);

        // assert
        Assert.True(result);

    }


    [Fact]
    public async Task TestGetHead()
    {
        // action
        var result = await _basicData.GetHead(JsonConvert.SerializeObject(_defaultParam));

        // assert
        Assert.NotNull(result);
    }

    [Fact]
    public async Task TestGetData()
    {
        // action
        var result = await _basicData.GetData(DefaultDataItem, DefaultRole, JsonConvert.SerializeObject(_defaultParam), Rates);

        // assert
        Assert.NotNull(result);
        Assert.True(2 == result[0].Count);

    }
}