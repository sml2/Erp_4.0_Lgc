namespace ExcelTest;

public class NOONTest : Setup
{

    private readonly NOON _basicData;
    private readonly NOON.ParameterItem _defaultParam = new()
    {
        brand = "fakeBrand",
        model_number = "fakeModelNumber",
        model_name = "fakeModelName",
        assembly_required = "fakeAssemblyRequired",
        product_type = "fakeProductType",
        product_subtype = "fakeProductSubType",
    };

    public NOONTest()
    {
        var logger = Mock.Of<ILogger<NOON>>();
        _basicData = new(ServerProvider.Object);
    }

    [Fact]
    public void TestValidate()
    {
        // action
        var result = _basicData.TryValidate(JsonConvert.SerializeObject(new { }), out var results);
        // assert
        ValidatorHelper.HasError(results, "brand", "model_number", "model_name", "assembly_required", "product_type", "product_subtype");

        // 验证成功
        // action
        result = _basicData.TryValidate(JsonConvert.SerializeObject(_defaultParam), out results);

        // assert
        Assert.True(result);

    }


    [Fact]
    public async Task TestGetHead()
    {
        // action
        var result = await _basicData.GetHead(JsonConvert.SerializeObject(_defaultParam));

        // assert
        Assert.NotNull(result);
    }

    [Fact]
    public async Task TestGetData()
    {
        // action
        var result = await _basicData.GetData(DefaultDataItem, DefaultRole, JsonConvert.SerializeObject(_defaultParam), Rates);

        // assert
        Assert.NotNull(result);
        Assert.True(3 == result[0].Count);

    }
}