namespace ExcelTest;

public class BasicDataTest : Setup
{

    private readonly BasicData _basicData;

    public BasicDataTest()
    {
        var logger = Mock.Of<ILogger<BasicData>>();
        _basicData = new BasicData(ServerProvider.Object);
    }


    [Fact]
    public void TestValidate()
    {
        // action
        var result = _basicData.TryValidate(JsonConvert.SerializeObject(new { }), out var results);

        // assert
        Assert.True(result);

    }


    [Fact]
    public async Task TestGetHead()
    {
        // action
        var result = await _basicData.GetHead("");

        // assert
        Assert.NotNull(result);
    }

    [Fact]
    public async Task TestGetData()
    {
        // action
        var result = await _basicData.GetData(DefaultDataItem, DefaultRole, "", Rates);

        // assert
        Assert.NotNull(result);
        Assert.True(3 == result[0].Count);

        var firstResult = result[0][1];
        var first = DefaultDataItem.Products[0];
        var i = 0;

        Assert.Equal(first.Name, firstResult[i++]);
        Assert.Equal(DefaultDataItem.Sku, firstResult[i++]);
        Assert.Equal("1.00", firstResult[i++]);
        Assert.Equal(DefaultDataItem.Source, firstResult[i++]);
        Assert.Equal(first.SID, firstResult[i++]);

    }
}