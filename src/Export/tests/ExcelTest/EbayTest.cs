namespace ExcelTest;

public class EbayTest : Setup
{

    private readonly EbayTemplate _basicData;
    private readonly EbayTemplate.ParameterItem _defaultParam = new()
    {
        Action = "fakeAction",
        Category = "fakeCategory",
        ConditionID = "fakeConditionID",
        Format = "fakeFormat",
        Duration = "fakeDuration",
        Location = "fakeLocation",
    };

    public EbayTest()
    {
        var logger = Mock.Of<ILogger<EbayTemplate>>();
        _basicData = new EbayTemplate(ServerProvider.Object);
    }


    [Fact]
    public void TestValidate()
    {
        // action
        var result = _basicData.TryValidate(JsonConvert.SerializeObject(new { }), out var results);
        // assert
        ValidatorHelper.HasError(results, "Action", "Category", "ConditionID", "Format", "Duration", "Location");

        // 验证成功
        // action
        result = _basicData.TryValidate(JsonConvert.SerializeObject(_defaultParam), out results);

        // assert
        Assert.True(result);

    }


    [Fact]
    public async Task TestGetHead()
    {
        // action
        var result = await _basicData.GetHead(JsonConvert.SerializeObject(_defaultParam));

        // assert
        Assert.NotNull(result);
    }

    [Fact]
    public async Task TestGetData()
    {
        // action
        var result = await _basicData.GetData(DefaultDataItem, DefaultRole, JsonConvert.SerializeObject(_defaultParam), Rates);

        // assert
        Assert.NotNull(result);
        Assert.True(1 == result[0].Count);

    }
}