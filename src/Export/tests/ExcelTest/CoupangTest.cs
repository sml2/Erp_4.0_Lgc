namespace ExcelTest;

public class CoupangTest : Setup
{

    private readonly Coupang _basicData;
    private readonly Coupang.ParameterItem _defaultParam = new Coupang.ParameterItem()
    {
        Category = "fakeCategory",
        SalesStartDate = "2022-12-05",
        SalesEndDate = "2022-12-06",
        brand = "fakeBrand",
        manufacturer = "fakeManufacturer",
        ProductNoticeInformation = "fakeProductNoticeInformation",
    };

    public CoupangTest()
    {
        var logger = Mock.Of<ILogger<Coupang>>();
        _basicData = new Coupang(ServerProvider.Object);
    }



    [Fact]
    public void TestValidate()
    {
        // action
        var result = _basicData.TryValidate(JsonConvert.SerializeObject(new { }), out var results);
        // assert
        ValidatorHelper.HasError(results, "Category", "SalesStartDate", "SalesEndDate", "brand", "manufacturer", "ProductNoticeInformation");

        // 验证成功
        // action
        result = _basicData.TryValidate(JsonConvert.SerializeObject(_defaultParam), out results);

        // assert
        Assert.True(result);

    }


    [Fact]
    public async Task TestGetHead()
    {
        // action
        var result = await _basicData.GetHead(JsonConvert.SerializeObject(_defaultParam));

        // assert
        Assert.NotNull(result);
    }

    [Fact]
    public async Task TestGetData()
    {
        // action
        var result = await _basicData.GetData(DefaultDataItem, DefaultRole, JsonConvert.SerializeObject(_defaultParam), Rates);

        // assert
        Assert.NotNull(result);
        Assert.True(2 == result[0].Count);

    }
}