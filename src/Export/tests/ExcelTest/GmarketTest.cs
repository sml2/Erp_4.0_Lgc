namespace ExcelTest;

public class GmarketTest : Setup
{

    private readonly Gmarket _basicData;
    private readonly Gmarket.ParameterItem _defaultParam = new()
    {
        origin_number = "fakeOriginNumber",
        cat_number = "fakeCatNumber",
        series_code = "fakeSeriesCode",
    };

    public GmarketTest()
    {
        var logger = Mock.Of<ILogger<Gmarket>>();
        _basicData = new(ServerProvider.Object);
    }


    [Fact]
    public void TestValidate()
    {
        // action
        var result = _basicData.TryValidate(JsonConvert.SerializeObject(new { }), out var results);
        // assert
        ValidatorHelper.HasError(results, "origin_number", "cat_number", "series_code");

        // 验证成功
        // action
        result = _basicData.TryValidate(JsonConvert.SerializeObject(_defaultParam), out results);

        // assert
        Assert.True(result);

    }


    [Fact]
    public async Task TestGetHead()
    {
        // action
        var result = await _basicData.GetHead(JsonConvert.SerializeObject(_defaultParam));

        // assert
        Assert.NotNull(result);
    }

    [Fact]
    public async Task TestGetData()
    {
        // action
        var result = await _basicData.GetData(DefaultDataItem, DefaultRole, JsonConvert.SerializeObject(_defaultParam), Rates);

        // assert
        Assert.NotNull(result);
        Assert.True(1 == result[0].Count);

    }
}