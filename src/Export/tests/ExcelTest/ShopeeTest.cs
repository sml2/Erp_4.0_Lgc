namespace ExcelTest;

public class ShopeeTest : Setup
{

    private readonly Shopee _basicData;
    private readonly Shopee.ParameterItem _defaultParam = new()
    {
        Category = "fakeCategory",
        Weight = "fakeWeight",
    };

    public ShopeeTest()
    {
        var logger = Mock.Of<ILogger<Shopee>>();
        _basicData = new(ServerProvider.Object);
    }

    [Fact]
    public void TestValidate()
    {
        // action
        var result = _basicData.TryValidate(JsonConvert.SerializeObject(new { }), out var results);
        // assert
        ValidatorHelper.HasError(results, "Category", "Weight");

        // 验证成功
        // action
        result = _basicData.TryValidate(JsonConvert.SerializeObject(_defaultParam), out results);

        // assert
        Assert.True(result);

    }


    [Fact]
    public async Task TestGetHead()
    {
        // action
        var result = await _basicData.GetHead(JsonConvert.SerializeObject(_defaultParam));

        // assert
        Assert.NotNull(result);
    }

    [Fact]
    public async Task TestGetData()
    {
        // action
        var result = await _basicData.GetData(DefaultDataItem, DefaultRole, JsonConvert.SerializeObject(_defaultParam), Rates);

        // assert
        Assert.NotNull(result);
        Assert.Equal(2, result.Count);
        Assert.True(2 == result[1].Count);

    }
}