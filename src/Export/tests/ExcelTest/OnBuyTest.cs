namespace ExcelTest;

public class OnBuyTest : Setup
{

    private readonly OnBuy _basicData;
    private readonly OnBuy.ParameterItem _defaultParam = new()
    {
        Brand = "fakeBrand",
        Category = "fakeCategories",
        Condition = "fakeCondition",
    };

    public OnBuyTest()
    {
        var logger = Mock.Of<ILogger<OnBuy>>();
        _basicData = new(ServerProvider.Object);
    }

    [Fact]
    public void TestValidate()
    {
        // action
        var result = _basicData.TryValidate(JsonConvert.SerializeObject(new { }), out var results);
        // assert
        ValidatorHelper.HasError(results, "Brand", "Category", "Condition");

        // 验证成功
        // action
        result = _basicData.TryValidate(JsonConvert.SerializeObject(_defaultParam), out results);

        // assert
        Assert.True(result);

    }


    [Fact]
    public async Task TestGetHead()
    {
        // action
        var result = await _basicData.GetHead(JsonConvert.SerializeObject(_defaultParam));

        // assert
        Assert.NotNull(result);
    }

    [Fact]
    public async Task TestGetData()
    {
        // action
        var result = await _basicData.GetData(DefaultDataItem, DefaultRole, JsonConvert.SerializeObject(_defaultParam), Rates);

        // assert
        Assert.NotNull(result);
        Assert.True(3 == result[0].Count);

    }
}