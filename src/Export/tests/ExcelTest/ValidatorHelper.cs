﻿
namespace ExcelTest;

public static class ValidatorHelper
{
    /// <summary>
    /// 验证错误列表中存在指定字段的报错
    /// </summary>
    /// <param name="assert"></param>
    /// <param name="results">错误列表</param>
    /// <param name="fields">需要存在的字段</param>
    public static void HasError(List<ValidationResult> results, params string[] fields)
    {
        var invalidFields = results.SelectMany(m => m.MemberNames).Distinct().ToArray();
        foreach (var field in fields)
        {
            Xunit.Assert.True(invalidFields.Contains(field), $"验证错误中不包含`{field}`字段");
        }
    }
}