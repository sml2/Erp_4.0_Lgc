namespace ExcelTest;

public class JapanNetseaTest : Setup
{

    private readonly JapanNetsea _basicData;

    public JapanNetseaTest()
    {
        var logger = Mock.Of<ILogger<JapanNetsea>>();
        _basicData = new(ServerProvider.Object);
    }

    [Fact]
    public void TestValidate()
    {
        // action
        var result = _basicData.TryValidate(JsonConvert.SerializeObject(new { }), out var results);

        // assert
        Assert.True(result);

    }


    [Fact]
    public async Task TestGetHead()
    {
        // action
        var result = await _basicData.GetHead("");

        // assert
        Assert.NotNull(result);
    }

    [Fact]
    public async Task TestGetData()
    {
        // action
        var result = await _basicData.GetData(DefaultDataItem, DefaultRole, "", Rates);

        // assert
        Assert.NotNull(result);
        Assert.Equal(2,result[0].Count);

    }
}