﻿namespace ExcelTest;

public abstract class Setup
{
    protected readonly Mock<IServiceProvider> ServerProvider;

    public Setup()
    {
        var env = new Mock<IWebHostEnvironment>();
        env.Setup(e => e.ContentRootPath).Returns(Environment.CurrentDirectory);
        env.Setup(e => e.EnvironmentName).Returns("Production");

        ServerProvider = new Mock<IServiceProvider>();
        var scopeFactory = new Mock<IServiceScopeFactory>();
        var scope = new Mock<IServiceScope>();
        scope.Setup(s => s.ServiceProvider)
            .Returns(ServerProvider.Object);
        scopeFactory.Setup(s => s.CreateScope())
            .Returns(scope.Object);

        ServerProvider.Setup(s => s.GetService(typeof(IOemProvider)))
            .Returns(new Mock<IOemProvider>().Object);
        ServerProvider.Setup(s => s.GetService(typeof(IServiceScopeFactory)))
            .Returns(scopeFactory.Object);
        ServerProvider.Setup(s => s.GetService(typeof(ProductExportHelper)))
            .Returns(new ProductExportHelper(env.Object, Mock.Of<ILogger<ProductExportHelper>>()));
        ServerProvider.Setup(s => s.GetService(typeof(ILoggerFactory)))
        .Returns(new Mock<ILoggerFactory>().Object);
    }

    public readonly DataItem DefaultDataItem = new()
    {
        PID = 1,
        Name = "fakeName",
        Source = "fakeSource",
        Sku = "fakeSku",
        ShowImage = "/fakeShowImage",
        Quantity = 50,
        Price = new Prices()
        {
            Cost = new Price() { Max = new Money(10000), Min = new Money(10000) },
            Sale = new Price() { Max = new Money(10000), Min = new Money(10000) },
        },
        Description = "fakeDescription",
        BulletPoint = new List<string>() { "fakeBulletPoint" },
        SearchTerms = "fakeSearchTerms",
        Category = "fakeCategory",
        ProductID = "fakeProductId",
        Attribute = new Dictionary<string, string[]>()
        {
            {"颜色", new []{"fakeColor1", "白色"}},
            {"大小", new []{"fakeSize1"}},
        },
        Products = new List<ProductsItem>()
        {
            new ProductsItem()
            {
                Name = "fakeName",
                Sku = "fakeSku1",
                Values = new List<int>() {0, 0},
                ProduceCode = "fakeProduceCode1",
                Price = "10000",
                Cost = "10000",
                BulletPoint = new List<string>() { "fakeBulletPoint1"},
                Description = "fakeDescription1",
                Images = new List<string>() {},
                DescriptionImages = new List<string>() {},
            },
            new ProductsItem()
            {
                Name = "fakeName",
                Sku = "fakeSku2",
                Values = new List<int>() {1, 0},
                ProduceCode = "fakeProduceCode2",
                Price = "10000",
                Cost = "10000",
                Description = "fakeDescription2",
                Images = new List<string>() {},
                DescriptionImages = new List<string>() {},
            }
        },
        ProductImages = new List<string>()
        {
            "fakeImage1"
        }
    };

    public readonly List<Unit> Rates = new List<Unit>()
    {
        new Unit("CNY", "CNY", 10000, DateTime.Now)
    };

    public readonly ExcleRole DefaultRole = new ExcleRole(Languages.Default, true, ExcleRole.KeywordType.Description,
            ExcleRole.SketchType.Description, ExcleRole.DescriptionType.Keyword,
            ExcleRole.MainProImgType.EveryFirstImage);
}