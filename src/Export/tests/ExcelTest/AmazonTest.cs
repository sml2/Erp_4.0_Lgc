using ERP.Export.Templates;
using Microsoft.Extensions.Options;
using AmazonExcel = ERP.Export.V4_Export.ExcelTemplates.Amazon;
namespace ExcelTest;

public class AmazonTest : Setup
{
    public readonly AmazonExcel.ParameterItem ParamDefault = new()
    {
        brand_name = "brand",
        manufacturer = "manufacturer",
        feed_product_type = "feed_product_type",
        is_expiration_dated_product = "No",
        custom_attribute = new List<CommonParameter.Custom_attribute>()
        {
            new CommonParameter.Custom_attribute()
            {
                key = "testKey",
                value = "testValue"
            }
        }
    };
    private readonly AmazonExcel _amazon;

    public AmazonTest()
    {
        var logger = Mock.Of<ILogger<AmazonExcel>>();
        _amazon = new AmazonExcel(ServerProvider.Object, Options.Create(new ExportFeatures()));
    }

    [Fact]
    public void TestValidate()
    {
        // action
        var result = _amazon.TryValidate(JsonConvert.SerializeObject(new { }), out var results);
        // assert
        ValidatorHelper.HasError(results, "brand_name", "manufacturer", "feed_product_type");

        // 验证成功
        // action
        result = _amazon.TryValidate(JsonConvert.SerializeObject(ParamDefault), out results);

        // assert
        Assert.True(result);

    }

    [Fact]
    public async Task TestGetHead()
    {
        var logger = Mock.Of<ILogger<AmazonExcel>>();

        IDictionary<string, List<List<string>>> result = await _amazon.GetHead("");
        Assert.Contains("sheet1", result);
    }

    [Fact]
    public async Task TestGetData()
    {
        // setup

        var data = DefaultDataItem;

        // action
        var result = await _amazon.GetData(data, DefaultRole, JsonConvert.SerializeObject(ParamDefault), Rates);

        // assert
        Assert.NotNull(result);
        Assert.True(3 == result[0].Count);

        var resultFirst = result[0][1]; // 第一个变体
        var first = data.Products[0];
        var i = 0;
        Assert.Equal(ParamDefault.feed_product_type, resultFirst[i++]);
        Assert.Equal("fakeSku1", resultFirst[i++]);
        Assert.Equal(ParamDefault.brand_name, resultFirst[i++]);
        Assert.Equal(first.Name, resultFirst[i++]);
        Assert.Equal(first.ProduceCode, resultFirst[i++]);
        Assert.Equal("", resultFirst[i++]);
        Assert.Equal(ParamDefault.item_type, resultFirst[i++]);
        Assert.Equal(ParamDefault.outer_material_type, resultFirst[i++]);
        i += 4;
        Assert.Equal("fakeColor1", resultFirst[i++]);
        Assert.Equal("fakeColor1", resultFirst[i++]);
        Assert.Equal("fakeSize1", resultFirst[i++]);
        Assert.Equal(ParamDefault.department_name, resultFirst[i++]);
        Assert.Equal("fakeSize1", resultFirst[i++]);
        Assert.Equal("1.00", resultFirst[i++]);
        Assert.Equal(first.Quantity.ToString(), resultFirst[i++]);
        Assert.Equal(first.MainImage, resultFirst[i++]);
        Assert.Equal(ParamDefault.material_composition, resultFirst[i++]);
        i += 9;
        Assert.Equal(ParamDefault.hard_disk_size_unit_of_measure, resultFirst[i++]);
        Assert.Equal(ParamDefault.computer_cpu_speed, resultFirst[i++]);
        Assert.Equal(ParamDefault.computer_cpu_type, resultFirst[i++]);
        Assert.Equal(ParamDefault.hardware_platform, resultFirst[i++]);
        Assert.Equal(ParamDefault.computer_cpu_manufacturer, resultFirst[i++]);
        Assert.Equal(ParamDefault.operating_system, resultFirst[i++]);
        Assert.Equal(ParamDefault.bbfc_rating, resultFirst[i++]);
        Assert.Equal(ParamDefault.binding, resultFirst[i++]);
        Assert.Equal(ParamDefault.metal_type, resultFirst[i++]);
        Assert.Equal(ParamDefault.setting_type, resultFirst[i++]);
        Assert.Equal(ParamDefault.gem_type, resultFirst[i++]);
        Assert.Equal(first.MainImage, resultFirst[i++]);
        i += 8;
        Assert.Equal("child", resultFirst[i++]);
        Assert.Equal("fakeSku", resultFirst[i++]);
        Assert.Equal("Variation", resultFirst[i++]);
        Assert.Equal("ColorSize", resultFirst[i++]);
        Assert.Equal("Update", resultFirst[i++]);
        Assert.Equal(ParamDefault.recommended_browse_nodes, resultFirst[i++]);
        Assert.Equal(first.Description, resultFirst[i++]);
        i++;
        Assert.Equal(ParamDefault.model, resultFirst[i++]);
        i += 5;
        Assert.Equal(ParamDefault.part_number, resultFirst[i++]);
        Assert.Equal(ParamDefault.manufacturer, resultFirst[i++]);
        Assert.Equal("fakeBulletPoint1", resultFirst[i++]);
        i += 4;
        Assert.Equal("fakeDescription1", resultFirst[i++]);
        i += 4;
        i += 9;
        Assert.Equal(ParamDefault.neck_style, resultFirst[i++]);
        i += 8;
        Assert.Equal(ParamDefault.style_name, resultFirst[i++]);
        i++;
        Assert.Equal("", resultFirst[i++]);
        Assert.Equal("", resultFirst[i++]);
        i += 8;
        Assert.Equal(ParamDefault.fufillment_center_id, resultFirst[i++]);
        i += 12;
        Assert.Equal(ParamDefault.country_of_origin, resultFirst[i++]);
        i += 38;
        Assert.Equal(ParamDefault.condition_type, resultFirst[i++]);
        Assert.Equal("1.00", resultFirst[i++]);
        i++;
        Assert.Equal(ParamDefault.fulfillment_latency, resultFirst[i++]);
        Assert.Equal(DateTime.Today.ToString("yyyy/MM/dd"), resultFirst[i++]);
        Assert.Equal("CNY", resultFirst[i++]);
        i += 8;
        Assert.Equal(ParamDefault.number_of_items, resultFirst[i++]);
        i += 4;
        Assert.Equal(ParamDefault.merchant_shipping_group_name, resultFirst[i++]);
        i += 4;
        Assert.Equal(ParamDefault.pattern_type, resultFirst[i++]);
        i += 2;
        Assert.Equal(ParamDefault.unit_count, resultFirst[i++]);
        Assert.Equal(ParamDefault.unit_count_type, resultFirst[i++]);
        Assert.Equal(ParamDefault.season, resultFirst[i++]);
        Assert.Equal(ParamDefault.target_gender, resultFirst[i++]);
        Assert.Equal(ParamDefault.is_adult_product, resultFirst[i++]);
        Assert.Equal("", resultFirst[i++]);
        Assert.Equal("", resultFirst[i++]);
        Assert.Equal("", resultFirst[i++]);
        Assert.Equal("", resultFirst[i++]);
        Assert.Equal("", resultFirst[i++]);
        Assert.Equal("", resultFirst[i++]);
        Assert.Equal(ParamDefault.is_expiration_dated_product, resultFirst[i++]);
        Assert.Equal("fakeSize1", resultFirst[i++]); // i=192
        Assert.Equal(ParamDefault.item_form, resultFirst[i++]);
        Assert.Equal(ParamDefault.net_content_length_unit_of_measure, resultFirst[i++]);
        Assert.Equal(ParamDefault.net_content_count_unit_of_measure, resultFirst[i++]);
        Assert.Equal(ParamDefault.net_content_volume_unit_of_measure, resultFirst[i++]);
        Assert.Equal(ParamDefault.net_content_count, resultFirst[i++]);
        Assert.Equal(ParamDefault.net_content_area, resultFirst[i++]);
        Assert.Equal(ParamDefault.net_content_weight, resultFirst[i++]);
        Assert.Equal(ParamDefault.net_content_weight_unit_of_measure, resultFirst[i++]);
        Assert.Equal(ParamDefault.net_content_area_unit_of_measure, resultFirst[i++]);
        Assert.Equal(ParamDefault.net_content_volume, resultFirst[i++]);
        Assert.Equal(ParamDefault.net_content_length, resultFirst[i++]);

        Assert.Equal("", resultFirst[i++]);
        Assert.Equal("", resultFirst[i++]);
        Assert.Equal("", resultFirst[i++]);
        Assert.Equal("", resultFirst[i++]);
        i += 9;
        Assert.Equal("", resultFirst[i++]);
        Assert.Equal("", resultFirst[i++]);
        Assert.Equal("testValue", resultFirst[i++]);
    }

    [Fact]
    public async Task TestGetData_ExportAmazonB2CEnabled()
    {
        // setup
        var amazon = new AmazonExcel(ServerProvider.Object, Options.Create(new ExportFeatures(){ ExportAmazonB2CEnabled = true }));

        var data = DefaultDataItem;

        // action
        var result = await amazon.GetData(data, DefaultRole, JsonConvert.SerializeObject(ParamDefault), Rates);

        // assert
        Assert.NotNull(result);
        Assert.True(3 == result[0].Count);
        var resultFirst = result[0][1]; // 第一个变体
        var i = 209;
        Assert.Equal("0.95", resultFirst[i++]);
        Assert.Equal("percent", resultFirst[i++]);
        Assert.Equal("5", resultFirst[i++]);
        Assert.Equal("10", resultFirst[i++]);
    }
    
    [Theory]
    [InlineData(true)]
    [InlineData(false)]
    public async Task TestGetData_EnableApparelSize(bool enabled)
    {
        // setup
        var amazon = new AmazonExcel(ServerProvider.Object, Options.Create(new ExportFeatures(){ ExportAmazonB2CEnabled = true }));

        var data = DefaultDataItem;
        var param = ParamDefault;
        param.EnableApparelSize = enabled;
        
        // action
        var result = await amazon.GetData(data, DefaultRole, JsonConvert.SerializeObject(param), Rates);

        // assert
        var resultFirst = result![0][1]; // 第一个变体
        Assert.Equal(enabled ? "fakeSize1" : string.Empty, resultFirst[197]);
    }
    
    [Theory]
    [InlineData(1000)]
    [InlineData(0)]
    public async Task TestGetData_WebsiteShippingWeight(int weight)
    {
        // setup
        var amazon = new AmazonExcel(ServerProvider.Object, Options.Create(new ExportFeatures(){ ExportAmazonB2CEnabled = true }));

        var data = DefaultDataItem;
        var param = ParamDefault;
        param.website_shipping_weight = weight.ToString();
        
        // action
        var result = await amazon.GetData(data, DefaultRole, JsonConvert.SerializeObject(param), Rates);

        // assert
        var resultFirst = result![0][1]; // 第一个变体
        var i = 96;
        Assert.Equal(weight > 0 ? (weight/1000).ToString() : string.Empty, resultFirst[i++]);
        Assert.Equal(weight > 0 ? "KG" : string.Empty, resultFirst[i]);
    }
}