using ERP.Export;
using ERP.Export.Templates;

namespace ExcelTest;

public class ExportUtilsTest
{

    [Fact]
    public void TestRemoveHtmlTag()
    {
        var result = ExportUtils.RemoveHtmlTag(
            "品牌:HENLL<br/>适用对象:粉末、颗粒<br/>电压:380<br/>尺寸:1~50KG<br/>适用行业:新能源、农业、化工<br/>功率:75<br/><br/>", true,
            TemplateType.ZYingExcel);
        var nl = Environment.NewLine;
        // assert
        Assert.Equal($"品牌:HENLL{nl}适用对象:粉末、颗粒{nl}电压:380{nl}尺寸:1~50KG{nl}适用行业:新能源、农业、化工{nl}功率:75{nl}", result);
    }
}