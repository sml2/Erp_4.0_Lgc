﻿namespace ERP.Config
{
    public class ScheduleConfig
    {
        public string ExchangeRate { get; set; } = "* */5 * * *";
        public string Statistics { get; set; } = "55 23 * * *";
    }
}
