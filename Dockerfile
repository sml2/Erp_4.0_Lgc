﻿ARG DOTNET_VERSION=6.0

FROM mcr.microsoft.com/dotnet/aspnet:${DOTNET_VERSION} AS base
ENV TZ="Asia/Shanghai"
RUN sed -i 's/deb.debian.org/mirrors.ustc.edu.cn/g' /etc/apt/sources.list
RUN apt-get update &&  apt-get install -y apt-utils fontconfig curl
RUN rm -rf /var/lib/apt/lists/*
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:${DOTNET_VERSION} AS build
WORKDIR /src
COPY ERP.sln ERP.sln
COPY ERP.csproj ERP.csproj
COPY src/Common/src/Common/Common.csproj src/Common/src/Common/Common.csproj
COPY src/Common/src/Common.Controller/Common.Controller.csproj src/Common/src/Common.Controller/Common.Controller.csproj
COPY src/Distribution/src/Distribution/Distribution.csproj src/Distribution/src/Distribution/Distribution.csproj
COPY src/Distribution/src/Distribution.Abstraction/Distribution.Abstraction.csproj src/Distribution/src/Distribution.Abstraction/Distribution.Abstraction.csproj
COPY src/Warehouse/src/Storage/Storage.csproj src/Warehouse/src/Storage/Storage.csproj
COPY src/Warehouse/src/Storage.Abstraction/Storage.Abstraction.csproj src/Warehouse/src/Storage.Abstraction/Storage.Abstraction.csproj
COPY src/Warehouse/src/Storage.Api/Storage.Api.csproj src/Warehouse/src/Storage.Api/Storage.Api.csproj
COPY src/Waybill/src/Waybill.Abstraction/Waybill.Abstraction.csproj src/Waybill/src/Waybill.Abstraction/Waybill.Abstraction.csproj
COPY src/Order/OrderApi/OrderSDK.csproj src/Order/OrderApi/OrderSDK.csproj
COPY src/ProductSyncWorker/src/FileStorage/FileStorage.csproj src/ProductSyncWorker/src/FileStorage/FileStorage.csproj
COPY src/ProductSyncWorker/src/Product.Abstraction/Product.Abstraction.csproj src/ProductSyncWorker/src/Product.Abstraction/Product.Abstraction.csproj
COPY src/ProductSyncWorker/src/ProductSync.Abstraction/ProductSync.Abstraction.csproj src/ProductSyncWorker/src/ProductSync.Abstraction/ProductSync.Abstraction.csproj
COPY src/Export/src/Export/Export.csproj src/Export/src/Export/Export.csproj
COPY src/Common/src/ERP.Models/ERP.Models.csproj src/Common/src/ERP.Models/ERP.Models.csproj
RUN dotnet restore "ERP.csproj" --disable-parallel --runtime linux-x64
COPY . .
# WORKDIR "/src/"
# RUN dotnet build "ERP.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "ERP.csproj" -c Release -o /app/publish \
    --runtime linux-x64 \
    --no-restore \
    --self-contained false
# /p:PublishTrimmed=true \
# /p:PublishSingleFile=true

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
COPY --from=swr.cn-east-3.myhuaweicloud.com/erp4/erp4_ui:latest /web/src/dist ./wwwroot/
HEALTHCHECK  --interval=10s --timeout=3s \
  CMD curl --fail http://localhost/health || exit 1
ENTRYPOINT ["dotnet", "ERP.dll"]
