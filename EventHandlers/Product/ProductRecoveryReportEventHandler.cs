using ERP.DomainEvents.Product;
using ERP.Services.DB.Statistics;
using ERP.ViewModels.Statistics;
using MediatR;

namespace ERP.EventHandlers.Product;

public class ProductRecoveryReportEventHandler : INotificationHandler<ProductRecoveryEvent>
{
    private readonly Statistic _statisticService;
    private ILogger<ProductRecoveryReportEventHandler> _logger;

    public ProductRecoveryReportEventHandler(Statistic statisticService,
        ILogger<ProductRecoveryReportEventHandler> logger)
    {
        _statisticService = statisticService;
        _logger = logger;
    }

    public async Task Handle(ProductRecoveryEvent notification, CancellationToken cancellationToken)
    {
        var model = notification.Product;
        var isInRecovery = notification.IsInRecovery;
        _logger.LogInformation($"产品移入回收站触发计数 | ID:[${model.ID}]，isInRecovery:[${isInRecovery}]" );

        #region 统计-回收站产品数

        if (isInRecovery)
        {
            await _statisticService.Increase(new DataBelongDto(model), Statistic.NumColumnEnum.ProductRecovery);
        }
        else
        {
            await _statisticService.Decrease(new DataBelongDto(model), Statistic.NumColumnEnum.ProductRecovery);
        }

        #endregion
    }
}