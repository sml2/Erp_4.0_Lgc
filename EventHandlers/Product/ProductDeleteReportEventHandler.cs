using ERP.DomainEvents.Product;
using ERP.Models.Product;
using ERP.Services.DB.Statistics;
using ERP.ViewModels.Statistics;
using MediatR;

namespace ERP.EventHandlers.Product;

public class ProductDeleteReportEventHandler : INotificationHandler<ProductDeleteEvent>
{
    private readonly Statistic _statisticService;
    private ILogger<ProductDeleteReportEventHandler> _logger;

    public ProductDeleteReportEventHandler(Statistic statisticService, ILogger<ProductDeleteReportEventHandler> logger)
    {
        _statisticService = statisticService;
        _logger = logger;
    }
    

    public async Task Handle(ProductDeleteEvent notification, CancellationToken cancellationToken)
    {
        var model = notification.Product;

        _logger.LogInformation($"产品删除触发计数 | ID:[${model.ID}]");

        #region 统计-删除产品数

        if (model.IsRecycle())
        {
            await _statisticService.Decrease(new DataBelongDto(model), Statistic.NumColumnEnum.ProductRecovery);
        }

        await _statisticService.Increase(new DataBelongDto(model), Statistic.NumColumnEnum.ProductDelete);
        await _statisticService.Decrease(new DataBelongDto(model), Statistic.NumColumnEnum.ProductCount);

        #endregion
    }
}