using ERP.DomainEvents;
using ERP.DomainEvents.Users;
using ERP.Enums.Identity;
using ERP.Models.Abstract;
using ERP.Models.Finance;
using ERP.Services.DB.Statistics;
using ERP.Services.DB.Orders;
using ERP.Services.Statistics;
using ERP.ViewModels.Statistics;
using MediatR;

namespace ERP.EventHandlers;

/// <summary>
/// 用户/员工创建成功后上报数据
/// </summary>
public class EmployeeCreatedReportEventHandler : INotificationHandler<EmployeeCreatedEvent>
{
    private readonly Statistic _statisticService;
    private ILogger<EmployeeCreatedReportEventHandler> _logger;

    public EmployeeCreatedReportEventHandler(Statistic statisticService,
        ILogger<EmployeeCreatedReportEventHandler> logger)
    {
        _statisticService = statisticService;
        _logger = logger;
    }


    public async Task Handle(EmployeeCreatedEvent notification, CancellationToken cancellationToken)
    {
        var newUser = notification.NewUser;
        var createUser = notification.CreateUser;

        await _statisticService.IncUserOrEmployeeCount(createUser,newUser);
    }
}