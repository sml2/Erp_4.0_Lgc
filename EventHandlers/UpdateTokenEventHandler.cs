﻿using MediatR;
using OrderSDK.DomainEvents;
using ERP.Services.Stores;

namespace ERP.EventHandlers;

/// <summary>
/// 亚马逊api update token
/// </summary>
public class UpdateTokenEventHandler : INotificationHandler<UpdateTokenEvent>
{

    private readonly StoreService storeService ;


    public UpdateTokenEventHandler( StoreService _storeService)
    {
        storeService = _storeService;
    }
    public async Task Handle(UpdateTokenEvent notification, CancellationToken cancellationToken)
    {              
        await storeService.UpdateAmazonToken(notification.StoreId, notification.Token,notification.Flag);
    }
}