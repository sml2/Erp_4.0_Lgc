using ERP.DomainEvents;
using ERP.Models.Finance;
using ERP.Services.DB.Statistics;
using ERP.Services.DB.Orders;
using ERP.Services.Statistics;
using MediatR;

namespace ERP.EventHandlers;

/// <summary>
/// 订单创建成功后上报数据
/// </summary>
public class StoreCreatedReportEventHandler : INotificationHandler<StoreCreatedEvent>
{
    private readonly Statistic _statisticService;
    private ILogger<StoreCreatedReportEventHandler> _logger;

    public StoreCreatedReportEventHandler(Statistic statisticService, ILogger<StoreCreatedReportEventHandler> logger)
    {
        _statisticService = statisticService;
        _logger = logger;
    }


    public async Task Handle(StoreCreatedEvent notification, CancellationToken cancellationToken)
    {
        var store = notification.Store;

        #region 统计-新增店铺数量

        await _statisticService.IncStoreCount(store);

        #endregion
    }
}