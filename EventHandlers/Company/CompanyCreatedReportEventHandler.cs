using ERP.DomainEvents.Company;
using ERP.Services.DB.Statistics;
using MediatR;

namespace ERP.EventHandlers.Company;

public class CompanyCreatedReportEventHandler : INotificationHandler<CompanyCreatedEvent>
{
    
    private readonly Statistic _statisticService;
    private ILogger<CompanyCreatedReportEventHandler> _logger;

    public CompanyCreatedReportEventHandler(Statistic statisticService, ILogger<CompanyCreatedReportEventHandler> logger)
    {
        _statisticService = statisticService;
        _logger = logger;
    }

    public async Task Handle(CompanyCreatedEvent notification, CancellationToken cancellationToken)
    {
        
        var newCompany = notification.NewCompany;
        var createUser = notification.CreateUser;

        #region 统计-新增公司数量

        await _statisticService.IncCompanyCount(createUser,newCompany);

        #endregion
    }
}