﻿using ERP.DomainEvents;
using ERP.Models.Finance;
using ERP.Services.DB.Statistics;
using ERP.Services.DB.Orders;
using ERP.Services.Statistics;
using MediatR;

namespace ERP.EventHandlers;

/// <summary>
/// 订单创建成功后上报数据
/// </summary>
public class OrderCreatedReportEventHandler : INotificationHandler<OrderCreatedEvent>
{
    private readonly Statistic _statisticService;
    private readonly Order _orderService;
    private  ILogger<OrderCreatedReportEventHandler> _logger;

    public OrderCreatedReportEventHandler(Statistic statisticService, Order orderService,ILogger<OrderCreatedReportEventHandler> logger)
    {
        _statisticService = statisticService;
        _orderService = orderService;
        _logger = logger;
    }
    public async Task Handle(OrderCreatedEvent notification, CancellationToken cancellationToken)
    {
        var order = notification.Order;

        #region 统计-新增订单数量

        await _statisticService.InsertOrCount(order.CompanyID, order.OEMID, order.UserID, order.GroupID, order.StoreId,
            Statistic.NumColumnEnum.OrderInsert,
            Statistic.NumColumnEnum.OrderCount, isDistribution: order.IsDistributionOrder());

        #endregion
        _logger.LogInformation($"OrderNo{order.OrderNo} Increment");
        
        
        #region 金额统计

        var statisticMoney = new StatisticMoney(_statisticService);
        await statisticMoney.InitStatisticWithOrder(order);

        #endregion

        //新增订单汇总sku和goodid
        await _orderService.SumOrderSkuAndAsin(order);
    }
}