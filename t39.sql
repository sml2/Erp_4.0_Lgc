﻿START TRANSACTION;

ALTER TABLE `Eanupc` MODIFY COLUMN `Value` varchar(255) CHARACTER SET utf8mb4 NOT NULL COMMENT '码值';


CREATE UNIQUE INDEX `IX_Eanupc_Value` ON `Eanupc` (`Value`);

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20230410052753_t60', '6.0.5');

COMMIT;

