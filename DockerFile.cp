ARG DOTNET_VERSION=6.0-alpine

FROM mcr.microsoft.com/dotnet/aspnet:${DOTNET_VERSION} AS base
# RUN sed -i 's/deb.debian.org/mirrors.ustc.edu.cn/g' /etc/apt/sources.list
RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.ustc.edu.cn/g' /etc/apk/repositories
RUN apk add --no-cache fontconfig
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:${DOTNET_VERSION} AS build
WORKDIR /src
COPY ERP.sln ERP.sln
COPY ERP.csproj ERP.csproj
COPY src/Common/src/Common/Common.csproj src/Common/src/Common/Common.csproj
COPY src/Common/src/Common.Controller/Common.Controller.csproj src/Common/src/Common.Controller/Common.Controller.csproj
COPY src/Distribution/src/Distribution/Distribution.csproj src/Distribution/src/Distribution/Distribution.csproj
COPY src/Distribution/src/Distribution.Abstraction/Distribution.Abstraction.csproj src/Distribution/src/Distribution.Abstraction/Distribution.Abstraction.csproj
COPY src/Warehouse/src/Storage/Storage.csproj src/Warehouse/src/Storage/Storage.csproj
COPY src/Warehouse/src/Storage.Abstraction/Storage.Abstraction.csproj src/Warehouse/src/Storage.Abstraction/Storage.Abstraction.csproj
COPY src/Warehouse/src/Storage.Api/Storage.Api.csproj src/Warehouse/src/Storage.Api/Storage.Api.csproj
COPY src/Waybill/src/Waybill.Abstraction/Waybill.Abstraction.csproj src/Waybill/src/Waybill.Abstraction/Waybill.Abstraction.csproj
RUN dotnet restore "ERP.csproj" --disable-parallel
COPY . .
# WORKDIR "/src/"
# RUN dotnet build "ERP.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "ERP.csproj" -c Release -o /app/publish \
    # --runtime alpine-x64 \
    --no-restore
# --self-contained true \
# /p:PublishTrimmed=true \
# /p:PublishSingleFile=true

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
COPY --from=erp_ui:latest /web/src/dist ./wwwroot/
ENTRYPOINT ["dotnet", "ERP.dll"]
