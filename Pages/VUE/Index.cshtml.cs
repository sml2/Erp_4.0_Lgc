﻿using ERP.ControllersWithoutToken.Email;
using ERP.Interface;
using ERP.Models.Setting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ERP.Pages.VUE
{
    public class IndexModel : PageModel
    {
        public static volatile object locker = new object();
        public static volatile bool Initialized = false;
        public static string StrVersion = null!;
        public static string StrMainJs = null!;
        public static List<string> LstJss = null!;
        public static List<string> LstCss = null!;
        public string MainJs { get => StrMainJs; }
        public string Version { get => StrVersion; }
        public List<string> Jss { get => LstJss; }
        public List<string> Css { get => LstCss; }
        public string LastUpdateInfo = string.Empty;
        private readonly ILogger<IndexModel> _logger;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public readonly OEM OEM;

        public IndexModel(ILogger<IndexModel> logger, IWebHostEnvironment webHostEnvironment, IOemProvider oemProvider)
        {
            _logger = logger;
            _webHostEnvironment = webHostEnvironment;
            OEM = oemProvider.OEM!;
        }

        public void OnGet()
        {
            //内存屏障+双判断锁，防止指令重排列以及执行效率
            if (!Initialized)
                lock (locker)
                {
                    if (!Initialized)
                    {
                        _logger.LogInformation("Initializion");
                        Initialize();
                        Initialized = true;
                        _logger.LogInformation("Initialized");
                    }
                }
        }
        private async void Initialize()
        {
            var fileProvider = _webHostEnvironment.WebRootFileProvider;
            //无需考虑运行时切换VUE内容的情况，目前docker下打包前端会重启
            const string path = "index.html";
            _logger.LogTrace("path:{path}", path);
            var fileInfo = fileProvider.GetFileInfo(path);
            StrVersion = fileInfo.LastModified.Ticks.ToString();
            _logger.LogTrace("Version:{Version}", Version);
            using var rs = fileInfo.CreateReadStream();
            _logger.LogTrace("ReadStream");
            using StreamReader tr = new(rs);
            _logger.LogTrace("ReadText");
            var content = await tr.ReadToEndAsync();
            _logger.LogTrace("Analyze");
            /*
                <script type="module" crossorigin src="assets/js/main-20526c4c.js"></script>
                <link rel="modulepreload" href="assets/js/vendor-6be8a3b7.js">
                <link rel="modulepreload" href="assets/js/chunk-element-plus-icon-a0cbdbb6.js">
                <link rel="modulepreload" href="assets/js/chunk-element-plus-1feb0ad7.js">
                <link rel="modulepreload" href="assets/js/chunk-vue-a366a318.js">
                <link rel="modulepreload" href="assets/js/chunk-locales-29c1cf15.js">
                <link rel="modulepreload" href="assets/js/chunk-common-76b1a4a8.js">
                <link rel="modulepreload" href="assets/js/chunk-zrender-7bc6e4ec.js">
                <link rel="modulepreload" href="assets/js/chunk-echarts-a53a662c.js">
                <link rel="stylesheet" href="assets/css/vendor-5d71fd4b.css">
                <link rel="stylesheet" href="assets/css/chunk-element-plus-a4fc0697.css">
                <link rel="stylesheet" href="assets/css/chunk-common-bf4d4313.css">
                <link rel="stylesheet" href="assets/css/main-2b51733d.css">
             */
            const string mainJsSpliter = @"crossorigin src=""";
            if (content.Contains(mainJsSpliter))
            {
                StrMainJs = content.Split(mainJsSpliter)[1].Split(@"""")[0];
                _logger.LogTrace("Found StrMainJs:{StrMainJs}", StrMainJs);
            }
            else
            {
                _logger.LogWarning($"NotFound{nameof(mainJsSpliter)}");
            }
            var JsSpliter = @"modulepreload"" href=""";
            if (!content.Contains(JsSpliter))
                JsSpliter = "modulepreload\" crossorigin href=\"";

            if (content.Contains(JsSpliter))
            {
                LstJss = new();
                foreach (var Js in content.Split(JsSpliter).Skip(1))
                {
                    LstJss.Add(Js.Split(@"""")[0]);
                }
                _logger.LogTrace("Found Js Count:{Count}", LstJss.Count);
            }
            else
            {
                _logger.LogWarning($"NotFound{nameof(JsSpliter)}");
            }
            const string CssSpliter = @"stylesheet"" href=""";
            if (content.Contains(CssSpliter))
            {
                LstCss = new();
                foreach (var Js in content.Split(CssSpliter).Skip(1))
                {
                    LstCss.Add(Js.Split(@"""")[0]);
                }
                _logger.LogTrace("Found Css Count:{Count}", LstCss.Count);
            }
            else
            {
                _logger.LogWarning($"NotFound{nameof(CssSpliter)}");
            }
            _logger.LogTrace("Analyzed");
        }
    }
}