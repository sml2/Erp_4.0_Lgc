﻿START TRANSACTION;

ALTER TABLE `ProductUpload` DROP COLUMN `Pid`;

ALTER TABLE `ProductUpload` ADD `LanguageSign` longtext CHARACTER SET utf8mb4 NOT NULL;

ALTER TABLE `ProductItem` MODIFY COLUMN `Quantity` int NOT NULL DEFAULT 0 COMMENT '库存';

UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-03-14 17:11:31', `PasswordHash` = 'AQAAAAEAACcQAAAAEHN9A1m0DHJmQEm5ZgsKnH/wxAGEru5cCKOipONqHYMcXDUj6nrphO8oQzvyGoMq6g==', `UpdatedAt` = TIMESTAMP '2023-03-14 17:11:31'
WHERE `Id` = 444445;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-03-14 17:11:31', `PasswordHash` = 'AQAAAAEAACcQAAAAEEMQM6mUrcI+QyPx1llwYfHd01Kdx0il+cJV0nzGjahdXxGCzCxWUYlSgXeHHlVA4Q==', `UpdatedAt` = TIMESTAMP '2023-03-14 17:11:31'
WHERE `Id` = 444446;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-03-14 17:11:31', `PasswordHash` = 'AQAAAAEAACcQAAAAEEe3m7tj/+SV113b6yMGhBa/2wR0MH/x3vj6hF1hVHt3XE7xUwuRE1yy+jfdUuqQig==', `UpdatedAt` = TIMESTAMP '2023-03-14 17:11:31'
WHERE `Id` = 444447;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-03-14 17:11:31', `PasswordHash` = 'AQAAAAEAACcQAAAAEDerc1bbz8SJNjekFkRSShh5/ozGXui7UhGMDpTwq0DpyVi87LX9JtDDfEMFTFh1BQ==', `UpdatedAt` = TIMESTAMP '2023-03-14 17:11:31'
WHERE `Id` = 444448;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-03-14 17:11:31', `PasswordHash` = 'AQAAAAEAACcQAAAAEIbltl27RhPxLe0XNlcNKQ2WWn+Yz/710R7MJ7AJt7JoD3MoNuGZ5DY3HLSpdk8N8A==', `UpdatedAt` = TIMESTAMP '2023-03-14 17:11:31'
WHERE `Id` = 444449;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-03-14 17:11:31', `PasswordHash` = 'AQAAAAEAACcQAAAAEEhvdGmpSh2vSk8pneOb5PJ4rpABRZ/k2IwH5LQGuHONpzoettMtgVtH3Unen10d4g==', `UpdatedAt` = TIMESTAMP '2023-03-14 17:11:31'
WHERE `Id` = 444450;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-03-14 17:11:31', `PasswordHash` = 'AQAAAAEAACcQAAAAEME19RowmpZlQLEshR4JY23fZVtN1MEIvOtWY0Gkuzb80DDH4ZilqHWmQrBIds7BBg==', `UpdatedAt` = TIMESTAMP '2023-03-14 17:11:31'
WHERE `Id` = 444451;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-03-14 17:11:31', `PasswordHash` = 'AQAAAAEAACcQAAAAEHuv83mNzSUkPzSxKtfzWhXPxuW6hYa3e3o0WM2epQok5At0Imh7dO/BaGqZ9H+HnQ==', `UpdatedAt` = TIMESTAMP '2023-03-14 17:11:31'
WHERE `Id` = 444452;
SELECT ROW_COUNT();


UPDATE `UserGroup` SET `Sort` = -1637300280
WHERE `ID` = 1;
SELECT ROW_COUNT();


UPDATE `UserValidities` SET `CreatedAt` = TIMESTAMP '2023-03-14 17:11:31', `UpdatedAt` = TIMESTAMP '2023-03-14 17:11:31'
WHERE `ID` = 1;
SELECT ROW_COUNT();


UPDATE `UserValidities` SET `CreatedAt` = TIMESTAMP '2023-03-14 17:11:31', `UpdatedAt` = TIMESTAMP '2023-03-14 17:11:31'
WHERE `ID` = 2;
SELECT ROW_COUNT();


UPDATE `UserValidities` SET `CreatedAt` = TIMESTAMP '2023-03-14 17:11:31', `UpdatedAt` = TIMESTAMP '2023-03-14 17:11:31'
WHERE `ID` = 3;
SELECT ROW_COUNT();


INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20230314091135_t52', '6.0.5');

COMMIT;

START TRANSACTION;

ALTER TABLE `ProductUpload` DROP COLUMN `LanguageSign`;

ALTER TABLE `ProductUpload` ADD `languages` bigint NOT NULL DEFAULT 0;

UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-03-17 18:47:44', `PasswordHash` = 'AQAAAAEAACcQAAAAEDlmd/tkPlGUJhQ6qH1ha7vOxz/UBDOiGV3zcXrWLM8ylUl0m5IcZdDJAC2Q4v4arg==', `UpdatedAt` = TIMESTAMP '2023-03-17 18:47:44'
WHERE `Id` = 444445;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-03-17 18:47:44', `PasswordHash` = 'AQAAAAEAACcQAAAAENvY5Ah1WAG2Hjk6v6GdzsIkcgV7/3etNRXAAvZiBCcrksItiVzHXD/oHPuE67FhlQ==', `UpdatedAt` = TIMESTAMP '2023-03-17 18:47:44'
WHERE `Id` = 444446;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-03-17 18:47:44', `PasswordHash` = 'AQAAAAEAACcQAAAAEL0u7CIzdKW48Ln0qEKSErCXE1o/zfV7yc6MJM22TmxBkrs2QodjRj43nK1av1EaWg==', `UpdatedAt` = TIMESTAMP '2023-03-17 18:47:44'
WHERE `Id` = 444447;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-03-17 18:47:44', `PasswordHash` = 'AQAAAAEAACcQAAAAEM9ADkqhMFjJ8FTa+l1nrfiKpcZRscbnyKXUw67trrUBqUlcOnIKpCU94YvRUXD9eQ==', `UpdatedAt` = TIMESTAMP '2023-03-17 18:47:44'
WHERE `Id` = 444448;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-03-17 18:47:44', `PasswordHash` = 'AQAAAAEAACcQAAAAEMGjTVBrHQrYrofL9tIDvj0rY/18GvKL3szlNHk+tQrX0zvcPRm4Y41IHglINGoPTA==', `UpdatedAt` = TIMESTAMP '2023-03-17 18:47:44'
WHERE `Id` = 444449;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-03-17 18:47:44', `PasswordHash` = 'AQAAAAEAACcQAAAAEHKUCHlENkdCZwOWGD0g1x8+oqQ/tyvXb37NWKhc9cJP/kjfLbIR22KWkOi8aW6w1Q==', `UpdatedAt` = TIMESTAMP '2023-03-17 18:47:44'
WHERE `Id` = 444450;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-03-17 18:47:44', `PasswordHash` = 'AQAAAAEAACcQAAAAEB6nbEniKC9EXXhNfqoCq49un3AHXVPZf/f/qEI+QtVrKJrOtGivuigS39zcAAfm8Q==', `UpdatedAt` = TIMESTAMP '2023-03-17 18:47:44'
WHERE `Id` = 444451;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-03-17 18:47:44', `PasswordHash` = 'AQAAAAEAACcQAAAAEFjWW8hQtTLhOPQAPuOpX76O1Z0vg5/DLWhY7q6VWOo4ag2mDgJpSviwkH4XUcj4uQ==', `UpdatedAt` = TIMESTAMP '2023-03-17 18:47:44'
WHERE `Id` = 444452;
SELECT ROW_COUNT();


UPDATE `UserValidities` SET `CreatedAt` = TIMESTAMP '2023-03-17 18:47:44', `UpdatedAt` = TIMESTAMP '2023-03-17 18:47:44'
WHERE `ID` = 1;
SELECT ROW_COUNT();


UPDATE `UserValidities` SET `CreatedAt` = TIMESTAMP '2023-03-17 18:47:44', `UpdatedAt` = TIMESTAMP '2023-03-17 18:47:44'
WHERE `ID` = 2;
SELECT ROW_COUNT();


UPDATE `UserValidities` SET `CreatedAt` = TIMESTAMP '2023-03-17 18:47:44', `UpdatedAt` = TIMESTAMP '2023-03-17 18:47:44'
WHERE `ID` = 3;
SELECT ROW_COUNT();


INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20230317104749_t53', '6.0.5');

COMMIT;

