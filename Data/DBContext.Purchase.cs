﻿
using Microsoft.EntityFrameworkCore;
namespace ERP.Data;
using Models.DB.Purchase;

public partial class DBContext //采购
{
    public DbSet<Purchase> Purchase { get; set; } = null!;
    public DbSet<PurchaseLog> PurchaseLog { get; set; } = null!;
    public DbSet<PurchasePick> PurchasePick { get; set; } = null!;
    public DbSet<PurchasingChannels> PurchasingChannels { get; set; } = null!;
    public DbSet<Supplier> Supplier { get; set; } = null!;
    public DbSet<Cart> Carts { get; set; } = null!;
}
