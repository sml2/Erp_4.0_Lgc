using ERP.Models.DB.Packages;
using ERP.Models.DB.Setting;
using Microsoft.EntityFrameworkCore;

namespace ERP.Data;

using ERP.Models.Users;
using Models.DB.Identity;
using Models.DB.Users;
using Models.IntegralBalance;
using Models.Setting;

public partial class DBContext //用户 设置
{
    public DbSet<DataBase> DataBase { get; set; } = null!;
    public DbSet<FileBucket> FileBucket { get; set; } = null!;

    public DbSet<OEM> OEM { get; set; } = null!;

    // public DbSet<OemDomainModel> OemDomain { get; set; }
    public DbSet<Redis> Redis { get; set; } = null!;
    public DbSet<Theme> Theme { get; set; } = null!;
    public DbSet<Group> UserGroup { get; set; } = null!;
    public DbSet<Company> Company { get; set; } = null!;
    public DbSet<User> User { get; set; } = null!;
    public DbSet<UserOperateLog> UserOperateLog { get; set; } = null!;
    public DbSet<UserEmail> UserEmails { get; set; } = null!;
    public DbSet<UserValidity> UserValidities { get; set; } = null!;
    public DbSet<Unit> Units { get; set; } = null!;
    public DbSet<CompanyOem> CompanyOem { get; set; } = null!;
    public DbSet<IntegralBill> IntegralBills { get; set; } = null!;
    public DbSet<BalanceBill> BalanceBills { get; set; } = null!;
    public DbSet<UserOnlineModel> UserOnline { get; set; } = null!;
    public DbSet<LoginLogModel> LoginLog { get; set; } = null!;
    public DbSet<CompanyResourcePackagesModel> CompanyResourcePackages { get; set; } = null!;
    public DbSet<CompanyResourceConsumptionLogModel> CompanyResourceConsumptionLog { get; set; } = null!;
    public DbSet<PackagesModel> Packages { get; set; } = null!;
    public DbSet<MattingPackageItemModel> MattingPackageItem { get; set; } = null!;
    public DbSet<TranslationPackageItemModel> TranslationPackageItem { get; set; } = null!;
    
    public DbSet<PackageItemsModel> PackageItems { get; set; } = null!;
}