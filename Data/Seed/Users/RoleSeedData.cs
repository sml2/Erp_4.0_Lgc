﻿namespace ERP.Data.Seed.Users;
using ENUM = Enums.ID.Role;
using Model = Models.DB.Identity.Role;
public class RoleSeedData : AbsSeed<Model, RoleSeedData>
{
    public override Model[] Create()
    {

        var t = Enum.GetValues<ENUM>().Where(i => i != 0).Select(r => new Model(r)).ToArray();
        t.ForEach(r =>
        {
            r.CreatedAt = Now;
            r.UpdatedAt = Now;
            r.ConcurrencyStamp = $"ERP4[{r.Id}]";
        });
        return t;
    }
}