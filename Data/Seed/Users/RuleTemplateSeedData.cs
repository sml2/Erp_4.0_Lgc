﻿using ERP.Enums.Identity;
using ERP.Enums.Rule.Config;
using ERP.Enums.Rule.Menu;
using ERP.Models.Abstract;
using ERP.Models.DB.Rule;
using ERP.Models.DB.Users;
using ERP.Services.Identity;

namespace ERP.Data.Seed.Users;
using Enums.ID;
using ROLE = Role;
using ENUM = Enums.Identity.User;
using Model = RuleTemplate;
using Data;
using Permission = Enums.Rule.Permission;

public class RuleTemplateSeedData : AbsSeed<Model, RuleTemplateSeedData>
{
    public override Model[] Create()
    {
        var ruleTemplate = new Model()
        {
            ID = 1, CompanyID = (int)Company.XiaoMi, OEMID = (int)OEM.XiaoMi, Name = "Empty",
            State = BaseModel.StateEnum.Open,

            ElectronicBusiness= Permission.ElectronicBusiness.SFC,
            Logistics = Permission.Logistics.SFC,
            Export = Permission.Export.SFC,
            DistributionMenu = Enums.Rule.Menu.Distribution.Module,
            Distribution = new(),
            FinanceMenu = Enums.Rule.Menu.Finance.Module,
            Finance =new(),
            MessageMenu = Enums.Rule.Menu.Message.Module,
            Message = new(),
            OrderMenu = Enums.Rule.Menu.Order.Module,
            Order = new(),
            ProductMenu = Enums.Rule.Menu.Product.Module,
            Product = new(),
            ProductAbout = new(),
            SettingMenu = Enums.Rule.Menu.Setting.Module,
            Setting = new(),
            StorageMenu = Enums.Rule.Menu.Storage.Module,
            Storage = new(),
            StoreMenu = Enums.Rule.Menu.Store.Module,
            Store = new(),
            UserMenu = Enums.Rule.Menu.User.Module,
            User = new(),
            WaybillMenu = Enums.Rule.Menu.Waybill.Module,
            Waybill = new(),
            PurchaseMenu = Enums.Rule.Menu.Purchase.Module,
            Purchase = new Config<Enums.Rule.Config.Purchase>(),
            OperationsMenu = Enums.Rule.Menu.Operations.Module,
            Operations = new Config<Enums.Rule.Config.Operations>(),
            B2CMenu = Enums.Rule.Menu.B2C.Module,
            B2C = new Config<Enums.Rule.Config.B2C>(),
            ToolsMenu = Enums.Rule.Menu.Tools.Module,
            Tools = new Config<Enums.Rule.Config.Tools>(),
            //Modules = long.MaxValue,
            //Distribution = Enums.Rule.Menu.Distribution.Company,
            //DistributionConfig = long.MaxValue,
            //Purchase = PurchaseManager.Module,
            //Product = ProductManager.Module,
            //ProductConfig = long.MaxValue,
            //Finance = FinanceManager.Module,
            //JobMenu = Job.Module,
            //Job = new Config<Enums.Rule.Config.Job>(),
            //Order = Enums.Rule.Menu.Order.Module,
            //Setting = SettingManager.Module,
            //Storage = StorageManager.Module,
            //Store = StoreManager.Module,
            //User = Enums.Rule.Menu.User.Module,
            //Waybill = WayBillManager.Module,
            //Message = long.MaxValue,
            CreatedAt = Now,
            UpdatedAt = Now,
        };
        return new[] { ruleTemplate };
    }
}