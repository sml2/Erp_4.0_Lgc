﻿using Model = ERP.Models.DB.Identity.UserClaim;

namespace ERP.Data.Seed.Users;
using UIDS = Enums.Identity.User;
using Enums.Rule.Menu;
using Authorization;
using Data;

public class UserClaim : AbsSeed<Model, UserClaim>
{
    public override Model[] Create()
    {
        return Array.Empty<Model>();
        //return Enum.GetValues<UIDS>().SelectMany(u =>
        //{
        //    if (u == UIDS.None)
        //        return Array.Empty<Model>();
        //    Model[] t =
        //    {
        //        new(Indexer,u,new ClaimMenu(Distribution.Module)),
        //        new(Indexer,u,new ClaimMenu(Finance.Module)),
        //        new(Indexer,u,new ClaimMenu<Order>()),
        //        new(Indexer,u,new ClaimMenu(Product.Module)),
        //        new(Indexer,u,new ClaimMenu(Purchase.Module)),
        //        new(Indexer,u,new ClaimMenu(Storage.Module)),
        //        new(Indexer,u,new ClaimMenu(Store.Module)),
        //        new(Indexer,u,new ClaimMenu<User>()),
        //        new(Indexer,u,new ClaimMenu(Setting.Module)),
        //        new(Indexer,u,new ClaimMenu(Waybill.Module)),
        //        new(Indexer,u,new ClaimMenu(Message.Module)),
        //    };
        //    return t;
        //}).ToArray();
    }
}