﻿namespace ERP.Data.Seed.Users;

using ERP.Enums.ID;
using ENUM = Enums.Identity.User;
using Model = Models.DB.Identity.UserRole;
public class UserRoleSeedData : AbsSeed<Model, UserRoleSeedData>
{
    public override Model[] Create() => Enum.GetValues<ENUM>().Where(i => i != ENUM.BASE).Select(r => new Model(r, r == ENUM.SU ? Role.SU : Role.DEV)).ToArray();
}