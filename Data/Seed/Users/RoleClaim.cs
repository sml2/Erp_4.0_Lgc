﻿using Model = ERP.Models.DB.Identity.RoleClaim;

namespace ERP.Data.Seed.Users;
using ENUM = Enums.Identity.Role;
using Enums.Rule.Menu;
using Authorization;
using Data;
using ERP.Interface.Rule;

public class RoleClaim : AbsSeed<Model, RoleClaim>
{
    public override Model[] Create() => new[] { ENUM.DEV, ENUM.SU }.
            SelectMany(r => ServiceProvider.GetRequiredService<IManager>().GetSeed(r).Select(kv => new Model(Indexer, r, new(kv.Key, kv.Value)))).ToArray();
}