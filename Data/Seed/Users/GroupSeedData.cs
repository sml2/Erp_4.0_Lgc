﻿using ERP.Models.DB.Users;

namespace ERP.Data.Seed.Users;
using ENUM = Enums.ID.Group;
using Enums.ID;
public class GroupSeedData : AbsSeed<Models.DB.Users.Group, GroupSeedData>
{
    public override Models.DB.Users.Group[] Create()
    {
        return new Models.DB.Users.Group[]
        {
            new (ENUM.DEFAUTE,Company.XiaoMi,OEM.XiaoMi){ CreatedAt = Now, UpdatedAt = Now,}
        };
    }
}