﻿using ERP.Enums.Identity;
using ERP.Models.DB.Identity;
using ERP.Services.Identity;

namespace ERP.Data.Seed.Users;

using Enums.ID;
using ROLE = Enums.Identity.Role;
using ENUM = Enums.Identity.User;
using Model = Models.DB.Identity.User;
using CompanyEnum = Enums.ID.Company;

public class UserSeedData : AbsSeed<Model, UserSeedData>
{
    public override Model[] Create()
    {
        // var now = DateTime.Now;
        // var now = DateTime.Now;
        var now = Convert.ToDateTime("2023-04-27 00:00:00");
        // var passwordHasher = new PasswordHasher();
        const string securityStamp = "W7TU6MEYD6PIF7BS6QCMS2XUDJAHKNXJ";

        var erpPasswordHash = "AQAAAAEAACcQAAAAEBU+9xt/rz0U1u71lPrcNXU9Pwqf6G+nvL+RTstNvQGub4Ret/dU7gQ5UFR6+5bHDw==";
        var simplePasswordHash = "AQAAAAEAACcQAAAAEO9fU1rNX0BzO5nZfTKLg2dQ5tCJd09/uHuNga4IvyqugBT8YWxJz1LuhnrNYJXDSg==";
        
        Model erp = new(ENUM.SU, Group.DEFAUTE, Company.XiaoMi, OEM.MiJingTong, Company.XiaoMi, "10086")
        {
            SecurityStamp = securityStamp, Role = ROLE.DEV, Level = 1, Pid = 0, CreateCompanyId = 0,
            Pids = Newtonsoft.Json.JsonConvert.SerializeObject(Array.Empty<int>()), Avatar = "/head_portrait.png",
            CreatedAt = now, UpdatedAt = now, FormRuleTemplateID = 1
        };
        // erp.PasswordHash = passwordHasher.HashPassword(erp, "erp456789");
        erp.PasswordHash = erpPasswordHash;

        Model xm = new(ENUM.XIAOMI, Group.DEFAUTE, Company.XiaoMi, OEM.MiJingTong, Company.XiaoMi, "10086")
        {
            SecurityStamp = securityStamp, Role = ROLE.ADMIN, Level = 1, Pid = 0, CreateCompanyId = 0,
            Pids = Newtonsoft.Json.JsonConvert.SerializeObject(Array.Empty<int>()),
            Avatar = "/head_portrait.png",
            CreatedAt = now, UpdatedAt = now, FormRuleTemplateID = 1,
        };
        // xm.PasswordHash = passwordHasher.HashPassword(xm, "123456");
        xm.PasswordHash = simplePasswordHash;

        Model lyq = new(ENUM.LYQ, Group.DEFAUTE, Company.XiaoMi, OEM.MiJingTong, Company.XiaoMi, "10086")
        {
            SecurityStamp = securityStamp, Role = ROLE.DEV, Level = 1, Pid = (int) ENUM.XIAOMI, CreateCompanyId = (int)Company.XiaoMi,
            Pids = Newtonsoft.Json.JsonConvert.SerializeObject(new List<int>(){(int) ENUM.XIAOMI}),
            Avatar = "/head_portrait.png",
            CreatedAt = now, UpdatedAt = now, FormRuleTemplateID = 1
        };
        // lyq.PasswordHash = passwordHasher.HashPassword(lyq, "123456");
        lyq.PasswordHash = simplePasswordHash;

        Model ady = new(ENUM.ADY, Group.DEFAUTE, Company.XiaoMi, OEM.MiJingTong, Company.XiaoMi, "10086")
        {
            SecurityStamp = securityStamp, Role = ROLE.DEV, Level = 1, Pid = (int) ENUM.XIAOMI, CreateCompanyId = (int)Company.XiaoMi,
            Pids = Newtonsoft.Json.JsonConvert.SerializeObject(new List<int>(){(int) ENUM.XIAOMI}),
            Avatar = "/head_portrait.png",
            CreatedAt = now, UpdatedAt = now, FormRuleTemplateID = 1
        };
        // ady.PasswordHash = passwordHasher.HashPassword(ady, "123456");
        ady.PasswordHash = simplePasswordHash;

        Model sy = new(ENUM.SYY, Group.DEFAUTE, Company.XiaoMi, OEM.MiJingTong, Company.XiaoMi, "18678799126")
        {
            SecurityStamp = securityStamp, Role = ROLE.DEV, Level = 1, Pid = (int) ENUM.XIAOMI, CreateCompanyId = (int)Company.XiaoMi,
            Pids = Newtonsoft.Json.JsonConvert.SerializeObject(new List<int>(){(int) ENUM.XIAOMI}),
            Avatar = "/head_portrait.png",
            CreatedAt = now, UpdatedAt = now, FormRuleTemplateID = 1
        };
        // sy.PasswordHash = passwordHasher.HashPassword(sy, "123456");
        sy.PasswordHash = simplePasswordHash;

        Model xqj = new(ENUM.XQJ, Group.DEFAUTE, Company.XiaoMi, OEM.MiJingTong, Company.XiaoMi, "17302517072")
        {
            SecurityStamp = securityStamp, Role = ROLE.DEV, Level = 1, Pid = (int) ENUM.XIAOMI, CreateCompanyId = (int)Company.XiaoMi,
            Pids = Newtonsoft.Json.JsonConvert.SerializeObject(new List<int>(){(int) ENUM.XIAOMI}),
            Avatar = "/head_portrait.png",
            CreatedAt = now, UpdatedAt = now, FormRuleTemplateID = 1
        };
        // xqj.PasswordHash = passwordHasher.HashPassword(xqj, "123456");
        xqj.PasswordHash = simplePasswordHash;

        // Model sunadmin = new(ENUM.SunAdmin, Group.DEFAUTE, Company.YongHe, OEM.YongHe, Company.YongHe, "10000")
        // {
        //     SecurityStamp = securityStamp, Role = ROLE.ADMIN, Level = 1, Pid = (int) ENUM.XIAOMI, CreateCompanyId = (int)Company.XiaoMi,
        //     Pids = Newtonsoft.Json.JsonConvert.SerializeObject(new List<int>(){(int) ENUM.XIAOMI}),
        //     Avatar = "/head_portrait.png",
        //     CreatedAt = now, UpdatedAt = now, FormRuleTemplateID = 1
        // };
        // sunadmin.PasswordHash = passwordHasher.HashPassword(sunadmin, "123456");

        // Model fn = new(ENUM.FengNiao, Group.DEFAUTE, Company.FengNiao, OEM.FengNiao, Company.FengNiao, "10000")
        // {
        //     SecurityStamp = securityStamp, Role = ROLE.ADMIN, Level = 1, Pid = (int) ENUM.XIAOMI, CreateCompanyId = (int)Company.XiaoMi,
        //     Pids = Newtonsoft.Json.JsonConvert.SerializeObject(new List<int>(){(int) ENUM.XIAOMI}),
        //     Avatar = "/head_portrait.png",
        //     CreatedAt = now, UpdatedAt = now, FormRuleTemplateID = 1
        // };
        // fn.PasswordHash = passwordHasher.HashPassword(fn, "123456");
        //, sunadmin, fn
        var Erp4 = new[] {erp, xm, lyq, ady, sy, xqj};
        var Erp2 = UserSeedDataErp2.Create();
        return Erp4.Concat(Erp2.Select(u2=>u2.User)).ToArray();
    }
}