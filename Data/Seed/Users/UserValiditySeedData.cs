﻿using ERP.Models.DB.Users;
using Microsoft.AspNetCore.Razor.Language;

namespace ERP.Data.Seed.Users;

using UserValidityEnum = ERP.Enums.ID.UserValidity;
using CompanyEnum = Enums.ID.Company;
using OemEnum = Enums.ID.OEM;
using userEnum = Enums.Identity.User;

public class UserValiditySeedData : AbsSeed<UserValidity, UserValiditySeedData>
{
    public override UserValidity[] Create()
    {
        var now = Convert.ToDateTime("2023-04-27 00:00:00");
        return new[]
        {
            new UserValidity()
            {
                ID = (int) UserValidityEnum.FiveYear,
                CompanyID = (int) CompanyEnum.XiaoMi,
                OEMID = (int) OemEnum.XiaoMi,
                Validity = now.AddYears(5),
                CreatedAt = now,
                UpdatedAt = now,
            },

            // new UserValidity()
            // {
            //     ID = (int) UserValidityEnum.YhOneYear,
            //     CompanyID = (int) CompanyEnum.YongHe,
            //     OEMID = (int) OemEnum.YongHe,
            //     Validity = DateTime.MinValue
            // },
            // new UserValidity()
            // {
            //     ID = (int) UserValidityEnum.FnOneYear,
            //     CompanyID = (int) CompanyEnum.FengNiao,
            //     OEMID = (int) OemEnum.FengNiao,
            //     Validity = DateTime.MinValue
            // }
        };
    }
}