﻿using static ERP.Models.Abstract.BaseModel;
namespace ERP.Data.Seed;
using Model = Models.Setting.Unit;

public class Unit : AbsSeed<Model, Unit>
{
    public override Model[] Create()
    {
        Model[] t =
        {
        new( 1, "美元","USD",70721.35785m,Now),
        new( 2, "欧元","EUR",79402.89027m,Now),
        new( 3, "人民币","CNY",10000m,Now),
        new( 4, "日元","JPY",662.734442m,Now),
        new( 5, "港币","HKD",9129.085265m,Now),
        new( 6, "韩元","KRW",58.560695m,Now),
        new( 7, "卢布","RUB",1020.418575m,Now),
        new( 8, "英镑","GBP",87558.00718m,Now),
        new( 9, "新加坡元","SGD",50864.6999m,Now),
        new( 10, "新台币","TWD",2392.516209m,Now),
        new( 11, "加拿大元","CAD",52246.60397m,Now),
        new( 12, "澳大利亚元","AUD",49751.24378m,Now),
        new( 13, "巴西雷亚尔","BRL",13200.27457m,Now),
        new( 14, "印度卢比","INR",928.737938m,Now),
        new( 15, "瑞士法郎","CHF",74738.41555m,Now),
        new( 16, "泰国铢","THB",2283.939338m,Now),
        new( 17, "澳门元","MOP",8881.073544m,Now),
        new( 18, "新西兰元","NZD",46511.62791m,Now),
        new( 19, "南非兰特","ZAR",4081.965874m,Now),
        new( 20, "瑞典克朗","SEK",7513.148009m,Now),
        new( 21, "印尼卢比","IDR",4.966229m,Now),
        new( 22, "墨西哥比索","MXN",3121.770918m,Now),
        new( 23, "阿根廷比索","ARS",1017.686371m,Now),
        new( 24, "林吉特","MYR",16549.98924m,Now),
        new( 25, "阿曼里亚尔","OMR",184128.1532m,Now),
        new( 26, "埃及镑","EGP",4395.044148m,Now),
        new( 27, "爱尔兰镑","IEP",10000m,Now),
        new( 28, "奥地利先令","ATS",10000m,Now),
        new( 29, "巴基斯坦卢比","PKR",427.96256m,Now),
        new( 30, "巴拉圭瓜拉尼","PYG",10.565693m,Now),
        new( 31, "巴林第纳尔","BHD",188040.6168m,Now),
        new( 32, "巴拿马巴尔博亚","PAB",70891.81908m,Now),
        new( 33, "百慕大元","BMD",70891.81908m,Now),
        new( 34, "保加利亚列弗","BGN",40675.20846m,Now),
        new( 35, "比利时法郎","BEF",10000m,Now),
        new( 36, "冰岛克朗","ISK",518.405736m,Now),
        new( 37, "波兰兹罗提","PLN",17795.49418m,Now),
        new( 38, "玻利维亚诺","BOB",10334.1015m,Now),
        new( 39, "博茨瓦纳普拉","BWP",6013.952369m,Now),
        new( 40, "丹麦克朗","DKK",10658.59456m,Now),
        new( 41, "德国马克","DEM",10000m,Now),
        new( 42, "法国法郎","FRF",10000m,Now),
        new( 43, "菲律宾比索","PHP",1413.8872m,Now),
        new( 44, "芬兰马克","FIM",10000m,Now),
        new( 45, "哥伦比亚比索","COP",18.936649m,Now),
        new( 46, "古巴比索","CUP",2835.737093m,Now),
        new( 47, "哈萨克斯坦坚戈","KZT",175.098168m,Now),
        new( 48, "荷兰盾","ANG",39737.73098m,Now),
        new( 49, "加纳塞地","GHC",10000m,Now),
        new( 50, "捷克克朗","CZK",2977.280373m,Now),
        new( 51, "津巴布韦元","ZWD",10000m,Now),
        new( 52, "卡塔尔里亚尔","QAR",19475.71378m,Now),
        new( 53, "科威特第纳尔","KWD",230467.8497m,Now),
        new( 54, "克罗地亚库纳","HRK",10645.20593m,Now),
        new( 55, "肯尼亚先令","KES",667.658807m,Now),
        new( 56, "拉脱维亚拉图","LVL",139606.3102m,Now),
        new( 57, "老挝基普","LAK",7.846829m,Now),
        new( 58, "黎巴嫩镑","LBP",47.08242m,Now),
        new( 59, "立陶宛立特","LTL",25010.62952m,Now),
        new( 60, "罗马尼亚列伊","RON",16413.89272m,Now),
        new( 61, "毛里求斯卢比","MUR",1778.970083m,Now),
        new( 62, "蒙古图格里克","MNT",25.140344m,Now),
        new( 63, "孟加拉塔卡","BDT",835.793351m,Now),
        new( 64, "秘鲁新索尔","PEN",20138.14769m,Now),
        new( 65, "缅甸缅元","BUK",10000m,Now),
        new( 66, "摩洛哥迪拉姆","MAD",7315.717086m,Now),
        new( 67, "挪威克朗","NOK",7437.93047m,Now),
        new( 68, "葡萄牙埃斯库多","PTE",10000m,Now),
        new( 69, "沙特里亚尔","SAR",18899.6617m,Now),
        new( 70, "斯里兰卡卢比","LKR",379.852617m,Now),
        new( 71, "索马里先令","SOS",123.720773m,Now),
        new( 72, "坦桑尼亚先令","TZS",30.689442m,Now),
        new( 73, "突尼斯第纳尔","TND",24830.53162m,Now),
        new( 74, "土耳其里拉","TRY",10339.97849m,Now),
        new( 75, "危地马拉格查尔","GTQ",9200.139842m,Now),
        new( 76, "委内瑞拉博利瓦","VEB",10000m,Now),
        new( 77, "乌拉圭比索","UYU",1669.421211m,Now),
        new( 78, "西班牙比塞塔","ESP",10000m,Now),
        new( 79, "希腊德拉克马","GRD",10000m,Now),
        new( 80, "匈牙利福林","HUF",229.41652m,Now),
        new( 81, "牙买加元","JMD",507.132051m,Now),
        new( 82, "以色列谢克尔","ILS",20541.05129m,Now),
        new( 83, "意大利里拉","ITL",10000m,Now),
        new( 84, "约旦第纳尔","JOD",100160.2564m,Now),
        new( 85, "越南盾","VND",3.047572m,Now),
        new( 86, "智利比索","CLP",86.775364m,Now),
        new( 87, "巴布亚新几内亚基那","PGK",20728.8255m,Now),
        new( 88, "朝鲜圆","KPW",78.768886m,Now),
        new( 89, "莱索托洛提","LSL",4055.610531m,Now),
        new( 90, "利比亚第纳尔","LYD",51114.29156m,Now),
        new( 91, "卢旺达法郎","RWF",76.542355m,Now),
        new( 92, "缅甸元","MMK",51.198033m,Now),
        new( 93, "毛里塔尼亚乌吉亚","MRO",199.695783m,Now),
        new( 94, "马拉维克瓦查","MWK",97.337668m,Now),
        new( 95, "尼加拉瓜科多巴","NIO",2091.210225m,Now),
        new( 96, "尼泊尔卢比","NPR",583.233211m,Now),
        new( 97, "所罗门群岛元","SBD",8509.624385m,Now),
        new( 98, "塞舌尔法郎","SCR",4055.610531m,Now),
        new( 99, "文莱元","BND",50789.7811m,Now),
        new( 100, "叙利亚镑","SYP",138.191024m,Now),
        new( 101, "阿尔及利亚第纳尔","DZD",549.861434m,Now),
        new( 102, "阿联酋迪拉姆","AED",19302.0383m,Now),
        new( 103, "巴巴多斯元","BBD",35445.90954m,Now),
        new( 104, "阿富汗尼","AFN",920.07747m,Now),
        new( 105, "阿尔巴尼亚勒克","ALL",641.846875m,Now),
        new( 106, "亚美尼亚德拉姆","AMD",147.867266m,Now),
        new( 107, "安哥拉宽扎","AOA",119.807885m,Now),
        new( 108, "阿鲁巴盾弗罗林","AWG",39549.13981m,Now),
        new( 109, "阿塞拜疆新马纳特","AZN",41713.59446m,Now),
        new( 110, "波斯尼亚马尔卡","BAM",41666.66667m,Now),
        new( 111, "布隆迪法郎","BIF",37.355029m,Now),
        new( 112, "巴哈马元","BSD",70891.81908m,Now),
        new( 113, "不丹努扎姆","BTN",928.938956m,Now),
        new( 114, "白俄罗斯卢布","BYR",3.541058m,Now),
        new( 115, "伯利兹美元","BZD",35487.41971m,Now),
        new( 116, "刚果法郎","CDF",38.18933m,Now),
        new( 117, "哥斯达黎加科朗","CRC",123.673279m,Now),
        new( 118, "古巴可兑换比索","CUC",70891.81908m,Now),
        new( 119, "佛得角埃斯库多","CVE",721.767348m,Now),
        new( 120, "吉布提法郎","DJF",399.391487m,Now),
        new( 121, "多明尼加比索","DOP",1222.697172m,Now),
        new( 122, "尼日利亚奈拉","NGN",184.614589m,Now),
        new( 123, "厄立特里亚纳克法","ERN",4710.426529m,Now),
        new( 124, "埃塞俄比亚比尔","ETB",2065.616369m,Now),
        new( 125, "斐济元","FJD",33010.92662m,Now),
        new( 126, "福克兰镑","FKP",87535.01401m,Now),
        new( 127, "格鲁吉亚拉里","GEL",23393.45451m,Now),
        new( 128, "直布罗陀镑","GIP",87496.71887m,Now),
        new( 129, "冈比亚达拉西","GMD",1377.881149m,Now),
        new( 130, "几内亚法郎","GNF",7.434144m,Now),
        new( 131, "圭亚那元","GYD",342.987046m,Now),
        new( 132, "洪都拉斯伦皮拉","HNL",2865.08323m,Now),
        new( 133, "海地古德","HTG",662.542121m,Now),
        new( 134, "伊拉克第纳尔","IQD",59.824473m,Now),
        new( 135, "伊朗里亚尔","IRR",1.687904m,Now),
        new( 136, "吉尔吉斯斯坦索姆","KGS",947.193937m,Now),
        new( 137, "柬埔寨瑞尔","KHR",17.448961m,Now),
        new( 138, "科摩罗法郎","KMF",163.685061m,Now),
        new( 139, "开曼群岛元","KYD",86452.83998m,Now),
        new( 140, "利比里亚元","LRD",359.839597m,Now),
        new( 141, "摩尔多瓦列伊","MDL",4154.238569m,Now),
        new( 142, "马尔加什阿里亚","MGA",18.655789m,Now),
        new( 143, "马其顿第纳尔","MKD",1291.529118m,Now),
        new( 144, "马尔代夫拉菲亚","MVR",4553.112052m,Now),
        new( 145, "新莫桑比克梅蒂卡尔","MZN",1025.043871m,Now),
        new( 146, "纳米比亚元","NAD",4074.016736m,Now),
        new( 147, "塞尔维亚第纳尔","RSD",678.035054m,Now),
        new( 148, "苏丹镑","SDG",1288.945487m,Now),
        new( 149, "圣圣赫勒拿镑","SHP",87496.71887m,Now),
        new( 150, "塞拉利昂利昂","SLL",7.346854m,Now),
        new( 151, "苏里南元","SRD",9585.154512m,Now),
        new( 152, "圣多美多布拉","STD",3.308783m,Now),
        new( 153, "斯威士兰里兰吉尼","SZL",4066.826086m,Now),
        new( 154, "塔吉克斯坦索莫尼","TJS",6878.052135m,Now),
        new( 155, "土库曼斯坦马纳特","TMT",20789.58857m,Now),
        new( 156, "汤加潘加","TOP",30416.40052m,Now),
        new( 157, "特立尼达多巴哥元","TTD",10557.20952m,Now),
        new( 158, "乌克兰格里夫纳","UAH",2651.162667m,Now),
        new( 159, "乌干达先令","UGX",19.009072m,Now),
        new( 160, "乌兹别克斯坦苏姆","UZS",6.997532m,Now),
        new( 161, "委内瑞拉玻利瓦尔","VEF",0.285614m,Now),
        new( 162, "瓦努阿图瓦图","VUV",618.333973m,Now),
        new( 163, "西萨摩亚塔拉","WST",26112.38772m,Now),
        new( 164, "中非金融合作法郎","XAF",121.19533m,Now),
        new( 165, "东加勒比元","XCD",26256.36717m,Now),
        new( 166, "西非法郎","XOF",121.19533m,Now),
        new( 167, "法属波利尼西亚法郎","XPF",670.690811m,Now),
        new( 168, "也门里亚尔","YER",284.421225m,Now),
        new( 169, "赞比亚克瓦查","ZMW",3886.619534m,Now),
        new( 170, "萨尔瓦多科朗","SVC",8101.955001m,Now),
        };
        t.ForEach(n =>
        {
            n.CreatedAt = Now;
            n.UpdatedAt = Now;
        });
        return t;
    }
}
