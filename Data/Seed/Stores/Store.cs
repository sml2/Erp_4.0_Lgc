using ERP.Enums;
using ERP.Models;
using ERP.Models.Setting;
using Microsoft.AspNetCore.Identity;
using static ERP.Models.Stores.StoreRegion;
using Model = ERP.Models.Stores.StoreRegion;

namespace ERP.Data.Seed;
using EID = Enums.ID;
public class Store : AbsSeed<Model, Store>
{
    public override Model[] Create()
    {
        #region MyRegion
        //var t = new Model[]
        //{
        //    new(1, ERP.Enums.Identity.User.LYQ, EID.Group.DEFAUTE, EID.Company.XiaoMi, EID.OEM.XiaoMi, AddModes.Multiple,
        //        Continents.Europe,
        //        "{\"Mws_DebugView\":\"\",\"MarketplaceId_DebugView\":\"\",\"Mws_DebugCache\":\"https://mws.amazonservices.fr\",\"MarketplaceId_DebugCache\":\"A13V1IB3VIYZZH\",\"Choose\":1,\"KeyID\":2,\"PlatformDataID\":14,\"SellerID\":\"A3P51T51XDLL42\",\"AWSAccessKeyId\":null,\"SecretKey\":null,\"MWSAuthToken\":\"amzn.mws.f0bbeba5-db8c-cb22-7fd4-fbdfa723c246\"}",
        //        Flags.NO, 1993073995763109104, "Test-法国", 54, "法国", "FR", Platforms.AMAZON, 2, "欧元", "EUR"),
        //    new(2, ERP.Enums.Identity.User.LYQ, EID.Group.DEFAUTE, EID.Company.XiaoMi, EID.OEM.XiaoMi, AddModes.Multiple,
        //        Continents.Europe,
        //        "{\"Mws_DebugView\":\"\",\"MarketplaceId_DebugView\":\"\",\"Mws_DebugCache\":\"https://mws.amazonservices.de\",\"MarketplaceId_DebugCache\":\"A1PA6795UKMFR9\",\"Choose\":1,\"KeyID\":2,\"PlatformDataID\":15,\"SellerID\":\"A3P51T51XDLL42\",\"AWSAccessKeyId\":null,\"SecretKey\":null,\"MWSAuthToken\":\"amzn.mws.f0bbeba5-db8c-cb22-7fd4-fbdfa723c246\"}",
        //        Flags.NO, 1993073995763109104, "Test-德国", 46, "德国", "DE", Platforms.AMAZON, 2, "欧元", "EUR"),
        //    new(3, ERP.Enums.Identity.User.LYQ, EID.Group.DEFAUTE, EID.Company.XiaoMi, EID.OEM.XiaoMi, AddModes.Multiple,
        //        Continents.Europe,
        //        "{\"Mws_DebugView\":\"\",\"MarketplaceId_DebugView\":\"\",\"Mws_DebugCache\":\"https://mws.amazonservices.it\",\"MarketplaceId_DebugCache\":\"APJ6JRA9NG5V4\",\"Choose\":1,\"KeyID\":2,\"PlatformDataID\":17,\"SellerID\":\"A3P51T51XDLL42\",\"AWSAccessKeyId\":null,\"SecretKey\":null,\"MWSAuthToken\":\"amzn.mws.f0bbeba5-db8c-cb22-7fd4-fbdfa723c246\"}",
        //        Flags.NO, 1993073995763109104, "Test-意大利", 214, "意大利", "IT", Platforms.AMAZON, 2, "欧元", "EUR"),
        //    new(4, ERP.Enums.Identity.User.LYQ, EID.Group.DEFAUTE, EID.Company.XiaoMi, EID.OEM.XiaoMi, AddModes.Multiple,
        //        Continents.Europe,
        //        "{\"Mws_DebugView\":\"\",\"MarketplaceId_DebugView\":\"\",\"Mws_DebugCache\":\"http://mws-eu.amazonservices.com\",\"MarketplaceId_DebugCache\":\"A1805IZSGTT6HS\",\"Choose\":1,\"KeyID\":2,\"PlatformDataID\":20,\"SellerID\":\"A3P51T51XDLL42\",\"AWSAccessKeyId\":null,\"SecretKey\":null,\"MWSAuthToken\":\"amzn.mws.f0bbeba5-db8c-cb22-7fd4-fbdfa723c246\"}",
        //        Flags.NO, 1993073995763109104, "Test-荷兰", 77, "荷兰", "NL", Platforms.AMAZON, 2, "欧元", "EUR"),
        //    new(5, ERP.Enums.Identity.User.LYQ, EID.Group.DEFAUTE, EID.Company.XiaoMi, EID.OEM.XiaoMi, AddModes.Multiple,
        //        Continents.Europe,
        //        "{\"Mws_DebugView\":\"\",\"MarketplaceId_DebugView\":\"\",\"Mws_DebugCache\":\"https://mws-eu.amazonservices.com\",\"MarketplaceId_DebugCache\":\"A1F83G8C2ARO7P\",\"Choose\":1,\"KeyID\":2,\"PlatformDataID\":28,\"SellerID\":\"A3P51T51XDLL42\",\"AWSAccessKeyId\":null,\"SecretKey\":null,\"MWSAuthToken\":\"amzn.mws.f0bbeba5-db8c-cb22-7fd4-fbdfa723c246\"}",
        //        Flags.NO, 1993073995763109104, "Test-英国", 217, "英国", "GB", Platforms.AMAZON, 8, "英镑", "GBP"),
        //    new(6, ERP.Enums.Identity.User.LYQ, EID.Group.DEFAUTE, EID.Company.XiaoMi, EID.OEM.XiaoMi, AddModes.Multiple,
        //        Continents.Europe,
        //        "{\"Mws_DebugView\":\"\",\"MarketplaceId_DebugView\":\"\",\"Mws_DebugCache\":\"https://mws-eu.amazonservices.com\",\"MarketplaceId_DebugCache\":\"A2NODRKZP88ZB9\",\"Choose\":1,\"KeyID\":2,\"PlatformDataID\":25,\"SellerID\":\"A3P51T51XDLL42\",\"AWSAccessKeyId\":null,\"SecretKey\":null,\"MWSAuthToken\":\"amzn.mws.f0bbeba5-db8c-cb22-7fd4-fbdfa723c246\"}",
        //        Flags.NO, 1993073995763109104, "Test-瑞典", 156, "瑞典", "SE", Platforms.AMAZON, 2, "欧元", "EUR"),
        //    new(7, ERP.Enums.Identity.User.LYQ, EID.Group.DEFAUTE, EID.Company.XiaoMi, EID.OEM.XiaoMi, AddModes.Multiple,
        //        Continents.Europe,
        //        "{\"Mws_DebugView\":\"\",\"MarketplaceId_DebugView\":\"\",\"Mws_DebugCache\":\"https://mws.amazonservices.es\",\"MarketplaceId_DebugCache\":\"A1RKKUPIHCS9HS\",\"Choose\":1,\"KeyID\":2,\"PlatformDataID\":24,\"SellerID\":\"A3P51T51XDLL42\",\"AWSAccessKeyId\":null,\"SecretKey\":null,\"MWSAuthToken\":\"amzn.mws.f0bbeba5-db8c-cb22-7fd4-fbdfa723c246\"}",
        //        Flags.NO, 1993073995763109104, "Test-西班牙", 202, "西班牙", "ES", Platforms.AMAZON, 2, "欧元", "EUR"),
        //    new(8, ERP.Enums.Identity.User.LYQ, EID.Group.DEFAUTE, EID.Company.XiaoMi, EID.OEM.XiaoMi, AddModes.Multiple,
        //        Continents.Europe,
        //        "{\"Mws_DebugView\":\"\",\"MarketplaceId_DebugView\":\"\",\"Mws_DebugCache\":\"https://mws-eu.amazonservices.com\",\"MarketplaceId_DebugCache\":\"A1C3SOZRARQ6R3\",\"Choose\":1,\"KeyID\":2,\"PlatformDataID\":21,\"SellerID\":\"A3P51T51XDLL42\",\"AWSAccessKeyId\":null,\"SecretKey\":null,\"MWSAuthToken\":\"amzn.mws.f0bbeba5-db8c-cb22-7fd4-fbdfa723c246\"}",
        //        Flags.NO, 1993073995763109104, "Test-波兰", 37, "波兰", "PL", Platforms.AMAZON, 2, "欧元", "EUR"),
        //};
        //t.ForEach(n =>
        //{
        //    n.CreatedAt = Now;
        //    n.UpdatedAt = Now;
        //});
        //return t;
        #endregion
        return Array.Empty<Model>();
    }
}