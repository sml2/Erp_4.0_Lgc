﻿using static ERP.Models.Abstract.BaseModel;

namespace ERP.Data.Seed
{
    public class EmailConfig : AbsSeed<Models.Setting.EmailConfig, EmailConfig>
    {
        public override Models.Setting.EmailConfig[] Create()
        {
            Models.Setting.EmailConfig[] t =
            {
                new()
                {
                    ID = 1, Name = "网易企业邮箱", Domain = "163.com", Pop = "pop.ym.163.com", Smtp = "smtp.ym.163.com",
                    PopPort = 25, SmtpPort = 25, PopSSL = StateEnum.Open, SmtpSSL = StateEnum.Close, Sort = 0,
                    State = StateEnum.Close, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 3, Name = "QQ邮箱", Domain = "qq.com", Pop = "pop.qq.com", Smtp = "smtp.qq.com", PopPort = 995,
                    SmtpPort = 587, PopSSL = StateEnum.Open, SmtpSSL = StateEnum.Open, Sort = 0,
                    State = StateEnum.Open,
                    CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 4, Name = "网易邮箱", Domain = "163.com", Pop = "pop.163.com", Smtp = "smtp.163.com", PopPort = 25,
                    SmtpPort = 25, PopSSL = StateEnum.Close, SmtpSSL = StateEnum.Open, Sort = 0,
                    State = StateEnum.Close,
                    CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 5, Name = "新浪邮箱", Domain = "sina.com", Pop = "pop.sina.com", Smtp = "smtp.sina.com",
                    PopPort = 25, SmtpPort = 25, PopSSL = StateEnum.Open, SmtpSSL = StateEnum.Open, Sort = 0,
                    State = StateEnum.Close, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 6, Name = "搜狐邮箱", Domain = "sohu.com", Pop = "pop.sohu.com", Smtp = "smtp.sohu.com",
                    PopPort = 25, SmtpPort = 25, PopSSL = StateEnum.Open, SmtpSSL = StateEnum.Close, Sort = 0,
                    State = StateEnum.Close, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 7, Name = "谷歌邮箱", Domain = "gmail.com", Pop = "pop.gmail.com", Smtp = "smtp.gmail.com",
                    PopPort = 465, SmtpPort = 465, PopSSL = StateEnum.Open, SmtpSSL = StateEnum.Close, Sort = 0,
                    State = StateEnum.Close,
                    CreatedAt = Now, UpdatedAt = Now
                },
            };
            return t;
        }
    }
}