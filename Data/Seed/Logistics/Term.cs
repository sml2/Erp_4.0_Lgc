﻿namespace ERP.Data.Seed.Logistics;
/*
 * Logistics
 *  n.后勤;物流;组织工作
 * logistic 
 *  adj.物流的;逻辑的，后勤学的
 *  n.数理（符号）逻辑；逻辑斯蒂；计算术
 * logistically
 *  adv.逻辑地
 * waybill
 *  n.乘客名单；运货单
 * express
 *  adj.特快的;快速的;快递的;用快递寄送的;提供快递服务的;明确的
 *  adv.使用快速服务
 *  n.特快列车;快件服务;快递服务;快运服务
 *  vt.表示;表达;表露;表达(自己的思想感情);显而易见;不言自明;（用符号等）表示，代表;压榨，挤压出（空气或液体）;快递邮寄（或发送）
 */
