using static ERP.Models.Abstract.BaseModel;
using ERP.Models.DB;
using ERP.Enums;
using Newtonsoft.Json;
using static ERP.Models.PlatformData;
using ERP.Enums.ID;

namespace ERP.Data.Seed;
using Model = Models.PlatformData;
public class PlatformData : AbsSeed<Model, PlatformData>
{
    public override Model[] Create()
    {
        Model[] t =
        {
            // Shopee
            new() { ID = 1, NationId = 206, Platform = Platforms.SHOPEE,
                Data = JsonConvert.SerializeObject(new ShopeeData{ AreaName = "Singapore", ShorterForm = "SG",KeyID=1}), CreatedAt = Now, UpdatedAt = Now},
            new() { ID = 2, NationId = 119, Platform = Platforms.SHOPEE,
                Data = JsonConvert.SerializeObject(new ShopeeData{ AreaName = "Malaysia", ShorterForm = "MY",KeyID=1}), CreatedAt = Now, UpdatedAt = Now},
            new() { ID = 3, NationId = 184, Platform = Platforms.SHOPEE,
                Data = JsonConvert.SerializeObject(new ShopeeData{ AreaName = "Thailand", ShorterForm = "TH",KeyID=1}), CreatedAt = Now, UpdatedAt = Now},
            new() { ID = 4, NationId = 183, Platform = Platforms.SHOPEE,
                Data = JsonConvert.SerializeObject(new ShopeeData{ AreaName = "Taiwan", ShorterForm = "TW",KeyID=1}), CreatedAt = Now, UpdatedAt = Now},
            new() { ID = 5, NationId = 216, Platform = Platforms.SHOPEE,
                Data = JsonConvert.SerializeObject(new ShopeeData{ AreaName = "Indonesia", ShorterForm = "ID",KeyID=1}), CreatedAt = Now, UpdatedAt = Now},
            new() { ID = 6, NationId = 221, Platform = Platforms.SHOPEE,
                Data = JsonConvert.SerializeObject(new ShopeeData{ AreaName = "Vietnam", ShorterForm = "VN",KeyID=1}), CreatedAt = Now, UpdatedAt = Now},
            new() { ID = 7, NationId = 59, Platform = Platforms.SHOPEE,
                Data = JsonConvert.SerializeObject(new ShopeeData{ AreaName = "Philippines", ShorterForm = "PH", KeyID = 1}), CreatedAt = Now, UpdatedAt = Now},
            new() { ID = 8, NationId = 26, Platform = Platforms.SHOPEE,
                Data = JsonConvert.SerializeObject(new ShopeeData{ AreaName = "Brazil", ShorterForm = "BR", KeyID = 1}), CreatedAt = Now, UpdatedAt = Now},





            new() { ID = 50, NationId = 141, Platform = Platforms.SHOPEE,
                Data = JsonConvert.SerializeObject(new ShopeeData{ AreaName = "Mexico", ShorterForm = "MX", KeyID = 1}), CreatedAt = Now, UpdatedAt = Now},

            new() { ID = 51, NationId = 37, Platform = Platforms.SHOPEE,
                Data = JsonConvert.SerializeObject(new ShopeeData{ AreaName = "Poland", ShorterForm = "PL", KeyID = 1}), CreatedAt = Now, UpdatedAt = Now},

            new() { ID =52, NationId = 202, Platform = Platforms.SHOPEE,
                Data = JsonConvert.SerializeObject(new ShopeeData{ AreaName = "Spain", ShorterForm = "ES", KeyID = 1}), CreatedAt = Now, UpdatedAt = Now},

            new() { ID = 53, NationId = 54, Platform = Platforms.SHOPEE,
                Data = JsonConvert.SerializeObject(new ShopeeData{ AreaName = "France", ShorterForm = "FR", KeyID = 1}), CreatedAt = Now, UpdatedAt = Now},

             
           // Amazon
           // Mapping:https://developer-docs.amazon.com/sp-api/docs/mapping-apis-from-amazon-mws-to-the-selling-partner-api
           // MWS:https://docs.developer.amazonservices.com/en_US/dev_guide/DG_Endpoints.html
           // SPI:https://developer-docs.amazon.com/sp-api/docs/marketplace-ids
          
            new(9,Nations.BR,Platforms.AMAZON, new AmazonData("Brazil","巴西","BR","BRL","mws.amazonservices.com","A2Q3Y263D00KWC","https://www.amazon.com.br",Continents.NorthAmerica,1)),
            new(10,Nations.CA,Platforms.AMAZON,new AmazonData("Canada","加拿大","CA","CAD","mws.amazonservices.ca","A2EUQ1WTGCTBG2","https://www.amazon.ca",Continents.NorthAmerica,1)),
            new(11,Nations.MX,Platforms.AMAZON,new AmazonData("Mexico","墨西哥","MX","MXN","mws.amazonservices.com.mx","A1AM78C64UM0Y8","https://www.amazon.com.mx",Continents.NorthAmerica,1)),
            new(12,Nations.US,Platforms.AMAZON,new AmazonData("United States","美国","US","USD","mws.amazonservices.com","ATVPDKIKX0DER","https://www.amazon.com",Continents.NorthAmerica,1)),

            new(13,Nations.AE,Platforms.AMAZON,new AmazonData("United Arab Emirates", "阿拉伯联合酋长国", "AE", "AED","mws.amazonservices.ae","A2VIGQ35RCS4UG","https://www.amazon.ae",Continents.Europe,2)),
            new(14,Nations.DE,Platforms.AMAZON,new AmazonData("Germany(Deutschland)","德国","DE","EUR","mws.amazonservices.de","A1PA6795UKMFR9","https://www.amazon.de", Continents.Europe,2)),
            new(15,Nations.EG,Platforms.AMAZON,new AmazonData("Egypt","埃及","EG","EGP","mws-eu.amazonservices.com","ARBP9OOSHTCHU","https://www.amazon.eg/", Continents.Europe ,2 )),
            new(16,Nations.ES,Platforms.AMAZON,new AmazonData("Spain","西班牙","ES","EUR","mws.amazonservices.es","A1RKKUPIHCS9HS","https://www.amazon.es",Continents.Europe,2)),
            new(17,Nations.FR,Platforms.AMAZON,new AmazonData("France","法国","FR","EUR","mws.amazonservices.fr","A13V1IB3VIYZZH","https://www.amazon.fr",Continents.Europe,2)),
            new(18, Nations.BE, Platforms.AMAZON,new AmazonData("Belgium","比利时", "BE", "","mws.amazonservices.be","AMEN7PMS3EDWL","https://www.amazon.be",Continents.Europe,2)),
            new(19, Nations.GB, Platforms.AMAZON,new AmazonData("United Kingdom", "英国","GB","GBP", "mws-eu.amazonservices.com","A1F83G8C2ARO7P","https://www.amazon.co.uk",Continents.Europe,2)),
            new(20, Nations.IN, Platforms.AMAZON,new AmazonData("India","印度","IN","INR","mws.amazonservices.in","A21TJRUUN4KGV","https://www.amazon.in",Continents.Europe,2)),
            new(21, Nations.IT, Platforms.AMAZON,new AmazonData("Italy", "意大利", "IT", "EUR", "mws.amazonservices.it","APJ6JRA9NG5V4","https://www.amazon.it",Continents.Europe,2)),
            new(22, Nations.NL, Platforms.AMAZON,new AmazonData("Netherlands", "荷兰", "NL", "EUR", "mws-eu.amazonservices.com","A1805IZSGTT6HS","https://www.amazon.nl",Continents.Europe,2)),
            new(23, Nations.PL, Platforms.AMAZON,new AmazonData("Poland", "波兰", "PL", "EUR", "mws-eu.amazonservices.com","A1C3SOZRARQ6R3",  "https://www.amazon.pl",Continents.Europe,2)),
            new(24, Nations.SE, Platforms.AMAZON,new AmazonData("Sweden", "瑞典", "SE", "EUR", "mws-eu.amazonservices.com","A2NODRKZP88ZB9",  "https://www.amazon.se",Continents.Europe,2)),
            new(25, Nations.SA, Platforms.AMAZON,new AmazonData("Saudi Arabia", "沙特阿拉伯", "SA", "SAR", "mws-eu.amazonservices.com","A17E79C6D8DWNP", "https://www.amazon.sa",Continents.Europe,2)),
            new(26, Nations.TR, Platforms.AMAZON,new AmazonData("Turkey", "土耳其", "TR", "TRY", "mws-eu.amazonservices.com","A33AVAJ2PDY3EV","https://www.amazon.com.tr",Continents.Europe,2)),
            new(27, Nations.SG, Platforms.AMAZON,new AmazonData("Singapore", "新加坡", "SG", "SGD", "mws-fe.amazonservices.com","A19VAU5U5O7RUS", "https://www.amazon.sg",Continents.FarEast,8)),
            new(28, Nations.AU, Platforms.AMAZON,new AmazonData( "Australia", "澳大利亚", "AU","AUD", "mws.amazonservices.com.au", "A39IBJ37TRP1C6","https://www.amazon.com.au",Continents.FarEast,8)),
            new(29, Nations.JP, Platforms.AMAZON,new AmazonData( "Japan", "日本", "JP", "JPY", "mws.amazonservices.jp","A1VC38T7YXB528", "https://www.amazon.co.jp",Continents.FarEast,8)),
             
            //amazon sp api
            //远东
            new(30, Nations.CA, Platforms.AMAZONSP,new AmazonData( "Canada", "加拿大", "CA", "CAD", "https://sellingpartnerapi-na.amazon.com","A2EUQ1WTGCTBG2","https://www.amazon.ca",
               Continents.NorthAmerica,1,"https://sellercentral.amazon.ca")),
            new(31, Nations.US, Platforms.AMAZONSP,new AmazonData("United States", "美国", "US", "USD", "https://sellingpartnerapi-na.amazon.com","ATVPDKIKX0DER", "https://www.amazon.com",
               Continents.NorthAmerica,1,"https://sellercentral.amazon.com")),
            new(32, Nations.MX, Platforms.AMAZONSP,new AmazonData( "Mexico", "墨西哥", "MX", "MXN", "https://sellingpartnerapi-na.amazon.com","A1AM78C64UM0Y8",  "https://www.amazon.com.mx",
               Continents.NorthAmerica,1,"https://sellercentral.amazon.com.mx")),
            new(33, Nations.BR, Platforms.AMAZONSP,new AmazonData( "Brazil", "巴西", "BR", "BRL", "https://sellingpartnerapi-na.amazon.com","A2Q3Y263D00KWC",  "https://www.amazon.com.br",
               Continents.NorthAmerica,1,"https://sellercentral.amazon.com.br")),
            new(34, Nations.ES, Platforms.AMAZONSP,new AmazonData( "Spain", "西班牙", "ES", "EUR", "https://sellingpartnerapi-eu.amazon.com","A1RKKUPIHCS9HS",  "https://www.amazon.es",
               Continents.Europe,1 ,"https://sellercentral-europe.amazon.com")),
            new(35, Nations.GB, Platforms.AMAZONSP,new AmazonData( "United Kingdom", "英国", "GB", "GBP", "https://sellingpartnerapi-eu.amazon.com","A1F83G8C2ARO7P", "https://www.amazon.co.uk",
              Continents.Europe,1,"https://sellercentral-europe.amazon.com")),
            new(36, Nations.FR, Platforms.AMAZONSP,new AmazonData( "France", "法国", "FR", "EUR", "https://sellingpartnerapi-eu.amazon.com","A13V1IB3VIYZZH", "https://www.amazon.fr",
              Continents.Europe,1,"https://sellercentral-europe.amazon.com")),
            new(37, Nations.NL, Platforms.AMAZONSP,new AmazonData( "Netherlands", "荷兰", "NL", "EUR", "https://sellingpartnerapi-eu.amazon.com","A1805IZSGTT6HS",  "https://www.amazon.nl",
              Continents.Europe,1,"https://sellercentral.amazon.nl")),
            new(38, Nations.DE, Platforms.AMAZONSP,new AmazonData( "Germany(Deutschland)", "德国", "DE", "EUR", "https://sellingpartnerapi-eu.amazon.com","A1PA6795UKMFR9",  "https://www.amazon.de",
              Continents.Europe,1,"https://sellercentral-europe.amazon.com")),
            new(39, Nations.IT, Platforms.AMAZONSP,new AmazonData( "Italy", "意大利", "IT", "EUR", "https://sellingpartnerapi-eu.amazon.com","APJ6JRA9NG5V4",  "https://www.amazon.it",
              Continents.Europe,1,"https://sellercentral-europe.amazon.com")),
            new(40, Nations.SE, Platforms.AMAZONSP,new AmazonData( "Sweden", "瑞典", "SE", "EUR", "https://sellingpartnerapi-eu.amazon.com","A2NODRKZP88ZB9",  "https://www.amazon.se",
              Continents.Europe,1,"https://sellercentral.amazon.se")),
            new(41, Nations.PL, Platforms.AMAZONSP,new AmazonData( "Poland", "波兰", "PL", "EUR", "https://sellingpartnerapi-eu.amazon.com","A1C3SOZRARQ6R3",  "https://www.amazon.pl",
                Continents.Europe,1,"https://sellercentral.amazon.pl")),
            new(42, Nations.EG, Platforms.AMAZONSP, new AmazonData( "Egypt", "埃及", "EG","EGP","https://sellingpartnerapi-eu.amazon.com", "ARBP9OOSHTCHU","https://mws-eu.amazonservices.com",
                Continents.Europe ,1 ,"https://sellercentral.amazon.eg")),
            new(43, Nations.TR, Platforms.AMAZONSP,new AmazonData( "Turkey", "土耳其", "TR", "TRY", "https://sellingpartnerapi-eu.amazon.com","A33AVAJ2PDY3EV","https://www.amazon.com.tr",
                Continents.Europe,1,"https://sellercentral.amazon.com.tr")),
            new(44, Nations.SA, Platforms.AMAZONSP,new AmazonData( "Saudi Arabia", "沙特阿拉伯", "SA", "SAR", "https://sellingpartnerapi-eu.amazon.com","A17E79C6D8DWNP", "https://www.amazon.sa",
              Continents.Europe,1,"https://sellercentral.amazon.sa")),
            new(45, Nations.AE, Platforms.AMAZONSP,new AmazonData( "United Arab Emirates", "阿拉伯联合酋长国", "AE", "AED", "https://sellingpartnerapi-eu.amazon.com","A2VIGQ35RCS4UG", "https://www.amazon.ae",
              Continents.Europe,1,"https://sellercentral.amazon.ae")),

            new(46, Nations.IN, Platforms.AMAZONSP,new AmazonData( "India", "印度", "IN", "INR", "https://sellingpartnerapi-eu.amazon.com","A21TJRUUN4KGV", "https://www.amazon.in",
                Continents.Europe,1,"https://sellercentral.amazon.in")),    
            
             new(78, Nations.BE, Platforms.AMAZONSP,new AmazonData("Belgium","比利时", "BE", "BEF","https://sellingpartnerapi-eu.amazon.com","AMEN7PMS3EDWL","https://www.amazon.be",Continents.Europe,1,"https://sellercentral.amazon.com.be")),

            new(47, Nations.AU, Platforms.AMAZONSP,new AmazonData( "Australia", "澳大利亚", "AU","AUD", "https://sellingpartnerapi-fe.amazon.com", "A39IBJ37TRP1C6","https://www.amazon.com.au",
            Continents.FarEast,1,"https://sellercentral.amazon.com.au")),

            new(48, Nations.JP, Platforms.AMAZONSP,new AmazonData( "Japan", "日本", "JP", "JPY", "https://sellingpartnerapi-fe.amazon.com","A1VC38T7YXB528", "https://www.amazon.co.jp",
            Continents.FarEast,1 ,"https://sellercentral-japan.amazon.com")),

            new(49, Nations.SG, Platforms.AMAZONSP,new AmazonData( "Singapore", "新加坡", "SG", "SGD", "https://sellingpartnerapi-fe.amazon.com","A19VAU5U5O7RUS", "https://www.amazon.sg",
            Continents.FarEast,1,"https://sellercentral.amazon.sg")),

            new(Enums.ID.PlatformData.SFC),//54
            new(Enums.ID.PlatformData.Y7F),
            new(Enums.ID.PlatformData.YunTu),
            new(Enums.ID.PlatformData.K5_HL),
            new(Enums.ID.PlatformData.RTB_SDGJ),
            new(Enums.ID.PlatformData.YanWen),
            new(Enums.ID.PlatformData.FPX),
            new(Enums.ID.PlatformData.CNE_DYGJ),
            new(Enums.ID.PlatformData.K5_JHGJ),
            new(Enums.ID.PlatformData.UBI),
            new(Enums.ID.PlatformData.K5_BZY),
            new(Enums.ID.PlatformData.HuaLei_YH),
            new(Enums.ID.PlatformData.DEPPON),
            new(Enums.ID.PlatformData.RTB_YDH),
            new(Enums.ID.PlatformData.HZGJ),
            new(Enums.ID.PlatformData.RTB_ZHXT),
            new(Enums.ID.PlatformData.EMS),
            new(Enums.ID.PlatformData.RTB_JFCC),
            new(Enums.ID.PlatformData.SFGJ),
            new(Enums.ID.PlatformData.KJHYXB),
            new(Enums.ID.PlatformData.EOC),
            new(Enums.ID.PlatformData.WANB),
            new(Enums.ID.PlatformData.YH),//云盒            
            new(79,Enums.ID.PlatformData.HuaLei_OWN),
            // SFC三态速递提供的国家英文名称
            //new(){ID = 30, NationId=1,Platform= Platforms.SFC,Data=JsonConvert.SerializeObject(new SFCData{ EnglishName="Albania" }), CreatedAt = Now, UpdatedAt = Now },

        };
        t.ForEach(n =>
        {
            n.CreatedAt = Now;
            n.UpdatedAt = Now;
        });
        return t;
    }
}
