using static ERP.Models.Abstract.BaseModel;

namespace ERP.Data.Seed
{
    using Types = Models.Setting.Platform.Types;

    public class Platform : AbsSeed<Models.Setting.Platform, Platform>
    {
        public override Models.Setting.Platform[] Create()
        {
            Models.Setting.Platform[] t =
            {
                new()
                {
                    ID = 1, Name = "亚马逊", English = "Amazon", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = true, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 2, Name = "EBay", English = "EBay", State = StateEnum.Open, Sort = 0,
                    Type = Types.Collection, Collect = true, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 3, Name = "速卖通", English = "AliExpress", State = StateEnum.Open, Sort = 0,
                    Type = Types.Collection, Collect = true, Export = false, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 4, Name = "wish", English = "Wish", State = StateEnum.Open, Sort = 0,
                    Type = Types.Collection, Collect = true, Export = false, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 5, Name = "1688", English = "Ali88", State = StateEnum.Open, Sort = 0,
                    Type = Types.Collection, Collect = true, Export = false, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 6, Name = "阿里巴巴国际站", English = "Alibaba", State = StateEnum.Open, Sort = 0,
                    Type = Types.Collection, Collect = true, Export = false, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 7, Name = "Lazada", English = "lazada", State = StateEnum.Open, Sort = 0,
                    Type = Types.Export, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 10, Name = "雅虎", English = "yahoo", State = StateEnum.Open, Sort = 0, Type = Types.Private,
                    Collect = true, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 11, Name = "智赢", English = "zhiying", State = StateEnum.Open, Sort = 0, Type = Types.Private,
                    Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 12, Name = "备份数据", English = "backups", State = StateEnum.Open, Sort = 0,
                    Type = Types.Export, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 13, Name = "Gmarket", English = "Gmarket", State = StateEnum.Open, Sort = 0,
                    Type = Types.Private, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 15, Name = "Netsea", English = "Netsea", State = StateEnum.Open, Sort = 0,
                    Type = Types.Private, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 16, Name = "店小秘", English = "dianXiaoMi", State = StateEnum.Open, Sort = 0,
                    Type = Types.Private, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 17, Name = "京东", English = "JD", State = StateEnum.Open, Sort = 0, Type = Types.Universal,
                    Collect = true, Export = false, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 18, Name = "Fecshop", English = "fecshop", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 19, Name = "Globalsources", English = "Globalsources", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = true, Export = false, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 20, Name = "Lightinthebox", English = "Lightinthebox", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = true, Export = false, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 21, Name = "Flipkart", English = "Flipkart", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = true, Export = false, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 22, Name = "Jumia", English = "Jumia", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = true, Export = false, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 23, Name = "Shopify", English = "Shopify", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 24, Name = "洛琴", English = "LUOQIN", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = true, Export = false, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 25, Name = "汉顶云", English = "zencart", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 26, Name = "虾皮", English = "Shopee", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = true, Export = false, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 27, Name = "环球华品", English = "ChinaBrands", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = true, Export = false, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 28, Name = "定制模版", English = "temp1", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 29, Name = "环球易购", English = "GearBest", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = true, Export = false, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 30, Name = "Linio平台", English = "Linio", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 31, Name = "Cdiscount", English = "Cdiscount", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 32, Name = "Shopee", English = "shopee", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 33, Name = "Korea Erp", English = "Korea Erp", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 34, Name = "趣天", English = "Qutian", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 35, Name = "Noon电商", English = "noon", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 36, Name = "淘宝", English = "Taobao", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = true, Export = false, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 37, Name = "拼多多", English = "Pinduoduo", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = true, Export = false, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 38, Name = "nopCommerce电子商城", English = "nopCommerce", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 39, Name = "onbuy", English = "onbuy", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 40, Name = "Coupang团购网", English = "coupang", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 41, Name = "Ueeshop跨境独立站", English = "Ueeshop", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 42, Name = "冠通", English = "GotenChina", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = true, Export = false, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 43, Name = "大健云仓", English = "GigaCloud", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = true, Export = false, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 44, Name = "Ymmbh", English = "Ymmbh", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 45, Name = "新蛋网", English = "NewEgg", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = true, Export = false, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 46, Name = "鑫创", English = "xinchuang", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 47, Name = "独立站", English = "standAloneStation", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 48, Name = "zood", English = "zood", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 49, Name = "沃尔玛", English = "WalMart", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 50, Name = "allegro", English = "allegro", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 51, Name = "QuickMaxvic", English = "QuickMaxvic", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 52, Name = "云盒", English = "YunHe", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 53, Name = "Fruugo", English = "Fruugo", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 55, Name = "Hktvmall", English = "Hktvmall", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
                new()
                {
                    ID = 56, Name = "Bol", English = "Bol", State = StateEnum.Open, Sort = 0,
                    Type = Types.Universal, Collect = false, Export = true, CreatedAt = Now,
                    UpdatedAt = Now
                },
            };
            return t;
        }
    }
}