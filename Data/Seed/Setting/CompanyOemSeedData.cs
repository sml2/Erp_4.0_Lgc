﻿using ERP.Extensions;
using ERP.Models.DB.Users;
using static ERP.Models.DB.Users.Company;

namespace ERP.Data.Seed.Setting;
using ENUM = Enums.ID.OEM;
using UserEnum = Enums.Identity.User;
using CompanyEnum = Enums.ID.Company;
public class CompanyOemSeedData : AbsSeed<CompanyOem, CompanyOemSeedData>
{
    public override CompanyOem[] Create()
    {
        List<ENUM> oems = new()
        {
            ENUM.MiJingTong,
            ENUM.FengNiao,
            ENUM.YongHe,
            ENUM.PengXu4,
            ENUM.MengHao4,
            ENUM.Yangchen4,
            ENUM.XuMeng4,
            ENUM.JHKJ4,
            ENUM.XunLiYou4,
            ENUM.CTG4,
            ENUM.HW4,
            ENUM.DXKJ4,
            ENUM.XH4,
            ENUM.YZKJ4,
            ENUM.JJKJ4,
            ENUM.YCD4,
            ENUM.SYF4,
        };
        return oems.Select(m => new CompanyOem()
        {
            ID = m.GetID(),
            OEMID = (int)m,
            CompanyID = (int)CompanyEnum.XiaoMi,
            AllotUserId = (int)UserEnum.XIAOMI,
            AllotTrueName = UserEnum.XIAOMI.GetDescription(),
            UserName = UserEnum.XIAOMI.GetDescription(),
            CreatedAt = Now,
            UpdatedAt = Now,
        }).ToArray();
    }
}