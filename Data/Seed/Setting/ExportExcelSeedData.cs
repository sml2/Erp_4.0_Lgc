using ERP.Models.Abstract;
using ERP.Models.DB.Setting;
using ERP.Models.Setting;

namespace ERP.Data.Seed.Setting;

public class ExportExcelSeedData : AbsSeed<ExportModel, ExportExcelSeedData>
{
    public override ExportModel[] Create()
    {
        var t = new[]
        {
            new ExportModel()
            {
                ID = 1, Name = "导出亚马逊通用表", Sign = 1, Filename = "amazon", Sort = -1617864549, Remark = "",
                PlatformId = 1,
                State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 2, Name = "导出店小秘(随机颜色版)", Sign = 2, Filename = "dianXiaoMi", Sort = 2, Remark = "随机颜色",
                PlatformId = 16, State = BaseModel.StateEnum.Close, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Private,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 3, Name = "备份基础数据", Sign = 3, Filename = "backups", Sort = 3, Remark = "", PlatformId = 12,
                State = BaseModel.StateEnum.Open,
                Type = ExportModel.TypeEnum.Porduct, Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 4, Name = "导出Lazada单国家通用表", Sign = 4, Filename = "lazadaOneNation", Sort = 4, Remark = "",
                PlatformId = 7, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 5, Name = "导出Lazada Small Appliances表", Sign = 5, Filename = "lazadaSmallAppliances", Sort = 5,
                Remark = "", PlatformId = 7, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 6, Name = "导出Lazada Furniture & Organization表", Sign = 6, Filename = "lazadaFurnitureOrganization",
                Sort = 6, Remark = "原 Furniture & Décor", PlatformId = 7, State = BaseModel.StateEnum.Open,
                Type = ExportModel.TypeEnum.Porduct, Quality = ExportModel.QualityEnum.Public,
                CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 7, Name = "导出Lazada Mother & Baby表", Sign = 7, Filename = "lazadaMotherBaby", Sort = 7,
                Remark = "",
                PlatformId = 7, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 8, Name = "导出Lazada Mobiles & Tablets表", Sign = 8, Filename = "lazadaMobilesTablets", Sort = 8,
                Remark = "", PlatformId = 7, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 9, Name = "导出Lazada Toys & Games表", Sign = 9, Filename = "lazadaToysGames", Sort = 9, Remark = "",
                PlatformId = 7, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 10, Name = "导出Lazada Watches Sunglasses Jewellery表", Sign = 10,
                Filename = "lazadaWatchesSunglasses",
                Sort = 10, Remark = "", PlatformId = 7, State = BaseModel.StateEnum.Open,
                Type = ExportModel.TypeEnum.Porduct, Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 11, Name = "导出Lazada Sports & Outdoors表", Sign = 11, Filename = "lazadaSportsOutdoors", Sort = 11,
                Remark = "原lazada Bags and Travel", PlatformId = 7, State = BaseModel.StateEnum.Open,
                Type = ExportModel.TypeEnum.Porduct, Quality = ExportModel.QualityEnum.Public,
                CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 12, Name = "导出Lazada Smart Devices表", Sign = 12, Filename = "lazadaSmartDevices", Sort = 12,
                Remark = "", PlatformId = 7, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 13, Name = "导出Lazada Special Digital Products表", Sign = 13,
                Filename = "lazadaSpecialDigitalProducts",
                Sort = 13, Remark = "", PlatformId = 7, State = BaseModel.StateEnum.Open,
                Type = ExportModel.TypeEnum.Porduct, Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 14, Name = "导出Lazada Stationery & Craft表", Sign = 14, Filename = "lazadaStationeryCraft",
                Sort = 14,
                Remark = "", PlatformId = 7, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 15, Name = "导出Lazada Televisions & Videos表", Sign = 15, Filename = "lazadaTelevisionsVideos",
                Sort = 15, Remark = "", PlatformId = 7, State = BaseModel.StateEnum.Open,
                Type = ExportModel.TypeEnum.Porduct, Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 16, Name = "导出Lazada Tools & Home Improvement表", Sign = 16,
                Filename = "lazadaToolsHomeImprovement",
                Sort = 16, Remark = "", PlatformId = 7, State = BaseModel.StateEnum.Open,
                Type = ExportModel.TypeEnum.Porduct, Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 17, Name = "导出lazada Women's Shoes and Clothing表", Sign = 17,
                Filename = "lazadaWomenShoesClothing",
                Sort = 17, Remark = "", PlatformId = 7, State = BaseModel.StateEnum.Open,
                Type = ExportModel.TypeEnum.Porduct, Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 18, Name = "导出Lazada Audio表", Sign = 18, Filename = "lazadaAudio", Sort = 18, Remark = "",
                PlatformId = 7, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 19, Name = "导出Lazada Beauty表", Sign = 19, Filename = "lazadaBeauty", Sort = 19, Remark = "",
                PlatformId = 7, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 20, Name = "导出Lazada Bedding & Bath表", Sign = 20, Filename = "lazadaBeddingBath", Sort = 20,
                Remark = "", PlatformId = 7, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 21, Name = "导出Lazada Cameras & Drones表", Sign = 21, Filename = "lazadaCamerasDrones", Sort = 21,
                Remark = "", PlatformId = 7, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 22, Name = "导出Lazada Computers & Laptops表", Sign = 22, Filename = "lazadaComputersLaptops",
                Sort = 22,
                Remark = "", PlatformId = 7, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 23, Name = "导出Lazada Data Storage表", Sign = 23, Filename = "lazadaDataStorage", Sort = 23,
                Remark = "",
                PlatformId = 7, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 24, Name = "导出Lazada Digital Goods表", Sign = 24, Filename = "lazaDadigitalGoods", Sort = 24,
                Remark = "", PlatformId = 7, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 25, Name = "导出Lazada Groceries表", Sign = 25, Filename = "lazadaGroceries", Sort = 25, Remark = "",
                PlatformId = 7, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 26, Name = "导出Lazada Health表", Sign = 26, Filename = "lazadaHealth", Sort = 26, Remark = "",
                PlatformId = 7, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 27, Name = "导出Lazada Household Supplies表", Sign = 27, Filename = "lazadaHouseholdSupplies",
                Sort = 27,
                Remark = "", PlatformId = 7, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 28, Name = "导出Lazada Kitchen & Dining表", Sign = 28, Filename = "lazadaKitchenDining", Sort = 28,
                Remark = "", PlatformId = 7, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 29, Name = "导出Lazada Laundry & Cleaning Equipment表", Sign = 29,
                Filename = "lazadaLaundryCleaningEquipment", Sort = 29, Remark = "", PlatformId = 7,
                State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 30, Name = "导出Lazada Lighting & Décor表", Sign = 30, Filename = "lazadaLightingDecor", Sort = 30,
                Remark = "", PlatformId = 7, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 31, Name = "导出Lazada Men's Shoes and Clothing表", Sign = 31, Filename = "lazadaMenShoesClothing",
                Sort = 31, Remark = "", PlatformId = 7, State = BaseModel.StateEnum.Open,
                Type = ExportModel.TypeEnum.Porduct, Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 32, Name = "导出Lazada Monitors & Printers表", Sign = 32, Filename = "lazadaMonitorsPrinters",
                Sort = 32,
                Remark = "", PlatformId = 7, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 33, Name = "导出Lazada Motors表", Sign = 33, Filename = "lazadaMotors", Sort = 33, Remark = "",
                PlatformId = 7, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 34, Name = "导出Lazada Outdoor & Garden表", Sign = 34, Filename = "lazadaOutdoorGarden", Sort = 34,
                Remark = "", PlatformId = 7, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 35, Name = "导出Lazada Pet Supplies表", Sign = 35, Filename = "lazadaPetSupplies", Sort = 35,
                Remark = "",
                PlatformId = 7, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 36, Name = "导出日本雅虎", Sign = 36, Filename = "JapanYahoo", Sort = 36, Remark = "", PlatformId = 10,
                State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 37, Name = "导出智赢Excel", Sign = 37, Filename = "zhiying", Sort = 37, Remark = "", PlatformId = 11,
                State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Private,
                CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 38, Name = "导出Gmarket表", Sign = 38, Filename = "gmarket", Sort = 38, Remark = "", PlatformId = 13,
                State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 39, Name = "导出日本netsea表", Sign = 39, Filename = "JapanNetsea", Sort = 39, Remark = "",
                PlatformId = 15,
                State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 40, Name = "导出Fecshop", Sign = 40, Filename = "fecshop", Sort = 40, Remark = "", PlatformId = 18,
                State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 41, Name = "导出Shopify CSV", Sign = 41, Filename = "shopify", Sort = 41, Remark = "",
                PlatformId = 23,
                State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 42, Name = "导出船长ERPebay表", Sign = 42, Filename = "pushauction", Sort = 42, Remark = "",
                PlatformId = 2,
                State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 43, Name = "导出zencart", Sign = 43, Filename = "zencart", Sort = 43, Remark = "", PlatformId = 25,
                State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 44, Name = "17678004554", Sign = 44, Filename = "temp1", Sort = 44, Remark = "", PlatformId = 28,
                State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Private,
                CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 45, Name = "导出Cdiscount", Sign = 45, Filename = "cdiscount", Sort = 45, Remark = "",
                PlatformId = 31,
                State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 46, Name = "导出Shopee", Sign = 46, Filename = "shopee", Sort = 46, Remark = "", PlatformId = 32,
                State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 47, Name = "导出Korea Erp", Sign = 47, Filename = "koreaerp", Sort = 47, Remark = "",
                PlatformId = 33,
                State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Private,
                CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 48, Name = "导出LINIO", Sign = 48, Filename = "linio", Sort = 48, Remark = "", PlatformId = 30,
                State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 49, Name = "导出店小秘", Sign = 49, Filename = "dianXiaoMiPublic", Sort = 49, Remark = "公共版本",
                PlatformId = 16, State = BaseModel.StateEnum.Close, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public,
                CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 50, Name = "导出趣天", Sign = 50, Filename = "qutian", Sort = -1597642715, Remark = "",
                PlatformId = 34,
                State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 51, Name = "导出Noon电商", Sign = 51, Filename = "noon", Sort = -1597989028, Remark = "",
                PlatformId = 35,
                State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 52, Name = "导出Ebay迷你基础模版", Sign = 52, Filename = "basicMihniTemplate", Sort = -1605060127,
                Remark = "File_Exchange_Basic_Template_Mini_Guide", PlatformId = 2, State = BaseModel.StateEnum.Open,
                Type = ExportModel.TypeEnum.Porduct, Quality = ExportModel.QualityEnum.Public,
                CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 53, Name = "导出nopCommerce电商", Sign = 53, Filename = "nopCommerce", Sort = -1606788524, Remark = "",
                PlatformId = 38, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 54, Name = "导出onbuy电商", Sign = 54, Filename = "onbuy", Sort = -1607753405, Remark = "",
                PlatformId = 39, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 55, Name = "导出Coupang团购", Sign = 55, Filename = "coupang", Sort = -1609725417, Remark = "",
                PlatformId = 40, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 56, Name = "导出Ueeshop跨境独立站", Sign = 56, Filename = "ueeshop", Sort = -1609927813, Remark = "",
                PlatformId = 41, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 57, Name = "导出Ymmbh", Sign = 57, Filename = "ymmbh", Sort = -1618368856, Remark = "",
                PlatformId = 44,
                State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Private,
                CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 58, Name = "导出鑫创", Sign = 58, Filename = "xinchuang", Sort = -1618645670, Remark = "",
                PlatformId = 46,
                State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Private,
                CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 59, Name = "智赢Excel(多变体)", Sign = 59, Filename = "zhiying", Sort = -1621061210, Remark = "",
                PlatformId = 11, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Private,
                CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 60, Name = "独立站", Sign = 60, Filename = "standAloneStation", Sort = -1623309489, Remark = "",
                PlatformId = 47, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Private,
                CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 61, Name = "zood", Sign = 61, Filename = "zood", Sort = -1623742917, Remark = "", PlatformId = 48,
                State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Private,
                CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 62, Name = "导出沃尔玛", Sign = 62, Filename = "wal", Sort = -1631082233, Remark = "", PlatformId = 49,
                State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Private,
                CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 63, Name = "导出Allegro", Sign = 63, Filename = "bulk", Sort = -1631684989, Remark = "",
                PlatformId = 50,
                State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Private,
                CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 64, Name = "导出QuickMaxvic", Sign = 64, Filename = "QuickMaxvic", Sort = -1634111832, Remark = "",
                PlatformId = 51, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Private,
                CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 65, Name = "导出Lazada单国家通用表(新)", Sign = 65, Filename = "lazadaSingleNation", Sort = -1635425161,
                Remark = "", PlatformId = 7, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 66, Name = "导出云盒", Sign = 66, Filename = "YunHe", Sort = -1638874788, Remark = "", PlatformId = 1,
                State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 67, Name = "导出fruugo", Sign = 67, Filename = "fruugo", Sort = -1645615815, Remark = "",
                PlatformId = 53, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 69, Name = "导出Hktvmall", Sign = 74, Filename = "Hktvmall", Sort = -1645615815, Remark = "",
                PlatformId = 55, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
            new ExportModel()
            {
                ID = 70, Name = "导出Bol", Sign = 75, Filename = "Bol", Sort = -1645615815, Remark = "",
                PlatformId = 56, State = BaseModel.StateEnum.Open, Type = ExportModel.TypeEnum.Porduct,
                Quality = ExportModel.QualityEnum.Public, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now
            },
        };
        t.ForEach(n =>
        {
            n.CreatedAt = Now;
            n.UpdatedAt = Now;
        });
        return t;
    }
}