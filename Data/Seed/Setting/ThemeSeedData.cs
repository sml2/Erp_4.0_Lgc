﻿using ERP.Models.Setting;

namespace ERP.Data.Seed.Setting;

public class ThemeSeedData : AbsSeed<Theme, ThemeSeedData>
{
    public override Theme[] Create()
    {
        return new[]
        {
            new Theme() { ID = 1, CreatedAt = Now, UpdatedAt = Now }
        };
    }
}