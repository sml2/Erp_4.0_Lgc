using Common.Enums.ResourcePackage;
using ERP.Models.DB.Packages;

namespace ERP.Data.Seed.Setting;

public class PackagesSeedData : AbsSeed<PackagesModel, PackagesSeedData>
{
    public override PackagesModel[] Create()
    {
        var now = DateTime.Now;
        return new[]
        {
            new PackagesModel() {ID = 1, Name = "智能抠图",Type = PackageTypeEnum.Matting ,Desc = "", CreatedAt = Now, UpdatedAt = Now},
            new PackagesModel() {ID = 2, Name = "文本翻译",Type = PackageTypeEnum.Translation, Desc = "", CreatedAt = Now, UpdatedAt = Now},
        };
    }
}