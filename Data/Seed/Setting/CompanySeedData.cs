﻿using ERP.Extensions;
using ERP.Models.DB.Users;
using static ERP.Models.DB.Users.Company;

namespace ERP.Data.Seed.Setting;
using Model = Company;
using Enums.ID;
using ENUM = Enums.ID.Company;
public class CompanySeedData : AbsSeed<Model, CompanySeedData>
{
    public override Model[] Create()
    {
        var fiveYear = Now.AddYears(5);
        var oneYear = Now.AddYears(1);
        return new Model[]
        {
            new (ENUM.Empty,OEM.MiJingTong,ENUM.Empty)
            {
                ReportId = 2,
                State = States.ENABLE,
                IsChangeCompany = false,
                IsOpenStore = true,
                Validity = fiveYear,
                ReportPids = Newtonsoft.Json.JsonConvert.SerializeObject(Array.Empty<int>()),
                Source = Sources.ONE,
                FirstRechargeDate = DateTime.MinValue,
                ProductAudit = ProductAudits.NO,
                PurchaseAudit = PurchaseAudits.NO,
                WaybillConfirm = WaybillConfirms.YES,
                BillAudit = BillAudits.YES,
                AllowPhone = AllowPhones.YES,
                IsOpenUser = true,
                Type = Types.ONE,
                CreatedAt = Now,
                UpdatedAt = Now,
                PrivateWallet = 0,
                PublicWallet = 0,
                Pids = Newtonsoft.Json.JsonConvert.SerializeObject(Array.Empty<int>())
            },
            new (ENUM.XiaoMi,OEM.MiJingTong,ENUM.Empty)
            {
                ReportId = 2,
                State = States.ENABLE,
                IsChangeCompany = false,
                IsOpenStore = true,
                Validity = fiveYear,
                ReportPids = Newtonsoft.Json.JsonConvert.SerializeObject(Array.Empty<int>()),
                Source = Sources.ONE,
                FirstRechargeDate = DateTime.MinValue,
                ProductAudit = ProductAudits.NO,
                PurchaseAudit = PurchaseAudits.NO,
                WaybillConfirm = WaybillConfirms.YES,
                BillAudit = BillAudits.YES,
                AllowPhone = AllowPhones.YES,
                IsOpenUser = true,
                Type = Types.ONE,
                CreatedAt = Now,
                UpdatedAt = Now,
                PrivateWallet = 0,
                PublicWallet = 0,
                Pids = Newtonsoft.Json.JsonConvert.SerializeObject(Array.Empty<int>())
            },
            // new (ENUM.YongHe,OEM.YongHe,ENUM.Empty)
            // {
            //     ReportId = 2,
            //     State = States.ENABLE,
            //     IsChangeCompany = false,
            //     IsOpenStore = true,
            //     Validity = oneYear,
            //     ReportPids = Newtonsoft.Json.JsonConvert.SerializeObject(Array.Empty<int>()),
            //     Source = Sources.ONE,
            //     FirstRechargeDate = DateTime.MinValue,
            //     ProductAudit = ProductAudits.NO,
            //     PurchaseAudit = PurchaseAudits.NO,
            //     WaybillConfirm = WaybillConfirms.YES,
            //     BillAudit = BillAudits.YES,
            //     AllowPhone = AllowPhones.YES,
            //     IsOpenUser = true,
            //     Type = Types.ONE,
            //     CreatedAt = Now,
            //     UpdatedAt = Now,
            //     PrivateWallet = 0,
            //     PublicWallet = 0,
            //     Pids = Newtonsoft.Json.JsonConvert.SerializeObject(new List<int>(){(int)ENUM.XiaoMi})
            // },
            // new (ENUM.FengNiao,OEM.FengNiao,ENUM.Empty)
            // {
            //     ReportId = 2,
            //     State = States.ENABLE,
            //     IsChangeCompany = false,
            //     IsOpenStore = true,
            //     Validity = oneYear,
            //     ReportPids = Newtonsoft.Json.JsonConvert.SerializeObject(Array.Empty<int>()),
            //     Source = Sources.ONE,
            //     FirstRechargeDate = DateTime.MinValue,
            //     ProductAudit = ProductAudits.NO,
            //     PurchaseAudit = PurchaseAudits.NO,
            //     WaybillConfirm = WaybillConfirms.YES,
            //     BillAudit = BillAudits.YES,
            //     AllowPhone = AllowPhones.YES,
            //     IsOpenUser = true,
            //     Type = Types.ONE,
            //     CreatedAt = Now,
            //     UpdatedAt = Now,
            //     PrivateWallet = 0,
            //     PublicWallet = 0,
            //     Pids = Newtonsoft.Json.JsonConvert.SerializeObject(new List<int>(){(int)ENUM.XiaoMi})
            // }
        };
    }
}