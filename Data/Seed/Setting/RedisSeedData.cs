﻿using ERP.Models.Setting;

namespace ERP.Data.Seed.Setting;

public class RedisSeedData : AbsSeed<Redis, RedisSeedData>
{
    public override Redis[] Create()
    {
        return new[]
        {
            new Redis() {ID = 1, Name = "系统Redis", Host = "127.0.0.1", Port = 6379, CreatedAt = Now, UpdatedAt = Now},
        };
    }
}