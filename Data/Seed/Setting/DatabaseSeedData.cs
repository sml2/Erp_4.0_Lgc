﻿using ERP.Models.Setting;
using ERP.Services.Rule;

namespace ERP.Data.Seed.Setting;

public class DatabaseSeedData : AbsSeed<DataBase, DatabaseSeedData>
{
    public override DataBase[] Create()
    {
        return new[]
        {
            new DataBase() {ID = 1, Name = "系统Rds", Host = "192.168.1.75", Port = 3306, Database = "erp3", Prefix = "erp3_", Username = "root", Password = "root", CreatedAt = Now, UpdatedAt = Now},
        };
    }


}