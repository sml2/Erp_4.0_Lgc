﻿using ERP.Enums;
using static ERP.Models.Abstract.BaseModel;
using ENUM = ERP.Enums.Languages;
using Model = ERP.Models.Setting.Language;

namespace ERP.Data.Seed
{
    public class Language : AbsSeed<Models.Setting.Language, Language>
    {
        public override Models.Setting.Language[] Create() =>  Enum.GetValues<ENUM>()
            .Where(i => i != ENUM.Default)
            .Select(r => new Model(r))
            .ToArray();
        // public override Models.Setting.Language[] Create()
        // {
        //     Models.Setting.Language[] models =  Enum.GetValues<ENUM>().Where(i => i != ENUM.Default).Select(r => new Model(r)).ToArray();
        //     
        //     Models.Setting.Language[] t =
        //     {
        //         new()
        //         {
        //             ID = 1, Name = "中文", Code = "zh", Enum = "1", Flag = Languages.ZH, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 2, Name = "西班牙语", Code = "es", Enum = "1", Flag = Languages.ES, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 3, Name = "意大利语", Code = "it", Enum = "1", Flag = Languages.IT, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 4, Name = "法语", Code = "fr", Enum = "1", Flag = Languages.FR, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 5, Name = "德语", Code = "de", Enum = "1", Flag = Languages.DE, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 6, Name = "日语", Code = "ja", Enum = "1", Flag = Languages.JA, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 7, Name = "英语", Code = "en", Enum = "1", Flag = Languages.EN, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 8, Name = "荷兰语", Code = "nl", Enum = "1", Flag = Languages.NL, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 9, Name = "越南语", Code = "vi", Enum = "1", Flag = Languages.VI, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 10, Name = "葡萄牙语", Code = "pt", Enum = "1", Flag = Languages.PT, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 11, Name = "阿拉伯语", Code = "ar", Enum = "1", Flag = Languages.AR, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 12, Name = "泰语", Code = "th", Enum = "1", Flag = Languages.TH, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 13, Name = "印尼语", Code = "id", Enum = "1", Flag = Languages.ID, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 14, Name = "韩语", Code = "ko", Enum = "1", Flag = Languages.KO, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 15, Name = "马来语", Code = "ms", Enum = "1", Flag = Languages.MS, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 16, Name = "菲律宾语", Code = "tl", Enum = "1", Flag = Languages.TL, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 17, Name = "波兰语", Code = "pl", Enum = "1", Flag = Languages.PL, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 18, Name = "芬兰语", Code = "fi", Enum = "1", Flag = Languages.FI, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 19, Name = "俄语", Code = "ru", Enum = "1", Flag = Languages.RU, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 20, Name = "加泰罗尼亚语", Code = "ca", Enum = "1", Flag = Languages.CA, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 21, Name = "老挝语", Code = "lo", Enum = "1", Flag = Languages.LO, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 22, Name = "希伯来语", Code = "iw", Enum = "1", Flag = Languages.IW, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 23, Name = "尼泊尔语", Code = "ne", Enum = "1", Flag = Languages.NE, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 24, Name = "瑞典语", Code = "sv", Enum = "1", Flag = Languages.SV, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 25, Name = "吉尔吉斯语", Code = "ky", Enum = "1", Flag = Languages.KY, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 26, Name = "丹麦语", Code = "da", Enum = "1", Flag = Languages.DA, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 27, Name = "白俄罗斯语", Code = "be", Enum = "1", Flag = Languages.BE, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 28, Name = "土耳其语", Code = "tr", Enum = "1", Flag = Languages.TR, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 29, Name = "印地语", Code = "hi", Enum = "1", Flag = Languages.HI, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 30, Name = "冰岛语", Code = "is", Enum = "1", Flag = Languages.IS, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 31, Name = "匈牙利语", Code = "hu", Enum = "1", Flag = Languages.HU, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 32, Name = "蒙古语", Code = "mn", Enum = "1", Flag = Languages.MN, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 33, Name = "乌克兰语", Code = "uk", Enum = "1", Flag = Languages.UK, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 34, Name = "爱尔兰语", Code = "ga", Enum = "1", Flag = Languages.GA, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 35, Name = "缅甸语", Code = "my", Enum = "1", Flag = Languages.MY, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 36, Name = "拉丁语", Code = "la", Enum = "1", Flag = Languages.LA, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 37, Name = "希腊语", Code = "el", Enum = "1", Flag = Languages.EL, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 38, Name = "保加利亚语", Code = "bg", Enum = "1", Flag = Languages.BG, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 39, Name = "泰米尔语", Code = "ta", Enum = "1", Flag =Languages.TA, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 40, Name = "立陶宛语", Code = "lt", Enum = "1", Flag = Languages.LT, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 41, Name = "挪威语", Code = "no", Enum = "1", Flag = Languages.NO, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 42, Name = "罗马尼亚语", Code = "ro", Enum = "1", Flag = Languages.RO, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 43, Name = "波斯语", Code = "fa", Enum = "1", Flag = Languages.FA, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //         new()
        //         {
        //             ID = 44, Name = "繁體中文", Code = "zh_tw", Enum = "1", Flag = Languages.ZHTW, CreatedAt = Now,
        //             UpdatedAt = Now
        //         },
        //     };
        //     return t;
        // }
    }
}