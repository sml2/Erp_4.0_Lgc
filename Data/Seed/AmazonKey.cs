namespace ERP.Data.Seed;
using Model = Models.Setting.AmazonKey;
using ModelSP = Models.Setting.AmazonSPKey;
using ModelShopee = Models.Setting.ShopeeKey;

public class AmazonKey : AbsSeed<Model, AmazonKey>
{
    public override Model[] Create()
    {
        Model[] t =
        {
            new()
            {
                ID = 1, Title = "Mendens 北美", CodeNumber = "714467252092", Name = "Mendens 北美",
                AccessKey = "AKIAIOBJK7TY5HT3NFDA", SecretKey = "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",
                Remark = "美国, 加拿大, 墨西哥 卖家记号：AIEX8K1Q94T1Z", CreatedAt = Now, UpdatedAt = Now
            },
            new()
            {
                ID = 2, Title = "Fgsdsdsvc 欧洲", CodeNumber = "931020309560", Name = "Fgsdsdsvc",
                AccessKey = "AKIAJ6RRJ4HKZQIPYJOQ", SecretKey = "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",
                Remark = "英国, 法国, 德国, 意大利, 西班牙,瑞典,沙特阿拉伯,荷兰,阿拉伯联合酋长国", CreatedAt = Now,
                UpdatedAt = Now
            },
            new()
            {
                ID = 3, Title = "miwaimao 北美", CodeNumber = "5953-0499-9010", Name = "xmwm",
                AccessKey = "AKIAJWJBWAQM7UXP53PQ", SecretKey = "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",
                Remark = "美国, 加拿大, 墨西哥 无法获取到详细信息", CreatedAt = Now, UpdatedAt = Now
            },
            new()
            {
                ID = 4, Title = "xiaomiwaimao 欧洲", CodeNumber = "4120-5832-2526", Name = "uxmwm",
                AccessKey = "AKIAJEA5Z4JIMKJ2QSJA", SecretKey = "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",
                Remark = "英国, 法国, 德国, 意大利, 西班牙 无法拉取", CreatedAt = Now, UpdatedAt = Now
            },
            new()
            {
                ID = 5, Title = "无忧猫商家开发者ID", CodeNumber = "3825-8885-4658", Name = "Amour Lighting",
                AccessKey = "AKIAJVXGOZVKYKHK3LFQ", SecretKey = "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",
                Remark = "英国, 法国, 德国, 意大利, 西班牙 已经无法继续绑定", CreatedAt = Now, UpdatedAt = Now
            },
            new()
            {
                ID = 6, Title = "ZYing 澳大利亚", CodeNumber = "7299-3472-3579", Name = "ZYing 澳大利亚",
                AccessKey = "AKIAIDH67SSGYGLPRG7A", SecretKey = "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",
                Remark = "澳大利亚", CreatedAt = Now, UpdatedAt = Now
            },
            new()
            {
                ID = 7, Title = "ZYing 印度", CodeNumber = "3805-2785-6651", Name = "ZYing",
                AccessKey = "AKIAI3QIAFF4BPZZKBQQ", SecretKey = "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",
                Remark = "印度", CreatedAt = Now, UpdatedAt = Now
            },
            new()
            {
                ID = 8, Title = "Bhdjysi 日本", CodeNumber = "112939318357", Name = "Bhdjysi 日本",
                AccessKey = "AKIAJHSKXPKIZQIGMDAQ", SecretKey = "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",
                Remark = "日本", CreatedAt = Now, UpdatedAt = Now
            },
            new()
            {
                ID = 9, Title = "荷兰占位数据", CodeNumber = "0001-0001-0001", Name = "None",
                AccessKey = "AKIAJIYIIATKE3FFC5ZA", SecretKey = "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",
                Remark = "错误秘钥", CreatedAt = Now, UpdatedAt = Now
            },
            new()
            {
                ID = 10, Title = "土耳其占位数据", CodeNumber = "0001-0001-0001", Name = "None",
                AccessKey = "AKIAJIYIIATKE3FFC5ZA", SecretKey = "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",
                Remark = "错误秘钥", CreatedAt = Now, UpdatedAt = Now
            },
            new()
            {
                ID = 11, Title = "阿拉伯联合酋长国占位数据", CodeNumber = "0001-0001-0001", Name = "None",
                AccessKey = "AKIAJIYIIATKE3FFC5ZA", SecretKey = "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",
                Remark = "错误秘钥", CreatedAt = Now, UpdatedAt = Now
            },
            new()
            {
                ID = 12, Title = "中国占位数据", CodeNumber = "0001-0001-0001", Name = "None",
                AccessKey = "AKIAJIYIIATKE3FFC5ZA", SecretKey = "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",
                Remark = "错误秘钥", CreatedAt = Now, UpdatedAt = Now
            },
            new()
            {
                ID = 13, Title = "巴西占位数据", CodeNumber = "0001-0001-0001", Name = "None",
                AccessKey = "AKIAJIYIIATKE3FFC5ZA", SecretKey = "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",
                Remark = "错误秘钥", CreatedAt = Now, UpdatedAt = Now
            },
            new()
            {
                ID = 14, Title = "xiaomi 澳洲", CodeNumber = "112939318357", Name = "xiaomi 澳洲",
                AccessKey = "AKIAJHSKXPKIZQIGMDAQ", SecretKey = "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",
                Remark = "澳大利亚", CreatedAt = Now, UpdatedAt = Now
            },           
        };
        return t;
    }
}



public class AmazonSPKey : AbsSeed<ModelSP, AmazonSPKey>
{
    public override ModelSP[] Create()
    {
        ModelSP[] t =
        {            
            new()
            {
                ID = 1, Name = "spapi",
                AccessKey = "AKIA3IHAFQFE7MZ5ZO4W", SecretKey = "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",
                RoleArn="arn:aws:iam::773564236105:role/LDKJMY",
                ClientId="amzn1.application-oa2-client.5ec4eb25669b4da68dcc5dee4fb63c05", ClientSecret="03c5833d98b6b9ad2beb9e24b937bcb4004e21418f269033295ae10f366b5954",
                ApplicationId="amzn1.sp.solution.122a2528-cbde-452b-9ee5-ac3d9cc3290f",
                //http://localhost:9528/#/AVerify
                RedirectUri="https://auth.liulergou.com",
               CreatedAt = Now, UpdatedAt = Now
            },
        };
        return t;
    }
}

public class ShopeeKey : AbsSeed<ModelShopee, ShopeeKey>
{
    public override ModelShopee[] Create()
    {
        ModelShopee[] t =
        {
            new()
            {
                ID = 1, Name = "shopee",
                PartnerId = 1004247, SecretKey = "3cc5c351e53b40318507d682d1cf08168fdbf367ca10a3b03e3d93bd562767df",               
                RedirectUri="https://auth.liulergou.com",
                CreatedAt = Now, UpdatedAt = Now
            },
        };
        return t;
    }
}
