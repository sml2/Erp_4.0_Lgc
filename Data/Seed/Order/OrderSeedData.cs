﻿namespace ERP.Data.Seed.Order;
using Newtonsoft.Json;
using Enums.Orders;
using Model = Models.DB.Orders.Order;
using static Models.DB.Orders.Order;
using ERP.Enums;

public class OrderSeedData : AbsSeed<Model, OrderSeedData>
{
    public override Model[] Create()
    {
        Good good = new()
        {
            ID = "B08Q8KMVHS",
            Name =
                "kijighg Attack On Titan Anime Figure Eren Mikasa Levi Ackerman Figma Action PVC Figure Collection Model Toy Collection Miglior Regalo",
            Sku = "IT-SP-28",
            ImageURL = string.Empty,
            ItemPrice = new MoneyRecord(new Money(2222486.8986573m),
                new MoneyMeta
                {
                    CurrencyCode = "EUR",
                    Amount = 27.99m,
                    Raw = "27.99",
                    Result = new Money(2222486.8986573m),
                    Rate = 79402.89027m,
                    Success = true,
                    FromType = MoneyMeta.FromTypes.Decimal
                }),
            ShippingPrice = new MoneyRecord(new Money(397014.4513500m),
                new MoneyMeta
                {
                    CurrencyCode = "EUR",
                    Amount = 5.00m,
                    Raw = "5.00",
                    Result = new Money(397014.4513500m),
                    Rate = 79402.89027m,
                    Success = true,
                    FromType = MoneyMeta.FromTypes.Decimal
                }),
            promotionDiscount = new MoneyRecord(new Money(),
                new MoneyMeta
                {
                    CurrencyCode = "EUR",
                    Amount = 0.90m,
                    Raw = "0.00",
                    Result = new Money(0.0m),
                    Rate = 79402.89027m,
                    Success = true,
                    FromType = MoneyMeta.FromTypes.Decimal
                }),
            ItemTax = new MoneyRecord(new Money(400984.5958635m),
                new MoneyMeta
                {
                    CurrencyCode = "EUR",
                    Amount = 5.05m,
                    Raw = "5.05",
                    Result = new Money(400984.5958635m),
                    Rate = 79402.89027m,
                    Success = true,
                    FromType = MoneyMeta.FromTypes.Decimal
                }),
            ShippingTax = new MoneyRecord(new Money(71462.6012430m),
                new MoneyMeta
                {
                    CurrencyCode = "EUR",
                    Amount = 0.90m,
                    Raw = "0.90",
                    Result = new Money(71462.6012430m),
                    Rate = 79402.89027m,
                    Success = true,
                    FromType = MoneyMeta.FromTypes.Decimal
                }),
            ShippingDiscountTax = new MoneyRecord(new Money(),
                new MoneyMeta
                {
                    CurrencyCode = "EUR",
                    Amount = 0.90m,
                    Raw = "0.00",
                    Result = new Money(0.0m),
                    Rate = 79402.89027m,
                    Success = true,
                    FromType = MoneyMeta.FromTypes.Decimal
                }),
            PromotionDiscountTax = new MoneyRecord(new Money(),
                new MoneyMeta
                {
                    CurrencyCode = "EUR",
                    Amount = 0.90m,
                    Raw = "0.00",
                    Result = new Money(0.0m),
                    Rate = 79402.89027m,
                    Success = true,
                    FromType = MoneyMeta.FromTypes.Decimal
                }),
            QuantityOrdered = 103,
            QuantityShipped = 1,
            WaitForSendNum = 102,
            FromURL = "(True, mws.amazonservices.it, System.Text.UTF8Encoding+UTF8EncodingSealed)/dp/B08Q8KMVHS",
            UnitPrice = new MoneyRecord(2222486.8986573m),
            TotalPrice = new MoneyRecord(2619501.3500073m)
        };
        Model testOrder = new()
        {
            ID = 1,
            Bits = BITS.Progresses_Unprocessed | BITS.ShippingType_MFN,
            BuyerInvoice = null,
            BuyerMessage = null,
            BuyerUserName = "mauro",
            Coin = null,
            CompanyID = 2,
            ConsignTime = Convert.ToDateTime("0001-01-01 00:00:00"),
            CreateTime = Convert.ToDateTime("2022-02-24 11:32:41.49"),
            CreateTimeRaw = "2022-02-24T03:32:41.490Z",
            CreatedAt = Convert.ToDateTime("2022-03-23 05:45:02.802274"),
            DefaultShipFromLocationAddress = null,
            DeliverId = 0,
            Delivery = 0,
            DeliveryFail = 0,
            DeliveryNo = 0,
            DeliverySuccess = 0,
            DeliveryTime = null,
            DistributionAddress = null,
            EarliestShipTime = null,
            EntryMode = 0,

            Express = null,
            FakeProductTime = null,
            //Fee_FinancialAffairsID = null,
            //Fee_Money = -877401.9374835,
            //从属实体?
            Fee = new MoneyRecordFinancialAffairs(-877401.9374835M),
            Invoice = null,
            InvoiceNumber = null,
            IsBatchShip = false,
            IsMain = 0,
            LastTime = null,
            LastUpdateDate = Convert.ToDateTime("2022-02-24 18:06:34.4"),
            LatestShipTime = null,
            Loss = new MoneyRecordFinancialAffairs(0.0M),
            MergeId = 0,
            Note = null,
            OEMID = 1,
            OperateUserId = 0,
            OrderNo = "408-4846985-6744356",
            OrderNoHash = 4851312761550333612,
            State = 0,
            OrderTotal = new Data.MoneyRecordFinancialAffairs(2619501.3500073m, new MoneyMeta
            {
                CurrencyCode = "EUR",
                Amount = 32.99m,
                Raw = "32.99",
                Result = new Money(2619501.3500073m),
                Rate = 79402.89027m,
                Success = true,
                FromType = Data.MoneyMeta.FromTypes.Decimal
            }),
            PayTime = Convert.ToDateTime("2022-02-24 11:32:41.49"),
            PayTimeRaw = "2022-02-24T03:32:41.490Z",
            PaymentMethod = "Other",
            Plateform = Platforms.AMAZON,
            ProcessPurchase = 0,
            ProcessWaybill = 0,
            GoodsMoney = new Data.MoneyRecord(2222486.8986573m),
            ProductNum = 1,
            Profit = new Data.MoneyRecordFinancialAffairs(3496903.2874908m),
            PurchaseCompanyId = 0,
            PurchaseFee = new MoneyRecordFinancialAffairs(),
            RefundBill = 0,
            Refund = new Data.MoneyRecordFinancialAffairs(),
            ShippingMoney = new Data.MoneyRecordFinancialAffairs(),
            ShippingType = ShippingTypes.MFN,
            ShopID = "A3P51T51XDLL42",
            StoreId = 3,
            UserID = 3,
            ReceiverID = 1,
            GroupID = 1,
            UpdatedAt = Now,
        };
        testOrder.GoodsScope(gs => { gs.Add(good); });
        return new Model[] { testOrder };
    }
}

