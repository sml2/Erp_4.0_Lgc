﻿namespace ERP.Data.Seed.Order;

using Enums.Orders;
using Model = Models.DB.Orders.Address;
public class Address : AbsSeed<Model, Address>
{
    public override Model[] Create()
    {
        return new Model[]{ new()
        {
            ID = 1,
            Address1 = "Via vicariato 25/f",
            City = "BARBARANO VICENTINO",
            Email = "01jcs0glqq6sm5s@marketplace.amazon.it",
            Name = "Malà?drin vittorio",
            Nation = "意大利",
            NationId = 214,
            NationShort = "IT",
            Phone = "3476424678",
            Province = "Vicenza",
            Zip = "36021",
            OrderID = 1,
            UserID = 3,
            GroupID = 1,
            CompanyID = 2,
            OEMID = 1,
            CreatedAt = Now,
            UpdatedAt = Now,
        } };
    }
}

