
using Microsoft.EntityFrameworkCore;
namespace ERP.Data;

using ERP.Models.Logistics;
using Models.DB.Logistics;

public partial class DBContext //物流
{
    public DbSet<Parameter> LogisticsParam { get; set; } = null!;
    public DbSet<Shipper> LogisticsShipper { get; set; } = null!;
    public DbSet<ShipOrder> ShipOrder { get; set; } = null!;
    public DbSet<ShipTemplate> ShipTemplate { get; set; } = null!;
    public DbSet<ShipRatesEstimationTemplate> ShipRatesEstimationTemplates { get; set; } = null!;
    public DbSet<WaybillLog> WaybillLogs { get; set; } = null!;
    public DbSet<Waybill> Waybill { get; set; } = null!;
    public DbSet<OperationLogging> OperationLogging { get; set; } = null!;

    public DbSet<ShipTypes> ShipTypes { get; set; } = null!;

    public DbSet<ShipTypesLog> ShipTypesLog { get; set; } = null!;

    public DbSet<UserShipType> UserShipType { get; set; } = null!;
}
