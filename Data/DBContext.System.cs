﻿
using ERP.Models;
using ERP.Models.DataStatistics;
using Microsoft.EntityFrameworkCore;
namespace ERP.Data;
using Models.Statistics;
using Models.Setting;
using Models.DB.Setting;
using ERP.Models.DB.Operations;

public partial class DBContext //系统 运维
{
    #region 系统设置
    public DbSet<AmazonKey> AmazonKey { get; set; } = null!;
    public DbSet<AmazonSPKey> AmazonSPKey { get; set; } = null!;
    public DbSet<ShopeeKey> ShopeeKey { get; set; } = null!;
    public DbSet<EmailConfig> EmailConfig { get; set; } = null!;
    public DbSet<Platform> Platform { get; set; } = null!;
    public DbSet<LanguageUnit> LanguageUnit { get; set; } = null!;
    
    public DbSet<Language> Language { get; set; } = null!;
    public DbSet<Nation> Nations { get; set; } = null!;
    public DbSet<Menu> Menu { get; set; } = null!;

    #endregion

    #region 数据监控
    public DbSet<StatisticsChartModel> StatisticsChart { get; set; } = null!;
    public DbSet<CounterRequest> CounterRequest { get; set; } = null!;
    public DbSet<CounterCompanyBusiness> CounterCompanyBusiness { get; set; } = null!;
    public DbSet<CounterGroupBusiness> CounterGroupBusiness { get; set; } = null!;
    public DbSet<CounterUserBusiness> CounterUserBusiness { get; set; } = null!;
    public DbSet<CounterStoreBusiness> CounterStoreBusiness { get; set; } = null!;
    public DbSet<CounterRealFinance> CounterRealFinance { get; set; } = null!;

    #endregion

    #region 运维
    public DbSet<TaskLog> TaskLogOperation { get; set; } = null!;

    #endregion
    
    #region SMS
    public DbSet<SmsCodeModel> SmsCode { get; set; } = null!;

    #endregion

}
