﻿using ERP.Extensions;
using ERP.Interface;
using ERP.Models;
using Microsoft.EntityFrameworkCore;

namespace ERP.Data;

public partial class DBContext : BaseDBContext
{
    private readonly IServiceProvider _serviceProvider;

    public DBContext(DbContextOptions<DBContext> options, IOemProvider oemProvider,IServiceProvider serviceProvider) : base(options, oemProvider)
    {
        _serviceProvider = serviceProvider;
        this.HandleDataChanged();
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        
        modelBuilder.Entity<Models.DB.Orders.Order>().HasOne(a => a.Receiver).WithOne(b => b.Order).HasForeignKey<Models.DB.Orders.Address>(b => b.OrderID);
        
        SeedData(modelBuilder, _serviceProvider.CreateScope().ServiceProvider);
    }
}
