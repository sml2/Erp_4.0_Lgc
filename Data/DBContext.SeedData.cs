﻿using ERP.Data.Seed.Order;
using ERP.Data.Seed.Setting;
using ERP.Data.Seed.Users;
using ERP.Interface.Rule;
using ERP.Services.Rule;
using Microsoft.EntityFrameworkCore;

namespace ERP.Data;
public partial class DBContext
{

    private static void SeedData(ModelBuilder modelBuilder, IServiceProvider serviceProvider)
    {
        AbsSeed.SetStatic(serviceProvider);
        DatabaseSeedData.Seed(modelBuilder);
        RedisSeedData.Seed(modelBuilder);
        FileBucketSeedData.Seed(modelBuilder);
        ThemeSeedData.Seed(modelBuilder);
        OemSeedData.Seed(modelBuilder);
        CompanySeedData.Seed(modelBuilder);
        GroupSeedData.Seed(modelBuilder);
        UserValiditySeedData.Seed(modelBuilder);
        RuleTemplateSeedData.Seed(modelBuilder);
        UserSeedData.Seed(modelBuilder);
        CompanyOemSeedData.Seed(modelBuilder);
        // Seed.Order.Address.Seed(modelBuilder);
        // OrderSeedData.Seed(modelBuilder);
        //Context.SeedData.Logistics.Seed(modelBuilder);
        //Context.SeedData.LogisticsParameter.Seed(modelBuilder);
        Seed.PlatformData.Seed(modelBuilder);
        Seed.AmazonKey.Seed(modelBuilder);
        Seed.AmazonSPKey.Seed(modelBuilder);
        Seed.EmailConfig.Seed(modelBuilder);
        Seed.Language.Seed(modelBuilder);
        Seed.Nation.Seed(modelBuilder);
        Seed.Platform.Seed(modelBuilder);
        Seed.Unit.Seed(modelBuilder);
        Seed.Store.Seed(modelBuilder);
        Seed.MenuData.Seed(modelBuilder);
        RoleSeedData.Seed(modelBuilder);
        RoleClaim.Seed(modelBuilder);
        UserClaim.Seed(modelBuilder);
        UserRoleSeedData.Seed(modelBuilder);
        ExportExcelSeedData.Seed(modelBuilder);
        PackagesSeedData.Seed(modelBuilder);
    }
}
