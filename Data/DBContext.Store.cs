
using ERP.Models.DB.Setting;
using Microsoft.EntityFrameworkCore;
namespace ERP.Data;

using Models.DB.Users;
using Models.DB.Stores;
using Models.Export;
using ERP.Models.Stores;
using ERP.Models.Setting;

public partial class DBContext //店铺
{
    public DbSet<RemoteModel> Remote { get; set; } = null!;   
    public DbSet<SyncUploadTask> SyncUploadTasks { get; set; } = null!;
    public DbSet<CategoryNodeDefault> categoryNodeDefaults { get; set; } = null!;
    public DbSet<RecommendedBrowseNodes> RecommendedBrowseNode { get; set; } = null!;
    public DbSet<UserStore> UserStore { get; set; } = null!;

    public DbSet<StoreRegion> StoreRegion { get; set; } = null!;

    public DbSet<StoreMarket> StoreMarket { get; set; } = null!;

    public DbSet<AsinProduct> AsinProduct { get; set; } = null!;
    
    public DbSet<EanupcModel> Eanupc { get; set; } = null!;


    #region 导出
    public DbSet<ExportModel> Export { get; set; } = null!;
    public DbSet<ExportProduct> ExportProduct { get; set; } = null!;
    public DbSet<SyncExport> SyncExport { get; set; } = null!;
    public DbSet<SyncExportProduct> SyncExportProduct { get; set; } = null!;
    public DbSet<SyncProductModel> SyncProduct { get; set; } = null!;
    public DbSet<ExportTask> ExportTasks { get; set; } = null!;

    #endregion

}
