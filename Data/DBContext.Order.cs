
using Microsoft.EntityFrameworkCore;
namespace ERP.Data;

using Models.DB.Orders;

public partial class DBContext //订单
{
    public DbSet<Order> Order { get; set; } = null!;
    public DbSet<OrderDistributedLink> OrderDistributedLinks { get; set; } = null!;
    public DbSet<Address> Address { get; set; } = null!;
    public DbSet<OrderGoodSku> OrderGoodSku { get; set; } = null!;
    public DbSet<OrderGoodId> OrderGoodId { get; set; } = null!;
    public DbSet<Action> OrderAction { get; set; } = null!;
    public DbSet<Remark> OrderRemark { get; set; } = null!;
    public DbSet<Merge> OrderMerge { get; set; } = null!;
    public DbSet<Refund> OrderRefund { get; set; } = null!;

    public DbSet<SyncLogisticInfoLogModel> SyncLogisticInfoLog { get; set; } = null!;
}
