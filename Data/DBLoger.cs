﻿using Microsoft.EntityFrameworkCore.Diagnostics;

namespace ERP.Data;

public class DBLoger:IDbContextLogger
{
    
    public void Log(EventData eventData)
    {
        Console.WriteLine(eventData.ToString());
    }

    public bool ShouldLog(EventId eventId, LogLevel logLevel) => true;
}