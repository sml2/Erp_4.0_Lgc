﻿
using ERP.Models.Message;
using Microsoft.EntityFrameworkCore;
namespace ERP.Data;
using Models.Statistics;
using Models.Setting;

public partial class DBContext //系统 运维
{
    public DbSet<EmailInfo> EmailInfos { get; set; } = null!;
    public DbSet<EmailMass> EmailMasses { get; set; } = null!;

    public DbSet<Notice> Notices { get; set; } = null!;
    public DbSet<FeedBack> FeedBacks { get; set; } = null!;
    public DbSet<FeedBackReply> FeedBackReplies { get; set; } = null!;
    public DbSet<WorkOrder> WorkOrders { get; set; } = null!;
    public DbSet<WorkOrderCategory> WorkOrderCategories { get; set; } = null!;
    public DbSet<WorkOrderReply> WorkOrderReplies { get; set; } = null!;
    public DbSet<WorkOrderProgress> WorkOrderProgresses { get; set; } = null!;
}
