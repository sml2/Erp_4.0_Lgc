﻿using System.ComponentModel.DataAnnotations.Schema;
using ERP.Models.Finance;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
namespace ERP.Data;
/// <summary>
/// 货币及其财务归档
/// </summary>
[Owned]
public record MoneyFinancialAffairs
{
    public static MoneyFinancialAffairs Empty => new();

    public MoneyFinancialAffairs() { }
    public MoneyFinancialAffairs(decimal money) => Money = money;
    public MoneyFinancialAffairs(Money money) => Money = money;
    public MoneyFinancialAffairs(Money money, int fid) : this(money)
    {
        FinancialAffairsID = fid;
    }
    public MoneyFinancialAffairs(Money money, FinancialAffairsModel financialAffairs) : this(money)
    {
        FinancialAffairs = financialAffairs;
    }
    /// <summary>
    /// 基准货币
    /// </summary>
    public Money Money { get; init; } = Money.Zero;
    /// <summary>
    /// 财务归档
    /// </summary>
    public int? FinancialAffairsID { get; init; }

    public FinancialAffairsModel? FinancialAffairs { get; init; } = default;

    public static MoneyFinancialAffairs operator +(MoneyFinancialAffairs mr, decimal v) => new(mr.Money + v);
    public static MoneyFinancialAffairs operator +(MoneyFinancialAffairs mr, decimal? v) => mr + v.GetValueOrDefault();
    public static MoneyFinancialAffairs operator -(MoneyFinancialAffairs mr, decimal v) => new(mr.Money - v);
    public static MoneyFinancialAffairs operator -(MoneyFinancialAffairs mr, decimal? v) => mr - v.GetValueOrDefault();
    public static implicit operator MoneyFinancialAffairs(decimal v) => new(v);
    public static implicit operator MoneyFinancialAffairs(Money v) => new(v);

    public override string ToString()
    {
        return Money.ToString();
    }
    public string ToString(string unitSign)=>ToString(unitSign, true);
    public string ToString(string unitSign, bool showUnit)
    {
        return $"{Money.To(unitSign):0.00}{(showUnit ? $" {unitSign}" : "")}";
    }
    public string ToString(bool showUnit = true)
    {
        return Money.ToString();
    }

}