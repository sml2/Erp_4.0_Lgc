﻿using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
namespace ERP.Data;
/// <summary>
/// 货币变化量
/// </summary>
[Owned]
public record MoneyDelta
{
    public Money Old { get; init; }
    public Money New { get; init; }
    public Money Delta { get; init; }

    public MoneyDelta(Money @old, Money @new)
    {
        Old = @old;
        New = @new;
        Delta = @new - @old;
    }

}