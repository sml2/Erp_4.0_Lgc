﻿using Ryu.Data.Abstraction;
namespace ERP.Data;
using Cache = Services.Caches.Unit;

public class MoneySeeder : ISeeder
{
    private readonly Cache cache;

    public MoneySeeder(Cache cache) {
        this.cache = cache;
    }
    public IDictionary<string, decimal> Init()
    {
        return cache.List()!.ToDictionary(o => o.Sign,o=> o.Rate);
    }
}