using System.ComponentModel.DataAnnotations;

namespace ERP.Data.Products;

public class FieldTypeNameAttribute : ValidationAttribute
{
    public string Name { get; set; }
    
    public FieldTypeNameAttribute(string Name)
    {
        this.Name = Name;
    }
}