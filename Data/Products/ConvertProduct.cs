using System.Text;
using ERP.Controllers.Product.Client.Upload;
using ERP.Enums;
using ERP.Interface;
using ERP.Models.DB.Product;
using ERP.Models.Product;
using ERP.Models.Setting;
using ERP.Models.Stores;
using ERP.Models.View.Products.Product;

namespace ERP.Data.Products;

public class ConvertProduct
{
    public ConvertProduct()
    {
    }


    public Product? ToBeServiceStruct(EditProductItemDto vm, string? carrie = null)
    {
        //前端产品表结构转为为后端结构
        Product product = new();
        //Version
        product.Version = product.CurrentVersion;
        //Base
        product.SetSource(vm.Common.Source);
        product.SetQuantity(vm.Info.Quantity);
        product.SetPid(vm.Common.Pid ?? string.Empty);
        product.SetPrices(vm.Info.Prices);
        product.SetCode(vm.Common.Code);
        product.SetAsin(vm.Common.Asin);
        product.SetSku(vm.Common.Sku);
        product.SetDescriptionType(vm.Info.DescType);

        if (carrie is not null)
        {
            product.SetCarrie(carrie);
        }

        var edit = vm.Common.EditorImages;
        //editorImages
        if (vm.Common.Images != null)
        {
            var editorImages = vm.Common.Images?.Where(m => m.IsEditor).ToDictionary(m => m.ID,m => m);
            if (editorImages != null)
            {
                foreach (var i in editorImages)
                {
                    var imgId = i.Key;
                    var item = i.Value;
                    if (vm.Info.Desc != null && !vm.Info.Desc.Contains(item.Path!))
                    {
                        var temp = vm.Common.Images?.Where(m => m.ID == imgId).FirstOrDefault();
                        if (temp != null)
                        {
                            var index = vm.Common.Images?.IndexOf(temp);
                            if (index.HasValue)
                            {
                                vm.Common.Images![index.Value].IsEditor = false;
                            }
                        
                        }
                    }
                }
            }
            
            
        }
      

        //Images        
        product.Images = vm.Common.Images?.ToImages();


        if (vm.Info.Title is null)
        {
            return null;
        }

        product.SetTitle(vm.Info.Title);
        product.SetKeyword(vm.Info.Keyword);
        product.SetDescription(vm.Info.Desc);

        var sketches = new List<string>();
        if (vm.Info.Sketch != null)
        {
            foreach (var i in vm.Info.Sketch)
            {
                sketches.Add(i["value"]);
            }
        }

        product.SetSketches(sketches);


        
        //Attribute
        var attributes = vm.Info.Attributes;
        Attributes productAttributes = new Attributes();

        productAttributes.IsSingle = vm.Info.IsSingle;


        if (attributes is null)
        {
            return null;
        }

        foreach (var i in attributes)
        {
            productAttributes.Add(i.Key, attributes[i.Key].Name, attributes[i.Key].Value, attributes[i.Key].State);
        }


        if (vm.Info.AttributeTag == 0)
        {
            return null;
        }

        productAttributes.CurCreateNum = vm.Info.AttributeTag;

        //Variants
        Variants productVariants = new Variants();
        var variants = vm.Info.Variants;

        if (variants is null)
        {
            return null;
        }
        foreach (var i in variants)
        {
            if (vm.VariantImageRmIds.Count > 0)
            {
                var VariantImageRmIds = vm.VariantImageRmIds;

                if (i.Images.Main.HasValue)
                {
                    var isExist = VariantImageRmIds.Any(m => m == i.Images.Main.Value);
                    if (isExist)
                        i.Images.Main = null;
                }

                if (i.Images.Affiliate != null && i.Images.Affiliate.Count > 0)
                {
                    var affiliate = new List<int>();
                    for (int j = 0; j < i.Images.Affiliate.Count; j++)
                    {
                        var imgId = i.Images.Affiliate[j];
                        var isExist = VariantImageRmIds.Any(m => m == imgId);
                        if (!isExist)
                        {
                            affiliate.Add(imgId); 
                        }
                    }
                    i.Images.Affiliate = affiliate;
                }
                
            }
            //生成sid
            if (string.IsNullOrEmpty(i.Sid))
            {
                var variantName = GetVariantName(productAttributes!, i.Ids);
                i.Sid = Helpers.CreateHashCode(variantName).ToString();
            }
            
            productVariants.Add(i, vm.Info.Coin);
        }

        product.Variants = productVariants;
        product.Variants.Attributes = productAttributes;
        

        if (vm.Info.Features is not null)
        {
            //特性
            foreach (var item in vm.Info.Features)
            {
                product.Properties[item.PropertyKey].Value = item.Value;
            }
        }


        return product;
    }

    public EditProductItemDto ToBeProViewModel(Product productStruct, ProductModel productModel,
        ProductItemModel itemModel, List<ProductItemModel> listItemModels, string unit, bool productShowUnit,
        IOemProvider oemProvider)
    {
        var viewModel = new ProductViewModel(productModel, itemModel, productStruct,unit,productShowUnit);
        viewModel.Info.Desc = string.IsNullOrEmpty(productStruct.GetDescription())
            ? string.Empty
            : GetProductDesc(productStruct.GetDescription()!, oemProvider);
        // viewModel.Info.Coin = productShowUnit ? itemModel.Coin.IsNotEmpty() ? itemModel.Coin : unit : unit;
            // itemModel.Coin.IsNotEmpty() ? itemModel.Coin : unit;
        // viewModel.Common.Coin = productShowUnit ? itemModel.Coin.IsNotEmpty() ? itemModel.Coin : unit : unit;
            // productModel.Coin.IsNotEmpty() ? productModel.Coin : unit;
        // viewModel.Info.Prices = productStruct.GetPrices()!;
        viewModel.LanguagePacks = GetLanguagePacks(listItemModels);
        return viewModel;
    }

    public EditProductItemDto ToBeProViewModel(Product productStruct, ProductModel productModel,
        ProductItemModel itemModel, string unit, IOemProvider oemProvider)
    {
        var viewModel = new ProductViewModel(productModel, itemModel, productStruct,unit);
        viewModel.Info.Desc = string.IsNullOrEmpty(productStruct.GetDescription())
            ? string.Empty
            : GetProductDesc(productStruct.GetDescription()!, oemProvider);
        viewModel.Info.Coin = itemModel.Coin.IsNotEmpty() ? itemModel.Coin : unit;
        viewModel.Common.Coin = productModel.Coin.IsNotEmpty() ? productModel.Coin : unit;
        viewModel.Info.Prices = productStruct.GetPrices()!;
        return viewModel;
    }

    public EditProductItemDto ToBeProViewModelWithHistory(Product productStruct, ProductModel productModel,
        ProductItemModel itemModel, string unit, IOemProvider oemProvider)
    {
        var viewModel = new ProductHistoryViewModel(productModel, itemModel, productStruct,unit)
        {
            Info =
            {
                Desc = string.IsNullOrEmpty(productStruct.GetDescription())
                    ? string.Empty
                    : GetProductDesc(productStruct.GetDescription()!, oemProvider),
                Coin = itemModel.Coin.IsNotEmpty() ? itemModel.Coin : unit,
                Prices = productStruct.GetPrices()!
            },
            Common =
            {
                Coin = productModel.Coin.IsNotEmpty() ? productModel.Coin : unit
            }
        };
        return viewModel;
    }

    private Dictionary<string, ProductViewModel.Packs> GetLanguagePacks(List<ProductItemModel> listItem)
    {
        var packs = new Dictionary<string, ProductViewModel.Packs>();
        foreach (var item in listItem)
        {
            var language = Enum.Parse<Languages>(item.LanguageName, true);
            var pack = new ProductViewModel.Packs();
            pack.ItemId = item.ID;
            if (language == Languages.Default)
            {
                pack.Value = 0;
            }
            else
            {
                pack.Value = (int)Math.Log2((long)language) + 1;
            }

            if (pack.Value != item.LanguageId)
            {
                throw new Exceptions.Product.NotFoundException("数值不匹配");
            }

            pack.Label = language.GetDescriptionByKey("Name");
            packs.Add(language.GetDescriptionByKey("Code"), pack);
        }

        return packs;
    }


    public string GetProductDesc(string description, IOemProvider oemProvider)
    {
        var oem = oemProvider.OEM;
        return description.Replace("{ImgDomain}", $"https://{oem?.Oss?.BucketDomain}");
    }
    
    public string GetProductDesc(string description, OEM oem)
    {
        return description.Replace("{ImgDomain}", $"https://{oem.Oss?.BucketDomain}");
    }

    public string GetImageUrl(ERP.Data.Products.Image image, IOemProvider oemProvider)
    {
        if (image.IsNetwork)
        {
            return image.Url;
        }

        var oem = oemProvider.OEM;
        return $"https://{oem?.Oss?.BucketDomain}/{image.Path}";
    }

    public string GetImageUrl(string src, IOemProvider oemProvider)
    {
        if (src.StartsWith("http"))
        {
            return src;
        }

        var oem = oemProvider.OEM;
        return $"https://{oem?.Oss?.BucketDomain}/{src}";
    }

    public string GetAttributeName(Dictionary<int, Attribute> attributes, Dictionary<int, int> ids)
    {
        var temp = new List<string>();
        foreach (var i in ids)
        {
            temp.Add(attributes[i.Key].Value[i.Value]);
        }

        return string.Join(",", temp);
    }

    public string MakeVariantTitle(string title, string attributeName)
    {
        return GetVariantTitle(SyncProductModel.TitleSuffixs.FALSE, title, attributeName);
    }

    public string GetVariantTitle(SyncProductModel.TitleSuffixs titleSuffix, string title, string attributeName)
    {
        return titleSuffix == SyncProductModel.TitleSuffixs.FALSE ? title : $"{title}+{attributeName}";
    }

    public string GetFullImageUrl(int imgId, Images images, IOemProvider oemProvider)
    {
        var url = images.FirstOrDefault(m => m.ID == imgId);

        if (url is null)
        {
            return String.Empty;
        }

        return GetImageUrl(url, oemProvider);
    }

    public List<string> GetFullImageUrl(List<int> imgIds, Images images, IOemProvider oemProvider)
    {
        return images.Where(m => imgIds.Contains(m.ID)).Select(m => GetImageUrl(m, oemProvider)).ToList();
    }

    public Dictionary<string, string[]> RecombinationAttr(Dictionary<int, Attribute> attributes)
    {
        var data = new Dictionary<string, string[]>();
        foreach (var attribute in attributes)
        {
            data.Add(attribute.Value.Name, attribute.Value.Value);
        }

        return data;
    }

    public string GetImageUrl(int imgId, Images images, IOemProvider oemProvider)
    {
        var image = images.Where(m => m.ID == imgId).FirstOrDefault();
        if (image == null)
        {
            return string.Empty;
        }

        return image.IsNetwork ? image.Url : $"https://{oemProvider.OEM?.Oss?.BucketDomain}/{image.Path}";
    }

    public List<string> GetImageUrl(List<int> imgIds, Images images, IOemProvider oemProvider)
    {
        var multiple = new List<string>();
        var selectImages = images.Where(m => imgIds.Contains(m.ID)).ToList();
        foreach (var image in selectImages)
        {
            multiple.Add(image.IsNetwork ? image.Url : $"https://{oemProvider.OEM?.Oss?.BucketDomain}/{image.Path}");
        }

        return multiple;
    }

    public List<string> GetImageUrl(Images images, IOemProvider oemProvider)
    {
        var multiple = new List<string>();
        foreach (var image in images)
        {
            multiple.Add(image.IsNetwork ? image.Url : $"https://{oemProvider.OEM?.Oss?.BucketDomain}/{image.Path}");
        }

        return multiple;
    }

    public Dictionary<string, string> GetAttributeNameWithUpload(Dictionary<int, Attribute> attributes,
        Dictionary<int, int> ids)
    {
        var temp = new Dictionary<string, string>();
        foreach (var i in ids)
        {
            temp.Add(attributes[i.Key].Name, attributes[i.Key].Value[i.Value]);
        }

        return temp;
    }

    public Product? ToBeServiceStruct(UploadDto vm, string unit, List<Image> images, Variants variants,
        List<Controllers.Product.Client.Upload.Attribute> attributes)
    {
        Product product = new();
        product.SetSource(vm.Url);
        product.SetQuantity(vm.Products?.Sum(m => m.Quantity) ?? 0);
        product.SetPid(vm.PID);
        product.SetSku(vm.Sku);
        //Pirce
        var prices = new Price();
        prices.SetMax(variants.Max(m => m.Cost.To(unit)), unit);
        prices.SetMin(variants.Max(m => m.Cost.To(unit)), unit);
        product.SetPrices(new Prices()
        {
            Cost = prices,
            Sale = prices,
        });
        product.SetCarrie(unit);

        //Image
        product.Images = images.ToImages();
        var firstVariant = variants.First();
        product.MainImage = product.Images != null && firstVariant.Images.Main.HasValue
            ? product.Images.FirstOrDefault(m => m.ID == firstVariant.Images.Main.Value)
            : null;

        //Basic
        product.SetTitle(vm.Name);
        product.SetKeyword(vm.Keywords);
        product.SetDescription(vm.Description);
        product.SetSketches(Helpers.TransitionBr(vm.Sketch).Split("\r\n").ToList());

        //Variants
        product.Variants = variants;

        //Attribute
        // var attributes = vm.Attributes ?? new List<Controllers.Product.Client.Upload.Attribute>();
        Attributes productAttributes = new Attributes();
        // productAttributes.IsSingle = vm.Info.IsSingle;
        foreach (var i in attributes)
        {
            productAttributes.Add(i.Id, i.Name, i.Values.ToArray(), AttributeStateEnum.Enable);
        }

        var tag = attributes.Max(m => m.Id);
        productAttributes.CurCreateNum = tag == 0 ? 1 : tag;
        product.Variants.Attributes = productAttributes;
        return product;
    }

    public Dictionary<int, Attribute> ReplaceAttributes(Dictionary<int, Attribute> items,
        Dictionary<string, string> map)
    {
        var attributes = items;
        foreach (var attr in attributes)
        {
            //Name
            if (map.ContainsKey(attr.Value.Name))
                attr.Value.Name = map[attr.Value.Name];
            //Value
            var indexs = new List<int>();
            var values = attr.Value.Value;
            for (int i = 0; i < values.Length; i++)
            {
                if (map.ContainsKey(values[i]))
                    indexs.Add(i);
            }

            foreach (var i in indexs)
            {
                attr.Value.Value[i] = map[attr.Value.Value[i]];
            }
        }

        return items;
    }

    public string GetVariantName(Data.Products.Attributes attributes,List<Ids> ids)
    {
        var str = new StringBuilder();
        foreach (var item in ids)
        {
            if (attributes.items.TryGetValue(item.Tag, out var attr) && attr.Value.Length > item.Val)
            {
                str.Append($"{attr.Name}:{attr.Value[item.Val]},");
            }
        }

        if (str.Length > 0)
        {
            str.Remove(str.Length - 1, 1);
        }

        return str.ToString();
    }

    public string GetVariantName(List<Controllers.Product.Client.Upload.Attribute> attributes, Dictionary<int, int> ids)
    {
        var str = new StringBuilder();
        foreach (var item in ids)
        {
            str.Append($"{attributes[item.Key].Name}:{attributes[item.Key].Values[item.Value]},");
        }
        if (str.Length > 0)
        {
            str.Remove(str.Length - 1, 1);
        }

        return str.ToString();
    }

    public string GetVariantName(Attributes attributes, Dictionary<int, int> ids)
    {
        var str = new StringBuilder();
        foreach (var item in ids)
        {
            str.Append($"{attributes.items[item.Key].Name}:{attributes.items[item.Key].Value[item.Value]},");
        }

        if (str.Length > 0)
        {
            str.Remove(str.Length - 1, 1);
        }

        return str.ToString();
    }
}