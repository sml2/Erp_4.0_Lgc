﻿using ERP.Enums.Product;

namespace ERP.Data.Products;

public class Properties : Dictionary<string, IProperty>
{
    //快速索引key的操作
    //this[languages,vguid]
    private void CheckKey(string key)
    {
        if (!ContainsKey(key)) Add(key, new Property(key));
    }

    public new IProperty this[string key]
    {
        get
        {
            CheckKey(key);
            return base[key];
        }
        set
        {
            CheckKey(key);
            base[key] = value;
        }
    }

    /// <summary>
    /// pid: amazon/sp  asin
    /// </summary>
    public string? Pid
    {
        get => (string?)this[PropertyKey.Pid].Value;
        set => this[PropertyKey.Pid].Value = value;
    }


    /// <summary>
    /// 标题
    /// </summary>
    public string Title
    {
        get => (string)this[PropertyKey.Title].Value!;
        set => this[PropertyKey.Title].Value = value;
    }

    /// <summary>
    /// 关键词
    /// </summary>
    public string? Keyword
    {
        get => (string?)this[PropertyKey.Keyword].Value;
        set => this[PropertyKey.Keyword].Value = value;
    }

    /// <summary>
    /// 描述
    /// </summary>
    public string? Description
    {
        get => (string?)this[PropertyKey.Description].Value;
        set => this[PropertyKey.Description].Value = value;
    }

    /// <summary>
    /// 卖点
    /// </summary>
    public List<string>? Sketches
    {
        get => (List<string>?)this[PropertyKey.Sketches].Value;
        set => this[PropertyKey.Sketches].Value = value;
    }

    /// <summary>
    /// 一级分类
    /// </summary>
    public int? CategoryId
    {
        get => Convert.ToInt32(this[PropertyKey.CategoryId].Value);
        set => this[PropertyKey.CategoryId].Value = value;
    }

    /// <summary>
    /// 二级分类
    /// </summary>
    public int? SubCategoryId
    {
        get => Convert.ToInt32(this[PropertyKey.SubCategoryId].Value);
        set => this[PropertyKey.SubCategoryId].Value = value;
    }

    /// <summary>
    /// 来源
    /// </summary>
    public string? Source
    {
        get => (string?)this[PropertyKey.Source].Value;
        set => this[PropertyKey.Source].Value = value;
    }
    

    public string? Sku
    {
        get => (string?)this[PropertyKey.Sku].Value;
        set => this[PropertyKey.Sku].Value = value;
    }
    

    public string? Asin
    {
        get => (string?)this[PropertyKey.Asin].Value;
        set => this[PropertyKey.Asin].Value = value;
    }
    
 
    public string? Sid
    {
        get => (string?)this[PropertyKey.Sid].Value;
        set => this[PropertyKey.Sid].Value = value;
    }
    

    public string? Code
    {
        get => (string?)this[PropertyKey.Code].Value;
        set => this[PropertyKey.Code].Value = value;
    }
    
    /// <summary>
    /// Code
    /// </summary>
    public ProductUploadEnum? UploadState
    {
        get => (ProductUploadEnum?)this[PropertyKey.UploadState].Value;
        set => this[PropertyKey.UploadState].Value = value;
    }

    /// <summary>
    /// 库存
    /// </summary>
    public int Quantity
    {
        get => Convert.ToInt32(this[PropertyKey.Quantity].Value);
        set => this[PropertyKey.Quantity].Value = value;
    }

    /// <summary>
    /// 平台
    /// </summary>
    public Platforms? Platform
    {
        get => (Platforms?)this[PropertyKey.Platform].Value ?? Platforms.None;
        set => this[PropertyKey.Platform].Value = value;
    }

    /// <summary>
    /// 货币符号
    /// </summary>
    public string? Carrie
    {
        get => (string?)this[PropertyKey.Carrie].Value;
        set => this[PropertyKey.Carrie].Value = value;
    }

    /// <summary>
    /// 金额
    /// </summary>
    public Money? Price
    {
        get => (Money?)this[PropertyKey.Price].Value;
        set => this[PropertyKey.Price].Value = value;
    }

    public Prices? Prices
    {
        get => (Prices?)this[PropertyKey.Prices].Value;
        set => this[PropertyKey.Prices].Value = value;
    }

    public ulong? PidHashCode
    {
        get => (ulong?)this[PropertyKey.PidHashCode].Value;
        set => this[PropertyKey.PidHashCode].Value = value;
    }


    public long TypeFlag
    {
        get => (long)this[PropertyKey.TypeFlag].Value;
        set => this[PropertyKey.TypeFlag].Value = value;
    }

    public long LanguageFlag
    {
        get => (long)this[PropertyKey.LanguageFlag].Value;
        set => this[PropertyKey.LanguageFlag].Value = value;
    }


    public void SetPrice(string priceInfo)
    {
        if (Money.TryParse(priceInfo, out var p, out var u, out var r))
        {
            Carrie = u;
            Price = p;
        }
        else
        {
            throw new InvalidCastException($"can not cast price [priceInfo:{priceInfo}]#48");
        }
    }

    public void SetPrice(string carrie, double price)
    {
        if (Money.TryParse(price.ToString(), carrie, out var p, out var r))
        {
            Carrie = carrie;
            Price = p;
        }
        else
        {
            throw new InvalidCastException($"can not cast price [carrie:{carrie},price:{price}]#60");
        }
    }

    #region 属性

    /// <summary>
    /// 电池
    /// </summary>
    public bool Battery
    {
        get => (bool)this[PropertyKey.Battery].Value;
        set => this[PropertyKey.Battery].Value = value;
    }

    /// <summary>
    /// 颜色
    /// </summary>
    public string Color
    {
        get => (string)this[PropertyKey.Color].Value;
        set => this[PropertyKey.Color].Value = value;
    }

    /// <summary>
    /// 尺码
    /// </summary>
    public string Size
    {
        get => (string)this[PropertyKey.Size].Value;
        set => this[PropertyKey.Size].Value = value;
    }

    /// <summary>
    /// 重量(克)
    /// </summary>
    public decimal Weight
    {
        get => Convert.ToDecimal(this[PropertyKey.Weight].Value);
        set => this[PropertyKey.Weight].Value = value;
    }

    /// <summary>
    /// 长度(cm)
    /// </summary>
    public decimal Length
    {
        get => Convert.ToDecimal(this[PropertyKey.Length].Value);
        set => this[PropertyKey.Length].Value = value;
    }

    /// <summary>
    /// 宽度（cm）
    /// </summary>
    public decimal Width
    {
        get => Convert.ToDecimal(this[PropertyKey.Width].Value);
        set => this[PropertyKey.Width].Value = value;
    }

    /// <summary>
    /// 高度（cm）
    /// </summary>
    public decimal Height
    {
        get => Convert.ToDecimal(this[PropertyKey.Height].Value);
        set => this[PropertyKey.Height].Value = value;
    }

    /// <summary>
    /// 原产国
    /// </summary>
    public string Origin
    {
        get => (string)this[PropertyKey.Origin].Value;
        set => this[PropertyKey.Origin].Value = value;
    }

    /// <summary>
    /// 品牌
    /// </summary>
    public string Brand
    {
        get => (string)this[PropertyKey.Brand].Value;
        set => this[PropertyKey.Brand].Value = value;
    }

    /// <summary>
    /// 制造商
    /// </summary>
    public string Facturer
    {
        get => (string)this[PropertyKey.Facturer].Value;
        set => this[PropertyKey.Facturer].Value = value;
    }

    /// <summary>
    /// 厂商编号
    /// </summary>
    public string Number
    {
        get => (string)this[PropertyKey.Number].Value;
        set => this[PropertyKey.Number].Value = value;
    }

    /// <summary>
    /// 材料
    /// </summary>
    public string Material
    {
        get => (string)this[PropertyKey.Material].Value;
        set => this[PropertyKey.Material].Value = value;
    }

    /// <summary>
    /// 珠宝类型
    /// </summary>
    public string Gem
    {
        get => (string)this[PropertyKey.Gem].Value;
        set => this[PropertyKey.Gem].Value = value;
    }

    /// <summary>
    /// 金属材质
    /// </summary>
    public string Metal
    {
        get => (string)this[PropertyKey.Metal].Value;
        set => this[PropertyKey.Metal].Value = value;
    }

    /// <summary>
    /// 包装材料
    /// </summary>
    public string Package
    {
        get => (string)this[PropertyKey.Package].Value;
        set => this[PropertyKey.Package].Value = value;
    }

    /// <summary>
    /// 生产日期
    /// </summary>
    public DateTime Produced
    {
        get => (DateTime)this[PropertyKey.Produced].Value;
        set => this[PropertyKey.Produced].Value = value;
    }

    /// <summary>
    /// 有效期（月）
    /// </summary>
    public int Validity
    {
        get => (int)this[PropertyKey.Validity].Value;
        set => this[PropertyKey.Validity].Value = value;
    }

    /// <summary>
    /// 推荐浏览节点(亚马逊相关)
    /// </summary>
    public string RecommendedBrowseNodes
    {
        get => (string)this[PropertyKey.RecommendedBrowseNodes].Value;
        set => this[PropertyKey.RecommendedBrowseNodes].Value = value;
    }
    
    /// <summary>
    /// 推荐浏览节点(亚马逊相关)
    /// </summary>
    public Dictionary<string,object> Custom
    {
        get => (Dictionary<string,object>)this[PropertyKey.Custom].Value;
        set => this[PropertyKey.Custom].Value = value;
    }


    #endregion
}