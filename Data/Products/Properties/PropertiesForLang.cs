namespace ERP.Data.Products;

public class PropertiesForLang : Dictionary<Enums.Languages, Properties>
{
    //快速索引key的操作
    //this[languages,Properties]
    private void CheckKey(Enums.Languages key)
    {
        if (!ContainsKey(key)) Add(key, new Properties());
    }

    public new Properties this[Enums.Languages key]
    {
        get
        {
            CheckKey(key);
            return base[key];
        }
        set
        {
            CheckKey(key);
            base[key] = value;
        }
    }
}