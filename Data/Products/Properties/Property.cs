﻿namespace ERP.Data.Products;

public class Property : IProperty
{
    public Property(string key)
    {
        Key = key;
    }
    public string Key { get; init; }

    public object? Value { get; set; }
}