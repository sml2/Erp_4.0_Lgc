﻿namespace ERP.Data.Products;

public class Property<ValueType> : Property, IProperty<ValueType>
{
    public Property(string key) : base(key)
    {
    }

    public new ValueType? Value
    {
        get => (ValueType?) base.Value;
        set => base.Value = value;
    }
}