using OrderSDK.Modles.Amazon.CatalogItems;
using PriceInfo = OrderSDK.Modles.Amazon.ProductPricing.Price;
using listItem = OrderSDK.Modles.Amazon.ListingsItems.Item;
using asinProduct = ERP.Models.Stores.AsinProduct;
using Newtonsoft.Json;

namespace ERP.Data.Products;

public class ProductResult
{
    public ProductResult(PriceInfo? price, ItemRes? itemRes, listItem? listItem)
    {
        Price = price;
        this.itemRes = itemRes;
        this.listInfo = listItem;
    }

    public PriceInfo? Price { get; set; }

    public ItemRes? itemRes { get; set; }


    public listItem? listInfo { get; set; }
    
    /// <summary>
    /// amazon sp
    /// </summary>
    public Product ToProduct(asinProduct asin, Platforms platform = Enums.Platforms.AMAZONSP)
    {
        var product = new Product();
        product.storeId = asin.StoreId;
        product.HashCode = asin.PidHashCode;
        product.productID = asin.SellerID + asin.Sku;
        var price = Price;
        var item = itemRes;
        var listInfo = this.listInfo;
        product.Platform = platform;
        if (listInfo!=null && listInfo.Offers.IsNotNull())
        {
                      
        }
        if (item is not null)
        {

            if(item.identifiers.IsNotNull() && item.identifiers.Count()>0)
            {
                var identity = item.identifiers[0];
                if(identity.identifiers is not null && identity.identifiers.Count > 0)
                {
                    var iden = identity.identifiers[0];
                    product.Code = iden.identifier;
                }
            }
            if (item.images.IsNotNull() && item.images.Count > 0)
            {
                var image = item.images[0];
                product.ImageUrl = image.images[0].link;
                foreach (var i in image.images)
                {
                    product.Images.Add(new(1, i.link));  // to do
                }
            }
            if (item.summaries.IsNotNull() && item.summaries.Count > 0)
            {
                var summary = item.summaries[0];
                if (summary != null && !summary.itemName.IsNullOrWhiteSpace())
                {
                    product.Title = summary.itemName;
                }
            }

          
            if (item.relationships.IsNotNull() && item.relationships.Count > 0)
            {
                if (item.attributes.IsNotNull())
                {
                    foreach (var attr in item.attributes)
                    {
                        product.Variants.Attributes.Add(attr.Key, new List<string>() { attr.Value.ToString()! }.ToArray());
                    }
                }
                var relation = item.relationships[0];
                foreach (var r in relation.relationships)   //变体属性
                {
                    if (r.variationTheme.IsNotNull())
                    {
                        product.Variants.Attributes.Add(r.variationTheme.theme, r.variationTheme.attributes.ToArray());
                    }
                }
            }
            if (item.productTypes.IsNotNull() && item.productTypes.Count > 0)
            {
                var productType = item.productTypes[0];
                product.productType = productType.productType;
            }

            //添加brand，color，size，更新asinproduct
            if (item.attributes is not null)
            {
                var ss = JsonConvert.SerializeObject(item.attributes);
                foreach (var attr in item.attributes)
                {
                    if (attr.Key.ToLower() == "brand")
                    {
                        var brandList = JsonConvert.DeserializeObject<CommonItemList>(attr.Value.ToString());
                        if (brandList is not null)
                        {
                            product.Brand = brandList[0].Value;
                        }
                    }
                    if (attr.Key.ToLower() == "color")
                    {
                        var colorList = JsonConvert.DeserializeObject<CommonItemList>(attr.Value.ToString());
                        if (colorList is not null)
                        {
                            product.Color = colorList[0].Value;
                        }
                    }
                    if (attr.Key.ToLower() == "item_type_name")
                    {
                        var colorList = JsonConvert.DeserializeObject<CommonItemList>(attr.Value.ToString());
                        if (colorList is not null)
                        {
                            product.LanguageTag = colorList[0].LanguageTag;
                        }
                    }                    
                    //size 没找到                    
                }
            }
        }
        if (listInfo.IsNotNull())
        {
            
            if(listInfo.Sku!=null)
            {
                product.Sku= listInfo.Sku;

            }
            if (listInfo.Summaries.IsNotNull() && listInfo.Summaries.Count > 0)
            {
                var summary = listInfo.Summaries[0];
                product.Title = summary.ItemName?? "";
                product.Asin = summary.Asin ?? "";
                product.productType= summary.ProductType ??"";
            }
            if (listInfo!.FulfillmentAvailability.IsNotNull() && listInfo.FulfillmentAvailability.Count > 0)
            {
                var Fulfillment = listInfo.FulfillmentAvailability[0];
                product.Quantity = Fulfillment.Quantity ?? 0;
            }

            if (listInfo!.Offers.IsNotNull() && listInfo.Offers.Count > 0)
            {
                var priceInfo = listInfo.Offers[0];
                if (priceInfo.Price.IsNotNull())
                {
                    decimal d = Convert.ToDecimal(priceInfo.Price.Amount);
                    product.Price = new MoneyMeta(priceInfo.Price.CurrencyCode, d);
                }
            }
            if (listInfo.Attributes.IsNotNull() && listInfo.Attributes.Count > 0)
            {
                if (listInfo.Attributes.IsNotNull())
                {
                    foreach (var attr in listInfo.Attributes)
                    {
                        product.Variants.Attributes.Add(attr.Key, new List<string>() { attr.Value.ToString()! }.ToArray());
                    }
                }
            }
        }

        return product;
    }
   
}
