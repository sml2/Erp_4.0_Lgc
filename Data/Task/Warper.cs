﻿namespace ERP.Data.Task
{
    public class Warper : ILogger
    {
        private ILogger loger;
        public State taskState;

        public Warper(ILogger loger,State taskState)
        {
            this.loger = loger;
            this.taskState = taskState;
        }

        public IDisposable BeginScope<TState>(TState state)=> loger.BeginScope(state);

        public bool IsEnabled(LogLevel logLevel)=>loger.IsEnabled(logLevel);

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception? exception, Func<TState, Exception?, string> formatter) {
            taskState.AddLog(logLevel, formatter.Invoke(state,exception));
            loger.Log(logLevel, eventId, state, exception, formatter);
        }
    }
}
