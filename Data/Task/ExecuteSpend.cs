﻿namespace ERP.Data.Task;

public record ExecuteSpend
{
    public static Log Empty = new(LogLevel.None, string.Empty);
    public ExecuteSpend()
    {
        StartTime = DateTime.Now;
    }
    public DateTime End() => EndTime = DateTime.Now;
    public DateTime StartTime { get; init; }
    public DateTime EndTime { get; private set; }
    public TimeSpan SpendTime { get => EndTime - StartTime; }
}
