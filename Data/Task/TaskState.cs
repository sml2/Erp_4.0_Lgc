﻿using NPOI.SS.Formula.Functions;

namespace ERP.Data.Task
{
    public class State
    {
        /// <summary>
        /// 类名称
        /// </summary>
        public string ClassName { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 介绍
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 间隔时间
        /// </summary>
        public TimeSpan Interval { get; set; }
        /// <summary>
        /// 任务数
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// 索引
        /// </summary>
        public int Index { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// 最后更新时间
        /// </summary>
        public DateTime LastTime { get; set; }
        /// <summary>
        /// 最后信息
        /// </summary>
        public Log LastLog { get; set; } = Log.Empty;
        /// <summary>
        /// 最后信息
        /// </summary>
        public LogLevel LastLeave { get => LastLog.Level; }
        /// <summary>
        /// 最后信息
        /// </summary>
        public string LastContent { get => LastLog.Content; }
        public Queue<Log> Logs = new();

        public void AddLog(LogLevel level, string content) => AddLog(new(level, content));

        public void AddLog(Log log)
        {
            LastLog = log;
            LastTime = log.Time;
            Logs.Enqueue(log);
            //保留最后10条
            if (Logs.Count > 10)
                Logs.Dequeue();
        }
        public Queue<ExecuteSpend> ExecuteSpends = new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="executeSpend"></param>
        public void AddExecuteSpend(ExecuteSpend executeSpend)
        {
            ExecuteSpends.Enqueue(executeSpend);
            //保留100天内得执行情况
            while (ExecuteSpends.TryPeek(out var first) && DateTime.Now - first.StartTime > TimeSpan.FromDays(100))
                ExecuteSpends.Dequeue();
        }
    }
}
