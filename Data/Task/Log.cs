﻿namespace ERP.Data.Task;

public record Log
{
    public static Log Empty = new(LogLevel.None, string.Empty);
    public Log(LogLevel level, string content)
    {
        Level = level;
        Content = content;
        Time = DateTime.Now;
    }
    public DateTime Time { get; init; }
    public LogLevel Level { get; init; }
    public string Content { get; init; }
}
