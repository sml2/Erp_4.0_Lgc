using ERP.Models.DB.Product;
using Microsoft.EntityFrameworkCore;

namespace ERP.Data;

using Models.Product;
using Models.DB.Stores;
using ERP.Models.Stores;

public partial class DBContext //产品
{
    public DbSet<ProductModel> Product { get; set; } = null!;

    public DbSet<ProductItemModel> ProductItem { get; set; } = null!;


    public DbSet<TempImagesModel> TempImages { get; set; } = null!;
    public DbSet<ResourceModel> Resource { get; set; } = null!;
  

    public DbSet<ProductUpload>  productUpload { get; set; } = null!;

    //public DbSet<ProductUploadJson> productUploadJson { get; set; } = null!;

    //public DbSet<ProductUploadLog> productUploadLog { get; set; } = null!;
    public DbSet<ProductMigrationTask> ProductMigrationTasks { get; set; } = null!;

}