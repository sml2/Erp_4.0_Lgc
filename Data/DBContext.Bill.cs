﻿
using Microsoft.EntityFrameworkCore;
namespace ERP.Data;
using Models.Finance;

public partial class DBContext //财务
{
    public DbSet<BillLog> BillLog { get; set; } = null!;
    public DbSet<PayBillModel> PayBill { get; set; } = null!;
    public DbSet<PayConfig> PayConfig { get; set; } = null!;

    public DbSet<FinancialAffairsModel> FinancialAffairs { get; set; } = null!;
}
