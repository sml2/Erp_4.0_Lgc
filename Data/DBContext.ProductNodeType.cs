
using Microsoft.EntityFrameworkCore;
namespace ERP.Data;

using ERP.Models.ProductNodeType.Amazon;
using Models;
using Models.DB;
using Models.ProductNodeType;

public partial class DBContext 
{
    public DbSet<UserInfo> UserInfo { get; set; } = null!;
    public DbSet<PlatformData> PlatformData { get; set; } = null!;
    public DbSet<AreaCategoryRelation> AreaCategoryRelation { get; set; } = null!;
    public DbSet<ProductNodeTypeCategory> ProductNodeTypeCategory { get; set; } = null!;
    
    public DbSet<CategoryAttrRelation> CategoryAttrRelation { get; set; } = null!;
    public DbSet<ProductNodeTypeAttribute> ProductNodeTypeAttribute { get; set; } = null!;
    public DbSet<AttrValueRelation> AttrValueRelation { get; set; } = null!;
    public DbSet<Value> Value { get; set; } = null!;   
    public DbSet<ErrorDatas> ErrorDatas { get; set; } = null!;

    public DbSet<RealShipRaw> RealShipRaw { get; set; } = null!;
    public DbSet<RealShip> RealShip { get; set; } = null!;
    public DbSet<NodeRaw> NodeRaw { get; set; } = null!;
    public DbSet<ProductRaw> ProductRaw { get; set; } = null!;

    public DbSet<Node> Node { get; set; } = null!;

    //第三版 amazon产品节点和产品类型，属性

    public DbSet<AttributeModel> AttributeModels { get; set; } = null!;

    public DbSet<Category> NodeCategory { get; set; } = null!;
}
