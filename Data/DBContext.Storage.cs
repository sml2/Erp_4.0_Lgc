﻿
using Microsoft.EntityFrameworkCore;
namespace ERP.Data;
using Models.DB.Storage;

public partial class DBContext //仓库
{
    public DbSet<Warehouse> Warehouses { get; set; } = null!;
    public DbSet<Shelf> Shelves { get; set; } = null!;
    public DbSet<Stock> Stock { get; set; } = null!;
    public DbSet<StockLog> StockLog { get; set; } = null!;
    public DbSet<StockProduct> StockProduct { get; set; } = null!;
}
