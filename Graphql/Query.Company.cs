﻿using System.Security.Claims;
using ERP.Interface.Services;
using ERP.Models.DB.Users;
using ERP.Services.Identity;
using HotChocolate;

namespace ERP.Graphql;

public partial class Query
{

    public async Task<IQueryable<Company>> GetValidDistributionCompanies(ClaimsPrincipal claimsPrincipal,
        [Service(ServiceKind.Synchronized)] ICompanyService service, [Service] UserManager userManager)
    {
        var user = await userManager.GetUserAsync(claimsPrincipal);
        return service.GetValidDistributionListQuery(user.CompanyID);
    }
}