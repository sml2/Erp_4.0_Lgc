﻿using System.Security.Claims;
using ERP.Models.DB.Storage;
using ERP.Services.Identity;
using ERP.Storage.Abstraction.Services;
using ERP.Storage.Api.Controllers;
using HotChocolate;
using HotChocolate.Types;

namespace ERP.Graphql;

public partial class Query
{
    /// <summary>
    /// 获取所有仓库
    /// </summary>
    /// <param name="claimsPrincipal"></param>
    /// <param name="service"></param>
    /// <param name="userManager"></param>
    /// <param name="query"></param>
    /// <returns></returns>
    [UseOffsetPaging, UseFiltering, UseSorting]
    public async Task<IQueryable<Warehouse>> GetWarehouses(ClaimsPrincipal claimsPrincipal,
        [Service(ServiceKind.Synchronized)] IWarehouseService service, [Service] UserManager userManager,
        WarehouseController.ListQuery query)
    {
        var user = await userManager.GetUserAsync(claimsPrincipal);
        return service.ListQuery(user, query.Name, query.Type);
    }

    /// <summary>
    /// 获取仓库详情
    /// </summary>
    /// <param name="id"></param>
    /// <param name="claimsPrincipal"></param>
    /// <param name="service"></param>
    /// <param name="userManager"></param>
    /// <returns></returns>
    [UseFirstOrDefault]
    public async Task<IQueryable<Warehouse>> GetWarehouse(int id, ClaimsPrincipal claimsPrincipal,
        [Service(ServiceKind.Synchronized)] IWarehouseService service, [Service] UserManager userManager)
    {
        var user = await userManager.GetUserAsync(claimsPrincipal);
        return service.InfoQuery(user, id);
    }

    public record TypeDto<T>(T Value, string Label);
    public record TypeDto(int Value, string Label);

    /// <summary>
    /// 获取仓库类型
    /// </summary>
    /// <returns></returns>
    public IEnumerable<TypeDto> GetWarehouseTypes()
    {
        return Enum.GetValues(typeof(Warehouse.Types)).Cast<Warehouse.Types>()
            .Select(t => new TypeDto((int)t,Ryu.Extensions.EnumExtension.GetDescription(t)));
    }
    
    /// <summary>
    /// 获取所有货架
    /// </summary>
    /// <param name="claimsPrincipal"></param>
    /// <param name="service"></param>
    /// <param name="userManager"></param>
    /// <param name="query"></param>
    /// <returns></returns>
    [UseOffsetPaging, UseFiltering, UseSorting]
    public async Task<IQueryable<Shelf>> GetShelves(string? name, int? warehouseId, ClaimsPrincipal claimsPrincipal,
        [Service(ServiceKind.Synchronized)] IShelfService service, [Service] UserManager userManager)
    {
        var user = await userManager.GetUserAsync(claimsPrincipal);
        return service.SearchShelvesQuery(user.CompanyID, name, warehouseId);
    }

    /// <summary>
    /// 获取货架详情
    /// </summary>
    /// <param name="id"></param>
    /// <param name="claimsPrincipal"></param>
    /// <param name="service"></param>
    /// <param name="userManager"></param>
    /// <returns></returns>
    [UseFirstOrDefault]
    public async Task<IQueryable<Shelf>> GetShelf(int id, ClaimsPrincipal claimsPrincipal,
        [Service(ServiceKind.Synchronized)] IShelfService service, [Service] UserManager userManager)
    {
        var user = await userManager.GetUserAsync(claimsPrincipal);
        return service.GetShelfByIdQuery(id, user);
    }
    
    /// <summary>
    /// 获取所有库存
    /// </summary>
    /// <param name="claimsPrincipal"></param>
    /// <param name="service"></param>
    /// <param name="userManager"></param>
    /// <param name="query"></param>
    /// <returns></returns>
    [UseOffsetPaging, UseFiltering, UseSorting]
    public async Task<IQueryable<Stock>> GetStocks(int? stockProductId, string? name, int? warehouseId, int? shelfId, string? spu, ClaimsPrincipal claimsPrincipal,
        [Service(ServiceKind.Synchronized)] IStockService service, [Service] UserManager userManager)
    {
        var user = await userManager.GetUserAsync(claimsPrincipal);
        return service.StockListQuery(user, stockProductId, name, warehouseId, shelfId, spu);
    }
    
}