﻿using System.Security.Claims;
using ERP.Models.DB.Identity;
using ERP.Services.Identity;
using HotChocolate;
using HotChocolate.Authorization;

namespace ERP.Graphql;

[Authorize]
public partial class Query
{
    public async Task<User> Me(ClaimsPrincipal user,
        [Service] UserManager userManager)
    {
        return await userManager.GetUserAsync(user);
    }
}