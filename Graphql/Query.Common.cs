﻿using ERP.Models.Setting;
using ERP.Services.Caches;
using HotChocolate;

namespace ERP.Graphql;

public partial class Query
{
    /// <summary>
    /// 获取所有货币单位
    /// </summary>
    /// <param name="unitCache"></param>
    /// <returns></returns>
    public IEnumerable<Models.Setting.Unit> GetUnits([Service] Services.Caches.Unit unitCache)
    {
        return unitCache.List() ?? Enumerable.Empty<Models.Setting.Unit>();
    }
    
    /// <summary>
    /// 获取所有语言
    /// </summary>
    /// <param name="languagesCache"></param>
    /// <returns></returns>
    public IEnumerable<Language> GetLanguages([Service] LanguagesCache languagesCache)
    {
        return languagesCache.List() ?? Enumerable.Empty<Language>();
    }
}