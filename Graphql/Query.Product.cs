﻿using System.Security.Claims;
using ERP.Controllers.Product;
using ERP.Data;
using ERP.Models.DB.Product;
using ERP.Models.DB.Stores;
using ERP.Models.Template;
using ERP.Services.Identity;
using ERP.Services.Product;
using HotChocolate;
using HotChocolate.Types;
using Microsoft.EntityFrameworkCore;

namespace ERP.Graphql;

public partial class Query
{
    /// <summary>
    /// 获取当前用户所有的改价模板
    /// </summary>
    /// <param name="user"></param>
    /// <param name="userManager"></param>
    /// <param name="changePriceService"></param>
    /// <returns></returns>
    public async Task<IQueryable<ChangePriceTemplateModel>> GetChangePriceTemplates(ClaimsPrincipal user,
        [Service] UserManager userManager, [Service(ServiceKind.Synchronized)] ChangePriceService changePriceService)
    {
        var range = await userManager.GetProductModuleRange(user, ChangePriceController.CurrentRange);
        var root = changePriceService.DataWatchRange(range);
        if (root is null)
            return Enumerable.Empty<ChangePriceTemplateModel>().AsQueryable();

        return root.OrderByDescending(a => a.UpdatedAt);
    }

    /// <summary>
    /// 获取当前用户的上传任务
    /// </summary>
    /// <param name="claim"></param>
    /// <param name="dbContext"></param>
    /// <param name="userManager"></param>
    /// <returns></returns>
    [UseOffsetPaging, UseFiltering, UseSorting]
    public async Task<IQueryable<SyncUploadTask>> GetSyncProductTasks(ClaimsPrincipal claim,
        [Service(ServiceKind.Synchronized)] DBContext dbContext, [Service] UserManager userManager)
    {
        var user = await userManager.GetUserAsync(claim);
        return dbContext.SyncUploadTasks.Include(t => t.Store)
            .Where(t => t.CompanyID == user.CompanyID && t.UserID == user.ID);
    }

    /// <summary>
    /// 获取当前用户的上传任务详情
    /// </summary>
    /// <param name="id"></param>
    /// <param name="claim"></param>
    /// <param name="dbContext"></param>
    /// <param name="userManager"></param>
    /// <returns></returns>
    [UseFirstOrDefault]
    public async Task<IQueryable<SyncUploadTask>> GetSyncProductTask(int id, ClaimsPrincipal claim,
        [Service(ServiceKind.Synchronized)] DBContext dbContext, [Service] UserManager userManager)
    {
        var user = await userManager.GetUserAsync(claim);
        return dbContext.SyncUploadTasks.Include(t => t.Store)
            .Include(t => t.ProductUploads)
            .Where(t => t.CompanyID == user.CompanyID && t.UserID == user.ID && t.ID == id);
    }
}