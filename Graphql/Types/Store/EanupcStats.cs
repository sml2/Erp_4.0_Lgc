﻿namespace ERP.Graphql.Types.Store;

public class EanupcStats
{
    /// <summary>
    /// 当前ean数量
    /// </summary>
    public int EanCount { get; set; }

    /// <summary>
    /// 当前upc数量
    /// </summary>
    public int UpcCount { get; set; }
    
    /// <summary>
    /// 单类型数量上限
    /// </summary>
    public int MaxCount { get; set; }
}