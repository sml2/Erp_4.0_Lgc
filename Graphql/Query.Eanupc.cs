using System.Security.Claims;
using ERP.Graphql.Types.Store;
using ERP.Models.DB.Identity;
using ERP.Models.DB.Stores;
using ERP.Models.Stores;
using ERP.Services.Identity;
using ERP.Services.Stores;
using ERP.ViewModels.Store.Eanupc;
using HotChocolate;
using HotChocolate.Types;

namespace ERP.Graphql;

public partial class Query
{
    /// <summary>
    /// 获取所有ean/upc
    /// </summary>
    /// <param name="user"></param>
    /// <param name="service"></param>
    /// <param name="userManager"></param>
    /// <returns></returns>
    [UseOffsetPaging, UseFiltering, UseSorting]
    public async Task<IQueryable<EanupcModel>> GetEanupcs(ClaimsPrincipal user, [Service(ServiceKind.Synchronized)] EanupcService service, [Service] UserManager userManager)
    {
        var range = await userManager.GetProductModuleRange(user, Enums.Rule.Config.Store.EanUpcRange);
        return service.GetListQuery(range);
    }

    /// <summary>
    /// 获取ean/upc统计信息
    /// </summary>
    /// <param name="user"></param>
    /// <param name="service"></param>
    /// <param name="userManager"></param>
    /// <returns></returns>
    public async Task<EanupcStats> GetEanupcStats(ClaimsPrincipal user, [Service(ServiceKind.Synchronized)] EanupcService service, [Service] UserManager userManager)
    {
        var range = await userManager.GetProductModuleRange(user, Enums.Rule.Config.Store.EanUpcRange);
        return new EanupcStats()
        {
            EanCount = await service.GetCodeNum(EanupcModel.TypeEnum.EAN,range),
            UpcCount = await service.GetCodeNum(EanupcModel.TypeEnum.UPC,range),
            MaxCount = service.GetRangeTotal(range)
        };
    }
}