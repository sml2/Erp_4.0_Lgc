namespace ERP.Controllers;

[Route("Account")]
public class AccountController  : BaseController
{
    [Route("AccessDenied")]
    [HttpGet]
    public async Task<ResultStruct> AccessDenied()
    {
        return Error("暂无权限");
    }
}