using ERP.Models;

namespace ERP.Controllers.ProductNodeType;

public class TokenParams
{
    public string Code { get; set; } = string.Empty;
    public int ShopId { get; set; }
}

public class ShopInfo
{
    public int ShopId { get; set; }

    public string ShorterForm { get; set; }=string.Empty;
}

public class AreaShopInfo
{
    public AreaShopInfo(PlatformData x)
    {
        var data = x.GetShopeeData();
        Id = x.ID;
        //AreaName = x.AreaName,
        AreaName = data.AreaName;
        ShopId = data.ShopId;
        ShorterForm = data.ShorterForm;
        IsExist = x.AreaCategoryRelations.Count() > 0 ? true : false;
        Count = x.AreaCategoryRelations.Count(x => x.HasChildren == false && x.Flag == false);
        ShopType = data.ShopType == null ? string.Empty : data.ShopType.ToString();
    }

    //public static string getShopType(EnumShopType? i)
    //{
    //    string shoptype = "";
    //    switch (i)
    //    {
    //        case EnumShopType.LocalShop:
    //            shoptype = "LocalShop";
    //            break;
    //        case EnumShopType.CrossBorderShop:
    //            shoptype = "CrossBorderShop";
    //            break;
    //        case EnumShopType.Merchant:
    //            shoptype = "Merchant";
    //            break;
    //        default:
    //            break;
    //    }
    //    return shoptype;
    //}
    public int Id { get; set; }
    public string AreaName { get; set; }
    public string ShorterForm { get; set; }
    public int ShopId { get; set; }

    public bool? IsExist { get; set; }

    public int? Count { get; set; }

    public string? ShopType { get; set; }
}
