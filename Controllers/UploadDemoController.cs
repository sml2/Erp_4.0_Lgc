using ERP.Services;
using ERP.Services.Upload;

namespace ERP.Controllers;

public class UploadDemoController
{
    public readonly UploadService _service;

    public UploadDemoController(UploadService service)
    {
        _service = service;
    }

    public void Put()
    {
        
    }

    public void Delete()
    {
        
    }

    public void GetSize()
    {
        
    }

    public void GetMetatype()
    {
        
    }
}