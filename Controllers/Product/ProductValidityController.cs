using ERP.Interface;
using ERP.Models.View.Products.Product;
using ERP.Services.Identity;
using ERP.Services.Product;
using ERP.ViewModels;
using Microsoft.AspNetCore.Identity;

namespace ERP.Controllers.Product;

[Route("/api/[controller]/[action]")]
[ApiController]
public class ProductValidityController : BaseController
{
    private readonly ProductValidityService _productValidityService;
    private readonly ISessionProvider _ISessionProvider;

    public ProductValidityController(ProductValidityService productValidityService, ISessionProvider iSessionProvider)
    {
        _productValidityService = productValidityService;
        _ISessionProvider = iSessionProvider;
    }

    [Route("{userId}")]
    public async Task<ResultStruct> List(int userId)
    {
        var result = await _productValidityService.List(userId);
        return Success(result);
    }

    public async Task<ResultStruct> Add(ProductValidityDto req)
    {
        try
        {
            var result = await _productValidityService.AddOrEdit(req,_ISessionProvider.Session);
        }
        catch (Exception e)
        {
            return Error("新增失败");
        }

        return Success("新增成功");
    }
    [Route("{id}")]
    public async Task<ResultStruct> Show(int Id)
    {
        var result = await _productValidityService.GetInfoById(Id);
        return result != null ? Success(result) : Error("找不到相关数据");
    }
    
    [HttpPost]
    public async Task<ResultStruct> Delete(IdDto req)
    {
        var res = await _productValidityService.Delete(req.ID);
        return res ? Success("删除成功") : Error("删除失败");
    }
}