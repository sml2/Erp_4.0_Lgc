using ERP.Data;
using ERP.Extensions;
using ERP.Interface;

namespace ERP.Controllers.Product.Client;

public class BaseController : AbstractBaseController
{
    protected readonly DBContext _dbContext;
    protected readonly ISessionProvider _sessionProvider;
    protected readonly IOemProvider _oemProvider;

    public BaseController(DBContext dbContext, ISessionProvider sessionProvider, IOemProvider oemProvider)
    {
        _dbContext = dbContext;
        _sessionProvider = sessionProvider;
        _oemProvider = oemProvider;
    }

    protected ISession _session => _sessionProvider.Session!;
    protected int _userId => _session.GetUserID();
    protected int _groupId => _session.GetGroupID();

    protected int _companyId => _session.GetCompanyID();
    protected int _oemId => _session.GetOEMID();

    public override int GetApiId()
    {
        throw new NotImplementedException();
    }
}