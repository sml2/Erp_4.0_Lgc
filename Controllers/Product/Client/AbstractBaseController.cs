using Newtonsoft.Json;

namespace ERP.Controllers.Product.Client;

abstract public class AbstractBaseController : ControllerBase
{
    protected bool _needReturn = false;

    const int SUCCESS = 1000;
    protected const int ERR_VERSION = 1001;
    const int ERR_FIELD = 1002;
    protected const int ERR_PARAMETER = 1003;
    const int NOT_LOGIN = 1004;
    const int ERR_NOTIMPT = 1005;
    const int ERR_UNKNOW = 1999;

    abstract public int GetApiId();

    protected object Success(object? data = null, string? message = null)
    {
        return Output(SUCCESS, data, message);
    }

    protected object Fail(int errCode, object? data = null, string? message = null)
    {
        var apiId = GetApiId();
        var baseCode = 1000;
        return Output(errCode > baseCode ? errCode : apiId * baseCode + errCode, data, message);
    }

    private object Output(int code, object? data = null, string? message = null)
    {
        _needReturn = true;
        return new
        {
            Code = code,
            Data = JsonConvert.SerializeObject(data),
            Info = message
        };
    }
    
    protected DateTime GetNow()
    {
        return DateTime.Now;
    }
}