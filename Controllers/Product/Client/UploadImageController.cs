using ERP.Data;
using ERP.Exceptions.Upload;
using ERP.Interface;
using ERP.Models.DB.Product;
using ERP.Services.Upload;
using ERP.ViewModels.UplaodImages;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ERP.Controllers.Product.Client;

public class UploadImageController : BaseController
{
    protected readonly UploadService _uploadService;
    const int ImageIdIsEmpty = 1; //图片ID为空
    const int ImageIsEmpty = 2; //图片为空
    const int ImageImageIsOss = 3; //图片已经同步完成
    const int ImageUploadFailed = 4; //图片上传oss失败
    const int ImageUpdateFailed = 5; //数据修改失败
    const int ImageExtensionsFailed = 6; //图片扩展名非法

    public override int GetApiId()
    {
        return 180;
    }

    public UploadImageController(DBContext dbContext, ISessionProvider sessionProvider, IOemProvider oemProvider,
        UploadService uploadService) :
        base(dbContext, sessionProvider, oemProvider)
    {
        _uploadService = uploadService;
    }

    public async Task<object> Handle(UploadImageDto req)
    {
        var imageId = req.Id;
        if (imageId < 0)
        {
            return Fail(ImageIdIsEmpty);
        }

        var imageModel = await _dbContext.Resource.Where(m => m.ID == imageId).FirstOrDefaultAsync();

        if (imageModel is null)
        {
            return Fail(ImageIsEmpty);
        }

        if (imageModel.Type == ResourceModel.Types.Storage)
        {
            return Fail(ImageImageIsOss);
        }

        try
        {
            var path = await _uploadService.UploadImageByBase64(req.Base64Data, req.Extension, _oemId, _userId);
            var size = await _uploadService.GetSize(path);
            imageModel.OEMID = _oemProvider.OEM.OssId;
            imageModel.Size = size;
            imageModel.Path = path;
            imageModel.Type = ResourceModel.Types.Storage;

            await _dbContext.Resource.SingleUpdateAsync(imageModel);
            await _dbContext.SaveChangesAsync();
            return Success(new
            {
                ID = imageModel.ID,
                Size = imageModel.Size,
                ImgUrl = imageModel.Path
            });
        }
        catch (UploadException e)
        {
            return Fail(ImageUploadFailed, e.Message);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return Fail(ImageUpdateFailed, e.Message);
        }

        return Fail(ImageUpdateFailed);
    }
}

public class UploadImageDto
{
    public int Id { get; set; }
    public string Base64Data { get; set; }

    public string Extension { get; set; }
}