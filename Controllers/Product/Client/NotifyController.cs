using ERP.Data;
using ERP.Data.Products;
using ERP.Enums.Product;
using ERP.Interface;
using ERP.Models.DB.Product;
using ERP.Models.Product;
using ERP.Services.Product;
using ERP.ViewModels;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace ERP.Controllers.Product.Client;

public class NotifyController : BaseController
{
    protected readonly ProductService _productService;

    public NotifyController(DBContext dbContext, ISessionProvider sessionProvider, IOemProvider oemProvider,
        ProductService productService) : base(dbContext, sessionProvider, oemProvider)
    {
        _productService = productService;
    }
    
    
    private const int ProductIdIsEmpty = 1;           //产品ID为空
    private const int ProductIsEmpty = 2;              //产品为空
    private const int ProductImageIsEmpty = 3;        //产品图片为空
    private const int ProductImageIsOss = 4;          //产品已经同步完成
    private const int ProductUpdateFailed = 5;          //产品修改失败
    private const int ProductImageIsNotComplete = 6;          //产品图片未同步完成
    
    public int GetApiId()
    {
        return 190;
    }

    public async Task<object> Handle(IdDto req)
    {
        var productId = req.ID;
        if (productId < 0)
        {
            return Fail(ProductIdIsEmpty);
        }
        
        var model = _productService.ProductDataWatchRange();
        int auditState = ProductModel.AUDIT_AUDITED;
        //排除审核
        model = model.Where(m => m.Audit == auditState && m.ID == req.ID);
        model = model.Where(m =>
            (m.Type & (long)Types.Collect) == (long)Types.Collect ||
            (m.Type & (long)Types.Collect) != (long)Types.Collect);
        var product = model.FirstOrDefault();
        if (product is null)
        {
            return Fail(ProductIsEmpty);
        }
        
        var images = product.Gallery;

        if (images is null)
        {
            return Fail(ProductImageIsEmpty);
        }

        if (!product.IsImageAsync)
        {
            return Fail(ProductImageIsOss);
        }

        var ids = images.Select(m => m.ID).ToList();

        var imageModels = await _dbContext.Resource.Where(m => ids.Contains(m.ID)).ToListAsync();

        if (imageModels.Count <= 0)
        {
            return Fail(ProductImageIsEmpty);
        }
        
        if (ids.Count != imageModels.Count)
        {
            return Fail(ProductImageIsNotComplete);
        }

        var size = imageModels.Sum(m => m.Size);

        var newImages = imageModels.Select(m => new Image(m)).ToList();

        product.IsImageAsync = false;
        product.ImagesSize = size;
        product.Images = JsonConvert.SerializeObject(newImages);
        product.MainImage = newImages.Count > 0 ? JsonConvert.SerializeObject(newImages.First()) : null;

        await _dbContext.Product.SingleUpdateAsync(product);

        if (await _dbContext.SaveChangesAsync() > 0)
        {
            return Success();
        }
        return Fail(ProductUpdateFailed);

    }
}