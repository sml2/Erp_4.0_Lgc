using ERP.Data;
using ERP.Interface;
using ERP.Models.Product;
using ERP.Models.View.Product.Translation;
using ERP.Services.Product;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Attribute = ERP.Data.Products.Attribute;

namespace ERP.Controllers.Product.Client.Translation;

[Route("client/translation")]
public class GetController : BaseController
{
    protected readonly ProductService _productService;
    private readonly ILogger<GetController> _logger;

    public GetController(DBContext dbContext, ISessionProvider sessionProvider, IOemProvider oemProvider,
        ProductService productService, ILogger<GetController> logger) : base(
        dbContext, sessionProvider, oemProvider)
    {
        _productService = productService;
        _logger = logger;
    }

    const int ProductDoesNotExist = 1; //产品不存在
    const int ProductIsNotAudit = 2; //产品未审核
    const int ProductDescriptionError = 3; //详情解析出错

    public override int GetApiId()
    {
        return 160;
    }

    [HttpPost("get")]
    public async Task<object> Handle([FromForm(Name = "request")] string request)
    {
        var req = JsonConvert.DeserializeObject<GetDto>(request);
        var id = req.ID;

        if (id < 0)
        {
            return Fail(ERR_PARAMETER, "ID");
        }

        var product = await _productService.GetInfoByIdWithUser(id);
    
        if (product is null)
        {
            return Fail(ProductDoesNotExist);
        }

        if (product.Audit != ProductModel.AUDIT_AUDITED)
        {
            return Fail(ProductIsNotAudit);
        }

        var uiLanguageId = product.UiLanguageId;
        var itemInfo = await _productService.GetItemInfoByLangId(product.ID, uiLanguageId);
        if (itemInfo is null)
        {
            return Fail(ProductDoesNotExist);
        }

        var itemStruct = await _productService.GetProductStruct(itemInfo.FilePath,product);

        try
        {
            return Success(new ClientTranVm(itemStruct, product.ID));
        }
        catch (Exception e)
        {
            _logger.LogError(e, "产品{Id}语言{Lang}解析描述出错:\n{Desc}", id, uiLanguageId, itemStruct.GetDescription());
            return Fail(ProductDescriptionError);
        }
    }
}

