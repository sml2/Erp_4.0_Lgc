using Common.Html;
using ERP.Data;
using ERP.Data.Products;
using ERP.Enums;
using ERP.Interface;
using ERP.Models.DB.Product;
using ERP.Models.Product;
using ERP.Services.Product;
using ERP.Services.Upload;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Z.EntityFramework.Plus;
using Attribute = ERP.Data.Products.Attribute;

namespace ERP.Controllers.Product.Client.Translation;

[Route("client/translation")]
public class SetController : BaseController
{
    protected readonly ProductService _productService;
    private readonly UploadService _uploadService;

    public SetController(DBContext dbContext, ISessionProvider sessionProvider, IOemProvider oemProvider,
        ProductService productService, UploadService uploadService) : base(
        dbContext, sessionProvider, oemProvider)
    {
        _productService = productService;
        _uploadService = uploadService;
    }

    const int ProductDoesNotExist = 1; //产品不存在
    const int LanguageBiterNotExist = 2; //该语言的biter值不存在
    const int ProductTranslationFailed = 3; //产品翻译失败
    const int ProductIsNotAudit = 4; //产品未审核

    public override int GetApiId()
    {
        return 170;
    }

    [HttpPost("set")]
    public async Task<object> Handle([FromForm(Name = "request")]string request)
    {
        var req = JsonConvert.DeserializeObject<SetDto>(request);
        var id = req.ID;

        if (id < 0)
        {
            return Fail(ERR_PARAMETER, "ID");
        }

        var product = await _dbContext.Product
            .FirstOrDefaultAsync(m => m.ID == id && m.CompanyID == _companyId && m.OEMID == _oemId);

        if (product is null)
        {
            return Fail(ProductDoesNotExist);
        }

        if (product.Audit != ProductModel.AUDIT_AUDITED)
        {
            return Fail(ProductIsNotAudit);
        }

        //翻译的同时 默认语种被修改
        var uiLanguageId = product.UiLanguageId;
        var itemInfo = await _dbContext.ProductItem
            .FirstOrDefaultAsync(m => m.LanguageId == uiLanguageId && m.Pid == product.ID);

        if (itemInfo is null)
        {
            return Fail(ProductDoesNotExist);
        }

        var itemStruct = await _productService.GetProductStruct(itemInfo.FilePath,product);
        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            var desc = req.Desc;
            //描述替换
            var originDesc = itemStruct.GetDescription()!;
            if (!string.IsNullOrEmpty(desc))
            {
                desc = HtmlParser.Parse(originDesc).ReplaceContent(desc,
                    new GetContentSetting() { Unescape = true, RemoveWhiteSpaceEndLines = true, RemoveWhiteSpaceStartLines = true}).ToString();
            }
            else // 原始描述全是图或是空白描述时
            {
                desc = originDesc;
            }
           
            //替换语种
            //Basic
            itemStruct.SetTitle(req.Name);
            itemStruct.SetKeyword(req.Keywords);
            itemStruct.SetDescription(desc);
            itemStruct.SetSketches(req.Sketch);

            // //Images
            // var images = itemStruct.Images;
            // var (imageComparison, copySuccessImages, allTempImages) =
            //     await _productService.BeforeCloneProWithImages(images, product);
            // itemStruct.Variants = ReplaceVariantsImgId(imageComparison, itemStruct.Variants);

            //attributes
            var map = req.Map;
            if (map != null)
            {
                var attributes = itemStruct.Variants.Attributes.items;
                foreach (var attr in attributes)
                {
                    //Name
                    if (map.ContainsKey(attr.Value.Name))
                        attr.Value.Name = map[attr.Value.Name];
                    //Value
                    var indexs = new List<int>();
                    var values = attr.Value.Value;
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (map.ContainsKey(values[i]))
                            indexs.Add(i);
                    }

                    foreach (var i in indexs)
                    {
                        attr.Value.Value[i] = map[attr.Value.Value[i]];
                    }
                }
                itemStruct.Variants.Attributes.items = attributes;
            }
            

            var now = DateTime.Now;

            ProductItemModel newItemModel = new ProductItemModel();
            var newVersion = now.GetTimeStampMilliseconds();
            // newItemModel.CurrentVersion = now.GetTimeStampMilliseconds();
            //判断item是存在
            var langItemInfo = await _dbContext.ProductItem
                .FirstOrDefaultAsync(m => m.LanguageId == req.LanguageId && m.Pid == product.ID);
            if (langItemInfo == null)
            {
                newItemModel = new ProductItemModel(itemInfo, _sessionProvider.Session);
                //设置语言
                newItemModel.LanguageId = req.LanguageId;
                newItemModel.LanguageName = req.Language.GetDescriptionByKey("Code");
                newItemModel.HistoryVersion = JsonConvert.SerializeObject(new List<long>() { newVersion });
                newItemModel.Pid = product.ID;
                // newItemModel.CurrentVersion = newVersion;
                await _productService.InitItemVersion(itemStruct, product, newItemModel, newVersion);
                await _dbContext.ProductItem.AddAsync(newItemModel);
            }
            else
            {
                newItemModel = langItemInfo;
                var historyVersion = newItemModel.HistoryVersionList;
                historyVersion.Add(newVersion);
                newItemModel.HistoryVersion = JsonConvert.SerializeObject(historyVersion);
                newItemModel.CurrentVersion = newVersion;
                var jsonProduct = JsonConvert.SerializeObject(itemStruct, new JsonSerializerSettings()
                {
                    TypeNameHandling = TypeNameHandling.All
                });
                var jsonFilePath = await _productService.UploadStructFile(jsonProduct, newVersion, product.ID);
                newItemModel.FilePath = jsonFilePath;
                newItemModel.FilePathHashCode = Helpers.CreateHashCode(jsonFilePath);
                newItemModel.FilesSize += jsonProduct.Length;
                await _dbContext.ProductItem.SingleUpdateAsync(newItemModel);
            }
            

            product.FilesSize += newItemModel.FilesSize;
            product.UpdatedAt = now;
            product.SetLanguage(req.Language,true);
            await _dbContext.Product.SingleUpdateAsync(product);
            await _dbContext.SaveChangesAsync();
            await transaction.CommitAsync();
            return Success();
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            Console.WriteLine(e);
            return Fail(ProductTranslationFailed);
            throw;
        }
    }

    // public Variants ReplaceVariantsImgId(Dictionary<int, int> imageComparison, Variants variants)
    // {
    //     if (variants.Count > 0)
    //     {
    //         foreach (var item in variants)
    //         {
    //             //处理主图
    //             if (item.Images.Main.HasValue)
    //             {
    //                 item.Images.Main = imageComparison.ContainsKey((int)item.Images.Main)
    //                     ? imageComparison[(int)item.Images.Main]
    //                     : null;
    //             }
    //
    //             if (item.Images.Affiliate != null && item.Images.Affiliate.Count > 0)
    //             {
    //                 var affiliate = new List<int>();
    //                 var oldAffiliate = item.Images.Affiliate;
    //                 foreach (int i in oldAffiliate)
    //                 {
    //                     if (imageComparison.ContainsKey(i))
    //                     {
    //                         affiliate.Add(imageComparison[i]);
    //                     }
    //                 }
    //
    //                 item.Images.Affiliate = affiliate;
    //             }
    //         }
    //     }
    //
    //     return variants;
    // }
}

public class SetDto
{
    public int ID { get; set; }
    public string Name { get; set; }
    public string? Keywords { get; set; }
    public List<string>? Sketch { get; set; }
    public string? Desc { get; set; }

    public List<Attribute> Attribute { get; set; }
    public string TargetLang { get; set; }
    public Languages Language => Enum.Parse<Languages>(TargetLang, true);
    public int LanguageId => Language == Languages.Default ? 0 : (int)Math.Log2((long)Language) + 1;
    
    public Dictionary<string, string>? Map { get; set; }
}