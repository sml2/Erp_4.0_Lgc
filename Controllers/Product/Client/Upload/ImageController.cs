using ERP.Data;
using ERP.Exceptions.Upload;
using ERP.Interface;
using ERP.Models.DB.Product;
using ERP.Services.Product;
using ERP.Services.Upload;
using Microsoft.VisualBasic.CompilerServices;
using Newtonsoft.Json;

namespace ERP.Controllers.Product.Client.Upload;
[Route("client/product")]
public class ImageController : BaseController
{
    const int NetworkImageDownloadFailed = 1; //网络图片下载失败
    const int ImageUploadFailed = 2; //图片上传oss失败
    const int DownloadFailed = 3; //下载失败
    const int FailedExtensions = 5;
    const int ProductIdIsEmpty = 6;
    const int ProductIsEmpty = 7;
    const int TempImagesInsertFailed = 9;
    const int ImagesEmpty = 10;

    protected readonly ProductService _productService;
    protected readonly UploadService _uploadService;

    public ImageController(ProductService productService, ISessionProvider sessionProvider, DBContext dbContext,
        UploadService uploadService, IOemProvider oemProvider) : base(dbContext, sessionProvider, oemProvider)
    {
        _productService = productService;
        _uploadService = uploadService;
    }


    public override int GetApiId()
    {
        return 130;
    }
    [HttpPost("image")]
    // public async Task<object> Handle(ImageDto req)
    public async Task<object> Handle([FromForm(Name = "request")]string request)
    {
        var req = JsonConvert.DeserializeObject<ImageDto>(request);
        if (req == null)
            return Fail(ERR_PARAMETER);
        
        var id = req.ID;

        if (id < 0)
        {
            return Fail(ProductIdIsEmpty);
        }

        object res = new { };
        switch (req.DataType)
        {
            case DataTypeEnum.Base64:
                res = await Base64Image(req);
                break;
            case DataTypeEnum.Network:
                res = await NetworkImage(req);
                break;
            case DataTypeEnum.Url:
                res = await UrlImage(req);
                break;
        }

        return res;
    }

    private async Task<object> Base64Image(ImageDto req)
    {
        var base64 = req.Base64Data;
        if (base64.IsNull())
        {
            return Fail(ImagesEmpty);
        }

        var extension = req.Extension;

        if (extension.IsNull())
        {
            return Fail(FailedExtensions);
        }

        try
        {
            var path = await _uploadService.UploadImageByBase64(base64!, extension!, _oemId, _userId);
            var size = await _uploadService.GetSize(path);
            var tempImageModel = new TempImagesModel(size,
                path,
                _oemProvider.OEM.OssId,
                ResourceModel.Types.Storage,
                _session);

            await _dbContext.TempImages.AddAsync(tempImageModel);
            var state = await _dbContext.SaveChangesAsync();
            if (state < 0)
            {
                await _uploadService.Delete(path);
            }
            else
            {
                return Success(new
                {
                    id = tempImageModel.ID,
                    size = size,
                    imgUrl = path
                });
            }
        }
        catch (UploadException e)
        {
            return Fail(NetworkImageDownloadFailed, e.Message);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return Fail(TempImagesInsertFailed, e.Message);
        }

        return Fail(TempImagesInsertFailed);
    }


    private async Task<object> NetworkImage(ImageDto req)
    {
        var url = req.Url;
        if (Url.IsNull())
        {
            return Fail(ImagesEmpty);
        }

        try
        {
            var path = await _uploadService.UploadImageByNetwork(req.Url, _oemId, _userId);
            var size = await _uploadService.GetSize(path);
            var tempImageModel = new TempImagesModel(size,
                path,
                _oemProvider.OEM.OssId,
                ResourceModel.Types.Storage,
                _session);

            await _dbContext.TempImages.AddAsync(tempImageModel);
            var state = await _dbContext.SaveChangesAsync();
            if (state < 0)
            {
                await _uploadService.Delete(path);
            }
            else
            {
                return Success(new
                {
                    id = tempImageModel.ID,
                    size = size,
                    imgUrl = path
                });
            }
        }
        catch (UploadException e)
        {
            return Fail(NetworkImageDownloadFailed, e.Message);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return Fail(TempImagesInsertFailed, e.Message);
        }

        return Fail(TempImagesInsertFailed);
    }

    private async Task<object> UrlImage(ImageDto req)
    {
        var url = req.Url;
        if (Url.IsNull())
        {
            return Fail(ImagesEmpty);
        }

        var tempImageModel = new TempImagesModel(-1,
            url,
            _oemProvider.OEM.OssId,
            ResourceModel.Types.Network,
            _session);
        await _dbContext.TempImages.AddAsync(tempImageModel);
        var state = await _dbContext.SaveChangesAsync();
        if (state < 0)
        {
            return Fail(TempImagesInsertFailed);
        }

        return Success(new
        {
            id = tempImageModel.ID,
            size = -1,
            imgUrl = url
        });
    }
}

public class ImageDto
{
    public DataTypeEnum DataType { get; set; }
    public int ID { get; set; }
    public string? Base64Data { get; set; }
    public string? Extension { get; set; }
    public string? Url { get; set; }
}

public enum DataTypeEnum
{
    Base64 = 1,
    Network = 2,
    Url = 3
}