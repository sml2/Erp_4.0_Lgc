using Common.Enums.Products;
using ERP.Data;
using ERP.Data.Products;
using ERP.DomainEvents.Product;
using ERP.Exceptions.Product;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.DB.Product;
using ERP.Services.Caches;
using ERP.Services.Product;
using ERP.Services.Upload;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Z.EntityFramework.Plus;
using Enumerable = System.Linq.Enumerable;
using Exception = System.Exception;
using Unit = ERP.Services.Caches.Unit;

namespace ERP.Controllers.Product.Client.Upload;

[Route("client/product")]
public class UpdateController : BaseController
{
    protected readonly PlatformCache _platformCache;
    protected readonly Unit _uniCache;
    protected readonly ProductService _productService;
    protected readonly ConvertProduct _convertProduct;
    private readonly IMediator _mediator;
    private readonly UploadService _uploadService;

    public UpdateController(DBContext dbContext, ISessionProvider sessionProvider, IOemProvider oemProvider,
        PlatformCache platformCache, ProductService productService, ConvertProduct convertProduct,
        Unit uniCache, IMediator mediator, UploadService uploadService) : base(
        dbContext, sessionProvider, oemProvider)
    {
        _platformCache = platformCache;
        _productService = productService;
        _convertProduct = convertProduct;
        _uniCache = uniCache;
        _mediator = mediator;
        _uploadService = uploadService;
    }

    private const int ProductIdIsEmpty = 9; //产品id为空
    private const int ProductIsEmpty = 2; //产品数据为空
    private const int OverMaxLimitCount = 3; //商品数超过上限
    private const int CurrencyFail = 4; //货币不存在
    private const int SourceUrlToLong = 5; //产品来源Url过长
    private const int PlatformIsEmpty = 6; //平台标识为空
    private const int NotFoundLanguage = 7; //语言标识不合法,需要升级最新版系统
    private const int NotFoundPlatform = 8; //平台标识不合法,需要升级最新版系统
    private const int ProductUpdateFail = 1; //产品更新失败
    private const int VariantsFail = 10; //变体数据出错
    private const int FailCurrency = 11; //货币不存在
    private const int FailPrice = 12; //货币不存在
    private const int FailMakeUp = 13; //组合失败
    private const int ImagesIsNull = 14; //组合失败
    private const int VariantDuplication = 15; //变体重复

    public override int GetApiId()
    {
        return 150;
    }

    [HttpPost("upload")]
    public async Task<object> Handle([FromForm(Name = "request")] string request)
    {
        var req = JsonConvert.DeserializeObject<UploadDto>(request);
        if (req == null)
            return Fail(ERR_PARAMETER);

        var productId = req.Id;
        if (productId < 0)
        {
            return Fail(ProductIdIsEmpty);
        }

        var collectPlatform = _platformCache?.List()?
            .Where(m => m.Collect)
            .ToList();

        if (collectPlatform is null)
        {
            return Fail(NotFoundPlatform);
        }

        var platformInfo = collectPlatform?.Where(m => m.English.ToLower() == req.Platform.ToLower() && m.Collect)
            .FirstOrDefault();

        if (platformInfo is null)
        {
            return Fail(NotFoundPlatform);
        }

        var model = _dbContext.Product.Where(m =>
            // (m.Type & (long)Types.OnGoing) == (long)Types.OnGoing
            // && 
            m.ID == productId);
        var product = model.FirstOrDefault();
        if (product is null)
        {
            return Fail(ProductIsEmpty);
        }


        var attributes = req.Attributes ?? new List<Attribute>();
        //校验属性值是否重复
        foreach (var item in attributes)
        {
            var values = item.Values;
            foreach (var value in values)
            {
                var count = values.Count(m => m == value);
                if (count > 1)
                {
                    return Fail(VariantDuplication);
                }
            }
        }

        var products = req.Products ?? new List<Product>();
        var currency = req.Currency;

        if (currency.Length <= 0)
        {
            return Fail(CurrencyFail, currency);
        }

        if (currency == "RMB")
        {
            currency = "CNY";
        }

        var currencyInfo = _uniCache?.List()?.Where(m => m.Sign.ToLower() == currency.ToLower()).FirstOrDefault();
        if (currencyInfo is null)
        {
            return Fail(CurrencyFail, currency);
        }

        var variants = new Variants();
        if (attributes.Count > 0)
        {
            // 所有变体的ids
            var productsIdsOrder = ResolveProducts(attributes);
            // 采集到的变体
            var productsIds = GetProductsIds(products);

            if (productsIds.Count != products.Count)
            {
                return Fail(VariantsFail);
            }

            if (attributes.Count > 0 && productsIds.Count <= 0)
            {
                return Fail(VariantsFail);
            }

            if (productsIds.Count() > productsIdsOrder.Count)
            {
                return Fail(VariantsFail);
            }

            // 差集
            var differenceIds = ContrastProducts(productsIds, productsIdsOrder);
            try
            {
                variants = MakeUpVariants(products, attributes, differenceIds, req.Currency);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                if (Int32.Parse(e.Message) == VariantsFail)
                {
                    return Fail(VariantsFail);
                }

                if (Int32.Parse(e.Message) == FailPrice)
                {
                    return Fail(FailPrice);
                }
            }
        }

        var reqImages = req.Images ?? new List<int>();
        var reqAllImages = req.AllImages ?? new Dictionary<string, string>();

        if (variants.Count == 0)
        {
            attributes.Add(new Attribute()
            {
                Id = 0,
                Name = "默认变体",
                Values = new List<string>() { "default" }
            });
            variants.Add(new Variant()
            {
                Images = new VariantImages()
                {
                    Main = reqImages.FirstOrDefault(),
                    Affiliate = reqImages.Skip(1).ToList()
                },
                State = VariantStateEnum.Enable,
                Ids = new Dictionary<int, int>() { { 0, 0 } },
                Cost = MoneyFactory.Instance.Create(req.Price, currency),
                Sale = MoneyFactory.Instance.Create(req.Price, currency),
                Sid = Helpers.CreateHashCode("默认变体:default").ToString(),
                Quantity = 50,
                //hash_code
            });
        }

        var allImageIds = reqAllImages.Keys.Select(m => Int32.Parse(m)).ToList();
        var itemModelParams = new ProductItemModel(_sessionProvider.Session!);
        var images = _dbContext.TempImages.Where(m => allImageIds.Contains(m.ID)).Select(m => new Image(m)).ToList();
        // if (images.Count <= 0)
        // {
        //     return Fail(ImagesIsNull);
        // }
        var structProduct = _convertProduct.ToBeServiceStruct(req, currency, images, variants, attributes);
        

        if (structProduct is null)
        {
            return Fail(FailMakeUp);
        }
        
        //设置产品描述编辑器
        if (!req.DescriptionType.HasValue)
        {
            try
            {
                structProduct.SetDescriptionType(ISessionExtension.GetProductEditor(_session));
            }
            catch (Exception e)
            {
                structProduct.SetDescriptionType(DescriptionTypeEnum.PlainText);
            }
        }
        else
        {
            structProduct.SetDescriptionType(req.DescriptionType.Value);
        }

        var now = DateTime.Now;
        product.SetOngoing(false);
        product.SetCollect(true);
        product.IsImageAsync = req.ImageType == DataTypeEnum.Url;
        product.Title = req.Name;
        product.Source = req.Url;
        product.Pid = req.PID;
        product.Sku = req.Sku;
        product.PidHashCode = product.Pid.IsNotNull() ? Helpers.CreateHashCode(product.Pid).ToString() : null;
        var firstVariant = variants.First();
        product.MainImage = images.Count > 0 && firstVariant.Images.Main.HasValue
            ? JsonConvert.SerializeObject(images.FirstOrDefault(m => m.ID == firstVariant.Images.Main.Value))
            : null;
        product.Images = JsonConvert.SerializeObject(images);
        product.UpdatedAt = now;
        product.Coin = currency;

        // structProduct.SetTypesFlag(model.Type);
        var jsonProduct = JsonConvert.SerializeObject(structProduct, new JsonSerializerSettings()
        {
            TypeNameHandling = TypeNameHandling.All
        });
        var historyVersion = new List<long>();
        var version = now.GetTimeStampMilliseconds();
        historyVersion.Add(version);
        itemModelParams.LanguageName = "default";
        itemModelParams.LanguageId = 0;
        itemModelParams.Coin = currency;
        itemModelParams.Quantity = structProduct.GetQuantity();
        itemModelParams.CurrentVersion = version;
        itemModelParams.HistoryVersion = Newtonsoft.Json.JsonConvert.SerializeObject(historyVersion);

        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            product.Prices = JsonConvert.SerializeObject(structProduct.GetPrices());
            product.Quantity = structProduct.GetQuantity();
            product.FilesSize += jsonProduct.Length;
            await _dbContext.Product.SingleUpdateAsync(product);
            await _dbContext.SaveChangesAsync();

            //获取产品json路径
            var jsonFilePath = _productService.GetProductJsonPath(itemModelParams.CurrentVersion, product.ID);

            //临时图片迁移
            var newCreateImagesIds = reqAllImages.Keys.Select(m => Int32.Parse(m)).ToList();
            if (newCreateImagesIds.Count > 0)
            {
                var newCreateImages = await _dbContext.TempImages
                    .Where(m => newCreateImagesIds.Contains(m.ID))
                    .Select(m => new ResourceModel(m, product.ID)).ToListAsync();

                await _dbContext.Resource.AddRangeAsync(newCreateImages);
                await _dbContext.TempImages.Where(m => newCreateImagesIds.Contains(m.ID)).DeleteAsync();

                var newCreateImagesSize = newCreateImages.Sum(m => m.Size);
                var savedImagesSize = await _dbContext.Resource
                    .Where(m => m.ProductId == product.ID)
                    .SumAsync(m => m.Size);
                //计算产品图片大小
                product.ImagesSize = newCreateImagesSize + savedImagesSize;
            }

            //Pid
            itemModelParams.Pid = product.ID;
            //写入文件路径
            itemModelParams.FilePath = jsonFilePath;
            itemModelParams.FilePathHashCode = Helpers.CreateHashCode(jsonFilePath);
            //文件大小
            itemModelParams.FilesSize += jsonProduct.Length;
            await _dbContext.ProductItem.AddAsync(itemModelParams);
            await _dbContext.SaveChangesAsync();
            //设置重复次数
            await _productService.SetUserLevelRepeatNum(product.HashCode);


            var uploadState = await _uploadService.PutAsync(jsonFilePath, jsonProduct);

            if (!uploadState)
            {
                throw new SavedException("数据包存储失败");
            }

            //关键词追加使用次数
            await transaction.CommitAsync();

            #region 新增产品数/当前产品数

            var productCreatedEvent = new ProductCreatedEvent(product);
            await _mediator.Publish(productCreatedEvent);

            #endregion

            return Success();
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            return Fail(ProductUpdateFail, e.ToString());
        }
    }

    private Variants MakeUpVariants(List<Product> products, List<Attribute> attributes, List<List<int>> differenceIds,
        string unit)
    {
        var data = new Variants();
        foreach (var product in products)
        {
            // var str = string.Empty;
            var value = new Dictionary<int, int>();
            for (int i = 0; i < product.IDS.Count; i++)
            {
                var val = product.IDS[i];
                if (val < 0)
                {
                    throw new Exception(VariantsFail.ToString());
                }

                value.Add(i, val);
            }
            // foreach (var val in product.IDS)
            // {
            //     var key = product.IDS.IndexOf(val);
            //     // str += $"{attributes[key].Name}:{product.Value[key]}";
            //     if (val < 0)
            //     {
            //         throw new Exception(VariantsFail.ToString());
            //     }
            //
            //     value.Add(key, val);
            // }

            var variantName = _convertProduct.GetVariantName(attributes, value);
            data.Add(new Variant()
            {
                Images = new VariantImages()
                {
                    Main = product.Images.First(),
                    Affiliate = product.Images.Skip(1).ToList(),
                },
                Ids = value,
                Cost = MoneyFactory.Instance.Create(product.Price, unit),
                Sale = MoneyFactory.Instance.Create(product.Price, unit),
                Quantity = product.Quantity,
                //采集的sid可能存在重复，弃用采集，采用系统生成
                // Sid = product.SID.Replace(' ', '-'),
                Sid = Helpers.CreateHashCode(variantName).ToString(),
                State = VariantStateEnum.Enable
                //hash_code
            });
        }

        if (differenceIds.Count > 0)
        {
            foreach (var item in differenceIds)
            {
                // var str = string.Empty;
                var value = new Dictionary<int, int>();
                // foreach (var val in item)
                for (var i = 0; i < item.Count; i++)
                {
                    var key = i;
                    var val = item[i];
                    // str += $"{attributes[key].Name}:{attributes[key].Values[key]}";
                    if (val < 0)
                    {
                        throw new Exception(VariantsFail.ToString());
                    }

                    value.Add(key, val);
                }
                var variantName = _convertProduct.GetVariantName(attributes, value);
                data.Add(new Variant()
                {
                    Images = new VariantImages()
                    {
                        Main = null,
                        Affiliate = new List<int>(),
                    },
                    Ids = value,
                    Cost = 0,
                    Sale = 0,
                    Quantity = 0,
                    Sid = Helpers.CreateHashCode(variantName).ToString(),
                    State = VariantStateEnum.Disable
                    //hash_code
                });
            }
        }

        return data;
    }


    private List<List<int>> ContrastProducts(List<List<int>> productIds, List<List<int>> productIdsOrder)
    {
        return productIdsOrder
            .Where(m => !productIds.Any(p => p.Select((id, i) => (id, i)).All(item => item.id == m[item.i]))).ToList();
    }

    private List<List<int>> ResolveProducts(List<Attribute> attributes)
    {
        var attrValue = new List<List<string>>();
        //属性,替换英文逗号
        foreach (var item in attributes)
        {
            var tmp = new List<string>();
            foreach (var attr in item.Values)
            {
                tmp.Add(attr.Replace(',', '，'));
            }

            attrValue.Add(tmp);
        }

        return ProductsIdsOrderBy(attrValue, new List<string>() { "" }, 0);
    }

    private List<List<int>> ProductsIdsOrderBy(List<List<string>> attr, List<string>? makeUp = null, int index = 0)
    {
        var attrLen = attr.Count;
        if (attrLen == index)
        {
            // var arr = new List<List<string>>();
            // foreach (var v in makeUp)
            // {
            //     var value = Helpers.DelLastComma(v);
            //     arr.Add(value.Split(",").ToList());
            // }
            var arr = makeUp?.Select(m => Helpers.DelComma(m).Split(",").ToList()).ToList();
            var ids = new List<List<int>>();
            foreach (var item in arr)
            {
                var temp = new List<int>();
                foreach (var i in item)
                {
                    temp.Add(ProductsKey(i, attr));
                }

                ids.Add(temp);
            }

            return ids;
            // return arr.Select(m => m.Select(m => ProductsKey(m, attr)).ToList()).ToList();
        }
        else
        {
            var collect = new List<string>();
            var tempLen = makeUp.Count;
            var currentAttr = attr[index];
            var currentAttrLen = currentAttr.Count;

            for (int i = 0; i < currentAttrLen; i++)
            {
                for (int j = 0; j < tempLen; j++)
                {
                    collect.Add($"{makeUp[j]},{currentAttr[i]}");
                }
            }

            index += 1;
            return ProductsIdsOrderBy(attr, collect, index);
        }
    }

    private int ProductsKey(string findVal, List<List<string>> attr)
    {
        foreach (var v in attr)
        {
            foreach (var m in v)
            {
                if (m == findVal)
                {
                    return v.IndexOf(m);
                }
            }
        }

        throw new Exception("Illegal Value");
    }

    private List<List<int>> GetProductsIds(List<Product> products)
    {
        return Enumerable.DistinctBy(products.Select(m => m.IDS), m => string.Join("-", m)).ToList();
    }
}

public class UploadDto
{
    /// <summary>
    /// 产品图片类型
    /// </summary>
    public DataTypeEnum ImageType { get; set; }

    /// <summary>
    /// 主键ID
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// 是否重复
    /// </summary>
    public bool IsRepetition { get; set; }

    /// <summary>
    /// 产品PID用户自定义
    /// </summary>
    public string PID { get; set; } = string.Empty;

    /// <summary>
    /// 产品标题
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// 平台
    /// </summary>
    public string Platform { get; set; }

    /// <summary>
    /// 变体
    /// </summary>
    public List<Product>? Products { get; set; }

    /// <summary>
    /// 分类 默认为采集任务名称
    /// </summary>
    public string Category { get; set; } = string.Empty;

    /// <summary>
    /// 产品来源
    /// </summary>
    public string Url { get; set; } = string.Empty;

    public string Sku { get; set; } = string.Empty;

    /// <summary>
    /// 关键字
    /// </summary>
    public string Keywords { get; set; } = string.Empty;

    /// <summary>
    /// 价格
    /// </summary>
    public decimal Price { get; set; }

    /// <summary>
    /// 货币
    /// </summary>
    public string Currency { get; set; }

    /// <summary>
    /// 图库ids
    /// </summary>
    public List<int>? Images { get; set; }

    /// <summary>
    /// 未知
    /// </summary>
    public bool NeedUploadImage { get; set; }

    /// <summary>
    /// 属性
    /// </summary>
    public List<Attribute>? Attributes { get; set; }

    /// <summary>
    /// 店铺名称
    /// </summary>
    public string Shopname { get; set; } = string.Empty;

    /// <summary>
    /// 卖点
    /// </summary>
    public string Sketch { get; set; } = string.Empty;

    /// <summary>
    /// 描述
    /// </summary>
    public string Description { get; set; } = string.Empty;

    /// <summary>
    /// 属性
    /// </summary>
    public string? Fields { get; set; } = string.Empty;

    /// <summary>
    /// 图库信息
    /// </summary>
    public Dictionary<string, string>? AllImages { get; set; }

    /// <summary>
    /// 图片大小
    /// </summary>
    public Dictionary<string, int>? ImagesInfo { get; set; }
    
    public DescriptionTypeEnum? DescriptionType{ get; set; }
}

public class Attribute
{
    /// <summary>
    /// ID
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// 属性名称
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// SKUID
    /// </summary>
    public string SkuID { get; set; }

    /// <summary>
    /// 属性值
    /// </summary>
    public List<string> Values { get; set; }
}

public class Product
{
    /// <summary>
    /// SID
    /// </summary>
    public string SID { get; set; } = string.Empty;

    /// <summary>
    /// 变体属性
    /// </summary>
    public List<string> Value { get; set; }

    /// <summary>
    /// 变体属性IDS
    /// </summary>
    public List<int> IDS { get; set; }

    /// <summary>
    /// 变体图
    /// </summary>
    public List<int> Images { get; set; }

    /// <summary>
    /// 价格
    /// </summary>
    public decimal Price { get; set; }

    /// <summary>
    /// 库存
    /// </summary>
    public int Quantity { get; set; }

    /// <summary>
    /// ID
    /// </summary>
    public int ID { get; set; }
}