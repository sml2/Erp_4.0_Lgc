using ERP.Data;
using ERP.Enums.Identity;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.Product;
using ERP.Services.Caches;
using ERP.Services.Product;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Ryu.Linq;

namespace ERP.Controllers.Product.Client.Upload;

[Route("client/product")]
public class CheckController : BaseController
{
    protected readonly PlatformCache _platformCache;
    protected readonly Company _companyCache;
    protected readonly ProductService _productService;

    public CheckController(PlatformCache platformCache, DBContext dbContext, ISessionProvider sessionProvider,
        ProductService productService, Company companyCache, IOemProvider oemProvide) : base(dbContext, sessionProvider,
        oemProvide)
    {
        _platformCache = platformCache;
        _productService = productService;
        _companyCache = companyCache;
    }

    const int TitleRepetition = 1; //标题重复
    const int TitleIsEmpty = 2; //标题为空
    const int TitleProhibitedWords = 3; //标题存在违禁词
    const int ProductSeizeASeatFailed = 4; //产品占位失败
    const int FailPlatform = 5; //错误的采集平台
    const int TaskInsertFailed = 6; //采集任务创建失败
    const int FailCompany = 7; //采集任务创建失败
    const int PlatformIsNull = 7; //采集任务创建失败

    public override int GetApiId()
    {
        return 120;
    }

    /// <summary>
    /// 违禁词检测
    /// </summary>
    /// <param name="words"></param>
    /// <param name="word"></param>
    /// <returns></returns>
    private string? IllegalWordCheck(List<string> words, string word)
    {
        if (words.Contains(word))
        {
            var index = words.IndexOf(word);
            return words[index];
        }

        return null;
    }

    [HttpPost("init")]
    public async Task<object> Handle([FromForm(Name = "request")]string request)
    {
        var req = JsonConvert.DeserializeObject<CheckDto>(request);
        var oemId = _oemId;
        var companyId = _companyId;
        var userId = _userId;
        var groupId = _groupId;
        

        var collectPlatform = _platformCache?.List()?
            .Where(m => m.Collect)
            .ToList();

        if (collectPlatform is null)
        {
            return Fail(PlatformIsNull);
        }

        var platformInfo = collectPlatform.FirstOrDefault(m => m.English.ToLower() == req.Platform.ToLower() && m.Collect);

        if (platformInfo is null)
        {
            return Fail(FailPlatform);
        }

        var taskId = 0;
        var taskHashCode = Helpers.CreateHashCode($"{req.TaskName}{req.Platform}").ToString();

        var taskInfo = await _dbContext.Task
            .Where(m => m.OEMID == oemId
                        && m.CompanyID == companyId
                        && m.UserID == userId
                        && m.HashCode == taskHashCode
                        && !m.IsDel
            ).FirstOrDefaultAsync();

        if (taskInfo is not null)
        {
            taskId = taskInfo.ID;
        }
        else
        {
            var taskModel = new TaskModel()
            {
                Name = req.TaskName,
                PlatformName = platformInfo.Name,
                PlatformId = platformInfo.ID,
                OEMID = oemId,
                CompanyID = companyId,
                GroupID = groupId,
                UserID = userId,
                HashCode = taskHashCode,
                IsDel = false
            };

            await _dbContext.Task.AddAsync(taskModel);

            await _dbContext.SaveChangesAsync();
            taskId = taskModel.ID;
        }

        if (taskId < 0)
        {
            return Fail(TaskInsertFailed);
        }

        var name = req.Name;

        if (req.IsCheckWords)
        {
            var illegalWords = await _dbContext.IllegalWord
                .Where(m => m.OEMID == oemId && m.CompanyID == companyId)
                .WhenWhere(_session.GetRole() == Role.Employee, m => m.GroupID == groupId)
                .Select(m => m.Value)
                .ToListAsync();

            var illegal = IllegalWordCheck(illegalWords, name);

            if (illegal is not null)
            {
                return Fail(TitleProhibitedWords, illegal);
            }
        }

        // 占坑id
        var titleHashCode = Helpers.CreateHashCode(name);
        var isRepeat = await _productService.CheckProductTitle(titleHashCode);
        // 获取当前公司配置
        var companyInfo = _companyCache.List()?.Where(m => m.ID == companyId).FirstOrDefault();

        if (companyInfo is null)
        {
            return Fail(FailCompany);
        }

        var modelParams = new ProductModel(_session);
        modelParams.Title = req.Name;
        modelParams.HashCode = Helpers.CreateHashCode(req.Name);
        modelParams.TaskId = taskId;
        modelParams.PlatformId = platformInfo.ID;

        modelParams.SetAudit((int)companyInfo.ProductAudit);
        modelParams.SetOngoing(true);

        await _dbContext.Product.AddAsync(modelParams);
        var state = await _dbContext.SaveChangesAsync();

        if (state > 0)
        {
            var productId = modelParams.ID;
            if (isRepeat)
            {
                return Fail(TitleRepetition, productId);
            }

            return Success(productId, isRepeat ? TitleRepetition.ToString() : String.Empty);
        }


        return Fail(ProductSeizeASeatFailed);
    }
}

public class CheckDto
{
    public string Name { get; set; }
    public string TaskName { get; set; }
    public string Platform { get; set; }
    public bool IsInherits { get; set; }
    public bool IsCheckWords { get; set; }
}