using ERP.Data;
using ERP.Enums.Product;
using ERP.Interface;
using ERP.Models.DB.Product;
using ERP.Services.Product;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Org.BouncyCastle.Ocsp;

namespace ERP.Controllers.Product.Client.Upload;
[Route("client/product")]
public class ImagesController : BaseController
{
    protected readonly ProductService _productService;

    public ImagesController(DBContext dbContext, ProductService productService, ISessionProvider sessionProvider,
        IOemProvider oemProvider) : base(dbContext,sessionProvider,oemProvider)
    {
        _productService = productService;
    }

    const int NotArray = 1; //数据格式错误,非数组
    const int ArrayIsEmpty = 2; //数据格式错误,数组为空
    const int TempImagesInsertFailed = 3; //数据保存失败
    const int ProductIdIsEmpty = 4;
    const int ProductIsEmpty = 5;

    public override int GetApiId()
    {
        return 140;
    }
    [HttpPost("images")]
    public async Task<object> Handle([FromForm(Name = "request")]string request)
    {
        var req = JsonConvert.DeserializeObject<ImagesDto>(request);
        var productId = req.Id;

        if (productId < 0)
        {
            return Fail(ProductIdIsEmpty);
        }

        var productModel = await _productService.GetOnGoingProduct(productId);

        if (productModel is null)
        {
            return Fail(ProductIsEmpty);
        }

        var urls = req.Urls;
        if (urls.Count <= 0)
        {
            return Fail(ArrayIsEmpty);
        }

        var tempImageModels = urls
            .Select(m => new TempImagesModel(
                -1,
                m,
                _oemProvider.OEM.OssId,
                ResourceModel.Types.Network,
                _session))
            .ToList();

        await _dbContext.TempImages.AddRangeAsync(tempImageModels);
        var state = await _dbContext.SaveChangesAsync();

        if (state > 0)
        {
            var result = tempImageModels.Select(m => new
            {
                m.ID, m.Size,
                ImgUrl = m.Path
            }).ToList();
            return Success(result);
        }

        return Fail(TempImagesInsertFailed);
    }
}

public class ImagesDto
{
    [NJP("ID")]
    public int Id { get; set; }

    public List<string> Urls { get; set; }
}