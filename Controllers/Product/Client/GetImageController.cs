using ERP.ControllersWithoutToken.Product;
using ERP.Data;
using ERP.Enums.Product;
using ERP.Interface;
using ERP.Models.Product;
using ERP.Services.Product;
using ERP.ViewModels;

namespace ERP.Controllers.Product.Client;

public class GetImageController : BaseController
{
    const int ProductIdIsEmpty = 1; //产品ID为空
    const int ProductIsEmpty = 2; //产品为空
    const int ProductImageIsEmpty = 3; //产品图片为空
    const int ProductImageIsOss = 4; //产品已经同步完成

    protected readonly ProductService _productService;

    public GetImageController(DBContext dbContext, ISessionProvider sessionProvider, IOemProvider oemProvider, ProductService productService) : base(
        dbContext, sessionProvider, oemProvider)
    {
        _productService = productService;
    }

    public override int GetApiId()
    {
        return 110;
    }

    public async Task<object> Handle(IdDto req)
    {
        if (req.ID < 0)
        {
            return Fail(ProductIdIsEmpty);
        }
        var model = _productService.ProductDataWatchRange();
        int auditState = ProductModel.AUDIT_AUDITED;
        //排除审核
        model = model.Where(m => m.Audit == auditState && m.ID == req.ID);
        model = model.Where(m =>
            (m.Type & (long)Types.Collect) == (long)Types.Collect ||
            (m.Type & (long)Types.Collect) != (long)Types.Collect);
        var product = model.FirstOrDefault();
        if (product is null)
        {
            return Fail(ProductIsEmpty);
        }

        var images = product.Gallery;

        if (images is null)
        {
            return Fail(ProductImageIsEmpty);
        }

        if (!product.IsImageAsync)
        {
            return Fail(ProductImageIsOss);
        }
        
        return new
        {
            product.ID,
            product.Title,
            Images = images
        };
    }
}