using ERP.Data;
using ERP.Interface;

namespace ERP.Controllers.Product.Client;

public class GetUploadProductController : BaseController
{
    public GetUploadProductController(DBContext dbContext, ISessionProvider sessionProvider, IOemProvider oemProvider) :
        base(
            dbContext, sessionProvider, oemProvider)
    {
    }

    public override int GetApiId()
    {
        return 210;
    }

    public object Handle()
    {
        throw new NotImplementedException();
    }
}