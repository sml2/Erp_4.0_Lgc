using ERP.Data;
using ERP.Interface;
using ERP.Models.Product;
using ERP.Models.View.Product.ImageSync;
using ERP.Services.Product;
using Newtonsoft.Json;

namespace ERP.Controllers.Product.Client.ImageSync;

[Route("client/imagesync")]
public class GetController : BaseController
{
    protected readonly ProductService _productService;

    public GetController(DBContext dbContext, ISessionProvider sessionProvider, IOemProvider oemProvider,
        ProductService productService) : base(dbContext, sessionProvider, oemProvider)
    {
        _productService = productService;
    }

    const int ProductIdIsEmpty = 1; //产品ID为空
    const int ProductIsEmpty = 2; //产品为空
    const int ProductImageIsEmpty = 3; //产品图片为空
    const int ProductImageIsObs = 4; //产品已经同步完成
    const int ProductIsNotAudit = 5; //产品未审核


    public override int GetApiId()
    {
        return 110;
    }

    [HttpPost("get")]
    public async Task<object> Handle([FromForm(Name = "request")] string request)
    {
        var req = JsonConvert.DeserializeObject<GetDto>(request)!;
        var id = req.Id;

        if (id < 0)
            return Fail(ProductIdIsEmpty);

        var product = await _productService.GetInfoByIdWithUser(id);

        if (product is null)
            return Fail(ProductIsEmpty);

        if (product.Audit != ProductModel.AUDIT_AUDITED)
            return Fail(ProductIsNotAudit);

        var images = product.Gallery;

        if (images == null)
            return Fail(ProductImageIsEmpty);

        if (req.IsConvert && !product.IsImageAsync)
            return Fail(ProductImageIsObs);
        var item = await _productService.GetItemInfoByLangId(product.ID, 0);
        var productStruct = await _productService.GetProductStruct(item.FilePath, product);

        foreach (var image in images)
        {
            if (string.IsNullOrEmpty(image.Url))
            {
                image.Url = $"http://{_oemProvider.OEM!.Oss!.BucketDomain}/{image.Path}";
            }
        }

        return Success(new
        {
            ID = product.ID,
            Title = product.Title,
            Images = images,
            VariantsImages = productStruct.Variants.Select(v => v.Images)
        });
    }
}