using ERP.Data;
using ERP.Data.Products;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.DB.Product;
using ERP.Models.View.Product.ImageSync;
using ERP.Services.Images;
using ERP.Services.Upload;
using ERP.ViewModels.UplaodImages;
using Newtonsoft.Json;
using NPOI.SS.Formula.Functions;

namespace ERP.Controllers.Product.Client.ImageSync;

[Route("client/imagesync")]
public class UploadController : BaseController
{
    protected readonly ImagesResourceService _imagesResourceService;
    protected readonly UploadService _uploadService;

    public UploadController(DBContext dbContext, ISessionProvider sessionProvider, IOemProvider oemProvider,
        ImagesResourceService imagesResourceService, UploadService uploadService) : base(
        dbContext, sessionProvider, oemProvider)
    {
        _imagesResourceService = imagesResourceService;
        _uploadService = uploadService;
    }

    private const int ImageIdIsEmpty = 1; //图片ID为空
    private const int ImageIsEmpty = 2; //图片为空
    private const int ImageImageIsObs = 3; //图片已经同步完成
    private const int ImageUploadFailed = 4; //图片上传oss失败
    private const int ImageUpdateFailed = 5; //数据修改失败
    private const int ImageExtensionsFailed = 6; //图片扩展名非法

    public override int GetApiId()
    {
        return 180;
    }

    [HttpPost("upload")]
    public async Task<object> Handle([FromForm(Name = "request")] string request)
    {
        var req = JsonConvert.DeserializeObject<UploadDto>(request)!;

        var id = req.Id;

        if (id < 0)
            return Fail(ImageIdIsEmpty);

        if (string.IsNullOrEmpty(req.Extension))
            return Fail(ImageExtensionsFailed);

        if (string.IsNullOrEmpty(req.Base64Data))
            return Fail(ImageIsEmpty);

        var imageModel = await _imagesResourceService.GetInfoById(id);

        if (imageModel == null)
            return Fail(ImageIsEmpty);

        if (req.IsConvert && imageModel.Type == ResourceModel.Types.Storage)
            return Fail(ImageImageIsObs);

        string imgPath = string.Empty;

        try
        {
            imgPath = await _uploadService.UploadImageByBase64(req.Base64Data, req.Extension, _session.GetOEMID(),
                _session.GetUserID());

        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return Fail(ImageUploadFailed, e.Message);
            
        }
        
        imageModel.Path = imgPath;
        imageModel.Size = await _uploadService.GetSize(imgPath);
        imageModel.StorageId = _oemProvider.OEM.OssId;
        imageModel.Type = ResourceModel.Types.Storage;
        imageModel.UpdatedAt = DateTime.Now;

        await _dbContext.Resource.SingleUpdateAsync(imageModel);
        var result = await _dbContext.SaveChangesAsync() > 0;

        if (!result)
        {
           await _uploadService.Delete(imgPath);
           return Fail(ImageUpdateFailed);
        }

        return Success(new Image(imageModel));
    }
}