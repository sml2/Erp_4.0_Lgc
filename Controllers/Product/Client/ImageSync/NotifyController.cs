using ERP.Data;
using ERP.Data.Products;
using ERP.Interface;
using ERP.Models.DB.Product;
using ERP.Models.View.Product.ImageSync;
using ERP.Services.Images;
using ERP.Services.Product;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Z.EntityFramework.Plus;

namespace ERP.Controllers.Product.Client.ImageSync;

[Route("client/imagesync")]
public class NotifyController : BaseController
{
    protected readonly ProductService _productService;
    protected readonly ImagesResourceService _imagesResourceService;

    public NotifyController(DBContext dbContext, ISessionProvider sessionProvider, IOemProvider oemProvider,
        ProductService productService, ImagesResourceService imagesResourceService) : base(dbContext, sessionProvider,
        oemProvider)
    {
        _productService = productService;
        _imagesResourceService = imagesResourceService;
    }

    private const int ProductIdIsEmpty = 1; //产品ID为空
    private const int ProductIsEmpty = 2; //产品为空
    private const int ProductImageIsEmpty = 3; //产品图片为空
    private const int ProductImageIsObs = 4; //产品已经同步完成
    private const int ProductUpdateFailed = 5; //产品修改失败
    private const int ProductImageIsNotComplete = 6; //产品图片未同步完成

    public override int GetApiId()
    {
        return 190;
    }
[HttpPost("notify")]
    public async Task<object> Handle([FromForm(Name = "request")] string request)
    {
        var req = JsonConvert.DeserializeObject<NotifyDto>(request)!;
        var id = req.Id;

        if (id < 0)
            return Fail(ProductIdIsEmpty);

        var product = await _productService.GetInfoByIdWithUser(id);

        if (product is null)
            return Fail(ProductIsEmpty);
        
        var itemList = await _productService.GetItemInfoList(product.ID);

        if (itemList.Count <= 0)
            return Fail(ProductIsEmpty);

        var images = product.Gallery;
        var mainImageObj = product.MainImageObj;

        if (images == null)
            return Fail(ProductImageIsEmpty);

        if (req.IsConvert && !product.IsImageAsync)
            return Fail(ProductImageIsObs);
        
        //临时图片迁移
        var newCreateImagesIds = req.MainImages?.Distinct().ToList() ?? new List<int>();
        if (newCreateImagesIds.Count > 0)
        {
            var newCreateImages = await _dbContext.TempImages
                .Where(m => newCreateImagesIds.Contains(m.ID))
                .Select(m => new ResourceModel(m, product.ID)).ToListAsync();

            await _dbContext.Resource.AddRangeAsync(newCreateImages);
            await _dbContext.TempImages.Where(m => newCreateImagesIds.Contains(m.ID)).DeleteAsync();
            await _dbContext.SaveChangesAsync();
        }

        var syncImages = await _imagesResourceService.GetImagesByPid(req.Id, ResourceModel.Types.Storage);

        if (syncImages.Count <= 0)
            return Fail(ProductImageIsEmpty);

        if (images.Count + newCreateImagesIds.Count < syncImages.Count)
            return Fail(ProductImageIsNotComplete);

        var dicSyncImages = syncImages.ToDictionary(m => m.ID, m => m);

        //图库
        foreach (var item in images)
        {
            if (dicSyncImages.Keys.Contains(item.ID))
            {
                var syncItem = dicSyncImages[item.ID];
                item.Path = syncItem.Path;
                item.Size = syncItem.Size;
                item.Time = syncItem.UpdatedAt;
            }
           
        }

        if (req.MainImages is not null)
        {
            foreach (var mainImage in req.MainImages)
            {
                if (dicSyncImages.TryGetValue(mainImage, out var syncItem))
                {
                    images.Add(new Image(syncItem));
                }
            }
        }
        
        //主图
        if (mainImageObj != null)
        {
            if (dicSyncImages.Keys.Contains(mainImageObj.ID))
            {
                var syncMainItem = dicSyncImages[mainImageObj.ID];
                mainImageObj.Path = syncMainItem.Path;
                mainImageObj.Size = syncMainItem.Size;
                mainImageObj.Time = syncMainItem.UpdatedAt;
            }
        }
        
        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            //主图
            if (mainImageObj != null)
            {
                product.MainImage = JsonConvert.SerializeObject(mainImageObj);

            }
            //图库
            product.Images = JsonConvert.SerializeObject(images);
            product.IsImageAsync = false;
            product.UpdatedAt = DateTime.Now;
            product.ImagesSize = images.Sum(i => i.Size);
            
            List<ProductItemModel> itemModels = new List<ProductItemModel>();
            var updatedAt = DateTime.Now;
            var version = updatedAt.GetTimeStampMilliseconds();
            
            foreach (var proItem in itemList)
            {
                var currentVersionJsonFilepath = proItem.FilePath;
                var productStruct = await _productService.GetProductStruct(currentVersionJsonFilepath,product);
                productStruct.MainImage = mainImageObj;
                productStruct.Images = images;
                productStruct.Size = images.Sum(m => m.Size).ToString();

                if (req.MainImages is not null && req.MainImages.Count == productStruct.Variants.Count)
                {
                    for (int i = 0; i < req.MainImages.Count; i++)
                    {
                        var newMainImage = req.MainImages[i];
                        if (newMainImage <= 0)
                            continue;
                        
                        var variant = productStruct.Variants[i];
                        if (variant.Images.Main is not null)
                        {
                            variant.Images.Affiliate ??= new List<int>();
                            variant.Images.Affiliate.Insert(0, variant.Images.Main.Value);
                        }

                        variant.Images.Main = newMainImage;
                    }
                }
                
                proItem.CurrentVersion = version;
                var historyVersionList = proItem.HistoryVersionList;
                historyVersionList.Add(version);
                proItem.HistoryVersion = JsonConvert.SerializeObject(historyVersionList);
                
                var jsonProduct = JsonConvert.SerializeObject(productStruct, new JsonSerializerSettings()
                {
                    TypeNameHandling = TypeNameHandling.All
                });
                
                var jsonFilePath = await _productService.UploadStructFile(jsonProduct, version, product.ID);
                proItem.FilePath = jsonFilePath;
                proItem.FilePathHashCode = Helpers.CreateHashCode(jsonFilePath);
                proItem.FilesSize += jsonProduct.Length;
                
                itemModels.Add(proItem);
                product.FilesSize += proItem.FilesSize;
            }
            
            await _dbContext.Product.SingleUpdateAsync(product);
            _dbContext.ProductItem.UpdateRange(itemModels);
            await _dbContext.SaveChangesAsync();
            await transaction.CommitAsync();
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            Console.WriteLine(e.Message);
            return Fail(ProductUpdateFailed,e.Message);
        }

        return Success();

    }
}