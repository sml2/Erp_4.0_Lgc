using ERP.Data;
using ERP.Interface;

namespace ERP.Controllers.Product.Client;

public class GetNodesController : BaseController
{
    public GetNodesController(DBContext dbContext, ISessionProvider sessionProvider, IOemProvider oemProvider) : base(
        dbContext, sessionProvider, oemProvider)
    {
    }

    public override int GetApiId()
    {
        return 220;
    }

    public object Handle()
    {
        throw new NotImplementedException();
    }
}