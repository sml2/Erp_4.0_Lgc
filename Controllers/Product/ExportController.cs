﻿using ERP.Data;
using ERP.Enums;
using ERP.Export.Queue;
using ERP.Export.Templates;
//using ERP.Export.Templates;
using ERP.Extensions;
using ERP.Models.Export;
using ERP.Models.View.Products.Export;
using ERP.Services.Product;
using ERP.ViewModels;
using Newtonsoft.Json;
using ERP.Models.View.Products.Product;
using ERP.Services.Export;
using ERP.Services.Host;
using ERP.Services.Identity;
using Newtonsoft.Json.Linq;
using ERP.Services.Stores;
using Microsoft.EntityFrameworkCore;
using UnitCache = ERP.Services.Caches.Unit;

namespace ERP.Controllers.Product;

using SyncExportModel = Models.Export.SyncExport;

[Route("api/[controller]/[action]")]
[Route("api/product/[controller]/[action]")]
[ApiController]
public class ExportController : BaseController
{
    private readonly SyncExportService _syncExportServiceService;
    private readonly SyncProductService _syncProductService;
    private readonly ExportProductService _exportProductServiceService;
    private readonly ProductService _productService;
    private readonly UnitCache _unitCache;
    private readonly ExportService _exportService;
    private readonly Enums.Rule.Config.Store _currentRange = Enums.Rule.Config.Store.UploadProgressRange;
    private readonly UserManager _userManager;
    private readonly DBContext _dbContext;


    public ExportController(SyncExportService syncExportServiceService
        , UnitCache unitCache
        , ProductService productService
        , ExportProductService exportProductServiceService, SyncProductService syncProductService,
        ExportService exportService, UserManager userManager, DBContext dbContext)
    {
        _syncExportServiceService = syncExportServiceService;
        _exportProductServiceService = exportProductServiceService;
        _syncProductService = syncProductService;
        _exportService = exportService;
        _userManager = userManager;
        _dbContext = dbContext;
        _unitCache = unitCache;
        _productService = productService;
    }

    /// <summary>
    /// 获取条件导出产品ids
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> GetExportIds(ListDto req)
    {
        var data = await _exportProductServiceService.GetExportIds(req);
        return Success(data);
    }

    /// <summary>
    /// 添加任务集信息
    /// </summary>
    /// <returns></returns>
    [HttpPost()]
    public async Task<ResultStruct> AddSyncExport(AddSyncExportDto req)
    {
        var result = await _syncExportServiceService.AddExportTask(req);
        return result is not null ? Success(result) : Error("添加任务失败");
    }

    /// <summary>
    /// 任务集任务
    /// </summary>
    /// <returns></returns>
    [HttpPost()]
    public async Task<ResultStruct> CopyExport(CopyExportDto req)
    {
        var result = await _syncExportServiceService.CopyExport(req);
        return result.ToResultStruct();
    }

    /// <summary>
    /// 初始化导出任务列表
    /// </summary>
    /// <returns></returns>
    [HttpPost()]
    public async Task<ResultStruct> InitExport()
    {
        return Success(data: await _exportProductServiceService.ExportList());
    }


    /// <summary>
    /// 获取导出任务集
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> GetExport(ExportQuery exportQuery)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var res = await _syncExportServiceService.GetSyncExportPage(exportQuery,range);
        return Success(res);
    }

    /// <summary>
    /// 更新导出任务名
    /// </summary>
    /// <returns></returns>
    [HttpGet("{id:int}")]
    public async Task<ResultStruct> GetExportById(int id)
    {
        var export = await _dbContext.SyncExport.Select(e => new {Id = e.ID, e.Param, e.Name}).FirstOrDefaultAsync(e => e.Id == id);
        if (export is null)
            return Error("无法找到信息");
        var removeDescriptionHtml = false;
        if (!string.IsNullOrWhiteSpace(export.Param))
        {
            var param = JObject.Parse(export.Param);
            removeDescriptionHtml = param.Property("removeDescriptionHtml", StringComparison.OrdinalIgnoreCase)?.Value
                .Value<bool?>() is true;
        }
        return Success(new
        {
            export.Id,
            export.Name,
            RemoveDescriptionHtml = removeDescriptionHtml,
        });
    }
    
    /// <summary>
    /// 更新导出任务名
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> EditExport(EditExportDto req)
    {
        var res = await _syncExportServiceService.EditExport(req);
        return Success();
    }

    /// <summary>
    /// 分配导出任务下产品EAN/UPC
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> AllotExportProduct(AllotExportProductDto req)
    {
        try
        {
            var result = await _exportProductServiceService.AllotExportProduct(req);
            return Success(result);
        }
        catch (Exception e)
        {
            return Error(e.Message);
        }
    }

    /// <summary>
    /// 删除导出任务
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> GetExportProductIds(EidDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _exportProductServiceService.GetExportProductIds(req.Eid,range);
        return Success(result);
    }

    [HttpPost()]
    public async Task<ResultStruct> DeleteExport(IdDto req)
    {
        var res = await _syncExportServiceService.DeleteExport(req.ID);
        return res ? Success() : Error();
    }

    [HttpGet("{id:int}")]
    public async Task<ResultStruct> TaskStart(int id, [FromServices] ITaskQueue<ExportQueueItem> queue)
    {
        var syncExportExists = await _dbContext.SyncExport.FirstOrDefaultAsync(e => e.ID == id);
        if (syncExportExists is null)
            return Error("导出模板不存在");
        
        if (!syncExportExists.CanExport(out var error))
            return Error(error);

        var user = await _userManager.GetUserAsync(User);
        var taskId = await _dbContext.ExportTasks
            .Where(t => t.Type == ExportTask.TaskType.Products && t.State < ExportTask.TaskState.Downloaded && t.UserID == user.Id).Select(t => t.ID)
            .FirstOrDefaultAsync();
        var name = syncExportExists.GenerateExportFileName();
        if (taskId != 0)
            return Success(new { ExistsTaskId = taskId, name }, "已有导出任务正在进行中");

        var newTask = new ExportTask()
        {
            SyncExportId = id,
            UserID = user.Id,
            GroupID = user.GroupID,
            CompanyID = user.CompanyID,
            OEMID = user.OEMID,
        };
        _dbContext.Add(newTask);
        await _dbContext.SaveChangesAsync();
        queue.Enqueue(new ExportQueueItem(newTask));
        return Success(new { TaskId = newTask.ID, name }, "新建导出任务成功");
    }
    
    [HttpGet("{id:int?}")]
    public async Task<ResultStruct> TaskState(int? id)
    {
        var task = await _dbContext.ExportTasks.FirstOrDefaultAsync(t => t.ID == id);
        if (task is null)
            return Error("任务不存在");
        var productErrors = string.IsNullOrEmpty(task.ProductErrors)
            ? null
            : JsonConvert.DeserializeObject<Dictionary<int, List<string>>>(task.ProductErrors);
        return Success(new { task.State, task.TotalCount, task.CompletedCount, task.FailedMessage, productErrors });
    }
    
    [Route("{id:int?}")]
    public async Task<IActionResult> Download(int? id)
    {
        var task = await _dbContext.ExportTasks.Include(t => t.SyncExport).FirstOrDefaultAsync(t => t.ID == id);
        if (task is null)
            return Json(Error("任务不存在"));
        if (task.State != ExportTask.TaskState.Exported)
            return Json(Error("此任务无法下载"));

        var filename = task.GenerateExportFileName();
        var encodeFilename = System.Net.WebUtility.UrlEncode(filename);

        var tempPath = task.TempPath;
        if (!System.IO.File.Exists(tempPath))
        {
            const string message = "导出文件已丢失";
            task.MarkFailed(message);
            await _dbContext.SaveChangesAsync();
            return Json(Error(message));
        }
        
        var stream = System.IO.File.OpenRead(tempPath);

        return File(stream, "application/octet-stream", encodeFilename);
    }
    
    [HttpGet("{id:int?}")]
    public async Task<ResultStruct> Downloaded(int? id)
    {
        var task = await _dbContext.ExportTasks.FirstOrDefaultAsync(t => t.ID == id);
        if (task is null)
            return Error("任务不存在");

        task.State = ExportTask.TaskState.Downloaded;
        
        await _dbContext.SaveChangesAsync();

        return Success();
    }

    [HttpPost]
    public ResultStruct TemplateVerify(TemplateVerifyDto req,
        [FromServices] ExcelTemplateFactory optionExcelTemplateFactory)
    {
        var template = optionExcelTemplateFactory[req.Format];
        if (template is null)
        {
            return Error($"无对应模板[{req.Format}]");
        }

        return template.TryValidate(req.Append, out var results) ? Success() : Warning(results, "校验未通过");
    }

    /// <summary>
    /// 获取导出信息 
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> GetExportInfo(int ID)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        //获取导出产品ids
        var ids = await _syncExportServiceService.GetExportProductIdsByTask(ID, range);

        if (ids.Count == 0)
        {
            return Error("当前导出产品模版下暂无产品");
        }

        var product = await _syncExportServiceService.GetInfoById(ID);

        if (product == null)
        {
            return Error(message: "产品不存在");
        }

        return Success(new
        {
            param = product.Param,
            export_sign = product.ExportSign,
            append = product.Append,
            ids
        });
    }

    /// <summary>
    /// 导出任务产品列表
    /// </summary>
    /// <returns></returns>
    [HttpPost()]
    public async Task<ResultStruct> GetExportProduct(GetExportProductDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var exportProductIds = await _syncExportServiceService.GetExportProductIdsByTask(req.Eid, range);

        if (exportProductIds.Count <= 0)
        {
            return Success(message: "当前导出产品模版下暂无产品");
        }

        var result = await _syncProductService.List(req, exportProductIds, range);
        return Success(result);
    }

    /// <summary>
    /// 删除导出产品
    /// </summary>
    /// <returns></returns>
    [HttpPost()]
    public async Task<ResultStruct> DeleteExportProduct(DeleteExportProductDto req)
    {
        var result = await _exportService.DeleteExportProduct(req);

        return result ? Success() : Error();
    }

    /// <summary>
    /// 产品加入已存在导出任务
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> JoinExport(JoinExportDto request)
    {
        if (request.spid.IsNull() && request.source_id.IsNull())
        {
            return Error("参数错误");
        }

        if (request.eid.IsNull())
        {
            return Error($"参数错误: {nameof(request.eid)}");
        }

        // 获取导出任务集信息
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var info = await _syncExportServiceService.GetInfoById(request.eid, range);

        if (info == null)
        {
            return Error("导出产品模版不存在");
        }

        if (!string.IsNullOrEmpty(request.spid))
        {
            // 已克隆只上传产品加入导出任务
            //todo  $res = $this->joinSyncExportProduct($spid, $info);
        }
        else
        {
            // 产品列表原始产品加入导出任务
            var res = await _exportService.JoinExportProduct(request.source_id, request.ProductConfig, info);
            return res.ToResultStruct();
        }

        return Error();
    }

    /// <summary>
    /// 已克隆产品加入导出任务
    /// </summary>
    /// <returns></returns>
    private async Task<ResultStruct> joinSyncExportProduct()
    {
        /************************************************  
    
        ************************************************/
        //todo 
        /**
         * $param = json_decode($info['param'], true);
    
        //查找克隆产品关联关系
        $syncProduct = $this->syncProductService->getInfoById($spid);
        //查找克隆产品详情
        $product = $this->productService->getInfoById($syncProduct['pid']);
        //查找克隆产品是否存在导出语言包
        if ($info['language_id'] != 0 && !isset($product['languages'][$param['language']])) {
            return returnArr(false, '当前产品不存在导出产品模版语言包');
        }
    
        $insertData = [
            'eid'         => $info['id'],
            'sid'         => $spid,
            'language'    => $param['language'],
            'language_id' => $info['language_id'],
            'export_sign' => $info['export_sign'],
            'platform_id' => $info['platform_id'],
            'user_id'     => $this->sessionData->getUserId(),
            'group_id'    => $this->sessionData->getGroupId(),
            'company_id'  => $this->sessionData->getCompanyId(),
            'oem_id'      => $this->sessionData->getOemId(),
            'created_at'  => $this->datetime,
            'updated_at'  => $this->datetime
        ];
    
        DB::beginTransaction();
        try {
            //加入导出任务产品关联信息
            $this->syncExportService->addSyncExportProduct($insertData);
            //导出任务数量自增
            $this->syncExportService->exportIncrement($info['id']);
            //更新产品克隆关联表type
            $this->syncProductService->joinProductType($syncProduct, 2);
            //更新克隆产品type
            $this->productService->updateProductType($product, 2);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return returnArr(false, $e->getMessage());
        }
         **/
        return await System.Threading.Tasks.Task.FromResult(Success());
    }


    /// <summary>
    /// 更新导出任务语言
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> UpdateExportLanguage(UpdateExportLanguageDto req)
    {
        var info = await _syncExportServiceService.GetInfoById(req.Id);
        if (info == null)
            return Error("产品不存在");

        if (info.Param == null)
            return Error("参数为空");

        var param = info.ParamObj;
        if (param == null)
            return Error("数据结构无法反序列化");

        param.SetPropOrFieldValue(nameof(ExcleRole.Language), req.Language);

        var updateData = new SyncExportModel();

        updateData.Param = JsonConvert.SerializeObject(param);
        updateData.LanguageId = req.LanguageId;


        if (req.UpdateUnit is not null)
        {
            var append = JObject.Parse(info.Append ?? "");
            var unit = append.Property("unit", StringComparison.OrdinalIgnoreCase);
            if (unit is not null)
                unit.Value = req.UpdateUnit;
            else
                append.Add("unit", req.UpdateUnit);

            updateData.Append = append.ToString(Formatting.None);
        }

        var res = await _syncExportServiceService.UpdateExport(req.Id, updateData);

        return res.ToResultStruct();
    }

    /// <summary>
    /// 检测导出、上传克隆产语言包是否存在
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> CheckExportProductLanguage(CheckExportProductLanguageDto req)
    {
        // 默认语言
        if (req.LanguageId == 0 || req.Language == Languages.Default)
            return Success();

        // 获取syncProduct
        var syncProduct = await _syncExportServiceService.GetSyncProductInfoById(req.Sid);
        if (syncProduct == null)
        {
            return Error("产品不存在");
        }

        var res = await _productService.CheckProductLanguageRT(syncProduct.Pid, req.Language);

        return res.IsSuccess ? Success(res.Reasons.First().Message) : Error(res.Reasons.First().Message);
    }

    /// <summary>
    /// 更新导出产品指定语言
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> UpdateExportProductLanguage(UpdateExportProductLanguageDto request)
    {
        var updateData = new SyncExportProduct() { Language = request.Language, LanguageId = request.LanguageId };
        var res = await _syncExportServiceService.UpdateExportProduct(m => m.Eid == request.Eid && m.Sid == request.Sid,
            updateData);
        return res.IsSuccess ? Success() : Error(res.Errors.First().Message);
    }

    /// <summary>
    /// 删除无产品任务
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> clearNoProExport()
    {
        if (Request.TryGetValue("eids", out var eIds))
        {
            return Error("参数丢失: eids");
        }

        var ids = JsonConvert.DeserializeObject<List<int>>(eIds);

        if (ids?.Count == 0)
        {
            var res = await _syncExportServiceService.DeleteExport(ids);
            return res.State ? Success() : Error();
        }
        else
        {
            var id = int.Parse(eIds);
            var res = await _syncExportServiceService.DeleteExport(id);
            return res ? Success() : Error();
        }
    }
}