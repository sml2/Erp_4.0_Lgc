using ERP.Models.View.Product;
using ERP.ViewModels;

namespace ERP.Controllers.Product;

interface IProductController
{
    public Task<ResultStruct> Show(IdDto req);

    public Task<ResultStruct> Destroy(int id);
}