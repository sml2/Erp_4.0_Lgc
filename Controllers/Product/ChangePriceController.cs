using ERP.Authorization.Rule;
using ERP.Enums.Rule.Config;
using ERP.Models.View.Product.Template;
using ERP.Models.View.Products.Template;
using ERP.Services.Product;
using ERP.Services.Identity;
using ERP.ViewModels;

namespace ERP.Controllers.Product;

[Route("api/[controller]")]
public class ChangePriceController : BaseController
{
    private readonly ChangePriceService _service;
    private readonly UserManager _userManager;
    public const ProductAbout CurrentRange = ProductAbout.TemplateRange;

    public ChangePriceController(ChangePriceService service, UserManager userManager)
    {
        _service = service;
        _userManager = userManager;
    }

    [HttpPost("list")]
    [Permission(ProductAbout.TemplateChangePrice)]
    public async Task<ResultStruct> List(ListDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, CurrentRange);
        var res = await _service.List(req, range);
        return Success(res, "获取成功");
    }

    [HttpPost("getTemplate")]
    public async Task<ResultStruct> GetTemplate()
    {
        var range = await _userManager.GetProductModuleRange(User, CurrentRange);
        var result = await _service.Get(range);
        return Success(result);
    }

    [HttpPost("edit")]
    [Permission(ProductAbout.TemplateChangePrice)]
    public async Task<ResultStruct> Edit(ChangePriceDto vm)
    {
        var range = await _userManager.GetProductModuleRange(User, CurrentRange);
        var res = await _service.AddOrUpdate(vm, range);
        return res.State ? Success(message: (vm.ID > 0) ? "修改成功" : "添加成功") : Error(message: res.Msg);
    }

    [HttpGet("destroy/{id}")]
    [Permission(ProductAbout.TemplateChangePrice)]
    public virtual async Task<ResultStruct> Destroy(int id)
    {
        var range = await _userManager.GetProductModuleRange(User, CurrentRange);
        var res = await _service.Destroy(id, range);
        return res ? Success(message: "删除成功") : Error("删除失败");
    }

    [HttpPost("show")]
    [Permission(ProductAbout.TemplateChangePrice)]
    public virtual async Task<ResultStruct> Show(IdDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, CurrentRange);
        var info = await _service.Info(req.ID, range);
        // var nations = await _service.GetAllNation();

        return Success(new { info });
    }
}