using ERP.Data;
using ERP.Enums;
using ERP.Export.Queue;
using ERP.Models.DB.Identity;
using ERP.Models.DB.Product;
using ERP.Models.Stores;
using ERP.Models.View.Products.Export;
using ERP.Models.View.Products.Product;
using ERP.Services.Caches;
using ERP.Services.DB.Product;
using ERP.Services.Identity;
using ERP.Services.Product;
using ERP.Services.Stores;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Z.EntityFramework.Plus;
using PlatformDataCache = ERP.Services.Caches.PlatformData;
using CategoryService = ERP.Services.DB.ProductType.CategoryService;

namespace ERP.Controllers.Product
{
    [Route("api/[controller]")]
    public class ProductUploadController : BaseController
    {
        private readonly ProductUploadService _service;
        private readonly UserManager _userManager;
        private readonly LanguagesCache _cache;
        private readonly DBContext _dbContext;
        private readonly PlatformDataCache platformDataCache;
        private readonly CategoryService categoryService;

        public ProductUploadController(PlatformDataCache _platformDataCache, ProductUploadService service,
            UserManager userManager, LanguagesCache cache, CategoryService _categoryService, DBContext dbContext)
        {
            _service = service;
            _userManager = userManager;
            _cache = cache;
            _dbContext = dbContext;
            platformDataCache = _platformDataCache;
            categoryService = _categoryService;
        }

        [HttpPost("init")]

        public async Task<ResultStruct> Init()
        {
           var amazonNation = platformDataCache.List(new List<Platforms>() { Platforms.AMAZONSP })
          .Where(x => x.GetAmazonData()!.Unit.IsNotWhiteSpace()).Select(x => new MarketPlaceInfo { MarketPlace = x.GetAmazonData().Marketplace, Name = x.GetAmazonData().Name }).ToList();
            return await System.Threading.Tasks.Task.FromResult(Success(new
            {
                menu = new
                {
                    uploadVerity = true,
                    uploadFail = true,
                    uploadSuccess = true,
                },               
            item = new
                {
                    search = true
                },
                @params = new
                {
                    language = _cache.List()!.ToDictionary(m => m.Code, m => m),
                    nation= amazonNation,
                }
            }));
        }

        [HttpPost("list")]
        public async Task<ResultStruct> List(UploadSearchVM req)
        {          
            var result = await _service.List(req);
            return Success(result);
        }





        [HttpPost("Delete")]
        public async Task<ResultStruct> Delete(VMPids vm)
        {
            var result = await _service.Delete(vm);
            return result ? Success() : Error("删除失败");
        }

        //[HttpPost("GetProductInfo")]
        //public ResultStruct GetProductInfo(SearchJsonVM vm)
        //{
        //    return Success(_service.GetProductInfo(vm));
        //}



        [HttpPost("GetUploadById")]
        public async Task<ResultStruct> GetUploadById(int id)
        {
             var result= await _service.GetModelAsync(id);
            return Success(result);
        }

        public class ProductUploadAddRequest
        {
            public int ProductId { get; set; }
            public int SyncUploadTaskId { get; set; }
            public int StoreId { get; set; }
            public Languages Language { get; set; }
            public string Marketplace { get; set; } = null!;
            public bool IsPush { get; set; }
            public int CountryOfOrigin { get; set; }
            public ProductConfig ProductConfig { get; set; }
            public int? Spid { get; set; }
            public DateTime LeadTime { get; set; }
            public string[] TypeData { get; set; }
            public long[] TypeDataId{ get; set; }
            public SyncProductModel.SkuTypes SkuType { get; set; }
            public JObject Params { get; set; } = null!;
            public int? ChangePriceId { get; set; }
            public string? Unit { get; set; }
            public JObject Ext { get; set; } = null!;
            public Platforms? Platform { get; set; }

            public string ProductType { get; set; }
        }

        [HttpPost("[action]")]
        public async Task<ResultStruct> Add([FromBody] ProductUploadAddRequest request)
        {
            try
            {
                long id = request.TypeDataId.Last();
                string nodeIds = string.Join(',', request.TypeDataId);
                var list = await categoryService.GetLastNodeCategory(request.Marketplace, nodeIds, id);
                if (list is not null && list.Count == 1)
                {
                    var model = list.First();
                    if (model.HasChild)
                    {
                        return Error("请选择最后的产品分类节点");
                    }
                    var res = await _service.Add(request);
                    return res.ToResultStruct();
                }
                else
                {
                    return Error("请选择最后的产品分类节点");
                }
            }
            catch (Exception e)
            {
                return Error(e);
            }
        }
        
        public class ProductUploadAddTaskRequest
        {
            public int StoreId { get; set; }
            public Languages Language { get; set; }
            public string Marketplace { get; set; } = null!;
            public string[] TypeData { get; set; }
            public Platforms Platform { get; set; }
        }
        
        [HttpPost("[action]")]
        public async Task<ResultStruct> AddTask([FromBody]ProductUploadAddTaskRequest request)
        {
            try
            {
                var res = await _service.AddTask(request);
                return res.ToResultStruct();
            }
            catch (Exception e)
            {
                return Error(e);
            }
        }
        
        public class ProductUploadEditTaskRequest
        {
            public string Name { get; set; } = null!;
        }
        
        [HttpPost("[action]/{id:int}")]
        public async Task<ResultStruct> EditTask(int id, [FromBody]ProductUploadEditTaskRequest request)
        {
            try
            {
                var res = await _service.EditTask(id, request);
                return res.ToResultStruct();
            }
            catch (Exception e)
            {
                return Error(e);
            }
        }
        
        [HttpPost("[action]/{id:int}")]
        public async Task<ResultStruct> DeleteTask(int id)
        {
            try
            {
                var res = await _service.DeleteTask(id);
                return res.ToResultStruct();
            }
            catch (Exception e)
            {
                return Error(e);
            }
        }
        
        [HttpPost("[action]/{id:int}")]
        public async Task<ResultStruct> Upload(int id, [FromServices]ITaskQueue<ProductUploadQueueItem> queue)
        {
            var task = await _dbContext.SyncUploadTasks.FindAsync(id);
            if (task is null)
                return Error("任务不存在");

            if (task.State == UploadResult.Uploading)
            {
                return Error("当前任务上传中");
            }
            
            if (task.State != UploadResult.None)
            {
                return Error("当前任务无法上传"); 
            }


            task.State = UploadResult.Uploading;
            await _dbContext.SaveChangesAsync();
            
            // 加入后台队列
            queue.Enqueue(new ProductUploadQueueItem(task));
            return Success();
        }
        
        [HttpPost("[action]")]
        public async Task<ResultStruct> AllotExportProduct(AllotExportProductDto req, [FromServices] SyncProductService service)
        {
            var result = await service.DistributionCodeToUploadProducts(req.Sid, req.Mode, req.Cover);
            return Success(result);
        }
        
        [HttpPost("[action]/{tid:int}/{spid:int}")]
        public async Task<ResultStruct> DeleteUploadProduct(int tid, int spid, [FromServices] SyncProductService service)
        {
            var productUpload = await _dbContext.productUpload.Where(p => p.SyncUploadTaskId == tid && p.Spid == spid).FirstOrDefaultAsync();

            if (productUpload is null)
                return Error("产品不存在#1");

            var transaction = await _dbContext.Database.BeginTransactionAsync();
            try
            {
                //删除关联关系
                _dbContext.Remove(productUpload);
                //任务产品数减一
                await _dbContext.SyncUploadTasks.Where(t => t.ID == tid).UpdateAsync(t => new {Amount = t.Amount - 1});

                await service.DeleteSyncProduct(spid, SyncProductModel.ProductTypes.Upload);
                await _dbContext.SaveChangesAsync();
                await transaction.CommitAsync();
            }
            catch (Exception e)
            {
                return Error(e.Message);
            }

            return Success(productUpload.ProductName!);
        }
    }
}
