﻿using ERP.Authorization.Rule;
using ERP.Enums.ID;
using ERP.Enums.Rule.Config;
using ERP.Enums.Rule.Config.Internal;
using ERP.Extensions;
using ERP.Interface.Rule;
using ERP.Models.View.Product.Template;
using ERP.Models.View.Products.Template;
using ERP.Services.Identity;
using ERP.Services.Product;
using ERP.ViewModels;
using ERP.ViewModels.Template;

namespace ERP.Controllers.Product;

[Route("api/[controller]")]
[ApiController]
public class SketchController : BaseController
{
    private readonly SketchService _service;
    private readonly UserManager _userManager;
    private readonly ProductAbout _currentRange = ProductAbout.TemplateRange;

    public SketchController(SketchService service, UserManager userManager)
    {
        _service = service;
        _userManager = userManager;
    }

    /// <summary>
    /// 初始化
    /// </summary>
    /// <returns></returns>
    [HttpGet("init")]
    [Permission(ProductAbout.TemplateSketch)]
    public Task<ResultStruct> Init()
    {
        // if($this->getSessionObj()->getConcierge() == User::TYPE_CHILD && !$this->rules()->hasSketchTemplate()){
        //     return error('无权访问');
        // }
        // $data = [
        // 'search' => ($this->getSessionObj()->getConcierge() == User::TYPE_OWN) ? true :$this->ruleInfo()->hasSketchTemplateSearch(),
        //     ];
        return Task.FromResult(Success(new { search = true }));
    }

    /// <summary>
    /// 获取卖点模板数据
    /// </summary>
    /// <returns></returns>
    [HttpPost("list")]
    [Permission(ProductAbout.TemplateSketch)]
    public async Task<ResultStruct> List(ListDto vm)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var res = await _service.List(vm, range);
        return res.State ? Success(res.Data!, "获取成功") : Error(message: res.Msg);
    }

    /// <summary>
    /// 新增修改卖点模板
    /// </summary>
    /// <param name="vm"></param>
    /// <returns></returns>
    [HttpPost("edit")]
    [Permission(ProductAbout.TemplateSketch)]
    public async Task<ResultStruct> Edit(SketchEditViewModel vm)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var res = await _service.AddOrUpdateSketch(vm, range);
        return res.State ? Success(message: (vm.ID > 0) ? "修改成功" : "添加成功") : Error(message: res.Msg);
    }


    /// <summary>
    /// 获取卖点模板
    /// </summary>
    /// <returns></returns>
    [HttpPost("getSketch")]
    public async Task<ResultStruct> GetSketch()
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _service.GetSketch(range);
        return Success(result);
    }

    [HttpPost("show")]
    [Permission(ProductAbout.TemplateSketch)]
    public virtual async Task<ResultStruct> Show(IdDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var res = await _service.Info(req.ID, range);
        return res.State ? Success(data: res.Data!) : Error(res.Msg);
    }

    [HttpGet("destroy/{id}")]
    [Permission(ProductAbout.TemplateSketch)]
    public virtual async Task<ResultStruct> Destroy(int id)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var res = await _service.Destroy(id, range);
        return res ? Success(message: "删除成功") : Error("删除失败");
    }
}