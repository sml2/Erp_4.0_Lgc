﻿using ERP.Authorization.Rule.Component;
using ERP.Enums.ID;
using ERP.Enums.Rule.Config;
using ERP.Enums.Rule.Config.Internal;
using Microsoft.AspNetCore.Mvc;
using ERP.Models.Product;
using ERP.Extensions;
using ERP.Interface.Rule;
using ERP.Models.View.Product.Template;
using ERP.Models.View.Products.Template;
using ERP.Services.Identity;
using ERP.Services.Product;
using ERP.ViewModels;
using ERP.ViewModels.Template;

namespace ERP.Controllers.Product;

[Route("api/product/[controller]")]
[ApiController]
public class ExpandController : BaseController
{
    private readonly ExpandService _service;
    private readonly UserManager _userManager;
    private readonly ProductAbout _currentRange = ProductAbout.TemplateRange;

    public ExpandController(ExpandService service, UserManager userManager)
    {
        _service = service;
        _userManager = userManager;
    }

    [Route("init")]
    [HttpGet]
    public async Task<ResultStruct> Init()
    {
        /************************************************  
         
        ************************************************/
        return await System.Threading.Tasks.Task.FromResult(Success());
    }

    //protected function rules(): \App\Rule\BitwiseFlags\Product
    //{
    //    return $this->getSessionObj()->getRule()->getProduct()->getBiter();
    //}

    /// <summary>
    /// 获取属性模板
    /// </summary>
    /// <returns></returns>
    [Route("list")]
    [HttpPost]
    public async Task<ResultStruct> List(ListDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var resutl = await _service.List(req,range);
        return Success(resutl);
    }

    [Route("edit")]
    [HttpPost]
    public async Task<ResultStruct> Edit(ExpandEditDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var resutl = await _service.SaveOrUpdate(req,range);
        return resutl ? Success() : Error();
    }

    [Route("show")]
    [HttpPost]
    public async Task<ResultStruct> Show(IdDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var resutl = await _service.Show(req.ID,range);
        return Success(resutl);
    }

    [Route("destroy")]
    [HttpPost]
    public async Task<ResultStruct> Destroy(IdDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var resutl = await _service.Destroy(req.ID,range);
        return resutl ? Success() : Error();
    }

    [Route("set")]
    [HttpPost]
    public async Task<ResultStruct> Set()
    {
        if (!Request.TryGetValue("mode", out var mode))
        {
            return Error(message: "缺少填充方式");
        }

        if (!Request.TryGetValue("id", out var id))
        {
            return Error(message: "缺少产品ID");
        }

        if (!Request.TryGetValue("content", out var content))
        {
            return Error(message: "缺少属性内容");
        }

        //todo ExtTemplateController Set
        var result = await _service.Set(Convert.ToInt32(id), content, mode);
        return result != null ? Success(message: "填充成功") : Error(message: "填充失败");
    }

    [HttpPost("get")]
    public async Task<ResultStruct> Get()
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _service.Get(range);
        return Success(result);
    }
}