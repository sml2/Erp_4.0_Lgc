﻿using ERP.Authorization.Rule;
using ERP.Enums.ID;
using ERP.Enums.Rule.Config;
using ERP.Enums.Rule.Config.Internal;
using ERP.Services.Product;
using ERP.Models.View.Product.Category;
using ERP.Services.Identity;
using ERP.ViewModels;

namespace ERP.Controllers.Product;

[Route("api/[controller]")]
[ApiController]
public class CategoryController : BaseController
{
    ////原ERP3.0注释

    #region ERP3.0 PHP

    //    protected CategoryService $categoryService;

    //    public function __construct()
    //    {
    //        parent::__construct();
    //        $this->categoryService = new CategoryService();
    //    }

    //    public function list()
    //    {
    //        if (!$this->rules()->hasCategory() && $this->getSessionObj()->getConcierge() == User::TYPE_CHILD) {
    //            return error('无权访问');
    //        }
    //        $res = $this->categoryService->getCategoryList();
    //        return $res['state'] ? success('获取成功', $res['data']) : error($res['msg']);
    //    }

    //    public function categoryInfo()
    //    {
    //        $id = request()->input('id');
    //        if (!$id) {
    //            return error('缺少ID');
    //        }
    //        $res = $this->categoryService->categoryInfo($id);
    //        return $res['state'] ? success('获取成功', $res['data']) : error($res['msg']);
    //    }

    //    public function categoryDel($id = '')
    //    {
    //        if (!$id) {
    //            return error('缺少ID');
    //        }
    //        $res = $this->categoryService->delCategory($id);
    //        return $res['state'] ? success('删除成功') : error($res['msg']);
    //    }


    //    public function edit()
    //    {
    //        $data = request()->input();

    //        $validator = Validator::make(
    //            $data,
    //            [
    //                'name' => ['required', 'max:255'],
    //            ],
    //            [
    //                'name.required' => '请填写分类名称',
    //                'name.max'      => '最大长度不可超过255',
    //            ]
    //        );
    //        if ($validator->fails()) {
    //            return error($validator->errors()->first());
    //        }

    //        $res = $this->categoryService->addOrUpdateCategory($data);

    //        return $res['state'] ? success(isset($data['id']) && $data['id'] ? '修改成功' : '添加成功') : error($res['msg']);
    //    }

    //    /**
    //     * 获取一级分类
    //     * User: ZY
    //     * Date: 2020/6/20 15:27
    //     * @return \Illuminate\Http\JsonResponse
    //     */
    //    public function firstCategory()
    //    {
    //        $res = $this->categoryService->firstCategory();
    //        return $res['state'] ? success('获取成功', $res['data']) : error($res['msg']);
    //    }

    //    /**
    //     * 获取一级分类
    //     * User: ZY
    //     * Date: 2020/6/20 15:27
    //     * @return \Illuminate\Http\JsonResponse
    //     */
    //    public function parentCategory()
    //    {
    //        $res = $this->categoryService->parentCategory();
    //        return $res['state'] ? success('获取成功', $res['data']) : error($res['msg']);
    //    }

    //    /**
    //     * 权限
    //     * User: ZY
    //     * @return \App\Rule\BitwiseFlags\Product
    //     */
    //    private function rules(): \App\Rule\BitwiseFlags\Product
    //    {
    //        return $this->sessionData->getRule()->getProduct()->getBiter();
    //}

    //public function init()
    //{
    //    if (!$this->rules()->hasCategory() && $this->getSessionObj()->getConcierge() == User::TYPE_CHILD) {
    //        return error('无权访问');
    //    }
    //    if ($this->getSessionObj()->getConcierge() == User::TYPE_OWN) {
    //            $data = [
    //                'rule' => [
    //                    'add'  => true,
    //                    'edit' => true,
    //                    'del'  => true,
    //                ]
    //            ];
    //    } else
    //    {
    //            $data = [
    //                'rule' => [
    //                    'add'  => $this->rules()->hasAddCategory(),
    //                    'edit' => $this->rules()->hasEditCategory(),
    //                    'del'  => $this->rules()->hasDeleteCategory(),
    //                ]
    //            ];
    //    }

    //    return success('', $data);
    //}

    #endregion

    #region sql

    private readonly CategoryService _service;
    private readonly UserManager _userManager;
    private readonly ProductAbout _currentRange = ProductAbout.CategoryRange;


    public CategoryController(CategoryService service, UserManager userManager)
    {
        _service = service;
        _userManager = userManager;
    }

    /// <summary>
    /// 权限
    /// </summary>
    /// <returns></returns>
    [HttpPost("init")]
    public async Task<ResultStruct> Init()
    {
        var rule = new
        {
            add = true,
            del = true,
            edit = true
        };
        return await System.Threading.Tasks.Task.FromResult(Success(new { rule }));
    }

    /// <summary>
    /// 权限
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> rules()
    {
        return await System.Threading.Tasks.Task.FromResult(Success());
    }

    [HttpPost("list")]
    public async Task<ResultStruct> list()
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var res = await _service.List(range);
        return res.State ? Success(res.Data!, "获取成功") : Error(message: res.Msg);
    }

    #endregion

    #region PHP->C#

    [Route("edit")]
    [HttpPost]
    public async Task<ResultStruct> Edit(EditVM req)
    {
        var res = await _service.AddOrUpdateCategory(req);
        return res.State ? Success(message: res.Msg) : Error(message: res.Msg);
    }

    /// <summary>
    /// 获取一级分类
    /// </summary>
    /// <returns></returns>
    [Route("first")]
    [HttpPost]
    public async Task<ResultStruct> FirstCategory()
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _service.FirstCategory(range,false);
        return Success(message: "获取成功", data: result);
    }


    /// <summary>
    /// 产品获取一级分类
    /// </summary>
    /// <returns></returns>
    [HttpPost("getFirstCategory")]
    public async Task<ResultStruct> GetFirstCategory()
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _service.FirstCategory(range,true);
        return Success(message: "获取成功", data: result);
    }

    /// <summary>
    /// 获取二级分类
    /// </summary>
    /// <returns></returns>
    [HttpPost("getSubCategory")]
    public async Task<ResultStruct> GetSubCategory(IdDto req)
    {
        var result = await _service.SubCategory(req.ID);
        return Success(message: "获取成功", data: result);
    }

    #endregion

    [HttpPost("show")]
    [Permission(ProductAbout.CategoryEdit)]
    public virtual async Task<ResultStruct> Show(IdDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var res = await _service.Info(req.ID, range);
        return res.State ? Success(data: res.Data!) : Error(res.Msg);
    }

    [HttpGet("destroy/{id}")]
    [Permission(ProductAbout.CategoryDelete)]
    public virtual async Task<ResultStruct> Destroy(int id)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var res = await _service.Destroy(id, range);
        return res ? Success(message: "删除成功") : Error("删除失败");
    }
}