﻿using Microsoft.AspNetCore.Mvc;
using ERP.Models.Product;
using ERP.Extensions;
using ERP.Services.Product;
using ERP.ViewModels;

namespace ERP.Controllers.Product;

[Route("api/product/[controller]")]
[ApiController]
public class ToSellController : BaseController
{
    private readonly ToSellService _service;

    public ToSellController(ToSellService service)
    {
        _service = service;
    }

    /// <summary>
    /// 跟卖配置项权限
    /// </summary>
    /// <returns></returns>
    [Route("ruleInfo")]
    [HttpGet]
    public async Task<ResultStruct> ruleInfo()
    {
        /************************************************  
         
        ************************************************/
        //return $this->sessionData->getRule()->getProduct()->getBiter2();
        return await System.Threading.Tasks.Task.FromResult(Success(true));
    }

    /// <summary>
    /// 获取列表初始数据
    /// </summary>
    /// <returns></returns>
    [Route("init")]
    [HttpPost]
    public async Task<ResultStruct> Init()
    {
        var ruleInit = await _service.Init();
        return Success(ruleInit);
    }

    /// <summary>
    /// 获取列表数据
    /// </summary>
    /// <returns></returns>
    //[Route("list")]
    //[HttpPost]
    //public async Task<ResultStruct> List()
    //{
    //    var list = await _service.List();
    //    return Success(list);
    //}

    /// <summary>
    /// 删除跟卖产品
    /// </summary>
    /// <returns></returns>
    [Route("delSellProduct")]
    [HttpPost]
    public async Task<ResultStruct> delSellProduct()
    {
        /************************************************  
         
        ************************************************/
        //$id = $request->input('id');
        //$store_id = $request->input('store_id');
        //if (!$id || !$store_id) {
        //    return error('参数错误');
        //}

        ////检测是否存在同步中产品
        //$res = $this->service->checkTaskProductSync($id);
        //if ($res) {
        //    return error('当前产品存在同步中的数据，禁止删除');
        //}


        //DB::beginTransaction();
        //try
        //{
        //    //删除日志
        //    $this->service->delProductLog($id);
        //    //删除同步产品
        //    $this->service->delSyncProductByPid($id);
        //    //删除跟卖产品
        //    $this->service->delSellProductById($id, $store_id);

        //    DB::commit();
        //}
        //catch (Exception $e) {
        //    DB::rollBack();
        //    return error($e->getMessage());
        //}
        //return await Task.FromResult(ReturnArr(true));
        return await System.Threading.Tasks.Task.FromResult(Success(true));
    }
    /************************ 添加待同步列表 **************************/

    /// <summary>
    /// 获取添加待同步列表需求数据
    /// </summary>
    /// <returns></returns>
    [Route("getUploadInit")]
    [HttpPost]
    public async Task<ResultStruct> getUploadInit()
    {
        /************************************************  
         
        ************************************************/
        //$data = [
        //    'currency'      => $this->getUnitCache(),
        //    'product_state' => ToSellTaskProduct::PRODUCT_STATE
        //];
        //return success('', $data);
        return await System.Threading.Tasks.Task.FromResult(Success(true));
    }

    /// <summary>
    /// 添加待同步信息
    /// </summary>
    /// <returns></returns>
    [Route("subUploadData")]
    [HttpPost]
    public async Task<ResultStruct> subUploadData()
    {
        /************************************************  
         
        ************************************************/
        //$info = $request->input('info');
        //$productInfo = $request->input('productInfo');
        //$storeData = $request->input('storeData');
        //if (!$info || !$productInfo || !$storeData) {
        //    return error('参数错误');
        //}

        //$jsonData = [
        //    'sku'           => '',
        //    'price'         => '',
        //    'unit'          => '',
        //    'stock'         => '',
        //    'stock_up'      => '',
        //    'product_state' => '',
        //];
        //foreach ($storeData as $value) {
        //    $sku = skucode();
        //    //asin+sku+marketplace+seller_id
        //    $hash_code = createHashCode($productInfo['asin']. $sku. $productInfo['marketplace']. $value['seller_id']);
        //    $insertData[] = [
        //        'marketplace'      => $productInfo['marketplace'],
        //        'sku'              => $sku,
        //        'asin'             => $productInfo['asin'],
        //        'price'            => $info['price'],
        //        'base_price'       => FromConversion($info['price'], $info['unit'], $info['rate']),
        //        'unit'             => $info['unit'],
        //        'rate'             => $info['rate'],
        //        'stock'            => $info['stock'],
        //        'stock_up'         => $info['stock_up'],
        //        'product_state'    => $info['product_state'],
        //        'pid'              => $productInfo['id'],
        //        'hash_code'        => $hash_code,
        //        'product_json'     => json_encode($productInfo),
        //        'product_result'   => ToSellTaskProduct::RESULT_ONE,
        //        'price_result'     => ToSellTaskProduct::RESULT_ONE,
        //        'stock_result'     => ToSellTaskProduct::RESULT_ONE,
        //        'last_finish_data' => json_encode($jsonData),
        //        'upload_state'     => ToSellTaskProduct::UPLOAD_ONE,
        //        'type'             => ToSellTaskProduct::TYPE_UPLOAD,
        //        'store_id'         => $value['id'],
        //        'seller_id'        => $value['seller_id'],
        //        'truename'         => $this->sessionData->getTruename(),
        //        'user_id'          => $this->sessionData->getUserId(),
        //        'group_id'         => $this->sessionData->getGroupId(),
        //        'company_id'       => $this->sessionData->getCompanyId(),
        //        'oem_id'           => $this->sessionData->getOemId(),
        //        'created_at'       => $this->datetime
        //    ];
        //}

        //$res = $this->service->insertTaskProduct($insertData);
        //return $res? await Task.FromResult(ReturnArr(true)) : error();
        return await System.Threading.Tasks.Task.FromResult(Success(true));
    }

    /************************ 同步列表 **************************/

    /// <summary>
    /// 获取跟卖上传数据
    /// </summary>
    /// <returns></returns>
    [Route("getSyncList")]
    [HttpPost]
    public async Task<ResultStruct> getSyncList()
    {
        /************************************************  
         
        ************************************************/
        //$asin = $request->input('asin');
        //$store_id = $request->input('store_id');
        //$pid = $request->input('pid');
        ////1全部 2待同步 3同步中 4完成 5失败
        //$type = $request->input('type');
        //if (!$type) {
        //    return error('参数错误');
        //}
        //$store_ids = [];
        //if (!$store_id && $this->sessionData->getConcierge() == UserModel::TYPE_CHILD) {
        //    $store = $this->getStorePersonalCache();
        //    //员工无可允许查看店铺，返回空信息
        //    if (!checkArr($store))
        //    {
        //        return success('', ["list" => []]);
        //    }
        //    $store_ids = array_column($store, 'id');
        //}

        //$list = $this->service->getListPage($type, $asin, $pid, $store_id, $store_ids);
        //return success('', $list);
        return await System.Threading.Tasks.Task.FromResult(Success(true));
    }

    /// <summary>
    /// 添加同步更新
    /// </summary>
    /// <returns></returns>
    [Route("subUpload")]
    [HttpPost]
    public async Task<ResultStruct> subUpload()
    {
        /************************************************  
         
        ************************************************/
        //$storeId = $request->input('storeId');
        //$ids = $request->input('ids');
        //if (!$storeId || !$ids) {
        //    return error('参数错误');
        //}

        ////查询需同步产品信息
        //$list = $this->service->getWaitList($ids, $storeId)->toArray();

        //DB::beginTransaction();
        //try
        //{
        //    //生成一条任务
        //    $task_id = $this->service->addTask($storeId);
        //    //生成关联关系
        //    foreach ($ids as $value) {
        //        $insertRelation[] = [
        //            'task_id'         => $task_id,
        //            'task_product_id' => $value,
        //            'user_id'         => $this->sessionData->getUserId(),
        //            'group_id'        => $this->sessionData->getGroupId(),
        //            'company_id'      => $this->sessionData->getCompanyId(),
        //            'oem_id'          => $this->sessionData->getOemId(),
        //            'created_at'      => $this->datetime
        //        ];
        //    }
        //    $this->service->addTaskRelation($insertRelation);
        //    //更新同步产品信息
        //    foreach ($list as $value) {
        //        $jsonData = [
        //            'sku'           => $value['sku'],
        //            'price'         => $value['price'],
        //            'unit'          => $value['unit'],
        //            'stock'         => $value['stock'],
        //            'stock_up'      => $value['stock_up'],
        //            'product_state' => $value['product_state'],
        //        ];
        //        $update = [
        //            'last_finish_data' => json_encode($jsonData),
        //            'product_result'   => ToSellTaskProduct::RESULT_TWO,
        //            'price_result'     => ToSellTaskProduct::RESULT_TWO,
        //            'stock_result'     => ToSellTaskProduct::RESULT_TWO,
        //            'upload_state'     => ToSellTaskProduct::UPLOAD_TWO,
        //            'type'             => ToSellTaskProduct::TYPE_UPLOAD,
        //        ];
        //        $this->service->updateTaskProduct($value['id'], $update);

        //        $insertDataLog[] = [
        //            'remark'          => '跟卖产品上架',
        //            'pid'             => $value['pid'],
        //            'task_product_id' => $value['id'],
        //            'truename'        => $this->sessionData->getTruename(),
        //            'user_id'         => $this->sessionData->getUserId(),
        //            'group_id'        => $this->sessionData->getGroupId(),
        //            'company_id'      => $this->sessionData->getCompanyId(),
        //            'oem_id'          => $this->sessionData->getOemId(),
        //            'created_at'      => $this->datetime,
        //            'updated_at'      => $this->datetime
        //        ];
        //    }

        //    //上架日志
        //    $this->service->addTaskProductLog($insertDataLog);

        //    DB::commit();
        //}
        //catch (Exception $e) {
        //    DB::rollBack();
        //    return error($e->getMessage());
        //}
        //return await Task.FromResult(Success(true));
        return await System.Threading.Tasks.Task.FromResult(Success(true));
    }

    /// <summary>
    /// 删除同步产品session_state
    /// </summary>
    /// <returns></returns>
    [Route("delSyncProduct")]
    [HttpPost]
    public async Task<ResultStruct> delSyncProduct()
    {
        /************************************************  
         
        ************************************************/

        /*
         $id = $request->input('id');

        if (!$id) {
            return error('参数错误');
        }

        //删除同步产品，日志忽略
        $res = $this->service->delSyncProductById($id);

        return $res ? await Task.FromResult(Success(true)) : error();
        */
        return await System.Threading.Tasks.Task.FromResult(Success(true));
    }

    /// <summary>
    /// 修改同步产品
    /// </summary>
    /// <returns></returns>
    [Route("editUploadData")]
    [HttpPost]
    public async Task<ResultStruct> editUploadData()
    {
        /************************************************  
         
        ************************************************/

        /*
         $data = $request->input('info');
        $change = $request->input('change');
        if (!$data || !$change) {
            return error('参数错误');
        }

        $info = $this->service->getTaskProductById($data['id']);
        if (!$info) {
            return error('产品不存在');
        }
        if ($info['upload_state'] == ToSellTaskProduct::UPLOAD_TWO) {
            return error('同步中的产品无法修改');
        }
        $log = '';
        //查询sku重复性
        if ($info['upload_state'] == ToSellTaskProduct::UPLOAD_ONE && $data['sku'] != $info['sku']) {
            $hash_code = createHashCode($info['asin'] . $data['sku'] . $info['marketplace'] . $info['seller_id']);
            $res = $this->service->checkTaskProductRepeat($hash_code);
            if ($res) {
                return error('当前店铺已存在此SKU');
            }
            $updateData['sku'] = $data['sku'];
            $updateData['hash_code'] = $hash_code;
            $log .= '修改原SKU【' . $info['sku'] . '】,';
        }


        if ($change['upload']) {
            $updateData['product_state'] = $data['product_state'];
            $log .= '修改原产品状态【' . $info['product_state'] . '】,';
        }

        if ($change['price']) {
            $updateData['price'] = $data['price'];
            $updateData['unit'] = $data['unit'];
            $updateData['rate'] = $data['rate'];
            $updateData['base_price'] = FromConversion($data['price'], $data['unit'], $data['rate']);
            $log .= '修改原产品价格【' . floatval($info['price']) . $info['unit'] . '】,';
        }

        if ($change['stock']) {
            $updateData['stock'] = $data['stock'];
            $updateData['stock_up'] = $data['stock_up'];
            $log .= '修改原产品库存【' . $info['stock'] . '】，备货时间【' . $info['stock_up'] . '天】';
        }
        $log = trim($log, ",");

        //更改数据变更信息
        if ($info['upload_state'] != ToSellTaskProduct::UPLOAD_ONE) {
            $last_finish_data = $info['last_finish_data'];
            if ($data['unit'] != $last_finish_data['unit'] || floatval($data['price']) != floatval($last_finish_data['price']) || $data['stock'] != $last_finish_data['stock'] || $data['stock_up'] != $last_finish_data['stock_up'] || $data['product_state'] != $last_finish_data['product_state']) {
                $updateData['data_state'] = ToSellTaskProduct::DATA_TRUE;
            }
        }

        DB::beginTransaction();
        try {
            //更新产品
            $this->service->updateTaskProduct($data['id'], $updateData);
            //增加日志
            $this->service->addProductLog($data['id'], $info['pid'], $log);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return error($e->getMessage());
        }
        return await Task.FromResult(ReturnArr(true));
        */
        return await System.Threading.Tasks.Task.FromResult(Success(true));
    }

    /// <summary>
    /// 获取同步产品修改日志
    /// </summary>
    /// <returns></returns>
    [Route("getProductLog")]
    [HttpPost]
    public async Task<ResultStruct> getProductLog()
    {
        /************************************************  
         
        ************************************************/

        /*
         $id = $request->input('id');
        if (!$id) {
            return error('参数错误');
        }
        $list = $this->service->getLogPage($id);
        return success('', $list);
        */
        return await System.Threading.Tasks.Task.FromResult(Success(true));
    }

    /// <summary>
    /// 上、下架、同步产品
    /// </summary>
    /// <returns></returns>
    [Route("syncProduct")]
    [HttpPost]
    public async Task<ResultStruct> syncProduct()
    {
        /************************************************  
         
        ************************************************/

        /*
         $id = $request->input('id');
        $type = $request->input('type');
        if (!$id || !$type) {
            return error('参数错误');
        }

        //同步更新产品详情
        $info = $this->service->getTaskProductById($id);
        if (!$info) {
            return error('产品不存在');
        }
        if ($info['upload_state'] == ToSellTaskProduct::UPLOAD_TWO) {
            return error('同步中的产品无法操作');
        }


        //生成关联关系
        $insertRelation = [
            'task_product_id' => $id,
            'user_id'         => $this->sessionData->getUserId(),
            'group_id'        => $this->sessionData->getGroupId(),
            'company_id'      => $this->sessionData->getCompanyId(),
            'oem_id'          => $this->sessionData->getOemId(),
            'created_at'      => $this->datetime
        ];

        //更新同步产品信息
        $jsonData = [
            'sku'           => $info['sku'],
            'price'         => $info['price'],
            'unit'          => $info['unit'],
            'stock'         => $info['stock'],
            'stock_up'      => $info['stock_up'],
            'product_state' => $info['product_state'],
        ];
        $updateTaskProduct = [
            'last_finish_data' => json_encode($jsonData),
            'upload_state'     => ToSellTaskProduct::UPLOAD_TWO,
            'data_state'       => ToSellTaskProduct::DATA_FALSE,
            'type'             => $info['upload_state'] == ToSellTaskProduct::UPLOAD_ONE ? ToSellTaskProduct::TYPE_UPLOAD : $type,
        ];

        if (($type == ToSellTaskProduct::TYPE_UPLOAD || $type == ToSellTaskProduct::TYPE_DELETE) && $info['upload_state'] == ToSellTaskProduct::UPLOAD_THREE) {
            $tip = $type == ToSellTaskProduct::TYPE_UPLOAD ? '上架' : '下架';
            $updateTaskProduct['stock_result'] = ToSellTaskProduct::RESULT_TWO;
            $updateTaskProduct['stock_reason'] = '待' . $tip;
        } else {
            $last_finish_data = $info['last_finish_data'];

            if (($info['unit'] != $last_finish_data['unit'] || floatval($info['price']) != floatval($last_finish_data['price'])) && $info['price_result'] != ToSellTaskProduct::RESULT_TWO) {
                $updateTaskProduct['price_result'] = ToSellTaskProduct::RESULT_TWO;
                $updateTaskProduct['price_reason'] = '待同步';
            }

            if ($info['stock'] != $last_finish_data['stock'] && $info['stock_result'] != ToSellTaskProduct::RESULT_TWO) {
                $updateTaskProduct['stock_result'] = ToSellTaskProduct::RESULT_TWO;
                $updateTaskProduct['stock_reason'] = '待同步';
            }

            if ($info['product_state'] != $last_finish_data['product_state'] && $info['product_result'] != ToSellTaskProduct::RESULT_TWO) {
                $updateTaskProduct['product_result'] = ToSellTaskProduct::RESULT_TWO;
                $updateTaskProduct['product_reason'] = '待同步';
            }

            $tip = '同步修改信息';
        }
        //操作日志
        $insertDataLog = [
            'remark'          => '跟卖产品' . $tip,
            'pid'             => $info['pid'],
            'task_product_id' => $info['id'],
            'truename'        => $this->sessionData->getTruename(),
            'user_id'         => $this->sessionData->getUserId(),
            'group_id'        => $this->sessionData->getGroupId(),
            'company_id'      => $this->sessionData->getCompanyId(),
            'oem_id'          => $this->sessionData->getOemId(),
            'created_at'      => $this->datetime,
            'updated_at'      => $this->datetime
        ];

        DB::beginTransaction();
        try {

            //生成一条任务
            $task_id = $this->service->addTask($info['store_id']);
            //生成关联关系
            $insertRelation['task_id'] = $task_id;
            $this->service->addTaskRelation($insertRelation);
            //更新同步产品信息
            $this->service->updateTaskProduct($id, $updateTaskProduct);
            //操作日志
            $this->service->addTaskProductLog($insertDataLog);


            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return error($e->getMessage());
        }
        return await Task.FromResult(ReturnArr(true));
        */
        return await System.Threading.Tasks.Task.FromResult(Success(true));
    }
    
    //[Route("show")]
    //[HttpPost]
    //public virtual async Task<ResultStruct> Show(IdDto req)
    //{
    //    var res = await _service.Info(req.ID);

    //    return res.State ? Success(data: res.Data!) : Error(res.Msg);
    //}

    [Route("destroy/{id}")]
    [HttpGet]
    public virtual async Task<ResultStruct> Destroy(int id)
    {
        var res = await _service.Destroy(id);
        return res ? Success(message: "删除成功") : Error("删除失败");
    }
}