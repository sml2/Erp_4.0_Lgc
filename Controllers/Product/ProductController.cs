using ERP.Authorization.Rule;
using ERP.Enums.Rule.Config.Internal;
using ERP.Models.View.Product.Translation;
using ERP.Models.View.Products.Product;
using ERP.Services.Identity;
using ERP.Services.Product;
using ERP.ViewModels;

namespace ERP.Controllers.Product;

[Route("api/[controller]")]
public class ProductController : BaseController
{
    private readonly ProductService _service;
    private readonly UserManager _userManager;
    private readonly Enums.Rule.Config.Product _currentRange = Enums.Rule.Config.Product.ProductRange;


    public ProductController(ProductService service, UserManager userManager)
    {
        _service = service;
        _userManager = userManager;
    }

    /// <summary>
    /// 初始化
    /// </summary>
    /// <returns></returns>
    [HttpPost("init")]
    public async Task<ResultStruct> Init()
    {
        var result = await _service.Init();
        return Success(result);
    }

    /// <summary>
    /// 列表
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost("list")]
    public async Task<ResultStruct> List(ListDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _service.List(req, range);
        return Success(result);
    }

    /// <summary>
    /// 新增或修改
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost("edit")]
    public async Task<ResultStruct> Edit(EditProductItemDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _service.SaveOrUpdate(req, range);
        return result ? Success() : Error();
    }

    [HttpPost("editWithShow")]
    public async Task<ResultStruct> EditWithShow(EditWithShowDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _service.EditWithShow(req, range);
        return result is not null ? Success(result) : Error();
    }

    /// <summary>
    /// 获取详情
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [Route("show")]
    [HttpPost]
    public async Task<ResultStruct> Show(ShowDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _service.Show(req, range);
        return Success(result);
    }
    
    /// <summary>
    /// b2c 获取详情
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [Route("b2cShow")]
    [HttpPost]
    public async Task<ResultStruct> B2CShow(ShowDto req)
    {
        var result = await _service.Show(req, DataRange_2b.Company,true);
        return Success(result);
    }

    /// <summary>
    /// 移入回收站
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost("moveRecycle")]
    public async Task<ResultStruct> MoveRecycle(IdDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _service.Recovery(req.ID, range, true);
        return result ? Success() : Error();
    }

    /// <summary>
    /// 回收站还原
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost("recovery")]
    public async Task<ResultStruct> Recovery(IdDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _service.Recovery(req.ID, range, false);
        return result ? Success() : Error();
    }

    /// <summary>
    /// 删除产品
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost("destroy")]
    [Permission(Enums.Rule.Config.Product.ProductTrueDelete)]
    public async Task<ResultStruct> Destroy(DestroyDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _service.Destroy(req, range);
        return result ? Success() : Error();
    }

    /// <summary>
    /// 分享
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost("share")]
    [Permission(Enums.Rule.Config.Product.ProductShare)]
    public async Task<ResultStruct> Share(IdDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _service.Share(req.ID, range);
        return result ? Success() : Error();
    }

    /// <summary>
    /// 取消分享
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost("cancelShare")]
    [Permission(Enums.Rule.Config.Product.ProductShare)]
    public async Task<ResultStruct> CancelShare(IdDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _service.CancelShare(req.ID, range);
        return result ? Success() : Error();
    }

    /// <summary>
    /// 绑定分类
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost("bindCategory")]
    [Permission(Enums.Rule.Config.Product.ProductBindCategory)]
    public async Task<ResultStruct> BindCategory(BindCategoryDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _service.BindCategory(req, range);
        return result ? Success() : Error();
    }

    /// <summary>
    /// 设置卖点
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost("setProductSketch")]
    [Permission(Enums.Rule.Config.Product.ProductFillBasic)]
    public async Task<ResultStruct> SetProductSketch(SetProductSketchDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _service.SetProductSketch(req, range);
        return result.State ? Success(result.Message) : Error(result.Message);
    }

    /// <summary>
    /// 设置关键词
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost("setProductKeyword")]
    [Permission(Enums.Rule.Config.Product.ProductFillBasic)]
    public async Task<ResultStruct> SetProductKeyword(SetProductKeywordDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _service.SetProductKeyword(req, range);
        return result.State ? Success(result.Message) : Error(result.Message);
    }

    /// <summary>
    /// 设置属性
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost("setProductExt")]
    [Permission(Enums.Rule.Config.Product.ProductFillBasic)]
    public async Task<ResultStruct> SetProductExt(SetProductExtDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _service.SetProductExpand(req, range);
        return result.State ? Success(result.Message) : Error(result.Message);
    }

    /// <summary>
    /// 分享导入
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost("importShare")]
    public async Task<ResultStruct> ShareImport(IdDto req)
    {
        var result = await _service.Clone(req, true);
        return result.State ? Success(message: result.Message) : Error(message: result.Message);
    }

    /// <summary>
    /// 标题重复检测
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost("checkTitle")]
    [Permission(Enums.Rule.Config.Product.ProductCheckTitle)]
    public async Task<ResultStruct> CheckTitle(IdDto req)
    {
        var result = await _service.CheckTitle(req.ID);
        return result ? Success(message: "检测完成") : Error();
    }

    /// <summary>
    /// 设置默认语种
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost("SetDefaultLanguage")]
    public async Task<ResultStruct> SetDefaultLanguage(SetDefaultLanguageDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _service.SetDefaultLanguage(req, range);
        return Success(data: result);
    }

    /// <summary>
    /// 随机填充
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost("setProductRandom")]
    [Permission(Enums.Rule.Config.Product.ProductRandomFill)]
    public async Task<ResultStruct> SetProductRandom(SetProductRandomDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _service.SetProductRandom(req, range);
        return result.State ? Success(result.Message) : Error(result.Message);
    }

    /// <summary>
    /// 批量改价
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost("setProductPrice")]
    [Permission(Enums.Rule.Config.Product.ProductChangePrices)]
    public async Task<ResultStruct> SetProductPrice(SetProductPriceDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _service.SetProductPrice(req);
        return result.State ? Success(result.Message) : Error(result.Message);
    }

    /// <summary>
    /// 复制克隆产品
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost("clone")]
    public async Task<ResultStruct> Clone(IdDto req)
    {
        var result = await _service.Clone(req, false);
        return result.State ? Success("克隆成功") : Error(result.Message);
    }

    /// <summary>
    /// 历史版本
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost("historyView")]
    public async Task<ResultStruct> HistoryView(HistoryViewDto req)
    {
        var result = await _service.HistoryView(req);
        return Success(result);
    }

    /// <summary>
    /// 版本回滚
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost("rollbackVersion")]
    public async Task<ResultStruct> RollbackVersion(HistoryViewDto req)
    {
        var result = await _service.RollbackVersion(req);
        return result ? Success("版本回滚成功") : Error();
    }

    [HttpPost("syncNetworkImage")]
    public async Task<ResultStruct> SyncNetworkImage(SyncNetworkImageDto req)
    {
        var result = await _service.SyncNetworkImage(req);
        return result ? Success("版本回滚成功") : Error();
    }

    [HttpPost("fastSetAttributes")]
    public async Task<ResultStruct> FastSetAttributes(FastSetAttributesDto req)
    {
        var result = await _service.FastSetAttributes(req);
        return result ? Success("添加成功") : Error();
    }

    [HttpPost("getWaitDelete")]
    public async Task<ResultStruct> GetWaitDelete()
    {
        var result = await _service.GetWaitDelete();
        return Success(result);
    }

    [HttpPost("replaceProductImage")]
    public async Task<ResultStruct> ReplaceProductImage(ReplaceProductImageDto req)
    {
        var result = await _service.ReplaceProductImage(req);
        return result.State ? Success(result.ImgObj) : Error("图片编辑失败");
    }

    [HttpPost("matting")]
    public Task<ResultStruct> Matting()
    {
        var result = new Dictionary<string, object>();
        result.Add("options", new List<Dictionary<string, string>>()
        {
            new() { { "label", "黑色" }, { "value", "000000" } },
            new() { { "label", "白色" }, { "value", "FFFFFF" } },
            new() { { "label", "红色" }, { "value", "FF0000" } },
            new() { { "label", "蓝色" }, { "value", "0000FF" } },
            new() { { "label", "绿色" }, { "value", "008000" } },
        });
        result.Add("tips", "");
        return Task.FromResult(Success(result));
    }

    [HttpPost("goMatting")]
    public async Task<ResultStruct> GoMatting(GoMattingDto req)
    {
        var result = await _service.GoMatting(req);
        return result.State ? Success(data: result.Result) : Error(result.Result);
    }

    [HttpPost("getInfoWithUpload")]
    public async Task<ResultStruct> GetInfoWithUpload([FromBody] GetInfoWithUploadDto req)
    {
        var result = await _service.GetInfoWithUpload(req);
        return result.Products != null ? Success(result.Products) : Error(result.Msg);
    }

    [HttpPost("translateInit")]
    public async Task<ResultStruct> TranslateInit()
    {
        var result = await _service.TranslateInit();
        return Success(result);
    }

    [HttpPost("translate")]
    [Permission(Enums.Rule.Config.Product.ProductTranslate)]
    public async Task<ResultStruct> Translate(SubDto req)
    {
        var result = await _service.NewTranslate(req);
        return result.State ? Success(result.Message) : Error(result.Message);
    }
}