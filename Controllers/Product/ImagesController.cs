using ERP.Models.View.Products.Product;
using ERP.Models.View.Products.Product.ImageTran;
using ERP.Services.Identity;
using ERP.Services.Product;
using Microsoft.AspNetCore.Identity;

namespace ERP.Controllers.Product;

[Route("api/product/[controller]")]
public class ImagesController : BaseController
{
    private readonly ImagesService _service;
    private readonly UserManager _userManager;
    private readonly Enums.Rule.Config.Product _currentRange = Enums.Rule.Config.Product.ProductRange;

    public ImagesController(UserManager userManager, ImagesService service)
    {
        _userManager = userManager;
        _service = service;
    }

    [HttpPost("list")]
    public async Task<ResultStruct> List(ListDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _service.GetList(req, range);
        return Success(result);
    }

    [HttpPost("tran")]
    public async Task<ResultStruct> Tran(ImageTranDto req)
    {
        var result = await _service.Tran(req);
        return result.State ? Success("图片翻译成功") : Error(result.Message);
    }

    [HttpPost("tranNotify")]
    public async Task<ResultStruct> TranNotify(TranNotifyDto req)
    {
        var result = await _service.TranNotify(req);
        return result.State ? Success("图片翻译成功") : Error(result.Message);
    }
    [HttpPost("singleTran")]
    public async Task<ResultStruct> SingleTran(GoTranDto req)
    {
        var result = await _service.Tran(req);
        return result.State
            ? Success(data: new
            {
                path = result.Message
            }, "图片翻译成功")
            : Error(result.Message);
    }
}