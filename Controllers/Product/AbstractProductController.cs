using ERP.Models.View.Product;
using ERP.Services;
using ERP.ViewModels;

namespace ERP.Controllers.Product;

public abstract class AbstractProductController : BaseController
{
    private BaseWithUserService _service;
    

    protected AbstractProductController(BaseWithUserService service)
    {
        _service = service;
    }


    // [Route("show")]
    // [HttpPost]
    // public virtual async Task<ResultStruct> Show(IdDto req)
    // {
    //     var res = await _service.Info(req.ID);
    //
    //     return res.State ? Success(data: res.Data!) : Error(res.Msg);
    // }
    //
    // [Route("destroy/{id}")]
    // [HttpGet]
    // public virtual async Task<ResultStruct> Destroy(int id)
    // {
    //     var res = await _service.Destroy(id);
    //     return res ? Success(message: "删除成功") : Error("删除失败");
    // }
}