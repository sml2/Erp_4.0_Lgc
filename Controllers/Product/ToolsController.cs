using Common.Enums.Products;
using ERP.Services.Product;

namespace ERP.Controllers.Product;

[Route("api/product/[controller]")]
[ApiController]
public class ToolsController : BaseController
{
    private readonly ToolsService _service;

    public ToolsController(ToolsService service)
    {
        _service = service;
    }

    [HttpGet("getKeyword")]
    public async Task<ResultStruct> GetKeyword([FromQuery] string asin, [FromQuery] MarketplaceIds marketplaceIds)
    {
        var res = await _service.GetKeyword(asin, marketplaceIds);

        return Success(data:res,message:"请求成功，正在处理结果");
    }

    public async Task<ResultStruct> GetUnavailableProducts([FromQuery] string keyword, [FromQuery] Countries country)
    {
        var res = await _service.GetUnavailableProducts(keyword, country,false);

        return Success(data:res,message:"请求成功，正在处理结果"); 
    }
}