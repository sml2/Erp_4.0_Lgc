﻿using Microsoft.AspNetCore.Mvc;
using ERP.Services.Product;
using ERP.Models.Product;
using ERP.Extensions;
using ERP.Models.View.Product.Audit;
using ERP.ViewModels;

namespace ERP.Controllers.Product;

using Model = Models.Product;

[Route("api/product/[controller]")]
public class AuditController : BaseController
{

    private readonly AuditService _service;

    public AuditController(AuditService service)
    {
        _service = service;
    }

    /// <summary>
    /// 初始化审核状态
    /// </summary>
    /// <returns></returns>
    [HttpPost("init")]
    public async Task<ResultStruct> Init(IdDto req)
    {
        var result = await _service.Init(req);
        return Success(result);
    }

    [HttpPost("setAudit")]
    public async Task<ResultStruct> SetAudit(SetDto req)
    {
        var result = await _service.SetAudit(req);
        return result ? Success(message: "审核成功") : Error("审核失败");
    }
}