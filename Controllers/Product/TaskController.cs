﻿using ERP.Enums.Rule.Config;
using Microsoft.AspNetCore.Mvc;
using ERP.Models.Product;
using ERP.Extensions;
using ERP.Models.View.Product.Task;
using ERP.Services.Identity;
using ERP.Services.Product;
using ERP.ViewModels;
using ERP.ViewModels.Task;

namespace ERP.Controllers.Product;

[Route("api/product/[controller]")]
[ApiController]
public class TaskController : BaseController
{
    private readonly TaskService _service;
    private readonly UserManager _userManager;
    private readonly ProductAbout _currentRange = Enums.Rule.Config.ProductAbout.TaskRange;

    public TaskController(TaskService service, UserManager userManager)
    {
        _service = service;
        _userManager = userManager;
    }


    //todo TaskController RuleInfo 权限
    // protected function ruleInfo(): \App\Rule\BitwiseFlags\Product
    // {
    //     return $this->getSessionObj()->getRule()->getProduct()->getBiter();
    // }

    /// <summary>
    /// 初始化
    /// </summary>
    /// <returns></returns>
    [Route("init")]
    [HttpPost]
    public async Task<ResultStruct> Init()
    {
        /************************************************  
        
       ************************************************/
        //todo TaskController Init 权限
        // if (!$this->rules()->hasTask() && $this->getSessionObj()->getConcierge() == User::TYPE_CHILD) {
        // return error('无权访问');
        // }
        // $list = $this->service->init();
        //     $rule = [
        // 'edit' => ($this->getSessionObj()->getConcierge() == User::TYPE_OWN) ? true : $this->ruleInfo(
        // )->hasEditTask(),
        // 'del'  => ($this->getSessionObj()->getConcierge() == User::TYPE_OWN) ? true : $this->ruleInfo(
        //     )->hasDeleteTask(),
        //     ];
        // $data = [
        // 'list' => $list,
        // 'rule' => $rule,
        //     ];

        var list = await _service.Init();


        return Success(new
        {
            list = list,
            rule = new { edit = true, del = true }
        });
    }

    /// <summary>
    /// 列表
    /// </summary>
    /// <returns></returns>
    [Route("list")]
    [HttpPost]
    public async Task<ResultStruct> List(ListDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _service.GetPaginateList(req, range);
        return Success(result, "获取成功");
    }

    /// <summary>
    /// 详情
    /// </summary>
    /// <returns></returns>
    [Route("info")]
    [HttpPost]
    public async Task<ResultStruct> Info(IdDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var res = await _service.Detail(req.ID, range);
        return res.State ? Success(res.Data!, "获取成功") : Error(message: res.Msg);
    }

    /// <summary>
    /// 二级分类
    /// </summary>
    /// <returns></returns>
    [Route("secondCategory")]
    [HttpPost]
    public async Task<ResultStruct> SecondCategory()
    {
        /************************************************  
         
        ************************************************/

        if (!Request.TryGetValue("pid", out var pid))
        {
            return Error(message: "缺少PID");
        }

        var res = await _service.SecondCategory(Convert.ToInt32(pid));
        return res.State ? Success(res.Data!, "获取成功") : Error(message: res.Msg);
    }

    /// <summary>
    /// 修改
    /// </summary>
    /// <param name="vm"></param>
    /// <returns></returns>
    [Route("edit")]
    [HttpPost]
    public async Task<ResultStruct> Edit(EditViewModel vm)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        try
        {
            await _service.Edit(vm, range);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return Error(message: e.Message);
        }
        return Success(message: "修改成功");
    }

    /// <summary>
    /// 删除
    /// </summary>
    /// <returns></returns>
    [Route("delete")]
    [HttpPost]
    public async Task<ResultStruct> Delete(DeleteDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var res = await _service.Delete(req, range);
        return res ? Success(message: "删除成功") : Error();
    }

    //todo TaskController Rules 权限
    // private function rules(): \App\Rule\BitwiseFlags\Product
    // {
    //     return $this->sessionData->getRule()->getProduct()->getBiter();
    // }
}