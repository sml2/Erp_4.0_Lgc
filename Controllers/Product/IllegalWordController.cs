﻿using Amazon.Runtime;
using ERP.Authorization.Rule;
using ERP.Authorization.Rule.Component;
using ERP.Enums.ID;
using ERP.Enums.Rule.Config;
using ERP.Enums.Rule.Config.Internal;
using ERP.Services.Product;
using ERP.Extensions;
using ERP.Interface.Rule;
using ERP.Models.View.Product.IllegalWord;
using ERP.Services.Identity;
using ERP.ViewModels;
using ERP.ViewModels.IllegalWord;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;

namespace ERP.Controllers.Product;

[Route("api/product/[controller]")]
[ApiController]
public class IllegalWordController : BaseController
{
    private readonly IllegalWordService _service;
    private readonly UserManager _userManager;
    private readonly ProductAbout _currentRange = ProductAbout.IllegalWordRange;

    public IllegalWordController(IllegalWordService service, UserManager userManager)
    {
        _service = service;
        _userManager = userManager;
    }

    /// <summary>
    /// 违禁词列表
    /// </summary>
    /// <returns></returns>
    [HttpPost("list")]
    public async Task<ResultStruct> List(ListDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var res = await _service.List(req, range);
        return Success(res, "获取成功");
    }

    /// <summary>
    /// 删除违禁词
    /// </summary>
    /// <returns></returns>
    [HttpPost("del")]
    [Permission(ProductAbout.IllegalWordDelete)]
    public async Task<ResultStruct> Del()
    {
        if (!Request.TryGetValue("id", out var id))
        {
            return Error(message: "缺少参数: id");
        }

        var ids = JsonConvert.DeserializeObject<List<int>>(id);

        if (ids == null || ids.Count < 0)
        {
            return Error(message: "缺少参数");
        }

        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var res = await _service.Del(ids, range);
        return res ? Success(message: "删除成功") : Error(message: "删除失败");
    }

    [HttpPost("add")]
    [Permission(ProductAbout.IllegalWordAdd)]
    public async Task<ResultStruct> Add(IllegalWordEditDto req)
    {
        var res = await _service.Add(req);

        return res.State ? Success(message: (req.ID > 0) ? "修改成功" : "添加成功") : Error(message: res.Msg);
    }

    /// <summary>
    /// 修改违禁词
    /// </summary>
    /// <param name="vm"></param>
    /// <returns></returns>
    [HttpPost("edit")]
    [Permission(ProductAbout.IllegalWordEdit)]
    public async Task<ResultStruct> Edit(Models.Product.IllegalWordModel vm)
    {
        if (vm.ID == 0)
        {
            return Error("缺少参数");
        }

        if (vm.Value.IsNull())
        {
            return Error("请填写违禁词");
        }

        if (vm.Value.Length < 255)
        {
            return Error("违禁词长度不可超过255");
        }

        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var res = await _service.Edit(vm, range);

        return res.State ? Success(message: "修改成功") : Error(message: res.Msg);
    }

    [HttpPost("clear")]
    [Permission(ProductAbout.IllegalWordDelete)]
    public async Task<ResultStruct> Clear()
    {
        var res = await _service.Clear();

        return res.State ? Success(message: "删除成功") : Error(message: "删除失败");
    }


    [Route("init")]
    [HttpPost]
    public async Task<ResultStruct> Init()
    {
        var res = await _service.Init();

        return res.State ? Success(data: res.Data!) : Error(res.Msg);
    }

    [HttpPost("show")]
    public virtual async Task<ResultStruct> Show(IdDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var res = await _service.Info(req.ID, range);

        return res == null ? Success(data: res) : Error();
    }

    [HttpGet("destroy/{id}")]
    [Permission(ProductAbout.IllegalWordDelete)]
    public virtual async Task<ResultStruct> Destroy(int id)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var res = await _service.Destroy(id, range);
        return res ? Success(message: "删除成功") : Error("删除失败");
    }
}