using ERP.Services.Caches;
using ERP.Services.Product;
using ERP.Services.Stores;
using Coravel.Events.Interfaces;
using Coravel.Queuing.Broadcast;
using Coravel.Queuing.Interfaces;
using ERP.Interface;
using ERP.Services.Host;
using ERP.Services.DB.ProductType;
using ERP.Extensions;
using Model = ERP.Models.Product.ToSellModel;
using ERP.Models.DB.Product;
using ERP.Models.View.Products.SmartHijacking;
using ERP.Data.Products;

namespace ERP.Controllers.Product
{
    [Route("api/product/[controller]")]
    [ApiController]
    public class SmartHijackingController : BaseController
    {
        private readonly ISessionProvider _sessionProvider;
        private readonly LanguagesCache _cache;
        private readonly ToSellService _service;
        private readonly StoreService _storeService;
        private readonly ProductHost _productHost;
        private readonly ILogger<SmartHijackingController> _logger;

        public SmartHijackingController(ISessionProvider sessionProvider, LanguagesCache cache, ToSellService service, StoreService storeService, ProductHost productHost, ILogger<SmartHijackingController> logger)
        {
            _cache = cache;
            _service = service;
            _storeService = storeService;
            _sessionProvider = sessionProvider;
            _productHost = productHost;
            _logger = logger;
        }

        private ISession Session { get => _sessionProvider.Session!; }

        [Route("init")]
        [HttpPost]
        public async Task<ResultStruct> Init()
        {
            return await System.Threading.Tasks.Task.FromResult(Success(new
            {
                menu = new
                {
                    hijacking = true,
                    time = true,
                    price = true,
                },
                item = new
                {
                    search = true
                },
                @params = new
                {
                    language = _cache.List()!.ToDictionary(m => m.Code, m => m)
                }
            }));
        }

        /// <summary>
        /// 获取列表初始数据
        /// </summary>
        /// <returns></returns>
        [Route("index")]


        [HttpPost]
        public async Task<ResultStruct> Index()
        {
            var ruleInit = await _service.Init();
            return Success(ruleInit);
        }

       
        [Route("CreateUploadAsins")]
        [HttpPost]
        public async Task<ResultStruct> CreateUploadAsins([FromServices] IQueue queue, [FromServices] IListener<QueueTaskCompleted> ilistener, CreateUploadAsinsVM vm)
        {
            int userid = Session.GetUserID();
            var id = Session.GetAsinProduct();
            var listener = (ilistener as TaskCompletedListener)!;
            if (!listener.IsCompleted(id))
            {
                var metric = queue.GetMetrics();
                return Warning($"当前正在推送产品，请稍后重试", "Pulling");
            }
            else
            {
                //判断店铺
                var store = await _storeService.GetStoreByID(vm.storeID);
                if (store is null || store.IsDel == true || store.IsVerify != Models.Stores.StoreRegion.IsVerifyEnum.Certified)
                {
                    return Error("店铺状态异常，请确认");
                }

                vm.asins = vm.asins.Distinct().ToList();

                //开启线程，拉取asin产品【一次不超过20个，超过需要分批】，比较marketplace

                var guid = Guid.NewGuid();
                Session.SetAsinProduct(guid);
                listener.AddTask(guid);
                queue.QueueTask(async () =>
                {
                    try
                    {
                        await _productHost.GetProducts(vm.storeID, userid, vm.marketPlaceID, vm.asins);
                    }
                    catch (Exception e)
                    {
                        _logger.LogError($"pull Product by asins,error:{e.Message}");
                    }
                    listener.RunningTasks[guid] = true;
                });
                return Success("开始拉取，请稍后查看结果");
            }
        }

        /// <summary>
        /// 批量修改，同一storeid,同一市场
        /// </summary>
        /// <param name="vm"></param>
        /// <param name="queue"></param>
        /// <param name="ilistener"></param>
        /// <returns></returns>
        [Route("UpdateUploadAsins")]
        [HttpPost]
        public async Task<ResultStruct> UpdateUploadAsins(UpdateAsinsVM vm, [FromServices] IQueue queue, [FromServices] IListener<QueueTaskCompleted> ilistener)
        {
            int userid = Session.GetUserID();
            var id = Session.GetAsinProduct();
            var listener = (ilistener as TaskCompletedListener)!;
            if (!listener.IsCompleted(id))
            {
                var metric = queue.GetMetrics();
                return Warning($"当前正在推送产品，请稍后重试", "Pulling");
            }
            else
            {
                var result = _service.CheckModels(vm);
                if (result.Count<=0)
                {
                    return Error("没有找到记录！");
                }
                if (await _service.Updates(result))
                {
                    //var item = result.First();
                    ////推送产品
                    //var guid = Guid.NewGuid();
                    //Session.SetAsinProduct(guid);
                    //listener.AddTask(guid);
                    //queue.QueueTask(async () =>
                    //{
                    //    try
                    //    {
                    //        await _productHost.HijackingProducts(item.storeRegion.ID, userid, item.AsinMarketplace, vm.ids, ActionType.Update);
                    //    }
                    //    catch (Exception e)
                    //    {
                    //        _logger.LogError($"pull Product by asins,error:{e.Message}");
                    //    }
                    //    listener.RunningTasks[guid] = true;
                    //});
                    return Success("更新成功");
                }
                else
                {
                    return Error($"更新失败");
                }
            }
        }

        /// <summary>
        /// 上架或下架,同一店铺，同一市场
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [Route("BatchAsins")]
        [HttpPost]
        public async Task<ResultStruct> BatchAsins(BatchAsinsVM vm, [FromServices] IQueue queue, [FromServices] IListener<QueueTaskCompleted> ilistener)
        {
            int userid = Session.GetUserID();
            var id = Session.GetAsinProduct();
            var listener = (ilistener as TaskCompletedListener)!;
            if (!listener.IsCompleted(id))
            {
                var metric = queue.GetMetrics();
                return Warning($"当前正在推送产品，请稍后重试", "Pulling");
            }
            else
            {
                var type = vm.actionType == "1" ? ActionType.PUtOn : ActionType.TakeOff;
                var result = _service.GetModels(vm.Ids);
                if (result is not null)
                {
                    //上架时候，发货天数和价格不能为0
                    if(type == ActionType.PUtOn)
                    {                                                
                        if (result.Where(x => x.UnitPrice == 0).Count() > 0)
                        {
                            return Error($"价格不能为0！");
                        }

                        if (result.Where(x => x.StockDays is null || x.StockDays == 0).Count() > 0)
                        {
                            return Error($"需要填写发货天数！");
                        }
                        //更新sku
                        await _service.UpdateSkus(result);
                    }
                   
                    var item = result.First();
                    //推送产品
                    var guid = Guid.NewGuid();
                    Session.SetAsinProduct(guid);
                    listener.AddTask(guid);
                    queue.QueueTask(async () =>
                    {
                        try
                        {
                            await _productHost.HijackingProducts(item.storeRegion.ID, userid, item.AsinMarketplace, vm.Ids, type);
                        }
                        catch (Exception e)
                        {
                            _logger.LogError($"pull Product by asins,error:{e.Message}");
                        }
                        listener.RunningTasks[guid] = true;
                    });
                    return Success();
                }
                else
                {
                    return Error($"更新失败！");
                }
            }
        }


        /// <summary>
        /// 上架或下架,同一店铺，同一市场
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [Route("DeleteAsins")]
        [HttpPost]
        public async Task<ResultStruct> DeleteAsins(ParamIDs vm, [FromServices] IQueue queue, [FromServices] IListener<QueueTaskCompleted> ilistener)
        {
            int userid = Session.GetUserID();
            var id = Session.GetAsinProduct();
            var listener = (ilistener as TaskCompletedListener)!;
            if (!listener.IsCompleted(id))
            {
                var metric = queue.GetMetrics();
                return Warning($"当前正在推送产品，请稍后重试", "Pulling");
            }
            else
            {
                var result = _service.GetModels(vm.Ids);
                if (result is not null)                                 
                {
                    var delList = result.Where(x => x.Sync==0).ToList();
                    if (delList.Count>0)
                    {
                        //删除未同步过的数据
                      bool f= await _service.Deletes(delList);
                        if(!f) return Error($"删除失败");
                    }
                    var syncList = result.Where(x => x.Sync == 1).ToList();
                    if (syncList!=null && syncList.Count>0)
                    {
                        var item = result.First();
                        //推送产品
                        var guid = Guid.NewGuid();
                        Session.SetAsinProduct(guid);
                        listener.AddTask(guid);
                        queue.QueueTask(async () =>
                        {
                            try
                            {
                                var list = syncList;
                                await _productHost.HijackingProducts(item.storeRegion.ID, userid, item.AsinMarketplace, list.Select(x=>x.ID).ToList(), ActionType.Delete);
                            }
                            catch (Exception e)
                            {
                                _logger.LogError($"pull Product by asins,error:{e.Message}");
                            }
                            listener.RunningTasks[guid] = true;
                        });
                    }                  
                    return Success("删除成功");
                }
                else
                {
                    return Error($"删除失败");
                }
            }
        }


        /// <summary>
        /// 上架或下架,同一店铺，同一市场
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [Route("GetHijackingProduct")]
        [HttpPost]
        public async Task<ResultStruct> GetHijackingProduct(ParamIDs vm)
        {
            var result = _service.GetModels(vm.Ids);
            if (result is not null && result.Count > 0)
            {
                var item = result.Select(a => new
                {
                    a.ID,
                    a.ImageUrl,
                    a.storeRegion,
                    a.User.TrueName,
                    a.Title,
                    a.Brand,
                    a.Manufacturer,
                    UnitPrice = a.UnitPrice.ToString(a.Currency, false),
                    ShippingPrice = a.ShippingPrice.ToString(a.Currency, false),
                    CreatedAt = a.CreatedAt.ToString("yyyy-MM-dd"),
                    a.Msg,
                    ProductResult = a.ProductResult.GetDescription(),
                    a.Asin,
                    a.AsinMarketplace,
                    a.Quantity,
                    a.Currency,
                    a.StockDays,
                    a.Sku,
                    a.ProductState,
                    a.SyncTime,
                });

                return Success(item.First());
            }
            else
            {
                return Error($"更新失败！");
            }
        }

        /// <summary>
        /// 获取列表数据
        /// </summary>
        /// <returns></returns>
        [Route("list")]
        [HttpPost]
        public async Task<ResultStruct> List(HijackingVM vM)
        {
            var data = await _service.List(vM);
            var list = data.Transform(a => new
            {
                a.ID,
                a.ImageUrl,
                a.storeRegion,
                a.User.TrueName,
                a.Title,
                a.Brand,
                a.Manufacturer,
                UnitPrice = a.UnitPrice.ToString(a.Currency, false),
                ShippingPrice = a.ShippingPrice.ToString(a.Currency, false),
                CreatedAt = a.CreatedAt.ToString("yyyy-MM-dd"),
                a.Msg,
                ProductResult = a.ProductResult.GetDescription(),
                a.Asin,
                a.AsinMarketplace,
                a.Quantity,
                a.Currency,
                a.StockDays,
                a.Sku,
                a.SyncTime,
                a.ProductState,
                a.Sync,
            });
            return Success(list);
        }





        /// <summary>
        /// 保证同一店铺，同一市场
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [Route("CheckAsins")]
        [HttpPost]
        public async Task<ResultStruct> CheckAsins(ParamIDs vm)
        {
            var result = _service.GetModels(vm.Ids);
            if (result.Count > 0)
            {
                if (result.Distinct(x => x.storeRegion.ID).Count() > 1)
                {
                    return Error($"请选择同一店铺同一市场的数据！");
                }

                if (result.Distinct(x => x.AsinMarketplace).Count() > 1)
                {
                    return Error($"请选择同一店铺同一市场的数据！");
                }
                return Success();
            }
            else
            {
                return Error($"没有查到数据，请刷新重试！");
            }
        }
    }
}
