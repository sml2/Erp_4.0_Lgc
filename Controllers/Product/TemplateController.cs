﻿using Microsoft.AspNetCore.Mvc;
using ERP.Controllers;
using ERP.Services.Caches;
using ERP.Services.Product;

namespace ERP.Controllers.Product;

[Route("api/product/[controller]")]
[ApiController]
public class TemplateController : BaseController
{
    private readonly LanguagesCache _languagesCache;
    private readonly Nation _nationCache;

    public TemplateController(LanguagesCache languagesCache, Nation nationCache)
    {
        _languagesCache = languagesCache;
        _nationCache = nationCache;
    }

    [Route("init")]
    [HttpPost]
    public async Task<ResultStruct> Init()
    {
        //todo TemplateController Init 权限 Cache
        //$extMenu = ($this->getSessionObj()->getConcierge() == User::TYPE_OWN) ? true : $this->ruleInfo()
        //    ->hasExtTemplate();
        //$sketchMenu = ($this->getSessionObj()->getConcierge() == User::TYPE_OWN) ? true : $this->ruleInfo()
        //    ->hasSketchTemplate();
        //$keywordMenu = ($this->getSessionObj()->getConcierge() == User::TYPE_OWN) ? true : $this->ruleInfo()
        //    ->hasKeywordTemplate();
        //$data = [
        //    'menu' => [
        //        'ext'     => $extMenu,
        //        'sketch'  => $sketchMenu,
        //        'keyword' => $keywordMenu,
        //    ],
        //    'item' => [
        //        'search' => ($this->getSessionObj()->getConcierge() == User::TYPE_OWN) ? true : $this->ruleInfo()
        //            ->hasTemplateSearch(),
        //    ],
        //    'params' => [
        //      'language' => $this->getLanguageCache()
        //    ],
        //];
        //return success('获取成功',$data);
        return await System.Threading.Tasks.Task.FromResult(Success(new
        {
            menu = new
            {
                ext = true,
                sketch = true,
                keyword = true,
            },
            item = new
            {
                search = true
            },
            @params = new
            {
                language = _languagesCache.List()!.ToDictionary(m => m.Code, m => m),
                nation = _nationCache.List()!.ToDictionary(m => m.Short, m => m)
            }
        }));
    }

    [Route("GetLanguages")]
    [HttpPost]
    public async Task<ResultStruct> GetLanguages()
    {
        //todo TemplateController Init 权限 Cache
        //$extMenu = ($this->getSessionObj()->getConcierge() == User::TYPE_OWN) ? true : $this->ruleInfo()
        //    ->hasExtTemplate();
        //$sketchMenu = ($this->getSessionObj()->getConcierge() == User::TYPE_OWN) ? true : $this->ruleInfo()
        //    ->hasSketchTemplate();
        //$keywordMenu = ($this->getSessionObj()->getConcierge() == User::TYPE_OWN) ? true : $this->ruleInfo()
        //    ->hasKeywordTemplate();
        //$data = [
        //    'menu' => [
        //        'ext'     => $extMenu,
        //        'sketch'  => $sketchMenu,
        //        'keyword' => $keywordMenu,
        //    ],
        //    'item' => [
        //        'search' => ($this->getSessionObj()->getConcierge() == User::TYPE_OWN) ? true : $this->ruleInfo()
        //            ->hasTemplateSearch(),
        //    ],
        //    'params' => [
        //      'language' => $this->getLanguageCache()
        //    ],
        //];
        //return success('获取成功',$data);
        return await System.Threading.Tasks.Task.FromResult(
            Success(_languagesCache.List()!.ToDictionary(m => m.Code, m => m)
            ));
    }
}