﻿using ERP.Data;
using ERP.Models.Product;
using ERP.Services.Identity;
using Microsoft.EntityFrameworkCore;

namespace ERP.Controllers.Product;

[Route("api/product/[controller]/[action]")]
[ApiController]
public class MigrationTaskController : BaseController
{
    private readonly UserManager _userManager;
    private readonly DBContext _dbContext;
    private readonly DBContext3 _dbContext3;

    public MigrationTaskController(UserManager userManager, DBContext dbContext, DBContext3 dbContext3)
    {
        _userManager = userManager;
        _dbContext = dbContext;
        _dbContext3 = dbContext3;
    }

    /// <summary>
    /// 列表
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> List()
    {
        var user = await _userManager.GetUserAsync(User);
        var result = await _dbContext.ProductMigrationTasks.Where(t => t.UserID == user.Id)
            .OrderByDescending(t => t.ID)
            .Select(t => new ListDto(t))
            .ToArrayAsync();
        foreach (var item in result)
        {
            var wait = await _dbContext.ProductMigrationTasks
                .Where(t => t.State != ProductMigrationTask.TaskState.Completed && t.ID < item.Id).CountAsync();
            item.WaitCount = wait;
        }
        return Success(result, "获取成功");
    }

    public class ListDto
    {
        public ListDto(ProductMigrationTask task)
        {
            CreatedAt = task.CreatedAt;
            Id = task.ID;
            Key = task.Key;
            Type = task.Type;
            State = task.State;
            TotalCount = task.TotalCount;
            CompletedCount = task.CompletedCount;
            SuccessCount = task.SuccessCount;
            FailedCount = task.FailedCount;
        }

        public int Id { get; set; }
        public Guid Key { get; set; }
        public DateTime CreatedAt { get; set; }
        public ProductMigrationTask.TaskType Type { get; set; }
        public ProductMigrationTask.TaskState State { get; set; }
        public int TotalCount { get; set; }
        public int CompletedCount { get; set; }
        public int SuccessCount { get; set; }
        public int FailedCount { get; set; }
        
        public int WaitCount { get; set; }
    }

    public record AddRequest(string Key);

    /// <summary>
    /// 添加迁移任务
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> Add(AddRequest request)
    {
        var key = Guid.Parse(request.Key);
        var user = await _userManager.GetUserAsync(User);
        var count = await _dbContext.ProductMigrationTasks.Where(t => t.Key == key)
            .CountAsync();

        if (count > 0)
            return Error(message: "该口令已被使用");
        var erp3Task = await _dbContext3.MigrationTasks.Where(t => t.d_key == request.Key).FirstOrDefaultAsync();
        if (erp3Task is null)
            return Error("口令不存在");

        var task = ProductMigrationTask.FromErp3Task(erp3Task);

        task.Key = key;
        task.UserID = user.Id;
        task.GroupID = user.GroupID;
        task.CompanyID = user.CompanyID;
        task.OEMID = user.OEMID;
        _dbContext.Add(task);
        await _dbContext.SaveChangesAsync();
        return Success("添加迁移任务成功");
    }
}