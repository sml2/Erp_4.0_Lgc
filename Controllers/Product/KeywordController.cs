﻿using ERP.Authorization.Rule;
using ERP.Enums.ID;
using ERP.Enums.Rule.Config;
using ERP.Enums.Rule.Config.Internal;
using Microsoft.AspNetCore.Mvc;
using ERP.Services.Product;
using ERP.Models.Product;
using ERP.Extensions;
using ERP.Interface.Rule;
using ERP.Models.View.Product;
using ERP.Models.View.Product.Template;
using ERP.Services.Identity;
using ERP.ViewModels;
using ERP.ViewModels.Template;

namespace ERP.Controllers.Product;

[Route("api/[controller]")]
[ApiController]
public class KeywordController : BaseController
{
    private readonly KeywordService _service;
    private readonly UserManager _userManager;
    private readonly ProductAbout _currentRange = ProductAbout.TemplateRange;


    public KeywordController(KeywordService service, UserManager userManager)
    {
        _service = service;
        _userManager = userManager;
    }

    //todo 权限
    // protected function ruleInfo(): \App\Rule\BitwiseFlags\Product
    // {
    //     return $this->getSessionObj()->getRule()->getProduct()->getBiter();
    // }

    [HttpGet("init")]
    public async Task<ResultStruct> Init()
    {
        return await System.Threading.Tasks.Task.FromResult(Success(new
        {
            search = true,
        }));
    }

    /// <summary>
    /// 获关键字词组
    /// </summary>
    /// <returns></returns>
    [HttpPost("list")]
    [Permission(ProductAbout.TemplateKeyword)]
    public async Task<ResultStruct> List(ListDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var res = await _service.List(req, range);
        return Success(res, "获取成功");
    }

    /// <summary>
    /// 获关键字词
    /// </summary>
    /// <returns></returns>
    [HttpPost("getKeyword")]
    public async Task<ResultStruct> GetKeyword()
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _service.GetKeyword(range);
        return Success(result);
    }

    /// <summary>
    /// 修改关键词模板
    /// </summary>
    /// <param name="vm"></param>
    /// <returns></returns>
    [HttpPost("edit")]
    [Permission(ProductAbout.TemplateKeyword)]
    public async Task<ResultStruct> Edit(KeywordEditVM vm)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var res = await _service.AddOrUpdateKeyword(vm, range);
        return res.State ? Success(message: (vm.ID > 0) ? "修改成功" : "添加成功") : Error(message: res.Msg);
    }


    [HttpPost("import")]
    [Permission(ProductAbout.TemplateKeyword)]
    public async Task<ResultStruct> ImportKeyword(KeywordEditVM req)
    {
        var res = await _service.ImportKeyword(req);
        return res ? Success(message: "导入成功") : Error(message: "导入失败");
    }


    /// <summary>
    /// 关键词工具初始化
    /// </summary>
    /// <returns></returns>
    [HttpPost("keywordToolInit")]
    [Permission(ProductAbout.TemplateKeyword)]
    public async Task<ResultStruct> KeywordToolInit()
    {
        //todo KeywordToolInit rules 权限
        // $rule = [
        // 'import' => ($this->getSessionObj()->getConcierge() == User::TYPE_OWN) ? true : $this->ruleInfo(
        // )->hasKeywordToolImport(),
        // 'maintain' => true
        //     ];
        return await System.Threading.Tasks.Task.FromResult(Success(new
        {
            import = true,
            maintain = true,
        }));
    }

    //todo KeywordController rules 权限
    // private function rules(): \App\Rule\BitwiseFlags\Product
    // {
    //     return $this->sessionData->getRule()->getProduct()->getBiter();
    // }

    [HttpPost("increment")]
    public async Task<ResultStruct> Increment(IdDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var resutl = await _service.Increment(req.ID, range);
        return resutl ? Success() : Error();
    }

    [HttpPost("show")]
    [Permission(ProductAbout.TemplateKeyword)]
    public virtual async Task<ResultStruct> Show(IdDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var res = await _service.Info(req.ID, range);
        return res.State ? Success(data: res.Data!) : Error(res.Msg);
    }

    [HttpGet("destroy/{id}")]
    [Permission(ProductAbout.TemplateKeyword)]
    public virtual async Task<ResultStruct> Destroy(int id)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var res = await _service.Destroy(id, range);
        return res ? Success(message: "删除成功") : Error("删除失败");
    }
}