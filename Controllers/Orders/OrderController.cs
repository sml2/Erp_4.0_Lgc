using ERP.DomainEvents;
using UnitCache = ERP.Services.Caches.Unit;
using MergeService = ERP.Services.DB.Orders.Merge;
using OrderService = ERP.Services.DB.Orders.Order;
using OrderHostService = ERP.Services.DB.Orders.OrderHost;
using UserService = ERP.Services.DB.Users.UserService;
using StoreService = ERP.Services.Stores.StoreService;
using PurchaseService = ERP.Services.Purchase.Purchase;
using OrderModel = ERP.Models.DB.Orders.Order;
using OrderRemarkModel = ERP.Models.DB.Orders.Remark;
using OrderMergeModel = ERP.Models.DB.Orders.Merge;
using static ERP.Models.DB.Orders.Order;
using ERP.Extensions;
using ERP.ViewModels.Order.AmazonOrder;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using NationCache = ERP.Services.Caches.Nation;
using CompanyCache = ERP.Services.Caches.Company;
using ERP.Enums.Orders;
using ERP.Interface;
using ERP.Models.Api.Logistics;
using ERP.Models.View.Order.AmazonOrder;
using ERP.Enums;
using ERP.Export.Queue;
using ERP.Models.DB.Orders;
using ERP.Models.Export;
using ERP.Services.Caches;
using ERP.Services.DB.Users;
using ERP.Services.Export;
using ERP.Services.Finance;
using ERP.Services.Statistics;
using MediatR;
using Z.EntityFramework.Plus;
using BillLog = ERP.Models.Finance.BillLog;
namespace ERP.Controllers.Orders;

using Coravel.Events.Interfaces;
using Coravel.Queuing.Broadcast;
using Coravel.Queuing.Interfaces;
using ERP.Data;
using ERP.Enums.Purchase;
using ERP.Models.View.Order;
using DisorderService =ERP.Services.Distribution.AmazonOrderService;
using ERP.Services.Excel;
using ERP.Services.Host;
using Microsoft.AspNetCore.Identity;
using NPOI.SS.Formula.Functions;
using Sy;
using System.Text;
using WaybillService = ERP.Services.DB.Logistics.WayBill;
using PlatformDataCache = PlatformData;

public class OrderController : BaseController
{
    private readonly OrderService _orderService;
    private readonly OrderHostService _orderHostService;
    private readonly WaybillService waybillService;
    private readonly PurchaseService _purchaseService;
    public readonly MergeService _MergeService;
    public readonly UserService _userService;
    public readonly StoreService _storeService;
    private readonly DBContext _dbContext;
    private readonly NationCache _nationCache;
    private readonly PlatformDataCache _platformDataCache;
    private readonly UserManager<Models.DB.Identity.User> _userManager;
    private readonly ERP.Services.DB.Statistics.Statistic _statisticService;
    private readonly ISessionProvider _sessionProvider;
    private readonly IMediator _mediator;
    private readonly StatisticMoney _statisticMoney;
    private readonly IExcelFactory _excelFactory;
    private readonly BillingRecordsService _billingRecordsService;
    private readonly CompanyService _companyService;

    public OrderController(OrderHostService orderHostService, OrderService orderService,
        PurchaseService purchaseService,
        DBContext dbContext,
        NationCache nationCache,
        ERP.Services.DB.Statistics.Statistic statisticService,
        UserService userService,
        StoreService storeService,
        IExcelFactory excelFactory,
        UserManager<Models.DB.Identity.User> userManager,
        MergeService mergeService, WaybillService _waybillService, ISessionProvider sessionProvider, IMediator mediator, BillingRecordsService billingRecordsService, CompanyService companyService, PlatformData platformDataCache)
    {
        _orderHostService = orderHostService;
        _orderService = orderService;
        _purchaseService = purchaseService;
        _dbContext = dbContext;
        _MergeService = mergeService;
        _sessionProvider = sessionProvider;
        _mediator = mediator;
        _billingRecordsService = billingRecordsService;
        _companyService = companyService;
        _platformDataCache = platformDataCache;
        _statisticService = statisticService;
        _nationCache = nationCache;
        _userService = userService;
        _userManager = userManager;
        _storeService = storeService;
        _excelFactory = excelFactory;
        _statisticMoney = new StatisticMoney(_statisticService);
        waybillService = _waybillService;
    }
    private ISession Session { get => _sessionProvider.Session!; }
    private int UserID { get => Session.GetUserID(); }
    private int GroupID { get => Session.GetGroupID(); }
    private int CompanyID { get => Session.GetCompanyID(); }
    private int OEMID { get => Session.GetOEMID(); }

    [HttpPost]
    public async Task<ResultStruct> Index(OrderIndexListQueryVM rq)
    {
        var user = await _userManager.GetUserAsync(User);
        await _dbContext.Entry(user).Reference(u => u.OrderUnitConfig).LoadAsync();
        var unitInfo = user.OrderUnitConfig;

        var data = await _orderService.GetOrderListPage(rq);
        if (data.Items.Count == 0) return Success(data);
        var order = data.Transform(a => new
        {
            a.ID,
            a.OrderNo,
            a.EntryMode,
            a.StoreId,
            Store = a.Store?.Name,//店铺
            a.ShippingType, //配送方式
            ShippingTypeName = a.ShippingType.ToString(),
            ReceiverNation = a.Receiver?.Nation, // 收货人所在国家
            NationShort = a.Receiver?.NationShort,
            ReceiverName = a.Receiver?.Name,
            Platform = a.Plateform,
            a.ProductNum,//购买商品数量
            a.Delivery,//已发货数
            OrderTotal = Helpers.TransMoney(a.OrderTotal, unitInfo.Total, a.OrderTotal.Raw.CurrencyCode, Session.GetUnitConfig(), true), // 订单总价(订单实付金额、成交价、基准货币)
            Fee = Helpers.TransMoney(a.Fee, unitInfo.Fee, a.OrderTotal.Raw.CurrencyCode, Session.GetUnitConfig(), true),//手续费(基准货币)
            Refund = Helpers.TransMoney(a.Refund, unitInfo.Refund, a.OrderTotal.Raw.CurrencyCode, Session.GetUnitConfig(), true), //订单退款金额(基准货币)
            PurchaseFee = Helpers.TransMoney(a.PurchaseFee, unitInfo.Purchase, a.OrderTotal.Raw.CurrencyCode, Session.GetUnitConfig(), true),//采购花费(基准货币)
            ShippingMoney = Helpers.TransMoney(a.ShippingMoney, unitInfo.Deliver, a.OrderTotal.Raw.CurrencyCode, Session.GetUnitConfig(), true),//运费
            Loss = Helpers.TransMoney(a.Loss, unitInfo.LOSS, a.OrderTotal.Raw.CurrencyCode, Session.GetUnitConfig(), true),//订单损耗CNY(基准货币)
            //Profit = Helpers.TransMoneyP(a.Profit, unitInfo.Profit, a.OrderTotal.Raw.CurrencyCode, Session.GetUnitConfig(),true),//订单获利(基准货币)

            Profit = Helpers.TransMoneyM(a.OrderTotal.Money - a.Fee.Money - a.Refund.Money - a.PurchaseFee.Money - a.ShippingMoney.Money - a.Loss.Money, a.OrderTotal.Raw.Rate, unitInfo.Profit, a.OrderTotal.Raw.CurrencyCode, Session.GetUnitConfig(), true),//订单获利(基准货币)

            CreateTime = a.CreateTime.ToString("yyyy-MM-dd"),//订单创建时间戳
            a.LatestShipTime,//您承诺的订单发货时间范围的最后一天
            a.PurchaseCompanyId,//采购上报公司id
            a.ProcessPurchase,//第一个添加采购用户
            a.WaybillCompanyId,//物流上报公司id
            a.ProcessWaybill,//第一个添加运单用户
            a.MergeId,
            a.IsBatchShip,
            OrderState = a.State, //订单状态 1未付款2取消6配货7待发8已发9交付Extensions.EnumExtension.GetDescription(a.State)
            a.Invoice,//发票
            a.Url, // 图片
            HasPrintLabel = (a.Bits & BITS.Waybill_HasPrintLabel) == BITS.Waybill_HasPrintLabel,//打印面单
            HasSync = (a.Bits & BITS.Waybill_HasSync) == BITS.Waybill_HasSync,//同步运单到店铺
            HasUploadInvoicetLabel = (a.Bits & BITS.Waybill_HasUploadInvoice) == BITS.Waybill_HasUploadInvoice,//上传发票
            CustomMark = a.GetCustomMark(),
            PurchaseTrackingNumber = Array.Empty<string>(),
            WaybillOrder = Array.Empty<string>(),
            DistributedLinkFlag = CheckOrderDistributedLinks(a.ID)
        });
        return Success(order, "成功");
    }

    private int CheckOrderDistributedLinks(int orderID)
    {
        var result = false;

        if (_sessionProvider.Session.GetIsDistribution())
        {
            result = _dbContext.OrderDistributedLinks.Any(x => x.SourceType == OrderDistributedLink.SourceIdType.DistributionOrderId &&
                                                               x.DistributedType == OrderDistributedLink.DistributedIdType.UserId && x.SourceId == orderID);
        }
        else
        {
            result = _dbContext.OrderDistributedLinks.Any(x => x.SourceType == OrderDistributedLink.SourceIdType.OrderId &&
                                                               x.DistributedType == OrderDistributedLink.DistributedIdType.UserId && x.SourceId == orderID);
        }

        return result ? 1 : 0;

        // if (!result)
        // {
        //     result = _dbContext.OrderDistributedLinks.Any(x => x.SourceType == OrderDistributedLink.SourceIdType.DistributionCompanyId && x.SourceId == CompanyID);
        //     if (result)
        //     {
        //         return 2;
        //     }
        //     else
        //     {
        //         return 0;
        //     }
        // }
        // return 1;
    }


    [HttpPost]
    public async Task<ResultStruct> GetPurchaseWaybill(List<int> ids)
    {
        if (ids.IsNullOrEmpty())
            return Success();
        var data = await _orderService.GetPurchaseWaybill(ids);
        return Success(data, "成功");
    }

    /// <summary>
    /// 手动添加订单 获取下拉框数据
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> AddOrderGetSelect()
    {
        var user = await _userManager.GetUserAsync(User);
        var nation = _nationCache.List();

        // 员工自己的店铺 + 公司分配的店铺
        var stores = await _storeService.CurrentUserGetStores(true);

        var platform = PlatformsType.platformElectronicBusiness;

        return await Task.FromResult(Success(new
        {
            nation,
            stores = stores
            .Select(s => new
            {
                s.ID,
                s.Platform,
                s.Name,
            }).GroupBy(a => a.Platform).ToDictionary(a => a.Key.GetID(), a => a),
            platform
        }));
    }

    /// <summary>
    /// 手动编辑订单 获取下拉框数据
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> InitEditOrder()
    {
        var user = await _userManager.GetUserAsync(User);
        var nation = _nationCache.List();

        // 员工自己的店铺 + 公司分配的店铺
        var stores = await _storeService.CurrentUserGetStores(true);

        var platform = PlatformsType.platformElectronicBusiness;

        return await Task.FromResult(Success(new
        {
            stores = stores.Select(s => new
            {
                s.ID,
                s.Platform,
                s.Name,
            }).GroupBy(a => a.Platform).ToDictionary(a => a.Key.GetID(), a => a),
            platform
        }));
    }

    /// <summary>
    /// 手动编辑订单 提交
    /// </summary>
    /// <param name="rq"></param>
    /// <returns></returns>
    public async Task<ResultStruct> SubmitEditOrder(SubmitEditOrderVM rq)
    {
        var order = await _orderService.GetOrderByIdAsync(rq.Id);
        var orderTotal_old = order.OrderTotal;
        var profitl_old = order.Profit;
        order.StoreId = rq.StoreId;
        order.Plateform = rq.Plateform;
        order.OrderNo = rq.OrderNo;
        order.OrderTotal = order.OrderTotal.Next(Session.GetUnitConfig(), rq.OrderTotal);
        order.Profit = order.Profit.Next(profitl_old - orderTotal_old + order.OrderTotal);
        await _orderService.Update(order);
        return Success();
    }

    /// <summary>
    /// 手动添加订单 
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> AddOrder(AddOrderRequestVM rq, [FromServices] UnitCache unitCache)
    {
        if (await _orderService.GetOrderInfoWhereFirst(null, rq.OrderNo, Session.GetCompanyID()) is not null)
        {
            return Error("订单号已存在");
        }

        var nation = await _dbContext.Nations.FirstOrDefaultAsync(a => a.ID == rq.ReceiverNationId);
        var stores = await _dbContext.StoreRegion.FirstOrDefaultAsync(a => a.ID == rq.StoreId);

        OrderModel order = new(Session);

        if (rq.Goods.Count > 0)
        {
            order.GoodsScope(goods =>
            {
                foreach (var item in rq.Goods)
                {
                    Good good = new();
                    good.ID = item.Id.IsNullOrWhiteSpace() ? Guid.NewGuid().ToString("N") : item.Id;
                    good.OrderItemId = good.ID;
                    good.Sku = item.SKU;
                    good.ImageURL = item.ImageURL;
                    good.Name = item.Name;
                    good.FromURL = item.FromURL;
                    good.QuantityOrdered = item.QuantityOrdered;
                    good.UnitPrice = item.UnitPrice.Mate;
                    good.ItemPrice = item.TotalPrice.Mate;
                    good.TotalPrice = good.ItemPrice;
                    goods.Add(good);
                }
                order.GoodsMoney = goods.Sum(x => x.TotalPrice.Money);
                order.ProductNum = goods.Sum(x => x.QuantityOrdered);
            }, Modes.Save);
        }
        order.OrderTotal = new MoneyMeta(Session.GetUnitConfig(), rq.OrderTotal);
        order.Profit = new MoneyMeta(Session.GetUnitConfig(), rq.OrderTotal);
        //order.PurchaseNum = rq.Goods.Sum(x => x.QuantityOrdered);
        order.DeliveryNo = order.PurchaseNum;
        order.DeliverySuccess = 0;

        order.OrderNo = rq.OrderNo;
        order.StoreId = rq.StoreId;
        //order.Store = stores;
        order.EntryMode = EntryModes.WebForm;
        order.CreateTime = DateTime.Now;
        order.BuyerUserName = rq.ReceiverName;//买家名称
        order.OperateUserId = UserID;
        order.Receiver = new()
        {
            Nation = nation!.Name,
            NationShort = nation.Short,
            NationId = rq.ReceiverNationId,
            Address1 = rq.ReceiverAddress.Trim(),
            County = "",
            City = rq.ReceiverCity,
            District = rq.ReceiverDistrict,
            Email = rq.ReceiverEmail,
            Name = rq.ReceiverName,
            Phone = rq.ReceiverPhone,
            Province = rq.ReceiverProvince,
            Zip = rq.ReceiverZip,
        };
        order.Plateform = rq.Plateform;
        order.Bits = BITS.Progresses_Unprocessed;//未处理
        order.State = Enums.Orders.States.UNSHIPPED;
        order.Delivery = 0;
        order.ShippingType = ShippingTypes.MFN;
        order.ShopID = "1";// 没用到
        order.DeliveryTime = DateTime.Now; // 
        order.EarliestShipTime = DateTime.Now; // 
        order.LastTime = DateTime.Now; // 
        order.PaymentMethod = "";
        order.ShippingMoney = 0;
        order.IsMain = IsMains.VICE;
        order.MergeId = -1;
        order.pullOrderState = PullOrderState.OutOfProcess;
        var unit = Session.GetUnitConfig();
        order.Coin = unit;
        //var rate = unitCache.Get(unit);
        //order.Rate = rate.Rate;
        order.CreatedAt = DateTime.Now;

        var transaction = _dbContext.Database.BeginTransaction();

        try
        {
            await _orderService.InsertOrder(order);
            await _orderService.AddLog(order.ID, 0, "手动添加订单", $"[{Session.GetTrueName()}]手动添加此订单成功");

            var orderCreatedEvent = new OrderCreatedEvent(order);
            await _mediator.Publish(orderCreatedEvent);
            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            return Error($"ERROR: {ex.Message}");
        }

        return Success();
    }

    /// <summary>
    /// 获取订单地址
    /// </summary>
    /// <param name="id">订单id</param>
    /// <returns></returns>
    [HttpGet("{id}")]
    public async Task<ResultStruct> GetConsigneeInfo(int id)
    {
        try
        {
            var address = await _orderService.GetAddressByOrderId(id);
            var nation = _nationCache.List();
            return Success(new { mainForm = address, nation });
        }
        catch (Exception ex)
        {
            return Error(ex.Message);
        }
    }

    //************************** 亚马逊订单详情页面***********************//
    [HttpGet("{id}")]
    public async Task<ResultStruct> Info(int id)
    {
        var orderInfo = await _orderService.GetOrderByIdAsync(id);

        if (orderInfo is null)
        {
            return Error("当前订单不存在!");
        }

        var user = await _userManager.GetUserAsync(User);
        await _dbContext.Entry(user).Reference(u => u.OrderUnitConfig).LoadAsync();
        var unitInfo = user.OrderUnitConfig;

        int is_sku = 1;
        var ids = new[] { orderInfo.ID };

        //订单操作日志
        var orderLogs = await _orderService.GetOrderAction(ids);
        //订单备注信息
        var orderRemarks = await _orderService.GetOrderRemarks(ids);

        //获取物流信息
        var waybillIds = await waybillService.GetWaybillIDsByOrder(orderInfo.ID, orderInfo.MergeId);
        var waybillAll = await _orderService.GetWaybill(waybillIds, orderInfo.Plateform.Platform);

        var waybil = waybillAll.Where(a => a.State != ERP.Enums.Logistics.States.Cancle).ToList();
        Money waybillMoney = new Ryu.Data.Money();
        var CoinShipp = "";
        if (waybil != null && waybil.Count > 0)
        {
            CoinShipp = waybil[0].CarriageNum.Raw.CurrencyCode;

            //to  do..  重新计算运费
            waybillMoney += waybil.Where(x => x.MergeID <= 0 || x.MergeID == null).Sum(x => x.CarriageNum.Money);
            var wls = waybil.Where(x => x.MergeID > 0).ToList();
            if (wls is not null && wls.Count > 0)
            {
                foreach (var w in wls)
                {
                    List<int> ds = new();
                    w.OrderIDs.Split(",").ForEach(x => ds.Add(Convert.ToInt32(x)));
                    //计算合并运单的运费，shippMoney/Order count
                    waybillMoney += w.CarriageNum.Money / ds.Count;
                }
            }
        }

        //直接转化订单状态
        //$orderInfo["progress_name"] = $this->getProgress($orderInfo["progress"]);
        List<int>? orderIds = new();
        orderIds.Add(orderInfo.ID);
        //采购信息
        var purchase = await _purchaseService.GetPurchaseInfo(orderIds);
        var CoinPurchase = "";
        Money purchaseMoney = new Ryu.Data.Money();
        var usedpurchase = purchase.Where(x => x.PurchaseAffirm != PurchaseAffirms.Cancel).ToList();
        if (usedpurchase != null && usedpurchase.Count > 0)
        {
            CoinPurchase = usedpurchase[0].PurchaseTotal.Raw.CurrencyCode;
            purchaseMoney = usedpurchase.Sum(x => x.PurchaseTotal.Money);
        }
        var code = orderInfo.OrderTotal.Raw.CurrencyCode;
        Dictionary<int, string> WAYBILL_STATE = Enum<Enums.Logistics.States>.ToDictionaryForUIByDescription();

        Dictionary<int, object> ORDER_PROGRESS = OrderState.GetOrderProgress(orderInfo.Bits);

        Money Profit = new Ryu.Data.Money();

        string orderProfitMargin = "";
        // 订单利润率
        if (orderInfo.OrderTotal.Money == 0)
        {
            orderProfitMargin = "暂无利润率";
        }
        else
        {
            //为了兼容错误数据
            //decimal m = 0;
            //int length = orderInfo.Profit.Raw.Amount.ToString().Split('.')[1].Length;
            //if (length > 2)
            //{
            //    m = orderInfo.Profit.Raw.Amount;
            //}
            //else
            //{
            //    m = orderInfo.Profit.Money.Value;
            //}
            //orderProfitMargin = $"{m / orderInfo.OrderTotal.Money.Value * 100:0.00}%";

            Profit = (orderInfo.OrderTotal.Money - orderInfo.Fee.Money - orderInfo.Refund.Money - waybillMoney - orderInfo.PurchaseFee.Money - orderInfo.Loss.Money);
            orderProfitMargin = $"{Profit.Value / orderInfo.OrderTotal.Money.Value * 100:0.00}%";
        }
        var order = new
        {
            orderInfo.ID,
            orderInfo.OrderNo,
            OrderTotal = Helpers.TransMoney(orderInfo.OrderTotal, unitInfo.Total, code, Session.GetUnitConfig(), false),
            Fee = Helpers.TransMoney(orderInfo.Fee, unitInfo.Fee, code, Session.GetUnitConfig(), false),
            Refund = Helpers.TransMoney(orderInfo.Refund, unitInfo.Refund, code, Session.GetUnitConfig(), false),
            ShippingMoney = Helpers.TransMoney(orderInfo.ShippingMoney, unitInfo.Deliver, code, Session.GetUnitConfig(), false),
            PurchaseFee = Helpers.TransMoney(orderInfo.PurchaseFee, unitInfo.Purchase, code, Session.GetUnitConfig(), false),
            Loss = Helpers.TransMoney(orderInfo.Loss, unitInfo.LOSS, code, Session.GetUnitConfig(), false),
            Profit = Helpers.TransMoneyM(Profit, orderInfo.OrderTotal.Raw.Rate, unitInfo.Profit, code, Session.GetUnitConfig(), false),
            ProfitMargin = orderProfitMargin,
            ShippingType = (orderInfo.ShippingType & ShippingTypes.AFN) == 0 ? 2 : 1,
            orderInfo.StoreId,
            orderInfo.EntryMode,
            orderInfo.ProcessWaybill,
            Coin = code,
            CoinLoss = unitInfo.LOSS ? code : Session.GetUnitConfig(),
            CoinProfit = unitInfo.Profit ? code : Session.GetUnitConfig(),
            //CoinPurchase=unitInfo.Purchase? CoinPurchase:Session.GetUnitConfig(),
            //CoinShipp = unitInfo.Deliver ? CoinShipp : Session.GetUnitConfig(),
            orderInfo.PurchaseCompanyId,
            orderInfo.WaybillCompanyId,
            Progress = orderInfo.Bits,
            orderInfo.DeliverySuccess,
            orderInfo.DeliveryFail,
            orderInfo.Url,
            orderInfo.Plateform,
            State = Extensions.EnumExtension.GetDescription(orderInfo.State),
        };
        string orderUnit = orderInfo.OrderTotal.Raw.CurrencyCode;
        var goods = orderInfo.GoodsScope().Select(a => new
        {
            a.ID,
            a.OrderItemId,
            a.Name,
            fromUrl = a.FromURL,//网址
            a.Sku,
            UnitPrice = Helpers.TransMoney(a.UnitPrice, unitInfo.ProductPrice, Session.GetUnitConfig(), false),
            ItemPrice = Helpers.TransMoney(a.ItemPrice, unitInfo.ProductPrice, Session.GetUnitConfig(), false),//商品总价
            TotalPrice = Helpers.TransMoney(a.TotalPrice, unitInfo.ProductPrice, Session.GetUnitConfig(), false),//   a.TotalPrice.ToString(Session.GetUnitConfig(), false),//总价(包含别的费用)
            a.ImageURL,//图片
            a.QuantityOrdered,//订购数量
            a.QuantityShipped,//已发货数
            a.WaitForSendNum,//待发货数
            a.Brand,
            a.Size,
            a.Color,
            ShippingPrice = Helpers.TransMoney(a.ShippingPrice, unitInfo.Deliver, Session.GetUnitConfig(), false),
            Unit = a.TotalPrice.Raw.CurrencyCode,
            goodIdText = orderInfo.Plateform.Platform.GetGoodIdDescription(),
            a.CustomizedURL,
        });
        // goods,
        var list = new
        {
            order = order,
            isSku = is_sku,
            purchase = purchase.Select(p => new
            {
                p.ID,
                p.Url,
                p.Name,
                p.Num,
                Price = Helpers.TransMoney(p.Price, unitInfo.Purchase, Session.GetUnitConfig(), false),
                PurchasePrice = Helpers.TransMoney(p.PurchasePrice, unitInfo.Purchase, code, Session.GetUnitConfig(), false),
                PurchaseTotal = Helpers.TransMoney(p.PurchaseTotal, unitInfo.Purchase, code, Session.GetUnitConfig(), false),
                p.PurchaseOrderNo,
                p.PurchaseTrackingNumber,
                p.OperateCompanyId,
                p.PurchaseAffirm,
                PurchaseState = Extensions.EnumExtension.GetDescription(p.PurchaseState),
            }),
            nation = _nationCache.List(),
            orderLogs,
            goods,
            waybill = waybillAll.Select(a => new
            {
                a.ID,
                a.WaybillOrder,
                Logistic = a.Logistic,
                LogisticSign = a.Logistic.Platform.ToString(),
                a.Express,
                a.Tracking,
                CarriageNum = Helpers.TransMoney(a.CarriageNum, unitInfo.Deliver, code, Session.GetUnitConfig(), false),
                a.DeliverDate,
                a.CompanyID,
                OperateCompanyId = a.OperatecompanyID,
                a.WaybillCompanyID,
                a.State,
                stateStr = Extensions.EnumExtension.GetDescription(a.State),
                a.IsVerify,
                a.ButtonName,
                a.LogisticExt,
                a.ProductInfo,
                a.Sender,
                a.CarriageUnit,
                a.CarriageBase.Money,
                a.ShipMethod,
            }),
            waybillStatus = WAYBILL_STATE,
            orderStatus = GetSupportsOrderStates(),
            orderProgress = ORDER_PROGRESS,
            orderRemarks,
            purchaseAudit = 2, // purchase_audit  采购权限???,
            //store = _storeCache.List(), //store
            companyId = Session.GetCompanyID(), // $this->sessionData->getCompanyId()
            stockLog = Array.Empty<object>(),// []
            receiver = orderInfo.Receiver,
        };
        return Success(list);
    }

    /// <summary>
    /// 下拉框获取订单进度
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> SelectGetOrderProgress()
    {
        return await Task.FromResult(Success(OrderState.OrderProgressSelect));
    }



    /// <summary>
    /// 自定义添加订单商品
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> AddGoods(EditGoodsVM data)
    {

        var orderData = await _orderService.GetOrderInfoWhereFirst(data.OrderId);
        if (orderData == null)
        {
            return Error(message: "查询不到此订单");
        }
        if (orderData.EntryMode != EntryModes.WebForm)
        {
            return Error(message: "非手动订单不能添加商品");
        }
        var rate = orderData.OrderTotal.Raw.Rate;
        //$this->getSessionObj()->getUnitConfig() ?? 'CNY';
        var currencyUnit = Session.GetUnitConfig() ?? "CNY";
        //添加自定义订单商品时，如果【通用展示货币】和此订单添加时候的【货币单位】相等,并且此时的订单汇率配置项采用【亚马逊订单拉取时汇率】,在此采用此前创建订单时所记录的汇率。
        //这两种配置项搭配起来可以还原原始数据
        if (orderData.GoodsScope().FirstOrDefault(a => a.ID == data.Id) != null)
            return Error("产品id不许重复");
        Good good = new();
        good.ID = data.Id.IsNullOrWhiteSpace() ? Guid.NewGuid().ToString("N") : data.Id;
        good.OrderItemId = good.ID;
        good.Sku = data.Sku;
        good.ImageURL = data.Url;
        good.Name = data.Name;
        good.QuantityOrdered = data.Num;
        good.FromURL = data.FromUrl;
        //good.Unit = currencyUnit;
        good.UnitPrice = new MoneyMeta(currencyUnit, data.Price).ToRecode();
        good.ItemPrice = new MoneyMeta(currencyUnit, data.Price * data.Num).ToRecode();
        good.TotalPrice = new MoneyMeta(currencyUnit, data.Price * data.Num).ToRecode();
        good.QuantityShipped = 0;

        orderData.GoodsScope(goods => goods.Add(good));

        orderData.ProductNum += good.QuantityOrdered;
        //orderData.GoodsMoney = orderData.GoodsMoney with { Money = orderData.GoodsMoney.Money + good.TotalPrice.Money };
        orderData.GoodsMoney += good.TotalPrice;
        //orderData.OrderTotal = orderData.OrderTotal.Next(orderData.OrderTotal += good.TotalPrice.Money);
        //orderData.Profit = orderData.Profit.Money + good.TotalPrice.Money;

        var transaction = _dbContext.Database.BeginTransaction();
        try
        {
            //#region 修改数据-订单总价统计/跟踪

            //await _statisticService.OrderTotalEdit(orderData, Session);

            //#endregion

            await _orderService.Update(orderData);
            transaction.Commit();
        }
        catch (Exception e)
        {
            transaction.Rollback();
            Console.WriteLine(e.Message);
            throw new Exception(e.Message);
        }
        return Success();
    }

    // 订单单个商品回填
    [HttpPost]
    public async Task<ResultStruct> GetOrderSingleGood(EditGoodsVM data)
    {
        //a.ID,
        //a.Name,
        //a.FromURL,//网址
        //a.Sku,
        //UnitPrice = a.UnitPrice.ToString(Session.GetUnitConfig(), false), //商品单价
        //ItemPrice = a.ItemPrice.ToString(Session.GetUnitConfig(), false),//商品总价
        //TotalPrice = a.TotalPrice.ToString(Session.GetUnitConfig(), false),//总价(包含别的费用)
        //a.ImageURL,//图片
        //a.QuantityOrdered,//订购数量
        //a.QuantityShipped,//已发货数
        //a.WaitForSendNum,//待发货数
        //ShippingPrice = a.ShippingPrice.ToString(Session.GetUnitConfig(), false),
        //Unit = Session.GetUnitConfig(),
        //goodIdText = orderInfo.Plateform.Platform.GetGoodIdDescription()

        var orderData = await _orderService.GetOrderInfoWhereFirst(data.OrderId);
        if (orderData == null)
        {
            return Error(message: "查询不到此订单");
        }
        if (!orderData.HasGoods())
        {
            return Error(message: "订单商品不存在");
        }
        //var goods = JsonConvert.DeserializeObject<List<OrderGood>>(orderData.Goods!)!;
        var goods = orderData.GoodsScope();
        var good = goods.FirstOrDefault(a => a.OrderItemId == data.Id);
        if (good == null)
        {
            return Error(message: "订单商品不存在!");
        }
        return Success(new
        {
            good.ID,
            good.Name,
            good.Sku,
            FromUrl = good.FromURL,//网址
            Price = good.UnitPrice.ToString(Session.GetUnitConfig(), false), //商品单价
            //ItemPrice = good.ItemPrice.ToString(Session.GetUnitConfig(), false),//商品总价
            //TotalPrice = good.TotalPrice.ToString(Session.GetUnitConfig(), false),//总价(包含别的费用)
            Url = good.ImageURL,//图片
            Num = good.QuantityOrdered,//订购数量
            //Unit = Session.GetUnitConfig(),
            //goodIdText = orderInfo.Plateform.Platform.GetGoodIdDescription()
        });
    }
    /// <summary>
    /// 修改自定义订单商品
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> EditGoods(EditGoodsVM data)
    {
        var orderData = await _orderService.GetOrderInfoWhereFirst(data.OrderId);
        if (orderData == null)
        {
            return Error(message: "查询不到此订单");
        }
        if (orderData.EntryMode != EntryModes.WebForm)
        {
            return Error(message: "非手动订单不能编辑商品");
        }
        if (!orderData.HasGoods())
        {
            return Error(message: "订单商品不存在");
        }
        var good = orderData.GoodsScope().FirstOrDefault(a => a.ID == data.OldGoodId);
        if (good is null)
            return Error($"未找到[订单ID:{data.OrderId}][产品ID:{data.OldGoodId}]的订单产品");

        if (orderData.GoodsScope().FirstOrDefault(a => a.ID != data.OldGoodId && a.ID == data.Id) != null)
            return Error("产品id不许重复");
        else
        {
            orderData.GoodsScope(goods =>
            {
                var good = goods.FirstOrDefault(a => a.ID == data.OldGoodId);
                goods.Remove(good);
                var Total = data.Price * data.Num;
                var tempOrderTotal = orderData.OrderTotal.Money - orderData.GoodsMoney.Money;
                orderData.GoodsMoney -= good!.TotalPrice;

                //good.ID = data.Id;
                good.ID = data.Id.IsNullOrWhiteSpace() ? Guid.NewGuid().ToString("N") : data.Id;
                good.OrderItemId = good.ID;
                good.Sku = data.Sku;
                good.ImageURL = data.Url;
                good.Name = data.Name;
                good.FromURL = data.FromUrl;
                good.QuantityOrdered = data.Num;

                good.UnitPrice = new MoneyMeta(Session.GetUnitConfig(), data.Price).ToRecode();
                good.ItemPrice = new MoneyMeta(Session.GetUnitConfig(), Total).ToRecode();
                good.TotalPrice = new MoneyMeta(Session.GetUnitConfig(), Total).ToRecode();
                //good.QuantityShipped = good.QuantityShipped;
                goods.Add(good);
                orderData.ProductNum = orderData.ProductNum - good!.QuantityOrdered + data.Num;
                orderData.GoodsMoney += good!.TotalPrice;
                orderData.OrderTotal = orderData.OrderTotal.Next(tempOrderTotal + orderData.GoodsMoney.Money);
                //orderData.Profit = orderData.Profit.Next(orderData.Profit.Money - good.TotalPrice.Money + Total);
            });
        }

        var transaction = _dbContext.Database.BeginTransaction();
        try
        {
            //不需要
            //#region 修改数据-订单总价

            //// var financeId = await _statisticService.ReportMoney(Session,
            ////     Statistic.MoneyColumnEnum.OrderTotal, orderData.OrderTotal, TableFieldEnum.OrderTotal
            ////     , orderData.ID, orderData.OrderTotal.FinancialAffairsID);
            //await _statisticMoney.OrderTotalEdit(orderData);
            //#endregion

            //#region 修改数据-订单获利

            //await _statisticMoney.OrderProfitEdit(orderData);

            //#endregion

            // orderData.OrderTotal = orderData.OrderTotal with { FinancialAffairsID = financeId };

            await _orderService.Update(orderData);
            transaction.Commit();
        }
        catch (Exception e)
        {
            transaction.Rollback();
            Console.WriteLine(e.Message);
            throw new Exception(e.Message);
        }
        return Success();
    }

    /// <summary>
    /// 删除自定义订单商品
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> DelGoods(string goodId, int orderId)
    {
        var orderData = await _orderService.GetOrderInfoWhereFirst(orderId);
        if (orderData == null)
        {
            return Error(message: "查询不到此订单");
        }
        if (!orderData.HasGoods())
        {
            return Error(message: "订单商品不存在");
        }

        // var orderTotalSid = orderData.OrderTotal.FinancialAffairsID;
        // var orderProfitSid = orderData.Profit.FinancialAffairsID;

        //var goods = JsonConvert.DeserializeObject<List<AmazonOrderModel.OrderGood>>(orderData.Goods!)!;
        var num = 0;
        orderData.GoodsScope(goods =>
        {
            var goodid = goods.Where(a => a.OrderItemId == goodId);
            num = goodid.Count();
            if (num == 1)
            {
                var good = goods.FirstOrDefault(a => a.OrderItemId == goodId);
                goods.Remove(good);
                orderData.ProductNum -= good.QuantityOrdered;
                orderData.GoodsMoney -= good.TotalPrice;
                // orderData.OrderTotal = orderData.OrderTotal with { Money = orderData.OrderTotal.Money - good.TotalPrice.Money };
                // orderData.Profit = orderData.Profit with { Money = orderData.Profit.Money - good.TotalPrice.Money };
                orderData.OrderTotal = orderData.OrderTotal.Next(orderData.OrderTotal.Money - good.TotalPrice.Money);
                orderData.Profit = orderData.Profit.Next(orderData.Profit.Money - good.TotalPrice.Money);
            }
        });
        if (num == 0)
        {
            return Error(message: "订单商品不存在");
        }
        else if (num > 1)
        {
            return Error(message: "订单商品重复");
        }
        else if (num == 1)
        {

            var transaction = _dbContext.Database.BeginTransaction();
            try
            {
                #region 修改数据-订单总价统计/跟踪

                // var orderTotalFid = await _statisticService.ReportMoney(Session,
                //     Statistic.MoneyColumnEnum.OrderTotal, orderData.OrderTotal, TableFieldEnum.OrderTotal
                //     , orderData.ID, orderTotalSid);
                await _statisticMoney.OrderTotalEdit(orderData);
                #endregion

                // orderData.OrderTotal = orderData.OrderTotal with { FinancialAffairsID = orderTotalFid };

                #region 修改数据-订单获利跟踪

                // var profitFid =
                //     await _statisticService.ReportFinancialRecord(orderData.Profit, TableFieldEnum.OrderProfit,
                //         orderData.ID, orderProfitSid);
                await _statisticMoney.OrderProfitEdit(orderData);
                #endregion

                // orderData.Profit = orderData.Profit with { FinancialAffairsID = profitFid };

                await _orderService.Update(orderData);
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                Console.WriteLine(e.Message);
                throw new Exception(e.Message);
            }

            return Success();
        }
        else
        {
            return Error();
        }
    }

    /// <summary>
    /// 转换状态
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetProgress(/*progress*/)
    {
        //$progress = (int)$progress;
        //$progress == 8 && $progress = 7;
        //return AmazonOrderModel::ORDER_PROGRESS[$progress];
        return await Task.FromResult(Success());
    }

    /// <summary>
    /// 修改订单进度
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> SetOrderProgress(/*progress*/)
    {
        //Request.TryGetValue("id", out var id);
        Request.TryGetValue("progress_index", out var progress_index);
        if (!Request.TryGetValue("id", out var id) || Convert.ToInt32(progress_index) < 0)
        {
            return Error("参数丢失");
        }
        Request.TryGetValue("progress_name", out var order_state_text);
        var action = $"将订单进度更改为{order_state_text}";
        if (await SetOrderProgressBegin(Convert.ToInt32(id), System.Enum.Parse<Enums.Orders.States>(progress_index), action, order_state_text))
        {
            return Success();
        }
        return Error("修改进度失败");
    }

    /// <summary>
    /// 修改订单状态以及添加日志事务处理
    /// </summary>
    /// <returns></returns>
    /// 
    public async Task<bool> SetOrderProgressBegin(int id, Enums.Orders.States progress_index, string action, string order_state_text)
    {
        // 启动事务
        using var transaction = _dbContext.Database.BeginTransaction();
        try
        {
            var orderInfo = await _orderService.GetOrderInfoFirst(Convert.ToInt32(id));
            //$biter = new Progress($orderInfo->progress_original);
            //$biter->setProgress($progress_index);
            //AmazonOrderModel::query()->where('id', $id)->update(['progress' => $biter->getFlags()]);
            //增加订单日志
            if (orderInfo != null)
            {
                await _orderService.AddLog(orderInfo.ID, progress_index, action, order_state_text);
            }
            // 提交事务
            transaction.Commit();
        }
        catch (Exception /*e*/)
        {
            // 回滚事务
            transaction.Rollback();
            return false;
        }
        return true;
    }

    /// <summary>
    /// 修改收货人信息
    /// </summary>
    /// <param name="rq"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    [HttpPost]
    public async Task<ResultStruct> EditInformation(AddressVM rq)
    {
        var address = await _orderService.GetAddressById(rq.Id);

        //处理国家
        var currentNation = _nationCache.Get(Convert.ToInt32(rq.NationId));
        if (currentNation == null) return Error($"未找到Id为{rq.NationId}");

        address.Name = rq.Name;
        address.Phone = rq.Phone;
        address.Email = rq.Email;
        address.Address1 = rq.Address1;
        address.Address2 = rq.Address2;
        address.Address3 = rq.Address3;
        address.Zip = rq.Zip;
        address.NationId = rq.NationId;
        address.NationShort = currentNation.Short;
        address.Nation = currentNation.Name;
        address.Province = rq.Province;
        address.District = rq.District;
        address.City = rq.City;

        //    return Success("修改收货信息成功");
        if (await _orderService.EditAddress(address))
            return Success("修改收货信息成功");
        return Error("修改失败");
    }

    /// <summary>
    /// 添加内部备注
    /// </summary>
    /// <param name="rq"></param>
    /// <returns></returns>
    public async Task<ResultStruct> DoAddRemark(RemarkVM rq)
    {
        if (rq.Remark.IsNullOrWhiteSpace())
        {
            return Error("请填写备注信息!");
        }

        if (rq.Type == 2)
        {
            rq.Remark = $"【物流】{rq.Remark}";
        }
        else if (rq.Type == 3)
        {
            rq.Remark = $"【采购】{rq.Remark}";
        }
        else
        {
            rq.Remark = $"【订单】{rq.Remark}";
        }
        try
        {
            var orderInfo = await _orderService.GetOrderInfoFirst(rq.Id);

            var data = new OrderRemarkModel
            {
                OrderId = rq.Id,
                Value = rq.Remark,
                UserID = Session.GetUserID(),
                GroupID = Session.GetGroupID(),
                CompanyID = Session.GetCompanyID(),
                OEMID = Session.GetOEMID(),
                UserName = Session.GetUserName(),
                Truename = Session.GetTrueName(),
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
            };

            await _orderService.DoAddRemarkCreate(data);

            return Success();
        }
        catch (Exception ex)
        {
            return Error(message: ex.Message);
        }
    }

    // 前端注释掉了
    public async Task<ResultStruct> DoEditFee()
    {
        //if (!$this->rules()->hasSupplierAdd()) {
        //    return error('无权访问');
        //}
        //var res = await _service.Add(vm);
        //$input = $request->validated();
        return await Task.FromResult(Success());
    }

    // DoEditFee 调用
    public async Task<ResultStruct> DoEditFeeBegin()
    {
        return await Task.FromResult(Success());
    }

    // 前端注释掉了
    public async Task<ResultStruct> DoEditRefund()
    {
        return await Task.FromResult(Success());
    }

    // DoEditRefund 调用
    public async Task<ResultStruct> DoEditRefundBegin()
    {
        return await Task.FromResult(Success());
    }

    /// <summary>
    /// 合并订单 - 添加到待合并订单列表
    /// </summary>
    /// <param name="OrderId"></param>
    /// <returns></returns>
    [HttpPost]
    [Route("{id}")]
    public async Task<ResultStruct> AddAmazonMerge(int id)
    {
        //if (!$this->rules()->hasAddMergeList()) {
        //    return error('无权访问');
        //}

        var info = await _orderService.GetOrderInfoFirst(id);
        if (info == null) return Error(message: "订单不存在");

        if (info.MergeId == 0) return Error(message: "订单已在合并订单中");

        info.MergeId = 0;
        info.OperateUserId = UserID;
        _dbContext.Update(info);
        if (_dbContext.SaveChanges() > 0)
        {
            return Success("添加成功！");
        }
        return Error("添加失败！");
    }

    /// <summary>
    /// 移除待合并订单列表
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpPost]
    [Route("{id}")]
    public async Task<ResultStruct> RemovePending(int id)
    {
        //if (!($this->rules()->hasClearConsolidatedOrder() || $this->rules()->hasRemoveMergePending())) {
        //    return error('无权访问');
        //}

        //TODO:移除待合并订单列表时把已经设置的主订单状态取消掉，防止不同店铺订单同时出现在待合并订单列表时，会出现多个主订单的BUG(2.0BUG)

        if (id == -1)//批量移除
        {

            await _orderService.RemoveOrderPending(null);
        }
        else//单个移除
        {
            var info = await _orderService.GetOrderInfoFirst(id);
            if (info == null || info.OperateUserId != UserID)
            {
                return Error("订单不存在！");
            }
            await _orderService.RemoveOrderPending(id);
        }
        return Success("移除成功！");
    }

    /// <summary>
    /// 设置主订单
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet]
    [Route("{id}")]
    public async Task<ResultStruct> SetMainOrder(int id)
    {
        //if (!$this->rules()->hasSetMainOrder()) {
        //    return error('无权访问');
        //}
        var info = await _orderService.GetOrderInfoFirst(id);
        if (info is null   /* || info.OperateUserId == 0 /*$this->getSessionObj()->user_id*/)     //==0
        {
            return Error("订单不存在！");
        }
        if (await SetMainOrderBegin(id))
        {
            return Success("设置主订单成功");
        }
        return Error("设置主订单失败");
    }

    /// <summary>
    /// 设置主订单事务处理   erp3.0 有问题???
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<bool> SetMainOrderBegin(int id)
    {
        var Res = await _orderService.IsMainOne(id);
        return Res.State;
    }

    private record DoMergeC(int Type, int id);
    /// <summary>
    /// 合并订单
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    [HttpGet]
    [Route("{type}")]
    public async Task<ResultStruct> DoMerge(int Type, [FromServices] DisorderService amazonOrderService)
    {
        try
        {
            var list = await _orderService.GetOrderInfoWhere();
            if (list == null)
            {
                return Error("暂无待合并订单！");
            }
            List<DoMergeOrderInfo> orderInfo = new();
            foreach (var value in list)
            {
                DoMergeOrderInfo order_info = new();
                order_info.order_id = value.ID;
                order_info.is_main = (int)value.IsMain;

                orderInfo.Add(order_info);
            }
            var insertData = new OrderMergeModel
            {
                UserID = UserID,
                CompanyID = CompanyID,
                GroupID = GroupID,
                OEMID = OEMID,
                OperateCompanyId = CompanyID,
                OperateUserId = UserID,
                Type = (MergeTypes)Type,
                WaybillCompanyId = CompanyID,
                OrderInfo = JsonConvert.SerializeObject(orderInfo)
            };
            await _MergeService.DoMergeBegin(insertData, list);

            //1.判断用户权限，自动上报
            var user = await _userManager.GetUserAsync(User);
            var company = user.Company;
            if (company.DistributionOrder == Models.DB.Users.Company.DistributionOrders.TRUE)
            {
                await amazonOrderService.ToReportMergeOrder(insertData.ID, user.CompanyID, user.Company.ReportId);
            }
            return Success("合并订单成功");
        }
        catch
        {
            return Error("合并订单失败！");
        }
    }

    /// <summary>
    /// 获取待发货订单 ship
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> DeliverList(DeliverVM deliverVM)
    {
        //Dictionary<int, object> orderProgres = OrderProgressData;
        var ship = await _orderService.Shipped(deliverVM);
        var data = ship.Transform(a => new
        {
            a.ID,
            a.OrderNo,
            a.Url,
            a.Receiver,
            a.ProductNum,
            a.CreateTime,
            a.LatestShipTime
        });
        var orderProgress = OrderState.OrderProgressData;
        return Success(new { data, sortBy = Enum<SortBy>.ToDictionaryForUIByDescription(), orderProgress });
    }

    /// <summary>
    /// 查看收件人地址
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<ResultStruct> Consignee(int id)
    {
        var info = await _orderService.FindAddress(id);

        return info is not null ? Success(info) : Error();
    }

    /// <summary>
    /// 订单采购商品匹配产品库
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    private async Task<int> CheckPro(string seller_sku = "", int type = 1)
    {
        if (seller_sku == "")
        {
            return await Task.FromResult(0);
        }

        //if (strpos($seller_sku, '-') !== false)
        //{
        //    $id = decodeSku($seller_sku);
        //    if ($id === false) {
        //        return 0;
        //    }
        //    //拿到id后到产品表去查询记录，如果查询不到说明产品被删或者此产品不是3.0产品
        //    //2.0Sku解析后无法和3.0进行有效区分,会产生一个问题,2.0的产品解析到了3.0的产品，却对不上
        //    $info = $this->ProductService->getProductWhereFirst(["user_id", "company_id"], $id);
        //    if ($info) {
        //        if ($info["user_id"] == $this->getSessionObj()->user_id) {
        //            return $id;
        //        }
        //        if ($this->getSessionObj()->concierge == 1 && $info["company_id"] == $this->sessionData->getCompanyId()) {
        //            return $id;
        //        }
        //    }
        //    return 0;
        //}
        //else
        //{
        //    return 0;
        //}
        return await Task.FromResult(0);
    }

    /// <summary>
    /// 修改发货数
    /// </summary>
    /// <param name="value"></param>
    /// <param name="good_id"></param>
    /// <param name="order_id"></param>
    /// <returns></returns>
    public async Task<ResultStruct> FixNumber(int value, string good_id, int order_id)
    {
        /*
            Request:
            value: 10
            good_id: B099N5TK4Z
            order_id: 538
        */

        //if (!$this->rules()->hasEditPendingShipment()) {
        //    return error('无权访问');
        //}

        //$currency_unit = $this->getSessionObj()->getUnitConfig();
        var order = await _orderService.GetOrderInfoWhereFirst(order_id);
        if (order == null)
        {
            return Error("没有查到此订单");
        }
        if (order.HasGoods())
        {
            return Error("没有查到此订单产品");
        }
        //var goods = JsonConvert.DeserializeObject<List<object>>(order.Goods);
        var goods = order.GoodsScope();
        foreach (var good in goods)
        {
            //$good['price'] = FromConversion($good['price'], $currency_unit);
            //$good['total'] = FromConversion($good['total'], $currency_unit);
            //if ($good["good_id"] == $data['good_id']) {
            //    $good["send_num"] = (int)$data["value"];
            //}
            //good["Price"] = 
        }
        var goodsStr = JsonConvert.SerializeObject(goods);
        //var resu = await AmazonOrderService.Update(order_id, goodsStr);
        //if (resu == 0)
        //{
        //    return Error("失败");
        //}
        //else
        //{
        //    return Success("成功");
        //}
        return Success("成功");
    }


    private async Task<OrderRule> GetorderReport()
    {
        OrderRule rule = new OrderRule();
        var user = await _userManager.GetUserAsync(User);
        var company = user.Company;
        if (company is not null)
        {
            switch (company.DistributionPurchase)
            {
                case Models.DB.Users.Company.DistributionPurchases.TRUE:
                    rule.purchaseBack = false;
                    rule.purchaseReport = true;
                    break;
                case Models.DB.Users.Company.DistributionPurchases.PORTION:
                    rule.purchaseBack = true;
                    rule.purchaseReport = true;
                    break;
                case Models.DB.Users.Company.DistributionPurchases.FALSE:
                    rule.purchaseBack = true;
                    rule.purchaseReport = false;
                    break;
            }

            switch (company.DistributionWaybill)
            {
                case Models.DB.Users.Company.DistributionWaybills.TRUE:
                    rule.waybillBack = false;
                    rule.waybillReport = true;
                    break;
                case Models.DB.Users.Company.DistributionWaybills.PORTION:
                    rule.waybillBack = true;
                    rule.waybillReport = true;
                    break;
                case Models.DB.Users.Company.DistributionWaybills.FALSE:
                    rule.waybillBack = true;
                    rule.waybillReport = false;
                    break;
            }
            switch (company.DistributionOrder)
            {
                case Models.DB.Users.Company.DistributionOrders.TRUE:
                    rule.orderBack = false;
                    rule.orderReport = true;
                    break;
                case Models.DB.Users.Company.DistributionOrders.PORTION:
                    rule.orderBack = true;
                    rule.orderReport = true;
                    break;
                case Models.DB.Users.Company.DistributionOrders.FALSE:
                    rule.orderBack = true;
                    rule.orderReport = false;
                    break;
            }
        }
        return rule;
    }

    /// <summary>
    /// 订单初始化获取权限、参数
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetListInit()
    {
        //var user = await _userManager.GetUserAsync(User);
        //var company = user.Company;
        //var purchaseBack = false;
        //var purchaseReport = false;

        //var waybillBack = false;
        //var waybillReport = false;

        //var orderBack = false;
        //var orderReport = false;

        //if (company is not null)
        //{
        //    switch (company.DistributionPurchase)
        //    {
        //        case Models.DB.Users.Company.DistributionPurchases.TRUE:
        //            purchaseBack = false;
        //            purchaseReport = true;
        //            break;
        //        case Models.DB.Users.Company.DistributionPurchases.PORTION:
        //            purchaseBack = true;
        //            purchaseReport = true;
        //            break;
        //        case Models.DB.Users.Company.DistributionPurchases.FALSE:
        //            purchaseBack = true;
        //            purchaseReport = false;
        //            break;
        //    }

        //    switch (company.DistributionWaybill)
        //    {
        //        case Models.DB.Users.Company.DistributionWaybills.TRUE:
        //            waybillBack = false;
        //            waybillReport = true;
        //            break;
        //        case Models.DB.Users.Company.DistributionWaybills.PORTION:
        //            waybillBack = true;
        //            waybillReport = true;
        //            break;
        //        case Models.DB.Users.Company.DistributionWaybills.FALSE:
        //            waybillBack = true;
        //            waybillReport = false;
        //            break;
        //    }
        //    switch (company.DistributionOrder)
        //    {
        //        case Models.DB.Users.Company.DistributionOrders.TRUE:
        //            orderBack = false;
        //            orderReport = true;
        //            break;
        //        case Models.DB.Users.Company.DistributionOrders.PORTION:
        //            orderBack = true;
        //            orderReport = true;
        //            break;
        //        case Models.DB.Users.Company.DistributionOrders.FALSE:
        //            orderBack = true;
        //            orderReport = false;
        //            break;
        //    }
        //}
        //todo ChangeOrderState批量修改订单状态配置项

        var orderRule = await GetorderReport();
        //公司管理全部权限
        var rule = new
        {
            ImportEmail = true,
            AmazonGood = true,
            AmazonDetail = true,
            purchaseBack = orderRule.purchaseBack,
            purchaseReport = orderRule.purchaseReport,
            waybillBack = orderRule.waybillBack,
            waybillReport = orderRule.waybillReport,
            orderReport = orderRule.orderReport,
            orderBack = orderRule.orderBack,
            AddSelfOrder = true,
            PullTheOrder = true,
            AddMergeList = true,
            EditOrderFee = true,
            OrderRefund = true,
            OperateOrderMessage = true,
            EditOrderProgress = true,
            PrepareForShipment = true,
            EditPendingShipment = true,
            AddPurchase = true,
            EditPurchase = true,
            AddConsignmentNote = true,
            EditReceiptInformation = true,
            AddRemark = true,
            PurchaseConfirm = true,
            ChangeOrderProgress = true,
            //批量绑定
            batchPurchase = true,
            //我的店铺 添加店铺
            MyStoreAdd = User.Has(Enums.Rule.Config.Store.MYSTORE_ADD),
            OrderListShowProfit = true,
            RemoveMerge = true,
            isDistribution = Session.GetIsDistribution()
        };
        List<Models.Stores.StoreRegion> storeList;

        storeList = await _storeService.CurrentUserGetStores();

        var orderState = GetSupportsOrderStates();
        var orderProgress = OrderState.OrderProgressData;

        var continents = storeList.Where(s => s.Continent > 0).GroupBy(s => s.Continent).Select(g => new
        {
            label = Extensions.EnumExtension.GetDescription(g.Key),
            value = g.Key,
            children = g.Select(gg => new
            {
                label = gg.Name,
                value = gg.ID,
            })
        });

        var amazonNation = _platformDataCache.List(new List<Platforms>() { Platforms.AMAZON, Platforms.AMAZONSP })
            .Where(x => x.GetAmazonData()!.Unit.IsNotWhiteSpace())
           
            .GroupBy(s => s.GetAmazonData()!.Continent).Select(g => new
            {
                label = Extensions.EnumExtension.GetDescription(g.Key),
                value = g.Key,
                children = g.Select(gg => new
                {
                    label = gg.Nation.Name,
                    value = gg.GetAmazonData().Marketplace,
                })
            });

        var list = new
        {
            rule,
            orderState,
            orderProgress,
            sortBy = Enum<SortBy>.ToDictionaryForUIByDescription(),

            continents,
            amazonNation,
            store = storeList.Select(s => new
            {
                id = s.ID,
                name = s.Name,
                state = s.State,
                continents_id = s.Continent
            }),
            validStore = storeList.Where(s => s.State == Models.Abstract.BaseModel.StateEnum.Open).Select(s => new
            {
                id = s.ID,
                name = s.Name,
            }),
            is_has_within_one_day_order = (await _orderService.GetOrderListPage(new OrderIndexListQueryVM { LatestShipTimeFilter = "WithInOne" })).Total,
            is_has_within_three_day_order = (await _orderService.GetOrderListPage(new OrderIndexListQueryVM { LatestShipTimeFilter = "WithInThree" })).Total,
            is_has_exceed_order = (await _orderService.GetOrderListPage(new OrderIndexListQueryVM { LatestShipTimeFilter = "Exceed" })).Total,
        };
        return await Task.FromResult(Success(list, "成功"));
    }

    /// <summary>
    /// 获取订单页权限
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetRule()
    {
        return await Task.FromResult(Success("11"));
    }

    /************************ 财务->亚马逊订单统计***************************/

    /// <summary>
    /// 获取亚马逊订单统计
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> StatisticsIndex()
    {
        return await Task.FromResult(Success());
    }
    public async Task<ResultStruct> EditLoss(EditLossVM rq)
    {
        if (rq.Id == 0)
        {
            return Error("ID不存在");
        }
        var orderInfo = await _orderService.GetOrderInfoWhereFirst(rq.Id);
        if (orderInfo == null)
        {
            return Error("订单不存在");
        }
        MoneyRecordFinancialAffairs lossMoney = new MoneyMeta(rq.Unit, rq.Loss);
        var lossMeta = lossMoney.Money.To(orderInfo.OrderTotal.Raw.CurrencyCode);
        var differ = orderInfo.Loss - lossMoney;
        // 转换为基准货币 
        if (orderInfo.Loss.Raw.CurrencyCode == "")
        {
            orderInfo.Loss = lossMoney;
        }
        else
        {
            orderInfo.Loss = orderInfo.Loss.Next(lossMoney);
        }
        Money amountProfit = orderInfo.OrderTotal.Money - orderInfo.Fee.Money - orderInfo.Refund.Money - orderInfo.ShippingMoney.Money - orderInfo.Loss.Money - orderInfo.PurchaseFee.Money;
        //decimal p = Math.Round(amountProfit.To(orderInfo.OrderTotal.Raw.CurrencyCode), 2);
        decimal p = Math.Round(amountProfit / orderInfo.OrderTotal.Raw.Rate, 2);

        orderInfo.Profit = orderInfo.Profit.Next(orderInfo.OrderTotal.Raw.CurrencyCode, p);
        var transaction = _dbContext.Database.BeginTransaction();
        try
        {
            #region 修改数据-订单损耗统计/跟踪

            await _statisticMoney.OrderLossEdit(orderInfo);

            #endregion


            #region 修改数据-订单获利跟踪

            await _statisticMoney.OrderProfitEdit(orderInfo);
            #endregion

            //todo 添加账单 需要审核 自用

            await _orderService.Update(orderInfo);
            transaction.Commit();
        }
        catch (Exception e)
        {
            transaction.Rollback();
            Console.WriteLine(e.Message);
            throw new Exception(e.Message);
        }
        return Success();
        //return Success();



    }
    /******************************** 权限*****************************/

    /// <summary>
    /// 获取亚马逊订单统计
    /// </summary>
    /// <returns></returns>
    private async Task<ResultStruct> Rules()
    {
        return await Task.FromResult(Success());
    }

    /// <summary>
    /// 获取订单备注
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetRemark(int id)
    {
        //$list = AmazonOrderRemark::query()->where('order_id',$id)->select(['remark', 'user_name'])->get();
        return await Task.FromResult(Success());
    }

    /// <summary>
    /// 批量修改订单状态
    /// </summary>
    /// <param name="rq"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> ChangeState(ChangeStateVM rq)
    {
        if (rq.OrderIds.Count == 0)
        {
            return Error("缺少参数: orderIds");
        }
        var state = await _orderService.BatchChangeState(rq);
        if (state == true)
        {
            return Success();

        }
        else
        {
            return Error();
        }
    }

    /// <summary>
    /// 同步订单信息
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> SyncLogisticInfo(DeliveryGoodVM deliveryGood, [FromServices] ILogger<OrderController> logger, [FromServices] IQueue queue, [FromServices] IListener<QueueTaskCompleted> ilistener)
    {
        if (deliveryGood.CarrierName.IsNullOrWhiteSpace())
        {
            return Error("请选择承运人");
        }
        var id = Session.GetSyncOrder(deliveryGood.ID.ToString());
        var listener = (ilistener as TaskCompletedListener)!;
        if (!listener.IsCompleted(id))
        {
            return Warning($"当前正在同步订单操作，请稍后重试", "Pulling");
        }
        else
        {
            SyncToAmazon syncToAmazon = new SyncToAmazon();
            syncToAmazon.IsSync = true;
            syncToAmazon.CarrierName = deliveryGood.CarrierName;
            syncToAmazon.ShippingMethod = deliveryGood.ShippingMethod;
            var result = await _orderService.SyncAmazon(deliveryGood.ID, syncToAmazon, deliveryGood.TrackingNumber, "", deliveryGood.HandWrite);
            if (!string.IsNullOrWhiteSpace(result))
            {
                return Error(result);
            }

            await _orderService.AddSyncLogisticInfoLog(deliveryGood);
            return Success("运单上报中，稍后更新");
        }
    }

    [HttpPost]
    public async Task<ResultStruct> LoadLastLogisticLog()
    {
        var result = await _orderService.LoadLastLogisticLog();
        return Success(result);
    }

    /// <summary>
    /// 上传发票
    /// </summary>
    /// <param name="ordervm"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> UploadInvoice(OrderInfoVM ordervm)
    {
        var order = await _orderService.GetOrderByIdAsync(ordervm.ID);
        if (order is not null)
        {
            var store = await _storeService.GetInfoById(order.StoreId);
            if (store.IsNull())
            {
                return Error("店铺不存在");
            }
            if (store.Platform != Enums.Platforms.AMAZON)
            {
                return Error("这不是亚马逊的订单");
            }
            //await _orderService.UploadInvoice(order, store);
            return Error("该功能暂未实现！");
        }
        else
        {
            return Error("订单没找到");
        }
    }

    /// <summary>
    /// 亚马逊-更新订单信息
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> UpdateOrderInfo(OrderInfoVM orderInfo, [FromServices] ILogger<OrderController> logger, [FromServices] IQueue queue, [FromServices] IListener<QueueTaskCompleted> ilistener)
    {
        var id = Session.GetUpdateOrder(orderInfo.OrderNo);
        var listener = (ilistener as TaskCompletedListener)!;
        if (!listener.IsCompleted(id))
        {
            return Warning($"当前正在同步订单操作，请稍后重试", "Pulling");
        }
        else
        {
            var userid = Session.GetUserID();

            var user = await _userService.GetCurrentUser(userid);
            if (user == null)
            {
                return Error($"未找到用户信息");
            }
            var order = await _orderService.GetOrderByIdAsync(orderInfo.ID);
            if (order is null)
            {
                return Error($"未找到订单信息");
            }
            var store = await _storeService.GetInfoById(order.StoreId);
            if (store.IsNull())
            {
                return Error($"未找到店铺");
            }
            else
            {
                if (store.Platform != Platforms.AMAZONSP)
                {
                    return Error($"暂未实现该平台{store.Platform.GetDescription()}");
                }
            }

            var guid = Guid.NewGuid();
            Session.SetUpdateOrder(orderInfo.OrderNo, guid);
            listener.AddTask(guid);
            queue.QueueTask(async () =>
            {
                try
                {
                    await _orderHostService.UpdateOrderInfo(order, store.ID);
                }
                catch (Exception e)
                {
                    logger.LogError($"UpdateOrderInfo error:{e.Message}");
                }
                listener.RunningTasks[guid] = true;
            });
            return Success("更新订单中，稍后更新");
        }
    }

    /// <summary>
    /// 获取订单状态
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public ResultStruct GetOrderStatus()
    {
        var result = OrderState.SearchState.ToList();
        return Success(result);
    }

    /// <summary>
    /// 订单关联的自定义标记(批量)
    /// </summary>
    /// <param name="vm"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> BatchSetOrderCustomMark(SetOrderCustomMarkVM vm)
    {
        await _orderService.SetOrderCustomMark(vm.OrderIds, vm.CustomMark);
        return Success();
    }

    /// <summary>
    /// 拉取订单获取用户绑定店铺 - 选择器
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetUserStores()
    {
        List<Models.Stores.StoreRegion> stores = new();
        var user = await _userManager.GetUserAsync(User);
        stores = await _storeService.CurrentUserGetStores();
        var data = stores
            .Where(s => s.IsVerify == Models.Stores.StoreRegion.IsVerifyEnum.Certified)
            .Select(s => new
            {
                value = s.ID,
                label = $"{Extensions.EnumExtension.GetDescription(s.Platform)}-{s.Name}"
            });
        return Success(data);
    }

    public class ManualPullOrderVM
    {
        public void SetTime(List<DateTime> lst)
        {
            _dateRange = lst;
            if (_dateRange is not null && _dateRange.Count == 2)
            {
                BeginTime = _dateRange[0].IsNull() ? DateTime.MinValue : _dateRange[0].ToLocalTime();
                EndTime = _dateRange[1].IsNull() ? DateTime.MinValue : _dateRange[1].ToLocalTime();
            }
        }
        private List<DateTime>? _dateRange;
        public List<DateTime>? DateRange { get => _dateRange; set => SetTime(value); }
        public DateTime BeginTime { get; set; } = DateTime.MinValue;

        public DateTime EndTime { get; set; } = DateTime.MinValue;
        public bool LastOrderPullTime { get; set; }//自动拉取最后一个有效订单时间
        public bool CurrentTime { get; set; }//当前时间
        public List<int?> StoreIds { get; set; } = default!;
    }

    ///// <summary>
    ///// 上传发票
    ///// </summary>
    ///// <returns></returns>
    //[HttpPost]
    //public async Task<ResultStruct> UploadInvoice()
    //{
    //    // 上传发票 api
    //    return Success();
    //}

    /// <summary>
    /// 手动拉取订单
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> ManualPullOrder(ManualPullOrderVM vm, [FromServices] ILogger<OrderController> logger, [FromServices] IQueue queue, [FromServices] IListener<QueueTaskCompleted> ilistener)
    {
        DateTime begin, end = DateTime.Now;
        var id = Session.GetOrderPulling();
        var listener = (ilistener as TaskCompletedListener)!;
        if (!listener.IsCompleted(id))
        {
            var metric = queue.GetMetrics();
            return Warning($"当前正在拉取订单，请稍后重试", "Pulling");
        }
        else
        {
            //但是有个异常处理，就是开始时间亚马逊最早应该是不能超过半年的，不然之前的拉不到或者可能返回错误，截止时间如果超过当前时间应该也会
            //结束时间，要取当前时间，晚5min，否则拉取太快，也会出错
            //时间逻辑判断
            if (vm.StoreIds is null || vm.StoreIds.Count == 0) return Warning("请选择需要拉取的店铺");

            //手动拉取时间
            if (!vm.LastOrderPullTime && !vm.CurrentTime)
            {
                if (vm.BeginTime == DateTime.MinValue) return Warning("请选择开始拉取时间");
                if (vm.EndTime == DateTime.MinValue) return Warning("请选择拉取时间");
                begin = vm.BeginTime;
                if (vm.EndTime > DateTime.Now)
                {
                    end = DateTime.Now.AddMinutes(-5);
                }
                else
                {
                    end = vm.EndTime;
                }
            }
            else  //选择系统拉取时间
            {
                if (vm.LastOrderPullTime) //最后一笔订单时间
                {
                    begin = DateTime.MinValue;

                    if (vm.CurrentTime)
                    {
                        end = DateTime.Now.AddMinutes(-5);
                    }
                    else
                    {
                        if (vm.EndTime != DateTime.MinValue)
                        {
                            if (vm.EndTime > DateTime.Now) end = DateTime.Now.AddMinutes(-5);
                            else end = vm.EndTime;
                        }
                        else
                        {
                            return Warning("请选择结束拉取时间");
                        }
                    }
                }
                else if (vm.CurrentTime)
                {
                    end = DateTime.Now.AddMinutes(-5);

                    if (vm.LastOrderPullTime)
                    {
                        begin = DateTime.MinValue;
                    }
                    else
                    {
                        if (vm.BeginTime != DateTime.MinValue)
                        {
                            if (vm.BeginTime > DateTime.Now)
                            {
                                begin = DateTime.Now.AddMinutes(-5);
                            }
                            else begin = vm.BeginTime;
                        }
                        else
                        {
                            return Warning("请选择开始拉取时间");
                        }
                    }
                }
                else
                {
                    return Warning("请选择拉取时间");
                }
            }
            var stores = await _storeService.GetStoresByIds(vm.StoreIds);
            var noStores = stores.Where(x => x.Platform != Enums.Platforms.AMAZONSP);
            if (noStores.Any())
            {
                StringBuilder platNames = new StringBuilder();
                noStores.ForEach(x => platNames.Append($"{x.Platform.GetDescription()} "));
                return Warning($"暂未实现{platNames.ToString()}平台");
            }

            // 1 logger被提前销毁
            // 2 Task HTTP 被销毁
            // 3 User已经退出，Session 不存在了
            // 4 是否允许在HTTP的生命周期外操作了Seeion ====>不能再操作session了
            var guid = Guid.NewGuid();
            Session.SetOrderPulling(guid);
            listener.AddTask(guid);
            queue.QueueTask(async () =>
            {
                try
                {
                    var ids = stores.Select(x => x.ID).Cast<int?>().ToList();
                    await _orderHostService.PullOrder(ids, begin, end);
                }
                catch (Exception e)
                {
                    logger.LogError($"ManualPullOrder  error:{e.Message}");
                }
                listener.RunningTasks[guid] = true;
            });
            return Success("开始拉取，请稍后查看订单");
        }
    }

    [HttpPost]
    public async Task<ResultStruct> getOrderExportNum(OrderIndexListQueryVM rq)
    {
        return Success(await _orderService.getOrderExportNum(rq));
    }

    [HttpPost]
    public async Task<ResultStruct> DownOrderFileStart(OrderChooseFieldParam orderChoose, [FromServices] ITaskQueue<ExportQueueItem> queue)
    {
        if (orderChoose.OrderIDs is not { Count: > 0 })
            return Error("导出订单不能为空");

        var user = await _userManager.GetUserAsync(User);
        var taskId = await _dbContext.ExportTasks
            .Where(t => t.Type == ExportTask.TaskType.Orders && t.State < ExportTask.TaskState.Downloaded && t.UserID == user.Id).Select(t => t.ID)
            .FirstOrDefaultAsync();
        if (taskId != 0)
            return Success(new { ExistsTaskId = taskId }, "已有导出任务正在进行中");

        var newTask = new ExportTask()
        {
            OrderIds = JsonConvert.SerializeObject(orderChoose.OrderIDs),
            Params = JsonConvert.SerializeObject(orderChoose),
            Type = ExportTask.TaskType.Orders,
            UserID = user.Id,
            GroupID = user.GroupID,
            CompanyID = user.CompanyID,
            OEMID = user.OEMID,
        };
        _dbContext.Add(newTask);
        await _dbContext.SaveChangesAsync();
        queue.Enqueue(new ExportQueueItem(newTask));
        return Success(new { TaskId = newTask.ID, Name = newTask.GenerateExportFileName() }, "新建导出任务成功");
    }

    public static string FileNameReplace(string Name)
    {
        char[] invalidFileNameChars = Path.GetInvalidFileNameChars();
        string[] array = new string[7] { "CON", "AUX", "PRN", "COM", "LPT", ".", ".." };
        char[] array2 = invalidFileNameChars;
        foreach (char c in array2)
        {
            if (Name.Contains(Convert.ToString(c)))
            {
                Name = Name.Replace(Convert.ToString(c), "_");
            }
        }
        string[] array3 = array;
        foreach (string value in array3)
        {
            if (Name.Equals(value))
            {
                Name = "";
                break;
            }
        }
        if (Name.Length == 0)
        {
            return DataTimeFormat.Fromat_yyyyMMddHHmmssffff();
        }

        return Name;
    }


    public class BatchEditOrderProgressVM
    {
        public List<int> Ids { get; set; } = default!;
        public List<ProgressValue> OrderProgress { get; set; } = default!;
    }
    /// <summary>
    /// 批量修改订单进度
    /// </summary>
    /// <param name="ids"></param>
    /// <param name="list"></param>
    /// <returns></returns>
    public async Task<ResultStruct> BatchEditOrderProgress(BatchEditOrderProgressVM vm)
    {
        var orders = await _orderService.GetOrdersByIdsAsync(vm.Ids);
        var transcation = _dbContext.Database.BeginTransaction();
        try
        {
            foreach (var order in orders)
            {
                await _orderService.BatchEditOrderProgress(order, vm.OrderProgress);
            }
            transcation.Commit();
        }
        catch (Exception ex)
        {
            transcation.Rollback();
            return Error(ex.Message);
        }
        return Success();
    }

    public async Task<ResultStruct> OrderProductSalesRanking(OrderProductSalesRankingDto request)
    {
        var res = await _orderService.OrderProductSalesRanking(request);
        return Success(res);
    }

    public async Task<ResultStruct> ImportOrderExcel([FromForm] IFormFile file, [FromForm] ExcelTemplateTypes type)
    {
        var transaction = _dbContext.Database.BeginTransaction();
        try
        {
            var msg = await _excelFactory.Create(type).CheckAndCreateSheet(file, out var sheet).ImportExcel(sheet, UserID, CompanyID, GroupID, OEMID);
            if (msg == "")
            {
                transaction.Commit();
                return Success("导入成功");
            }
            else
            {
                transaction.Rollback();
                return Error(msg);
            }

        }
        catch (Exception ex)
        {
            transaction.Rollback();
            return Error(ex.Message + ex.StackTrace);
        }
    }

    /// <summary>
    /// 将订单分配给用户
    /// </summary>
    /// <param name="orderId"></param>
    /// <param name="userIds"></param>
    /// <returns></returns>
    [HttpPost("{orderId:int}/to")]
    public async Task<ResultStruct> DistributeOrder(int orderId, [FromBody] int[] userIds)
    {
        var filteredIds = userIds.Distinct().ToArray();
        var existIds = await _dbContext.OrderDistributedLinks.Where(l =>
                l.SourceType == OrderDistributedLink.SourceIdType.OrderId && l.SourceId == orderId &&
                l.DistributedType == OrderDistributedLink.DistributedIdType.UserId)
            .Select(l => l.DistributedId)
            .ToArrayAsync();
        var needToAddIds = filteredIds.Except(existIds).ToArray();
        var needToRemoveIds = existIds.Except(filteredIds).ToArray();

        _ = needToAddIds.Select(id => _dbContext.Add(new OrderDistributedLink()
        {
            UserID = UserID,
            GroupID = GroupID,
            CompanyID = CompanyID,
            OEMID = OEMID,
            SourceType = OrderDistributedLink.SourceIdType.OrderId,
            SourceId = orderId,
            DistributedType = OrderDistributedLink.DistributedIdType.UserId,
            DistributedId = id,
        })).ToArray();

        await _dbContext.OrderDistributedLinks.Where(l =>
            l.SourceType == OrderDistributedLink.SourceIdType.OrderId && l.SourceId == orderId &&
            l.DistributedType == OrderDistributedLink.DistributedIdType.UserId &&
            needToRemoveIds.Contains(l.DistributedId)).DeleteAsync();

        await _dbContext.SaveChangesAsync();

        return Success();
    }

    public record DistributedUser(int DistributedId, string DistributedUserName);

    /// <summary>
    /// 获取订单已分配的用户
    /// </summary>
    /// <param name="sourceType"></param>
    /// <param name="sourceId"></param>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> GetDistributedOrderLink(OrderDistributedLink.SourceIdType sourceType, int sourceId)
    {
        int[]? distributedId = null;
        distributedId = await _dbContext.OrderDistributedLinks.Where(l =>
          l.SourceType == sourceType && l.SourceId == sourceId &&
          l.DistributedType == OrderDistributedLink.DistributedIdType.UserId)
      .Select(l => l.DistributedId)
      .ToArrayAsync();


        var users = distributedId.Length > 0
            ? await _dbContext.User.Where(u => distributedId.Contains(u.Id)).Select(u => new DistributedUser(u.Id, u.TrueName))
                .ToArrayAsync()
            : Array.Empty<DistributedUser>();

        return Success(data: new
        {
            users
        });
    }

    [HttpGet]
    public async Task<ResultStruct> GetDistributedOrderUsers(OrderDistributedLink.SourceIdType sourceType, int sourceId,int page=1,int limit=20)
    {
        int[]? distributedId = null;
        distributedId = await _dbContext.OrderDistributedLinks.Where(l =>
          l.SourceType == sourceType && l.SourceId == sourceId &&
          l.DistributedType == OrderDistributedLink.DistributedIdType.UserId)
      .Select(l => l.DistributedId)
      .ToArrayAsync();

        if (distributedId.Length > 0)
        {
            var res = await _dbContext.User.Where(u => distributedId.Contains(u.Id)).OrderBy(a => a.Sort)
                .ToPaginateAsync(page: page, limit: limit);

            if (res.Items.Count > 0)
            {
                res.Transform(a => new
                {
                    a.Id,
                    a.UserName,
                    a.TrueName,
                    a.GroupID,
                    a.FormRuleTemplateID,
                    a.State,
                    a.CreatedAt,
                    a.CompanyID,
                });
            };
            return Success(res);
        }
        return Success(null);
    }
}


public class ExcelGoodInfo
{
    public ExcelGoodInfo()
    {

    }

    public ExcelGoodInfo(string iD, string imageURL, string name, string asin, string sku, int count, MoneyRecord price, MoneyRecord fee)
    {
        ID = iD;
        ImageURL = imageURL;
        Name = name;
        Asin = asin;
        Sku = sku;
        Count = count;
        Price = price;
        Fee = fee;
        ItemPrice = ItemPrice with { Money = price.Money * count };
    }

    public string ID { get; set; }

    /// <summary>
    /// 产品图
    /// </summary>
    public string ImageURL { get; set; }

    /// <summary>
    /// 产品名称
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// asin
    /// </summary>
    public string? Asin { get; set; }

    /// <summary>
    /// Sku
    /// </summary>
    public string? Sku { get; set; }

    /// <summary>
    /// 购买数量
    /// </summary>
    public int Count { get; set; }

    /// <summary>
    /// 单价
    /// </summary>
    public MoneyRecord Price { get; set; }

    /// <summary>
    /// 运费
    /// </summary>
    public MoneyRecord Fee { get; set; }

    public MoneyRecord ItemPrice { get; set; }
}
