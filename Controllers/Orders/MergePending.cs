﻿using AmazonOrderService = ERP.Services.DB.Orders.Order;
using OrderModel = ERP.Models.DB.Orders.Order;
namespace ERP.Controllers.Orders;

using ViewModels.Order.Merge;
using Extensions;
using StoreCache = Services.Caches.Store;
public class MergePending : BaseController
{
    //原erp3.0 注释

    //public $platform = array(
    //    'amazon_order' => '亚马逊'
    //);
    private readonly AmazonOrderService _OrderService;
    public MergePending(AmazonOrderService amazonOrder)
    {
        _OrderService = amazonOrder;
    }
    public record MergeSettings
    {
        private string? name { get; set; }
        private int? state { get; set; }
    }
    //    //* 待合并订单列表
    [HttpPost]
    public async Task<ResultStruct> Index(MergeSettings? mergeSettings, [FromServices] StoreCache storeCache)
    {
        //int OperateUserId=,$this->getSessionObj()->user_id;
        var Lists = await _OrderService.GetOrderMergeDesc();
        if (Lists is not null)
        {
            var list = Lists.Select(x => new PendindList
              (
                  x.ID,
                  x.Receiver?.Nation ?? "暂无",
                  x.GoodsScope().FirstOrDefault()?.ImageURL ?? "暂无",
                  x.Store?.Name ?? "暂无",
                  x.OrderNo ?? "暂无",
                  x.ProductNum,
                  x.CompanyID,
                  x.Receiver?.NationId,
                  x.StoreId,
                  x.State.GetDescription(),
                  x.IsMain.GetID()
              )).ToList();
            return Success(list);
        }
        //    $stores = Cache::instance()->storeCompany()->get();

        return Error();
        //var StoreName = (object)null;
        //var OrderStatusName = (object)null;
        //var Progresse = (object)null;
        //if (Lists.Data != null)
        //{
        //    //return Success(Lists);
        //    foreach (var item in Lists.Data)
        //    {
        //        StoreName = storeCache.Get(item.StoreId).Name ?? "无店铺信息";

        //        bool OrderName = Enum.IsDefined(typeof(OrderStates), item.OrderState);
        //        OrderStatusName = OrderName ? item.OrderState : "";

        //        bool BoolProgress = Enum.IsDefined(typeof(Progresses), item.Progress);
        //        Progresse = BoolProgress ? item.Progress : "";
        //    }
        //    var list = new { Lists, StoreName, OrderStatusName, Progresse };

        //}

        //return Error(message: "Error");

    }

    public ResultStruct getRule()
    {
        //    $rule = $this->sessionData->getRule()->getOrder()->getBiter();

        //        if ($this->sessionData->getConcierge() == UserModel::TYPE_OWN){
        //        //权限
        //        $data['rule'] = [
        //            'ConsolidatedOrder'      => true,
        //            'ClearConsolidatedOrder' => true,
        //            'SetMainOrder'           => true,
        //            'RemoveMergePending'     => true,
        //        ];
        //        }else
        //        {
        //        //权限
        //        $data['rule'] = [
        //            'ConsolidatedOrder'      => $rule->hasConsolidatedOrder(),//合并订单
        //            'ClearConsolidatedOrder' => $rule->hasClearConsolidatedOrder(),//清空待合并订单
        //            'SetMainOrder'           => $rule->hasSetMainOrder(),//设置主订单
        //            'RemoveMergePending'     => $rule->hasRemoveMergePending()//移除
        //        ];
        //        }
        //        return success('', $data);
        return Success();
    }
}
