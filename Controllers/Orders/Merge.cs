using ERP.Models.DB.Orders;
using ERP.ViewModels.Order.Merge;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Service = ERP.Services.DB.Orders.Merge;
using AmazonOrderService = ERP.Services.DB.Orders.Order;
using Newtonsoft.Json.Linq;
using ERP.Models.DB.Stores;
using ERP.Services;
using NationCache = ERP.Services.Caches.Nation;
using ERP.Services.Finance;

namespace ERP.Controllers.Orders;
using static Models.DB.Orders.Order;
using AmazonOrderModel = Models.DB.Orders.Order;
using Extensions;
using StoreCache = Services.Caches.Store;
using Enums.Orders;
using static Models.DB.Identity.User;
using Interface;
using ERP.Data;
using ERP.Services.Identity;
using ERP.Models.View.Products.Product;
using ERP.Authorization.Rule.Component;
using ERP.Distribution.Services;
using WaybillService = ERP.Services.DB.Logistics.WayBill;
using OrderMergeModel = ERP.Models.DB.Orders.Merge;
using DisorderService=  ERP.Services.Distribution.AmazonOrderService;
using NPOI.SS.Formula.Functions;

public class Merge : BaseController
{


    Dictionary<int, string> ORDER_PROGRESS = new()
    {
        { 0, "未处理" },//1,5,10,13
        { 1, "处理中" },//7,8,3,4,6
        { 2, "完成(交付)" },//9
        { 3, "退款" },//12
        { 4, "取消" },//2
        { 5, "补发" },//11
        { 6, "FBA" },//14
        { 7, "仓库已发货(包裹已寄出)" },//董哥要求
                              //        7 => '仓库已发货',
                              //        8 => '仓库已收货',
        { 9, "异常订单" },
    };
    //  //原erp3.0 注释
    #region ERP3.0 PHPService引用及签名
    // private  $AmazonOrderService;

    //   public  ResultStruct   __construct()
    //  {
    //  //        parent::__construct();
    //  //    $this->AmazonOrderService = new AmazonOrderService();
    //}
    private readonly Service _Service;
    private readonly StoreCache _StoreCache;
    private readonly AmazonOrderService _orderService;
    private readonly DBContext _dbContext;
    private readonly UserManager _userManager;
    private readonly NationCache _nationCache;
    private readonly WaybillService waybillService;
    private readonly ISessionProvider _SessionProvider;
    private readonly ILogger<Merge> _logger;
    private readonly DisorderService disorderService;
    public Merge(Service Service, AmazonOrderService AmazonOrderService, UserManager userManager, NationCache nationCache, DBContext dbContext, StoreCache StoreCache, WaybillService _waybillService, DisorderService _disorderService, ILogger<Merge> logger, ISessionProvider SessionProvider)
    {
        _SessionProvider = SessionProvider;
        _Service = Service;
        _orderService = AmazonOrderService;
        _StoreCache = StoreCache;
        _userManager = userManager;
        _dbContext= dbContext;
        _nationCache= nationCache;
        waybillService = _waybillService;
        _logger= logger;
        disorderService = _disorderService;
    }
    private ISession Session { get => _SessionProvider.Session!; }

    [HttpPost]
    public async Task<ResultStruct> GetMergeList(MergeList merge, [FromServices] StoreCache storeCache)
    {
        try
        {
            List<int> Ids = new();
            List<OrderMergeModel> list = new();

            var SerchMergeIds = await _Service.GetAmazonlList(merge, x => x.CompanyID == Session.GetCompanyID()); 
            if (SerchMergeIds is not null && SerchMergeIds.Count() >= 0)
            {
                list = await _Service.GetOrderMergeList(SerchMergeIds.Select(x => x.MergeId).ToList(), merge, x => x.CompanyID == Session.GetCompanyID());
            }
            return await GetMergeResult(merge, list);
        }
        catch (Exception e)
        {
            _logger.LogError($"GetMergeList exception:{e.Message}");
            _logger.LogError($"GetMergeList exception:{e.StackTrace}");
            return Success();
        }
    }


    private async Task<ResultStruct> GetMergeResult(MergeList merge, List<OrderMergeModel> list)
    {
        List<AmazonOrderModel> Orders = new();
        if (list is not null && list.Count() >= 0)
        {
           //合并订单信息
            foreach (var item in list)
            {
                if (!item.OrderInfo.IsNullOrWhiteSpace())
                {
                    var orders = await _orderService.GetOrdersByMIdsAsync(item.ID);
                    if (orders.Count() > 0)
                    {
                        foreach (var order in orders)
                        {
                            order.MergeType = item.Type;   //0                           
                            var OrderInfos = JsonConvert.DeserializeObject<List<MergeOrderInfo>>(item.OrderInfo);
                            if (OrderInfos is null)
                            {
                                return Error("订单信息错误");
                            }
                            foreach (var OrderInfo in OrderInfos)
                            {
                                if (OrderInfo.order_id == order.ID && OrderInfo.is_main == IsMains.THELORD)
                                {
                                    order.IsMain = IsMains.THELORD;
                                    if (order.IsMain == IsMains.THELORD)
                                    {
                                        order.MergeCount = orders.Count();
                                    }
                                    else
                                    {
                                        order.MergeCount = -1;
                                    }
                                }
                            }
                        }
                    }
                    Orders.AddRange(orders);
                }
            }
        }

        var orderGood = Orders.Select(x => new MergeListVm
            (
                x.ID,
                x.OrderNo,
                x.ProductNum,
                x.State.GetDescription(),
                x.Store?.Name ?? "暂无店铺信息",
                x.MergeId,
                x.Url,
                x.Coin,
                x.OrderTotal.ToString(Session.GetUnitConfig()),
                x.Bits.GetDescription(),
                x.FakeProductTime.ToString() ?? "暂无",
                x.TrueProductTime.ToString() ?? "暂无",
                x.CreateTime,
                x.LatestShipTime.ToString() ?? "暂无",
                x.MergeCount,
                x.Receiver?.Nation ?? "暂无国家信息",
                x.MergeType.GetDescription(),
                x.OrderCompanyId
            )).ToList();
        return Success(new
        {
            mergeType = 1,
            is_show = 1,
            orderGood,
            orderStatus = GetSupportsOrderStates(),
            progressStatus = ORDER_PROGRESS
        });
    }

    [HttpPost]
    public async Task<ResultStruct> GetDisMergeList(MergeList merge, [FromServices] StoreCache storeCache)
    {
        try
        {
            List<int> Ids = new();
            List<OrderMergeModel> list = new();          
            var SerchMergeIds = await _Service.GetAmazonlList(merge, x=>x.OrderCompanyId == Session.GetCompanyID()); 
            if (SerchMergeIds is not null && SerchMergeIds.Count() >= 0)
            {
                list = await _Service.GetOrderMergeList(SerchMergeIds.Select(x => x.MergeId).ToList(), merge, x => x.OrderCompanyID == Session.GetCompanyID());
            }           
            return await GetMergeResult(merge, list);
        }
        catch (Exception e)
        {
            _logger.LogError($"GetDisMergeList exception:{e.Message}");
            _logger.LogError($"GetDisMergeList exception:{e.StackTrace}");
            return Success();
        }
    }



    [HttpGet("{mergeID}")]
    public async Task<ResultStruct> Info(int? mergeID)
    {
        var user = await _userManager.GetUserAsync(User);
        await _dbContext.Entry(user).Reference(u => u.OrderUnitConfig).LoadAsync();
        var unitInfo = user.OrderUnitConfig;

        int is_sku = 1;       
        if (mergeID is null || mergeID<=0)
        {
            return Error("合并订单参数传递失败");
        }
        
        var orders = await _orderService.GetOrdersByMIdsAsync(mergeID);
        if (orders is null || orders.Count <= 0)
        {
            return Error("没有找到订单信息");
        }
        var exampleOrder = orders.First();
        int isAmazon = orders.Count(x => x.Plateform != Platforms.AMAZONSP);
        int entryMode= orders.Count(x => x.EntryMode == EntryModes.WebForm)>0?1:2;
        List<GoodListVM> result = new();
        foreach (var order in orders)
        {           
            var goods = order.GoodsScope().Select(a => new GoodListVM
            {
                OrderID=order.ID,
                OrderNo=order.OrderNo,                
                OrderUnit = order.OrderTotal.Raw.CurrencyCode,
                ID=a.ID,
                Name= a.Name,
                FromUrl = a.FromURL,//网址
                Sku= a.Sku,
                UnitPrice = Helpers.TransMoney(a.UnitPrice, unitInfo.ProductPrice, Session.GetUnitConfig(), false),
                ItemPrice = Helpers.TransMoney(a.ItemPrice, unitInfo.ProductPrice, Session.GetUnitConfig(), false),//商品总价
                TotalPrice = Helpers.TransMoney(a.TotalPrice, unitInfo.ProductPrice, Session.GetUnitConfig(), false),//   a.TotalPrice.ToString(Session.GetUnitConfig(), false),//总价(包含别的费用)
                ImageURL= a.ImageURL,//图片
                QuantityOrdered = a.QuantityOrdered,//订购数量
                QuantityShipped = a.QuantityShipped,//已发货数
                WaitForSendNum = a.WaitForSendNum,//待发货数
                Brand = a.Brand,
                Size = a.Size,
                Color = a.Color,
                ShippingPrice = Helpers.TransMoney(a.ShippingPrice, unitInfo.Deliver, Session.GetUnitConfig(), false),
                Unit = a.TotalPrice.Raw.CurrencyCode,
                goodIdText = order.Plateform.Platform.GetGoodIdDescription()
            });           
            result.AddRange(goods);
        }
        return Success( new
        {
            result,
            entryMode,
            orderID= exampleOrder.ID,
            receiver= exampleOrder.Receiver,
            Plateform = isAmazon > 0 ? Platforms.SHOPEE : Platforms.AMAZONSP
        });
    }


    public record RemoveMerges { public int MergeId { get; set; } public int? OrderId { get; set; } };
    [HttpPost]
    public async Task<ResultStruct> RemoveMerge(RemoveMerges removeMerges)
    {
        var Info = await _Service.GetOrderMergeListFirst(removeMerges.MergeId);
      
        if (Info != null)
        {
            var OrderInfoObj = JsonConvert.SerializeObject(Info.OrderInfo);
            int OrderInfoCount = OrderInfoObj.GetType().GetProperties().Length;
            if (OrderInfoCount == 2)
            {
                if (Info.OrderInfo!.Length > 0)
                {
                    var ExecSplitOrd = _Service.ExecSplit(removeMerges.MergeId, Info.OrderInfo);
                    var OrderInfos = JsonConvert.DeserializeObject<List<MergeOrderInfo>>(Info.OrderInfo);
                    if (OrderInfos is null)
                    {
                        return Error("订单信息错误");
                    }
                    foreach (var OrderInfo in OrderInfos)
                    {
                        // dump($value);
                        if (OrderInfo.order_id == removeMerges.OrderId && OrderInfo.is_main == IsMains.THELORD)
                        {
                            return Error(message: "主订单不允许移除！");
                        }
                    }
                    var RemoveMerge = await _orderService.RemoveMergeBegin(Info.OrderInfo, removeMerges);

                    return RemoveMerge.State ? Success("移除成功") : Error("移除失败");
                }
            }
        }
        return Error("未找到");
    }

    /// <summary>
    /// 拆分合并订单
    /// </summary>
    /// <param name="MergeId"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> SplitMerge(ViewModels.IdDto MergeId)
    {
        var Info = await _Service.GetOrderMergeListFirst(MergeId.ID);
        
        if (Info is null || Info.OrderInfo is null)
        {
            return Error("请选中拆分订单");
        }

        //如果该合并订单下，有未取消的运单，则不能拆分
        var mergeWaybills = await waybillService.GetMergeWaybillList(MergeId.ID);
        if(mergeWaybills.Where(x => x.State != ERP.Enums.Logistics.States.Cancle).Any())
        {
           return  Error(message: "拆分失败，该订单已经成功发起运单");
        }

        if (Info.OrderCompanyID != 0)
        {
            return Error(message: "拆分失败，该订单已经成功上报，请先退回");
        }

        //添加权限判断，如果是分销公司，并且自动上报合并运单，那么可以撤回的时候，需要把上报也退回
        //1.判断用户权限，自动上报
        var user = await _userManager.GetUserAsync(User);
        var company = user.Company;
        if (company.DistributionOrder == Models.DB.Users.Company.DistributionOrders.TRUE)
        {
            await disorderService.ToBackMerge(MergeId.ID, user.CompanyID, user.Company.ReportId);           
        }

        var Res = await _Service.ExecSplit(MergeId.ID, Info.OrderInfo);

        return Res.State ? Success("成功") : Error(message: "失败");
    }

    /// <summary>
    /// 获取权限
    /// return \Illuminate\Http\JsonResponse
    /// </summary>
    /// <returns></returns>
    public ResultStruct getRule()
    {
        //    $rule = $this->sessionData->getRule()->getOrder()->getBiter();

        //        if ($this->sessionData->getConcierge() == UserModel::TYPE_OWN){
        //        //权限
        //        $data['rule'] = [
        //            'AmazonDetail'        => true,
        //            'removeMerge'         => true,
        //            'AddConsignmentNote'  => true,
        //        ];
        //        }else
        //        {
        //        //权限
        //        $data['rule'] = [
        //            'AmazonDetail'        => $rule->hasAmazonDetail(),//订单详情
        //            'removeMerge'         => $rule->hasRemoveMergePending(),//移除-拆分
        //            'AddConsignmentNote'  => $rule->hasAddConsignmentNote()//发起运单
        //        ];
        //        }

        //        return success('', $data);
        return Success();
    }

    /// ***********************************
    /// * 权限
    /// ***********************************
    /// <summary>
    /// 获取权限
    /// return \App\Rule\BitwiseFlags\Order
    /// </summary>
    /// <returns></returns>
    private object rules()
    {
        //        return $this->sessionData->getRule()->getOrder()->getBiter();

        return Success();
    }//end rules()
    #endregion
}
