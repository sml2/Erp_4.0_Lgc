﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Service = ERP.Services.DB.Orders.Refund;
using OrderRefundModel=ERP.Models.DB.Orders.Refund;

namespace ERP.Controllers.Orders
{
    public class Refund : BaseController
    {
        private readonly IMemoryCache _Cache;
        private readonly Service _Service;

        public Refund(IMemoryCache cache, Service Service)
        {
            _Cache = cache;
            _Service=Service;
        }
        //public async Task<ResultStruct> GetConstructCache()
        //{
        //    DateTime dateTime;
        //    var S=_Cache.GetOrCreate
        //}

        //  //原erp3.0 注释
        #region ERP3.0 PHPService引用及签名
        //        private OrderRefundService $OrderRefundService;
        //    /**
        //     * @var CounterStoreBusinessService
        //     */
        //    private CounterStoreBusinessService $CounterStoreBusinessService;
        //    /**
        //     * @var CounterCompanyBusinessService
        //     */
        //    private CounterCompanyBusinessService $CounterCompanyBusinessService;
        //    /**
        //     * @var CounterGroupBusinessService
        //     */
        //    private CounterGroupBusinessService $CounterGroupBusinessService;
        //    /**
        //     * @var CounterUserBusinessService
        //     */
        //    private CounterUserBusinessService $CounterUserBusinessService;
        //    private AmazonOrderModel $AmazonOrderModel;
        //    /**
        //     * @var BillLog
        //     */
        //    private BillLog $BillLog;
        //    /**
        //     * @var CompanyService
        //     */
        //    private CompanyService $CompanyService;
        //    /**
        //     * @var CounterRealFinanceService
        //     */
        //    private CounterRealFinanceService $CounterRealFinanceService;


        //public  ResultStruct  __construct()
        //      {
        //            parent::__construct();
        //        $this->OrderRefundService = new OrderRefundService();
        //        $this->CounterStoreBusinessService = new CounterStoreBusinessService();
        //        $this->CounterCompanyBusinessService = new CounterCompanyBusinessService();
        //        $this->CounterGroupBusinessService = new CounterGroupBusinessService();
        //        $this->CounterUserBusinessService = new CounterUserBusinessService();
        //        $this->AmazonOrderModel = new AmazonOrderModel();
        //        $this->BillLog = new BillLog();
        //        $this->CompanyService = new CompanyService();
        //        $this->CounterRealFinanceService = new CounterRealFinanceService();

        //        }

        //public async Task<ResultStruct> __Construct()
        //{

        //}

        //        /**
        //         * 获取订单退款列表数据
        //         */
        [HttpPost]
        public ResultStruct refundList(ViewModels.Order.OrderRefund.OrderRefund  Refund/*Request $rq*/)
        {
            //        $stores = Cache::instance()->StoreCompany()->get();
            //        $company_id = $this->getSessionObj()->company_id;
            int CompanyId=0;
            var List = _Service.GetOrderRefundListPage(Refund, CompanyId);
            return Success(new {List});
        }

        //        /**
        //         *  配置项 审核操作
        //         * 审核 订单退款申请
        //         * @param OrderRefundRequest $request
        //         * @return JsonResponse
        //        */
        public async Task<ResultStruct> AuditRefund(int Id,int Type /*OrderRefundRequest $request*/)
        {
            //        //参数基础验证
            //        $input = $request->validated();
            //        $refund_id = $input['id'];
            //        $input_type = $input['type'];
            //if(Type==OrderRefundModel.RefundState.STATUS_REFUSE)
            //{
            //}

            //            //拒绝审核
            //            if ((int)$input_type === OrderRefundModel::STATUS_REFUSE){
            //                if (!$this->rules()->hasRefuse()) {
            //                    return error('无权访问');
            //                }

                             //   await _Service.RefuseOrderRefund(Id);
            //               
            //            }

            //            if (!$this->rules()->hasAdopt()) {
            //                return error('无权访问');
            //            }

            //退款订单数据
            var RefundInfo =await  _Service.GetOrderRefundInfoFirst(Id);
            if (RefundInfo == null)
            {
                return Error("当前申请不存在");
            }
            //数据汇总
            // return  $this->doAuditRefundBegin($refund_info,$refund_id);
            await DoAuditRefundBegin(RefundInfo, Id);
            return Success();
        }

        //        //修改退款订单事务操作

        //        /**
        //         * @param $info(refund_info)
        //         * @param $refund_id
        //         * @return JsonResponse
        //         */
        public async Task<ResultStruct> DoAuditRefundBegin(OrderRefundModel OrderRefund ,int Id  /*$info,$refund_id*/)
        {

            //        $sessionObj = $this->getSessionObj();

            //        $company_id = $sessionObj->company_id;
            //        $user_id = $sessionObj->user_id;
            //        $group_id = $sessionObj->group_id;
            //        $truename = $sessionObj->truename;
            //            // 启动事务
            //            DB::beginTransaction();
            //            try
            //            {
            //            $refund = $info->getRawOriginal('refund');
            //            $order_refund = $info->getRawOriginal('order_refund');
            //                //财务记录
            //                if (($money = $refund - $order_refund) > 0) {
            //                //增加退款金额
            //                $type = 2;
            //                //店铺汇总
            //                $this->CounterStoreBusinessService->addOrderTotalRefund($info['store_id'],$money);
            //                //公司汇总
            //                $this->CounterCompanyBusinessService->addOrderTotalRefund($company_id,$money);
            //                //用户汇总
            //                $this->CounterUserBusinessService->addOrderTotalRefund($user_id,$money);
            //                //用户组汇总
            //                $this->CounterGroupBusinessService->addOrderTotalRefund($group_id,$money);
            //                //公司账单
            //                $this->CompanyService->privateWalletReduse($company_id,$money);

            //                /**
            //                 * 7.9新增 月汇总
            //                 */
            //                $this->CounterRealFinanceService->incrementOrderTotalRefund($this->getCompany(),$money);

            ////              //修改订单信息
            //                $updateOrder = array(
            //                    "profit"     => DB::raw($this->AmazonOrderModel->transformKey("profit").'-'. $money),
            //                    "refund"     => $refund//退款金额
            //                );
            //                } else
            //                {
            //                //减少退款金额
            //                $money = $order_refund - $refund;
            //                $type = 1;
            //                //店铺汇总
            //                $this->CounterStoreBusinessService->reduceOrderTotalRefund($info['store_id'],$money);
            //                //公司汇总
            //                $this->CounterCompanyBusinessService->reduceOrderTotalRefund($company_id,$money);
            //                //用户汇总
            //                $this->CounterUserBusinessService->reduceOrderTotalRefund($user_id,$money);
            //                //用户组汇总
            //                $this->CounterGroupBusinessService->reduceOrderTotalRefund($group_id,$money);
            //                //公司账单
            //                $this->CompanyService->privateWalletAdd($company_id,$money);
            //                /**
            //                 * 7.9新增 月汇总
            //                 */
            //                $this->CounterRealFinanceService->decrementOrderTotalRefund($this->getCompany(),$money);

            ////              //修改订单信息
            //                $updateOrder = array(
            //                    "profit"     => DB::raw($this->AmazonOrderModel->transformKey("profit").'+'. $money),
            //                    "refund"     => $refund
            //                );
            //                }
            //            $info['refund_difference'] = $money;
            //            $info['info_url'] = 'amazonOrder/info';

            //            $bill_log_id = $this->BillLog->refundBillLog($info,$audit = 4, $type);
            //            //修改订单信息
            //            $updateOrder['refund_bill'] = $bill_log_id;

            //            $updateData['parent_company_id'] = $sessionObj->report_id;
            //            $updateData['audit_user_id'] = $user_id;
            //            $updateData['audit_truename'] = $truename;
            //            $updateData['state'] = OrderRefundModel::STATUS_BY;
            //            $updateData['refund_bill'] = $bill_log_id;

            //                AmazonOrderModel::query()->Id($info['order_id'])->update($updateOrder);
            //                OrderRefundModel::query()->where('id',$refund_id)->update($updateData);
            //                // 提交事务
            //                DB::commit();
            //                return success("通过订单退款成功");
            //            }
            //            catch (\Exception $e){
            //                // 回滚事务
            //                DB::rollBack();
            //                return error("通过订单退款失败");
            //            }
            return await Task.FromResult(Success());
        }

        /// <summary>
        /// 获取权限
        /// return \Illuminate\Http\JsonResponse
        /// </summary>
        /// <returns></returns>
        public ResultStruct getRule()
                       {
            //        $rule = $this->sessionData->getRule()->getOrder()->getBiter();

            //                if ($this->sessionData->getConcierge() == UserModel::TYPE_OWN){
            //            //权限
            //            $data['rule'] = [
            //                'AmazonDetail' => true,
            //                'Adopt'        => true,
            //                'Refuse'       => true,
            //            ];
            //            }else
            //            {
            //            //权限
            //            $data['rule'] = [
            //                'AmazonDetail' => $rule->hasAmazonDetail(),//订单详情
            //                'Adopt'        => $rule->hasAdopt(),//通过
            //                'Refuse'       => $rule->hasRefuse(),//拒绝
            //            ];
            //            }

            //            return success('', $data);
            return Success();
        }

        /// <summary>
        /// 获取权限
        /// return \App\Rule\BitwiseFlags\Order
        /// </summary>
        /// <returns></returns>
        private ResultStruct rules()
        {
            //            return $this->sessionData->getRule()->getOrder()->getBiter();
            return Success();
        }//end rules()
        #endregion
    }
}
