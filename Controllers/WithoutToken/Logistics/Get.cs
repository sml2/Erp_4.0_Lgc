﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.Logistics
{
    public class Get : Controller
    {
        ////原ERP3.0注释
        #region ERP3.0 PHP
        //    //510
        //    private const LogisticsIsEmpty = 1; //物流信息不存在(公司未配置物流信息,请配置)
        //    private LogisticsParamService $LogisticsParamService;

        //public function __construct()
        //    {
        //    $this->LogisticsParamService = new LogisticsParamService();
        //    }

        //    public function getApiId(): int
        //{
        //    return 510;
        //}

        //public function handle($request, $userId, $rule, $session)
        //{
        //    $field = ['id', 'data', 'customize_name', 'remark', 'logistics_id'];
        //    $list = $this->LogisticsParamService->getListByCompanyId($session->getCompanyId(), $field);
        //    if ($list->isEmpty()) {
        //        return $this->fail(self::LogisticsIsEmpty);
        //    }
        //    $data = [];
        //    $logistics = Cache::instance()->logistics()->get();
        //    foreach ($list as $v) {
        //        $data[] = [
        //            'ID' => $v['id'],
        //            'CustomizeName' => $v['customize_name'],
        //            'Remark' => $v['remark'],
        //            'Data' => $v['data'],
        //            'Sign'=>isset($logistics[$v['logistics_id']]) ?$logistics[$v['logistics_id']]['sign']:'',
        //            'Name'=>isset($logistics[$v['logistics_id']]) ?$logistics[$v['logistics_id']]['name']:'',
        //        ];
        //    }
        //    return $this->success($data);
        //}
        #endregion
        public IActionResult invoke()
        {
            /************************************************  
             ================'Logistics\Get@invoke'
             select `id`, `d_data`, `d_customize_name`, `d_remark`, `i_logistics_id` from `logistics_param` where `i_company_id` = ? and `t_state` = ? order by `d_last_time` desc
             select * from `store` where `i_company_id` = ? and `t_flag` = ? and `t_is_del` = ? and `t_is_verify` = ?
            ************************************************/
            return View();
        }
    }
}
