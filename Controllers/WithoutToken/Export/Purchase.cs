﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.Export;
[Route("api/[controller]")]
[ApiController]
public class Purchase : ControllerBase
{
    ////原ERP3.0注释
    #region ERP3.0 PHP
    //const PurchaseIsEmpty = 1; //采购不存在

    //private PurchaseService $service;

    //public function __construct()
    //{
    //    $this->service = new PurchaseService();
    //}

    //public function getApiId()
    //{
    //    return 740;
    //}

    //public function verifyParams()
    //{
    //    return ['PurchaseIDs'];
    //}

    //public function handle($request, $userId, $rule, $session)
    //{
    //    $PurchaseIDs = $request['PurchaseIDs'];

    //    if (empty($PurchaseIDs))
    //    {
    //        return $this->fail(self::ERR_PARAMETER, 'ID');
    //    }

    //    $field = ['id', 'name', 'asin', 'num', 'purchase_state', 'url', 'order_no', 'from_url', 'purchase_unit', 'purchase_total', 'purchase_url', 'purchase_order_no', 'purchase_tracking_number', 'remark', 'find_truename', 'store_id', 'store_name', 'order_item_id', 'seller_sku', 'created_at'];
    //    $purchase = $this->service->getPurchaseByIds($PurchaseIDs, $field)->toArray();
    //    if (!checkArr($purchase))
    //    {
    //        return $this->fail(self::PurchaseIsEmpty);
    //    }

    //    return $this->success($purchase);
    //}
    #endregion
    public async Task<int> invoke()
    {
        return await Task.FromResult(0);
        /************************************************  
         
        ************************************************/
    }
}
