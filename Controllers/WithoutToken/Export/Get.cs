﻿
using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.Export;
[Route("api/[controller]")]
[ApiController]
public class Get : ControllerBase
{

    ////原ERP3.0注释
    #region ERP3.0 PHP
    //use ProductRange, ProductCommon;

    //public const IDS_Is_Empty = 1;//ids为空
    //private $suffix_param = '';

    //private $syncProductService;
    //private $syncExportService;

    ////主产品完整sku
    //private $mainSku = '';

    //public function __construct()
    //{
    //    $this->syncProductService = new SyncProductService();
    //    $this->syncExportService = new SyncExportService();
    //}

    public int getApiId()
    {
        return 720;
    }

    //public function verifyParams()
    //{
    //    return [
    //        'IDS',//货币
    //    ];
    //}

    //public function handle($request, $userId, $rule, $session)
    //{
    //    $ids = $request['IDS'];
    //    if (!count($ids))
    //    {
    //        return $this->fail(self::IDS_Is_Empty);
    //    }

    //    //图片后缀
    //    $this->getSessionObj()->getProductImgConfig() == 2 && $this->suffix_param = '?t='. $this->getTime();


    //    $language = $request['Language'];
    //    !$language && $language = 'default';

    //    //获取SyncProduct 产品信息
    //    $syncProduct = $this->syncProductService->getSyncProduct($ids);

    //    $pids = array_column($syncProduct, 'pid');
    //    $syncProduct = array_combine($pids, $syncProduct);

    //    //查询产品
    //    $products = Product::query()->where(
    //        [
    //            ['oem_id', $this->getSessionObj()->getOemId()],
    //            ['company_id', $this->getSessionObj()->getCompanyId()],
    //        ]
    //    )->whereIn('id', $pids)->get()->toArray();
    //    foreach ($products as $value) {
    //        $result[] = $this->assembleProductInfo($value, $syncProduct[$value['id']], $language);
    //    }
    //    return $this->success($result);
    //}

    //private function assembleProductInfo($product, $syncProduct, $language)
    //{
    //    if (isset($product['price_original']))
    //    {
    //        $price = $product['price_original'];
    //        $origin_price = json_decode($price, true);
    //    }
    //    else
    //    {
    //        return [
    //            'ID'    => $product['id'],
    //            'State' => false,
    //            'Data'  => [],
    //            'Info'  => '该产品价格存在问题'
    //        ];
    //    }
    //    if (isset($product['variants_original']))
    //    {
    //        $variants = $product['variants_original'];
    //        $origin_variants = json_decode($variants, true);
    //    }
    //    else
    //    {
    //        return [
    //            'ID'    => $product['id'],
    //            'State' => false,
    //            'Data'  => [],
    //            'Info'  => '该产品变体存在问题'
    //        ];
    //    }
    //    $showImg = '';
    //    if ($product['image']) {
    //        $showImg = $this->getImageUrl($product['image']). $this->suffix_param;
    //    }

    //    $Products = [];
    //    $extend = $product['ext'] ?? [];
    //    $productInfoImages = $product['images'];
    //    $tasksGet = new TasksGetClass();

    //    $lp = [
    //        'n' => $product['lp_n'],
    //        's' => $product['lp_s'],
    //        'k' => $product['lp_k'],
    //        'd' => $product['lp_d'],
    //    ];
    //    $languagePack = $product['languages'];
    //    if (isset($languagePack[$language]))
    //    {
    //        $currentLanguagePack = $languagePack[$language];
    //        $lp = [
    //            'n' => $currentLanguagePack['n'],
    //            's' => $currentLanguagePack['s'],
    //            'k' => $currentLanguagePack['k'],
    //            'd' => $currentLanguagePack['d'],
    //        ];
    //    }
    //    //判断是否有无变体
    //    $autoGenerate = ProductService::AutoGenerate($product['attribute']);
    //    //主产品sku
    //    $this->mainSku = $this->makeSku($syncProduct);

    //    if ($origin_variants) {
    //        foreach ($origin_variants as $k => $v) {
    //            //过滤掉失效变体
    //            if ($v['state'] == 1) {
    //                if (isset($v['lps']) && isset($v['lps'][$language]) && checkArr($v['lps'][$language]))
    //                {
    //                    $name = $v['lps'][$language]['n'] ?? $lp['n'];
    //                    $description = $v['lps'][$language]['d'] ?? $lp['d'];
    //                    $bulletPoint = $v['lps'][$language]['s'] ?? $lp['s'];
    //                    $searchTerms = $v['lps'][$language]['k'] ?? $lp['k'];
    //                }
    //                else
    //                {
    //                    $name = $lp['n'];
    //                    $description = $lp['d'];
    //                    $bulletPoint = $lp['s'];
    //                    $searchTerms = $lp['k'];
    //                }
    //                $currentIds = $this->getVariantIds($v['ids']);

    //                $attributeName = $this->makeAttribute($syncProduct, $product['attribute'], $v);

    //                $Sku = '-'. $this->makeVariantSku($syncProduct, $k, $attributeName);
    //                if ($autoGenerate) {
    //                    $Sku = $this->mainSku;
    //                }
    //                $Products[] = [
    //                    'Name'        => $this->makeVariantTitle($syncProduct, $name, $attributeName),
    //                    'Description' => transitionbr($description, '<br>'),//ReadOnly Property Description As String
    //                    'BulletPoint' => $bulletPoint,//ReadOnly Property BulletPoint As List(Of String)
    //                    'SearchTerms' => $searchTerms,//ReadOnly Property SearchTerms As List(Of String)
    //                    'Values'      => $currentIds,//ReadOnly Property Values As String
    //                    'Sku'         => $Sku,//Property Sku As String
    //                    'MainImage'   => $this->getFullUrl($v['img']['main'], $productInfoImages),
    //                    'Images'      => $this->getFullUrl($this->imageIds($v['img']), $productInfoImages) ?: [],
    //                    'Quantity'    => (int)$v['quantity'],     // ReadOnly Property Quantity As Integer
    //                    'Price'       => $v['sale'],
    //                    'Cost'        => $v['cost'],
    //                    'ProduceCode' => $v['code'],
    //                    'Fields'      => $tasksGet->getExtProperty($extend, $v['ext'] ?? null),
    //                    'SID'         => $v['sid']
    //                ];
    //            }
    //        }
    //    }

    //    $data = [
    //        'PID'         => $product['id'],
    //        'Name'        => $lp['n'],
    //        'Source'      => $product['source'],
    //        'Sku'         => $this->mainSku,
    //        'Quantity'    => $product['quantity'],//库存
    //        'ShowImg'     => $showImg,
    //        'Price'       => $origin_price,//未转换
    //        'Description' => transitionbr($lp['d'], '<br>'),
    //        'BulletPoint' => $lp['s'],
    //        'SearchTerms' => $lp['k'],
    //        'Category'    => $tasksGet->categoryInfo($product['category_id']),
    //        'Attribute'   => $tasksGet->setLanguageAttribute($product['attribute'], $languagePack, $language),
    //        'Products'    => $Products,
    //        'ProductID'   => $product['pid']
    //    ];
    //    return [
    //        'ID'    => $product['id'],
    //        'State' => true,
    //        'Data'  => $data,
    //        'Info'  => '',
    //    ];
    //}


    ///**
    // * 商品再用图片的ids
    // * @param $imgArray
    // * @return array
    // */
    //protected function imageIds($imgArray): array
    //{
    //    if (!$imgArray) {
    //        return [];
    //    }
    //    $imageIds = [];
    //    if (!empty($imgArray['affiliate'])) {
    //        foreach ($imgArray['affiliate'] as $k => $v) {
    //            array_push($imageIds, $v);
    //        }
    //    }
    //    return $imageIds;
    //}

    ///**
    // * 取完整产品图片域名地址
    // * @param $data
    // * @param $productInfoImages
    // * @return array|string
    // */
    //protected function getFullUrl($data, $productInfoImages)
    //{
    //    $images = [];
    //    if ($data) {
    //        if (!is_array($data)) {
    //            foreach ($productInfoImages as $value) {
    //                if ($value->id == $data) {
    //                    return $this->getImageUrl($value->path) . $this->suffix_param;
    //                }
    //            }
    //        } else {
    //            $productInfoImages = arrayCombine($productInfoImages, 'id');
    //            foreach ($data as $v) {
    //                if (isset($productInfoImages[$v])) {
    //                    $images[] = $this->getImageUrl($productInfoImages[$v]->path) . $this->suffix_param;
    //                }
    //            }
    //            return $images;
    //        }
    //    } else {
    //        return '';
    //    }
    //}


    ///**
    // * 导出产品主产品sku
    // * User: CCY
    // * Date: 2020/8/5 19:44
    // * @param $id
    // * @param $syncProduct
    // * @return string
    // */
    //protected function makeSku($syncProduct)
    //{
    //    return $this->getMainSku($syncProduct['sku_type'], $syncProduct['source_id'], $syncProduct['sku']);
    //}

    ///**
    // * 获取变体sku追加字符串
    // * User: CCY
    // * Date: 2020/12/22
    // * @param $syncProduct
    // * @param $attribute
    // * @param $nonius
    // */
    //protected function makeVariantSku($syncProduct, $nonius, $attributeName)
    //{
    //    $sku = $this->getVariantSku($syncProduct['sku_suffix'], $attributeName, $nonius);
    //    return str_replace(' ', '', $sku);
    //}

    ///**
    // * 获取变体标题追加属性名
    // * User: CCY
    // * Date: 2020/12/22
    // * @param $syncProduct
    // * @param $title
    // * @return mixed
    // */
    //protected function makeVariantTitle($syncProduct, $title, $attributeName)
    //{
    //    return $this->getVariantTitle($syncProduct['title_suffix'], $title, $attributeName);
    //}

    ///**
    // * 获取导出产品变体需要追加属性时属性名
    // * User: CCY
    // * Date: 2020/12/22
    // * @param $syncProduct
    // * @param $attribute
    // * @param $variants
    // * @return string
    // */
    //protected function makeAttribute($syncProduct, $attribute, $variants)
    //{
    //    return $this->getAttributeName($syncProduct, $attribute, $variants['ids']);
    //}

    #endregion
    //public async Task invoke()
    //{
        /************************************************  
         ================/export/get
select * from `sync_product` where `id` in (?, ?, ?)
select * from `product` where (`i_oem_id` = ? and `i_company_id` = ?) and `id` in (?, ?, ?)
select * from `product` where `id` = ? and b_type & * limit *
select count(*) as count, `i_store_id` from `sync_mws` where `t_state` in (?, ?) and `i_company_id` = ? and `i_oem_id` = ? group by `i_store_id`
select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
insert into `temp_images` (`d_oss_path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `i_product_id`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
update `store` set `d_last_auto_time` = ?, `store`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
update `store` set `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?, ?, ?)
insert into `resource` (`created_at`, `d_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
update `product` set `u_lp_k` = ?, `u_lp_s` = ?, `u_lp_d` = ?, `b_type` = ?, `u_price` = ?, `u_quantity` = ?, `d_source` = ?, `d_ext` = ?, `d_attribute` = ?, `d_variants` = ?, `d_images` = ?, `u_image` = ?, `d_pid` = ?, `d_pid_hash_code` = ?, `c_size` = ?, `d_coin` = ?, `d_rate` = ?, `product`.`updated_at` = ? where `id` = ?
update `counter_company_business` set `c_product_num` = `c_product_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` = ?
update `counter_company_business` set `c_report_product_num` = `c_report_product_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` in (?, ?, ?, ?)
update `counter_user_business` set `c_product_num` = `c_product_num` + *, `counter_user_business`.`updated_at` = ? where `i_user_id` = ?
select `id`, `d_button_name` from `waybill` where ((`i_company_id` = ? and `i_operate_company_id` = ?) or `i_waybill_company_id` = ?) and `d_button_name` in (?, ?) order by `updated_at` asc limit *
update `waybill` set `waybill`.`updated_at` = ? where `id` in (?, ?, ?, ?, ?)
select * from `waybill` where `waybill`.`id` = ? limit *
select `i_store_id` from `to_sell_task` where `i_company_id` = ? and `t_is_finish` = ? and `i_oem_id` = ?
select * from `store` where `i_company_id` = ? and `t_flag` = ? and `t_is_del` = ? and `t_is_verify` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `id`, `i_operate_id`, `updated_at` from `sync_upload` where (`i_store_id` = ? and `t_state` = ? and `updated_at` < ?)
select `id` from `sync_upload` where `i_store_id` = ? and `t_state` = ? and `i_operate_id` = ?
update `sync_upload` set `d_pricing_id` = ?, `t_pricing_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
insert into `sync_session_log` (`i_upload_id`, `d_index`, `d_state`, `i_company_id`, `i_user_id`, `i_group_id`, `i_oem_id`, `i_last_operate_id`, `d_count`, `d_server_old_state`, `d_client_old_state`, `d_invoke_api`, `d_index_text`, `d_state_text`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
insert into `sync_alive_session` (`i_upload_id`, `d_index`, `d_state`, `i_company_id`, `i_user_id`, `i_group_id`, `i_oem_id`, `i_last_operate_id`, `d_count`, `d_server_old_state`, `d_client_old_state`, `d_invoke_api`, `d_index_text`, `d_state_text`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select * from `sync_upload` where `id` = ? limit *
select column_name as `column_name` from information_schema.columns where table_schema = ? and table_name = ?
insert into `sync_invoke_api_log` (`i_upload_id`, `d_request_data`, `i_upload_operate_id`, `i_user_id`, `i_company_id`, `i_group_id`, `i_oem_id`, `t_api`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
update `sync_alive_session` set `d_state` = ?, `d_state_text` = ?, `i_last_operate_id` = ?, `i_user_id` = ?, `d_count` = ?, `sync_alive_session`.`updated_at` = ? where (`i_upload_id` = ? and `d_index` = ?)
select `id`, `d_title`, `i_user_id`, `t_state` from `work_order` where `i_company_id` = ? and `t_state` in (?, ?) limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `waybill`.`updated_at` = ? where `id` = ?
select * from `product` where `id` = ? limit *
select count(*) as aggregate from `product` where (`d_hash_code` = ? and `i_company_id` = ? and `id` != ?) and !(b_type&*) AND !(b_type&*) AND !(b_type&*)
update `product` set `u_lp_n` = ?, `u_lp_d` = ?, `d_hash_code` = ?, `b_type` = ?, `u_price` = ?, `d_variants` = ?, `product`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
select `d_order_no` from `amazon_order` where (`i_store_id` = ? and `t_order_state` = ? and `i_company_id` = ? and `t_entry_mode` = ?) limit *
update `store` set `d_last_auto_update_time` = ?, `store`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_oem_id` = ?) and `updated_at` between ? and ?
select `id`, `i_oem_id`, `i_user_id`, `i_company_id`, `i_oem_id`, `i_category_id`, `i_platform_id`, `d_reason`, `u_quantity`, `u_lp_n`, `u_image`, `u_price`, `d_source`, `b_type`, `b_language`, `d_languages`, `updated_at`, `created_at`, `d_coin`, `d_rate` from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_oem_id` = ?) and `updated_at` between ? and ? order by `id` desc limit * offset *
select * from `category` where `category`.`id` in (?)
select * from `task` where `task`.`id` in (?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?) order by `d_last_auto_update_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select * from `task` where (`i_oem_id` = ? and `i_user_id` = ? and `i_group_id` = ? and `d_hash_code` = ?) limit *
select count(*) as aggregate from `product` where (`d_hash_code` = ? and `i_company_id` = ?) and !(b_type&*) AND !(b_type&*) AND !(b_type&*)
insert into `product` (`i_oem_id`, `i_company_id`, `i_user_id`, `i_group_id`, `i_task_id`, `u_lp_n`, `u_lp_k`, `u_lp_s`, `u_lp_d`, `d_source`, `d_hash_code`, `d_languages`, `b_language`, `b_type`, `u_price`, `i_platform_id`, `u_platform`, `d_attribute`, `d_variants`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
update `product` set `b_type` = b_type | *, `product`.`updated_at` = ? where (`d_hash_code` = ? and `i_company_id` = ?) and !(b_type&*) AND !(b_type&*) AND !(b_type&*)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?)
update `sync_upload` set `d_image_id` = ?, `t_image_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `d_image_id`, `t_image_state`, `id`, `i_operate_id` from `sync_upload` where `id` = ? limit *
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
insert into `amazon_order` (`d_refund`, `d_fee`, `t_order_state`, `d_order_no`, `d_order_no_hash`, `t_shipping_type`, `d_user_name`, `d_pay_time`, `d_receiver_phone`, `i_receiver_nation_id`, `d_receiver_nation_short`, `d_receiver_nation`, `d_receiver_province`, `d_receiver_city`, `d_receiver_district`, `d_receiver_address`, `d_receiver_address*`, `d_receiver_address*`, `d_receiver_zip`, `d_receiver_name`, `d_receiver_email`, `i_store_id`, `d_seller_id`, `d_order_money`, `d_pay_money`, `d_create_time`, `d_coin`, `d_delivery_time`, `d_last_time`, `d_delivery`, `d_delivery_no`, `d_rate`, `d_latest_ship_time`, `d_earliest_ship_time`, `i_company_id`, `i_user_id`, `i_group_id`, `i_oem_id`, `d_default_ship_from_location_address`, `u_url`, `d_goods`, `d_product_num`, `d_product_money`, `b_progress`, `d_delivery_num`, `d_profit`, `u_truename`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
insert into `amazon_order_action` (`t_order_status`, `d_order_status_text`, `i_order_id`, `d_action`, `i_user_id`, `i_company_id`, `i_group_id`, `i_oem_id`, `d_user_name`, `d_truename`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
update `counter_group_business` set `c_order_num` = c_order_num+*, `c_order_total` = c_order_total+*.*, `c_order_total_fee` = c_order_total_fee+*, `c_order_total_refund` = c_order_total_refund+*, `counter_group_business`.`updated_at` = ? where `i_group_id` = ?
update `counter_company_business` set `c_order_num` = c_order_num+*, `c_order_total` = c_order_total+*.*, `c_order_total_fee` = c_order_total_fee+*, `c_order_total_refund` = c_order_total_refund+*, `counter_company_business`.`updated_at` = ? where `i_company_id` = ?
update `counter_company_business` set `c_report_order_num` = `c_report_order_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` in (?, ?)
update `counter_real_finance` set `c_order_total` = c_order_total+*.*, `c_order_total_fee` = c_order_total_fee+*, `c_order_total_refund` = c_order_total_refund+*, `counter_real_finance`.`updated_at` = ? where `i_company_id` = ? and `i_oem_id` = ? and `d_month` = ?
update `store` set `d_last_order_time` = ?, `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?, ?, ?, ?)
insert into `resource` (`created_at`, `d_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
delete from `temp_images` where `id` in (?, ?, ?, ?, ?)
select * from `store` where `t_flag` = ? and `t_is_del` = ? and `t_is_verify` = ? and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
update `sync_upload` set `d_relationship_id` = ?, `t_relationship_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
update `product` set `u_lp_s` = ?, `u_lp_d` = ?, `b_type` = ?, `u_price` = ?, `u_quantity` = ?, `d_source` = ?, `d_ext` = ?, `d_attribute` = ?, `d_variants` = ?, `d_images` = ?, `u_image` = ?, `d_pid` = ?, `d_pid_hash_code` = ?, `c_size` = ?, `d_coin` = ?, `d_rate` = ?, `product`.`updated_at` = ? where `id` = ?
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?)
select count(*) as aggregate from `company` where `t_state` = ? and `i_report_id` = ? and `id` != ?
select `id`, `d_name` from `task` where (`i_company_id` = ? and `i_oem_id` = ?) order by `created_at` desc
insert into `reporter_request` (`created_at`, `d_action`, `d_cpu_time`, `d_method`, `d_module`, `d_name`, `d_namespace`, `d_net_time`, `d_request_id`, `d_request_time`, `d_uri`, `i_company_id`, `i_group_id`, `i_oem_id`, `i_user_id`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
delete from `temp_images` where `id` in (?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
update `product` set `u_lp_d` = ?, `b_type` = ?, `u_price` = ?, `d_variants` = ?, `product`.`updated_at` = ? where `id` = ?
update `product` set `u_lp_n` = ?, `u_lp_d` = ?, `d_hash_code` = ?, `b_type` = ?, `u_price` = ?, `d_attribute` = ?, `d_variants` = ?, `product`.`updated_at` = ? where `id` = ?
select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?, ?, ?, ?, ?)
delete from `temp_images` where `id` in (?, ?, ?, ?, ?, ?)
insert into `temp_images` (`created_at`, `d_oss_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
update `product` set `b_type` = ?, `u_price` = ?, `d_attribute` = ?, `d_variants` = ?, `d_images` = ?, `u_image` = ?, `c_size` = ?, `d_rate` = ?, `product`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_oem_id` = ?) and `i_task_id` = ? and `updated_at` between ? and ?
select `id`, `i_oem_id`, `i_user_id`, `i_company_id`, `i_oem_id`, `i_category_id`, `i_platform_id`, `d_reason`, `u_quantity`, `u_lp_n`, `u_image`, `u_price`, `d_source`, `b_type`, `b_language`, `d_languages`, `updated_at`, `created_at`, `d_coin`, `d_rate` from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_oem_id` = ?) and `i_task_id` = ? and `updated_at` between ? and ? order by `id` desc limit * offset *
insert into `reporter_request` (`created_at`, `d_action`, `d_cpu_time`, `d_method`, `d_module`, `d_name`, `d_namespace`, `d_net_time`, `d_request_id`, `d_request_time`, `d_uri`, `i_company_id`, `i_group_id`, `i_oem_id`, `i_user_id`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_name`, `d_amount`, `u_export_sign`, `i_language_id`, `created_at`, `u_append` from `sync_export` where `i_company_id` = ? and `i_oem_id` = ? order by `d_sort` asc limit * offset *
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
insert into `sync_export` (`d_name`, `d_amount`, `u_param`, `u_append`, `d_sort`, `i_language_id`, `u_export_sign`, `i_platform_id`, `i_user_id`, `i_group_id`, `i_company_id`, `i_oem_id`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
insert into `reporter_request` (`created_at`, `d_action`, `d_cpu_time`, `d_method`, `d_module`, `d_name`, `d_namespace`, `d_net_time`, `d_request_id`, `d_request_time`, `d_uri`, `i_company_id`, `i_group_id`, `i_oem_id`, `i_user_id`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
insert into `resource` (`created_at`, `d_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
update `product` set `u_lp_n` = ?, `u_lp_s` = ?, `u_lp_d` = ?, `d_hash_code` = ?, `b_type` = ?, `u_price` = ?, `u_quantity` = ?, `d_attribute` = ?, `d_variants` = ?, `d_images` = ?, `c_size` = ?, `d_rate` = ?, `product`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_group_id` = ? and `i_oem_id` = ?) and `updated_at` between ? and ?
select `id`, `i_oem_id`, `i_user_id`, `i_company_id`, `i_oem_id`, `i_category_id`, `i_platform_id`, `d_reason`, `u_quantity`, `u_lp_n`, `u_image`, `u_price`, `d_source`, `b_type`, `b_language`, `d_languages`, `updated_at`, `created_at`, `d_coin`, `d_rate` from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_group_id` = ? and `i_oem_id` = ?) and `updated_at` between ? and ? order by `id` desc limit * offset *
select * from `product` where `id` = ? and `i_company_id` = ? and `i_oem_id` = ? limit *
insert into `product` (`i_oem_id`, `i_company_id`, `i_user_id`, `i_group_id`, `i_category_id`, `i_sub_category_id`, `u_lp_n`, `u_lp_s`, `u_lp_k`, `u_lp_d`, `d_source`, `d_hash_code`, `d_languages`, `b_language`, `b_type`, `u_price`, `i_platform_id`, `u_platform`, `d_attribute`, `d_variants`, `d_ext`, `d_images`, `u_image`, `c_size`, `d_pid`, `created_at`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
insert into `sync_product` (`i_pid`, `i_source_id`, `u_product_name`, `u_img_url`, `d_sku`, `t_sku_type`, `t_sku_suffix`, `t_title_suffix`, `t_product_type`, `u_category_id`, `u_sub_category_id`, `t_already_upload`, `t_uploading`, `i_user_id`, `i_group_id`, `i_company_id`, `i_oem_id`, `t_eanupc_status`, `d_type_data`, `d_type_data_id`, `created_at`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
update `sync_export` set `d_amount` = `d_amount` + *, `sync_export`.`updated_at` = ? where `id` = ? and `i_company_id` = ? and `i_oem_id` = ?
select `i_sid` from `sync_export_product` where `i_eid` = ? and `i_company_id` = ? and `i_oem_id` = ?
select `id` as `sid`, `d_sku` from `sync_product` where `id` in (?)
select * from `sync_product` where `sync_product`.`id` = ? limit *
select * from `product` where `i_oem_id` = ? and `id` in (?)
delete from `ean_upc` where `id` in (?, ?, ?, ?)
update `product` set `d_variants` = ?, `product`.`updated_at` = ? where `id` = ?
select * from `sync_product` where `i_pid` = ? and `i_company_id` = ? and `i_oem_id` = ? limit *
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
update `product` set `u_lp_n` = ?, `u_lp_d` = ?, `d_hash_code` = ?, `b_type` = ?, `d_attribute` = ?, `d_variants` = ?, `product`.`updated_at` = ? where `id` = ?
update `sync_upload` set `t_product_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
delete from `sync_alive_session` where `i_upload_id` = ? and `d_index` = ?
select `id`, `d_sku_change_data`, `i_spid` from `sync_mws` where `id` = ? limit *
select * from `product` where (`i_oem_id` = ? and `i_company_id` = ? and `id` = ?) limit *
update `sync_upload` set `t_inventory_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
update `sync_upload` set `t_image_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
select * from `product` where (`i_oem_id` = ? and `i_company_id` = ?) and `id` in (?)
update `sync_upload` set `t_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
select `id`, `i_operate_id` from `sync_upload` where `id` = ? limit *
update `product` set `u_lp_n` = ?, `u_lp_d` = ?, `d_hash_code` = ?, `b_type` = ?, `u_price` = ?, `u_quantity` = ?, `d_attribute` = ?, `d_variants` = ?, `product`.`updated_at` = ? where `id` = ?
select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?)
insert into `resource` (`created_at`, `d_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?)
delete from `temp_images` where `id` in (?)
select `d_temp_name`, `i_browser_node_id`, `u_browser_ui`, `t_level`, `i_nation_id`, `d_type_data` from `category_node_default` where `d_type_hash` = ? and (`t_level` = ? or (`i_company_id` = ? and `t_level` = ?) or (`i_company_id` = ? and `i_group_id` = ? and `t_level` = ?)) order by `t_level` desc, `d_sort` asc
update `product` set `b_type` = ?, `product`.`updated_at` = ? where `id` = ?
update `resource` set `c_reuse` = `c_reuse` + *, `resource`.`updated_at` = ? where `id` in (?, ?, ?, ?)
select count(*) as aggregate from `sync_product` where `i_company_id` = ? and `i_oem_id` = ?
delete from `ean_upc` where `id` in (?)
select * from `sync_mws` where `i_store_id` in (?, ?, ?) and `t_state` in (?, ?) and `t_eanupc_status` = ? order by `updated_at` desc limit * offset *
update `sync_product` set `t_already_upload` = ?, `t_uploading` = ?, `sync_product`.`updated_at` = ? where `id` in (?)
insert into `sync_upload` (`i_store_id`, `i_user_id`, `i_company_id`, `i_oem_id`, `i_group_id`, `i_operate_id`, `t_state`, `t_product_state`, `t_variant_state`, `t_pricing_state`, `t_inventory_state`, `t_image_state`, `t_relationship_state`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
insert into `sync_online` (`i_upload_id`, `i_user_id`, `i_company_id`, `i_group_id`, `i_oem_id`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?)
select `i_pid`, `id`, `t_sku_type`, `t_sku_suffix`, `t_title_suffix`, `i_source_id`, `d_sku`, `d_type_data`, `d_type_data_id` from `sync_product` where `i_company_id` = ? and `id` = ? limit *

        ************************************************/
    //    return Task.FromResult;
    //}
}

