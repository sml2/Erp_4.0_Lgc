﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.Export;
[Route("api/[controller]")]
[ApiController]
public class Finish : ControllerBase
{

    ////原ERP3.0注释
    #region ERP3.0 PHP
    public int getApiId()
    {
        return 730;
    }

    //public function handle($request, $userId, $rule, $session)
    //{
    //    // TODO: Implement handle() method.
    //}
    #endregion


    public async Task<int> invoke()
    {
        return await Task.FromResult(0);
        /************************************************  
         
        ************************************************/
    }
}
