﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.Export;
[Route("api/[controller]")]
[ApiController]
public class Waybill : ControllerBase
{
    ////原ERP3.0注释
    #region ERP3.0 PHP
    //private WayBillService $service;

    //public function __construct()
    //{
    //    $this->service = new WayBillService();
    //}


    //const WaybillIsEmpty = 1; //运单不存在

    //public function getApiId()
    //{
    //    return 750;
    //}

    //public function verifyParams()
    //{
    //    return ['WayBillIDs'];
    //}

    //public function handle($request, $userId, $rule, $session)
    //{
    //    $WayBillIDs = $request['WayBillIDs'];

    //    if (empty($WayBillIDs))
    //    {
    //        return $this->fail(self::ERR_PARAMETER, 'ID');
    //    }

    //    $field = ['id', 'order_no', 'img_url', 'logistic', 'ship_method', 'waybill_order', 'express', 'tracking', 'product_info', 'waybill_state', 'carriage_num', 'truename', 'is_verify', 'remark', 'created_at'];
    //    $purchase = $this->service->getWaybillByIds($WayBillIDs,$field)->toArray();
    //    if (!checkArr($purchase))
    //    {
    //        return $this->fail(self::WaybillIsEmpty);
    //    }

    //    return $this->success($purchase);
    //}
    #endregion
    public async Task<int> invoke()
    {
        return await Task.FromResult(0);
        /************************************************  
         
        ************************************************/
    }
}
