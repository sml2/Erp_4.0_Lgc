﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.Tasks;
[Route("api/[controller]")]
[ApiController]
public class GetTaskID : ControllerBase
{
    #region ERP3.0 PHP
    //public function verifyParams()
    //{
    //    return ['SID'];
    //}

    //public function getApiId()
    //{
    //    return 233;
    //}
    //public function __construct()
    //{
    //    parent::__construct();
    //}

    //public function handle($requestData, $userId, $rule, $session)
    //{
    //    //  SID --->店铺ID
    //    $SID = $requestData['SID'];

    //    $stores = Cache::instance()->storeCompany()->get();

    //    if (!$stores){
    //        return $this->fail(self::StoreNotExist);
    //    }

    //    $stores = arrayCombine($stores, "id");

    //    if (!isset($stores[$SID]))
    //    {
    //        return $this->fail(self::StoreNotExist);
    //    }

    //    $time = request()->input('time');
    //    $sessionObj = $this->getSessionObj();
    //    $user_id = $sessionObj->user_id;
    //    $company_id = $sessionObj->company_id;
    //    $oem_id = $sessionObj->oem_id;
    //    $group_id = $sessionObj->group_id;
    //    $tasks_ids =  $this->GetTasksIDS($time,$SID,$user_id,$group_id,$company_id,$oem_id);
    //    if ($tasks_ids){
    //        return $this->success([
    //            "TaskID" => $tasks_ids
    //        ]);
    //    }else
    //    {
    //        return $this->fail(self::TaskIsEmpty);
    //    }
    //}
    #endregion
    public async Task<int> invoke()
    {
        return await Task.FromResult(0);
        /************************************************  
         ================/tasks/getTaskID
select `id`, `i_operate_id`, `updated_at` from `sync_upload` where (`i_store_id` = ? and `t_state` = ? and `updated_at` < ?)
select `id` from `sync_upload` where `i_store_id` = ? and `t_state` = ? and `i_operate_id` = ?
select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
update `store` set `d_last_auto_time` = ?, `store`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
update `store` set `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
select `i_store_id` from `to_sell_task` where `i_company_id` = ? and `t_is_finish` = ? and `i_oem_id` = ?
select `id`, `i_category_id`, `i_sub_category_id`, `i_distribution_category_id`, `i_distribution_sub_category_id`, `u_lp_n`, `u_lp_k`, `u_lp_s`, `u_lp_d`, `u_price`, `u_quantity`, `d_languages`, `b_language`, `d_source`, `d_ext`, `d_attribute`, `d_variants`, `d_images`, `b_type`, `updated_at`, `d_pid`, `d_coin`, `d_rate` from `product` where (`i_company_id` = ? and `id` = ? and `i_oem_id` = ?) limit *
select * from `company` where `company`.`id` = ? limit *
select * from `store` where `i_company_id` = ? and `t_flag` = ? and `t_is_del` = ? and `t_is_verify` = ?
update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `d_tracking` = ?, `waybill`.`updated_at` = ? where `id` = ?
select `id`, `d_button_name` from `waybill` where ((`i_company_id` = ? and `i_operate_company_id` = ?) or `i_waybill_company_id` = ?) and `d_button_name` in (?, ?) order by `updated_at` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?)
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?)
select `id`, `d_title`, `i_user_id`, `t_state` from `work_order` where `i_company_id` = ? and `t_state` in (?, ?) limit *
update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `waybill`.`updated_at` = ? where `id` = ?

        ************************************************/
    }
}
