﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.Tasks;
[Route("api/[controller]")]
[ApiController]
public class AddSession : ControllerBase
{
    #region ERP3.0 PHP
    //const Err_DATABASE = 1;
    //const Err_Log_Session = 2;
    ///**
    // * @var SyncUploadService
    // */
    //private SyncUploadService $SyncUploadService;
    ///**
    // * @var SyncSessionLogService
    // */
    //private SyncSessionLogService $SyncSessionLogService;
    ///**
    // * @var SyncAliveSessionService
    // */
    //private SyncAliveSessionService $SyncAliveSessionService;

    //public function __construct()
    //{
    //    $this->SyncUploadService = new SyncUploadService();
    //    $this->SyncAliveSessionService = new SyncAliveSessionService();
    //    $this->SyncSessionLogService = new SyncSessionLogService();
    //}

    //public function verifyParams()
    //{
    //    return ['ID', 'Index', 'SessionID'];
    //}

    //public function getApiId()
    //{
    //    return 240;
    //}

    //public function handle($requestData, $userId, $rule, $session)
    //{
    //    $uploadID = (int)$requestData["ID"];
    //    $seIndex = (int)$requestData["Index"];
    //    $session = $requestData["SessionID"];
    //    $sessionObj = $this->getSessionObj();
    //    $company_id = $sessionObj->company_id;
    //    $user_id = $sessionObj->user_id;
    //    $group_id = $sessionObj->group_id;
    //    $oem_id = $sessionObj->oem_id;
    //    if ($seIndex >= 0 && $seIndex <= 5){

    //        $Sessions = ['product', 'variant', 'pricing', 'inventory', 'image', 'relationship'];
    //        $SessionKey = $Sessions[$seIndex];
    //        $updateData = [
    //            "{$SessionKey}_id" => $session,
    //            "{$SessionKey}_state" => SyncUploadModel::SUBMITTED
    //        ];
    //        DB::beginTransaction();
    //        try
    //        {
    //                $count = $this->SyncUploadService->updateData(
    //                    array(["id", '=',$uploadID]),
    //                    $updateData
    //                );

    //                $this->SyncSessionLogService->insert(
    //                    [
    //                        'upload_id'         =>  $uploadID,
    //                        'index'             =>  $seIndex,
    //                        'state'             =>  SyncUploadModel::SUBMITTED,
    //                        'company_id'        =>  $company_id,
    //                        'user_id'           =>  $user_id,
    //                        'group_id'          =>  $group_id,
    //                        'oem_id'            =>  $oem_id,
    //                        'last_operate_id'   =>  $user_id,
    //                        'count'             =>  $count,
    //                        'server_old_state'  =>  null,//添加时
    //                        'client_old_state'  =>  null,//添加时
    //                        'invoke_api'        =>  'tasks/add_session',
    //                        'index_text'        =>  $SessionKey,
    //                        'state_text'        =>  SyncUploadModel::SUBMITTED//具体意义待补充
    //                    ]
    //                );

    //                $this->SyncAliveSessionService->insert(
    //                    [
    //                        'upload_id'         =>  $uploadID,
    //                        'index'             =>  $seIndex,
    //                        'state'             =>  SyncUploadModel::SUBMITTED,
    //                        'company_id'        =>  $company_id,
    //                        'user_id'           =>  $user_id,
    //                        'group_id'          =>  $group_id,
    //                        'oem_id'            =>  $oem_id,
    //                        'last_operate_id'   =>  $user_id,
    //                        'count'             =>  $count,
    //                        'server_old_state'  =>  null,//添加时
    //                        'client_old_state'  =>  null,//添加时
    //                        'invoke_api'        =>  'tasks/add_session',
    //                        'index_text'        =>  $SessionKey,
    //                        'state_text'        =>  SyncUploadModel::SUBMITTED//具体意义待补充
    //                    ]
    //                );

    //            /** 调用日志记录开始 */
    //            $tasksInfo = $this->SyncUploadService->info([],$uploadID)->first();
    //            try
    //            {//包了两层的 try catch

    //                $insert_data_invoke_api_log = array(
    //                    'upload_id'         => $uploadID,
    //                    'request_data'      => json_encode($requestData),
    //                    'upload_operate_id' => $tasksInfo['operate_id'],
    //                    'user_id'           => $sessionObj->user_id,
    //                    'company_id'        => $sessionObj->company_id,
    //                    'group_id'          => $sessionObj->group_id,
    //                    'oem_id'            => $sessionObj->oem_id,
    //                    'api'               => SyncInvokeApiLogModel::API_ADD_SESSION
    //                );

    //                SyncInvokeApiLogModel::query()->create($insert_data_invoke_api_log);
    //            }
    //            catch (\Exception $e){
    //                //            dd($e->getMessage());
    //            }
    //            /** 调用日志记录结束 */

    //            DB::commit();

    //            return $this->success();

    //            }
    //            catch (\Exception $e){
    //                DB::rollBack();
    //            $this->sqlError($requestData,$e->getMessage(), request()->getRequestUri());
    //                return $this->fail(self::Err_DATABASE, null,$e->getMessage());
    //            }
    //            }else
    //            {
    //                return $this->fail(self::ERR_PARAMETER,$seIndex, "Index[$seIndex] OUT OF RANGE");
    //            }
    //        }
    #endregion
    public async Task<int> invoke()
    {
        return await Task.FromResult(0);
        /************************************************  
         ================/tasks/addSession
update `sync_upload` set `d_product_id` = ?, `t_product_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
insert into `sync_session_log` (`i_upload_id`, `d_index`, `d_state`, `i_company_id`, `i_user_id`, `i_group_id`, `i_oem_id`, `i_last_operate_id`, `d_count`, `d_server_old_state`, `d_client_old_state`, `d_invoke_api`, `d_index_text`, `d_state_text`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
insert into `sync_alive_session` (`i_upload_id`, `d_index`, `d_state`, `i_company_id`, `i_user_id`, `i_group_id`, `i_oem_id`, `i_last_operate_id`, `d_count`, `d_server_old_state`, `d_client_old_state`, `d_invoke_api`, `d_index_text`, `d_state_text`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select * from `sync_upload` where `id` = ? limit *
select column_name as `column_name` from information_schema.columns where table_schema = ? and table_name = ?
insert into `sync_invoke_api_log` (`i_upload_id`, `d_request_data`, `i_upload_operate_id`, `i_user_id`, `i_company_id`, `i_group_id`, `i_oem_id`, `t_api`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
update `store` set `d_last_auto_time` = ?, `store`.`updated_at` = ? where `id` = ?

        ************************************************/
    }
}
