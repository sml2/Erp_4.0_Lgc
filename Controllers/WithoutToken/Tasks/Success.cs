﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.Tasks;
[Route("api/[controller]")]
[ApiController]
public class Success : ControllerBase
{
    #region ERP3.0 PHP
    //const UpdateFailed = 1;
    //const SyncMwsNotFound = 2;
    ///**
    // * @var SyncMwsService
    // */
    //private SyncMwsService $SyncMwsService;
    ///**
    // * @var SyncProductService
    // */
    //private SyncProductService $SyncProductService;
    ///**
    // * @var SyncUploadService
    // */
    //private SyncUploadService $SyncUploadService;

    //public function __construct()
    //{
    //    $this->SyncProductService = new SyncProductService();
    //    $this->SyncMwsService = new SyncMwsService();
    //    $this->SyncUploadService = new SyncUploadService();
    //}

    //public function getApiId()
    //{
    //    return 265;
    //}

    //public function verifyParams()
    //{
    //    return ['ID', 'SkuToAsinJson', 'TasksID'];
    //}

    //public function handle($requestData, $userId, $rule, $session)
    //{
    //    //Success上报多个Asin(tasks/success):
    //    //参数：
    //    //ID(Int)
    //    //SkuToAsinJson(string) 例如(键值对形式序列化而来的):{"Sku-1":"Asin1","Sku2":"Asin2","Sku3":"Asin3"}
    //    $mws_id = $requestData['ID'];
    //    $skuToAsinJson = $requestData['SkuToAsinJson'];
    //    $upload_id = $requestData['TasksID'];
    //    /** 调用日志记录开始 */
    //    try
    //    {
    //        $sessionObj = $this->getSessionObj();
    //        $tasksInfo = $this->SyncUploadService->info([],$upload_id)->first();
    //        $insert_data_invoke_api_log = array(
    //            'upload_id'         => $upload_id,
    //            'request_data'      => json_encode($requestData),
    //            'upload_operate_id' => $tasksInfo['operate_id'],
    //            'user_id'           => $sessionObj->user_id,
    //            'company_id'        => $sessionObj->company_id,
    //            'group_id'          => $sessionObj->group_id,
    //            'oem_id'            => $sessionObj->oem_id,
    //            'api'               => SyncInvokeApiLogModel::API_SUCCESS
    //        );
    //        SyncInvokeApiLogModel::query()->create($insert_data_invoke_api_log);
    //    }
    //    catch (\Exception $e){
    //        Log::channel('TasksError')->info(
    //            '================上传错误日志！================ (Success接口)',
    //            $logData = [
    //                'route'    => request()->getRequestUri() ?? '',
    //                'codeLine' => 'Line82',
    //                'errorMsg' => $e->getMessage(),
    //                'errorLine' => $e->getLine(),
    //                'request_data' => $requestData
    //            ]
    //        );
    //    }
    //    /** 调用日志记录结束 */

    //    $skuToAsin = json_decode($skuToAsinJson, true);

    //    try
    //    {
    //        $mws_data = $this->SyncMwsService->info(["id", "sku_change_data", "spid"],$mws_id)->first();
    //        if ($mws_data){
    //            $sync_product_data = $this->SyncProductService->info(["*"],$mws_data['spid'])->first();
    //            $product_infos = $products = Product::query()->where(
    //                [
    //                    ['oem_id', $this->getSessionObj()->getOemId()],
    //                    ['company_id', $this->getSessionObj()->getCompanyId()],
    //                    ['id',$sync_product_data['pid']]
    //                ]
    //            )->first()->toArray();
    //            $sku_to_index = (new Result())->getSku($product_infos,$sync_product_data);
    //            $skuChangeData = (new Result())->getSkuChangeData($mws_data['sku_change_data'],$sku_to_index,$sync_product_data);
    //        }else
    //        {
    //            return  $this->fail(self::SyncMwsNotFound, '', '任务不存在');
    //        }
    //        foreach ($skuChangeData as &$skd){
    //            if (array_key_exists($skd["Sku"],$skuToAsin))
    //            {
    //                $skd["Asin"] = $skuToAsin[$skd["Sku"]];
    //            }
    //        }
    //        /**
    //         * 将Sku还原的操作 还原成数据库存储形式
    //         */
    //        $skuChangeData = (new Result())->recoverySkuChangeData($skuChangeData,$sku_to_index,$sync_product_data);
    //        $update_data = [
    //            "sku_change_data"  => $skuChangeData
    //        ];
    //        $this->SyncMwsService->updateData([["id", "=",$mws_id]],$update_data);
    //        return $this->success();
    //    }
    //    catch (\Exception $e){
    //        return $this->fail(self::UpdateFailed, null,$e->getMessage());
    //    }
    //    }
    #endregion
    public async Task<int> invoke()
    {
        return await Task.FromResult(0);
        /************************************************  
         ================/tasks/success
select * from `sync_upload` where `id` = ? limit *
select column_name as `column_name` from information_schema.columns where table_schema = ? and table_name = ?
insert into `sync_invoke_api_log` (`i_upload_id`, `d_request_data`, `i_upload_operate_id`, `i_user_id`, `i_company_id`, `i_group_id`, `i_oem_id`, `t_api`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_sku_change_data`, `i_spid` from `sync_mws` where `id` = ? limit *
select * from `sync_product` where `i_company_id` = ? and `id` = ? limit *
select * from `product` where (`i_oem_id` = ? and `i_company_id` = ? and `id` = ?) limit *
update `sync_mws` set `d_sku_change_data` = ?, `sync_mws`.`updated_at` = ? where (`id` = ?)
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
update `store` set `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
update `store` set `d_last_auto_time` = ?, `store`.`updated_at` = ? where `id` = ?
select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `company` where `t_state` = ? and `i_report_id` = ? and `id` != ?
select `id`, `d_name` from `task` where (`i_company_id` = ? and `i_oem_id` = ?) order by `created_at` desc
select * from `category` where (`i_company_id` = ? and `i_oem_id` = ? and `t_type` = ? and `t_level` = ?)
select count(*) as aggregate from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_oem_id` = ?) and `updated_at` between ? and ?
select `id`, `i_oem_id`, `i_user_id`, `i_company_id`, `i_oem_id`, `i_category_id`, `i_platform_id`, `d_reason`, `u_quantity`, `u_lp_n`, `u_image`, `u_price`, `d_source`, `b_type`, `b_language`, `d_languages`, `updated_at`, `created_at`, `d_coin`, `d_rate` from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_oem_id` = ?) and `updated_at` between ? and ? order by `id` desc limit * offset *
select * from `category` where `category`.`id` in (?)
select * from `task` where `task`.`id` in (?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `d_variant_id`, `t_variant_state`, `id`, `i_operate_id` from `sync_upload` where `id` = ? limit *
update `sync_upload` set `t_variant_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
select * from `waybill` where `waybill`.`id` = ? limit *
insert into `sync_session_log` (`i_upload_id`, `d_index`, `d_state`, `i_company_id`, `i_user_id`, `i_last_operate_id`, `i_group_id`, `i_oem_id`, `d_count`, `d_server_old_state`, `d_client_old_state`, `d_invoke_api`, `d_index_text`, `d_state_text`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
update `sync_alive_session` set `d_state` = ?, `d_state_text` = ?, `i_last_operate_id` = ?, `i_user_id` = ?, `d_count` = ?, `sync_alive_session`.`updated_at` = ? where (`i_upload_id` = ? and `d_index` = ?)
update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `d_tracking` = ?, `waybill`.`updated_at` = ? where `id` = ?
select * from `product` where `id` = ? limit *
select count(*) as aggregate from `product` where (`d_hash_code` = ? and `i_company_id` = ? and `id` != ?) and !(b_type&*) AND !(b_type&*) AND !(b_type&*)
update `product` set `b_type` = ?, `u_price` = ?, `d_variants` = ?, `d_rate` = ?, `product`.`updated_at` = ? where `id` = ?
select `id`, `d_button_name` from `waybill` where ((`i_company_id` = ? and `i_operate_company_id` = ?) or `i_waybill_company_id` = ?) and `d_button_name` in (?, ?) order by `updated_at` asc limit *
select count(*) as aggregate from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_oem_id` = ?) and `i_task_id` = ? and `updated_at` between ? and ?
select `id`, `i_oem_id`, `i_user_id`, `i_company_id`, `i_oem_id`, `i_category_id`, `i_platform_id`, `d_reason`, `u_quantity`, `u_lp_n`, `u_image`, `u_price`, `d_source`, `b_type`, `b_language`, `d_languages`, `updated_at`, `created_at`, `d_coin`, `d_rate` from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_oem_id` = ?) and `i_task_id` = ? and `updated_at` between ? and ? order by `id` desc limit * offset *
select `id`, `d_title`, `i_user_id`, `t_state` from `work_order` where `i_company_id` = ? and `t_state` in (?, ?) limit *
select `id`, `i_category_id`, `i_sub_category_id`, `i_distribution_category_id`, `i_distribution_sub_category_id`, `u_lp_n`, `u_lp_k`, `u_lp_s`, `u_lp_d`, `u_price`, `u_quantity`, `d_languages`, `b_language`, `d_source`, `d_ext`, `d_attribute`, `d_variants`, `d_images`, `b_type`, `updated_at`, `d_pid`, `d_coin`, `d_rate` from `product` where (`i_company_id` = ? and `id` = ? and `i_oem_id` = ?) limit *
insert into `reporter_request` (`created_at`, `d_action`, `d_cpu_time`, `d_method`, `d_module`, `d_name`, `d_namespace`, `d_net_time`, `d_request_id`, `d_request_time`, `d_uri`, `i_company_id`, `i_group_id`, `i_oem_id`, `i_user_id`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
update `waybill` set `waybill`.`updated_at` = ? where `id` in (?, ?, ?, ?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `d_inventory_id`, `t_inventory_state`, `id`, `i_operate_id` from `sync_upload` where `id` = ? limit *
update `sync_upload` set `t_inventory_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)

        ************************************************/
    }
}
