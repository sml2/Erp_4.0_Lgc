﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.Tasks;
[Route("api/[controller]")]
[ApiController]
public class State : ControllerBase
{
    #region ERP3.0 PHP
    //    const TransactionFail = 1;
    //    const UpdateFailed = 2;
    //    const SyncMwsNotFound = 3;

    //    /**
    //     * @var SyncMwsService
    //     */
    //    private SyncMwsService $SyncMwsService;
    //    /**
    //     * @var SyncProductService
    //     */
    //    private SyncProductService $SyncProductService;
    //    /**
    //     * @var SyncUploadService
    //     */
    //    private SyncUploadService $SyncUploadService;
    //    /**
    //     * @var ApiGetController
    //     */
    //    private ApiGetController $ApiGetController;


    //    public function __construct()
    //    {
    //        $this->SyncMwsService = new SyncMwsService();
    //        $this->SyncUploadService = new SyncUploadService();
    //        $this->SyncProductService = new SyncProductService();
    //        $this->ApiGetController = new ApiGetController();
    //    }

    //    public function getApiId()
    //    {
    //        return 255;
    //    }

    //    /**
    //     * Tasks_id,Session_index,(bool)Success->true Failed->false,[AnyInfo]
    //     * @return array|string[]
    //     */
    //    public function verifyParams()
    //    {
    //        return ['ID', 'Index', 'Success', 'AnyInfo'];
    //    }

    //    public function handle($requestData, $userId, $rule, $session)
    //    {
    //        $upload_id = $requestData['ID'];
    //        $session_index = $requestData['Index'];
    //        $success = $requestData['Success'];
    //        $any_info = $requestData['AnyInfo'];
    //        $mws_res = $this->SyncMwsService->info(["id", "spid", "sku_change_data"],'','',$upload_id)->get();
    //        if ($mws_res && $mws_res = $mws_res->toArray()){
    //            DB::beginTransaction();
    //            try
    //            {
    //                $spids = [];
    //                foreach ($mws_res as $mws){
    //                    $spids[] = $mws['spid'];
    //                    if ($success){
    ////                        $mwsid = $mws['id'];
    ////                        $result =  $this->dealSkuChangeInfo($mws['id'],$mws['sku_change_data']);
    //                        $this->dealSkuChangeInfo($mws['id'],$mws['sku_change_data']);
    //                    }else
    //                    {//填充Upload失败原因
    //                        $this->SyncMwsService->updateData([["upload_id", "=",$upload_id]], ["state" => SyncMwsModel::STATE_ERROR]);
    //                    }
    //                }
    //                $uploaddata = ["state"=>SyncUploadModel::STATE_TRUE];
    //                if (!$success){
    //                    $uploaddata['error_index'] = $session_index;
    //                    $uploaddata['error_info'] = $any_info;
    //                    //当出现店铺层级的错误时，不保留当前upload下的所有会话，保证AliveSession在没有上传任务时应该没有数据；
    //                    SyncAliveSessionModel::query()->where("upload_id", "=",$upload_id)->delete();
    //                }
    //                $this->SyncUploadService->updateData([["id", "=",$upload_id]],$uploaddata);
    //                $session_data = $this->getSessionObj();
    //                $upload_datas = SyncUploadModel::query()->select(["id", "operate_id"])->Id($upload_id)->first();
    //                /** 调用日志记录开始 */
    //                try
    //                {
    //                    $sessionObj = $this->getSessionObj();
    //                    $insert_data_invoke_api_log = array(
    //                        'upload_id'         => $upload_id,
    //                        'request_data'      => json_encode($requestData),
    //                        'upload_operate_id' => $upload_datas['operate_id'],
    //                        'user_id'           => $sessionObj->user_id,
    //                        'company_id'        => $sessionObj->company_id,
    //                        'group_id'          => $sessionObj->group_id,
    //                        'oem_id'            => $sessionObj->oem_id,
    //                        'api'               => SyncInvokeApiLogModel::API_STATE_FINISH
    //                    );
    //                    SyncInvokeApiLogModel::query()->create($insert_data_invoke_api_log);
    //                }
    //                catch (\Exception $e){
    //                    dd($e->getMessage());
    //                }
    //                $insert_data_user = array(
    //                    'upload_id'    => $upload_id,
    //                    'old_user_id'  => $upload_datas['operate_id'],
    //                    'new_user_id'  => $session_data->user_id,
    //                    'last_updated' => null,
    //                    'company_id'   => $session_data->company_id,
    //                    'group_id'     => $session_data->group_id,
    //                    'oem_id'       => $session_data->oem_id,
    //                    'is_changed'   => null,
    //                    'is_in_5_min'  => null,
    //                    'type'         => SyncUserModel::TYPE_END
    //                );

    //                SyncUserModel::query()->create($insert_data_user);
    //                // TODO already_upload没有修改，产品无法删除，不确定是不是想要的设计
    //                SyncProductModel::query()->whereIn("id",$spids)->update(["uploading" => SyncProductModel::UPLOADING_FALSE]);
    //                DB::commit();
    //                return $this->success();
    //                }
    //                catch (\Exception $e){
    //                    DB::rollBack();
    //                    return $this->fail(self::UpdateFailed, null,$e->getMessage());
    //                }
    //                }else
    //                {
    //                    return $this->fail(self::SyncMwsNotFound, null, '任务不存在');//任务不存在
    //                }
    //            }
    //    private function dealSkuChangeInfo($mws_id,$mws_info)
    //            {
    //        $result = SyncMwsModel::STATE_SUCCESS;
    //                foreach ($mws_info as $key => &$skd){
    //                if ($skd["IsMain"]){
    //                    if ($skd['ProductData']['Error'] === null){
    //                    $skd['ProductData']['Action'] > 4 && $skd['ProductData']['Action'] -= 4;
    //                    }else
    //                    {
    //                    $result = SyncMwsModel::STATE_ERROR;
    //                    }
    //                }else
    //                {
    //                    if ($skd['VariantData']['Error'] === null){
    //                        $skd['VariantData']['Action'] > 4 && $skd['VariantData']['Action'] -= 4;
    //                    }else
    //                    {
    //                    $result = SyncMwsModel::STATE_ERROR;
    //                    }
    //                    if ($skd["IsDelete"]){
    //                        unset($mws_info[$key]);
    //                        continue;
    //                    }
    //                    if ($skd['ImageData']['Error'] === null){
    //                    $skd['ImageData']['Action'] > 4 && $skd['ImageData']['Action'] -= 4;
    //                    }else
    //                    {
    //                    $result = SyncMwsModel::STATE_ERROR;
    //                    }
    //                    if ($skd['PricingData']['Error'] === null){
    //                    $skd['PricingData']['Action'] > 4 && $skd['PricingData']['Action'] -= 4;
    //                    }else
    //                    {
    //                    $result = SyncMwsModel::STATE_ERROR;
    //                    }
    //                    if ($skd['InventoryData']['Error'] === null){
    //                    $skd['InventoryData']['Action'] > 4 && $skd['InventoryData']['Action'] -= 4;
    //                    }else
    //                    {
    //                    $result = SyncMwsModel::STATE_ERROR;
    //                    }
    //                    if ($skd['RelationshipData']['Error'] === null){
    //                    $skd['RelationshipData']['Action'] > 4 &&  $skd['RelationshipData']['Action'] -= 4;
    //                    }else
    //                    {
    //                    $result = SyncMwsModel::STATE_ERROR;
    //                    }
    //                }
    //            }
    //        $this->SyncMwsService->updateData([["id", "=",$mws_id]], ["sku_change_data" => array_values($mws_info), "state" => $result]);
    //        }
    #endregion
    public async Task<int> invoke()
    {
        return await Task.FromResult(0);
        /************************************************  
         ================/tasks/state
select `id`, `i_spid`, `d_sku_change_data` from `sync_mws` where `i_upload_id` = ?
update `sync_mws` set `d_sku_change_data` = ?, `t_state` = ?, `sync_mws`.`updated_at` = ? where (`id` = ?)
update `sync_upload` set `t_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
select `id`, `i_operate_id` from `sync_upload` where `id` = ? limit *
select column_name as `column_name` from information_schema.columns where table_schema = ? and table_name = ?
insert into `sync_invoke_api_log` (`i_upload_id`, `d_request_data`, `i_upload_operate_id`, `i_user_id`, `i_company_id`, `i_group_id`, `i_oem_id`, `t_api`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
insert into `sync_user` (`i_upload_id`, `i_old_user_id`, `i_new_user_id`, `d_last_updated`, `i_company_id`, `i_group_id`, `i_oem_id`, `t_is_changed`, `t_is_in_*_min`, `t_type`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
update `sync_product` set `t_uploading` = ?, `sync_product`.`updated_at` = ? where `id` in (?)
select `id`, `d_button_name` from `waybill` where ((`i_company_id` = ? and `i_operate_company_id` = ?) or `i_waybill_company_id` = ?) and `d_button_name` in (?, ?) order by `updated_at` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
update `store` set `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
select * from `waybill` where `waybill`.`id` = ? limit *
update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `d_tracking` = ?, `waybill`.`updated_at` = ? where `id` = ?
select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select * from `task` where (`i_oem_id` = ? and `i_user_id` = ? and `i_group_id` = ? and `d_hash_code` = ?) limit *
select count(*) as aggregate from `product` where (`d_hash_code` = ? and `i_company_id` = ?) and !(b_type&*) AND !(b_type&*) AND !(b_type&*)
insert into `product` (`i_oem_id`, `i_company_id`, `i_user_id`, `i_group_id`, `i_task_id`, `u_lp_n`, `u_lp_k`, `u_lp_s`, `u_lp_d`, `d_source`, `d_hash_code`, `d_languages`, `b_language`, `b_type`, `u_price`, `i_platform_id`, `u_platform`, `d_attribute`, `d_variants`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?)
select * from `product` where `id` = ? and b_type & * limit *
select `id`, `d_title`, `i_user_id`, `t_state` from `work_order` where `i_company_id` = ? and `t_state` in (?, ?) limit *
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `i_store_id` from `to_sell_task` where `i_company_id` = ? and `t_is_finish` = ? and `i_oem_id` = ?
insert into `temp_images` (`d_oss_path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `i_product_id`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select * from `store` where `i_company_id` = ? and `t_flag` = ? and `t_is_del` = ? and `t_is_verify` = ?
select distinct `i_order_id` from `waybill` where `d_waybill_order` = ?
select count(*) as aggregate from `amazon_order` where `i_company_id` = ? and `i_oem_id` = ? and `id` in (?)
select `t_entry_mode`, `id`, `d_receiver_nation`, `d_receiver_nation_short`, `u_url`, `d_earliest_ship_time`, `d_order_no`, `d_pay_money`, `i_company_id`, `d_product_num`, `t_order_state`, `d_create_time`, `t_shipping_type`, `d_fake_product_time`, `d_true_product_time`, `i_store_id`, `id`, `d_coin`, `u_truename`, `d_receiver_name`, `d_receiver_email`, `b_progress`, `d_latest_ship_time`, `i_merge_id`, `d_process_waybill`, `d_process_purchase`, `i_purchase_company_id`, `i_waybill_company_id`, `created_at`, `d_rate`, `t_is_batch_ship`, `d_seller_id`, `d_profit`, `d_invoice` from `amazon_order` where `i_company_id` = ? and `i_oem_id` = ? and `id` in (?) order by `d_create_time` desc, `id` desc limit * offset *
select `id`, `i_order_id`, `d_purchase_tracking_number`, `d_purchase_order_no` from `purchase` where `i_order_id` in (?)
select `id`, `i_order_id`, `d_waybill_order`, `d_express` from `waybill` where `i_order_id` in (?)
update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `waybill`.`updated_at` = ? where `id` = ?
select `d_action`, `d_user_name`, `updated_at` from `waybill_log` where `i_waybill_id` = ?
insert into `reporter_request` (`created_at`, `d_action`, `d_cpu_time`, `d_method`, `d_module`, `d_name`, `d_namespace`, `d_net_time`, `d_request_id`, `d_request_time`, `d_uri`, `i_company_id`, `i_group_id`, `i_oem_id`, `i_user_id`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)

        ************************************************/
    }
}
