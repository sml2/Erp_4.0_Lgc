﻿


namespace ERP.ControllersWithoutToken.Tasks;
//[Route("api/[controller]")]
//[ApiController]
public class GetStore : ControllerBase
{


    #region ERP3.0 PHP
    //const TaskNotExist = 1;
    //const CanNotFoundStoreInCache = 2;
    //const StoreNotExist = 3;
    //const LostStoreKeyId = 4;

    //const THIRD_PART = 1;
    //const TYPE_AMAZON = 1;//亚马逊店铺类型

    ///**
    // * @var $amazonKey
    // */
    //private $amazonKey;
    ///**
    // * @var SyncMwsService
    // */
    //private SyncMwsService $SyncMwsService;

    ///**
    // * GetStore constructor.
    // */
    //public function __construct()
    //{
    //    $this->SyncMwsService = new SyncMwsService();
    //    $this->amazonKey = Cache::instance()->amazonKey()->get();
    //}

    ///**
    // * @return int
    // */
    //public function getApiId()
    //{
    //    return 220;
    //}

    ///**
    // * @param $requestData
    // * @param $userId
    // * @param $rule
    // * @param SessionData $session
    // * @return \Illuminate\Http\JsonResponse
    // */
    //public function handle($requestData, $userId, $rule, SessionData $session)
    //{

    //    $stores = Cache::instance()->storeCompany()->get();

    //    if (!$stores){
    //        return $this->fail(self::StoreNotExist);
    //    }

    //    $stores = arrayCombine($stores, "id");

    //    $res = SyncMwsModel::query()->selectRaw("count(*) as count")
    //        ->addSelect("store_id")
    //        ->whereIn(
    //            "state",[
    //                SyncMwsModel::STATE_WAITING,
    //                SyncMwsModel::STATE_EXECUTING
    //            ]
    //        )
    //        ->where('company_id',$session->getCompanyId())
    //        ->where('oem_id',$session->getOemId())
    //        ->groupBy("store_id")
    //        ->get()->toArray();

    //    $res = arrayCombine($res, "store_id");
    //    //array:1 [
    //    //  1 => array:2 [
    //    //    "count" => 1
    //    //    "store_id" => 1
    //    //  ]
    //    //]
    //    $ret = [];
    //    $noKeyCount = 0;

    //    foreach ($stores as $store){
    //        foreach ($res as $key =>$value){
    //            if ($store['id'] == $key){
    //                $count = $value;
    //                $id = $store['id'];
    //                $storeData =$store['data'];
    //                if (array_key_exists($id,$stores))
    //                {
    //                    $store = $stores[$id];
    //                    $storeData =$store['data'];
    //                    $keyID = $this->getCountryKeyID($store['nation_id']);
    //                    if (isset($storeData["choose"]) && $storeData["choose"] == self::THIRD_PART) {
    //                        $keyID = $this->getCountryKeyID($store['nation_id']);
    //                        if (array_key_exists($keyID,$this->amazonKey))
    //                        {
    //                            $keyData = $this->amazonKey[$keyID];
    //                            $storeData['aws_access_key'] = $keyData["aws_access_key"];
    //                            $storeData['secret_key'] = $keyData["secret_key"];
    //                        }
    //                        else
    //                        {
    //                            $noKeyCount += 1;
    //                            continue;
    //                        }
    //                    }
    //                    if ($store['type'] == self::TYPE_AMAZON){
    //                        $ret[] = [
    //                            'ID'            => $store['id'],
    //                            'Name'          => $store['name'],
    //                            'Count'         => $count,
    //                            'Marketplace'   => $storeData['marketplace'],
    //                            'SellerID'      => $storeData['seller_id'],
    //                            'MwsAuthToken'  => $storeData['mws_auth_token'],
    //                            'AwsAccessKey'  => $storeData['aws_access_key'],
    //                            'SecretKey'     => $storeData['secret_key'],
    //                        ];
    //                    }
    //                }
    //                else
    //                {
    //                    return $this->fail(self::CanNotFoundStoreInCache,['id'=>$id], "Can Not Found Store[{$id}] In Cache");
    //                }
    //            }
    //        }
    //    }
    //    $info = $noKeyCount ? "{$noKeyCount}个店铺丢失通讯权限" : null;
    //    return checkArr($ret) ? $this->success($ret, $info) : $this->fail(self::TaskNotExist);
    //}

    //public function getCountryKeyID($nation_id)
    //{
    //    $country = Cache::instance()->amazonNation()->get();
    //    $country = arrayCombine($country, "id");
    //    if (isset($country[$nation_id]))
    //    {
    //        return $country[$nation_id]['key_id'];
    //    }
    //    else
    //    {
    //        return $this->fail(self::LostStoreKeyId, null, "Can Not Find Store Key Id");
    //    }
    //}
    #endregion
    public async Task<int> invoke()
    {
        return await Task.FromResult(0);


        /************************************************  
         ================/tasks/store
select count(*) as count, `i_store_id` from `sync_mws` where `t_state` in (?, ?) and `i_company_id` = ? and `i_oem_id` = ? group by `i_store_id`
update `store` set `d_last_auto_time` = ?, `store`.`updated_at` = ? where `id` = ?
select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
update `store` set `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?

        ************************************************/
    }
}
