﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.Tasks;
[Route("api/[controller]")]
[ApiController]
public class Get : ControllerBase
{
    #region ERP3.0 PHP
    //    use ProductCommon;

    //    const TaskIsEmpty = 1;
    //    const TransactionFail = 2;
    //    const ProductInfoIsEmpty = 3;
    //    const MwsDataIsNotFound = 4;
    //    const NoAuthority = 5;
    //    const StoreNotExist = 6;
    //    const UPDATE_AUTO_GENERATE_STATE_FAILED = 7;

    //    //设定的每次任务处理的最大产品数量
    //    const TasksMaxExcuteProducts = 20;

    //    /**
    //     * @var SyncMwsService
    //     */
    //    private SyncMwsService $SyncMwsService;
    //    /**
    //     * @var SyncUploadService
    //     */
    //    private SyncUploadService $SyncUploadService;
    //    /**
    //     * @var ProductService
    //     */
    //    private ProductService $ProductService;
    //    /**
    //     * @var SyncOnlineService
    //     */
    //    private SyncOnlineService $SyncOnlineService;
    //    /**
    //     * @var SyncProductService
    //     */
    //    private SyncProductService $SyncProductService;
    //    /**
    //     * @var SyncUserService
    //     */
    //    private SyncUserService $SyncUserService;
    //    //主产品完整sku
    //    private $mainSku = '';
    //    //临时下标对应变体sku
    //    protected $variantSku = [];

    //    //是否需要继续请求此接口
    //    protected $hasNext = false;
    //    //本次请求已经处理的产品数量
    //    protected $hasExcuteProduct = 0;

    //    public function __construct()
    //    {
    //        $this->SyncMwsService = new SyncMwsService();
    //        $this->SyncUploadService = new SyncUploadService();
    //        $this->SyncProductService = new SyncProductService();
    //        $this->ProductService = new ProductService();
    //        $this->SyncOnlineService = new SyncOnlineService();
    //        $this->SyncUserService = new SyncUserService();
    //    }

    //    public function verifyParams()
    //    {
    //        return ['SID', 'Type'];
    //    }

    //    public function getApiId()
    //    {
    //        return 230;
    //    }

    //    public function handle($requestData, $userId, $rule, $session)
    //    {
    //        //  SID --->店铺ID
    //        //  Type --->任务类型 1 重载五分钟未操作的未完成业务
    //        //  Type --->任务类型 2 构造任务 + 重载五分钟未操作的未完成业务

    //        $sid = $requestData['SID'];

    //        $stores = Cache::instance()->storeCompany()->get();

    //        if (!$stores) {
    //            return $this->fail(self::StoreNotExist);
    //        }

    //        $stores = arrayCombine($stores, "id");

    //        if (!isset($stores[$sid]))
    //        {
    //            return $this->fail(self::StoreNotExist);
    //        }

    //        $currency = $stores[$sid]['unit_sign'];
    //        $type = $requestData['Type'];
    //        $time = request()->input('time');
    //        $sessionObj = $this->getSessionObj();
    //        $user_id = $sessionObj->user_id;
    //        $company_id = $sessionObj->company_id;
    //        $oem_id = $sessionObj->oem_id;
    //        $group_id = $sessionObj->group_id;
    //        $upload_origin_state = SyncMwsModel::STATE_WAITING;
    //        $TaskIDs = [];
    //        if ($type == 2) {
    //            $TaskIDs = self::createUploadTasks(false, $sid, $user_id, $company_id, $oem_id, $group_id, $upload_origin_state, $time);
    //        }
    //        $TaskIDs = $this->GetTasksIDS($time,$sid,$user_id,$group_id,$company_id,$oem_id,$TaskIDs);
    //        $taskCount = count($TaskIDs);

    //        $excuteTasksCount = 0;
    //        if ($taskCount) {
    //            $TasksData = [];
    //            foreach ($TaskIDs as $tasksID) {
    //                //如果已经处理超过设定的一次性处理的最大产品数量，就跳出
    //                if ($this->hasExcuteProduct >= self::TasksMaxExcuteProducts) {//5
    //                    $this->hasNext = true;
    //                    break;
    //                } else
    //                {
    //                    $excuteTasksCount++;
    //                    //任务集中的任务不好计算总数
    //                    $DataOrResp = $this->GetTasksInfo($tasksID, $currency, $sessionObj, $requestData);
    //                    if ($this->needreturn) {
    //                        return $DataOrResp;
    //                    } else
    //                    {
    //                        $TasksData[] = $DataOrResp;
    //                    }
    //                }
    //            }
    //            return $this->success(
    //                [
    //                    'SID'   => $sid,
    //                    'Count' => $excuteTasksCount,//这个值需要改
    //                    'Data'  => $TasksData //[Tasks]
    //                ],
    //                [
    //                    "HasNext" => $this->hasNext,
    //                ]
    //            );
    //        } else
    //        {
    //            return $this->fail(self::TaskIsEmpty);
    //        }
    //    }

    //    protected function GetTasksIDS($time,$sid,$user_id,$group_id,$company_id,$oem_id,$TaskIDs=[])
    //    {
    //        $_time_ = 10 * 60;
    ////        $_time_ = 2*60;
    //        $beforeTime = date("Y-m-d H:i:s", $time - $_time_);
    ////        $beforeTime =date("Y-m-d H:i:s",$time);

    //        $where = [
    //            ['store_id', '=', $sid],
    //            ['state', '=', SyncUploadModel::STATE_FALSE],
    //            ['updated_at', '<', $beforeTime],
    //        ];
    //        /*****pluck("spid")->all();**/
    //        //获取upload_id对应的之前的操作人以及最后更改时间
    //        $will_be_changed_user_ids = SyncUploadModel::query()->select(["id", "operate_id", "updated_at"])->where($where)->get();
    //        if ($will_be_changed_user_ids) {
    //            $will_be_changed_user_ids = $will_be_changed_user_ids->toArray();
    //            foreach ($will_be_changed_user_ids as $_wbcus_k => $_wbcus_v) {
    //                if ($_wbcus_v['operate_id'] != $user_id) {
    //                    if (UserOnline::query()->UserId($_wbcus_v['operate_id'])->exists())
    //                    {
    //                        unset($will_be_changed_user_ids[$_wbcus_k]);
    //                    }
    //                }
    //            }
    //            if ($will_be_changed_user_ids) {
    //                $_tasks_ids = array_unique(array_column($will_be_changed_user_ids, "id"));
    //            }
    //        }
    //        if (isset($_tasks_ids))
    //        {
    //            if (
    //            SyncUploadModel::query()->whereIn('id', $_tasks_ids)->update(['operate_id' => $user_id, "updated_at" => date("Y-m-d H:i:s", $time)])
    ////            $this->SyncUploadService->updateData($where,);
    //            ) {
    //                //        dd($will_be_changed_user_ids,$_tasks_ids);
    ////        $this->SyncUploadService->updateData($where,['operate_id'=>$user_id]); //TODO-- 测试方便先注释
    //                //分配逻辑 当任务持有人和调用用户不一致的时候,判断之前任务持有人是否在线，若不在线则可分配给此次调用接口的用户
    //                //分配逻辑 当任务持有人和调用用户一致的时候 ====多线程会多次调用此接口==== 不过滤，还分配当前用户

    ////          if($will_be_changed_user_ids){
    //                $insert_data_user = [];
    //                //                    dd($will_be_changed_user_ids);
    //                foreach ($will_be_changed_user_ids as $old_upload_data) {
    //                    if ($old_upload_data['operate_id'] == $user_id) {
    //                        $is_changed = 2;
    //                    } else
    //                    {
    //                        $is_changed = 1;
    //                    }
    //                    $last_updated = date("Y-m-d H:i:s", strtotime($old_upload_data['updated_at']));
    //                    /* 现在时间 减去 之前最近操作时间 小于 5min */
    //                    if ($time - strtotime($old_upload_data['updated_at']) < $_time_) {
    //                        $is_in_5_min = 1;
    //                    } else
    //                    {
    //                        $is_in_5_min = 2;
    //                    }
    //                    $request_time = date("Y-m-d H:i:s", $time);
    //                    $insert_data_user[] = [
    //                        'upload_id'    => $old_upload_data['id'],
    //                        'old_user_id'  => $old_upload_data['operate_id'],
    //                        'new_user_id'  => $user_id,
    //                        'last_updated' => $last_updated,
    //                        'company_id'   => $company_id,
    //                        'group_id'     => $group_id,
    //                        'oem_id'       => $oem_id,
    //                        'is_changed'   => $is_changed,
    //                        'is_in_5_min'  => $is_in_5_min,
    //                        'updated_at'   => $request_time,
    //                        'created_at'   => $request_time,
    //                        'type'         => SyncUserModel::TYPE_EDIT,
    //                    ];
    //                    $is_in_5_min = '';
    //                    $is_changed = '';
    //                }
    //                if ($insert_data_user) {
    //                    try
    //                    {
    //                        SyncUserModel::query()->insert($insert_data_user);
    //                    }
    //                    catch (\Exception $e) {
    //                    }
    //                    }
    //                    //            }
    //                }
    //            }

    //        //!!!以上的代码只是处理更换任务用户的情况(包括自己更换自己)，和下面的逻辑要区分
    //        //获取所有此店铺下有未完成任务的id
    //        $ids = $this->SyncUploadService
    //            ->info(["id"], '', $sid, SyncUploadModel::STATE_FALSE)
    //            ->where('operate_id', $user_id)
    //            ->get();
    //            //重置error信息为null ==> GetTasksInfo(放这里操作)
    //            foreach ($ids as $k => $v) {
    //            $TaskIDs[] = $v['id'];//upload表id
    //            }
    //            //此处修复了重载老任务时会将新任务拉取到的情况,将重复的id处理掉
    //            return array_unique($TaskIDs);
    //        }
    //        /**
    //         * 获取sync_mws表详情信息
    //         */
    //        /**
    //         * @param $upload_id
    //         * @param $currency
    //         * @param $sessionObj
    //         * @param $requestData
    //         * @return array|\Illuminate\Http\JsonResponse
    //         */
    //        protected function GetTasksInfo($upload_id, $currency, $sessionObj, $requestData)
    //    {
    //        //根据 mws 表中的 upload_id 取出 upload 表中的数据;
    //        $company_id = $this->getSessionObj()->company_id;
    //        $tasks = $this->SyncMwsService->info([], '', '', $upload_id, '')->get();
    //        if ($tasks) {
    //            $tasks = $tasks->toArray();
    //            $this->hasExcuteProduct += count($tasks);
    //            //dump($this->hasExcuteProduct);
    //            $data = [];
    //            $TempSID = null;
    //            foreach ($tasks as $task) {
    //                //让sync_product表中的变体和产品数据不可修改操作 已提前到构建任务时修改
    //                //构建任务时错误信息清null -- 任务在上传过程中会被重新调用这个接口，导致已经存在的skuchangedata内的错误信息被清空，已经转移到将失败任务转为待执行时转换。
    //                $TempSID = $task["store_id"];
    //                $sync_product_data = $this->SyncProductService->info(["pid", "id", "sku_type", "sku_suffix", "title_suffix", "source_id", "sku", "type_data", "type_data_id"], $task['spid'])->first();

    //                $field = [];

    //                $productInfo = $this->ProductService->apiInfo($field, $sync_product_data['pid'], '', '', $company_id)->first();
    ////            dd($productInfo);
    //                if (!$productInfo) {

    //                    return $this->fail(self::ProductInfoIsEmpty, null, "Err : Product Information is Empty Or Can Not Found Product");

    //                } else {
    //                    /**
    //                     * 不是本公司无权操作
    //                     */
    //                    if ($productInfo['company_id'] != $company_id) {

    //                        return $this->fail(self::NoAuthority, null, "Err : The Current User Does Not Have Access To The Item ");

    //                    }
    //                    /**
    //                     * 价格处理
    //                     */
    //                    if (isset($productInfo->priceOriginal)) {

    //                        $origin_price = json_decode($productInfo->priceOriginal, true);

    //                        $origin_price['cost']['max'] = ToConversion($origin_price['cost']['max'], $currency);
    //                        $origin_price['cost']['min'] = ToConversion($origin_price['cost']['min'], $currency);
    //                        $origin_price['sale']['max'] = ToConversion($origin_price['sale']['max'], $currency);
    //                        $origin_price['sale']['min'] = ToConversion($origin_price['sale']['min'], $currency);

    //                    } else {

    //                        $origin_price = '';

    //                    }

    //                    /**
    //                     * 获取原始变体信息
    //                     */
    //                    if (isset($productInfo->variantsOriginal)) {

    //                        $origin_variants = json_decode($productInfo->variantsOriginal, true);
    //                        foreach ($origin_variants as $k => $v) {
    //                            $origin_variants[$k]['cost'] = ToConversion($v['cost'], $currency);
    //                            $origin_variants[$k]['sale'] = ToConversion($v['sale'], $currency);
    //                        }

    //                    } else {

    //                        $origin_variants = '';

    //                    }

    //                    $productInfo = $productInfo->toArray();
    //                }

    //                //---------------------Products----------------------
    //                //重组结构
    //                if (!strstr($productInfo['image'], 'http')) {
    //                    $ShowImg = $this->getImageUrl($productInfo['image']);
    ////                $ShowImg = "http://".$_SERVER['HTTP_HOST'].'/'.$productInfo['image'];
    //                } else {
    //                    $ShowImg = $productInfo['image'];
    //                }
    ////            dd($productInfo);
    //                $Products = [];
    //                $extend = $productInfo['ext'] ?? '';
    //                $variants = $productInfo['variants'] ?? '';
    //                $productInfoImages = $productInfo['images'];

    //                $language = $task['language'] ?? 'default';
    //                $lp = [
    //                    'n' => $productInfo['lp_n'],
    //                    's' => $productInfo['lp_s'],
    //                    'k' => $productInfo['lp_k'],
    //                    'd' => $productInfo['lp_d'],
    //                ];
    //                $languagePack = $productInfo['languages'];
    //                if (isset($languagePack[$language])) {
    //                    $currentLanguagePack = $languagePack[$language];
    //                    $lp = [
    //                        'n' => $currentLanguagePack['n'] ?? $lp['n'],
    //                        's' => $currentLanguagePack['s'] ?? $lp['s'],
    //                        'k' => $currentLanguagePack['k'] ?? $lp['k'],
    //                        'd' => $currentLanguagePack['d'] ?? $lp['d'],
    //                    ];
    //                }
    //                //判断是否有无变体
    //                $autoGenerate = ProductService::AutoGenerate($productInfo['attribute']);
    //                //主产品sku
    //                $this->mainSku = $this->makeSku($sync_product_data);

    //                if ($origin_variants) {

    //                    foreach ($origin_variants as $k => $v) {
    //                        //客户端根据skuChangeData 获取有效变体
    ////                    if ($v['state']== 1) {//过滤掉失效变体

    ////                        $name = $v['lp']['n']??$productInfo['lp_n'];
    ////
    ////                        $description = $v['lp']['d']??$productInfo['lp_d'];
    ////
    ////                        if(isset($v['lp'])){
    ////                            $bulletPoint =  array_column($v['lp']['s']->toArray(),'value');
    ////                        }else{
    ////                            $bulletPoint = $productInfo['lp_s'];
    ////                        }
    ////                        //['AAAAAAAAA','xxxxxxxxxx']
    ////                        $searchTerms =  $v['lp']['k']??$productInfo['lp_k'];
    //                        if (isset($v['lps']) && isset($v['lps'][$language]) && checkArr($v['lps'][$language])) {
    //                            $name = $v['lps'][$language]['n'] ?? $lp['n'];
    //                            $description = $v['lps'][$language]['d'] ?? $lp['d'];
    //                            $bulletPoint = $v['lps'][$language]['s'] ?? $lp['s'];
    //                            $searchTerms = $v['lps'][$language]['k'] ?? $lp['k'];
    //                        } else {
    //                            $name = $lp['n'];
    //                            $description = $lp['d'];
    //                            $bulletPoint = $lp['s'];
    //                            $searchTerms = $lp['k'];
    //                        }


    //                        $currentIds = $this->getVariantIds($v['ids']);

    //                        $attributeName = $this->makeAttribute($sync_product_data, $productInfo['attribute'], $v);


    //                        $Sku = $this->mainSku . '-' . $this->makeVariantSku($sync_product_data, $k, $attributeName);
    ////                    $Sku = $this->getUploadSku($sync_product_data['sku_type'], $sync_product_data['source_id'], $sync_product_data['sku']) . "-" . $k;
    //                        if ($autoGenerate) {
    //                            $Sku = $this->mainSku;
    //                        }

    //                        $this->variantSku[$k] = $Sku;

    //                        $Products[] = [
    //                            'Name'        => $this->makeVariantTitle($sync_product_data, $name, $attributeName),
    //                            'Description' => transitionbr($description, '<br>'),//ReadOnly Property Description As String
    //                            'BulletPoint' => $bulletPoint,//ReadOnly Property BulletPoint As List(Of String)
    //                            'SearchTerms' => $searchTerms,//ReadOnly Property SearchTerms As List(Of String)
    //                            'Values'      => $currentIds,//ReadOnly Property Values As String
    //                            'Sku'         => $Sku,
    //                            'MainImage'   => $this->getFullUrl($v['img']['main'], $productInfoImages),
    //                            'Images'      => $this->getFullUrl($this->imageIds($v['img']), $productInfoImages) ?: [],
    //                            'Quantity'    => (int)$v['quantity'],     // ReadOnly Property Quantity As Integer
    //                            'Price'       => $origin_variants[$k]['sale'],
    //                            'Cost'        => $origin_variants[$k]['cost'],
    //                            'ProduceCode' => $v['code'],
    //                            'Fields'      => $this->getExtProperty($extend, $v['ext'] ?? null),
    //                        ];

    ////                    }

    //                    }//End Foreach $variants

    //                }
    //                //无变体产品处理skuChangeData
    //                $SkuChangeData = $this->getSkuChangeData($task['sku_change_data']);

    //                if ($autoGenerate) {
    //                    $ProductData = $SkuChangeData[0]['ProductData'];
    //                    unset($SkuChangeData[0]);
    //                    $SkuChangeData = array_values($SkuChangeData);
    //                    $SkuChangeData[0]['Sku'] = $this->mainSku;
    //                    $SkuChangeData[0]['ProductData'] = $ProductData;
    //                }
    //                //匹配老数据，正则替换成新的数据 HomeImprovementTools => Tools
    //                $sync_product_data['type_data'] = str_replace("HomeImprovementTools", "Tools", "{$sync_product_data['type_data']}");
    //                $p_data = [
    //                    'ID'                 => $task['id'],
    //                    'PID'                => $sync_product_data['pid'],
    //                    'Name'               => $lp['n'],
    //                    'BrowserNodeID'      => $task['browser_node_id'],
    //                    'CountryOfOrigin'    => $task['country_of_origin'],
    //                    'TypeData'           => json_decode($sync_product_data['type_data']),
    //                    'TypeDataID'         => json_decode($sync_product_data['type_data_id']),
    //                    'Sku'                => $this->makeSku($sync_product_data),
    //                    'Quantity'           => (int)$productInfo['quantity'],//库存
    //                    'ShowImg'            => $ShowImg,
    //                    'Price'              => $origin_price,//未转换
    //                    'Currency'           => $currency,
    //                    //                'Description'   =>  transitionbr($productInfo['lp_d'],'<br>'),
    //                    'Description'        => transitionbr($lp['d'], '<br>'),
    //                    'BulletPoint'        => $lp['s'],
    //                    'SearchTerms'        => $lp['k'],
    //                    //                'BulletPoint'   =>  $productInfo['lp_s'],
    //                    //                'SearchTerms'   =>  $productInfo['lp_k'],
    //                    'Category'           => $this->categoryInfo($productInfo['category_id']),
    //                    'Attribute'          => $this->setLanguageAttribute($productInfo['attribute'], $languagePack, $language),
    //                    'autoGenerate'       => $autoGenerate,
    //                    'Products'           => $Products,
    //                    'FulfillmentLatency' => $task['lead_time'],
    //                    'SkuChangeData'      => $SkuChangeData,
    //                ];

    //                //防止影响下次循环
    //                $origin_variants = '';
    //                $origin_price = '';

    //                //支持自定义属性key
    //                if ($extend) {

    //                    $extendData = $this->getExtProperty($extend, null);

    //                    $p_data['Fields'] = $extendData;

    //                } else {

    //                    $p_data['Fields'] = null;

    //                }

    //                $data[] = $p_data;

    //            }
    //            //Client:TaskData
    //            $tasksInfo = $this->SyncUploadService->info([], $upload_id)->first();

    //            /** 调用日志记录开始 */
    //            try {
    //                $insert_data_invoke_api_log = [
    //                    'upload_id'         => $upload_id,
    //                    'request_data'      => json_encode($requestData),
    //                    'upload_operate_id' => $tasksInfo['operate_id'],
    //                    'user_id'           => $sessionObj->user_id,
    //                    'company_id'        => $sessionObj->company_id,
    //                    'group_id'          => $sessionObj->group_id,
    //                    'oem_id'            => $sessionObj->oem_id,
    //                    'api'               => SyncInvokeApiLogModel::API_GET,
    //                ];

    //                SyncInvokeApiLogModel::query()->create($insert_data_invoke_api_log);
    //            } catch (\Exception $e) {
    ////            dd($e->getMessage());
    //            }
    //            /** 调用日志记录结束 */

    //            return [
    //                'ID'                 => $upload_id,
    //                'Count'              => count($tasks),
    //                'SID'                => $TempSID,
    //                'ID_PRODUCT'         => $tasksInfo['product_id'],//{"ID："",STATE:""}
    //                'ID_VARIANT'         => $tasksInfo['variant_id'],
    //                'ID_PRICING'         => $tasksInfo['pricing_id'],
    //                'ID_INVENTORY'       => $tasksInfo['inventory_id'],
    //                'ID_IMAGE'           => $tasksInfo['image_id'],
    //                'ID_RELATIONSHIP'    => $tasksInfo['relationship_id'],
    //                'STATE_PRODUCT'      => $tasksInfo['product_state'],
    //                'STATE_VARIANT'      => $tasksInfo['variant_state'],
    //                'STATE_PRICING'      => $tasksInfo['pricing_state'],
    //                'STATE_INVENTORY'    => $tasksInfo['inventory_state'],
    //                'STATE_IMAGE'        => $tasksInfo['image_state'],
    //                'STATE_RELATIONSHIP' => $tasksInfo['relationship_state'],
    //                'Products'           => $data,
    //            ];


    //        } else {
    //            return $this->fail(self::TaskIsEmpty);
    //        }
    //    }

    //    /**
    //     * 变体属性值处理
    //     * @param $mainExt
    //     * @param $variantsExt
    //     * @return array|null
    //     */
    //    public function getExtProperty($mainExt, $variantsExt)
    //    {
    ////    dump($mainExt,$variantsExt);
    //        $end_result = '';
    //        $extendData = null;
    //        if ($mainExt != null) {
    //            $mainExt = arrayCombine($mainExt, 'key');
    //            if ($variantsExt) {
    //                //变 + 主
    //                if (isset($variantsExt['copy']) && isset($variantsExt['del'])) {
    //                    $variantsExt['copy'] = arrayCombine($variantsExt['copy'], 'key');
    ////                    $variantsExt['del'] = arrayCombine($variantsExt['del'],'key');
    //                    $variantsExt['del'] = array_values($variantsExt['del']);
    //                    foreach ($mainExt as $k => $v) {
    //                        foreach ($variantsExt['del'] as $key => $value) {
    ////                        foreach($variantsExt['del'] as $key=>$value){
    //                            if ($k == $value) {
    //                                unset($mainExt[$k]);
    //                            }
    //                        }
    //                    }
    ////                    dd(array_merge($mainExt,$variantsExt['copy']));
    //                    $end_result = array_merge($mainExt, $variantsExt['copy']);
    //                } elseif (isset($variantsExt['copy']) && !isset($variantsExt['del'])) {//覆盖主，取并集
    //                    $variantsExt['copy'] = arrayCombine($variantsExt['copy'], 'key');
    ////                    dump(1);
    //                    return array_merge($mainExt, $variantsExt['copy']);
    //                } elseif (isset($variantsExt['del']) && !isset($variantsExt['copy'])) {//主取反
    ////                    $variantsExt['del'] = arrayCombine($variantsExt['del'],'key');
    //                    $variantsExt['del'] = array_values($variantsExt['del']);

    //                    foreach ($mainExt as $k => $v) {
    //                        foreach ($variantsExt['del'] as $key => $value) {
    ////                            if($k == $key){
    //                            if ($k == $value) {
    //                                unset($mainExt[$k]);
    //                            }
    //                        }
    //                    }

    //                    $end_result = $mainExt;
    ////                    dump($end_result);
    //                }
    //            } else {
    //                $end_result = $mainExt;//主
    //            }
    //        } else {
    //            if ($variantsExt) {
    //                //主是null，取变体属性中的copy中的值
    //                if (isset($variantsExt['copy'])) {
    //                    $end_result = $variantsExt['copy'];
    //                } else {
    //                    $end_result = null;
    //                }
    //            } else {
    //                $end_result = null;//null
    //            }
    //        }
    //        if ($end_result) {
    //            foreach ($end_result as $key => $value) {
    //                if (isset(SyncUploadModel::$extendDesc[$value["key"]])) {
    //                    $extendData[] = [
    //                        'Name'        => $value["key"],
    //                        'Type'        => 0, //Type 0 对应 TypeName "String" 写死
    //                        'TypeName'    => 'String',
    //                        'Data'        => $value["value"],
    //                        'Description' => SyncUploadModel::$extendDesc[$value["key"]],
    //                    ];
    //                } else {
    //                    $extendData[] = [
    //                        'Name'        => $value["key"],
    //                        'Type'        => 0, //Type 0 对应 TypeName "String" 写死
    //                        'TypeName'    => 'String',
    //                        'Data'        => $value["value"],
    //                        'Description' => $value["key"],
    //                    ];
    //                }
    //            }
    ////            dump($extendData);
    //            return $extendData;
    //        } else {
    //            return null;
    //        }
    //    }

    //    /**
    //     * 取完整产品图片域名地址
    //     * @param $data
    //     * @param $productInfoImages
    //     * @return array|string
    //     */
    //    private function getFullUrl($data, $productInfoImages)
    //    {
    ////        dump($productInfoImages);dump("产品图片");
    //        $images = [];
    //        if ($data) {
    //            if (!is_array($data)) {
    //                foreach ($productInfoImages as $value) {
    //                    if ($value->id == $data) {
    //                        if (!strstr($value->path, 'http')) {
    //                            return $this->getImageUrl($value->path);
    ////                            return "http://".$_SERVER['HTTP_HOST'].'/'.$value->path;
    //                        } else {
    //                            return $value->path;
    //                        }
    //                    }
    //                }
    //            } else {
    //                $productInfoImages = arrayCombine($productInfoImages, 'id');
    //                foreach ($data as $v) {
    //                    if (isset($productInfoImages[$v])) {
    //                        $images[] = $this->getImageUrl($productInfoImages[$v]->path);
    ////                        $images[] = "http://" . $_SERVER['HTTP_HOST'] . '/' . $productInfoImages[$v]->path;
    //                    }
    //                }
    //                return $images;
    //            }
    //        } else {
    //            return '';
    //        }
    //    }

    //    /**商品分类
    //     * @param $id
    //     * @return string
    //     */
    //    public function categoryInfo($id)
    //    {
    //        if ($id) {
    //            $category = CategoryModel::query()->id($id)->first();
    //            if ($category) {
    //                return $category->name;
    //            }
    //        }

    //        return '';
    //    }

    //    /**
    //     * 商品变体属性
    //     * @param $tmp
    //     * @return array
    //     */
    //    public function setAttribute($tmp)
    //    {
    //        if (!$tmp) {
    //            return NUll;
    //        }

    //        $data = [];

    //        foreach ($tmp as $k => $v) {
    //            $data[$v['name']] = $v['value'];
    //        }

    //        if (!$data) {
    //            return NUll;
    //        }

    //        return $data;
    //    }

    //    /**
    //     * 商品变体属性
    //     * @param $tmp
    //     * @return array
    //     */
    //    public function setLanguageAttribute($tmp, $languagePack, $language)
    //    {

    //        if (!$tmp) {
    //            return NUll;
    //        }

    //        $data = [];
    //        if (isset($languagePack[$language]) && isset($languagePack[$language]['map']) && checkArr($languagePack[$language]['map'])) {
    //            $map = $languagePack[$language]['map'];
    //            foreach ($tmp as $key => $value) {
    //                $arr = [];
    //                foreach ($value['value'] as $k => $v) {
    //                    $arr[] = isset($map[$v]) ? $map[$v] : $v;
    //                }
    //                $data[$value['name']] = $arr;
    //            }
    //        } else {
    //            foreach ($tmp as $k => $v) {
    //                $data[$v['name']] = $v['value'];
    //            }
    //        }

    //        if (!$data) {
    //            return NUll;
    //        }

    //        return $data;
    //    }

    //    /**
    //     * 商品再用图片的ids
    //     * @param $imgArray
    //     * @return array
    //     */
    //    public function imageIds($imgArray)
    //    {
    //        if (!$imgArray) {
    //            return [];
    //        }

    //        $imageIds = [];

    ////        if ($imgArray['main']){
    ////            array_push($imageIds, $imgArray['main']);
    ////        }

    //        if (!empty($imgArray['affiliate'])) {
    //            foreach ($imgArray['affiliate'] as $k => $v) {
    //                array_push($imageIds, $v);
    //            }
    //        }

    //        return $imageIds;
    //    }

    //    /**
    //     * 根据用户选择获取上传sku
    //     * @param string $skuType sku_type
    //     * @param string $source_id source_id
    //     * @param string $task_sku sku
    //     * @return string
    //     */
    //    public function getUploadSku($skuType, $source_id, $task_sku)
    //    {
    //        return $this->getMainSku($skuType, $source_id, $task_sku);
    //    }

    //    /**
    //     * 获取用户可见的Sku
    //     * @param $sku_change_data
    //     * @param $sku_type
    //     * @param $source_id
    //     * @param $sku
    //     * @return mixed
    //     * REMARK 此方法多处引用，修改请注意！！！
    //     */
    //    public function getSkuChangeData($sku_change_data)
    //    {
    //        foreach ($sku_change_data as &$skd) {
    //            if ($skd["IsMain"] === true) {
    //                $skd["Sku"] = $this->mainSku;
    //            } else {
    //                $skd["Sku"] = $this->variantSku[$skd["Sku"]];
    ////                $skd["Sku"] = $this->mainSku."-".$skd["Sku"];
    //            }
    //        }
    //        return $sku_change_data;
    //    }

    //    /**
    //     * 转换成数据库存储的sku_change_data格式
    //     * @param $sku_change_data
    //     * @param $sku
    //     * @return mixed
    //     * REMARK 此方法多处引用，修改请注意！！！
    //     */
    //    public function recoverySkuChangeData($sku_change_data, $sku)
    //    {
    //        foreach ($sku_change_data as &$skd) {
    ////            dump($skd);
    //            if ($skd["IsMain"] === true) {
    //                $skd["Sku"] = $sku;
    //            } else {
    //                $temp = explode('-', $skd["Sku"]);
    //                $skd["Sku"] = end($temp);
    //            }
    //        }
    //        return $sku_change_data;
    //    }

    //    /**
    //     * 构建新任务
    //     * @param bool $action_type
    //     * @param $sid
    //     * @param $user_id
    //     * @param $company_id
    //     * @param $oem_id
    //     * @param $group_id
    //     * @param $upload_origin_state
    //     * @param $time
    //     * @return array|\Illuminate\Http\JsonResponse|string
    //     */
    //    public static function createUploadTasks($action_type, $sid, $user_id, $company_id, $oem_id, $group_id, $upload_origin_state, $time)
    //    {
    ////        dd($action_type,$sid,$user_id,$company_id,$oem_id,$group_id,$upload_origin_state,$time);
    //        $SyncOnlineService = new SyncOnlineService();
    //        DB::beginTransaction();
    //        try {
    //            $spid = SyncMwsModel::query()->StoreId($sid)->EanupcStatus(2)->UploadId(null)->pluck("spid")->all();  //新增条件，只有全部分配了eanupc才可被构建任务
    //            if (count($spid)) {
    //                //让sync_product表中的变体和产品数据不可修改操作
    //                SyncProductModel::query()->whereIn("id", array_unique($spid))->update(
    //                    ["already_upload" => SyncProductModel::ALREADY_UPLOAD_TRUE, "uploading" => SyncProductModel::UPLOADING_TRUE]
    //                );
    //                $insertData = [
    //                    "store_id"             => $sid
    //                    , "user_id"            => $user_id
    //                    , "company_id"         => $company_id
    //                    , "oem_id"             => $oem_id
    //                    , "group_id"           => $group_id
    //                    ////
    //                    , "operate_id"         => $user_id//刚建的任务不给别人拿到也没事
    //                    ////
    //                    , "state"              => SyncUploadModel::STATE_FALSE
    //                    , "product_state"      => $upload_origin_state
    //                    , "variant_state"      => $upload_origin_state
    //                    , "pricing_state"      => $upload_origin_state
    //                    , "inventory_state"    => $upload_origin_state
    //                    , "image_state"        => $upload_origin_state
    //                    , "relationship_state" => $upload_origin_state
    //                    , "created_at"         => date("Y-m-d H:i:s", $time),
    //                ];
    //                //计算每次要循环的次数
    //                $each_count = ceil(count($spid)/self::TasksMaxExcuteProducts);
    //                //循环构建任务
    //                for($i=0;$i<$each_count;$i++){
    //                    //insertGetId这个方法不会自动更新created_at字段，在上一步单独写
    //                    $upload_id = SyncUploadModel::query()->insertGetId($insertData);

    //                    $insert_data_user = [
    //                        'upload_id'    => $upload_id,
    //                        'old_user_id'  => null,
    //                        'new_user_id'  => $user_id,
    //                        'last_updated' => null,
    //                        'company_id'   => $company_id,
    //                        'group_id'     => $group_id,
    //                        'oem_id'       => $oem_id,
    //                        'is_changed'   => null,
    //                        'is_in_5_min'  => null,
    //                        'type'         => SyncUserModel::TYPE_START,
    //                    ];

    //                    SyncUserModel::query()->create($insert_data_user);

    //                    $insertData_online = [
    //                        'upload_id'  => $upload_id,
    //                        'user_id'    => $user_id,
    //                        'company_id' => $company_id,
    //                        'group_id'   => $group_id,
    //                        'oem_id'     => $oem_id,
    //                    ];

    //                    $SyncOnlineService->insertData($insertData_online);
    //                    //limit控制每次构建任务集中的数量
    //                    $count = SyncMwsModel::query()->StoreId($sid)->EanupcStatus(2)->UploadId(null)->limit(self::TasksMaxExcuteProducts)->update(
    //                        [
    //                            "upload_id" => $upload_id,
    //                            "state"     => SyncMwsModel::STATE_EXECUTING,
    //                        ]
    //                    );
    //                    SyncUploadModel::query()->Id($upload_id)->update(['count' => $count]);
    //                }

    //                $TaskIDs[] = $upload_id;
    //            }
    //            DB::commit();
    //            if (!$action_type) {
    //                return $TaskIDs ?? [];
    //            }
    //        } catch (\Exception $e) {
    ////                        dd($e->getMessage());
    //            DB::rollBack();
    //            $data = $e->getMessage();
    //            if (!$action_type) {
    //                $apiID = 230;
    //                $errcode = self::TransactionFail;
    //                $base = 1000;
    //                $code = $errcode > $base ? $errcode : $apiID * $base + $errcode;

    //                $info = '';
    //                return response()->json(['Code' => $code, 'Data' => json_encode($data), 'Info' => $info]);
    //            } else {
    //                return $data;
    //            }
    //        }
    //    }


    //    /**
    //     * @param $id
    //     * @param $syncProduct
    //     * @return string
    //     * 上传产品主产品sku
    //     * User: CCY
    //     * Date: 2020/8/5 19:44
    //     */
    //    protected function makeSku($syncProduct)
    //    {
    //        return $this->getMainSku($syncProduct['sku_type'], $syncProduct['source_id'], $syncProduct['sku']);
    //    }

    //    /**
    //     * @param $syncProduct
    //     * @param $attribute
    //     * @param $variants
    //     * @return string
    //     * 获取上传产品变体需要追加属性时属性名
    //     * User: CCY
    //     * Date: 2020/12/22
    //     */
    //    protected function makeAttribute($syncProduct, $attribute, $variants)
    //    {
    //        return $this->getAttributeName($syncProduct, $attribute, $variants['ids']);
    //    }

    //    /**
    //     * @param $syncProduct
    //     * @param $attribute
    //     * @param $nonius
    //     * 获取变体sku 追加字符串
    //     * User: CCY
    //     * Date: 2020/12/22
    //     */
    //    protected function makeVariantSku($syncProduct, $nonius, $attributeName)
    //    {
    //        $sku = $this->getVariantSku($syncProduct['sku_suffix'], $attributeName, $nonius);
    //        return str_replace(' ', '', $sku);
    //    }

    //    /**
    //     * @param $syncProduct
    //     * @param $title
    //     * @return mixed
    //     * 获取变体标题追加属性名
    //     * User: CCY
    //     * Date: 2020/12/22
    //     */
    //    protected function makeVariantTitle($syncProduct, $title, $attributeName)
    //    {
    //        return $this->getVariantTitle($syncProduct['title_suffix'], $title, $attributeName);
    //    }

    #endregion
    public async Task<int> invoke()
    {
        return await Task.FromResult(0);
        /************************************************  
         
        ************************************************/
    }
}
