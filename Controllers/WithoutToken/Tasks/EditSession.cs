﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.Tasks;
[Route("api/[controller]")]
[ApiController]
public class EditSession : ControllerBase
{

    #region ERP3.0 PHP
    //    const Err_DATABASE = 1;
    //    const Err_NotExistUploadRecord = 2;
    //    /**
    //     * @var SyncUploadService
    //     */
    //    private SyncUploadService $SyncUploadService;
    //    /**
    //     * @var SyncAliveSessionService
    //     */
    //    private SyncAliveSessionService $SyncAliveSessionService;
    //    /**
    //     * @var SyncSessionLogService
    //     */
    //    private SyncSessionLogService $SyncSessionLogService;

    //    public function __construct()
    //    {
    //        $this->SyncUploadService = new SyncUploadService();
    //        $this->SyncAliveSessionService = new SyncAliveSessionService();
    //        $this->SyncSessionLogService = new SyncSessionLogService();
    //    }

    //    public function verifyParams()
    //    {
    //        return ['ID', 'Index', 'SessionState', 'OldState'];
    //    }

    //    public function getApiId()
    //    {
    //        return 250;
    //    }

    //    /**
    //     * @param $requestData
    //     * @param $userId
    //     * @param $rule
    //     * @param \App\Http\SessionData $session
    //     * @return \Illuminate\Http\JsonResponse
    //     */
    //    public function handle($requestData, $userId, $rule, $session)
    //    {

    //        $uploadID = (int)$requestData["ID"];//sync_upload表主键Id
    //        $sessionIndex = (int)$requestData["Index"];//0-5
    //        $sessionState = (int)$requestData["SessionState"];//本次要修改成为的状态
    //        $clientOldState = (int)$requestData["OldState"];//修改之前的老状态

    //        $sessionObj = $this->getSessionObj();
    //        $oem_id = $sessionObj->oem_id;
    //        $user_id = $sessionObj->user_id;
    //        $group_id = $sessionObj->group_id;
    //        $company_id = $sessionObj->company_id;

    //        if ($sessionIndex >= 0 && $sessionIndex <= 5){

    //            $Sessions = ['product', 'variant', 'pricing', 'inventory', 'image', 'relationship'];

    //            $SessionKey = $Sessions[$sessionIndex];

    //            $session_state_field = "{$SessionKey}_state";
    //            $session_id_field = "{$SessionKey}_id";//预留查询某个SessionID,可能为Null时，是否要多余处理，目前没使用；
    //            $SessionKeyData = $this->SyncUploadService->info([$session_id_field,$session_state_field, "id", "operate_id"],$uploadID)->first();
    //            if ($SessionKeyData) {
    //                DB::beginTransaction();
    //                try
    //                {
    ////                    $where = [["id","=",$uploadID],["operate_id","=",$userId]];
    //                    $where = [["id", "=",$uploadID]];//加了operate_id后会使新拿到任务的人无法更新进度，导致任务完成的情况下进度值显示错误
    //                                                     //                    $count=$this->SyncUploadService->updateData($where,["{$SessionKey}_state" => $sessionState]);
    //                                                     //当服务器老状态和要更改的状态相同时，就免于更新状态,将count值标记为2,在前端反映为【未更新】
    //                    if ($SessionKeyData[$session_state_field] == $sessionState){
    //                        //这样的upload表记录的更新时间会有所延迟，暂无问题
    //                        $count = 2;
    //                    }else
    //                    {
    //                        $count = $this->SyncUploadService->updateData($where,[$session_state_field => $sessionState]);
    //                    }

    //                    $this->SyncSessionLogService->insert(
    //                        [
    //                            'upload_id'         =>  $uploadID,
    //                            'index'             =>  $sessionIndex,
    //                            'state'             =>  $sessionState,
    //                            'company_id'        =>  $company_id,
    //                            'user_id'           =>  $SessionKeyData['operate_id'],//这个任务在upload表当前的操作人
    //                            'last_operate_id'   =>  $user_id,
    //                            'group_id'          =>  $group_id,
    //                            'oem_id'            =>  $oem_id,
    //                            'count'             =>  $count,
    //                            'server_old_state'  =>  $SessionKeyData[$session_state_field],
    //                            'client_old_state'  =>  $clientOldState,
    //                            'invoke_api'        =>  'tasks/edit_session',
    //                            'index_text'        =>  $SessionKey,
    //                            'state_text'        =>  $sessionState//具体意义待补充
    //                        ]
    //                    );

    //                    if ($sessionState === 6){

    //                        $this->SyncAliveSessionService->info([],'',$uploadID,'',$sessionIndex)->delete();

    //                    }else
    //                    {
    //                        $updateData_alive_session = [
    //                            'state'           =>  $sessionState,
    //                            'state_text'      =>  $sessionState,//具体意义待补充
    //                            'last_operate_id' =>  $user_id,
    //                            'user_id'         =>  $SessionKeyData['operate_id'],//这个任务在upload表当前的操作人
    //                            'count'           =>  $count == 1 ? 1 : 0
    //                        ];

    //                        $where = [['upload_id', '=',$uploadID],['index','=',$sessionIndex]];
    //                        $this->SyncAliveSessionService->updateData($where,$updateData_alive_session);
    //                    }

    //                    /** 调用日志记录开始 */
    //                    try
    //                    {//包了两层的 try catch

    //                        $insert_data_invoke_api_log = array(
    //                            'upload_id'         => $uploadID,
    //                            'request_data'      => json_encode($requestData),
    //                            'upload_operate_id' => $SessionKeyData['operate_id'],
    //                            'user_id'           => $sessionObj->user_id,
    //                            'company_id'        => $sessionObj->company_id,
    //                            'group_id'          => $sessionObj->group_id,
    //                            'oem_id'            => $sessionObj->oem_id,
    //                            'api'               => SyncInvokeApiLogModel::API_EDIT_SESSION
    //                        );

    //                        SyncInvokeApiLogModel::query()->create($insert_data_invoke_api_log);
    //                    }
    //                    catch (\Exception $e){
    //                        //            dd($e->getMessage());
    //                    }
    //                    /** 调用日志记录结束 */

    //                    DB::commit();
    //                    return $this->success();

    //                    }
    //                    catch (\Exception $e){
    //                    $this->sqlError($requestData,$e->getMessage(), request()->getRequestUri());
    //                        DB::rollBack();
    //                        return $this->fail(self::Err_DATABASE, null,$e->getMessage());
    //                    }
    //                    }else
    //                    {
    //                        return $this->fail(self::Err_NotExistUploadRecord,$uploadID, "没有查询到{$uploadID}的记录");
    //                    }
    //                }else
    //                {
    //                    return $this->fail(self::ERR_PARAMETER,$sessionIndex, "Index[$sessionIndex] OUT OF RANGE");
    //                }
    //            }
    #endregion
    public async Task<int> invoke()
    {
        return await Task.FromResult(0);
        /************************************************  
         ================/tasks/edtSession
select `d_product_id`, `t_product_state`, `id`, `i_operate_id` from `sync_upload` where `id` = ? limit *
insert into `sync_session_log` (`i_upload_id`, `d_index`, `d_state`, `i_company_id`, `i_user_id`, `i_last_operate_id`, `i_group_id`, `i_oem_id`, `d_count`, `d_server_old_state`, `d_client_old_state`, `d_invoke_api`, `d_index_text`, `d_state_text`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
update `sync_alive_session` set `d_state` = ?, `d_state_text` = ?, `i_last_operate_id` = ?, `i_user_id` = ?, `d_count` = ?, `sync_alive_session`.`updated_at` = ? where (`i_upload_id` = ? and `d_index` = ?)
select column_name as `column_name` from information_schema.columns where table_schema = ? and table_name = ?
insert into `sync_invoke_api_log` (`i_upload_id`, `d_request_data`, `i_upload_operate_id`, `i_user_id`, `i_company_id`, `i_group_id`, `i_oem_id`, `t_api`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
update `store` set `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
select `i_store_id` from `store_group` where `i_group_id` = ?
select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
update `store` set `d_last_auto_time` = ?, `store`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `id`, `d_title`, `i_user_id`, `t_state` from `work_order` where `i_company_id` = ? and `t_state` in (?, ?) limit *
select * from `waybill` where `waybill`.`id` = ? limit *
update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `waybill`.`updated_at` = ? where `id` = ?
update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `d_tracking` = ?, `waybill`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *

        ************************************************/
    }
}
