﻿using ERP.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.ToSell;
[Route("api/[controller]")]
[ApiController]
public class Finish : BaseController
{
    ////原ERP3.0注释
    #region ERP3.0 PHP
    //private ToSell $service;

    //public function __construct()
    //{
    //    $this->service = new ToSell();
    //}

    //const PRODUCTNOTASK = 1;//任务下产品不存在
    //const EDITFAIL = 2;//提交失败

    //public function getApiId()
    //{
    //    return 1160;
    //}

    //public function verifyParams()
    //{
    //    return ['ID', 'Success', 'AnyInfo'];
    //}

    //public function handle($request, $userId, $rule, $session)
    //{
    //    $taskId = $request['ID'];

    //    $taskProductId = $this->service->getProductIdByTaskId($taskId)->toArray();
    //    if (!checkArr($taskProductId))
    //    {
    //        return $this->fail(self::PRODUCTNOTASK);
    //    }

    //    $taskProductIds = array_column($taskProductId, 'task_product_id');

    //    $updateData = [
    //        'is_finish'  => ToSellTask::FINISH_TRUE,
    //        'end_result' => json_encode(['result' => $request['Success'], 'message' => $request['AnyInfo']]),
    //        'updated_at' => $this->getDataTime()
    //    ];
    //    $updateUpload = [
    //        'upload_state' => ToSellTaskProduct::UPLOAD_THREE,
    //        'end_result'   => $request['Success'] == true ? 1 : 2,
    //        'other_info'   => $request['AnyInfo'],
    //        'updated_at'   => $this->getDataTime()
    //    ];
    //    DB::beginTransaction();
    //    try
    //    {
    //        //更新任务
    //        $this->service->updateTask($taskId, $updateData);
    //        //更新任务下产品
    //        $this->service->updateTaskProducts($taskProductIds, $updateUpload);

    //        //更新产品上架完成状态
    //        $this->service->updateTaskProductUpload($taskProductIds);
    //        //更新产品价格完成状态
    //        $this->service->updateTaskProductPrice($taskProductIds);
    //        //更新产品库存完成状态
    //        $this->service->updateTaskProductStock($taskProductIds);

    //        DB::commit();
    //    }
    //    catch (Exception $e) {
    //        DB::rollBack();
    //        return $this->fail(self::EDITFAIL, null, $e->getMessage());
    //    }
    //    return $this->success();
    //    }
    #endregion

    [Route("invoke")]
    [HttpPost]
    public async Task<ResultStruct> invoke()
    {
        /************************************************  

        ************************************************/
        return await Task.FromResult(Success());
    }
}
