﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.ToSell;
[Route("api/[controller]")]
[ApiController]
public class FollowUp : ControllerBase
{
    ////原ERP3.0注释
    #region ERP3.0 PHP
    //    private ToSell $service;

    //    public function __construct()
    //    {
    //        $this->service = new ToSell();
    //    }


    //    const ProductIsExists = 1;//产品已存在
    //    const TitleIsEmpty = 2;//标题为空
    //    const CurrencyFail = 3;//货币不存在
    //    const ProductUpdateFail = 4;//产品上传失败


    //    public function verifyParams()
    //    {
    //        return [
    //            'Marketplace', 'Title', 'Asin', 'LowestPrice', 'Brand', 'Manufacturer', 'ImageUrl', 'Unit', 'StoreId'
    //        ];
    //    }

    //    public function getApiId()
    //    {
    //        return 1110;
    //    }

    //    public function handle($request, $userId, $rule, $session)
    //    {
    //        if (empty($request['Title']))
    //        {
    //            return $this->fail(self::TitleIsEmpty);
    //        }
    //        $titleHashCode = createHashCode($request['Title']);
    //        $res = $this->service->checkExists($request['Marketplace'], $titleHashCode, $request['Asin']);
    //        if ($res) {
    //            return $this->fail(self::ProductIsExists);
    //        }
    //        // 货币
    //        $currency = $request['Unit'];
    //        !checkStr($currency) && $currency = 'CNY';
    ////        if (!$currency) {
    ////            return $this->fail(self::CurrencyFail, $currency);
    ////        }
    //        $cacheCurrency = Cache::instance()->unit()->get();
    //        if (!isset($cacheCurrency[$currency]))
    //        {
    //            return $this->fail(self::CurrencyFail, $currency);
    //        }
    //        $rate = $cacheCurrency[$currency]['rate'];
    //        $lowestprice = FromConversion($request['LowestPrice'], $request['Unit'], $rate);
    //        $Shipping = isset($request['Shipping']) ? FromConversion($request['Shipping'], $request['Unit'], $rate) : 0;
    //        $insertData = [
    //            'marketplace'  => $request['Marketplace'],
    //            'title'        => $request['Title'],
    //            'hash_code'    => $titleHashCode,
    //            'asin'         => $request['Asin'],
    //            'lowestprice'  => $lowestprice,
    //            'shipping'     => $Shipping,
    //            'rate'         => $rate,
    //            'brand'        => $request['Brand'],
    //            'manufacturer' => $request['Manufacturer'],
    //            'imageurl'     => $request['ImageUrl'],
    //            'unit'         => $request['Unit'],
    //            'platform_id'  => 1,
    //            'store_id'     => $request['StoreId'],
    //            'truename'     => $session->getTruename(),
    //            'user_id'      => $session->getUserId(),
    //            'group_id'     => $session->getGroupId(),
    //            'company_id'   => $session->getCompanyId(),
    //            'oem_id'       => $session->getOemId(),
    //            'created_at'   => $this->getDataTime()
    //        ];

    //        $res = $this->service->insertData($insertData);
    //        return $res ? $this->success() : $this->fail(self::ProductUpdateFail);
    //    }
    #endregion

    //[Route("invoke")]
    //[HttpPost]
    //public async Task<ResultStruct> invoke()
    //{
    //    /************************************************  

    //    ************************************************/
    //    return Success();
    //}
}
