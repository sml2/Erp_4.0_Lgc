﻿using ERP.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.ToSell;
[Route("api/[controller]")]
[ApiController]
public class ReportError : BaseController
{
    ////原ERP3.0注释
    #region ERP3.0 PHP
    //private ToSell $service;

    //public function __construct()
    //{
    //    $this->service = new ToSell();
    //}

    //const IndexError = 1;//步骤错误
    //const EDITFAIL = 2;//提交失败

    //public function getApiId()
    //{
    //    return 1150;
    //}

    ////Index 0产品 1价格 2库存
    //public function verifyParams()
    //{

    //    return ['TaskID', 'PID', 'Index', 'Result'];
    //}

    //public function handle($request, $userId, $rule, $session)
    //{
    //    $id = $request['PID'];
    //    $TaskID = $request['TaskID'];
    //    $Index = $request['Index'];

    //    switch ($Index) {
    //        case  0 :
    //            $updateUpload = [
    //                'product_result' => 4,
    //                'product_reason' => $request['Result'],
    //                'updated_at'     => $this->getDataTime()
    //            ];
    //        break;
    //        case  1 :
    //            $updateUpload = [
    //                'price_result' => 4,
    //                'price_reason' => $request['Result'],
    //                'updated_at'   => $this->getDataTime()
    //            ];
    //        break;
    //        case  2 :
    //            $updateUpload = [
    //                'stock_result' => 4,
    //                'stock_reason' => $request['Result'],
    //                'updated_at'   => $this->getDataTime()
    //            ];
    //        break;
    //        default:
    //            return $this->fail(self::IndexError);
    //        break;
    //    }

    //    DB::beginTransaction();
    //    try
    //    {
    //        //更新任务
    //        $this->service->updateTask($TaskID, ['updated_at' => $this->getDataTime()]);
    //        //更新任务下产品
    //        $this->service->updateTaskProduct($id, $updateUpload);
    //        DB::commit();
    //    }
    //    catch (Exception $e) {
    //        DB::rollBack();
    //        return $this->fail(self::EDITFAIL, null, $e->getMessage());
    //    }
    //    return $this->success();
    //    }
        #endregion

     [Route("invoke")]
    [HttpPost]
    public async Task<ResultStruct> invoke()
    {
        /************************************************  
         
        ************************************************/
        return  await Task.FromResult(Success());
    }
}
