﻿using ERP.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.ToSell;
[Route("api/[controller]")]
[ApiController]
public class EditSession : BaseController
{
    ////原ERP3.0注释
    #region ERP3.0 PHP
    //private ToSell $service;

    //public function __construct()
    //{
    //    $this->service = new ToSell();
    //}

    //const IndexError = 1;//步骤错误
    //const PRODUCTNOTASK = 2;//任务下产品不存在
    //const EDITFAIL = 3;//提交失败

    //public function getApiId()
    //{
    //    return 1140;
    //}

    ////Index 0产品 1价格 2库存
    //public function verifyParams()
    //{
    //    return ['ID', 'Index', 'SessionState', 'SessionID'];
    //}

    //public function handle($request, $userId, $rule, $session)
    //{
    //    $id = $request['ID'];
    //    $Index = $request['Index'];
    //    switch ($Index) {
    //        case  0 :
    //            $updateData = [
    //                'upload_session_state' => $request['SessionState'],
    //                'updated_at'           => $this->getDataTime()
    //            ];
    //            $request['SessionID'] && $updateData['upload_session_id'] = $request['SessionID'];
    //        break;
    //        case  1 :
    //            $updateData = [
    //                'price_session_state' => $request['SessionState'],
    //                'updated_at'          => $this->getDataTime()
    //            ];
    //            $request['SessionID'] && $updateData['price_session_id'] = $request['SessionID'];
    //        break;
    //        case  2 :
    //            $updateData = [
    //                'stock_session_state' => $request['SessionState'],
    //                'updated_at'          => $this->getDataTime()
    //            ];
    //            $request['SessionID'] && $updateData['stock_session_id'] = $request['SessionID'];
    //        break;
    //        default:
    //            return $this->fail(self::IndexError);
    //        break;
    //    }
    //    //更新任务
    //    $res = $this->service->updateTask($id, $updateData);
    //    return $res ? $this->success() : $this->fail(self::EDITFAIL);
    //}
    #endregion
    [Route("invoke")]
    [HttpPost]
    public async Task<ResultStruct> invoke()
    {
        /************************************************  
         
        ************************************************/
        return await Task.FromResult(Success());
    }
}
