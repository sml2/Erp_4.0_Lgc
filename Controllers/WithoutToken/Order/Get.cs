﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.Order
{
    public class Get : Controller
    {
        #region ERP3.0
        //        const OrderIsEmpty = 1; //订单不存在
        //        const OrderProductsIsEmpty = 2; //订单下无产品

        //        public function getApiId()
        //        {
        //            return 330;
        //        }

        //        public function verifyParams()
        //        {
        //            return ['ID', 'Type'];
        //        }

        //        public function handle($request, $userId, $rule, $session)
        //        {
        //        $orderId = $request['ID'];

        //            if (empty($orderId))
        //            {
        //                return $this->fail(self::ERR_PARAMETER, 'ID');
        //            }
        //        //20200909 开启手动录入订单使用EXE客户端发货
        //        $orderInfo = AmazonOrder::query()->where(
        //            [
        ////                ['entry_mode', '=', AmazonOrder::API_ENTRY_MODE],
        //                ['id', '=', $orderId],
        //            ]
        //        )->first();

        //            if (!$orderInfo) {
        //                return $this->fail(self::OrderIsEmpty);
        //            }
        //        $orderInfo = $orderInfo->toArray();
        //        $products = $orderInfo['goods'];

        //            if (!$products) {
        //                return $this->fail(self::OrderProductsIsEmpty);
        //            }

        //            unset($orderInfo['goods']);
        //            unset($orderInfo['entry_mode']);

        //        $orderTime = [
        //            'pay_time'           => strtotime($orderInfo['pay_time']) ?: (int)$orderInfo['pay_time'],
        //            'sign_time'          => strtotime($orderInfo['sign_time']) ?: (int)$orderInfo['sign_time'],
        //            'consign_time'       => strtotime($orderInfo['consign_time']) ?: (int)$orderInfo['consign_time'],
        //            'create_time'        => strtotime($orderInfo['create_time']) ?: (int)$orderInfo['create_time'],
        //            'finish_time'        => strtotime($orderInfo['finish_time']) ?: (int)$orderInfo['finish_time'],
        //            'delivery_time'      => strtotime($orderInfo['delivery_time']) ?: $orderInfo['delivery_time'],
        //            'shipping_time'      => strtotime($orderInfo['shipping_time']) ?: $orderInfo['shipping_time'],
        //            'last_time'          => strtotime($orderInfo['last_time']) ?: (int)$orderInfo['last_time'],
        //            'fake_product_time'  => strtotime($orderInfo['fake_product_time']) ?: (int)$orderInfo['fake_product_time'],
        //            'true_product_time'  => strtotime($orderInfo['true_product_time']) ?: (int)$orderInfo['true_product_time'],
        //            'latest_ship_time'   => strtotime($orderInfo['latest_ship_time']) ?: (int)$orderInfo['latest_ship_time'],
        //            'earliest_ship_time' => strtotime($orderInfo['earliest_ship_time']) ?: (int)$orderInfo['earliest_ship_time'],
        //            'timestamp'          => strtotime($orderInfo['timestamp']) ?: (int)$orderInfo['timestamp'],
        //            'SendNum'            => $orderInfo['delivery_num'],
        //            'FailNum'            => $orderInfo['delivery_fail'],
        //            'suffix'             => mt_rand(0, 9).$orderInfo['delivery_num']
        //        ];
        //        $orderInfo = array_merge($orderInfo, $orderTime);
        //            if ($orderInfo['merge_id'] != -1 ){//合并订单
        //            $orderInfo['Products'] = self::getMergeOrderMessage($orderInfo['merge_id']);
        //            }else
        //            {
        //            $orderInfo['Products'] = json_decode($products, true);
        //            }
        //        $orderInfo['default_ship_from_location_address'] = json_decode($orderInfo['default_ship_from_location_address'], true);
        //            return $this->success($orderInfo);
        //        }

        //        /**
        //         * @param $merge_id
        //         * @param int $is_original 是否要取原数据
        //         * @return array
        //         */
        //        public static function getMergeOrderMessage($merge_id,$is_original = false)
        //        {
        //        $orderIds = AmazonOrder::query()
        //            ->select(["*"])
        //            ->where(
        //            [
        //                ['merge_id', '=', $merge_id]
        //                ]
        //        )->get();
        //            if ($orderIds){
        //            $orderIds = $orderIds->toArray();
        //            }
        //        $orderIds = array_column($orderIds, "id");
        //        $order_goods = AmazonOrder::query()->select(["*"])->whereIn('id', $orderIds)->get();
        //        $goods_data = [];
        //            if ($is_original){
        //                foreach ($order_goods as $k => $v) {
        //                $v = json_decode($v->getRawOriginal('goods'), true);
        //                    //                $v = json_decode($v['goods'], true);
        //                    //                dd($v1,$v2);
        //                    foreach ($v as $v_1){
        //                    $goods_data[] = $v_1;
        //                    }
        //                }
        //            }else
        //            {
        //                foreach ($order_goods->toArray() as $k => $v) {
        //                $v = json_decode($v['goods'], true);
        //                    foreach ($v as $v_1){
        //                    $goods_data[] = $v_1;
        //                    }
        //                }
        //            }
        //            return $goods_data;
        //        }

        #endregion

        #region SQL
        public IActionResult Invoke(/*$request, $userId, $rule, $session*/)
        {
            /************************************************  
             ================/order/get  
            select * from `amazon_order` where (`id` = ?) limit *
            select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
            update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
            select `i_store_id` from `to_sell_task` where `i_company_id` = ? and `t_is_finish` = ? and `i_oem_id` = ?
            ************************************************/
            return View();
        }
        #endregion
    }
}
