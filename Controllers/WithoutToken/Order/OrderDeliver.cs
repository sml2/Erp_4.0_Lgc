﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.Order
{
    public class OrderDeliver : Controller
    {
        ////原ERP3.0注释
        #region ERP3.0 PHP
        //        /**
        //     * 订单不存在
        //    */
        //        const ORDER_NOT_EXIST = 1;
        //        /**
        //         * 采购不存在
        //        */
        //        const PURCHASE_NOT_EXIST = 2;
        //        /**
        //         * 店铺不存在
        //        */
        //        const STORE_NOT_EXIST = 3;
        //        /**
        //         * 运单不存在
        //        */
        //        const WAYBILL_NOT_EXIST = 4;
        //        /**
        //         * 合并订单不存在
        //        */
        //        const ORDER_MERGE_NOT_EXIST = 6;


        //        public function getApiId()
        //        {
        //            return 340;

        //        }//end getApiId()

        //        /**
        //         * 获取指定公司redis
        //         * @param string $company_id
        //         * @return mixed
        //         */
        //        private function getCompany($company_id = '')
        //        {
        //            return Cache::instance()->Company()->get($company_id);
        //        }

        //        public function handle($request, $userId, $rule, $session)
        //        {
        //            if (!array_key_exists('OrderID', $request))
        //            {
        //                return $this->fail(self::ERR_PARAMETER, 'OrderID');
        //            }

        //        $orderId = $request['OrderID'];

        //            if (!array_key_exists('Type', $request))
        //            {
        //                return $this->fail(self::ERR_PARAMETER, 'Type');
        //            }

        //        $type = $request['Type'];

        //        // 订单信息
        //        $order = AmazonOrder::find($orderId);

        //            if (!$order) {
        //                return $this->fail(self::ORDER_NOT_EXIST);
        //            }

        //            if ($session->getCompanyId() != $order["company_id"]) {
        //            //查询订单公司redis信息
        //            $orderCompany = $this->getCompany($order["company_id"]);
        //                if ($orderCompany['is_distribution'] != Company::DISTRIBUTION_TRUE){
        //                    return $this->fail(self::ORDER_NOT_EXIST);
        //                }
        //                //分校公司上级为当前公司
        //                if ($session->getCompanyId() != $orderCompany['report_id']){
        //                    return $this->fail(self::ORDER_NOT_EXIST);
        //                }

        //            // if ($this->userInfo['company_id'] != $order["parent_company_id"] || $order["process_waybill"] == 1) {
        ////            if (order["process_waybill"] == AmazonOrder::PROCESSWAYBILL_INSIDE) {
        ////                return $this->fail(self::ORDER_NOT_EXIST);
        ////            }

        //            $StoreService = new StoreService();
        //            $store = $StoreService->getInfoById($order['store_id']);
        //                if (!$store){
        //                    return $this->fail(self::STORE_NOT_EXIST);
        //                }
        //            $tmp = $StoreService->getMwsInfo($store);
        //            }else
        //            {
        //            // 店铺信息
        //            $store = (Cache::instance()->storeCompany()->get()[$order['store_id']] ?? null);
        //                if ($store === null) {
        //                    return $this->fail(self::STORE_NOT_EXIST);
        //                }
        //            $tmp = (new StoreService())->getMwsInfo($store);
        //            }

        //        $storeInfo = [
        //            'ID'            => $store['id'],
        //            'Name'          => $store['name'],
        //            'Marketplace'   => $tmp['marketplace'],
        //            'SellerID'      => $tmp['seller_id'],
        //            'MwsAuthToken'  => $tmp['mws_auth_token'],
        //            'AwsAccessKey'  => $tmp['aws_access_key'],
        //            'SecretKey'     => $tmp['secret_key'],
        //            // 'LastOrderTime' => time()
        //        ];

        //        $time = $this->getTime();
        //            // 发货时间
        //            // if ($order['earliest_ship_time'] < $currentTime && $order['latest_ship_time'] > $currentTime) {
        //            // $time = $currentTime;
        //            // } else if ($order['earliest_ship_time'] < $order['delivery_time'] && $order['latest_ship_time'] > $order['delivery_time']) {
        //            // $time = $order['delivery_time'];
        //            // } else {
        //            // $time = intval(floor(($order['latest_ship_time'] - $order['earliest_ship_time']) / 2)) + $order['earliest_ship_time'];
        //            // }
        //            if ($type) {
        //                // 运单信息
        //                if ($order["merge_id"] > 0) {
        //                $merge = $this->getMergeOrderIds($order, 2);
        //                    if ($merge instanceof JsonResponse) {
        //                        return $merge;
        //                    }

        //                $where[] = $this->getTrueWaybillOrderIds($order, $merge);
        //                } else
        //                {
        //                $where[] = [
        //                    'order_id',
        //                    '=',
        //                    $order['id'],
        //                ];
        //                }

        //            $waybills = WayBill::where($where)->get();

        //                if ($waybills->count() === 0) {
        //                    return $this->fail(self::WAYBILL_NOT_EXIST);
        //                }

        //            $time = gmdate("Y-m-d\TH:i:s\Z", $time);
        //                foreach ($waybills as $value) {
        //                $productTmp = json_decode($value['product_info'], true);
        //                    foreach ($productTmp as $v) {
        //                    $xml[] = [
        //                        'AmazonOrderID'         => $order['order_no'],
        //                        'FulfillmentDate'       => $time,
        //                        'ShippingMethod'        => $value['ship_method'],
        //                        'ShipperTrackingNumber' => !empty($value['tracking']) ? $value['tracking'] : $value['waybill_order'],
        //                        'AmazonOrderItemCode'   => isset($v['order_item_id']) ? $v['order_item_id'] : '',
        //                        'Quantity'              => $v['num'],
        //                    ];
        //                    }
        //                }
        //            } else
        //            {
        //            // 获取订单下的商品信息
        //            $goods = json_decode($order->goods, true);

        //                if (!is_array($goods) || count($goods) === 0)
        //                {
        //                    return $this->fail(self::PURCHASE_NOT_EXIST);
        //                }

        //            $time = gmdate("Y-m-d\TH:i:s\Z", $time);
        //                foreach ($goods as $value) {
        //                $xml[] = [
        //                    'AmazonOrderID'         => $order['order_no'],
        //                    'FulfillmentDate'       => $time,
        //                    'ShippingMethod'        => 'Standard',
        //                    'ShipperTrackingNumber' => getNumString(),
        //                    'AmazonOrderItemCode'   => $value['order_item_id'] ?? '',
        //                    'Quantity'              => $value['num'],
        //                ];
        //                }
        //            }//end if

        //        $data = [
        //            'Shop'     => $storeInfo,
        //            'Products' => $xml,
        //        ];

        //            return $this->success($data);

        //        }//end handle()


        //        private function getMergeOrderIds($order, $type= 1)
        //        {
        //        $merge_id = $order["merge_id"];
        //        // 查询合并订单数据
        //        $merge = OrderMerge::find($merge_id);
        //            if (!$merge) {
        //                return $this->fail(self::ORDER_MERGE_NOT_EXIST);
        //            }

        //            if ($type != 1) {
        //                return $merge;
        //            }

        //        $order_info = json_decode($merge["order_info"], true);
        //            // d($order_info);
        //            return array_column($order_info, "order_id");

        //        }//end getMergeOrderIds()


        //        private function getTrueWaybillOrderIds($order, $merge)
        //        {
        //            if (($order["is_main"] == AmazonOrder::ISMAIN_THELORD && $merge["type"] != OrderMerge::TYPE_ALLSHARING) || $merge["type"] == OrderMerge::TYPE_INDEPENDENT) {
        //                return [
        //                    'order_id',
        //                    '=',
        //                    $order["id"],

        //                ];
        //            }

        //        $order_info = json_decode($merge["order_info"], true);
        //            if ($merge["type"] == OrderMerge::TYPE_ALLSHARING) {
        //                return [
        //                    'order_id',
        //                    'in',
        //                    array_column($order_info, "order_id"),

        //                ];
        //            }

        //        $order_info = arrayCombine($order_info, "is_main");
        //        $main_id = $order_info[2]["order_id"];
        //            if ($merge["type"] == OrderMerge::TYPE_MASTERORDERONLY) {
        //                return [
        //                    'order_id',
        //                    '=',
        //                    $main_id,

        //                ];
        //            }

        //            // type 4
        //            return [
        //                'order_id',
        //                'in',

        //                [
        //                $order["id"],
        //                    $main_id,

        //                ],

        //            ];

        //        }//end getTrueWaybillOrderIds()


        #endregion

        #region SQL
        public IActionResult Invoke()
        {
            /************************************************  
             ================/order/orderDeliver
            select * from `amazon_order` where `amazon_order`.`id` = ? limit *
            select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
            update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
            select `i_store_id` from `to_sell_task` where `i_company_id` = ? and `t_is_finish` = ? and `i_oem_id` = ?
            select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
            update `store` set `d_last_auto_time` = ?, `store`.`updated_at` = ? where `id` = ?
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            update `store` set `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select * from `waybill` where `waybill`.`id` = ? limit *
            update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `waybill`.`updated_at` = ? where `id` = ?
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select `id`, `d_button_name` from `waybill` where ((`i_company_id` = ? and `i_operate_company_id` = ?) or `i_waybill_company_id` = ?) and `d_button_name` in (?, ?) order by `updated_at` asc limit *
            select * from `category` where (`i_company_id` = ? and `i_user_id` = ? and `i_oem_id` = ? and `t_type` = ? and `t_level` = ?)
            select `id`, `i_category_id`, `i_sub_category_id`, `i_distribution_category_id`, `i_distribution_sub_category_id`, `u_lp_n`, `u_lp_k`, `u_lp_s`, `u_lp_d`, `u_price`, `u_quantity`, `d_languages`, `b_language`, `d_source`, `d_ext`, `d_attribute`, `d_variants`, `d_images`, `b_type`, `updated_at`, `d_pid`, `d_coin`, `d_rate` from `product` where (`i_company_id` = ? and `id` = ? and `i_oem_id` = ?) limit *
            select `id`, `d_title`, `i_user_id`, `t_state` from `work_order` where `i_company_id` = ? and `t_state` in (?, ?) limit *
            update `waybill` set `waybill`.`updated_at` = ? where `id` in (?, ?, ?, ?, ?)
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `d_tracking` = ?, `waybill`.`updated_at` = ? where `id` = ?
            select `id`, `i_operate_id`, `updated_at` from `sync_upload` where (`i_store_id` = ? and `t_state` = ? and `updated_at` < ?)
            select `id` from `sync_upload` where `i_store_id` = ? and `t_state` = ? and `i_operate_id` = ?
            select * from `product` where `id` = ? limit *
            select count(*) as aggregate from `product` where (`d_hash_code` = ? and `i_company_id` = ? and `id` != ?) and !(b_type&*) AND !(b_type&*) AND !(b_type&*)
            update `product` set `u_lp_n` = ?, `u_lp_d` = ?, `d_hash_code` = ?, `product`.`updated_at` = ? where `id` = ?
            select count(*) as aggregate from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_oem_id` = ?) and `i_task_id` = ? and `updated_at` between ? and ?
            select `id`, `i_oem_id`, `i_user_id`, `i_company_id`, `i_oem_id`, `i_category_id`, `i_platform_id`, `d_reason`, `u_quantity`, `u_lp_n`, `u_image`, `u_price`, `d_source`, `b_type`, `b_language`, `d_languages`, `updated_at`, `created_at`, `d_coin`, `d_rate` from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_oem_id` = ?) and `i_task_id` = ? and `updated_at` between ? and ? order by `id` desc limit * offset *
            select * from `category` where `category`.`id` in (?)
            select * from `task` where `task`.`id` in (?)
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select * from `task` where (`i_oem_id` = ? and `i_user_id` = ? and `i_group_id` = ? and `d_hash_code` = ?) limit *
            select count(*) as aggregate from `product` where (`d_hash_code` = ? and `i_company_id` = ?) and !(b_type&*) AND !(b_type&*) AND !(b_type&*)
            insert into `product` (`i_oem_id`, `i_company_id`, `i_user_id`, `i_group_id`, `i_task_id`, `u_lp_n`, `u_lp_k`, `u_lp_s`, `u_lp_d`, `d_source`, `d_hash_code`, `d_languages`, `b_language`, `b_type`, `u_price`, `i_platform_id`, `u_platform`, `d_attribute`, `d_variants`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select * from `product` where `id` = ? and b_type & * limit *
            insert into `temp_images` (`d_oss_path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `i_product_id`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?)
            select * from `store` where `i_company_id` = ? and `t_flag` = ? and `t_is_del` = ? and `t_is_verify` = ?
            select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?, ?, ?, ?)
            insert into `resource` (`created_at`, `d_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
            delete from `temp_images` where `id` in (?, ?, ?, ?, ?)
            update `product` set `u_lp_k` = ?, `u_lp_s` = ?, `u_lp_d` = ?, `b_type` = ?, `u_price` = ?, `u_quantity` = ?, `d_source` = ?, `d_ext` = ?, `d_attribute` = ?, `d_variants` = ?, `d_images` = ?, `u_image` = ?, `d_pid` = ?, `d_pid_hash_code` = ?, `c_size` = ?, `d_coin` = ?, `d_rate` = ?, `product`.`updated_at` = ? where `id` = ?
            update `counter_company_business` set `c_product_num` = `c_product_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` = ?
            update `counter_company_business` set `c_report_product_num` = `c_report_product_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` in (?, ?, ?, ?)
            update `counter_user_business` set `c_product_num` = `c_product_num` + *, `counter_user_business`.`updated_at` = ? where `i_user_id` = ?
            ************************************************/
            return View();
        }
        #endregion
    }
}
