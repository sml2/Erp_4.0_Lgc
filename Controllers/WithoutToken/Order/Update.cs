﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.Order
{
    public class Update : Controller
    {
        ////+ 原ERP3.0注释
        #region ERP3.0 PHP
        //        //Public Platform As Inf_Collecting.Platform
        //        //Public OrderID As Integer
        //        //Public Type As Boolean

        //        //Public Enum Results
        //        //        ApiNum = 350
        //        //        Base = ApiNum * 1000
        //        //        <Description("")>
        //        //        OrderIsEmpty = Base + 1
        //        //        <Description("[T]")>
        //        //        DeliveryFailed = Base + 2
        //        //    End Enum

        //        private  $AmazonOrder;
        //    private  $AmazonOrderService;
        //    /**
        //     * 订单不存在
        //     */
        //    const CanNotFindOrder = 1;
        //        /**
        //         * 发货失败
        //         */
        //        const DeliveryFailed = 2;

        //        public function __construct()
        //        {
        //        $this->AmazonOrder = new AmazonOrder;
        //        $this->AmazonOrderService = new AmazonOrderService;
        //        }

        //        public function verifyParams()
        //        {

        //            return ['Platform', 'OrderID', 'Type'];

        //        }

        //        public function getApiId()
        //        {
        //            return 350;
        //        }

        //        public function handle($requestData, $userId, $rule, $session)
        //        {

        //        $platform = $requestData['Platform'];
        //        $order_id = $requestData['OrderID'];
        //        //临时
        //        $type = $requestData['Type'] < 0 ? 1 : $requestData['Type'];

        //        //订单信息
        //        $order_info = $this->AmazonOrderService->getOrderInfoWhereFirst([],$order_id);
        //            /*
        //                    Log::info(var_export($order_info,true));
        //            */
        //            if (!$order_info){

        //                return $this->fail(self::CanNotFindOrder, null, "Can Not Find This Order");

        //            }
        //            //        $now_time = date("Y-m-d H:i:s",$this->getTime());
        //            //        if ($type) {
        //            //
        //            //            $action = "更新订单真发货信息";
        //            //            $update_data = ['true_product_time' => $now_time];
        //            //
        //            //        } else {
        //            //
        //            //            $action = "更新订单假发货信息";
        //            //            $update_data = ['fake_product_time' => $now_time];
        //            //
        //            //        }

        //            DB::beginTransaction();

        //            try
        //            {
        //            $this->AmazonOrderService->changeState($order_id, $type);
        ////            $this->AmazonOrderService->update($order_id,$update_data);
        ////            $where = [
        ////                ["id" , $order_id]
        //////                ["platform" , $platform]
        ////            ];

        ////            $this->AmazonOrderService->updateData($where,$update_data);
        ////            Db::name('amazon_order')->where(['id' => $orderId])->update($updateData);
        //            //增加订单日志
        //            $order_status_text = AmazonOrderModel::ORDER_STATE[$order_info['order_state']];

        //            $this->AmazonOrderService->addLog($order_id, $order_status_text, $order_info['order_state'], '同步店铺信息');
        //                // 提交事务
        //                DB::commit();
        //                return $this->success();

        //            }
        //            catch (\Exception $e) {

        //                // 回滚事务
        //                DB::rollBack();
        //                return  $this->fail(self::DeliveryFailed, null,$e->getMessage());

        //            }
        //            }
        #endregion

        #region SQL
        public IActionResult Invoke()
        {
            /************************************************  
             ================/order/update
            select * from `amazon_order` where `id` = ? limit *
            update `amazon_order` set `b_progress` = (b_progress & ~*) ^ (* << *) , `amazon_order`.`updated_at` = ? where `id` = ?
            insert into `amazon_order_action` (`i_user_id`, `i_group_id`, `i_company_id`, `i_oem_id`, `i_order_id`, `d_action`, `d_user_name`, `d_truename`, `t_order_status`, `d_order_status_text`, `created_at`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
            update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
            select * from `store` where `i_company_id` = ? and `t_flag` = ? and `t_is_del` = ? and `t_is_verify` = ?
            select `id`, `i_operate_id`, `updated_at` from `sync_upload` where (`i_store_id` = ? and `t_state` = ? and `updated_at` < ?)
            select `id` from `sync_upload` where `i_store_id` = ? and `t_state` = ? and `i_operate_id` = ?
            select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
            update `store` set `d_last_auto_time` = ?, `store`.`updated_at` = ? where `id` = ?
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            update `store` set `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
            select `d_order_no` from `amazon_order` where (`i_store_id` = ? and `t_order_state` = ? and `i_company_id` = ? and `t_entry_mode` = ?) limit *
            update `store` set `d_last_auto_update_time` = ?, `store`.`updated_at` = ? where `id` = ?
            select * from `waybill` where `waybill`.`id` = ? limit *
            update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `d_tracking` = ?, `waybill`.`updated_at` = ? where `id` = ?
            select `id`, `d_button_name` from `waybill` where ((`i_company_id` = ? and `i_operate_company_id` = ?) or `i_waybill_company_id` = ?) and `d_button_name` in (?, ?) order by `updated_at` asc limit *
            update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `waybill`.`updated_at` = ? where `id` = ?
            select * from `temp_images` where `id` = ? limit *
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
            select `i_store_id` from `to_sell_task` where `i_company_id` = ? and `t_is_finish` = ? and `i_oem_id` = ?
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select `i_store_id` from `store_group` where `i_group_id` = ?
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?)
            select `id`, `d_title`, `i_user_id`, `t_state` from `work_order` where `i_company_id` = ? and `t_state` in (?, ?) limit *
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select * from `task` where (`i_oem_id` = ? and `i_user_id` = ? and `i_group_id` = ? and `d_hash_code` = ?) limit *
            select count(*) as aggregate from `product` where (`d_hash_code` = ? and `i_company_id` = ?) and !(b_type&*) AND !(b_type&*) AND !(b_type&*)
            insert into `product` (`i_oem_id`, `i_company_id`, `i_user_id`, `i_group_id`, `i_task_id`, `u_lp_n`, `u_lp_k`, `u_lp_s`, `u_lp_d`, `d_source`, `d_hash_code`, `d_languages`, `b_language`, `b_type`, `u_price`, `i_platform_id`, `u_platform`, `d_attribute`, `d_variants`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            insert into `temp_images` (`d_oss_path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `i_product_id`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?)
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?) order by `d_last_auto_time` asc limit *
            insert into `resource` (`created_at`, `d_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
            delete from `temp_images` where `id` in (?, ?, ?)
            update `product` set `u_lp_k` = ?, `u_lp_s` = ?, `u_lp_d` = ?, `b_type` = ?, `u_price` = ?, `u_quantity` = ?, `d_source` = ?, `d_ext` = ?, `d_attribute` = ?, `d_variants` = ?, `d_images` = ?, `u_image` = ?, `d_pid` = ?, `d_pid_hash_code` = ?, `c_size` = ?, `d_coin` = ?, `d_rate` = ?, `product`.`updated_at` = ? where `id` = ?
            update `counter_company_business` set `c_product_num` = `c_product_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` = ?
            update `counter_company_business` set `c_report_product_num` = `c_report_product_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` in (?, ?, ?, ?)
            update `counter_user_business` set `c_product_num` = `c_product_num` + *, `counter_user_business`.`updated_at` = ? where `i_user_id` = ?
            select `d_action`, `d_truename`, `created_at` from `amazon_order_action` where `i_order_id` = ? order by `created_at` desc
            select `i_order_id`, `d_remark`, `d_truename`, `created_at` from `amazon_order_remark` where `i_company_id` = ? and `i_order_id` = ? order by `created_at` desc
            select `id`, `d_logistic`, `d_waybill_order`, `i_company_id`, `d_express`, `d_tracking`, `d_deliver_date`, `t_waybill_state`, `d_carriage_unit`, `d_carriage_num`, `d_button_name`, `i_operate_company_id`, `t_is_verify`, `i_waybill_company_id` from `waybill` where `i_order_id` = ? and `i_platform_id` = ?
            select * from `purchase` where `i_order_id` = ? order by `id` desc
            select * from `stock_log` where `i_execute_company_id` = ? and `i_company_id` = ? and `h_order_no_hashcode` = ? order by `created_at` desc
            select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?)
            update `amazon_order` set `b_progress` = ?, `amazon_order`.`updated_at` = ? where `id` = ?
            select count(*) as aggregate from `amazon_order` where `i_company_id` = ? and `i_oem_id` = ? and `t_order_state` = ? and `d_latest_ship_time` < ? and `d_latest_ship_time` > ?
            select `t_entry_mode`, `id`, `d_receiver_nation`, `d_receiver_nation_short`, `u_url`, `d_earliest_ship_time`, `d_order_no`, `d_pay_money`, `i_company_id`, `d_product_num`, `t_order_state`, `d_create_time`, `t_shipping_type`, `d_fake_product_time`, `d_true_product_time`, `i_store_id`, `id`, `d_coin`, `u_truename`, `d_receiver_name`, `d_receiver_email`, `b_progress`, `d_latest_ship_time`, `i_merge_id`, `d_process_waybill`, `d_process_purchase`, `i_purchase_company_id`, `i_waybill_company_id`, `created_at`, `d_rate`, `t_is_batch_ship`, `d_seller_id`, `d_profit`, `d_invoice` from `amazon_order` where `i_company_id` = ? and `i_oem_id` = ? and `t_order_state` = ? and `d_latest_ship_time` < ? and `d_latest_ship_time` > ? order by `d_create_time` desc, `id` desc limit * offset *
            select `id`, `i_order_id`, `d_purchase_tracking_number`, `d_purchase_order_no` from `purchase` where `i_order_id` in (?)
            select `id`, `i_order_id`, `d_waybill_order`, `d_express` from `waybill` where `i_order_id` in (?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select count(*) as aggregate from `amazon_order` where `i_company_id` = ? and `i_oem_id` = ?
            select `t_entry_mode`, `id`, `d_receiver_nation`, `d_receiver_nation_short`, `u_url`, `d_earliest_ship_time`, `d_order_no`, `d_pay_money`, `i_company_id`, `d_product_num`, `t_order_state`, `d_create_time`, `t_shipping_type`, `d_fake_product_time`, `d_true_product_time`, `i_store_id`, `id`, `d_coin`, `u_truename`, `d_receiver_name`, `d_receiver_email`, `b_progress`, `d_latest_ship_time`, `i_merge_id`, `d_process_waybill`, `d_process_purchase`, `i_purchase_company_id`, `i_waybill_company_id`, `created_at`, `d_rate`, `t_is_batch_ship`, `d_seller_id`, `d_profit`, `d_invoice` from `amazon_order` where `i_company_id` = ? and `i_oem_id` = ? order by `d_create_time` desc, `id` desc limit * offset *
            select `id`, `i_order_id`, `d_purchase_tracking_number`, `d_purchase_order_no` from `purchase` where `i_order_id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `i_order_id`, `d_waybill_order`, `d_express` from `waybill` where `i_order_id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            update `waybill` set `waybill`.`updated_at` = ? where `id` in (?, ?, ?, ?, ?)
            select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select * from `stock_log` where `i_execute_company_id` = ? and `i_company_id` = ? and `h_order_no_hashcode` = ? and (`i_group_id` = ? or `i_group_id` = ?) order by `created_at` desc
            select count(*) as aggregate from `amazon_order` where `i_company_id` = ? and `i_oem_id` = ? and (b_progress&*)>>*=*
            select `t_entry_mode`, `id`, `d_receiver_nation`, `d_receiver_nation_short`, `u_url`, `d_earliest_ship_time`, `d_order_no`, `d_pay_money`, `i_company_id`, `d_product_num`, `t_order_state`, `d_create_time`, `t_shipping_type`, `d_fake_product_time`, `d_true_product_time`, `i_store_id`, `id`, `d_coin`, `u_truename`, `d_receiver_name`, `d_receiver_email`, `b_progress`, `d_latest_ship_time`, `i_merge_id`, `d_process_waybill`, `d_process_purchase`, `i_purchase_company_id`, `i_waybill_company_id`, `created_at`, `d_rate`, `t_is_batch_ship`, `d_seller_id`, `d_profit`, `d_invoice` from `amazon_order` where `i_company_id` = ? and `i_oem_id` = ? and (b_progress&*)>>*=* order by `d_create_time` desc, `id` desc limit * offset *
            select `id`, `i_order_id`, `d_purchase_tracking_number`, `d_purchase_order_no` from `purchase` where `i_order_id` in (?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `i_order_id`, `d_waybill_order`, `d_express` from `waybill` where `i_order_id` in (?, ?, ?, ?, ?, ?, ?, ?)
            select count(*) as aggregate from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_oem_id` = ?) and `i_task_id` = ? and `updated_at` between ? and ?
            select `id`, `i_oem_id`, `i_user_id`, `i_company_id`, `i_oem_id`, `i_category_id`, `i_platform_id`, `d_reason`, `u_quantity`, `u_lp_n`, `u_image`, `u_price`, `d_source`, `b_type`, `b_language`, `d_languages`, `updated_at`, `created_at`, `d_coin`, `d_rate` from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_oem_id` = ?) and `i_task_id` = ? and `updated_at` between ? and ? order by `id` desc limit * offset *
            select * from `category` where `category`.`id` in (?)
            select * from `task` where `task`.`id` in (?)
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
            ************************************************/
            return View();
        }
        #endregion
    }
}
