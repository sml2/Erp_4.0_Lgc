﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.Order
{
    public class Upload : Controller
    {
        ////原ERP3.0注释
        #region ERP3.0 PHP
        //        private AmazonOrderService $AmazonOrderService;
        //    private StoreService $StoreService;
        //    private storeBusinessService $storeBusinessService;
        //    private companyBusinessService $companyBusinessService;
        //    private realFinanceService $realFinanceService;
        //    private AmazonOrder $AmazonOrder;
        //    private Finance $finance;

        //    public function __construct(Finance $finance)
        //        {
        //        $this->AmazonOrder = new AmazonOrder();
        //        $this->StoreService = new StoreService();
        //        $this->AmazonOrderService = new AmazonOrderService();
        //        $this->realFinanceService = new realFinanceService();
        //        $this->storeBusinessService = new storeBusinessService();
        //        $this->companyBusinessService = new companyBusinessService();
        //        $this->finance = $finance;
        //        }

        //        /** @var int 亚马逊订单已存在 更新 */
        //        const Exist = 0;
        //        /** @var int 亚马逊订单已失效 */
        //        const Canceled = 1;
        //        /** @var int 订单已存在且无更新 */
        //        const OrderNoUpdate = 2;
        //        /** @var int 亚马逊订单商品不存在 */
        //        const NoExist = 3;
        //        /** @var int 上传失败 */
        //        const OrderInsertFailed = 4;
        //        /** @var int 订单已存在且存在更新 */
        //        const OrderUpdateSucceed = 5;
        //        /** @var int 公司下无店铺 */
        //        const ComanyNoStore = 6;
        //        /** @var int 店铺不存在 */
        //        const StoreNoExist = 7;
        //        /** @var int 国家字典为空 */
        //        const NoNationDictionary = 8;
        //        /** @var int 找不到对应国家码 */
        //        const DictionaryNoKey = 9;
        //        /** @var int 订单汇总失败 */
        //        const OrderUpdateStatisticFailed = 10;
        //        /** @var int 获取货币汇率失败 */
        //        const UnitCacheFailed = 11;

        //        const PULL_ORDER = 1;
        //        const UPDATE_ORDER = 2;

        //        /** @var int 手动拉取订单 */
        //        public const AUTO_HAND = 1;
        //        /** @var int 自动分配，@周扬扬 */
        //        public const AUTO_ALLOT = 2;
        //        /** @var int 自动更新订单 */
        //        public const AUTO_UPLOAD = 3;

        //        private int $isAuto = self::AUTO_HAND;

        //    /**
        //     * 订单状态对应值
        //     * @var array|int[]
        //     * PendingAvailability
        //     * 只有预订订单才有此状态。订单已生成，但是付款未授权且商品的发售日期是将来的某一天。订单尚不能进行发货。请注意：仅在日本 (JP)，Preorder 才可能是一个 OrderType 值。
        //     * Pending
        //     * 订单已生成，但是付款未授权。订单尚不能进行发货。请注意：对于 OrderType = Standard 的订单，初始的订单状态是 Pending。对于 OrderType = Preorder 的订单（仅适用于 JP），初始的订单状态是 PendingAvailability，当进入付款授权流程时，订单状态将变为 Pending。
        //     * Unshipped
        //     * 付款已经过授权，订单已准备好进行发货，但订单中商品尚未发运。
        //     * PartiallyShipped
        //     * 订单中的一个或多个（但并非全部）商品已经发货。
        //     * Shipped
        //     * 订单中的所有商品均已发货。
        //     * InvoiceUnconfirmed
        //     * 订单中的所有商品均已发货。但是卖家还没有向亚马逊确认已经向买家寄出发票。请注意：此参数仅适用于中国地区。
        //     * Canceled
        //     * 订单已取消。
        //     * Unfulfillable
        //     * 订单无法进行配送。该状态仅适用于通过亚马逊零售网站之外的渠道下达但由亚马逊进行配送的订单。
        //     */
        //    private array $orderState = [
        //        'PendingAvailability' => AmazonOrderModel::ORDER_STATE_UNPAID,
        //        'Pending'             => AmazonOrderModel::ORDER_STATE_UNPAID,
        //        'Canceled'            => AmazonOrderModel::ORDER_STATE_CANCEL,
        //        'Unfulfillable'       => AmazonOrderModel::ORDER_STATE_DISTRIBUTION,
        //        'Unshipped'           => AmazonOrderModel::ORDER_STATE_PENDING,
        //        'PartiallyShipped'    => AmazonOrderModel::ORDER_STATE_SENT,
        //        'Shipped'             => AmazonOrderModel::ORDER_STATE_SENT,
        //        'InvoiceUnconfirmed'  => AmazonOrderModel::ORDER_STATE_DELIVERY,
        //    ];

        //    /** @var array|int[] 结束订单状态 */
        //    private static array $endState = [
        //        AmazonOrderModel::ORDER_STATE_CANCEL,
        //        AmazonOrderModel::ORDER_STATE_DISTRIBUTION,
        //        AmazonOrderModel::ORDER_STATE_SENT
        //    ];

        //    /**
        //     *  货运方式
        //     * @var array|int[]
        //     */
        //    private array $shipping_type = [
        //        'AFN' => AmazonOrderModel::SHIPPINGTYPE_AFN,
        //        'MFN' => AmazonOrderModel::SHIPPINGTYPE_MFN
        //    ];

        //    /**
        //     *          验证参数是否缺失
        //     * @Sid           店铺ID
        //     * @OrderInfo     订单详情信息
        //     * @UploadType    1手动拉取上报 2自动拉取待发货订单3自动更新待发货状态订单
        //     * @return array|string[]
        //     */
        //    public function verifyParams()
        //        {
        //            return ['Sid', 'OrderInfo', 'UploadType'];
        //        }

        //        /**
        //         * 获取接口值
        //         * @return int
        //         */
        //        public function getApiId()
        //        {
        //            return 320;
        //        }

        //        /**
        //         * 处理
        //         * @param                       $requestData
        //         * @param                       $userId
        //         * @param                       $rule
        //         * @param SessionData $session
        //         * @return array|JsonResponse
        //         */
        //        public function handle($requestData, $userId, $rule, $session)
        //        {
        //        //自动拉取
        //        $this->isAuto = (int)($requestData['UploadType'] ?? 1);
        //        //解析json数据
        //        $orderInfo = json_decode($requestData['OrderInfo'], true);

        //            //亚马逊订单基础信息
        //            if (array_key_exists('OrderBasic', $orderInfo))
        //            {
        //            $orderBasic = $orderInfo['OrderBasic'];
        //            }
        //            else
        //            {
        //                return $this->fail(self::ERR_PARAMETER, 'OrderBasic');
        //            }
        //            //亚马逊订单商品
        //            if (array_key_exists('OrderMessage', $orderInfo))
        //            {
        //            $orderMessage = $orderInfo['OrderMessage'];
        //            }
        //            else
        //            {
        //                return $this->fail(self::ERR_PARAMETER, 'OrderMessage');
        //            }

        //            //亚马逊订单费用汇总数据
        //            if (array_key_exists('ShipmentEventList', $orderInfo))
        //            {
        //            $ShipmentEventList = $orderInfo['ShipmentEventList'];
        //            }
        //            else
        //            {
        //                return $this->fail(self::ERR_PARAMETER, 'ShipmentEventList');
        //            }

        //            //退款费用计算
        //            if (array_key_exists('RefundEventList', $orderInfo))
        //            {
        //            $RefundEventList = $orderInfo['RefundEventList'];
        //            }
        //            else
        //            {
        //                return $this->fail(self::ERR_PARAMETER, 'RefundEventList');
        //            }

        //        $stores = $session->getStores();
        //            if (!$stores) {
        //                return $this->fail(self::ComanyNoStore);
        //            }

        //        $sid = $requestData['Sid'];
        //            //寻找传过来的店铺是否在公司的子店铺中存在
        //            if (!array_key_exists($sid, $stores))
        //            {
        //                return $this->fail(self::StoreNoExist);
        //            }
        //        //数据词典
        //        $nation = $this->getNationCache();
        //            if (!$nation) {
        //                return $this->fail(self::NoNationDictionary);
        //            }
        //        //数字字典 国家信息
        //        $nation = arrayCombine($nation, 'short');
        //        //订单初始手续费
        //        $fee = $this->initializeOrderFee($ShipmentEventList, $RefundEventList);
        //        //存在退款 存在手续费退还
        //        $refund = $this->initializeOrderRefund($RefundEventList);

        //        $currency_unit = $orderBasic['OrderTotal']['CurrencyCode'] ?? 'CNY';
        //        // TODO: bcmath function argument is not well-formed
        //        $fee = FromConversion(abs($fee), $currency_unit);
        //        $refund = FromConversion(abs($refund), $currency_unit);

        //        //拉取订单店铺
        //        $store = $stores[$sid];
        //        //当前公司信息
        //        $company = $this->getCompanyCache();

        //        //组装订单基础信息
        //        $orderData = $this->orderBasic($orderBasic, $orderMessage, $nation, $store, $session, $fee, $refund, $orderMessage[0]['Url']);
        //            if ($this->needreturn) {
        //                return $orderData;
        //            }
        //            //分销设置分销信息
        //            if ($orderData['shipping_type'] == AmazonOrderModel::SHIPPINGTYPE_MFN && $company['is_distribution'] === CompanyModel::DISTRIBUTION_TRUE) {
        //                if ($company['distribution_waybill'] === CompanyModel::WAYBILL_REPORT_TRUE) {
        //                $orderData['waybill_company_id'] = $session->getReportId();
        //                }
        //                if ($company['distribution_purchase'] === CompanyModel::PURCHASE_REPORT_TRUE) {
        //                $orderData['purchase_company_id'] = $session->getReportId();
        //                }
        //            }

        //        $order_total = $orderData['pay_money'];
        //        $order_total_fee = $fee ?? 0;
        //        $order_total_refund = ($refund ?? 0);

        //            DB::beginTransaction();
        //            try
        //            {
        //            $newOrder = AmazonOrderModel::query()->create($orderData);
        //            $this->AmazonOrder->addLog($newOrder->id, '拉取订单', 0, '成功从亚马逊拉取本订单');

        //            $this->finance->statisticsOrder($orderData, $company, $order_total, $order_total_fee, $order_total_refund);

        //            //更改last_order_time  (时间是亚马逊平台创建订单的时间)
        //            $this->updateStoreLastTime($store['id'], $orderBasic['PurchaseDate'], $orderData['order_state']);
        //                // 提交事务
        //                DB::commit();

        //                return $this->success();

        //            }
        //            catch (\Exception $e) {
        //                // 回滚事务
        //                DB::rollback();
        //                return $this->fail(self::OrderInsertFailed, $e->getMessage());
        //            }

        //            }

        //            /**
        //             * 整理订单数据
        //             * @param           $orderBasic
        //             * @param           $orderMessage
        //             * @param           $nation
        //             * @param           $store
        //             * @param           $session
        //             * @param int|float $fee
        //             * @param int|float $refund
        //             * @param string    $url
        //             * @return array|JsonResponse
        //             */
        //            public function orderBasic($orderBasic, $orderMessage, $nation, $store, $session, $fee = 0, $refund = 0, $url = '')
        //            {

        //        $pay_time = Carbon::parse($orderBasic['PurchaseDate'], 'Asia/Shanghai')->tz(config('timezone'))->toDateTimeString();

        //        $orderInfo = $this->AmazonOrderService->getOrderInfoWhereFirst([], '', $orderBasic['AmazonOrderId'], $session->company_id);
        //        $amazon_shipping_type = $orderBasic['FulfillmentChannel'];
        //            //订单取消,订单原状态有效
        //            if ($orderBasic['OrderStatus'] === 'Canceled') {
        //                if ($orderInfo) {
        //                $this->handleRefund($orderInfo, $pay_time, $store['id']);
        //                }
        //                return $this->fail(self::Canceled);
        //            }

        //        $currency_unit = $orderBasic['OrderTotal']['CurrencyCode'] ?? '';//TODO-- 这个值有可能为空，至于怎么处理等待商量解决
        //        // TODO 这是真厉害
        //        $order_state = @$this->orderState[$orderBasic['OrderStatus']] ? $this->orderState[$orderBasic['OrderStatus']] : AmazonOrderModel::ORDER_STATE_DISTRIBUTION;

        //        $address = $orderBasic['ShippingAddress']['AddressLine1'] ?? '';
        //        $address2 = $orderBasic['ShippingAddress']['AddressLine2'] ?? '';
        //        $address3 = $orderBasic['ShippingAddress']['AddressLine3'] ?? '';

        //        $order_total = (isset($orderBasic['OrderTotal']['Amount']) && $currency_unit) ? FromConversion($orderBasic['OrderTotal']['Amount'], $currency_unit) : 0;

        //        $delivery = $orderBasic['NumberOfItemsShipped'] ?? 0;

        //        $delivery_no = $orderBasic['NumberOfItemsUnshipped'] ?? 0;

        //        $latest = Carbon::parse($orderBasic['LatestShipDate'])->tz(config('timezone'));

        //        $earliest = Carbon::parse($orderBasic['EarliestShipDate'])->tz(config('timezone'));

        //        $latest_ship_time = isset($orderBasic['LatestShipDate']) ? $latest->toDateTimeString() : '';

        //        $earliest_ship_time = isset($orderBasic['EarliestShipDate']) ? $earliest->toDateTimeString() : '';

        //        $receiver_province = $orderBasic['ShippingAddress']['StateOrRegion'] ?? '';

        //        $receiver_city = $orderBasic['ShippingAddress']['City'] ?? $receiver_province;

        //        $nationShort = $orderBasic['ShippingAddress']['CountryCode'] ?? $store['nation_short'];
        //        //订单数据
        //        $data = [
        //            'refund'                => $refund,
        //            'fee'                   => $fee,
        //            'order_state'           => $order_state,
        //            'order_no'              => $orderBasic['AmazonOrderId'],
        //            'order_no_hash'         => createHashCode($orderBasic['AmazonOrderId']),
        //            'shipping_type'         => $this->shipping_type[$orderBasic['FulfillmentChannel']],
        //            'user_name'             => $orderBasic['ShippingAddress']['Name'] ?? '',
        //            'pay_time'              => $pay_time,
        //            'receiver_phone'        => $orderBasic['ShippingAddress']['Phone'] ?? '',
        //            'receiver_nation_id'    => $nation[$nationShort]['id'],
        //            'receiver_nation_short' => $nationShort,
        //            'receiver_nation'       => $nation[$nationShort]['name'],
        //            'receiver_province'     => $receiver_province,
        //            'receiver_city'         => $receiver_city,
        //            'receiver_district'     => $orderBasic['ShippingAddress']['County'] ?? '',
        //            'receiver_address'      => $address,
        //            'receiver_address2'     => $address2,
        //            'receiver_address3'     => $address3,
        //            'receiver_zip'          => $orderBasic['ShippingAddress']['PostalCode'] ?? '',
        //            'receiver_name'         => $orderBasic['ShippingAddress']['Name'] ?? '',
        //            'receiver_email'        => $orderBasic['BuyerEmail'] ?? '',
        //            'store_id'              => $store['id'],
        //            'seller_id'             => $store['seller_id'],
        //            'order_money'           => $order_total,
        //            'pay_money'             => $order_total,
        //            'create_time'           => $pay_time,
        //            'coin'                  => $currency_unit,
        //            'delivery_time'         => date("Y-m-d H:i:s", ((int)floor(($latest->timestamp - $earliest->timestamp) / 2) + $earliest->timestamp)),
        //            'last_time'             => isset($orderBasic['EarliestDeliveryDate']) ? Carbon::parse($orderBasic['EarliestDeliveryDate'])->tz(config('timezone'))->toDateTimeString() : date("Y-m-d 23:59:59"),
        //            'delivery'              => $delivery,
        //            'delivery_no'           => $delivery_no,
        //            'rate'                  => isset($currency_unit) ? $this->getUnitSingleRate($currency_unit) : 0,
        //            'latest_ship_time'      => $latest_ship_time,
        //            'earliest_ship_time'    => $earliest_ship_time,
        //            'company_id'            => $session->company_id,
        //            'user_id'               => $session->user_id,
        //            'group_id'              => $session->group_id,
        //            'oem_id'                => $session->oem_id,
        //            'default_ship_from_location_address' => json_encode($orderBasic['DefaultShipFromLocationAddress'] ?? null)   //发票上传稳定后此数据不需要再被更新
        //        ];

        //        if ($orderInfo) {
        //            $old_goods_data = json_decode($orderInfo->getRawOriginal('goods'), true);
        //            $old_goods_data = array_combine(array_column($old_goods_data, "asin"), $old_goods_data);

        //            $goods_data = $this->getGoodsData($orderMessage, $store, self::UPDATE_ORDER, $old_goods_data);
        //            $data['goods'] = array_values($goods_data);
        //            $data['profit'] = $order_total - $fee - $refund - $orderInfo->getRawOriginal('purchase_fee') - $orderInfo->getRawOriginal('shipping_money') - $orderInfo->getRawOriginal('loss');
        //            $res = $this->AmazonOrderService->update($orderInfo['id'], $data);
        //            if (!$res) {
        //                return $this->fail(self::OrderNoUpdate);
        //            }

        //            DB::beginTransaction();
        //            try {
        //                //TODO-- 这里应该重新计算毛利，手续费有可能发生变更
        //                //手续费和退款发生变更的时候，汇总差额，重新计算毛利

        //                $temp_statistic_order_total = $data['order_money'] - $orderInfo->getRawOriginal('order_money');
        //                $temp_statistic_order_total_fee = $data['fee'] - $orderInfo->getRawOriginal('fee');
        //                $temp_statistic_order_total_refund = $data['refund'] - $orderInfo->getRawOriginal('refund');
        //                $company = $this->getCompanyCache('');
        //                //汇总差额 目前版本忽略 个人和用户组
        //                if ($temp_statistic_order_total > 0 || $temp_statistic_order_total_fee > 0 || $temp_statistic_order_total_refund > 0) {
        //                    $temp_statistic_order_total_increment = $temp_statistic_order_total > 0 ? $temp_statistic_order_total : 0;
        //                    $temp_statistic_order_total_fee_increment = $temp_statistic_order_total_fee > 0 ? $temp_statistic_order_total_fee : 0;
        //                    $temp_statistic_order_total_refund_increment = $temp_statistic_order_total_refund > 0 ? $temp_statistic_order_total_refund : 0;

        //                    //汇总差额 店铺统计
        //                    $this->storeBusinessService->incrementOrder($data['store_id'], $temp_statistic_order_total_increment, $temp_statistic_order_total_fee_increment, $temp_statistic_order_total_refund_increment);
        //                    //汇总差额 当前公司计数、统计
        //                    $this->companyBusinessService->incrementOrder($data['company_id'], 0, $temp_statistic_order_total_increment, $temp_statistic_order_total_fee_increment, $temp_statistic_order_total_refund_increment);
        //                    //汇总差额 月报
        //                    $this->realFinanceService->incrementOrder($company, $temp_statistic_order_total_increment, $temp_statistic_order_total_fee_increment, $temp_statistic_order_total_refund_increment);
        //                }

        //                if ($temp_statistic_order_total < 0 || $temp_statistic_order_total_fee < 0 || $temp_statistic_order_total_refund < 0) {
        //                    $temp_statistic_order_total_decrement = $temp_statistic_order_total < 0 ? abs($temp_statistic_order_total) : 0;
        //                    $temp_statistic_order_total_fee_decrement = $temp_statistic_order_total_fee < 0 ? abs($temp_statistic_order_total_fee) : 0;
        //                    $temp_statistic_order_total_refund_decrement = $temp_statistic_order_total_refund < 0 ? abs($temp_statistic_order_total_refund) : 0;

        //                    //汇总差额 店铺统计
        //                    $this->storeBusinessService->decrementOrder($data['store_id'], $temp_statistic_order_total_decrement, $temp_statistic_order_total_fee_decrement, $temp_statistic_order_total_refund_decrement);
        //                    //汇总差额 当前公司计数、统计
        //                    $this->companyBusinessService->decrementOrder($data['company_id'], 0, $temp_statistic_order_total_decrement, $temp_statistic_order_total_fee_decrement, $temp_statistic_order_total_refund_decrement);
        //                    //汇总差额 月报
        //                    $this->realFinanceService->decrementOrder($company, $temp_statistic_order_total_decrement, $temp_statistic_order_total_fee_decrement, $temp_statistic_order_total_refund_decrement);
        //                }


        //                //更改last_order_time  @todo (时间是亚马逊平台创建订单的时间)
        //                $this->updateStoreLastTime($store['id'], $orderBasic['PurchaseDate'], $order_state);
        //                // 提交事务
        //                DB::commit();
        //            } catch (\Exception $e) {
        //                // 回滚事务
        //                DB::rollback();
        //                return $this->fail(self::OrderInsertFailed, $e->getMessage());
        //            }

        //            $this->AmazonOrder->addLog($orderInfo['id'], '更新订单', $order_state, '拉取更新当前订单');
        //            //终止，跳过
        //            /**
        //             * 更新订单信息成功，从这退出
        //             */
        //            return $this->fail(self::OrderUpdateSucceed);

        //        }

        //        $data['url'] = $url;

        //        $goods_data = $this->getGoodsData($orderMessage, $store, self::PULL_ORDER);

        //        if ($goods_data) {
        //            $data['goods'] = json_encode($goods_data['goods']);
        //            $data['product_num'] = $goods_data['Sum_num'];
        //            $data['product_money'] = $goods_data['Sum_money'];
        //        } else {
        //            return $this->fail(self::NoExist);
        //        }

        //        $biter = new Progress(BitwiseFlag::FLAG_BASE);
        //        if ($amazon_shipping_type === 'AFN') {
        //            $biter->setProgress(AmazonOrderModel::PROGRESS_FBA);
        //        } else {
        //            $biter->setProgress(AmazonOrderModel::PROGRESS_UNTREATED);

        //            switch ($order_state) {
        //                case AmazonOrderModel::ORDER_STATE_CANCEL:
        //                    $biter->setProgress(AmazonOrderModel::PROGRESS_CANCEL);
        //                    break;
        //                case AmazonOrderModel::ORDER_STATE_DISTRIBUTION:
        //                    $biter->setProgress(AmazonOrderModel::PROGRESS_PROCESSING);
        //                    break;
        ////            case  7:
        ////                $biter->setProgress(AmazonOrderModel::PROGRESS_UNTREATED);
        ////                break;
        //                case AmazonOrderModel::ORDER_STATE_DELIVERY:
        //                case AmazonOrderModel::ORDER_STATE_SENT:
        //                    $biter->setProgress(AmazonOrderModel::PROGRESS_COMPLETE);
        //                    break;
        //            }
        //        }

        //        $data['progress'] = $biter->getFlags();
        //        $data['delivery_num'] = $delivery;
        //        $data['profit'] = $order_total - $fee - $refund;
        //        $data['user_id'] = $session->user_id;
        //        $data['truename'] = $session->truename;
        //        return $data;
        //    }

        //    /**
        //     * 已存在订单取消
        //     * User: CCY
        //     * Date: 2020/9/10
        //     * @param AmazonOrderModel $orderInfo
        //     */
        //    private function handleRefund($orderInfo, $pay_time, $sid)
        //    {
        //        if ($orderInfo['order_state'] === AmazonOrderModel::ORDER_STATE_CANCEL) {
        //            return false;
        //        }
        //        $progressBiter = new Progress($orderInfo['progress_original']);
        //        $progressBiter->setProgress(AmazonOrderModel::PROGRESS_CANCEL);
        //        $updateOrder = [
        //            'progress'    => $progressBiter->getFlags(),
        //            'order_state' => AmazonOrderModel::ORDER_STATE_CANCEL
        //        ];

        //        // 启动事务
        //        DB::beginTransaction();
        //        try {
        //            $this->AmazonOrderService->update($orderInfo['id'], $updateOrder);
        //            $this->AmazonOrder->addLog($orderInfo['id'], '拉取订单', AmazonOrderModel::ORDER_STATE_CANCEL, '当前订单在亚马逊中已取消');
        //            // 退回账单统计
        //            // TODO 可能出现为负
        //            $this->finance->statisticsOrder($orderInfo, $this->getCompanyCache(), -$orderInfo->getRawOriginal('pay_money'), -$orderInfo->getRawOriginal('fee'), -$orderInfo->getRawOriginal('refund'), -1);
        //            $this->updateStoreLastTime($sid, $pay_time, AmazonOrderModel::ORDER_STATE_CANCEL);
        //            // 提交事务
        //            DB::commit();
        //        } catch (\Exception $e) {
        //            // 回滚事务
        //            DB::rollback();
        //            return $this->fail(self::Canceled, $e->getMessage());
        //        }
        //        return $this->fail(self::Canceled);
        //    }

        //    /**
        //     * 更新店铺最后拉取订单时间
        //     * @param int|string $storeId
        //     * @param int|string $payTime
        //     * @param int|string $orderState
        //     * @author ryu <mo5467@126.com>
        //     */
        //    private function updateStoreLastTime($storeId, $payTime, $orderState): void
        //    {
        //        //更改last_order_time (时间是亚马逊平台创建订单的时间)
        //        $payTime = Carbon::create($payTime)->tz(config('timezone'))
        //            ->toDateTimeString();
        //        $updateData = [
        //            'last_order_time' => $payTime,
        //            'updated_at'      => date('Y-m-d H:i:s', $this->getTime())
        //        ];

        //        if (in_array((int)$orderState, self::$endState, true)) {
        //            $updateData['last_ship_time'] = $payTime;
        //            if ($this->isAuto === self::AUTO_ALLOT) {
        //                $updateData['last_auto_ship_time'] = $payTime;
        //            }
        //        }
        //        if ($this->isAuto === self::AUTO_ALLOT) {
        //            $updateData['last_allot_time'] = $this->getTime();
        //        }
        //        if ($this->isAuto === self::AUTO_UPLOAD) {
        //            $updateData['last_auto_update_time'] = $this->getTime();
        //        }
        //        $this->StoreService->updateById($storeId, $updateData);
        //    }

        //    /**
        //     * 初始化手续费
        //     * @param $ShipmentEventList
        //     * @param $RefundEventList
        //     * @return int|float
        //     * @author ryu <mo5467@126.com>
        //     */
        //    private function initializeOrderFee($ShipmentEventList, $RefundEventList)
        //    {
        //        $fee = 0;
        //        if (isset($ShipmentEventList['ShipmentEvent']['ShipmentItemList']['ShipmentItem'])) {
        //            $ShipmentItem = $ShipmentEventList['ShipmentEvent']['ShipmentItemList']['ShipmentItem'];
        //            foreach ($ShipmentItem as $value) {
        //                //税 2021.4.8 董哥要求
        //                if (isset($value['ItemTaxWithheldList']['TaxWithheldComponent']['TaxesWithheld']['ChargeComponent'])) {
        //                    foreach ($value['ItemTaxWithheldList']['TaxWithheldComponent']['TaxesWithheld']['ChargeComponent'] as $v) {
        //                        $fee += $v['ChargeAmount']['CurrencyAmount'];
        //                    }
        //                }

        //                //佣金
        //                if (isset($value['ItemFeeList']['FeeComponent'])) {
        //                    foreach ($value['ItemFeeList']['FeeComponent'] as $v) {
        //                        $fee += $v['FeeAmount']['CurrencyAmount'];
        //                    }
        //                }
        //            }
        //        }

        //        if (isset($RefundEventList['ShipmentEvent']['ShipmentItemAdjustmentList']['ShipmentItem'])) {
        //            $ShipmentItem = $RefundEventList['ShipmentEvent']['ShipmentItemAdjustmentList']['ShipmentItem'];
        //            foreach ($ShipmentItem as $value) {
        //                //税 2021.4.8 董哥要求
        //                if (isset($value['ItemTaxWithheldList']['TaxWithheldComponent']['TaxesWithheld']['ChargeComponent'])) {
        //                    foreach ($value['ItemTaxWithheldList']['TaxWithheldComponent']['TaxesWithheld']['ChargeComponent'] as $v) {
        //                        $fee += $v['ChargeAmount']['CurrencyAmount'];
        //                    }
        //                }
        //                //退款手续费
        //                if (isset($value['ItemFeeAdjustmentList']['FeeComponent'])) {
        //                    foreach ($value['ItemFeeAdjustmentList']['FeeComponent'] as $v) {
        //                        $fee += $v['FeeAmount']['CurrencyAmount'];
        //                    }
        //                }
        //            }
        //        }
        //        return $fee;
        //    }

        //    /**
        //     * 初始化退款
        //     * @param $RefundEventList
        //     * @return int|float
        //     * @author ryu <mo5467@126.com>
        //     */
        //    private function initializeOrderRefund($RefundEventList)
        //    {
        //        $refund = 0;
        //        if (isset($RefundEventList['ShipmentEvent']['ShipmentItemAdjustmentList']['ShipmentItem'])) {
        //            $ShipmentItem = $RefundEventList['ShipmentEvent']['ShipmentItemAdjustmentList']['ShipmentItem'];
        //            foreach ($ShipmentItem as $value) {
        //                //退款金额
        //                if (isset($value['ItemChargeAdjustmentList']['ChargeComponent'])) {
        //                    foreach ($value['ItemChargeAdjustmentList']['ChargeComponent'] as $v) {
        //                        $refund += $v['ChargeAmount']['CurrencyAmount'];
        //                    }
        //                }
        //            }
        //        }
        //        return $refund;
        //    }

        //    /**
        //     * @param      $orderMessage
        //     * @param      $store
        //     * @param int  $type 1 拉取新订单  2 更新已存在订单
        //     * @param null $old_goods_data
        //     * @return array
        //     */
        //    private function getGoodsData($orderMessage, $store, $type = 1, $old_goods_data = null)
        //    {
        //        $goods = [];
        //        $Sum_num = 0;
        //        $Sum_money = 0;

        //        $good_id = uniqid();
        //        //整理订单商品信息
        //        foreach ($orderMessage as $k => $v) {
        //            $num = (int)$v['QuantityOrdered'];
        //            $product_money = $v['ItemPrice']['Amount'] ?? 0;
        //            $price = $num > 0 ? $product_money / $num : $product_money;
        //            $unit = $v['ItemPrice']['CurrencyCode'] ?? 'CNY';
        //            $shipping_money = $v['ShippingPrice']['Amount'] ?? 0;
        //            $shipping_unit = $v['ShippingPrice']['CurrencyCode'] ?? 'CNY';
        //            $shipping_money_from = FromConversion($shipping_money, $shipping_unit);
        //            $product_money_from = FromConversion($product_money, $unit);
        //            $shipping_tax_amount = $v['ShippingTax']['Amount'] ?? 0;
        //            $shipping_tax_unit = $v['ShippingTax']['CurrencyCode'] ?? 'CNY';
        //            $shipping_tax = FromConversion($shipping_tax_amount, $shipping_tax_unit);
        //            $item_tax_amount = $v['ItemTax']['Amount'] ?? 0;
        //            $item_tax_unit = $v['ItemTax']['CurrencyCode'] ?? 'CNY';
        //            $item_tax = FromConversion($item_tax_amount, $item_tax_unit);

        //            $data = [
        //                'num'                   => $num,
        //                'url'                   => trim($v["Url"]),
        //                'asin'                  => $v["ASIN"],
        //                'unit'                  => $unit,
        //                'price'                 => FromConversion($price, $unit),
        //                'total'                 => $product_money_from + $shipping_money_from,//商品总价
        //                'name'                  => trim($v["Title"]),
        //                'from_url'              => $store['org'] . "/dp/" . $v['ASIN'],
        //                'send_num'              => $num,
        //                'good_id'               => $v["ASIN"] ?? $good_id . $k,
        //                'seller_sku'            => $v['SellerSKU'],
        //                'delivery'              => 0,
        //                'amazon_shipping_money' => $shipping_money_from,  //亚马逊运费
        //                'shipping_tax'          => $shipping_tax,
        //                'item_tax'              => $item_tax
        //            ];
        //            $Sum_num += $num;
        //            $Sum_money += $product_money_from;
        //            $Sum_money += $shipping_money_from;
        //            if ($data) {
        //                $goods[] = $data;
        //            }
        //        }
        //        if ($type === 1) {
        //            return ["goods" => $goods, "Sum_money" => $Sum_money, "Sum_num" => $Sum_num];
        //        } else {
        //            $new_goods_data = array_combine(array_column($goods, "asin"), $goods);
        //            foreach ($old_goods_data as $key => &$value) {
        //                if (array_key_exists($key, $new_goods_data)) {
        //                    $old_goods_data[$key]['unit'] = $new_goods_data[$key]['unit'];
        //                    $old_goods_data[$key]['price'] = $new_goods_data[$key]['price'];
        //                    $old_goods_data[$key]['total'] = $new_goods_data[$key]['total'];
        //                    $old_goods_data[$key]['amazon_shipping_money'] = $new_goods_data[$key]['amazon_shipping_money'];
        //                }
        //            }
        //            return $old_goods_data;
        //        }
        //    }

        #endregion

        #region SQL
        public IActionResult Invoke()
        {
            /************************************************  
             ================/order/upload
             select * from `amazon_order` where `d_order_no` = ? and `i_company_id` = ? limit *
             update `amazon_order` set `d_refund` = ?, `d_fee` = ?, `t_order_state` = ?, `d_order_no` = ?, `d_order_no_hash` = ?, `t_shipping_type` = ?, `d_user_name` = ?, `d_pay_time` = ?, `d_receiver_phone` = ?, `i_receiver_nation_id` = ?, `d_receiver_nation_short` = ?, `d_receiver_nation` = ?, `d_receiver_province` = ?, `d_receiver_city` = ?, `d_receiver_district` = ?, `d_receiver_address` = ?, `d_receiver_address*` = ?, `d_receiver_address*` = ?, `d_receiver_zip` = ?, `d_receiver_name` = ?, `d_receiver_email` = ?, `i_store_id` = ?, `d_seller_id` = ?, `d_order_money` = ?, `d_pay_money` = ?, `d_create_time` = ?, `d_coin` = ?, `d_delivery_time` = ?, `d_last_time` = ?, `d_delivery` = ?, `d_delivery_no` = ?, `d_rate` = ?, `d_latest_ship_time` = ?, `d_earliest_ship_time` = ?, `i_company_id` = ?, `i_user_id` = ?, `i_group_id` = ?, `i_oem_id` = ?, `d_default_ship_from_location_address` = ?, `d_goods` = ?, `d_profit` = ?, `amazon_order`.`updated_at` = ? where `id` = ?
             update `counter_store_business` set `c_order_total` = c_order_total+*, `c_order_total_fee` = c_order_total_fee+*.*, `c_order_total_refund` = c_order_total_refund+*, `counter_store_business`.`updated_at` = ? where `i_store_id` = ?
             update `counter_company_business` set `c_order_num` = c_order_num+*, `c_order_total` = c_order_total+*, `c_order_total_fee` = c_order_total_fee+*.*, `c_order_total_refund` = c_order_total_refund+*, `counter_company_business`.`updated_at` = ? where `i_company_id` = ?
             update `counter_real_finance` set `c_order_total` = c_order_total+*, `c_order_total_fee` = c_order_total_fee+*.*, `c_order_total_refund` = c_order_total_refund+*, `counter_real_finance`.`updated_at` = ? where `i_company_id` = ? and `i_oem_id` = ? and `d_month` = ?
             update `store` set `d_last_order_time` = ?, `d_last_ship_time` = ?, `d_last_auto_update_time` = ?, `store`.`updated_at` = ? where `id` = ?
             insert into `amazon_order_action` (`t_order_status`, `d_order_status_text`, `i_order_id`, `d_action`, `i_user_id`, `i_company_id`, `i_group_id`, `i_oem_id`, `d_user_name`, `d_truename`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
             select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
             select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
             select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
             update `store` set `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
             select `i_store_id` from `store_group` where `i_group_id` = ?
             select * from `store` where `t_flag` = ? and `t_is_del` = ? and `t_is_verify` = ? and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
             select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
             update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
             select `id`, `i_category_id`, `i_sub_category_id`, `i_distribution_category_id`, `i_distribution_sub_category_id`, `u_lp_n`, `u_lp_k`, `u_lp_s`, `u_lp_d`, `u_price`, `u_quantity`, `d_languages`, `b_language`, `d_source`, `d_ext`, `d_attribute`, `d_variants`, `d_images`, `b_type`, `updated_at`, `d_pid`, `d_coin`, `d_rate` from `product` where (`i_company_id` = ? and `id` = ? and `i_oem_id` = ?) limit *
             select * from `company` where `company`.`id` = ? limit *
             select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
             select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
             select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
             select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
             update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `waybill`.`updated_at` = ? where `id` = ?
             select `id`, `d_title`, `i_user_id`, `t_state` from `work_order` where `i_company_id` = ? and `t_state` in (?, ?) limit *
             select `id`, `d_button_name` from `waybill` where ((`i_company_id` = ? and `i_operate_company_id` = ?) or `i_waybill_company_id` = ?) and `d_button_name` in (?, ?) order by `updated_at` asc limit *
            ************************************************/
            return View();
        }
        #endregion
    }
}
