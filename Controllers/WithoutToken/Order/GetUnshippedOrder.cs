﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.Order
{
    public class GetUnshippedOrder : Controller
    {
        #region ERP3.0 PHP
        //    const NOTEXIST = 1;//店铺不存在
        //    const NOORDER = 2;//订单不存在
        //    const THIRD_PART = 1;
        //    const TYPE_AMAZON = 1;//亚马逊店铺类型

        //    const SleepSecond = 600;//exe沉睡时间 秒

        //    private $returnData = [
        //        'SleepSecond' => self::SleepSecond,
        //        'Store'       => null,
        //        'Order'       => null
        //];


        //public function getApiId()
        //    {
        //        return 450;
        //    }

        //    private function getAmazonKey()
        //    {
        //        return Cache::instance()->amazonKey()->get();
        //    }

        //    public function handle($request, $userid, $rule, $session)
        //    {
        //        //        $str = 'UserId:' . $session->getUserId()  . ' Time:' . $this->getDataTime() ;
        //        //        writelog($str);
        //        //        Log::channel('UnshippedOrder')->info($str);
        //        //        return $this->success($str);

        //        //权限
        //        if ($session->getConcierge() == UserModel::TYPE_OWN) {
        //        //公司管理查看公司下全部店铺
        //        $list = Cache::instance()->StoreCompany()->get($session->getCompanyId());
        //        } else
        //        {
        //        //员工权限查看
        //        $list = Cache::instance()->StorePersonal()->get($session->getCompanyId(), $session->getGroupId());
        //        }
        //        if (!checkArr($list))
        //        {
        //        //无分配用户，沉睡时间10小时
        //        $this->returnData['SleepSecond'] = 36000;
        //            return $this->success($this->returnData);
        //        }

        //    $storeIds = array_keys($list);
        //    $time = $this->getTime();
        //    $where = [
        //        ['company_id', '=', $session->getCompanyId()],
        //        ['flag', '=', storeModel::FLAG_NO],
        //        ['state', '=', storeModel::STATE_ENABLE],
        //        ['is_del', '=', storeModel::NOT_DEL],
        //        ['type', '=', storeModel::TYPE_AMAZON],
        //        ['is_verify', '=', storeModel::IS_VERIFY_TRUE],
        //        ['last_auto_update_time', '<', $time - self::SleepSecond],
        //    ];

        //    //统计10分钟内未拉取店铺数
        //    $num = storeModel::query()
        //        ->where($where)
        //        ->whereIn('id', $storeIds)
        //        ->count();
        //        if ($num == 0) {
        //            return $this->success($this->returnData);
        //        }


        //    $info = storeModel::query()
        //        ->where($where)
        //        ->whereIn('id', $storeIds)
        //        ->orderBy('last_auto_update_time')
        //        ->select(['id', 'data', 'nation_id', 'name', 'last_order_time', 'last_ship_time'])
        //        ->first();

        //    //获取待发货订单
        //    $where = [
        //        ['store_id', '=', $info['id']],
        //        ['order_state', '=', AmazonOrderModel::ORDER_STATE_PENDING],
        //        ['company_id', '=', $session->getCompanyId()],
        //        ['entry_mode', '=', AmazonOrderModel::API_ENTRY_MODE]
        //    ];
        //    $order = AmazonOrderModel::query()
        //        ->where($where)
        //        ->select(['order_no'])
        //        ->limit(100)
        //        ->get()
        //        ->toArray();
        //        if (!checkArr($order))
        //        {
        //        //没订单有店铺更新店铺时间
        //        $updateData = [
        //            'last_auto_update_time' => $time,
        //            'updated_at'            => date("Y-m-d H:i:s", $time)
        //        ];
        //            storeModel::query()->where('id', $info['id'])->update($updateData);
        //            return $this->success($this->returnData);
        //        }


        //    $storeData = $info['data'];
        //        if (isset($storeData["choose"]) && $storeData["choose"] == self::THIRD_PART) {//魔鬼数字
        //        $keyID = $this->getCountryKeyID($info['nation_id']);
        //        $amazonKey = $this->getAmazonKey();
        //            if (array_key_exists($keyID, $amazonKey))
        //            {
        //            $keyData = $amazonKey[$keyID];
        //            $storeData['aws_access_key'] = $keyData["aws_access_key"];
        //            $storeData['secret_key'] = $keyData["secret_key"];
        //            }

        //        }
        //    $store = [
        //        'ID'            => $info['id'],
        //        'Name'          => $info['name'],
        //        'Marketplace'   => $storeData['marketplace'],
        //        'SellerID'      => $storeData['seller_id'],
        //        'MwsAuthToken'  => $storeData['mws_auth_token'],
        //        'AwsAccessKey'  => $storeData['aws_access_key'],
        //        'SecretKey'     => $storeData['secret_key'],
        //        'LastOrderTime' => $info['last_order_time'],
        //        'LastShipTime'  => $info['last_ship_time']
        //    ];

        //    $updateData = [
        //        'last_auto_update_time' => $time,
        //        'updated_at'            => date("Y-m-d H:i:s", $time)
        //    ];
        //        storeModel::query()->where('id', $info['id'])->update($updateData);

        //    $time = self::SleepSecond - ($num - 1) *30;

        //    $this->returnData = [
        //        'SleepSecond' => $time > 0 ? $time: 0,
        //        'Store'       => $store,
        //        'Order'       => array_column($order, 'order_no')
        //    ];

        //    //日志
        //    $str = 'UserId:'. $session->getUserId(). ' StoreId:'. $info['id']. ' StoreNum:'. $num. ' Time:'. $this->getDataTime(). ' OrderNum:'.count($order). ' SleepSecond:'. $time;
        //        Log::channel('UnshippedOrder')->info($str);

        //        return $this->success($this->returnData);
        //    }

        //    public function getCountryKeyID($nation_id)
        //    {
        //    $country = Cache::instance()->amazonNation()->get();
        //    $country = arrayCombine($country, "id");
        //        if (isset($country[$nation_id]))
        //        {
        //            return $country[$nation_id]['key_id'];
        //        }
        //        else
        //        {
        //            return $this->success($this->returnData);
        //        }
        //    }
        #endregion

        #region SQL
        public IActionResult invoke()
        {
            /************************************************  
             ================/order/getUnshippedOrder
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
            select `d_order_no` from `amazon_order` where (`i_store_id` = ? and `t_order_state` = ? and `i_company_id` = ? and `t_entry_mode` = ?) limit *
            update `store` set `d_last_auto_update_time` = ?, `store`.`updated_at` = ? where `id` = ?
            select `id`, `d_button_name` from `waybill` where ((`i_company_id` = ? and `i_operate_company_id` = ?) or `i_waybill_company_id` = ?) and `d_button_name` in (?, ?) order by `updated_at` asc limit *
            select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
            update `store` set `d_last_auto_time` = ?, `store`.`updated_at` = ? where `id` = ?
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            update `store` set `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
            update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
            select `i_store_id` from `to_sell_task` where `i_company_id` = ? and `t_is_finish` = ? and `i_oem_id` = ?
            select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?)
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            update `waybill` set `waybill`.`updated_at` = ? where `id` in (?, ?, ?, ?, ?)
            select * from `store` where `i_company_id` = ? and `t_flag` = ? and `t_is_del` = ? and `t_is_verify` = ?
            select * from `waybill` where `waybill`.`id` = ? limit *
            update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `d_tracking` = ?, `waybill`.`updated_at` = ? where `id` = ?
            ************************************************/
            return View();
        }
        #endregion
    }
}
