﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.Order
{
    public class OriginalData : Controller
    {
        ////原ERP3.0注释
        #region ERP3.0 PHP
        //const OrderIsEmpty = 1; //订单不存在
        //const OrderProductsIsEmpty = 2; //订单下无产品
        //const NotAmazonOrder = 3; //只有亚马逊订单才可开发票
        //const UnsupportedMergeOrder = 4; //不支持合并订单

        //public function getApiId()
        //{
        //    return 480;
        //}

        //public function verifyParams()
        //{
        //    return ['OrderID'];
        //}

        ///**
        // * 订单发票数据整合
        // * @param $request
        // * @param $userId
        // * @param $rule
        // * @param \App\Http\SessionData $session
        // * @return \Illuminate\Http\JsonResponse
        // */
        //public function handle($request, $userId, $rule, $session)
        //{
        //$orderId = $request['OrderID'];

        //    if (empty($orderId))
        //    {
        //        return $this->fail(self::ERR_PARAMETER, 'OrderID');
        //    }

        //$fields = ["id", "user_id", "group_id", "company_id", "oem_id", "order_no", "receiver_phone", "receiver_nation_id", "receiver_nation_short", "receiver_nation", "receiver_province", "receiver_city", "receiver_district", "receiver_address", "receiver_address2", "receiver_address3", "receiver_zip", "receiver_name", "receiver_email", "default_ship_from_location_address", "goods", "rate", "entry_mode", "merge_id"];

        //$orderInfo = AmazonOrder::query()->select($fields)->where('id', $orderId)->first();

        //    if ($orderInfo->entry_mode == 1){
        //        return $this->fail(self::NotAmazonOrder);
        //    }

        //    if ($orderInfo->merge_id != -1 ){//合并订单
        //        return $this->fail(self::UnsupportedMergeOrder);
        //    }

        //    if (!$orderInfo) {
        //        return $this->fail(self::OrderIsEmpty);
        //    }

        //$originalGoodsData = json_decode($orderInfo->getRawOriginal('goods'), true);
        //    if (!$originalGoodsData) {
        //        return $this->fail(self::OrderProductsIsEmpty);
        //    }

        //$orderInfo = $orderInfo->toArray();

        //// 用拉取订单时候的汇率去还原拉取时候的原始数据
        //$rate = $orderInfo['rate'];

        //    foreach ($originalGoodsData as $key => &$value) {
        //    $value['price'] = ToConversion($value['price'], '',$rate);
        //    $value['total'] = ToConversion($value['total'], '',$rate);
        //    $value['amazon_shipping_money'] = ToConversion($value['amazon_shipping_money'], '',$rate);
        //    $value['shipping_tax'] = ToConversion($value['shipping_tax'] ?? 0, '',$rate);
        //    $value['item_tax'] = ToConversion($value['item_tax'] ?? 0, '',$rate);
        //    }

        //    unset($orderInfo['goods']);
        //    unset($orderInfo['entry_mode']);
        //$orderInfo['Products'] = $originalGoodsData;
        //$orderInfo['default_ship_from_location_address'] = json_decode($orderInfo['default_ship_from_location_address'], true);
        //    return $this->success($orderInfo);
        //}
        #endregion
    }
}
