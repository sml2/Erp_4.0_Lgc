﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.User;
[Route("api/[controller]")]
[ApiController]
public class Bill : ControllerBase
{
    ////原ERP3.0注释
    #region ERP3.0 PHP
    //const Unauthorized  = 1;//无权获取
    //const CompanyDoesNotExist = 2;//公司不存在
    //const CompanyAdminDoesNotExist = 3;//公司管理员

    //public function getApiId()
    //{
    //    return 1210;
    //}

    //public function verifyParams()
    //{
    //    return [
    //        'Cid',//公司id
    //    ];
    //}

    //public function handle($request, $userId, $rule, $session)
    //{
    //    $cid = $request['Cid'];
    //    $currentCid = $this->getSessionObj()->getCompanyId();
    //    if (empty($currentCid))
    //    {
    //        return $this->fail(self::Unauthorized);
    //    }
    //    $companyInfo = Company::query()->where([
    //        ['id',$cid],
    //        ['parent_company_id',$currentCid],
    //    ])->first();

    //    if (!$companyInfo){
    //        return $this->fail(self::CompanyDoesNotExist);
    //    }

    //    $Sheet = User::query()->where([
    //        ['company_id',$cid],
    //        ['concierge',User::TYPE_OWN],
    //    ])->select(['username', 'truename'])->first();

    //    if (!$Sheet){
    //        return $this->fail(self::CompanyAdminDoesNotExist);
    //    }

    //    $IntegralBalanceService = new IntegralBalanceService();
    //    $Data = $IntegralBalanceService->getOtherCompanyBalanceListPage($cid, null, [], null, null, false);

    //    return $this->success(compact(['Sheet', 'Data']));
    //}
    #endregion
}
