﻿using ERP.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.Store;
[Route("api/[controller]")]
[ApiController]
public class FinishAuto : BaseController
{
    #region ERP3.0 PHP

    //const NOTEXIST = 1;//店铺不存在

    //public function getApiId()
    //{
    //    return 410;
    //}

    //public function verifyParams()
    //{
    //    return ['ID'];
    //}

    //public function handle($request, $userid, $rule, $session)
    //{
    //    $storeId = $request['ID'];

    //    $where = [
    //        ['id', '=', $storeId],
    //        ['company_id', '=', $session->getCompanyId()],
    //        ['flag', '=', storeModel::FLAG_NO],
    //        ['state', '=', storeModel::STATE_ENABLE],
    //        ['type', '=', storeModel::TYPE_AMAZON],
    //        ['is_verify', '=', storeModel::IS_VERIFY_TRUE],
    //        ['is_del', '=', storeModel::NOT_DEL]
    //    ];
    //    $info = storeModel::query()->where($where)->select(['id', 'last_order_time', 'last_ship_time'])->first();

    //    if (!$info) {
    //        return $this->fail(self::NOTEXIST);
    //    }

    //    $date = date("Y-m-d H:i:s", $this->getTime());
    //    $updateData = [
    //        'last_auto_time' => $date,
    //        'updated_at'     => $date
    //    ];
    //    storeModel::query()->where('id', $info['id'])->update($updateData);
    //    return $this->success();
    //}
    #endregion

    [Route("invoke")]
    [HttpGet]
    public async Task<ResultStruct> invoke()
    {
        /************************************************  
         ================/store/finishAuto
select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
update `store` set `d_last_auto_time` = ?, `store`.`updated_at` = ? where `id` = ?
select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
update `user_online` set `user_online`.`updated_at` = ? where `id` = ?

        ************************************************/
        return await Task.FromResult(Success());
    }
}
