﻿using ERP.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.Store;
[Route("api/[controller]")]
[ApiController]
public class GetInfo : BaseController
{
    #region ERP3.0 PHP
    //const NOTEXIST = 1;//店铺不存在

    //public function getApiId()
    //{
    //    return 390;
    //}

    //public function verifyParams()
    //{
    //    return ['ID'];
    //}

    //public function handle($request, $userid, $rule, $session)
    //{
    //    $storeId = $request['ID'];

    //    $where = [
    //        ['id', '=', $storeId],
    //        ['company_id', '=', $session->getCompanyId()],
    //        ['flag', '=', storeModel::FLAG_NO],
    //        ['state', '=', storeModel::STATE_ENABLE],
    //        ['type', '=', storeModel::TYPE_AMAZON],
    //        ['is_verify', '=', storeModel::IS_VERIFY_TRUE],
    //        ['is_del', '=', storeModel::NOT_DEL]
    //    ];
    //    $info = storeModel::query()
    //        ->where($where)
    //        ->select(['id', 'last_order_time', 'last_ship_time'])
    //        ->first();

    //    if (!$info) {
    //        return $this->fail(self::NOTEXIST);
    //    }

    //    //转换exe字段
    //    $returnData = [
    //        'ID'               => $info['id'],
    //        'LastOrderTime'    => $info['last_order_time'],
    //        'LastShipTime'     => $info['last_ship_time']
    //    ];
    //    return $this->success($returnData);
    //}
    #endregion
    [Route("invoke")]
    [HttpPost]
    public async Task<ResultStruct> invoke()
    {
        /************************************************  
         ================/store/getInfo
select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
update `store` set `d_last_auto_time` = ?, `store`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
update `store` set `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select * from `product` where `id` = ? and b_type & * limit *
select `id`, `d_title`, `i_user_id`, `t_state` from `work_order` where `i_company_id` = ? and `t_state` in (?, ?) limit *
insert into `temp_images` (`d_oss_path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `i_product_id`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
select `i_store_id` from `to_sell_task` where `i_company_id` = ? and `t_is_finish` = ? and `i_oem_id` = ?
select count(*) as count, `i_store_id` from `sync_mws` where `t_state` in (?, ?) and `i_company_id` = ? and `i_oem_id` = ? group by `i_store_id`
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?, ?, ?)
insert into `resource` (`created_at`, `d_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
delete from `temp_images` where `id` in (?, ?, ?, ?)
update `product` set `u_lp_s` = ?, `u_lp_d` = ?, `b_type` = ?, `u_price` = ?, `u_quantity` = ?, `d_source` = ?, `d_ext` = ?, `d_attribute` = ?, `d_variants` = ?, `d_images` = ?, `u_image` = ?, `d_pid` = ?, `d_pid_hash_code` = ?, `c_size` = ?, `d_coin` = ?, `d_rate` = ?, `product`.`updated_at` = ? where `id` = ?
update `counter_company_business` set `c_product_num` = `c_product_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` = ?
update `counter_company_business` set `c_report_product_num` = `c_report_product_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` in (?, ?, ?, ?, ?)
update `counter_user_business` set `c_product_num` = `c_product_num` + *, `counter_user_business`.`updated_at` = ? where `i_user_id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select * from `amazon_order` where `id` = ? limit *
insert into `waybill` (`i_order_id`, `i_logistic_id`, `i_nation_id`, `d_logistic`, `d_waybill_order`, `d_express`, `d_tracking`, `d_deliver_date`, `d_remark`, `i_user_id`, `d_truename`, `i_bill_log_id`, `i_platform_id`, `d_order_no`, `d_img_url`, `d_product_info`, `d_track_info`, `d_logistic_params`, `d_logistic_ext`, `t_waybill_state`, `d_button_name`, `d_sender`, `d_carriage_base`, `d_carriage_other`, `d_carriage_num`, `d_carriage_unit`, `i_store_id`, `d_ship_method`, `t_is_verify`, `d_waybill_rate`, `t_waybill_rate_type`, `d_rate`, `d_label_url`, `i_company_id`, `i_operate_company_id`, `i_waybill_company_id`, `created_at`, `updated_at`, `i_oem_id`, `i_group_id`, `u_purchase_tracking_number`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
update `counter_user_business` set `c_waybill_num` = c_waybill_num+*, `c_distribution_waybill_num` = c_distribution_waybill_num+*, `counter_user_business`.`updated_at` = ? where `i_user_id` = ?
update `counter_group_business` set `c_waybill_num` = c_waybill_num+*, `c_distribution_waybill_num` = c_distribution_waybill_num+*, `counter_group_business`.`updated_at` = ? where `i_group_id` = ?
update `counter_company_business` set `c_waybill_num` = c_waybill_num+*, `c_distribution_waybill_num` = c_distribution_waybill_num+*, `counter_company_business`.`updated_at` = ? where `i_company_id` = ?
update `counter_company_business` set `c_report_waybill_num` = `c_report_waybill_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` in (?, ?)
insert into `amazon_order_action` (`i_order_id`, `d_action`, `i_user_id`, `i_group_id`, `i_company_id`, `i_oem_id`, `d_user_name`, `d_truename`, `t_order_status`, `d_order_status_text`, `created_at`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
update `logistics_shipper` set `d_last_time` = ?, `logistics_shipper`.`updated_at` = ? where `id` = ?
update `logistics_param` set `d_last_time` = ?, `logistics_param`.`updated_at` = ? where `id` = ?
update `amazon_order` set `d_process_waybill` = ?, `u_express` = ?, `d_delivery_num` = d_delivery_num+*, `b_progress` = ?, `d_goods` = ?, `amazon_order`.`updated_at` = ? where (`id` = ?)
insert into `waybill_log` (`i_waybill_id`, `d_action`, `i_order_id`, `i_platform_id`, `d_user_name`, `d_truename`, `i_user_id`, `i_company_id`, `i_group_id`, `i_oem_id`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
update `amazon_order` set `b_progress` = (b_progress & ~*) ^ (* << *) , `amazon_order`.`updated_at` = ? where `id` = ?
select * from `store` where `i_company_id` = ? and `t_flag` = ? and `t_is_del` = ? and `t_is_verify` = ?
select * from `amazon_order` where `amazon_order`.`id` = ? limit *
select * from `waybill` where (`i_order_id` = ?)
update `amazon_order` set `d_refund` = ?, `d_fee` = ?, `t_order_state` = ?, `d_order_no` = ?, `d_order_no_hash` = ?, `t_shipping_type` = ?, `d_user_name` = ?, `d_pay_time` = ?, `d_receiver_phone` = ?, `i_receiver_nation_id` = ?, `d_receiver_nation_short` = ?, `d_receiver_nation` = ?, `d_receiver_province` = ?, `d_receiver_city` = ?, `d_receiver_district` = ?, `d_receiver_address` = ?, `d_receiver_address*` = ?, `d_receiver_address*` = ?, `d_receiver_zip` = ?, `d_receiver_name` = ?, `d_receiver_email` = ?, `i_store_id` = ?, `d_seller_id` = ?, `d_order_money` = ?, `d_pay_money` = ?, `d_create_time` = ?, `d_coin` = ?, `d_delivery_time` = ?, `d_last_time` = ?, `d_delivery` = ?, `d_delivery_no` = ?, `d_rate` = ?, `d_latest_ship_time` = ?, `d_earliest_ship_time` = ?, `i_company_id` = ?, `i_user_id` = ?, `i_group_id` = ?, `i_oem_id` = ?, `d_default_ship_from_location_address` = ?, `d_goods` = ?, `d_profit` = ?, `amazon_order`.`updated_at` = ? where `id` = ?
update `counter_store_business` set `c_order_total` = c_order_total+*.*, `c_order_total_fee` = c_order_total_fee+*.*, `c_order_total_refund` = c_order_total_refund+*, `counter_store_business`.`updated_at` = ? where `i_store_id` = ?
update `counter_real_finance` set `c_order_total` = c_order_total+*.*, `c_order_total_fee` = c_order_total_fee+*.*, `c_order_total_refund` = c_order_total_refund+*, `counter_real_finance`.`updated_at` = ? where `i_company_id` = ? and `i_oem_id` = ? and `d_month` = ?
update `store` set `d_last_order_time` = ?, `d_last_ship_time` = ?, `store`.`updated_at` = ? where `id` = ?
select column_name as `column_name` from information_schema.columns where table_schema = ? and table_name = ?
insert into `amazon_order` (`d_refund`, `d_fee`, `t_order_state`, `d_order_no`, `d_order_no_hash`, `t_shipping_type`, `d_user_name`, `d_pay_time`, `d_receiver_phone`, `i_receiver_nation_id`, `d_receiver_nation_short`, `d_receiver_nation`, `d_receiver_province`, `d_receiver_city`, `d_receiver_district`, `d_receiver_address`, `d_receiver_address*`, `d_receiver_address*`, `d_receiver_zip`, `d_receiver_name`, `d_receiver_email`, `i_store_id`, `d_seller_id`, `d_order_money`, `d_pay_money`, `d_create_time`, `d_coin`, `d_delivery_time`, `d_last_time`, `d_delivery`, `d_delivery_no`, `d_rate`, `d_latest_ship_time`, `d_earliest_ship_time`, `i_company_id`, `i_user_id`, `i_group_id`, `i_oem_id`, `d_default_ship_from_location_address`, `u_url`, `d_goods`, `d_product_num`, `d_product_money`, `b_progress`, `d_delivery_num`, `d_profit`, `u_truename`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
update `counter_group_business` set `c_order_num` = c_order_num+*, `c_order_total` = c_order_total+*.*, `c_order_total_fee` = c_order_total_fee+*, `c_order_total_refund` = c_order_total_refund+*, `counter_group_business`.`updated_at` = ? where `i_group_id` = ?
update `counter_store_business` set `c_order_total` = c_order_total+*.*, `c_order_total_fee` = c_order_total_fee+*, `c_order_total_refund` = c_order_total_refund+*, `counter_store_business`.`updated_at` = ? where `i_store_id` = ?
update `counter_company_business` set `c_report_order_num` = `c_report_order_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` in (?, ?)
update `counter_real_finance` set `c_order_total` = c_order_total+*.*, `c_order_total_fee` = c_order_total_fee+*, `c_order_total_refund` = c_order_total_refund+*, `counter_real_finance`.`updated_at` = ? where `i_company_id` = ? and `i_oem_id` = ? and `d_month` = ?
select `id`, `d_button_name` from `waybill` where ((`i_company_id` = ? and `i_operate_company_id` = ?) or `i_waybill_company_id` = ?) and `d_button_name` in (?, ?) order by `updated_at` asc limit *
update `waybill` set `waybill`.`updated_at` = ? where `id` in (?, ?, ?, ?, ?)
select * from `waybill` where `waybill`.`id` = ? limit *
update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `waybill`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select * from `task` where (`i_oem_id` = ? and `i_user_id` = ? and `i_group_id` = ? and `d_hash_code` = ?) limit *
select count(*) as aggregate from `product` where (`d_hash_code` = ? and `i_company_id` = ?) and !(b_type&*) AND !(b_type&*) AND !(b_type&*)
insert into `product` (`i_oem_id`, `i_company_id`, `i_user_id`, `i_group_id`, `i_task_id`, `u_lp_n`, `u_lp_k`, `u_lp_s`, `u_lp_d`, `d_source`, `d_hash_code`, `d_languages`, `b_language`, `b_type`, `u_price`, `i_platform_id`, `u_platform`, `d_attribute`, `d_variants`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select * from `purchase` where `purchase`.`id` = ? limit *
update `purchase` set `d_num` = ?, `d_product_total` = ?, `t_purchase_state` = ?, `d_purchase_unit` = ?, `d_purchase_total` = ?, `d_purchase_other` = ?, `d_purchase_price` = ?, `d_purchase_url` = ?, `d_purchase_order_no` = ?, `d_purchase_tracking_number` = ?, `d_remark` = ?, `d_order_item_id` = ?, `d_purchase_rate` = ?, `d_purchase_rate_type` = ?, `t_is_stock` = ?, `i_warehouse_id` = ?, `u_warehouse_name` = ?, `t_purchase_affirm` = ?, `purchase`.`updated_at` = ? where `id` = ?
insert into `purchase_log` (`i_purchase_id`, `u_action`, `i_user_id`, `u_username`, `u_truename`, `i_oem_id`, `i_group_id`, `i_company_id`, `i_order_id`, `i_platform_id`, `created_at`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
insert into `reporter_request` (`created_at`, `d_action`, `d_cpu_time`, `d_method`, `d_module`, `d_name`, `d_namespace`, `d_net_time`, `d_request_id`, `d_request_time`, `d_uri`, `i_company_id`, `i_group_id`, `i_oem_id`, `i_user_id`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select count(*) as aggregate from `purchase` where `i_company_id` = ? and `i_operate_company_id` = ? and `d_purchase_order_no` like ?
select * from `purchase` where `i_company_id` = ? and `i_operate_company_id` = ? and `d_purchase_order_no` like ? order by `created_at` desc limit * offset *
select `i_order_id`, `d_remark`, `d_truename`, `created_at` from `amazon_order_remark` where `i_company_id` = ? and `i_order_id` in (?) order by `created_at` desc
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select * from `sync_mws` where `i_store_id` in (?, ?, ?, ?, ?) order by `updated_at` desc limit * offset *
select `id`, `t_uploading`, `i_pid` from `sync_product` where `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)

        ************************************************/
        return await Task.FromResult( Success());
    }
}
