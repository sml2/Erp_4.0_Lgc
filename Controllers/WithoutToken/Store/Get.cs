﻿using ERP.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.Store;
[Route("api/[controller]")]
[ApiController]
public class Get : BaseController
{
    #region ERP3.0 PHP
    //    const NOTEXIST = 1;
    //    const LostStoreKeyId = 2;
    //    const THIRD_PART = 1;
    //    const TYPE_AMAZON = 1;//亚马逊店铺类型
    //    private $amazonKey;

    //    public function __construct()
    //    {
    //        $this->amazonKey = Cache::instance()->amazonKey()->get();
    //    }

    //    public function getApiId()
    //    {
    //        return 310;
    //    }

    //    public function handle($requestData, $userid, $rule, $session)
    //    {
    //        // $ret[] = [
    //        //     'ID'            => 119,
    //        //     'Name'          => '美国店铺',
    //        //     'Marketplace'   => 'ATVPDKIKX0DER',
    //        //     'SellerID'      => 'A353BO8BIHY0L7',
    //        //     'MwsAuthToken'  => 'amzn.mws.3eb55d02-fca7-42b0-cc80-49c41b792bc6',
    //        //     'AwsAccessKey'  => 'AKIAJN35LXC6JTSCJIVQ',
    //        //     'SecretKey'     => '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',
    //        //     'LastOrderTime' => time()
    //        // ];
    //        // return $this->success($ret);
    //        //权限
    //        if ($session->getConcierge() == UserModel::TYPE_OWN) {
    //            //公司管理查看公司下全部店铺
    //            $list = Cache::instance()->StoreCompany()->get($session->getCompanyId());
    //        } else
    //        {
    //            //员工权限查看
    //            $list = Cache::instance()->StorePersonal()->get($session->getCompanyId(), $session->getGroupId());
    //        }
    //        if (!checkArr($list))
    //        {
    //            return $this->fail(self::NOTEXIST);
    //        }
    //        $ret = [];
    //        $noKeyCount = 0;
    //        foreach ($list as $store) {
    //            //过滤禁用无效店铺
    //            if ($store['state'] == storeModel::STATE_DISABLE) {
    //                continue;
    //            }
    //            $storeData = $store['data'];
    //            //            $keyID = $this->getCountryKeyID($store['nation_id']);
    //            if (isset($storeData["choose"]) && $storeData["choose"] == self::THIRD_PART) {//魔鬼数字
    ////                $keyID = $store["key_id"];
    //                $keyID = $this->getCountryKeyID($store['nation_id']);
    //                if (array_key_exists($keyID, $this->amazonKey))
    //                {
    //                    $keyData = $this->amazonKey[$keyID];
    //                    $storeData['aws_access_key'] = $keyData["aws_access_key"];
    //                    $storeData['secret_key'] = $keyData["secret_key"];
    //                }
    //                else
    //                {
    //                    $noKeyCount += 1;
    //                    continue;
    //                }

    //            }
    //            if ($store['type'] == self::TYPE_AMAZON) {
    //                $ret[] = [
    //                    'ID'            => $store['id'],
    //                    'Name'          => $store['name'],
    //                    'State'         => $store['state'],
    //                    'Marketplace'   => $storeData['marketplace'],
    //                    'SellerID'      => $storeData['seller_id'],
    //                    'MwsAuthToken'  => $storeData['mws_auth_token'],
    //                    'AwsAccessKey'  => $storeData['aws_access_key'],
    //                    'SecretKey'     => $storeData['secret_key'],
    //                    'LastOrderTime' => $store['last_order_time'],
    //                    'LastShipTime'  => $store['last_ship_time']
    //                ];
    //            }

    //        }
    //        $info = $noKeyCount ? "{$noKeyCount}个店铺丢失通讯权限" : null;
    //        return checkArr($ret) ? $this->success($ret, $info) : $this->fail(self::NOTEXIST);
    //    }

    //    public function getCountryKeyID($nation_id)
    //    {
    //        $country = Cache::instance()->amazonNation()->get();
    //        $country = arrayCombine($country, "id");
    //        if (isset($country[$nation_id]))
    //        {
    //            return $country[$nation_id]['key_id'];
    //        }
    //        else
    //        {
    //            return $this->fail(self::LostStoreKeyId, null, "Can Not Find Store Key Id");
    //        }
    //    }
    #endregion
    [Route("invoke")]
    [HttpPost]
    public async Task<ResultStruct> invoke()
    {
        /************************************************  
         
        ************************************************/
        return await Task.FromResult(Success());
    }
}
