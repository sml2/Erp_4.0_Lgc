﻿using ERP.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.Store;
[Route("api/[controller]")]
[ApiController]
public class GetAutoStore : BaseController
{
    #region ERP3.0 PHP
    //const NOTEXIST = 1;//店铺不存在
    //const THIRD_PART = 1;
    //const TYPE_AMAZON = 1;//亚马逊店铺类型

    //const Sleep = 5;
    //const SleepSecond = 300;

    //private storeModel $storeModel;
    //private $amazonKey;

    //public function __construct()
    //{
    //    $this->storeModel = new storeModel();
    //    $this->amazonKey = Cache::instance()->amazonKey()->get();
    //}

    //public function getApiId()
    //{
    //    return 400;
    //}

    //public function handle($request, $userid, $rule, $session)
    //{
    //    //权限
    //    if ($session->getConcierge() == UserModel::TYPE_OWN) {
    //        //公司管理查看公司下全部店铺
    //        $list = Cache::instance()->StoreCompany()->get($session->getCompanyId());
    //    } else
    //    {
    //        //员工权限查看
    //        $list = Cache::instance()->StorePersonal()->get($session->getCompanyId(), $session->getGroupId());
    //    }
    //    if (!checkArr($list))
    //    {
    //        return $this->fail(self::NOTEXIST, ['sleep' => self::Sleep, 'SleepSecond' => self::SleepSecond]);
    //    }

    //    $storeIds = array_keys($list);

    //    $time = $this->getTime();
    //    $where = [
    //        ['company_id', '=', $session->getCompanyId()],
    //        ['flag', '=', storeModel::FLAG_NO],
    //        ['state', '=', storeModel::STATE_ENABLE],
    //        ['is_del', '=', storeModel::NOT_DEL],
    //        ['type', '=', storeModel::TYPE_AMAZON],
    //        ['is_verify', '=', storeModel::IS_VERIFY_TRUE],
    //        ['last_allot_time', '<', $time - self::SleepSecond],
    //    ];

    //    //统计5分钟内未拉取店铺数
    //    $num = storeModel::query()
    //        ->where($where)
    //        ->whereIn('id', $storeIds)
    //        ->count();
    //    if ($num == 0) {
    //        return $this->fail(self::NOTEXIST, ['sleep' => self::Sleep, 'SleepSecond' => self::SleepSecond]);
    //    }


    //    $info = storeModel::query()
    //        ->where($where)
    //        ->whereIn('id', $storeIds)
    //        ->orderBy('last_auto_time')
    //        ->select(['id', 'data', 'nation_id', 'name', 'last_order_time', 'last_ship_time', 'last_auto_ship_time'])
    //        ->first();

    //    $storeData = $info['data'];
    //    if (isset($storeData["choose"]) && $storeData["choose"] == self::THIRD_PART) {//魔鬼数字
    //        $keyID = $this->getCountryKeyID($info['nation_id']);
    //        if (array_key_exists($keyID, $this->amazonKey))
    //        {
    //            $keyData = $this->amazonKey[$keyID];
    //            $storeData['aws_access_key'] = $keyData["aws_access_key"];
    //            $storeData['secret_key'] = $keyData["secret_key"];
    //        }

    //    }
    //    $store = [
    //        'ID'            => $info['id'],
    //        'Name'          => $info['name'],
    //        'Marketplace'   => $storeData['marketplace'],
    //        'SellerID'      => $storeData['seller_id'],
    //        'MwsAuthToken'  => $storeData['mws_auth_token'],
    //        'AwsAccessKey'  => $storeData['aws_access_key'],
    //        'SecretKey'     => $storeData['secret_key'],
    //        'LastOrderTime' => $info['last_order_time'],
    //        'LastShipTime'  => $info['last_ship_time']
    //    ];

    //    $updateData = [
    //        'last_allot_time' => $time,
    //        'updated_at'      => date("Y-m-d H:i:s", $time)
    //    ];
    //    storeModel::query()->where('id', $info['id'])->update($updateData);

    //    $time = self::SleepSecond - $num * 30;
    //    return $this->success(['store' => $store, 'sleep' => self::Sleep, 'SleepSecond' => $time > 0 ? $time: 0]);
    //}

    //public function getCountryKeyID($nation_id)
    //{
    //    $country = Cache::instance()->amazonNation()->get();
    //    $country = arrayCombine($country, "id");
    //    if (isset($country[$nation_id]))
    //    {
    //        return $country[$nation_id]['key_id'];
    //    }
    //    else
    //    {
    //        return $this->fail(self::LostStoreKeyId, ['sleep' => self::Sleep, 'SleepSecond' => self::SleepSecond], "Can Not Find Store Key Id");
    //    }
    //}
    #endregion
    [Route("invoke")]
    [HttpPost]
    public async Task<ResultStruct> invoke()
    {
        /************************************************  
         ================/store/getAutoStore
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
update `store` set `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *

        ************************************************/
        return await Task.FromResult( Success());
    }
}
