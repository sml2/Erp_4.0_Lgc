﻿using ERP.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.Store;
[Route("api/[controller]")]
[ApiController]
public class UpdateVerify : BaseController
{
    #region ERP3.0 PHP
    //const NOTEXIST = 1;//店铺不存在
    //const AUTHENTICATED = 2;//店铺为已验证通过店铺
    //const Database = 2; //更新失败

    //public function getApiId()
    //{
    //    return 460;
    //}

    //public function verifyParams()
    //{
    //    return ['ID'];
    //}

    //public function handle($requestData, $userid, $rule, $session)
    //{
    //    $storeId = $requestData['ID'];
    //    $where = [
    //        ['id', '=', $storeId],
    //        ['company_id', '=', $session->getCompanyId()],
    //        ['flag', '=', storeModel::FLAG_NO],
    //        ['state', '=', storeModel::STATE_ENABLE],
    //        ['type', '=', storeModel::TYPE_AMAZON],
    //        ['is_del', '=', storeModel::NOT_DEL]
    //    ];
    //    $info = storeModel::query()
    //        ->where($where)
    //        ->select(['id', 'is_verify'])
    //        ->first();

    //    if (!$info) {
    //        return $this->fail(self::NOTEXIST);
    //    }

    //    if ($info['is_verify'] == storeModel::IS_VERIFY_TRUE) {
    //        return $this->fail(self::AUTHENTICATED);
    //    }

    //    $updateData = [
    //        'is_verify'  => storeModel::IS_VERIFY_TRUE,
    //        'updated_at' => $this->getDataTime()
    //    ];
    //    $res = storeModel::query()->InId($storeId)->update($updateData);
    //    if (!$res) {
    //        return $this->fail(self::Database);
    //    }
    //    //清除缓存
    //    Cache::instance()->StoreCompany()->del(false, $session->getCompanyId());
    //    Cache::instance()->StorePersonal()->del(true, $session->getCompanyId());
    //    return $this->success();
    //}
    #endregion
    [Route("invoke")]
    [HttpPost]
    public async Task<ResultStruct> invoke()
    {
        /************************************************  
         
        ************************************************/
        return await Task.FromResult( Success());
    }
}
