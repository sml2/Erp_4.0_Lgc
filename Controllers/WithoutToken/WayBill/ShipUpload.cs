﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.WayBill
{
    public class ShipUpload : Controller
    {
        ////原ERP3.0注释
        #region ERP3.0 PHP
        //        private AmazonOrderService $AmazonOrderService;
        //    private ShipOrderService $ShipOrderService;
        //    private CounterGroupService $CounterGroupService;
        //    private CounterUserService $CounterUserService;
        //    private CounterCompanyService $CounterCompanyService;
        //    private AmazonOrderActionService $AmazonOrderActionService;
        //    private WayBillService $WayBillService;
        //    private PurchaseModel $PurchaseModel;
        //    private PurchaseService $PurchaseService;
        //    private LogisticsShipperService $LogisticsShipperService;
        //    private LogisticsParamService $LogisticsParamService;

        //    public function __construct()
        //        {
        //        $this->AmazonOrderModel = new AmazonOrderModel();
        //        $this->AmazonOrderService = new AmazonOrderService();
        //        $this->ShipOrderService = new ShipOrderService();
        //        $this->CounterGroupService = new CounterGroupService();
        //        $this->CounterUserService = new CounterUserService();
        //        $this->CounterCompanyService = new CounterCompanyService();
        //        $this->AmazonOrderActionService = new AmazonOrderActionService();
        //        $this->WayBillService = new WayBillService();
        //        $this->PurchaseModel = new PurchaseModel();
        //        $this->PurchaseService = new PurchaseService();
        //        $this->LogisticsShipperService = new LogisticsShipperService();
        //        $this->LogisticsParamService = new LogisticsParamService();
        //        }

        //        public function getApiId()
        //        {
        //            return 830;
        //        }

        //        const NotFoundLogistics = 1; //物流信息有误
        //        const ShipOrderIsEmpty = 2; //待发货订单不存在
        //        const Database = 3; //添加运单失败（上报失败,可能缺少必须参数[T]）
        //        const LowStocks = 5; // 库存不足

        //        const AMAZON_PLATFORM_ID = 1; //订单平台
        //        const WAYBILL_SUBMITTED = 100;  //运单状态--已提交

        //        public function verifyParams()
        //        {
        //            return [
        //                'ShipID',
        //                'Num',
        //                'OrderNum',
        //                'LType',
        //                'ShipMethod',
        //                'Date',
        //                'Unit',
        //                'Price',
        //                'TrackNum',
        //                'TrackInfo',
        //                'TrackButtonName',
        //                'State',
        //                'Note',
        //                'SenderInfo',
        //                'Parameter',

        //            ];
        //        }
        //        private function getRate($unit)
        //        {
        //            return $unit == 'CNY' ? 10000 : Cache::instance()->unitSingle()->get($unit)['rate'];
        //        }

        //        private function getCompany($company_id = '')
        //        {
        //            return Cache::instance()->Company()->get($company_id);
        //        }

        //        public function handle($request, $userId, $rule, $session)
        //        {
        //        $shipID = $request['ShipID'];
        //        //待发货订单
        //        $ShipOrder = $this->ShipOrderService->getInfo($shipID);
        //            if (!$ShipOrder) {
        //                return $this->fail(self::ShipOrderIsEmpty);
        //            }

        //            // 物流
        //            if ($request['LType'] == 'none') {
        //            $logistic_id = 0;
        //            $logistic_name = $request['Note'];
        //            } else
        //            {
        //            $logistics = arrayCombine(Cache::instance()->logistics()->get(), 'sign');
        //            $logisticsInfo = $logistics[strtolower($request['LType'])];
        //                if (!$logisticsInfo) {
        //                    return $this->fail(self::NotFoundLogistics);
        //                }
        //            $logistic_id = $logisticsInfo['id'];
        //            $logistic_name = $logisticsInfo['name'];
        //            }

        //        $order = $this->AmazonOrderService->getOrderInfo($ShipOrder['order_id']);
        //            if (!$order) {
        //                return $this->fail(self::ShipOrderIsEmpty);
        //            }

        //        //订单所属公司
        //        $company = $this->getCompany($order['company_id']);

        //        $deliver_num = 0;
        //        $product = json_decode($order->getRawOriginal('goods'), true);

        //            foreach ($product as &$value) {
        //            $value = array_change_key_case($value, CASE_LOWER);
        //            $deliver_num += $value['num'];
        //            $value['send_num'] = 0;
        //            $value['delivery'] ??= 0;
        //            $value['delivery'] += $value['num'];
        //            $value['stock_out_num'] = Stock::checkGoodsOut($value) ? $value['num'] : 0;
        //            }
        //        $updateOrder['goods'] = json_encode($product);

        //        $_product = arrayCombine($product, 'good_id');
        //        $goods = json_decode($request['goods'], true);
        //        $img_url = '';
        //            foreach ($goods as &$good) {
        //            $good['send_num'] = 0;
        //            $img_url == '' && $img_url = $value['url'];
        //                if (isset($_product[$good['good_id']]))
        //                {
        //                $good['delivery'] = $_product[$good['good_id']]['num'];
        //                $good['sendnum'] = $_product[$good['good_id']]['num'];
        //                $good['warehouse_id'] = $_product[$good['good_id']]['warehouse_id'] ?? 0;
        //                $good['warehouse_name'] = $_product[$good['good_id']]['warehouse_name'] ?? '';
        //                $good['product_id'] = $_product[$good['good_id']]['product_id'] ?? 0;
        //                $good['variant_id'] = $_product[$good['good_id']]['variant_id'] ?? '';
        //                $good['tags'] = $_product[$good['good_id']]['tags'] ?? [];
        //                $good['stock_out_num'] = 0;
        //                }
        //            }

        //        $carriage_base = $request['Price'] ? FromConversion($request['Price'], $request['Unit']) : 0;
        //        $waybillInsertData = [
        //            'order_id'           => $order['id'],
        //            'logistic_id'        => $logistic_id,
        //            'nation_id'          => $order['receiver_nation_id'],
        //            'logistic'           => $logistic_name,
        //            'waybill_order'      => $request['OrderNum'],
        //            'express'            => $request['Num'],
        //            'tracking'           => $request['TrackNum'] ?? '',
        //            'deliver_date'       => date("Y-m-d H:i:s", $request['Date']),
        //            'remark'             => $request['Note'],
        //            'user_id'            => $session->getUserId(),
        //            'truename'           => $session->getTruename(),
        //            'bill_log_id'        => 0,
        //            'platform_id'        => self::AMAZON_PLATFORM_ID,
        //            'order_no'           => $order['order_no'],
        //            'img_url'            => $img_url,
        //            'product_info'       => json_encode($goods),
        //            'track_info'         => $request['TrackInfo'],
        //            'logistic_params'    => $request['Parameter'],
        //            'logistic_ext'       => $request['ParameterExt'] ?? json_encode([]),
        //            'waybill_state'      => self::WAYBILL_SUBMITTED,
        //            'button_name'        => $request['TrackButtonName'],
        //            'sender'             => $request['SenderInfo'],
        //            'carriage_base'      => $carriage_base,
        //            'carriage_other'     => 0,
        //            'carriage_num'       => $carriage_base,
        //            'carriage_unit'      => $request['Unit'],
        //            'store_id'           => $order['store_id'],
        //            'ship_method'        => $request['ShipMethod'],
        //            'is_verify'          => WayBillModel::ISVERIFY_NO,
        //            'waybill_rate'       => 0,
        //            'waybill_rate_type'  => $company['waybill_rate_type'],
        //            'rate'               => $this->getRate($request['Unit']),
        //            'company_id'         => $order['company_id'],
        //            'operate_company_id' => $session->getCompanyId(),
        //            'waybill_company_id' => $order['waybill_company_id'],
        //            'label_url'          => $request['LabelUrl'] ?? '',
        //            'created_at'         => date("Y-m-d H:i:s", $this->getTime()),
        //            'updated_at'         => date("Y-m-d H:i:s", $this->getTime()),
        //        ];

        //        /*************************************分销运单**********************************************/
        //        $oem_id = $session->getOemId();
        //        $group_id = $session->getGroupId();
        //        $distribution_waybill_num = 0;
        //        $process_waybill = AmazonOrderModel::PROCESSWAYBILL_INSIDE;
        //            if ($company['is_distribution'] == companyModel::DISTRIBUTION_TRUE) {
        //            $isDistribution = false;
        //                if ($company['id'] != $session->getCompanyId()){
        //                //上级发货
        //                $isDistribution = true;
        //                $group_id = 0;
        //                }else
        //                {
        //                //下级发货，使用上级分配物流
        //                $logisticParams = json_decode($request['Parameter'], true);
        //                $logisticParamId = $logisticParams['ID'];
        //                //判断物流参数是否上级分配  对公钱包
        //                $logisticParam = $this->LogisticsParamService->getLogisticsInfo($logisticParamId);
        //                    if ($logisticParam && $logisticParam['state'] == 1 && $logisticParam['is_sys_state'] == 1 && $logisticParam['source'] == 2 && $logisticParam['charge'] == 1){
        //                    $isDistribution = true;

        //                    $data['operate_company_id'] = $session->getReportId();
        //                    }
        //                }

        //                if ($isDistribution) {
        //                //上级发货
        //                $oem_id = $company['oem_id'];
        //                $distribution_waybill_num = 1;
        //                $process_waybill = AmazonOrderModel::PROCESSWAYBILL_SUPERIOR;

        //                $waybill_rate = $company['waybill_rate_type'] == 1 ? bcdiv(bcmul($carriage_base, $company['waybill_rate'], 6), 100, 6) : FromConversion($company['waybill_rate_num'], $request['Unit']);
        //                $carriage_num = bcadd($carriage_base, $waybill_rate);
        //                //分销手续费
        //                $waybillInsertData['waybill_rate'] = $waybill_rate;
        //                $waybillInsertData['carriage_num'] = $carriage_num;
        //            }
        //        }
        //        $order['process_waybill'] == AmazonOrderModel::PROCESSWAYBILL_NONE && $updateOrder['process_waybill'] = $process_waybill;
        //        $waybillInsertData['oem_id'] = $oem_id;
        //        $waybillInsertData['group_id'] = $group_id;
        //        $waybillInsertData['purchase_tracking_number'] = checkArr($order['purchase_tracking_number']) ? join(',', $order['purchase_tracking_number']) : '';
        //        /*************************************分销运单**********************************************/

        //        //启动事务
        //        Db::begintransaction();
        //        try {
        //            //删除批量待发货订单
        //            $this->ShipOrderService->delShipOrder($shipID);

        //            //添加运单信息
        //            $waybill_id = WayBillModel::query()->insertGetId($waybillInsertData);
        //            $express = $order['express'];
        //            if(checkArr($express)){
        //                $express[] = $waybill_id;
        //                $updateOrder['express'] = implode(',', $express);
        //            }else{
        //                $updateOrder['express'] = $waybill_id;
        //            }


        //            //修改计数
        //            $this->CounterUserService->waybillNumAdd(1, $distribution_waybill_num);
        //            $session->getGroupId() != 0 && $this->CounterGroupService->waybillNumAdd(1, $distribution_waybill_num);
        //            $this->CounterCompanyService->WaybillNumAdd(1, $distribution_waybill_num, $company['id']);
        //            $this->CounterCompanyService->reportWaybillNumAdd(1, json_decode($company['report_pids'], true));
        //            //商店统计表中没有计算运单个数的字段 $this->CounterStoreService
        //            //增加订单日志
        //            $this->AmazonOrderActionService->addLog($order["id"], '订单添加运单', $order["order_state"]);
        //            //修改订单进度
        //            if ($order["merge_idmerge_id"] > 0) {
        //                $order_ids = $this->getMergeOrderIds($order);
        //                $whereUpdate[] = ['id', 'in', $order_ids];
        //            } else {
        //                $whereUpdate[] = ['id', '=', $order["id"]];
        //            }
        //            $updateOrder['delivery_num'] = Db::raw(
        //                $this->AmazonOrderModel->transformKey('delivery_num') . '+' . $deliver_num
        //            );
        //            //更新sender和logisticparam时间
        //            $this->LogisticsShipperService->updateLastTime(json_decode($request['SenderInfo'], true)['SID']);
        //            $this->LogisticsParamService->updateLastTime(json_decode($request['Parameter'], true)['ID']);

        //            //修改订单为发货中
        //            //查询物流是否在发货中
        //            $progressBiter = new Progress($order['progress_original']);
        //            $hassend = $progressBiter->hasDeliver();
        //            //董哥要求 2021.6.7 已发货后再次发货要求重新变更处理中  去除判断条件
        ////            if (!$hassend) {
        //                $progressBiter->setDeliver(true);
        //                $progressBiter->setProgress(AmazonOrderModel::PROGRESS_PROCESSING);
        //                $progress = $progressBiter->getFlags();
        //                $updateOrder['progress'] = $progress;
        ////            }
        //            //向订单/采购表中添加物流单号
        ////            AmazonOrder::updateWaybillOrder($order['id'], $waybill_id);
        //            //移除订单批量发货状态
        //            $updateOrder['is_batch_ship'] = AmazonOrderModel::BATCH_SHIP_NO;
        //            $this->AmazonOrderService->updateData($whereUpdate, $updateOrder);
        //            //增加运单日志
        //            $this->WayBillService->addLog($order["id"], $waybill_id, $waybillInsertData["platform_id"], '添加新运单');
        //            //处理订单进度
        //            if (isset($request['AmazonOrderStatus']) && !is_null($request['AmazonOrderStatus']) && (int)$request['AmazonOrderStatus'] != -1) {
        //                $this->AmazonOrderService->changeState($order["id"], $request['AmazonOrderStatus']);
        //            }
        //            Db::commit();

        //            // // 自动扣减库存
        //            // Stock::orderGoodsOut($goods, $order['order_no'], $waybill_id, $waybillInsertData['waybill_order'], '客户端批量运单发货');
        //        } catch (LowStocksException $e) {
        ////            $f = function (\Exception $e) use (&$f) {
        ////                if ($e->getPrevious()) {
        ////                    return $e->getMessage().$f($e->getPrevious());
        ////                } else {
        ////                    return $e->getMessage();
        ////                }
        ////            };
        ////            dd($f($e));
        //            return $this->fail(self::LowStocks, null, $e->getMessage());
        //        } catch (\Exception $e) {
        //            Db::rollBack();
        //            return $this->fail(self::Database, $e->getMessage());
        //        }
        //        return $this->success($waybill_id);
        //    }
        #endregion
    }
}
