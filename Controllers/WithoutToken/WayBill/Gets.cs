﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.WayBill
{
    public class Gets : Controller
    {
        ////原ERP3.0注释
        #region ERP3.0 PHP
        //    public const WaybillIsEmpty = 1;   //暂无运单信息


        //    public function getApiId(): int
        //{
        //    return 590;
        //}

        //public function handle($request, $userId, $rule, $session)
        //{

        //    $companyId = $this->getSessionObj()->getCompanyId();
        //    $waybills = WayBill::query()
        //        ->where(fn($query) => $query->where(fn($queryAnd) => $queryAnd->companyId($companyId)
        //    ->OperateCompanyId($companyId))
        //            ->orWhere('waybill_company_id', $companyId)
        //        )
        //        ->whereIn('button_name', ['跟踪', '追踪'])
        //        ->select(['id', 'button_name'])
        //        ->orderby('updated_at')
        //        ->limit(5)
        //        ->get();

        //    if ($waybills->isEmpty()) {
        //        return $this->fail(self::WaybillIsEmpty);
        //    }
        //    $ids = $waybills->map(fn($item) => $item['id']);

        //    //更新最后修改时间
        //    WayBill::query()->whereIn('id', $ids)->update(['updated_at' => Carbon::now()]);
        //    return $this->success($ids);

        //}
        #endregion

        #region SQL
        public IActionResult invoke(/*$request, $userId, $rule, $session*/)
        {
            /************************************************  
             ================/waybill/gets
            select `id`, `d_button_name` from `waybill` where ((`i_company_id` = ? and `i_operate_company_id` = ?) or `i_waybill_company_id` = ?) and `d_button_name` in (?, ?) order by `updated_at` asc limit *
            ************************************************/
            return View();
        }
        #endregion
    }
}
