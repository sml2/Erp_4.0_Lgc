﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.WayBill
{
    public class UpdateMethod : Controller
    {
        ////原ERP3.0注释
        #region ERP3.0 PHP
        //    const NotFoundWaybill = 1; //运单不存在
        //    const Database = 2; //更新运单货运方式失败（上报失败,可能缺少必须参数[T]）

        //    private WayBillService $WayBillService;
        //private AmazonOrderService $AmazonOrderService;

        //public function __construct()
        //    {
        //    $this->WayBillService = new WayBillService();
        //    $this->AmazonOrderService = new AmazonOrderService();
        //    }

        //    public function getApiId()
        //    {
        //        return 580;
        //    }

        //    public function verifyParams()
        //    {
        //        return [
        //            'ID',
        //            'ShipMethod'
        //        ];
        //    }

        //    public function handle($request, $userId, $rule, $session)
        //    {

        //    $id = $request['ID'];
        //    $shipMethod = $request['ShipMethod'];

        //    $info = $this->WayBillService->getWaybillByOrderId($id)->toArray();
        //        if (!checkArr($info))
        //        {
        //            return $this->fail(self::NotFoundWaybill);
        //        }
        //        //启动事务
        //        Db::begintransaction();
        //        try
        //        {

        //            foreach ($info as $value) {
        //            $logistic_ext = json_decode($value['logistic_ext'], true);
        //                if (!checkArr($logistic_ext) || $logistic_ext['ShipMethod'] == $shipMethod) {
        //                    continue;
        //                }

        //            $action = '修改货运方式'. $logistic_ext['ShipMethod']. '为'. $shipMethod;
        //            $logistic_ext['ShipMethod'] = $shipMethod;

        //            $this->WayBillService->addLog($value['order_id'], $value['id'], $value['platform_id'], $action);
        //            $this->WayBillService->updateData($value['id'], ['logistic_ext' => json_encode($logistic_ext)]);
        //            }
        //            Db::commit();
        //        }
        //        catch (\Exception $e) {
        //            Db::rollBack();
        //            return $this->fail(self::Database, $e->getMessage());
        //        }
        //        return $this->success();
        //        }
        #endregion

        #region SQL
        public IActionResult invoke(/*$request, $userId, $rule, $session*/)
        {
            /************************************************  
             ================/order/get  
            select * from `amazon_order` where (`id` = ?) limit *
            select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
            update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
            select `i_store_id` from `to_sell_task` where `i_company_id` = ? and `t_is_finish` = ? and `i_oem_id` = ?
            ************************************************/
            return View();
        }
        #endregion
    }
}
