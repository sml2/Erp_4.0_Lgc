﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.WayBill
{
    public class Upload : Controller
    {
        ////原ERP3.0注释
        #region ERP3.0 PHP
        //        const AMAZON_PLATFORM_ID = 1;
        //        //530
        //        const NotFoundOrder = 1; //（订单不存在）
        //        const OrderIsEmpty = 2; //（订单下无产品）
        //        const LogisticsIsEmpty = 3; //（不存在的物流平台,请确认是否删除）
        //        const Database = 4; //（上报失败,可能缺少必须参数[T]）数据库事务报错
        //        const LowStocks = 5; // 库存不足

        //        const WAYBILL_SUBMITTED = 100;  //运单状态--已提交

        //        private $AmazonOrderService;
        //    private $AmazonOrderModel;
        //    private $CounterGroupService;
        //    private $CounterUserService;
        //    private $CounterCompanyService;
        //    private $AmazonOrderActionService;
        //    private $WayBillService;
        //    private $PurchaseModel;
        //    private $PurchaseService;
        //    private $LogisticsShipperService;
        //    private $LogisticsParamService;

        //    public function __construct()
        //        {
        //        $this->AmazonOrderService = new AmazonOrderService();
        //        $this->AmazonOrderModel = new AmazonOrderModel();
        //        $this->CounterGroupService = new CounterGroupService();
        //        $this->CounterUserService = new CounterUserService();
        //        $this->CounterCompanyService = new CounterCompanyService();
        //        $this->AmazonOrderActionService = new AmazonOrderActionService();
        //        $this->WayBillService = new WayBillService();
        //        $this->PurchaseModel = new PurchaseModel();
        //        $this->PurchaseService = new PurchaseService();
        //        $this->LogisticsShipperService = new LogisticsShipperService();
        //        $this->LogisticsParamService = new LogisticsParamService();
        //        }

        //        public function getApiId()
        //        {
        //            return 530;
        //        }

        //        public function verifyParams()
        //        {
        //            return [
        //                'Num',
        //                'OrderNum',
        //                'LType',
        //                'Date',
        //                'TrackNum',
        //                'TrackInfo',
        //                'TrackButtonName',
        //                'State',
        //                'OrderID',
        //                'SenderInfo',
        //                'Parameter',
        //                'OrderProducts',
        //                'ParameterExt',
        //                'Note',
        //                'Unit',
        //                'Price',
        //                'ShipMethod'
        //            ];
        //        }

        //        private function getRate($unit)
        //        {
        //            return $unit == 'CNY' ? 10000 : Cache::instance()->unitSingle()->get($unit)['rate'];
        //        }

        //        public function handle($request, $userId, $rule, $session)
        //        {
        //        $product = json_decode($request['OrderProducts'], true);
        //            //        $product = $request['OrderProducts'];
        //            if (!checkArr($product))
        //            {
        //                return $this->fail(self::OrderIsEmpty);
        //            }

        //        $orderInfo = $this->AmazonOrderService->getOrderInfoFirst($request['OrderID']);
        //            if (!$orderInfo) {
        //                return $this->fail(self::NotFoundOrder);
        //            }

        //            //是否合并订单
        //            if ($orderInfo->merge_id != -1){
        //            $is_merge_order = 1;
        //            $orderGoods = \App\Http\ControllersWithoutToken\Order\Get::getMergeOrderMessage($orderInfo->merge_id, true);
        //            }else
        //            {
        //            $orderGoods = $orderInfo->getRawOriginal("goods");
        //            $orderGoods = json_decode($orderGoods, true);
        //            $is_merge_order = 0;
        //            }

        //            if (!count($orderGoods))
        //            {
        //                return $this->fail(self::OrderIsEmpty);
        //            }
        //        //订单所属公司
        //        $company = Cache::instance()->Company()->get($orderInfo['company_id']);

        //        $goodIdPrefixOrderGoods = arrayCombine($orderGoods, 'good_id');
        //        $goodIdPrefixProduct = arrayCombine($product, 'good_id');
        //        $goodIdPrefixProductKeys = array_keys($goodIdPrefixProduct);
        //            foreach ($goodIdPrefixProductKeys as $k => $v) {
        //                if (isset($goodIdPrefixOrderGoods[$v]))
        //                {
        //                $goodIdPrefixOrderGoods[$v]['delivery'] ??= 0;
        //                $goodIdPrefixOrderGoods[$v]['delivery'] += $goodIdPrefixProduct[$v]['Count'];
        //                $goodIdPrefixOrderGoods[$v]['send_num'] -= $goodIdPrefixProduct[$v]['Count'];
        //                $goodIdPrefixOrderGoods[$v]['sendnum'] = $goodIdPrefixProduct[$v]['Count'];
        //                $goodIdPrefixOrderGoods[$v]['send_num'] < 0 && $goodIdPrefixOrderGoods[$v]['send_num'] = 0;
        //                    if (Stock::checkGoodsOut($goodIdPrefixOrderGoods[$v]))
        //                    {
        //                    $goodIdPrefixOrderGoods[$v]['stock_out_num'] = isset($goodIdPrefixOrderGoods[$v]['stock_out_num']) ? $goodIdPrefixOrderGoods[$v]['stock_out_num'] + $goodIdPrefixProduct[$v]['Count'] : $goodIdPrefixProduct[$v]['Count'];
        //                    }
        //                    else
        //                    {
        //                    $goodIdPrefixOrderGoods[$v]['stock_out_num'] = 0;
        //                    }
        //                }
        //                else
        //                {
        //                    return $this->fail(self::OrderIsEmpty);
        //                }
        //            }

        //        $deliver_num = 0;
        //        $img_url = '';
        //            foreach ($product as &$value) {
        //            $img_url == '' && $img_url = $value['URL'];
        //            $value = array_change_key_case($value, CASE_LOWER);
        //            $deliver_num = $deliver_num + $value['count'];
        //            $num = $value['send_num'] - $value['count'];
        //            $value['send_num'] = $num > 0 ? $num: 0;
        //            $value['delivery'] = $value['count'];
        //                if ($goodIdPrefixOrderGoods[$value['good_id']]) {
        //                $value['warehouse_id'] = $goodIdPrefixOrderGoods[$value['good_id']]['warehouse_id'] ?? 0;
        //                $value['warehouse_name'] = $goodIdPrefixOrderGoods[$value['good_id']]['warehouse_name'] ?? '';
        //                $value['product_id'] = $goodIdPrefixOrderGoods[$value['good_id']]['product_id'] ?? 0;
        //                $value['variant_id'] = $goodIdPrefixOrderGoods[$value['good_id']]['variant_id'] ?? '';
        //                $value['tags'] = $goodIdPrefixOrderGoods[$value['good_id']]['tags'] ?? [];
        //                $value['stock_out_num'] = 0;
        //                }
        //                unset($value['count']);
        //            }

        //            // 物流
        //            if ($request['LType'] == 'none') {
        //            $logistic_id = 0;
        //            $logistic_name = $request['Note'];
        //            } else
        //            {
        //            $logistics = arrayCombine(Cache::instance()->logistics()->get(), 'sign');
        //            $logisticsInfo = $logistics[strtolower($request['LType'])];
        //                if (!$logisticsInfo) {
        //                    return $this->fail(self::LogisticsIsEmpty);
        //                }
        //            $logistic_id = $logisticsInfo['id'];
        //            $logistic_name = $logisticsInfo['name'];
        //            }

        //        $carriage_base = $request['Price'] ? FromConversion($request['Price'], $request['Unit']) : 0;

        //        $data = [
        //            'order_id'           => $request['OrderID'],
        //            'logistic_id'        => $logistic_id,
        //            'nation_id'          => $orderInfo['receiver_nation_id'],
        //            'logistic'           => $logistic_name,
        //            'waybill_order'      => $request['OrderNum'],
        //            'express'            => $request['Num'],
        //            'tracking'           => $request['TrackNum'] ?? '',
        //            'deliver_date'       => date("Y-m-d H:i:s", $request['Date']),
        //            'remark'             => $request['Note'],
        //            'user_id'            => $session->getUserId(),
        //            'truename'           => $session->getTruename(),
        //            'bill_log_id'        => 0,
        //            'platform_id'        => self::AMAZON_PLATFORM_ID,
        //            'order_no'           => $orderInfo['order_no'],
        //            'img_url'            => $img_url,
        //            'product_info'       => json_encode($product),
        //            'track_info'         => $request['TrackInfo'],
        //            'logistic_params'    => $request['Parameter'],
        //            'logistic_ext'       => $request['ParameterExt'],
        //            'waybill_state'      => self::WAYBILL_SUBMITTED,
        //            'button_name'        => $request['TrackButtonName'],
        //            'sender'             => $request['SenderInfo'],
        //            'carriage_base'      => $carriage_base,
        //            'carriage_other'     => 0,
        //            'carriage_num'       => $carriage_base,
        //            'carriage_unit'      => $request['Unit'],
        //            'store_id'           => $orderInfo['store_id'],
        //            'ship_method'        => $request['ShipMethod'],
        //            'is_verify'          => WayBillModel::ISVERIFY_NO,
        //            'waybill_rate'       => 0,
        //            'waybill_rate_type'  => $company['waybill_rate_type'],
        //            'rate'               => $this->getRate($request['Unit']),
        //            'label_url'          => $request['LabelUrl'] ?? '',
        //            'company_id'         => $orderInfo['company_id'],
        //            'operate_company_id' => $this->getSessionObj()->getCompanyId(),
        //            'waybill_company_id' => $orderInfo['waybill_company_id'],
        //            'created_at'         => date("Y-m-d H:i:s", $this->getTime()),
        //            'updated_at'         => date("Y-m-d H:i:s", $this->getTime()),
        //        ];


        //        /*************************************分销运单**********************************************/
        //        $oem_id = $session->getOemId();
        //        $group_id = $session->getGroupId();
        //        $distribution_waybill_num = 0;
        //        $process_waybill = AmazonOrderModel::PROCESSWAYBILL_INSIDE;

        //        if ($company['is_distribution'] == companyModel::DISTRIBUTION_TRUE) {
        //            $isDistribution = false;
        //            if ($company['id'] != $session->getCompanyId()){
        //                //上级发货
        //                $isDistribution = true;
        //                $group_id = 0;
        //            }else{
        //                //下级发货，使用上级分配物流
        //                $logisticParams = json_decode($request['Parameter'],true);
        //                $logisticParamId = $logisticParams['ID'];
        //                //判断物流参数是否上级分配  对公钱包
        //                $logisticParam = $this->LogisticsParamService->getLogisticsInfo($logisticParamId);
        //                if($logisticParam && $logisticParam['state'] == 1 && $logisticParam['is_sys_state'] == 1 && $logisticParam['source'] == 2 && $logisticParam['charge'] == 1){
        //                    $isDistribution = true;

        //                    $data['operate_company_id'] = $session->getReportId();
        //                }
        //            }

        //            if ($isDistribution) {
        //                //上级发货
        //                $oem_id = $company['oem_id'];
        //                $distribution_waybill_num = 1;
        //                $process_waybill = AmazonOrderModel::PROCESSWAYBILL_SUPERIOR;

        //                $waybill_rate = $company['waybill_rate_type'] == 1 ? bcdiv(bcmul($carriage_base, $company['waybill_rate'], 6), 100, 6) : FromConversion($company['waybill_rate_num'], $request['Unit']);
        //                $carriage_num = bcadd($carriage_base, $waybill_rate, 6);
        //                //分销手续费
        //                $data['waybill_rate'] = $waybill_rate;
        //                $data['carriage_num'] = $carriage_num;
        //            }
        //        }
        //        $orderInfo['process_waybill'] == AmazonOrderModel::PROCESSWAYBILL_NONE && $updateOrder['process_waybill'] = $process_waybill;
        //        $data['oem_id'] = $oem_id;
        //        $data['group_id'] = $group_id;
        //        $data['purchase_tracking_number'] = checkArr($orderInfo['purchase_tracking_number']) ? join(',', $orderInfo['purchase_tracking_number']) : '';
        //        /*************************************分销运单**********************************************/
        //        //启动事务
        //        Db::begintransaction();
        //        try {
        //            //添加运单信息
        //            $waybill_id = WayBillModel::query()->insertGetId($data);
        //            $express = $orderInfo['express'];
        //            if(checkArr($express)){
        //                $express[] = $waybill_id;
        //                $updateOrder['express'] = implode(',', $express);
        //            }else{
        //                $updateOrder['express'] = $waybill_id;
        //            }

        //            //修改计数
        //            $this->CounterUserService->waybillNumAdd(1, $distribution_waybill_num);
        //            $session->getGroupId() != 0 && $this->CounterGroupService->waybillNumAdd(1, $distribution_waybill_num);
        //            $this->CounterCompanyService->WaybillNumAdd(1, $distribution_waybill_num, $company['id']);
        //            $this->CounterCompanyService->reportWaybillNumAdd(1, json_decode($company['report_pids'], true));
        //            //商店统计表中没有计算运单个数的字段 $this->CounterStoreService
        //            //增加订单日志
        //            $this->AmazonOrderActionService->addLog($orderInfo["id"], '订单添加运单', $orderInfo["order_state"]);
        //            //修改订单进度
        //            $updateOrder['delivery_num'] = Db::raw(
        //                $this->AmazonOrderModel->transformKey('delivery_num') . '+' . $deliver_num
        //            );
        //            //更新sender和logisticparam时间
        //            $this->LogisticsShipperService->updateLastTime(json_decode($request['SenderInfo'], true)['SID']);
        //            $this->LogisticsParamService->updateLastTime(json_decode($request['Parameter'], true)['ID']);

        //            //修改订单为发货中
        //            //查询物流是否在发货中
        //            $progressBiter = new Progress($orderInfo['progress_original']);
        //            $hassend = $progressBiter->hasDeliver();
        //            //董哥要求 2021.6.7 已发货后再次发货要求重新变更处理中  去除判断条件
        ////            if (!$hassend) {
        //                $progressBiter->setDeliver(true);
        //                $progressBiter->setProgress(AmazonOrderModel::PROGRESS_PROCESSING);
        //                $progress = $progressBiter->getFlags();
        //                $updateOrder['progress'] = $progress;
        ////            }
        //            //向订单/采购表中添加物流单号
        ////            AmazonOrder::updateWaybillOrder($orderInfo['id'], $waybill_id);

        //            if($is_merge_order){//合并订单
        //                /**
        //                    分别拿出合并订单下每个单独订单的商品，把本次发货的信息通过比对goodid将处理过的商品信息进行覆盖
        //                 * 然后分别存储到数据库
        //                 **/
        ////                dd($goodIdPrefixOrderGoods);

        //                $order_ids = $this->getMergeOrderIds($orderInfo);
        //                foreach($order_ids as $id){
        //                    $_order_info = $this->AmazonOrderService->getOrderInfoWhereFirst(["*"],$id);
        //                    if($_order_info){
        //                        $_order_info_goods = $_order_info->getRawOriginal("goods");
        //                        if($_order_info_goods){
        //                            $_order_info_goods = json_decode($_order_info_goods,true);
        //                            $_order_info_goods = arrayCombine($_order_info_goods, 'good_id');
        ////                            dump($_order_info_goods);
        //                            foreach($_order_info_goods as $_oigk => &$_orgv){
        //                                if(isset($goodIdPrefixOrderGoods[$_oigk])){
        //                                    $_order_info_goods[$_oigk] = $goodIdPrefixOrderGoods[$_oigk];
        //                                }
        //                            }
        //                            //处理完数据，进行存储
        //                            $this->AmazonOrderService->updateData([["id","=",$id]],["goods" => array_values($_order_info_goods)]);
        //                        }
        //                    }
        //                }
        //            }else{//非合并订单
        //                //修改订单下的产品
        //                $updateOrder['goods'] = json_encode(array_values($goodIdPrefixOrderGoods));
        //            }
        //            //上面的共同条件
        //            $whereUpdate[] = ['id', '=', $request['OrderID']];
        //            $this->AmazonOrderService->updateData($whereUpdate, $updateOrder);

        //            //增加运单日志
        //            $this->WayBillService->addLog($orderInfo["id"], $waybill_id, $data["platform_id"], '添加新运单');
        //            //处理订单进度
        //            if (isset($request['AmazonOrderStatus']) && !is_null($request['AmazonOrderStatus']) && (int)$request['AmazonOrderStatus'] != -1) {
        //                $this->AmazonOrderService->changeState($orderInfo["id"], $request['AmazonOrderStatus']);
        //            }
        //            Db::commit();
        //            // 自动扣减库存
        //            // Stock::orderGoodsOut($product, $orderInfo['order_no'], $waybill_id, $data['waybill_order'], '客户端运单发货');
        //        } catch (LowStocksException $e) {
        //            return $this->fail(self::LowStocks, null, $e->getMessage());
        //        } catch (\Exception $e) {
        //            Db::rollBack();
        //            return $this->fail(self::Database, $e->getMessage());
        //        }
        //        return $this->success();
        //    }

        //    /**
        //     * 获取合并订单的订单id
        //     * User: CCY
        //     * Date: 2019/11/18 15:10
        //     * @param $order
        //     */
        //    private function getMergeOrderIds($order)
        //    {
        //        $merge_id = $order["merge_id"];
        //        //查询合并订单数据
        //        $merge = OrderMergeModel::query()->find($merge_id);
        //        if (!$merge) {
        //            return $this->fail(self::NotFoundOrder);
        //        }
        //        $order_info = json_decode($merge["order_info"], true);
        //        return array_column($order_info, "order_id");
        //    }
        #endregion

        #region SQL
        public IActionResult Invoke(/*$request, $userId, $rule, $session*/)
        {
            /************************************************  
            ================/waybill/upload
            select * from `amazon_order` where `id` = ? limit *
            insert into `waybill` (`i_order_id`, `i_logistic_id`, `i_nation_id`, `d_logistic`, `d_waybill_order`, `d_express`, `d_tracking`, `d_deliver_date`, `d_remark`, `i_user_id`, `d_truename`, `i_bill_log_id`, `i_platform_id`, `d_order_no`, `d_img_url`, `d_product_info`, `d_track_info`, `d_logistic_params`, `d_logistic_ext`, `t_waybill_state`, `d_button_name`, `d_sender`, `d_carriage_base`, `d_carriage_other`, `d_carriage_num`, `d_carriage_unit`, `i_store_id`, `d_ship_method`, `t_is_verify`, `d_waybill_rate`, `t_waybill_rate_type`, `d_rate`, `d_label_url`, `i_company_id`, `i_operate_company_id`, `i_waybill_company_id`, `created_at`, `updated_at`, `i_oem_id`, `i_group_id`, `u_purchase_tracking_number`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            update `counter_user_business` set `c_waybill_num` = c_waybill_num+*, `c_distribution_waybill_num` = c_distribution_waybill_num+*, `counter_user_business`.`updated_at` = ? where `i_user_id` = ?
            update `counter_company_business` set `c_waybill_num` = c_waybill_num+*, `c_distribution_waybill_num` = c_distribution_waybill_num+*, `counter_company_business`.`updated_at` = ? where `i_company_id` = ?
            update `counter_company_business` set `c_report_waybill_num` = `c_report_waybill_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` in (?, ?)
            insert into `amazon_order_action` (`i_order_id`, `d_action`, `i_user_id`, `i_group_id`, `i_company_id`, `i_oem_id`, `d_user_name`, `d_truename`, `t_order_status`, `d_order_status_text`, `created_at`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            update `logistics_shipper` set `d_last_time` = ?, `logistics_shipper`.`updated_at` = ? where `id` = ?
            update `logistics_param` set `d_last_time` = ?, `logistics_param`.`updated_at` = ? where `id` = ?
            update `amazon_order` set `d_process_waybill` = ?, `u_express` = ?, `d_delivery_num` = d_delivery_num+*, `b_progress` = ?, `d_goods` = ?, `amazon_order`.`updated_at` = ? where (`id` = ?)
            insert into `waybill_log` (`i_waybill_id`, `d_action`, `i_order_id`, `i_platform_id`, `d_user_name`, `d_truename`, `i_user_id`, `i_company_id`, `i_group_id`, `i_oem_id`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            update `amazon_order` set `b_progress` = (b_progress & ~*) ^ (* << *) , `amazon_order`.`updated_at` = ? where `id` = ?
            select * from `amazon_order` where `amazon_order`.`id` = ? limit *
            select * from `waybill` where (`i_order_id` = ?)
            select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
            update `store` set `d_last_auto_time` = ?, `store`.`updated_at` = ? where `id` = ?
            select `id`, `d_button_name` from `waybill` where ((`i_company_id` = ? and `i_operate_company_id` = ?) or `i_waybill_company_id` = ?) and `d_button_name` in (?, ?) order by `updated_at` asc limit *
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            update `store` set `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
            select * from `product` where `id` = ? and b_type & * limit *
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            insert into `temp_images` (`d_oss_path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `i_product_id`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            update `waybill` set `waybill`.`updated_at` = ? where `id` in (?, ?, ?, ?, ?)
            select * from `waybill` where `waybill`.`id` = ? limit *
            update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `waybill`.`updated_at` = ? where `id` = ?
            select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
            update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
            select count(*) as count, `i_store_id` from `sync_mws` where `t_state` in (?, ?) and `i_company_id` = ? and `i_oem_id` = ? group by `i_store_id`
            select `i_store_id` from `to_sell_task` where `i_company_id` = ? and `t_is_finish` = ? and `i_oem_id` = ?
            select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?, ?, ?, ?, ?)
            insert into `resource` (`created_at`, `d_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
            delete from `temp_images` where `id` in (?, ?, ?, ?, ?, ?)
            update `product` set `u_lp_k` = ?, `u_lp_s` = ?, `u_lp_d` = ?, `b_type` = ?, `u_price` = ?, `u_quantity` = ?, `d_source` = ?, `d_ext` = ?, `d_attribute` = ?, `d_variants` = ?, `d_images` = ?, `u_image` = ?, `d_pid` = ?, `d_pid_hash_code` = ?, `c_size` = ?, `d_coin` = ?, `d_rate` = ?, `product`.`updated_at` = ? where `id` = ?
            update `counter_company_business` set `c_product_num` = `c_product_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` = ?
            update `counter_company_business` set `c_report_product_num` = `c_report_product_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` in (?, ?, ?, ?)
            update `counter_user_business` set `c_product_num` = `c_product_num` + *, `counter_user_business`.`updated_at` = ? where `i_user_id` = ?
            select `id`, `i_category_id`, `i_sub_category_id`, `i_distribution_category_id`, `i_distribution_sub_category_id`, `u_lp_n`, `u_lp_k`, `u_lp_s`, `u_lp_d`, `u_price`, `u_quantity`, `d_languages`, `b_language`, `d_source`, `d_ext`, `d_attribute`, `d_variants`, `d_images`, `b_type`, `updated_at`, `d_pid`, `d_coin`, `d_rate` from `product` where (`i_company_id` = ? and `id` = ? and `i_oem_id` = ?) limit *
            select * from `store` where `i_company_id` = ? and `t_flag` = ? and `t_is_del` = ? and `t_is_verify` = ?
            select `id`, `d_title`, `i_user_id`, `t_state` from `work_order` where `i_company_id` = ? and `t_state` in (?, ?) limit *
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
            select `d_order_no` from `amazon_order` where (`i_store_id` = ? and `t_order_state` = ? and `i_company_id` = ? and `t_entry_mode` = ?) limit *
            update `store` set `d_last_auto_update_time` = ?, `store`.`updated_at` = ? where `id` = ?
            select `d_temp_name`, `i_browser_node_id`, `u_browser_ui`, `t_level`, `i_nation_id`, `d_type_data` from `category_node_default` where `d_type_hash` = ? and (`t_level` = ? or (`i_company_id` = ? and `t_level` = ?) or (`i_company_id` = ? and `i_group_id` = ? and `t_level` = ?)) order by `t_level` desc, `d_sort` asc
            insert into `reporter_request` (`created_at`, `d_action`, `d_cpu_time`, `d_method`, `d_module`, `d_name`, `d_namespace`, `d_net_time`, `d_request_id`, `d_request_time`, `d_uri`, `i_company_id`, `i_group_id`, `i_oem_id`, `i_user_id`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select * from `task` where (`i_oem_id` = ? and `i_user_id` = ? and `i_group_id` = ? and `d_hash_code` = ?) limit *
            select count(*) as aggregate from `product` where (`d_hash_code` = ? and `i_company_id` = ?) and !(b_type&*) AND !(b_type&*) AND !(b_type&*)
            insert into `product` (`i_oem_id`, `i_company_id`, `i_user_id`, `i_group_id`, `i_task_id`, `u_lp_n`, `u_lp_k`, `u_lp_s`, `u_lp_d`, `d_source`, `d_hash_code`, `d_languages`, `b_language`, `b_type`, `u_price`, `i_platform_id`, `u_platform`, `d_attribute`, `d_variants`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select * from `product` where `id` = ? and `i_company_id` = ? and `i_oem_id` = ? limit *
            insert into `product` (`i_oem_id`, `i_company_id`, `i_user_id`, `i_group_id`, `i_category_id`, `i_sub_category_id`, `u_lp_n`, `u_lp_s`, `u_lp_k`, `u_lp_d`, `d_source`, `d_hash_code`, `d_languages`, `b_language`, `b_type`, `u_price`, `i_platform_id`, `u_platform`, `d_attribute`, `d_variants`, `d_ext`, `d_images`, `u_image`, `c_size`, `d_pid`, `created_at`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            update `resource` set `c_reuse` = `c_reuse` + *, `resource`.`updated_at` = ? where `id` in (?, ?, ?, ?, ?)
            insert into `sync_product` (`i_pid`, `i_source_id`, `u_product_name`, `u_img_url`, `d_sku`, `t_sku_type`, `t_sku_suffix`, `t_title_suffix`, `t_product_type`, `u_category_id`, `u_sub_category_id`, `t_already_upload`, `t_uploading`, `i_user_id`, `i_group_id`, `i_company_id`, `i_oem_id`, `t_eanupc_status`, `d_type_data`, `d_type_data_id`, `created_at`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            insert into `sync_mws` (`created_at`, `d_browser_node_id`, `d_country_of_origin`, `d_lead_time`, `d_sku_change_data`, `i_company_id`, `i_group_id`, `i_oem_id`, `i_spid`, `i_store_id`, `i_user_id`, `t_state`, `u_language`, `u_product`, `u_product_name`, `u_store_name`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select count(*) as aggregate from `sync_product` where `i_company_id` = ? and `i_oem_id` = ?
            select * from `sync_product` where `i_company_id` = ? and `i_oem_id` = ? order by `id` desc limit * offset *
            select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?)
            select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?, ?, ?, ?)
            insert into `resource` (`created_at`, `d_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
            delete from `temp_images` where `id` in (?, ?, ?, ?, ?)
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select * from `product` where `i_oem_id` = ? and `id` in (?)
            delete from `ean_upc` where `id` in (?)
            update `product` set `d_variants` = ?, `product`.`updated_at` = ? where `id` = ?
            select * from `sync_product` where `i_pid` = ? and `i_company_id` = ? and `i_oem_id` = ? limit *
            update `sync_mws` set `t_eanupc_status` = ?, `d_sku_change_data` = ?, `i_upload_id` = ?, `t_state` = ?, `sync_mws`.`updated_at` = ? where `i_spid` in (?) and `i_company_id` = ? and `i_oem_id` = ?
            select `d_action`, `d_truename`, `created_at` from `amazon_order_action` where `i_order_id` = ? order by `created_at` desc
            select `i_order_id`, `d_remark`, `d_truename`, `created_at` from `amazon_order_remark` where `i_company_id` = ? and `i_order_id` = ? order by `created_at` desc
            select `id`, `d_logistic`, `d_waybill_order`, `i_company_id`, `d_express`, `d_tracking`, `d_deliver_date`, `t_waybill_state`, `d_carriage_unit`, `d_carriage_num`, `d_button_name`, `i_operate_company_id`, `t_is_verify`, `i_waybill_company_id` from `waybill` where `i_order_id` = ? and `i_platform_id` = ?
            select * from `stock_log` where `i_execute_company_id` = ? and `i_company_id` = ? and `h_order_no_hashcode` = ? order by `created_at` desc
            select * from `sync_mws` where `i_store_id` in (?, ?, ?) and `t_state` in (?, ?) and `t_eanupc_status` = ? order by `updated_at` desc limit * offset *
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            insert into `sync_upload` (`i_store_id`, `i_user_id`, `i_company_id`, `i_oem_id`, `i_group_id`, `i_operate_id`, `t_state`, `t_product_state`, `t_variant_state`, `t_pricing_state`, `t_inventory_state`, `t_image_state`, `t_relationship_state`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select column_name as `column_name` from information_schema.columns where table_schema = ? and table_name = ?
            insert into `sync_user` (`i_upload_id`, `i_old_user_id`, `i_new_user_id`, `d_last_updated`, `i_company_id`, `i_group_id`, `i_oem_id`, `t_is_changed`, `t_is_in_*_min`, `t_type`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            insert into `sync_online` (`i_upload_id`, `i_user_id`, `i_company_id`, `i_group_id`, `i_oem_id`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?)
            update `sync_mws` set `i_upload_id` = ?, `t_state` = ?, `sync_mws`.`updated_at` = ? where `i_store_id` = ? and `t_eanupc_status` = ? and `i_upload_id` is null limit *
            update `sync_upload` set `c_count` = ?, `sync_upload`.`updated_at` = ? where `id` = ?
            select `id`, `i_operate_id`, `updated_at` from `sync_upload` where (`i_store_id` = ? and `t_state` = ? and `updated_at` < ?)
            select `id` from `sync_upload` where `i_store_id` = ? and `t_state` = ? and `i_operate_id` = ?
            select `i_pid`, `id`, `t_sku_type`, `t_sku_suffix`, `t_title_suffix`, `i_source_id`, `d_sku`, `d_type_data`, `d_type_data_id` from `sync_product` where `i_company_id` = ? and `id` = ? limit *
            select * from `product` where `id` = ? and `i_company_id` = ? limit *
            select * from `sync_upload` where `id` = ? limit *
            insert into `sync_invoke_api_log` (`i_upload_id`, `d_request_data`, `i_upload_operate_id`, `i_user_id`, `i_company_id`, `i_group_id`, `i_oem_id`, `t_api`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
            select * from `sync_mws` where `id` = ? limit *
            select * from `sync_product` where `i_company_id` = ? and `id` = ? limit *
            select * from `product` where (`i_oem_id` = ? and `i_company_id` = ? and `id` = ?) limit *
            update `sync_mws` set `d_sku_change_data` = ?, `sync_mws`.`updated_at` = ? where (`id` = ?)
            update `sync_upload` set `t_state` = ?, `d_error_index` = ?, `d_error_info` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
            ************************************************/
            return View();
        }
        #endregion
    }
}
