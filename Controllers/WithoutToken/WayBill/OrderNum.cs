﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.WayBill
{
    public class OrderNum : Controller
    {

        ////原ERP3.0注释
        #region ERP3.0 PHP
        //    //550
        //    const NotFoundOrder = 1; //物流信息不存在(订单不存在)
        //    const Database = 2;//上报失败,可能缺少必须参数[T]
        //    private $WayBillService;
        //public function __construct()
        //    {
        //    $this->WayBillService = new WayBillService();
        //    }
        //    public function getApiId()
        //    {
        //        return 550;
        //    }

        //    public function handle($request, $userId, $rule, $session)
        //    {

        //        //检查权限
        //        //        if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$session->getRule()->getWaybill()->getBiter()->hasWaybillEdit()) {
        //        //            return error('无权访问');
        //        //        }
        //        if (!$request['ID']){
        //            return  $this->fail(self::Database, 'ID');
        //        }
        //        if (!$request['OrderNum']){
        //            return  $this->fail(self::Database, 'OrderNum');
        //        }
        //    $info = $this->WayBillService->findWayBill($request['ID']);
        //        if (!$info){
        //            return $this->fail(self::NotFoundOrder);
        //        }
        //    $updateData = [
        //        'waybill_order' => $request['OrderNum']
        //    ];
        //    $this->WayBillService->updateData($request['ID'],$updateData);
        //        return $this->success();
        //    }
        #endregion

        #region SQL
        public IActionResult invoke(/*$request, $userId, $rule, $session*/)
        {
            /************************************************  
             ================/order/get  
            select * from `amazon_order` where (`id` = ?) limit *
            select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
            update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
            select `i_store_id` from `to_sell_task` where `i_company_id` = ? and `t_is_finish` = ? and `i_oem_id` = ?
            ************************************************/
            return View();
        }
        #endregion
    }
}
