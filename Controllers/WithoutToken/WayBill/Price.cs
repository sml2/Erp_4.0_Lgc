﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.WayBill
{
    public class Price : Controller
    {
        ////原ERP3.0注释
        #region ERP3.0 PHP
        //        //560
        //        const NotFoundOrder = 1; //运单信息不存在(上报失败,可能缺少必须参数[T])
        //        const WAYBILL_TRANS_FAILED = 2; //更新失败
        //        const WAYBILL_CONFIRM = 3; //运单已确认，无法更新运费
        //        const WAYBILL_PRICE = 4; //运费存在，无需更新运费
        //        private $BillLogService;
        //    private $WayBillService;
        //    private $CounterUserService;
        //    private $CounterCompanyService;
        //    private $realFinanceService;
        //    private $CounterGroupService;
        //    private $CounterStoreService;
        //    private $AmazonOrderService;
        //    private $AmazonOrderModel;
        //    private $LogisticsParamService;

        //    public function __construct()
        //        {
        //        $this->BillLogService = new BillLogService();
        //        $this->WayBillService = new WayBillService();
        //        $this->CounterUserService = new CounterUserService();
        //        $this->CounterCompanyService = new CounterCompanyService();
        //        $this->realFinanceService = new realFinanceService();
        //        $this->CounterGroupService = new CounterGroupService();
        //        $this->CounterStoreService = new CounterStoreService();
        //        $this->AmazonOrderService = new AmazonOrderService();
        //        $this->AmazonOrderModel = new AmazonOrderModel();
        //        $this->LogisticsParamService = new LogisticsParamService();

        //        }

        //        public function getApiId()
        //        {
        //            return 560;
        //        }

        //        private function getCompany($company_id = '')
        //        {
        //            return Cache::instance()->Company()->get($company_id);
        //        }

        //        private function getRate($unit)
        //        {
        //            return $unit == 'CNY' ? 10000 : Cache::instance()->unitSingle()->get($unit)['rate'];
        //        }

        //        public function handle($request, $userId, $rule, $session)
        //        {
        //            //洛琴oem跳过更新价格
        //            if ($session->getOemId() == 2 && Str::contains($_SERVER['HTTP_HOST'], 'luoqin')) {
        //                return $this->success();
        //            }
        //            if (!isset($request['ID']) || !$request['ID']) {
        //                return $this->fail(self::ERR_PARAMETER, 'ID');
        //            }
        //            if (!$request['Unit']) {
        //                return $this->fail(self::ERR_PARAMETER, 'Unit');
        //            }
        //            if (!$request['Price']) {
        //                return $this->fail(self::ERR_PARAMETER, 'Price');
        //            }
        //        $info = $this->WayBillService->findWayBill($request['ID']);
        //            if (!$info) {
        //                return $this->fail(self::NotFoundOrder);
        //            }
        //            if ($info['carriage_num'] != 0) {
        //                //            return $this->success();
        //                return $this->fail(self::WAYBILL_PRICE);
        //            }
        //            //已确认运单跳过
        //            if ($info['is_verify'] === WayBill::ISVERIFY_YES) {
        //                //            return $this->success();
        //                return $this->fail(self::WAYBILL_CONFIRM);
        //            }

        //        $carriage_num = FromConversion($request['Price'], $request['Unit']);

        //        $updateData = [
        //            'carriage_base' => $carriage_num,
        //            'carriage_num'  => $carriage_num,
        //            'rate'          => $this->getRate($request['Unit']),
        //            'carriage_unit' => $request['Unit'],
        //        ];

        //        $company = $this->getCompany($info['company_id']);
        //            //上级添加 下级分销上报运单
        //            if ($company['is_distribution'] == companyModel::DISTRIBUTION_TRUE && $info['waybill_company_id'] != 0 && $info['operate_company_id'] == $info['waybill_company_id']) {
        //            $waybill_rate = $company['waybill_rate_type'] == 1 ? bcdiv(bcmul($carriage_num, $company['waybill_rate'], 6), 100, 6) : FromConversion($company['waybill_rate_num'], $request['Unit']);
        //            $carriage_num = bcadd($carriage_num, $waybill_rate);
        //            //分销手续费
        //            $updateData['waybill_rate'] = $waybill_rate;
        //            $updateData['carriage_num'] = $carriage_num;
        //            }

        //            //手动确认
        //            if ($company['waybill_confirm'] == companyModel::WAYBILL_CONFIRM_NO) {
        //                Db::beginTransaction();
        //                try
        //                {
        //                $this->WayBillService->updateData($request['ID'], $updateData);
        //                //增加运单日志
        //                $this->WayBillService->addLog($info["order_id"], $info["id"], $info["platform_id"], '同步更新运单运费');
        //                    DB::commit();
        //                }
        //                catch (\Exception $e) {
        //                    Db::rollBack();
        //                    return $this->fail(self::WAYBILL_TRANS_FAILED, $e->getMessage());
        //                }
        //                return $this->success();
        //                }
        //                //自动确认
        //                Db::beginTransaction();
        //                try
        //                {
        //                    //财务扣款记录
        //                    //上级添加 下级分销上报运单
        //                    if ($company['is_distribution'] == companyModel::DISTRIBUTION_TRUE && $info['waybill_company_id'] != 0 && $info['operate_company_id'] == $info['waybill_company_id']) {
        //                $belong = 2;
        //                        companyModel::query()->CompanyId($company['id'])->decrement('public_wallet', $carriage_num);
        //                    } else
        //                    {
        //                $belong = 1;
        //                        companyModel::query()->CompanyId($company['id'])->decrement('private_wallet', $carriage_num);
        //                    }

        ////            if ($info['waybill_company_id'] == 0) {
        ////                //内部运单
        ////                $company = $this->getCompany();
        ////                //分销
        ////                if ($this->getSessionObj()->getIsDistribution() == companyModel::DISTRIBUTION_TRUE) {
        ////                    //查询运单物流信息
        ////                    $logistics = $this->LogisticsParamService->getLogisticsInfo($info['logistic_id']);
        ////                    if ($logistics && $logistics['charge'] == LogisticParamModel::CHARGE_PUBLIC) {
        ////                        $belong = 2;
        ////                        companyModel::query()->CompanyId($company['id'])->decrement('public_wallet', $carriage_num);
        ////                    } else {
        ////                        $belong = 1;
        ////                        companyModel::query()->CompanyId($company['id'])->decrement('private_wallet', $carriage_num);
        ////                    }
        ////                } else {
        ////                    $belong = 1;
        ////                    companyModel::query()->CompanyId($company['id'])->decrement('private_wallet', $carriage_num);
        ////                }
        ////            } else {
        ////                //上报
        ////                $company = $this->getCompany($info['company_id']);
        ////                $belong = 2;
        ////                companyModel::query()->CompanyId($company['id'])->decrement('public_wallet', $carriage_num);
        ////            }

        //            //上报四张表
        //            $this->CounterUserService->distributionWaybillTotalAdd($session->getUserId(), $carriage_num);
        //            $this->CounterStoreService->addWaybillTotal($info['store_id'], $carriage_num);
        //            $session->getGroupId() != 0 && $this->CounterGroupService->distributionWaybillTotal($carriage_num);
        //            $this->CounterCompanyService->waybillTotalAdd($company['id'], $carriage_num);
        //            $this->realFinanceService->incrementWaybillTotal($company, $carriage_num);

        //            //修改订单汇总
        //            $updateOrder = array(
        //                "shipping_money" => Db::raw($this->AmazonOrderModel->transformKey('shipping_money'). '+'. $carriage_num),
        //                "profit"         => Db::raw($this->AmazonOrderModel->transformKey('profit'). '-'. $carriage_num)
        //            );
        //            $this->AmazonOrderService->update($info['order_id'], $updateOrder);


        //            $bill_log_id = $this->BillLogService->makeBillLog($carriage_num, $info['company_id'], $info['id'], $belong, $info['store_id'], 3, '订单编号：'. $info['order_no']. ';物流运单号：'. $info['waybill_order']);
        //            //当前数据确认
        //            $updateData['bill_log_id'] = $bill_log_id;
        ////            $updateData['carriage_num'] = $carriage_num;
        //            $updateData['is_verify'] = WayBill::ISVERIFY_YES;
        //            $this->WayBillService->updateData($request['ID'], $updateData);
        //            //增加运单日志
        //            $this->WayBillService->addLog($info["order_id"], $info["id"], $info["platform_id"], '同步更新运单运费');
        //                    DB::commit();
        //                }
        //                catch (\Exception $e) {
        //                    Db::rollBack();
        //                    return $this->fail(self::WAYBILL_TRANS_FAILED, $e->getMessage());
        //                }
        //                return $this->success();
        //                }
        #endregion

        #region SQL
        public IActionResult Invoke(/*$request, $userId, $rule, $session*/)
        {
            /************************************************  
            ================/waybill/price
            select * from `waybill` where `waybill`.`id` = ? limit *
            update `company` set `d_private_wallet` = `d_private_wallet` - *.*, `company`.`updated_at` = ? where `id` = ?
            update `counter_user_business` set `c_waybill_total` = `c_waybill_total` + *.*, `counter_user_business`.`updated_at` = ? where `i_user_id` = ?
            update `counter_store_business` set `c_waybill_total` = `c_waybill_total` + *.*, `counter_store_business`.`updated_at` = ? where `i_store_id` = ?
            update `counter_company_business` set `c_waybill_total` = `c_waybill_total` + *.*, `counter_company_business`.`updated_at` = ? where `i_company_id` = ?
            update `counter_real_finance` set `c_waybill_total` = `c_waybill_total` + *.*, `counter_real_finance`.`updated_at` = ? where `i_company_id` = ? and `i_oem_id` = ? and `d_month` = ?
            insert into `bill_log` (`d_money`, `t_belong`, `t_type`, `d_datetime`, `d_reason`, `t_audit`, `i_user_id`, `u_truename`, `u_audit_truename`, `i_company_id`, `i_group_id`, `i_oem_id`, `i_creat_company_id`, `u_operate_company_name`, `i_report_company_id`, `i_operate_company_id`, `i_store_id`, `t_module`, `i_module_id`, `d_remark`, `created_at`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            update `waybill` set `d_carriage_base` = ?, `d_carriage_num` = ?, `d_rate` = ?, `d_carriage_unit` = ?, `i_bill_log_id` = ?, `t_is_verify` = ?, `waybill`.`updated_at` = ? where `id` = ?
            insert into `waybill_log` (`i_waybill_id`, `d_action`, `i_order_id`, `i_platform_id`, `d_user_name`, `d_truename`, `i_user_id`, `i_company_id`, `i_group_id`, `i_oem_id`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
            update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
            select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
            update `store` set `d_last_auto_time` = ?, `store`.`updated_at` = ? where `id` = ?
            update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `waybill`.`updated_at` = ? where `id` = ?
            select `id`, `d_button_name` from `waybill` where ((`i_company_id` = ? and `i_operate_company_id` = ?) or `i_waybill_company_id` = ?) and `d_button_name` in (?, ?) order by `updated_at` asc limit *
            select count(*) as count, `i_store_id` from `sync_mws` where `t_state` in (?, ?) and `i_company_id` = ? and `i_oem_id` = ? group by `i_store_id`
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            update `store` set `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
            select `i_store_id` from `to_sell_task` where `i_company_id` = ? and `t_is_finish` = ? and `i_oem_id` = ?
            select * from `store` where `i_company_id` = ? and `t_flag` = ? and `t_is_del` = ? and `t_is_verify` = ?
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select `id`, `d_title`, `i_user_id`, `t_state` from `work_order` where `i_company_id` = ? and `t_state` in (?, ?) limit *
            select * from `task` where (`i_oem_id` = ? and `i_user_id` = ? and `i_group_id` = ? and `d_hash_code` = ?) limit *
            select count(*) as aggregate from `product` where (`d_hash_code` = ? and `i_company_id` = ?) and !(b_type&*) AND !(b_type&*) AND !(b_type&*)
            insert into `product` (`i_oem_id`, `i_company_id`, `i_user_id`, `i_group_id`, `i_task_id`, `u_lp_n`, `u_lp_k`, `u_lp_s`, `u_lp_d`, `d_source`, `d_hash_code`, `d_languages`, `b_language`, `b_type`, `u_price`, `i_platform_id`, `u_platform`, `d_attribute`, `d_variants`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select * from `product` where `id` = ? and b_type & * limit *
            insert into `temp_images` (`d_oss_path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `i_product_id`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?)
            update `sync_upload` set `d_image_id` = ?, `t_image_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
            insert into `sync_session_log` (`i_upload_id`, `d_index`, `d_state`, `i_company_id`, `i_user_id`, `i_group_id`, `i_oem_id`, `i_last_operate_id`, `d_count`, `d_server_old_state`, `d_client_old_state`, `d_invoke_api`, `d_index_text`, `d_state_text`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            insert into `sync_alive_session` (`i_upload_id`, `d_index`, `d_state`, `i_company_id`, `i_user_id`, `i_group_id`, `i_oem_id`, `i_last_operate_id`, `d_count`, `d_server_old_state`, `d_client_old_state`, `d_invoke_api`, `d_index_text`, `d_state_text`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select * from `sync_upload` where `id` = ? limit *
            insert into `sync_invoke_api_log` (`i_upload_id`, `d_request_data`, `i_upload_operate_id`, `i_user_id`, `i_company_id`, `i_group_id`, `i_oem_id`, `t_api`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `d_image_id`, `t_image_state`, `id`, `i_operate_id` from `sync_upload` where `id` = ? limit *
            update `sync_alive_session` set `d_state` = ?, `d_state_text` = ?, `i_last_operate_id` = ?, `i_user_id` = ?, `d_count` = ?, `sync_alive_session`.`updated_at` = ? where (`i_upload_id` = ? and `d_index` = ?)
            select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?, ?, ?)
            insert into `resource` (`created_at`, `d_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
            delete from `temp_images` where `id` in (?, ?, ?, ?)
            update `product` set `u_lp_k` = ?, `u_lp_s` = ?, `u_lp_d` = ?, `b_type` = ?, `u_price` = ?, `u_quantity` = ?, `d_source` = ?, `d_ext` = ?, `d_attribute` = ?, `d_variants` = ?, `d_images` = ?, `u_image` = ?, `d_pid` = ?, `d_pid_hash_code` = ?, `c_size` = ?, `d_coin` = ?, `d_rate` = ?, `product`.`updated_at` = ? where `id` = ?
            update `counter_company_business` set `c_report_product_num` = `c_report_product_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` in (?, ?, ?, ?)
            update `counter_user_business` set `c_product_num` = `c_product_num` + *, `counter_user_business`.`updated_at` = ? where `i_user_id` = ?
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            update `waybill` set `waybill`.`updated_at` = ? where `id` in (?, ?, ?, ?, ?)
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?)
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            update `sync_upload` set `d_relationship_id` = ?, `t_relationship_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
            select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?)
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select * from `product` where `id` = ? limit *
            select count(*) as aggregate from `product` where (`d_hash_code` = ? and `i_company_id` = ? and `id` != ?) and !(b_type&*) AND !(b_type&*) AND !(b_type&*)
            update `product` set `u_lp_d` = ?, `b_type` = ?, `u_price` = ?, `d_variants` = ?, `product`.`updated_at` = ? where `id` = ?
            insert into `reporter_request` (`created_at`, `d_action`, `d_cpu_time`, `d_method`, `d_module`, `d_name`, `d_namespace`, `d_net_time`, `d_request_id`, `d_request_time`, `d_uri`, `i_company_id`, `i_group_id`, `i_oem_id`, `i_user_id`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select count(*) as aggregate from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_oem_id` = ?) and `updated_at` between ? and ?
            select `id`, `i_oem_id`, `i_user_id`, `i_company_id`, `i_oem_id`, `i_category_id`, `i_platform_id`, `d_reason`, `u_quantity`, `u_lp_n`, `u_image`, `u_price`, `d_source`, `b_type`, `b_language`, `d_languages`, `updated_at`, `created_at`, `d_coin`, `d_rate` from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_oem_id` = ?) and `updated_at` between ? and ? order by `id` desc limit * offset *
            select * from `category` where `category`.`id` in (?)
            select * from `task` where `task`.`id` in (?)
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?, ?, ?, ?, ?)
            delete from `temp_images` where `id` in (?, ?, ?, ?, ?, ?)
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
            update `store` set `d_last_auto_update_time` = ?, `store`.`updated_at` = ? where `id` = ?
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            update `product` set `u_lp_n` = ?, `u_lp_k` = ?, `u_lp_s` = ?, `u_lp_d` = ?, `d_hash_code` = ?, `b_type` = ?, `u_price` = ?, `d_attribute` = ?, `d_variants` = ?, `d_rate` = ?, `product`.`updated_at` = ? where `id` = ?
            insert into `reporter_request` (`created_at`, `d_action`, `d_cpu_time`, `d_method`, `d_module`, `d_name`, `d_namespace`, `d_net_time`, `d_request_id`, `d_request_time`, `d_uri`, `i_company_id`, `i_group_id`, `i_oem_id`, `i_user_id`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select count(*) as aggregate from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_user_id` = ? and `i_oem_id` = ?) and `i_task_id` = ? and `updated_at` between ? and ?
            select `id`, `i_oem_id`, `i_user_id`, `i_company_id`, `i_oem_id`, `i_category_id`, `i_platform_id`, `d_reason`, `u_quantity`, `u_lp_n`, `u_image`, `u_price`, `d_source`, `b_type`, `b_language`, `d_languages`, `updated_at`, `created_at`, `d_coin`, `d_rate` from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_user_id` = ? and `i_oem_id` = ?) and `i_task_id` = ? and `updated_at` between ? and ? order by `id` desc limit * offset *
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?, ?, ?, ?)
            insert into `resource` (`created_at`, `d_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
            delete from `temp_images` where `id` in (?, ?, ?, ?, ?)
            ************************************************/
            return View();
        }
        #endregion
    }
}
