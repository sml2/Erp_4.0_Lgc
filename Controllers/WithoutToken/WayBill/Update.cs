﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.WayBill
{
    public class Update : Controller
    {
        ////原ERP3.0注释
        #region ERP3.0 PHP
        //    public const WaybillIsEmpty = 1;   //暂无运单信息
        //    const Database = 2; //更新失败
        //                        //570
        //    private $WayBillService;

        //public function __construct()
        //    {
        //    $this->WayBillService = new WayBillService();
        //    }

        //    public function getApiId()
        //    {
        //        return 570;
        //    }

        //    public function verifyParams()
        //    {
        //        return [
        //            'ID',
        //            'TrackNum',
        //            'TrackInfo',
        //            'TrackButtonName',
        //            'State'
        //        ];
        //    }

        //    public function handle($request, $userId, $rule, $session)
        //    {
        //    $info = $this->WayBillService->getInfoById($request['ID']);
        //        if (!$info) {
        //            return $this->fail(self::WaybillIsEmpty);
        //        }

        //    $updateData = [
        //        //            'tracking'      => $request['TrackNum'],
        //        'track_info'    => $request['TrackInfo'],
        //        'button_name'   => $request['TrackButtonName'],
        //        'waybill_state' => $request['State'],
        //    ];
        //    $request['TrackNum'] && $updateData['tracking'] = $request['TrackNum'];

        //    $request['TrackButtonName'] == '' && $updateData['is_verify'] = WayBillModel::ISVERIFY_YES;
        //    $tmp = $this->WayBillService->updateData($request['ID'], $updateData);

        //        if (checkStr($request['TrackNum']) && $request['TrackNum'] != $info['tracking']){
        //        $action = '运单追踪更新,面单追踪号【'. $info['tracking']. '】更新'. $request['TrackNum'];
        //        $this->WayBillService->addLog($info['order_id'], $info['id'], 1, $action);
        //        }

        //        if (!$tmp) {
        //            return $this->fail(self::Database);
        //        }
        //        return $this->success();
        //    }
        #endregion
        #region SQL       
        public IActionResult invoke(/*$request, $userId, $rule, $session*/)
        {
            /************************************************  
             ================/waybill/update
            select * from `waybill` where `waybill`.`id` = ? limit *
            update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `waybill`.`updated_at` = ? where `id` = ?
            select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
            update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
            select count(*) as count, `i_store_id` from `sync_mws` where `t_state` in (?, ?) and `i_company_id` = ? and `i_oem_id` = ? group by `i_store_id`
            select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
            update `store` set `d_last_auto_time` = ?, `store`.`updated_at` = ? where `id` = ?
            select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
            update `store` set `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
            select * from `store` where `i_company_id` = ? and `t_flag` = ? and `t_is_del` = ? and `t_is_verify` = ?
            ************************************************/
            return View();
        }
        #endregion
    }
}
