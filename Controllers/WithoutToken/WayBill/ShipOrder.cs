﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.ControllersWithoutToken.WayBill
{
    public class ShipOrder : Controller
    {
        ////原ERP3.0注释
        #region ERP3.0 PHP
        //    private ShipOrderService $service;

        //public function __construct()
        //    {
        //    $this->service = new ShipOrderService();
        //    }

        //    public function getApiId()
        //    {
        //        return 820;
        //    }

        //    const ShipOrderIsEmpty = 1; //待发货订单不存在

        //public function handle($request, $userId, $rule, $session)
        //{
        //$list = $this->service->getList();

        //$data = $list->each(function($item) {
        //        /** @var \App\Models\Logistics\ShipOrder $item  */
        //        if (!empty($item->order) && !is_null($item->order->delivery_num))
        //        {
        //        $item->SendNum = $item->order->delivery_num;
        //        $item->FailNum = $item->order->delivery_fail;
        //        }
        //        else
        //        {
        //        $item->SendNum = 0;
        //        $item->FailNum = 0;
        //        }

        //    $item->suffix = mt_rand(0, 9). $item->SendNum;
        //    $item->Currency = $item->declare_unit;

        //        unset($item->order);
        //    });
        //    if ($data->isEmpty()){
        //        return $this->fail(self::ShipOrderIsEmpty);
        //    }
        //    return $this->success($data);

        //    //测试模拟数据
        //    //        $arr = [
        //    //            [
        //    //                'id'               => 1,
        //    //                'order_id'         => 1,
        //    //                'order_no'         => '407-1679041-3293964',
        //    //                'nation_short'     => 'IT',
        //    //                'address'          => [
        //    //                    'phone'        => '0183780219',
        //    //                    'nation_id'    => '214',
        //    //                    'nation_short' => 'IT',
        //    //                    'nation'       => '意大利',
        //    //                    'province'     => 'Imperia',
        //    //                    'city'         => 'Imperia',
        //    //                    'district'     => '',
        //    //                    'address'      => 'Via Regina Pacis 18',
        //    //                    'address2'     => '',
        //    //                    'address3'     => '',
        //    //                    'zip'          => '18100',
        //    //                    'name'         => 'Domenico Puppo',
        //    //                    'email'        => 'hm6zczb83k180wx@marketplace.amazon.it'
        //    //                ],
        //    //                'declare'          => '10',
        //    //                'declare_zh'       => '中文申报名称',
        //    //                'declare_en'       => '英文申报名称',
        //    //                'customs_code'     => '海关编码',
        //    //                'description'      => '商品描述',
        //    //                'tag'              => '自定义标签',
        //    //                'battery'          => '1',
        //    //                'length'           => '10',
        //    //                'width'            => '10',
        //    //                'height'           => '10',
        //    //                'weight'           => '1000',
        //    //                'goods'            => [
        //    //                    [
        //    //                        'num'                   => 1,
        //    //                        'url'                   => 'https=>//m.media-amazon.com/images/I/51z1EdNzr5L._SL75_.jpg',
        //    //                        'asin'                  => 'B07Y8HJHJ5',
        //    //                        'name'                  => 'ZKZK Tovaglia Rotonda antimacchia',
        //    //                        'unit'                  => 'EUR',
        //    //                        'price'                 => 10,
        //    //                        'total'                 => 10,
        //    //                        'good_id'               => 'goodid5f40d5141f18c',
        //    //                        'delivery'              => 0,
        //    //                        'from_url'              => 'https=>//www.amazon.it/dp/B07Y8HJHJ5',
        //    //                        'send_num'              => 1,
        //    //                        'seller_sku'            => 'ZYY-ZB-C19',
        //    //                        'amazon_shipping_money' => 0,
        //    //                        'declare_zh'            => '中文申报名称',
        //    //                        'declare_en'            => '英文申报名称',
        //    //                        'description'           => '商品描述',
        //    //                        'tag'                   => '自定义标签',
        //    //                        'declare'               => '10'
        //    //                    ],
        //    //                    [
        //    //                        'num'                   => 1,
        //    //                        'url'                   => 'https=>//m.media-amazon.com/images/I/51z1EdNzr5L._SL75_.jpg',
        //    //                        'asin'                  => 'B07Y8HJHJ5',
        //    //                        'name'                  => 'ZKZK Tovaglia Rotonda antimacchia',
        //    //                        'unit'                  => 'EUR',
        //    //                        'price'                 => 10,
        //    //                        'total'                 => 10,
        //    //                        'good_id'               => 'goodid5f40d5141f18c',
        //    //                        'delivery'              => 0,
        //    //                        'from_url'              => 'https=>//www.amazon.it/dp/B07Y8HJHJ5',
        //    //                        'send_num'              => 1,
        //    //                        'seller_sku'            => 'ZYY-ZB-C19',
        //    //                        'amazon_shipping_money' => 0,
        //    //                        'declare_zh'            => '中文申报名称',
        //    //                        'declare_en'            => '英文申报名称',
        //    //                        'description'           => '商品描述',
        //    //                        'tag'                   => '自定义标签',
        //    //                        'declare'               => '10'
        //    //                    ]
        //    //                ],
        //    //                'ship_template_id' => 4
        //    //            ],
        //    //            [
        //    //                'id'               => 2,
        //    //                'order_id'         => 2,
        //    //                'order_no'         => '407-1679041-111',
        //    //                'nation_short'     => 'IT',
        //    //                'address'          => [
        //    //                    'phone'        => '0183780219',
        //    //                    'nation_id'    => '214',
        //    //                    'nation_short' => 'IT',
        //    //                    'nation'       => '意大利',
        //    //                    'province'     => 'Imperia',
        //    //                    'city'         => 'Imperia',
        //    //                    'district'     => '',
        //    //                    'address'      => 'Via Regina Pacis 18',
        //    //                    'address2'     => '',
        //    //                    'address3'     => '',
        //    //                    'zip'          => '18100',
        //    //                    'name'         => 'Domenico Puppo',
        //    //                    'email'        => 'hm6zczb83k180wx@marketplace.amazon.it'
        //    //                ],
        //    //                'declare'          => '10',
        //    //                'declare_zh'       => '中文申报名称',
        //    //                'declare_en'       => '英文申报名称',
        //    //                'customs_code'     => '海关编码',
        //    //                'description'      => '商品描述',
        //    //                'tag'              => '自定义标签',
        //    //                'battery'          => '1',
        //    //                'length'           => '10',
        //    //                'width'            => '10',
        //    //                'height'           => '10',
        //    //                'weight'           => '1000',
        //    //                'goods'            => [
        //    //                    [
        //    //                        'num'                   => 1,
        //    //                        'url'                   => 'https=>//m.media-amazon.com/images/I/51z1EdNzr5L._SL75_.jpg',
        //    //                        'asin'                  => 'B07Y8HJHJ5',
        //    //                        'name'                  => 'ZKZK Tovaglia Rotonda antimacchia',
        //    //                        'unit'                  => 'EUR',
        //    //                        'price'                 => 10,
        //    //                        'total'                 => 10,
        //    //                        'good_id'               => 'goodid5f40d5141f18c',
        //    //                        'delivery'              => 0,
        //    //                        'from_url'              => 'https=>//www.amazon.it/dp/B07Y8HJHJ5',
        //    //                        'send_num'              => 1,
        //    //                        'seller_sku'            => 'ZYY-ZB-C19',
        //    //                        'amazon_shipping_money' => 0,
        //    //                        'declare_zh'            => '中文申报名称',
        //    //                        'declare_en'            => '英文申报名称',
        //    //                        'description'           => '商品描述',
        //    //                        'tag'                   => '自定义标签',
        //    //                        'declare'               => '10'
        //    //                    ],
        //    //                    [
        //    //                        'num'                   => 1,
        //    //                        'url'                   => 'https=>//m.media-amazon.com/images/I/51z1EdNzr5L._SL75_.jpg',
        //    //                        'asin'                  => 'B07Y8HJHJ5',
        //    //                        'name'                  => 'ZKZK Tovaglia Rotonda antimacchia',
        //    //                        'unit'                  => 'EUR',
        //    //                        'price'                 => 10,
        //    //                        'total'                 => 10,
        //    //                        'good_id'               => 'goodid5f40d5141f18c',
        //    //                        'delivery'              => 0,
        //    //                        'from_url'              => 'https=>//www.amazon.it/dp/B07Y8HJHJ5',
        //    //                        'send_num'              => 1,
        //    //                        'seller_sku'            => 'ZYY-ZB-C19',
        //    //                        'amazon_shipping_money' => 0,
        //    //                        'declare_zh'            => '中文申报名称',
        //    //                        'declare_en'            => '英文申报名称',
        //    //                        'description'           => '商品描述',
        //    //                        'tag'                   => '自定义标签',
        //    //                        'declare'               => '10'
        //    //                    ]
        //    //                ],
        //    //                'ship_template_id' => 4
        //    //            ]
        //    //        ];
        //    //        return $this->success($arr);


        //}
        #endregion
    }
}
