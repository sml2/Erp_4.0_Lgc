using ERP.Extensions;
using ERP.Interface;
using Newtonsoft.Json.Linq;

namespace ERP.ControllersWithoutToken.User;

[ApiController]
public class CheckController : ControllerBase
{
    [Route("/client/check")]
    public string Check()
    {
        return HttpContext.Session.GetString("UserName") ?? string.Empty;
    }

    [Route("/client/download")]
    public async Task<IActionResult> Download([FromServices] IOemProvider oemProvider)
    {
        var oem = oemProvider.OEM!.ID;
        var repository = oem == 7 ? "mjt_client" : "crawler_client";
        var url = $"https://gitee.com/mi-archive/{repository}/releases/latest";
        var handler = new HttpClientHandler()
        {
            AllowAutoRedirect = true
        };
        var client = new HttpClient(handler);
        client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
        var resp = await client.GetStringAsync(url);
        JObject? json = null;
        try
        {
            json = JObject.Parse(resp);
        }
        catch { }

        if (json is null)
            return NotFound();

        var downloadUrl = json["release"]?["release"]?["attach_files"]?[0]?["download_url"];
        if (downloadUrl is null)
            return NotFound();

        return Redirect($"https://gitee.com{downloadUrl}");
    }
}
