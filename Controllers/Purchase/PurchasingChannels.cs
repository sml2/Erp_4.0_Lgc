﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.Controllers.Purchase;
using Service = Services.Purchase.PurchasingChannels;
using PurchasingChannelsModel = Models.DB.Purchase.PurchasingChannels;


public class PurchasingChannels : BaseController
{
    private readonly Service _service;
    public PurchasingChannels(Service service)
    {
        _service = service;
    }

    public async Task<ResultStruct> Index(string? name)
    {
        //$input = $request->validated();
        var res = await _service.List(name);
        return Success(res.Data);
    }

    public async Task<ResultStruct> Add(PurchasingChannelsModel vm)
    {
        //if (!$this->rules()->hasSupplierAdd()) {
        //    return error('无权访问');
        //}

        //$input = $request->validated();
        var res = await _service.Add(vm);
        return res.State ? Success("成功") : Error(res.Msg);
    }

    public async Task<ResultStruct> Edit(PurchasingChannelsModel vm)
    {
        //if (!$this->rules()->hasSupplierEdit()) {
        //    return error('无权访问');
        //}

        //$input = $request->validated();

        var res = await _service.Edit(vm);
        return res.State ? Success("成功") : Error(res.Msg);
    }

    public async Task<ResultStruct> Info(int id)
    {
        //$input = $request->validated();
        var res = await _service.Info(id);

        //return success('', new PurchasingChannelsResource($res['data']));
        return res.State ? Success("成功") : Error(res.Msg);
    }

    public async Task<ResultStruct> Delete(List<int> ids)
    {
        //if (!$this->rules()->hasSupplierDelete()) {
        //    return error('无权访问');
        //}

        //$input = $request->validated();

        var res = await _service.Delete(ids);
        return res.State ? Success("删除成功") : Error(res.Msg);
    }

    //        /**
    //         * 获取权限
    //         * @return JsonResponse
    //         */
    public async Task<ResultStruct> GetRule()//: JsonResponse
    {
        //    $biter = $this->rules();
        //            if ($this->sessionData->getConcierge() == UserModel::TYPE_OWN) {
        //        $data = [
        //            'add'    => true,
        //            'edit'   => true,
        //            'delete' => true,
        //        ];
        //            } else
        //            {
        //        $data = [
        //            'add'    => $biter->hasSupplierAdd(),
        //            'edit'   => $biter->hasSupplierEdit(),
        //            'delete' => $biter->hasSupplierDelete(),
        //        ];
        //            }

        //            return success('', $data);
        return await Task.FromResult(Success());
    }//end getRule()


    //        /**
    //         * 获取权限
    //         * @return \App\Rule\BitwiseFlags\Purchase
    //         */
    private ResultStruct rules()
    {
        //            return $this->sessionData->getRule()->getPurchase()->getBiter();
        return Success();
    }//end rules()
}

