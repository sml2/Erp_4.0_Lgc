using ERP.Authorization.Rule;
using ERP.Enums.Orders;
using ERP.Models.DB.Storage;
using ERP.Models.DB.Users;
using ERP.Services.DB.Statistics;
using ERP.Services.Finance;
using ERP.Services.Statistics;
using ERP.Storage.Abstraction.Services;
using ERP.ViewModels.Purchase;
using ERP.Services.Stores;
using Microsoft.AspNetCore.Identity;
using NPOI.OpenXmlFormats.Shared;
using Org.BouncyCastle.Ocsp;
using BillLog = ERP.Models.Finance.BillLog;

namespace ERP.Controllers.Purchase;

using Enums.Identity;
using Enums.Purchase;
using Enums.Rule.Menu;
using Extensions;
using Models.View.Purchase;
using Newtonsoft.Json;
using CompanyCache = Services.Caches.Company;
using CompanyModel = Company;
using OrderModel = Models.DB.Orders.Order;
using OrderService = Services.DB.Orders.Order;
using PurchaseModel = Models.DB.Purchase.Purchase;
using PurchaseService = Services.Purchase.Purchase;
using UnitCache = Services.Caches.Unit;

using ERP.Data;
using static ERP.Services.DB.Statistics.Statistic;

public class Purchase : BaseController
{
    private readonly PurchaseService _purchaseService;
    private readonly OrderService _orderService;
    private readonly IWarehouseService _warehouseService;
    private readonly CompanyCache _companyCache;
    private readonly UnitCache _unitCache;
    private readonly DBContext _dbContext;
    private readonly Statistic _statistic;
    private readonly StatisticMoney _statisticMoney;
    private readonly UserManager<Models.DB.Identity.User> _userManager;
    private readonly IStockService _stockService;
    private readonly StoreService _storeService;
    private readonly BillingRecordsService _billingRecordsService;
    private readonly ILogger<Purchase> _logger;


    public Purchase(
        PurchaseService purchaseService,
        OrderService orderService,
        IWarehouseService warehouseService,
        CompanyCache companyCache,
        UnitCache unitCache,
        StoreService storeService,
        DBContext dbContext, Statistic statistic,
        UserManager<Models.DB.Identity.User> userManager,
        IStockService stockService, BillingRecordsService billingRecordsService, ILogger<Purchase> logger)
    {
        _purchaseService = purchaseService;
        _orderService = orderService;
        _warehouseService = warehouseService;
        _companyCache = companyCache;
        _unitCache = unitCache;
        _dbContext = dbContext;
        _storeService = storeService;
        _statistic = statistic;
        _userManager = userManager;
        _stockService = stockService;
        _billingRecordsService = billingRecordsService;
        _logger = logger;
        _statisticMoney = new StatisticMoney(_statistic);
    }

    private ISession Session { get => HttpContext.Session; }

    private int UserID { get => Session.GetUserID(); }

    private int GroupID { get => Session.GetGroupID(); }

    private int CompanyID { get => Session.GetCompanyID(); }

    private int OEMID { get => Session.GetOEMID(); }

    #region order 调用

    #region 4.0 新增采购
    //[HttpPost, Permission(Enums.Rule.Config.Purchase.PURCHASE_ADD_CUSTOM_PURCHASE)]
    public async Task<ResultStruct> Create(PurchaseEditVM vm)
    {
        var user = await _userManager.GetUserAsync(User);
        if (vm.OrderId > 0)
        {
            var order = await _orderService.GetOrderByIdAsync(vm.OrderId);

            var Company = _companyCache.Get(Session.GetCompanyID());
            if (Company is null) return Error("Company is null");

            return await AddPurchase(vm, order, Company, user);
        }
        else
        {
            //直接手动添加采购信息(来自采购管理)
            var res = await _purchaseService.AddCustomPurchase(vm, user);
            return res.State ? Success(res.Data!, res.Msg) : Warning(res.Msg);
        }
    }
    #endregion

    #region 4.0 编辑采购
    /// <summary>
    /// 编辑采购
    /// </summary>
    /// <param name="vm"></param>
    /// <returns></returns>
    public async Task<ResultStruct> Edit(PurchaseEditVM vm)
    {
        var user = await _userManager.GetUserAsync(User);
        var purchase = await _purchaseService.GetPurchaseById(vm.ID);

        var tempUnit = vm.PurchaseUnit;
        var purchaseFee_old = purchase.PurchaseTotal;
        var num_old = purchase.Num ?? 0;

        purchase.PurchaseUrl = vm.PurchaseUrl;
        purchase.Num = vm.Num;
        purchase.PurchasePrice = purchase.PurchasePrice.Next(tempUnit, vm.PurchasePrice);
        purchase.ProductTotal = purchase.ProductTotal.Next(tempUnit, vm.ProductTotal);
        purchase.PurchaseOther = purchase.PurchaseOther.Next(tempUnit, vm.PurchaseOther);
        purchase.PurchaseTotal = purchase.PurchaseTotal.Next(tempUnit, vm.PurchaseTotal);

        purchase.PurchaseUnit = vm.PurchaseUnit;
        purchase.PurchaseOrderNo = vm.PurchaseOrderNo;
        purchase.PurchaseTrackingNumber = vm.PurchaseTrackingNumber;
        purchase.PurchaseState = vm.PurchaseState;
        purchase.Remark = vm.Remark;
        //purchase.PurchaseAffirm = vm.PurchaseAffirm;

        //仓库
        //purchase.IsStock = vm.IsStock ?? false;
        //purchase.WarehouseId = 0;
        //purchase.WarehouseName = string.Empty;

        var purchaseFee_new = purchase.PurchaseTotal;
        var num_new = purchase.Num ?? 0;

        var transaction = _dbContext.Database.BeginTransaction();
        try
        {
            #region 修改数据-采购总价统计/跟踪

            await _statisticMoney.PurchaseTotalEdit(purchase);

            #endregion

            #region 修改数据-采购商品单价跟踪

            await _statisticMoney.PurchasePriceEdit(purchase);

            #endregion

            #region 修改数据-采购商品总价跟踪

            await _statisticMoney.PurchaseProductTotalEdit(purchase);

            #endregion

            #region 修改数据-采购其他价格跟踪

            await _statisticMoney.PurchaseOtherEdit(purchase);

            #endregion
            
            if (vm.PurchaseState == PurchaseStates.PURCHASED)
            {
                var order = _dbContext.Order.Where(m => m.ID == vm.OrderId).First();
                var conpany = _dbContext.Company.Where(m => m.ID == purchase.CompanyID).First();
                //增加账单 自用 无需审核
                var msg = $"订单编号：【{order.OrderNo}】；";
                if (!string.IsNullOrEmpty(vm.PurchaseTrackingNumber))
                    msg += $"采购追踪号：【{vm.PurchaseTrackingNumber}】";
                //todo 采购财务
                // await _billingRecordsService.AddBillLogNoNeedReviewWithSelfUse(new AddBillLogDto()
                // {
                //     Money = purchase.PurchaseTotal.Money,
                //     Type = BillLog.Types.Deduction,
                //     Datetime = DateTime.Now,
                //     Reason = msg,
                //     Remark = "订单采购扣款",
                //     UserID = user.ID,
                //     GroupID = user.GroupID,
                //     CompanyID = user.CompanyID,
                //     Truename = user.TrueName,
                //     ModuleId = purchase.ID,
                //     StoreId = purchase.StoreId
                // }, conpany, BillLog.Modules.PURCHASE);
            }

            // 1.添加日志
            purchase.AddLog("编辑采购信息", user);
            // 2.编辑采购
            await _purchaseService.UpdatePurchase();
            // 3.非取消状态下 => 同步订单-采购
            if (purchase.PurchaseAffirm != PurchaseAffirms.Cancel)
            {
                await _orderService.UpdateOrderPurchase(purchase.OrderId??0, purchaseFee_old, purchaseFee_new, num_old, num_new);
            }

            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            return Error(ex.Message);
        }
        return Success();
    }
    #endregion

    #region 4.0 确认采购
    [Route("{id}")]
    public async Task<ResultStruct> Confirm(int id)
    {
        var user = await _userManager.GetUserAsync(User);
        var purchase = await _purchaseService.GetPurchaseById(id);
        if (purchase.PurchaseAffirm == PurchaseAffirms.CONFIRMED)
        {
            return Error("采购已确认");
        }
        var transaction = _dbContext.Database.BeginTransaction();
        try
        {
            // 取消变为确认 同步订单 
            if (purchase.PurchaseAffirm == PurchaseAffirms.Cancel)
            {
                #region 金额统计

                await _statisticMoney.InitStatisticWithPurchase(purchase);

                #endregion

                decimal purchaseFee_old = 0;
                var num_old = 0;

                var purchaseFee_new = purchase.PurchaseTotal;
                var num_new = purchase.Num ?? 0;
                if (purchase.OrderId > 0)
                    await _orderService.UpdateOrderPurchase(purchase.OrderId ?? 0, 0, purchaseFee_new, num_old, num_new);
            }

            // 更新采购
            purchase.PurchaseAffirm = PurchaseAffirms.CONFIRMED;
            purchase.AddLog("确认采购", user);
            await _purchaseService.UpdatePurchase(purchase);

            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            return Error(ex.Message);
        }
        return Success();
    }
    #endregion

    #region 4.0 取消采购
    [Route("{id}")]
    public async Task<ResultStruct> Cancel(int id)
    {
        var user = await _userManager.GetUserAsync(User);
        var purchase = await _purchaseService.GetPurchaseById(id);
        if (purchase.PurchaseAffirm == PurchaseAffirms.Cancel) return Error("采购已取消");

        var transaction = _dbContext.Database.BeginTransaction();
        try
        {
            purchase.PurchaseAffirm = PurchaseAffirms.Cancel;

            var purchaseFee_old = purchase.PurchaseTotal;
            var num_old = purchase.Num ?? 0;
            var purchaseFee_new = 0;// new MoneyMeta(purchase.PurchaseTotal.Raw.CurrencyCode, 0.00);
            var num_new = 0;
            // 更新采购
            purchase.AddLog("取消采购", user);

            // 采购财务 撤销
            await _statisticMoney.SetInvalid(purchase);
            // todo 是否数量-1

            await _purchaseService.UpdatePurchase(purchase);
            if (purchase.OrderId > 0)
                await _orderService.UpdateOrderPurchase(purchase.OrderId ?? 0, purchaseFee_old, purchaseFee_new, num_old, num_new);

            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            return Error(ex.Message);
        }
        return Success();
    }
    #endregion

    #region 添加新采购


    /// <summary>
    /// 添加新采购--关联订单的采购
    /// </summary>
    /// <param name="vm"></param>
    /// <param name="order"></param>
    /// <param name="company">订单所属公司</param>
    /// <param name="type">1内部添加 2分销上级添加</param>
    /// <returns></returns>
    private async Task<ResultStruct> AddPurchase(PurchaseEditVM vm, OrderModel order, CompanyModel company, Models.DB.Identity.User user, int type = 1)
    {
        //内部仓库
        if (vm.IsStock is true && vm.WarehouseId.HasValue)
        {
            var warehouse = await _warehouseService.GetWarehouseById(vm.WarehouseId.Value, user.CompanyID);
            if (warehouse is null)
            {
                return Error("仓库不存在");
            }
        }

        OrderModel.Good? good = new();
        //采购商品信息
        if (type == 1)
        {
            if (vm.PurchaseTrackingNumber is null) vm.PurchaseTrackingNumber = "";
            vm.PurchaseFee = 0;

            good = order.GoodsScope().FirstOrDefault(o => o.ID == vm.GoodId);
            if (good == null) return Error("订单中未包含此产品");
        }

        #region 分销上级添加 暂时忽略

        else
        {
            //var goodList = JsonConvert.DeserializeObject<List<AmazonOrderModel.OrderGood>>(order.Goods);
            var goodList = order.GoodsScope();
            good = goodList.FirstOrDefault(a => a.ID == vm.GoodId);
            if (good?.ID is null || good.ID == "")
            {
                return Warning("采购商品不存在");
            }

            vm.GoodId = good.ID;
            vm.Url = good.ImageURL;
            vm.Asin = good.ID;
            vm.Name = good.Name;
            
            //根据用户设置显示是order还是session
            if (true)
            {
                vm.Unit = user.OrderUnitConfig.Purchase ? vm.PurchaseUnit : Session.GetUnitConfig();
                vm.Price = good.TotalPrice.Money.To(vm.Unit);
            }
        }

        #endregion

        MoneyRecord money = new MoneyMeta(vm.PurchaseUnit, vm.PurchaseTotal);

        using var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            // 增加订单日志
            await _orderService.AddLog(order.ID, order.State, "更新订单采购信息");
            // 添加采购
            var purchase = await _purchaseService.InsertPurchase(vm, order, company, type, user);
            // 非取消状态下 => 同步订单-采购
            if (purchase.PurchaseAffirm != PurchaseAffirms.Cancel)
            {
                await _orderService.UpdateOrderPurchase(purchase.OrderId ?? 0,
                   0, purchase.PurchaseTotal, 0, purchase.Num ?? 0);
            }

            if (vm.PurchaseState == PurchaseStates.PURCHASED)
            {
                //增加账单 自用 无需审核
                var msg = $"订单编号：【{order.OrderNo}】；";
                if (!string.IsNullOrEmpty(vm.PurchaseTrackingNumber))
                    msg += $"采购追踪号：【{vm.PurchaseTrackingNumber}】";
                //todo 采购财务
                // var billLogId = await _billingRecordsService.AddBillLogNoNeedReviewWithSelfUse(new AddBillLogDto()
                // {
                //     Money = money.Money,
                //     Type = BillLog.Types.Deduction,
                //     Datetime = DateTime.Now,
                //     Reason = msg,
                //     Remark = "订单采购扣款",
                //     UserID = user.ID,
                //     GroupID = user.GroupID,
                //     CompanyID = user.CompanyID,
                //     Truename = user.TrueName,
                //     ModuleId = purchase.ID,
                //     StoreId = purchase.StoreId,
                // }, company, BillLog.Modules.PURCHASE);
                // purchase.BillLogId = billLogId;
                await _dbContext.Purchase.SingleUpdateAsync(purchase);
                await _dbContext.SaveChangesAsync();
            }
            
            await transaction.CommitAsync();
        }
        catch (Exception ex)
        {
            _logger.LogError(ex,ex.Message);
            await transaction.RollbackAsync();
            return Error(ex.Message);
        }
        return Success();
    }

    #endregion

    /// <summary>
    /// 获取货币单位cache
    /// </summary>
    /// <returns></returns>
    public ResultStruct GetUnit()
    {
        var unit = _unitCache.List();
        return Success(unit!);
    }

    /// <summary>
    /// 采购详情
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id:int}")]
    public async Task<ResultStruct> Info(int id)
    {
        var user = await _userManager.GetUserAsync(User);
        await _dbContext.Entry(user).Reference(u => u.OrderUnitConfig).LoadAsync();
        var unitInfo = user.OrderUnitConfig;

        var purchase = await _purchaseService.Info(id);
        // 产品库暂时不考虑 ???
        //$product = null;
        //if ($res['data']['product_id']) {
        //    $productRes = (new ProductService())->info($res['data']['product_id']);
        //    if ($productRes['state']) {
        //        $product = $productRes['data']['info'];
        //    }
        //}
        if(purchase.OrderId is null)
        {
            return Error("所属订单不存在！");
        }
        var order = await _orderService.GetOrderById((int)purchase.OrderId);
        if(order == null) return Error("所属订单不存在！");
        string orderUnit = order.OrderTotal.Raw.CurrencyCode;
        var info = new
        {
            purchase.ID,
            purchase.Name,
            purchase.Url,
            purchase.Asin,            
            purchase.Num,
            purchase.Unit,
            purchase.PurchaseUnit,
            purchase.PurchaseState,
            purchase.PurchaseAffirm,
            purchase.OrderNo,
            purchase.PurchaseOrderNo,
            purchase.PurchaseTrackingNumber,
            purchase.FindTruename,
            purchase.AffirmTruename,
            purchase.Remark,
            purchase.OrderId,
            Price = purchase.Price.Raw.Amount.ToString(),           
            ProductTotal = purchase.ProductTotal.Raw.Amount.ToString(),
            PurchasePrice = purchase.PurchasePrice.Raw.Amount.ToString(),
            PurchaseOther = purchase.PurchaseOther.Raw.Amount.ToString(),
            PurchaseTotal = purchase.PurchaseTotal.Raw.Amount.ToString(),
            goodIdText = ((Platforms)purchase?.PlatformId).GetGoodIdDescription()
        };
        var log = await _purchaseService.GetAllLog(id);
        return Success(new { info, log });
    }

    /// <summary>
    /// 获取绑定采购产品信息
    /// </summary>
    /// <param name="orderIds"></param>
    /// <returns></returns>
    public async Task<ResultStruct> GetAmazonGood(List<int> orderIds)
    {
        if (orderIds is null)
        {
            return Error("参数错误");
        }

        //获取选中订单信息
        var orders = await _orderService.GetAllOrderByIds(orderIds,null);
        if (orders is null)
        {
            return Error("订单不存在");
        }

        //获取选中订单下采购信息
        var purchase = await _purchaseService.GetPurchaseInfo(orderIds);
        //var x = MakePurchase(purchase);
        List<object> productList = new();
        foreach (var order in orders)
        {
            var res = await MakeProduct(order, purchase);
            if (res is not null)
                productList.AddRange(res);
        }

        return Success(new { product = productList, unit = _unitCache.List() });
    }

    /// <summary>
    /// 批量绑定采购
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public async Task<ResultStruct> BatchAdd(PurchaseEditVM data)
    {
        try
        {
            var user = await _userManager.GetUserAsync(User);
            var order = await _orderService.GetOrderByIdAsync(data.OrderId);
            if (order is null)
            {
                return Error("暂无数据");
            }

            if (!order.HasGoods())
            {
                return Error("暂无订单产品数据");
            }

            data.PurchaseState = PurchaseStates.INPROCUREMENT;

            var Company = _companyCache.Get(CompanyID);
            if (Company is not null)
            {
                return await AddPurchase(data, order, Company, user);
            }
        }
        catch (Exception ex)
        {
            return Error(ex.Message);
        }
        return Success();
    }

    #endregion

    //****************************************************************************order用到的方法

    /// <summary>
    /// 获取采购列表数据
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> Index([FromQuery] IndexVM request)
    {
        if (Session.Is(Role.Employee) /*&& !$this->rules()->hasPurchaseList()*/)
        {
            return Error("无权访问");
        }

        var store_id = request.StoreId > 0 ? request.StoreId : 0;
        List<int> store_ids = new();
        if (store_id == 0 &&
            Session.Is(Role.Employee) /*&& !$this->rules()->hasAmazonStore()*/)
        {
            //$store = $this->getStorePersonalCache();
            //员工无可允许查看店铺，返回空信息
            //$store_ids = array_column($store, 'id');
        }

        var purchaseList = await _purchaseService.GetPaginateList(request, store_id);
        // var list = purchaseList.Transform(a => new
        // {
        //     a.ID,
        //     a.OrderId,
        //     a.Url,
        //     a.Name,
        //     a.OrderNo,
        //     a.PurchaseOrderNo,
        //     a.PurchaseTrackingNumber,
        //     a.Num,
        //     a.PurchaseState,
        //     a.PurchaseAffirm,
        //     PurchaseStateText = a.PurchaseState.GetDescription(),
        //     PurchaseAffirmText = a.PurchaseAffirm.GetDescription(),
        //     a.StoreName,
        //     a.Remark,
        //     a.PurchaseCompanyId,
        //     a.CompanyID,
        //     a.CreatedAt,
        //     PurchasePrice = a.PurchasePrice.ToString(Session.GetUnitConfig()),
        //     //a.IsAffirm,
        //     a.OperateCompanyId,
        // });
        //获取对应订单备注信息
        var customMarks = await GetCustomMark(purchaseList.Items.Where(a => a.OrderId.HasValue).Select(a => a.OrderId!.Value).Distinct().ToList());

        return Success(new { list = purchaseList,customMarks });
    }
    
    private async Task<object?> GetCustomMark(List<int> orderIds) //: array
    {
        var res = await _orderService.GetOrdersByIdsAsync(orderIds);
        return res.Select(a => new
        {
            OrderId = a.ID,
            CustomMark =  a.GetCustomMark()
        }).ToDictionary(a => a.OrderId, a => a.CustomMark);
    }

    private async Task<object?> GetRemarks(List<int> orderIds) //: array
    {
        var res = await _orderService.GetOrderRemarks(orderIds);
        var x = res.Select(a => new
        {
            a.ID,
            a.OrderId,
            a.Value,
            a.Truename,
            a.CreatedAt
        }).GroupBy(a => a.OrderId).Select(g => new
        {
            orderId = g.Key,
            list = g.ToList()
        }).ToDictionary(a => a.orderId, a => a.list);
        return x;
    }

    /// <summary>
    /// 修改状态为无需找货
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> NoFindProduct(int id)
    {
        //if ($this->sessionData->getConcierge() == UserModel::TYPE_CHILD && !$this->rules()->hasPurchaseNoFindProduct()) {
        //    return error('无权访问');
        //}

        //$input = $request->validated();

        try
        {
            var res = await _purchaseService.NoFindProductPipeLine(id);
        }
        catch (Exception ex)
        {
            return Error(ex.Message, "修改失败");
        }

        return Success();
    }

    /// <summary>
    /// 采购确认 获取采购信息 只获取可以处理采购信息
    /// </summary>
    /// <param name="urlType"></param>
    /// <param name="type"></param>
    /// <param name="state"></param>
    /// <param name="number"></param>
    /// <param name="is_auto"></param>
    /// <returns></returns>
    [Permission(Enums.Rule.Config.Purchase.PURCHASE_CONFIRM)]
    public async Task<ResultStruct> GetPurchase(string urlType, int type, int state, string number, bool is_auto)
    {
        var purchase_order_no = "";
        var purchase_tracking_number = "";
        if (type == 1)
        {
            purchase_order_no = number;
        }
        else
        {
            purchase_tracking_number = number;
        }

        var list = await _purchaseService.GetPurchase(purchase_order_no, purchase_tracking_number);
        var msg = type == 1 ? "国内订单号" : "国内追踪号";
        if (list is null)
        {
            return Error($"当前{msg}【{number}】不存在");
        }

        var orderIds = list.Where(x => x.OrderId.HasValue).Select(x => x.OrderId!.Value).ToList();

        var data = new
        {
            order = await _orderService.GetMfnOrderByIds(orderIds)
            // 订单状态
            //$data['state'] = AmazonOrderModel::ORDER_SEARCH_STATE;
            //// 订单进度
            //$data['progress'] = AmazonOrderModel::ORDER_PROGRESS;
            //// 分销性质
            //$data['isDistribution'] = $this->sessionData->getIsDistribution();
            //// 当前公司
            //$data['company_id'] = $this->sessionData->getCompanyId();
        };
        if (!is_auto || list.Count > 1)
        {
            return Success(new { state = true, list = list, order = data });
        }

        var info = list[0];
        if (info.PurchaseState == PurchaseStates.INPROCUREMENT && info.PurchaseAffirm == PurchaseAffirms.UNCONFIRMED)
        {
            return Error($"当前{msg}【{number}】已拒收");
        }

        if (info.PurchaseState == PurchaseStates.PURCHASED)
        {
            return Success(new { state = false, list = list, order = data, msg = $"当前{msg}【{number}】已被签收" });
        }

        // 自动拒收`
        if (state == 2)
        {
            var boolres = await _purchaseService.Rejection(info.ID);
            return boolres ? Success(new { state = false, msg = "自动拒收成功" }) : Error("自动拒收失败");
        }

        //采购确认 完成采购
        var res = await SignFor(info, data.order[0]);
        if ((res.Code & Codes.Success) == Codes.Success)
        {
            return Success(new
            {
                state = false,
                list,
                order = data,
                msg = "自动签收成功",
            });
        }

        return Error(res.Message);
    }

    /// <summary>
    /// 采购确认 签收 拒绝
    /// </summary>
    /// <param name="urlType"></param>
    /// <param name="state"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    [Permission(Enums.Rule.Config.Purchase.PURCHASE_CONFIRM)]
    public async Task<ResultStruct> SubPurchase(string urlType, int state, int id)
    {
        // 拒签
        if (state == 2)
        {
            var res = await _purchaseService.Rejection(id);
            return res ? Success() : Error("拒收失败");
        }

        // 获取采购详情
        var info = await _purchaseService.GetPurchaseById(id);
        if (info is null)
        {
            return Error("采购信息不存在");
        }

        //采购确认 完成采购

        //检测订单
        var order = await _orderService.GetOrderInfo(info.OrderId);
        if (order is null) return Error("暂无数据");

        return await SignFor(info, order);
    }


    //    /**
    //     * 获取权限
    //     * @return     JsonResponse
    //     * @deprecated version
    //     */
    [HttpGet]
    public async Task<ResultStruct> GetRule()
    {
        //        $biter = $this->rules();
        //        if ($this->sessionData->getConcierge() == UserModel::TYPE_OWN) {
        //            $data = [
        //                'editInfo'          => true,
        //                'noFindProduct'     => true,
        //                'edit'              => true,
        //                'getPurchase'       => true,
        //                'subPurchase'       => true,
        //                'addCustomPurchase' => true,
        //            ];
        //        } else {
        //            $isDistribution = $this->sessionData->getIsDistribution() === User::DISTRIBUTION_TRUE;
        //            $data = [
        //                'editInfo'          => $biter->hasPurchaseEdit(),
        //                'noFindProduct'     => $biter->hasPurchaseNoFindProduct(),
        //                'edit'              => $biter->hasPurchaseEdit(),
        //                'getPurchase'       => $isDistribution ? $biter->hasPurchaseConfirm() : !($biter->getPurchaseConfirmRange() == 1),
        //                'addCustomPurchase' => $biter->hasPurchaseAddCustomPurchase(),
        //            ];
        //        }

        //        $company = $this->getCompany();

        //        $data['purchaseAudit'] = isset($company['purchase_audit']) ? ($company['purchase_audit'] === 1) : false;
        //        $data['is_distribution'] = $this->sessionData->getIsDistribution();

        var data = new
        {
            editInfo = true,
            noFindProduct = true,
            edit = true,
            getPurchase = true,
            addCustomPurchase = true,
            purchaseAudit = false,
        };
        return await Task.FromResult(Success(data));
    }

    [HttpGet]
    public async Task<ResultStruct> InitData([FromServices] Services.Identity.UserManager userManager)
    {
        var user = await userManager.GetUserAsync(User);
        var company = _companyCache.Get(CompanyID);
        var isDistribution = Convert.ToBoolean(Session.GetIsDistribution());
        var purchaseBack = false;
        var purchaseReport = false;
        if (isDistribution && company is not null)
        {
            switch (company.DistributionPurchase)
            {
                case CompanyModel.DistributionPurchases.TRUE:
                    purchaseBack = false;
                    purchaseReport = true;
                    break;
                case CompanyModel.DistributionPurchases.PORTION:
                    purchaseBack = true;
                    purchaseReport = true;
                    break;
                case CompanyModel.DistributionPurchases.FALSE:
                    purchaseBack = true;
                    purchaseReport = false;
                    break;
                default:
                    throw new Exception($"{nameof(company.DistributionPurchase)}错误");
            }
        }

        var rules = new
        {
            editInfo = true,
            noFindProduct = true,
            edit = true,
            getPurchase = true,
            addCustomPurchase = true,
            //分销自由切换上报状态
            purchaseBack,
            purchaseReport,
            SwitchDistribution = true,
            //批量绑定
            batchPurchase = true,

            purchaseAudit = company is not null ? company.PurchaseAudit == CompanyModel.PurchaseAudits.YES : false,
            isDistribution,
        };

        var store = (await _storeService.CurrentUserGetStores()).Select(a => new
        {
            a.ID,
            a.Name,
            a.State,
        }).ToList();

        var data = new
        {
            rules,
            store = store.ToDictionary(a => a.ID, a => a),
            companyId = CompanyID,
            progress = OrderModel.GetSupportsOrderProcess(),
            warehouse = await _warehouseService.AvailableWarehouses(user),
        };
        return Success(data);
    }


    /// <summary>
    /// 全部产品列表
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> ProductList()
    {
        // request()->merge(['action' => 'purchase']);
        //        return (new Product())->list();

        // 产品 controller 中的 list 方法
        return await Task.FromResult(Success("11"));
    }

    //    /**
    //     * 获取权限
    //     * @return \App\Rule\BitwiseFlags\Purchase
    //     */
    private ResultStruct rules()
    {
        //        return $this->sessionData->getRule()->getPurchase()->getBiter();
        return Success();
    }

    //    /**
    //     * 根据sku获取产品信息
    //     * @param $sku
    //     * @return JsonResponse
    //     */
    public async Task<ResultStruct> ProductDetail(string sku, string skus)
    {
        if (sku == "multi")
        {
            var skuList = skus.Split(",");
        }
        else
        {
            var skuList = new { sku };
        }

        //        $res = [];
        //        foreach ($skuList as $item) {
        //            验证sku前缀并返回产品id
        //            $productId = decodeSku($item);
        //            if ($productId) {
        //                $res[$item] = $productId;
        //            }
        //        }

        //        if (count($res) === 0) {
        //            return success('没有相关产品');
        //        }

        //        $productList = \App\Models\Product\Product::query()->whereIn('id', array_values($res))
        //            ->get(['id', 'variants', 'attribute']);
        //        if (count($productList) === 0) {
        //            return success('没有相关产品');
        //        }

        //        if ($sku === 'multi') {
        //            $productList = arrayCombine($productList->toArray(), 'id');
        //            $newRes = array_map(fn($item) => $productList[$item] ?? null, $res);
        //            return success('', $newRes);
        //        } else {
        //            return success('', $productList[0]);
        //        }
        return await Task.FromResult(Success());
    }



    //    /******************** 分销采购 start  ****************************/


    //    /**
    //     * 采购分销 上级添加
    //     * @param Request $request
    //     * @return JsonResponse
    //     * User: CCY
    //     * Date: 2020/10/20
    //     */
    public ResultStruct addDistributePurchase( /*Request $request*/)
    {
        //        if ($this->sessionData->getConcierge() == UserModel::TYPE_CHILD && !$this->ruleInfo()->hasAddPurchase()) {
        //            return error('无权访问');
        //        }
        //        $requestData = $request->validated();
        //        //检测订单
        //        $this->AmazonOrderService = new AmazonOrderService();
        //        $order = $this->AmazonOrderService->getOrderInfo($requestData['id']);
        //        if (!$order) {
        //            return error('暂无数据');
        //        }

        //        //下级公司信息
        //        $company = $this->getCompany($order['company_id']);
        //        //采购完成，检测钱包余额
        //        if ($company['is_negative'] == Company::NEGATIVE_FALSE && $requestData['info']['purchase_state'] == PurchaseModel::STATE_PURCHASED) {
        //            $company = $this->companyService->getCurrentConfig($order['company_id']);
        //            $total = FromToConversion($requestData['info']['purchase_total'], $requestData['info']['purchase_unit'], $this->sessionData->getUnitConfig());
        //            if (bcsub($company['public_wallet'], $total) < 0) {
        //                return error('当前余额不足，请先提醒对方充值钱包金额');
        //            }
        //        }
        //        $res = $this->addPurchase($requestData['info'], $order, $company, 2);

        //        return $res['state'] ? success() : error($res['msg']);
        return Success();
    }

    //    /**
    //     * 采购分销 上级编辑
    //     * @param Request $request
    //     * @return JsonResponse
    //     * @throws \Throwable
    //     * User: CCY
    //     * Date: 2020/10/27
    //     */
    public ResultStruct editDistributePurchase( /*Request $request*/)
    {
        //        if ($this->sessionData->getConcierge() == UserModel::TYPE_CHILD && !$this->ruleInfo()->hasEditPurchase()) {
        //            return error('无权访问');
        //        }
        //        $requestData = $request->validated();
        //        //采购详情
        //        $info = $this->service->getInfoById($requestData['id']);
        //        if (!$info) {
        //            return error('暂无数据');
        //        }

        //        if ($requestData['info']['purchase_state'] == PurchaseModel::STATE_PURCHASED) {
        //            //完成采购 未完成->已完成  已完成更新
        //            $res = $this->updateEndPurchase($requestData['info'], $info, 2);
        //        } else {
        //            //未完成采购
        //            $res = $this->updateNoEndPurchase($requestData['info'], $info, 2);
        //        }


        //        return $res['state'] ? success() : error($res['msg']);
        return Success();
    }

    /******************** 分销采购 end  ****************************/

    /******************** 分销、内部采购公用 start  ****************************/

    /// <summary>
    /// 采购签收
    /// </summary>
    /// <returns></returns>
    private async Task<ResultStruct> SignFor(PurchaseModel info, OrderModel order)
    {
        var user = await _userManager.GetUserAsync(User);
        Warehouse? warehouse = null;
        if (info.IsStock && info.WarehouseId.HasValue)
        {
            warehouse = await _warehouseService.GetWarehouseById(info.WarehouseId.Value, CompanyID);
            if (warehouse is null)
            {
                return Error("仓库不存在");
            }
        }

        var money = info.PurchaseTotal;
        var company = _companyCache.Get(info.CompanyID);
        if (company is null)
            throw new Exception("无法找到对应公司");
        //        DB::beginTransaction();
        try
        {
            //更新订单信息
            await _orderService.UpdateOrderByPurchase(order, info, money.Money, null!);
            //增加订单日志
            await _orderService.AddLog(order.ID, order.State, "更新订单采购信息");

            var wallet = info.GetWallet(company, user);
            var type = wallet.GetBelong();
            await wallet.Expense(money.Money);
            // TODO 添加财务
            //            $CounterUserBusinessService->purchaseTotalAdd($money);
            //            $this->sessionData->getConcierge() == UserModel::TYPE_CHILD && $CounterGroupBusiness->purchaseTotalAdd($money);
            //            $CounterStoreBusinessService->addPurchaseTotal($order['store_id'], $money);
            //            $CounterCompanyBusinessService->purchaseTotalAdd($money, $order['company_id']);
            //            //月报
            //            $CounterRealFinance->incrementPurchaseTotal($company, $money);

            var msg = $"订单编号：【{order.OrderNo}】";
            if (!string.IsNullOrEmpty(info.PurchaseTrackingNumber))
                msg += $"采购追踪号：【{info.PurchaseTrackingNumber}】";
            var log = wallet.AddLog(money.Money, false, BillLog.Modules.PURCHASE, "订单采购扣款", msg,info.StoreId,info.ID);
            info.BillLog = log;
            // 入库
            if (info.IsStock && info.StockProductId.HasValue && info.WarehouseId.HasValue && info.Num.HasValue)
            {
                var purchaseTrackingNumber = order.PurchaseTrackingNumber;
                // TODO 需要修改ui，改为在绑定仓库时创建库存产品，这里就可以直接使用库存产品id，而不是走判断不存在创建的逻辑
                await _stockService.In(info.StockProductId.Value, info.WarehouseId.Value, null, info.Num.Value, user, new StockExtData("采购添加",
                    purchaseTrackingNumber: info.PurchaseTrackingNumber, purchaseId: info.ID, orderNo: order.OrderNo));
            }
            info.PurchaseState = PurchaseStates.PURCHASED;
            info.PurchaseAffirm = PurchaseAffirms.CONFIRMED;
            //采购日志
            info.AddLog("确认采购信息", UserID, GroupID, Session.GetUserName(), Session.GetTrueName());
            //更新采购
            await _purchaseService.UpdatePurchase(info);

            //            DB::commit();
        }
        catch (Exception e)
        {
            //            DB::rollBack();
            return Error(e.Message);
        }
        return Success();
    }

    //    /******************** 分销、内部采购公用 end  ****************************/

    //    /**
    //     * 切换分销上报状态
    //     * @param Request $request
    //     * @return \Illuminate\Http\JsonResponse
    //     * User: CCY
    //     * Date: 2020/11/27
    //     */
    public async Task<ResultStruct> Switch( /*Request $request*/)
    {
        //        $id = $request->input('id');
        //        if (!$id) {
        //            return error(getlang('public.ParamError'));
        //        }

        //        $type = $request->input('type');
        //        if (!$type) {
        //            return error(getlang('public.ParamError'));
        //        }

        //        //采购详情
        //        $info = $this->service->getInfoById($id);
        //        if (!$info) {
        //            return error('采购信息不存在');
        //        }

        //        if ($type == 1) {
        //            $action = '采购上报上级处理';
        //            $updateData = ['purchase_company_id' => $this->sessionData->getReportId()];
        //        } else {
        //            $updateData = ['purchase_company_id' => 0];
        //            $action = '采购上报撤回内部处理';
        //        }

        //        DB::beginTransaction();
        //        try {
        //            $this->service->updateData($id, $updateData);
        //            //采购日志
        //            $this->service->addLog($info['order_id'], $info['id'], $action, $info['company_id'], $info['oem_id']);
        //            DB::commit();
        //        } catch (Exception $e) {
        //            DB::rollBack();
        //            return error(false, $e->getMessage());
        //        }
        //        return success();
        return Success();
    }


    //    /******************** 批量采购 start  ****************************/


    //    /**
    //     * 处理采购信息
    //     * @param $purchase
    //     * @return array
    //     * User: CCY
    //     * Date: 2021/2/1
    //     */
    private ResultStruct MakePurchase(List<PurchaseModel>? purchase)
    {
        //if (!checkArr($purchase))
        //{
        //    return [];
        //}

        //$arr = [];
        //foreach ($purchase as $value) {
        //    $arr[$value['order_id']][] = $value;
        //}
        //return $arr;
        return Success();
    }

    /// <summary>
    /// 获取订单产品
    /// </summary>
    /// <param name="order"></param>
    /// <param name="purchase"></param>
    /// <returns></returns>
    private async Task<List<object>?> MakeProduct(OrderModel order, List<PurchaseModel> purchase)
    {
        var products = order.GoodsScope();
        List<object> product = new();
        foreach (var value in products!)
        {
            var arr = new
            {
                purchaseUnit = "CNY",
                purchaseTotal = 0m,
                purchaseOther = 0m,
                purchasePrice = 0m,
                productTotal = 0m,
                purchaseUrl = "",
                purchaseOrderNo = "",
                purchaseTrackingNumber = "",
                remark = "",
                orderId = order.ID,
                orderNo = order.OrderNo,
                purchase = purchase.Count == 0
                    ? null
                    : GetProductPurchase(value, purchase.Where(a => a.OrderId == order.ID).ToList()),

                GoodId = value.ID,
                FromUrl = value.FromURL,
                SendNum = value.WaitForSendNum,
                SellerSku = value.Sku,
                value.Name,
                Total = value.TotalPrice.ToString(Session.GetUnitConfig(), false),
                //value.Unit,
                Url = value.ImageURL,
                Asin = value.ID,
                Delivery = value.QuantityShipped,
                Num = value.QuantityOrdered,
                Price = value.TotalPrice.ToString(Session.GetUnitConfig(), false),
                Unit = Session.GetUnitConfig()
                //shipping_tax
                //amazon_shipping_money
                //item_tax
            };
            product.Add(arr);
        }

        return await Task.FromResult(product);
    }

    //    /**
    //     * 订单产品绑定对应采购
    //     * @param $product
    //     * @param $purchase
    //     * @return array
    //     * User: CCY
    //     * Date: 2021/2/1
    //     */
    private static object GetProductPurchase(OrderModel.Good product, List<PurchaseModel> purchase)
    {
        List<object> arr = new();
        foreach (var value in purchase)
        {
            if (value.OrderGoodId is not null && product.ID == value.Asin)
            {
                arr.Add(new
                {
                    purchaseState = value.PurchaseState,
                    purchaseUnit = value.PurchaseUnit,
                    purchaseTotal = value.PurchaseTotal,
                    purchaseOrderNo = value.PurchaseOrderNo,
                    purchaseTrackingNumber = value.PurchaseTrackingNumber
                });
            }
        }

        return arr;
    }

    /// <summary>
    /// 获取回填采购信息
    /// </summary>
    /// <param name="rq"></param>
    /// <returns></returns>
    public async Task<ResultStruct> GetBatchPurchase(BatchPurchaseVM rq)
    {
        //$type = $request->input('type');
        //if (!$type) {
        //    return error('参数错误');
        //}
        if (rq.Type == null)
        {
            return Error("参数错误");
        }

        //if ($type == 1) {
        //            $orderIds = $request->input('orderIds');
        //    if (!checkArr($orderIds))
        //    {
        //        return error('参数错误');
        //    }
        //            $list = $this->service->getPurchaseByOrderIds($orderIds);
        //}
        if (rq.Type == 1)
        {
            if (rq.OrderIds == null || rq.OrderIds.Count == 0)
            {
                return Error("参数错误");
            }

            var list = await _purchaseService.GetPurchaseByOrderIdsWithInprocurement(rq.OrderIds);
            var data = list.Select(a => new
            {
                a.ID,
                a.Url,
                a.Name,
                a.OrderNo,
                PurchaseTotal = a.PurchaseTotal.ToString(Session.GetUnitConfig()),
                a.PurchaseTrackingNumber,
                a.PurchaseOrderNo,
                a.PurchaseState,
                a.FromUrl,
            });
            return Success(data);
        }

        //if ($type == 2) {
        //            $purchaseIds = $request->input('purchaseIds');
        //    if (!checkArr($purchaseIds))
        //    {
        //        return error('参数错误');
        //    }
        //            $list = $this->service->getPurchaseByIds($purchaseIds);
        //}
        if (rq.Type == 2)
        {
            if (rq.PurchaseIds == null || rq.PurchaseIds.Count == 0)
            {
                return Error("参数错误");
            }

            var list = await _purchaseService.GetPurchaseByIds(rq.PurchaseIds);
            var data = list.Select(a => new
            {
                a.ID,
                a.Url,
                a.Name,
                a.OrderNo,
                PurchaseTotal = a.PurchaseTotal.ToString(Session.GetUnitConfig()),
                a.PurchaseTrackingNumber,
                a.PurchaseOrderNo,
                a.PurchaseState,
                a.FromUrl,
            });
            return Success(data);
        }

        //return success('', $list);
        return Error("参数错误");
    }

    /// <summary>
    /// 批量回填采购信息
    /// </summary>
    /// <param name="rq"></param>
    /// <returns></returns>
    public async Task<ResultStruct> BatchEdit(BatchEditVM rq)
    {
        if (rq.Id == 0)
        {
            return Error("参数错误");
        }

        await _purchaseService.BatchEdit(rq);
        return Success();
    }
    ///******************** 批量采购 end  ****************************/



    //*****************************************************ERP3.0 应该不用了

    // 1.编辑采购逻辑
    // 1.1.[采购状态:完成]=>[1.1.1.未完成=>已完成]/[1.1.2.已完成]
    // 1.2 [采购状态:未完成]

    #region 1.编辑采购信息

    /// <summary>
    /// 编辑采购信息
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> EditInfo(PurchaseEditVM vm)
    {
        if (Session.Is(Role.Employee) /*&& !$this->rules()->hasPurchaseEdit()*/)
        {
            return Error("无权访问");
        }

        //采购详情
        // TODO 未验权
        var purchase = await _purchaseService.GetPurchaseById(vm.ID);

        if (vm.PurchaseState == PurchaseStates.PURCHASED)
        {
            // 完成采购 未完成->已完成  已完成更新
            return await UpdateEndPurchase(vm, purchase);
        }
        else
        {
            //未完成采购
            return await UpdateNoEndPurchase(vm, purchase);
        }
    }

    #endregion

    #region 1.1.编辑完成采购

    /// <summary>
    /// 编辑完成采购 未完成->已完成  已完成更新
    /// </summary>
    /// <param name="vm"></param>
    /// <param name="type"></param>
    /// <returns></returns>
    private async Task<ResultStruct> UpdateEndPurchase(PurchaseEditVM vm, PurchaseModel info, int type = 1)
    {
        // 采购详情
        if (info.PurchaseState == PurchaseStates.PURCHASED)
        {
            // 完成采购后编辑，差额汇总->只允许修改编号v
            return await UpdateEndToEndPurchase(vm, info, type);
        }
        else
        {
            // 未完成->完成采购  汇总
            return await UpdateToEndPurchase(vm, info, type);
        }
    }

    #endregion

    #region 1.1.1.未完成->完成采购

    /// <summary>
    /// 未完成->完成采购
    /// </summary>
    /// <param name="vm"></param>
    /// <param name="info"></param>
    /// <param name="type">1内部添加 2分销上级添加</param>
    /// <returns></returns>
    private async Task<ResultStruct> UpdateToEndPurchase(PurchaseEditVM vm, PurchaseModel info, int type = 1)
    {
        #region 内部仓库

        #endregion
        var order = info.OrderId.HasValue ? await _orderService.GetOrderInfo(info.OrderId) : null;
        var company = _companyCache.Get(info.CompanyID);
        Money money = new MoneyMeta(vm.PurchaseUnit, -vm.PurchaseTotal);

        var tempUnit = vm.PurchaseUnit;
        info.Num = vm.Num;
        // info.ProductTotal = new MoneyMeta(tempUnit, info.Price.Money * vm.Num);
        info.ProductTotal = info.ProductTotal.Next(tempUnit, info.Price.Money * vm.Num);
        info.PurchaseState = vm.PurchaseState;
        info.PurchaseUnit = vm.PurchaseUnit;

        info.PurchaseTotal = info.PurchaseTotal.Next(tempUnit, vm.PurchaseTotal);
        info.PurchaseOther = info.PurchaseOther.Next(tempUnit, vm.PurchaseOther);
        info.PurchasePrice = info.PurchasePrice.Next(tempUnit, vm.PurchaseOther);

        info.PurchaseOrderNo = vm.PurchaseOrderNo;
        info.PurchaseTrackingNumber = vm.PurchaseTrackingNumber;
        info.Remark = vm.Remark;
        info.PurchaseAffirm = vm.PurchaseAffirm;
        //info.PurchaseRate = 0; ???
        //info.PurchaseRateType = 

        info.IsStock = vm.IsStock ?? false;
        info.WarehouseId = 0;
        info.WarehouseName = string.Empty;
        info.OrderItemId = vm.OrderItemId;
        info.PurchaseUrl = vm.PurchaseUrl;

        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            #region 修改数据-采购总价统计/跟踪

            await _statisticMoney.PurchaseTotalEdit(info);

            #endregion

            #region 修改数据-采购商品单价跟踪

            await _statisticMoney.PurchasePriceEdit(info);

            #endregion


            #region 修改数据-采购其他费用跟踪

            await _statisticMoney.PurchaseOtherEdit(info);

            #endregion

            #region 修改数据-采购商品总价跟踪

            await _statisticMoney.PurchaseProductTotalEdit(info);

            #endregion
            
            //增加账单 
            var msg = $"订单编号：【{order!.OrderNo}】；";
            if (!string.IsNullOrEmpty(vm.PurchaseTrackingNumber))
                msg += $"采购追踪号：【{vm.PurchaseTrackingNumber}】";
            var belong = CompanyID == info.CompanyID ? BillLog.Belongs.SELFUSE : BillLog.Belongs.RIGHT;
            var user = await _userManager.GetUserAsync(User);
            //todo 采购财务
            // await _billingRecordsService.AddBillLogNoNeedReviewUnKnownBelong(new AddBillLogDto()
            // {
            //     Money = money,
            //     Type =  BillLog.Types.Deduction,
            //     Datetime = DateTime.Now,
            //     Reason = msg,
            //     Remark = "订单采购扣款",
            //     UserID = UserID,
            //     GroupID = GroupID,
            //     CompanyID = CompanyID,
            //     Truename = user.TrueName,
            //     ModuleId = info.ID,
            //     StoreId = info.StoreId
            // }, company, belong, BillLog.Modules.PURCHASE);


            if (order is not null)
            {
                //更新订单信息
                await _orderService.UpdateOrderByPurchase(order, info, money, vm);
                // 增加订单日志
                await _orderService.AddLog(order.ID, order.State, "更新订单采购信息");
            }

            //入库

            // 更新采购
            info.AddLog("完成采购信息", UserID, GroupID, Session.GetUserName(), Session.GetTrueName());
            await _purchaseService.UpdatePurchase(info);

            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            return Error(ex.Message);
        }

        return Success();
    }

    #endregion

    #region 1.1.2.差额汇总

    /// <summary>
    /// 差额汇总 ->只更新编号
    /// </summary>
    /// <param name="vm"></param>
    /// <param name="type"></param>
    /// <returns></returns>
    private async Task<ResultStruct> UpdateEndToEndPurchase(PurchaseEditVM vm, PurchaseModel info, int type = 1)
    {
        info.PurchaseOrderNo = vm.PurchaseOrderNo;
        info.PurchaseTrackingNumber = vm.PurchaseTrackingNumber;
        info.Remark = vm.Remark;

        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            // 更新采购
            info.AddLog("采购完成更改采购信息", UserID, GroupID, Session.GetUserName(), Session.GetTrueName());
            await _purchaseService.UpdatePurchase(info);
            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            return Error(ex.Message);
        }

        return Success();
    }

    #endregion

    #region 1.2.更新未完成采购信息

    /// <summary>
    /// 更新未完成采购信息
    /// </summary>
    /// <param name="vm"></param>
    /// <param name="type">1内部添加 2分销上级添加</param>
    /// <returns></returns>
    private async Task<ResultStruct> UpdateNoEndPurchase(PurchaseEditVM vm, PurchaseModel info, int type = 1)
    {
        var tempUnit = vm.PurchaseUnit;

        info.PurchaseUnit = vm.PurchaseUnit;

        info.Num = vm.Num;
        info.PurchasePrice = info.PurchasePrice.Next(tempUnit, vm.PurchasePrice);
        info.ProductTotal = info.ProductTotal.Next(tempUnit, vm.ProductTotal);
        info.PurchaseOther = info.PurchaseOther.Next(tempUnit, vm.PurchaseOther);
        info.PurchaseTotal = info.PurchaseTotal.Next(tempUnit, vm.PurchaseTotal);

        info.PurchaseOrderNo = vm.PurchaseOrderNo;
        info.PurchaseTrackingNumber = vm.PurchaseTrackingNumber;
        info.PurchaseState = vm.PurchaseState;
        info.Remark = vm.Remark;
        info.PurchaseAffirm = vm.PurchaseAffirm;

        info.IsStock = vm.IsStock ?? false;
        info.WarehouseId = 0;
        info.WarehouseName = string.Empty;

        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            #region 修改数据-采购总价统计/跟踪

            await _statisticMoney.PurchaseTotalEdit(info);

            #endregion


            #region 修改数据-采购商品单价跟踪

            await _statisticMoney.PurchasePriceEdit(info);

            #endregion

            #region 修改数据-采购商品总价跟踪

            await _statisticMoney.PurchaseProductTotalEdit(info);

            #endregion

            #region 修改数据-采购其他价格跟踪

            await _statisticMoney.PurchaseOtherEdit(info);

            #endregion

            //更新采购
            info.AddLog("更新采购信息", UserID, GroupID, Session.GetUserName(), Session.GetTrueName());
            await _purchaseService.UpdatePurchase(info);

            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            return Error(ex.Message);
        }

        return Success();
    }

    #endregion

    #region 添加采购

    /// <summary>
    /// 手动添加自定义采购
    /// </summary>
    /// <param name="vm"></param>
    /// <returns></returns>
    [HttpPost, Permission(Enums.Rule.Config.Purchase.PURCHASE_ADD_CUSTOM_PURCHASE)]
    public async Task<ResultStruct> AddCustomPurchase(PurchaseEditVM vm)
    {
        var user = await _userManager.GetUserAsync(User);
        if (vm.OrderId > 0)
        {
            // 订单中添加采购，合并(来自订单管理)
            // FIXME 检测订单, 没有验证订单权限
            var order = await _orderService.GetOrderByIdAsync(vm.OrderId);
            if (order is null) return Error("暂无数据");

            var Company = _companyCache.Get(Session.GetCompanyID());
            if (Company is null) return Error("Company is null");

            return await AddPurchase(vm, order, Company, user);
        }
        else
        {
            //直接手动添加采购信息(来自采购管理)
            var res = await _purchaseService.AddCustomPurchase(vm, user);
            return res.State ? Success(res.Data!, res.Msg) : Warning(res.Msg);
        }
    }

    #endregion
}