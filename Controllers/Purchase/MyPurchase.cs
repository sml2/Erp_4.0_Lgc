﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.Controllers.Purchase;
using Service = Services.Purchase.MyPurchase;

public class MyPurchase : BaseController
{
    private readonly Service _service;
    public MyPurchase(Service service)
    {
        _service = service;
    }

    [HttpGet]
    public async Task<ResultStruct> Index(Enums.Purchase.PurchaseStates? state)
    {
        var res = await _service.MyPurchaseList(state);
        return Success(res.Data);
    }
}

