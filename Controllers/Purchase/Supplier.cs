﻿using ERP.Controllers.Message;
using ERP.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace ERP.Controllers.Purchase;

using Service = Services.Purchase.Supplier;
using SupplierModel = Models.DB.Purchase.Supplier;

public class Supplier : BaseController
{
    private readonly Service _service;

    public record IndexQuery(int Page, int? Total, int Limit, string? Name);
    
    public Supplier(Service service)
    {
        _service = service;
    }

    [HttpGet]
    public async Task<ResultStruct> Index([FromQuery]IndexQuery query)
    {
        //$input = $request->validated();
        var res = await _service.SupplierList(query);
        return Success(res);
    }

    [HttpPost]
    public async Task<ResultStruct> Add(SupplierModel vm)
    {
        //if (!$this->rules()->hasSupplierAdd()) {
        //    return error('无权访问');
        //}
        var res = await _service.Add(vm);
        //$input = $request->validated();
        return res.State ? Success(res.Msg) : Error(res.Msg);
    }

    [HttpPost]
    public async Task<ResultStruct> Edit(SupplierModel vm)
    {
        //if (!$this->rules()->hasSupplierEdit()) {
        //    return error('无权访问');
        //}

        //$input = $request->validated();
        var res = await _service.Edit(vm);
        return res.State ? Success(res.Msg) : Error(res.Msg);
    }

    [HttpGet]
    [Route("{id}")]
    public async Task<ResultStruct> Info(int id)
    {
        //$input = $request->validated();
        var res = await _service.Info(id);
        return res.State ? Success(res.Data!, res.Msg) : Error(res.Msg);
    }

    [HttpPost]
    public async Task<ResultStruct> Delete(IdDto req)
    {
        //if (!$this->rules()->hasSupplierDelete()) {
        //    return error('无权访问');
        //}

        //$input = $request->validated();
        var res = await _service.Delete(req.ID);
        return res.State ? Success("删除成功") : Error(res.Msg);
    }


    //    /**
    //     * 获取权限
    //     *
    //     * @return JsonResponse
    //     */
    [HttpGet]
    public async Task<ResultStruct> GetRule() //: JsonResponse
    {
        //        $biter = $this->rules();
        //        if ($this->sessionData->getConcierge() === UserModel::TYPE_OWN) {
        //            $data = [
        //                'add'    => true,
        //                'edit'   => true,
        //                'delete' => true,
        //            ];
        //        } else
        //        {
        //            $data = [
        //                'add'    => $biter->hasSupplierAdd(),
        //                'edit'   => $biter->hasSupplierEdit(),
        //                'delete' => $biter->hasSupplierDelete(),
        //            ];
        //        }

        //        return success('', $data);
        return await Task.FromResult(Success());
    } //end getRule()


    //    /**
    //     * 获取权限
    //     *
    //     * @return \App\Rule\BitwiseFlags\Purchase
    //     */
    private ResultStruct rules()
    {
        //        return $this->sessionData->getRule()->getPurchase()->getBiter();
        return Success();
    } //end rules()
}