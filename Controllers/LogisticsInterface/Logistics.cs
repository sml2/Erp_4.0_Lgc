using ERP.Models.Api.Logistics;

using logisticsFactory = ERP.Services.Api.Logistics.Factory;
using logInfo = ERP.Services.Caches.Logistics;
using static ERP.Models.DB.Orders.Order;
using Model = ERP.Models.DB.Logistics.Waybill;
using WaybillService = ERP.Services.DB.Logistics.WayBill;
using CompanyService = ERP.Services.DB.Users.CompanyService;
using OrderService = ERP.Services.DB.Orders.Order;
using static ERP.Models.DB.Users.Company;
using ERP.Interface;
using ERP.Extensions;
using OperateService = ERP.Services.DB.Logistics.OperationLogging;
using Newtonsoft.Json;
using System.Text;
using ERP.Enums.Finance;
using ERP.Services.DB.Statistics;
using ERP.ViewModels.Logistics;
using ERP.Data;
using ShipTypeService = ERP.Services.DB.Logistics.ShipType;
using ERP.Enums.Orders;
using ERP.Models.View.Order.AmazonOrder;
using ERP.Enums.Logistics;
using ERP.Services.Statistics;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using ERP.Models.Api.Logistics.YH;
using Coravel.Events.Interfaces;
using Coravel.Queuing.Broadcast;
using Coravel.Queuing.Interfaces;
using ERP.Services.Host;
using OrderHostService = ERP.Services.DB.Orders.OrderHost;
using Statistic = ERP.Services.DB.Statistics.Statistic;
using ERP.Services.Provider;
using System.Security.Policy;
using ERP.Models.DB.Orders;
using Microsoft.AspNetCore.Mvc;
using ERP.Models.Api.Logistics.EMS;
using Aop.Api.Domain;
using ERP.Exceptions.Logistics;
using ERP.Services.DB.Orders;
using mergeService = ERP.Services.DB.Orders.Merge;
using ERP.ViewModels.Order.Merge;
using ERP.Models.View.Products.Product;
using OrderModel =ERP.Models.DB.Orders.Order;
using ERP.Models.DB.Logistics;
using ERP.Models.View.Logistics.WayBill;
using NPOI.SS.Formula.Functions;
using System.Diagnostics;
using ERP.Models.Finance;
using ERP.Models.View.User;
using ERP.Services.DB.Logistics;
using OrderSDK.Modles.Amazon.Authorization;
using static ERP.Models.Finance.BillLog;
using LogisticsParamService = ERP.Services.DB.Logistics.LogisticsParam;

namespace ERP.Controllers.LogisticsInterface
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class Logistics : BaseController
    {
        private logisticsFactory logistics;
        private readonly ILogger<Logistics> logger;

        private readonly DBContext Mydbcontext;
        private logInfo logisticsinfo;
        private readonly ISessionProvider _ISessionProvider;

        private readonly WaybillService waybillService;
        private readonly OrderService orderService;
        private readonly mergeService _mergeService;
        private readonly CompanyService companyService;
        private readonly OperateService operateService;
        private readonly Statistic _statisticService;
        private readonly ShipTypeService shipTypeService;
        private readonly UserManager<Models.DB.Identity.User> _userManager;
        private readonly StatisticMoney _statisticMoney;
        private readonly OrderHostService _orderHostService;
        private readonly WaybillFeeHost _waybillFeeHost;

        private readonly LogisticsParamService logisticsParamService;
        private readonly ShippingFeeService _shippingFeeService;

        public Logistics(mergeService mergeService, logisticsFactory _logistics,
             UserManager<Models.DB.Identity.User> userManager,
             logInfo _logisticsinfo, DBContext _Mydbcontext, ILogger<Logistics> _logger,
             ISessionProvider sessionProvider, WaybillService _waybillService, OrderService _orderService, CompanyService _companyService,
             OperateService _operateService, Statistic statisticService, ShipTypeService _shipTypeService, OrderHostService orderHostService, LogisticsParamService _logisticsParamService, ShippingFeeService shippingFeeService, WaybillFeeHost waybillFeeHost)
        {
            _mergeService = mergeService;
            logistics = _logistics;
            logisticsinfo = _logisticsinfo;
            Mydbcontext = _Mydbcontext;
            logger = _logger;
            _ISessionProvider = sessionProvider;
            waybillService = _waybillService;
            orderService = _orderService;
            companyService = _companyService;
            operateService = _operateService;
            _statisticService = statisticService;
            shipTypeService = _shipTypeService;
            _userManager = userManager;
            _statisticMoney = new StatisticMoney(_statisticService);
            _orderHostService = orderHostService;
            logisticsParamService = _logisticsParamService;
            _shippingFeeService = shippingFeeService;
            _waybillFeeHost = waybillFeeHost;
        }
        
        private ISession Session { get => _ISessionProvider.Session!; }
        private int CompanyId => _ISessionProvider.Session!.GetCompanyID();
        private int OemId => _ISessionProvider.Session!.GetOEMID();
        private int UserId => _ISessionProvider.Session!.GetUserID();
        private int GroupId => _ISessionProvider.Session!.GetGroupID();


        /// <summary>
        /// 加载承运人
        /// </summary>
        /// <param name="Country"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultStruct LoadCarrierName(CarrierNameOfCountry country)
        {
            if (country.Country.IsNotWhiteSpace() && CarrierInfo.DicCarrierName.ContainsKey(country.Country!))
            {
                return Success(CarrierInfo.DicCarrierName[country.Country!].ToArray());
            }
            else
            {
                List<string> AllCarrier = new List<string>();
                CarrierInfo.DicCarrierName.Values.ForEach(r => AllCarrier.AddRange(r));
                return Success(AllCarrier.Distinct().ToArray());
            }
        }

        [HttpPost]
        public async Task<ResultStruct> LoadChannelCode(BaseParameter baseParameter)
        {
            string resultError = "";
            var result = await logistics.LoadChannelCode(baseParameter);
            if (!result.Success)
            {
                resultError = result.exception.Message;
            }
            return Success(new
            {
                success = result.Success,
                errors = resultError,
                results = result.CommonResults,
            });
        }

        [HttpPost]
        public async Task<ResultStruct> GetWarehouseCode(BaseParameter baseParameter)
        {
            List<CommonResult> results = new List<CommonResult>();
            string resultErrors = "";
            var result = await logistics.GetWarehouseCode(baseParameter);
            if (!result.Success)
            {
                resultErrors = result.exception.Message;
            }
            return Success(new
            {
                success = result.Success,
                errors = resultErrors,
                results = result.CommonResults,
            });
        }


        [HttpPost]
        public async Task<ResultStruct> GetCounties(BaseParameter baseParameter)
        {
            List<string> resultErrors = new List<string>();
            var result = await logistics.GetCounties(baseParameter);
            if (!result.Success)
            {
                resultErrors.Add(result.exception.Message);
            }
            return Success(new
            {
                success = result.Success,
                errors = resultErrors,
                Departures = result.Departure,
                Destinations = result.Destination,
            });
        }

        /// <summary>
        /// 获取货运方式
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="logName"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResultStruct> GetShipTypes(ShipTypeParameter shipTypeParameter)
        {
            string resultError = "";
            var result = await logistics.GetShipTypes(shipTypeParameter);
            if (!result.Success)
            {
                if (result.exception.GetType() == typeof(Exceptions.Logistics.UnKnownError))
                {
                    resultError = "未查到货运方式";
                }
                else
                {
                    resultError = result.exception.Message;
                }
            }
            else
            {
                using var transaction = Mydbcontext.Database.BeginTransaction();
                try
                {
                    //入库
                    await shipTypeService.AddShipTypesLog(shipTypeParameter.Parameter, shipTypeParameter.LogName, result.CommonResults.Count(), JsonConvert.SerializeObject(result.CommonResults));
                    await shipTypeService.AddShipTypes(shipTypeParameter.Parameter, shipTypeParameter.LogName, result.CommonResults);
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    logger.LogError("GetShipTypes Insert DB:" + e.Message);
                }
            }
            return Success(new
            {
                success = result.Success,
                error = resultError,
                results = result.CommonResults,
            });
        }


        [HttpPost]
        public async Task<ResultStruct> CancleOrder(CancleWayBillParam wayBill)
        {            
            var waybill = await waybillService.GetInfoById(wayBill.ID);
            if (waybill.IsNull())
            {
                return Error("运单查询失败");
            }
            //增加运单日志
            var user = await _userManager.GetUserAsync(User);

            waybill.AddLog(user, "取消运单操作");

            var msg =await  waybillService.CancleWaybill(waybill, user);
            if(!string.IsNullOrWhiteSpace(msg))
            {
                return Error(msg);
            }
            //调用取消接口
            using var transaction = Mydbcontext.Database.BeginTransaction();
            try
            {
                if (waybill.Logistic.Platform.IsNotNull() && waybill.Logistic.Platform != Platforms.None) //非报备发货，属于自动、手动发货
                {
                    string resultError = "";
                    BaseParameter parameter = new BaseParameter(waybill.Logistic.Platform, waybill.LogisticExt);
                    var methods = await logistics.methodDefineds(parameter);
                    if (methods.PlatformMethod.CancleEnabled) //接口有取消方法
                    {
                        CancleParameter cancleParameter = new CancleParameter(waybill.ID, waybill.WaybillOrder, waybill.Logistic.Platform, waybill.LogisticExt);
                        var result = await logistics.CancleOrder(cancleParameter);

                        //request                                             
                        var cancledata = new
                        {
                            LogName = waybill.Logistic.Platform.GetDescription(),
                            Parameter = parameter,
                            wayBillID = waybill.ID,
                        };
                        string cancleRequest = JsonConvert.SerializeObject(cancledata);
                        string cancleResultStr = JsonConvert.SerializeObject(result);
                        await operateService.UpdateCancleInfo(wayBill.ID, cancleRequest, cancleResultStr, result.Response, result.Success);
                        if (!result.Success)
                        {
                            resultError = result.exception.Message;
                            return Error(resultError);
                        }
                    }
                }
                //更新waybill状态                                      
                waybill.State = Enums.Logistics.States.Cancle;
                Mydbcontext.Waybill.Update(waybill);
                await Mydbcontext.SaveChangesAsync();
                transaction.Commit();
                return Success();
            }

            catch (Exception e)
            {
                transaction.Rollback();
                logger.LogError("取消运单出现异常" + e.Message);
                return Error("取消运单失败");
            }
        }





        //public async Task<bool> UpdateFail(int orderID)
        //{
        //    using var transactionNum = Mydbcontext.Database.BeginTransaction();
        //    try
        //    {
        //        orderService.UpdateDeliveryFail(orderID);
        //        transactionNum.Commit();
        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        transactionNum.Rollback();
        //        return false;
        //    }
        //}


        /// <summary>
        /// 合并订单自动发货/手动发货
        /// </summary>
        /// <param name="sendParameter"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResultStruct> Send(SendParameter sendParameter)
        {
            logger.LogInformation($"Logistics Send Request:"+JsonConvert.SerializeObject(sendParameter));
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            int waybillID = 0;
            string code = "";
            List<OrderModel> orders = new List<OrderModel>();
            List<CommonResult> resultErrors = new List<CommonResult>();
            List<string> results = new List<string>();

            var orderParam = sendParameter.OrderInfos.First(); //目前业务而已，只有一个订单信息

            if (sendParameter.MergeID>0)  //合并订单发货
            {
                var result = await orderService.GetOrdersByMergeID(sendParameter.MergeID);
                if (!string.IsNullOrWhiteSpace(result.Item1))
                {
                    return Error(result.Item1);
                }
                orders.AddRange(result.Item2!);
            }
            else
            {
                var orderInfo = await orderService.GetOrderByIdAsync(orderParam!.id);
                if (orderInfo is null)
                {
                    return Error("订单信息没有找到！");
                }
                orders.Add(orderInfo);
            }
            
            var exampleOrder = orders!.FirstOrDefault(x => x.ID == orderParam.id);
            if (exampleOrder is null)
            {
                return Error("订单信息没有找到！");
            }
            //0、获取所属分销公司ID
            int waybillCompanyID = 0;
            if (exampleOrder.WaybillCompanyId != 0)
            {
                waybillCompanyID = exampleOrder.WaybillCompanyId;
            }
            else
            {
                waybillCompanyID = logisticsParamService.GetWaybillCompanyIDByLogisticParamID(orderParam.PlatformId);
            }
            //获取订单所属公司
            var orderCompany = await companyService.GetInfoById(exampleOrder.CompanyID);
            //是否分销
            var isDistribution = waybillCompanyID != 0 && orderCompany.IsDistribution;
            //是否是自动发货
            var isAuto = sendParameter.Type == SendType.Auto;
            
            Money freeze = 0;
            var user = await _userManager.GetUserAsync(User);
            var freezeWallet = new FreezeWaybill(orderCompany, user);// 预冻结              
            //1.如果运单存在上报，说明是分销 如果不允许为负小于等于0不允许创建
            if (isDistribution)
            {
                //钱包没钱，且不能为负
                if (!orderCompany.IsNegative && orderCompany.PublicWallet.Money <= 0)
                {
                    return Error("钱包余额不足");
                }
                //钱包有钱，自动发货且设置了冻结
                if (orderCompany.PublicWalletCanFreeze && isAuto)
                {
                    var isEnough = freezeWallet.IsEnough(0);
                    if (!isEnough)
                    {
                        return Error("钱包余额不足！");
                    }
                    freeze = freezeWallet.GetFreeze();

                    if (freeze > 0)
                    {
                        //扣除预冻结款
                        await freezeWallet.Expense(freeze);
                    }
                   
                }
            }


            if (isAuto)
            {
                //添加授权码字符
                code = await companyService.GetrefererCode();
                exampleOrder.DeliveryFail += 1;  //设置标记，推送物流接口不重复
                orderParam.ReceiverAddress.GetReceiver(exampleOrder.Receiver); //取收件人信息
                orderParam.SendNum = exampleOrder.DeliverySuccess;
                orderParam.FailNum = exampleOrder.DeliveryFail;
            }                              
            //根据orderid，补齐orderinfo参数           
            orderParam.Url = exampleOrder.Url;            
            orderParam.OrderNo = code+"_" + exampleOrder.OrderNo;

            stopwatch.Stop();
            logger.LogInformation("Send time:构造参数 " + stopwatch.ElapsedMilliseconds + "毫秒");
            ReportResultList response = new ReportResultList();
            using var transaction = Mydbcontext.Database.BeginTransaction();
            try
            {
                stopwatch.Start();
                if (isAuto)
                {
                     response = await logistics.Send(sendParameter);
                }
                else
                {
                     response = await logistics.SendReport(sendParameter);
                }
                stopwatch.Stop();
                logger.LogInformation("Send time:调用物流接口  " + stopwatch.ElapsedMilliseconds + "毫秒");

                //分析物流接口返回结果              
                if (response.ReportResults.IsNotNull() && response.ReportResults.Count > 0)
                {
                   
                    foreach (var report in response.ReportResults)
                    {
                        if (report.Success)
                        {
                            Model waybill = new();
                            var orderLst = orderParam.GoodInfos.Select(x => x.OrderId).ToList();                           
                            try
                            {
                                stopwatch.Start();
                                //1.添加运单
                                waybill = await waybillService.InsertWaybill(orderCompany,exampleOrder, sendParameter.LogName, report, orderParam, sendParameter.Parameter, sendParameter.SenderInfo, waybillCompanyID, sendParameter.MergeID, string.Join(",", orderLst),freeze);
                                waybillID=waybill.ID;
                                
                                //自动订单需要冻结
                                if (isDistribution && freeze > 0 && isAuto)
                                {
                                     string s = $"订单编号：【{waybill.OrderNo}】；物流运单号：【{waybill.Express}】";  
                                     await Mydbcontext.BillLog.AddAsync(freezeWallet.AddLog(freeze, false , Modules.WAYBILL, "运单预付款冻结", s, waybill.StoreId, waybill.ID));
                                     await Mydbcontext.SaveChangesAsync();
                                }

                                //2.根据goods，循环修改合并订单中的数量
                                //更新  减待发货数量，添加已发货数量（json&字段），添加总运单价格(order表)                               
                                foreach (var order in orders)//循环合并订单
                                {                                    
                                    var goodsInfo = orderParam.GoodInfos.Where(x => x.OrderId == order.ID).ToList(); //找出合并订单中，修改了的订单商品信息
                                    if (goodsInfo is not null && goodsInfo.Count > 0)
                                    {                                       
                                        orderService.CountGoods(order!, goodsInfo, sendParameter.Type);
                                    }

                                    if (orderLst.Any(x => x == order.ID))
                                    {
                                        //4.添加运单日志
                                        await waybillService.AddLog(order.ID, waybill.ID, order.Plateform, "增加订单运单信息",new UserInfoDto(user));
                                    }
                                }
                                //5.
                                if (sendParameter.DistributedUserId is > 0)
                                {
                                    await DistributeOrderToUser(new int[] { orderParam.id }, (int)sendParameter.DistributedUserId);
                                }

                                stopwatch.Stop();
                                logger.LogInformation("Send time:添加运单 " + stopwatch.ElapsedMilliseconds + "毫秒");
                            }
                            catch (Exception e)
                            {
                                await transaction.RollbackAsync();
                                logger.LogError($"Send 添加合并运单失败,{e.Message}");
                                logger.LogError($"Send 添加合并运单失败,{e.StackTrace}");
                                // resultErrors.Add(new CommonResult(report.CustomNumber, "添加运单失败"));
                                resultErrors.Add(new CommonResult(report.CustomNumber, e.Message));
                                return Success(new
                                {
                                    errors = resultErrors,
                                    results = results,
                                });
                            }

                            stopwatch.Start();
                            //6.同步订单信息，进度
                            if (waybill.IsVerify != VerifyStates.Cancel)
                            {
                                await orderService.UpdateMergeOrderWaybill(sendParameter.MergeID, sendParameter.OrderProgress, exampleOrder.ID);
                            }

                            stopwatch.Stop();
                            logger.LogInformation("Send time:同步订单 " + stopwatch.ElapsedMilliseconds + "毫秒");

                            stopwatch.Start();
                            //7.同步亚马逊                                
                            if (sendParameter.SyncAmazon.IsSync)
                            {
                                if (report.OrderNum.IsNullOrWhiteSpace())
                                {
                                    logger.LogError($"Send 运单号为空，同步亚马逊失败");
                                    resultErrors.Add(new CommonResult(report.CustomNumber, "运单号为空，同步亚马逊失败！"));
                                    continue;
                                }

                                foreach (var id in orderLst)
                                {                                   
                                    var values = await orderService.SyncAmazon(id, sendParameter.SyncAmazon, report.OrderNum, report.TrackNum,true);
                                    if (!string.IsNullOrWhiteSpace(values))
                                    {
                                        return Error(values);
                                    }                                    
                                }
                            }
                            results.Add(report.CustomNumber);                           
                        }
                        else
                        {
                            //分析错误类型
                            string str = "";
                            if (report.exception.GetType() == typeof(Exceptions.Logistics.UnKnownError))
                            {
                                str = "发送运单失败!" + report.exception.Message;
                            }
                            else if (report.exception.GetType() == typeof(Exceptions.Logistics.ParameterError))
                            {
                                str = "发送运单失败!" + report.exception.Message;
                            }
                            else
                            {
                                str = report.exception.Message;
                            }
                            resultErrors.Add(new CommonResult(report.CustomNumber, str));
                        }
                    }
                    //7.记录物流接口请求
                    await operateService.Insert(sendParameter, waybillID, response);

                    await transaction.CommitAsync();
                }
                else
                {
                    resultErrors.Add(new CommonResult("", response.exception.Message));
                }
            }
            catch (Exception e)
            {
                await transaction.RollbackAsync();
                resultErrors.Add(new CommonResult("", e.Message));
                logger.LogError("Send Exception:" + e.Message);
                logger.LogError("Send Exception:" + e.StackTrace);
            }
            stopwatch.Stop();
            logger.LogInformation("Send time:保存数据入库 " + stopwatch.ElapsedMilliseconds+"毫秒");
            return Success(new
            {
                errors = resultErrors,
                results = results,
            });
        }


        /// <summary>
        /// 合并订单报备发货
        /// </summary>
        /// <returns></returns>

        [HttpPost]
        public async Task<ResultStruct> HandAdds(HanAddParam handParam)
        {
            var orderParam = handParam.OrderInfo; //目前业务而已，只有一个订单信息
            List<OrderModel> orders = new List<OrderModel>();
            if (handParam.MergeID>0)
            {
                var result = await orderService.GetOrdersByMergeID(handParam.MergeID);
                if (!string.IsNullOrWhiteSpace(result.Item1))
                {
                    return Error(result.Item1);
                }
                orders.AddRange(result.Item2);                               
            }
            else
            {
                var orderInfo = await orderService.GetOrderByIdAsync(orderParam!.id);
                if (orderInfo is null)
                {
                    return Error("订单信息没有找到！");
                }
                orders.Add(orderInfo);
            }
            var exampleOrder = orders!.FirstOrDefault(x => x.ID == orderParam.id);
            if (exampleOrder is null)
            {
                return Error("订单信息没有找到！");
            }
            //根据orderid，补齐orderinfo参数           
            orderParam.Url = exampleOrder.Url;
            orderParam.OrderNo = exampleOrder.OrderNo;
            using var transaction = await Mydbcontext.Database.BeginTransactionAsync();
            try
            {
                Model waybill = new();               
                var orderLst = orderParam.GoodInfos.Select(x => x.OrderId).ToList();
                try
                {
                    //1.运单添加数据
                    waybill = await waybillService.AddWayBillActions(exampleOrder, orderParam, handParam.MergeID, string.Join(",", orderLst));
                    foreach (var order in orders)
                    {
                        var goodsInfo = orderParam.GoodInfos.Where(x => x.OrderId == order.ID).ToList(); //找出合并订单中，修改了的订单商品信息
                        if (goodsInfo is not null && goodsInfo.Count > 0)
                        {
                            //2.修改订单数量
                            orderService.CountGoods(order!, goodsInfo, handParam.Type);
                            
                            if (orderLst.Any(x => x == order.ID))
                            {
                                //4.添加运单日志
                                await waybillService.AddLog(order.ID, waybill.ID, order.Plateform, "增加合并订单手动运单信息",new UserInfoDto(_ISessionProvider.Session!));
                            }
                        }
                    }
                    Mydbcontext.Order.UpdateRange(orders);                    
                }
                catch(Exception e)
                {
                    await transaction.RollbackAsync();
                    logger.LogError($"HandMergeAdds 添加合并手动运单失败,{e.Message}");
                    return Error("发送运单失败");
                }

                //6.同步订单信息
                if (waybill.IsVerify != VerifyStates.Cancel)
                {
                    await orderService.UpdateMergeOrderWaybill(handParam.MergeID, handParam.OrderProgress, exampleOrder.ID);
                }

                //7.同步亚马逊                                
                if (handParam.SyncAmazon.IsSync)
                {
                    if (orderParam.WaybillOrder.IsNullOrWhiteSpace())
                    {
                        logger.LogError($"HandMergeAdds 运单号为空，同步亚马逊失败");                       
                    }
                    foreach (var id in orderLst)
                    {                        
                        var msg = await orderService.SyncAmazon(id, handParam.SyncAmazon, orderParam.Express, orderParam.Tracking, true);
                        if (!string.IsNullOrWhiteSpace(msg))
                        {
                            return Error(msg);
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                await transaction.RollbackAsync();
                logger.LogError("HandAdds Exception:" + ex.Message);
                logger.LogError("HandAdds Exception:" + ex.StackTrace);
                return Error("发送运单失败");
            }
            await transaction.CommitAsync();
            return Success();
        }

        private async Task DistributeOrderToUser(int[] orderIds, int distributedUserId)
        {
            var existOrderIds = await Mydbcontext.OrderDistributedLinks.Where(l =>
                l.SourceType == OrderDistributedLink.SourceIdType.DistributionOrderId
                && orderIds.Contains(l.SourceId) &&
                l.DistributedType == OrderDistributedLink.DistributedIdType.UserId
                && l.DistributedId == distributedUserId).Select(l => l.SourceId).ToArrayAsync();

            Mydbcontext.OrderDistributedLinks.AddRange(orderIds.Except(existOrderIds).Select(orderId => new OrderDistributedLink()
            {
                SourceType = OrderDistributedLink.SourceIdType.DistributionOrderId,
                SourceId = orderId,
                DistributedType = OrderDistributedLink.DistributedIdType.UserId,
                DistributedId = distributedUserId,
                UserID = UserId,
                GroupID = GroupId,
                CompanyID = CompanyId,
                OEMID = OemId,
            }));
            await Mydbcontext.SaveChangesAsync();
        }


        /// <summary>
        /// 跟踪
        /// </summary>
        /// <param name="trackParameter"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResultStruct> Track(TrackParameter trackParameter)
        {
            string resultError = "";
            var user = await _userManager.GetUserAsync(User);
            var result = await logistics.Track(trackParameter);
            var transaction = Mydbcontext.Database.BeginTransaction();
            try
            {
                if (result.Success)
                {
                    if (result.TrackResults.Count > 0 && result.TrackResults is not null)
                    {
                        foreach (var trackInfo in result.TrackResults)
                        {
                            if (trackInfo.Success)
                            {
                                var waybillModel = waybillService.SearchWayBill(trackInfo.WaybillId);
                                await EditWayBillState(trackInfo);  //update waybill state,orderNumber                                                    
                                if (trackInfo.Price.IsNotNull() && trackInfo.Price!.Success)
                                {
                                    await _shippingFeeService.EditWayBillPrice(trackInfo.Price, waybillModel.MergeID,user,false);                                     
                                    var pricedata = new
                                    {
                                        LogName = trackParameter.LogName,
                                        Parameter = trackParameter.Parameter,
                                        TrackInfo = trackParameter.TrackOrderInfoLst.FirstOrDefault(x => x.waybillId == trackInfo.WaybillId),
                                    };
                                    string priceRequest = JsonConvert.SerializeObject(pricedata);
                                    string priceResultStr = JsonConvert.SerializeObject(trackInfo.Price);
                                    await operateService.UpdatePriceInfo(trackInfo.WaybillId, priceRequest, priceResultStr, trackInfo.Price.Response, trackInfo.Price.Success);
                                }
                            }
                            else
                            {
                                resultError = trackInfo.exception.Message;
                            }
                            //request                                             
                            var data = new
                            {
                                LogName = trackParameter.LogName,
                                Parameter = trackParameter.Parameter,
                                TrackInfo = trackParameter.TrackOrderInfoLst.FirstOrDefault(x => x.waybillId == trackInfo?.WaybillId),
                            };
                            string request = JsonConvert.SerializeObject(data);
                            string resultStr = JsonConvert.SerializeObject(trackInfo);
                            await operateService.UpdateTrackInfo(trackInfo!.WaybillId, request, resultStr, trackInfo.Response, trackInfo.Success);
                        }
                    }
                }
                else
                {
                    foreach (var trackInfo in trackParameter.TrackOrderInfoLst)
                    {
                        var data = new
                        {
                            LogName = trackParameter.LogName,
                            Parameter = trackParameter.Parameter,
                            TrackInfo = trackParameter.TrackOrderInfoLst.FirstOrDefault(x => x.waybillId == trackInfo.waybillId),
                        };
                        string request = JsonConvert.SerializeObject(data);
                        string response = JsonConvert.SerializeObject(result.exception);
                        await operateService.UpdateTrackInfo(trackInfo.waybillId, request, response, "", false);
                    }
                    resultError = result.exception.Message;
                }
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                logger.LogError("track method error:" + e.Message);
                resultError = e.Message;
            }

            return Success(new
            {
                success = result.Success,
                error = resultError,
                results = result.TrackResults,
            });
        }

        public async Task<bool> EditWayBillState(TrackResult trackResult)
        {
            var info = await waybillService.GetInfoById(trackResult.WaybillId);
            if (info == null)
            {
                return false;
            }
            if (info.IsVerify == Enums.Logistics.VerifyStates.Confirm)
            {
               // info.ButtonName = "";
            }
            info.Tracking = trackResult.TrackNum;
            info.TrackInfo = trackResult.TrackInfo;
            info.State = trackResult.State ?? Enums.Logistics.States.OTHER;
            info.Express = trackResult.Express??"";
            
            Mydbcontext.Update(info);
            Mydbcontext.SaveChanges();
            string ACtion = "修改运单信息";
            int s = string.Compare(info.Tracking, trackResult.TrackNum);
            ACtion = s < 0 ? ACtion : ACtion + ",面单追踪号" + $"{info.Tracking}更新" + $"{trackResult.TrackNum}";
            await waybillService.AddLog(info.OrderId, info.ID, info.Logistic.Platform, ACtion,new UserInfoDto(_ISessionProvider.Session!));
            return true;
        }


        public async Task<bool> EditWayBillPrice(TrackPrice trackPrice,int? mergeId)
        {

            if(trackPrice.Unit.Equals("RMB",StringComparison.CurrentCultureIgnoreCase))
            {
                trackPrice.Unit = "CNY";
            }

            if (trackPrice.WaybillId == 0)
            {
                return false;
            }
            //洛琴oem跳过更新价格
            if (OemId == 2 && HttpContext.Request.Host.ToString().Contains("luoqin"))
            {
                return true;
            }
            var info = await waybillService.GetWaybillInfoById(trackPrice.WaybillId);
            if (info == null)
            {
                return false;
            }

            var OldMoney = info.CarriageNum.Money;
            var carriage_num = new MoneyMeta(trackPrice.Unit, trackPrice.Price);
            // info.WaybillRate = carriage_num.Rate;

            var n = new Money(Convert.ToDecimal(trackPrice.Price));
            var m = new MoneyRecordFinancialAffairs(n, carriage_num);

            info.CarriageBase = info.CarriageBase.Next(trackPrice.Unit, trackPrice.Price);
            info.CarriageNum  =  info.CarriageBase.Next(trackPrice.Unit, trackPrice.Price);
            info.CarriageUnit = trackPrice.Unit;
            //物流平台扣费时间
            if (trackPrice.DeductionTime is not null)
            {
                info.DeductionTime = trackPrice.DeductionTime;
            }
            //最后拉取运费的时间
            info.LastAutoShipTime = DateTime.Now;

            var company = await companyService.GetInfoById(info.CompanyID);
            var user = await _userManager.GetUserAsync(User);
            string s = $"订单编号：【{info.OrderNo}】；物流运单号：【{info.Express}】"; 
            //上级添加 下级分销上报运单
            if (company.IsDistribution && info.WaybillCompanyID != 0 )
            {
                var waybill_rate = info.WaybillRate;
                if (waybill_rate <= 0)
                {
                    waybill_rate = company.WaybillRateType == 1 ? carriage_num.Amount*company.WaybillRate/100 : new MoneyMeta(trackPrice.Unit, company.WaybillRateNum);
                }
                var carriagenum = carriage_num + waybill_rate;
                info.WaybillRate = waybill_rate;
                info.CarriageNum = new MoneyMeta(trackPrice.Unit, carriagenum);
                
                //退回预扣冻结
                if (!info.IsReturnFreeze && info.WaybillFreeze.HasValue && info.WaybillFreeze.Value > 0)
                {
                    var freezeWaybill = new FreezeWaybill(company, user); // 钱包
                    await freezeWaybill.Recharge(info.WaybillFreeze.Value);
                    await Mydbcontext.BillLog.AddAsync(freezeWaybill.AddLog(info.WaybillFreeze.Value, true , Modules.WAYBILL, "返还运单预付款冻结", s, info.StoreId, info.ID));
                    info.IsReturnFreeze = true;
                }
            }
            //运单不需要审核
            if (company.WaybillConfirm == WaybillConfirms.NO)
            {
                try
                {
                    await waybillService.Update(info);
                    await waybillService.AddLog(info.OrderId, info.ID, info.Logistic, "同步更新运单运费",new UserInfoDto(user));
                }
                catch (Exception /*e*/)
                {
                    return false;
                }
                return true;
            }
            //自动确认           
            // int belong = 0;
            //
            var Money = new Money(info.CarriageNum.Money.Value);            
            // if (company.IsDistribution && info.WaybillCompanyID != 0)
            // {
            //     belong = 2;
            //     await companyService.ExpensePublicWallet(Money, info.CompanyID);
            // }
            // else
            // {
            //     belong = 1;
            //     await companyService.ExpenseWallet(Money);
            // }
            //当前数据确认           
            info.IsVerify = Enums.Logistics.VerifyStates.Confirm;
            //已确认运单，调用钱包接口
            // if (info.IsVerify == Enums.Logistics.VerifyStates.Confirm && OldMoney.Value!= Money.Value)
            if (info.IsVerify == Enums.Logistics.VerifyStates.Confirm && OldMoney.Value!= Money.Value)
            {               
                              
                var b = OldMoney > Money ? true : false;
                string msg = "";
                if (OldMoney.Value == 0)
                {
                    msg = "运费扣款";
                }
                else
                {
                    msg = b == true ? "运费返还" : "运费补款";
                }                
               
                var wallet = info.GetUsedWallet(company, user); // 钱包
               
                //钱包扣款 
                //运费补款扣余额 运费返还加余额
                var absMoney = Math.Abs(OldMoney - Money);
                if (!b)
                {
                    //拉取运费时不再判断钱包是否充足直接扣费。
                    //if (!wallet.IsEnough(absMoney))
                    //{
                    //    throw new Exception("钱包余额不足");
                    //}

                    await wallet.Expense(absMoney);
                }
                else
                {
                    await wallet.Recharge(absMoney);
                }
                // await wallet.Expense(absMoney);
                
                var billLog = wallet.AddLog(absMoney, b, Modules.WAYBILL, msg, s, info.StoreId, info.ID);
                info.BillLog = billLog;
            }


            await waybillService.Update(info);
            //同步订单
            await orderService.UpdateMergeOrderWaybill(mergeId, null, info.OrderId);
            //增加运单日志
            await waybillService.AddLog(info.OrderId, info.ID, info.Logistic, "确认运单信息",new UserInfoDto(user));
            // #region 修改数据-基础运费跟踪
            // await _statisticMoney.WaybillCarriageBaseEdit(info);
            // #endregion
            //
            // #region 修改数据-最终运费数值
            //
            // await _statisticMoney.WaybillCarriageNumEdit(info);
            //
            // #endregion
            
            #region 修改数据-运单相关金额上报

            await _statisticMoney.ReportWayBillEdit(info);
            #endregion
            
            return true;
        }


        [HttpPost]
        public async Task<ResultStruct> GetFee(WaybillID waybillID)
        {
            int id = waybillID.ID;
            var waybillModel = waybillService.SearchWayBill(id);
            if(waybillModel is null)
            {
                return Error("没有找到运单！");//
            }
            //有价格的时候，就不更新
            //if(waybillModel.CarriageNum.Raw.Amount>0)
            //{
            //    return Success("已经成功获取运费！");//
            //}
            WaybillInfos waybill = new WaybillInfos(waybillModel);
            // {
            //     waybillId = id,
            //     LogName = waybillModel.Logistic.Platform,
            //     Parameter = waybillModel.LogisticExt,
            //     CustomNumber = waybillModel.WaybillOrder,
            //     LogictisNumber = waybillModel.Express,
            //     TrackNumber = waybillModel.Tracking,
            // };

            var result = await logistics.GetFee(waybill);
            if(result is null)
            {
                
                return Error($"{waybillModel.OrderNo} 获取运费出错！");
                
            }
            if (result.Success)
            {
                var priceInfo = result.Price; 
                if (priceInfo.Success)
                {
                    var transaction = await Mydbcontext.Database.BeginTransactionAsync();
                    try
                    {
                        var user = await _userManager.GetUserAsync(User);
                        await _shippingFeeService.EditWayBillPrice(priceInfo, waybillModel.MergeID,user,false);
                        await transaction.CommitAsync();
                    }
                    catch (Exception e)
                    {
                        logger.LogError(e.Message,e);
                        await transaction.RollbackAsync();
                        return Error($"{waybillModel.OrderNo} 修改运费出错！");
                    }
                    return Success("成功获取运费！");
                    //if (priceInfo?.Price > 0)
                    //{
                    // if( await EditWayBillPrice(priceInfo, waybillModel.MergeID))
                    // {
                    //     return Success("成功获取运费！");
                    // }
                    // else
                    // {
                    //     return Error($"{waybillModel.OrderNo} 修改运费出错！");
                    // }
                    //}
                    //else
                    //{
                    //    return Success("运费尚未生成！");
                    //}
                }

                return Error($"{priceInfo.exception.Message}");
            }
            if(result.exception.GetType() == typeof(Exceptions.Logistics.MethodNotRealized))
            {
                return Success("物流接口不支持获取运费！");
            }
            return Error($"{result.exception.Message}");
        }


        /// <summary>
        /// 预估费用
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResultStruct> EstimatePrice(PriceParameter priceParameter)
        {
            string resultError = "";
            var result = await logistics.EstimatePrice(priceParameter);
            if (!result.Success)
            {
                if (result.exception.GetType() == typeof(Exceptions.Logistics.UnKnownError))
                {
                    resultError = "预估运费失败";
                }
                else
                {
                    resultError = result.exception.Message;
                }               
            }
            return Success(new
            {
                Success = result.Success,
                error = resultError,
                results = result.PriceInfos,
            });
        }


        [HttpPost]
        public async Task<ResultStruct> methodDefineds(BaseParameter baseParameter)
        {
            string resultErrors = "";
            var result = await logistics.methodDefineds(baseParameter);
            if (!result.success)
            {
                resultErrors = result.exception.Message;
            }
            return Success(new
            {
                success = result.success,
                errors = resultErrors,
                results = result.PlatformMethod,
            });
        }


        [HttpPost]
        public async Task<ResultStruct> GetPrint(FileTypeParameter fileTypeParameter)
        {
            PrintParamResult results = new PrintParamResult();
            string resultError = "";
            var result = await logistics.GetPrint(fileTypeParameter);
            if (!result.Success)
            {
                resultError = result.exception.Message;
            }
            return Success(new
            {
                Success = result.Success,
                errors = resultError,
                results = result,
            });
        }


        [HttpPost]
        public async Task<ResultStruct> ShowPrint(MultiPrint multiPrint)
        {
            var transaction = Mydbcontext.Database.BeginTransaction();
            bool f = false;
            string resultError = "";
            string url = "";
            try
            {
                var param = waybillService.findWayBillByID(multiPrint.waybillId);
                if (param is not null)
                {
                    multiPrint.printParams = new List<PrintParam>() { param };
                    var result = await logistics.Print(multiPrint);
                    f = result.Success;
                    if (!result.Success)
                    {
                        resultError = result.exception.Message;
                    }
                    else
                    {
                        //单独打印
                        var print = result.PrintResults[0];
                        f = print.Success;
                        if (!print.Success)
                        {
                            if (print.exception.GetType() == typeof(Exceptions.Logistics.UnKnownError))
                            {
                                resultError = "打印失败";
                            }
                            else if (print.exception.GetType() == typeof(Exceptions.Logistics.NotGeneratedLabelError))
                            {
                                resultError = print.exception.Message;
                            }
                            else
                            {
                                resultError = result.exception.Message;
                            }
                        }
                        else
                        {
                            url = print.Url;
                        }

                    }
                    var data = new
                    {
                        print = multiPrint,
                    };
                    string request = JsonConvert.SerializeObject(data);
                    string resultStr = "";
                    await operateService.UpdatePrintInfo(multiPrint.waybillId, request, result.Response, resultStr, result.Success);
                }
                else
                {
                    resultError = "没有找到面单!";
                    var data = new
                    {
                        print = multiPrint,
                    };
                    string request = JsonConvert.SerializeObject(data);
                    string resultStr = JsonConvert.SerializeObject(resultError);
                    await operateService.UpdatePrintInfo(multiPrint.waybillId, request, "", resultStr, false);

                }
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                logger.LogError("print method error:" + e.Message);
                resultError = e.Message;
            }
            return Success(new
            {
                success = f,
                error = resultError,
                url = url,
            });
        }


        /// <summary>
        /// Print Label
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> DownLoadLabel([FromQuery] MultiPrint multiPrint)
        {
            string resultError = ""; 
            string resultStr = "";
            var transaction = Mydbcontext.Database.BeginTransaction();
            try
            {
                var param = waybillService.findWayBillByID(multiPrint.waybillId);
                if (param is not null)
                {
                    multiPrint.printParams = new List<PrintParam>() { param };
                    var result = await logistics.Print(multiPrint);

                    string request = JsonConvert.SerializeObject(multiPrint.printParams);
                    
                    if (result.Success && result.PrintResults[0].Success && result.PrintResults[0].Stream is not null)
                    {                       
                        var print = result.PrintResults[0];                       
                        var stream = print.Stream;
                        var streamCopy= print.Stream;
                        var contentType = result.PrintResults[0].ContentType;
                        var ext = result.PrintResults[0].Ext;
                       
                        await operateService.UpdatePrintInfo(multiPrint.waybillId, request, result.Response, resultStr, result.Success);

                        return new FileStreamResult(stream, contentType)
                        {
                            FileDownloadName = Random.Shared.NextInt64(100000, 999999).ToString() + ext,
                        };
                    }
                    else
                    {                       
                        if (result.exception.GetType() == typeof(Exceptions.Logistics.UnKnownError))
                        {
                            resultError = "下载失败";
                        }
                        else
                        {
                            resultError = result.PrintResults[0].exception.Message;
                        }                       
                    }
                    await operateService.UpdatePrintInfo(multiPrint.waybillId, request, result.Response, resultError, result.Success);
                }
                else
                {
                    resultError = "没有找到面单数据！";
                }
            }
            catch (Exception e)
            {
                transaction.Rollback();
                logger.LogError("print method error:" + e.Message);
                resultError = e.Message;
            }           
            return Ok(resultError);           
        }
        
        [HttpPost]
        public async Task<ResultStruct> PullWaybill(PullWaybillDto req,[FromServices] ILogger<Logistics> iLogger, [FromServices] IQueue queue, [FromServices] IListener<QueueTaskCompleted> iListener)
        {
            if (req.DateRange.Count < 2)
            {
                return Error("请选择时间范围");
            }

            var id = Session.GetWaybillFeePulling();
            var listener = (iListener as TaskCompletedListener)!;
            
            if (!listener.IsCompleted(id))
            {
                var metric = queue.GetMetrics();
                return Warning($"当前正在拉取运单运费，请稍后重试", "Pulling");
            }

            var user = await _userManager.GetUserAsync(User);
            
            var guid = Guid.NewGuid();
            Session.SetWaybillFeePulling(guid);
            listener.AddTask(guid);
            
            queue.QueueTask(async () =>
            {
                try
                {
                    await _waybillFeeHost.PullWaybillFee(req.IsDistribution,req.DateRange[0],req.DateRange[1],user);
                }
                catch (Exception e)
                {
                    logger.LogError($"PullWaybillFee  error:{e.Message}");
                }
                listener.RunningTasks[guid] = true;
            });
            return Success("开始拉取，请稍后查看运单");
        }
        
    }
}

public class PullWaybillDto
{
    
    public bool IsDistribution { get; set; }
    public List<DateTime> DateRange { get; set; }
}
