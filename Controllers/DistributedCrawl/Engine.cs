﻿using Microsoft.AspNetCore.Mvc;
using Service = ERP.Services.DistributedCrawl.Engine;
using ERP.Models.Product;
using ERP.Extensions;

namespace ERP.Controllers.DistributedCrawl;
public class Engine : BaseController
{
    readonly Service Service;
    public Engine(Service service)
    {
        Service = service;
    }

    /// <summary>
    /// 获取用户页面配置信息
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> Config()
    {
        return Success(await Service.BuildUIConfig());
    }
  
}
