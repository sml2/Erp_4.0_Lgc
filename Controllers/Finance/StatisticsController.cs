﻿using Microsoft.AspNetCore.Mvc;
using ERP.Extensions;
using ERP.Services.Finance;
using ERP.Services.Host;
using ERP.ViewModels;
using ERP.ViewModels.Statistics;

namespace ERP.Controllers.Finance;

[Route("api/finance/[controller]")]
[ApiController]
public class StatisticsController : BaseController
{
    private readonly StatisticsService _service;

    public StatisticsController(StatisticsService service)
    {
        _service = service;
    }

    [Route("init")]
    [HttpGet]
    public async Task<ResultStruct> Init()
    {
        /************************************************  
         
        ************************************************/
        return await Task.FromResult(Success());
    }

    /// <summary>
    /// 亚马逊订单财务
    /// </summary>
    /// <returns></returns>
    [Route("getCompanyAmazon")]
    [HttpGet]
    public async Task<ResultStruct> getCompanyAmazon()
    {
        return await Task.FromResult(Success());
    }


    [HttpPost("companyTotal")]
    public async Task<ResultStruct> CompanyStatistics(CompanyTotalDto req)
    {
        var result = await _service.CompanyTotal(req);

        return Success(result!);
    }

    [HttpPost("companyMonth")]
    public async Task<ResultStruct> CompanyMonthStatistics(CompanyMonthDto req)
    {
        var result = await _service.CompanyMonthStatistics(req);

        return Success(result);
    }

    [HttpPost("userGroup")]
    public async Task<ResultStruct> UserGroupStatistics(UserGroupOrStoreDto req)
    {
        var result = await _service.UserGroupOrStoreStatistics(req, ActionEnum.UserGroup);

        return await Task.FromResult(Success(result));
    }

    [HttpPost("store")]
    public async Task<ResultStruct> StoreStatistics(UserGroupOrStoreDto req)
    {
        var result = await _service.UserGroupOrStoreStatistics(req, ActionEnum.Store);

        return await Task.FromResult(Success(result));
    }

    [HttpPost("order")]

    public async Task<ResultStruct> OrderStatistics(OrderDto req)
    {
        var result = await _service.OrderStatistics(req);

        return await Task.FromResult(Success(result));
    }
    [HttpPost("storeInfo")]
    public async Task<ResultStruct> StoreInfo(IdDto req)
    {
        var result = await _service.StoreInfo(req.ID);

        return await Task.FromResult(Success(result));
    }
}