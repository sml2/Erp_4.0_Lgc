﻿using ERP.Data;
using Microsoft.AspNetCore.Mvc;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.Statistics;
using ERP.Services.DB.Statistics;
using ERP.Services.DB.Users;
using ERP.Services.Finance;
using ERP.Services.Statistics;
using ERP.ViewModels.Finance;
using BillLog = ERP.Models.Finance.BillLog;
using Exception = ERP.Exceptions.Exception;
using StoreCache = ERP.Services.Caches.Store;
using ERP.Enums.Identity;
using ERP.Models.Abstract;
using ERP.Models.Finance;
using Microsoft.AspNetCore.Identity;

namespace ERP.Controllers.Finance
{
    public class AuditController : BaseController
    {
        private readonly DBContext _dbContext;
        
        private readonly BillLogService _billLogService;

        private readonly CompanyService _companyService;

        private readonly ISessionProvider _session;

        private readonly StoreCache _storeCache;
        private readonly Statistic _statisticService;
        private readonly StatisticMoney _statisticMoney;
        private readonly UserManager<Models.DB.Identity.User> _userManager;

        private ISession _Session => _session.Session!;
        private int _userId => _Session.GetUserID();
        private int _groupId => _Session.GetGroupID();
        private int _companyId => _Session.GetCompanyID();
        private int _oemId => _Session.GetOEMID();

        public AuditController(BillLogService billLogService, ISessionProvider session, StoreCache storeCache,
            CompanyService companyServicel, DBContext dbContext, Statistic statisticService, UserManager<Models.DB.Identity.User> userManager)
        {
            _billLogService = billLogService;
            _session = session;
            _storeCache = storeCache;
            _companyService = companyServicel;
            _dbContext = dbContext;
            _statisticService = statisticService;
            _userManager = userManager;
            _statisticMoney = new StatisticMoney(_statisticService);
        }

        /// <summary>
        /// 列表页
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<ResultStruct> Index(BillLogIndexDto req)
        {
            
            var data = await  _billLogService.GetBillLogListWithSelfuse(req, true);
            var list = data.Transform(m => new BillLogVM(_session.Session, m));
            var company = await _companyService.GetInfoById(_companyId);
            if(company is null)
            {
                return Error("当前公司不存在");
            }

            object rule = GetRule();

            return Success(new
            {
                list,
                store = _storeCache.List()!.Where(m => m.CompanyID == _companyId &&m.IsDel==false && m.State == BaseModel.StateEnum.Open).ToList(),
                company_id = _companyId,
                rule,
                module = Enum<BillLog.Modules>.ToDictionaryForUIByDescription()
            });
            
         
        }

        /// <summary>
        /// 获取权限
        /// </summary>
        /// <returns></returns>
        private object GetRule()
        {
            // $rule = $this->sessionData->getRule()->getFinance()->getBiter();
            object rules;
            if (!User.IsInRole(Role.Employee))
            {
                rules = new
                {
                    BillDetails = true,
                    Approval = true
                };
            }
            else
            {
                rules = new
                {
                    BillDetails = true, //$rule->hasApproval(),//财务审核
                    Approval = true //$rule->hasBillDetails()//账单详情
                };
            }

            return rules;
        }

        [HttpPost]
        public async Task<ResultStruct> DoAudit(DoAuditDto req)
        {
            // if (!$this->rules()->hasApproval()) {
            //     return warning('无权访问');
            // }   

            //查询账单信息
            var info = await _billLogService.CheckAuditStatus(req.Id);
            if (info == null)
            {
                return Error("当前账单不存在");
            }

            var company = await _companyService.GetInfoById(_companyId);
            if (company is null)
            {
                return Error("当前公司不存在");
            }
            if (req.Audit == BillLog.Audits.DidNotPass)
            {
                var res = await _billLogService.CheckBill(req.Id, req.Audit, company);

                return res ? Success() : Error();
            }

            var money = info.Money.Value;
            
            var transaction = _dbContext.Database.BeginTransaction();

            try
            {
                // var userBusinessService = new CounterUserBusinessService(_dbContext,_session);
                // var companyBusinessService = new CounterCompanyBusinessService(_dbContext,_session);
                // var realFinanceService = new CounterRealFinanceService(_dbContext);
                var user = await _userManager.GetUserAsync(User);
                var wallet = new SelfWallet(company, user);
                // await wallet.Recharge(money);
                // wallet.AddLog(money, true, req.Info.Modules ?? BillLog.Modules.INTERCHANGEABLE, req.Info.Reason, req.Info.Remark);
                //
                // #region 自用钱包跟踪
                //
                // await _statisticMoney.SelfWalletReport(company);
                //
                // #endregion
                if (info.Type == BillLog.Types.Recharge)
                {
                    //充值
                    // await _companyService.RechargeWallet(money,company);
                    await wallet.Recharge(money);
                    // await userBusinessService.SelfRechargeNumAdd(_userId, money);
                    // await companyBusinessService.SelfRechargeNumAdd(_userId, money);
                    // await realFinanceService.SelfRechargeTotal(company, money);

                    // if (User.IsInRole(Role.Employee))
                    // {
                    //     var groupBusinessService = new CounterGroupBusinessService(_dbContext, _session);
                    //     await groupBusinessService.SelfRechargeNumAdd(_groupId, money);
                    // }



                }
                else
                {
                    // await _companyService.ExpenseWallet(money,company);
                    await wallet.Expense(money);
                    // await userBusinessService.SelfExpenseNumAdd(_userId, money);
                    // await companyBusinessService.SelfExpenseNumAdd(_companyId, money);
                    // await realFinanceService.SelfExpenseTotal(company, money);


                    // if (User.IsInRole(Role.Employee))
                    // {
                    //     var groupBusinessService = new CounterGroupBusinessService(_dbContext, _session);
                    //     await groupBusinessService.SelfExpenseNumAdd(_groupId, money);
                    // }
                    // #region 自用钱包支出汇总
                    //
                    // await _statisticMoney.SelfExpenseTotal(company, money);
                    //
                    // #endregion
                }
                
                #region 自用钱包跟踪

                await _statisticMoney.SelfWalletReport(company);

                #endregion

                if (!await _billLogService.CheckBill(req.Id, req.Audit, company))
                {
                    throw new Exception("找不到账单记录");
                }
                
                await _dbContext.SaveChangesAsync();
               
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return Error(e.Message);
            }

            return Success();
        }
    }
}