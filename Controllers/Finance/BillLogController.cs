﻿using ERP.Enums.Identity;
using ERP.Data;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.DB.Users;
using ERP.Services.DB.Users;
using ERP.Services.Finance;
using ERP.Services.Statistics;
using ERP.ViewModels.Finance;
using BillLog = ERP.Models.Finance.BillLog;
using StoreCache = ERP.Services.Caches.Store;
using ERP.Models.Abstract;
using ERP.Models.Finance;
using Microsoft.AspNetCore.Identity;

namespace ERP.Controllers.Finance
{
    public class BillLogController : BaseController
    {
        private readonly DBContext _dbContext;

        private readonly BillLogService _billLogService;

        private readonly CompanyService _companyService;

        private readonly ISessionProvider _session;

        private readonly StoreCache _storeCache;
        private readonly StatisticMoney _statisticMoney;
        private readonly UserManager<Models.DB.Identity.User> _userManager;

        public BillLogController(DBContext dbContext, BillLogService billLogService, CompanyService companyService,
            ISessionProvider session, StoreCache storeCache, StatisticMoney statisticMoney,
            UserManager<Models.DB.Identity.User> userManager)
        {
            _dbContext = dbContext;
            _billLogService = billLogService;
            _companyService = companyService;
            _session = session;
            _storeCache = storeCache;
            _statisticMoney = statisticMoney;
            _userManager = userManager;
        }

        private ISession Session => _session.Session!;
        private int _userId => Session.GetUserID();
        private int _groupId => Session.GetGroupID();
        private int _companyId => Session.GetCompanyID();
        private int _oemId => Session.GetOEMID();

        [HttpPost]
        public async Task<ResultStruct> Index(BillLogIndexDto req, bool isAuditList = false)
        {
            var data = await _billLogService.GetBillLogListWithSelfuse(req, isAuditList);
            var list = data.Transform(m => new BillLogVM(_session.Session, m));
            var company = await _companyService.GetInfoById(_companyId);
            if (company is null)
            {
                return Error("当前公司不存在");
            }

            object rule;
            if (isAuditList)
            {
                rule = new { };
            }
            else
            {
                rule = GetRule();
            }

            return Success(new
            {
                list,
                store = _storeCache.List()?.Where(m =>
                    m.CompanyID == _companyId && m.IsDel == false && m.State == BaseModel.StateEnum.Open).ToList(),
                company_id = _companyId,
                wallet = new { money = company.PrivateWallet.ToString(_session.Session!.GetUnitConfig()) },
                rule,
                module = Enum<BillLog.Modules>.ToDictionaryForUIByDescription()
            });
        }

        private object GetRule()
        {
            object rule;
            if (!User.IsInRole(Role.Employee))
            {
                rule = new
                {
                    BillExport = true,
                    BillRecharge = true,
                    BillDisburse = true,
                    BillDetails = true,
                };
            }
            else
            {
                rule = new
                {
                    BillExport = true, //$rule->hasBillExport(),//账单导出
                    BillRecharge = true, //$rule->hasBillRecharge(),//充值
                    BillDisburse = true, //$rule->hasBillDisburse(),//支出
                    BillDetails = true, //$rule->hasBillDetails()//账单详情
                };
            }

            return rule;
        }

        /// <summary>
        /// 公司钱包充值
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResultStruct> Recharge(RechargeDto req)
        {
            var company = await _companyService.GetInfoById(_companyId);
            if (company == null)
            {
                return Error("当前公司不存在");
            }

            var money = MoneyFactory.Instance.Create(req.Info.Money, _session.Session!.GetUnitConfig());

            //公司配置需要审核
            if (company.BillAudit == Company.BillAudits.YES)
            {
                await _billLogService.AddWalletBill(req, money, company);
                return Success();
            }

            //无需审核
            var transaction = _dbContext.Database.BeginTransaction();
            try
            {
                // //统计
                // var userBusinessService = new CounterUserBusinessService(_dbContext, _session);
                // var companyBusinessService = new CounterCompanyBusinessService(_dbContext, _session);
                // var realFinanceService = new CounterRealFinanceService(_dbContext);
                // var groupBusinessService = new CounterGroupBusinessService(_dbContext, _session);
                // //公司钱包汇总
                // await companyBusinessService.SelfRechargeNumAdd(company.ID, money);
                // //月报
                // await realFinanceService.SelfRechargeTotal(company, money);
                // //用户组钱包汇总
                // if (_groupId != 0)
                // {
                //     await groupBusinessService.SelfRechargeNumAdd(_groupId, money);
                // }
                //
                // //用户钱包汇总
                // await userBusinessService.SelfRechargeNumAdd(_userId, money);
                //余额充值
                // await _companyService.RechargeWallet(money,company);

                //账单
                //无需审核 备注中记录
                // req.Info.Remark += "【公司账单审核为‘无需审核’，系统自动通过】";
                // await _billLogService.AddWalletBill(req, money, company);

                var user = await _userManager.GetUserAsync(User);
                var wallet = new SelfWallet(company, user);
                await wallet.Recharge(money);

                await _dbContext.BillLog.AddAsync(wallet.AddLog(money, true,
                    req.Info.Modules ?? BillLog.Modules.INTERCHANGEABLE, req.Info.Reason, req.Info.Remark));

                #region 自用钱包跟踪

                await _statisticMoney.SelfWalletReport(company);

                #endregion

                await _dbContext.SaveChangesAsync();
                await transaction.CommitAsync();

                return Success();
            }
            catch (Exception e)
            {
                await transaction.RollbackAsync();
                return Error(e.Message);
            }
        }

        [HttpPost]
        public async Task<ResultStruct> Spend(RechargeDto req)
        {
            var money = MoneyFactory.Instance.Create(req.Info.Money, _session.Session!.GetUnitConfig());
            var company = await _companyService.GetInfoById(_companyId);
            if (company is null)
            {
                return Error("Company Is Null");
            }

            //公司配置需要审核
            if (company.BillAudit == Company.BillAudits.YES)
            {
                await _billLogService.AddWalletBill(req, money, company, BillLog.Types.Deduction);
                return Success();
            }

            //无需审核
            var transaction = await _dbContext.Database.BeginTransactionAsync();
            try
            {
                // //统计
                // var userBusinessService = new CounterUserBusinessService(_dbContext, _session);
                // var companyBusinessService = new CounterCompanyBusinessService(_dbContext, _session);
                // var realFinanceService = new CounterRealFinanceService(_dbContext);
                // var groupBusinessService = new CounterGroupBusinessService(_dbContext, _session);
                // //公司钱包汇总
                // await companyBusinessService.SelfExpenseNumAdd(company.ID, money);
                // //月报
                // await realFinanceService.SelfRechargeTotal(company, money);
                // //用户组钱包汇总
                // if (_groupId != 0)
                // {
                //     await groupBusinessService.SelfExpenseNumAdd(_groupId, money);
                // }
                //
                // //用户钱包汇总
                // await userBusinessService.SelfExpenseNumAdd(_userId, money);
                //余额支出
                // await _companyService.ExpenseWallet(money, company);
                //账单
                // await _billLogService.AddWalletBill(req, money, company, BillLog.Types.Deduction);
                
                var user = await _userManager.GetUserAsync(User);
                var wallet = new SelfWallet(company, user);
                await wallet.Expense(money);

                await _dbContext.BillLog.AddAsync(wallet.AddLog(money, false,
                    req.Info.Modules ?? BillLog.Modules.INTERCHANGEABLE, req.Info.Reason, req.Info.Remark));

                #region 自用钱包跟踪

                await _statisticMoney.SelfWalletReport(company);

                #endregion

                await _dbContext.SaveChangesAsync();

               await transaction.CommitAsync();
            }
            catch (Exception e)
            {
                await transaction.RollbackAsync();
                return Error(e.Message);
            }

            return Success();
        }

        public void RefundBillLog()
        {
        }
    }
}