using ERP.Authorization.Rule;
using ERP.Enums.Finance;
using ERP.Enums.Logistics;
using ERP.Extensions;
using ERP.Models.DB.Logistics;
using ERP.Models.DB.Orders;
using ERP.Models.View.Logistics.WayBill;
using ERP.Services.DB.Statistics;
using ERP.Services.DB.Users;
using ERP.Services.Finance;
using ERP.Storage.Abstraction.Services;
using ERP.ViewModels.Logistics;
using ERP.ViewModels.Order.AmazonOrder;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using ERP.WaybillModule.Abstraction.DomainEnvents;
using BillLogService = ERP.Services.Finance.BillLog;
using CompanyCache = ERP.Services.Caches.Company;
using LogisticsCache = ERP.Services.Caches.Logistics;
using NationCache = ERP.Services.Caches.Nation;
using NationModel = ERP.Models.Setting.Nation;
using OrderService = ERP.Services.DB.Orders.Order;
using StoreService = ERP.Services.Stores.StoreService;
using UnitCache = ERP.Services.Caches.Unit;
using WayBillLog = ERP.Services.DB.Logistics.WayBillLog;
using WayBillLogModel = ERP.Models.DB.Logistics.WaybillLog;
using WayBillModel = ERP.Models.DB.Logistics.Waybill;
using WaybillService = ERP.Services.DB.Logistics.WayBill;
using WayBillViewModel = ERP.ViewModels.Logistics.WayBillVM;
using BillLogModel = ERP.Models.Finance.BillLog;
using UserManager = ERP.Services.Identity.UserManager;
using logisticsFactory = ERP.Services.Api.Logistics.Factory;
using ERP.Models.DB.Identity;
using ERP.Enums.Identity;
using ERP.Data;
using ERP.Enums.Orders;
using ERP.Models.Api.Logistics;
using ERP.Models.View.Order.AmazonOrder;
using ERP.Services.Statistics;
using Coravel.Events.Interfaces;
using Coravel.Queuing.Broadcast;
using Coravel.Queuing.Interfaces;
using ERP.Services.Host;
using Microsoft.EntityFrameworkCore;
using OrderHostService = ERP.Services.DB.Orders.OrderHost;
using ERP.Distribution.Abstraction.Services;
using ERP.Models.Finance;
using ERP.Models.Logistics;
using ERP.Models.View.User;

namespace ERP.Controllers.Logistics;

using static Models.DB.Identity.User;
using static Models.Finance.BillLog;
using StoreCache = Services.Caches.Store;
using Role = Enums.Identity.Role;
using Statistic = Services.DB.Statistics.Statistic;
using Order = Models.DB.Orders.Order;

//[Route("api/[controller]/[action]")]
//[ApiController]
public class Waybill : BaseController
{
    private readonly OrderService _orderService;
    private readonly BillLogService _BillLogService;
    private readonly WaybillService _waybillService;
    private readonly StoreService _StoreService;
    private readonly LogisticsCache _LogisticsCache;
    private readonly NationCache _NationCache;
    private readonly CompanyCache _CompanyCache;
    private readonly UnitCache _UnitCache;
    private readonly WayBillLog _WayBillLog;
    private readonly IStockService _StockService;
    private readonly CompanyService _CompanyService;
    private readonly DBContext _dbContext;
    private readonly Statistic _statisticService;
    private readonly StatisticMoney _statisticMoney;
    private readonly UserManager<Models.DB.Identity.User> _userManager;
    private readonly IWarehouseService _warehouseService;
    private readonly IMediator _mediator;
    private readonly ILogger<Waybill> logger;
    private readonly OrderHostService _orderHostService;
    private readonly IDistributionCompanyService discompanyService;
    public Waybill(DBContext dbContext, CompanyService CompanyService, IStockService StockService, OrderService orderService, BillLogService BillLogService, WaybillService Service, StoreService StoreService,
        LogisticsCache LogisticsService, NationCache nationCache, CompanyCache companyCache, UnitCache unitCache, WayBillLog wayBillLog, Statistic statisticService
        , UserManager<Models.DB.Identity.User> userManager, IWarehouseService warehouseService, IMediator mediator, ILogger<Waybill> logger, OrderHostService orderHostService,IDistributionCompanyService _discompanyService)
    {
        _dbContext = dbContext;
        _CompanyService = CompanyService;
        _StockService = StockService;
        _orderService = orderService;
        _BillLogService = BillLogService;
        _waybillService = Service;
        _StoreService = StoreService;
        _LogisticsCache = LogisticsService;
        _NationCache = nationCache;
        _CompanyCache = companyCache;
        _UnitCache = unitCache;
        _WayBillLog = wayBillLog;
        _statisticService = statisticService;
        _userManager = userManager;
        _warehouseService = warehouseService;
        _mediator = mediator;
        _statisticMoney = new StatisticMoney(_statisticService);
        this.logger = logger;
        _orderHostService = orderHostService;
        discompanyService=_discompanyService;
    }
    private ISession Session => HttpContext.Session;

    /// <summary>
    /// 获取权限
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> GetRule()//: JsonResponse
    {
        #region MyRegion

        var user = await _userManager.GetUserAsync(User);
        var company = user.Company;

        var waybillBack = false;
        var waybillReport = false;
        switch (company.DistributionWaybill)
        {
            case Models.DB.Users.Company.DistributionWaybills.TRUE:
                waybillBack = false;
                waybillReport = true;
                break;
            case Models.DB.Users.Company.DistributionWaybills.PORTION:
                waybillBack = true;
                waybillReport = true;
                break;
            case Models.DB.Users.Company.DistributionWaybills.FALSE:
                waybillBack = true;
                waybillReport = false;
                break;
        }

        //        if ($this->sessionData->getConcierge() === UserModel::TYPE_OWN) {
        //            $data = [
        //                /**运单列表**/
        //                'download'           => true,
        //                'send'               => true,
        //                'follow'             => true,
        //                'info'               => true,
        //                'order'              => true,
        //                'confirm'            => true,
        //                'edit'               => true,
        //                /**待发货列表**/
        //                'add'                => true,
        //                'receiverInfo'       => true,
        //                /**寄件人信息**/
        //                'senderAdd'          => true,
        //                'senderEdit'         => true,
        //                'senderDelete'       => true,
        //                /**物流管理**/
        //                'logisticAdd'        => true,
        //                'logisticEdit'       => true,
        //                'logisticDelete'     => true,
        //                //分销状态
        //                'waybillBack'        => $waybillBack,
        //                'waybillReport'      => $waybillReport,
        //                'PurchaseConfirm'    => true,
        //                //分销自由切换上报状态
        //                'SwitchDistribution' => true,
        //                //批量发货
        //                'AddConsignmentNote' => true,
        //                //批量绑定
        //                'batchPurchase'      => true
        //            ];
        //} else {
        //            $BackWaybill = $this->sessionData->getRule()->getDistribution()->getBiter()->hasBackWaybill();
        //            $isDistribution = $this->sessionData->getIsDistribution() === User::DISTRIBUTION_TRUE;
        //            $purchaseBiter = $this->sessionData->getRule()->getPurchase()->getBiter();
        //            $data = [
        //                /**运单列表**/
        //                'download'           => $biter->hasWaybillDownload(),
        //                'send'               => $biter->hasWaybillSend(),
        //                'follow'             => $biter->hasWaybillFollow(),
        //                'info'               => $biter->hasWaybillInfo(),
        //                'order'              => $biter->hasWaybillOrder(),
        //                'confirm'            => $biter->hasWaybillConfirm(),
        //                'edit'               => $biter->hasWaybillEdit(),
        //                /**待发货列表**/
        //                'add'                => $biter->hasWaybillAdd(),
        //                'receiverInfo'       => $biter->hasWaybillReceiverInfo(),
        //                /**寄件人信息**/
        //                'senderAdd'          => $biter->hasWaybillSenderAdd(),
        //                'senderEdit'         => $biter->hasWaybillSenderEdit(),
        //                'senderDelete'       => $biter->hasWaybillSenderDelete(),
        //                /**物流管理**/
        //                'logisticAdd'        => $biter->hasLogisticAdd(),
        //                'logisticEdit'       => $biter->hasLogisticEdit(),
        //                'logisticDelete'     => $biter->hasLogisticDelete(),
        //                //分销状态
        //                'waybillBack'        => $waybillBack ? $BackWaybill: false,
        //                'waybillReport'      => $waybillReport ? $BackWaybill: false,
        //                'PurchaseConfirm'    => $isDistribution ? $biter->hasPurchaseConfirm() : ($biter->getPurchaseConfirmRange() !== 1),
        //                //分销自由切换上报状态
        //                'SwitchDistribution' => $biter->hasSwitchDistribution(),
        //                //批量发货
        //                'AddConsignmentNote' => $this->sessionData->getRule()->getOrder()->getBiter()->hasAddConsignmentNote(),
        //                //批量绑定
        //                'batchPurchase'      => $purchaseBiter->hasPurchaseEdit(),
        //            ];
        //}
        //        $data ['is_distribution'] = $this->sessionData->getIsDistribution();
        //        return success('', $data);
        #endregion

        var data = new
        {
            addConsignmentNote = true,
            purchaseConfirm = true,
            switchDistribution = true,
            add = true,
            batchPurchase = true,
            confirm = true,
            download = true,
            edit = true,
            follow = true,
            info = true,
            logisticAdd = true,
            logisticDelete = true,
            logisticEdit = true,
            order = true,
            receiverInfo = true,
            send = true,
            senderAdd = true,
            senderDelete = true,
            senderEdit = true,
            isDistribution =  Session.GetIsDistribution(),
            waybillBack,
            waybillReport,
            batchGetFee=true,
        };
        return Success(data);
    }

    /// <summary>
    /// 查找用户拥有的店铺
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> WayBillList([FromServices] UserManager userManager, [FromServices] IWarehouseService _WarehouseService)//: JsonResponse
    {
        var user = await userManager.GetUserAsync(User);
        var Owned = !HttpContext.Session.Is(Role.Employee);
        var GeyType = Owned ? StoreCache.Types.Company : StoreCache.Types.User;
        var GetID = Owned ? HttpContext.Session.GetCompanyID() : HttpContext.Session.GetUserID();
        var Stores = await _StoreService.CurrentUserGetStores();
        var StoreView = Stores.Select(x => new { x.ID, x.Name }).ToList();

        var Logistic = _LogisticsCache.List()!.Select(x => new { LogisticID = x.Platform, LogisticName = x.Name }).ToList();
        Logistic = Logistic != null ? Logistic : null;

        var Nations = _NationCache.List();
        Nations = Nations != null ? Nations.Select(x => new NationModel { ID = x.ID, Name = x.Name }).ToList() : null;
        //GetSupportsOrderStates

        Dictionary<int, string> WaybillState = Enum<Enums.Logistics.States>.ToDictionaryForUIByDescription();        

        Dictionary<int, object> Progress = OrderState.OrderProgressData;

        var Warehouse = await _WarehouseService.AvailableWarehouses(user);

        var companyUsers = await _dbContext.User.Where(m => m.CompanyID == user.CompanyID)
            .Select(m => new { m.ID, m.TrueName }).ToDictionaryAsync(m => m.ID, m => m);

        //分销 获取当前上报公司数据
        var company = await discompanyService.GetValidDistributionList(user.CompanyID);

        return Success(new { WaybillState, store = StoreView, Logistic, Nations, Progress, Warehouse = Warehouse,companyUsers, company });
    }

    /// <summary>
    /// 运单列表(运单管理->运单列表)
    /// </summary>
    [HttpPost]
    public async Task<ResultStruct> List(WayBills WayBills)
    {
        var Storeids = new List<int>();
        if (Storeids.Count > 0 && Session.Is(Role.Employee))
        {
            return Success();
        }
        //获取对应订单备注信息
        var list = await _waybillService.GetList(Storeids, WayBills);
        var data = list.Transform(a => new
        {
            a.ID,
            a.OrderNo,
            a.OrderId,
            a.Express,
            a.WaybillOrder,
            a.Tracking,            
            CarriageNum = a.CarriageNum.ToString(Session.GetUnitConfig()),
            a.NationId,
            a.DeliverDate,
            a.StoreId,
            a.WaybillCompanyID,
            waybillState= a.State.GetDescription(),
            a.IsVerify,
            Remark = a.Remark ?? string.Empty,
            a.ImgUrl,
            a.ProductInfo,
            a.Logistic,
            a.LogisticExt,
            a.Sender,
            LogisticSign = a.Logistic.Platform.ToString(),
            a.CompanyID,
            a.ButtonName,
            a.CreatedAt,
            a.UpdatedAt,
            ShipMethod = a.ShipMethod,
            UserId = a.UserID,
            a.DeductionTime
        });
        // var res = await _orderService.GetOrderRemarks(data.Items.Select(a => a.OrderId).Distinct().ToList());
        // var remarks = res.Select(a => new
        // {
        //     a.ID,
        //     a.OrderId,
        //     a.Value,
        //     a.Truename,
        //     a.CreatedAt
        // }).GroupBy(a => a.OrderId).Select(g => new
        // {
        //     orderId = g.Key,
        //     list = g.ToList()
        // }).ToDictionary(a => a.orderId, a => a.list);

        return Success(new { data }, "成功");
    }

    /// <summary>
    /// 获取我的运单列表
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetDisMyList(WayBills WayBills)
    {
        var user = await _userManager.GetUserAsync(User);
        var list = await _waybillService.GetDisList(Session.GetCompanyID(), WayBills);
        var data = list.Transform(a => new
        {
            a.ID,
            a.OrderNo,
            a.OrderId,
            a.Express,
            a.WaybillOrder,
            a.Tracking,
            CarriageNum = a.CarriageNum.ToString(Session.GetUnitConfig()),
            a.NationId,
            a.DeliverDate,
            a.StoreId,
            a.WaybillCompanyID,
            waybillState = a.State.GetDescription(),
            a.IsVerify,
            Remark = a.Remark ?? string.Empty,
            a.ImgUrl,
            a.ProductInfo,
            a.Logistic,
            a.LogisticExt,
            a.Sender,
            LogisticSign = a.Logistic.Platform.ToString(),
            a.CompanyID,
            a.ButtonName,
            a.CreatedAt,
            a.UpdatedAt,
            ShipMethod = a.ShipMethod,
            UserId = a.UserID,
            a.DeductionTime
        });
        return Success(new { data }, "成功");
        // return Success(new { list });
    }


    /// <summary>
    /// 修改运单
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    public async Task<ResultStruct> EditInfo(WayBilEditVM request)
    {               
        try
        {
            var waybill = await _waybillService.GetInfoById(request.Id);
            var OldMoney = waybill.CarriageNum.Money;           
            var unit = request.CarriageUnit;
            var waybillFee_old = waybill.CarriageNum;
            if (waybill.Logistic.Platform.GetID() == 0)
            {
                waybill.Logistic = new PlatformInfo(request.Logistic);
            }
            waybill.CarriageBase = waybill.CarriageBase.Next(unit, request.CarriageBase);
            waybill.CarriageOther = waybill.CarriageOther.Next(unit, request.CarriageOther);
            // waybill.WaybillRate = request.WaybillRate;
            waybill.CarriageNum = waybill.CarriageNum.Next(unit, request.CarriageNum);
            waybill.WaybillOrder = request.WaybillOrder;
            waybill.Express = request.Express;
            waybill.Tracking = request.Tracking;
            waybill.CarriageUnit = request.CarriageUnit;
            waybill.Remark = request.Remark;
            if (waybill.WaybillRate == 0)
            {
                waybill.WaybillRate = request.WaybillRate;
            }
            
            // 
            var company = await _CompanyService.GetInfoById(waybill.CompanyID);
            //上级添加 下级分销上报运单 前端创建运单时已经记录手续费
            // if (company.IsDistribution && waybill.WaybillCompanyID != 0)
            // {
            //     //未设置手续费
            //     if (waybill.WaybillRate == 0)
            //     {
            //         var waybill_rate = company.WaybillRateType == 1 ? request.CarriageNum*company.WaybillRate/100 : new MoneyMeta(unit, company.WaybillRateNum);
            //         var carriagenum = request.CarriageNum + waybill_rate;
            //         waybill.WaybillRate = waybill_rate;
            //         waybill.CarriageNum = new MoneyMeta(unit, carriagenum);
            //     }
            //     //已设置手续费
            //     waybill.CarriageNum = new MoneyMeta(unit, request.CarriageNum + waybill.WaybillRate);
            //    
            // }
            
            var waybillFee_new = waybill.CarriageNum;

            var transaction = await _dbContext.Database.BeginTransactionAsync();
            try
            {
                //增加运单日志
                string action = "修改运单信息";
                if (request.Tracking != waybill.Tracking) action += $",面单追踪号【{waybill.Tracking}】更新{request.Tracking}";
                await _waybillService.AddLog(waybill.OrderId, waybill.ID, waybill.Logistic, action,new UserInfoDto(Session));

                var Money = waybill.CarriageNum.Money;
                //已确认运单，调用钱包接口
                if (waybill.IsVerify == Enums.Logistics.VerifyStates.Confirm && OldMoney.Value != Money.Value)
                {
                   
                    string s = $"订单编号：【{waybill.OrderNo}】；物流运单号：【{waybill.Express}】";
                    var b = OldMoney > Money ? true : false;
                    string msg = b == true ? "运费返还" : "运费补款";
                    var user = await _userManager.GetUserAsync(User);
                    var wallet = waybill.GetUsedWallet(company, user); // 钱包
                    //钱包扣款               
                    var absMoney = Math.Abs(OldMoney - Money);
                    if (!b)
                    {
                        if (!wallet.IsEnough(absMoney))
                        {
                            throw new Exception("钱包余额不足");
                        }

                        await wallet.Expense(absMoney);
                    } else
                    {
                        await wallet.Recharge(absMoney);
                    }
                    // await wallet.Expense(absMoney);
                    var billLog = wallet.AddLog(absMoney, b, Modules.WAYBILL, msg, s, waybill.StoreId, waybill.ID);
                    waybill.BillLog = billLog;
                }

                #region 修改数据-运单相关金额上报

                await _statisticMoney.ReportWayBillEdit(waybill);
                #endregion
                if (waybill.IsVerify != VerifyStates.Cancel)
                {
                    await _orderService.UpdateMergeOrderWaybill(0, null, waybill.OrderId);
                }

                await _dbContext.SaveChangesAsync();
                await transaction.CommitAsync();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw new Exception($"edit waybill Exception:{e.Message}");
            }           
        }
        catch (Exception e)
        {           
            return Error(e.Message);
        }
        return Success();
    }


    /// <summary>
    /// 物流详情
    /// </summary>
    /// <param name=""></param>
    /// <param name=""></param>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> ShowInfo(int Id, string? Express, bool Action, int Source)//: JsonResponse
    {
        var Info = Id > 0 ? await _waybillService.GetInfoById(Id) : await _waybillService.FindWayBillByWaybillOrder(Express);

        InfoItem item;
        if (Action = false && Source != 2)
        {
            item = new InfoItem(Info, null);
        }
        else
        {
            item = new InfoItem(Info, HttpContext.Session.GetUnitConfig());
        }
        var Commpany = _CompanyCache.Get(Info.CompanyID);
        var Rate = new object();
        if (Commpany is not null)
        {
            Rate = new
            {
                Commpany.WaybillRate,
                Commpany.WaybillRateType,
                Commpany.WaybillRateNum
            };
        }

        return Success(new ShowInfoVm(item, JsonConvert.DeserializeObject<List<WaybillProblems>?>(Info.Problem), _UnitCache.List(), Enum<Enums.Logistics.States>.ToDictionaryForUIByDescription(), await _WayBillLog.FindLog(Id), Rate));
    }

    /// <summary>
    /// 手动添加运单（订单首页方法，暂废）
    /// </summary>
    public ResultStruct HandAdd()
    {
        //        return error('无权访问，方法废弃');
        //    $data = request()->input();
        //    $add = $this->WayBillService->addWayBillAction($data);
        //        if (!$add) {
        //        return error('手动添加失败');
        //    }
        //    return success('手动添加成功');
        return Success();
    }

    public async Task<ResultStruct> MergeWaybillList(WayBills wayBills)
    {

        List<int> waybillIds = new();
        var result = await _orderService.GetOrdersByMergeID(wayBills.MergeID);
        if (!string.IsNullOrWhiteSpace(result.Item1))
        {
            return Error(result.Item1);
        }
        foreach (var order in result.Item2)
        {
            waybillIds.AddRange(await _waybillService.GetWaybillIDsByOrder(order.ID, order.MergeId));
        }
        var wids = waybillIds.Distinct().ToList();

        //获取对应订单备注信息
        var list = await _waybillService.GetMergeList(wids, wayBills);
        var data = list.Transform(a => new
        {
            a.ID,
            a.OrderNo,
            a.OrderId,
            a.Express,
            a.WaybillOrder,
            a.Tracking,
            CarriageNum = a.CarriageNum.ToString(Session.GetUnitConfig()),
            a.NationId,
            a.DeliverDate,
            a.StoreId,
            a.WaybillCompanyID,
            a.State,
            a.IsVerify,
            Remark = a.Remark ?? string.Empty,
            a.ImgUrl,
            a.ProductInfo,
            a.Logistic,
            a.LogisticExt,
            a.Sender,
            LogisticSign = a.Logistic.Platform.ToString(),
            a.CompanyID,
            a.ButtonName,
            a.CreatedAt,
            a.UpdatedAt,
            ShipMethod = a.ShipMethod,
            UserId = a.UserID,
        });
        // var res = await _orderService.GetOrderRemarks(data.Items.Select(a => a.OrderId).Distinct().ToList());
        // var remarks = res.Select(a => new
        // {
        //     a.ID,
        //     a.OrderId,
        //     a.Value,
        //     a.Truename,
        //     a.CreatedAt
        // }).GroupBy(a => a.OrderId).Select(g => new
        // {
        //     orderId = g.Key,
        //     list = g.ToList()
        // }).ToDictionary(a => a.orderId, a => a.list);

        return Success(new { data }, "成功");
    }

    /// <summary>
    /// 提交运单问题
    /// </summary>
    /// <param name=""></param>
    /// <param name=""></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> SendProblem(SendProblemVM request)
    {
        if (request.Problem.IsNullOrWhiteSpace()) return Error("运单问题不能为空");
        var waybill = await _waybillService.FindInfo(request.Id);
        List<WaybillProblems> problemList = JsonConvert.DeserializeObject<List<WaybillProblems>>(waybill.Problem ?? "[]") ?? new();
        WaybillProblems p = new()
        {
            Id = request.Id,
            Problem = request.Problem,
            User = Session.GetUserName(),
            Time = DateTime.UtcNow,
        };
        problemList.Add(p);
        waybill.Problem = JsonConvert.SerializeObject(problemList);
        await _waybillService.Update(waybill);
        return Success();
    }

    /// <summary>
    /// 获取货币单位
    /// </summary>
    [HttpPost]
    public async Task<ResultStruct> Currency()//: JsonResponse
    {
        var Unit = _UnitCache.List();
        return await Task.FromResult(Unit != null ? Success(Unit, "成功") : Error(message: "失败"));
    }

    /// <summary>
    /// 手动确认运单信息
    /// </summary>
    [Route("{id}"), /*Permission(WayBillManager.WaybillConfirm)*/]
    public async Task<ResultStruct> Confirm(int Id, [FromServices] FinancialAffairsService _FinancialAffairsService)
    {
        var sessionGroupID = Session.GetGroupID();
        var waybill = await _waybillService.GetInfoById(Id);
        if (waybill.IsVerify == VerifyStates.Confirm) return Error("运单已经确认");
        var user = await _userManager.GetUserAsync(User);
        logger.LogInformation("Confirm waybill sessionGroupID:" + sessionGroupID.ToString());
        logger.LogInformation("Confirm waybill user info:" + JsonConvert.SerializeObject(user));
        var company = await _CompanyService.GetInfoById(waybill.CompanyID);
        if (company is null) return Error("公司信息不存在");
        var Goods = JsonConvert.DeserializeObject<List<ProductInfoItem>?>(waybill.ProductInfo);
        if (Goods is null || Goods.Count<=0)
        {
            return Error("运单产品信息不能为空");
        }
        foreach (var item in Goods)
        {
            //TODO  可能有问题 stockoutnum,warehouseid 没有序列化进来  
            //检查是否有仓库
            if (item.WarehouseId > 0)
            {
                item.stockOutNum = item.stockOutNum > 0 ? item.stockOutNum : item.GoodInfos.SendNum;
            }
        }
        var Transaction = _dbContext.Database.BeginTransaction();

        try
        {
            var money = new Money(waybill.CarriageNum.Money.Value);
            var wallet = waybill.GetUsedWallet(company, user); // 钱包
            if (money.IsNotNull() && money!=0)
            {
                if (!wallet.IsEnough(money))
                {
                    throw new Exception("钱包余额不足");
                }
                // 取消变为确认 同步订单 
                if (waybill.IsVerify == VerifyStates.Cancel)
                {
                    #region 金额统计
                    await _statisticMoney.InitStatisticWithWaybill(waybill);
                    #endregion                   
                }
                string s = $"订单编号：【{waybill.OrderNo}】；物流运单号：【{waybill.Express}】";                        
               
                //钱包扣款
                await wallet.Expense(money);  
                var billLog = wallet.AddLog(money, false, Modules.WAYBILL, "运费扣款", s,waybill.StoreId,waybill.ID);
                waybill.BillLog = billLog;
            }

            //当前数据确认
            waybill.IsVerify = VerifyStates.Confirm;
            //增加运单日志
            waybill.AddLog(user, "确认运单信息");
            await _waybillService.Update(waybill);
            //同步订单
            await _orderService.UpdateMergeOrderWaybill(0, null, waybill.OrderId);

            var waybillCreatedEvent = new WaybillCreatedDomainEvent(waybill, Goods.Select(g => new StockOutGood(g.StockProductId, g.WarehouseId, g.stockOutNum)))
            {
                User = user
            };
            await _mediator.Publish(waybillCreatedEvent);
            Transaction.Commit();            
        }
        catch (Exception e)
        {
            Transaction.Rollback();
            logger.LogError("Confirm waybill error:" + e.Message);
            logger.LogError(e.StackTrace);
            return Error(e.Message);
            throw;
        }       
        return Success();
    }

    /// <summary>
    /// 语言
    /// </summary>
    /// <param name=""></param>
    /// <param name=""></param>
    /// <returns></returns>
    public ResultStruct EditLang(string lang /*Request $request*/)//: JsonResponse         
    {
        //$lang = $request->cookie();

        if (lang != "zh" && lang != "en")
        {
            lang = "zh";
        }
        lang += '/';                           //???
                                               //  session(['lang' => $lang]);
                                               //  return success();
        return Success(lang);
    }


    public class SwitchVM
    {
        public int ID { get; set; }
        public int Type { get; set; }
    }

    /// <summary>
    /// 切换运单上报状态
    /// 1. 普通用户使用个人
    /// 2. 分销上级使用个人
    /// 3. 分销下级根据分配的物流方式使用，钱包消耗方式(charge)
    /// </summary>
    public async Task<ResultStruct> Switch(SwitchVM vm)
    {
        var Info = await _waybillService.GetInfoById(vm.ID);
        if (Info == null)
        {
            return Error("logic.WayBill.waybillNotExists");
        }

        string Action;
        WayBillViewModel viewModel = new();
        if (vm.Type == 1)
        {
            Action = "运单上报上级处理";
            viewModel.WaybillCompanyId = HttpContext.Session.GetReportId();
        }
        else
        {
            Action = "运单上报撤回内部处理";
            viewModel.WaybillCompanyId = 0;
        }

        await _waybillService.updateData(vm.ID, viewModel);
        await _waybillService.AddLog(Info.OrderId, Info.ID, Info.Logistic, Action,new UserInfoDto(Session));

        return Success();
    }



    /// <summary>
    /// 财务统计
    /// </summary>
    //private async void FinancialStatistics(Company Compand, double Money, WayBillModel wayBill /*$company, $money, $info*/
    //[FromService] CounterGroupBusinessService _CompanyBusinessService
    //[FromService]CounterUserBusinessService _CounterUserBusinessService  [FromService]CounterStoreBusinessService _CounterStoreBusinessService
    //[FromService]CounterRealFinanceService _RealFinanceService [FromService]CounterCompanyBusinessService _CompanyBusinessService
    //)//Info?
    //{
    //    //公司 运费
    //    await _CompanyBusinessService.WaybillTotalAdd(Compand.ID, Money); //companyId total = 0.00
    //                                                                      //月报
    //    await _RealFinanceService.IncrementWaybillTotal(Compand, Money);
    //    //增加用户信息上报 distributionWaybillTotalAdd

    //    await _UserBusinessService.DistributionWaybillTotalAdd(wayBill.UserID, Money);   //$info['store_id'], $waybill_total = 0    
    //                                                                                     //增加店铺信息上报(手动添加运单的数据不存在商铺，上报时忽略店铺运费的统计)
    //    if (wayBill.StoreId <= 0 && !wayBill.StoreId.Equals(null))
    //    {
    //        await _StoreBusinessService.AddWaybillTotal(wayBill.StoreId, Money);//$store_id, $money
    //    }
    //    //    //增加用户组信息上报
    //    //  if ($this->sessionData->getGroupId() !== 0) {
    //    await _GroupBusinessService.DistributionWaybillTotal(Money);
    //    // }   
    //}


    //获取指定订单下，使用的物流信息
    [HttpPost]
    public async Task<ResultStruct> GetLogisticByWaybill(FeeVM order)
    {
        var logisticType = await _waybillService.GetLogisticByWaybill(order.Id);
        return logisticType.Data is not null ? Success(logisticType.Data, "成功") : Error(message: "失败");
    }


    /// <summary>
    /// 物流详情
    /// </summary>
    /// <param name=""></param>
    /// <param name=""></param>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> WaybillInfo(int Id)
    {
        if (Id > 0)
        {
            var res = await _waybillService.WaybillInfo(Id);
            return res.State ? Success(res.Data) : Error("search error");
        }
        else
        {
            return Error("parameter error");
        }
    }
}




