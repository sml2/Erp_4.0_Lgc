﻿using ERP.Services.Caches;
using NationCache = ERP.Services.Caches.Nation;
using Service = ERP.Services.DB.Logistics.ShipRatesEstimationTemplate;
using UnitCache = ERP.Services.Caches.Unit;
using ViewModel = ERP.ViewModels.Logistics.ShipRatesEstimationTemplate.EditShipRatesEstimationTemplate;

namespace ERP.Controllers.Logistics;
using ViewModels.Logistics.ShipRatesEstimationTemplate;

[Route("api/[controller]/[action]/{id?}")]
[ApiController]
public class ShipRatesEstimationTemplate : BaseController
{
    /// <summary>
    /// 运费估算
    /// </summary>
    private readonly Service _Service;
    private readonly UnitCache _UnitCache;
    private readonly NationCache _NationCache;
    private readonly PlatformData _platformDataCache;

    public ShipRatesEstimationTemplate(Service Service, UnitCache unitCache, NationCache nationCache, PlatformData platformDataCache)
    {
        _Service = Service;
        _UnitCache = unitCache;
        _NationCache = nationCache;
        _platformDataCache = platformDataCache;
    }
    /// <summary>
    /// 获取运费模版数据
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    [HttpGet]
    public ResultStruct GetList(string? name)
    {
        //$session = $this->getSessionObj();
        var Lists = _Service.GetList(name);
        var NationList = _NationCache.List();
        var AmazonNationList = _platformDataCache.List(Platforms.AMAZON).Select(x => x.GetAmazonData()).ToList();
        var UnitList = _UnitCache.List();
        return Success(new { Lists.Data, NationList, UnitList,AmazonNationList }, message: "成功");
    }
    /// <summary>
    /// 获取运费模版详情
    /// </summary>
    /// <param name="Id"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> Info(int Id)
    {
        //$session = $this->getSessionObj();       ->ERP3.0注释
        if (Id < 0)
        {
            return Error(message: "请选中编辑的列");
        }
        else
        {
            var nationList = _platformDataCache.List(Platforms.AMAZON).Select(x => x.GetAmazonData()).ToList();
            var unitList = _UnitCache.List();
            var data = await _Service.Info(Id);
            return data.State ? Success(new { data.Data, nationList, unitList }) : Error(message: "错误");
        }
    }
    [HttpPost]
    public async Task<ResultStruct> SubInfo(ViewModel ViewModel)
    {
        if (ViewModel == null)
        {
            return Error("");
        }

        if (ViewModel.ID > 0)
        {
            var Update = await _Service.Update(ViewModel);
            return Update.State ? Success("成功") : Error("失败");
        }
        else
        {
            if (_Service.GetTemplateNum() > 5)
            {
                return Error("模板不能超过5个");
            }
            var Add = await _Service.Create(ViewModel);
            return Add.State ? Success(message: "成功") : Error(message: "失败");
        }
    }


    [HttpPost]
    public async Task<ResultStruct> EditTemplateNation(data data)
    {
        return await _Service.EditTemplateNation(data) is true ? Success() : Error();
    }
    [HttpPost]
    public ResultStruct DelInfo(int ID)
    {
        if (ID < 0)
        {
            return Error("请重新选中");
        }
        var res = _Service.DeleteInfoById(ID);
        return res ? Success(message: "删除成功") : Error(message: "删除失败");
    }

    public async Task<ResultStruct> InsertInfo(ViewModel viewModel)
    {
        if (_Service.GetTemplateNum() > 5)
        {
            return Error("模板不能超过5个");
        }
        var Res = await _Service.Create(viewModel);
        return Res.State ? Success("添加成功") : Error("添加失败");
    }
    public async Task<ResultStruct> UpdateInfo(ViewModel viewModel)
    {
        var Res = await _Service.Update(viewModel);
        return Res.State ? Success("添加成功") : Error("添加失败");
    }
}
