global using Microsoft.AspNetCore.Mvc;
using System.Text.Json.Serialization;
using LogisticsParamService = ERP.Services.DB.Logistics.LogisticsParam;
using Model = ERP.Models.PlatformData.LogisticsData;
using LogisticsService = ERP.Services.Caches.Logistics;
using ERP.ViewModels.Logistics;
using static ERP.Models.DB.Logistics.Parameter;
using ERP.Enums.Logistics;
using System.Threading.Tasks;
using ERP.Attributes.Logistics;
using ERP.Authorization.Rule;
using ERP.Extensions;
using Newtonsoft.Json;
using ERP.Interface;
using CacheLogistics = ERP.Services.Caches.Logistics;
using ERP.Enums;
using ERP.Enums.Rule.Config;
using ERP.Enums.Rule.Menu;
using ERP.Enums.Identity;
using waybillService = ERP.Services.DB.Logistics.WayBill;


namespace ERP.Controllers.Logistics;
/// <summary>
/// 物流管理
/// </summary>
[Route("api/[controller]/[action]")]
[ApiController]
public class Logistics : BaseController
{

    private readonly LogisticsParamService _Service;
    private readonly LogisticsService LogisticsService;
    private readonly ISessionProvider _ISessionProvider;
    private readonly CacheLogistics _CacheLogistics;
    public Logistics(LogisticsParamService logisticsParamService, LogisticsService logisticsService, ISessionProvider sessionProvider, CacheLogistics cacheLogistics)
    {
        _Service = logisticsParamService;
        LogisticsService = logisticsService;
        _ISessionProvider = sessionProvider;
        _CacheLogistics = cacheLogistics;
    }
    private int CompanyID { get => _ISessionProvider.Session!.GetCompanyID(); }

    /// <summary>
    /// 当前公司物流列表 初始化信息
    /// </summary>
    [HttpGet]
    public async Task<ResultStruct> GetLogisticsInit()
    {
        object rule = new();
        int is_distribution = _ISessionProvider.Session!.GetIsDistribution() ? 1 : 0;
        if (!User.IsInRole(Role.Employee))
        {
            rule = new
            {
                logisticAdd = true,
                logisticDelete = true,
                logisticEdit = true,
                is_distribution
            };
        }
        else
        {
            rule = new
            {
                logisticAdd = true,       //$this->ruleInfo()->hasLogisticAdd(),
                logisticDelete = true,    //$this->ruleInfo()->hasLogisticEdit(),
                logisticEdit = true,      //$this->ruleInfo()->hasLogisticDelete(),
                is_distribution
            };
        }
        return await Task.FromResult(Success(new { logistics = _CacheLogistics.List(), rule }, "获取成功"));
    }
    /// <summary>
    /// 当前公司物流列表
    /// </summary>
    [HttpGet]
    public async Task<ResultStruct> GetList(Platforms? logistic_id, bool? state, string? customize_name)
    {
        var Res = await _Service.GetLogisticListPage(logistic_id, state, customize_name);
        return Res != null ? Success(Res, "成功") : Error(message: "null");
    }

    /// <summary>
    /// 当前公司物流登录信息列表
    /// </summary>
    [HttpPost]
    public async Task<ResultStruct> GetLogisticParamList()
    {
        var res = await _Service.GetLogisticParamList();
        return res.ToResultStruct();
    }

    [HttpGet]
    public IList<Model>? GetListsNoPage()
    {
        var info = _CacheLogistics.List();
        if (info != null) return info;

        return null;
    }
    /// <summary>
    /// 删除物流信息
    /// </summary>
    [HttpPost]
    public ResultStruct DelInfo(int id, [FromServices] waybillService waybill)
    {
        //检查权限
        //if ($this->->getConcierge() === UserModel::TYPE_CHILD && !$this->ruleInfo()->hasLogisticDelete()) {
        //    return error(getlang('public.authfailed'));
        //}
        if (User.IsInRole(Role.Employee))
        {
            return Error("没有权限");
        }
        var logisticsParam = _Service.GetLogisticsInfo(id);
        var CompanyId = _ISessionProvider.Session!.GetCompanyID();
        //安全验证
        if (logisticsParam == null || !logisticsParam.CompanyID.Equals(CompanyId))
        {
            return Error("物流不存在");
        }
        else
        {
            if(waybill.GetWaybillByLogisticParamID(id))
            {
                return Error("该物流已经被使用，暂时不能被删除");
            }
            else
            {
                return _Service.Del(id) ? Success("删除成功") : Error("删除出错");
            }
            
        }
    }

    /// <summary>
    /// 内部添加物流配置参数信息
    /// </summary>
    /// <param name="logisticsParam"></param>
    /// <returns></returns>
    // [HttpPost, Permission(Enums.Rule.Config.Waybill.LOGISTIC_ADD)]
    public async Task<ResultStruct> SubAddLogistics(ERP.Models.Logistics.ViewModels.Parameter.EditModel LogistVm)
    {
        if (LogistVm.Data is null)
        {
            return Error("请填写");
        }
        var Res = await _Service.AddLogisticParam(LogistVm);
        return Res.State ? Success("成功") : Error("失败");
        //return Success();
    }
    /// <summary>
    /// 获取编辑需要信息
    /// </summary>
    [HttpPost]
    public async Task<ResultStruct> GetEditInfo(int Id)
    {
        //检查权限
        //if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->ruleInfo()->hasLogisticEdit()) {
        //    return error(getlang('public.authfailed'));
        //}
        var logisticsParam = _Service.GetLogisticsInfo(Id);

        if (logisticsParam == null || !logisticsParam.CompanyID.Equals(CompanyID))
        {
            return Error("物流不存在");
        }
        else
        {
            GetEditInfo Data = new()
            {
                ID = logisticsParam.ID,
                Source = logisticsParam.Source,
                CustomizeName = logisticsParam.CustomizeName,
                Remark = logisticsParam.Remark,
                State = logisticsParam.Opened,

            };

            //内部添加物流
            if (logisticsParam.Source == Sources.JUNIOR)
            {
                var Logistics = _CacheLogistics.Get(logisticsParam.Platform.Platform);
                //var formListItems=await FontEndRenderingService.GetParameterById(logisticsParam.LogisticsID);

                if (Logistics is not null)     //Any 判断
                {
                    Data.Param = logisticsParam.Data ?? string.Empty;
                    Data.Logistics = Logistics;
                    Data.Config = Logistics.GetParameterConfigCache();
                }
                else
                {
                    return await Task.FromResult(Error("物流公司错误"));
                }
            }
            return await Task.FromResult(Success(Data));
        }
    }
    /// <summary>
    /// 更新内部物流绑定参数
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public ResultStruct SubEditLogistics(EditInfo editInfo)
    {
        //检查权限
        //if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->ruleInfo()->hasLogisticEdit()) {
        //    return error(getlang('public.authfailed'));
        //}
        //过滤上级分配不修改参数

        if (editInfo.Source == Sources.JUNIOR)
        {
            if (string.IsNullOrEmpty(editInfo.Param))
            {
                return Error("参数错误");
            }
            else
            {
                try
                {
                    var r1 = _Service.EditLogisticParam(editInfo);
                    var r2 = _Service.updateAllot(editInfo);
                    return r1 && r2 ? Success() : Error();
                }
                catch (Exception)
                {

                    return Error("错误");
                }
                //DB::beginTransaction();
            }
        }
        else
        {
            var r = _Service.EditLogisticParam(editInfo);
            return r ? Success() : Error();
        }
    }



    public object? GetLogisticsPlatform(int ID)
    {
        if (ID > 0)
        {
            return _CacheLogistics.Get(ID)!;
        }
        return null;

    }

    [HttpPost]
    public async Task<ResultStruct> GetParameters(Platforms platform)
    {
        var Res = _CacheLogistics.Get(platform);
        return await Task.FromResult(Res is not null ? Success(new { Res.Platform, Config = Res.GetParameterConfigCache() }, message: "成功") : Error(message: "失败"));
    }

    [HttpGet]
    public async Task<ResultStruct> getDefaultLogisticsConfig()
    {
        var Res = _CacheLogistics.List()!.FirstOrDefault();
        return await Task.FromResult(Res is not null ? Success(new
        {
            Res.Platform,
            Config = Res.GetParameterConfigCache(),
        }, message: "成功") : Error(message: "失败"));
    }


}
