﻿using ERP.Interface;
using ERP.Models.DB.Logistics;
using SenderService = ERP.Services.DB.Logistics.Shipper;
using LogisticsShipperModel = ERP.Models.View.Logistics.ShipidOrder.LogisticsVmShipper;

namespace ERP.Controllers.Logistics;
/// <summary>
/// 寄件信息管理
/// </summary>

public class Sender : BaseController
{

    private readonly SenderService SenderService;
    private readonly ISessionProvider _ISessionProvider;
    public Sender(SenderService service, ISessionProvider sessionProvider)
    {
        SenderService = service;
        _ISessionProvider = sessionProvider;
    }
    /// <summary>
    /// * 寄件信息列表页
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> senderIndex(string? name)
    {
        //$company_id = $this->sessionData->company_id;
        var res = await SenderService.getList(name);
        return Success(res, "成功");
    }
    [HttpPost]
    public async Task<ResultStruct> SenderIndexNPage(string? name)
    {
        var Res = await SenderService.GetListNoPage(name);
        return Res.State ? Success(new { Res!.Data }, "成功") : Error(message: "失败");
    }
    /// <summary>
    /// 删除寄件信息
    /// </summary>
    /// <param name="Id"></param>
    /// <returns></returns>
    [HttpPost]
    [Route("{id}")]
    public async Task<ResultStruct> DelSender(int Id)
    {
        //检查权限
        //if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->sessionData->getRule()->getWaybill()->getBiter()->hasWaybillSenderDelete()) {
        //    return error(getlang('public.authfailed'));
        //}
        return SenderService.DelInfo(Id) ? Success() : Error("物流不存在");
    }
    /// <summary>
    /// 修改/添加寄件人信息
    /// </summary>
    /// <param name="ID"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> editSenderInfo(LogisticsShipperModel? Shipper)
    {
        //if(!$this->sessionData->getRule()->getWaybill()->getBiter()->hasWaybillSenderEdit()) {
        //    return error(getlang('public.authfailed'));
        //}
        //$data = $request->validated();
        var res = await SenderService.sendInfo(Shipper);
        return res.State ? Success() : Error();
    }

}

