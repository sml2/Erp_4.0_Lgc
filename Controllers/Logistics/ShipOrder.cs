﻿using ERP.Data;
using ERP.ViewModels;
using ERP.ViewModels.Logistics.ShipidOrder;
using System.Text.RegularExpressions;
using ShipOrderService = ERP.Services.DB.Logistics.ShipOrder;
using ShipTemplateService = ERP.Services.DB.Logistics.ShipTemplate;
using UnitCache = ERP.Services.Caches.Unit;

namespace ERP.Controllers.Logistics;
using AmazonOrderModel = Models.DB.Orders.Order;


[Route("api/[controller]/[action]")]
[ApiController]
public class ShipOrder : BaseController
{
    private readonly ShipOrderService _Service;
    private readonly DBContext _MyDbContext;
    private readonly Services.DB.Orders.Order _AmazonService;
    private readonly ShipTemplateService _ShipTemplateService;
    private readonly UnitCache _UnitCache;

    public ShipOrder(ShipOrderService service, Services.DB.Orders.Order amazonService, ShipTemplateService shipTemplate, DBContext MyDbContext, UnitCache unitCache)
    {
        _UnitCache = unitCache;
        _MyDbContext = MyDbContext;
        _Service = service;
        _AmazonService = amazonService;
        _ShipTemplateService = shipTemplate;
    }

    public async Task<ResultStruct> GetInfo(int Id)
    {
        var data = await _Service.GetInfo(Id);
        return data != null ? Success(data, "成功") : Success(message: "失败");
    }

    /// <summary>
    /// 添加批量待发货订单
    /// </summary>
    /// <param name="IDDto"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> JoinShipOrder(IdDto IDDto)
    {
        //throw new Exceptions.Git.Merge_26a159902ba76edfea0ec15663b0da07529b4f9c();
        int OrderCount = 0;
        var list = await _Service.GetList();
        if (list != null)
        {
            OrderCount = list.Where(x => x.OrderId == IDDto.ID).Count();
            if (OrderCount > 0) return Error(false, "订单已存在");
        }
        var order = await _AmazonService.GetOrderByIdAsync(IDDto.ID);
        if (!order.HasGoods())
        {
            return Warning($"{order.OrderNo}商品为空");
        }
        order.IsBatchShip = true;
        var transaction = await _MyDbContext.Database.BeginTransactionAsync();
        try
        {
            await _AmazonService.Update(order);
            await _Service.AddShipOrder(order);
            transaction.Commit();
        }
        catch
        {
            transaction.Rollback();
            return Error("添加失败");
        }
        return Success();
    }

    [HttpGet]
    public async Task<ResultStruct> GetList()
    {
        //$rule = $this->sessionData->getRule()->getWaybill()->getBiter()->hasShipTemplate();
        var shipTemplate = await _ShipTemplateService.GetFullList();
        var allRates = _UnitCache.List()!.ToDictionary(x => x.Sign, x => x);
        var shipOrders = await _Service.GetList();
        var Rule = true;
        return Success(new { shipTemplate = shipTemplate, shipOrder = shipOrders, Rule, Rates = allRates, shipTemplatelist = shipTemplate });
    }

    public class ShopDel
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
    }
    /// <summary>
    /// 移除批量待发货订单
    /// </summary>
    /// <param name="Id"></param>
    /// <param name="OrderId"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> DelInfo(ShopDel shopDel)
    {
        AmazonOrderModel? amazonOrderModel = await _AmazonService.GetOrderById(shopDel.OrderId);
        if (amazonOrderModel != null)
        {
            amazonOrderModel.IsBatchShip = false;
            if (await _AmazonService.Update(amazonOrderModel) && await _Service.delShipOrder(shopDel.Id))
                return Success("完成");
        }
        return Error("请重试");
    }

    public record BindShip(int ShipTemplateId, int[] Ids);
    /// <summary>
    /// 批量待发货订单绑定发货模版
    /// </summary>
    /// <param name = "shipTemplateId" ></ param >
    /// < param name="Ids"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> BindShipTemplate(BindShip bindShip)
    {
        var ShipTemplates = await _ShipTemplateService.GetInfo(bindShip.ShipTemplateId);
        if (ShipTemplates is null)
        {
            return Error("模版不存在");
        }
        var res = await _Service.UpdateOrders(bindShip.Ids, default, default, ShipTemplates.ID);
        return res.State ? Success("完成") : Error("请重试");
    }

    /// <summary>
    /// 批量覆盖待发货订单数据申报价格
    /// </summary>
    /// <param name = "Ids" ></ param >
    /// < param name="Declare"></param>
    /// <param name = "DeclareUnit" ></ param >
    /// < returns ></ returns >
    [HttpPost]
    public async Task<ResultStruct> batchUpdateDeclare(ShipTemplateVM? Temp)
    {
        //decimal declare = DeclareUnit=="CNY"?10000:_UnitCache.List()!.Where(x => x.Sign == DeclareUnit).Select(x => x.Rate).FirstOrDefault();
        var Res = await _Service.UpdateOrders(Temp!.Ids, decimal.MinValue, Temp.DeclareUnit, 0);
        return Res.State ? Success(true) : Error(false);
    }
    public class ChangeOrderUnit
    {
        public int ID { get; set; }
        public string Param { get; set; } = string.Empty;
        public string? Value { get; set; }

        public override string ToString()=> $"ID:{ID},Param:{Param},Value:{Value}";
    }    /// <summary>
    /// 更新批量待发货物流附加参数
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="Param"></param>
    /// <param name="Value"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> ChangeOrder(ChangeOrderUnit changeOrderUnit)
    {
        string Param = changeOrderUnit.Param;
        var Row = await _Service.GetInfo(changeOrderUnit.ID);
        if (Row is null)
        {
            return Error("无权访问");
        }
        if(changeOrderUnit.Value is null) {//此处最好走ViewModel验证方法
            return Error("单位为空");
        }
        var Value = changeOrderUnit.Value!;
        var RowProperties = Row.GetType().GetProperties();
        if (RowProperties is not null && RowProperties.Length > 0)
        {
            foreach (var item in RowProperties!)
            {
                if (item is not null) {
                    if (item.Name.Equals(Param,StringComparison.CurrentCultureIgnoreCase))
                    {
                        var p = item.PropertyType;
                        if (p.Equals(typeof(int)) && int.TryParse(Value, out var i))
                        {
                            _MyDbContext.Entry(Row).Property(item.Name).CurrentValue = i;
                        }
                        else if (p.Equals(typeof(string)))
                        {
                            _MyDbContext.Entry(Row).Property(item.Name).CurrentValue = Value;
                        }
                        else if (p.Equals(typeof(bool)) && bool.TryParse(Value, out var b))
                        {
                            _MyDbContext.Entry(Row).Property(item.Name).CurrentValue = b;
                        }
                        else if (p.Equals(typeof(Money)) && decimal.TryParse(Value, out var d))
                        {
                            _MyDbContext.Entry(Row).Property(item.Name).CurrentValue = d;
                        }
                        else {
                            return Error($"{changeOrderUnit}","无法进行数据的转换，请联系客服");
                        }
                    }
                    else {
                        //小概率异常
                        return Error("反射异常,枚举过程中出错");
                    }
                }
            }
            _MyDbContext.Update(Row);
            return _MyDbContext.SaveChanges() > 0 ? Success("完成") : Error("请重试");
        }
        else {
            //小概率异常
            return Error("反射异常,枚举出错");
        }
    }
}
