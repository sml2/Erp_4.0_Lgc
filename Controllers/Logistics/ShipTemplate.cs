using ERP.ViewModels.Store.Remote;
using Microsoft.AspNetCore.Mvc;
using static ERP.Models.Abstract.BaseModel;
using Model = ERP.Models.Logistics.ShipTemplate;
using ShipTemplateService = ERP.Services.DB.Logistics.ShipTemplate;
using ShipTemplateViewModel = ERP.ViewModels.Logistics.EditShipTemplate;

namespace ERP.Controllers.Logistics
{
    [Route("api/[controller]/[action]/{id?}")]
    [ApiController]
    /// <summary>
    /// 发货
    /// </summary>
    public class ShipTemplate : BaseController
    {
        #region ERP3.0 PHPService引用及签名
        //    /// <summary>
        //    /// 获取发货模版数据
        //    /// </summary>
        //    /// <param name=""></param>
        //    /// <param name=""></param>
        //    /// <returns></returns>
        //    public object getList(/*Request $request*/)//: JsonResponse
        //    {
        //        //    $name = $request->input('name');
        //        //    $state = $request->input('state');
        //        //    $list = $this->service->getList($state, $name);

        //        //    return success('', ['list' => $list]);
        //        return 1;
        //    }

        //    /// <summary>
        //    /// 获取模版详情
        //    /// </summary>
        //    /// <param name=""></param>
        //    /// <param name=""></param>
        //    /// <returns></returns>
        //    public object info(/*Request $request*/)
        //    {
        //        //    $id = $request->input('id');
        //        //    if (!$id) {
        //        //        return error(getlang('public.missingparam', ['param' => 'ID']));
        //        //    }

        //        //    $info = $this->service->getInfoById($id);
        //        //    return success('', $info);
        //        return 1;
        //    }

        //    /// <summary>
        //    /// 保存提交
        //    /// </summary>
        //    /// <param Request=""></param>
        //    /// <returns></returns>
        //    public object subInfo(/*Request $request*/)
        //    {
        //        //    $id = $request->input('id');
        //        //    $data = $request->input('info');
        //        //    if (!$data) {
        //        //        return error(getlang('public.missingparam', ['param' => 'Info']));
        //        //    }
        //        //    if ($id) {
        //        //        try
        //        //        {
        //        //            $this->updateInfo($id, $data);
        //        //            $res = true;
        //        //        }
        //        //        catch (Exception $e) {
        //        //            $res = false;
        //        //        }
        //        //        } else
        //        //        {
        //        //        $res = $this->insertInfo($data);
        //        //        $res = $res['state'];
        //        //        }
        //        //        return $res? success() : error();
        //        return 1;
        //    }

        //    /// <summary>
        //    /// 新增发货模版
        //    /// </summary>
        //    /// <param requestData=""></param>
        //    /// <returns></returns>
        //    private object insertInfo(/*$requestData*/)//: array
        //    {
        //        //    //查询当前用户模版数量
        //        //    $num = $this->service->getShipTemplateNum();
        //        //        if ($num > 50) {
        //        //            return returnArr(false, getlang('logic.ShipTemplate.hasNum'));
        //        //        }
        //        //    $res = $this->service->insertShipTemplate($requestData);
        //        //        return returnArr($res);
        //        //    }

        //        //    更新发货模版
        //        //    private function updateInfo($id, $requestData): array
        //        //{
        //        //    $res = $this->service->updateShipTemplate($id, $requestData);
        //        //        return returnArr($res);
        //        return 1;
        //    }

        //    /// <summary>
        //    /// 删除
        //    /// </summary>
        //    /// <param id=""></param>
        //    /// <returns></returns>
        //    public object delInfo(/*Request $request*/)/*: JsonResponse*/
        //    {
        //        //    $id = $request->input('id');

        //        //        if (!$id) {
        //        //            return error(getlang('public.missingparam', ['param' => 'ID']));
        //        //        }
        //        //    $res = $this->service->deleteInfoById($id);
        //        //        return $res? success() : error();
        //        return 1;
        //    }

        #endregion

        #region PHP->C#
        private readonly ShipTemplateService _Service;

        public ShipTemplate(ShipTemplateService shipTemplateService)
        {
            _Service = shipTemplateService;
        }
        /// <summary>
        /// 获取发货模版数据
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="State"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResultStruct> getList(string? Name, StateEnum? State)
        {
            var Res = await _Service.GetList(Name, State);
            return Res.State ? Success(Res.Data!) : Error(message: "失败");
        }

        [HttpGet]
        public async Task<ResultStruct> getListNPage(string? Name, StateEnum State)
        {
            var Res = await _Service.GetListNPage(Name, State);
            return Res.State ? Success(Res.Data!, "成功") : Error(message: "失败");
        }


        [HttpPost]
        public async Task<ResultStruct> SetSort(SetSortDto setSetSorts)
        {
            try
            {
                await _Service.UpdateSort(setSetSorts);
                return Success();
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }
        /// <summary>
        /// 获取模版详情
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>.
        [HttpPost]
        public async Task<ResultStruct> Info(int ID)
        {
            if (ID<0)
            {
                return Error("");
            }
            var Info =await _Service.GetInfoById(ID);
            return Info.State ? Success(Info.Data!,"") : Error("");
            
        }

        /// <summary>
        /// 保存提交
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="Info"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResultStruct> SubInfo(ShipTemplateViewModel? Info)//{"ID":10,"info":{"ID":0,"Name":"xxxx"}}
        {
            if (Info!.Id>0)
            {
                var Res = await _Service.UpdateShipTemplate(Info);
               return Res.State ? Success() : Error();
            }
           else
            {
                var Count = _Service.GetShipTemplateNum();
                if (Count > 50)
                {
                    return Error("不能大于50");
                }
                if (Info == null)
                {
                    return Error("请输入");
                }
                return _Service.InsertShipTemplate(Info).State ? Success() : Error();
            }
        }
        /// <summary>
        /// 更新发货模版
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResultStruct> updateInfo(ShipTemplateViewModel Info)
        {
            return await Task.FromResult(Success());//SubInfo

        }
        /// <summary>
        /// 新增发货模版
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ResultStruct insertInfo(ShipTemplateViewModel? model)
        {
            return Success();//SubInfo
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultStruct DelInfo(int ID)
        {
            if (ID < 0)
            {
                return Error(message: "ID为空");
            }
            return _Service.DeleteInfoById(ID) == true ? Success("成功") : Error(message: "错误");
        }
    }
    #endregion
}
