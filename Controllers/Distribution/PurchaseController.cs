using ERP.Enums.Identity;
using ERP.Authorization.Rule;
using ERP.Data;
using ERP.Distribution.Abstraction.Services;
using ERP.Enums.Orders;
using ERP.Enums.Purchase;
using ERP.Extensions;
using ERP.Models.DB.Users;
using ERP.Services.Distribution;
using ERP.ViewModels.Order.AmazonOrder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Ryu.Linq;

namespace ERP.Controllers.Distribution;
[Route("api/distribution/[controller]/[action]")]
[ApiController]
public class PurchaseController : BaseController
{
    private readonly PurchaseService _purchaseService;
    private readonly IDistributionCompanyService _companyService;

    private readonly UserManager<Models.DB.Identity.User> _userManager;

    public PurchaseController(PurchaseService purchaseService, IDistributionCompanyService companyService, UserManager<Models.DB.Identity.User> userManager)
    {
        _purchaseService = purchaseService;
        _companyService = companyService;
        _userManager = userManager;
    }

    /// <summary>
    /// 获取待采购配置项
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetWaitSearchList()
    {
        var user = await _userManager.GetUserAsync(User);
        //获取当前上报公司数据
        var company = await _companyService.GetValidDistributionList(user.CompanyID);
        //订单状态
        var state = OrderState.SearchState;
        //订单进度
        // var progress = AmazonOrderModel::ORDER_PROGRESS Enum<AmazonOrder.OrderStates>;
        //订单排序
        var sortBy = Enum<SortBy>.ToDictionaryForUIByDescription();

        return Success(new
        {
            company, state, sortBy
        });
    }

    public record WaitListQuery(string? OrderNo, int? CompanyId, ProgressMaskValue? progress, Enums.Orders.States? OrderState, int? SortBy);
    
    /// <summary>
    /// 获取分销待采购列表信息
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> GetWaitList([FromQuery]WaitListQuery query, [FromServices] AmazonOrderService amazonOrderService)
    {
        var user = await _userManager.GetUserAsync(User);
        var list = await amazonOrderService.GetWaitListPage(user.CompanyID, query.OrderNo, query.OrderState, query.CompanyId, 1, query.SortBy);
        return Success(new { list });
    }

    /// <summary>
    /// 获取分销采购中列表信息
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetHasList(string? orderNo, int? companyId)
    {
        var user = await _userManager.GetUserAsync(User);
        var list = await _purchaseService.GetListPage(user.CompanyID, orderNo, PurchaseStates.INPROCUREMENT, companyId);
        return Success(new { list });
    }

    /// <summary>
    /// 获取采购中配置项
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetHasSearchList()
    {
        var user = await _userManager.GetUserAsync(User);

        //获取当前上报公司数据
        var company = await _companyService.GetValidDistributionList(user.CompanyID);
        
        return Success(new
        {
            company,
            company_id = user.CompanyID,
        });
    }

    /// <summary>
    /// 获取采购列表信息
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetList(string? orderNo, int? companyId, PurchaseStates? purchaseState)
    {
        var user = await _userManager.GetUserAsync(User);
        var list = await _purchaseService.GetListPage(user.CompanyID, orderNo, purchaseState, companyId);
        return Success(new { list });
    }

    /// <summary>
    /// 获取采购列表配置项
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetSearchList()
    {
        var user = await _userManager.GetUserAsync(User);
        //获取当前上报公司数据
        var company = await _companyService.GetValidDistributionList(user.CompanyID);
        var purchaseState = Enum<PurchaseStates>.ToDictionaryForUI();
        
        return Success(new { company, purchaseState, CompanyId = user.CompanyID });
    }

    /// <summary>
    /// 获取我的采购列表信息
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetMyList(string? orderNo, int? companyId, PurchaseStates? purchaseState)
    {
        var user = await _userManager.GetUserAsync(User);
        var list = await _purchaseService.GetListPage(user.CompanyID, orderNo, purchaseState, companyId, user.Id);

        return Success(new { list });
    }

    /// <summary>
    /// 获取我的采购列表配置项
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetMySearchList()
    {
        var user = await _userManager.GetUserAsync(User);
        //获取当前上报公司数据
        var company = await _companyService.GetValidDistributionList(user.CompanyID);
        var purchaseState = Enum<PurchaseStates>.ToDictionaryForUI();
        
        return Success(new { company, purchaseState, CompanyId = user.CompanyID });
    }

    /// <summary>
    /// 上级分销采购详情数据
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> Info(int id, bool isEdit = false)
    {
        var purchase = await _purchaseService.GetInfoById(id);
        if (purchase is null) {
            return Error(message: "暂无数据");
        }

        var user = await _userManager.GetUserAsync(User);
        string unit = (isEdit ? purchase.PurchaseUnit : user.UnitConfig)!;
        var info = new
        {
            purchase.ID,
            purchase.Url,
            purchase.Name,
            purchase.Num,
            purchase.FromUrl,
            purchase.PurchaseAffirm,
            purchase.PurchaseState,
            purchase.PurchaseOrderNo,
            purchase.PurchaseTrackingNumber,
            purchase.Remark,
            ProductTotal = purchase.ProductTotal.ToString(user.UnitConfig, false),
            purchase.Asin,
            PurchaseUnit = unit,
            Price = purchase.Price.ToString(unit, false),
            PurchasePrice = purchase.PurchasePrice.ToString(unit, false),
            PurchaseOther = purchase.PurchaseOther.ToString(unit, false),
            PurchaseTotal = purchase.PurchaseTotal.ToString(unit, false),
            // PurchaseTotal = purchase..ToString(unit, false),
            purchase.PurchaseRate,
        };
        //采购日志
        var log = await _purchaseService.GetAllLog(id);

        //对下级公司采购手续费率
        var company = await _companyService.GetInfoById(purchase.CompanyID);
        var rate = new object();
        if (company != null)
        {
            rate = new
            {
                company.PurchaseRate,
                company.PurchaseRateType,
                company.PurchaseRateNum,
            };
        }
        
        return Success(new { info, log, rate});
    }

    //    /*************************************** 分销下级 *********************************************/

    /// <summary>
    /// 获取待采购列表
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetJuniorWaitList(string? orderNo, int? storeId, ProgressMaskValue? progress, Enums.Orders.States? orderState, string? sortBy
        , [FromServices] AmazonOrderService amazonOrderService, [FromServices] Services.Caches.Store storeCache)
    {
        var user = await _userManager.GetUserAsync(User);
        var storeIds = Array.Empty<int>();
        if (storeId is null && !User.IsInRole(Role.ADMIN) && !User.Has(Enums.Rule.Config.Distribution.PURCHASE_STORE)) 
        {
            var store = storeCache.List(user);
            //员工无可允许查看店铺，返回空信息
            if (store is { Count: <= 0 }) {
                return Success(new { List = new {} });
            }
            storeIds = store.Select(s => s.ID).ToArray();
        }

        var list = await amazonOrderService.GetJuniorWaitListPage(user.CompanyID, user.Company.ReportId,orderNo, orderState, progress, storeId, storeIds, 1, sortBy);
        return Success(new { list });
    }

    /// <summary>
    /// 获取待采购配置项
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetJuniorWaitSearchList([FromServices] Services.Caches.Store storeCache)
    {
        var user = await _userManager.GetUserAsync(User);

        var rule =  new
        {
            showBack = user.Company.DistributionPurchase != Company.DistributionPurchases.TRUE
        };

        //公司管理查看公司下全部店铺
        var stores = User.IsInRole(Role.ADMIN) || User.Has(Enums.Rule.Config.Distribution.PURCHASE_STORE)
            ? storeCache.List(user.Company) : storeCache.List(user);
        var store = stores.Select(s => new {s.ID, s.Name, s.State}).KeyBy(s => s.ID);

        //订单状态
        var state = Enum<Enums.Orders.States>.ToDictionaryForUI();
        //订单进度
        // var progress = Enum<OrderProgress>;
        //订单排序
        // $data['sort_by'] = AmazonOrderModel::ORDER_SORT_BY;

        return Success(new
        {
            rule,
            state,
            store,
        });
    }

    /// <summary>
    /// 下级待采购信息退回内部处理
    /// </summary>
    /// <returns></returns>
    [HttpPost("{id:int}"), Permission(Enums.Rule.Config.Distribution.BACK_PURCHASE)]
    public async Task<ResultStruct> BackJuniorPurchase(int id, [FromServices] AmazonOrderService amazonOrderService)
    {
        var user = await _userManager.GetUserAsync(User);
        
        var res = await amazonOrderService.ToBack(id, user.CompanyID, user.Company.ReportId);

        return res > 0 ? Success() : Error();
    }

    /// <summary>
    /// 下级待采购信息上报内部处理
    /// </summary>
    /// <returns></returns>
    [HttpPost("{id:int}")]
    public async Task<ResultStruct> ReportJuniorPurchase(int id, [FromServices] AmazonOrderService amazonOrderService)
    {
        var user = await _userManager.GetUserAsync(User);
        var res = await amazonOrderService.ToReport(id, user.CompanyID, user.Company.ReportId);

        return res > 0 ? Success() : Error();
    }

    /// <summary>
    /// 获取采购中列表
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetJuniorHasList(string? orderNo, int? storeId, [FromServices] Services.Caches.Store storeCache)
    {
        var user = await _userManager.GetUserAsync(User);
        var storeIds = Array.Empty<int>();
        if (storeId is null && !User.IsInRole(Role.ADMIN) && !User.Has(Enums.Rule.Config.Distribution.PURCHASE_STORE)) 
        {
            var store = storeCache.List(user);
            //员工无可允许查看店铺，返回空信息
            if (store is { Count: <= 0 }) {
                return Success(new { List = new {} });
            }
            storeIds = store.Select(s => s.ID).ToArray();
        }

        var list = await _purchaseService.GetJuniorListPage(user.CompanyID, user.Company.ReportId, orderNo, PurchaseStates.INPROCUREMENT, storeId, storeIds);
        return Success(new { list });
    }

    /// <summary>
    /// 获取采购中配置项
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetJuniorHasSearchList([FromServices] Services.Caches.Store storeCache)
    {
        var user = await _userManager.GetUserAsync(User);
        //当前用户可选店铺
        var store = User.IsInRole(Role.ADMIN) || User.Has(Enums.Rule.Config.Distribution.PURCHASE_STORE) 
            ? storeCache.List(user.Company) : storeCache.List(user);
        
        return Success(new { user.CompanyID, store });
    }

    /// <summary>
    /// 获取采购列表
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetJuniorList(string orderNo, int? storeId, PurchaseStates? purchaseState, [FromServices] Services.Caches.Store storeCache)
    {
        var user = await _userManager.GetUserAsync(User);
        var storeIds = Array.Empty<int>();
        if (storeId is null && !User.IsInRole(Role.ADMIN) && !User.Has(Enums.Rule.Config.Distribution.PURCHASE_STORE)) 
        {
            var store = storeCache.List(user);
            //员工无可允许查看店铺，返回空信息
            if (store is { Count: <= 0 }) {
                return Success(new { List = new {} });
            }
            storeIds = store.Select(s => s.ID).ToArray();
        }

        var list = await _purchaseService.GetJuniorListPage(user.CompanyID, user.Company.ReportId, orderNo, purchaseState, storeId, storeIds);
        return Success(new { list });
    }

    /// <summary>
    /// 获取采购列表配置项
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetJuniorSearchList([FromServices] Services.Caches.Store storeCache)
    {
        var user = await _userManager.GetUserAsync(User);
        //当前用户可选店铺
        var store = User.IsInRole(Role.ADMIN) || User.Has(Enums.Rule.Config.Distribution.PURCHASE_STORE) 
            ? storeCache.List(user.Company) : storeCache.List(user);
        
        var purchaseState = Enum<PurchaseStates>.ToDictionaryForUI();
        return Success(new { purchaseState, user.CompanyID, store, });
    }

    /// <summary>
    /// 下级采购分销产品确认
    /// </summary>
    /// <returns></returns>
    public async Task<object> Confirm(int[] ids, [FromServices] DBContext dbContext)
    {
        var purchases = await dbContext.Purchase.Where(p => ids.Contains(p.ID) && p.PurchaseState != PurchaseStates.PURCHASED
                                                                         && p.PurchaseState != PurchaseStates.REFUSED)
            // ->select(['id', 'purchase_total', 'company_id', 'order_id', 'group_id', 'purchase_user_id'])
            .ToListAsync();
        if (purchases is { Count: <= 0 }) {
            return Error(message: "无效数据#1");
        }
        var purchaseFirst = purchases.First();
        var company = await _companyService.GetInfoById(purchaseFirst.CompanyID);
        var order = await dbContext.Order.FindAsync(purchaseFirst.OrderId);
        if (order is null) {
            return Error(message: "无效数据#2");
        }

        var total = purchases.Sum(p => p.PurchaseTotal);
        if(company is null)
        {
            return Error(message: "无效数据#3");
        }
        if (!company.CheckPublicWalletEnough(total.Money)) {
            return Error(message: "当前余额不足，请先提醒对方充值钱包金额");
        }

        //        DB::beginTransaction();
        //        try {
        //            $bill_log_id = $company->expenseWalletOnPurchase($total, $company, $purchaseFirst->group_id, '订单编号：【' . $order->order_no . '】；');

        //            PurchaseModel::query()->whereIn('id', $purchases->pluck('id'))->update([
        //                'bill_log_id' => $bill_log_id,
        //                'purchase_state' => PurchaseModel::STATE_PURCHASED
        //            ]);

        //            $count = PurchaseModel::query()->whereOrderId($order->id)
        //                ->where('purchase_state', PurchaseModel::STATE_INPROCUREMENT)
        //                ->whereNotIn('id', $purchases->pluck('id'))
        //                ->count();
        //            if ($count === 0) {
        //                AmazonOrderModel::whereId($order->id)->update([
        //                    'purchase_audit' => AmazonOrderModel::PURCHASE_AUDIT_FALSE
        //                ]);
        //            }


        //            // TODO 统计上报
        //            $CounterUserBusinessService = new CounterUserBusiness();
        //            $CounterCompanyBusinessService = new CounterCompanyBusiness();
        //            $CounterRealFinance = new CounterRealFinance();
        //            $CounterUserBusinessService->orderPurchaseNumAdd(1, 1, $total, $total, $purchaseFirst->purchase_user_id);
        //            if ($this->sessionData->getGroupId() !== 0) {
        //                $CounterGroupBusiness = new CounterGroupBusiness();
        //                $CounterGroupBusiness->orderPurchaseNumAdd(1, 1, $total, $total, $purchaseFirst->group_id);
        //            }
        //            $CounterCompanyBusinessService->orderPurchaseNumAdd(1, 1, $total, $total, $purchaseFirst->company_id);
        //            //$CounterCompanyBusinessService->reportOrderPurchaseNumAdd(1, 1, $total, $total);
        //            //月报
        //            $CounterRealFinance->incrementOrderPurchaseTotal($company, $total);
        //            DB::commit();
        //        } catch (\Exception $e) {
        //            DB::rollBack();
        //            return error('请重试', $e->getMessage());
        //        }

        //        return success();
        return 1;
    }

    /// <summary>
    /// 下级采购分销产品拒绝
    /// </summary>
    /// <returns></returns>
    public async Task<object> Reject(int[] ids, [FromServices] DBContext dbContext)
    {
        var purchases = await dbContext.Purchase.Where(p =>
                ids.Contains(p.ID) && p.PurchaseState != PurchaseStates.PURCHASED &&
                p.PurchaseState != PurchaseStates.REFUSED)
            .ToListAsync();
        if (purchases.Count <= 0) {
            return Error("无效数据#445");
        }

        var purchaseFirst = purchases.First();
        var order = await dbContext.Order.FindAsync(purchaseFirst.OrderId);;
        if (order is null) {
            return Error("无效数据#451");
        }
        
        purchases.ForEach(p =>
        {
            p.ChangeStateToRefused();
        });

        //            $count = PurchaseModel::query()->whereOrderId($order->id)
        //                ->where('purchase_state', PurchaseModel::STATE_INPROCUREMENT)
        //                ->whereNotIn('id', $purchases->pluck('id'))
        //                ->count();
        //            if ($count === 0) {
        //                AmazonOrderModel::whereId($order->id)->update([
        //                    'purchase_audit' => AmazonOrderModel::PURCHASE_AUDIT_FALSE
        //                ]);
        //            }
        await dbContext.SaveChangesAsync();
        return Success();
    }

}
