using AutoMapper;
using ERP.Enums.Identity;
using ERP.Data;
using ERP.Distribution.Abstraction.Services;
using ERP.Enums.Orders;
using ERP.Extensions;
using ERP.Services.Distribution;
using ERP.Storage.Abstraction.Services;
using ERP.ViewModels.Order.AmazonOrder;
using Newtonsoft.Json;
using Ryu.Linq;
using UserManager = ERP.Services.Identity.UserManager;
using ERP.Interface;
using ERP.Enums.Purchase;
using ERP.Services.DB.Logistics;
using OrderService = ERP.Services.DB.Orders.Order;
using ERP.Models.View.Distribution;

namespace ERP.Controllers.Distribution;

[Route("api/distribution/[controller]/[action]")]
[ApiController]
public class OrderController : BaseController
{
    private readonly AmazonOrderService _amazonOrderService;
    private readonly IDistributionCompanyService _companyService;
    private readonly PurchaseService _purchaseService;
    private readonly IDistributionWaybillService _DiswaybillService;
    private readonly WayBill _waybillService;
    private readonly OrderService orderService;
    private readonly UserManager _userManager;
    private readonly IMapper _mapper;
    private readonly ISessionProvider _sessionProvider;

    public OrderController(AmazonOrderService amazonOrderService, ISessionProvider sessionProvider, IDistributionCompanyService companyService,
        PurchaseService purchaseService
        , IDistributionWaybillService diswaybillService, OrderService _orderService,WayBill waybillService, UserManager userManager, IMapper mapper)
    {
        _amazonOrderService = amazonOrderService;
        _companyService = companyService;
        _purchaseService = purchaseService;
        _DiswaybillService = diswaybillService;
        _userManager = userManager;
        _mapper = mapper;
        _sessionProvider = sessionProvider;
        _waybillService=waybillService;
        orderService = _orderService;
    }

    private ISession Session { get => _sessionProvider.Session!; }

    /// <summary>
    /// 获取分销订单信息
    /// </summary>
    /// <param name="query"></param>
    /// <returns></returns>
    public async Task<ResultStruct> GetList(AmazonOrderListQuery query)
    {
        var user = await _userManager.GetUserAsync(User);
        string unit = Session.GetUnitConfig();
        var list = await _amazonOrderService.GetListPage(query, user, unit);
        //unit,user, query.OrderNo, progress, query.OrderState,query.CompanyId, oids, query.SortBy, query.Type, createTimeStart, createTimeEnd, query.OnlyDistributed);
        return Success(new { List = list });
    }

    /// <summary>
    /// 上级获取配置项
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetSearchList()
    {
        var user = await _userManager.GetUserAsync(User);
        // 获取当前上报公司数据
        var company = await _companyService.GetValidDistributionList(user.CompanyID);
        //订单状态
        var state = OrderState.SearchState;
        //订单进度
        var progress = OrderState.OrderProgressData;
        //订单排序
        var sortBy = Enum<SortBy>.ToDictionaryForUIByDescription();

        return Success(new { company, state, sortBy, progress, });
    }

    /// <summary>
    /// 下级公司订单列表搜索项
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetJuniorSearchList([FromServices] Services.Caches.Store storeCache)
    {
        var state = OrderState.SearchState;
        //        //发货状态
        //        $data['deliver'] = AmazonOrderModel::ORDER_DELIVER;
        //订单排序
        var sortBy = Enum<SortBy>.ToDictionaryForUIByDescription();
        var progress = OrderState.OrderProgressData;
        //当前用户可选店铺
        var user = await _userManager.GetUserAsync(User);
        var stores = User.IsInRole(Role.ADMIN) || User.Has(Enums.Rule.Config.Distribution.STORERANGE)
            ? storeCache.List(user.Company)
            : storeCache.List(user);

        var store = stores.Select(s => new { s.ID, s.Name, s.State }).KeyBy(s => s.ID);

        return Success(new { state, store, sortBy, progress });
    }

    /// <summary>
    /// 下级分销用户获取当前公司上报订单
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> GetJuniorList(AmazonOrderListQuery query)
    {
        var user = await _userManager.GetUserAsync(User);
        var oids = Array.Empty<int>();
        if (!string.IsNullOrEmpty(query.PurchaseTrackingNumber) || !string.IsNullOrEmpty(query.WaybillOrder))
        {
            var pids = Array.Empty<int>();
            if (query.PurchaseTrackingNumber is { Length: > 0 })
            {
                var purchase = await _purchaseService.getPurchase("", query.PurchaseTrackingNumber, 2);
                if (purchase.IsNullOrEmpty())
                {
                    return Success(new { List = oids });
                }

                pids = purchase.Pluck(p => p.OrderId).Cast<int>().ToArray();
            }

            var wids = Array.Empty<int>();
            if (query.WaybillOrder is { Length: > 0 })
            {
                var waybill = await _DiswaybillService.GetWaybill(user.CompanyID, query.WaybillOrder);
                if (waybill.Count <= 0)
                {
                    return Success(new { List = oids });
                }

                wids = waybill.Pluck(w => w.OrderId).Cast<int>().ToArray();
            }

            oids = (pids, wids) switch
            {
                ({ Length: > 0 }, { Length: > 0 }) => pids.Intersect(wids).ToArray(),
                ({ Length: 0 }, { Length: > 0 }) => wids,
                ({ Length: > 0 }, { Length: 0 }) => pids,
                _ => oids
            };

            if (oids.IsNullOrEmpty())
            {
                return Success(new { List = oids });
            }
        }
        string unit = Session.GetUnitConfig();
        var progress = query.progress;
        var list = await _amazonOrderService.GetJuniorListPage(user.OrderUnitConfig, unit, user.CompanyID, query.OrderNo, progress,
            query.OrderState, oids
            , query.SortBy);
        return Success(new { List = list });

        //        $store_ids = [];
        //        if (!$store_id && $this->sessionData->getConcierge() === UserModel::TYPE_CHILD && $this->ruleInfo()->hasAmazonStore() == false) {
        //            $store = $this->getStorePersonalCache();
        //            //员工无可允许查看店铺，返回空信息
        //            if (!checkArr($store)) {
        //                return success('', ["list" => []]);
        //            }
        //            $store_ids = array_column($store, 'id');
        //        }
        //        $list = $this->amazonOrderService->getJuniorListPage($order_no, $state, $progress, $store_id, $store_ids, $oids, $sort_by);
    }

    /// <summary>
    /// 上级查看订单详情
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<ResultStruct> Info(int id, [FromServices] DBContext dbContext,
        [FromServices] IStockService stockService)
    {
        var info = await _amazonOrderService.GetInfoById(id);
        if (info is null)
        {
            return Error(message: "暂无数据");
        }

        var user = await _userManager.GetUserAsync(User);
        var unitInfo = user.OrderUnitConfig;
        //订单下相关运单
        await dbContext.Entry(info).Collection(o => o.Waybills).LoadAsync();
        //运单状态
        var waybillState = Enum<ERP.Enums.Logistics.States>.ToDictionaryForUIByDescription();

        //订单下采购信息
        await dbContext.Entry(info).Collection(o => o.Purchases).LoadAsync();

        await dbContext.Entry(info).Reference(o => o.Receiver).LoadAsync();

        // 订单下仓库日志
        var stockLog = await stockService.LogList(user, info.OrderNo);

        //订单操作日志
        var orderLogs = await _amazonOrderService.GetOrderAction(id);
        //订单备注信息
        var orderRemarks = await _amazonOrderService.GetOrderRemarks(id);

        //订单状态
        var state = OrderState.SearchState;
        //订单进度
        var progress = OrderState.GetOrderProgress(info.Bits);

        Money waybillMoney = new Ryu.Data.Money();
        //if (info.Waybills != null && info.Waybills.Count > 0)
        //{
        //    waybillMoney = info.Waybills.Where(x => x.State != ERP.Enums.Logistics.States.Cancle).Sum(x => x.CarriageNum.Money);
        //}
        //获取物流信息
        var waybillIds = await _waybillService.GetWaybillIDsByOrder(info.ID, info.MergeId);
        var waybillAll = await orderService.GetWaybill(waybillIds, info.Plateform.Platform);

        var waybil = waybillAll.Where(a => a.State != ERP.Enums.Logistics.States.Cancle).ToList();
       // Money waybillMoney = new Ryu.Data.Money();
        var CoinShipp = "";
        if (waybil != null && waybil.Count > 0)
        {
            CoinShipp = waybil[0].CarriageNum.Raw.CurrencyCode;

            //重新计算运费
            waybillMoney += waybil.Where(x => x.MergeID <= 0 || x.MergeID==null).Sum(x => x.CarriageNum.Money);
            var wls = waybil.Where(x => x.MergeID > 0).ToList();
            if (wls is not null && wls.Count > 0)
            {
                foreach (var w in wls)
                {
                    List<int> ds = new();
                    w.OrderIDs.Split(",").ForEach(x => ds.Add(Convert.ToInt32(x)));
                    //计算合并运单的运费，shippMoney/Order count
                    waybillMoney += w.CarriageNum.Money/ ds.Count;
                }
            }
        }

        Money purchaseMoney = new Ryu.Data.Money();
        if (info.Purchases != null && info.Purchases.Count > 0)
        {
            purchaseMoney = info.Purchases.Where(x => x.PurchaseAffirm != PurchaseAffirms.Cancel).Sum(x => x.PurchaseTotal.Money);
        }

        var goods = info.GoodsScope().Select(a => new
        {
            a.ID,
            a.OrderItemId,
            a.Name,
            a.FromURL, //网址
            a.Sku,
            UnitPrice = Helpers.TransMoney(a.UnitPrice, unitInfo.ProductPrice, Session.GetUnitConfig(), false),
            ItemPrice = Helpers.TransMoney(a.ItemPrice, unitInfo.ProductPrice, Session.GetUnitConfig(), false), //商品总价
            TotalPrice = Helpers.TransMoney(a.TotalPrice, unitInfo.ProductPrice, Session.GetUnitConfig(), false),//总价(包含别的费用)
            a.ImageURL, //图片
            a.QuantityOrdered, //订购数量
            a.QuantityShipped, //已发货数
            a.WaitForSendNum, //待发货数
            a.Brand,
            a.Size,
            a.Color,
            ShippingPrice = a.ShippingPrice.ToString(user.UnitConfig, false),
            Unit = user.UnitConfig,
            goodIdText = info.Plateform.Platform.GetGoodIdDescription(),
            a.CustomizedURL
        });

        Money Profit = new Ryu.Data.Money();
        var code = info.OrderTotal.Raw.CurrencyCode;
        string orderProfitMargin = "";
        // 订单利润率
        if (info.OrderTotal.Money == 0)
        {
            orderProfitMargin = "暂无利润率";
        }
        else
        {
            //为了兼容错误数据
            //decimal m = 0;
            //int length = info.Profit.Raw.Amount.ToString().Split('.')[1].Length;
            //if (length > 2)
            //{
            //    m = info.Profit.Raw.Amount;
            //}
            //else
            //{
            //    m = info.Profit.Money.Value;
            //}
            //orderProfitMargin = $"{m / info.OrderTotal.Money.Value * 100:0.00}%";
            Profit = (info.OrderTotal.Money - info.Fee.Money - info.Refund.Money - waybillMoney - info.PurchaseFee.Money - info.Loss.Money);
            orderProfitMargin = $"{Profit.Value / info.OrderTotal.Money.Value * 100:0.00}%";
        }

        var order = new 
        { 
            info.ID,
            info.OrderNo,
            OrderTotal = Helpers.TransMoney(info.OrderTotal, unitInfo.Total, code, Session.GetUnitConfig(), false),
            Fee = Helpers.TransMoney(info.Fee, unitInfo.Fee, code, Session.GetUnitConfig(), false),
            Refund = Helpers.TransMoney(info.Refund, unitInfo.Refund, code, Session.GetUnitConfig(), false),
            ShippingMoney = Helpers.TransMoney(info.ShippingMoney, unitInfo.Deliver, code, Session.GetUnitConfig(), false),
            PurchaseFee = Helpers.TransMoney(info.PurchaseFee, unitInfo.Purchase, code, Session.GetUnitConfig(), false),
            Loss = Helpers.TransMoney(info.Loss, unitInfo.Total, code, Session.GetUnitConfig(), false),
            Profit = Helpers.TransMoneyM(Profit, info.OrderTotal.Raw.Rate, unitInfo.Profit, code, Session.GetUnitConfig(), false),
            ProfitMargin = orderProfitMargin,
            ShippingType = (info.ShippingType & ShippingTypes.AFN) == 0 ? 2 : 1,
            info.StoreId,
            info.EntryMode,
            info.ProcessWaybill,
            Coin= code,
            info.PurchaseCompanyId,
            info.WaybillCompanyId,
            Progress = info.Bits,
            info.DeliverySuccess,
            info.DeliveryFail,
            info.Url,
            info.Plateform,
            State = Extensions.EnumExtension.GetDescription(info.State),
            goods,
            Purchases = info.Purchases.Select(p => new
            {
                Id = p.ID,
                p.Url,
                p.FromUrl,
                p.Name,
                p.Num,
                Price = Helpers.TransMoney(p.Price, unitInfo.Purchase, Session.GetUnitConfig(), false),
                PurchasePrice = Helpers.TransMoney(p.PurchasePrice, unitInfo.Purchase, code, Session.GetUnitConfig(), false),
                PurchaseTotal= Helpers.TransMoney(p.PurchaseTotal, unitInfo.Purchase, code, Session.GetUnitConfig(), false),
                p.PurchaseOrderNo,
                p.PurchaseTrackingNumber,
                p.OperateCompanyId,
                p.PurchaseState,
                p.CreatedAt,
            }),
            Waybills = waybillAll.Select(a => new
            {
                a.ID,
                a.WaybillOrder,
                a.Express,
                Logistic = a.Logistic,
                LogisticSign = a.Logistic.Platform.ToString(),
                a.Tracking,
                CarriageNum = Helpers.TransMoney(a.CarriageNum, unitInfo.Deliver, code, Session.GetUnitConfig(), false),
                a.DeliverDate,
                a.CompanyID,
                OperateCompanyId = a.OperatecompanyID,
                a.WaybillCompanyID,
                a.State,
                stateStr = Extensions.EnumExtension.GetDescription(a.State),
                a.IsVerify,
                a.ButtonName,
                a.LogisticExt,
                a.ProductInfo,
                a.Sender,
                a.CarriageUnit,
                a.CarriageBase.Money,
                a.ShipMethod,
            }),
            info.Receiver,
        };


        //当前公司id
        var companyId = user.CompanyID;

        //        //当前订单店铺信息
        //        $store = [];
        //        if ($info['entry_mode'] === AmazonOrderModel::ENTRY_MODE_API) {
        //            $StoreService = new StoreService();
        //            $store = $StoreService->getInfoById($info['store_id']);
        //            $store = $StoreService->getMwsInfo($store);
        //        }
        //        $data['store'] = $store;

        return Success(new
        {
            info = order,
            orderLogs,
            orderRemarks,
            state,
            stockLog,
            progress,
            companyId,
            waybillState,
        });
    }

    /// <summary>
    /// 上级获取订单商品
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id:int}")]
    public async Task<ResultStruct> PurchaseGood(int id)
    {
        var info = await _amazonOrderService.GetInfoById(id);
        if (info is null)
        {
            return Error(message: "暂无数据");
        }

        var user = await _userManager.GetUserAsync(User);
        var good = info.GoodsScope().Select(a => new
        {
            a.ID,
            a.Name,
            a.FromURL, //网址
            a.Sku,
            UnitPrice = a.UnitPrice.ToString(user.UnitConfig, false), //商品单价
            ItemPrice = a.ItemPrice.ToString(user.UnitConfig, false), //商品总价
            TotalPrice = a.TotalPrice.ToString(user.UnitConfig, false), //总价(包含别的费用)
            a.ImageURL, //图片
            a.QuantityOrdered, //订购数量
            a.QuantityShipped, //已发货数
            a.WaitForSendNum, //待发货数
            ShippingPrice = a.ShippingPrice.ToString(user.UnitConfig, false),
            Unit = user.UnitConfig,
            goodIdText = info.Plateform.Platform.GetGoodIdDescription()
        });

        //对下级公司采购手续费率
        var company = await _companyService.GetInfoById(info.CompanyID);
        object rate = new();
        if (company is not null)
        {
            rate = new
            {
                company.PurchaseRate,
                company.PurchaseRateType,
                company.PurchaseRateNum,
            };
        }

        return Success(new { rate, good, });
    }


    /// <summary>
    /// 下级待发货信息退回内部处理
    /// </summary>
    /// <returns></returns>
    [HttpPost("{mid:int}")]
    public async Task<ResultStruct> BackJuniorOrder(int mid, [FromServices] AmazonOrderService amazonOrderService)
    {
        var user = await _userManager.GetUserAsync(User);

        if(await _waybillService.GerMergeWaybillCount(mid)>0)
        {
            return Error("上级已经成功发起运单，不能退回");
        }
        var res = await amazonOrderService.ToBackMerge(mid, user.CompanyID, user.Company.ReportId);

        return res > 0 ? Success() : Error();
    }

    /// <summary>
    /// 下级待发货信息上报内部处理
    /// </summary>
    /// <returns></returns>
    [HttpPost("{mid:int}")]
    public async Task<ResultStruct> ReportJuniorOrder(int mid, [FromServices] AmazonOrderService amazonOrderService)
    {
        var user = await _userManager.GetUserAsync(User);

        var res = await amazonOrderService.ToReportMergeOrder(mid, user.CompanyID, user.Company.ReportId);

        return res > 0 ? Success() : Error();
    }

}