﻿using ERP.ControllersWithoutToken.Tasks;
using ERP.Enums.Orders;
using ERP.Models.DB.Orders;
using ERP.Services.Distribution;
using Microsoft.AspNetCore.Mvc;

namespace ERP.Controllers.Distribution;
[Route("api/distribution/[controller]/[action]")]
[ApiController]
public class CartController : BaseController
{
    private CartService _cartService;

    public CartController(CartService cartService)
    {
        _cartService = cartService;
    }

    /// <summary>
    /// 获取指定用户购物车信息
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> getList()
    {
        var data = await _cartService.getList();
        return Success(new
        {
            data
        });
    }

    /// <summary>
    /// 移除购物车
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<ResultStruct> delCart(int? id)
    {
        var res = 0;
        if (id.HasValue)
        {
            res = await _cartService.deleteInfoById(id.Value);
        }
        else
        {
            res = await _cartService.clearCart();
        }
        return res > 0 ? Success() : Error();
    }

    /// <summary>
    /// 查询订单信息
    /// </summary>
    /// <param name="orderNo"></param>
    /// <param name="amazonOrderService"></param>
    /// <returns></returns>
    public async Task<ResultStruct> getOrder(string orderNo, [FromServices] AmazonOrderService amazonOrderService)
    {
        var info = await amazonOrderService.GetInfoByOrderNo(orderNo);

        if (info is not null)
        {
            if (info.ProcessPurchase == Belongs.SUPERIOR
                || (info.ProcessPurchase == Belongs.NONE && info.PurchaseCompanyId != 0))
            {
                return Warning("当前订单已上报，内部无法采购");
            }
        }
        //        $data = [
        //            'info'     => $info,
        //            'nation'   => $this->getNationCache(),
        //            'progress' => AmazonOrderModel::ORDER_PROGRESS
        //        ];
        return Success(new
        {
            info
        });
    }

    ///**
    // * 购物车绑定无订单新增
    // * @author CCY
    // * @createdAt 2020/6/20 13:34
    // * @param Request $request
    // * @return JsonResponse
    // */
    public object addOrder(/*Request $request*/)//: JsonResponse
    {
        //        $info = $request->input('info');
        //    if (!$info) {
        //        return error('参数错误');
        //    }
        //        $order_no = $request->input('order_no');
        //    if (!$order_no) {
        //        return error('参数错误');
        //    }
        //        $good = $request->input('good');
        //    if (!$good) {
        //        return error('参数错误');
        //    }

        //        //检测钱包余额
        //        $CompanyService = new CompanyService();
        //        $company = $CompanyService->getInfoById($this->sessionData->getCompanyId());
        //        $total = array_sum(array_column($good, 'total'));
        //        $totals = FromConversion($total, $this->sessionData->getUnitConfig());
        //    if (!$company->checkPublicWalletEnough($totals)) {
        //        return error('当前余额不足，请先提醒对方充值钱包金额');
        //    }

        //        $nation = $this->getNationCache()[$info['receiver_nation_short']];

        //    DB::beginTransaction();
        //    try
        //    {
        //            //商品出库
        //            $goods = [];
        //            $num = 0;
        //        foreach ($good as $value) {
        //                $price = FromConversion($value['price'], $this->sessionData->getUnitConfig());
        //                $goods[] = [
        //                    'num'        => $value['num'],
        //                    'url'        => $value['img'],
        //                    'asin'       => '',
        //                    'name'       => $value['name']. ','. $value['attr_name'],
        //                    'unit'       => $this->sessionData->getUnitConfig(),
        //                    'price'      => $price,
        //                    'total'      => FromConversion($value['total'], $this->sessionData->getUnitConfig()),
        //                    'good_id'    => $value['spu'],
        //                    'from_url'   => '',
        //                    'send_num'   => 0,
        //                    'seller_sku' => $value['sku'] ?: encodeSku($value['product_id'], skucode())
        //                ];
        //                $num += $value['num'];
        //        }

        //            //添加订单
        //            $AmazonOrderService = new AmazonOrderService();
        //            $order_id = $AmazonOrderService->orderManualAdd($order_no, $info, $nation, $goods, $num, $totals);

        //            //添加采购
        //            $PurchaseService = new PurchaseService();
        //            $PurchaseService->puechaseAddAll($order_id, $order_no, $goods);

        //            //清除购物车
        //            $this->CartService->clearCart();
        //        DB::commit();
        //    }
        //    catch (Exception $e) {
        //        DB::rollBack();
        //        return error($e->getMessage());
        //    }
        //    return success();
        return 1;
    }

    //    /**
    //     * 购物车商品绑定订单
    //     * @author CCY
    //     * @createdAt 2020/6/20 18:28
    //     * @param Request $request
    //     * @return JsonResponse
    //     */
    public object editOrder(/*Request $request*/)//: JsonResponse
    {
        //        $info = $request->input('order');
        //        if (!$info) {
        //            return error('参数错误');
        //        }
        //        $order_no = $request->input('order_no');
        //        if (!$order_no) {
        //            return error('参数错误');
        //        }
        //        $good = $request->input('good');
        //        if (!$good) {
        //            return error('参数错误');
        //        }

        //        $goods = [];
        //        $ids = [];
        //        $total_num = 0;
        //        $total_price = 0;
        //        $good = arrayCombine($good, 'spu');

        //        foreach ($info['goods'] as $value) {
        //            if (isset($value['id'], $good[$value['id']]) && $value['id'] !== '') {
        //                $goods[$value['id']] = $good[$value['id']];
        //                $goods[$value['id']]['good_id'] = $value['good_id'];
        //                $total_num += $good[$value['id']]['num'];
        //                $total_price += $good[$value['id']]['total'];
        //                $ids[] = $good[$value['id']]['id'];
        //            }
        //        }
        //        if (!checkArr($goods))
        //        {
        //            return error('未选择产品');
        //        }

        //        $AmazonOrderService = new AmazonOrderService();
        //        $order = AmazonOrderService::getInfoById($info['id']);
        //        if (!$order) {
        //            return error('订单不存在');
        //        }


        //        $CompanyService = new CompanyService();
        //        $company = $CompanyService->getInfoById($this->sessionData->getCompanyId());
        //        //检测钱包余额
        //        if (!$company->allowNegative()) {
        //            $total = array_sum(array_column($good, 'total'));
        //            // TODO: 无需转换
        //            // $total = FromConversion($total, $this->sessionData->getUnitConfig());
        //            if (bcsub($company['public_wallet'], $total) < 0)
        //            {
        //                return error('当前余额不足，请先提醒对方充值钱包金额');
        //            }
        //        }

        //        DB::beginTransaction();
        //        try
        //        {
        //            $updateData = [
        //                'send_mode' => $info['send_mode'],
        //                'distribution_address' => json_encode($info['distribution_address']),
        //                'purchase_audit' => AmazonOrderModel::PURCHASE_AUDIT_TRUE,
        //            ];
        //            //更新订单信息
        //            $AmazonOrderService->updateOrderByPurchaseAdd($order, AmazonOrderModel::PROGRESS_PENDING, $total_price, $total_num, '', $updateData);
        //            //增加订单日志
        //            $AmazonOrderService->addLog($order['id'], '更新订单采购信息', $order['order_state']);

        //            //添加采购
        //            $PurchaseService = new PurchaseService();
        //            $PurchaseService->puechaseAdd($order, $goods);
        //            //删除指定购物车产品
        //            $this->CartService->deleteInfoByIds($ids);

        //            DB::commit();
        //        }
        //        catch (Exception $e) {
        //            DB::rollBack();
        //            return error($e->getMessage());
        //        }
        //        return success();
        return 1;
    }
}
