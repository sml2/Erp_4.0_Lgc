﻿using ERP.Authorization.Rule;
using ERP.Data;
using ERP.Distribution.Abstraction;
using ERP.Distribution.Abstraction.Services;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.Abstract;
using ERP.Services.Stores;
using ERP.Services.DB.Users;
using ERP.ViewModels;
using ERP.ViewModels.Finance;
using Microsoft.AspNetCore.Identity;
using ERP.Enums.Identity;
using ERP.Models.DB.Users;
using ERP.Services.Finance;
using ERP.Services.Statistics;
using Microsoft.EntityFrameworkCore;
using Ryu.Linq;
using BillLog = ERP.Models.Finance.BillLog;

namespace ERP.Controllers.Distribution;

[Route("api/distribution/[controller]/[action]")]
[ApiController]
public class FinanceController : BaseController
{
    private readonly DBContext _dbContext;
    private readonly IDistributionFinanceService _financeService;
    private readonly StatisticsService _statisticsService;
    private readonly IDistributionCompanyService _companyService;
    private readonly UserService _userService;
    private readonly ISessionProvider _sessionProvider;
    private readonly UserManager<Models.DB.Identity.User> _userManager;
    private readonly StatisticMoney _statisticMoney;
    private readonly Services.Caches.Store _storeCache;

    public FinanceController(IDistributionCompanyService companyService,
        UserManager<Models.DB.Identity.User> userManager,
        ISessionProvider sessionProvider, IDistributionFinanceService financeService,
        StatisticsService statisticsService, DBContext dbContext, StatisticMoney statisticMoney,
        UserService userService, Services.Caches.Store storeCache)
    {
        _financeService = financeService ?? throw new ArgumentNullException(nameof(financeService));
        _statisticsService = statisticsService;
        _dbContext = dbContext;
        _statisticMoney = statisticMoney;
        _userService = userService;
        _storeCache = storeCache;
        _companyService = companyService ?? throw new ArgumentNullException(nameof(companyService));
        _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        _sessionProvider = sessionProvider;
    }

    /// <summary>
    /// 获取下级公司对公钱包配置项
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetWalletSearchList()
    {
        var user = await _userManager.GetUserAsync(User);
        //获取当前上报公司数据
        var company = await _companyService.GetValidDistributionList(user.CompanyID);

        return Success(new
        {
            company,
        });
    }

    /// <summary>
    /// 上级获取下级公司对公钱包列表
    /// </summary>
    /// <param name="companyId"></param>
    /// <returns></returns>
    [Permission(Enums.Rule.Config.Distribution.WALET_LIST)]
    public async Task<ResultStruct> GetWalletList([FromQuery] int? companyId)
    {
        var user = await _userManager.GetUserAsync(User);
        var list = await _companyService.GetWalletListPage(user.CompanyID, companyId);
        return Success(list);
    }

    /// <summary>
    /// 获取下级公司信息
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetJuniorCompany()
    {
        var user = await _userManager.GetUserAsync(User);
        //获取当前上报公司数据
        var company = await _companyService.GetValidDistributionList(user.CompanyID);
        return Success(company);
    }

    public record GetStatisticsListVm(int? CompanyId, DateOnly? Month);

    /// <summary>
    /// 上级下级公司财务统计
    /// </summary>
    /// <returns></returns>
    [Permission(Enums.Rule.Config.Distribution.FINANCIAL_STATISTICS)]
    public async Task<ResultStruct> GetStatisticsList([FromQuery] GetStatisticsListVm vm)
    {
        var user = await _userManager.GetUserAsync(User);
        var companyIds = Array.Empty<int>();
        Dictionary<int, CompanyOnlyNameDto> company = new();
        if (!vm.CompanyId.HasValue)
        {
            company = await _companyService.GetValidDistributionList(user.CompanyID);
            if (company is { Count: > 0 })
            {
                companyIds = company.Keys.ToArray();
            }
        }
        else
        {
            companyIds = new[] { vm.CompanyId.Value };
        }

        var res = await _financeService.GetMonthFinance(companyIds, vm.Month);
        return Success(res);
    }

    /// <summary>
    /// 获取上级公司账单审批配置项
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetApprovalSearchList()
    {
        //获取当前上报公司数据
        var user = await _userManager.GetUserAsync(User);
        var companies = await _companyService.GetValidDistributionList(user.CompanyID);
        //账单类型
        var types = Enum<BillLog.Types>.ToDictionaryForUIByDescription();
        //账单模块
        var modules = Enum<BillLog.Modules>.ToDictionaryForUIByDescription();
        //账单审核状态
        var audits = Enum<BillLog.Audits>.ToDictionaryForUIByDescription();
        //权限
        var rule = new
        {
            Approval = true,
            BillDetail = true,
        };

        return Success(new
        {
            companies,
            types,
            modules,
            audits,
            rule
        });
    }
    

    /// <summary>
    /// 获取上级公司账单审批
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="type"></param>
    /// <param name="audit"></param>
    /// <param name="module"></param>
    /// <returns></returns>
    [Permission(Enums.Rule.Config.Distribution.APPROVAL_LIST)]
    [HttpPost]
    public async Task<ResultStruct> GetApprovalList(GetApprovalListVm vm)
    {
        var user = await _userManager.GetUserAsync(User);
        var list = await _financeService.GetBillPage(user.CompanyID, vm.Type, vm.Module, vm.Audit, vm.CompanyId,
            _sessionProvider.Session.GetUnitConfig(), vm?.Date, vm?.OrderNo, vm?.PurchaseNo, vm?.WaybillNo);
        return Success(list);
    }

    public record ApprovalVm(int Id, BillLog.Audits Audit);

    /// <summary>
    /// 账单审核
    /// </summary>
    /// <returns></returns>
    [Permission(Enums.Rule.Config.Distribution.APPROVAL)]
    public async Task<ResultStruct> Approval(ApprovalVm vm)
    {
        var user = await _userManager.GetUserAsync(User);

        var info = await _financeService.GetBillLogInfo(vm.Id);
        if (info is null)
            return Error("未查询到相关信息#1");

        if (info.Audit != BillLog.Audits.PendingReview)
        {
            return Warning("审批已处理");
        }


        if (vm.Audit == BillLog.Audits.DidNotPass)
        {
            //否决，更新账单状态
            var res = await _financeService.UpdateBillLog(vm.Id, vm.Audit, user, null);
            return res > 0 ? Success() : Error();
        }

        var company = await _companyService.GetInfoById(info.CompanyID);
        if (company is null)
            return Error("未查询到相关信息#2");


        //通过，更改钱包余额，上报记录
        var money = info.Money;
        // var trans = DB::beginTransaction();
        var trans = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            //            $CompanyBusinessService = new CompanyBusinessService();
            //            $CounterRealFinanceService = new CounterRealFinanceService();
            //            $CounterGroupBusinessService = new CounterGroupBusinessService();
            //            $CounterUserBusiness = new CounterUserBusinessService();
            if (info.Type == BillLog.Types.Recharge)
            {
                //充值
                // await _companyService.RechargeWallet(info.CompanyID, money);

                company.PublicWallet += money;

                //财务
                await _statisticMoney.PublicRechargeTotal(info, money.Value);

                //                //公司钱包汇总
                //                $CompanyBusinessService->rechargeNumAdd($info['company_id'], $money);
                //                //用户组钱包汇总
                //                $CounterGroupBusinessService->rechargeNumAdd($this->sessionData->getGroupId(), $money);
                //                //用户钱包汇总
                //                $CounterUserBusiness->rechargeNumAdd($this->sessionData->getUserId(), $money);
                //                //月份汇总
                //                $CounterRealFinanceService->rechargeTotal($this->getCompany($info['company_id']), $money);
            }
            else
            {
                // await _companyService.ExpenseWallet(info.CompanyID, money);

                company.PublicWallet -= money;

                //是否允许为负数
                if (!company.IsNegative)
                {
                    if (company.PublicWallet.Money < 0)
                    {
                        return Error("钱包不允许为负");
                    }
                }

                //财务
                await _statisticMoney.PublicExpenseTotal(info, money.Value);
                //                $CompanyBusinessService->expenseNumAdd($info['company_id'], $money);
                //                $CounterGroupBusinessService->expenseNumAdd($this->sessionData->getGroupId(), $money);
                //                $CounterUserBusiness->expenseNumAdd($this->sessionData->getUserId(), $money);
                //                //月份汇总
                //                $CounterRealFinanceService->expenseTotal($this->getCompany($info['company_id']), $money);
            }

            #region 自用钱包跟踪

            await _statisticMoney.SelfWalletReport(company);

            #endregion

            await _financeService.UpdateBillLog(vm.Id, vm.Audit, user, company.PublicWallet.Money);


            await _dbContext.Company.SingleUpdateAsync(company);
            await _dbContext.SaveChangesAsync();
            //            DB::commit();
            await trans.CommitAsync();
        }
        catch (Exception e)
        {
            ;
            // DB::rollBack();
            await trans.RollbackAsync();
            return Error(e.Message);
        }

        return Success();
    }

    //    /********************************** 下级财务 **************************************************/

    /// <summary>
    /// 下级员工财务统计
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetUserStatisticsList(string? name, BaseModel.StateEnum? state,
        [FromServices] UserService userService,
        [FromServices] CounterUserBusinessService userBusinessService)
    {
        var user = await _userManager.GetUserAsync(User);
        var res = await userService.GetUserBusiness(name, state, user.CompanyID);
        if (res is { Items.Count: <= 0 })
        {
            return Success(new
            {
                user = Array.Empty<int>(),
                data = Array.Empty<int>(),
            });
        }

        var list = await userBusinessService.GetFinance(res.Items.Select(u => u.Id));
        return Success(new { user = res, data = list.KeyBy(m => m.AboutId) });
    }

    /// <summary>
    /// 下级店铺财务统计
    /// </summary>
    /// <returns></returns>
    public async Task<object> GetStoreStatisticsList(string? name, BaseModel.StateEnum? state,
        [FromServices] StoreService storeService,
        [FromServices] CounterStoreBusinessService storeBusinessService)
    {
        var user = await _userManager.GetUserAsync(User);
        var res = await storeService.GetStorePage(user.CompanyID, name, state);
        if (res is { Items.Count: <= 0 })
        {
            return Success(new
            {
                store = Array.Empty<int>(),
                data = Array.Empty<int>(),
            });
        }

        var list = await storeBusinessService.GetFinance(res.Items.Select(m => m.Id));
        return Success(new { store = res, data = list.KeyBy(m => m.AboutId) });
    }

    /// <summary>
    /// 下级用户组财务统计
    /// </summary>
    /// <returns></returns>
    public async Task<object> GetGroupStatisticsList(string? name, BaseModel.StateEnum? state,
        [FromServices] GroupService groupService,
        [FromServices] CounterGroupBusinessService groupBusinessService)
    {
        var user = await _userManager.GetUserAsync(User);
        var res = await groupService.GetGroupBusiness(user.CompanyID, name, state);
        if (res is { Items.Count: <= 0 })
        {
            return Success(new
            {
                store = Array.Empty<int>(),
                data = Array.Empty<int>(),
            });
        }

        var list = await groupBusinessService.GetFinance(res.Items.Select(m => m.Id));
        return Success(new { group = res, data = list.KeyBy(m => m.AboutId) });
    }

    /// <summary>
    /// 获取下级公司账单配置项
    /// </summary>
    /// <returns></returns>
    public ResultStruct GetCompanyBillSearchList()
    {
        //账单类型
        var types = Enum<BillLog.Types>.ToDictionaryForUIByDescription();
        //账单模块
        var modules = Enum<BillLog.Modules>.ToDictionaryForUIByDescription();
        //账单审核状态
        var audits = Enum<BillLog.Audits>.ToDictionaryForUIByDescription();

        return Success(new
        {
            types,
            modules,
            audits
        });
    }

    /// <summary>
    /// 获取下级公司账单
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> GetCompanyBillList(BillLogIndexDto req)
    {
        var user = await _userManager.GetUserAsync(User);
        var company = await _companyService.GetInfoById(user.CompanyID);
        if (company is null)
        {
            return Error("当前公司不存在");
        }

        var unit = _sessionProvider.Session.GetUnitConfig();
        var list = await _financeService.GetCurrentBillPageMoreSearch(user.CompanyID, req.Type, req.Audit, req.Module,unit,req.Date,req.OrderNo,req.PurchaseNo,req.WaybillNo,req.ShopId);
        return Success(new
        {
            list,
            store = _storeCache.List()?.Where(m =>
                m.CompanyID == user.CompanyID && m.IsDel == false && m.State == BaseModel.StateEnum.Open).ToList(),
            wallet = new { money = company.PublicWallet.ToString(_sessionProvider.Session!.GetUnitConfig()) },
            freeze = company.FreezeTotalAmountWaybill.Value > 0 ?new { money = new MoneyRecordFinancialAffairs(company.FreezeTotalAmountWaybill.Value).ToString(_sessionProvider.Session!.GetUnitConfig()) } : new { money = "0.00 "+_sessionProvider.Session!.GetUnitConfig()},
        });
    }

    /// <summary>
    /// 获取下级公司待审核账单配置项
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetApprovalJuniorSearchList()
    {
        var user = await _userManager.GetUserAsync(User);
        //账单类型
        var type = Enum<BillLog.Types>.ToDictionaryForUIByDescription();
        //账单模块
        var module = Enum<BillLog.Modules>.ToDictionaryForUIByDescription();
        //获取当前公司对公钱包余额
        var company = await _companyService.GetInfoById(user.CompanyID);

        var companyAdminUser = await _userService.GetUserWithCompanyAdmin(user.CompanyID);

        var companyInfo =
            new CompanyWalletDto(company.ID, company.Name, company.PublicWallet,company.FreezeTotalAmountWaybill, companyAdminUser.UserName);

        var audits = Enum<BillLog.Audits>.ToDictionaryForUIByDescription();

        //公司管理全部权限
        var rule = new
        {
            Recharge = true,
            Expend = true,
            BillDetail = true,
        };
        if (!User.IsInRole(Role.ADMIN))
        {
            //            //权限
            //            $data['rule'] = [
            //                'Recharge'   => $this->ruleInfo()->hasRecharge(),
            //                'Expend'     => $this->ruleInfo()->hasExpend(),
            //                'BillDetail' => $this->ruleInfo()->hasBillDetail(),
            //            ];
        }

        return Success(new
        {
            type, module, company = companyInfo, rule, audits
        });
    }

    /// <summary>
    /// 获取下级公司待审核账单
    /// </summary>
    /// <returns></returns>
    // public async Task<object> GetApprovalJuniorList(BillLog.Types? type, BillLog.Modules? module)
    [HttpPost]
    public async Task<object> GetApprovalJuniorList(BillLogIndexDto req)
    {
        var user = await _userManager.GetUserAsync(User);
        var unit = _sessionProvider.Session.GetUnitConfig();
        var list =  await _financeService.GetCurrentBillPageMoreSearch(user.CompanyID, req.Type, BillLog.Audits.PendingReview, req.Module,unit,req.Date,req.OrderNo,req.PurchaseNo,req.WaybillNo,req.ShopId);
        return Success(new
        {
            list,
            store = _storeCache.List()?.Where(m =>
                m.CompanyID == user.CompanyID && m.IsDel == false && m.State == BaseModel.StateEnum.Open).ToList(),
        });
    }

    public record ActionInfoVm(int company_id, DateTime Execdate, string[] Imgs, decimal Money, string Reason,
        string Remark, BillLog.Types Type);

    /// <summary>
    /// 分销财务上下级钱包充值、支出
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> Action(ActionInfoVm info)
    {
        var user = await _userManager.GetUserAsync(User);
        if (info.company_id != user.CompanyID)
        {
            //上级
            return await SuperiorAction(info, user);
        }

        //下级,账单审核
        return await JuniorAction(info, user);
    }

    /// <summary>
    /// 分销财务上级钱包充值、支出
    /// </summary>
    /// <returns></returns>
    private async Task<ResultStruct> SuperiorAction(ActionInfoVm info, Models.DB.Identity.User user)
    {
        var company = await _companyService.GetInfoById(info.company_id);
        var companyCurrent = user.Company;

        //        DB::beginTransaction();
        //        try {
        //            //临时表图片数据删除
        //            $img_all = [];
        //            if (checkArr($info['imgs'])) {
        //                $ids = array_keys($info['imgs']);
        //                $img_all = array_values($info['imgs']);
        //                TempImages::query()->whereIn('id', $ids)->delete();
        //            }
        var money = MoneyFactory.Instance.Create(info.Money, user.UnitConfig);
        if (companyCurrent.BillAudit == Company.BillAudits.YES)
        {
            //需要走审核
            await _financeService.AddWalletBill(money, info.Type, info.Execdate, info.Reason, info.Remark,
                BillLog.Audits.PendingReview, info.Imgs, company, user.Company, user, null);
        }
        else
        {
            var trans = await _dbContext.Database.BeginTransactionAsync();
            try
            {
                //                $CompanyBusinessService = new CompanyBusinessService();
                //                $CounterRealFinanceService = new CounterRealFinanceService();
                //                $CounterGroupBusinessService = new CounterGroupBusinessService();
                //                $CounterUserBusiness = new CounterUserBusinessService();
                if (info.Type == BillLog.Types.Recharge)
                {
                    //充值
                    await _companyService.RechargeWallet(info.company_id, money);
                    //财务
                    await _statisticMoney.PublicRechargeTotal(company, money.Value);

                    //                    $CompanyBusinessService->rechargeNumAdd($info['company_id'], $info['money']);
                    //                    $CounterGroupBusinessService->rechargeNumAdd($this->sessionData->getGroupId(), $info['money']);
                    //                    $CounterUserBusiness->rechargeNumAdd($this->sessionData->getUserId(), $info['money']);
                    //                    //月份汇总
                    //                    $CounterRealFinanceService->rechargeTotal($this->getCompany($info['company_id']), $info['money']);
                }
                else
                {
                    //是否允许为负数
                    if (!company.IsNegative)
                    {
                        if ((company.PublicWallet - money).Money < 0)
                        {
                            return Error("钱包不允许为负");
                        }
                    }

                    //支出
                    await _companyService.ExpenseWallet(info.company_id, money);
                    //财务
                    await _statisticMoney.PublicExpenseTotal(company, money.Value);


                    //                    $CompanyBusinessService->expenseNumAdd($info['company_id'], $info['money']);
                    //                    $CounterGroupBusinessService->expenseNumAdd($this->sessionData->getGroupId(), $info['money']);
                    //                    $CounterUserBusiness->expenseNumAdd($this->sessionData->getUserId(), $info['money']);
                    //                    //月份汇总
                    //                    $CounterRealFinanceService->expenseTotal($this->getCompany($info['company_id']), $info['money']);
                }

                //            }
                //            DB::commit();
                //        } catch (Exception $e) {
                //            DB::rollBack();
                //            return returnArr('false', $e->getMessage());
                //无需审核,通过账单，更新钱包余额,金额汇总
                await _financeService.AddWalletBill(money, info.Type, info.Execdate, info.Reason, info.Remark,
                    BillLog.Audits.Audited, info.Imgs, company, user.Company, user, user);
                await trans.CommitAsync();
            }
            catch (Exception e)
            {
                await trans.RollbackAsync();
                Console.WriteLine(e);
                throw;
            }
        }

        return Success();
    }

    /// <summary>
    /// 分销财务下级钱包充值、支出
    /// </summary>
    /// <returns></returns>
    private async Task<ResultStruct> JuniorAction(ActionInfoVm info, Models.DB.Identity.User user)
    {
        var company = await _companyService.GetInfoById(user.Company.ReportId);

        if (company is null)
        {
            return Error("找不到上级公司");
        }

        //        DB::beginTransaction();
        //        try {
        //            //临时表图片数据删除
        //            $img_all = [];
        //            if (checkArr($info['imgs'])) {
        //                $ids = array_keys($info['imgs']);
        //                $img_all = array_values($info['imgs']);
        //                TempImages::query()->whereIn('id', $ids)->delete();
        //            }
        var money = MoneyFactory.Instance.Create(info.Money, user.UnitConfig);
        await _financeService.AddWalletBill(money, info.Type, info.Execdate, info.Reason, info.Remark,
            BillLog.Audits.PendingReview, info.Imgs, user.Company, company, user, null);
        //            DB::commit();
        //        } catch (Exception $e) {
        //            DB::rollBack();
        //            return returnArr('false', $e->getMessage());
        //        }
        return Success();
    }

    /// <summary>
    /// 账单详情
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> BillInfo(IdDto req)
    {
        var res = await _financeService.GetBillLogInfo(req.ID);
        if (res is null)
        {
            return Error("数据错误");
        }

        //整理详情展示数据
        var info = new BillLogVM(_sessionProvider.Session, res);

        //        if ($res['img_all']) {
        ////            if ($this->currentOem()) {
        ////                $resourceOss = arrayCombine($this->resourceOss(), 'id');
        ////                $currentOss = $resourceOss[$this->currentOem()['oss_id']];
        ////                $currentOssDomain = $currentOss['bucket'] . '.' . $currentOss['endpoint_domain'] . '/';
        //                $currentOssDomain = app('oem')->getCurrentOssDomain();
        ////            } else {
        ////                $currentOssDomain = env('OSS_DOMAIN', 'http://img3.waimaomvp.com/');
        ////            }

        //            $img_all = json_decode($res['img_all'], true);
        //            foreach ($img_all as $value) {
        //                $info['img_all'][] = $currentOssDomain . '/' . $value;
        //            }
        //        } else {
        //            $info['img_all'] = [];
        //        }

        return Success(info);
    }
}

public class GetApprovalListVm
{
    public int? CompanyId { get; set; }
    public BillLog.Types? Type { get; set; }
    public BillLog.Audits? Audit { get; set; }
    public BillLog.Modules? Module { get; set; }
    public List<DateTime>? Date { get; set; }
    public string? OrderNo { get; set; }
    public string? PurchaseNo { get; set; }
    public string? WaybillNo { get; set; }
}