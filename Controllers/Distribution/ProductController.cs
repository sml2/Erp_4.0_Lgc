﻿using ERP.Enums.Identity;
using ERP.Enums.Rule.Config.Internal;
using ERP.Extensions;
using ERP.Models.View.Products.Product;
using ERP.Services.Identity;
using ERP.Services.Product;
using Microsoft.AspNetCore.Mvc;

namespace ERP.Controllers.Distribution;
[Route("api/distribution/[controller]/[action]")]
[ApiController]
public class ProductController : BaseController
{
    private readonly UserManager _userManager;

    private readonly CategoryService _categoryService;
    private readonly ProductService _productService;
    public ProductController(UserManager userManager, CategoryService categoryService, ProductService productService)
    {
        _userManager = userManager;
        _categoryService = categoryService;
        _productService = productService;
    }

    /// <summary>
    /// 分销产品列表权限
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetSearch()
    {
        var user = await _userManager.GetUserAsync(User);
       var userType = !user.IsDistribution;
        object rule = new {};
        //权限
        if (!user.IsDistribution) {
            if (User.IsInRole(Role.ADMIN)) {
                rule = new {
                    BindingCategory    = true,
                    CancelDistribution = true,
                    ProductDatail      = true,
                };
            } else {
                // rule = new {
                //     BindingCategory    = $this->ruleInfo()->hasBindingCategory(),
                //     CancelDistribution = $this->ruleInfo()->hasCancelDistribution(),
                //     ProductDatail      = $this->ruleInfo()->hasProductDatail(),
                // };
            }
        } else
        {
            if (User.IsInRole(Role.ADMIN)) {
                        rule = new {
                            CartAdd       = true,
                            ProductDatail = true,
                            AmazonUpload  = true,
                            download      = true
                        };
            } else
            {
                        // rule = new {
                        //     CartAdd       = $this->ruleInfo()->hasCartAdd(),
                        //     ProductDatail = $this->ruleInfo()->hasProductDatail(),
                        //     AmazonUpload  = true,
                        //     download      = true
                        // };
            }
        }
        //产品分类信息
        var companyId = user.CompanyID;
        var oemId = user.OEMID;
        if (user.IsDistribution) {
             companyId = user.Company.ReportId;
             // oem_id = company['oem_id'];
        }

        var category = await _categoryService.DistributionCategory(companyId, oemId);
        return Success(new { category, rule, userType });
    }

    public async Task<ResultStruct> GetList(ListDto req)
    {

        var list = await _productService.List(req, DataRange_2b.Company);
        return Success(list);
    }

    //    /**
    //     * 分销产品列表
    //     * @author CCY
    //     * @createdAt 2020/6/18 18:06
    //     * @param Request $request
    //     * @return JsonResponse
    //     */
    public object getList(/*Request $request*/)
    {
        //        $name = $request->input('name');
        //        $pid = $request->input('pid');
        //        $firsCategory = $request->input('firsCategory');
        //        $subCategory = $request->input('subCategory');
        //    if ($this->sessionData->getIsDistribution() === companyModel::DISTRIBUTION_TRUE) {
        //            $company_id = $this->sessionData->getReportId();
        //            $company = $this->getCompany($company_id);
        //            $oem_id = $company['oem_id'];
        //    } else
        //    {
        //            $company_id = $this->sessionData->getCompanyId();
        //            $oem_id = $this->sessionData->getOemId();
        //    }

        //        $data = $this->ProductsService->distributionProduct($name, $firsCategory, $subCategory, $company_id, $oem_id, $pid);
        //    return success('', $data['data']);
        return 1;
    }

    ///**
    // * 购物车产品详情
    // * @author CCY
    // * @createdAt 2020/6/19 17:26
    // * @param Request $request
    // * @return JsonResponse
    // */
    public object getInfo(/*Request $request*/)
    {
        //        $id = $request->input('id');
        //    if (!$id) {
        //        return error('服务器异常: 缺少ID');
        //    }
        //        //分销下级查看上级产品详情
        //        $company = $this->getCompany($this->sessionData->getReportId());
        //        $info = $this->ProductsService->distributionInfo($id, $company['id'], 0, 0, $company['oem_id']);
        //    if (null === $info) {
        //        return error('未查询到信息');
        //    }
        //    return success('成功', ['info' => $info]);
        return 1;
    }

    ///**
    // * 添加购物车
    // * @link https://sswz432avo.feishu.cn/file/boxcnA3ECXOCBWSlMOUe6sMTtjg
    // * @param Request $request
    // * @param Stock $stockService
    // * @return JsonResponse
    // * @author CCY
    // * @createdAt 2020/6/19 17:32
    // */
    public object addCart(/*Request $request, Stock $stockService*/)
    {
        //        $id = $request->input('id');
        //    if (!$id) {
        //        return error('服务器异常: 缺少ID');
        //    }

        //        $info = $request->input('info');
        //    if (!$info) {
        //        return error('服务器异常: 缺少info');
        //    }
        //        // TODO: 库存为0，也能购买
        //        // $res = $stockService->callGetStockQuantity($id, $info['spu'], $this->sessionData->getReportId());
        //        // if ($res['state'] === false) {
        //        //     return error($res['msg']);
        //        // }
        //        // if ($res['data'] < $info['num']) {
        //        //     return error('购买数量' . $info['num'] . '大于库存数量' . $res['data']);
        //        // }
        //        //存入购物车
        //        $CartService = new CartService();
        //        //查询当前商品是否已购买
        //        $cart = $CartService->getInfo($info['spu']);
        //    if ($cart) {
        //            $res = $CartService->updateCart($cart, $info);
        //    } else
        //    {
        //            $res = $CartService->cartAdd($id, $info);
        //    }

        //    return $res? success('添加成功') : error('添加失败');
        return 1;
    }

    /// <summary>
    /// 上级初始权限
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> SuperiorInit()
    {
        var user = await _userManager.GetUserAsync(User);
        var rule = new
        {
            product = true,
            cart = true,
        };
        if (user.Role == Role.Employee)
        {
            // rule.product = $this->ruleInfo()->hasProductDistribution(),
            // rule.cart    = $this->ruleInfo()->hasProCartDistribution(),
        }

        return Success(rule);
    }

    /// <summary>
    /// 下级初始权限
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> JuniorInit()
    {
        var user = await _userManager.GetUserAsync(User);
        var rule = new
        {
            product = true,
            cart = true,
        };
        if (user.Role == Role.Employee)
        {
            // rule.product = $this->ruleInfo()->hasProductDistribution(),
            // rule.cart    = $this->ruleInfo()->hasProCartDistribution(),
        }

        return Success(rule);
    }

    ///**
    // * 分销权限
    // * @author CCY
    // * @createdAt 2020/6/5 18:01
    // * @return Distribution
    // */
    private object ruleInfo()
    {
        //    return $this->sessionData->getRule()->getDistribution()->getBiter();
        return 1;
    }

    ///**
    // * 删除分类
    // * @author CCY
    // * @createdAt 2020/7/23 18:20
    // * @return JsonResponse
    // */
    public object categoryDel()
    {
        //        $id = request()->input('id');
        //    if (!$id) {
        //        return error('缺少ID');
        //    }
        //        $res = $this->categoryService->delCategory($id, CategoryModel::TYPE_TRUE);
        //    return $res['state'] ? success('删除成功') : error($res['msg']);
        return 1;
    }

    //    /**
    //     * 产品绑定分销分类
    //     * @author CCY
    //     * @createdAt 2020/7/24 11:20
    //     * @param Request $request
    //     * @return JsonResponse
    //     */
    public object binding(/*Request $request*/)
    {
        //        $id = $request->input('id');
        //        if (!$id) {
        //            return error('服务器异常: 缺少ID');
        //        }
        //        $info = $request->input('info');
        //        if (!$info) {
        //            return error('服务器异常: 缺少INFO');
        //        }
        //        $category_id = $info['category_id'] ?: 0;
        //        $sub_category_id = $info['sub_category_id'] ?: 0;
        //        $res = $this->ProductsService->binding($id, $category_id, $sub_category_id);
        //        return $res ? success() : error();
        return 1;
    }
    
}
