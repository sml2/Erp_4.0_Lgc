using Ryu.Linq;
using ERP.Storage.Abstraction.Services;
using ERP.Distribution.Abstraction.Services;
using Newtonsoft.Json;

namespace ERP.Controllers.Distribution;
using States = Enums.Logistics.States;
using Enums.Orders;
using Extensions;
using Services.Distribution;
using ViewModels.Order.AmazonOrder;
using Enums.Identity;
using Models.DB.Users;
using Services.Identity;

[Route("api/distribution/[controller]/[action]")]
[ApiController]
public class WaybillController : BaseController
{
    private readonly IDistributionWaybillService _waybillService;
    private readonly IDistributionCompanyService _companyService;
    private readonly UserManager _userManager;

    public WaybillController(IDistributionWaybillService waybillService, IDistributionCompanyService companyService, UserManager userManager)
    {
        _waybillService = waybillService;
        _companyService = companyService;
        _userManager = userManager;
    }

    private ISession Session => HttpContext.Session;

    /// <summary>
    /// 获取待发货订单配置项
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetWaitSearchList()
    {
        var user = await _userManager.GetUserAsync(User);
        //获取当前上报公司数据
        var company = await _companyService.GetValidDistributionList(user.CompanyID);
        //订单状态
        var state = OrderState.SearchState;
        //订单进度
        // $data ['progress'] = AmazonOrderModel::ORDER_PROGRESS;
        //订单排序
        var sortBy = Enum<SortBy>.ToDictionaryForUIByDescription();
        return Success(new { company, state, sortBy, });
    }

    /// <summary>
    /// 获取分销待发货订单列表信息
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetWaitList([FromQuery]string? orderNo, [FromQuery]int? companyId, [FromQuery]ProgressMaskValue? progress, [FromQuery]Enums.Orders.States? orderState, [FromQuery]string? sortBy
        , [FromServices] AmazonOrderService amazonOrderService)
    {
        var user = await _userManager.GetUserAsync(User);
        var list = await amazonOrderService.GetWaitListPage(user.CompanyID,orderNo, orderState, companyId, 2, sortBy);

        return Success(new { list });
    }

    /// <summary>
    /// 获取运单列表配置项
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetSearchList([FromServices] IWarehouseService warehouseService)
    {
        var user = await _userManager.GetUserAsync(User);
        //获取当前上报公司数据
        var company = await _companyService.GetValidDistributionList(user.CompanyID);
        var waybillState = Enum<States>.ToDictionaryForUI();

        var warehouseList = await warehouseService.AvailableWarehouses(user);
        return Success(new { company, waybillState, user.CompanyID, warehouseList, });
    }

    /// <summary>
    /// 获取运单列表
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetList(string? orderNo, string? express, string? waybillOrder, string? tracking, States? waybillState, int? companyId
        , int? warehouseId)
    {
        var user = await _userManager.GetUserAsync(User);
        var list = await _waybillService.GetListPage(user.CompanyID, orderNo, express, waybillOrder, tracking, waybillState, companyId, warehouseId: warehouseId);
        return Success(new { list });
    }

    /// <summary>
    /// 获取我的运单列表配置项
    /// </summary>
    /// <returns></returns>
    public async Task<object> GetMySearchList([FromServices] IWarehouseService warehouseService)
    {
        var user = await _userManager.GetUserAsync(User);
        //获取当前上报公司数据
        var company = await _companyService.GetValidDistributionList(user.CompanyID);
        
        Dictionary<int, string> WaybillState = Enum<Enums.Logistics.States>.ToDictionaryForUIByDescription();

        var warehouseList = await warehouseService.AvailableWarehouses(user);
        return Success(new
        {
            company,
            WaybillState, warehouseList
        });
    }
   

    //*********************************** 分销下级 *****************************************/

    /// <summary>
    /// 下级待发货信息退回内部处理
    /// </summary>
    /// <returns></returns>
    [HttpPost("{id:int}")]
    public async Task<ResultStruct> BackJuniorWaybill(int id, [FromServices] AmazonOrderService amazonOrderService)
    {
        var user = await _userManager.GetUserAsync(User);
        var res = await amazonOrderService.ToBack(id, user.CompanyID, user.Company.ReportId, 2);

        return res > 0 ? Success() : Error();
    }

    /// <summary>
    /// 下级待发货信息上报内部处理
    /// </summary>
    /// <returns></returns>
    [HttpPost("{id:int}")]
    public async Task<ResultStruct> ReportJuniorWaybill(int id, [FromServices] AmazonOrderService amazonOrderService)
    {
        var user = await _userManager.GetUserAsync(User);
        var res = await amazonOrderService.ToReport(id, user.CompanyID, user.Company.ReportId, 2);

        return res > 0 ? Success() : Error();
    }

    /// <summary>
    /// 获取待发货订单配置项
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetJuniorOrderSearchList([FromServices] Services.Caches.Store storeCache)
    {
        var user = await _userManager.GetUserAsync(User);

        var rule = new
        {
            showBack = user.Company.DistributionWaybill != Company.DistributionWaybills.TRUE,
        };
        //当前用户可选店铺
        var stores = User.IsInRole(Role.ADMIN) || User.Has(Enums.Rule.Config.Distribution.STORE_WAYBILL) 
            ? storeCache.List(user.Company) : storeCache.List(user);

        var store = stores.Select(s => new {s.ID, s.Name, s.State}).KeyBy(s => s.ID);

        //订单状态
        var state = OrderState.SearchState;
        //订单进度
        // $data['progress'] = AmazonOrderModel::ORDER_PROGRESS;
        //        //订单排序
        //        $data['sort_by'] = AmazonOrderModel::ORDER_SORT_BY;

        return Success(new { rule, store, state });
    }

    /// <summary>
    /// 获取分销待发货订单列表信息
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetJuniorOrderList(string? orderNo, int? storeId, string? progress, Enums.Orders.States? orderState, string? sortBy
        , [FromServices] AmazonOrderService amazonOrderService, [FromServices] Services.Caches.Store storeCache)
    {
        var user = await _userManager.GetUserAsync(User);
        var storeIds = Array.Empty<int>();
        if (storeId is null && !User.IsInRole(Role.ADMIN) && !User.Has(Enums.Rule.Config.Distribution.STORE_WAYBILL)) 
        {
            var store = storeCache.List(user);
            //员工无可允许查看店铺，返回空信息
            if (store is { Count: <= 0 }) {
                return Success(new { List = new {} });
            }
            storeIds = store.Select(s => s.ID).ToArray();
        }

        var p = JsonConvert.DeserializeObject<ProgressMaskValue>(progress);
        var list = await amazonOrderService.GetJuniorWaitListPage(user.CompanyID,user.Company.ReportId,orderNo, orderState, p, storeId, storeIds, 2, sortBy);
        return Success(new { list });
    }

    /// <summary>
    /// 获取运单列表配置项
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetJuniorSearchList([FromServices] Services.Caches.Store storeCache, [FromServices] IWarehouseService warehouseService)
    {
        var user = await _userManager.GetUserAsync(User);
        //当前用户可选店铺
        var stores = User.IsInRole(Role.ADMIN) || User.Has(Enums.Rule.Config.Distribution.STORE_WAYBILL) 
            ? storeCache.List(user.Company) : storeCache.List(user);

        var store = stores.Select(s => new {s.ID, s.Name, s.State}).KeyBy(s => s.ID);

        var waybillState = Enum<States>.ToDictionaryForUIByDescription();

        var warehouseList = await warehouseService.AvailableWarehouses(user);
        return Success(new { warehouseList, waybillState, user.CompanyID, store, });
    }

    /// <summary>
    /// 获取运单列表 
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetJuniorList(string? orderNo, string? express, string? waybillOrder, string? tracking, States? waybillState, int? storeId
        , int? warehouseId, [FromServices] Services.Caches.Store storeCache)
    {
        var user = await _userManager.GetUserAsync(User);
        var storeIds = Array.Empty<int>();
        if (storeId is null && !User.IsInRole(Role.ADMIN) && !User.Has(Enums.Rule.Config.Distribution.STORE_WAYBILL)) 
        {
            var store = storeCache.List(user);
            //员工无可允许查看店铺，返回空信息
            if (store is { Count: <= 0 }) {
                return Success(new { List = new {} });
            }
            storeIds = store.Select(s => s.ID).ToArray();
        }

        var list = await _waybillService.GetJuniorListPage(user.CompanyID, user.Company.ReportId, orderNo, express, waybillOrder, tracking, waybillState, storeId, storeIds,warehouseId);
        //            'progress' => AmazonOrderModel::ORDER_PROGRESS
        return Success(new { list });
    }

}
