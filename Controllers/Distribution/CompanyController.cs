﻿using System.ComponentModel.DataAnnotations;
using ERP.Enums.Identity;
using ERP.Authorization.Rule;
using ERP.Data;
using ERP.Distribution.Abstraction.Services;
using ERP.Distribution.Abstraction.ViewModels;
using ERP.Enums.Logistics;
using ERP.Extensions;
using ERP.Models.DB.Logistics;
using ERP.Models.DB.Orders;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Ryu.Linq;
using Z.EntityFramework.Plus;

namespace ERP.Controllers.Distribution;

[Route("api/distribution/[controller]/[action]")]
[ApiController]
public class CompanyController : BaseController
{
    private readonly IDistributionCompanyService _companyService;
    private readonly UserManager<Models.DB.Identity.User> _userManager;
    private readonly DBContext _dbContext;

    public CompanyController(IDistributionCompanyService companyService,
        UserManager<Models.DB.Identity.User> userManager, DBContext dbContext)
    {
        _companyService = companyService ?? throw new ArgumentNullException(nameof(companyService));
        _userManager = userManager;
        _dbContext = dbContext;
    }

    /// <summary>
    /// 获取分销公司列表
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetList()
    {
        var user = await _userManager.GetUserAsync(User);
        var list = await _companyService.GetDistributionListPage(user.CompanyID);
        var ruleConfig = User.IsInRole(Role.ADMIN) || User.Has(Enums.Rule.Config.Distribution.COMPANY_RATE);
        return Success(new { list, ruleConfig });
    }

    /// <summary>
    /// 获取分销公司费率信息
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id:int}"), Permission(Enums.Rule.Config.Distribution.COMPANY_RATE)]
    public async Task<ResultStruct> GetConfig(int id)
    {
        //获取公司配置信息
        var company = await _companyService.GetDistributionConfig(id);
        return Success(new { company });
    }

    /// <summary>
    /// 更新分销公司费率信息
    /// </summary>
    /// <returns></returns>
    [Permission(Enums.Rule.Config.Distribution.COMPANY_RATE)]
    public async Task<ResultStruct> SubConfig(SubConfigVm vm)
    {
        var res = await _companyService.UpdateDistributionConfig(vm.Id, vm);
        // todo 删除公司缓存
        // delCompanyRedis(vm.Id);
        return res ? Success() : Error();
    }

    /// <summary>
    /// 获取待分配物流
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetLogistic(int id, string? name,
        [FromServices] IDistributionWaybillService waybillService)
    {
        var user = await _userManager.GetUserAsync(User);
        //获取当前公司下物流参数
        var data = await waybillService.GetAllLogistics(user.CompanyID, name);
        if (data is { Items.Count: <= 0 })
        {
            return Success(data);
        }

        var lids = data.Items.Pluck(d => d.Id).ToArray();

        //获取指定公司被分配物流
        var allot = (await waybillService.GetAllotLogistics(id, lids)).KeyBy(m => m.ParentId);

        foreach (var item in data.Items)
        {
            if (allot.TryGetValue(item.Id, out var value))
            {
                item.Allot = 2;
                item.Charge = value.Charge;
            }
        }

        return Success(data);
    }

    public record SubLogisticVm([Required] int CompanyId, [Required] int Pid, [Required] Charges Charge);

    /// <summary>
    /// 提交物流分配信息
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> SubLogistic(SubLogisticVm vm,
        [FromServices] IDistributionWaybillService waybillService)
    {
        var info = await waybillService.GetLogisticsInfo(vm.Pid);
        if (info is null)
        {
            return Error("未查询到信息");
        }

        var user = await _userManager.GetUserAsync(User);
        var company = await _companyService.GetInfoById(vm.CompanyId);
        if (company is null)
        {
            return Error("分配公司不存在");
        }

        var insertData = new Parameter()
        {
            Data = info.Data,
            CompanyID = company.ID,
            CustomizeName = info.CustomizeName,
            Remark = info.Remark,
            UserID = user.ID,
            GroupID = user.GroupID,
            OEMID = company.OEMID,
            Platform = info.Platform.Platform,
            Source = Sources.SUPERIOR,
            ParentID = vm.Pid,
            Charge = vm.Charge,
            Opened = true,
            IsSysState = true,
        };
        var res = await waybillService.AddLogistics(insertData);
        return res ? Success() : Error();
    }

    /// <summary>
    /// 更新分配物流扣款方式
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> UpdateLogistic(SubLogisticVm vm,
        [FromServices] IDistributionWaybillService waybillService)
    {
        var res = await waybillService.ChangeChargeType(vm.CompanyId, vm.Pid, vm.Charge);
        return res ? Success() : Error();
    }

    public record DelLogisticVm(int CompanyId, int Pid);

    /// <summary>
    /// 取消分配物流信息
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> DelLogistic(DelLogisticVm vm,
        [FromServices] IDistributionWaybillService waybillService)
    {
        var res = await waybillService.DelLogistics(vm.CompanyId, vm.Pid);
        return res > 0 ? Success() : Error();
    }
    
    
    /// <summary>
    /// 将分销公司的订单分配给用户
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="userIds"></param>
    /// <returns></returns>
    [HttpPost("{orderId:int}/to")]

    public async Task<ResultStruct> DistributeOrder(int orderId, [FromBody] int[] userIds)
    {
        var user = await _userManager.GetUserAsync(User);
        var filteredIds = userIds.Distinct().ToArray();
        
        var existIds = await _dbContext.OrderDistributedLinks.Where(l =>
                l.SourceType == OrderDistributedLink.SourceIdType.DistributionOrderId && l.SourceId == orderId &&
                l.DistributedType == OrderDistributedLink.DistributedIdType.UserId)
            .Select(l => l.DistributedId)
            .ToArrayAsync();
        
        var needToAddIds = filteredIds.Except(existIds).ToArray();
        var needToRemoveIds = existIds.Except(filteredIds).ToArray();
        
        _ = needToAddIds.Select(id => _dbContext.Add(new OrderDistributedLink()
        {
            UserID = user.Id,
            GroupID = user.GroupID,
            CompanyID = user.CompanyID,
            OEMID = user.OEMID,
            SourceType = OrderDistributedLink.SourceIdType.DistributionOrderId,
            SourceId = orderId,
            DistributedType = OrderDistributedLink.DistributedIdType.UserId,
            DistributedId = id,
        })).ToArray();

        await _dbContext.OrderDistributedLinks.Where(l =>
            l.SourceType == OrderDistributedLink.SourceIdType.DistributionOrderId && l.SourceId == orderId &&
            l.DistributedType == OrderDistributedLink.DistributedIdType.UserId &&
            needToRemoveIds.Contains(l.DistributedId)).DeleteAsync();

        await _dbContext.SaveChangesAsync();

        return Success();
    }

    /// <summary>
    /// 将分销公司的所有订单分配给用户
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="userIds"></param>
    /// <returns></returns>
    [HttpPost("{companyId:int}/to")]
    public async Task<ResultStruct> DistributeOrders(int companyId, [FromBody]int[] userIds)
    {
        var user = await _userManager.GetUserAsync(User);
        var filteredIds = userIds.Distinct().ToArray();
        
        var existIds = await _dbContext.OrderDistributedLinks.Where(l =>
                l.SourceType == OrderDistributedLink.SourceIdType.DistributionCompanyId && l.SourceId == companyId &&
                l.DistributedType == OrderDistributedLink.DistributedIdType.UserId)
            .Select(l => l.DistributedId)
            .ToArrayAsync();
        var needToAddIds = filteredIds.Except(existIds).ToArray();
        var needToRemoveIds = existIds.Except(filteredIds).ToArray();

        _ = needToAddIds.Select(id => _dbContext.Add(new OrderDistributedLink()
        {
            UserID = user.Id,
            GroupID = user.GroupID,
            CompanyID = user.CompanyID,
            OEMID = user.OEMID,
            SourceType = OrderDistributedLink.SourceIdType.DistributionCompanyId,
            SourceId = companyId,
            DistributedType = OrderDistributedLink.DistributedIdType.UserId,
            DistributedId = id,
        })).ToArray();

        await _dbContext.OrderDistributedLinks.Where(l =>
            l.SourceType == OrderDistributedLink.SourceIdType.DistributionCompanyId && l.SourceId == companyId &&
            l.DistributedType == OrderDistributedLink.DistributedIdType.UserId &&
            needToRemoveIds.Contains(l.DistributedId)).DeleteAsync();

        await _dbContext.SaveChangesAsync();

        return Success();
    }
}