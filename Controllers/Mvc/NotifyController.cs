using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace ERP.Controllers.Mvc;

[Route("/Notify")]
public class NotifyController : Controller
{
    public NotifyController()
    {
        
    }
    [HttpGet("alipayReturnUrl")]
    public IActionResult AlipayReturnUrl([FromQuery] AlipayReturnDto req)
    {
        ViewData["total_amount"] = req.total_amount;
        ViewData["trade_no"] = req.trade_no;
        ViewData["out_trade_no"] = req.out_trade_no;
        ViewData["timestamp"] = req.timestamp;
        return View("~/Pages/Notify/alipayReturn.cshtml");
    }
}

public class AlipayReturnDto
{
    public decimal total_amount { get; set; }
    public decimal trade_no { get; set; }
    public decimal out_trade_no { get; set; }
    public DateTime timestamp { get; set; }
}