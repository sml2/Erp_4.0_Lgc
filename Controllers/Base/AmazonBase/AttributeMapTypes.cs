﻿namespace ERP.Controllers.AmazonBase
{
    public enum AttributeMapTypes
    {
        None, Required, Optional, Preferred, Recommended
    }
}
