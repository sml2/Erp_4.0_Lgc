using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using ERP.Models.ProductNodeType;
using ERP.Models.ProductNodeType.Enum;
using ERP.Models.DB;
using ERP.Enums;
using ERP.Data;
using ERP.Models;

namespace ERP.Controllers.AmazonBase
{
    //VTable
    public class AttributeMapRange:Dictionary<AttributeMapTypes, Dictionary<string, Dictionary<string, bool>>>
    {
        public string[] AttributeMapFeedProductType { get; set; }

        public AttributeMapRange(XSSFWorkbook XSSFWorkbook, int formRawID, int rID,DBContext DBContext)
        {
            try
            {
                var attributeSheet = XSSFWorkbook.GetSheet("AttributePTDMap");
                AttributeMapFeedProductType = GetRangeProduct(attributeSheet);
                var CTWorkbook = XSSFWorkbook.GetCTWorkbook();
                var definedNames = CTWorkbook.definedNames;
                foreach (var definedName in definedNames.definedName)
                {
                    var name = definedName.name.ToUpper();
                    if (name.Contains("ATTRIBUTEPTDMAP"))
                    {
                        string pattern = @"\d{1,}";
                        Regex r = new Regex(pattern);
                        //=AttributePTDMAP!$A$1:$K$33
                        //=AttributePTDMAP!$A$385:$K$385
                        MatchCollection mCollection = r.Matches(definedName.Value);
                        if (int.TryParse(mCollection[0].Value, out int begin) && int.TryParse(mCollection[1].Value, out int end))
                        {
                            AttributeMapTypes attributeMapTypes;
                            if (name.Contains("REQUIRED"))
                                attributeMapTypes = AttributeMapTypes.Required;
                            else if (name.Contains("OPTIONAL"))
                                attributeMapTypes = AttributeMapTypes.Optional;
                            else if (name.Contains("PREFERRED"))
                                attributeMapTypes = AttributeMapTypes.Preferred;
                            else if (name.Contains("RECOMMENDED"))
                                attributeMapTypes = AttributeMapTypes.Recommended;
                            else
                                attributeMapTypes = AttributeMapTypes.None;

                            Dictionary<string, Dictionary<string, bool>> keyValues = new Dictionary<string, Dictionary<string, bool>>();
                            for (int rowIndex = begin; rowIndex < end; rowIndex++)
                            {
                                IRow row = attributeSheet.GetRow(rowIndex);
                                var attr = row.GetCell(0).StringCellValue;
                                Dictionary<string, bool> valuePairs = new Dictionary<string, bool>();
                                for (int j = 1; j < row.Count(); j++)
                                {
                                    var productType = AttributeMapFeedProductType[j - 1];
                                    valuePairs.Add(productType, row.GetCell(j).ToString() == "1" ? true : false);
                                }
                                
                                if(!keyValues.ContainsKey(attr))
                                {
                                    keyValues.Add(attr, valuePairs);
                                }
                                else{
                                    //同一个区域中存在同名的attribute，并且值不一样，先取第一次出现的，看看最后生成的结果
                                    //throw new Exception("同一个区域中存在同名的attribute");
                                    string message=$"一个区域中存在同名的属性名称：{attr}";
                                    ErrorDatas error = new ErrorDatas(Platforms.AMAZON, formRawID, rID, message);
                                    DBContext.ErrorDatas.Add(error);
                                    DBContext.SaveChanges();
                                }
                            }
                            Add(attributeMapTypes, keyValues);
                        }
                        else
                        {
                            throw new Exception($"区域取值不对：{definedName.Value}");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }


        public string[] GetRangeProduct(ISheet attributeSheet)
        {
            IRow row = attributeSheet.GetRow(0);
            string[] array = new string[row.LastCellNum-1];           
            for (int j = 0; j < row.LastCellNum; j++)

            {
                if (row.GetCell(j).StringCellValue == "")
                {
                    continue;
                }
                array[j-1]=row.GetCell(j).StringCellValue;
            }
            return array;
        }
    }
}
