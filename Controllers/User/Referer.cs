﻿using ERP.Data;
using ERP.Extensions;
using ERP.Models.DB.Users;
using ERP.Models.View.User.Referer;
using ERP.Services.DB.Users;
using GroupService = ERP.Services.DB.Users.GroupService;
using OEMCache = ERP.Services.Caches.OEM;
using PackageModel = ERP.Models.DB.Users.Package;
using Role = ERP.Enums.Identity.Role;
using RuleTemplateModel = ERP.Models.DB.Users.RuleTemplate;
using RuleTemplateService = ERP.Services.DB.Users.RuleTemplate;
using UserGroupModel = ERP.Models.DB.Users.Group;
using UserModel = ERP.Models.DB.Identity.User;

namespace ERP.Controllers.User;

public class Referer : BaseController
{
    private readonly UserService _userService;
    private readonly CompanyService _companyService;
    private readonly OEMCache _oemCache;
    private readonly RuleTemplateService _ruleTemplateService;
    private readonly DBContext _dbContext;
    private readonly UserOperateLogService _userOperateLogServiceService;
    private readonly GroupService _groupService;

    public Referer(
        UserService userService,
        CompanyService companyService,
        OEMCache oemCache,
        DBContext dbContext,
        GroupService groupService,
        UserOperateLogService userOperateLogServiceService,
        RuleTemplateService ruleTemplateService)
    {
        _userService = userService;
        _companyService = companyService;
        _oemCache = oemCache;
        _ruleTemplateService = ruleTemplateService;
        _dbContext = dbContext;
        _userOperateLogServiceService = userOperateLogServiceService;
        _groupService = groupService;
    }
    private ISession Session => HttpContext.Session;

    private async Task<ResultStruct> ruleInfo(int id)
    {
        return await Task.FromResult(Success());
        //return $this->sessionData->getRule()->getUser()->getBiter();
    }

    /// <summary>
    /// 用户权限
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> GetList(GetListDTO dto)
    {
        var session = HttpContext.Session;
        var rule = new
        {
            add = true,
            recharge = true,
            buyPackage = true,
        };
        if (session.GetRole() == Role.Employee)
        {
            //$rule = [
            //    'add'        => $this->ruleInfo()->hasRefererAdd(),
            //    'recharge'   => $this->ruleInfo()->hasRefererRecharge(),
            //    'buyPackage' => $this->ruleInfo()->hasRefererBuyPackage(),
            //];
        }
        var companyId = session.GetCompanyID();
        DbSetExtension.PaginateStruct<Company> list;
        List<UserModel> user;
        int special = 1;
        //if ($companyId === UserModel::COMPANY_ID && (checkStr($mobile) || checkStr($username))) {
        if (companyId == 2/*//小米 米境通 公司id*/ && !dto.Mobile.IsNullOrWhiteSpace() || !dto.UserName.IsNullOrWhiteSpace())
        {
            //查询独立用户
            user = await _userService.GetOwnUser(null, dto.UserName, dto.Mobile);
            if (user.Count > 0)
                return Success(new
                {
                    list = new { data = "", total = 0 },
                    rule,
                    companyId,
                    special
                });
            //$user = array_combine($companyIds, $user);

            list = await _companyService.GetRefererList(dto.Name, user.Select(u => u.CompanyID).ToList(), 0, dto.IsAgency);
            if (list.Items.Count == 0)
                return Success(new
                {
                    list,
                    rule,
                    companyId,
                    special
                });
        }
        else
        {
            list = await _companyService.GetRefererList(dto.Name, null, companyId, dto.IsAgency);
            if (list.Items.Count == 0)
                return Success(new
                {
                    list,
                    rule,
                    companyId,
                    special = 1
                });
            user = await _userService.GetOwnUser(list.Items.Select(a => a.ID).ToList());
        }
        //特殊公司ids
        int[] companyIds = new[]
        {
            2,////小米 米境通 公司id
            3973,//陈婧
            //3974,//唐海龙
            3975,//冯小宁
        };
        object o = new();
        if (companyIds.Contains(companyId))
        {
            special = 2;
            var oems = _oemCache.List();
            o = list.Transform(a => new
            {
                a.ID,
                agency = a.IsAgency,
                uid = user.FirstOrDefault(u => u.CompanyID == a.ID)?.ID,
                oemId = a.OEMID,
                brand = oems?.FirstOrDefault(o => o.ID == a.OEMID)?.Name,
                userName = user.FirstOrDefault(u => u.CompanyID == a.ID)?.UserName,
                mobile = user.FirstOrDefault(u => u.CompanyID == a.ID)!.Mobile.IsNotWhiteSpace() ? user.FirstOrDefault(u => u.CompanyID == a.ID)!.Mobile : "暂未绑定",
                companyName = a.Name,
                a.ReferrerKwSign,
                a.CreatedAt,
                a.Type,
                package = Extensions.EnumExtension.GetDescription(a.Type),
                a.Balance,
                a.Discount,
                a.IsOpenUser,
                a.OpenUserNum,
                validity = a.Type == Company.Types.TWO ? "不限" : a.ShowValidityInfo,
                day = a.Type == Company.Types.TWO ? 0 : (int)Math.Floor(Convert.ToDecimal(a.Validity - DateTime.Now) / 86400),
                firstLoginWeb = user.FirstOrDefault(u => u.CompanyID == a.ID)?.FirstLoginWeb != null ? user.FirstOrDefault(u => u.CompanyID == a.ID)?.FirstLoginWeb.ToString() : "暂未登录",
                firstLoginExe = user.FirstOrDefault(u => u.CompanyID == a.ID)?.FirstLoginExe != null ? user.FirstOrDefault(u => u.CompanyID == a.ID)?.FirstLoginExe.ToString() : "暂未登录",
                firstRechargeDate = a.FirstRechargeDate != null ? a.FirstRechargeDate.ToString() : "暂未首冲"
            });
        }
        else
        {
            o = list.Transform(a => new
            {
                a.ID,
                uid = user.FirstOrDefault(u => u.CompanyID == a.ID)?.ID,
                a.OEMID,
                userName = user.FirstOrDefault(u => u.CompanyID == a.ID)?.UserName,
                companyName = a.Name,
                a.CreatedAt,
                a.Type,
                package = Extensions.EnumExtension.GetDescription(a.Type),
                validity = a.Type == Company.Types.TWO ? "不限" : a.ShowValidityInfo,
                day = a.Type == Company.Types.TWO ? 0 : (int)Math.Floor(Convert.ToDecimal(a.Validity - DateTime.Now) / 86400),
            });
        }

        return Success(new
        {
            list = o,
            rule,
            companyId,
            special
        });
    }

    /// <summary>
    /// 注册用户
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> SubInfo(SubInfoDTO dto)
    {
        var session = HttpContext.Session;
        if (session.Is(Role.Employee)  /*&& !$this->ruleInfo()->hasRefererAdd()*/ )
        {
            return Error(message: "没有权限");
        }
        //检测用户名是否存在
        var info = await _userService.CheckUserName(dto.UserName);
        if (info is not null)
        {
            return Error(message: "用户已存在");
        }
        var company = await _companyService.GetCurrentConfig(session.GetCompanyID());
        if (company is null)
        {
            return Error(message: "无法获取公司配置信息");
        }
        //小米开放注册用户 开户需要公共数据
        //???
        TimeSpan t = TimeSpan.FromTicks(1918007754);
        var registerInfo = new PackageModel
        {
            OEMID = dto.OemId > 0 ? dto.OemId : 1,
            Uid = 2,
            TrueName = "米境通",
            CompanyID = 2,
            Pids = "[1,2]",
            RuleTemplateId = company.ShowOemId != 1 ? 3881 : 1168,
            GiveIntegral = 50,
            RealityValidityId = 1,
            RealityValidity = Convert.ToDateTime(t.ToString()),
            OpenUserNum = 0,
            SecondPackageOem = company.ShowOemId != 1,
        };
        //贴牌 开通套餐二代
        var rule = await _ruleTemplateService.FindByIdAsync(registerInfo.RuleTemplateId);
        if (rule is null)
        {
            return Error(message: "获取权限信息失败");
        }
        var ruleTemplate = new RuleTemplateModel
        {
            Name = "初始权限组",
            State = Models.Abstract.BaseModel.StateEnum.Open,
            OEMID = registerInfo.OEMID,
            Remark = string.Empty
        };
        var userGrouop = new UserGroupModel
        {
            Name = "初始用户组",
            State = Models.Abstract.BaseModel.StateEnum.Open,
            OEMID = registerInfo.OEMID
        };
        using var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            //添加独立账号
            var user = await _userService.InitRegisterUser(dto, registerInfo);
            //增加创建用户信息日志
            await _userOperateLogServiceService.AddRegisterUserLog(user.ID, registerInfo);
            //添加有效期???
            var validity = DateTime.Now;
            //$UserValidityService = new UserValidity();
            //$show_validity_id = $UserValidityService->addValidity($validity);

            //初始化公司
            var newCompany = await _companyService.InitRegisterCompany(dto, registerInfo, 1, validity, HttpContext.Session.GetCompanyID());
            //更新用户公司id
            await _userService.UpdateUserCompanyId(user, newCompany.ID);
            //初始化权限模版 复制权限模板
            var newRule = await _ruleTemplateService.CopyRule(rule.ID, registerInfo.OEMID, newCompany.ID);
            ruleTemplate.CompanyID = newCompany.ID;
            await _ruleTemplateService.SaveInfo(0, newRule.ID, ruleTemplate, newCompany.ID, registerInfo.OEMID);
            //初始化用户组
            userGrouop.CompanyID = newCompany.ID;
            var group = await _groupService.SaveInfo(userGrouop);

            //初始化用户
            //业务统计数据
            //$CounterUserBusinessService = new CounterUserBusiness();
            //$CounterUserBusinessService->initCounterUserBusiness($user_id, $company_id, $registerInfo['oem_id']);

            ////初始化用户组业务统计数据
            //$CounterGroupBusiness = new CounterGroupBusiness();
            //$CounterGroupBusiness->initCounterGroupBusiness($group_id, $company_id, $registerInfo['oem_id']);

            ////初始化公司业务统计数据
            //$CounterCompanyBusinessService = new CounterCompanyBusiness();
            //$CounterCompanyBusinessService->initCounterCompanyBusiness($company_id, $registerInfo['oem_id']);

            ////初始化资源统计数据
            //$CounterRequestService = new CounterRequest();
            //$CounterRequestService->initCounterRequest($user_id, 0, $company_id, $register);

            ////上报当前用户开通用户数 d_user_num
            //$CounterUserBusinessService->userNumAdd(1, UserModel::REFERER_UID);
            ////上报当前公司新增用户数 d_user_num
            //$CounterCompanyBusinessService->userNumAdd(1, UserModel::COMPANY_ID);
            ////上报上级全部公司新增用户数 d_report_user_num
            //$CounterCompanyBusinessService->reportRegisterUserNumAdd(UserModel::REGISTER_INFO['pids']);

            await transaction.CommitAsync();
        }
        catch (Exception /*ex*/)
        {
            await transaction.RollbackAsync();
        }
        return Success();
    }

    /// <summary>
    /// 获取购买套餐需要余额信息
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    public async Task<ResultStruct> GetBuyInfo()
    {
        //$company = $this->companyService->getCurrentConfig($this->sessionData->getCompanyId());
        //$balance = ToConversion($company->getRawOriginal('balance'), 'CNY'); ???
        var company = await _companyService.GetCurrentConfig(Session.GetCompanyID());
        var balance = company.Balance;
        return Success(new { balance, discount = company.Discount });
    }


    public class SubConfigVM
    {
        public int CompanyId { get; set; }
        public int Discount { get; set; }
        public bool IsOpenUser { get; set; }
        public int OpenUserNum { get; set; }
        public int Uid { get; set; }
    }

    public async Task<ResultStruct> SubConfig(SubConfigVM request)
    {
        string log = $"修改分成比例为:{request.Discount}%";
        if (request.IsOpenUser)
        {
            request.OpenUserNum = 0;
            log += "，内部员工开通数不限制";
        }
        else
        {
            log += $"，内部员工限制为：{request.OpenUserNum}个";
        }
        var company = await _companyService.GetInfoById(request.CompanyId);
        company.Discount = request.Discount;
        company.IsOpenUser = request.IsOpenUser;
        company.OpenUserNum = request.OpenUserNum;
        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {

            await _companyService.UpdateCompanyById(company);
            //$this->delCompanyRedis($companyId);

            //增加更新用户信息日志
            await _userOperateLogServiceService.AddUserLog(request.Uid, log);
            transaction.Commit();
        }
        catch
        {
            transaction.Rollback();
            return Error();
        }
        return Success();
    }
}

