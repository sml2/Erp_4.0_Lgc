﻿using ERP.Authorization.Rule;
using ERP.Data;
using ERP.Extensions;
using ERP.Services.Statistics;
using ERP.Services.Stores;
using ERP.Services.DB.Users;
using static ERP.Models.Abstract.BaseModel;
using static ERP.Models.DB.Identity.User;
using ERP.Models.View.User.Group;
using ERP.Models.DB.Identity;
using ERP.Enums.Identity;
using Microsoft.EntityFrameworkCore;

namespace ERP.Controllers.User;
using Model = Models.DB.Users.Group;

public class Group : BaseController
{
    private readonly GroupService _groupService;
    private readonly CounterGroupBusinessService _counterGroupBusinessService;
    private readonly UserService _userService;
    private readonly StoreGroupService _storeGroupService;
    private readonly DBContext _dbContext;

    public Group(GroupService groupService, CounterGroupBusinessService counterGroupBusinessService, UserService userService, StoreGroupService storeGroupService, DBContext dbContext)
    {
        _groupService = groupService;
        _counterGroupBusinessService = counterGroupBusinessService;
        _userService = userService;
        _storeGroupService = storeGroupService;
        _dbContext = dbContext;
    }

    /**
 * 用户组权限
 * @author CCY
 * @createdAt 2020/6/3 17:52
 * @return \App\Rule\BitwiseFlags\User
 */
    //    private function ruleInfo(): \App\Rule\BitwiseFlags\User
    //{
    //    return $this->sessionData->getRule()->getUser()->getBiter();
    //}

    /// <summary>
    /// 获取用户组列表
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> GetList(GetListDTO dto)
    {
        var companyId = HttpContext.Session.GetCompanyID();
        var oemId = HttpContext.Session.GetOEMID();
        var list = await _groupService.GetGroupListPage(dto, companyId, oemId);
        return Success(list);
    }

    /// <summary>
    /// 获取配置项
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetSearchList()
    {
        var rule = new
        {
            ruleAdd = true,
            ruleEdit = true,
            ruleDel = true
        };
        //用户组权限
        if (HttpContext.Session.Is(Enums.Identity.Role.Employee))
        {
            //$data['rule'] = [
            //    'ruleAdd'  => $this->ruleInfo()->hasGroupAdd(),
            //    'ruleEdit' => $this->ruleInfo()->hasGroupEdit(),
            //    'ruleDel'  => $this->ruleInfo()->hasGroupDel()
            //];
        }
        var data = new { rule, group_id = HttpContext.Session.GetGroupID() };
        return await Task.FromResult(Success(data));
    }

    /// <summary>
    /// 用户组详情
    /// </summary>
    /// <returns></returns>
    [Route("{id}")]
    [HttpPost]
    [Permission(Enums.Rule.Config.User.GroupEdit)]
    public async Task<ResultStruct> Info(int id)
    {
        //if (!$this->ruleInfo()->hasGroupEdit() && $this->sessionData->getConcierge() == UserModel::TYPE_CHILD) {
        //    return error(getlang('public.authNoOperate'));
        //}
        //$id = $request->input('id');
        //if (!$id) {
        //    return error(getlang('public.missingparam',['param'=>'ID']));
        //}

        //$info = $this->GroupService->getInfoById($id);
        var info = await _groupService.GetInfoById(id);
        return Success(info);
    }

    /// <summary>
    /// 提交用户组信息
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    [Permission(Enums.Rule.Config.User.GroupAdd)]
    public async Task<ResultStruct> SubInfo(Model data)
    {
        //$id = $request->input('id');
        //$data = $request->input('info');
        //if (!$data) {
        //    return error(getlang('public.missingparam',['param'=>'Info']));
        //}
        int id = data.ID;
        data.CompanyID = HttpContext.Session.GetCompanyID();
        data.OEMID = HttpContext.Session.GetOEMID();
        //if (!$this->ruleInfo()->hasGroupAdd() && $this->sessionData->getConcierge() == UserModel::TYPE_CHILD) {
        //    return error(getlang('public.authNoOperate'));
        //}
        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            var group = await _groupService.SaveInfo(data);

            if (id == 0)//添加
            {
                //新增、增加统计记录
                ////初始化用户组业务统计数据
                //$CounterGroupBusiness = new CounterGroupBusiness();
                await _counterGroupBusinessService.InitCounterGroupBusiness(group.ID, data.CompanyID, data.OEMID);
            }
            await transaction.CommitAsync();
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            Console.WriteLine(e.Message);
            return Error(e.Message);
        }
       
        return Success();
    }

    /// <summary>
    /// 删除
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Route("{id}")]
    [Permission(Enums.Rule.Config.User.GroupDel)]
    public async Task<ResultStruct> DelInfo(int id)
    {
        //if (!$this->ruleInfo()->hasGroupDel() && $this->sessionData->getConcierge() == UserModel::TYPE_CHILD) {
        //    return error(getlang('public.authNoOperate'));
        //}
        //检测用户组下用户
        int userNum = await _userService.CheckUserNumByGroupId(id);
        if (userNum > 0)
        {
            return Error(message: "此用户组下存在内部员工");
        }
        
        //检测用户组下店铺
        
        int storeNum = await _userService.CheckStoreNumByGroupId(id);
        if (storeNum > 0)
        {
            return Error(message: "此用户组下存在分配的店铺");
        }

        var res = await _groupService.DeleteInfoById(id);
        if (res.State == false)
            return Error(message: res.Msg);
        //删除用户组统计数据
        await _counterGroupBusinessService.DelCounterGroupBusiness(id);
        //删除可能存在的被分配店铺信息
        await _storeGroupService.DelAllot(id);
        return Success();
    }
}

