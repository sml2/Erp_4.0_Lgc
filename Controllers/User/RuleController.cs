﻿using ERP.Authorization.Rule;
using ERP.Enums.Identity;
using ERP.Enums.Rule;
using ERP.Enums.View.Rule;
using ERP.Extensions;
using ERP.Interface;
using ERP.Interface.Rule;
using ERP.Models.Abstract;
using ERP.Models.DB.Users;
using ERP.Models.View.User.User;
using ERP.Services.Identity;
using ERP.ViewModels.User;
using ERP.Models.View.Users;
using ERP.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ERP.Controllers.User;

using RuleTemplateService = Services.DB.Users.RuleTemplate;

public class RuleController : BaseController
{
    private readonly RuleTemplateService _RuleTemplateService;
    private readonly UserManager _userManager;

    public RuleController(RuleTemplateService ruleTemplateService, UserManager userManager)
    {
        _RuleTemplateService = ruleTemplateService;
        _userManager = userManager;
    }

    /// <summary>
    /// 获取权限模版列表
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> GetList([FromQuery] string? name, [FromQuery] BaseModel.StateEnum? state)
    {
        var user = await _userManager.GetUserAsync(User);
        var list = await _RuleTemplateService.GetRuleListPage(name, state, user.CompanyID, user.OEMID);
        return Success(list);
    }

    [HttpPost]
    public async Task<ResultStruct> GetAuthItems(GetAuthItemsVM vm,
        [FromServices] Services.Caches.Menu menuCache,
        [FromServices] IManager config,
        [FromServices] ILogger<RuleController> logger,
        [FromServices] RoleManager roleManager)
    {
        int id = vm.Id;
        Modes mode = vm.Mode;
        Func<IHas, bool> Has;
        switch (mode)
        {
            case Modes.RuleTemplate:
                var ruleTemplate = await _RuleTemplateService.FindByIdAsync(id);
                if (ruleTemplate is null)
                    return Error("模版不存在#53");
                Has = ruleTemplate.Has;
                break;
            case Modes.Role:
                var role = await roleManager.FindByIdAsync(id);
                if (role is null)
                    return Error("角色不存在#59");
                var roleClaims = await roleManager.GetClaimsAsync(role);
                Has = roleClaims.Has;
                break;
            case Modes.User:
                var user = await _userManager.FindByIdAsync(id);
                if (user is null)
                    return Error("用户不存在#66");
                var userClaims = await _userManager.GetClaimsAsync(user);
                Has = userClaims.Has;
                break;
            default:
                throw new NotSupportedException($"不支持的改动模式");
        }

        Func<IHas, bool> CanSee = (hr) => User.Has(hr) || User.IsInRole(Role.SU);
        var orderModuleMenus = menuCache.ListClone()?.OrderBy(i => i.ParentId).ThenBy(i => i.Sort);
        if (orderModuleMenus is null) return Error("缓存异常#43");
        var orderModuleMenusWithPermission = orderModuleMenus.Where(m => CanSee(m));
        if (orderModuleMenusWithPermission.Any())
        {
            List<AuthItemData> list = new();
            List<string> keys = new();
            //模块菜单权限
            foreach (var module in orderModuleMenusWithPermission)
            {
                var HasAllMenu = true;
                var HasOneMenu = false;
                AuthItemData aiModule = new(module);
                //遍历二级菜单
                foreach (var menu in module.Children.Where(m => CanSee(m)))
                {
                    var HasAllVisible = true;
                    var HasOneVisible = false;
                    AuthItemData aimenu = new(menu);
                    aiModule.Add(aimenu);
                    //给二级菜单添加可配置项
                    var cs = config.Get(menu);
                    if (cs is not null)
                    {
                        foreach (var c in cs)
                        {
                            if (CanSee(c))
                                aimenu.Add(new(c, c.Data.AuthLabel));
                            if (Has(c))
                            {
                                HasOneVisible = true;
                                keys.Add($"{c.Key}.{c.Value}");
                            }
                            else
                                HasAllVisible = false;
                        }
                    }

                    var HasMenu = Has(menu);
                    if (false && HasOneVisible ? HasAllVisible && HasMenu : HasMenu) //:check-strictly="true"
                    {
                        HasOneMenu = true;
                        keys.Add(aimenu.IntID.ToString());
                    }
                    else
                        HasAllMenu = false;
                }

                var HasModule = Has(module);
                if (false && HasOneMenu ? HasAllMenu && HasModule : HasModule) //:check-strictly="true"
                    keys.Add(aiModule.IntID.ToString());
                list.Add(aiModule);
            }

            return Success(new {list, keys});
        }
        else
        {
            var msg = "未发现顶级菜单#79";
            logger?.LogWarning(msg);
            return Error(msg);
        }
    }

    /// <summary>
    /// 提交权限模版提交权限信息
    /// </summary>
    /// <param name="vm"></param>
    /// <returns></returns>
    /// <exception cref="NotSupportedException"></exception>
    [HttpPost]
    [Permission(Enums.Rule.Config.User.RuleConfig)]
    public async Task<ResultStruct> SubAuthInfo(AuthInfo vm)
    {
        switch (vm.Mode)
        {
            case Modes.RuleTemplate:
                await _RuleTemplateService.SetRuleTemplateAuthData(vm);
                break;
            case Modes.Role:

                break;
            case Modes.User:
                await _RuleTemplateService.SetUserAuthData(vm);
                break;
            default:
                throw new NotSupportedException($"不支持的改动模式");
        }

        //RuleTemplateService RuleTemplateService = new(_dbContext);
        //RuleTemplateService.UpdatePermission(ruleTemplate, authInfo.ID);
        return Success();
    }

    [HttpPost]
    public async Task<ResultStruct> GetConfigItems(GetConfigItemsVM vm,
        [FromServices] Services.Caches.Menu menuCache,
        [FromServices] IManager config,
        [FromServices] ILogger<RuleController> logger,
        [FromServices] RoleManager roleManager)
    {
        
        var rid = vm.rid;
        var mid = vm.mid;
        var range = vm.range;
        var mode = vm.mode;
        var id = rid;
        Func<IHas, bool> Has = User.Has;
        Func<IConfigVisitor, long> Val;

        switch (mode)
        {
            case Modes.RuleTemplate:
                var ruleTemplate = await _RuleTemplateService.FindByIdAsync(id);
                if (ruleTemplate is null)
                    return Error("模版不存在#137");
                Val = ruleTemplate.Has;
                break;
            case Modes.Role:
                var role = await roleManager.FindByIdAsync(id);
                if (role is null)
                    return Error("角色不存在#59");
                var roleClaims = await roleManager.GetClaimsAsync(role);
                Val = roleClaims.Has;
                break;
            case Modes.User:
                var user = await _userManager.FindByIdAsync(id);
                if (user is null)
                    return Error("用户不存在#66");
                var userClaims = await _userManager.GetClaimsAsync(user);
                Val = userClaims.Has;
                break;
            default:
                throw new NotSupportedException($"不支持的改动模式");
        }

        var isModule = range == ConfigRanges.Module;
        var TargetContent = menuCache.Menus(isModule).First(m => m.ID == mid);
        if (TargetContent is null) return Error("请求内容不存在#107");
        if (User.Has(TargetContent))
        {
            if (isModule)
            {
                List<object> keyValuePairs = new();
                foreach (var Menu in TargetContent.Children.Where(User.Has))
                {
                    var item = config.Get(Menu.ID);

                    if (item is null)
                    {
                        continue;
                    }

                    var data = item.RenderConfigItems(Has, Val);
                    keyValuePairs.Add(new {Menu.Name, data, Menu.ID});
                }

                return Success(keyValuePairs);
            }
            else
            {
                var ConfigItems = config.Get(mid);
                if (ConfigItems is null)
                {
                    return Success("该菜单下暂无配置项");
                }
                else
                {
                    return Success(ConfigItems.RenderConfigItems(Has, Val));
                }
            }
        }
        else
        {
            var msg = $"当前用户不具备请求{TargetContent.Name}的权限";
            logger.LogWarning(msg + "[UID={uid},MID={mid},RID ={rid}]", HttpContext.Session.GetUserID(), mid, rid);
            return Warning(msg, "权限不足");
        }
    }

    /// <summary>
    /// 提交权限模版提交权限信息
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost]
    [Permission(Enums.Rule.Config.User.RuleConfig)]
    public async Task<ResultStruct> SetConfigItems(SetConfigItemsDto req)
    {
        switch (req.Mode)
        {
            case Modes.RuleTemplate:
                await _RuleTemplateService.SetConfigItems(req.Id, req.Config);

                break;
            case Modes.Role:

                break;
            case Modes.User:
                await _RuleTemplateService.SetUserConfigItems(req);
                break;
            default:
                throw new NotSupportedException($"不支持的改动模式");
        }

        return Success();
    }


    [HttpPost]
    [Permission(Enums.Rule.Config.User.RuleEdit)]
    public async Task<ResultStruct> Info(IdDto req)
    {
        //if (!$this->ruleInfo()->hasRuleEdit() && $this->sessionData->getConcierge() == UserModel::TYPE_CHILD) {
        //    return error(getlang('public.authNoOperate'));
        //}
        //$id = $request->input('id');
        //if (!$id) {
        //    return error(getlang('public.missingparam', ['param' => 'ID']));
        //}
        var rule = await _RuleTemplateService.FindByIdAsync(req.ID);
        return Success(new {rule});
    }

    [HttpPost]
    [Permission(Enums.Rule.Config.User.RuleDel)]
    public async Task<ResultStruct> Delete(IdDto req)
    {
        //if (!$this->ruleInfo()->hasRuleEdit() && $this->sessionData->getConcierge() == UserModel::TYPE_CHILD) {
        //    return error(getlang('public.authNoOperate'));
        //}
        //$id = $request->input('id');
        //if (!$id) {
        //    return error(getlang('public.missingparam', ['param' => 'ID']));
        //}
        var result = await _RuleTemplateService.Delete(req.ID);
        return result ? Success() : Error();
    }

    public record SubInfoVm(string Name, string Remark, BaseModel.StateEnum State, int? Id = null);

    /// <summary>
    /// 提交权限模版信息
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    [Permission(Enums.Rule.Config.User.RuleAdd)]
    public async Task<ResultStruct> SubInfo(SubInfoVm vm, [FromServices] Services.DB.Users.UserService userService)
    {
        var model = new RuleTemplate()
        {
            Name = vm.Name,
            Remark = vm.Remark,
            State = vm.State,
        };
        if (vm.Id.HasValue)
        {
            // if (!$this->ruleInfo()->hasRuleEdit() && $this->sessionData->getConcierge() == UserModel::TYPE_CHILD) {
            //     return error(getlang('public.authNoOperate'));
            // }

            await _RuleTemplateService.SaveInfo(vm.Id.Value, 0, model);
            return Success();
        }

        //新增
        // if (!$this->ruleInfo()->hasRuleAdd() && $this->sessionData->getConcierge() == UserModel::TYPE_CHILD) {
        //     return error(getlang('public.authNoOperate'));
        // }

        var ruleId = 1;
        var user = await _userManager.GetUserAsync(User);
        if (user.Id != Constants.SuperId)
        {
            var admin = await userService.GetCompanyAdmin(user);
            //复制公司权限信息
            var ruleGroup = await _RuleTemplateService.FindByIdAsync(admin.FormRuleTemplateID);
            if (ruleGroup is null)
                return Error("无法找到权限模板");
            ruleId = ruleGroup.ID;
        }

        // DB::beginTransaction();
        try
        {
            var res = await _RuleTemplateService.CopyRule(ruleId, user.OEMID, user.CompanyID);

            await _RuleTemplateService.SaveInfo(res.ID, res.ID, model, user.CompanyID, user.OEMID);

            // DB::commit();
        }
        catch (Exception e)
        {
            // DB::rollBack();
            return Error(e.Message);
        }

        return Success();
    }

    /// <summary>
    /// 角色克隆
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost]
    [Permission(Enums.Rule.Config.User.RuleCopy)]
    public async Task<ResultStruct> Copy(CopyRuleDto req)
    {
        var info = await _RuleTemplateService.FindByIdAsync(req.Id);

        var user = await _userManager.GetUserAsync(User);


        var model = new RuleTemplate
        {
            Name = req.Name,
            State = BaseModel.StateEnum.Open,
        };

        try
        {
            var res = await _RuleTemplateService.CopyRule(info.ID, user.OEMID, user.CompanyID);

            await _RuleTemplateService.SaveInfo(res.ID, res.ID, model, user.CompanyID, user.OEMID);
        }
        catch (Exception e)
        {
            return Error(e.Message);
        }

        return Success();
    }
}