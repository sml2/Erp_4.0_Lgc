using ERP.Data;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.View.Information;
using ERP.Models.View.User.User;
using ERP.Services.DB.Statistics;
using ERP.Services.DB.Users;
using ERP.Services.Finance;
using ERP.Services.Identity;
using ERP.ViewModels.User;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using ERP.Authorization.Rule;
using ERP.DomainEvents;
using ERP.DomainEvents.Users;
using ERP.Models;
using ERP.Models.DB.Identity;
using ERP.Services;
using ERP.Services.DB.Sms;
using ERP.Services.IntegralBalance;
using ERP.Services.Statistics;
using ERP.Services.Tools;
using ERP.ViewModels.Statistics;
using Logistics.SFCLogisticsReference;
using MediatR;
using Ryu.Extensions;
using static ERP.Models.Abstract.BaseModel;
using static ERP.Models.DB.Identity.User;
using BalanceBill = ERP.Models.IntegralBalance.BalanceBill;
using CompanyOemService = ERP.Services.DB.Users.CompanyOem;
using CounterCompanyBusinessService = ERP.Services.Statistics.CounterCompanyBusinessService;
using CounterGroupBusinessService = ERP.Services.Statistics.CounterGroupBusinessService;
using CounterRequestService = ERP.Services.Statistics.CounterRequestService;
using CounterUserBusinessService = ERP.Services.Statistics.CounterUserBusinessService;
using data = ERP.ViewModels.Logistics.ShipRatesEstimationTemplate.data;
using DBContext = ERP.Data.DBContext;
using GroupService = ERP.Services.DB.Users.GroupService;
using Menu = ERP.Models.DB.Setting.Menu;
using MenuCache = ERP.Services.Caches.Menu;
using OEMCache = ERP.Services.Caches.OEM;
using RequiredAttribute = System.ComponentModel.DataAnnotations.RequiredAttribute;
using Role = ERP.Enums.Identity.Role;
using RuleTemplateModel = ERP.Models.DB.Users.RuleTemplate;
using RuleTemplateService = ERP.Services.DB.Users.RuleTemplate;
using UnitCache = ERP.Services.Caches.Unit;
using UserGroupModel = ERP.Models.DB.Users.Group;
using UserValidityModel = ERP.Models.DB.Users.UserValidity;

namespace ERP.Controllers.User;

[Route("/api/[controller]/[action]")]
[ApiController]
public partial class UserController : BaseController
{
    private readonly DBContext _dbContext;
    private readonly ISessionProvider SessionProvider;
    private readonly UserManager _userManager;
    private readonly SignInManager _signInManager;
    private readonly OEMCache _oemCache;
    private readonly UnitCache _unitCache;
    private readonly Statistic _statisticsService;
    private readonly StatisticMoney _statisticMoney;
    private readonly CompanyOemService _companyOemService;
    private readonly CompanyService _companyService;
    private readonly RuleTemplateService _ruleTemplateService;
    private readonly UserService _userService;
    private readonly UserOperateLogService _userOperateLogServiceService;
    private readonly GroupService _groupService;
    private readonly CounterUserBusinessService _counterUserBusinessService;
    private readonly CounterGroupBusinessService _counterGroupBusinessService;
    private readonly CounterCompanyBusinessService _counterCompanyBusinessService;
    private readonly CounterRequestService _counterRequestService;
    private readonly IOemProvider _oemProvider;
    private readonly IMediator _mediator;
    private readonly ILogger<UserController> _logger;
    private readonly IHttpContextAccessor _HttpContextAccessor;
    private readonly SendService _sendService;
    private readonly IConfiguration _configuration;
    private readonly UserOnlineService _userOnlineService;
    private readonly LoginLogService _loginLogService;
    private readonly IntegralBalanceService _integralBalanceService;
    private ISession _session => _HttpContextAccessor.HttpContext!.Session;
    public UserController(
        UserManager userManager,
        SignInManager signInManager,
        DBContext dbContext,
        ISessionProvider _SessionProvider,
        OEMCache oemCache,
        CompanyOemService companyOemService,
        CompanyService companyService,
        RuleTemplateService ruleTemplateService,
        UserService userService,
        UserOperateLogService userOperateLogServiceService,
        GroupService groupService,
        CounterUserBusinessService counterUserBusinessService,
        CounterGroupBusinessService counterGroupBusinessService,
        CounterCompanyBusinessService counterCompanyBusinessService,
        CounterRequestService counterRequestService,
        UnitCache unitCache, Statistic statisticsService, IOemProvider oemProvider, IMediator mediator, ILogger<UserController> logger, IHttpContextAccessor httpContextAccessor, SendService sendService, IConfiguration configuration, UserOnlineService userOnlineService, LoginLogService loginLogService, IntegralBalanceService integralBalanceService, StatisticMoney statisticMoney)
    {
        _userManager = userManager;
        _signInManager = signInManager;
        _dbContext = dbContext;
        SessionProvider = _SessionProvider;
        _oemCache = oemCache;
        _companyOemService = companyOemService;
        _companyService = companyService;
        _ruleTemplateService = ruleTemplateService;
        _userService = userService;
        _userOperateLogServiceService = userOperateLogServiceService;
        _groupService = groupService;
        _counterUserBusinessService = counterUserBusinessService;
        _counterGroupBusinessService = counterGroupBusinessService;
        _counterCompanyBusinessService = counterCompanyBusinessService;
        _counterRequestService = counterRequestService;
        _unitCache = unitCache;
        _statisticsService = statisticsService;
        _oemProvider = oemProvider;
        _mediator = mediator;
        _logger = logger;
        _HttpContextAccessor = httpContextAccessor;
        _sendService = sendService;
        _configuration = configuration;
        _userOnlineService = userOnlineService;
        _loginLogService = loginLogService;
        _integralBalanceService = integralBalanceService;
        _statisticMoney = statisticMoney;
    }

    private ISession Session
    {
        get => HttpContext.Session;
    }

    private int UserID
    {
        get => Session.GetUserID();
    }

    private int GroupID
    {
        get => Session.GetGroupID();
    }

    private int CompanyID
    {
        get => Session.GetCompanyID();
    }

    private int OEMID
    {
        get => Session.GetOEMID();
    }

    public async Task<ResultStruct> Login(LoginViewModel model, [FromServices] UserService userService)
    {
        var user = await _userManager.FindByNameAsync(model.UserName);
        if (user == null)
        {
            return Error(message: "用户不存在,请检查用户名#1");
        }

        if (user.OEMID != _oemProvider.OEM.ID)
        {
            return Error(message: "用户不存在,请检查用户名#2");
        }

        var signInResult =
            await _signInManager.PasswordSignInAsync(user, model.PassWord, model.RememberMe,
                lockoutOnFailure: false);
        if (signInResult.Succeeded)
        {

            var now = DateTime.Now;
            if (user.State == StateEnum.Close)
            {
                return Error(message: "当前账号已被禁用，请咨询客服吧#3");
            }
            
            var company = _dbContext.Company.FirstOrDefault(a => a.ID == user.CompanyID);
            if (company is not null)
            {
                HttpContext.Session.SetCompany(company!);
            }else
            {
                return Error(message: "公司不存在，请咨询客服吧#4");
            }
            
            //超管、免费用户过滤账号过期检测
            if (user.ID != Enums.Identity.User.SU.GetNumber() || user.CompanyID != Enums.ID.Company.XiaoMi.GetNumber())
            {
                if (company.Validity <= now)
                {
                    return Error(message: "账号过期，请咨询客服#5");
                }
            }

            var loginTime = DateTime.Now;
            HttpContext.Session.SetUser(user);

            #region 登录初始化统计

            Statistic statisticService =
                new Statistic(_dbContext, new FinancialAffairsService(_dbContext, SessionProvider));
            //oem
            await statisticService.CheckOrAdd(_dbContext.OEM.FirstOrDefault(a => a.ID == user.OEMID) ?? new());
            //company
            await statisticService.CheckOrAdd(company!);
            //store
            var storeIds = _dbContext.UserStore.Where(m => m.CorrelationUserID == user.ID)
                .Select(m => m.CorrelationStoreID);
            await _dbContext.StoreRegion.Where(m => storeIds.Contains(m.ID))
                .ForEachAsync(m => statisticService.CheckOrAdd(m));
            //group
            await statisticService.CheckOrAdd(_dbContext.UserGroup.FirstOrDefault(a => a.ID == user.GroupID) ??
                                              new());
            //user
            await statisticService.CheckOrAdd(user);

            #endregion

            #region 设置首次登录WEB时间

            if (!user.FirstLoginWeb.HasValue)
            {
                await _userService.SetUserFirstLoginWeb(user.ID, loginTime);
            }
            #endregion

            #region 记录在线用户

            var transaction = await _dbContext.Database.BeginTransactionAsync();
            try
            {
                await _userOnlineService.HandleOnline(user,loginTime);
                await transaction.CommitAsync();
            }
            catch (Exception e)
            {
                await transaction.RollbackAsync();
                _logger.LogError(e,e.Message);
                return Error(message:e.Message);
                throw;
            }

            #endregion

            return Success(new { Token = "123465" });
        }

        return Error(message: "密码错误");
        /************************************************  
        ================/api/user/login
        // 获取用户和公司
        select * from `user` where `d_username` = ? limit *
        select * from `company` where `company`.`id` = ? limit *
        // 验证用户过期时间
        select * from `user_validity` where `user_validity`.`id` = ? and `user_validity`.`id` is not null limit *
        //
        select SUM(c_request) as total_request, SUM(c_cpu) as total_cpu, SUM(c_memory) as total_memory, SUM(c_network) as total_network, SUM(c_disk) as total_disk, SUM(c_pic_api) as total_pic_api, SUM(c_translate_api) as total_translate_api from `counter_request` where `i_company_id` = ? limit *
        select * from `company_reduce_request` where `i_company_id` = ? and `i_oem_id` = ? limit *
        // 获取首页展示公司信息
        select `c_user_num`, `c_product_num`, `c_order_num`, `c_purchase_num`, `c_waybill_num`, `c_store_num` from `counter_company_business` where `i_company_id` = ? and `i_oem_id` = ? limit *
        // 修改登录时间
        update `user` set `login_time` = ?, `user`.`updated_at` = ? where `id` = ?
        // 获取用户权限
        select `id`, `i_rule_id` from `rule_template` where `rule_template`.`id` = ? limit *
        select * from `rule` where `rule`.`id` = ? limit *
        // 获取库存日志
        select count(*) as aggregate from `stock_log` where `t_shelve` = ? and (`i_company_id` = ? or `i_execute_company_id` = ?) and `i_waybill_id` = ?
        // 获取用户在线信息，并修改
        select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
        update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
        // 
        select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
        update `store` set `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
        select `id`, `d_title`, `i_user_id`, `t_state` from `work_order` where `i_company_id` = ? and `t_state` in (?, ?) limit *
        select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
        update `store` set `d_last_auto_time` = ?, `store`.`updated_at` = ? where `id` = ?
        select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
        select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
        select * from `task` where (`i_oem_id` = ? and `i_user_id` = ? and `i_group_id` = ? and `d_hash_code` = ?) limit *
        select count(*) as aggregate from `product` where (`d_hash_code` = ? and `i_company_id` = ?) and !(b_type&*) AND !(b_type&*) AND !(b_type&*)
        insert into `product` (`i_oem_id`, `i_company_id`, `i_user_id`, `i_group_id`, `i_task_id`, `u_lp_n`, `u_lp_k`, `u_lp_s`, `u_lp_d`, `d_source`, `d_hash_code`, `d_languages`, `b_language`, `b_type`, `u_price`, `i_platform_id`, `u_platform`, `d_attribute`, `d_variants`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
        delete from `product` where (`i_user_id` = ? and `i_oem_id` = ?) and b_type & *
        select * from `user` where `id` = ? limit *
        select * from `theme` where `theme`.`id` in (?)
        select * from `user_online`
        select * from `counter_request` where `i_user_id` = ? limit *
        update `counter_request` set `c_request` = c_request+*, `c_cpu` = c_cpu+*, `c_memory` = c_memory+*, `c_network` = c_network+*, `c_disk` = c_disk+*, `counter_request`.`updated_at` = ? where `i_user_id` = ?
        select * from `login_log` where (`i_user_id` = ? and `t_type` = ?) order by `id` desc limit *
        update `login_log` set `t_type` = ?, `d_remark` = ?, `d_ext` = ?, `login_log`.`updated_at` = ? where `id` = ?
        select count(*) as aggregate from `user_online` where `i_user_id` = ?
        insert into `login_log` (`i_user_id`, `d_username`, `i_oem_id`, `t_type`, `created_at`, `updated_at`, `d_login_ip`, `d_ext`) values (?, ?, ?, ?, ?, ?, ?, ?)
        insert into `user_online` (`i_user_id`, `d_username`, `created_at`, `updated_at`, `d_session_id`, `d_login_ip`, `d_login_time`, `i_oem_id`) values (?, ?, ?, ?, ?, ?, ?, ?)
        insert into `temp_images` (`d_oss_path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `i_product_id`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?)
        select `id`, `d_button_name` from `waybill` where ((`i_company_id` = ? and `i_operate_company_id` = ?) or `i_waybill_company_id` = ?) and `d_button_name` in (?, ?) order by `updated_at` asc limit *
        select * from `store` where `i_company_id` = ? and `t_flag` = ? and `t_is_del` = ? and `t_is_verify` = ?
        select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
        select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
        select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?)
        select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
        select `d_order_no` from `amazon_order` where (`i_store_id` = ? and `t_order_state` = ? and `i_company_id` = ? and `t_entry_mode` = ?) limit *
        update `store` set `d_last_auto_update_time` = ?, `store`.`updated_at` = ? where `id` = ?
        select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
        select * from `product` where `id` = ? limit *
        select count(*) as aggregate from `product` where (`d_hash_code` = ? and `i_company_id` = ? and `id` != ?) and !(b_type&*) AND !(b_type&*) AND !(b_type&*)
        update `product` set `b_type` = ?, `d_variants` = ?, `product`.`updated_at` = ? where `id` = ?
        select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
        select count(*) as aggregate from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_oem_id` = ?) and `i_task_id` = ? and `updated_at` between ? and ?
        select `id`, `i_oem_id`, `i_user_id`, `i_company_id`, `i_oem_id`, `i_category_id`, `i_platform_id`, `d_reason`, `u_quantity`, `u_lp_n`, `u_image`, `u_price`, `d_source`, `b_type`, `b_language`, `d_languages`, `updated_at`, `created_at`, `d_coin`, `d_rate` from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_oem_id` = ?) and `i_task_id` = ? and `updated_at` between ? and ? order by `id` desc limit * offset *
        select * from `category` where `category`.`id` in (?)
        select * from `task` where `task`.`id` in (?)
        select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?)
        select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?, ?, ?, ?, ?)
        insert into `resource` (`created_at`, `d_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
        delete from `temp_images` where `id` in (?, ?, ?, ?, ?, ?)
        update `product` set `u_lp_k` = ?, `u_lp_s` = ?, `u_lp_d` = ?, `b_type` = ?, `u_price` = ?, `u_quantity` = ?, `d_source` = ?, `d_ext` = ?, `d_attribute` = ?, `d_variants` = ?, `d_images` = ?, `u_image` = ?, `d_pid` = ?, `d_pid_hash_code` = ?, `c_size` = ?, `d_coin` = ?, `d_rate` = ?, `product`.`updated_at` = ? where `id` = ?
        update `counter_company_business` set `c_product_num` = `c_product_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` = ?
        select `id`, `d_path` as `oss_path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `resource` where `id` in (?, ?, ?, ?, ?, ?)
        update `counter_user_business` set `c_product_num` = `c_product_num` + *, `counter_user_business`.`updated_at` = ? where `i_user_id` = ?
        insert into `temp_images` (`created_at`, `d_oss_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
        update `product` set `u_lp_n` = ?, `u_lp_d` = ?, `d_hash_code` = ?, `b_type` = ?, `u_price` = ?, `d_variants` = ?, `d_images` = ?, `u_image` = ?, `c_size` = ?, `d_rate` = ?, `product`.`updated_at` = ? where `id` = ?
        select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?)
        select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
        insert into `reporter_request` (`created_at`, `d_action`, `d_cpu_time`, `d_method`, `d_module`, `d_name`, `d_namespace`, `d_net_time`, `d_request_id`, `d_request_time`, `d_uri`, `i_company_id`, `i_group_id`, `i_oem_id`, `i_user_id`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `waybill`.`updated_at` = ? where `id` = ?
        select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?)
        select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?) order by `d_last_auto_update_time` asc limit *
        select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?)
        select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
        ************************************************/
    }


    public async Task<ResultStruct> Logout()
    {
        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            //在线数据
            var online = await _userOnlineService.GetInfoByUserId(UserID);
            if (online != null)
            {
                //登出日志
                await _loginLogService.Loginout(online!);
                //删除在线数据
                await _userOnlineService.DeleteOnline(online!);
            }
            await _signInManager.SignOutAsync();
            await transaction.CommitAsync();
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            Console.WriteLine(e);
            return Error(e.Message);
        }
        return Success();
    }

    public async Task<ResultStruct> Info([FromServices] MenuCache menuCache,
        [FromServices] Interface.Rule.IManager manager, [FromServices] UnitCache unitCache,
        [FromServices] IHostEnvironment environment, [FromServices] IOemProvider oemProvider)
    {
        var allRoutes = menuCache.ListClone();
        if (allRoutes is null)
        {
            return Error("Menu缓存为空");
        }

        var routers = allRoutes.Where(m =>
        {
            bool Check(Menu o) => o.Module.IsNull() || User.Has(o.Module, o.PermissionFlag);
            m.Children = m.Children.Where(Check).ToList();
            return Check(m);
        });
        var user = await _userManager.GetUserAsync(User);
        await _dbContext.Entry(user).Reference(u => u.Company).LoadAsync();
        var oem = oemProvider.OEM;
        var hasB2CUserIds = new List<int>() { 444447 };
        var hasB2CAuthUserIds = new List<int>() { 444447 };
        var hasPackagesIds = new List<int>() { 444446,444447 };
        // var hasB2CUserIds = new List<int>() {  };
        return Success(new
        {
            userId = user.Id,
            Name = user.UserName,
            user.Avatar,
            routers,
            user.OEM.Theme,
            ossDomain = oem?.Oss?.BucketDomain,
            CompanyId = user.CompanyID,
            user.UnitConfig,
            user.OEM.Title,
            titleStyle = user.OEM.Style,
            user.OrderUnitConfig,
            OemId = user.OEMID,
            user.Company.ShowOemId,
            user.Company.Source,
            user.Company.FirstRecharge,
            product_show_unit = user.ProductConfig.ShowUnit,
            user.PhoneNumberConfirmed,
            Concierge = user.Role == Role.Employee ? Enums.View.Concierges.Child : Enums.View.Concierges.Own,
            user.Company.SecondPackageOem,
            downloadUrl = "",
            helpManualUrl = "",
            storeBindUrl = "",
            helpManualWebSiteUrl = "",
            resourcesChangeDate = "",
            ossType = oem?.Oss?.Type,
            env = environment.EnvironmentName,
            rules = User.GetRules(manager),
            UserState = user.State,
            Company = new
            {
                Id = user.Company.ID,
                user.Company.Name,
                user.Company.IsDistribution,
                user.Company.ProductTranslation,
                user.Company.ProductEditor,
                CompanyState = user.Company.State
            },
            Units = unitCache.List()?.Select(u => new { u.Name, u.Sign, u.Rate }),
            isShowB2C = hasB2CUserIds.Contains(user.Id),
            isShowPackage = hasPackagesIds.Contains(user.Id),
            isShowB2CAuth = hasB2CAuthUserIds.Contains(user.Id),
            user.Role,
        });
    }

    [HttpGet]
    public ResultStruct Rules([FromServices] Interface.Rule.IManager manager)
    {
        return Success(new { rules = User.GetRules(manager) });
    }

    [HttpPost]
    public async Task<ResultStruct> GetOems()
    {
        var oemId = HttpContext.Session.GetShowOemId();
        var oems = await GetAllOem(HttpContext.Session.GetCompanyID(), oemId);
        return Success(new { oemId, oems });
    }

    private async Task<object?> GetAllOem(int companyId, int oemId)
    {
        var oems = _oemCache.List()!.Select(a => new { a.ID, a.Name });
        if (companyId == 1 /*$this->super_id*/)
        {
            return oems;
        }
        else
        {
            var oemIds = await _companyOemService.GetOemIds(companyId);
            oems = oems.Where(a => oemIds.Contains(a.ID))
                .Select(a => new { a.ID, a.Name });
        }

        return oems;
    }

    /// <summary>
    /// 获取用户列表筛选条件信息
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetSearchList()
    {
        var session = HttpContext.Session;
        var companyId = session.GetCompanyID();
        var oemId = session.GetOEMID();
        var list = await _ruleTemplateService.GetRuleList(companyId, oemId);
        //$valid = arrayCombine($list, 'state');
        var rule = list.ToDictionary(x => x.ID, x => x);
        //独立账户权限
        var data = new
        {
            companyId,
            num = list.Count(a => a.State == StateEnum.Open),
            //获取用户所拥有的oems
            oems = await GetAllOem(companyId, session.GetOEMID()),
            rule,
            rules = session.GetRole() != Role.Employee
                ? new
                {
                    OwnUserAdd = true,
                    OwnUserEdit = true,
                    OwnUserConfig = true,
                    OwnUserRenewal = true,
                    OwnUserRecharge = true,
                    OwnUserLog = true,
                    UserResetPassword = true,
                    Balance = true,
                    ExportBill = HttpContext.Request.Host.ToString().StartsWith("cft"),
                }
                : new
                {
                    //'OwnUserAdd'        => $this->ruleInfo()->hasOwnUserAdd(),
                    //'OwnUserEdit'       => $this->ruleInfo()->hasOwnUserEdit(),
                    //'OwnUserConfig'     => $this->ruleInfo()->hasOwnUserConfig(),
                    //'OwnUserRenewal'    => $this->ruleInfo()->hasOwnUserRenewal(),
                    //'OwnUserRecharge'   => $this->ruleInfo()->hasOwnUserRecharge(),
                    //'OwnUserLog'        => $this->ruleInfo()->hasOwnUserLog(),
                    //'UserResetPassword' => $this->ruleInfo()->hasOwnUserResetPassword(),
                    //'Balance' => $this->ruleInfo()->hasOwn(),
                    //???
                    OwnUserAdd = true,
                    OwnUserEdit = true,
                    OwnUserConfig = true,
                    OwnUserRenewal = true,
                    OwnUserRecharge = true,
                    OwnUserLog = true,
                    UserResetPassword = true,
                    Balance = true,
                    ExportBill = HttpContext.Request.Host.ToString().StartsWith("cft"),
                },
            type = Models.DB.Users.Company.GetAllTypes(),
        };
        return Success(data);
    }

    /// <summary>
    /// 获取独立账号数据
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    public async Task<ResultStruct> GetOwnList(GetOwnListDTO dto)
    {
        List<int> companyIds = new();
        List<Models.DB.Users.Company> res = new();
        if (dto.Validity != null || dto.IsAgency.HasValue)
        {
            ValidityCompany validity = new()
            {
                validity = dto.Validity,
                IsAgency = dto.IsAgency,
                Types = dto.Type
            };
            res = await _companyService.GetCompanyValidity(validity);
            if (res.Count == 0)
                return Success(new { list = new { }, company = new { } });
            companyIds = res.Select(a => a.ID).ToList();
        }

        var list = await _userService.GetUserListPage(dto.Page, dto.Limit, dto.Name, dto.RuleId, dto.State,
            dto.IsDistribution,
            dto.OemId, companyIds, dto.Mobile);
        if (list.Items.Count > 0 && companyIds.Count == 0)
        {
            companyIds = list.Items.Select(a => a.CompanyId).ToList();
            if (companyIds.Count > 0)
            {
                ValidityCompany validity = new()
                {
                    CompanyIds = companyIds,
                    validity = dto.Validity,
                    IsAgency = dto.IsAgency,
                };
                res = await _companyService.GetCompanyValidity(validity);
            }
        }

        if (list.Items.Count > 0)
        {
            if (HttpContext.Session.GetCompanyID() == 2)
            {
                var data = (from user in list.Items
                    join company in res on user.CompanyId equals company.ID
                    select new SpecialOwnListVm
                    {
                        Validity = company != null ? company.ShowValidityInfo : "暂无数据",
                        Type = company != null ? company.Type : Models.DB.Users.Company.Types.ONE,
                        Source = company != null ? company.Source : Models.DB.Users.Company.Sources.ONE,
                        Agency = company != null ? company.IsAgency : false,
                        Balance = company != null ? company.BalanceMoney.To("CNY") : 0,
                        Balance2 = company.BalanceMoney.To("CNY"),
                        Integral = company != null ? company.Integral : 0,
                        TypeName = company != null ? Extensions.EnumExtension.GetDescription(company.Type) : "暂无数据",
                        Discount = company != null ? $"{company.Discount}%" : "暂无数据",
                        Mobile = user.Mobile.IsNullOrWhiteSpace() ? "暂未绑定" : user.Mobile,
                        FirstLoginWeb = user.FirstLoginWeb != null ? user.FirstLoginWeb : "暂未登录",
                        FirstLoginExe = user.FirstLoginExe != null ? user.FirstLoginExe: "暂未登录",
                        FirstRechargeDate = company != null
                            ? (company.FirstRechargeDate != null ? company.FirstRechargeDate.ToString() : "暂未首冲")
                            : "暂无数据",
                        FeferrerKwSign = company != null ? company.ReferrerKwSign : "",
                        PhoneNumberConfirmed = user.PhoneNumberConfirmed,
                        CompanyId = user.CompanyId,
                        CreatedAt = user.CreatedAt,
                        Id = user.ID,
                        IsDistribution = user.IsDistribution,
                        OemId = user.OemId,
                        RuleId = user.RuleId,
                        State = user.State,
                        Truename = user.Truename,
                        Username = user.Username,
                        OemName = user.OemName,
                    }).ToList();

                return Success(new { list.Total, list.CurrentPage, list.PageSize, items = data });
            }
            else
            {
                var data = (from user in list.Items
                    join company in res on user.CompanyId equals company.ID
                    select new OwnListVM
                    {
                        Validity = company != null ? company.ShowValidityInfo : "暂无数据",
                        Type = company != null ? company.Type : Models.DB.Users.Company.Types.ONE,
                        Source = company != null ? company.Source : Models.DB.Users.Company.Sources.ONE,
                        CompanyId = user.CompanyId,
                        CreatedAt = user.CreatedAt,
                        Id = user.ID,
                        IsDistribution = user.IsDistribution,
                        OemId = user.OemId,
                        RuleId = user.RuleId,
                        State = user.State,
                        Truename = user.Truename,
                        Username = user.Username,
                        OemName = user.OemName,
                    }).ToList();

                return Success(new { list.Total, list.CurrentPage, list.PageSize, items = data });
            }
        }

        return Success(list);
    }

    /// <summary>
    /// 获取用户信息
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Route("{id}")]
    public async Task<ResultStruct> GetInfo(int id)
    {
        // if (Session.GetRole() == Role.Employee)
        // {
        //     return Error("暂无权限");
        // }

        var user = await _userManager.FindByIdAsync(id.ToString());
        var data = new
        {
            user.ID,
            groupId = user.GroupID,
            user.Mobile,
            ruleId = user.FormRuleTemplateID,
            user.State,
            user.TrueName,
            user.UserName,
            user.Email
        };
        return Success(data);
    }


    /// <summary>
    /// 手机认证更改为已认证
    /// </summary>
    /// <returns></returns>
    [Route("{id}")]
    public async Task<ResultStruct> PhoneNumberConfirm(int id)
    {
        if (HttpContext.Session.GetCompanyID() != 2) return Error("暂无权限");
        // var res = await _userService.updateDataById(id);
         var user = await _userManager.FindByIdAsync(id.ToString());
         user.PhoneNumberConfirmed = true;
         await _userManager.UpdateAsync(user);
        // var rest = new Rest(new HttpClient());
        // rest.SetAccount("8aaf07087ca221d8017cc0c5b72405e3","efc17cf303934f5ca8ff5ea0b8348aab");
        // rest.SetAppId("8aaf07087ca221d8017cc0c5b7f105e9");
        // var data = new Dictionary<string, string>()
        // {
        //     { "Code", "1234" },
        //     { "time", "5分钟" },
        // };
        // await rest.SendTemplateSMS("18551229870", data, "942583");
        return Success();
    }
    [HttpPost]
    public async Task<ResultStruct> GetBindMobileCode(BindMobileCodeVM req,[FromServices] Captcha captcha)
    {
        //验证验证码正确性
        if (!captcha.Compare(_session.GetString("Captcha"), req.Captcha))
        {
            return Error(message: "验证码不正确");
        }
        
        //查询当前号码是否已存在
        var user = await _userService.CheckUserMobile(req.Mobile);

        if (user is not null && user.Id != SessionProvider.Session.GetUserID())
        {
            return Error("当前手机号码已存在，请重新输入");
        }

        if (user is not null && user.Id != SessionProvider.Session.GetUserID() && user.PhoneNumberConfirmed)
        {
            return Error("请输入新手机号码");
        }

        var smsCode = await _sendService.GetEndCode(req.Mobile, SmsCodeTypeEnum.BindOrChange);

        if (smsCode is not null &&  (DateTime.Now - smsCode.SendTime).TotalSeconds < 60)
        {
            return Error("发送过于频繁");
        }
        
        //判断ip出现次数
        var ip = _HttpContextAccessor.HttpContext!.GetIP();
        var isExceed = await _sendService.CheckIpRequest(ip);
        if (isExceed)
        {
            return Error($"IP:{ip}, 今日请求已经超出限定次数，请明天再试"); 
        }
        
        //6位随机数
        var code = (new Random()).Next(100000, 999999);
        
        var sendData = new Dictionary<string, object>()
        {
            { "code", code },
            { "time", "5分钟" },
        };

       var result =  await _sendService.SendBindCode(req.Mobile, sendData);
       if (result.state)
       {
           await _sendService.AddSms(req.Mobile, code, DateTime.Now, SmsCodeTypeEnum.BindOrChange,_session.GetOEMID(),ip);
           return Success("短信发送成功");
       }
        return Error(result.msg);
    }

    public async Task<ResultStruct> bindMobileCode(BindMobileCodeVM req, [FromServices] Captcha captcha)
    {
        //验证验证码正确性
        // if (!captcha.Compare(_session.GetString("Captcha"), req.Captcha))
        // {
        //     return Error(message: "验证码不正确");
        // }

        if (req.Yzm is null)
        {
            return Error("请输入验证码");
        }
        
        var smsCode = await _sendService.GetEndCode(req.Mobile, SmsCodeTypeEnum.BindOrChange);
        
        //验证码有效期 5分钟
        /*if (smsCode is null || smsCode.Code != req.Yzm || (DateTime.Now - smsCode.SendTime).TotalSeconds > 300 ||
            smsCode.Used)
        {
            return Error("短信验证码错误！");
        }*/
//查询当前号码是否已存在
        var user = await _userService.CheckUserMobile(req.Mobile);
        if (user is not null && user.Id != SessionProvider.Session.GetUserID())
        {
            return Error("当前手机号码已存在，请重新输入");
        }

        if (user is not null && user.Id != SessionProvider.Session.GetUserID() && user.PhoneNumberConfirmed)
        {
            return Error("请输入新手机号码");
        }

        try
        {
            await _userService.BindMobile(await _userManager.GetUserAsync(User), smsCode, req.Mobile);
            Session.SetMobile(req.Mobile);
            return Success("操作成功");
        }
        catch (Exception e)
        {
            Console.WriteLine();
            return Error("操作失败"+e.Message);
        }
    }

    /// <summary>
    /// 套餐用户升级业务用户 不清楚数据是否会出现问题
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<ResultStruct> PackageToBusiness(PackageToBusinessVM vm)
    {
        if (HttpContext.Session.GetCompanyID() != 2) return Error("暂无权限");
        //var res = await _userService.updateDataById(id);

        // 启动事务
        using var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            var user = await _userManager.FindByIdAsync(vm.Id.ToString());
            user.FormRuleTemplateID = vm.RuleId;
            await _userManager.UpdateAsync(user);

            var company = await _companyService.GetInfoById(vm.CompanyId);
            if (company is null)
            {
                transaction.Rollback();
                return Error("无法找到用户信息");
            }

            //company.RuleTemplateId = vm.RuleId;
            company.Type = Models.DB.Users.Company.Types.ONE;
            company.Source = Models.DB.Users.Company.Sources.ONE;
            await _companyService.UpdateCompanyById(company);
            //用户日志
            await _userOperateLogServiceService.AddUserLog(user.Id, "更改用户为业务用户");

            //删除公司redis
            //$this->delCompanyRedis($company_id);
            // 提交事务
            transaction.Commit();
        }
        catch (Exception e)
        {
            // 回滚事务
            transaction.Rollback();
            return Error(e);
        }

        return Success();
    }

    // public async Task<ResultStruct> UpdatePackage(UpdatePackageVM vm)
    // {
    //     if (HttpContext.Session.GetCompanyID() != 2) return Error("暂无权限");
    //     var company = await _companyService.GetCurrentConfig(vm.CompanyId);
    //     if (company is null)
    //     {
    //         return Error("");
    //     }
    //
    //     int ruleId = 0;
    //     switch (vm.PackageType)
    //     {
    //         case Models.DB.Users.Company.Types.ONE:
    //             break;
    //         case Models.DB.Users.Company.Types.TWO:
    //             //$rule_id = $company['second_package_oem'] == companyModel::SECOND_PACKAGE_OEM_TRUE ? UserModel::OEM_RULE_TEMPLATE_ID : UserModel::RULE_TEMPLATE_ID;
    //             ruleId = company.SecondPackageOem == true ? 3881 : 1168;
    //             break;
    //         // case Models.DB.Users.Company.Types.THREE:
    //         //     break;
    //         // case Models.DB.Users.Company.Types.FOUR:
    //         //     //UserModel::RULE_ID_PERSON;
    //         //     ruleId = 1182;
    //         //     break;
    //         // case Models.DB.Users.Company.Types.FIVE:
    //         //     //UserModel::RULE_ID_TEAM;
    //         //     ruleId = 1183;
    //         //     break;
    //         // case Models.DB.Users.Company.Types.SIX:
    //         //     //RULE_ID_BUSINESS
    //         //     ruleId = 1184;
    //         //     break;
    //     }
    //
    //     // 启动事务
    //     using var transaction = await _dbContext.Database.BeginTransactionAsync();
    //     try
    //     {
    //         var user = await _userManager.FindByIdAsync(vm.Id.ToString());
    //         if (ruleId > 0)
    //             user.FormRuleTemplateID = ruleId;
    //         await _userManager.UpdateAsync(user);
    //
    //         //var company = await _companyService.GetInfoById(vm.CompanyId);
    //         //company.RuleTemplateId = ruleId;
    //         company.Type = vm.PackageType;
    //         company.Source = Models.DB.Users.Company.Sources.TWO;
    //         await _companyService.UpdateCompanyById(company);
    //         //用户日志
    //         await _userOperateLogServiceService.AddUserLog(user.Id,
    //             $"更改用户套餐为{Extensions.EnumExtension.GetDescription(vm.PackageType)}");
    //
    //         //更换免费版、个人版 禁用员工
    //         if (company.Type > Models.DB.Users.Company.Types.FOUR)
    //         {
    //             await _userService.UpdateUserData(vm.CompanyId);
    //         }
    //
    //
    //         //    //删除公司redis
    //         //$this->delCompanyRedis($company_id);
    //         // 提交事务
    //         transaction.Commit();
    //     }
    //     catch (Exception e)
    //     {
    //         // 回滚事务
    //         transaction.Rollback();
    //         return Error(e);
    //     }
    //
    //     return Success();
    // }

    [Route("{id}")]
    public async Task<ResultStruct> GetConfigById(int id)
    {
        //if (!$this->ruleInfo()->hasOwnUserConfig() && $this->sessionData->getConcierge() == UserModel::TYPE_CHILD) {
        //    return error('暂无权限修改公司配置信息');
        //}

        //获取公司配置信息
        var company = await _companyService.GetInfoById(id);
        //if (company != null)
        //{
        //    if (company.ShowOemId != 1)
        //    {
        //        var priceCf = company.PriceConfig;
        //        if (priceCf.IsNotWhiteSpace())
        //        {
        //            priceCf = 
        //        }
        //    }
        //}
        //if ($company){
        //$company = $company->toArray();
        //    if ($company['show_oem_id'] != 1){
        //    $priceCf = $company['price_config'];
        //        if (!empty($priceCf))
        //        {
        //        $priceCf = json_decode($priceCf, true);
        //            //新增翻译次数
        //            if (!isset($priceCf['translate']))
        //            {
        //            $priceCf['translate'] = 0;
        //            }
        //            foreach ($priceCf as $k => $v){
        //            $priceCf[$k] = $v <= 0 ? 0 : bcmul($v, 1000, 2);
        //            }
        //        $company['price_config'] = $priceCf;
        //        }
        //    }else
        //    {
        //    $company['price_config'] = null;
        //    }
        //}

        //公司拥有OEM数
        var companyId = HttpContext.Session.GetCompanyID();
        var oems = await GetAllOem(companyId, HttpContext.Session.GetShowOemId());

        //获取当前公司限制项配置信息
        var config = await _companyService.GetLimitInfoById(companyId);

        //修改公司积分账户性质
        //$IntegralBill = $this->sessionData->getRule()->getSetting()->getBiter()->hasIntegralBalance();
        //$integral = ($IntegralBill && $this->sessionData->getSource() == companyModel::SOURCE_ONE) ? true : false;

        var data = new
        {
            companyId,
            company,
            config,
            oems,
            unit = _unitCache.List(),
            integral = true,
        };
        return Success(data);
    }

    /// <summary>
    /// 提交公司配置信息
    /// </summary>
    /// <param name="vm"></param>
    /// <returns></returns>
    [Permission(Enums.Rule.Config.User.OwnCompanyConfig)]
    public async Task<ResultStruct> SubConfig(SubConfigVM vm)
    {
        //if (!$this->ruleInfo()->hasOwnUserConfig() && $this->sessionData->getConcierge() == UserModel::TYPE_CHILD) {
        //    return error('暂无权限修改公司配置信息');
        //}

        var res = await _companyService.UpdateCompanyConfig(vm);
        //$this->delCompanyRedis($id);
        return Success();
    }

    private async Task<int> GetHasMonth(int companyId)
    {
        int hasMonth = 0;
        if (HttpContext.Session.GetUserID() == superId)
        {
            //超管直接可续期4年
            hasMonth = 48;
        }
        else
        {
            var company = await _companyService.GetInfoById(companyId);
            //获取上级过期时间
            var superior = await _companyService.GetInfoById(company.ParentCompanyId);

            if (company.Validity > superior.Validity)
            {
                hasMonth = 0;
            }
            else
            {
                hasMonth = (int)Math.Floor(
                    (superior.Validity.AddYears(1).BeginYearTick() - company.Validity).TotalSeconds / 2592000);
            }
        }

        return hasMonth;
    }

    /// <summary>
    /// 获取可续期月
    /// </summary>
    /// <param name="vm"></param>
    /// <returns></returns>
    [Permission(Enums.Rule.Config.User.OwnUserRenewal)]
    public async Task<ResultStruct> GetValidity(GetValidityVM vm)
    {
        //if (!$this->ruleInfo()->hasOwnUserRenewal() && $this->sessionData->getConcierge() == UserModel::TYPE_CHILD) {
        //    return error('暂无权限续期');
        //}
        var company = await _companyService.GetInfoById(vm.CompanyId);
        var showValidity = company.ShowValidityInfo;
        var hasMonth = await GetHasMonth(vm.CompanyId);
        
        //当前公司

        var currentCompany = await _companyService.GetInfoById(CompanyID);

        if (currentCompany is null)
        {
            return Error("找不到公司相关信息");
        }
        
        var duration = currentCompany.Validity.AddYears(1);

        if (HttpContext.Session.GetCompanyID() == Enums.ID.Company.XiaoMi.GetNumber())
        {
            duration = DateTime.MaxValue;
        }

        return Success(new { showValidity, hasMonth,duration });
    }

    /// <summary>
    /// 账户续期
    /// </summary>
    /// <param name="vm"></param>
    /// <returns></returns>
    [Permission(Enums.Rule.Config.User.OwnUserRenewal)]
    public async Task<ResultStruct> SubRenewal(SubRenewalVM vm)
    {
        //if (!$this->ruleInfo()->hasOwnUserRenewal() && $this->sessionData->getConcierge() == UserModel::TYPE_CHILD) {
        //    return error('暂无权限续期');
        //}
        var company = await _companyService.GetInfoById(vm.CompanyId);
        var showValidity = company.Validity;
        // int hasMonth = await GetHasMonth(vm.CompanyId);
        //
        // if (hasMonth < vm.Month)
        // {
        //     return Error("新增时长非法！");
        // }

        // var v = showValidity.AddMonths(vm.Month);
        
        var v = vm.Duration;
            
        //获取开户公司可选的最大有效期
        if (CompanyID != Enums.ID.Company.XiaoMi.GetNumber())
        {
            var companyInfo = await _companyService.GetInfoById(CompanyID);
            if (companyInfo is null)
            {
                return Error("找不到相关公司信息");
            }
            var optionalValidityMax = companyInfo.Validity.AddYears(1);
            if (v > optionalValidityMax)
            {
                return Error("非法的有效期");
            }
        }
        
        using var transcation = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            await _companyService.UpdateValidity(v, company.ID);
            //增加更新用户信息日志
            // await _userOperateLogServiceService.AddUserLog(vm.Id, $"账户续期{vm.Month}月");
            await _userOperateLogServiceService.AddUserLog(vm.Id, $"账户有效期改为{vm.Duration.ToString("yyyy-MM-dd")}");

            transcation.Commit();
        }
        catch (Exception ex)
        {
            transcation.Rollback();
            return Error(ex.Message);
        }

        return Success();
    }

    public record GetRechargeVM(int CompanyID, int Id);

    /// <summary>
    /// 获取可充值余额
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    [Permission(Enums.Rule.Config.User.OwnUserRecharge)]
    public async Task<ResultStruct> GetRecharge(GetRechargeVM vm)
    {
        //当前用户余额
        var company = await _companyService.GetInfoById(vm.CompanyID);
        if (company is null)
        {
            return Error($"错误数据#1");
        }

        var hasMoney = Helpers.ToConversion(company.Balance, "CNY");
        string hasShow;
        var userId = HttpContext.Session.GetUserID();
        if (userId == superId || HttpContext.Session.GetCompanyID() == 2)
        {
            //超管无线充值
            hasShow = "无限";
        }
        else
        {
            if (company.Balance < 0)
            {
                hasShow = "0";
            }
            else
            {
                var company2 = await _companyService.GetInfoById(HttpContext.Session.GetCompanyID());
                if (company2 is null)
                {
                    return Error($"错误数据#2");
                }

                hasShow = Helpers.ToConversion(company2.Balance, "CNY").ToString();
            }
        }

        return Success(new { hasMoney, hasShow, userId });
    }

    [Permission(Enums.Rule.Config.User.OwnUserRecharge)]
    public async Task<ResultStruct> SubRecharge(SubRechargeVM vm)
    {
        var userId = HttpContext.Session.GetUserID();
        //基准金额
        var money = MoneyFactory.Instance.Create(vm.Recharge, "CNY");
        var user = await _userService.GetInfoById(vm.Id);
        if (userId == superId || HttpContext.Session.GetCompanyID() == 2)
        {
            var company = await _companyService.GetInfoById(vm.CompanyId);
            using var transaction = await _dbContext.Database.BeginTransactionAsync();
            try
            {
                await _companyService.ChargeAdd(vm.CompanyId, money);

                await  _integralBalanceService.AddBalanceBill(money, _session.GetCompanyID(), _session.GetOEMID(), user, 0,
                    BalanceBill.Types.EXPENSE, "用户充值余额",vm.Remark);
              
                await _integralBalanceService.AddBalanceBill(money, vm.CompanyId, vm.OemId, user, company.Balance,
                    BalanceBill.Types.RECHARGE, "上级账号充值",vm.Remark);
                

                //增加更新用户信息日志
                await _userOperateLogServiceService.AddUserLog(vm.Id, $"账户充值{vm.Recharge}CNY");

                //增加余额扣款账单
                //$IntegralBalanceService = new IntegralBalanceService();
                //$IntegralBalanceService->addBalanceBill($money, 0, BalanceBillModel::TYPE_RECHARGE, '上级账号充值', '', $company_id, $oem_id, $id, $user['username'], $user['truename'], $company_id);
               

                //$IntegralBalanceService->addBalanceBill($money, 0, BalanceBillModel::TYPE_EXPENSE, '用户充值余额', $remark, $this->sessionData->getCompanyId(), $this->sessionData->getOemId(), $id, $user['username'], $user['truename'], $company_id);
              
                await transaction.CommitAsync();
            }
            catch (Exception ex)
            {
                await transaction.RollbackAsync();
                return Error(ex.Message);
            }
        }
        else
        {
            //检测充值金额合法性
            //当前用户余额
            var company = await _companyService.GetInfoById(HttpContext.Session.GetCompanyID());
            if (company is null) return Error("公司信息错误");

            if (company.Balance < money)
            {
                return Error("充值金额非法！");
            }

            using var transaction = await _dbContext.Database.BeginTransactionAsync();
            try
            {
                await _companyService.ChargeAdd(vm.CompanyId, money);

                await _integralBalanceService.AddBalanceBill(money, vm.CompanyId, vm.OemId, user, 0,
                    BalanceBill.Types.RECHARGE, "上级账号充值",vm.Remark);
                await _integralBalanceService.AddBalanceBill(money, _session.GetReportId(), _session.GetOEMID(), user, 0,
                    BalanceBill.Types.EXPENSE, "用户充值余额",vm.Remark);
                
                //充值扣款
                await _companyService.Charge(HttpContext.Session.GetCompanyID(), money);
                //增加余额扣款账单  ????
                //$IntegralBalanceService = new IntegralBalanceService();
                //充值账户余额
                //$company = $this->companyService->getInfoById($company_id);
                //$IntegralBalanceService->addBalanceBill($money, $company->getRawOriginal('balance'), BalanceBillModel::TYPE_RECHARGE, '上级账号充值', '', $company_id, $oem_id, $id, $user['username'], $user['truename'], $company_id);
                
               

                //增加更新用户信息日志
                await _userOperateLogServiceService.AddUserLog(vm.Id, $"账户充值{vm.Recharge}CNY");
                //$IntegralBalanceService->addBalanceBill($money, $company->getRawOriginal('balance'), BalanceBillModel::TYPE_EXPENSE, '用户充值余额', $remark, $this->sessionData->getCompanyId(), $this->sessionData->getOemId(), $id, $user['username'], $user['truename'], $company_id);
                

                await transaction.CommitAsync();
            }
            catch (Exception ex)
            {
                await transaction.RollbackAsync();
                return Error(ex.Message);
            }
        }

        return Success();
    }

    /// <summary>
    /// 获取当前公司限制信息
    /// </summary>
    /// <returns></returns>
    ///
    [Permission(Enums.Rule.Config.User.OwnCompanyConfig)]
    public async Task<ResultStruct> GetOwnConfig()
    {
        //获取当前公司限制项配置信息
        var company = await _companyService.GetLimitInfoById(HttpContext.Session.GetCompanyID());
        if (company is null)
        {
            return Error("无法获取到该公司");
        }

        bool distribution = false;
        bool integral = false;
        int hasMonth = 0;
        var duration = company.Validity.AddYears(1);
        //company.Integral = false;
        if (HttpContext.Session.GetUserID() == 1 /*$this->super_id*/)
        {
            //超管直接可续期4年
            hasMonth = 48;
            duration = DateTime.MaxValue;

        }
        else if (HttpContext.Session.GetCompanyID() == Enums.ID.Company.XiaoMi.GetNumber())
        {
            //米境通公司人员无限制
            duration = DateTime.MaxValue;
        }
        else
        {
            hasMonth = (int)Math.Floor((company.Validity.AddYears(1).BeginYearTick() - DateTime.Now).TotalSeconds /
                                       2592000);

            //判断当前用户是否拥有开通分销权限
            if (HttpContext.Session.GetRole() == Role.ADMIN)
            {
                //$is_distribution = $this->sessionData->getRule()->getDistribution()->getValue();
                distribution = User.Has(Enums.Rule.Menu.Distribution.Module);
            }
            else
            {
                //授权员工开通账号
                //var ruleGroup = await _ruleTemplateService.GetInfoById(company.RuleId);
                //var rule = await _ruleService.GetInfoById(ruleGroup.RuleId);

                //$companyRule = new Rule($rule);
                //$is_distribution = $companyRule->getDistribution()->getValue();

                var companyUser = await _dbContext.User
                    .Where(m => m.CompanyID == company.ID || m.Role != Role.ADMIN)
                    .FirstOrDefaultAsync();
                if (companyUser == null)
                {
                    return Error("无法获取到该公司管理员");
                }

                var companyUserClaimas = await _dbContext.UserClaims
                    .Where(m => m.UserId == companyUser.ID)
                    .ToListAsync();

                var companyUserClaimsPrincipal = new ClaimsPrincipal(new List<ClaimsIdentity>
                    { new ClaimsIdentity(companyUserClaimas.Select(m => m.ToClaim())) });
                distribution = companyUserClaimsPrincipal.Has(Enums.Rule.Menu.Distribution.Module);
            }
            //$is_distribution = $is_distribution ? 1 : 2;

            //$IntegralBill = $this->sessionData->getRule()->getSetting()->getBiter()->hasIntegralBalance();
            //if ($IntegralBill && $company['source'] == companyModel::SOURCE_ONE) {
            //$company['integral'] = true;
            //}
        }

        var isDistribution = distribution ? 1 : 2;

        //当前用户是否拥有开通分销权限 1选择独立、分销 2只有独立账号
        //$company['is_distribution'] = $is_distribution;

        var data = new
        {
            balance = company!.Balance,
            hasMonth,
            id = company.ID,
            integral,
            isDistribution,
            isOpenStore = company.IsOpenStore,
            isOpenUser = company.IsOpenUser,
            openStoreNum = company.OpenStoreNum,
            openUserNum = company.OpenUserNum,
            source = company.Source,
            duration
        };
        return Success(data);
    }

    /// <summary>
    /// 获取操作日志
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Route("{id}")]
    [Permission(Enums.Rule.Config.User.OwnUserLog)]
    public async Task<ResultStruct> GetLog(int id)
    {
        var list = await _userOperateLogServiceService.GetLogPage(id);
        return Success(list);
    }

    /// <summary>
    /// 提交用户信息
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [Permission(Enums.Rule.Config.User.OwnUserAdd)]
    public async Task<ResultStruct> SubOwnInfo(Company dto)
    {
        //检测用户名是否存在
        var info = await _userService.CheckUserName(dto.UserName);
        if (info != null && (dto.Id == 0 || info.ID != dto.Id))
        {
            return Error("用户名已存在");
        }

        if (dto.Id == 0)
        {
            if (dto.OemId == 0)
            {
                dto.OemId = OEMID;
            }

            return await AddOwnInfo(dto);
        }
        else
        {
            //return await EditOwnInfo(dto);
        }

        return Success();
    }

    /// <summary>
    /// 添加独立账号信息
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    private async Task<ResultStruct> AddOwnInfo(Models.View.User.User.Company dto)
    {
        var Session = HttpContext.Session;
        var rule = await _ruleTemplateService.FindByIdAsync(dto.RuleId);

        var ruleTemplate = new RuleTemplateModel
        {
            Name = "初始权限组",
            State = StateEnum.Open,
            OEMID = dto.OemId,
            Remark = ""
        };
        var groupInsert = new UserGroupModel
        {
            Name = "初始用户组",
            State = StateEnum.Open,
            OEMID = dto.OemId
        };
        // 启动事务
        using var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            //添加独立账号
            var user = await _userService.InitUser(dto, Role.ADMIN,
                _oemCache.Get(dto.OemId)!.ThemeId.GetValueOrDefault());
            //var userId = user.Id;
            //增加创建用户信息日志
            await _userOperateLogServiceService.AddUserLog(user.Id, "创建用户");
            //添加有效期
            // var validity = DateTime.Now.AddMonths(dto.Dredge);
            var validity = dto.Duration;
            
            //获取开户公司可选的最大有效期
            if (CompanyID != Enums.ID.Company.XiaoMi.GetNumber())
            {
                var companyInfo = await _companyService.GetInfoById(CompanyID);
                if (companyInfo is null)
                {
                    return Error("找不到相关公司信息");
                }
                
                var optionalValidityMax = companyInfo.Validity.AddYears(1);
                if (validity > optionalValidityMax)
                {
                    return Error("非法的有效期");
                }
            }

            //初始化公司
            var res = await _companyService.InitCompany(dto, validity);
            var companyId = res.ID;
            //更新用户公司id
            _ = await _userService.UpdateUserCompanyId(user, companyId);

            //初始化权限模版 复制权限模板
            var newRule = await _ruleTemplateService.CopyRule(rule.ID, dto.OemId, companyId);
            ruleTemplate.CompanyID = companyId;


            newRule.Name = ruleTemplate.Name;
            newRule.Remark = ruleTemplate.Remark;
            newRule.State = ruleTemplate.State;
            newRule.Sort = DateTime.Now.GetTimeStamp();
            newRule.OEMID = dto.OemId > 0 ? dto.OemId : OEMID;
            newRule.CompanyID = companyId > 0 ? companyId : CompanyID;

            await _ruleTemplateService.SaveInfo(newRule.ID, newRule.ID, newRule, companyId, dto.OemId);

            //初始化用户组
            groupInsert.CompanyID = companyId;
            var group = await _groupService.SaveInfo(groupInsert);

            // //group
            // await _statisticsService.CheckOrAdd(group);

            //SaveUserClaims
            await _userService.SaveUserClaims(dto.RuleId, user.ID);

            //用户统计 添加独立用户创建者公司计数
            var userCreatedEvent = new UserCreatedEvent(user,await _userManager.GetUserAsync(User));
            await _mediator.Publish(userCreatedEvent);
            // await _statisticsService.IncUserCount(Session, user, Enums.ID.Role.ADMIN);

            //初始化用户
            ////业务统计数据
            //await _counterUserBusinessService.InitCounterUserBusiness(user.ID, companyId, dto.OemId);

            ////初始化用户组业务统计数据
            //await _counterGroupBusinessService.InitCounterGroupBusiness(group.ID, companyId, dto.OemId);

            ////初始化公司业务统计数据
            //await _counterCompanyBusinessService.InitCounterCompanyBusiness(companyId, dto.OemId);

            ////初始化资源统计数据
            //var tempUser = new Models.User
            //{
            //    TrueName = dto.Truename,
            //    UserName = dto.Username,
            //};
            //await _counterRequestService.InitCounterRequest(user.ID, 0, companyId, tempUser);

            ////上报当前用户开通用户数 d_user_num
            //await _counterUserBusinessService.UserNumAdd();
            ////上报当前公司新增用户数 d_user_num
            //await _counterCompanyBusinessService.UserNumAdd();
            ////上报上级全部公司新增用户数 d_report_user_num
            //await _counterCompanyBusinessService.ReportUserNumAdd();

            //充值扣款 x???

            //if ($data['recharge'] != 0) {
            ////新用户增加余额账单
            //$IntegralBalanceService = new IntegralBalanceService();
            //$IntegralBalanceService->addBalanceBill($data['recharge'], $data['recharge'], BalanceBillModel::TYPE_RECHARGE, '创建用户充值余额', '', $company_id, $data['oem_id']);

            //    if ($this->sessionData->getUserId() != $this->super_id || $this->sessionData->getCompanyId() == UserModel::COMPANY_ID) {
            //    $this->companyService->charge($this->sessionData->getCompanyId(), $data['recharge']);

            //    $company = $this->companyService->getInfoById($this->sessionData->getCompanyId());
            //    $IntegralBalanceService->addBalanceBill($data['recharge'], $company->getRawOriginal('balance'), BalanceBillModel::TYPE_EXPENSE, '创建用户充值余额', '', $this->sessionData->getCompanyId(), $this->sessionData->getOemId());
            //    }
            //}

            // 提交事务
            transaction.Commit();
        }
        catch (Exception e)
        {
            _logger.LogError(e,e.Message);
            // 回滚事务
            transaction.Rollback();
            return Error(e);
        }

        return Success();
    }

    /// <summary>
    /// 更新用户信息
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPost]
    [Permission(Enums.Rule.Config.User.OwnUserEdit)]
    public async Task<ResultStruct> EditOwnInfo(Models.View.User.User.User req)
    {
        //禁用独立账户，查找公司下员工 ???
        //等权限好了弄 ???
        //if ($info && $info['state'] != $data['state'] && $data['state'] == UserModel::STATE_DISABLE) {
        //$user = $this->userService->grtUidsByCompanyId($info['company_id']);
        //}
        //DB::beginTransaction();
        //try
        //{
        ////更新用户信息
        //$this->userService->editUser($id, $data);
        //    //更新权限组、用户状态，同步更新公司权限组、用户状态
        //    if ($info && ($info['rule_id'] != $data['rule_id'] || $info['state'] != $data['state'])) {
        //    $this->companyService->updateSync($info['company_id'], $data['rule_id'], $data['state']);
        //    $this->delCompanyRedis($info['company_id']);

        //        //禁用用户，更新redis
        //        if ($info['state'] != $data['state'] && $data['state'] == UserModel::STATE_DISABLE) {
        //        $this->userService->UserStateDisable($info['company_id']);
        //            foreach ($user as $value) {
        //            $this->userService->userStateChange($value['id'], 1);
        //            }
        //        }
        //    }
        ////增加更新用户信息日志
        //$UserOperateLogService = new UserOperateLog();
        //$UserOperateLogService->addUserLog($id, '更新用户信息');
        //    DB::commit();
        //}
        //catch (Exception $e) {
        //    DB::rollBack();
        //    return returnArr(false, $e->getMessage());
        //}

        //检测用户名是否存在  
        var infoByUserName = await _userService.CheckUserName(req.UserName);

        if (infoByUserName != null && infoByUserName.ID != req.Id)
        {
            return Error("用户名已存在");
        }

        var info = await _userService.GetInfoById(req.Id);

        // var employeeIds = new List<int>();
        // if (info.State != req.State && info.State == StateEnum.Close)
        // {
        //     employeeIds = await _userService.getUidsByCompanyId(info.CompanyID);
        // }

        var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            if (info.State != req.State)
            {
                await _statisticsService.Reject(new DataBelongDto(info), Statistic.NumColumnEnum.UserReject,
                    req.State == StateEnum.Close);
            }
            
            //更新权限组、用户状态，同步更新公司权限组、用户状态
            if (info.FormRuleTemplateID != req.RuleId || info.State != req.State)
            {
                await _companyService.UpdateSync(info.CompanyID, (Models.DB.Users.Company.States)req.State);

                //禁用用户
                if (info.State != req.State && (req.State == StateEnum.Close || req.State == StateEnum.AdminToDisable ))
                {
                    await _userService.UserStateDisable(info.CompanyID,req.State);
                }
            }
            
            //SaveUserClaims 角色变更时更新权限
            if (req.RuleId != info.FormRuleTemplateID)
            {
                await _userService.SaveUserClaims(req.RuleId, info.ID);
            }

            //更新用户信息
            await _userService.EditUser(req);
            
            //增加更新用户信息日志
            await _userOperateLogServiceService.AddUserLog(info.ID, "更新用户信息");
            await transaction.CommitAsync();
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            Console.WriteLine(e.Message);
            return Error(e.Message);
        }

        return Success();
    }

    public record SetUserInfoVm
    {
        [Required] public string Truename { get; init; } = string.Empty;

        public string Email { get; init; } = string.Empty;
        public string Avatar { get; init; } = string.Empty;
        public string Password { get; init; } = string.Empty;

        [Compare(nameof(Password))] public string ConfirmPassword { get; init; } = string.Empty;

        public string UnitConfig { get; set; } = string.Empty;
        public OrderRateConfigs OrderRateConfig { get; set; }
        public OrderUnitConfig OrderUnitConfig { get; set; }
        public ProductConfig ProductConfig { get; set; }
    }

    // [Permission(Enums.Rule.Config.User.OwnUserEdit)]
    public async Task<ResultStruct> SetUserInfo(SetUserInfoVm vm)
    {
        var user = await _userManager.GetUserAsync(User);
        user.TrueName = vm.Truename;
        user.Avatar = vm.Avatar;
        user.Email = vm.Email;
        user.UnitConfig = vm.UnitConfig;
        Session.SetUnitConfig(user.UnitConfig);
        user.OrderRateConfig = vm.OrderRateConfig;
        user.OrderUnitConfig = vm.OrderUnitConfig ?? OrderUnitConfig.Default;
        user.ProductConfig = vm.ProductConfig ?? ProductConfig.Default;
        Session.SetProductShowUnit(user.ProductConfig.ShowUnit.ToString());
        if (!string.IsNullOrEmpty(vm.Password))
            user.PasswordHash = _userManager.PasswordHasher.HashPassword(user, vm.Password);

        await _dbContext.SaveChangesAsync();
        return Success();
    }

    public async Task<ResultStruct> GetUserInfo()
    {
        var user = await _userManager.GetUserAsync(User);
        await _dbContext.Entry(user).Reference(u => u.Company).LoadAsync();
        await _dbContext.Entry(user).Reference(u => u.ProductConfig).LoadAsync();
        await _dbContext.Entry(user).Reference(u => u.OrderUnitConfig).LoadAsync();
        var company = user.Company;
        var data = new
        {
            user.UserName,
            user.TrueName,
            user.Mobile,
            user.Email,
            user.Avatar,
            user.UnitConfig,
            user.OrderRateConfig,
            user.ProductConfig,
            user.OrderUnitConfig,
            company.AllowPhone,
            theme = "",
            unit = _unitCache.List(),
            company.Source,
            company.Type,
            user.CreatedAt,
            validity = company.ShowValidityInfo,
            product_show_unit = user.ProductConfig.ShowUnit,
        };

        return Success(data);
    }

    /// <summary>
    /// 独立账户重置密码123456
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Route("{id}")]
    [Permission(Enums.Rule.Config.User.UserResetPassword)]
    public async Task<ResultStruct> ResetPassword(int id)
    {
        //if (HttpContext.Session.GetConcierge() != Concierges.Child /*&& !$this->ruleInfo()->hasOwnUserResetPassword()*/ )
        //{
        //    return Error("暂无权限重置密码");
        //}
        //var res = _userService.ResetPassword(id);
        var user = await _userManager.FindByIdAsync(id.ToString());
        user.PasswordHash = _userManager.PasswordHasher.HashPassword(user, "123456");
        await _userManager.UpdateAsync(user);

        //增加更新用户信息日志
        await _userOperateLogServiceService.AddUserLog(id, "重置密码123456");
        return Success();
    }

    /************************************* 子账号 ****************************************************/
    /// <summary>
    /// 获取用户列表筛选条件信息
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetSearchChildList()
    {
        var Session = HttpContext.Session;
        var oemId = Session.GetOEMID();
        var groups = _groupService
            .GetGroupList()
            .Result
            .ToDictionary(m => m.ID, m => m);
        var ruleTemplate = await _ruleTemplateService
            .GetRuleList(Session.GetCompanyID(), Session.GetOEMID());


        //获取当前公司允许开通员工信息
        var companyInfo = await _companyService.GetLimitInfoById(Session.GetCompanyID());

        //当前公司下用户数
        var num = await _userService.GetUserNumByCompanyId(Session.GetCompanyID());
        //员工权限
        var rules = new
        {
            UserAdd = true,
            UserEdit = true,
            UserLog = true,
            UserResetPassword = true,
        };
        if (Session.GetRole() == Role.Employee)
        {
            //    $data['rules'] = [
            //    'UserAdd'           => $this->ruleInfo()->hasUserAdd(),
            //    'UserEdit'          => $this->ruleInfo()->hasUserEdit(),
            //    'UserLog'           => $this->ruleInfo()->hasUserLog(),
            //    'UserResetPassword' => $this->ruleInfo()->hasUserResetPassword(),
            //];
        }

        var data = new
        {
            company = new
            {
                isOpenUser = companyInfo?.IsOpenUser,
                openUserNum = companyInfo?.OpenUserNum - num + 1
            },
            groups,
            num = ruleTemplate.FirstOrDefault(a => a.State == StateEnum.Open) != null ? 1 : 0,
            rule = ruleTemplate.ToDictionary(m => m.ID, m => m),
            rules,
            userId = Session.GetUserID()
        };
        return Success(data);
    }

    /// <summary>
    /// 获取子账号数据
    /// </summary>
    /// <param name="vm"></param>
    /// <returns></returns>
    public async Task<ResultStruct> GetChildList([FromQuery] GetChildListVM vm)
    {
        var res = await _userService.GetChildUserListPage(vm, CompanyID);
        if (res.Items.Count > 0)
        {
            res.Transform(a => new
            {
                a.Id,
                a.UserName,
                a.TrueName,
                a.GroupID,
                a.FormRuleTemplateID,
                a.State,
                a.CreatedAt,
                a.CompanyID,
            });
        }

        return Success(res);
    }

    /// <summary>
    /// 提交账户信息(添加/修改)
    /// </summary>
    /// <param name="vm"></param>
    /// <returns></returns>
    [Permission(Enums.Rule.Config.User.UserEdit)]
    public async Task<ResultStruct> SubChildInfo(Models.View.User.User.User vm)
    {
        if (Session.GetRole() == Role.Employee)
        {
            return Error("暂无权限");
        }

        //检测用户名是否存在
        var info = await _userService.CheckUserName(vm.UserName);
        if (info != null && (vm.Id == 0 || info.Id != vm.Id))
        {
            return Error("用户名已存在");
        }

        // 前端没有手机号input!!!
        //检测手机号码是否存在 
        //if (vm.Mobile.IsNotWhiteSpace())
        //{
        //    if (vm.Mobile!.Length != 11)
        //    {
        //        vm.Mobile = string.Empty;
        //    }
        //    else
        //    {
        //        if (await _userService.CheckUserMobile(vm.Mobile))
        //            return Error("当前手机号码已存在!");
        //    }
        //}
        if (vm.Id == 0)
        {
            return await AddChildInfo(vm);
        }
        else
        {
            return await EditChildInfo(vm);
        }
    }

    /// <summary>
    /// 创建子账号
    /// </summary>
    /// <param name="vm"></param>
    /// <returns></returns>
    private async Task<ResultStruct> AddChildInfo(Models.View.User.User.User vm)
    {
        //获取当前公司配置信息
        var company = await _companyService.GetLimitInfoById(CompanyID);
        if (company == null)
        {
            return Error("无法找到公司-请联系客服");
        }

        if (company.IsOpenUser == false)
        {
            if (company.OpenUserNum == 0)
            {
                return Error("当前可开通子账户数量为0！");
            }

            //当前公司下用户数
            var num = await _userService.GetUserNumByCompanyId(CompanyID);
            num = company.OpenUserNum - num + 1;
            if (num <= 0)
            {
                return Error("当前可开通子账户数量为0！");
            }
        }

        using var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            //用户组未选择，新增用户组
            if (vm.GroupId == 0)
            {
                var groupInsert = new UserGroupModel
                {
                    Name = vm.TrueName,
                    State = StateEnum.Open,
                    OEMID = OEMID,
                    CompanyID = CompanyID
                };

                var group = await _groupService.SaveInfo(groupInsert);
                vm.GroupId = group.ID;
            }

            //添加子账号
            var user = await _userService.InitUser(vm, Role.Employee,
                _oemCache.Get(OEMID)!.ThemeId.GetValueOrDefault());

            //增加创建用户信息日志
            await _userOperateLogServiceService.AddUserLog(user.Id, "创建内部用户");

            //SaveUserClaims
            await _userService.SaveUserClaims(vm.RuleId, user.ID);

            //用户统计
            var employeeCreatedEvent = new EmployeeCreatedEvent(user,await _userManager.GetUserAsync(User));
            await _mediator.Publish(employeeCreatedEvent);

            //初始化用户业务统计数据
            //$CounterUserBusinessService = new CounterUserBusiness();
            //$CounterUserBusinessService->initCounterUserBusiness(
            //    $user_id,
            //    $this->sessionData->getCompanyId(),
            //    $this->sessionData->getOemId()
            //);

            ////初始化资源统计数据
            //$CounterRequestService = new CounterRequest();
            //$CounterRequestService->initCounterRequest(
            //    $user_id,
            //    $data['group_id'],
            //    $this->sessionData->getCompanyId(),
            //    $data
            //);

            ////上报当前用户开通用户数 d_user_num
            //$CounterUserBusinessService->userNumAdd();
            ////上报当前公司新增用户数 d_user_num
            //$CounterCompanyBusinessService = new CounterCompanyBusiness();
            //$CounterCompanyBusinessService->userNumAdd();
            ////上报上级全部公司新增用户数 d_report_user_num
            //$CounterCompanyBusinessService->reportUserNumAdd();
            await transaction.CommitAsync();
        }
        catch (Exception ex)
        {
            await transaction.RollbackAsync();
            return Error(ex.Message);
        }

        return Success();
    }

    /// <summary>
    /// 编辑子账号
    /// </summary>
    /// <param name="vm"></param>
    /// <returns></returns>
    [Permission(Enums.Rule.Config.User.UserEdit)]
    public async Task<ResultStruct> EditChildInfo(Models.View.User.User.User vm)
    {
        using var transaction = await _dbContext.Database.BeginTransactionAsync();
        try
        {
            var sourceUser = await _userManager.FindByIdAsync(vm.Id.ToString());

            if (sourceUser.State != vm.State)
            {
                await _statisticsService.Reject(new DataBelongDto(sourceUser), Statistic.NumColumnEnum.EmployeeReject,
                    vm.State == StateEnum.Close);
            }
            
            //用户组变更 财务迁移
            if (sourceUser.GroupID != 0  && vm.GroupId != 0 && sourceUser.GroupID != vm.GroupId)
            {
                //原用户组减去财务
                var sourceGroupId = sourceUser.GroupID;
                //新用户组增加财务
                var newGroupId = vm.GroupId;

                var amount = await _statisticsService.AmountByUser(sourceUser.Id);
                //减少财务
                await _statisticMoney.ReduceFianceToGroup(sourceGroupId,amount);
                //增加财务
                await _statisticMoney.PlusFianceToGroup(newGroupId,amount);
            }

            //更新用户信息
            var user = await _userService.EditUser(vm);
            //增加更新用户信息日志
            await _userOperateLogServiceService.AddUserLog(vm.Id, "更新用户信息");

            //SaveUserClaims
            await _userService.SaveUserClaims(vm.RuleId, vm.Id);
            
         

            await transaction.CommitAsync();
        }
        catch (Exception ex)
        {
            await transaction.RollbackAsync();
            return Error(ex.Message);
        }

        return Success();
    }

    /// <summary>
    /// 获取当前用户的自定义标记模板
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> GetUserCustomMarkTemplate()
    {
        var user = await _userManager.GetUserAsync(User);
        return Success(user.GetCustomMarkTemplate());
    }

    /// <summary>
    /// 设置当前用户的自定义标记模板
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> SetUserCustomMarkTemplate(List<CustomMarkJson> lc)
    {
        var user = await _userManager.GetUserAsync(User);
        user.SetCustomMarkTemplate(lc);
        await _userManager.UpdateAsync(user);
        return Success();
    }

    /*************************************************组配置****************************************************/
    public class GroupListVM
    {
        public int Id;
        public string Name = string.Empty;
    }

    /// <summary>
    /// 组-店铺配置 init
    /// </summary>
    /// <returns></returns>
    [Route("{id}")]
    public async Task<ResultStruct> GetStoreList(int id)
    {
        var allList = await _userService.GetStoreByCompanyId();
        var checkedList = await _userService.GetGroupStoreList(id);
        var data = new
        {
            list = allList.Select(a =>
                new GroupListVM
                {
                    Id = a.ID,
                    Name = a.Name
                }),
            checkedList = checkedList.Select(a => a.ID)
        };
        return Success(data);
    }

    /// <summary>
    /// 组-店铺配置-submit
    /// </summary>
    /// <returns></returns>
    [Route("{id}")]
    public async Task<ResultStruct> EditGroupStore(int id, List<int> storeIds)
    {
        try
        {
            await _userService.EditGroupStoreList(id, storeIds);
        }
        catch (Exception ex)
        {
            return Error(ex.Message);
        }

        return Success();
    }

    /// <summary>
    /// 组-用户配置 init
    /// </summary>
    /// <returns></returns>
    [Route("{id}")]
    public async Task<ResultStruct> GetUserList(int id)
    {
        var allList = await _userService.GetUserByCompanyId();
        var checkedList = await _userService.GetGroupUserList(id);
        var data = new
        {
            list = allList.Select(a =>
                new GroupListVM
                {
                    Id = a.ID,
                    Name = a.TrueName
                }),
            checkedList = checkedList.Select(a => a.ID)
        };
        return Success(data);
    }

    /// <summary>
    /// 组-用户配置-submit
    /// </summary>
    /// <param name="id"></param>
    /// <param name="userIds"></param>
    /// <returns></returns>
    [Route("{id}")]
    public async Task<ResultStruct> EditGroupUser(int id, List<int> userIds)
    {
        try
        {
            await _userService.EditGroupUserList(id, userIds);
        }
        catch (Exception ex)
        {
            return Error(ex.Message);
        }

        return Success();
    }

    /// <summary>
    /// 员工绑定店铺 init
    /// </summary>
    /// <param name="id">storeId</param>
    /// <returns></returns>
    [Route("{id}")]
    public async Task<ResultStruct> GetGroupStoreTree(int id)
    {
        var allList = await _userService.GetStoreByCompanyId();
        var checkedList = await _userService.GetUserBindStoreList(id);
        var groupTree = await _userService.GetGroupStoreTree();
        var data = new
        {
            list = allList.Select(a =>
                new GroupListVM
                {
                    Id = a.ID,
                    Name = a.Name
                }),
            checkedList = checkedList.Select(a => a.CorrelationStoreID),
            groupTree
        };
        return Success(data);
    }

    /// <summary>
    /// 店铺绑定员工 init
    /// </summary>
    /// <returns></returns>
    [Route("{id}")]
    public async Task<ResultStruct> GetGroupUserTree(int id)
    {
        var allList = await _userService.GetUserByCompanyId();
        var checkedList = await _userService.GetStoreBindUserList(id);
        var groupTree = await _userService.GetGroupUserTree();
        var data = new
        {
            list = allList.Select(a =>
                new GroupListVM
                {
                    Id = a.ID,
                    Name = a.TrueName
                }),
            checkedList = checkedList.Select(a => a.CorrelationUserID),
            groupTree
        };
        return Success(data);
    }

    /// <summary>
    /// 员工绑定店铺 edit
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="storeIds"></param>
    /// <returns></returns>
    [Route("{userId}")]
    public async Task<ResultStruct> EditUserBindStore(int userId, List<int> storeIds)
    {
        try
        {
            await _userService.UserBindStore(userId, storeIds);
        }
        catch (Exception ex)
        {
            return Error(ex.Message);
        }

        return Success();
    }

    /// <summary>
    ///  店铺绑定员工 edit
    /// </summary>
    /// <param name="storeId"></param>
    /// <param name="userIds"></param>
    /// <returns></returns>
    [Route("{storeId}")]
    public async Task<ResultStruct> EditStoreBindUser(int storeId, List<int> userIds)
    {
        
        try
        {
            await _userService.StoreBindUser(storeId, userIds);
        }
        catch (Exception ex)
        {
            return Error(ex.Message);
        }
        
        return Success();
    }

    [HttpPost]
    public async Task<ResultStruct> UpdateAgency(UpdateAgencyDto req)
    {
        var result = await _userService.UpdateAgency(req);

        return result ? Success() : Error();
    }

    [HttpPost]
    public async Task<ResultStruct> UpdateState(UpdateStateDto req)
    {
        if (SessionProvider.Session.GetCompanyID() != Enums.ID.Company.XiaoMi.GetNumber())
        {
            return Error("无权操作");
        }
        
        var result = await _userService.UpdateState(req);

        return result ? Success() : Error();
    }
    
    [HttpPost]
    public async Task<ResultStruct> SetUserState(SetUserStateDto req)
    {
        var result = await _userService.SetUserState(req);

        return result ? Success() : Error();
    }
}