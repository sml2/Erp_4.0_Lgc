using ERP.Enums.ID;
using ERP.Extensions;
using ERP.Models;
using ERP.Models.View.User.Register;
using ERP.Services.DB.Sms;
using ERP.Services.DB.Users;
using ERP.Services.Tools;
using Task = System.Threading.Tasks.Task;

namespace ERP.Controllers.User;

[Route("/api/[controller]/[action]")]
[ApiController]
public class RegisterController : BaseController
{
    private readonly UserService _userService;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly SendService _sendService;
    private readonly RegisterService _registerService;

    //验证码有效期 5分钟
    private const int ValidCode = 300;

    //验证码发送最少间隔时长 60秒
    private const int IntervalTime = 60;

    private ISession _session => _httpContextAccessor.HttpContext!.Session;


    public RegisterController(UserService userService, IHttpContextAccessor httpContextAccessor,
        SendService sendService, RegisterService registerService)
    {
        _userService = userService;
        _httpContextAccessor = httpContextAccessor;
        _sendService = sendService;
        _registerService = registerService;
    }

    [HttpPost]
    public async Task<ResultStruct> CheckUserName(CheckUserNameDto req)
    {
        var result = await _userService.CheckUserName(req.Username);

        if (result != null)
            return Error("当前用户名已存在，请重新输入");

        return Success("该用户名可以使用");
    }

    [HttpPost]
    public async Task<ResultStruct> GetCode(GetCodeDto req, [FromServices] Captcha captcha)
    {
        var result = await _userService.CheckUserMobile(req.Mobile);
        if (result != null)
            return Error("当前手机号码已存在，请重新输入或直接登录");

        object data = new();
        string key = captcha.GetRandomText(4);
        _session.SetString("Captcha", key.ToLower());
        return Success(data: new
        {
            Img = captcha.ImageToBase64(captcha.GetCaptcha(key)),
            Key = key
        }, message: "成功");
    }

    [HttpPost]
    public async Task<ResultStruct> SendSms(SendSmsDto req, [FromServices] Captcha captcha)
    {
        return Success("短信功能正在维护，请稍后再试");
        
        var user = await _userService.CheckUserMobile(req.Mobile);
        if (user != null)
            return Error("当前手机号码已存在，请重新输入或直接登录");

        var smsCode = await _sendService.GetEndCode(req.Mobile, SmsCodeTypeEnum.Register);

        if (smsCode is not null && (DateTime.Now - smsCode.SendTime).TotalSeconds < IntervalTime)
        {
            return Error("发送过于频繁");
        }
        
        //判断ip出现次数
        var ip = _httpContextAccessor.HttpContext!.GetIP();
        var isExceed = await _sendService.CheckIpRequest(ip);
        if (isExceed)
        {
            return Error($"IP:{ip}, 今日请求已经超出限定次数，请明天再试"); 
        }
        //6位随机数
        var code = (new Random()).Next(100000, 999999);

        var sendData = new Dictionary<string, object>()
        {
            { "code", code },
            { "time", "5分钟" },
        };

        var result = await _sendService.SendBindCode(req.Mobile, sendData);
        //debug
        // var result = new
        // {
            // state = true,
            // msg=""
        // };
        if (result.state)
        {
            await _sendService.AddSms(req.Mobile, code, DateTime.Now, SmsCodeTypeEnum.Register,OEM.MiJingTong.GetNumber(),ip);
            return Success("短信发送成功");
        }

        return Error(result.msg);
    }

    [HttpPost]
    public ResultStruct VerifyCode(VerifyCodeDto req, [FromServices] Captcha captcha)
    {
        //验证验证码正确性
        if (!captcha.Compare(_session.GetString("Captcha"), req.Code))
        {
            return Error(message: "验证码不正确");
        }

        return Success();
    }

    public async Task<ResultStruct> RegisterUser(SubDto req)
    {
        //检测当前手机获取验证码时效
        var smsCode = await _sendService.GetEndCode(req.Register.Mobile, SmsCodeTypeEnum.Register);

       /*if (smsCode is null || smsCode.Code != req.Register.Yzm ||
            (DateTime.Now - smsCode.SendTime).TotalSeconds > ValidCode ||
            smsCode.Used)
        {
            return Error("短信验证码错误");
        }*/

        var info = await _userService.CheckUserName(req.Register.Username);

        if (info != null)
            return Error("当前用户名已存在，请重新输入");
        try
        {
            await _registerService.SubRegister(req,smsCode);
            return Success();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return Error(e.Message);
        }
    }
}
