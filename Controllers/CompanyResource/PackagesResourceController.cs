using Common.Enums.ResourcePackage;
using ERP.Extensions;
using ERP.Models.View.Setting;
using ERP.Services.DB.CompanyResource;

namespace ERP.Controllers.CompanyResource;

[Route("api/[controller]")]
public class PackagesResourceController : BaseController
{
    private readonly PackagesResourceService _service;


    public PackagesResourceController(PackagesResourceService service)
    {
        _service = service;
    }


    [HttpPost("index")]
    public async Task<ResultStruct> Index(GetPackagesDto req)
    {
        var result = await _service.Index(req);
        return Success(result);
    }

    [HttpGet("info/{id}")]
    public async Task<ResultStruct> Info(int id)
    {
        var result = await _service.Info(id);
        return result != null ? Success(result) : Error();
    }

    [HttpPost("getPurchase")]
    public async Task<ResultStruct> GetPurchase(int id)
    {
        var matting = await _service.Packages(PackageTypeEnum.Matting);
        var translation = await _service.Packages(PackageTypeEnum.Translation);
        var imgTran = await _service.Packages(PackageTypeEnum.ImgTranslation);
        return Success(new Dictionary<int, object>()
        {
            { PackageTypeEnum.Matting.GetNumber(), matting },
            { PackageTypeEnum.Translation.GetNumber(), translation },
            { PackageTypeEnum.ImgTranslation.GetNumber(), imgTran },
        });
    }
    [HttpPost("subPurchase")]
    public async Task<ResultStruct> SubPurchase(SubPurchaseDto req)
    {
        var result = await _service.SubPurchase(req);
        return result.State ? Success() : Error(result.Message);
    }
    [HttpGet("useInfo/{id}")]
    public async Task<ResultStruct> UseInfo(int id)
    {
        var result = await _service.UseInfo(id);
        return Success(result);
    }
    
    [HttpPost("getPresent")]
    public async Task<ResultStruct> GetPresent(int id)
    {
        var matting = await _service.Packages(PackageTypeEnum.Matting,false);
        var translation = await _service.Packages(PackageTypeEnum.Translation,false);
        var imgTran = await _service.Packages(PackageTypeEnum.ImgTranslation,false);
        return Success(new Dictionary<int, object>()
        {
            { PackageTypeEnum.Matting.GetNumber(), matting },
            { PackageTypeEnum.Translation.GetNumber(), translation },
            { PackageTypeEnum.ImgTranslation.GetNumber(), imgTran },
        });
    }
    [HttpPost("subPresent")]
    public async Task<ResultStruct> SubPresent(SubPresentDto req)
    {
        var result = await _service.SubPresent(req);
        return result.State ? Success() : Error(result.Message);
    }
}