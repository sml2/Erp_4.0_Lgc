using Model = ERP.Models.DB.Users.Company;
using Cache = ERP.Services.Caches.Company;
using static ERP.Helpers;
using ERP.Extensions;
using Newtonsoft.Json;
using System;
using ERP.Services.DB.Users;
using static ERP.Models.DB.Users.Company;

using ERP.Data;
using ERP.Services.Finance;
using Microsoft.AspNetCore.Identity;
using BillLog = ERP.Models.Finance.BillLog;

namespace ERP.Controllers.Setting;

[Route("/api/setting/[controller]/[action]")]
public class CompanyController : BaseController
{
    private readonly CompanyService _companyService;
    private readonly DBContext _dbContext;
    private readonly UserManager<Models.DB.Identity.User> _userManager;
    private readonly Cache _cache;
    private readonly BillingRecordsService  _billingRecordsService;
    private readonly ILogger<CompanyController> _logger;
    //private readonly 
    public CompanyController(CompanyService companyService, DBContext myDbContext, Cache cache, BillingRecordsService billingRecordsService, UserManager<Models.DB.Identity.User> userManager, ILogger<CompanyController> logger)
    {
        _companyService = companyService;
        _dbContext = myDbContext;
        _cache = cache;
        _billingRecordsService = billingRecordsService;
        _userManager = userManager;
        _logger = logger;
    }

    //获取当前公司配置信息
    [HttpPost]
    public async Task<ResultStruct> GetConfig()
    {
        var company = await _companyService.GetCurrentConfig(HttpContext.Session.GetCompanyID());
        
        //邀请码 TODO: 分离
        string inviteCode = "";
        var key = "C" + From10to62(company.ID);
        if (!Convert.ToBoolean(HttpContext.Session.GetIsDistribution()))
        {
            string newMd5 = "";
            string str = Sy.Security.MD5.Encrypt(key);
            for (int i = 0; i < str.Length; i++)
            {
                var char1 = str.Substring(i, 1);
                newMd5 += new Random().Next(1, 3) == 1 ? char1.ToUpper() : char1;
            }
            inviteCode = From10to62(DateTime.Now.GetTimeStamp()) + key + newMd5;
        }

        //推广码，推广链接
        var refererCode = "";
        var refererUrl = "";
        if (HttpContext.Session.GetLevel() == 1)
        {
            refererCode = string.Concat(key, Sy.Security.MD5.Encrypt(key).AsSpan(8, 4));
            refererUrl = $"http://{Request.Host}/#/register?inviteCode={refererCode}";
        }

        if (company.ShowOemId != 1)
        {
            var priceCf = company.DefaultPriceConfig;
            if (priceCf != null)
            {
                //$priceCf = json_decode($priceCf, true);
                //foreach ($priceCf as $k => $v){
                //$priceCf[$k] = $v <= 0 ? 0 : bcmul($v, 1000, 2);
                //}
                //$company['default_price_config'] = $priceCf;
            }
        }
        else
        {
            company.DefaultPriceConfig = null;
        }

        var parentCompanyName = _cache.Get(company.ReportId)?.Name;
        //inviteCode
        //refererCode
        //refererUrl
        return Success(new { company, temp = new { inviteCode, refererCode, refererUrl, parentCompanyName } });
    }    

    /// <summary>
    /// 更新当前公司配置信息
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> SubConfig(Model? c)
    {
        //Request.HttpContext.Session["companyId"] 
        var list = await _companyService.UpdateCurrentConfig(c!);
        //$this->delCompanyRedis($this->sessionData->getCompanyId());
        return Success();
    }

    public class BindingInviteCodeDTO
    {
        public string InviteCode { get; set; } = string.Empty;
        public int Type { get; set; }
    }

    /// <summary>
    /// 分销邀请码绑定、解绑
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> BindingInviteCode(BindingInviteCodeDTO r)
    {
        ResultStruct res;
        if (r.Type == 1)
        {
            res = await Binding(r.InviteCode);
        }
        else if (r.Type == 2)
        {
            res = await Unbind();
        }
        else
        {
            return Error(message: "参数错误");
        }
        //$this->delCompanyRedis($this->sessionData->getCompanyId());???
        return res;
    }

    private async Task<ResultStruct> Binding(string inviteCode)
    {
        var Session = HttpContext.Session;
        //解析邀请码
        var time = From62to10(inviteCode[..6]);
        if (DateTime.Now.GetTimeStamp() - time > 48 * 60 * 60)
            return Error(message: "邀请码已过期");
        var id = From62to10(inviteCode[7..^32]);
        if (id == Session.GetCompanyID())
            return Error(message: "只能绑定除当前账号之外的邀请码");
        string res = From10to62(id);
        var md5 = inviteCode[^32..];
        if (md5 != Sy.Security.MD5.Encrypt("C" + res))
        {
            return Error(message: "请输入正确的邀请码");
        }
        //$company = $this->getCompany($id) //redis???
        var company = _cache.Get(id);
        if (company is null)
        {
            return Error(message: "无法获取到国家");
        }
        if (company.IsDistribution == true)
        {
            return Error("绑定公司已为分销，无法绑定");
        }
        if (Convert.ToBoolean(Session.GetIsDistribution()) == true)
        {
            //分销绑定
            //更新当前公司report_id
            var report_id = company.ReportId;
            var report_pids = company.ReportPids;
            var companyConfig = await _companyService.GetCurrentConfig();
            if (companyConfig is null) return Error(message: "没有当前该公司");
            if (report_id == companyConfig.ReportId)
            {
                return Error("邀请码已绑定，无需再次使用");
            }
            using var transaction = await _dbContext.Database.BeginTransactionAsync();
            try
            {
                await _companyService.InviteCode(report_id, report_pids, true, 1);
                //增加清除对公钱包数据账单
                //$FinanceService = new FinanceService();
                //$FinanceService->addEmptyWalletBill($company->getRawOriginal('public_wallet'), '分销公司换绑清除对公余额', $this->getCompany());
                Session.SetIsDistribution(true);
                //清除上级公司分配物流
                //$LogisticsParam = new LogisticsParam();
                //$LogisticsParam->clearAllot($company['id']);
                ////清除当前公司用户掉线
                //$userService = new userService();
                //$res = $userService->grtUidsByCompanyId($company['id']);
                //foreach ($res as $value) {
                //    $this->writeLog($value['id']);
                //}
                //添加账单 对公 无需审核
                //增加清除对公钱包数据账单
                var user = await _userManager.GetUserAsync(User);
                await _billingRecordsService.AddBillLogNoNeedReviewWithRight(new AddBillLogDto()
                {
                    Money = company.PublicWalletUi,
                    Type = BillLog.Types.Deduction,
                    Datetime = DateTime.Now,
                    Reason = "分销公司换绑清除对公余额",
                    Remark = string.Empty,
                    UserID = user.ID,
                    GroupID = user.GroupID,
                    CompanyID = user.CompanyID,
                    Truename = user.TrueName,
                },await _companyService.GetInfoById(user.CompanyID), BillLog.Modules.INTERCHANGEABLE);
                await transaction.CommitAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex,ex.Message);
                await transaction.RollbackAsync();
                return Error(ex.Message);
            }
        }

        else
        {
            //普通代理绑定
            //查找当前公司下是否存在分销账户
            var count = await _companyService.GetReportIdCompany();
            if (count > 0)
            {
                return Error("当前存在下级分销账户，无法绑定");
            }
            //更新当前公司report_id
            var report_id = id;
            var report_pids = company.ReportPids;

            await _companyService.InviteCode(report_id, report_pids, true);
            //$userService = new userService();
            //    $userService->updateDistribution();
            //    $this->sessionData->setIsDistribution(companyModel::DISTRIBUTION_TRUE);
            //    //清除当前公司用户掉线
            //    $res = $userService->grtUidsByCompanyId($this->sessionData->getCompanyId());
            //foreach ($res as $value) {
            //        $this->writeLog($value['id']);
            //}
            return Success();
        }
        return Success();
    }

    /// <summary>
    /// 分销用户解绑
    /// </summary>
    /// <returns></returns>
    private async Task<ResultStruct> Unbind()
    {
        var company = await _companyService.GetCurrentConfig();
        if (company is null) return Error(message: "无法找到");

        if (company.IsDistribution == false)
        {
            return Error("当前公司已经解绑，无法再次解绑");
        }
        var parent_company = _cache.Get(company.ParentCompanyId);
        if (parent_company is null) return Error(message: "无法找到");

        string report_pids;
        if (parent_company!.IsDistribution == false)
        {
            var pids = JsonConvert.DeserializeObject<List<int>>(parent_company.ReportPids);
            pids?.Add(company.ID);
            report_pids = JsonConvert.SerializeObject(pids);
        }
        else
        {
            report_pids = parent_company!.ReportPids;
        }
        using var transaction = await _dbContext.Database.BeginTransactionAsync();
        try{
            await _companyService.InviteCode(company.ID, report_pids, false, 1);
            //增加清除对公钱包数据账单
            //添加账单 对公 无需审核
            var user = await _userManager.GetUserAsync(User);
            await _billingRecordsService.AddBillLogNoNeedReviewWithRight(new AddBillLogDto()
            {
                Money = company.PublicWalletUi,
                Type = BillLog.Types.Deduction,
                Datetime = DateTime.Now,
                Reason = "分销公司换绑清除对公余额",
                Remark = string.Empty,
                UserID = user.ID,
                GroupID = user.GroupID,
                CompanyID = user.CompanyID,
                Truename = user.TrueName,
            },await _companyService.GetInfoById(user.CompanyID), BillLog.Modules.INTERCHANGEABLE);
            await transaction.CommitAsync();
        }
        catch (Exception ex)
        {
            _logger.LogError(ex,ex.Message);
            await transaction.RollbackAsync();
            return Error(ex.Message);
        }
        //$userService = new userService();
        //    $userService->updateDistribution(companyModel::DISTRIBUTION_FALSE);

        //    //增加清除对公钱包数据账单
        //    $FinanceService = new FinanceService();
        //    $FinanceService->addEmptyWalletBill($company->getRawOriginal('public_wallet'), '分销公司解绑清除对公余额', $this->getCompany());

        //    $this->sessionData->setIsDistribution(companyModel::DISTRIBUTION_FALSE);
        //    //清除上级公司分配物流
        //    $LogisticsParam = new LogisticsParam();
        //    $LogisticsParam->clearAllot($company['id']);
        //    //清除当前公司用户掉线
        //    $res = $userService->grtUidsByCompanyId($company['id']);
        //foreach ($res as $value) {
        //        $this->writeLog($value['id']);
        //}
        return Success();
    }

    /// <summary>
    /// 用户掉线日志
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    private async Task<ResultStruct> WriteLog(int id)
    {
        //$info = Cache::instance()->User()->get($id);
        //Log::channel('bindCode')->info($info);
        //Cache::instance()->User()->del($id);
        return await Task.FromResult(Success());
    }

    /// <summary>
    /// 推广信息
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetReferer()
    {
        var company = await _companyService.GetCurrentConfig();
        if (company is null)
        {
            return Error("");
        }
        //推广码，推广链接
        string key = "C" + From10to62(company.ID);
        var refererCode = string.Concat(key, Sy.Security.MD5.Encrypt(key).AsSpan(8, 4));

        var data = new
        {
            refererCode = refererCode,
            discount = company.Discount,
            refererUrl = "https://www.miwaimao.com/erp/?inviteCode=" + refererCode,
            refererRank = new[]
            {
                new { username = "huna***", num = 416, integral = 20562, commission = 15280 },
                new { username = "petan***", num = 379, integral = 18600, commission = 11800 },
                new { username = "plati***", num = 346, integral = 17000, commission = 8563 },
                new { username = "motho***", num = 259, integral = 12950, commission = 7880 },
                new { username = "ntra***", num = 210, integral = 10300, commission = 7630 },
            },
            refererRegister = await _companyService.GetRefererRegister()
        };
        return Success(data);
    }

    /// <summary>
    /// 获取公司实时信息
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> GetPackage()
    {
        var company = await _companyService.GetCurrentConfig();
        var data = new object();
        if (company is not null)
        {
            data = new
            {
                source = company.Source,
                type = company.Type,
                balance = company.Balance,
                day = (int)Math.Floor(Convert.ToDecimal(((Convert.ToInt32(company.Validity) - DateTime.Now.GetTimeStamp()) / 86400))),
            };
        }
        return Success(data);
    }
}

