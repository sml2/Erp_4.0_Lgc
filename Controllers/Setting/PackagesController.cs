using Common.Enums.ResourcePackage;
using ERP.Models.View.Setting;
using ERP.Services.Setting;
using NPOI.SS.Formula.Functions;

namespace ERP.Controllers.Setting;

[Route("api/[controller]")]
public class PackagesController : BaseController
{
    private readonly PackagesService _service;


    public PackagesController(PackagesService service)
    {
        _service = service;
    }



    [HttpPost("index")]
    public async Task<ResultStruct> Index(GetPackagesDto req)
    {
        var result = await _service.Index(req);
        return Success(result);
    }

    [HttpGet("setItemState/{type}/{id}")]
    public async Task<ResultStruct> SetItemState(PackageTypeEnum type, int id)
    {
        var result = await _service.SetItemState(type, id);
        return result ? Success() : Error();
    }

    [HttpGet("deleteItem/{type}/{id}")]
    public async Task<ResultStruct> DeleteItem(PackageTypeEnum type, int id)
    {
        var result = await _service.DeleteItem(type, id);
        return result ? Success() : Error();
    }

    [HttpGet("getPackageInfo/{type}")]
    public async Task<ResultStruct> GetPackageInfo(PackageTypeEnum type)
    {
        var result = await _service.GetPackageInfo(type);
        return Success(result);
    }

    [HttpPost("updatePackage")]
    public async Task<ResultStruct> UpdatePackage(UpdatePackageDto req)
    {
        var result = await _service.UpdatePackage(req);
        return result ? Success() : Error();
    }
    
    [HttpPost("itemEdit")]
    public async Task<ResultStruct> ItemEdit(PackageItemDto req)
    {
        var result = await _service.ItemEdit(req);
        return result ? Success() : Error();
    }

    // #region Matting
    //
    // [HttpPost("mattingEdit")]
    // public async Task<ResultStruct> MattingEdit(EditMattingDto req)
    // {
    //     var result = await _service.MattingEdit(req);
    //     return result ? Success() : Error();
    // }
    //
    // #endregion
    //
    // #region Translation
    //
    // [HttpPost("translationEdit")]
    // public async Task<ResultStruct> TranslationEdit(EditTranslationDto req)
    // {
    //     var result = await _service.TranslationEdit(req);
    //     return result ? Success() : Error();
    // }
    //
    // #endregion
}