using ERP.Enums;
using ERP.Services.Setting;
using ERP.Extensions;
using ERP.Models.Abstract;
using ERP.Models.DB.Setting;
using ERP.Services.Caches;
using ERP.ViewModels;
using ERP.ViewModels.Dictionary;
using AmazonKey = ERP.Services.Caches.AmazonKey;
using Unit = ERP.Services.Caches.Unit;
using ERP.Models.Setting;

namespace ERP.Controllers.Setting
{
    [Route("api/[controller]")]
    [ApiController]
    public class DictionaryController : BaseController
    {
        private readonly DictionaryService _service;
        private readonly AmazonKey _amazonCache;
        private readonly Unit _unitCache;
        private readonly Services.Caches.Nation _nationCache;
        private readonly PlatformCache _platformcache;

        public DictionaryController(DictionaryService service, AmazonKey amazonCache, Unit unitCache,
            Services.Caches.Nation nationCache, PlatformCache platformcache)
        {
            _service = service;
            _amazonCache = amazonCache;
            _unitCache = unitCache;
            _nationCache = nationCache;
            _platformcache = platformcache;
        }

        #region 亚马逊国家

        [Route("getAmazonNationList")]
        [HttpPost]
        public async Task<ResultStruct> GetAmazonNationList(AmazonNationListDto req)
        {
            var list = _service.GetAmazonNationList(req);


            //Cache
            //$data = [
            //'list'       => $list,
            //'amazonKey'  => $this->amazonKey(),
            //'continents' => $this->getContinentsCache(),
            //'unit'       => $this->getUnitCache(),
            //'nation'     => $this->getNationCache(),
            //];
            return await Task.FromResult(Success(new
            {
                list,
                amazonKey = _amazonCache.List()!.ToDictionary(m => m.ID),
                continents = Enum<Continents>.ToDictionaryForUIByDescription(),
                unit = _unitCache.List()!.ToDictionary(m => m.Sign),
                nation = _nationCache.List()!.ToDictionary(m => m.Short)
            }, "获取成功"));
        }

        [Route("editAmazonNation")]
        [HttpPost]
        public async Task<ResultStruct> EditAmazonNation(EditAmazonNationDto req)
        {
            var res = await _service.AddOrUpdateAmazonNation(req);

            return res ? Success() : Error();
        }

        [Route("delAmazonNation")]
        [HttpPost]
        public async Task<ResultStruct> AmazonNationDel(IdDto req)
        {
            if (req.ID <= 0)
            {
                return Error(message: "缺少参数: ID");
            }

            var res = await _service.AmazonNationDel(req.ID);
            return res.State ? Success(message: "删除成功") : Error(message: res.Msg);
        }

        #endregion

        #region 平台设置

        [Route("getPlatformList")]
        [HttpPost]
        public async Task<ResultStruct> GetPlatFormList(CommonDto req)
        {
            var list = await _service.GetPlatformList(req);

            return Success(list, "获取成功");
        }

        [Route("editPlatform")]
        [HttpPost]
        public async Task<ResultStruct> EditPlatform(EditPlatformDto req)
        {
            var res = await _service.AddOrUpdatePlatform(req);

            return res.State ? Success(message: res.Msg) : Error(message: res.Msg);
        }

        [Route("delPlatform")]
        [HttpPost]
        public async Task<ResultStruct> DelPlatform(IdDto req)
        {
            if (req.ID <= 0)
            {
                return Error(message: "缺少参数: ID");
            }

            var res = await _service.PlatFormDel(req.ID);
            return res.State ? Success(message: "删除成功") : Error(message: res.Msg);
        }

        #endregion

        #region Excel模板

        [Route("exportSearch")]
        [HttpPost]
        public async Task<ResultStruct> ExportSearch()
        {
            //获取导出平台列表
            //$data['platform'] = Cache::instance()->ExportPlatform()->get();
            //$data['state'] = exportModel::STATE_TEXT;
            //$data['quality'] = exportModel::QUALITY_TEXT;
            //return success('', $data);


            return await Task.FromResult(Success(new
            {
                platform = _platformcache.List()!.ToDictionary(m => m.ID),
                quality = Enum<ExportModel.QualityEnum>.ToDictionaryForUIByDescription(),
                state = Enum<BaseModel.StateEnum>.ToDictionaryForUIByDescription()
            }));
        }

        [Route("getExportList")]
        [HttpPost]
        public async Task<ResultStruct> GetExportList(ExportListDto req)
        {
            var list = await _service.GetExportList(req);

            return Success(list, "获取成功");
        }

        [Route("editExport")]
        [HttpPost]
        public async Task<ResultStruct> EditExport(EditExportDto req)
        {
            var res = await _service.AddOrUpdateExport(req);

            return res.State ? Success(message: res.Msg) : Error(message: res.Msg);
        }

        [Route("exportDel")]
        [HttpPost]
        public async Task<ResultStruct> ExportDel(IdDto req)
        {
            if (req.ID <= 0)
            {
                return Error(message: "缺少参数: ID");
            }

            var res = await _service.ExportDel(req.ID);
            return res.State ? Success(message: "删除成功") : Error(message: res.Msg);
        }

        [HttpGet("allotExportProduct")]
        public async Task<ResultStruct> AllotExportProduct()
        {
            Request.TryGetValue("username", out var username);
            Request.TryGetValue("export_id", out var exportId);

            var list = await _service.GetAllotList(Int32.Parse(exportId), username);

            return Success(list, "获取成功");
        }

        [HttpPost("addAllotExportProduct")]
        public async Task<ResultStruct> AddAllotExportProduct(AddAllotExportProductDto req)
        {
           
            var res = await _service.AddAllotExportProduct(req);

            return res ? Success() : Error();
        }

        [HttpPost("delAllotExportProduct")]
        public async Task<ResultStruct> DelAllotExportProduct(IdDto req)
        {

            var res = await _service.DelAllotExportProduct(req.ID);
            return res.State ? Success(message: "删除成功") : Error(message: res.Msg);
        }

        #endregion

        #region 物流设置

        [Route("getLogisticsList")]
        [HttpPost]
        public async Task<ResultStruct> GetLogisticsList(CommonDto req,
            [FromServices] Services.Caches.Logistics logistics)
        {
            var list = await _service.GetLogisticsList(req, logistics);

            return Success(list, "获取成功");
        }

        //public async Task<ResultStruct> EditLogistics(Models.Setting.Logistics data)
        //{
        //    var res = await _service.AddOrUpdateLogistics(data);

        //    //事务操作
        //    //DB::beginTransaction();
        //    //try
        //    //{
        //    //    $res = $this->dictionaryService->updateLogistics($data);
        //    //    if ($info->state !== (int)$data['state']) {
        //    //        LogisticsParam::updateSystemLogisticsState($info->id, ['is_sys_state' => $data['state']]);
        //    //    }
        //    //    DB::commit();
        //    //}
        //    //catch (Exception $e) {
        //    //    DB::rollBack();
        //    //    return error('', $e->getMessage());
        //    //}

        //    return res.State ? Success(message: res.Msg) : Error(message: res.Msg);
        //}

        public async Task<ResultStruct> LogisticsDel()
        {
            if (!Request.TryGetValue("id", out var id))
            {
                return Error(message: "缺少ID");
            }

            var res = await _service.LogisticsDel(Convert.ToInt32(id));
            return res.State ? Success(message: "删除成功") : Error(message: res.Msg);
        }

        #endregion

        #region 邮箱设置

        [Route("getEmailConfigList")]
        [HttpPost]
        public async Task<ResultStruct> GetEmailConfigList(CommonDto req)
        {
            var list = await _service.GetEmailConfigList(req);

            return Success(list, "获取成功");
        }

        [Route("editEmailConfig")]
        [HttpPost]
        public async Task<ResultStruct> EditEmailConfig(EditEmailConfigDto req)
        {
            var res = await _service.AddOrUpdateEmailConfig(req);

            return res.State ? Success(message: res.Msg) : Error(message: res.Msg);
        }

        [Route("emailConfigDel")]
        [HttpPost]
        public async Task<ResultStruct> EmailConfigDel(IdDto req)
        {
            if (req.ID <= 0)
            {
                return Error(message: "缺少参数: ID");
            }

            var res = await _service.EmailConfigDel(req.ID);
            return res.State ? Success(message: "删除成功") : Error(message: res.Msg);
        }

        #endregion

        #region 亚马逊店铺Key

        [Route("getAmazonKeyList")]
        [HttpPost]
        public async Task<ResultStruct> GetAmazonKeyList(CommonDto req)
        {
            var list = await _service.GetAamazonKeyList(req);

            return Success(list, "获取成功");
        }

        [Route("editAmazonKey")]
        [HttpPost]
        public async Task<ResultStruct> EditAmazonKey(EditAmazonKeyDto req)
        {
            var res = await _service.AddOrUpdateAmazonKey(req);

            return res.State ? Success(message: res.Msg) : Error(message: res.Msg);
        }

        #endregion
    }
}