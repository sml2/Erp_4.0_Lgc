﻿using Microsoft.AspNetCore.Mvc;
using ERP.Services.Setting;
using ERP.Models.Setting;
using ERP.Extensions;

namespace ERP.Controllers.Setting
{
    [Route("api/[controller]")]
    public class LanguageUnitController : BaseController
    {
        private readonly LanguageUnitService _service;

        public LanguageUnitController(LanguageUnitService service)
        {
            _service = service;
        }


        public async Task<ResultStruct> GetAmazonKeyList()
        {
            Request.TryGetValue("language_id", out var LanguageId);
            Request.TryGetValue("unit_id", out var UnitId);

            //$user_id = $this->sessionData->getUserId();
            //$company_id = $this->sessionData->getCompanyId();
            //$oem_id = $this->sessionData->getOemId();


            var List = await _service.GetList(int.Parse(LanguageId), int.Parse(UnitId));

            //$data = [
            //'language' => arrayCombine($this->getLanguageCache(), 'id'),
            //'unit'     => arrayCombine($this->getUnitCache(), 'id'),
            //'list'     => $list
            //];

            Dictionary<string, Object> Result = new() { };

            Result.Add("language", new object() { });
            Result.Add("unit", new object() { });
            Result.Add("list", List);

            return Success(Result, "获取成功");
        }

        public async Task<ResultStruct> SubInfo(LanguageUnit Data)
        {

            //var Type = 0;
            //if ($this->sessionData->getUserId() === $this->super_id) {
            //$type = 1;
            //}
            //elseif($this->sessionData->getConcierge() === UserModel::TYPE_OWN) {
            //$type = 2;
            //} else
            //{
            //$type = 3;
            //}

            if(Data.ID > 0)
            {
                await _service.Update(Data);
            }
            else
            {
                var Res = await _service.ChcekLanage(Data.LanguageId);
                if (Res)
                {
                    return Error(message: "当前语言已绑定货币单位");
                }

                await _service.Add(Data);
            }
            // Cache
            // Cache::instance()->languageUnit()->del($type, $this->sessionData->getCompanyId(), $this->sessionData->getUserId());
            return Success();
        }

        public async Task<ResultStruct> DelInfo(LanguageUnit Data)
        {
            if (!Request.TryGetValue("id", out var ID))
            {
                return Error(message: "缺少ID");
            }

            //var Type = 0;
            //if ($this->sessionData->getUserId() === $this->super_id) {
            //$type = 1;
            //}
            //elseif($this->sessionData->getConcierge() === UserModel::TYPE_OWN) {
            //$type = 2;
            //} else
            //{
            //$type = 3;
            //}
            await _service.Delete(int.Parse(ID));
            // Cache
            // Cache::instance()->languageUnit()->del($type, $this->sessionData->getCompanyId(), $this->sessionData->getUserId());
            return Success();
        }
    }
}
 