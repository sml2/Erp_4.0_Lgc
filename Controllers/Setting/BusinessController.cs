﻿using ERP.Authorization.Rule;
using ERP.Data;
using ERP.Enums.View;
using ERP.Extensions;
using ERP.Models.DB.Users;
using ERP.Services.DB.Users;
using ERP.Services.Finance;
using ERP.Services.Message;
using ERP.Services.Statistics;
using Microsoft.AspNetCore.Identity;
using Ryu.Linq;
using BaseModel = ERP.Models.Abstract.BaseModel;
using CompanyService = ERP.Services.DB.Users.CompanyService;
using Role = ERP.Enums.Identity.Role;

namespace ERP.Controllers.Setting;
[Route("api/[controller]/[action]")]
[ApiController]
public class BusinessController : BaseController
{
    private readonly UserService _userService;
    private readonly CompanyService _companyService;
    private readonly UserManager<Models.DB.Identity.User> _userManager;

    public BusinessController(UserService userService, CompanyService companyService, UserManager<Models.DB.Identity.User> userManager)
    {
        _userService = userService;
        _companyService = companyService;
        _userManager = userManager;
        // _helpManualUrl = config('erp.helpManualUrl');
    }

    /// <summary>
    /// 业务统计权限
    /// </summary>
    private void ruleInfo()
    {
        // return $this->sessionData->getRule()->getSetting()->getBiter();
    }

    /// <summary>
    /// 获取权限信息
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> RuleInit()
    {
        var user = await _userManager.GetUserAsync(User);
        //权限

        var rule = new
        {
            person = true,
            current = true,
            user = true,
            group = true,
            company = true,
            resource = true,
        };
        if (!User.IsInRole(Role.ADMIN))
        {
            // $rule = [
            // 'person'   => $this->ruleInfo()->hasStatisticsPerson(),
            // 'current'  => $this->ruleInfo()->hasStatisticsCurrent(),
            // 'user'     => $this->ruleInfo()->hasStatisticsUser(),
            // 'group'    => $this->ruleInfo()->hasStatisticsGroup(),
            // 'company'  => $this->ruleInfo()->hasStatisticsCompany(),
            // 'resource' => $this->ruleInfo()->hasStatisticsResource()
            // ];
        }
        var data = new
        {
            rule,
            Type = user.Role,
            user.CompanyID,
        };
        return Success(data);
    }

    /// <summary>
    /// 获取个人业务数据
    /// </summary>
    /// <returns></returns>
    [Permission(Enums.Rule.Config.Setting.STATISTICS_PERSON)]
    public async Task<ResultStruct> Person([FromServices] CounterUserBusinessService counterUserBusiness)
    {
        var user = await _userManager.GetUserAsync(User);
        var userBusiness = false;
        var userCompany = await _companyService.GetInfoById(user.CompanyID);
        var isDistribution = userCompany.IsDistribution;
        if (isDistribution == false)
        {
            //查找是否存在下级上报id为当前公司
            var count = await _companyService.GetReportIdCompany(user.CompanyID);
            if (count > 0)
            {
                //全部统计
                userBusiness = true;
            }
        }

        //获取用户数据ids
        var info = await counterUserBusiness.GetCountUserById(user.Id, userBusiness);
        return Success(new { info, userBusiness });
    }

    /// <summary>
    /// 获取当前公司统计数据
    /// </summary>
    /// <returns></returns>
    [Permission(Enums.Rule.Config.Setting.STATISTICS_CURRENT)]
    public async Task<ResultStruct> Current([FromServices] CounterCompanyBusinessService counterCompanyBusinessService)
    {
        var user = await _userManager.GetUserAsync(User);
        if (User.IsInRole(Role.Employee) /*&& !$this->ruleInfo()->hasStatisticsCurrent()*/)
        {
            return Success(new { userBusiness = 0, companyBusiness = 0, business = 0, isView = false });
        }
        var userCompany = await _companyService.GetInfoById(user.CompanyID);
        var isDistribution = userCompany.IsDistribution;
        //分销只有公司内部统计
        var userBusiness = false;
        var companyBusiness = false;
        if (!isDistribution)
        {
            //查找是否存在下级上报id为当前公司
            var count = await _companyService.GetReportIdCompany(user.CompanyID);
            if (count > 0)
            {
                //全部统计
                userBusiness = true;
                companyBusiness = true;
            }
            else
            {
                //去分销上报统计
                userBusiness = true;
                companyBusiness = false;
            }
        }

        //查询公司统计信息
        var business = await counterCompanyBusinessService.GetCompany(user.CompanyID,userBusiness,companyBusiness);

        return Success(new { userBusiness, companyBusiness, business, isView = true });
    }

    /// <summary>
    /// 公司员工业务统计
    /// </summary>
    /// <param name="name"></param>
    /// <param name="state"></param>
    /// <param name="counterUserBusinessService"></param>
    /// <returns></returns>
    [Permission(Enums.Rule.Config.Setting.STATISTICS_USER)]
    public async Task<ResultStruct> GetUserList(string? name, BaseModel.StateEnum? state, [FromServices] CounterUserBusinessService counterUserBusinessService)
    {
        var user = await _userManager.GetUserAsync(User);
        // if (!User.IsInRole(Role.ADMIN) /*&& !$this->ruleInfo()->hasStatisticsUser()*/)
        // {
        //     return Error(message: "暂无权限获取数据");
        // }
        var res = await _userService.GetUserBusiness(name, state,user.CompanyID);
        var userBusiness = false;
        var userCompany = await _companyService.GetInfoById(user.CompanyID);
        var isDistribution = userCompany.IsDistribution;
        if (!isDistribution)
        {
            //查找是否存在下级上报id为当前公司
            var count = await _companyService.GetReportIdCompany(user.CompanyID);
            if (count > 0)
            {
                //全部统计
                userBusiness = true;
            }
        }
        //获取用户数据ids
        var list = await counterUserBusinessService.getCountUserByIds(res.Items.Select(u => u.Id).ToList(), userBusiness);
        return Success(new { user = res, data = list.KeyBy(m => m.UserId), userBusiness });
    }

    /// <summary>
    /// 公司用户组业务统计
    /// </summary>
    /// <param name="name"></param>
    /// <param name="state"></param>
    /// <param name="groupService"></param>
    /// <param name="counterGroupBusinessService"></param>
    /// <returns></returns>
    [Permission(Enums.Rule.Config.Setting.STATISTICS_GROUP)]
    public async Task<ResultStruct> GetGroupList(string? name, BaseModel.StateEnum? state, [FromServices] GroupService groupService
        , [FromServices] CounterGroupBusinessService counterGroupBusinessService)
    {
        var user = await _userManager.GetUserAsync(User);
        // if (!User.IsInRole(Role.ADMIN) /*&& !$this->ruleInfo()->hasStatisticsGroup()*/)
        // {
        //     return Error(message: "暂无权限获取数据");
        // }

        var res = await groupService.GetGroupBusiness(user.CompanyID, name, state);
        var userBusiness = false;
        var userCompany = await _companyService.GetInfoById(user.CompanyID);
        var isDistribution = userCompany.IsDistribution;
        if (!isDistribution)
        {
            //查找是否存在下级上报id为当前公司
            var count = await _companyService.GetReportIdCompany(user.CompanyID);
            if (count > 0)
            {
                //全部统计
                userBusiness = true;
            }
        }

        //获取用户数据ids
        var list = await counterGroupBusinessService.GetCountGroupByIds(res.Items.Select(i => i.Id).ToList(), userBusiness);
        return Success(new { group = res, data = list.KeyBy(l => l.GroupId), userBusiness });
    }

    /// <summary>
    /// 下级公司业务统计
    /// </summary>
    /// <param name="name"></param>
    /// <param name="state"></param>
    /// <param name="counterCompanyBusinessService"></param>
    /// <returns></returns>
    [Permission(Enums.Rule.Config.Setting.STATISTICS_COMPANY)]
    public async Task<ResultStruct> GetCompanyList(string? name, Company.States? state, [FromServices] CounterCompanyBusinessService counterCompanyBusinessService)
    {
        var user = await _userManager.GetUserAsync(User);
        // if (!User.IsInRole(Role.ADMIN) /*&& !$this->ruleInfo()->hasStatisticsCompany()*/)
        // {
        //     return Error(message: "暂无权限获取数据");
        // }

        var res = await _companyService.GetCompanyBusiness(user.CompanyID, state, name);
        //获取公司数据ids
        var list = await counterCompanyBusinessService.GetCountCompanyByIds(res.Items.Select(r => r.ID).ToList());
        return Success(new { company = res, data = list.KeyBy(l => l.CompanyId) });

    }

    /// <summary>
    /// 用户资源统计
    /// </summary>
    /// <param name="name"></param>
    /// <param name="state"></param>
    /// <param name="counterRequestService"></param>
    /// <returns></returns>
    public async Task<ResultStruct> GetResourceList(string? name, BaseModel.StateEnum? state, [FromServices] CounterRequestService counterRequestService)
    {
        var user = await _userManager.GetUserAsync(User);
        // if (!User.IsInRole(Role.ADMIN) /*&& !$this->ruleInfo()->hasStatisticsResource()*/)
        // {
        //     return Error(message: "暂无权限获取数据");
        // }

        var res = await _userService.GetUserBusiness(name, state);

        var list = await counterRequestService.GetCountRequestByUserIds(res.Items.Select(r => r.Id).ToList());
        return Success(new { user = res, data = list.KeyBy(l => l.UserID), Type = user.Role });
    }

    /// <summary>
    /// 存在注册推荐人列表
    /// </summary>
    /// <param name="username"></param>
    /// <returns></returns>
    [Permission(Enums.Rule.Config.Setting.STATISTICS_COMPANY)]
    public async Task<ResultStruct> GetReferrerList([FromQuery]string? username)
    {
        var currentUser = await _userManager.GetUserAsync(User);
        // if (!($this->sessionData->getUserId() === $this->super_id || $this->sessionData->getCompanyId() === UserModel::COMPANY_ID)) {
        //     return Error("暂无权限查看");
        // }

        if (!string.IsNullOrEmpty(username))
        {
            var user = await _userService.CheckUserName(username);
            if (user is null || !User.IsInRole(Role.ADMIN) /*|| $user['state'] === UserModel::STATE_DISABLE*/)
            {
                return Success(new { data = Array.Empty<int>(), total = 0 });
            }

            var company = await _companyService.GetCurrentConfig(user.CompanyID);
            if(company is null) return Success(new { data = Array.Empty<int>(), total = 0 });

            var data = new object[]
            {
                new
                {
                    user.CompanyID,
                    user.UserName,
                    user.TrueName,
                    agency = company.IsAgency,
                    company.Name,
                    company.Balance,
                    company.CreatIp,

                }
            };

            return Success(new { data, total = 1 });
        }
        var referrer = await _companyService.GetReferrer();

        var result = new List<object>();
        if (referrer.Items is { Count: > 0 })
        {
            var referrerIds = referrer.Items;
            //获取推荐公司信息
            var company = (await _companyService.GetList(referrerIds, Company.States.DISABLE)).KeyBy(c => c.ID);
            //查询独立用户
            var user = (await _userService.GetOwnUser(referrerIds)).KeyBy(u => u.CompanyID);

            foreach (var id in referrerIds)
            {
                result.Add(new
                {
                    company_id = id,
                    username = user[id].UserName,
                    truename = user[id].TrueName,
                    agency = company[id].IsAgency,
                    company_name = company[id].Name,
                    balance = company[id].Balance,
                    creat_ip = company[id].CreatIp,
                });
            }
        }

        return Success(new { referrer, result });
    }

    /// <summary>
    /// 推荐注册用户
    /// </summary>
    /// <param name="companyId"></param>
    /// <param name="payBillService"></param>
    /// <returns></returns>
    [Route("{companyId}")]
    public async Task<ResultStruct> GetExtendList(int companyId, [FromServices] PayBillService payBillService)
    {
        var currentUser = await _userManager.GetUserAsync(User);
        // if (!($this->sessionData->getUserId() === $this->super_id || $this->sessionData->getCompanyId() === UserModel::COMPANY_ID)) {
        //     return error('暂无权限查看');
        // }

        //获取推荐公司信息
        var returnArr = await _companyService.GetRefererAllList(companyId);
        if (returnArr.Items is { Count: <= 0 })
        {
            return Success(returnArr);
        }
        var companyIds = returnArr.Items.Select(c => c.ID).ToList();
        var company = returnArr.Items.KeyBy(c => c.ID);

        //查询独立用户
        var user = (await _userService.GetOwnUser(companyIds)).KeyBy(u => u.CompanyID);

        //获取指定公司充值金额
        var payList = (await payBillService.GetCompanyStatistics(companyIds)).KeyBy(l => l.CompanyId);
        var arr = new List<object>();
        foreach (var value in companyIds)
        {
            arr.Add(new
            {
                company_id = value,
                username = user[value].UserName,
                truename = user[value].TrueName,
                created_at = user[value].CreatedAt,
                company_name = company[value].Name,
                balance = company[value].Balance,
                creat_ip = company[value].CreatIp,
                total_amount = payList[value].StatisticsTotalAmount,
            });
        }

        return Success(new { returnArr, arr });
    }

    [HttpPost]
    public async Task<ResultStruct> Dashboard([FromServices] NoticeService notice, [FromServices] CounterCompanyBusinessService counterCompanyBusinessService
        , [FromServices] CounterUserBusinessService counterUserBusiness, [FromServices] DBContext dbContext)
    {
        var user = await _userManager.GetUserAsync(User);
        var company = await _companyService.GetCurrentConfig(user.CompanyID);
        if (company is null)
        {
            return Error("获取当前公司信息失败");
        }
        if (company.ExtendTip == Company.ExtendTips.TRUE)
        {
            company.ExtendTip = Company.ExtendTips.FALSE;
        }

        return Success(new
        {
            current = (await Current(counterCompanyBusinessService)).Data,
            person = (await Person(counterUserBusiness)).Data,
            noticeList = await notice.NoticeList(user.CompanyID, user.OEMID),
            oem = user.OEMID,
            company = new
            {
                truename = user.TrueName,
                balance = company?.Balance,
                company?.Integral,
                validity = company?.ShowValidityInfo,
                company?.Source,
                company?.Type,
                Concierge = user.Role == Role.Employee ? Concierges.Child : Concierges.Own,
                company?.SecondPackageOem,
                company?.ExtendTip
            },
            feature = GetFeature(user.OEMID, ""),
            novice = getNovice(user.OEMID, ""),
            rule = BalanceType.GetBalanceType(company!, user.OEMID, user.Level),
        });
    }

    private object getNovice(int oemId, string helpManualUrl)
    {
        // TODO 大量重复
        if (oemId == 10003)
        {
            return new
            {
                first = new { url = "", text = "新手上路" },
                tyro = new object[]
                {
                    new { url = "", text = "绑定店铺", image = "/home/store.jpeg" },
                    new { url = "", text = "上传产品", image = "/home/upload.png" },
                    new { url = "", text = "拉取订单", image = "/home/order.png" },
                    new { url = "", text = "采购物流", image = "/home/purchase.png" },
                    new { url = "", text = "财务核算", image = "/home/finance.png" },
                }
            };
        }
        return new
        {
            first = new { url = "", text = "新手上路" },
            tyro = new object[]
            {
                new { url = "", text = "绑定店铺", image = "/home/store.jpeg" },
                new { url = "", text = "上传产品", image = "/home/upload.png" },
                new { url = "", text = "拉取订单", image = "/home/order.png" },
                new { url = "", text = "采购物流", image = "/home/purchase.png" },
                new { url = "", text = "财务核算", image = "/home/finance.png" },
            }
        };
    }

    /// <summary>
    /// 获取积分用户特色功能
    /// </summary>
    /// <param name="oemId"></param>
    /// <param name="helpManualUrl"></param>
    /// <returns></returns>
    private object[] GetFeature(int oemId, string helpManualUrl)
    {
        // TODO 大量重复
        if (oemId == 1)
        {

            return new object[]
                {
                    new { url = string.Empty, text = "AI智能一键白底抠图", icon = "ai" },
                    new { url = string.Empty, text = "产品图一键去水印", icon = "cc-pic" },
                    new { url = string.Empty, text = "产品一键跟卖", icon = "on-sale" },
                    new { url = string.Empty, text = "找不到相关产品数据#1采集", icon = "file-error" },
                    new { url = string.Empty, text = "Aisn关键词反查", icon = "keyword1" },
                    new { url = string.Empty, text = "产品以图找货", icon = "praise" },
                    new { url = string.Empty, text = "产品批量刊登", icon = "hot" },
                    new { url = string.Empty, text = "浏览器插件/爬虫采集", icon = "explorer" },
                    new { url = string.Empty, text = "EAN/UPC码一键生成", icon = "barcode" },
                    new { url = string.Empty, text = "小语种一键翻译", icon = "translate1" },
                };
        }
        //嘉杰跨境帮助手册
        if (oemId == 10003)
        {
            return new object[]
            {
                new { url = string.Empty, text = "AI智能一键白底抠图", icon = "ai" },
                new { url = string.Empty, text = "产品图一键去水印", icon = "cc-pic" },
                new { url = string.Empty, text = "Aisn关键词反查", icon = "keyword1" },
                new { url = string.Empty, text = "产品以图找货", icon = "praise" },
                new { url = string.Empty, text = "产品批量刊登", icon = "hot" },
                new { url = string.Empty, text = "浏览器插件/爬虫采集", icon = "explorer" },
                new { url = string.Empty, text = "EAN/UPC码一键生成", icon = "barcode" },
                new { url = string.Empty, text = "小语种一键翻译", icon = "translate1" },
            };
        }

        return new object[]
            {
                new { url = string.Empty, text = "AI智能一键白底抠图", icon = "ai" },
                new { url = string.Empty, text = "产品图一键去水印", icon = "cc-pic" },
                new { url = string.Empty, text = "Aisn关键词反查", icon = "keyword1" },
                new { url = string.Empty, text = "产品以图找货", icon = "praise" },
                new { url = string.Empty, text = "产品批量刊登", icon = "hot" },
                new { url = string.Empty, text = "浏览器插件/爬虫采集", icon = "explorer" },
                new { url = string.Empty, text = "EAN/UPC码一键生成", icon = "barcode" },
                new { url = string.Empty, text = "小语种一键翻译", icon = "translate1" },
            };
    }



    class BalanceType
    {
        /// <summary>
        /// 积分块
        /// </summary>
        public bool integralState;
        /// <summary>
        /// 余额块
        /// </summary>
        public bool balanceState;
        /// <summary>
        /// 余额块->充值按钮
        /// </summary>
        public bool rechargeState;
        /// <summary>
        /// 推广按钮
        /// </summary>
        public bool seoState;
        /// <summary>
        /// 余额块->提现按钮
        /// </summary>
        public bool withdrawState;
        /// <summary>
        /// 积分块->购买套餐
        /// </summary>
        public bool packageState;
        /// <summary>
        /// 广告位
        /// </summary>
        public bool advertState;
        /// <summary>
        /// 特色功能
        /// </summary>
        public bool characteristicState;
        /// <summary>
        /// public广告文件下图片名称
        /// </summary>
        public string advertImg=string.Empty;

        /// <summary>
        /// 无需展示广告位的贴牌代理Oem
        /// </summary>
        private static readonly int[] HiddenAdvert = { 10003 };

        /// <summary>
        /// 无需展示特色功能的贴牌代理Oem
        /// </summary>
        private static readonly int[] Characteristic = { 72 };
        /// <summary>
        /// 首页展示位
        /// </summary>
        /// <param name="company"></param>
        /// <param name="oemId"></param>
        /// <param name="userLevel"></param>
        /// <returns></returns>
        public static BalanceType GetBalanceType(Company company, int oemId, int userLevel)
        {


            var resultData = new BalanceType
            {
                integralState = company.Source == Company.Sources.TWO,
                balanceState = true,
                rechargeState = true,
                seoState = true,
                withdrawState = true,
                packageState = true,
                advertState = !HiddenAdvert.Contains(oemId),
                characteristicState = !Characteristic.Contains(oemId),
                advertImg = $"https://www.miwaimao.com/img/main_07.png?t={DateTime.Now.GetTimeStamp()}",
            };
            if (company.ShowOemId != (int)Enums.ID.OEM.XiaoMi)
            {
                resultData.advertState = false;
                resultData.advertImg = "";
                resultData.withdrawState = false;
                resultData.seoState = false;
            }

            if (company.SecondPackageOem || (company.Source == Company.Sources.ONE && userLevel > 2))
            {
                resultData.rechargeState = false;
                resultData.seoState = false;
                resultData.withdrawState = false;
                resultData.packageState = false;
            }
            return resultData;
        }

    }
}
