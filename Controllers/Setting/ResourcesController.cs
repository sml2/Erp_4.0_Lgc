﻿using Microsoft.AspNetCore.Mvc;
using ERP.Services.Setting;
using ERP.Models.Setting;
using ERP.Extensions;
using ERP.Models.View.Setting.Resources;
using Microsoft.EntityFrameworkCore;

using ERP.Data;
namespace ERP.Controllers.Setting
{
    [Route("api/[controller]")]
    public class ResourcesController : BaseController
    {
        public readonly DBContext _dbContext;

        public ResourcesController(DBContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpPost("getOptions")]
        public async Task<ResultStruct> GetOptions(IndexDto req)
        {
            Object List;

            switch (req.Type)
            {
                case 1:
                    List = await _dbContext.DataBase.WhenWhere(req.Name != null, m => m.Name.Contains(req.Name!))
                        .Select(m => new {m.ID, m.Name}).ToListAsync();
                    break;
                case 2:
                    List = await _dbContext.Redis.WhenWhere(req.Name != null, m => m.Name.Contains(req.Name!))
                        .Select(m => new {m.ID, m.Name}).ToListAsync();
                    break;
                case 3:
                default:
                    List = await _dbContext.FileBucket.WhenWhere(req.Name != null, m => m.Name.Contains(req.Name!))
                        .Select(m => new {m.ID, m.Name}).ToListAsync();
                    break;
            }

            return Success(List, "获取成功");
        }

        [HttpPost("getList")]
        public async Task<ResultStruct> GetList(IndexDto req)
        {
            Object List;

            switch (req.Type)
            {
                case 1:
                    List = await _dbContext.DataBase.WhenWhere(req.Name != null, m => m.Name.Contains(req.Name!))
                        .ToPaginateAsync(limit: req.Limit, page: req.Limit);
                    break;
                case 2:
                    List = await _dbContext.Redis.WhenWhere(req.Name != null, m => m.Name.Contains(req.Name!))
                        .ToPaginateAsync(limit: req.Limit, page: req.Limit);
                    break;
                case 3:
                default:
                    List = await _dbContext.FileBucket.WhenWhere(req.Name != null, m => m.Name.Contains(req.Name!))
                        .Select(m => new FileBucketVM(m)).ToPaginateAsync(limit: req.Limit, page: req.Limit);
                    break;
            }

            return Success(List, "获取成功");
        }

        [HttpPost("editOss")]
        public async Task<ResultStruct> EditOss(FileBucketDto req)
        {
            FileBucket? info = new FileBucket();
            if (req.Id != null)
            {
                info = await _dbContext.FileBucket.FirstOrDefaultAsync(m => m.ID == req.Id);
                if (info != null)
                {
                    if (info.UpdatedAtTicks != req.UpdatedAtTicks)
                    {
                        return Error("数据不同步");
                    }
                }
                else
                {
                    return Error("找不到相关数据");
                }
            }

            info.Name = req.Name;
            info.Bucket = req.Bucket;
            info.AccessKey = req.AccessKey;
            info.SecretKey = req.SecretKey;
            info.EndpointDomain = req.EndpointDomain;
            info.BucketDomain = req.BucketDomain;

            if (req.Id != 0)
            {
                _dbContext.FileBucket.Update(info);
            }
            else
            {
                _dbContext.FileBucket.Add(info);
            }

            return await _dbContext.SaveChangesAsync() > 0 ? Success() : Error();
        }
    }
}