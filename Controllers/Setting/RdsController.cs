﻿using Microsoft.AspNetCore.Mvc;
using ERP.Services.Setting;
using ERP.Models.Setting;
using ERP.Extensions;

namespace ERP.Controllers.Setting
{
    [Route("api/[controller]")]
    public class RdsController : BaseController
    {
        private readonly RdsService _service;

        public RdsController(RdsService service)
        {
            _service = service;
        }

        public async Task<ResultStruct> List()
        {
            Request.TryGetValue("name", out var Name);

            var List = await _service.GetList(Name);

            return Success(List, "获取成功");
        }

        public async Task<ResultStruct> Edit(DataBase Data)
        {
            var Res = await _service.AddOrUpdate(Data);

            return Res.State ? Success(message: Res.Msg) : Error(message: Res.Msg);
        }
    }
}
