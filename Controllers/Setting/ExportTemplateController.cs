using ERP.Enums.Identity;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.Export;
using ERP.Models.View.Setting;
using ERP.Services.DB.Users;
using CacheCompany = ERP.Services.Caches.Company;
using Enumtxtention = ERP.Extensions.EnumExtension;
using PlatformData = ERP.Services.Caches.PlatformData;

namespace ERP.Controllers.Setting;

using static Models.Abstract.BaseModel;
using static Models.Setting.ExportModel;
public class ExportTemplateController : BaseController
{
    private readonly Services.Export.ExportProductService _service;
    private readonly UserService _UserService;
    private readonly PlatformData _PlatformData;
    private readonly CacheCompany _CacheCompany;
    private readonly ISessionProvider _ISessionProvider;
    public ExportTemplateController(Services.Export.ExportProductService service, PlatformData PlatformData, UserService UserService, CacheCompany CacheCompany, ISessionProvider sessionProvider)
    {
        _service = service;
        _PlatformData = PlatformData;
        _UserService = UserService;
        _CacheCompany = CacheCompany;
        _ISessionProvider = sessionProvider;
    }
    [HttpGet]
    public string Getstring()
    {
        return "111";
    }
    /// <summary>
    /// 导出模版列表
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> GetList(string? Name)
    {
        var exportData = await _service.GetExportProduct();
        List<int> exportIds = new List<int>();
        if (exportData.Count() > 0)
        {
            //获取当前公司被分配模版id
            exportIds = exportData.Select(m => m.ExportId).ToList();
        }

        var allocation = exportData.ToDictionary(m => m.ExportId, m => m);
        var export = await _service.getExportTemplate(exportIds, Name);
        List<object> resultData = new List<object>();
        for (var index = 0; index < export.Count; index++)
        {
            if (exportIds.Contains(export[index].ID))
            {
                export[index].State = allocation[export[index].ID].State;
            }else{
                export[index].State =StateEnum.Open;
            }
         
            resultData.Add(new
            {
                export[index].ID,
                export[index].Name,
                export[index].Remark,
                export[index].Quality,
                export[index].PlatformId,
                export[index].State,
                PlatformName= export[index].Platforms?.Name??"暂无"
            });
        }
        //权限
       object rule = new();
       if (_ISessionProvider.Session!.Is(Role.Employee))
       {
           rule = new { AllotExport = true };
           //'AllotExport' => $this->sessionData->getRule()->getSetting()->getBiter()->hasAllotExport(),
       }
       else
       {
           rule = new { AllotExport = true };
       }

       var data = new
        {
            State = Enum.GetValues<StateEnum>().Cast<StateEnum>().Where(x=>x!=0).ToDictionary(x => (int)x,x =>Enumtxtention.GetDescription(x)),
            Quality = Enum.GetValues<QualityEnum>().Cast<QualityEnum>().ToDictionary(x => (int)x,x =>Enumtxtention.GetDescription(x)),
            Export = resultData,
            rule= rule
        };
        return Success(data, "获取成功");
    }

    /// <summary>
    /// 更新关联信息
    /// </summary>
    /// <returns></returns>
   
    [HttpPost]
    public async Task<ResultStruct> UpdateState(ExportUp exportUp)
    {
        // 查询分配关联信息
        var info = await _service.GetExportProductInfo(exportUp.ID);
        if (info != null)
        {
            var resUp = await _service.UpdateState(info.ID, exportUp.state);
            return resUp ? Success() : Error();
        }
        // 不存在关联信息，新增关联信息
        var resAdd = await _service.AddExportProduct(exportUp);
        return resAdd ? Success() : Error();
    }

  
    /// <summary>
    /// 指定模版分配列表
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> AllotList([FromQuery]Models.View.Setting.allotLists allotList)
    {      
        var list = await _UserService.GetUserListPage(allotList.Page,allotList.Limit,allotList.Name, null, StateEnum.Open, false);
        if(list.Items.Count == 0)
             return Success();

        IEnumerable<int> companyIds= list.Items.Select(x=>x.CompanyId).ToList();
        var export =await _service.GetAllot(allotList.exportId, allotList.platformId, companyIds);

        // foreach (var item in list.Items)
        // {
        //     if(Export.Contains(item.CompanyId))
        //     {
        //         item.SetPropOrFieldValue("Allot",true);
        //     }
        //     else
        //     {
        //         item.SetPropOrFieldValue("Allot",false);
        //     }
        // }
        return Success(new {list,allot = export});
    }


   
    /// <summary>
    /// 分配下级
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> updateAllot(Models.View.Setting.updateAllotDto updateAllotDto)
    {     
        var company = _CacheCompany.Get(updateAllotDto.companyId);
        if (company!=null)
        {
            int OEMID = company.ShowOemId;
            if (updateAllotDto.type == 1)
            { // 分配添加
                var resAdd = await _service.AddAllot(updateAllotDto, OEMID);
                return resAdd.State ? Success(message: "分配成功") : Error();
            }else{
                //  移除分配
                var resDel = await _service.DelAllot(updateAllotDto.id, updateAllotDto.platformId, updateAllotDto.companyId, OEMID);
                return resDel.State ? Success(message: "移除成功") : Error();
            }
        }
        return Error();

    }
}

