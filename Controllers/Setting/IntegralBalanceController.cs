﻿using ERP.Enums.Identity;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.DB.Users;
using ERP.Models.Finance;
using ERP.Models.View.Setting;
using ERP.Services.DB.Users;
using ERP.Services.Finance;
using ERP.Services.IntegralBalance;
using System.ComponentModel.DataAnnotations;
using System.Text;
using ERP.Authorization.Rule;
using ERP.Models.View.Setting.Balance;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using BalanceBill = ERP.Models.IntegralBalance.BalanceBill;
using IntegralBill = ERP.Models.IntegralBalance.IntegralBill;
using UserManager = ERP.Services.Identity.UserManager;

namespace ERP.Controllers.Setting
{
    [Route("/api/setting/[controller]/[action]")]
    public class IntegralBalanceController : BaseController
    {
        private readonly CompanyService _companyService;
        private readonly IntegralBalanceService _integralBalanceService;
        private readonly PayBillService _payBillService;
        private readonly ISessionProvider _sessionProvider;
        private readonly UserManager _userManager;
        private readonly ILogger<IntegralBalanceController> _logger;

        public IntegralBalanceController(CompanyService companyService, ISessionProvider sessionProvider,
            UserManager userManager, IntegralBalanceService integralBalanceService, PayBillService payBillService,
            ILogger<IntegralBalanceController> logger)
        {
            _companyService = companyService;
            _sessionProvider = sessionProvider;
            _userManager = userManager;
            _integralBalanceService = integralBalanceService;
            _payBillService = payBillService;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ResultStruct> SubPayment(SubPaymentDto req)
        {
            var user = await _userManager.GetUserAsync(User);
            var result = await _payBillService.CreateBill(user, req, PayBillTypes.Balance, "余额充值");
            return result != null ? Success() : Error();
        }

        [HttpGet]
        public async Task Recharge([FromQuery] RechargeDto req)
        {
            try
            {
                var body = await _payBillService.Recharge(req);
                Response.ContentType = "text/html;charset=utf-8";
                await Response.Body.WriteAsync(Encoding.UTF8.GetBytes(body));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Response.ContentType = "text/plain;charset=utf-8";
                await Response.Body.WriteAsync(Encoding.UTF8.GetBytes(e.Message));
            }
        }

        [HttpPost]
        public async Task<string> Notify([FromForm] AlipayNotifyDto req)
        {
            var body = Request.Body;
            var reader = new StreamReader(body, Encoding.UTF8, true, 1024, true);
            _logger.LogInformation(await reader.ReadToEndAsync());
            try
            {
                await _payBillService.AlipayNotify(req);
                return "success";
            }
            catch (Exception e)
            {
                _logger.LogInformation($"订单号【{req.out_trade_no}】,回调结果：{e.Message}");
                Console.WriteLine(e);
                return "fail";
            }
            return "fail";
        }


        #region ERP3.0 PHP

        /// <summary>
        /// 初始化权限
        /// </summary>
        /// <returns></returns>
        public async Task<ResultStruct> Init()
        {
            var company = await _companyService.GetCurrentConfig();
            var user = await _userManager.GetUserAsync(User);
            var rule = GetIntegralBalance(company, user.Level);
            if (!User.IsInRole(Role.ADMIN))
            {
                if (rule.integralBill)
                    rule.integralBill = User.Has(Enums.Rule.Menu.Setting.IntegralBill);
                if (rule.balanceBill)
                    rule.balanceBill = User.Has(Enums.Rule.Menu.Setting.IntegralBill);
                if (rule.rechargeBill)
                    rule.rechargeBill = User.Has(Enums.Rule.Menu.Setting.IntegralBill);
            }

            return Success(new { rule, companyId = company.ID });
        }

        /// <summary>
        /// 
        /// </summary>
        private record RuleData
        {
            /// <summary>
            /// 
            /// </summary>
            /// <param name="secondPackageOem">贴牌推荐二代</param>
            /// <param name="onlyBalance">仅存在余额账单</param>
            /// <param name="integralBill">积分账单</param>
            /// <param name="balanceBill">余额账单</param>
            /// <param name="rechargeBill">充值账单</param>
            /// <param name="balanceRecharge">在线充值</param>
            /// <param name="integralTransform">余额转积分</param>
            public RuleData(bool secondPackageOem, bool onlyBalance, bool integralBill, bool balanceBill,
                bool rechargeBill, bool balanceRecharge, bool integralTransform)
            {
                this.secondPackageOem = secondPackageOem;
                this.onlyBalance = onlyBalance;
                this.integralBill = integralBill;
                this.balanceBill = balanceBill;
                this.rechargeBill = rechargeBill;
                this.balanceRecharge = balanceRecharge;
                this.integralTransform = integralTransform;
            }

            /// <summary>贴牌推荐二代</summary>
            public bool secondPackageOem { get; set; }

            /// <summary>仅存在余额账单</summary>
            public bool onlyBalance { get; set; }

            /// <summary>积分账单</summary>
            public bool integralBill { get; set; }

            /// <summary>余额账单</summary>
            public bool balanceBill { get; set; }

            /// <summary>充值账单</summary>
            public bool rechargeBill { get; set; }

            /// <summary>在线充值</summary>
            public bool balanceRecharge { get; set; }

            /// <summary>余额转积分</summary>
            public bool integralTransform { get; set; }
        }

        /// <summary>
        /// 获取公司原始积分余额权限
        /// </summary>
        /// <returns></returns>
        private RuleData GetIntegralBalance(Company company, int level)
        {
            var ruleData = new RuleData(
                secondPackageOem: false,
                onlyBalance: false,
                integralBill: true,
                balanceBill: true,
                rechargeBill: true,
                balanceRecharge: true,
                integralTransform: false
            );
            if (company.SecondPackageOem)
            {
                ruleData.secondPackageOem = true;
                ruleData.rechargeBill = false;
                ruleData.balanceRecharge = false;
                if (company.Type == Company.Types.TWO)
                {
                    ruleData.integralTransform = true;
                }
            }
            else if (company.Type == Company.Types.ONE)
            {
                ruleData.integralBill = false;
                if (company.OEMID != Constants.OemId && level > 2)
                {
                    ruleData.rechargeBill = false;
                    ruleData.balanceRecharge = false;
                    ruleData.onlyBalance = true;
                }
            }
            else if (company.Type == Company.Types.TWO)
            {
                ruleData.integralTransform = true;
            }

            return ruleData;
        }

        //    /******************************** 积分账单 ***************************************************/

        /// <summary>
        /// 积分账单列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResultStruct> IntegralBill([FromQuery] IntegralBill.Types? type,
            [FromQuery] List<DateTime>? datetime, [FromServices] IntegralBalanceService service)
        {
            var company = await _companyService.GetCurrentConfig();
            var user = await _userManager.GetUserAsync(User);
            var rule = GetIntegralBalance(company, user.Level);
            var integralTransform = rule.integralTransform;
            if (!User.IsInRole(Role.ADMIN))
            {
                if (integralTransform)
                    integralTransform = User.Has(Enums.Rule.Menu.Setting.IntegralTransform);
            }

            var list = await service.GetIntegralListPage(user, type, datetime);

            return Success(new
            {
                list,
                integralTransform,
                company = new
                {
                    balance = company.Balance,
                    company.Integral
                }
            });
        }

        public record SubTransformVm([Range(1, int.MaxValue)] decimal Balance);

        /// <summary>
        /// 余额转换积分
        /// </summary>
        /// <returns></returns>
        public async Task<ResultStruct> SubTransform(SubTransformVm vm, [FromServices] IntegralBalanceService service)
        {
            var user = await _userManager.GetUserAsync(User);
            if (user.Company.Source == Company.Sources.ONE)
            {
                return Error("当前账户无需积分功能");
            }

            var balance = MoneyFactory.Instance.Create(vm.Balance, "CNY");
            var company = await _companyService.GetCurrentConfig();
            if (company.Balance < balance)
            {
                return Error("当前余额不足");
            }

            var integral = Helpers.GetIntegralFromBalance(balance);

            // DB::beginTransaction();
            try
            {
                //更新公司余额、积分数据
                await _companyService.RechargeIntegral(integral, balance, company.ID);

                //增加余额扣款账单
                service.AddBalanceBill(balance, user.CompanyID, user.OEMID, user, company.Balance - balance,
                    Models.IntegralBalance.BalanceBill.Types.EXPENSE, balance + "CNY转换积分");

                //增加积分账单
                service.AddIntegralBill(integral, user, company.Integral + integral,
                    Models.IntegralBalance.IntegralBill.Types.RECHARGE, balance + "CNY转换积分");
                // DB::commit();
            }
            catch (Exception e)
            {
                // DB::rollBack();
                return Error(e.Message);
            }

            return Success();
        }

        /******************************** 余额账单 ***************************************************/
        public record otherBalanceBillDto(int id, BalanceBill.Types? type, string? byusername, string? username,
            DateTime[]? datetime = null);

        /// <summary>
        /// 余额账单列表
        /// </summary>
        [HttpGet]
        public async Task<ResultStruct> OtherBalanceBill([FromQuery] otherBalanceBillDto req,
            [FromServices] UserService userService,
            [FromServices] IntegralBalanceService service)
        {
            var company_id = req.id;
            var user = await _userManager.GetUserAsync(User);
            var company = await _companyService.GetCurrentConfig(company_id);
            var rule = GetIntegralBalance(company, user.Level);
            if (!User.IsInRole(Role.ADMIN) && rule.onlyBalance)
            {
                if (rule.integralTransform)
                    rule.integralTransform = User.Has(Enums.Rule.Menu.Setting.IntegralTransform);
                if (rule.balanceRecharge)
                    rule.balanceRecharge = User.Has(Enums.Rule.Menu.Setting.BalanceRecharge);
            }

            var list = new DbSetExtension.PaginateStruct<BalanceBillListDto>();
            var companyReturn = new
            {
                balance = company.BalanceMoney.To("CNY"),
                Integral = company.Integral,
                id = company.ID,
            };
            var typeArrMsg = Enum<Models.IntegralBalance.BalanceBill.PriceConfig>.ToDictionaryForUIByDescription();

            //筛查条件选择
            var typeArr = Enum<Models.IntegralBalance.BalanceBill.PriceConfig>.ToDictionaryForUI();

            int? operateUserId = null;
            int? byUserId = null;
            if (!string.IsNullOrEmpty(req.username))
            {
                var operateUser = await userService.CheckUserName(req.username);
                if (operateUser is null)
                    return Success(new { list, rule, typeArrMsg, company = companyReturn, typeArr });
                operateUserId = user.Id;
            }

            if (!string.IsNullOrEmpty(req.byusername))
            {
                var byUser = await userService.CheckUserName(req.byusername);
                if (byUser is null)
                    return Success(new { list, rule, typeArrMsg, company = companyReturn, typeArr });
                byUserId = user.Id;
            }

            list = await service.GetBalanceListPage(company_id, req.type, req.datetime, operateUserId, byUserId);

            return Success(new { list, rule, typeArrMsg, company = companyReturn, typeArr });
        }

        /// <summary>
        /// 余额账单列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Permission(Enums.Rule.Config.Setting.BALANCE_BILL)]
        public async Task<ResultStruct> balanceBill([FromQuery] BalanceBill.Types? type,
            [FromQuery] DateTime[]? datetime, [FromQuery] string? username, [FromQuery] string? byusername,
            [FromServices] UserService userService,
            [FromServices] IntegralBalanceService service)
        {
            // TODO 此处和上面的方法一模一样
            var user = await _userManager.GetUserAsync(User);
            var company = await _companyService.GetCurrentConfig();
            var rule = GetIntegralBalance(company, user.Level);
            if (!User.IsInRole(Role.ADMIN) && rule.onlyBalance)
            {
                if (rule.integralTransform)
                    rule.integralTransform = User.Has(Enums.Rule.Menu.Setting.IntegralTransform);
                if (rule.balanceRecharge)
                    rule.balanceRecharge = User.Has(Enums.Rule.Menu.Setting.BalanceRecharge);
            }

            var list = new DbSetExtension.PaginateStruct<BalanceBillListDto>();
            var companyReturn = new
            {
                balance = company.BalanceMoney.To("CNY"),
                Integral = company.Integral,
                id = company.ID,
            };
            var typeArrMsg = Enum<Models.IntegralBalance.BalanceBill.PriceConfig>.ToDictionaryForUIByDescription();

            //筛查条件选择
            var typeArr = Enum<Models.IntegralBalance.BalanceBill.PriceConfig>.ToDictionaryForUI();

            int? operateUserId = null;
            int? byUserId = null;
            if (!string.IsNullOrEmpty(username))
            {
                var operateUser = await userService.CheckUserName(username);
                if (operateUser is null)
                    return Success(new { list, rule, typeArrMsg, company = companyReturn, typeArr });
                operateUserId = user.Id;
            }

            if (!string.IsNullOrEmpty(byusername))
            {
                var byUser = await userService.CheckUserName(byusername);
                if (byUser is null)
                    return Success(new { list, rule, typeArrMsg, company = companyReturn, typeArr });
                byUserId = user.Id;
            }

            list = await service.GetBalanceListPage(company.ID, type, datetime, operateUserId, byUserId);

            return Success(new { list, rule, typeArrMsg, company = companyReturn, typeArr });
        }

        //    /**
        //     * 购买、升级套餐
        //     * @author CCY
        //     * @createdAt 2020/12/16
        //     * @param Request $request
        //     * @return JsonResponse
        //     */
        //    public function buyPackage(Request $request): JsonResponse
        //    {
        //        $type = $request->input('type');
        //        if (!$type) {
        //            return error('参数错误');
        //        }

        //        $company = $this->CompanyService->getCurrentConfig();

        //        $res = $this->getChangeData($company, $type);
        //        if ($res['money'] == 0) {
        //            return error('金额错误，请重试');
        //        }

        //        $money = FromConversion($res['money'], 'CNY');
        //        //计算当前套餐可抵扣金额后，需要支付金额
        //        if ($res['type'] == 2) {
        //            $day = (int)floor(($company['show_validity'] - $this->time) / 86400);//向下取整
        //            $deduction = FromConversion($day * $res['perPrice'][$company['type']], 'CNY');
        //            $money = $money > $deduction ? $money - $deduction : 0;
        //        }

        //        $balance = $company->getRawOriginal('balance');
        //        if ($balance < $money) {
        //            return error('余额不足，请先充值');
        //        }

        //        //更新公司信息
        //        $upadteCompany = [
        //            'show_validity' => $res['show_validity'],
        //            'is_open_user'  => companyModel::OPEN_USER_FALSE,
        //            'type'          => $type
        //        ];
        //        if ($res['type'] == 1) {
        //            $companyModel = new companyModel();
        //            $upadteCompany['balance'] = DB::raw($companyModel->transformKey('balance') . '-' . $money);
        //        }
        //        $res['rule_id'] != 0 && $upadteCompany['rule_id'] = $res['rule_id'];
        //        $res['open_user_num'] != 0 && $upadteCompany['open_user_num'] = $res['open_user_num'];

        //        DB::beginTransaction();
        //        try {

        //            $this->CompanyService->updateCompanyById($this->sessionData->getCompanyId(), $upadteCompany);
        //            //更新有效期信息
        //            $UserValidityService = new UserValidity();
        //            $UserValidityService->updateValidity($company['show_validity_id'], $res['show_validity']);
        //            //更新公司管理员绑定权限
        //            if ($res['rule_id'] != 0) {
        //                $userService = new userService();
        //                $userService->updateRule($res['rule_id'], $this->sessionData->getCompanyId());
        //            }

        //            //增加余额账单信息
        //            if ($money > 0) {
        //                $IntegralBalanceService = new IntegralBalanceService();
        //                $IntegralBalanceService->addBalanceBill($money, $balance - $money, BalanceBillModel::TYPE_EXPENSE, $res['remark'] . '花费：【' . $res['money'] . 'CNY】');


        //                //返现
        //                if ($company['referrer_id'] != 0) {
        //                    $cashback = $money * $company['discount'] / 100;
        //                    $recommendCompany = $this->CompanyService->getCurrentConfig($company['referrer_id']);
        //                    $reason = '推荐用户【'.subString($recommendCompany['company_name'], 3, '***') . '】购买：' . companyModel::TYPE_RANGE[$type] . '套餐，返现比例：' . $company['discount'] . '%，返现金额： ' . ToConversion($cashback, 'CNY') . 'CNY';
        //                    //增加返现余额账单
        //                    $IntegralBalanceService->addCashbackBalanceBill($cashback, $recommendCompany->getRawOriginal('balance') + $cashback, $recommendCompany, $company['id'], $reason);
        //                    //增加返现余额
        //                    $this->CompanyService->chargeAdd($company['referrer_id'], $cashback);
        //                }
        //            }

        //            //删除公司redis
        //            $this->delCompanyRedis($this->sessionData->getCompanyId());
        //            DB::commit();
        //        } catch (\Exception $e) {
        //            DB::rollBack();
        //            return error($e->getMessage());
        //        }
        //        return success();
        //    }

        //    /**
        //     * 获取续期、升级更改项
        //     * @param $company
        //     * @param $type
        //     * @return array
        //     *@author CCY
        //     * @createdAt 2020/12/16
        //     */
        //    private function getChangeData($company, $type): array
        //    {
        ////        $personMonth = 298;
        //        $personMonth = 598;
        //        $personYear = 1800;
        //        $team = 5800;
        //        $business = 10880;
        //        //单用户版升级团队版
        //        $personToTeam = 5800;
        //        //团队版升级企业版
        //        $teamToBusiness = 10880;
        //        //单用户版升级企业版
        //        $personToBusiness = 10880;
        //        //续费折扣
        //        $renewalDiscount = 0.8;

        //        //小米开放注册用户（个人版） 权限组id
        //        $rule_id_person = UserModel::RULE_ID_PERSON;
        //        //小米开放注册用户（团队版） 权限组id
        //        $rule_id_team = UserModel::RULE_ID_TEAM;
        //        //小米开放注册用户（企业版） 权限组id
        //        $rule_id_business = UserModel::RULE_ID_BUSINESS;

        //        $data = [
        //            'rule_id'       => 0,
        //            'open_user_num' => 0,
        //            'money'         => 0,
        //            'show_validity' => strtotime("+1 year"),
        //            'remark'        => '',
        //            'type'          => 1, //2可能抵扣
        //            'perPrice'      => [
        ////                3 => 9.93,//bcdiv(298 , 30,2),//向下取整,个人月付版  298 / 30 = 9.933
        //                3 => 19.93,//bcdiv(598 , 30,2),//向下取整,个人月付版  598 / 30 = 9.933
        //                4 => 4.93,//bcdiv(1800 , 365,2),//个人年付版  1800 / 365 =4.931
        //                5 => 15.89,//bcdiv(5800 , 365,2)//团队版 5800 / 365=15.89
        //            ]
        //        ];

        //        if ($company['type'] == 2) {
        //            switch ($type) {
        //                case 3:
        //                    $data['rule_id'] = $rule_id_person;
        //                    $data['money'] = $personMonth;
        //                    $data['show_validity'] = strtotime("+1 month");
        //                    $data['remark'] = '升级个人月付版';
        //                    break;
        //                case 4:
        //                    $data['rule_id'] = $rule_id_person;
        //                    $data['money'] = $personYear;
        //                    $data['remark'] = '升级个人年付版';
        //                    break;
        //                case 5:
        //                    $data['rule_id'] = $rule_id_team;
        //                    $data['money'] = $team;
        //                    $data['open_user_num'] = 10;
        //                    $data['remark'] = '升级团队版';
        //                    break;
        //                case 6:
        //                    $data['rule_id'] = $rule_id_business;
        //                    $data['money'] = $business;
        //                    $data['open_user_num'] = 25;
        //                    $data['remark'] = '升级企业版';
        //                    break;
        //            }
        //        } else if ($company['type'] == 3 || $company['type'] == 4) {
        //            switch ($type) {
        //                case 3:
        //                    $data['money'] = $personMonth;
        //                    $data['show_validity'] = strtotime("+1 month", $company['show_validity']);
        //                    $data['remark'] = '续期个人月付版';
        //                    break;
        //                case 4:
        //                    $data['money'] = $company['type'] == 3 ? $personYear : $personYear * $renewalDiscount;
        //                    $data['show_validity'] = strtotime("+1 year", $company['show_validity']);
        //                    $data['remark'] = $company['type'] == 3 ? '升级个人年付版' : '续期个人年付版';
        //                    break;
        //                case 5:
        //                    $data['rule_id'] = $rule_id_team;
        //                    $data['money'] = $personToTeam;
        //                    $data['open_user_num'] = 10;
        //                    $data['remark'] = '升级团队版';
        //                    $data['type'] = 2;
        //                    break;
        //                case 6:
        //                    $data['rule_id'] = $rule_id_business;
        //                    $data['money'] = $personToBusiness;
        //                    $data['open_user_num'] = 25;
        //                    $data['remark'] = '升级企业版';
        //                    $data['type'] = 2;
        //                    break;
        //            }
        //        } else if ($company['type'] == 5) {
        //            switch ($type) {
        //                case 5:
        //                    $data['money'] = $team * $renewalDiscount;
        //                    $data['show_validity'] = strtotime("+1 year", $company['show_validity']);
        //                    $data['remark'] = '续期团队版';
        //                    break;
        //                case 6:
        //                    $data['rule_id'] = $rule_id_business;
        //                    $data['money'] = $teamToBusiness;
        //                    $data['open_user_num'] = 25;
        //                    $data['remark'] = '升级企业版';
        //                    $data['type'] = 2;
        //                    break;
        //            }
        //        } else if ($company['type'] == 6) {
        //            switch ($type) {
        //                case 6:
        //                    $data['money'] = $business * $renewalDiscount;
        //                    $data['show_validity'] = strtotime("+1 year", $company['show_validity']);
        //                    $data['remark'] = '续期企业版';
        //                    break;
        //            }
        //        }
        //        return $data;
        //    }

        //    /**
        //     * 帮推荐用户购买套餐
        //     * @author CCY
        //     * @createdAt 2021/3/24
        //     * @param Request $request
        //     * @return JsonResponse
        //     */
        //    public function buyPackageForOther(Request $request): JsonResponse
        //    {
        //        $companyId = $request->input('id');
        //        $type = $request->input('packageId');
        //        $uid = $request->input('uid');

        //        if (!$companyId || !$type || !$uid) {
        //            return error('参数错误');
        //        }

        //        $company = $this->CompanyService->getCurrentConfig($companyId);
        //        $res = $this->getChangeData($company, $type);
        //        if ($res['money'] == 0) {
        //            return error('金额错误，请重试');
        //        }

        //        $money = FromConversion($res['money'], 'CNY');
        //        //计算当前套餐可抵扣金额后，需要支付金额
        //        if ($res['type'] == 2) {
        //            $day = (int)floor(($company['show_validity'] - $this->time) / 86400);//向下取整
        //            $deduction = FromConversion($day * $res['perPrice'][$company['type']], 'CNY');
        //            $money = $money > $deduction ? $money - $deduction : 0;
        //        }

        //        $currentCompany = $this->CompanyService->getCurrentConfig();
        //        if ($money > 0) {
        //            $money = $money - $money * $currentCompany['discount'] / 100;
        //        }
        //        $balance = $currentCompany->getRawOriginal('balance');
        //        if ($balance < $money) {
        //            return error('余额不足，请先充值');
        //        }


        //        //更新公司信息
        //        $upadteCompany = [
        //            'show_validity' => $res['show_validity'],
        //            'is_open_user'  => companyModel::OPEN_USER_FALSE,
        //            'type'          => $type
        //        ];
        //        $res['rule_id'] != 0 && $upadteCompany['rule_id'] = $res['rule_id'];
        //        $res['open_user_num'] != 0 && $upadteCompany['open_user_num'] = $res['open_user_num'];
        //        DB::beginTransaction();
        //        try {
        //            $this->CompanyService->updateCompanyById($companyId, $upadteCompany);
        //            //更新有效期信息
        //            $UserValidityService = new UserValidity();
        //            $UserValidityService->updateValidity($company['show_validity_id'], $res['show_validity']);
        //            //更新公司管理员绑定权限
        //            if ($res['rule_id'] != 0) {
        //                $userService = new userService();
        //                $userService->updateRule($res['rule_id'], $companyId);
        //            }
        //            //增加余额账单信息
        //            if ($money > 0) {
        //                $IntegralBalanceService = new IntegralBalanceService();
        //                $IntegralBalanceService->addBalanceBill($money, $balance - $money, BalanceBillModel::TYPE_EXPENSE, '为推荐用户【' . $company['company_name'] . '】' . $res['remark'] . '花费：【' . $res['money'] . 'CNY】');

        //                $this->CompanyService->charge($this->sessionData->getCompanyId(), $money);
        //            }

        //            //用户日志
        //            $UserOperateLogService = new UserOperateLog();
        //            $UserOperateLogService->addUserLog($uid, '更改用户套餐为' . companyModel::TYPE_RANGE[$type]);

        //            //删除公司redis
        //            $this->delCompanyRedis($companyId);
        //            DB::commit();
        //        } catch (\Exception $e) {
        //            DB::rollBack();
        //            return error($e->getMessage());
        //        }
        //        return success();
        //    }


        /******************************** 充值账单 ***************************************************/

        /// <summary>
        /// 充值账单列表
        /// </summary>
        /// <returns></returns>
        [Permission(Enums.Rule.Config.Setting.RECHARGE_BILL)]
        public async Task<ResultStruct> RechargeBill([FromQuery]BillListDto req, [FromServices] PayBillService payBillService)
        {
            var company = await _companyService.GetCurrentConfig();
            var companyId = company.ID;
            var oemId = company.OEMID;
            var user = await _userManager.GetUserAsync(User);
            var list = await payBillService.GetPaymentBillPage(req,companyId,oemId);
            var rule = GetIntegralBalance(company, user.Level);
            var balanceRecharge = rule.balanceRecharge;
            if (!User.IsInRole(Role.ADMIN))
            {
                if (balanceRecharge)
                    balanceRecharge = User.Has(Enums.Rule.Menu.Setting.BalanceRecharge);
            }

            return Success(new
            {
                list,
                rule = balanceRecharge,
                company = new
                {
                    Balance = company.BalanceMoney.To("CNY"),
                    
                    company.Integral, 
                }
            });
        }

        private  List<DateTime>? GetFindTime(int? timeType)
        {
            var now = DateTime.Now;
            List<DateTime> timeSpan = new List<DateTime>();
            if (timeType.HasValue)
            {
                switch (timeType)
                {
                    //本周
                    case 1:
                        var currentDayOfWeek1 = Convert.ToInt32(now.DayOfWeek.ToString("d"));
                        var start1 = now.AddDays(1 - ((currentDayOfWeek1 == 0) ? 7 : currentDayOfWeek1));
                        timeSpan.Add(start1);
                        timeSpan.Add(start1.AddDays(6));
                        break;
                    //上周
                    case 2:
                        var currentDayOfWeek2 = Convert.ToInt32(now.DayOfWeek.ToString("d"));
                         var start2 = now.AddDays(Convert.ToInt32 (1 - ((currentDayOfWeek2 == 0) ? 7 : currentDayOfWeek2)) - 7);
                         timeSpan.Add(start2);
                         timeSpan.Add(start2.AddDays(6));
                        break;
                    //本月
                    case 3:
                        var start3 = now.AddDays(1 - now.Day);
                        timeSpan.Add(start3);
                        timeSpan.Add(start3.AddMonths(1).AddDays(-1));
                        break;
                    //上月
                    case 4:
                        var start4 = DateTime.Parse(now.ToString("yyyy-MM-01")).AddMonths(-1);
                        timeSpan.Add(start4);
                        timeSpan.Add(start4.AddMonths(1).AddDays(-1));
                        break;
                }

                return timeSpan;
            }
        
            return null;
        }
        
        public async Task<ResultStruct> EffectiveRechargeBill([FromQuery] BillListDto req,
            [FromServices] PayBillService payBillService, [FromServices] Services.Caches.Company companyCache)
        {
            req.State = PayBillStates.Yes;
            int? oemId = null;
            int? companyId = null;
            if (!string.IsNullOrEmpty(req.Name))
            {
                var companyInfo = await _companyService.GetCompanyByName(req.Name);
                if (companyInfo is not null)
                {
                    oemId = companyInfo.OEMID;
                    companyId = companyInfo.ID;
                }
            }

            req.Timespan = GetFindTime(req.TimeType);
            var data = await payBillService.GetPaymentBillPage(req, companyId,oemId);
            var companyIds = data.Items.Select(i => i.CompanyID).ToArray();
            var company = companyCache.List()?.Count > 0
                ? companyCache.List()!.Where(c => companyIds.Contains(c.ID)).ToDictionary(x => x.ID, x => x)
                : null!;
            var sum = await payBillService.GetPaymentBillSum(req, companyId,oemId);
            return Success(new { data, company, sum = new Money(sum).To("CNY") });
        }


        //    /**
        //     * 增加余额充值账单记录
        //     * @author CCY
        //     * @createdAt 2020/11/24
        //     * @param Request $request
        //     * @return JsonResponse
        //     */
        //    public function subPayment(Request $request): JsonResponse
        //    {
        //        $info = $request->input('info');
        //        if (!$info) {
        //            return error('参数错误');
        //        }
        //        $PayBillService = new PayBillService();
        //        $res = $PayBillService->insertBill($info, PayBillModel::TYPE_TWO, '余额充值');
        //        return $res ? success() : error();
        //    }

        //    /**
        //     * 打开支付宝支付
        //     * @author CCY
        //     * @createdAt 2020/11/24
        //     * @param Request $request
        //     */
        //    public function recharge(Request $request)
        //    {
        //        $out_trade_no = $request->input('out_trade_no');
        //        $describe = $request->input('describe');
        //        $total_amount = $request->input('total_amount');

        //        require app_path('./Extension/alipayMd5/lib/alipay_submit.class.php');
        //        include(app_path('./Extension/alipayMd5/alipay.config.php'));

        //        $url = "http://" . $_SERVER['HTTP_HOST'];

        //        $OemAll  = $this->getOemAllCache();
        //        $oem = $OemAll[$this->sessionData->getOemId()];
        //        if (checkStr($oem['alipay'])) {
        //            $alipay = json_decode($oem['alipay'], true);
        //            $alipay_config['partner'] = $alipay['partner'];
        //            $alipay_config['key'] = $alipay['key'];
        //        }

        //        $parameter = array(
        //            'service'           => $alipay_config['service'],
        //            'partner'           => $alipay_config['partner'],
        //            'seller_id'         => $alipay_config['partner'],
        //            'payment_type'      => $alipay_config['payment_type'],
        //            'notify_url'        => $url . '/integralBalance/balanceAlipayNotifyUrl',
        //            'return_url'        => $url . '/api/integralBalance/balanceAlipayReturnUrl',
        //            'anti_phishing_key' => $alipay_config['anti_phishing_key'], // 选填
        //            'exter_invoke_ip'   => $alipay_config['exter_invoke_ip'],// 选填
        //            'out_trade_no'      => $out_trade_no,     // 订单编号
        //            'subject'           => $describe, // 订单商品
        //            'total_fee'         => $total_amount,      // 订单总额
        //            'body'              => $describe,      // 商品描述
        //            'show_url'          => '',             // 选填
        //            '_input_charset'    => trim(strtolower($alipay_config['input_charset']))
        //        );
        //        $alipaySubmit = new \AlipaySubmit($alipay_config);
        //        $html_text = $alipaySubmit->buildRequestForm($parameter, "get", "确认");
        //        echo $html_text;
        //        exit;
        //    }

        //    /**
        //     * 余额支付 支付宝-老版同步回调
        //     * @param Request $request
        //     * @return Application|Factory|View
        //     * @author CCY
        //     * @createdAt 2020/11/24
        //     */
        //    public function balanceAlipayReturnUrl(Request $request)
        //    {
        //        $data = $request->input();

        //        !isset($data['total_fee']) && $data['total_fee'] = 0;
        //        !isset($data['trade_no']) && $data['trade_no'] = 0;
        //        !isset($data['out_trade_no']) && $data['out_trade_no'] = 0;
        //        !isset($data['buyer_email']) && $data['buyer_email'] = 0;
        //        !isset($data['gmt_payment']) && $data['gmt_payment'] = 0;

        //        return view('alipayReturn', $data);
        ////
        ////        $name = $request->input('subject');
        ////        $total_fee = $request->input('total_fee');
        ////        $trade_no = $request->input('trade_no');
        ////        $out_trade_no = $request->input('out_trade_no');
        ////
        ////        echo '<div style="text-align: center;margin-top: 100px">订单充值完成<br>';
        ////        echo '如果账户充值未到账，请稍后再次查看！<br>';
        ////        echo '商品名称：' . $name . '<br>';
        ////        echo '支付金额：' . $total_fee . ' CNY<br>';
        ////        echo '订单号：' . $out_trade_no . '<br>';
        ////        echo '订单流水号：' . $trade_no . '<br></div>';
        ////        exit();
        //    }

        //    /**
        //     * 时间范围(app/Service/Order/AmazonOrder.php line568)
        //     * @author CCY
        //     * @createdAt 2020/7/13 14:18
        //     * @param $mod 1本周 2上周 3本月 4上月 5汇总 6自定义
        //     * @param $cus
        //     * @return array
        //     */
        //    private function getFindTime($mod, $cus): array
        //    {
        //        switch ($mod) {
        //            case 1:
        //                $start = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") - date("w") + 1, date("Y")));
        //                $end = date('Y-m-d', mktime(23, 59, 59, date("m"), date("d") - date("w") + 7, date("Y")));
        //                break;
        //            case 2:
        //                $start = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') - date('w') + 1 - 7, date('Y')));
        //                $end = date('Y-m-d', mktime(23, 59, 59, date('m'), date('d') - date('w') + 7 - 7, date('Y')));
        //                break;
        //            case 3:
        //                $start = date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y')));
        //                $end = date('Y-m-d', mktime(23, 59, 59, date('m'), date('t'), date('Y')));
        //                break;
        //            case 4:
        //                $start = date('Y-m-01', strtotime('-1 month'));
        //                $end = date('Y-m-t', strtotime('-1 month'));
        //                break;
        //            case 5:
        //                $start = null;
        //                $end = null;
        //                break;
        //            case 6:
        //                if (checkArr($cus)) {
        //                    $start = $cus[0];
        //                    $end = $cus[1];
        //                } else {
        //                    $start = '';
        //                    $end = '';
        //                }
        //                break;
        //        }
        //        return [$start, $end];
        //    }

        #endregion
    }
}