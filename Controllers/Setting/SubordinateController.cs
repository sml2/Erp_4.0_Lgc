using ERP.Extensions;
using ERP.Interface;
using ERP.Services.Caches;
using ERP.Services.DB.Users;
using ERP.ViewModels.Subordinate;

namespace ERP.Controllers.Setting;

public class SubordinateController : BaseController
{
    private readonly UserService _userService;
    private readonly Services.DB.Users.CompanyService _companyService;
    private readonly ILogger<SubordinateController> _logger;
    private readonly ISessionProvider _sessionProvider;
    private readonly OEM _oemCache;

    public SubordinateController(UserService userService, CompanyService companyService,
        ILogger<SubordinateController> logger, ISessionProvider isessionProvider, OEM oemCache)
    {
        _userService = userService;
        _companyService = companyService;
        _logger = logger;
        _sessionProvider = isessionProvider;
        _oemCache = oemCache;
    }
    [HttpPost]
    public async Task<ResultStruct> Init()
    {
        var oemList = _oemCache.List()?.ToDictionary(m => m.ID, m => m);
        return Success(oemList);
    }

    [HttpPost]
    public async Task<ResultStruct> Find(FindSubDto req,
        [FromServices] ERP.Services.DB.Statistics.Statistic statisticService)
    {
        try
        {
            var user = await _userService.GetAdminUser(req);
            var company = await _companyService.GetInfoById(user.CompanyID);
            var subCompanyIds = await _companyService.GetSubCompanyIds(new List<int>() { company.ID }, new List<int>());
            var adminUsers = await _userService.GetAdminUser(subCompanyIds, req.OemId);
            var oemList = _oemCache.List()?.ToDictionary(m => m.ID, m => m);

            if (_sessionProvider.Session.GetCompanyID() != Enums.ID.Company.XiaoMi.GetNumber())
            {
                oemList = null;
            }

            if (adminUsers.Count > 0)
            {
                var companies = await _companyService.GetListAll(subCompanyIds.ToArray());
                var counterCompany = await statisticService.GetCountCompanyById(subCompanyIds);

                var data = (from lis in adminUsers
                    join counter in counterCompany
                        on lis.CompanyId equals counter.AboutID
                    join companys in companies
                        on lis.CompanyId equals companys.ID into r1
                    from m in r1.DefaultIfEmpty()
                    select new
                    {
                        UserId = lis.ID,
                        OemId = lis.OemId,
                        UserNum = counter.UserCount + counter.EmployeeCount,
                        reportUserNum = counter.ReportUserCount + counter.ReportEmployeeCount,
                        ShowValidity = m.Validity,
                        IsDistribution = lis.IsDistribution,
                        state = m.State,
                        ShowValidityInfo = m.ShowValidityInfo,
                        duration = m.Validity.GetTimeStamp() - DateTime.Now.GetTimeStamp() > 604800
                            ? true
                            : false,
                        truename = lis.TrueName ?? "",
                        username = lis.UserName,
                        createdAt = lis.CreatedAt,
                        CompanyId = lis.CompanyId,
                        lis.PhoneNumberConfirmed
                    }).ToList();
                return Success(new { data, oemList });
            }

            return Success(message: "没有找到相关的下级用户信息");
        }
        catch (Exception e)
        {
            _logger.LogError(e, e.Message);
            return Error(e.Message);
        }
    }
}