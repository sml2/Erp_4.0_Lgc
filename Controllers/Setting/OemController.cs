﻿using ERP.Extensions;
using ERP.Models.Abstract;
using ERP.Models.View.Setting.Oem;
using ERP.Services;
using ERP.Services.Setting;
using ERP.ViewModels;
using Newtonsoft.Json;
using OEMModel = ERP.Models.Setting.OEM;
using CompanyOemService = ERP.Services.DB.Users.CompanyOem;

namespace ERP.Controllers.Setting;

using UserService = Services.DB.Users.UserService;

public class OemController : BaseController
{
    private readonly OemService _oemService;
    private readonly CompanyOemService _companyOemService;
    private readonly UserService _userService;

    public OemController(OemService oemService, CompanyOemService companyOemService, UserService userService)
    {
        _oemService = oemService;
        _companyOemService = companyOemService;
        _userService = userService;
    }

    [HttpGet]
    public async Task<ResultStruct> List([FromQuery] string? name, [FromQuery] int page = 1, [FromQuery] int limit = 20)
    {
        var list = await _oemService.List(name, page, limit);
        var loginPageSelection = Enum<OEMModel.LoginPageEnum>.ToDictionaryValueWithDescription();
        return Success(new {list,loginPageSelection}, "操作成功");
    }

    [HttpPost]
    public async Task<ResultStruct> Edit(OemDataDto oem)
    {
        int count = await _oemService.EditOem(oem);
        return Success();
    }

    [HttpPost]
    public async Task<ResultStruct> EditResources(OEMModel oem)
    {
        int count = await _oemService.EditResources(oem);
        if (count > 0)
        {
            return Success();
        }

        return Error();
    }

    /// <summary>
    /// 获取OEM下分配公司
    /// </summary>
    /// <param name="oem_id"></param>
    /// <param name="username"></param>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> GetValidList([FromQuery] int oem_id, [FromQuery] string? username,
        [FromQuery] int page, [FromQuery] int limit)
    {
        var list = await _companyOemService.GetList(oem_id, username);
        return Success(list);
    }


    /// <summary>
    /// 添加分配
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> AddAllot(AllotDto req)
    {
        //查询用户信息
        var info = await _userService.CheckUserName(req.Username);
        if (info.IsNull()) return Error("用户不存在");
        var companyOem = await _companyOemService.CheckExist(req.OemId, info!.CompanyID);
        if (companyOem.IsNotNull()) return Error("用户所属公司已关联！");

        int id = await _companyOemService.AddAllot(req.OemId, info);
        return id > 0 ? Success() : Error();
    }

    /// <summary>
    /// 取消分配
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> DelAllot(IdDto req)
    {
        var res = await _companyOemService.DeleteInfoById(req.ID);
        return res.State ? Success() : Error();
    }

    [HttpPost]
    public async Task<ResultStruct> Config()
    {
        //if (request()->isMethod('post'))
        //{
        //    $res = $this->oemService->useMode(request()->input('oemFileState'));
        //    return $res['state'] ? success($res['msg']) : error($res['msg']);
        //}
        //try
        //{
        //    $oemFileState = Config::get('oem.config.open', 0);
        //}
        //catch (\Throwable $e) {
        //    $oemFileState = 0;
        //}
        //$data = [
        //    'oemFileState' => $oemFileState
        //];
        return await Task.FromResult(Success());
    }

    [HttpPost]
    public async Task<ResultStruct> Info(IdDto req)
    {
        var info = await _oemService.Info(req.ID);

        return info != null ? Success(info) : Error();
    }

    [HttpPost]
    public async Task<ResultStruct> Resources()
    {
        var info = _oemService.Resources();

        return Success(info);
    }

    [HttpPost]
    public async Task<ResultStruct> GetDomains(IdDto req)
    {
        var res = await _oemService.GetDomains(req.ID);

        if (res == null || res.Length <= 0)
        {
            return Success();
        }

        var list = JsonConvert.DeserializeObject(res);

        return Success(list);
    }

    public async Task<ResultStruct> EditDomain(OemDomainDto req)
    {
        var res = await _oemService.EditDomain(req);

        return res ? Success() : Error();
    }
}