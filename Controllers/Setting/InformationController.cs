﻿using ERP.Services.Distribution;
using ERP.Services.DB.Users;
using ERP.Interface;
using ERP.Extensions;
using ERP.Models.Abstract;
using ProductServiece = ERP.Services.DB.Product.Product;
using CompanyCaChe = ERP.Services.Caches.Company;
using ERP.ViewModels;
using RuleTemplate = ERP.Services.DB.Users.RuleTemplate;
using static ERP.Services.DB.Users.RuleTemplate;
using static ERP.Models.DB.Users.Company;
using Ryu.Linq;
using System.Text.Json.Serialization;
using ERP.Enums.View;
using ERP.Models.View.Information;
using ERP.Services.Caches;
using ERP.Services.Identity;
using ERP.Services.Product;
using ERP.ViewModels.Information;
using Microsoft.AspNetCore.Identity;
using Company = ERP.Models.DB.Users.Company;
using Role = ERP.Enums.Identity.Role;

namespace ERP.Controllers.Setting
{
    public class InformationController : BaseController
    {
        private readonly Services.DB.Users.CompanyService _CompanyService;
        private readonly UserService _userService;
        private readonly AmazonOrderService _AmazonOrderService;
        private readonly ISessionProvider _ISessionProvider;
        private readonly CompanyCaChe _CompanyCaChe;
        private readonly OEM _oemCache;
        private readonly UserManager _userManager;

        public InformationController(Services.DB.Users.CompanyService CompanyService, UserService UserService,
            AmazonOrderService AmazonOrderService, ISessionProvider ISessionProvider, CompanyCaChe CompanyCaChe, OEM oemCache, UserManager userManager)
        {
            _CompanyService = CompanyService;
            _userService = UserService;
            _AmazonOrderService = AmazonOrderService;
            _ISessionProvider = ISessionProvider;
            _CompanyCaChe = CompanyCaChe;
            _oemCache = oemCache;
            _userManager = userManager;
        }

        [HttpGet]
        public async Task<ResultStruct> GetJunior([FromQuery] GetChildDataDto req,
            [FromServices] RuleTemplate ruleTemplate,
            [FromServices] ERP.Services.DB.Statistics.Statistic statisticService)
        {
            List<int> companyIds = new();
            List<Company> Company = new();
            int CompanyID = _ISessionProvider.Session!.GetCompanyID();

            if (req.CompanyId.HasValue)
            {
                CompanyID = Convert.ToInt32(req.CompanyId);
            }

            Company = await _CompanyService.GetCompanyValidity(req).ConfigureAwait(false);
            var ConpanyIds = Company.Select(x => x.ID).ToList();


            DbSetExtension.PaginateStruct<UserService.JuniorUser> Lists;
            
            //如果公司是米境通，搜索用户的时 改为全局;
            if (_ISessionProvider.Session.GetCompanyID() == Enums.ID.Company.XiaoMi.GetNumber() && req.name is not null)
            {
                Lists = await _userService.GetJuniorUserAll(Role.ADMIN, req.name,req.OemId);
            }
            else
            {
                Lists = await _userService.GetJuniorUser(CompanyID, Role.ADMIN, req.name, ConpanyIds.ToArray(),req.OemId);
            }



            var oemList = _oemCache.List()?.ToDictionary(m => m.ID,m => m);

            if (_ISessionProvider.Session.GetCompanyID() != Enums.ID.Company.XiaoMi.GetNumber())
            {
                oemList = null;
            }
            
            
            //object data = new object();
            if (Lists != null)
            {
                //List<Services.DB.User.CompanyService.CompanyValidityDto> ts = new();
                //List<Company> companies = new();
                companyIds = Lists.Items.Select(x => x.CompanyId).ToList();
                //foreach (var item in Lists.Items)
                //{
                //    companyIds.Add(item.CompanyId);
                //}
                if (Company.Count > 0)
                {
                    Company = await _CompanyService.GetListAll(companyIds.ToArray()).ConfigureAwait(false);
                }

                List<RuleListTemplate> Rules = new();
                Rules = await ruleTemplate.GetRuleList(CompanyID, -1).ConfigureAwait(false);
                // var CounterCompany = await counterCompanyBusiness.GetCountCompanyByIds(companyIds).ConfigureAwait(false);
                var CounterCompany = await statisticService.GetCountCompanyById(companyIds);

                if (Lists.Items.Count > 0)
                {
                    var data = (from lis in Lists.Items
                        join Counter in CounterCompany
                            on lis.CompanyId equals Counter.AboutID
                        join Companys in Company
                            on lis.CompanyId equals Companys.ID
                        join rule in Rules  on lis.RuleId equals rule.ID into r1
                        from rules in r1.DefaultIfEmpty()
                        select new
                        {
                            UserId = lis.ID,
                            OemId = lis.OemId,
                            UserNum =  Counter.UserCount + Counter.EmployeeCount,
                            reportUserNum = Counter.ReportUserCount + Counter.ReportEmployeeCount,
                            // totalUserNum = this.UserNum + reportUserNum,
                            rule = rules?.Name ?? "-",
                            ShowValidity = Companys.Validity,
                            IsDistribution = lis.IsDistribution,
                            state = Companys.State,
                            ShowValidityInfo = Companys.ShowValidityInfo,
                            duration = Companys.Validity.GetTimeStamp() - DateTime.Now.GetTimeStamp() > 604800
                                ? true
                                : false,
                            truename = lis.TrueName ?? "",
                            username = lis.UserName,
                            createdAt = lis.CreatedAt,
                            CompanyId = lis.CompanyId,
                            lis.PhoneNumberConfirmed,
                        }).ToList();

                    return Success(new { Total = Lists!.Total, data = data,oemList });
                }
            }

            return Success(message: "没有找到相关的下级用户信息");
        }

        /// <summary>
        /// 下级员工信息
        /// </summary>
        /// <param name="IdDto"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<object> GetUser([FromQuery] GetChildDataDto req)
        {
            int? CompanyId = req.CompanyId;
            if (CompanyId.HasValue)
            {
                var info = await _userService
                    .GetJuniorUser(Convert.ToInt32(CompanyId), Role.Employee, "", Array.Empty<int>(),null)
                    .ConfigureAwait(false);
                return Success(data: info);
            }

            return Error("无法找到公司,请重试");
        }

        /// <summary>
        /// 查看下级公司已审核未删除产品信息
        /// </summary>
        /// <param name="CompanyId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<object> GetProduct([FromQuery] GetChildDataDto req,
            [FromServices] ProductService _ProductServiece)
        {
            int? CompanyId = req.CompanyId;
            if (CompanyId.HasValue)
            {
                var Company = _CompanyCaChe.Get(CompanyId);
                if (Company is not null)
                {
                    var info = await _ProductServiece.GetJuniorProduct(Company.OEMID, Company.ID, req.Page, req.Limit);
                    return Success(data: info);
                }
            }

            return Error("请重新选择");
        }

        /// <summary>
        /// 查看下级公司订单信息
        /// </summary>
        /// <param name="CompanyId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<object> GetOrder([FromQuery] GetChildDataDto req,
            [FromServices] AmazonOrderService _AmazonOrderService)
        {
            int? CompanyId = req.CompanyId;

            if (CompanyId.HasValue)
            {
                var Company = _CompanyCaChe.Get(CompanyId);
                if (Company is not null)
                {
                    var info = await _AmazonOrderService.GetJuniorOrders(Convert.ToInt32(CompanyId), Company.OEMID)
                        .ConfigureAwait(false);
                    return Success(info);
                }
            }

            return Error("请重新选择");
        }

        /// <summary>
        /// 查询公司统计信息
        /// </summary>
        /// <param name="IdDto"></param>
        /// <param name="_CounterCompanyBusiness"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<object> GetBusiness([FromQuery] GetChildDataDto req,
            [FromServices] Services.Statistics.CounterCompanyBusinessService _CounterCompanyBusiness)
        {
            bool userBusiness,
                companyBusiness;
            int? CompanyId = req.CompanyId;

            if (CompanyId.HasValue)
            {
                var Company = _CompanyCaChe.Get(CompanyId);
                if (Company == null) return Error("选中为空,请重新选择");

                if (Company.IsDistribution)
                {
                    userBusiness = false;
                    companyBusiness = false;
                }
                else
                {
                    int Count = await _CompanyService.GetReportIdCompany(Convert.ToInt32(CompanyId))
                        .ConfigureAwait(false);
                    if (Count > 0)
                    {
                        userBusiness = true;
                        companyBusiness = true;
                    }
                    else
                    {
                        userBusiness = true;
                        companyBusiness = false;
                    }
                }

                var info = await _CounterCompanyBusiness
                    .GetCompany(Convert.ToInt32(CompanyId), userBusiness, companyBusiness).ConfigureAwait(false);

                object Data = new { userBusiness = userBusiness, companyBusiness = companyBusiness, business = info };
                return Success(Data);
            }

            return Error("请重新选择");
        }
    }
}