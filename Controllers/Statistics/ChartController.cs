﻿using ERP.Authorization.Rule;
using Microsoft.AspNetCore.Mvc;
using ERP.Services.Statistics;
using ERP.Models.Setting;
using ERP.Extensions;
using static ERP.Controllers.BaseController;

namespace ERP.Controllers.Statistics
{
    [Route("api/[controller]")]
    public class ChartController : BaseController
    {
        private readonly ChartService _service;

        public ChartController(ChartService service)
        {
            _service = service;
        }

        /// <summary>
        /// 折线图
        /// </summary>
        /// <returns></returns>
        [HttpGet("line/{totalName}")]
        [Permission(Enums.Rule.Config.Setting.EMAILCHART)]
        public async Task<ResultStruct> GetChartInfo(string totalName)
        {
            var list = await _service.GetChart(totalName);

            return Success(list);
        }
        /// <summary>
        /// 月活
        /// </summary>
        /// <returns></returns>
        [HttpGet("month/{totalName}")]
        public async Task<ResultStruct> GetMonthChart(string totalName)
        {
            return Success( await _service.GetMonthChart(totalName));
        }
    }


}
