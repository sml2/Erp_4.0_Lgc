﻿using ERP.Authorization.Rule;
using Microsoft.AspNetCore.Mvc;
using ERP.Services.Statistics;
using ERP.Models.Setting;
using ERP.Extensions;
using ERP.ViewModels.Statistics;
using static ERP.Controllers.BaseController;

namespace ERP.Controllers.Statistics
{
    [Route("api/[controller]")]
    public class TrendController : BaseController
    {
        private readonly TrendService _service;


        [HttpPost("init")]
        public async Task<ResultStruct> Init()
        {
            return await Task.FromResult(Success(new
            {
                account = false,
                emailChart = false,
                essentialChart = false,
                expenses = false,
                onlineUser = false
            }));
        }

        public TrendController(TrendService service)
        {
            _service = service;
        }

        /// <summary>
        /// 获取在线用户
        /// </summary>
        /// <returns></returns>
        [HttpGet("onlineUser/{name?}")]
        public async Task<ResultStruct> OnlineUser(string? name)
        {
            
            var list = await _service.GetOnlineUser(name);
            return Success(list);
        }

        [HttpPost("account")]
        [Permission(Enums.Rule.Config.Setting.ACCOUNT)]
        public async Task<ResultStruct> Account(AccountDto req)
        {
            var list = await _service.GetAccount(req);
            return Success(list);
        }

        [HttpPost("getLog")]
        public async Task<ResultStruct> GetAccountLog(AccountLogDto req)
        {
            var list = await _service.GetAccountLog(req);
            return Success(list);
        }

        /// <summary>
        /// /用户资源列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResultStruct> Expenses()
        {
            Request.TryGetValue("name", out var name);
            var list = await _service.GetExpenses(name);
            return Success(list);
        }

        [HttpPost("companyCount")]
        public async Task<ResultStruct> CompanyCount(CompanyCountDto req)
        {
            var list = await _service.CompanyCount(req);
            return Success(list);
        }

        [HttpPost("userCount")]
        public async Task<ResultStruct> UserCount(UserCountDto req)
        {
            var list = await _service.UserCount(req);
            return Success(list);
        }

        [HttpPost("companyList")]
        public async Task<ResultStruct> CompanyList()
        {
            var list = await _service.CompanyList();
            return Success(list);
        }
    }
}