﻿using Microsoft.AspNetCore.Mvc;
using ERP.Services.Statistics;
using ERP.Models.Setting;
using ERP.Extensions;
using static ERP.Controllers.BaseController;

namespace ERP.Controllers.Statistics
{
    [Route("api/[controller]")]
    public class CounterRequestController : BaseController
    {
        private readonly CounterRequestService _service;

        public CounterRequestController(CounterRequestService service)
        {
            _service = service;
        }

        /// <summary>
        /// 添加图片抠图调用计数
        /// </summary>
        /// <returns></returns>
        public async Task<ResultStruct> SubPicApiNum()
        {
            var res = await _service.PicApiNumAdd();
            return res ? Success() : Error();
        }

        /// <summary>
        /// 添加翻译调用计数
        /// </summary>
        /// <returns></returns>
        public async Task<ResultStruct> SubTranslateApiNum()
        {
            var res = await _service.TranslateNumAdd();
            return res ? Success() : Error();
        }
    }
}
