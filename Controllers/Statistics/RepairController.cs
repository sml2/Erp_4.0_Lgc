using ERP.Services.Statistics;
using ERP.ViewModels.Statistics;

namespace ERP.Controllers.Statistics;

[Route("api/[controller]")]
public class RepairController : BaseController
{
    private readonly RepairService _service;

    public RepairController(RepairService service)
    {
        _service = service;
    }

    [HttpPost("reset")]
    public async Task<ResultStruct> Reset(RepairDto req)
    {
        var result = await _service.Handle(req);

        return result ? Success() : Error();
    }
}