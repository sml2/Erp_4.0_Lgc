﻿using ERP.Models.DB.Operations;
using ERP.Services.DB.Operations;
using ViewModel = ERP.Models.View.Operations.TaskLog;

namespace ERP.Controllers.Operations;

using ERP.Services.Caches;
using ERP.Services.Identity;
using Extensions;
using Model = TaskLog;
using Service = OperationService;

//[Route("api/Tasklog")]
[ApiController]
public class TaskLogController : BaseController
{
    private readonly Service _Service;
    public TaskLogController(Service service, UserManager UserManager)
    {
        _Service = service;
    }
   

    [HttpPost]
    public async Task<ResultStruct> List(ViewModel.TaskLogVm taskLogVm)
    {
        var List=await _Service.GetTaskOperations(taskLogVm);
       var data= List.Transform(x => new
        {
            result = x.results.GetDescription(),
            x.Data,x.EndTime,x.ID,x.CreatedAt,
            x.BeginTime,x.Flag,x.UpdatedAt,
        });

        return Success(data);
    }
 
    [HttpPost]
    public async Task<ResultStruct> ChangeFlag(ViewModel.ChangeFlag changeFlag)
    {
        return await _Service.TaskChangeFlag(changeFlag) ? Success(true) : Error(false);
    }

    [HttpPost]
    public async Task<ResultStruct> GettaskList()
    {       
        return Success(TaskList.AllList.OrderBy(x=>x.ClassName).OrderBy(x=>x.LastTime).ToList());
    }
}
