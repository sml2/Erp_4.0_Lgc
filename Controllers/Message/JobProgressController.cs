﻿using ERP.Authorization.Rule;
using ERP.Extensions;
using ERP.Models.Message;
using ERP.Services.Identity;
using ERP.Services.Message;
using Microsoft.EntityFrameworkCore;

namespace ERP.Controllers.Message;

[Route("api/[controller]/")]
[ApiController]
public class JobProgressController : BaseController
{
    private readonly WorkOrderService _workOrderService;

    private readonly WorkOrderProgressService _workOrderProgressService;

    private readonly UserManager _userManager;

    public JobProgressController(WorkOrderService workOrderService, WorkOrderProgressService workOrderProgressService
        , UserManager userManager)
    {
        _workOrderService = workOrderService;
        _workOrderProgressService = workOrderProgressService;
        _userManager = userManager;
    }

    public record ProgressListQuery(WorkOrderProgress.States? State, string Name);

    /// <summary>
    /// 获取工单进度类型数据
    /// </summary>
    /// <param name="query"></param>
    /// <returns></returns>
    [HttpPost("Index")]
    public async Task<ResultStruct> ProgressList(ProgressListQuery query)
    {
        //        $field = ["id", "name", "company_id", "truename", "state", "created_at"];
        var user = await _userManager.GetUserAsync(User);
        var info = await _workOrderProgressService.GetWorkOrderProgressInfo(user.CompanyID, query.State, query.Name);
        return Success(new { info });
    }

    public record ProgressAddVm(string Name);

    /// <summary>
    /// 添加工单问题类型数据
    /// </summary>
    /// <param name="vm"></param>
    /// <returns></returns>
    [HttpPost("Add")]
    [Permission(Enums.Rule.Config.Message.JOB_PROGRESS_ADD)]

    public async Task<ResultStruct> ProgressAdd(ProgressAddVm vm)
    {
        //        if (!$this->rules()->hasJobProgressAdd()) {
        //            return error('无权访问');
        //        }
        if (string.IsNullOrEmpty(vm.Name.Trim()))
        {
            return Error("请填写问题类型名称");
        }

        // $field = ["id", "name", "state"];
        var infoCheck = await _workOrderProgressService.GetWorkOrderProgressFirst(name: vm.Name.Trim());

        if (infoCheck is not null)
        {
            return Error("名称已存在，请重新编辑");
        }

        var user = await _userManager.GetUserAsync(User);
        var model = new WorkOrderProgress(vm.Name, user);

        await _workOrderProgressService.ProgressInsert(model);
        return Success("添加成功");
    }

    [HttpGet("{id:int}")]
    public async Task<ResultStruct> Info(int id)
    {
        // $field = ["id", "name", "state"];
        var data = await _workOrderProgressService.GetWorkOrderProgressFirst(id);
        return Success(data);
    }

    public record ProgressEditVm(string Name, WorkOrderProgress.States State);

    [HttpPost("Edit/{id:int}")]
    [Permission(Enums.Rule.Config.Message.JOB_PROGRESS_EDIT)]
    public async Task<ResultStruct> ProgressEdit(int id, ProgressEditVm vm)
    {
        //        if (!$this->rules()->hasJobProgressEdit()) {
        //            return error('无权访问');
        //        }

        //先排除不等于自己，再查是否重复
        // $field = ["id", "name"];
        var infoSelf = await _workOrderProgressService.GetWorkOrderProgressFirst(id);
        if (infoSelf is null)
        {
            return Error("已存在，请重新编辑");
        }
        if (infoSelf!.Name != vm.Name.Trim())
        {
            var infoCheck = await _workOrderProgressService.GetWorkOrderProgressFirst(name: vm.Name);
            if (infoCheck is not null)
            {
                return Error("名称重复，请重新编辑");
            }
        }

        //如果包含禁用状态
        if (vm.State == WorkOrderProgress.States.DISABLE)
        {
            var count = await _workOrderService.GetWorkOrderInfo(progressId: id)
                .Where(w => w.State == WorkOrder.States.ToBeProcessed || w.State == WorkOrder.States.Processing)
                .CountAsync();
            if (count > 0)
            {
                return Error("该选项正在被使用，无法禁用，请完成待处理工单后再重试！");
            }
        }

        infoSelf.Name = vm.Name;
        infoSelf.State = vm.State;
        await _workOrderProgressService.ProgressUpdate(infoSelf);
        return Success("更新成功");
    }

    [HttpGet("Del/{id:int}")]
    [Permission(Enums.Rule.Config.Message.JOB_PROGRESS_DEL)]
    public async Task<ResultStruct> ProgressDel(int id)
    {
        //        if (!$this->rules()->hasJobProgressDel()) {
        //            return error('无权访问');
        //        }

        var count = await _workOrderService.GetWorkOrderInfo(progressId: id)
            .Where(w => w.State == WorkOrder.States.ToBeProcessed || w.State == WorkOrder.States.Processing)
            .CountAsync();
        if (count > 0)
        {
            return Error("该选项正在被使用，请完成待处理工单后再重试！");
        }

        await _workOrderProgressService.ProgressDel(id);
        return Success("删除成功");
    }

    [HttpGet("[action]")]
    public ResultStruct GetJobProgressRule()
    {
        //        $rule = $this->sessionData->getRule()->getMessage()->getBiter();
        //        if($this->sessionData->getConcierge() == UserModel::TYPE_OWN){
        var rule = new object();
        rule = new
        {
            JobProgressAdd = true,
            JobProgressEdit = true,
            JobProgressDel = true,
        };
        if (!User.IsInRole(Enums.Identity.Role.ADMIN))
        {
            //        }else{
            //            $data['rule'] = [
            //                'JobProgressAdd'          => $rule->hasJobProgressAdd(),
            //                'JobProgressEdit'         => $rule->hasJobProgressEdit(),
            //                'JobProgressDel'          => $rule->hasJobProgressDel(),
            //            ];
            //        }
        }
        return Success(new { rule });
    }

    //    /**
    //     * 获取权限
    //     *
    //     * @return \App\Rule\BitwiseFlags\Message
    //     */
    private object rules()
    {
        //        return $this->sessionData->getRule()->getMessage()->getBiter();
        return 1;
    } //end rules()
}