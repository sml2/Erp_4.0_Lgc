﻿using ERP.Extensions;
using ERP.Models.Message;
using ERP.Services.Identity;
using ERP.Enums.Message;
using Microsoft.AspNetCore.Identity;
using ERP.Interface;
using ERP.ViewModels;
using static ERP.Models.View.Message.NoticeViewModel;
using static ERP.Models.View.Message.NoticeViewModel.IndexQuery;
using ERP.Authorization.Rule;
using ERP.Enums.Identity;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ERP.Controllers.Message;

[Route("api/[controller]/[action]")]
[ApiController]
public class NoticeController : BaseController
{
    private readonly Services.Message.NoticeService _noticeService;
    private readonly UserManager<Models.DB.Identity.User> _userManager;

    public  NoticeController(Services.Message.NoticeService noticeService, UserManager<Models.DB.Identity.User> userManager)
    {
        _noticeService = noticeService;
        _userManager = userManager;
    }

    /// <summary>
    /// 消息中心 公告列表
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> Index(IndexQuery query)
    {
        var user = await _userManager.GetUserAsync(User);

        var list = await _noticeService.info(user.Id, user.CompanyID, query.State, title: query.Title)
            .OrderByDescending(t => t.CreatedAt)
            .ToPaginateAsync(page: query.Page, limit: query.Limit);
      var data=list.Transform(x => new
        {
            x.ID,
            x.Title,
            x.NoticeBegin,
            x.NoticeEnd,
            x.State,
            x.Type,
            x.TrueName,
            x.CreatedAt,
            x.Content,
        });
        return Success(new { data });
    }

    /// <summary>
    /// 添加公告
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> Add(AddVm vm)
    {
        if (string.IsNullOrEmpty(vm.Info.content.Trim()))
        {
            return Error("内容不可为空");
        }

        DateTime? noticeBegin = null, noticeEnd = null;
        if (vm.Info.datetimerange is { Length: 2 })
        {
            noticeBegin = Convert.ToDateTime(vm.Info.datetimerange[0]);
            noticeEnd = Convert.ToDateTime(vm.Info.datetimerange[1]);
        }

        var user = await _userManager.GetUserAsync(User);
        if (vm.id is null)
        {
            // Tips 添加操作
            // if (!$this->rules()->hasNoticeAdd() && $this->sessionData->getConcierge() == UserModel::TYPE_CHILD) {
            //     return error('无权访问');
            // }
            
            var model = new Notice(vm.Info.title, vm.Info.content, vm.Info.state, vm.Info.type, vm.Info.size, noticeBegin, noticeEnd, user);
            await _noticeService.Insert(model);
            return Success();
        }
        else
        {
            // Tips 修改操作
            // if (!$this->rules()->hasNoticeEdit() && $this->sessionData->getConcierge() == UserModel::TYPE_CHILD) {
            //     return error('无权访问');
            // }
          var model = await _noticeService.Info(vm.id.Value,user);
            if (model is null) return Error(message:"无法找到要修改的公告");
            model.Title = vm.Info.title;
            model.Content = vm.Info.content;
            model.Type = vm.Info.type;
            model.State = vm.Info.state;
            model.Size = vm.Info.size;
            model.NoticeBegin = noticeBegin;
            model.NoticeEnd = noticeEnd;

            await _noticeService.UpdateData(model);
        }

        return Success();
    }

    /// <summary>
    /// 公告预览
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> Info(int id)
    {
        var user = await _userManager.GetUserAsync(User);
        var infos = await _noticeService.Info(id, user);
        if (infos is not null)
        {
            var info = new
            {
                infos.ID,
                infos.Title,
                infos.Content,
                infos.NoticeBegin,
                infos.NoticeEnd,
                infos.State,
                infos.Size
            };
            return Success(info);
        };
        return Error("读取公告异常！");
    }
    [HttpGet]
    public Task<ResultStruct> LoginRedirectNoticeInfoShow(int id)
    {
        return Info(id);
    }


    /// <summary>
    /// 首页的公告列表
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> NoticeList()
    {
        var user = await _userManager.GetUserAsync(User);

        var list = await _noticeService.NoticeList(user.CompanyID, user.OEMID);

        return Success(list);
    }

    /// <summary>
    /// 登录公告弹窗列表
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> LoginNoticeList()
    {
        var user = await _userManager.GetUserAsync(User);

        var list = await _noticeService.NoticeList(user.CompanyID, user.OEMID, true);

        return Success(list);
    }
    [HttpPost]
    public async Task<ResultStruct> Del(IdDto IdDto)
    {
        //        if (!$this->rules()->hasNoticeDel() && $this->sessionData->getConcierge() == UserModel::TYPE_CHILD) {
        //            return error('无权访问');
        //        }
        await _noticeService.Delete(IdDto.ID);
        return Success();
    }

    public async Task<ResultStruct> GetNoticeRule()
    {
        
        var user =await _userManager.GetUserAsync(User);
        //权限
        var rule = new 
            {
                NoticeAdd = true,
                NoticePreview = true,
                NoticeEdit = true,
                NoticeDel = true,
                NoticeAll = false,
            };
        if (User.IsInRole(Role.SU)||user.Company.ShowOemId ==(int) Enums.ID.OEM.XiaoMi|| User.IsInRole(Role.DEV))
        {
            rule = new
            {
                NoticeAdd = true,
                NoticePreview = true,
                NoticeEdit = true,
                NoticeDel = true,
                NoticeAll = true,
            };
        }
        return await Task.FromResult(Success(new { rule }));
    }

    /// <summary>
    /// 获取权限
    /// </summary>
    /// <returns></returns>
    private ResultStruct rules()
    {
        //        return $this->sessionData->getRule()->getMessage()->getBiter();
        return Success();
    } //end rules()
}