﻿using System.Linq.Expressions;
using ERP.Authorization.Rule;
using ERP.Extensions;
using ERP.Models.DB.Users;
using ERP.Models.Message;
using ERP.Services.DB.Users;
using ERP.Services.Identity;
using ERP.Services.Message;
using ERP.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace ERP.Controllers.Message;

[Route("api/[controller]/")]
[ApiController]
public class JobPendingController : BaseController
{
    private readonly WorkOrderService _workOrderService;

    private readonly WorkOrderReplyService _workOrderReplyService;

    private readonly WorkOrderCategoryService _workOrderCategoryService;

    private readonly WorkOrderProgressService _workOrderProgressService;
    private readonly UserOnlineService _userOnlineService;

    private readonly UserManager _userManager;

    public JobPendingController(WorkOrderService workOrderService, WorkOrderReplyService workOrderReplyService,
        WorkOrderCategoryService workOrderCategoryService
        , WorkOrderProgressService workOrderProgressService, UserManager userManager, UserOnlineService userOnlineService)
    {
        _workOrderService = workOrderService;
        _workOrderReplyService = workOrderReplyService;
        _workOrderCategoryService = workOrderCategoryService;
        _workOrderProgressService = workOrderProgressService;
        _workOrderProgressService = workOrderProgressService;
        _userManager = userManager;
        _userOnlineService = userOnlineService;
    }


    public record PendingListQuery(string? Title, WorkOrder.States? State, Enums.Message.Commits? Commit);

    /// <summary>
    /// 待处理工单列表
    /// </summary>
    /// <returns></returns>
    [HttpPost("Index")]
    public async Task<ResultStruct> PendingList(PendingListQuery query)
    {
        //todo 此处不要加权限,====>注意:轮询工单
        var user = await _userManager.GetUserAsync(User);
        Expression<Func<WorkOrder, bool>> stateQuery = query.State.HasValue
            ? w => w.State == query.State
            : order => order.State < WorkOrder.States.Complete;
        var list = await _workOrderService.GetWorkOrderInfo(null, user.CompanyID, query.Title, commit: query.Commit)
            .Where(stateQuery)
            .Select(x => new { x.Title, x.State, x.Category, x.Progress, x.SendNum, x.Commit, x.DeadLine, x.CreatedAt, x.ID, x.UpdatedAt })
            .OrderByDescending(w => w.CreatedAt)
            .ThenBy(w => w.State)
            .ToPaginateAsync();
        var categoryGroup =
            await _workOrderCategoryService.GetAll(user.CompanyID, WorkOrderCategory.States.ENABLE);
        var progressGroup =
            await _workOrderProgressService.GetAll(user.CompanyID, WorkOrderProgress.States.ENABLE);

        await _userOnlineService.UpdateOnline(await _userManager.GetUserAsync(User));

        return Success(new { list, categoryGroup, progressGroup });
    }

    [HttpGet("Index")]
    public async Task<ResultStruct> MessagePendingList()
    {
        var user = await _userManager.GetUserAsync(User);
        var list = await _workOrderService.GetWorkOrderInfo(companyId: user.CompanyID)
            .Where(w => w.State == WorkOrder.States.ToBeProcessed || w.State == WorkOrder.States.Processing)
            .Select(w => new { w.ID, w.Title, w.UserID, w.State })
            .Take(5)
            .ToListAsync();
        return Success(list, "2.0.2");
    }

    public record WorkOrderProcessVm(int progress_id, bool IsComplete, string Addcontent, int user_id);

    /// <summary>
    /// 订单处理 get获取回填数据 post修改数据
    /// </summary>
    /// <param name="id">工单干id</param>
    /// <param name="vm"></param>
    /// <returns></returns>
    [HttpGet("Process/{id:int}")]
    [Permission(Enums.Rule.Config.Message.JOB_PENDING_PROCESSING|Enums.Rule.Config.Message.JOB_MY_VIEW)]
    public Task<ResultStruct> WorkOrderProcess(int id) => WorkOrderProcess(id, null);
    [HttpPost("Process/{id:int}")]
    public async Task<ResultStruct> WorkOrderProcess(int id, WorkOrderProcessVm? vm)
    {
        //        if (!$this->rules()->hasJobMyView() || !$this->rules()->hasJobPendingProcessing()) {
        //            return error('无权访问');
        //        }
        var user = await _userManager.GetUserAsync(User);
        if (Request.Method == "POST")
        {
            //待处理订单 处理 按钮 提交操作
            var model = new WorkOrder() { ID = id, ProgressId = vm!.progress_id, State = WorkOrder.States.Processing };
            if (vm.IsComplete)
                model.State = WorkOrder.States.Complete;
            //添加回复信息
            var reply = new WorkOrderReply(id, vm.Addcontent, user);
            if (user.Id != vm.user_id)
            {
                //                $updateData['send_num'] = DB::raw($this->WorkOrderModel->transformKey('send_num').'+1');
                model.SendPopup = true;
            }
            else
            {
                //                $updateData['receive_num'] = DB::raw($this->WorkOrderModel->transformKey('receive_num').'+1');
                model.ReceivePopup = true;
            }

            await _workOrderService.Update(model);
            await _workOrderReplyService.Insert(reply);
            return Success("提交成功");
        }

        var listMate = await _workOrderService.GetWorkOrderInfo(id).FirstOrDefaultAsync();
        var list = new object();
        if (listMate != null)
        {
            list = new
            {
                listMate.Title,
                listMate.CategoryId,
                listMate.ProgressId,
                listMate.DeadLine,
                listMate.Content,
                listMate.CreatedAt
            };
        }
        //            if ($user_id == $list['user_id']) {
        //                if ($list['send_num'] > 0) {
        //                    $updateData = array(
        //                        "send_num"   => 0,
        //                        "send_popup" => WorkOrderModel::SENDPOPUP_NO
        //                    );
        //                    $this->WorkOrderService->update([["id",$id]],$updateData);
        //                }
        //            } else {
        //                if ($list['receive_num'] > 0) {
        //                    $updateData = array(
        //                        "receive_num"   => 0,
        //                        "receive_popup" => WorkOrderModel::RECEIVEPOPUP_NO
        //                    );
        //                    $this->WorkOrderService->update([["id",$id]],$updateData);
        //                }
        //            }
        var workOrderReply = await _workOrderReplyService.GetWorkOrderReplyInfo(work_order_id: id).ToListAsync();
        var categoryGroup =
            await _workOrderCategoryService.GetWorkOrderCategoryInfo(user.CompanyID, WorkOrderCategory.States.ENABLE);
        var progressGroup = await _workOrderProgressService.GetWorkOrderProgressInfo(user.CompanyID);
        return Success(new { list, workOrderReply, categoryGroup, progressGroup });
    }

    //更新工单状态完成
    [HttpPost("[action]")]
    [Permission(Enums.Rule.Config.Message.JOB_PENDING_COMPLETE)]
    public async Task<ResultStruct> Complete(IdDto IdDto)
    {
        //        if (!$this->rules()->hasJobPendingComplete()) {
        //            return error('无权访问');
        //        }

       var Model= await _workOrderService.GetInfoById(IdDto.ID);
        if (Model is null)
        {
            return Error();
        }
        Model.SendPopup = true;
        Model.ReceiveNum = 0;
        Model.ReceivePopup = false;
        Model.State = WorkOrder.States.Complete;
        
        //        $updateData['send_num'] = DB::raw($this->WorkOrderModel->transformKey('send_num').'+1');
        await _workOrderService.Update(Model);
        return Success("操作成功");
    }

    [HttpGet("[action]")]
    public ResultStruct GetJobPendingRule()
    {
        //        $rule = $this->sessionData->getRule()->getMessage()->getBiter();
        //        if($this->sessionData->getConcierge() == UserModel::TYPE_OWN){
        var rule = new
        {
            JobPendingProcessing = true,
            JobPendingComplete = true,
        };
        if (User.IsInRole(Enums.Identity.Role.ADMIN))
        {
             
        }
        // }else{
        //     $data['rule'] = [
        //         'JobPendingProcessing'       => $rule->hasJobPendingProcessing(),
        //         'JobPendingComplete'         => $rule->hasJobPendingComplete(),
        //     ];
        // }
        return Success(new {rule});
    }
    
    //    /**
    //     * 获取权限
    //     *
    //     * @return \App\Rule\BitwiseFlags\Message
    //     */
    private object Rules()
    {
        //        return $this->sessionData->getRule()->getMessage()->getBiter();
        return 1;
    } //end rules()

}