﻿using ERP.Authorization.Rule;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.Message;
using ERP.Services.Identity;
using ERP.Services.Message;
using ERP.Services.Tools;
using Microsoft.Extensions.Options;
using static ERP.Models.View.Message.FeedBackViewM;

namespace ERP.Controllers.Message;

[Route("api/[controller]")]
[ApiController]
public class FeedBackController : BaseController
{
    private readonly FeedBackService _feedbackService;
    private readonly FeedBackReplyService _feedbackReplyService;
    private readonly UserManager _userManager;
    private readonly IHttpContextAccessor _HttpContextAccessor;
    private ISession _Session => _HttpContextAccessor.HttpContext!.Session;

    #region ERP3.0 PHP

    public FeedBackController(FeedBackService feedBackService, FeedBackReplyService feedbackService,IHttpContextAccessor httpContextAccessor, UserManager userManager)
    {
        _feedbackService = feedBackService;
        _feedbackReplyService = feedbackService;
        _userManager = userManager;
        _HttpContextAccessor = httpContextAccessor;
    }


    /// <summary>
    /// 反馈列表
    /// </summary>
    /// <returns></returns>
    [HttpPost("Index")]
    public async Task<ResultStruct> Index(IndexQuery query)
    {
        var user = await _userManager.GetUserAsync(User);
        var userId = user.Id;
        var oemId = user.OEMID;
        var list = await _feedbackService.info(userId, oemId,query.title,query.types,query.state)
            .OrderBy(f => f.State)
            .ThenByDescending(f => f.UpdatedAt)
            .ToPaginateAsync(page:query.Page,limit:query.Limit);
        var data = list.Transform(x => new
        {
            x.ID,x.Title,x.Type,x.State,x.OemName,x.UserName,x.CreatedAt,x.UpdatedAt,x.Imgs
        });
        return Success(new { data });
    }

    /// <summary>
    /// 处理列表
    /// </summary>
    /// <returns></returns>
    [HttpPost("[action]")]
    public async Task<ResultStruct> ProIndex(IndexQuery query)
    {
        var data = await _feedbackService.info(0, 0,query.title,query.types,query.state)
            .OrderBy(f => f.State)
            .ThenByDescending(f => f.UpdatedAt)
            .ToPaginateAsync();
        return Success(new { data });
    }

    /// <summary>
    /// 添加反馈
    /// </summary>
    /// <returns></returns>
    [HttpPost("[action]")]
    public async Task<ResultStruct> Add(FeedbackVm vm, [FromServices] Captcha captcha)
    {
        //验证验证码正确性
        if (!captcha.Compare(_Session.GetString("Captcha"), vm.captcha)  )
        {
            return Error(message: "验证码不正确");
        }
        var user = await _userManager.GetUserAsync(User);
        var model = new FeedBack(vm.title.Trim(), vm.types, vm.content.Trim(), vm.imgs??Array.Empty<string>().ToList(),
            FeedBack.States.NOT_PROCESSED, user);

        await _feedbackService.InsertData(model);
        return Success();
    }


    /// <summary>
    /// 处理详情信息页
    /// </summary>
    /// <returns></returns>
    [HttpGet("[action]/{id:int}")]
    public async Task<ResultStruct> Info(int id)
    {
        var dataMate = await _feedbackService.Info(id);

        var replyInfo = await _feedbackReplyService.GetListByFeedBackId(id);
        //            if(!empty($info['imgs'])){
        //                $info['imgs'] = json_decode($info['imgs'],true);
        //                foreach($info['imgs'] as $v){
        ////                    $info['imgList'][] = $v['oss'];
        //                    if(!empty($v['url']) && (strpos($v['url'],'http'))===false){
        //                        $currentOssDomain = app('oem')->getCurrentOssDomain();
        //                        $info['imgList'][] = $currentOssDomain. '/' .trim($v['url'],'/');
        //                    }
        //                }
        //            }
        var data = new object();
        if(dataMate is not null)
        {
            data = new
            {
                dataMate.Title,
                dataMate.State,
                dataMate.Content,
                dataMate.UserName,
                dataMate.IsComplete,
                dataMate.Type,
                dataMate.Imgs,
                dataMate.CreatedAt,
            };
        };
        
        var replyList = replyInfo.Select(r => new
        {
            icon = r.UserID == dataMate?.UserID ? "el-icon-chat-dot-round" : "el-icon-s-comment",
            r.Content,
            r.UserName,
            Color = "#0bbd87",
            Size = "large",
            Type = "primary",
            Timestamp = r.CreatedAt,
        });

        return Success(new { data, replyList });
    }
    /// <summary>
    /// 处理详情信息页
    /// </summary>
    /// <returns></returns>
    [HttpPost("[action]")]
    [Permission(Enums.Rule.Config.Message.FEED_BACK_DETAILS)]
    public async Task<ResultStruct> Info(ProInfoVm vm)
    {
        var user = await _userManager.GetUserAsync(User);

        var newVm = vm with { reply = vm.reply.Trim() };
        if(!string.IsNullOrEmpty(newVm.reply)){
            //不是创建人的回复自动更改状态
            if(vm.userID != user.Id)
            {
                var feedback = new FeedBack() { ID = vm.id, State = FeedBack.States.IN_PROCESS };
                await _feedbackService.UpdateData(feedback);
            }
            
            var reply = new FeedBackReply(vm.id, vm.reply, user);
            await _feedbackReplyService.InsertData(reply);
        }
        
        if(vm.isComplete) 
        {
            var feedback = new FeedBack() { ID = vm.id, State = FeedBack.States.COMPLETED };
            await _feedbackService.UpdateData(feedback);
        }

        return Success();
    }

    /// <summary>
    /// 将反馈记录标记为处理完成
    /// </summary>
    /// <returns></returns>
    [HttpPost("[action]/{id:int}")]
    [Permission(Enums.Rule.Config.Message.FEED_BACK_COMPLETE)]
    public async Task<ResultStruct> Complete(int id)
    {
        //        if (!$this->rules()->hasFeedBackComplete() || !$this->rules()->hasProcessingFeedBackComplete()) {
        //            return error('无权访问');
        //        }
        var model = await _feedbackService.Info(id);
        if (model is null)
            return Error("无效请求");
        model.State = FeedBack.States.COMPLETED;
        await _feedbackService.UpdateData(model);
        return Success();
    }

    /// <summary>
    /// 删除图片
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> DelImage(int id)
    {
        await _feedbackService.DelImage(id);

        return Success();
    }

    /// <summary>
    /// 清除冗余图片
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("[action]/{id:int}")]
    [Permission(Enums.Rule.Config.Message.FEED_BACK_REVOKE)]
    public async Task<ResultStruct> Withdraw(int id)
    {
        //        if (!$this->rules()->hasFeedBackRevoke()) {
        //            return error('无权访问');
        //        }

        var model = await _feedbackService.Info(id);
        if (model is null)
            return Error("无效请求");
        model.State = FeedBack.States.REVOKE;
        await _feedbackService.UpdateData(model);
        return Success();
    }

    /// <summary>
    /// 清除用户编辑转状态下关闭页面时已经存在的图片
    /// </summary>
    /// <returns></returns>
    public async Task<ResultStruct> ClearImage(int[] imgs)
    {
        foreach(var value in imgs){
            try {
                await _feedbackService.DelImage(value);
            } catch (Exception) {
                return Error("清除图片出错");
            }
        }
        return Success();
    }
    [Permission(Enums.Rule.Config.Message.PROCESSING_FEED_BACK_DEL)]
    public async Task<ResultStruct> ProDel(int id)
    {
        //        if (!$this->rules()->hasProcessingFeedBackDel()) {
        //            return error('无权访问');
        //        }
        var info = await _feedbackService.Info(id);
        //        try{
        //            $imgs = $info->first()->imgs;
        //            if($imgs && $imgs = json_decode($imgs,true)){
        //                foreach($imgs as $value){
        //                    $this->FeedbackService->delImage($value['id']);
        //                }
        //            }
        //            $info->delete();
        //            $this->FeedbackReplyService->info(['id','feedback_id'],'',$feedback_id)->delete();

        //            return success();
        //        }catch (\Exception $e){
        //            return error('删除失败');
        //        }
        return Success();
    }

    [HttpGet("[action]")]
    public ResultStruct GetProcessingFeedBackRule()
    {
        //        $rule = $this->sessionData->getRule()->getMessage()->getBiter();

        //        if($this->sessionData->getConcierge() == UserModel::TYPE_OWN){
        var rule = new
        {
            ProcessingFeedBackDetails = true,
            ProcessingFeedBackComplete = true,
            ProcessingFeedBackDel = true,
        };
        //        }else{
        //            $data['rule'] = [
        //                'ProcessingFeedBackDetails'   => $rule->hasProcessingFeedBackDetails(),
        //                'ProcessingFeedBackComplete'  => $rule->hasProcessingFeedBackComplete(),
        //                'ProcessingFeedBackDel'       => $rule->hasProcessingFeedBackDel(),
        //            ];
        //        }

        return Success(new { rule });
    }
    [HttpGet("add/changeCodeImg")]
    public ResultStruct changeCodeImg([FromServices] Captcha captcha) => getCodeImg(captcha);

    [HttpGet("Add/getCodeImg")]
    public ResultStruct getCodeImg([FromServices] Captcha captcha)
    {
        object data = new();
        string Key= captcha.GetRandomText(4);
        _Session.SetString("Captcha", Key.ToLower());
        data = new
        {
             Img = captcha.ImageToBase64(captcha.GetCaptcha(Key))
        };
        return Success(data);
    }
    /// <summary>
    /// 获取权限
    /// </summary>
    /// <returns></returns>
     [HttpGet("[action]")]
    public ResultStruct getFeedBackRule()
    {
        var rule = new
        {
            FeedBackDetails = true,
            FeedBackComplete = true,
            FeedBackRevoke = true,
        };
        if (User.IsInRole(Enums.Identity.Role.ADMIN))
        {
            
        }
        return Success(new { rule });
    }

    #endregion
}