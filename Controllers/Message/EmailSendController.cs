﻿using ERP.Services.Caches;
using ERP.Services.Message;
using ERP.Services.DB.Orders;
using ERP.Services.Identity;
using ERP.Extensions;

namespace ERP.Controllers.Message;
[Route("api/[controller]/")]
[ApiController]
public class EmailSendController : BaseController
{
    private readonly EmailInfoService _emailInfoService;

    private readonly Order _amazonOrderService;

    private readonly EmailMassService _emailMassService;

    private readonly UserManager _userManager;


    public EmailSendController(EmailInfoService emailInfoService, Order amazonOrderService, EmailMassService emailMassService
        , UserManager userManager)
    {
        _emailInfoService = emailInfoService;
        _amazonOrderService = amazonOrderService;
        _emailMassService = emailMassService;
        _userManager = userManager;
    }

    #region 亚马逊订单邮件功能

    public class MassImportVM
    {
        public int Id { get; set; }
        public int Type { get; set; }
    }
    
    /// <summary>
    /// 导入邮件邮箱到群发邮箱
    /// </summary>
    /// <param name="vm"></param>
    /// <returns></returns>
    public async Task<ResultStruct> MassImport(MassImportVM vm)
    {
        (string? postbox, string? nick)? res;
        switch (vm.Type) {
            case 1:
                res = await SourceEmail(vm.Id);
                break;
            case 2:
                res = await SourceAmazonOrder(vm.Id);
                break;
            default:
                return Error(message: "参数错误");
        }

        if (res == null || string.IsNullOrEmpty(res.Value.postbox))
        {
            return Error(message: "当前邮箱地址不存在");
        }

        var postbox = res.Value.postbox;
        // var hashcode = Helpers.CreateHashCode(postbox);
        var session = await _userManager.GetUserAsync(User);
        
        var insertData = new Models.Message.EmailMass() {  
            PostBox = postbox,
            Nick       = res.Value.nick,
            UserID  = session.Id,
            CompanyID = session.CompanyID,
            GroupID   = session.GroupID,
            OEMID    = session.OEMID,
            ReportId  = session.Company.ReportId,
            // TODO hashcode
            HashCode   = 0
        };

        if (await _emailMassService.CheckExists(insertData)) {
            return Error(message: "当前邮箱已存在，无需导入");
        }

        if (await _emailMassService.InsertData(insertData)) {
            return Success(message: "导入成功");
        }

        return Error(message: "导入失败");
    }

    /// <summary>
    /// 导入邮箱来源邮件
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    private async Task<(string postbox, string nick)?> SourceEmail(int id)
    {
        var info = await _emailInfoService.Info(id);
        if (info == null) {
            return null;
        }

        string postbox, nick;
        if (!string.IsNullOrEmpty(info.Replyto))
        {
            postbox = info.Replyto;
            nick = info.ReplaytoNick ?? string.Empty;
        }
        else
        {
            postbox = info.EmailAddress ?? string.Empty;
            nick = info.OtherNick ?? string.Empty;
        }

        return (postbox, nick);
    }

    /// <summary>
    /// 导入邮箱来源亚马逊订单
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    private async Task<(string? postbox, string? nick)?> SourceAmazonOrder(int id)
    {
        var info = await _amazonOrderService.GetOrderInfo(id);// , o => new {o.ReceiverName, o.ReceiverEmail}
        if (info == null) {
            return null;
        }
        var adr = info.Receiver!;
        return (adr?.Email,string.IsNullOrEmpty(adr?.Name)? adr?.Email : adr.Name );
    }
    #endregion

    /// <summary>
    /// 邮件列表
    /// </summary>
    /// <returns></returns>
    [HttpPost("Index")]
    public async Task<ResultStruct> EmailSendIndex(EmailCollectionController.EmailCollectListQuery query, [FromServices] EmailConfigCache cache)
    {
        var user = await _userManager.GetUserAsync(User);
        var userId = user.Id;
        var email = cache.List();
        var state = Models.Message.EmailInfo.States.RECEIPT;
        var data = await _emailInfoService.CollectList(userId, state, query.email_id, query.email_address);

        //        $emailData = checkArr($email) ? arrayCombine($email, 'id') : [];

        //        foreach ($list as $k => $v) {
        //            $list[$k]['email_address_raw'] = $v['email_address'];
        //            $list[$k]['email'] = isset($emailData[$v['email_id']]['name']) ? $emailData[$v['email_id']]['name'] : '';
        //            $list[$k]['address'] = isset($emailData[$v['email_id']]['address']) ? $emailData[$v['email_id']]['address'] : '';
        //        }
        //        return success('', ["data" => $list, "email" => $email]);
        return Success( new { data, email });
    }
    
    [HttpGet("[action]")]
    public ResultStruct GetEmailSendRule()
    {
        //        $rule = $this->sessionData->getRule()->getMessage()->getBiter();

        //        if ($this->sessionData->getConcierge() == UserModel::TYPE_OWN) {
        var rule = new
        {
            EmailSendSending = true,
            EmailSendDel = true,
            EmailSendImport = true,
            EmailSendDetails = true,
        };
        if (User.IsInRole(Enums.Identity.Role.ADMIN))
        {
             
        }
        //        } else {
        //            $data['rule'] = [
        //                'EmailSendSending' => $rule->hasEmailSendSending(),
        //                'EmailSendDel'     => $rule->hasEmailSendDel(),
        //                'EmailSendImport'  => $rule->hasEmailSendImport(),
        //                'EmailSendDetails' => $rule->hasEmailSendDetails(),
        //            ];
        //        }

        //        return success('', $data);
        return Success(new { rule });
    }

    /// <summary>
    /// 获取权限
    /// </summary>
    /// <returns></returns>
    private ResultStruct rules()
    {
        //        return $this->sessionData->getRule()->getMessage()->getBiter();
        return Success();
    }

}
