﻿using ERP.Models.Message;
using ERP.Services.Caches;
using ERP.Services.Message;
using ERP.Services.Identity;
using Ryu.Convert;
using ERP.Enums.ID;
using ERP.Extensions;
using ERP.Authorization.Rule;

namespace ERP.Controllers.Message;
[Route("api/[controller]/")]
[ApiController]
public class EmailCollectionController : BaseController
{
    private readonly EmailInfoService _emailInfoService;

    private readonly UserManager _userManager;

    public EmailCollectionController(EmailInfoService emailInfoService, UserManager userManager)
    {
        _emailInfoService = emailInfoService;
        _userManager = userManager;
    }

    public record EmailCollectListQuery(int? email_id, string? email_address);
    /// <summary>
    /// 收件列表
    /// </summary>
    /// <returns></returns>
    [HttpPost("Index")]
    public async Task<ResultStruct> EmailCollectList(EmailCollectListQuery query, [FromServices] EmailConfigCache emailConfigCache)
    {
        var user = await _userManager.GetUserAsync(User);
        var userId = user.Id;
        var emailConfig = emailConfigCache.List();
        const EmailInfo.States state = EmailInfo.States.DELIVERY;
        var data = await _emailInfoService.CollectList(userId, state,query.email_id, query.email_address);

        Dictionary<int, Models.Setting.EmailConfig> emailData = new Dictionary<int, Models.Setting.EmailConfig>();
        if (emailConfig is not null)
        {
            emailData = emailConfig.ToDictionary(x => x.ID, x => x);
        }
        //        foreach ($list as $k => $v) {
        //            $list[$k]['email_address_raw'] = $v['email_address'];
        //            $list[$k]['email'] = isset($emailData[$v['email_id']]['name']) ? $emailData[$v['email_id']]['name'] : '';
        //            $list[$k]['address'] = isset($emailData[$v['email_id']]['address']) ? $emailData[$v['email_id']]['address'] : '';
        //        }

        return Success(new { data, email = emailConfig });

    }

    /// <summary>
    /// 删除
    /// </summary>
    /// <returns></returns>
    [Permission(Enums.Rule.Config.Message.EMAIL_COLLECTION_DEL| Enums.Rule.Config.Message.EMAIL_SEND_DEL)]
    public async Task<ResultStruct> EmailDel(int id)
    {
        //        if (!$this->rules()->hasEmailCollectionDel() || !$this->rules()->hasEmailSendDel()) {
        //            return error('无权访问');
        //        }
        var user = await _userManager.GetUserAsync(User);
        var userId = user.Id;
        await _emailInfoService.Delete(id, userId);
        //            Cache::instance()->emailUser()->del();
        //            Cache::instance()->emailUserAll()->del();
        return Success("删除成功");
    }

    /// <summary>
    /// 收件列表详情页
    /// </summary>
    /// <returns></returns>
    [Permission(Enums.Rule.Config.Message.EMAIL_COLLECTION_DETAILS| Enums.Rule.Config.Message.EMAIL_SEND_DETAILS)]
    public async Task<ResultStruct> EmailInfoPage(int id)
    {
        //        if (!$this->rules()->hasEmailCollectionDetails() || !$this->rules()->hasEmailSendDetails()) {
        //            return error('无权访问');
        //        }
        var info = await _emailInfoService.Info(id);
        if(info is not null&& info.Content.IsNotEmpty())
        {
            info.Content = Base64.Decode(info.Content!);
            return Success(info);
        }
        return Error();
    }

    [HttpGet("[action]")]
    public ResultStruct GetEmailCollectionRule()
    {
        //        $rule = $this->sessionData->getRule()->getMessage()->getBiter();
        var rule = new
        {
            EmailCollectionDel = true,
            EmailCollectionImport = true,
            EmailCollectionDetails = true,
            EmailCollectionReply = true
        };
        if (!User.IsInRole(Enums.Identity.Role.ADMIN))
        {
            // } else {
            //     $data['rule'] = [
            //         'EmailCollectionDel'     => $rule->hasEmailCollectionDel(),
            //         'EmailCollectionImport'  => $rule->hasEmailCollectionImport(),
            //         'EmailCollectionDetails' => $rule->hasEmailCollectionDetails(),
            //         'EmailCollectionReply'   => $rule->hasEmailCollectionReply(),
            //     ];
            // }
        }
        return Success(rule);
    }

    /// <summary>
    /// 获取权限
    /// </summary>
    /// <returns></returns>
    private ResultStruct rules()
    {
        //        return $this->sessionData->getRule()->getMessage()->getBiter();
        return Success();
    }
}
