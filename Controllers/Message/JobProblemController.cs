﻿using ERP.Authorization.Rule;
using ERP.Extensions;
using ERP.Models.Message;
using ERP.Services.Identity;
using ERP.Services.Message;
using Microsoft.EntityFrameworkCore;

namespace ERP.Controllers.Message;

[Route("api/[controller]/")]
[ApiController]
public class JobProblemController : BaseController
{
    private readonly WorkOrderService _workOrderService;

    private readonly WorkOrderCategoryService _workOrderCategoryService;

    private readonly UserManager _userManager;

    public JobProblemController(WorkOrderService workOrderService, WorkOrderCategoryService workOrderCategoryService
        , UserManager userManager)
    {
        _workOrderService = workOrderService;
        _workOrderCategoryService = workOrderCategoryService;
        _userManager = userManager;
    }

    public record CategoryListQuery(WorkOrderCategory.States? State, string Name);

    /// <summary>
    /// 获取工单问题类型数据
    /// </summary>
    /// <param name="query"></param>
    /// <returns></returns>
    [HttpPost("Index")]
    public async Task<ResultStruct> CategoryList(CategoryListQuery query)
    {
        var user = await _userManager.GetUserAsync(User);
        //        $field = ["id", "name", "company_id", "truename", "state", "created_at"];
        var info = await _workOrderCategoryService.GetWorkOrderCategoryInfo(user.CompanyID, query.State, query.Name);
        return Success(new { info });
    }

    public record CategoryAddVm(string Name);

    /// <summary>
    /// 添加工单问题类型数据
    /// </summary>
    /// <returns></returns>
    [HttpPost("Add")]
    [Permission(Enums.Rule.Config.Message.JOB_PROBLEM_ADD)]
    public async Task<ResultStruct> CategoryAdd(CategoryAddVm vm)
    {
        //        if (!$this->rules()->hasJobProblemAdd()) {
        //            return error('无权访问');
        //        }
        if (string.IsNullOrEmpty(vm.Name.Trim()))
        {
            return Error("请填写问题类型名称");
        }

        var infoCheck = await _workOrderCategoryService.GetWorkOrderCategoryFirst(name: vm.Name.Trim());
        if (infoCheck is not null)
        {
            return Error("名称已存在，请重新编辑");
        }

        var user = await _userManager.GetUserAsync(User);

        var model = new WorkOrderCategory(vm.Name.Trim(), user);
        await _workOrderCategoryService.CategoryInsert(model);

        return Success("添加成功");
    }

 

    /// <summary>
    /// 工单问题类型数据详情
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id:int}")]
    public async Task<ResultStruct> Info(int id)
    {
        // $field = ["id", "name", "state"];
        var data = await _workOrderCategoryService.GetWorkOrderCategoryFirst(id);
        return Success(data);
    }

    /// <summary>
    /// 更新工单问题类型数据
    /// </summary>
    /// <param name="id"></param>
    /// <param name="vm"></param>
    /// <returns></returns>
    [Route("Edit/{id:int}")]
    [Permission(Enums.Rule.Config.Message.JOB_PROBLEM_EDIT)]
    public async Task<ResultStruct> CategoryEdit(int id, Models.View.Message.JobProBlem.CategoryEditVm vm)
    {
        //        if (!$this->rules()->hasJobProblemEdit()) {
        //            return error('无权访问');
        //        }

        //先排除不等于自己，再查是否重复
        var infoSelf = await _workOrderCategoryService.GetWorkOrderCategoryFirst(id);
        if (infoSelf is null)
        {
             return Error("已存在，请重新编辑");
        }
        if (infoSelf!.Name != vm.Name.Trim())
        {
            var infoCheck = await _workOrderCategoryService.GetWorkOrderCategoryFirst(name: vm.Name.Trim());
            if (infoCheck is not null)
            {
                return Error("名称重复，请重新编辑");
            }
        }

        //如果包含禁用状态
        if (vm.State == WorkOrderCategory.States.DISABLE)
        {
            var count = await _workOrderService.GetWorkOrderInfo(categoryId: id)
                .Where(w => w.State == WorkOrder.States.ToBeProcessed || w.State == WorkOrder.States.Processing)
                .CountAsync();

            if (count > 0)
            {
                return Error("该选项正在被使用，无法禁用，请完成待处理工单后再重试！");
            }
        }

        infoSelf.Name = vm.Name.Trim();
        infoSelf.State = vm.State;
        await _workOrderCategoryService.CategoryUpdate(infoSelf);
        return Success("更新成功");
    }

    [HttpGet("Del/{id:int}")]
    [Permission(Enums.Rule.Config.Message.JOB_PROBLEM_DEL)]
    public async Task<ResultStruct> CategoryDel(int id)
    {
        // if (!$this->rules()->hasJobProblemDel()) {
        //     return error('无权访问');
        // }
        var count = await _workOrderService.GetWorkOrderInfo(categoryId: id)
            .Where(t => t.State == WorkOrder.States.ToBeProcessed || t.State == WorkOrder.States.Processing)
            .CountAsync();
        if (count > 0)
        {
            return Error("该选项正在被使用，请完成待处理工单后再重试！");
        }

        await _workOrderCategoryService.CategoryDel(id);
        return Success("删除成功");
    }

    [HttpGet("[action]")]
    public ResultStruct GetJobProblemRule()
    {
        //        $rule = $this->sessionData->getRule()->getMessage()->getBiter();


        //        if($this->sessionData->getConcierge() == UserModel::TYPE_OWN){

        var rule = new object();
        rule = new
        {
            JobProblemAdd = true,
            JobProblemEdit = true,
            JobProblemDel = true,
        };
        if (!User.IsInRole(Enums.Identity.Role.ADMIN))
        {
            
        }
        //        }else{
        //            $data['rule'] = [
        //                'JobProblemAdd'          => $rule->hasJobProblemAdd(),
        //                'JobProblemEdit'         => $rule->hasJobProblemEdit(),
        //                'JobProblemDel'          => $rule->hasJobProblemDel(),
        //            ];
        //        }

        //        return success('', $data);
        return Success(new { rule });
    }

    private object rules()
    {
        //        return $this->sessionData->getRule()->getMessage()->getBiter();
        return 1;
    }
}