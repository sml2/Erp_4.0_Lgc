﻿using System.ComponentModel.DataAnnotations;
using ERP.Extensions;
using ERP.Models.DB.Users;
using ERP.Services.Caches;
using ERP.Services.Message;
using ERP.Services.Identity;
using ERP.Services.DB.Users;
using Microsoft.EntityFrameworkCore;
using BaseModel = ERP.Models.Abstract.BaseModel;
using static ERP.Models.View.Message.EmailListViewModel;
using ERP.Authorization.Rule;

namespace ERP.Controllers.Message;

[Route("api/EmailList/")]
[ApiController]
public class EmailListController : BaseController
{
    private readonly EmailInfoService _emailInfoService;

    private readonly UserEmailService _userEmailService;
    private readonly UserManager _userManager;


    public EmailListController(EmailInfoService emailInfoService, UserEmailService userEmailService, UserManager userManager)
    {
        _emailInfoService = emailInfoService;
        _userEmailService = userEmailService;
        _userManager = userManager;
    }

    /// <summary>
    /// 邮箱配置数据列表
    /// </summary>
    /// <param name="data"></param>
    /// <param name="emailConfigCache"></param>
    /// <returns></returns>
    [HttpPost("Index")]
    public async Task<ResultStruct> EmailList(EmailListQuery data, [FromServices] EmailConfigCache emailConfigCache)
    {
        var state = data.State;
        var typeId = data.TypeId;
        var name = data?.Name?.Trim();
        var session = await _userManager.GetUserAsync(User);
        var userId = session.Id;
        var list = await _userEmailService.GetUserEmailInfo(null, state, userId, typeId, name??"").ToPaginateAsync();

        var emailConfig = emailConfigCache.List();
        Dictionary<int, Models.Setting.EmailConfig> emailData = new Dictionary<int, Models.Setting.EmailConfig>();
        if (emailConfig is not null)
        {
            emailData = emailConfig.ToDictionary(x => x.ID, x => x);
        }

        var result = new List<EmailListVm>();
        foreach (var item in list.Items)
        {
            emailData.TryGetValue(item.TypeId, out var value);
            result.Add(new EmailListVm()
            {
                ID = item.ID,
                Name = item.Name,
                Token = item.Token,
                TypeId = item.TypeId,
                State = item.State,
                Sort = item.Sort,
                Address = item.Address,
                Nick = item.Nick,
                email = value?.Name ?? "",
                pop = value?.Pop ?? "",
                pop_port = value?.PopPort ?? null,
                pop_ssl = value?.PopSSL == BaseModel.StateEnum.Open,
                // Time = item.Time != null ? HumanDate(item.Time) : "未同步",
                 msg = item.SyncMsg ?? "未同步",
                Tag = item.Tag ?? "",
                updatedAt=item.UpdatedAt,
            });
        }

        return Success(new
        {
            data = new DbSetExtension.PaginateStruct<EmailListVm>()
                { CurrentPage = list.CurrentPage, PageSize = list.PageSize, Items = result },
            email = emailConfig,
        });
    }

   

    [HttpGet("{id:int}")]
    public async Task<ResultStruct> Info(int id, [FromServices] EmailConfigCache emailConfigCache)
    {
        var info = await _userEmailService.GetUserEmailInfo(id).FirstOrDefaultAsync();
        return Success(new { info });
    }
    /// <summary>
    /// 添加邮箱
    /// </summary>
    /// <returns></returns>
    [HttpPost("[action]")]
    [Permission(Enums.Rule.Config.Message.EMAIL_LIST_ADD| Enums.Rule.Config.Message.EMAIL_LIST_EDIT)]
    public async Task<ResultStruct> AddEmail(AddEmailVm info)
    {
        
        //修改
        if (info.Id.HasValue)
        {
            // if (!$this->rules()->hasEmailListEdit()) {
            //     return error('无权访问');
            // }
            var row = await _userEmailService.Info(info.Id.Value);
            if (row != null)
            {
                row.Address = info.Address;
                row.Name = info.Name;
                row.Nick = info.Nick;
                row.State = info.State;
                row.Token = info.Token;
                row.TypeId = info.TypeId;
                await _userEmailService.Update(info.Id.Value, row);
            }
            return Success();
        }
        
        var model = new UserEmail()
        {
            Address = info.Address,
            Name = info.Name,
            Nick = info.Nick,
            State = info.State,
            Token = info.Token,
            TypeId = info.TypeId,
        };

        //添加
        //                if (!$this->rules()->hasEmailListAdd()) {
        //                    return error('无权访问');
        //                }
        var user = await _userManager.GetUserAsync(User);
        model.UserID = user.Id;
        model.GroupID = user.GroupID;
        model.CompanyID = user.CompanyID;
        model.OEMID = user.OEMID;
        model.Sort = -DateTime.Now.GetTimeStamp();
        await _userEmailService.Insert(model);
        //            Cache::instance()->emailUser()->del();
        //            Cache::instance()->emailUserAll()->del();
        return Success();
    }

    /// <summary>
    /// 删除邮箱
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpPost("[action]/{id:int}")]
    [Permission(Enums.Rule.Config.Message.EMAIL_LIST_DEL)]
    public async Task<ResultStruct> DelEmail(int id)
    {
        //        if (!$this->rules()->hasEmailListDel()) {
        //            return error('无权访问');
        //        }

        var result = await _userEmailService.Delete(id);

        //        Cache::instance()->emailUser()->del();
        //        Cache::instance()->emailUserAll()->del();

        return result ? Success() : Error();
    }

    /// <summary>
    /// 修改启用禁用状态
    /// </summary>
    /// <param name="id"></param>
    /// <param name="state"></param>
    /// <returns></returns>
    [HttpGet("[action]/{id:int}/{state}")]
    [Permission(Enums.Rule.Config.Message.LIST_CHANGE_STATE)]

    public async Task<ResultStruct> ChangeEmailState(int id, UserEmail.States state)
    {
        //        if (!$this->rules()->hasEmailListChangeState()) {
        //            return error('无权访问');
        //        }

        await _userEmailService.ChangeStateAsync(id, state);

        //        Cache::instance()->emailUser()->del();
        return Success();
    }

    /// <summary>
    /// 邮件重置
    /// </summary>
    /// <returns></returns>
    [HttpPost("[action]")]
    [Permission(Enums.Rule.Config.Message.EMAIL_LIST_RESET)]
    public async Task<ResultStruct> ResetEmail(int id, ResetAction action)
    {
        var user = await _userManager.GetUserAsync(User);
        var userId = user.Id;
        if ((action & ResetAction.ClearEmail) == ResetAction.ClearEmail)
        {
            await _emailInfoService.Delete(id, userId);
        }

        if ((action & ResetAction.ResetFlag) == ResetAction.ResetFlag)
        {
            await _userEmailService.ResetFlag(id, userId);
            //                    Cache::instance()->emailUser()->del();
            //                    Cache::instance()->emailUserAll()->del();
        }

        return Success("操作成功");
    }

    [HttpGet("[action]")]
    public ResultStruct GetEmailListRule()
    {
        //        $rule = $this->sessionData->getRule()->getMessage()->getBiter();
        var rule = new
        {
            EmailListAdd = true,
            EmailListSend = true,
            EmailListDel = true,
            EmailListEdit = true,
            EmailListReset = true,
            EmailListChangeState = true,
            EmailListSynchronous = true,
        };
        if (!User.IsInRole(Enums.Identity.Role.ADMIN))
        {
            //} else {
            //    $data['rule'] = [
            //        'EmailListAdd'         => $rule->hasEmailListAdd(),
            //        'EmailListSend'        => $rule->hasEmailListSend(),
            //        'EmailListDel'         => $rule->hasEmailListDel(),
            //        'EmailListEdit'        => $rule->hasEmailListEdit(),
            //        'EmailListReset'       => $rule->hasEmailListReset(),
            //        'EmailListChangeState' => $rule->hasEmailListChangeState(),
            //        'EmailListSynchronous' => $rule->hasEmailListSynchronous(),
            //    ];
        }

        return Success(new { rule });
    }
}