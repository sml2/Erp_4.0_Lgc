﻿using System.Linq;
using ERP.Authorization.Rule;
using ERP.Enums.Message;
using ERP.Extensions;
using ERP.Models.Message;
using ERP.Services.Identity;
using ERP.Services.Message;
using static ERP.Models.View.Message.WorkOrderView;

namespace ERP.Controllers.Message;

[Route("api/[controller]/")]
[ApiController]
public class JobMyController : BaseController
{
    private readonly WorkOrderService _workOrderService;

    private readonly WorkOrderCategoryService _workOrderCategoryService;

    private readonly WorkOrderProgressService _workOrderProgressService;

    private readonly UserManager _userManager;

    public JobMyController(WorkOrderService workOrderService,
        WorkOrderCategoryService workOrderCategoryService
        , WorkOrderProgressService workOrderProgressService, UserManager userManager)
    {
        _workOrderService = workOrderService;
        _workOrderCategoryService = workOrderCategoryService;
        _workOrderProgressService = workOrderProgressService;
        _userManager = userManager;
    }
   

    
    [HttpPost("Index")]
    public async Task<ResultStruct> WorkOrderList(WorkOrderListQuery query)
    {
        var user = await _userManager.GetUserAsync(User);
        var list = await _workOrderService.GetWorkOrderInfo(null, user.CompanyID, query.Title, query.category_id,
                query.progress_id, query.State, query.Commit)
            .OrderByDescending(w => w.CreatedAt)
            .ThenBy(w => w.State)
            .Select(m => new WorkOrderListDto(m.ID, m.Title, m.Category, m.Progress, m.SendNum, m.Commit, m.State, m.DeadLine, m.CreatedAt))
            .ToPaginateAsync(page:query.Page,limit:query.Limit);
    

        var categoryGroup =
            await _workOrderCategoryService.GetAll(user.CompanyID, WorkOrderCategory.States.ENABLE);
        var progressGroup =
            await _workOrderProgressService.GetAll(user.CompanyID, WorkOrderProgress.States.ENABLE);
        return Success(new { list, CategoryGroup = categoryGroup, ProgressGroup = progressGroup });
    }

    [HttpPost("Add")]
    [Permission(Enums.Rule.Config.Message.JOB_MY_ADD)]
    public async Task<ResultStruct> WorkOrderAdd(WorkOrderAddVm vm)
    {
        //        if (!$this->rules()->hasJobMyAdd()) {
        //            return error('无权访问');
        //        }
        var user = await _userManager.GetUserAsync(User);
        if (Request.Method == "POST")
        {
            //处理post请求
            var time = DateTime.Now;
            if (vm.Deadline < time)
            {
                return Error("处理期限不能小于当前时间");
            }

            var model = new WorkOrder(vm.Title, vm.Content, vm.category_id, vm.progress_id, vm.Deadline,user,vm.Commit);
            await _workOrderService.Create(model);
            return Success("添加成功");
        }

        //处理get请求
        var categoryGroup =
            await _workOrderCategoryService.GetWorkOrderCategoryInfo(user.CompanyID, WorkOrderCategory.States.ENABLE);
        var progressGroup =
            await _workOrderProgressService.GetWorkOrderProgressInfo(user.CompanyID, WorkOrderProgress.States.ENABLE);
        return Success(new { CategoryGroup = categoryGroup, ProgressGroup = progressGroup });
    }

    [HttpGet("Withdrawn/{id:int}")]
    [Permission(Enums.Rule.Config.Message.JOB_MY_REVOKE)]
    public async Task<ResultStruct> WorkOrderWithdrawn(int id)
    {
        //        if (!$this->rules()->hasJobMyRevoke()) {
        //            return error('无权访问');
        //        }
        var model = await _workOrderService.GetInfoById(id);
        if (model is null)
            return Error("无权访问");
        
        model.Withdraw();
        await _workOrderService.Update(model);
        return Success("撤回成功!");
    }

    [HttpGet("[action]")]
    public ResultStruct GetJobMyRule()
    {
        //        $rule = $this->sessionData->getRule()->getMessage()->getBiter();
        //        if($this->sessionData->getConcierge() == UserModel::TYPE_OWN){
        var rule = new
        {
            JobMyAdd = true,
            JobMyView = true,
            JobMyRevoke = true,
        };
        if (User.IsInRole(Enums.Identity.Role.ADMIN))
        {
            
        }
        //        }else{
        //            $data['rule'] = [
        //                'JobMyAdd'          => $rule->hasJobMyAdd(),
        //                'JobMyView'         => $rule->hasJobMyView(),
        //                'JobMyRevoke'       => $rule->hasJobMyRevoke(),
        //            ];
        //        }

        //        return success('', $data);
        return Success(new {rule});
    }

}