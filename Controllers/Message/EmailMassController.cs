﻿using ERP.Extensions;
using ERP.Models.Message;
using ERP.Services.Message;
using ERP.Services.Identity;
using Microsoft.EntityFrameworkCore;
using ERP.Interface;
using ERP.Enums.Identity;
using ERP.Authorization.Rule;

namespace ERP.Controllers.Message;
[Route("api/[controller]/")]
[ApiController]
public class EmailMassController : BaseController
{
    private readonly EmailMassService _emailMassService;

    private readonly UserManager _userManager;

    private readonly ISessionProvider _ISessionProvider;
    public EmailMassController(EmailMassService emailMassService, UserManager userManager,ISessionProvider provider)
    {
        _emailMassService = emailMassService;
        _userManager = userManager;
        _ISessionProvider = provider;
    }

    public record EmailMassListQuery(string? postbox = null);

    /// <summary>
    /// 获取邮件群发地址
    /// </summary>
    /// <returns></returns>
    [HttpPost("Index")]
    public async Task<ResultStruct> EmailMassList(EmailMassListQuery query)
    {
        var user = await _userManager.GetUserAsync(User);
        var userId = user.Id;
        var companyId = user.CompanyID;
        var data = await _emailMassService.info(userId, companyId, postbox: query.postbox).OrderByDescending(e => e.CreatedAt)
            .ToPaginateAsync();
 
        return Success(data);
    }

    public record EmailMassVm(string postbox, string nick, int? id = null);
    
    /// <summary>
    /// 群发邮箱 添加删除
    /// </summary>
    /// <returns></returns>
    [HttpPost("[action]")]
    [Permission(Enums.Rule.Config.Message.EMAIL_MASS_ADD|Enums.Rule.Config.Message.EMAIL_MASS_EDIT)]
    public async Task<ResultStruct> MassAdd(EmailMassVm vm)
    {
        var user = await _userManager.GetUserAsync(User);
        var userId = user.Id;
        var companyId = user.CompanyID;
        if (!vm.id.HasValue)
        {
            //                if (!$this->rules()->hasEmailMassAdd()) {
            //                    return error('无权访问');
            //                }
            
            //新增操作
            if (await _emailMassService.info(userId, companyId, Helpers.CreateHashCode(vm.postbox)).AnyAsync()) {
                return Error("当前邮箱已存在！");
            }

            var row = new EmailMass(vm.postbox, vm.nick, user);
             await _emailMassService.InsertData(row);
            return Success("添加成功");
        }

        //                if (!$this->rules()->hasEmailMassEdit()) {
        //                    return error('无权访问');
        //                }
        //修改操作
        var result = await _emailMassService.info(userId, companyId, Helpers.CreateHashCode(vm.postbox)).FirstOrDefaultAsync();
        if (result is not null && result.ID != vm.id) {
            return Error("当前邮箱已存在！");
        }
        var info = result ?? new EmailMass() { ID = vm.id.Value };
        info.Update(vm.postbox, vm.nick);

        await _emailMassService.UpdateData(info);

        return Success("修改成功");
    }

    [HttpGet("{id:int}")]
    public async Task<ResultStruct> MassInfo(int id)
    {
        var user = await _userManager.GetUserAsync(User);
        //get请求点击编辑回填数据
        var info = await _emailMassService.Info(id, user);
        return info is not null? Success(info!):Error();
    }

    /// <summary>
    /// 删除群发邮件
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpPost("[action]/{id:int}")]
    [Permission(Enums.Rule.Config.Message.EMAIL_MASS_DEL)]
    public async Task<ResultStruct> MassDel(int id)
    {
        //        if (!$this->rules()->hasEmailMassDel()) {
        //            return error('无权访问');
        //        }
        var user = await _userManager.GetUserAsync(User);
        await _emailMassService.DeleteAsync(id, user);
        
        return Success(message: "删除成功");
    }


    [HttpGet("[action]")]
    public ResultStruct GetEmailMassRule()
    {
        //        $rule = $this->sessionData->getRule()->getMessage()->getBiter();
        var rule = new
        {
            EmailMassAdd = true,
            EmailMassSend = true,
            EmailMassDel = true,
            EmailMassEdit = true,
        };
        //        if ($this->sessionData->getConcierge() == UserModel::TYPE_OWN) {
        if (User.IsInRole(Enums.Identity.Role.ADMIN))
        {
            
        }
        // } else {
        //     $data['rule'] = [
        //         'EmailMassAdd'  => $rule->hasEmailMassAdd(),
        //         'EmailMassSend' => $rule->hasEmailMassSend(),
        //         'EmailMassDel'  => $rule->hasEmailMassDel(),
        //         'EmailMassEdit' => $rule->hasEmailMassEdit(),
        //     ];
        // }
        return Success(new {rule});
    }
    /// <summary>
    /// 获取权限
    /// </summary>
    /// <returns></returns>
    private ResultStruct rules()
    {
        //        return $this->sessionData->getRule()->getMessage()->getBiter();
        return Success();
    }

}
