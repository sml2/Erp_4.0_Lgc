using ERP.Services.DB.B2C;
using ERP.ViewModels;
using ERP.ViewModels.B2C;
using NPOI.OpenXmlFormats.Vml;

namespace ERP.Controllers.B2C;

[Route("/api/b2c/[controller]/[action]")]
public class DomainController : BaseController
{
    private readonly DomainService _domainService;

    public DomainController(DomainService domainService)
    {
        _domainService = domainService;
    }

    [HttpPost]
    public async Task<ResultStruct> List(DomainListDto req)
    {
        var result = await _domainService.List(req);
        return Success(result);
    }

    [HttpPost]
    public async Task<ResultStruct> Update(DomainUpdateDto req)
    {
        try
        {
            await _domainService.AddOrUpdate(req);
            return Success();
        }
        catch (Exception e)
        {
            return Error(e.Message);
        }
    }

    [HttpPost]
    public async Task<ResultStruct> Info(IdDto req)
    {
        var result = await _domainService.GetInfoByID(req.ID);
        return Success(result);
    }

    [HttpPost]
    public async Task<ResultStruct> Delete(IdDto req)
    {
        var res = await _domainService.Delete(req.ID);
        return res ? Success() : Error();
    }
}