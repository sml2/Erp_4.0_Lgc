using ERP.Enums.Rule.Config;
using ERP.Extensions;
using ERP.Interface;
using ERP.Services.Caches;
using ERP.Services.DB.B2C;
using ERP.Services.DB.Users;
using ERP.Services.Identity;
using ERP.Services.Product;
using ERP.ViewModels;
using ERP.ViewModels.B2C;

namespace ERP.Controllers.B2C;

[Route("/api/b2c/[controller]")]
public class SelectionController : BaseController
{
    private readonly SelectionService _selectionService;
    private readonly ReleaseService _releaseService;
    private readonly DomainService _domainService;
    private readonly UserService _userService;
    private readonly PlatformCache _platformCache;
    private readonly LanguagesCache _languagesCache;
    private readonly CategoryService _categoryService;
    private readonly UserManager _userManager;

    public SelectionController(SelectionService selectionService, PlatformCache platformCache, UserService userService,
        DomainService domainService, LanguagesCache languagesCache, ReleaseService releaseService,
        CategoryService categoryService, UserManager userManager)
    {
        _selectionService = selectionService;
        _platformCache = platformCache;
        _userService = userService;
        _domainService = domainService;
        _languagesCache = languagesCache;
        _releaseService = releaseService;
        _categoryService = categoryService;
        _userManager = userManager;
    }

    [HttpPost("list")]
    public async Task<ResultStruct> List(ListDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, ProductAbout.CategoryRange);
        var product = await _selectionService.List(req);
        var platform = _platformCache?.List()?.ToDictionary(m => m.ID, m => m);
        var users = await _userService.GetUserOptions();
        var domain = await _domainService.GetDomainList();
        var language = _languagesCache.List();
        var category = await _categoryService.FirstCategory(range, true);
        return Success(new
        {
            goods = product,
            platform,
            users,
            releaseOptions = domain,
            language,
            category
        });
    }

    [HttpPost("languagePack")]
    public async Task<ResultStruct> LanguagePack(IdDto req)
    {
        var result = await _selectionService.GetLanguagePack(req.ID);

        return Success(result);
    }

    [HttpPost("clone")]
    public async Task<ResultStruct> Clone(CloneDto req)
    {
        try
        {
            await _selectionService.ClonePro(req);
            return Success();
        }
        catch (Exception e)
        {
            return Error(e.Message);
        }
    }

    [HttpPost("release")]
    public async Task<ResultStruct> Release(ReleaseDto req)
    {
        try
        {
            await _releaseService.BeforeManual(req);
            return Success();
        }
        catch (Exception e)
        {
            return Error(e.Message);
        }
    }

    [HttpPost("situneShop")]
    public async Task<ResultStruct> SituneShop(ReleaseDto req)
    {
        try
        {
            await _releaseService.BeforeManual(req);
            return Success();
        }
        catch (Exception e)
        {
            return Error(e.Message);
        }
    }

    [HttpGet("releaseInfo/{id}")]
    public async Task<ResultStruct> ReleaseInfo(int id)
    {
        var release = await _selectionService.ReleaseInfo(id);
        var tempDomain = await _domainService.GetDomainList();
        var domain = tempDomain.ToDictionary(m => m.Id, m => m);
        return Success(new
        {
            release,
            domain
        });
    }

    [HttpPost("cloneWithRelease")]
    public async Task<ResultStruct> CloneWithRelease(CloneWithReleaseDto req)
    {
        try
        {
            var copyVm = await _selectionService.ClonePro(req);
            
            await _releaseService.BeforeManual(new ReleaseDto()
            {
                Id = copyVm.NewProductId,
                DomainId = req.DomainId
            });
            return Success();
        }
        catch (Exception e)
        {
            return Error(e.Message);
        }
    }
}