using ERP.Services.DB.B2C;
using ERP.ViewModels;
using ERP.ViewModels.B2C;

namespace ERP.Controllers.B2C;

[Route("/api/b2c/[controller]/[action]")]
public class UsersController : BaseController
{
    private readonly B2CUsersService _usersService;

    public UsersController(B2CUsersService usersService)
    {
        _usersService = usersService;
    }

    [HttpPost]
    public async Task<ResultStruct> List(B2CUsersListDto req)
    {
        var result = await _usersService.List(req);
        return Success(result);
    }

    [HttpPost]
    public async Task<ResultStruct> DomainList(IdDto req)
    {
        var result = await _usersService.DomainList(req.ID);
        return Success(result);
    }
    
    [HttpPost]
    public async Task<ResultStruct> Delete(IdDto req)
    {
        var res = await _usersService.Delete(req.ID);
        return res ? Success() : Error();
    }

    [HttpPost]
    public async Task<ResultStruct> SubOpen(SubOpenDto req)
    {
        var result = await _usersService.OpenB2C(req.Username);
        return result.State ? Success("开通成功") : Error(result.Message);
    }
    
    [HttpPost]
    public async Task<ResultStruct> SetUserState(SetB2CUserStateDto req)
    
    {
        var result = await _usersService.SetUserState(req);

        return result.State ? Success() : Error(result.Message);
    }
}