using ERP.Extensions;
using ERP.Interface;
using ERP.Models.DB.Users;
using ERP.Services;
using ERP.Services.Stores;
using ERP.ViewModels;
using ERP.ViewModels.Store.Remote;
using static ERP.Models.Stores.StoreRegion;

namespace ERP.Controllers.Store;

using ERP.Authorization.Rule;
using ERP.Enums;
using ERP.Exceptions.Stores;
using ERP.Models.DB.Stores;
using ERP.Models.Stores;
using Org.BouncyCastle.Ocsp;
using Services.DB.Users;
using System.Text;
using AmazonKeyCache = Services.Caches.AmazonKey;
using BaseModel = Models.Abstract.BaseModel;
using NationCache = Services.Caches.Nation;
using PlatformDataCache = Services.Caches.PlatformData;
using UnitCache = Services.Caches.Unit;

public class StoreController : BaseController
{
    private readonly StoreService _service;
    private readonly UserService _userService;
    private readonly StoreGroupService _storeGroupService;
    private readonly GroupService _groupService;
    private readonly CompanyService _CompanyService;
    private readonly UnitCache _UnitCache;
    private readonly NationCache _NationCache;
    private readonly PlatformDataCache _PlatformDataCache;
    private readonly ISessionProvider _sessionProvider;

    public StoreController(StoreService service, UserService userService, StoreGroupService storeGroupService,
        GroupService groupService,
        CompanyService companyService, UnitCache UnitCache, NationCache nationCache,
        PlatformDataCache platformDataCache, ISessionProvider sessionProvider)
    {
        _service = service;
        _userService = userService;
        _storeGroupService = storeGroupService;
        _groupService = groupService;
        _CompanyService = companyService;
        _UnitCache = UnitCache;
        _NationCache = nationCache;
        _PlatformDataCache = platformDataCache;
        _sessionProvider = sessionProvider;
    }

    /// <summary>
    /// 获取页面初始化获取权限
    /// todo 权限
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> GetStoreRule()
    {
        return Success(new
        {
            rule = _service.StoreRule(),
            company = await _service.GetCompanyStoreNum()
        });
    }

    #region 我的店铺

    //todo 权限
    /// <summary>
    /// 我的店铺初始化
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> GetMyStoreRule()
    {
        return Success(new
        {
            rule = _service.StoreRule(),
            company = await _service.GetCompanyStoreNum()
        });
    }

    #endregion


    #region 其他店铺

    /// 其他店铺列表
    /// </summary>
    /// <param name="OtherDto"></param>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> OtherIndex([FromQuery] OtherStoreListDto OtherDto)
    {
        var result = await _service.OtherList(OtherDto);
        return Success(result);
    }

    #endregion


    #region amazon

    /// <summary>
    /// 亚马逊店铺列表
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> IndexList(AmazonStoreListDto req)
    {
        var result = await _service.AmazonList(req);

        return Success(result);
    }


    public async Task<ResultStruct> GetStoreList(int platformID)


    {
        Platforms platform = Enums.Platforms.AMAZONSP;
        if (platformID == 6)
        {
            platform = Enums.Platforms.SHOPEE;
        }
        else if (platformID == 8)
        {
            platform = Enums.Platforms.AMAZONSP;
        }
        else
        {
            return Error($"暂未对接的平台");
        }

        var result = await _service.GetStoreList(platform);

        return Success(result);
    }


    public async Task<ResultStruct> GetNationsByStoreID(int storeID)
    {
        var result = await _service.GetNationsByStoreID(storeID);
        return Success(result);
    }


    #region amazon-mws

    [HttpPost]
    public async Task<ResultStruct> AmazonVerifyStore(IdDto idDto, [FromServices] AmazonKeyCache CacheAmazonKey,
        [FromServices] PlatformDataCache CachePlatformData)
    {
        try
        {
            await _service.AmazonVerifyStore(idDto.ID, CacheAmazonKey, CachePlatformData);
            return Success(true);
        }
        catch (VerifyFail vf)
        {
            return Warning(vf.Message);
        }
        catch (Exception ex)

        {
            return Error(ex.Message);
        }
    }

    #endregion


    [HttpPost]
    [Permission(Enums.Rule.Config.Store.STORE_ADD | Enums.Rule.Config.Store.STORE_EDIT |
                Enums.Rule.Config.Store.MYSTORE_ADD | Enums.Rule.Config.Store.MYSTORE_EDIT)]
    public async Task<ResultStruct> EditAmazonStore(MwsStoreDto req)
    {
        BaseService.ReturnStruct res = new BaseService.ReturnStruct();
        if (req.ID > 0)
        {
            //修改
            res = await _service.EditAmazonStore(req);
        }
        else
        {
            //新增
            res = await _service.AddAmazonStore(req);
        }

        return res.State ? Success() : Error(message: res.Msg);
    }

    #region amazon-spapi

    public async Task<ResultStruct> getAmazonMarketInfo(IdDto req)
    {
        if (req.ID <= 0)
        {
            return Error(message: "缺少参数: ID");
        }

        var info = await _service.GetMarketByHash(req.ID);

        return Success(info);
    }

    public async Task<ResultStruct> UpdateAmazonMarketInfo(marketDto req)
    {
        if (req.ID <= 0)
        {
            return Error(message: "缺少参数: ID");
        }

        if (req.MID <= 0)
        {
            return Error(message: "缺少参数: MID");
        }

        var model = await _service.GetInfoById(req.ID);
        if (!model.PlatFormIds.Contains(req.PlatformID))
        {
            return Error(message: "该市场已被删除，请刷新页面");
        }

        var b = await _service.UpdateAmazonMarketInfo(req);
        if (b)
        {
            return Success(true);
        }
        else
        {
            return Error(message: "该市场已被删除");
        }
    }


    //验证店铺状态
    [HttpPost]
    public async Task<ResultStruct> CheckMarketIDs(IdDto req)
    {
        if (req.ID > 0)
        {
            var res = await _service.CheckMarketIDs(req);
            return res.State ? Success(true) : Error(false, message: res.Msg);
        }
        else
        {
            return Error("参数传递失败");
        }
    }

    public async Task<ResultStruct> GetAmazonSpUrl(IdDto req)
    {
        if (req.ID <= 0)
        {
            return Error(message: "缺少参数: ID");
        }
        else
        {
            var host = HttpContext.Request.Host.Value;
            var res = await _service.GetAmazonSpUrl(req, host);
            return res.State
                ? Success(new
                {
                    success = true,
                    data = res.Data!,
                })
                : Error(
                    new
                    {
                        success = false,
                        message = res.Msg,
                    });
        }
    }

    //[HttpPost]
    //public async Task<ResultStruct> SaveAmazonSPAccessToken(AmazonCode req)
    //{
    //    if (!string.IsNullOrWhiteSpace(req.Code) && !string.IsNullOrWhiteSpace(req.PartnerId) && req.ID > 0)
    //    {
    //        var res = await _service.SaveAmazonAccessToken(req);
    //        return res.State ? Success(data: res.Data!) : Error(message: res.Msg);
    //    }
    //    else
    //    {
    //        return Error("参数传递失败");
    //    }
    //}

    [HttpPost]
    public async Task<ResultStruct> RefreshAccessToken(IdDto req)
    {
        if (req.ID <= 0) return Error("参数传递失败");
        var res = await _service.RefreshAccessToken(req);
        return res.State ? Success(data: res.State) : Error(message: res.Msg);
    }

    #endregion

    #endregion


    //编辑/添加其他类型店铺
    public async Task<ResultStruct> editOther(EditOtherStoreDto req)
    {
        var res = await _service.EditOtherStore(req);
        return res.State ? Success() : Error(message: res.Msg);
    }

    /// <summary>
    /// 获取单个店铺信息
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> GetStoreInfo(IdDto req)
    {
        if (req.ID <= 0)
        {
            return Error(message: "缺少参数: ID");
        }

        var info = await _service.GetInfoById(req.ID);
        if (info != null)
        {
            // 其他类型店铺需转参数
            if (info.Platform == Platforms.OTHERS)
            {
                info.Data = info.GetOtherData()!.Note;
            }

            return Success(info);
        }

        return Error();
    }

    public async Task<ResultStruct> SetSort(SetSortDto setSortDto)
    {
        var data = await _service.SetSort(setSortDto);
        return data.State ? Success() : Error();
    }


    //查询所有有效用户组信息 & 分配用户组
    public async Task<ResultStruct> AmazonAssign(AmzonAssingDto amzonAssing,
        [FromServices] ILogger<StoreController> logger)
    {
        #region MyRegion

        //检查权限
        //if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->sessionData->getRule()->getStore()->getBiter()->hasStoreGroup()) {
        //    return error(getlang('public.authfailed'));
        //}

        #endregion

        DateTime Time = DateTime.Now;

        try
        {
            var correlationStoreId = amzonAssing.Id;
            if (!await _storeGroupService.DelAssign(correlationStoreId))
            {
                logger.LogError("无法重置店铺[{ID}]权限,请联系管理员", correlationStoreId);
                return Error(message: "无法重置店铺权限,请联系管理员");
            }

            List<UserStore> lst = new();
            foreach (var CorrelationUserID in amzonAssing.checkList)
            {
                UserStore userStore = new(HttpContext.Session);
                userStore.CorrelationUserID = CorrelationUserID;
                userStore.CorrelationStoreID = correlationStoreId;
                userStore.Platform = Platforms.AMAZON;
                userStore.IsAutoCreated = false;
                userStore.CreatedAt = Time;
                userStore.UpdatedAt = Time;
                lst.Add(userStore);
            }

            await _storeGroupService.insert(lst.ToArray());
            //$this->delStorePersonRedis();
            return Success("分配完成");
        }
        catch (Exception e)
        {
            return Error(message: e.Message);
        }
    }

    /// <summary>
    /// 获取已分配的用户组列表
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> getGroupList(int storeId)
    {
        if (storeId <= 0) return Error(message: "缺少参数: StoreId");
        var groupIds = await _storeGroupService.FindGroup(storeId);
        var group = await _groupService.GetGroupList(BaseModel.StateEnum.Open);
        return Success(new
        {
            group_id = groupIds,
            group
        });
    }

    /// <summary>
    /// 获取上传产品分类
    /// </summary>
    /// <returns></returns>
    //[Route("getGroupList")]
    [HttpGet]
    public ResultStruct getProductCategory()
    {
        /*
        return success('', [
            'types'   => config('producttype'),
            'ext'     => config('exttype'),
            'version' => "1.0.1"
        ]);
        */

        return Success(new
        {
            //todo 上传配置文件
            types = new { },
            //todo 上传配置文件
            ext = new { },
            version = "1.0.1"
        });
    }


    /// <summary>
    /// 删除店铺
    /// </summary>
    /// <param name="Id"></param>
    /// <returns></returns>
    public async Task<ResultStruct> deleteStore(int Id)
    {
        if (Id < 0) return Error("缺少参数");
        try
        {
            var data = await _service.GetInfoById(Id);
            if (data != null)
            {
                await _service.del(data);
            }

            return Success("删除成功");
        }
        catch (Exception e)
        {
            return Error("删除失败");
        }
    }


    /// <summary>
    /// 添加多店铺
    /// </summary>
    /// <param name="addMultiStore"></param>
    /// <returns></returns>
    //public async Task<ResultStruct> AddMwsMultiStore(MwsStoreDto addMultiStore)
    //{
    //    //检查权限
    //    //if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->sessionData->getRule()->getStore()->getBiter()->hasStoreAdd()) {
    //    //    return error(getlang('public.authfailed'));
    //    //}
    //    if (addMultiStore.checkList.Count() <= 0)
    //    {
    //        return Error(message: "请选择国家");
    //    }

    //    var o = await _service.nAddMultiStore(addMultiStore);
    //    return Success();
    //}

    /// <summary>
    /// 添加多店铺
    /// </summary>
    /// <param name="addMultiStore"></param>
    /// <returns></returns>
    //public async Task<ResultStruct> AddMwsSingleStore(MwsStoreDto addMultiStore)
    //{
    //    //检查权限
    //    //if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->sessionData->getRule()->getStore()->getBiter()->hasStoreAdd()) {
    //    //    return error(getlang('public.authfailed'));
    //    //}
    //    if (addMultiStore.NationId <= 0)
    //    {
    //        return Error(message: "请选择国家");
    //    }

    //    var o = await _service.nAddMultiStore(addMultiStore);
    //    return Success();
    //}


    /// <summary>
    /// 当前公司指定id亚马逊店铺绑定信息
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    //public async Task<ResultStruct> getAmazonInfo(IdDto req, [FromServices] AmazonKeyCache amazonKeyCache)
    //{
    //    if (req.ID <= 0)
    //    {
    //        return Error(message: "缺少参数: ID");
    //    }
    //    var info = await _service.GetInfoById(req.ID);
    //    if (info == null || info.Platform != Platforms.AMAZON || info.Flag != Models.DB.Stores.StoreRegion.Flags.NO ||
    //        info.IsDel ||
    //        info.CompanyID != _sessionProvider.Session!.GetCompanyID())
    //    {
    //        return Error(message: "店铺不存在");
    //    }

    //    if(info.PlatFormIds.IsNull())
    //    {
    //        return Error(message: "店铺下没有国家市场信息");
    //    }

    //    var amazonStruct = info.GetAmazonData()!;
    //    if (amazonStruct.Choose == Chooses.BINDMODE_THIRDPART)
    //    {
    //        var keyId = amazonStruct.KeyID;
    //        var amazonNation = _PlatformDataCache.List(Platforms.AMAZON)//.Where(pd => pd.NationId == info.NationId)
    //            .FirstOrDefault();

    //        if (amazonNation is null || amazonNation.GetAmazonData()!.KeyId != keyId)
    //        {
    //            //几乎不会出现这种情况，除非修改了PlatformData中的KeyID，需要运维具体情况具体分析来修复修改KeyID造成的数据不同步的情况
    //            return Error(message: "Can Not Find Store Key Id");
    //        }
    //        var amazonKey = amazonKeyCache.Get(keyId);
    //        if (amazonKey is null)
    //        {
    //            return Error(message: "Can Not Find Store Key");
    //        }
    //        //amazonStruct.GetAWSAccessKeyId 
    //        amazonStruct.MWSSecretKey = amazonKey.SecretKey;
    //    }

    //    return Success();
    //} 

    #region Shopee

    [HttpPost]
    public async Task<ResultStruct> EditShopee(ShopeeAndAmazonSpStoreViewModel req)
    {
        BaseService.ReturnStruct res = new BaseService.ReturnStruct();
        if (req.ID > 0)
        {
            res = await _service.EditShopeeStore(req);
        }
        else
        {
            res = await _service.AddShopeeStore(req);
        }

        return res.State ? Success() : Error(message: res.Msg);
    }


    /// <summary>
    /// Shopee店铺列表
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> IndexShopeeList([FromQuery] ShopeeStoreListDto req)
    {
        var list = await _service.IndexShopeelist(req);
        return Success(new
        {
            store = list,
        });
    }

    /// <summary>
    /// get shopee auth  url
    /// </summary>
    /// <param name="UserId"></param>
    /// <returns></returns>
    public async Task<ResultStruct> GetShopeeUrl(IdDto req)
    {
        if (req.ID <= 0)
        {
            return Error(message: "缺少参数: ID");
        }
        else
        {
            var res = await _service.GetShopeeUrl(req.ID);
            return res.State
                ? Success(new
                {
                    success = true,
                    data = res.Data!,
                })
                : Error(
                    new
                    {
                        success = false,
                        message = res.Msg,
                    });
        }
    }

    /// <summary> 
    /// 当前公司指定idshopee绑定的店铺信息
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    public async Task<ResultStruct> getShopeeInfo(IdDto req)
    {
        if (req.ID <= 0)
        {
            return Error(message: "缺少参数: ID");
        }

        var info = await _service.GetInfoById(req.ID);
        if (info == null || info.Platform != Platforms.SHOPEE || info.Flag != StoreRegion.Flags.NO ||
            info.IsDel)
        {
            return Error(message: "店铺不存在");
        }

        return Success(new { info = info });
    }

    [HttpPost]
    public async Task<ResultStruct> SaveShopeeAccessToken(ShopeeCode req)
    {
        if (!string.IsNullOrWhiteSpace(req.Code) && req.ID > 0 && (req.ShopId > 0 || req.MainAccountId > 0))
        {
            var res = await _service.SaveShopeeAccessToken(req);
            return res.State ? Success(data: res.Data!) : Error(message: res.Msg);
        }
        else
        {
            return Error("参数传递失败");
        }
    }

    #endregion


    public async Task<ResultStruct> GetContinentStores(string platformValue)
    {
        if (platformValue == "8")
        {
            var storeList = await _service.CurrentUserGetStores();
            var continents = storeList
                .Where(s => s.Continent > 0 && s.IsDel == false && s.IsVerify == IsVerifyEnum.Certified &&
                            s.State == BaseModel.StateEnum.Open).GroupBy(s => s.Continent).Select(g => new
                {
                    label = Extensions.EnumExtension.GetDescription(g.Key),
                    value = g.Key,
                    children = g.Select(gg => new
                    {
                        label = gg.Name,
                        value = gg.ID,
                    })
                });
            return Success(await Task.FromResult(new { continents }));
        }
        else
        {
            return Error("暂未对接该平台");
        }
    }
}