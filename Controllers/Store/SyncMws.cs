using ERP.Extensions;
using ERP.Interface;
using Microsoft.AspNetCore.Mvc;
using Ryu.Extensions;

namespace ERP.Controllers.Store;

using static Models.Stores.SyncMws;
using SyncMwsService = Services.Stores.SyncMws;
//[Route("api/[controller]")]
//[ApiController]
public class SyncMws : BaseController
{
    public SyncMws()
    {
    }
    /**
     * 获取权限
     * @return JsonResponse
     * @version 20200805
     */
    public async Task<ResultStruct> getRule()
    {
        //$biter = $this->sessionData->getRule()->getStore()->getBiter();
        
        object data = new();

        //if (Concierge == Models.User.Concierges.Own) {
            data = new
            {
                progressAdd=true,
                progressEdit=true,
                progressDistribution = true,
                progressUpload       = true,
                progressPlan         = true,
            };
        //} else
        //{
        //    $data = [
        //        'progressAdd'          => $biter->hasProgressAdd(),
        //        'progressEdit'         => $biter->hasProgressEdit(),
        //        'progressDistribution' => $biter->hasProgressDistribution(),
        //        'progressUpload'       => $biter->hasProgressUpload(),
        //        'progressPlan'         => $biter->hasProgressPlan(),
        //    ];
        //}

        return await Task.FromResult(Success());

    }//end getRule()

    [HttpGet]
    public async Task<ResultStruct> index()
    {
        #region MyRegion
        /************************************************  
        ================/api/syncMws/index
select count(*) as aggregate from `sync_mws` where `i_store_id` in (?, ?, ?) and `t_state` in (?, ?) and `t_eanupc_status` = ?
select * from `sync_mws` where `i_store_id` in (?, ?, ?) and `t_state` in (?, ?) and `t_eanupc_status` = ? order by `updated_at` desc limit * offset *
select `id`, `t_uploading`, `i_pid` from `sync_product` where `id` in (?)
update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
update `store` set `d_last_auto_time` = ?, `store`.`updated_at` = ? where `id` = ?

        ************************************************/

        //$data = $request->validated();
        //$msg = [];
        //if (isset($data['action']) && $data['action'] == 'create_upload_task') {
        //    $res = (new GetStore)->handle('', '', '', $this->getSessionObj());
        //    $res = $res->original;
        //    if (!empty($res['Data']))
        //    {
        //        $sessionObj = $this->getSessionObj();
        //        $user_id = $sessionObj->user_id;
        //        $company_id = $sessionObj->company_id;
        //        $oem_id = $sessionObj->oem_id;
        //        $group_id = $sessionObj->group_id;
        //        $upload_origin_state = SyncMwsModel::STATE_WAITING;
        //        $storeIds = json_decode($res['Data']);
        //        try
        //        {
        //            if (is_array($storeIds))
        //            {
        //                foreach ($storeIds as $sid) {
        //                    $resStatus = Get::createUploadTasks(true, $sid->ID, $user_id, $company_id, $oem_id, $group_id, $upload_origin_state, $this->time);
        //                    if (!is_null($resStatus))
        //                    {
        //                        $msg[] = $resStatus;
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                $msg[] = "storeIds不是一个有效数组,数据=>{$res['Data']}";
        //            }
        //        }
        //        catch (Exception $e) {
        //            $msg[] = $e->getMessage();
        //        }
        //        }
        //    }

        //    if ($msg) {
        //        Log::channel('TasksError')->info(
        //            '================上传错误日志！================ (构建任务时)',
        //            $logData = [
        //                'route'        => request()->getRequestUri() ?? '',
        //                'errorMsg'     => $msg,
        //                'codeLine'     => 'Line131',
        //                'request_data' => $request,
        //                'session_data' => $this->getSessionObj(),

        //            ]
        //        );
        //    }

        //$list = $this->service->list($data);

        //return success('', $list);
        #endregion


        return await Task.FromResult(Success());
    }

    [Route("add")]
    [HttpPost]
    public async Task<ResultStruct> add()
    {
        /************************************************  
        ================/api/syncMws/add
select * from `product` where `id` = ? and `i_company_id` = ? and `i_oem_id` = ? limit *
insert into `product` (`i_oem_id`, `i_company_id`, `i_user_id`, `i_group_id`, `i_category_id`, `i_sub_category_id`, `u_lp_n`, `u_lp_s`, `u_lp_k`, `u_lp_d`, `d_source`, `d_hash_code`, `d_languages`, `b_language`, `b_type`, `u_price`, `i_platform_id`, `u_platform`, `d_attribute`, `d_variants`, `d_ext`, `d_images`, `u_image`, `c_size`, `d_pid`, `created_at`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
update `resource` set `c_reuse` = `c_reuse` + *, `resource`.`updated_at` = ? where `id` in (?, ?, ?, ?, ?)
insert into `sync_product` (`i_pid`, `i_source_id`, `u_product_name`, `u_img_url`, `d_sku`, `t_sku_type`, `t_sku_suffix`, `t_title_suffix`, `t_product_type`, `u_category_id`, `u_sub_category_id`, `t_already_upload`, `t_uploading`, `i_user_id`, `i_group_id`, `i_company_id`, `i_oem_id`, `t_eanupc_status`, `d_type_data`, `d_type_data_id`, `created_at`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
insert into `sync_mws` (`created_at`, `d_browser_node_id`, `d_country_of_origin`, `d_lead_time`, `d_sku_change_data`, `i_company_id`, `i_group_id`, `i_oem_id`, `i_spid`, `i_store_id`, `i_user_id`, `t_state`, `u_language`, `u_product`, `u_product_name`, `u_store_name`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
update `store` set `d_last_auto_time` = ?, `store`.`updated_at` = ? where `id` = ?
select `id`, `d_button_name` from `waybill` where ((`i_company_id` = ? and `i_operate_company_id` = ?) or `i_waybill_company_id` = ?) and `d_button_name` in (?, ?) order by `updated_at` asc limit *
select * from `waybill` where `waybill`.`id` = ? limit *
update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `d_tracking` = ?, `waybill`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
update `store` set `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
select `i_store_id` from `store_group` where `i_group_id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *

        ************************************************/
        //$data = $request->validated();
        //if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->sessionData->getRule()->getStore()->getBiter()->hasProgressAdd()
        //) {
        //    return error(getlang('public.authfailed'));
        //}

        //DB::beginTransaction();
        //try
        //{
        //    $res = $this->service->add($data);
        //    DB::commit();
        //}
        //catch (Exception $e) {
        //    DB::rollBack();
        //    return error('', $e->getMessage());
        //}//end try

        //if (!$res['state']) {
        //    return error($res['msg']);
        //}
        //return success($res['msg']);
        return await Task.FromResult(Success());
    }

    public async Task<ResultStruct> edit()
    {
        /*
        $data = $request->validated();
        if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->sessionData->getRule()->getStore()->getBiter()->hasProgressEdit()
        ) {
            return error(getlang('public.authfailed'));
        }

        DB::beginTransaction();
        try {

            $res = $this->service->edit($data);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return error('', $e->getMessage());
        }//end try

        if (!$res['state']) {
            return error($res['msg']);
        }

        return success('');
        */
        return await Task.FromResult(Success());
    }

    public async Task<ResultStruct> state()
    {
        /*
        $data = $request->validated();
        // @todo 添加权限
//        if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->sessionData->getRule()->getStore()->getBiter()->hasProgressEdit()
//        ) {
//            return error('无权访问');
//        }

        DB::beginTransaction();
        try {

            $res = $this->service->switchState($data['id'], $data['state']);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return error('', $e->getMessage());
        }//end try

        if (!$res['state']) {
            return error($res['msg']);
        }

        return success('');
        */
        return await Task.FromResult(Success());
    }

    public async Task<ResultStruct> info()
    {
        /************************************************  
================/api/syncMws/info
select * from `sync_mws` where `id` = ? and `i_store_id` in (?, ?, ?) limit *
select * from `sync_product` where `id` = ? limit *
select `id`, `i_category_id`, `i_sub_category_id`, `i_distribution_category_id`, `i_distribution_sub_category_id`, `u_lp_n`, `u_lp_k`, `u_lp_s`, `u_lp_d`, `u_price`, `u_quantity`, `d_languages`, `b_language`, `d_source`, `d_ext`, `d_attribute`, `d_variants`, `d_images`, `b_type`, `updated_at`, `d_pid`, `d_coin`, `d_rate` from `product` where (`i_company_id` = ? and `id` = ? and `i_oem_id` = ?) limit *
select `d_temp_name`, `i_browser_node_id`, `u_browser_ui`, `t_level`, `i_nation_id`, `d_type_data` from `category_node_default` where `d_type_hash` = ? and (`t_level` = ? or (`i_company_id` = ? and `t_level` = ?) or (`i_company_id` = ? and `i_group_id` = ? and `t_level` = ?)) order by `t_level` desc, `d_sort` asc
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
update `store` set `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `amazon_order` where `i_company_id` = ? and `i_oem_id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `t_entry_mode`, `id`, `d_receiver_nation`, `d_receiver_nation_short`, `u_url`, `d_earliest_ship_time`, `d_order_no`, `d_pay_money`, `i_company_id`, `d_product_num`, `t_order_state`, `d_create_time`, `t_shipping_type`, `d_fake_product_time`, `d_true_product_time`, `i_store_id`, `id`, `d_coin`, `u_truename`, `d_receiver_name`, `d_receiver_email`, `b_progress`, `d_latest_ship_time`, `i_merge_id`, `d_process_waybill`, `d_process_purchase`, `i_purchase_company_id`, `i_waybill_company_id`, `created_at`, `d_rate`, `t_is_batch_ship`, `d_seller_id`, `d_profit`, `d_invoice` from `amazon_order` where `i_company_id` = ? and `i_oem_id` = ? order by `d_create_time` desc, `id` desc limit * offset *
select `id`, `i_order_id`, `d_purchase_tracking_number`, `d_purchase_order_no` from `purchase` where `i_order_id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `i_order_id`, `d_waybill_order`, `d_express` from `waybill` where `i_order_id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select * from `product` where `id` = ? and b_type & * limit *
insert into `temp_images` (`d_oss_path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `i_product_id`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?)
select * from `category` where (`i_company_id` = ? and `i_oem_id` = ? and `t_type` = ? and `t_level` = ?)
select * from `company` where `company`.`id` = ? limit *
select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
update `store` set `d_last_auto_time` = ?, `store`.`updated_at` = ? where `id` = ?
select `id`, `d_button_name` from `waybill` where ((`i_company_id` = ? and `i_operate_company_id` = ?) or `i_waybill_company_id` = ?) and `d_button_name` in (?, ?) order by `updated_at` asc limit *
select count(*) as count, `i_store_id` from `sync_mws` where `t_state` in (?, ?) and `i_company_id` = ? and `i_oem_id` = ? group by `i_store_id`

       ************************************************/
        /*
        $data = $request->validated();

        $storeIds = array_column($this->service->getStores(), 'id');
        $row = SyncMwsModel::query()->where('id', $data['id'])->whereIn('store_id', $storeIds)->first();
        if (!$row) {
            return error(getlang('store.uploadprogress.InfoNotExist'));
        }
        $sp = \App\Models\Store\SyncProduct::query()->where('id', $row->spid)->first();
        if (!$sp) {
            return error(getlang('store.uploadprogress.ProductInfoNotExist'));
        }
        $res = $this->productService->info($sp->pid);
        if (!$res['state']) {
            return error(getlang('store.uploadprogress.ProductInfoNotExist'));
        }

        $product = $res['data']['info'];


        if ($product['ext']) {
            foreach ($product['ext'] as $v) {
                if ($v['key'] == 'RecommendedBrowseNodes') {
                    $product['ext'] = $v['value'];
                }
            }
        } else {
            $product['ext'] = null;
        }
        $types = json_decode($sp->type_data, true);
        $categories = (new \App\Service\Store\CategoryNodeDefault())->getAllList($types);
        return success(
            '',
            [
                'store_id'            => $row->store_id,
                'types'               => $types,
                'sku'                 => $sp->sku,
                'goods_id'            => $sp->pid,
                'lp_n'                => $row->product_name,
                'image'               => $row->product['image'],
                'sku_type'            => $sp->sku_type,
                'sku_suffix'          => $sp->sku_suffix == 1,
                'title_suffix'        => $sp->title_suffix == 1,
                'browser_node_id'     => $row->browser_node_id,
                'country_of_origin'   => $row->country_of_origin,
                'language'            => $row->language,
                'languagePack'        => ($res['data']['languagePack'] ?? []),
                'attribute'           => $product['attribute'],
                'variants'            => $product['variants'],
                'images'              => $product['images'],
                'node'                => ($product['ext'] ?? null),
                'state'               => $row->state,
                'lead_time'           => $row->lead_time,
                'browser_node_nation' => RecommendedBrowseNodesModel::BROWSE_NODE_NATION,
                'categories'          => $categories,
            ]
        );
        */
        return await Task.FromResult( Success());
    }

    public async Task<ResultStruct> delete()
    {
        /************************************************  
================/api/syncMws/delete
select * from `sync_mws` where `id` = ? and `i_store_id` in (?, ?, ?) and `t_state` in (?, ?, ?, ?) limit *
select * from `sync_upload` where `id` = ? limit *
select count(*) as aggregate from `sync_mws` where `i_spid` = ?
select * from `sync_product` where `sync_product`.`id` = ? limit *
select * from `sync_product` where `id` in (?)
delete from `sync_product` where `id` = ?
delete from `product` where `id` = ?
update `resource` set `c_reuse` = `c_reuse` - *, `resource`.`updated_at` = ? where `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select * from `resource` where `c_reuse` = ? and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
select * from `product` where `id` = ? and b_type & * limit *
delete from `sync_mws` where `id` = ? and `i_store_id` in (?, ?, ?) and `t_state` in (?, ?, ?, ?) limit *
insert into `temp_images` (`d_oss_path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `i_product_id`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?)
select count(*) as aggregate from `sync_mws` where `i_store_id` in (?, ?, ?) and `t_state` = ?
select * from `sync_mws` where `i_store_id` in (?, ?, ?) and `t_state` = ? order by `updated_at` desc limit * offset *
select `id`, `t_uploading`, `i_pid` from `sync_product` where `id` in (?, ?, ?, ?, ?)
select `id`, `d_button_name` from `waybill` where ((`i_company_id` = ? and `i_operate_company_id` = ?) or `i_waybill_company_id` = ?) and `d_button_name` in (?, ?) order by `updated_at` asc limit *
select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
update `store` set `d_last_auto_time` = ?, `store`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
update `store` set `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select * from `store` where `i_company_id` = ? and `t_flag` = ? and `t_is_del` = ? and `t_is_verify` = ?
update `sync_upload` set `d_pricing_id` = ?, `t_pricing_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
insert into `sync_session_log` (`i_upload_id`, `d_index`, `d_state`, `i_company_id`, `i_user_id`, `i_group_id`, `i_oem_id`, `i_last_operate_id`, `d_count`, `d_server_old_state`, `d_client_old_state`, `d_invoke_api`, `d_index_text`, `d_state_text`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
insert into `sync_alive_session` (`i_upload_id`, `d_index`, `d_state`, `i_company_id`, `i_user_id`, `i_group_id`, `i_oem_id`, `i_last_operate_id`, `d_count`, `d_server_old_state`, `d_client_old_state`, `d_invoke_api`, `d_index_text`, `d_state_text`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select column_name as `column_name` from information_schema.columns where table_schema = ? and table_name = ?
insert into `sync_invoke_api_log` (`i_upload_id`, `d_request_data`, `i_upload_operate_id`, `i_user_id`, `i_company_id`, `i_group_id`, `i_oem_id`, `t_api`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
update `sync_alive_session` set `d_state` = ?, `d_state_text` = ?, `i_last_operate_id` = ?, `i_user_id` = ?, `d_count` = ?, `sync_alive_session`.`updated_at` = ? where (`i_upload_id` = ? and `d_index` = ?)
select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?, ?, ?, ?, ?)
insert into `resource` (`created_at`, `d_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
delete from `temp_images` where `id` in (?, ?, ?, ?, ?, ?)
update `product` set `u_lp_k` = ?, `u_lp_s` = ?, `u_lp_d` = ?, `b_type` = ?, `u_price` = ?, `u_quantity` = ?, `d_source` = ?, `d_ext` = ?, `d_attribute` = ?, `d_variants` = ?, `d_images` = ?, `u_image` = ?, `d_pid` = ?, `d_pid_hash_code` = ?, `c_size` = ?, `d_coin` = ?, `d_rate` = ?, `product`.`updated_at` = ? where `id` = ?
update `counter_company_business` set `c_product_num` = `c_product_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` = ?
update `counter_company_business` set `c_report_product_num` = `c_report_product_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` in (?, ?, ?, ?)
update `counter_user_business` set `c_product_num` = `c_product_num` + *, `counter_user_business`.`updated_at` = ? where `i_user_id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as count, `i_store_id` from `sync_mws` where `t_state` in (?, ?) and `i_company_id` = ? and `i_oem_id` = ? group by `i_store_id`
select `id`, `t_uploading`, `i_pid` from `sync_product` where `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?) order by `d_last_auto_time` asc limit *
update `waybill` set `waybill`.`updated_at` = ? where `id` in (?, ?, ?, ?, ?)
select * from `waybill` where `waybill`.`id` = ? limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
select `d_order_no` from `amazon_order` where (`i_store_id` = ? and `t_order_state` = ? and `i_company_id` = ? and `t_entry_mode` = ?) limit *
update `store` set `d_last_auto_update_time` = ?, `store`.`updated_at` = ? where `id` = ?
select * from `task` where (`i_oem_id` = ? and `i_user_id` = ? and `i_group_id` = ? and `d_hash_code` = ?) limit *
select count(*) as aggregate from `product` where (`d_hash_code` = ? and `i_company_id` = ?) and !(b_type&*) AND !(b_type&*) AND !(b_type&*)
insert into `product` (`i_oem_id`, `i_company_id`, `i_user_id`, `i_group_id`, `i_task_id`, `u_lp_n`, `u_lp_k`, `u_lp_s`, `u_lp_d`, `d_source`, `d_hash_code`, `d_languages`, `b_language`, `b_type`, `u_price`, `i_platform_id`, `u_platform`, `d_attribute`, `d_variants`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `waybill`.`updated_at` = ? where `id` = ?
update `sync_upload` set `d_image_id` = ?, `t_image_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select * from `store` where `t_flag` = ? and `t_is_del` = ? and `t_is_verify` = ? and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?)
select * from `sync_upload` where `sync_upload`.`id` = ? limit *
select `id`, `i_category_id`, `i_sub_category_id`, `i_distribution_category_id`, `i_distribution_sub_category_id`, `u_lp_n`, `u_lp_k`, `u_lp_s`, `u_lp_d`, `u_price`, `u_quantity`, `d_languages`, `b_language`, `d_source`, `d_ext`, `d_attribute`, `d_variants`, `d_images`, `b_type`, `updated_at`, `d_pid`, `d_coin`, `d_rate` from `product` where (`i_company_id` = ? and `id` = ? and `i_oem_id` = ?) limit *
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
update `sync_upload` set `d_relationship_id` = ?, `t_relationship_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `t_uploading`, `i_pid` from `sync_product` where `id` in (?, ?)
insert into `reporter_request` (`created_at`, `d_action`, `d_cpu_time`, `d_method`, `d_module`, `d_name`, `d_namespace`, `d_net_time`, `d_request_id`, `d_request_time`, `d_uri`, `i_company_id`, `i_group_id`, `i_oem_id`, `i_user_id`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `i_operate_id`, `updated_at` from `sync_upload` where (`i_store_id` = ? and `t_state` = ? and `updated_at` < ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?)
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?, ?, ?, ?)
insert into `resource` (`created_at`, `d_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
delete from `temp_images` where `id` in (?, ?, ?, ?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
insert into `amazon_order` (`d_refund`, `d_fee`, `t_order_state`, `d_order_no`, `d_order_no_hash`, `t_shipping_type`, `d_user_name`, `d_pay_time`, `d_receiver_phone`, `i_receiver_nation_id`, `d_receiver_nation_short`, `d_receiver_nation`, `d_receiver_province`, `d_receiver_city`, `d_receiver_district`, `d_receiver_address`, `d_receiver_address*`, `d_receiver_address*`, `d_receiver_zip`, `d_receiver_name`, `d_receiver_email`, `i_store_id`, `d_seller_id`, `d_order_money`, `d_pay_money`, `d_create_time`, `d_coin`, `d_delivery_time`, `d_last_time`, `d_delivery`, `d_delivery_no`, `d_rate`, `d_latest_ship_time`, `d_earliest_ship_time`, `i_company_id`, `i_user_id`, `i_group_id`, `i_oem_id`, `d_default_ship_from_location_address`, `u_url`, `d_goods`, `d_product_num`, `d_product_money`, `b_progress`, `d_delivery_num`, `d_profit`, `u_truename`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
insert into `amazon_order_action` (`t_order_status`, `d_order_status_text`, `i_order_id`, `d_action`, `i_user_id`, `i_company_id`, `i_group_id`, `i_oem_id`, `d_user_name`, `d_truename`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
update `counter_group_business` set `c_order_num` = c_order_num+*, `c_order_total` = c_order_total+*.*, `c_order_total_fee` = c_order_total_fee+*, `c_order_total_refund` = c_order_total_refund+*, `counter_group_business`.`updated_at` = ? where `i_group_id` = ?
update `counter_store_business` set `c_order_total` = c_order_total+*.*, `c_order_total_fee` = c_order_total_fee+*, `c_order_total_refund` = c_order_total_refund+*, `counter_store_business`.`updated_at` = ? where `i_store_id` = ?
update `counter_company_business` set `c_order_num` = c_order_num+*, `c_order_total` = c_order_total+*.*, `c_order_total_fee` = c_order_total_fee+*, `c_order_total_refund` = c_order_total_refund+*, `counter_company_business`.`updated_at` = ? where `i_company_id` = ?
update `counter_company_business` set `c_report_order_num` = `c_report_order_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` in (?, ?)
update `counter_real_finance` set `c_order_total` = c_order_total+*.*, `c_order_total_fee` = c_order_total_fee+*, `c_order_total_refund` = c_order_total_refund+*, `counter_real_finance`.`updated_at` = ? where `i_company_id` = ? and `i_oem_id` = ? and `d_month` = ?
update `store` set `d_last_order_time` = ?, `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?, ?, ?, ?)
select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?, ?, ?)
insert into `resource` (`created_at`, `d_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
delete from `temp_images` where `id` in (?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?) order by `d_last_auto_update_time` asc limit *
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
update `sync_upload` set `t_product_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
select * from `user` where `d_username` = ? limit *
select SUM(c_request) as total_request, SUM(c_cpu) as total_cpu, SUM(c_memory) as total_memory, SUM(c_network) as total_network, SUM(c_disk) as total_disk, SUM(c_pic_api) as total_pic_api, SUM(c_translate_api) as total_translate_api from `counter_request` where `i_company_id` = ? limit *
select * from `company_reduce_request` where `i_company_id` = ? and `i_oem_id` = ? limit *
select `c_user_num`, `c_product_num`, `c_order_num`, `c_purchase_num`, `c_waybill_num`, `c_store_num` from `counter_company_business` where `i_company_id` = ? and `i_oem_id` = ? limit *
update `user` set `login_time` = ?, `user`.`updated_at` = ? where `id` = ?
select `id`, `i_rule_id` from `rule_template` where `rule_template`.`id` = ? limit *
delete from `product` where (`i_user_id` = ? and `i_oem_id` = ?) and b_type & *
select * from `user` where `id` = ? limit *
select * from `theme` where `theme`.`id` in (?)
select * from `user_online`
select count(*) as aggregate from `user_online` where `i_user_id` = ?
insert into `login_log` (`i_user_id`, `d_username`, `i_oem_id`, `t_type`, `created_at`, `updated_at`, `d_login_ip`, `d_ext`) values (?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_sku_change_data`, `i_spid` from `sync_mws` where `id` = ? limit *
update `sync_mws` set `d_sku_change_data` = ?, `sync_mws`.`updated_at` = ? where (`id` = ?)
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
update `sync_upload` set `t_inventory_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
update `sync_upload` set `t_image_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
update `sync_mws` set `d_sku_change_data` = ?, `t_state` = ?, `sync_mws`.`updated_at` = ? where (`id` = ?)
insert into `sync_user` (`i_upload_id`, `i_old_user_id`, `i_new_user_id`, `d_last_updated`, `i_company_id`, `i_group_id`, `i_oem_id`, `t_is_changed`, `t_is_in_*_min`, `t_type`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
delete from `temp_images` where `id` in (?, ?, ?)
select count(*) as aggregate from `company` where `t_state` = ? and `i_report_id` = ? and `id` != ?
select `c_user_num`, `c_product_num`, `c_order_num`, `c_purchase_num`, `c_waybill_num`, `c_store_num`, `c_report_user_num`, `c_report_product_num`, `c_report_order_num`, `c_report_purchase_num`, `c_report_waybill_num`, `c_report_store_num`, `c_distribution_order_num`, `c_distribution_purchase_num`, `c_distribution_waybill_num` from `counter_company_business` where `i_company_id` = ? limit *
select `i_user_id`, `c_user_num`, `c_product_num`, `c_order_num`, `c_purchase_num`, `c_waybill_num`, `c_store_num` from `counter_user_business` where `i_user_id` = ? limit *
select * from `notice` where (`t_state` = ? and `t_type` = ?) or (`t_state` = ? and `i_oem_id` = ? and `i_company_id` != ? and `t_type` = ?) or (`t_state` = ? and `i_company_id` = ? and `i_oem_id` = ? and `t_type` = ?) order by `created_at` desc limit * offset *
select * from `task` where (`i_company_id` = ? and `i_group_id` = ? and `i_oem_id` = ?) order by `id` desc limit * offset *
select count(*) as aggregate from `amazon_order` where `i_company_id` = ? and `i_oem_id` = ? and `t_order_state` = ? and `d_latest_ship_time` < ? and `d_latest_ship_time` > ?
select `t_entry_mode`, `id`, `d_receiver_nation`, `d_receiver_nation_short`, `u_url`, `d_earliest_ship_time`, `d_order_no`, `d_pay_money`, `i_company_id`, `d_product_num`, `t_order_state`, `d_create_time`, `t_shipping_type`, `d_fake_product_time`, `d_true_product_time`, `i_store_id`, `id`, `d_coin`, `u_truename`, `d_receiver_name`, `d_receiver_email`, `b_progress`, `d_latest_ship_time`, `i_merge_id`, `d_process_waybill`, `d_process_purchase`, `i_purchase_company_id`, `i_waybill_company_id`, `created_at`, `d_rate`, `t_is_batch_ship`, `d_seller_id`, `d_profit`, `d_invoice` from `amazon_order` where `i_company_id` = ? and `i_oem_id` = ? and (b_progress&*)>>*=* order by `d_create_time` desc, `id` desc limit * offset *
select `id`, `i_order_id`, `d_purchase_tracking_number`, `d_purchase_order_no` from `purchase` where `i_order_id` in (?, ?, ?, ?)
select count(*) as aggregate from `amazon_order` where `i_company_id` = ? and `i_oem_id` = ?
select `t_entry_mode`, `id`, `d_receiver_nation`, `d_receiver_nation_short`, `u_url`, `d_earliest_ship_time`, `d_order_no`, `d_pay_money`, `i_company_id`, `d_product_num`, `t_order_state`, `d_create_time`, `t_shipping_type`, `d_fake_product_time`, `d_true_product_time`, `i_store_id`, `id`, `d_coin`, `u_truename`, `d_receiver_name`, `d_receiver_email`, `b_progress`, `d_latest_ship_time`, `i_merge_id`, `d_process_waybill`, `d_process_purchase`, `i_purchase_company_id`, `i_waybill_company_id`, `created_at`, `d_rate`, `t_is_batch_ship`, `d_seller_id`, `d_profit`, `d_invoice` from `amazon_order` where `i_company_id` = ? and `i_oem_id` = ? order by `d_create_time` desc, `id` desc limit * offset *
select `id`, `i_order_id`, `d_purchase_tracking_number`, `d_purchase_order_no` from `purchase` where `i_order_id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)

        ************************************************/
        /*
        $input = $request->validated();
        $storeIds = array_column($this->service->getStores(), 'id');

        $query = SyncMwsModel::query()->where('id', $input['id'])->whereIn('store_id', $storeIds)->editable();
        $row = $query->first();
        if (!$row) {
            return error(getlang('store.syncmws.NoInfo'));
        }
        //查询是在上传中
        $upload_data = \App\Models\Store\SyncUpload::query()->Select(["*"])->where("id", "=", $row->upload_id)->first();
        if ($upload_data && $upload_data->state == \App\Models\Store\SyncUpload::STATE_FALSE) {
            return warning("产品正在上传中,请稍后重试!");
        }

        //查询改产品关联mws数量
        $num = $this->service->getMwsNum($row['spid']);

        DB::beginTransaction();
        try {
            if ($num == 1) {
                //查询关联信息
                $SyncProductService = new SyncProductService();
                //获取syncProduct
                $syncProduct = $SyncProductService->getInfoById($row['spid']);

                if ($syncProduct) {
                    //syncProduct只被用户上传使用  物理删除复制产品信息
                    if ($syncProduct['type'] == 1) {
                        $SyncProductService->deleteSyncProduct($row['spid']);
                        $this->productService->delete($syncProduct['pid']);
                    } else {
                        //更新产品类型为只导出
                        $SyncProductService->removeProductType($row['spid'], 2);
                        $this->productService->removeProductType($syncProduct['pid'], 2);
                    }
                }
            }

            $query->delete();
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            return error(getlang('store.uploadprogress.Retry'), $e->getMessage());
        }

        return success('删除成功');
        */
        return await Task.FromResult(Success());
    }

    //进度详情
    public async Task<ResultStruct> progress()
    {
        /************************************************  
================/api/syncMws/progress
select * from `sync_mws` where `id` = ? and `i_store_id` in (?, ?, ?) and `t_state` in (?, ?, ?) limit *
select * from `sync_upload` where `sync_upload`.`id` = ? limit *
select * from `sync_product` where `sync_product`.`id` = ? limit *
select `id`, `i_category_id`, `i_sub_category_id`, `i_distribution_category_id`, `i_distribution_sub_category_id`, `u_lp_n`, `u_lp_k`, `u_lp_s`, `u_lp_d`, `u_price`, `u_quantity`, `d_languages`, `b_language`, `d_source`, `d_ext`, `d_attribute`, `d_variants`, `d_images`, `b_type`, `updated_at`, `d_pid`, `d_coin`, `d_rate` from `product` where (`i_company_id` = ? and `id` = ? and `i_oem_id` = ?) limit *
select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
update `store` set `d_last_auto_time` = ?, `store`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
update `store` set `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `id`, `d_button_name` from `waybill` where ((`i_company_id` = ? and `i_operate_company_id` = ?) or `i_waybill_company_id` = ?) and `d_button_name` in (?, ?) order by `updated_at` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select * from `waybill` where `waybill`.`id` = ? limit *
update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `waybill`.`updated_at` = ? where `id` = ?
select `id`, `d_title`, `i_user_id`, `t_state` from `work_order` where `i_company_id` = ? and `t_state` in (?, ?) limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select * from `task` where (`i_oem_id` = ? and `i_user_id` = ? and `i_group_id` = ? and `d_hash_code` = ?) limit *
select count(*) as aggregate from `product` where (`d_hash_code` = ? and `i_company_id` = ?) and !(b_type&*) AND !(b_type&*) AND !(b_type&*)
insert into `product` (`i_oem_id`, `i_company_id`, `i_user_id`, `i_group_id`, `i_task_id`, `u_lp_n`, `u_lp_k`, `u_lp_s`, `u_lp_d`, `d_source`, `d_hash_code`, `d_languages`, `b_language`, `b_type`, `u_price`, `i_platform_id`, `u_platform`, `d_attribute`, `d_variants`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
update `product` set `b_type` = b_type | *, `product`.`updated_at` = ? where (`d_hash_code` = ? and `i_company_id` = ?) and !(b_type&*) AND !(b_type&*) AND !(b_type&*)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `d_tracking` = ?, `waybill`.`updated_at` = ? where `id` = ?
select `i_store_id` from `to_sell_task` where `i_company_id` = ? and `t_is_finish` = ? and `i_oem_id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
update `waybill` set `waybill`.`updated_at` = ? where `id` in (?, ?, ?, ?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select * from `product` where `id` = ? and b_type & * limit *
insert into `temp_images` (`d_oss_path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `i_product_id`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?)
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?, ?, ?, ?)
insert into `resource` (`created_at`, `d_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
delete from `temp_images` where `id` in (?, ?, ?, ?, ?)
update `product` set `u_lp_k` = ?, `u_lp_s` = ?, `u_lp_d` = ?, `b_type` = ?, `u_price` = ?, `u_quantity` = ?, `d_source` = ?, `d_ext` = ?, `d_attribute` = ?, `d_variants` = ?, `d_images` = ?, `u_image` = ?, `d_pid` = ?, `d_pid_hash_code` = ?, `c_size` = ?, `d_coin` = ?, `d_rate` = ?, `product`.`updated_at` = ? where `id` = ?
update `counter_company_business` set `c_product_num` = `c_product_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` = ?
update `counter_company_business` set `c_report_product_num` = `c_report_product_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` in (?, ?, ?, ?)
update `counter_user_business` set `c_product_num` = `c_product_num` + *, `counter_user_business`.`updated_at` = ? where `i_user_id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select * from `product` where `id` = ? limit *
select count(*) as aggregate from `product` where (`d_hash_code` = ? and `i_company_id` = ? and `id` != ?) and !(b_type&*) AND !(b_type&*) AND !(b_type&*)
update `product` set `u_lp_d` = ?, `b_type` = ?, `u_price` = ?, `d_variants` = ?, `product`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_oem_id` = ?) and `updated_at` between ? and ?
select `id`, `i_oem_id`, `i_user_id`, `i_company_id`, `i_oem_id`, `i_category_id`, `i_platform_id`, `d_reason`, `u_quantity`, `u_lp_n`, `u_image`, `u_price`, `d_source`, `b_type`, `b_language`, `d_languages`, `updated_at`, `created_at`, `d_coin`, `d_rate` from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_oem_id` = ?) and `updated_at` between ? and ? order by `id` desc limit * offset *
select * from `category` where `category`.`id` in (?)
select * from `task` where `task`.`id` in (?)
select * from `user` where `d_username` = ? limit *
select SUM(c_request) as total_request, SUM(c_cpu) as total_cpu, SUM(c_memory) as total_memory, SUM(c_network) as total_network, SUM(c_disk) as total_disk, SUM(c_pic_api) as total_pic_api, SUM(c_translate_api) as total_translate_api from `counter_request` where `i_company_id` = ? limit *
select * from `company_reduce_request` where `i_company_id` = ? and `i_oem_id` = ? limit *
select `c_user_num`, `c_product_num`, `c_order_num`, `c_purchase_num`, `c_waybill_num`, `c_store_num` from `counter_company_business` where `i_company_id` = ? and `i_oem_id` = ? limit *
update `user` set `login_time` = ?, `user`.`updated_at` = ? where `id` = ?
select `id`, `i_rule_id` from `rule_template` where `rule_template`.`id` = ? limit *
select * from `rule` where `rule`.`id` = ? limit *
select `id`, `i_operate_id`, `updated_at` from `sync_upload` where (`i_store_id` = ? and `t_state` = ? and `updated_at` < ?)
select `id` from `sync_upload` where `i_store_id` = ? and `t_state` = ? and `i_operate_id` = ?
delete from `product` where (`i_user_id` = ? and `i_oem_id` = ?) and b_type & *
select * from `user` where `id` = ? limit *
select * from `theme` where `theme`.`id` in (?)
select * from `user_online`
update `counter_request` set `c_request` = c_request+*, `c_cpu` = c_cpu+*, `c_memory` = c_memory+*, `c_network` = c_network+*, `c_disk` = c_disk+*, `counter_request`.`updated_at` = ? where `i_user_id` = ?
select * from `login_log` where (`i_user_id` = ? and `t_type` = ?) order by `id` desc limit *
update `login_log` set `t_type` = ?, `d_remark` = ?, `d_ext` = ?, `login_log`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `user_online` where `i_user_id` = ?
insert into `login_log` (`i_user_id`, `d_username`, `i_oem_id`, `t_type`, `created_at`, `updated_at`, `d_login_ip`, `d_ext`) values (?, ?, ?, ?, ?, ?, ?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `d_order_no` from `amazon_order` where (`i_store_id` = ? and `t_order_state` = ? and `i_company_id` = ? and `t_entry_mode` = ?) limit *
update `store` set `d_last_auto_update_time` = ?, `store`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_oem_id` = ?) and `i_task_id` = ? and `updated_at` between ? and ?
select `id`, `i_oem_id`, `i_user_id`, `i_company_id`, `i_oem_id`, `i_category_id`, `i_platform_id`, `d_reason`, `u_quantity`, `u_lp_n`, `u_image`, `u_price`, `d_source`, `b_type`, `b_language`, `d_languages`, `updated_at`, `created_at`, `d_coin`, `d_rate` from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_oem_id` = ?) and `i_task_id` = ? and `updated_at` between ? and ? order by `id` desc limit * offset *
select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?, ?, ?, ?, ?, ?, ?)
update `product` set `u_lp_s` = ?, `u_lp_d` = ?, `b_type` = ?, `u_price` = ?, `u_quantity` = ?, `d_source` = ?, `d_ext` = ?, `d_attribute` = ?, `d_variants` = ?, `d_images` = ?, `u_image` = ?, `d_pid` = ?, `d_pid_hash_code` = ?, `c_size` = ?, `d_coin` = ?, `d_rate` = ?, `product`.`updated_at` = ? where `id` = ?
select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?, ?, ?, ?, ?)
delete from `temp_images` where `id` in (?, ?, ?, ?, ?, ?)
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?)
insert into `reporter_request` (`created_at`, `d_action`, `d_cpu_time`, `d_method`, `d_module`, `d_name`, `d_namespace`, `d_net_time`, `d_request_id`, `d_request_time`, `d_uri`, `i_company_id`, `i_group_id`, `i_oem_id`, `i_user_id`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)

        ************************************************/


        /*
        $data = $request->validated();
//        if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->sessionData->getRule()->getStore()->getBiter()->hasProgressPlan()) {
//            return error(getlang('public.authfailed'));
//        }

        $allowStates = [
            SyncMwsModel::STATE_EXECUTING,
            SyncMwsModel::STATE_SUCCESS,
            SyncMwsModel::STATE_ERROR,
        ];
        $storeIds = array_column($this->service->getStores(), 'id');
        $row = SyncMwsModel::query()->where('id', $data['id'])->whereIn('store_id', $storeIds)->whereIn('state', $allowStates)->first();
        if (!$row || !$row->upload) {
            return error(getlang('store.uploadprogress.InfoNotExist'));
        }

        $sessions = \App\Models\Store\SyncUpload::$sessions;
        $res = [];
        if (!empty($row->upload->error_info)) {
            $res['error_info'] = $row->upload->error_info;
        }
        $productInfo = $this->service->getProductInfo($row->spid, $row->language);
        //$productInfo =$this->productService->info($data['id']);
        foreach ($sessions as $key => $item) {
            $stateKey = $key . '_state';
            $dataKey = ucfirst($key) . 'Data';
            $errorMsg = [];
            foreach ($row->sku_change_data as $k => $changeData) {
                $msg = [];
                if ($key === 'product') {
                    if (!$changeData['IsMain']) {
                        continue;
                    }
                    if (!empty($changeData[$dataKey]['Error'])) {
                        $msg['error'] = $changeData[$dataKey]['Error'];
                        if ($row->language == 'default' && isset($productInfo['data']['info']['lp_n'])) {
                            $msg['title'] = $productInfo['data']['info']['lp_n'];
                        } elseif ($row->language !== 'default' && isset($productInfo['data']['info']['languages'][$row->language])) {
                            $msg['title'] = $productInfo['data']['info']['languages'][$row->language]['n'];
                        } else {
                            $msg['title'] = getlang('store.syncmws.InfoError');
                        }

                    }
                    if (checkArr($msg)) {
                        $errorMsg[] = $msg;
                    }
                    break;
                }

                if ($changeData['IsMain']) {
                    continue;
                }

                if (!empty($changeData[$dataKey]['Error'])) {
                    $msg['error'] = $changeData[$dataKey]['Error'];
                    if ($row->language == 'default' && !isset($productInfo['data']['info']['variants'][$changeData['Sku']]['lps']['default'])) {
                        $msg['title'] = $productInfo['data']['info']['lp_n'] ?? '未知错误 - 一';
                    } elseif ($row->language == 'default' && isset($productInfo['data']['info']['variants'][$changeData['Sku']]['lps']['default'])) {
                        $msg['title'] = $productInfo['data']['info']['variants'][$changeData['Sku']]['lps']['default']['n'] ?? '未知错误 - 二';
                    } elseif ($row->language !== 'default' && isset($productInfo['data']['info']['variants'][$changeData['Sku']]['lps'][$row->language])) {
                        $msg['title'] = $productInfo['data']['info']['variants'][$changeData['Sku']]['lps'][$row->language]['n'] ?? '未知错误 - 三';
                    } elseif ($row->language !== 'default' && !isset($productInfo['data']['info']['variants'][$changeData['Sku']]['lps'][$row->language])) {
                        $msg['title'] = $productInfo['data']['info']['languages'][$row->language]['n'] ?? '未知错误 - 四';
                    } else {
                        $msg['title'] = getlang('store.syncmws.InfoError') ?? '未知错误 - 五';
                    }
                    $msg['index'] = $changeData['Sku'];
                    $msg['attribute'] = $productInfo['data']['info']['attribute'];
                    $msg['ids'] = $productInfo['data']['info']['variants'][$changeData['Sku']]['ids'];
                    //$errorMsg['title'] = ($productInfo['data']['info']['variants'][$changeData['Sku']]['lp_n']) ?: '';
                }
                if (checkArr($msg)) {
                    $errorMsg[] = $msg;
                }

            }
            if (count($errorMsg)) {
                $res['content'][] = [
                    'key'        => $key,
                    'name'       => $item,
                    'state'      => 'error',
                    'state_text' => $errorMsg,
                ];
            } else {
                $res['content'][] = [
                    'key'        => $key,
                    'name'       => $item,
                    'state'      => $row->upload->$stateKey,
                    'state_text' => \App\Models\Store\SyncUpload::$states[$row->upload->$stateKey],
                ];
            }
        }//end foreach
        return success('', $res);
        */
        return await Task.FromResult(Success());
    }

    //获取店铺列表
    public async Task<ResultStruct> storeList()
    {
        /*
        $nationLangMap = [
            'AU' => 'en',
            'BR' => 'pt',
            'CA' => 'en',
            'CN' => 'zh',
            'DE' => 'de',
            'ES' => 'es',
            'FR' => 'fr',
            'IN' => 'en',
            'IT' => 'it',
            'JP' => 'ja',
            'MX' => 'en',
            'NL' => 'nl',
            'TR' => 'tr',
            'US' => 'en',
            'AE' => 'ar',
            'GB' => 'en',
            'SG' => 'en',
        ];
        return success(
            '',
            array_map(
                fn($item) => [
                    'id'               => $item['id'],
                    'name'             => $item['name'],
                    'seller_id'        => $item['seller_id'],
                    'state'            => $item['state'],
                    'nation'           => $item['nation'],
                    'nation_id'        => $item['nation_id'],
                    'nation_short'     => $item['nation_short'],
                    'user_id'          => $item['user_id'],
                    'default_language' => $nationLangMap[$item['nation_short']] ?? '',
                ],
                $this->service->getStores() ?: []
            )
        );

        */
        return await Task.FromResult(Success());
    }

    public async Task<ResultStruct> xxxx()
    {
        /*
    
        */
        return await Task.FromResult(Success());
    }

    /// <summary>
    /// 状态数组
    /// </summary>
    /// <returns></returns>
    //[Route("stateList")]
    [HttpGet]
    public async Task<ResultStruct> stateList()
    {
        var State = Enum.GetValues(typeof(States)).Cast<States>().ToDictionary(x =>x, x=>x);
        // var Stores = await _SyncMwsService.getStores();
        #region MyRegion


        /************************************************  
        ================/api/syncMws/stateList
select * from `store` where `i_company_id` = ? and `t_flag` = ? and `t_is_del` = ? and `t_is_verify` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
update `store` set `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
select `id`, `d_title`, `i_user_id`, `t_state` from `work_order` where `i_company_id` = ? and `t_state` in (?, ?) limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `i_export_id`, `t_state` from `export_product` where `i_company_id` = ? and `i_oem_id` = ? and `t_state` = ?
select `d_name`, `d_sign`, `d_filename`, `i_platform_id` from `export_excel` where `t_state` = ? and `t_quality` = ? order by `t_quality` desc, `d_sort` asc
select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
update `store` set `d_last_auto_time` = ?, `store`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `sync_export` where `i_company_id` = ? and `i_oem_id` = ?
select `id`, `d_name`, `d_amount`, `u_export_sign`, `i_language_id`, `created_at`, `u_append` from `sync_export` where `i_company_id` = ? and `i_oem_id` = ? order by `d_sort` asc limit * offset *
select * from `product` where `id` = ? and b_type & * limit *
insert into `temp_images` (`d_oss_path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `i_product_id`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select * from `waybill` where `waybill`.`id` = ? limit *
update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `d_tracking` = ?, `waybill`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `i_store_id` from `to_sell_task` where `i_company_id` = ? and `t_is_finish` = ? and `i_oem_id` = ?
select `id`, `d_button_name` from `waybill` where ((`i_company_id` = ? and `i_operate_company_id` = ?) or `i_waybill_company_id` = ?) and `d_button_name` in (?, ?) order by `updated_at` asc limit *
select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?, ?, ?, ?)
insert into `resource` (`created_at`, `d_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
delete from `temp_images` where `id` in (?, ?, ?, ?, ?)
update `product` set `u_lp_k` = ?, `u_lp_s` = ?, `u_lp_d` = ?, `b_type` = ?, `u_price` = ?, `u_quantity` = ?, `d_source` = ?, `d_ext` = ?, `d_attribute` = ?, `d_variants` = ?, `d_images` = ?, `u_image` = ?, `d_pid` = ?, `d_pid_hash_code` = ?, `c_size` = ?, `d_coin` = ?, `d_rate` = ?, `product`.`updated_at` = ? where `id` = ?
update `counter_company_business` set `c_product_num` = `c_product_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` = ?
update `counter_company_business` set `c_report_product_num` = `c_report_product_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` in (?, ?, ?, ?)
update `counter_user_business` set `c_product_num` = `c_product_num` + *, `counter_user_business`.`updated_at` = ? where `i_user_id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?)
select * from `product` where `id` = ? limit *
select count(*) as aggregate from `product` where (`d_hash_code` = ? and `i_company_id` = ? and `id` != ?) and !(b_type&*) AND !(b_type&*) AND !(b_type&*)
select `id`, `i_operate_id`, `updated_at` from `sync_upload` where (`i_store_id` = ? and `t_state` = ? and `updated_at` < ?)
select `id` from `sync_upload` where `i_store_id` = ? and `t_state` = ? and `i_operate_id` = ?
select count(*) as aggregate from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_oem_id` = ?) and `updated_at` between ? and ?
select `id`, `i_oem_id`, `i_user_id`, `i_company_id`, `i_oem_id`, `i_category_id`, `i_platform_id`, `d_reason`, `u_quantity`, `u_lp_n`, `u_image`, `u_price`, `d_source`, `b_type`, `b_language`, `d_languages`, `updated_at`, `created_at`, `d_coin`, `d_rate` from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_oem_id` = ?) and `updated_at` between ? and ? order by `id` desc limit * offset *
select * from `category` where `category`.`id` in (?)
select * from `task` where `task`.`id` in (?)

        ************************************************/
        #endregion
        /*
        $data['language'] = $this->getLanguageCache();

        $biter = $this->sessionData->getRule()->getStore()->getBiter();
        if ($this->sessionData->getConcierge() == UserModel::TYPE_OWN) {
            $data['rule'] = [
                'progressAdd'          => true,
                'progressEdit'         => true,
                'progressDistribution' => true,
                'progressUpload'       => true,
                'progressPlan'         => true,
            ];
        } else {
            $data['rule'] = [
                'progressAdd'          => $biter->hasProgressAdd(),
                'progressEdit'         => $biter->hasProgressEdit(),
                'progressDistribution' => $biter->hasProgressDistribution(),
                'progressUpload'       => $biter->hasProgressUpload(),
                'progressPlan'         => $biter->hasProgressPlan(),
            ];
        }

        $store = $this->service->getStores();
        if (checkArr($store)) {
            foreach ($store as $value) {
                $data['store'][$value['id']] = [
                    'id'    => $value['id'],
                    'name'  => $value['name'],
                    'state' => $value['state'],
                ];
            }
        } else {
            $data['store'] = [];
        }

        return success('', $data);

         
        */
        return await Task.FromResult(Success());
    }

    //获取产品信息
    [Route("productInfo")]
    [HttpPost]
    public async Task<ResultStruct> productInfo()
    {
        /************************************************  
================/api/syncMws/productInfo
select * from `sync_product` where (`id` = ? and `t_uploading` = ?) limit *
select * from `sync_mws` where `i_spid` = ? and `t_state` in (?, ?, ?, ?)
select `id`, `i_category_id`, `i_sub_category_id`, `i_distribution_category_id`, `i_distribution_sub_category_id`, `u_lp_n`, `u_lp_k`, `u_lp_s`, `u_lp_d`, `u_price`, `u_quantity`, `d_languages`, `b_language`, `d_source`, `d_ext`, `d_attribute`, `d_variants`, `d_images`, `b_type`, `updated_at`, `d_pid`, `d_coin`, `d_rate` from `product` where (`i_company_id` = ? and `id` = ? and `i_oem_id` = ?) limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
update `store` set `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
select * from `product` where `id` = ? and b_type & * limit *
select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
update `store` set `d_last_auto_time` = ?, `store`.`updated_at` = ? where `id` = ?
insert into `temp_images` (`d_oss_path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `i_product_id`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?)
select count(*) as count, `i_store_id` from `sync_mws` where `t_state` in (?, ?) and `i_company_id` = ? and `i_oem_id` = ? group by `i_store_id`
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `id`, `d_button_name` from `waybill` where ((`i_company_id` = ? and `i_operate_company_id` = ?) or `i_waybill_company_id` = ?) and `d_button_name` in (?, ?) order by `updated_at` asc limit *
update `waybill` set `waybill`.`updated_at` = ? where `id` in (?, ?, ?, ?, ?)
update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
select * from `waybill` where `waybill`.`id` = ? limit *
select `i_store_id` from `to_sell_task` where `i_company_id` = ? and `t_is_finish` = ? and `i_oem_id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?, ?, ?, ?)
insert into `resource` (`created_at`, `d_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
delete from `temp_images` where `id` in (?, ?, ?, ?, ?)
update `product` set `u_lp_k` = ?, `u_lp_s` = ?, `u_lp_d` = ?, `b_type` = ?, `u_price` = ?, `u_quantity` = ?, `d_source` = ?, `d_ext` = ?, `d_attribute` = ?, `d_variants` = ?, `d_images` = ?, `u_image` = ?, `d_pid` = ?, `d_pid_hash_code` = ?, `c_size` = ?, `d_coin` = ?, `d_rate` = ?, `product`.`updated_at` = ? where `id` = ?
update `counter_company_business` set `c_product_num` = `c_product_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` = ?
update `counter_company_business` set `c_report_product_num` = `c_report_product_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` in (?, ?, ?, ?)
update `counter_user_business` set `c_product_num` = `c_product_num` + *, `counter_user_business`.`updated_at` = ? where `i_user_id` = ?
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
update `sync_upload` set `d_variant_id` = ?, `t_variant_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
insert into `sync_session_log` (`i_upload_id`, `d_index`, `d_state`, `i_company_id`, `i_user_id`, `i_group_id`, `i_oem_id`, `i_last_operate_id`, `d_count`, `d_server_old_state`, `d_client_old_state`, `d_invoke_api`, `d_index_text`, `d_state_text`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
insert into `sync_alive_session` (`i_upload_id`, `d_index`, `d_state`, `i_company_id`, `i_user_id`, `i_group_id`, `i_oem_id`, `i_last_operate_id`, `d_count`, `d_server_old_state`, `d_client_old_state`, `d_invoke_api`, `d_index_text`, `d_state_text`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select * from `sync_upload` where `id` = ? limit *
select column_name as `column_name` from information_schema.columns where table_schema = ? and table_name = ?
select `d_variant_id`, `t_variant_state`, `id`, `i_operate_id` from `sync_upload` where `id` = ? limit *
update `sync_alive_session` set `d_state` = ?, `d_state_text` = ?, `i_last_operate_id` = ?, `i_user_id` = ?, `d_count` = ?, `sync_alive_session`.`updated_at` = ? where (`i_upload_id` = ? and `d_index` = ?)
update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `waybill`.`updated_at` = ? where `id` = ?
select `id`, `d_title`, `i_user_id`, `t_state` from `work_order` where `i_company_id` = ? and `t_state` in (?, ?) limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *

        ************************************************/

        /*
        $id = request()->input('id');
        if (!$id) {
            return error(getlang('public.ServerMissingParam'));
        }
        $res = $this->service->productInfo($id);

        return $res['state'] ? success('获取成功', $res['data']) : error($res['msg']); 
         */
        return await Task.FromResult(Success());
    }

    /// <summary>
    /// 修改产品信息后修改上传状态为待上传
    /// </summary>
    /// <returns></returns>
    [Route("editProduct")]
    [HttpPost]
    public async Task<ResultStruct> editProduct()
    {
        /************************************************  
================/api/syncMws/editProduct
select * from `sync_product` where `id` = ? and `t_uploading` = ? limit *
update `sync_mws` set `d_sku_change_data` = ?, `u_product_name` = ?, `u_product` = ?, `i_upload_id` = ?, `t_state` = ?, `sync_mws`.`updated_at` = ? where `id` = ? and `t_state` in (?, ?, ?, ?)
update `sync_product` set `u_product_name` = ?, `u_img_url` = ?, `u_category_id` = ?, `u_sub_category_id` = ?, `sync_product`.`updated_at` = ? where `id` = ? and `t_uploading` = ? limit *
select * from `product` where `id` = ? limit *
select count(*) as aggregate from `product` where (`d_hash_code` = ? and `i_company_id` = ? and `id` != ?) and !(b_type&*) AND !(b_type&*) AND !(b_type&*)
update `product` set `u_lp_k` = ?, `b_type` = ?, `u_quantity` = ?, `d_pid_hash_code` = ?, `d_rate` = ?, `product`.`updated_at` = ? where `id` = ?
select * from `sync_product` where `i_pid` = ? and `i_company_id` = ? and `i_oem_id` = ? limit *
update `sync_product` set `t_eanupc_status` = ?, `sync_product`.`updated_at` = ? where `i_pid` = ? and `i_company_id` = ? and `i_oem_id` = ?
update `sync_mws` set `t_eanupc_status` = ?, `sync_mws`.`updated_at` = ? where `i_spid` in (?) and `i_company_id` = ? and `i_oem_id` = ?
select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
select count(*) as aggregate from `sync_mws` where `i_store_id` in (?, ?, ?) and `t_state` = ?
select * from `sync_mws` where `i_store_id` in (?, ?, ?) and `t_state` = ? order by `updated_at` desc limit * offset *
update `store` set `d_last_auto_time` = ?, `store`.`updated_at` = ? where `id` = ?
select `id`, `t_uploading`, `i_pid` from `sync_product` where `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
update `store` set `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `sync_mws` where `i_store_id` in (?, ?, ?)
select * from `waybill` where `waybill`.`id` = ? limit *
update `waybill` set `d_track_info` = ?, `d_button_name` = ?, `t_waybill_state` = ?, `d_tracking` = ?, `waybill`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
update `sync_upload` set `d_variant_id` = ?, `t_variant_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
insert into `sync_session_log` (`i_upload_id`, `d_index`, `d_state`, `i_company_id`, `i_user_id`, `i_group_id`, `i_oem_id`, `i_last_operate_id`, `d_count`, `d_server_old_state`, `d_client_old_state`, `d_invoke_api`, `d_index_text`, `d_state_text`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
insert into `sync_alive_session` (`i_upload_id`, `d_index`, `d_state`, `i_company_id`, `i_user_id`, `i_group_id`, `i_oem_id`, `i_last_operate_id`, `d_count`, `d_server_old_state`, `d_client_old_state`, `d_invoke_api`, `d_index_text`, `d_state_text`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select * from `sync_upload` where `id` = ? limit *
select column_name as `column_name` from information_schema.columns where table_schema = ? and table_name = ?
insert into `sync_invoke_api_log` (`i_upload_id`, `d_request_data`, `i_upload_operate_id`, `i_user_id`, `i_company_id`, `i_group_id`, `i_oem_id`, `t_api`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `d_variant_id`, `t_variant_state`, `id`, `i_operate_id` from `sync_upload` where `id` = ? limit *
update `sync_alive_session` set `d_state` = ?, `d_state_text` = ?, `i_last_operate_id` = ?, `i_user_id` = ?, `d_count` = ?, `sync_alive_session`.`updated_at` = ? where (`i_upload_id` = ? and `d_index` = ?)
select count(*) as aggregate from `sync_mws` where `i_store_id` in (?, ?, ?) and `t_state` in (?, ?) and `t_eanupc_status` = ?
select * from `sync_mws` where `i_store_id` in (?, ?, ?) and `t_state` in (?, ?) and `t_eanupc_status` = ? order by `updated_at` desc limit * offset *
select `i_store_id` from `to_sell_task` where `i_company_id` = ? and `t_is_finish` = ? and `i_oem_id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
select `d_order_no` from `amazon_order` where (`i_store_id` = ? and `t_order_state` = ? and `i_company_id` = ? and `t_entry_mode` = ?) limit *
update `store` set `d_last_auto_update_time` = ?, `store`.`updated_at` = ? where `id` = ?
select `i_spid` from `sync_mws` where `i_store_id` = ? and `t_eanupc_status` = ? and `i_upload_id` is null
insert into `sync_upload` (`i_store_id`, `i_user_id`, `i_company_id`, `i_oem_id`, `i_group_id`, `i_operate_id`, `t_state`, `t_product_state`, `t_variant_state`, `t_pricing_state`, `t_inventory_state`, `t_image_state`, `t_relationship_state`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
insert into `sync_user` (`i_upload_id`, `i_old_user_id`, `i_new_user_id`, `d_last_updated`, `i_company_id`, `i_group_id`, `i_oem_id`, `t_is_changed`, `t_is_in_*_min`, `t_type`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
insert into `sync_online` (`i_upload_id`, `i_user_id`, `i_company_id`, `i_group_id`, `i_oem_id`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?)
update `sync_mws` set `i_upload_id` = ?, `t_state` = ?, `sync_mws`.`updated_at` = ? where `i_store_id` = ? and `t_eanupc_status` = ? and `i_upload_id` is null limit *
update `sync_upload` set `c_count` = ?, `sync_upload`.`updated_at` = ? where `id` = ?
select `id`, `d_button_name` from `waybill` where ((`i_company_id` = ? and `i_operate_company_id` = ?) or `i_waybill_company_id` = ?) and `d_button_name` in (?, ?) order by `updated_at` asc limit *
select `id`, `d_title`, `i_user_id`, `t_state` from `work_order` where `i_company_id` = ? and `t_state` in (?, ?) limit *
insert into `reporter_request` (`created_at`, `d_action`, `d_cpu_time`, `d_method`, `d_module`, `d_name`, `d_namespace`, `d_net_time`, `d_request_id`, `d_request_time`, `d_uri`, `i_company_id`, `i_group_id`, `i_oem_id`, `i_user_id`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select * from `task` where (`i_oem_id` = ? and `i_user_id` = ? and `i_group_id` = ? and `d_hash_code` = ?) limit *
insert into `product` (`i_oem_id`, `i_company_id`, `i_user_id`, `i_group_id`, `i_task_id`, `u_lp_n`, `u_lp_k`, `u_lp_s`, `u_lp_d`, `d_source`, `d_hash_code`, `d_languages`, `b_language`, `b_type`, `u_price`, `i_platform_id`, `u_platform`, `d_attribute`, `d_variants`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select * from `product` where `id` = ? and b_type & * limit *
insert into `temp_images` (`d_oss_path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `i_product_id`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?)
update `waybill` set `waybill`.`updated_at` = ? where `id` in (?, ?, ?, ?, ?)
insert into `resource` (`created_at`, `d_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
delete from `temp_images` where `id` in (?, ?, ?)
update `product` set `u_lp_k` = ?, `u_lp_s` = ?, `u_lp_d` = ?, `b_type` = ?, `u_price` = ?, `u_quantity` = ?, `d_source` = ?, `d_ext` = ?, `d_attribute` = ?, `d_variants` = ?, `d_images` = ?, `u_image` = ?, `d_pid` = ?, `d_pid_hash_code` = ?, `c_size` = ?, `d_coin` = ?, `d_rate` = ?, `product`.`updated_at` = ? where `id` = ?
update `counter_company_business` set `c_report_product_num` = `c_report_product_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` in (?, ?, ?, ?)
update `sync_upload` set `d_inventory_id` = ?, `t_inventory_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
select `d_inventory_id`, `t_inventory_state`, `id`, `i_operate_id` from `sync_upload` where `id` = ? limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?) order by `d_last_auto_time` asc limit *
update `sync_upload` set `d_image_id` = ?, `t_image_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
insert into `resource` (`created_at`, `d_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
delete from `temp_images` where `id` in (?, ?, ?, ?)
update `sync_upload` set `d_relationship_id` = ?, `t_relationship_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `id`, `i_operate_id`, `updated_at` from `sync_upload` where (`i_store_id` = ? and `t_state` = ? and `updated_at` < ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?, ?, ?, ?, ?)
delete from `temp_images` where `id` in (?, ?, ?, ?, ?, ?)
select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
insert into `resource` (`created_at`, `d_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
update `product` set `u_lp_s` = ?, `u_lp_d` = ?, `b_type` = ?, `u_price` = ?, `u_quantity` = ?, `d_source` = ?, `d_ext` = ?, `d_attribute` = ?, `d_variants` = ?, `d_images` = ?, `u_image` = ?, `d_pid` = ?, `d_pid_hash_code` = ?, `c_size` = ?, `d_coin` = ?, `d_rate` = ?, `product`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select count(*) as aggregate from `company` where `t_state` = ? and `i_report_id` = ? and `id` != ?
select count(*) as aggregate from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_oem_id` = ?) and `updated_at` between ? and ?
select `id`, `i_oem_id`, `i_user_id`, `i_company_id`, `i_oem_id`, `i_category_id`, `i_platform_id`, `d_reason`, `u_quantity`, `u_lp_n`, `u_image`, `u_price`, `d_source`, `b_type`, `b_language`, `d_languages`, `updated_at`, `created_at`, `d_coin`, `d_rate` from `product` where !(b_type&*) AND !(b_type&*) AND !(b_type&*) AND b_type&*=* AND !(b_type&*) and (`i_company_id` = ? and `i_oem_id` = ?) and `updated_at` between ? and ? order by `id` desc limit * offset *
select * from `category` where `category`.`id` in (?)
select * from `task` where `task`.`id` in (?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?, ?, ?, ?)
insert into `resource` (`created_at`, `d_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
delete from `temp_images` where `id` in (?, ?, ?, ?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?)
insert into `resource` (`created_at`, `d_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
update `sync_upload` set `t_product_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
delete from `sync_alive_session` where `i_upload_id` = ? and `d_index` = ?
select `id`, `d_sku_change_data`, `i_spid` from `sync_mws` where `id` = ? limit *
select * from `product` where (`i_oem_id` = ? and `i_company_id` = ? and `id` = ?) limit *
update `sync_mws` set `d_sku_change_data` = ?, `sync_mws`.`updated_at` = ? where (`id` = ?)
select * from `store` where `t_flag` = ? and `t_is_del` = ? and `t_is_verify` = ? and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
update `sync_upload` set `t_inventory_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
update `sync_upload` set `t_image_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
update `sync_upload` set `t_state` = ?, `sync_upload`.`updated_at` = ? where (`id` = ?)
select `id`, `i_operate_id` from `sync_upload` where `id` = ? limit *
update `counter_company_business` set `c_waybill_total` = `c_waybill_total` + *.*, `counter_company_business`.`updated_at` = ? where `i_company_id` = ?
update `counter_real_finance` set `c_waybill_total` = `c_waybill_total` + *.*, `counter_real_finance`.`updated_at` = ? where `i_company_id` = ? and `i_oem_id` = ? and `d_month` = ?
insert into `bill_log` (`d_money`, `t_belong`, `t_type`, `d_datetime`, `d_reason`, `t_audit`, `i_user_id`, `u_truename`, `u_audit_truename`, `i_company_id`, `i_group_id`, `i_oem_id`, `i_creat_company_id`, `u_operate_company_name`, `i_report_company_id`, `i_operate_company_id`, `i_store_id`, `t_module`, `i_module_id`, `d_remark`, `created_at`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
update `waybill` set `d_carriage_base` = ?, `d_carriage_num` = ?, `d_rate` = ?, `d_carriage_unit` = ?, `i_bill_log_id` = ?, `t_is_verify` = ?, `waybill`.`updated_at` = ? where `id` = ?
select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?, ?, ?, ?, ?, ?, ?)
delete from `temp_images` where `id` in (?, ?, ?, ?, ?, ?, ?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?, ?, ?, ?)
select * from `sync_mws` where `i_upload_id` = ?
select `i_pid`, `id`, `t_sku_type`, `t_sku_suffix`, `t_title_suffix`, `i_source_id`, `d_sku`, `d_type_data`, `d_type_data_id` from `sync_product` where `i_company_id` = ? and `id` = ? limit *
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?) order by `d_last_auto_update_time` asc limit *
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_auto_update_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_update_time` asc limit *
insert into `temp_images` (`created_at`, `d_oss_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
update `product` set `u_lp_n` = ?, `u_lp_k` = ?, `u_lp_s` = ?, `u_lp_d` = ?, `d_hash_code` = ?, `b_type` = ?, `u_price` = ?, `u_quantity` = ?, `d_ext` = ?, `d_attribute` = ?, `d_variants` = ?, `d_images` = ?, `u_image` = ?, `c_size` = ?, `d_rate` = ?, `product`.`updated_at` = ? where `id` = ?
select `d_temp_name`, `i_browser_node_id`, `u_browser_ui`, `t_level`, `i_nation_id`, `d_type_data` from `category_node_default` where `d_type_hash` = ? and (`t_level` = ? or (`i_company_id` = ? and `t_level` = ?) or (`i_company_id` = ? and `i_group_id` = ? and `t_level` = ?)) order by `t_level` desc, `d_sort` asc
select * from `product` where `id` = ? and `i_company_id` = ? and `i_oem_id` = ? limit *
insert into `product` (`i_oem_id`, `i_company_id`, `i_user_id`, `i_group_id`, `i_category_id`, `i_sub_category_id`, `u_lp_n`, `u_lp_s`, `u_lp_k`, `u_lp_d`, `d_source`, `d_hash_code`, `d_languages`, `b_language`, `b_type`, `u_price`, `i_platform_id`, `u_platform`, `d_attribute`, `d_variants`, `d_ext`, `d_images`, `u_image`, `c_size`, `d_pid`, `created_at`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
insert into `sync_product` (`i_pid`, `i_source_id`, `u_product_name`, `u_img_url`, `d_sku`, `t_sku_type`, `t_sku_suffix`, `t_title_suffix`, `t_product_type`, `u_category_id`, `u_sub_category_id`, `t_already_upload`, `t_uploading`, `i_user_id`, `i_group_id`, `i_company_id`, `i_oem_id`, `t_eanupc_status`, `d_type_data`, `d_type_data_id`, `created_at`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
insert into `sync_mws` (`created_at`, `d_browser_node_id`, `d_country_of_origin`, `d_lead_time`, `d_sku_change_data`, `i_company_id`, `i_group_id`, `i_oem_id`, `i_spid`, `i_store_id`, `i_user_id`, `t_state`, `u_language`, `u_product`, `u_product_name`, `u_store_name`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select count(*) as aggregate from `sync_product` where `i_company_id` = ? and `i_oem_id` = ?
select * from `product` where `i_oem_id` = ? and `id` in (?)
delete from `ean_upc` where `id` in (?)
update `product` set `d_variants` = ?, `product`.`updated_at` = ? where `id` = ?
update `sync_mws` set `t_eanupc_status` = ?, `d_sku_change_data` = ?, `i_upload_id` = ?, `t_state` = ?, `sync_mws`.`updated_at` = ? where `i_spid` in (?) and `i_company_id` = ? and `i_oem_id` = ?
select `id`, `d_oss_path` as `path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `temp_images` where `id` in (?, ?, ?, ?, ?, ?, ?)
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
insert into `resource` (`created_at`, `d_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
select `d_order_no`, `t_order_state` from `amazon_order` where `i_company_id` = ? and `d_order_no_hash` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
insert into `amazon_order` (`d_refund`, `d_fee`, `t_order_state`, `d_order_no`, `d_order_no_hash`, `t_shipping_type`, `d_user_name`, `d_pay_time`, `d_receiver_phone`, `i_receiver_nation_id`, `d_receiver_nation_short`, `d_receiver_nation`, `d_receiver_province`, `d_receiver_city`, `d_receiver_district`, `d_receiver_address`, `d_receiver_address*`, `d_receiver_address*`, `d_receiver_zip`, `d_receiver_name`, `d_receiver_email`, `i_store_id`, `d_seller_id`, `d_order_money`, `d_pay_money`, `d_create_time`, `d_coin`, `d_delivery_time`, `d_last_time`, `d_delivery`, `d_delivery_no`, `d_rate`, `d_latest_ship_time`, `d_earliest_ship_time`, `i_company_id`, `i_user_id`, `i_group_id`, `i_oem_id`, `d_default_ship_from_location_address`, `u_url`, `d_goods`, `d_product_num`, `d_product_money`, `b_progress`, `d_delivery_num`, `d_profit`, `u_truename`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
insert into `amazon_order_action` (`t_order_status`, `d_order_status_text`, `i_order_id`, `d_action`, `i_user_id`, `i_company_id`, `i_group_id`, `i_oem_id`, `d_user_name`, `d_truename`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
update `counter_group_business` set `c_order_num` = c_order_num+*, `c_order_total` = c_order_total+*.*, `c_order_total_fee` = c_order_total_fee+*, `c_order_total_refund` = c_order_total_refund+*, `counter_group_business`.`updated_at` = ? where `i_group_id` = ?
update `counter_store_business` set `c_order_total` = c_order_total+*.*, `c_order_total_fee` = c_order_total_fee+*, `c_order_total_refund` = c_order_total_refund+*, `counter_store_business`.`updated_at` = ? where `i_store_id` = ?
update `counter_company_business` set `c_order_num` = c_order_num+*, `c_order_total` = c_order_total+*.*, `c_order_total_fee` = c_order_total_fee+*, `c_order_total_refund` = c_order_total_refund+*, `counter_company_business`.`updated_at` = ? where `i_company_id` = ?
update `counter_company_business` set `c_report_order_num` = `c_report_order_num` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` in (?, ?)
update `counter_real_finance` set `c_order_total` = c_order_total+*.*, `c_order_total_fee` = c_order_total_fee+*, `c_order_total_refund` = c_order_total_refund+*, `counter_real_finance`.`updated_at` = ? where `i_company_id` = ? and `i_oem_id` = ? and `d_month` = ?
insert into `reporter_request` (`created_at`, `d_action`, `d_cpu_time`, `d_method`, `d_module`, `d_name`, `d_namespace`, `d_net_time`, `d_request_id`, `d_request_time`, `d_uri`, `i_company_id`, `i_group_id`, `i_oem_id`, `i_user_id`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
update `counter_store_business` set `c_waybill_total` = `c_waybill_total` + *, `counter_store_business`.`updated_at` = ? where `i_store_id` = ?
update `counter_company_business` set `c_waybill_total` = `c_waybill_total` + *, `counter_company_business`.`updated_at` = ? where `i_company_id` = ?
update `amazon_order` set `d_shipping_money` = d_shipping_money+*, `d_profit` = d_profit-*, `amazon_order`.`updated_at` = ? where `id` = ?
update `amazon_order` set `d_refund` = ?, `d_fee` = ?, `t_order_state` = ?, `d_order_no` = ?, `d_order_no_hash` = ?, `t_shipping_type` = ?, `d_user_name` = ?, `d_pay_time` = ?, `d_receiver_phone` = ?, `i_receiver_nation_id` = ?, `d_receiver_nation_short` = ?, `d_receiver_nation` = ?, `d_receiver_province` = ?, `d_receiver_city` = ?, `d_receiver_district` = ?, `d_receiver_address` = ?, `d_receiver_address*` = ?, `d_receiver_address*` = ?, `d_receiver_zip` = ?, `d_receiver_name` = ?, `d_receiver_email` = ?, `i_store_id` = ?, `d_seller_id` = ?, `d_order_money` = ?, `d_pay_money` = ?, `d_create_time` = ?, `d_coin` = ?, `d_delivery_time` = ?, `d_last_time` = ?, `d_delivery` = ?, `d_delivery_no` = ?, `d_rate` = ?, `d_latest_ship_time` = ?, `d_earliest_ship_time` = ?, `i_company_id` = ?, `i_user_id` = ?, `i_group_id` = ?, `i_oem_id` = ?, `d_default_ship_from_location_address` = ?, `d_goods` = ?, `d_profit` = ?, `amazon_order`.`updated_at` = ? where `id` = ?
select `id`, `d_path` as `oss_path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `resource` where `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
insert into `temp_images` (`created_at`, `d_oss_path`, `d_size`, `i_oem_id`, `i_oss_id`, `i_user_id`, `id`, `t_type`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)
select * from `sync_mws` where `id` = ? limit *
select * from `user` where `d_username` = ? limit *
select SUM(c_request) as total_request, SUM(c_cpu) as total_cpu, SUM(c_memory) as total_memory, SUM(c_network) as total_network, SUM(c_disk) as total_disk, SUM(c_pic_api) as total_pic_api, SUM(c_translate_api) as total_translate_api from `counter_request` where `i_company_id` = ? limit *
select `c_user_num`, `c_product_num`, `c_order_num`, `c_purchase_num`, `c_waybill_num`, `c_store_num` from `counter_company_business` where `i_company_id` = ? and `i_oem_id` = ? limit *
select `id`, `i_rule_id` from `rule_template` where `rule_template`.`id` = ? limit *
delete from `product` where (`i_user_id` = ? and `i_oem_id` = ?) and b_type & *
select * from `user` where `id` = ? limit *
select * from `user_online`
update `counter_request` set `c_request` = c_request+*, `c_cpu` = c_cpu+*, `c_memory` = c_memory+*, `c_network` = c_network+*, `c_disk` = c_disk+*, `counter_request`.`updated_at` = ? where `i_user_id` = ?
insert into `login_log` (`i_user_id`, `d_username`, `i_oem_id`, `t_type`, `created_at`, `updated_at`, `d_login_ip`, `d_ext`) values (?, ?, ?, ?, ?, ?, ?, ?)
select `c_user_num`, `c_product_num`, `c_order_num`, `c_purchase_num`, `c_waybill_num`, `c_store_num`, `c_report_user_num`, `c_report_product_num`, `c_report_order_num`, `c_report_purchase_num`, `c_report_waybill_num`, `c_report_store_num`, `c_distribution_order_num`, `c_distribution_purchase_num`, `c_distribution_waybill_num` from `counter_company_business` where `i_company_id` = ? limit *
select `i_user_id`, `c_user_num`, `c_product_num`, `c_order_num`, `c_purchase_num`, `c_waybill_num`, `c_store_num` from `counter_user_business` where `i_user_id` = ? limit *
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
select `t_entry_mode`, `id`, `d_receiver_nation`, `d_receiver_nation_short`, `u_url`, `d_earliest_ship_time`, `d_order_no`, `d_pay_money`, `i_company_id`, `d_product_num`, `t_order_state`, `d_create_time`, `t_shipping_type`, `d_fake_product_time`, `d_true_product_time`, `i_store_id`, `id`, `d_coin`, `u_truename`, `d_receiver_name`, `d_receiver_email`, `b_progress`, `d_latest_ship_time`, `i_merge_id`, `d_process_waybill`, `d_process_purchase`, `i_purchase_company_id`, `i_waybill_company_id`, `created_at`, `d_rate`, `t_is_batch_ship`, `d_seller_id`, `d_profit`, `d_invoice` from `amazon_order` where `i_company_id` = ? and `i_oem_id` = ? and (b_progress&*)>>*=* order by `d_create_time` desc, `id` desc limit * offset *
select count(*) as aggregate from `amazon_order` where `i_company_id` = ? and `i_oem_id` = ? and `t_order_state` = ? and `d_latest_ship_time` < ? and `d_latest_ship_time` > ?
select `id`, `i_order_id`, `d_purchase_tracking_number`, `d_purchase_order_no` from `purchase` where `i_order_id` in (?, ?, ?, ?)
select `id`, `i_order_id`, `d_waybill_order`, `d_express` from `waybill` where `i_order_id` in (?, ?, ?, ?)
select `id`, `d_path` as `oss_path`, `d_size`, `i_user_id`, `i_oss_id`, `i_oem_id`, `t_type`, `created_at`, `updated_at` from `resource` where `id` in (?, ?)
update `product` set `u_lp_n` = ?, `u_lp_k` = ?, `u_lp_s` = ?, `u_lp_d` = ?, `d_hash_code` = ?, `u_price` = ?, `d_ext` = ?, `d_attribute` = ?, `d_variants` = ?, `d_images` = ?, `u_image` = ?, `c_size` = ?, `d_rate` = ?, `product`.`updated_at` = ? where `id` = ?
select `t_entry_mode`, `id`, `d_receiver_nation`, `d_receiver_nation_short`, `u_url`, `d_earliest_ship_time`, `d_order_no`, `d_pay_money`, `i_company_id`, `d_product_num`, `t_order_state`, `d_create_time`, `t_shipping_type`, `d_fake_product_time`, `d_true_product_time`, `i_store_id`, `id`, `d_coin`, `u_truename`, `d_receiver_name`, `d_receiver_email`, `b_progress`, `d_latest_ship_time`, `i_merge_id`, `d_process_waybill`, `d_process_purchase`, `i_purchase_company_id`, `i_waybill_company_id`, `created_at`, `d_rate`, `t_is_batch_ship`, `d_seller_id`, `d_profit`, `d_invoice` from `amazon_order` where `i_company_id` = ? and `i_oem_id` = ? order by `d_create_time` desc, `id` desc limit * offset *
select `id`, `i_order_id`, `d_purchase_tracking_number`, `d_purchase_order_no` from `purchase` where `i_order_id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)

        ************************************************/
        /*
         $product = request()->input('Product');
        $spid = request()->input('Spid');
        $skuChangeData = request()->input('SkuChangeData');
        //  验证
        if (!$product || !$spid || !$skuChangeData) {
            return error(getlang('public.ServerMissingParam'));
        }
        DB::beginTransaction();
        try {
            $res = $this->service->editProduct($spid, $product, $skuChangeData);
            DB::commit();
        } catch (LengthException $e) {
            DB::rollBack();
            return error($e->getMessage());
        } catch (Exception $e) {
            DB::rollBack();
            return error(getlang('store.syncmws.ModifyTaskFailed'));
        }

        if (!$res['state']) {
            return error($res['msg']);
        }

        return success('修改成功');
         */
        return await Task.FromResult(Success());
    }

    public async Task<ResultStruct> nodeList()
    {
        /*
        $res = Db::table('recommended_browse_nodes')->select('id', 'i_Nation', 'i_Parent', 'd_CnPath', 'i_NodeID')
            ->where(
                "i_Parent", 0
            )
//            ->distinct()
            ->get()->toArray();
        return success('', $res);
        */
        return await Task.FromResult(Success());
    }

    //根据parent_id获取下一个节点数据
    public async Task<ResultStruct> getNextNodes()
    {
        /*
        if (isset($pre_node)) {

            $result = Db::table('recommended_browse_nodes')->where('i_Parent', $pre_node)->get();
        }
        if (isset($result) && !empty($result)) {
            return success("", $result);
        } else {
            return success("", '');
        }
        */
        return await Task.FromResult(Success());
    }
}
