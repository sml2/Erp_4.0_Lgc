﻿using ERP.Authorization.Rule;
using ERP.Enums;
using ERP.Extensions;
using ERP.Interface;
using ERP.Models.DB.Users;
using ERP.Services;
using ERP.Services.DB.Users;
using ERP.Services.Stores;
using ERP.ViewModels.Store.Remote;
using Role = ERP.Enums.Identity.Role;
using UserService = ERP.Services.DB.Users.UserService;

namespace ERP.Controllers.Store;

using PlatformDataCache = Services.Caches.PlatformData;
using UnitCache = Services.Caches.Unit;

[Route("api/mystore")]
[ApiController]
public class MyStoreController : BaseController
{
    private readonly StoreService _service;
    private readonly UserService _userService;
    private readonly StoreGroupService _storeGroupService;
    private readonly GroupService _groupService;
    private readonly UnitCache _unitCache;
    private readonly PlatformDataCache _PlatformDataCache;
    private readonly ISessionProvider _sessionProvider;
    private readonly CompanyService _companyService;
    private readonly UnitCache _nitCache;

    public MyStoreController(StoreService service, UserService userService, StoreGroupService storeGroupService,
        GroupService groupService, UnitCache unitCache, PlatformDataCache platformDataCache,
        ISessionProvider sessionProvider, CompanyService companyService, UnitCache nitCache)
    {
        _PlatformDataCache = platformDataCache;
        _sessionProvider = sessionProvider;
        _companyService = companyService;
        _nitCache = nitCache;
        _unitCache = unitCache;
        _service = service;
        _userService = userService;
        _storeGroupService = storeGroupService;
        _groupService = groupService;
    }

    //todo 权限
    /// <summary>
    /// 我的店铺初始化
    /// </summary>
    /// <returns></returns>
    [HttpGet("getMyStoreRule")]
    public async Task<ResultStruct> GetRule()
    {
        object rule = new
        {
            myStoreAdd = true,
            myStoreEdit = true,
            myStoreDelete = true,
            myStoreGroup = true,
        };
        
        if (!User.IsInRole(Role.ADMIN))
        {
               //$biter->hasMyStoreAdd(),
               //$biter->hasMyStoreEdit(),
               //$biter->hasMyStoreDelete(),
               //$biter->hasStoreGroup(),
           
        }
        var userId = _sessionProvider.Session!.GetUserID();
        var companyInfo = await _companyService.GetLimitInfoById(_sessionProvider.Session!.GetCompanyID());
        var company = new
        {
            is_open_store = companyInfo?.IsOpenStore,
            open_store_num =companyInfo?.OpenStoreNum
        };

        var DicamazonNation = _PlatformDataCache.List(Platforms.AMAZON)
            .Where(x => x.GetAmazonData()!.Unit.IsNotWhiteSpace()).ToDictionary(x => x.ID, x => x.GetAmazonDataWithID());
        var scontinents = Continents.None.GetIEnumerable();
        return Success(new
        {
            rule,
            user_id = userId,
            company,
            continents = scontinents,
            amazonNation = DicamazonNation,
            unit = _unitCache.List()
        });
    }

    /// <summary>
    /// 我的店铺列表
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost("indexmylist")]
    public async Task<ResultStruct> IndexMyList(AmazonStoreListDto req)
    {
        /*
 
        $groupStoreIds = [];
        if($this->sessionData->getConcierge() === UserModel::TYPE_CHILD)
        {
            //当前用户组有权限的店铺
            $groupStoreIds = $this->storeGroupService->getIdCollectByGroupId($this->getSessionObj()->getGroupId()) ?:[];
        }
     //查看自己添加的店铺
        $list = $this->storeService->myStoreList($data['name'], $data['seller_id'], $data['continents_id'], $data['nation_id'],$groupStoreIds,$data['last_order_time']);

      
        //自己添加的店铺不受权限控制
        foreach($list['data'] as $k => $v)
        {
            if($v['user_id'] == $this->getSessionObj()->getUserId())
            {
                $list['data'][$k]['creator'] = 1;
            }
        }
        $data = [
            'user'         => $user,
            'store'        => $list,
            'continents'   => $this->continents(),
            'amazonNation' => $this->amazonNation(),
            'unit'         => $this->getUnitCache(),
        ];
        return success('获取成功', $data);
        */
        //查询创建用户名
        var list = await _service.MyStoreList(req);
        Dictionary<int, UserInfoSelectStruct> users = new();
        if (list.Items.Count() > 0)
        {
            var items = list.Items;
            var userIds = items.Select(m => m.UserID).Distinct().ToList();
            var userInfo = await _userService.GetUsers(userIds, m => new UserInfoSelectStruct(m.ID, m.UserName));
            users = userInfo.ToDictionary(x => x.Id, x => x);
        }


        return Success(new
        {
            user = users,
            store = list,
            unit = _unitCache.List(),
            continents = Continents.None.GetIEnumerable(),
            amazonNation = _PlatformDataCache.List(Platforms.AMAZON)
                .ToDictionary(x => x.ID, x => x.GetAmazonDataWithID())
        });
    }

    /// <summary>
    /// 我的其他店铺列表
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpGet("otherIndex")]
    public async Task<ResultStruct> OtherIndex([FromQuery] OtherStoreListDto otherStoreDto) //OtherStoreListDto req
    {
        var list = await _service.OtherMyList(otherStoreDto);

        //todo user session
        foreach (var item in list.Items)
        {
            //var data = JsonConvert.DeserializeObject(item.Data);
            item.Data = item.GetOtherData()!.Note;

            if (item.UserID == HttpContext.Session.GetUserID())
            {
                item.Creator = 1;
            }
        }

        return Success(list);
    }

    /// <summary>
    /// Shopee店铺列表
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    public async Task<ResultStruct> IndexShopeeList([FromQuery] ShopeeStoreListDto req)
    {
        var list = await _service.IndexShopeelist(req);

        return Success(new
        {
            store = list,
        });
    }

    public async Task<ResultStruct> editStore(EditStoreDto req)
    {
        BaseService.ReturnStruct res = new BaseService.ReturnStruct();
        if (req.ID > 0)
        {
            //修改
            //res = await _service.EditStore(req);
        }
        else
        {
            //新增
            res = await _service.AddSingleStore(req);
        }

        return res.State ? Success() : Error(message: res.Msg);
    }

    /// <summary>
    /// 修改其他店铺
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost("editOther")]
    [Permission(Enums.Rule.Config.Store.STORE_ADD| Enums.Rule.Config.Store.STORE_EDIT)]
    public async Task<ResultStruct> EditOther(EditOtherStoreDto req)
    {
        var res = new BaseService.ReturnStruct();
        //todo 向当前用户组授权 user session

        if (req.ID > 0)
        {
            //修改
            //检查权限(自己添加的自己可以改，别人分配的不能改)
            //查询店铺的所有者

            var info = await _service.GetInfoById(req.ID);

            if (info == null)
            {
                return Error(message: "店铺不存在");
            }

            if (User.IsInRole(Role.Employee) && info.UserID != _sessionProvider.Session!.GetUserID())
            {
                return Error(message: "无权访问");
            }

            res = await _service.EditOtherStore(req);
        }
        else
        {
            //todo 权限
            if (!_sessionProvider.Session!.Is(Role.Employee) && User.Has(Enums.Rule.Config.Store.MYSTORE_ADD))
            {
                return Error(message: "无权访问");
            }
            res = await _service.EditOtherStore(req);
        }

        return res.State ? Success() : Error(message: res.Msg);
    }
}