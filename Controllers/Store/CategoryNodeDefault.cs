﻿using ERP.Enums.View;

namespace ERP.Controllers.Store;
using Enums;
using Interface;
using Models.DB.Users;
using Models.View.Setting;
using Services.Caches;
using Ryu.Extensions;
using Ryu.String;
using PlatformDataCache = Services.Caches.PlatformData;
using Service = Services.Stores.CategoryNodeDefault;
using Newtonsoft.Json;
using ERP.Models.View.Product;

//[Route("api/[controller]/")]
[ApiController]
public class CategoryNodeDefault : BaseController
{
    private readonly Service _service;
    private readonly Nation _NationCache;
    private readonly ISessionProvider _sessionProvider;
    private readonly PlatformDataCache _PlatformDataCache;
    public CategoryNodeDefault(Service service, Nation NationCache, ISessionProvider sessionProvider, PlatformDataCache platformDataCache)
    {
        _NationCache = NationCache;
        _service = service;
        _sessionProvider= sessionProvider;
        _PlatformDataCache = platformDataCache;
    }

    //private function biterRule()
    //{
    //    return $this->sessionData->getRule()->getStore()->getBiter();
    //}

    /// <summary>
    /// 权限、初始数据
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> GetRule()
    {
        
        //$data['nation'] = arrayCombine($this->amazonNation(), 'id');
        var nation =  _PlatformDataCache.List(Platforms.AMAZON).Where(x => x.GetAmazonData()!.Unit.IsNotWhiteSpace()).ToDictionary(x => x.ID, x => x.GetAmazonDataWithID());
        //return success('', $data);
        object rule = new object();
        //if (_sessionProvider.Session!.GetConcierge() == Concierges.Own)
        //{
            rule = new
            {
                CategoryNodeRange       = true,
                CategoryNodeDefaultAdd   = true,
                CategoryNodeDefaultEdit  = true,
                CategoryNodeDefaultDelete = true
            };
        //}
        //else
        //{
        //    rule = new
        //    {
        //        Category= Enum.GetValues<CategryMode>().Cast<CategryMode>().ToDictionary(x => (int)x, x => EnumExtension.GetDescription(x)),
        //        CategoryNodeRange = true,            => $this->biterRule()->hasCategoryNodeRange() == 0 ? true : false,
        //        CategoryNodeDefaultAdd = true,       => $this->biterRule()->hasCategoryNodeDefaultAdd(),
        //        CategoryNodeDefaultEdit = true,      => $this->biterRule()->hasCategoryNodeDefaultEdit(),
        //        CategoryNodeDefaultDelete = true     => $this->biterRule()->hasCategoryNodeDefaultDelete(),
        //    };
        //}
        return await Task.FromResult(Success(new { nation ,rule}));
    }


    public class CategoryList:PageDto
    {
        [JsonProperty("city")]
        public int? NationId { get; set; }
        [JsonProperty("browser_node_id")]
        public int? browserNodeId { get; set; }
        [JsonProperty("temp_name")]
        public string? tempName { get; set; }
        [JsonProperty("type_data")]
        public string? typeHash { get; set; }
        [JsonProperty("language_code")]
        public string? language_code { get; set; }
    }
    /// <summary>
    /// 列表
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> Index(CategoryList indexDto)
    {
        //$data = $request->validated();
        //$range = $this->sessionData->getConcierge() === UserModel::TYPE_CHILD && $this->biterRule()->hasCategoryNodeRange() == 1;
        
        var res = await _service.GetList(indexDto);
        return res.State ? Success(res.Data!, "获取成功") : Error(message: res.Msg);
    }

    
    
    [HttpPost]
    public async Task<ResultStruct> EditRemote(EditCate editCate)
    {
        //if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->sessionData->getRule()->getStore()->getBiter()->hasCategoryNodeDefaultEdit()) {
        //        return error(getlang('public.authfailed'));
        //    }
        if (_service.CheckExist(editCate.ID))
        {
            return Error("操作失败，已存在相同数据");
        }
        if (editCate.ID>0)
        {   
            var EditData = await _service.EditData(editCate);
            return EditData.State? Success(message: "成功") : Error(message: "失败");
         }
        else
        {
            var AddData=await _service.AddData(editCate);
            return AddData.State ? Success(message: "成功") : Error(message: "失败");
        }
    }

    /// <summary>
    /// 获取用户可使用分类
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> GetNode(string category)
    {
        var res = await _service.GetAllList(category);
        return res.State? Success(res.Data!,"成功") : Error(message: "失败");
    }

    [HttpPost]
    public async Task<ResultStruct> Delete(int ID)
    {
        //检查权限
        //if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->sessionData->getRule()->getStore()->getBiter()->hasCategoryNodeDefaultDelete()) {
        //    return error(getlang('public.authfailed'));
        //}
        return await _service.Del(ID)?Success(message:"成功"):Error(message: "失败");
    }

    [HttpPost]
    public async Task<ResultStruct> NodeInfo(int ID)
    {
        //检查权限
        //if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->sessionData->getRule()->getStore()->getBiter()->hasCategoryNodeDefaultDelete()) {
        //    return error(getlang('public.authfailed'));
        //}
        var Res = await _service.FindName(ID);
        string Path = string.Empty;
        if (Res != null)
        {
            char[] s =new char[]{'、', '，',',','/'};
            var NationName = _NationCache.List()?.Where(x => x.Short == Res.NationShort).Select(x => x.Name).ToString() ?? "暂无";
            Path = Res.CnPath.Substring(Res.CnPath.IndexOfAny(s));
            Path = $"{NationName + " / "+ Path}";
        }
        else
        {
            Path = "未匹配到数据";
        }
        return Success(Path);
    }
}
