﻿using ERP.Services.Stores;
using ERP.ViewModels.Store.Eanupc;
using ERP.Services.Identity;

namespace ERP.Controllers.Store;

public class EanupcController : BaseController
{
    private readonly EanupcService _service;
    private readonly UserManager _userManager;
    private readonly Enums.Rule.Config.Store _currentRange = Enums.Rule.Config.Store.EanUpcRange;

    public EanupcController(EanupcService eanupcService, UserManager userManager)
    {
        _service = eanupcService;
        _userManager = userManager;
    }

    /// <summary>
    /// Ean Upc 初始化获取权限
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ResultStruct> GetRule()
    {
        //$biter = $this->sessionData->getRule()->getStore()->getBiter();

        //if ($this->sessionData->getConcierge() === UserModel::TYPE_OWN) {
        //    $data = [
        //        /**上传商品权限**/
        //        'CodeCreate' => true,
        //        'CodeDel'    => true,
        //        'LeadMode'   => true,
        //        'LeadIn'     => true,
        //    ];
        //}else
        //{
        //    $data = [
        //        /**上传商品权限**/
        //        'CodeCreate' => $biter->hasCodeCreate(),
        //        'CodeDel' => $biter->hasCodeDel(),
        //        'LeadMode' => $biter->hasLeadMode(),
        //        'LeadIn' =>$biter->hasLeadIn(),
        //    ];
        //}
        //return success('', $data);
        //todo 获取权限
        var rule = new
        {
            CodeCreate = true,
            CodeDel = true,
            LeadMode = true,
            LeadIn = true,
        };

        return await Task.FromResult(Success(rule));
    }

    /// <summary>
    /// 获取ean upc 列表
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    //[Route("getEanUpcList")]
    [Obsolete("20230804"), HttpPost]
    public async Task<ResultStruct> GetEanUpcList(EanupcListDto req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _service.GetList(req,range);
        return Success(result);
    }

    [HttpGet]
    public async Task<ResultStruct> AddEanupc()
    {
        //todo 判断权限
        // if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->sessionData->getRule()->getStore()->getBiter()->hasCodeCreate()) {
        // return error(getlang('public.authfailed'));
        // }
        var result = await _service.AddEanupcParams();
        return Success(result);
    }

    /// <summary>
    /// 新增EAN
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> AddEan(EAN req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var result = await _service.AddCode(req,range);
        return result.ToResultStruct();
    }

    /// <summary>
    /// 新增UPC
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ResultStruct> AddUpc(UPC req)
    {
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var resule = await _service.AddCode(req,range);
        return Success(message: resule);
    }

    

    [HttpPost]
    public async Task<ResultStruct> Destroy(DestroyDto req)
    {
        //todo 权限
        // if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->sessionData->getRule()->getStore()->getBiter()->hasCodeDel()) {
        // return error(getlang('public.authfailed'));
        // }
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var res = await _service.Destroy(req,range);
        return res ? Success(message: "删除成功") : Error(message: "删除失败");
    }


    [HttpPost]
    public async Task<ResultStruct> Clear(ClearDto req)
    {
        /*
    
        if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->sessionData->getRule()->getStore()->getBiter()->hasCodeDel()) {
            return error(getlang('public.authfailed'));
        }
        */
        var range = await _userManager.GetProductModuleRange(User, _currentRange);
        var res = await _service.Clear(req,range);

        return res ? Success(message: "删除成功") : Error(message: "删除失败");
    }

    [HttpPost]
    public async Task<ResultStruct> Import([FromForm]ImportDto req)
    {
        if (req.File.Length > 500000)
        {
            return Error("文件大小不可超过500kb");
        }
        var res = await _service.Import(req);
        return res.State ? Success(message:res.Message) : Error(message:res.Message);
    }


    [HttpGet]
    public IActionResult  Download([FromQuery]string file, [FromServices]IWebHostEnvironment env)
    {
        try
        {
            var dir = Path.Combine(env.WebRootPath, "bak", file);
            var fileStream = new FileStream(dir, FileMode.Open, FileAccess.ReadWrite);
            return File(fileStream, "application/octet-stream", file);
        }
        catch (Exception e)
        {
            
            Console.WriteLine(e);
            throw new Exception("文件不存在");
        }
    }
}