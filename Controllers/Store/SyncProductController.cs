﻿
using ERP.Models.View.Products.Export;
using ERP.Services.Export;
using ERP.Services.Product;
using ERP.Services.Stores;
using ERP.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace ERP.Controllers.Store;

[Route("api/[controller]")]
[ApiController]
public class SyncProductController : BaseController
{
    private readonly SyncProductService _syncProductService;
    private readonly ProductService _productService;

    public SyncProductController(SyncProductService syncProductService, ProductService productService)
    {
        _syncProductService = syncProductService;
        _productService = productService;
    }

    [HttpGet("getRule")]
    public async Task<ResultStruct> GetRule()
    {
        /*
        $biter = $this->sessionData->getRule()->getStore()->getBiter();
        $pro_biter = $this->sessionData->getRule()->getProduct()->getBiter();

        if ($this->sessionData->getConcierge() == UserModel::TYPE_OWN) {
            $data = [
//                'progressAdd'          => true,
'progressEdit'         => true,
'progressDistribution' => true,
'productEdit'          => true,
'syncProDel'           => true,
//                'progressUpload'       => true,
//                'progressPlan'         => true,
            ];
        } else {
            $data = [
//                'progressAdd'          => $biter->hasProgressAdd(),
'progressEdit'         => $biter->hasProgressEdit(),
'progressDistribution' => $biter->hasProgressDistribution(),
'productEdit'          => $pro_biter->hasProductEdit(),
'syncProDel'           => $biter->hasSyncProDel(),
//                'progressUpload'       => $biter->hasProgressUpload(),
//                'progressPlan'         => $biter->hasProgressPlan(),
            ];
        }

        return success('', $data);
        */
        return await Task.FromResult(Success());
    }

    public class Query
    {
        public string? product_name { get; set; }
        public int? Id { get; set; }
        public string? type { get; set; }
        public string? sku { get; set; }
    };

    [HttpPost("index")]
    public async Task<ResultStruct> Index(SyncProductIndexDto req)
    {
        var result = await _syncProductService.List(req);
        return Success(result);
    }

    //获取店铺
    public async Task<ResultStruct> getStores()
    {
        /*
        if ($this->sessionData->getConcierge() == UserModel::TYPE_OWN) {
            $stores = $this->getStoreCompanyCache();
        } else {
            $stores = $this->getStorePersonalCache();
        }

        if (checkArr($stores)) {
            return array_filter($stores, fn($item) => $item['type'] === 1);
        }

        return [];
        */
        return await Task.FromResult(Success());
    }

    //加入待上传/导出列表
    public async Task<ResultStruct> addProductType()
    {
        /*
        $data = $request->validated();
        $res = $this->service->joinProductType($data, $data['type']);
        return $res ? success('修改成功') : error(getlang('public.ModifyFailed'));
        */
        return await Task.FromResult(Success());
    }

    //获取mws店铺列表
    [Route("mwsList")]
    [HttpPost]
    public async Task<ResultStruct> mwsList()
    {
        /************************************************  
        ================/api/syncProduct/mwsList
select * from `sync_product` where `t_uploading` = ? and `sync_product`.`id` = ? limit *
select `id`, `u_store_name`, `i_store_id`, `d_sku_change_data`, `i_spid`, `u_language`, `d_browser_node_id` from `sync_mws` where `i_store_id` in (?, ?, ?) and `i_spid` = ? and `t_state` in (?, ?, ?, ?)
select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
update `store` set `d_last_auto_time` = ?, `store`.`updated_at` = ? where `id` = ?
select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
update `store` set `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
select `id`, `d_title`, `i_user_id`, `t_state` from `work_order` where `i_company_id` = ? and `t_state` in (?, ?) limit *

        ************************************************/

        /*
        $spid = request()->input('spid');
        if (empty($spid)) {
            return error(getlang('public.ServerMissingParam'));
        }
        $res = $this->mwsService->getListBySpid($spid);

        if (!$res['state']) {
            return error($res['msg']);
        }
        return success('', $res['data']);
         */
        return await Task.FromResult(Success());
    }
    
    /// <summary>
    /// 修改sku
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    [HttpPost("editSku")]
    public async Task<ResultStruct> EditSku(EditSkuDto req)
    {
        var result = await _syncProductService.UpdateDataById(req);
        return result ? Success() : Error();
    }

    //删除产品
    [HttpPost("delete")]
    public async Task<ResultStruct> Delete(IdDto req)
    {
        var result = await _syncProductService.Delete(req.ID);
        return result ? Success() : Error();
    }
}