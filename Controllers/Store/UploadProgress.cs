﻿using ERP.Interface;
using Microsoft.AspNetCore.Mvc;
using static ERP.Models.DB.Identity.User;
using Service = ERP.Services.DB.Store.UploadProgress;
using MwsTaskService = ERP.Services.DB.Store.MwsTask;

namespace ERP.Controllers.Store;

public class UploadProgress : BaseController
{
    private readonly ISessionProvider _SessionProvider;
    private readonly Service _Service;
    private readonly MwsTaskService _MwsTaskService;
    public UploadProgress(ISessionProvider SessionProvider, Service service, MwsTaskService mwsTaskService)
    {
        _SessionProvider = SessionProvider;
        _Service = service;
        _MwsTaskService = mwsTaskService;
    }
    public  ResultStruct getRule()
    {
       // $biter = $this->sessionData->getRule()->getStore()->getBiter();

        object rule = new object();
        //if (_SessionProvider.Session.GetConcierge() == Concierges.Own)
        //{
            rule = new
            {
                progressAdd         =true,
                progressEdit        =true,
                progressDistribution=true,
                progressUpload      =true,
                progressPlan        =true
            };
        //}

    
        //} else {
        //    rule = new
        //    {
        //        progressAdd         =true,     // => $biter->hasProgressAdd(),
        //        progressEdit        =true,     // => $biter->hasProgressEdit(),
        //        progressDistribution=true,     // => $biter->hasProgressDistribution(),
        //        progressUpload      =true,     // => $biter->hasProgressUpload(),
        //        progressPlan        =true      // => $biter->hasProgressPlan(),
        //    };
        //}

        return Success(rule);

    }//end getRule()

    public class UploadList
    {
        public int? Id { get; set; }
        public int? SId { get; set; }
        public int? storeId { get; set; }
        public string? proName { get; set; }
        public Models.Abstract.BaseModel.StateEnum? State { get; set; }
        public string? Language { get; set; }
        public DateTime? updatedat { get; set; }
    }

  /*  public async Task<ResultStruct> index(UploadList uploadList)
{
        _MwsTaskService.Info()
        $data = $request->validated();
        $storeIds = array_column($this->getStores(), 'id');
        $list = MwsTask::whereIn('sid', $storeIds)
            ->SId(($data['store_id'] ?? null))
            ->productName(($data['pro_name'] ?? null))
            ->uiState(($data['state'] ?? null))
            ->productLanguage(($data['language'] ?? null))
            ->orderByDesc('updated_at')
            // ->dd();
            ->paginate($this->limit);
foreach ($list->items() as $item) {
//            if ($item->sku_type === MwsTask::SKU_TYPE_DEFAULT) {
//                $item->sku = (explode('-', $item->sku, 2)[1] ?? explode('-', $item->sku, 2)[0]);
//            }
            $item->url = $item->product_url;
            $item->state = MwsTask::exchangeState(intval($item->state), $item->tasks_id, $item->asin);
}

return success('', $list);

    }//end index()

    public function add(Request $request)
{
        $data = $request->validated();
    if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->sessionData->getRule()->getStore(
    )->getBiter()->hasProgressAdd()
        ) {
    return error(getlang('public.authfailed'));
}

        $myStores = $this->getStores();
foreach ($data['stores'] as $item) {
    if (!isset($item['store_id']) || !isset($item['language']) || !isset($item['browser_node_id']))
    {
        return error(getlang('public.ServerMissingParam'));
    }

    if (!isset($myStores[$item['store_id']]))
    {
        return error(getlang('store.syncmws.StoreNotExist'));
    }
}

        $skuType = ($data['sku_type'] ?? MwsTask::SKU_TYPE_DEFAULT);
DB::beginTransaction();
try
{
            $insertData = [];
    foreach ($data['stores'] as $item) {
                $res = $this->service->cloneProduct($data['goods_id'], $item['language'], ($data['source'] ?? 1));
        if (!$res['state']) {
            return error($res['msg']);
        }

                $insertData[] = [
                    'sid'              => $item['store_id'],
                    'store_name'       => $myStores[$item['store_id']]['name'],
                    'pid'              => $res['data']['pid'],
                    'source_id'        => $res['data']['source_id'] ?: $data['goods_id'],
                    'product_language' => $item['language'],
                    'product_name'     => $res['data']['name'],
                    'product_url'      => $data['image'],
                    //                    'sku'              => ($skuType === MwsTask::SKU_TYPE_DEFAULT ? encodeSku(
                    //                        $data['goods_id'],
                    //                        $data['sku']
                    //                    ) : $data['sku']),
                    'sku'              => $data['sku'],
                    'sku_type'         => ($skuType),
                    'company_id'       => $myStores[$item['store_id']]['company_id'],
                    'oem_id'           => $this->getSessionObj()->getOemId(),
                    'user_id'          => $this->getSessionObj()->getUserId(),
                    'group_id'         => $this->getSessionObj()->getGroupId(),
                    'browser_node_id'  => ($item['browser_node_id'] ?? ''),
                    'type_data'        => json_encode($data['types']),
                    'state'            => MwsTask::STATE_WAITING,
                    'lead_time'        => $data['lead_time'],
                    'created_at'       => date('Y-m-d H:i:s', $request->input('time')),
                    'updated_at'       => date('Y-m-d H:i:s', $request->input('time')),
                ];
    }//end foreach

    MwsTask::insert($insertData);
    DB::commit();
}
catch (\Exception $e) {
    DB::rollBack();
    return error('', $e->getMessage());
}//end try

return success($res['msg']);

}//end add()


public function edit(Request $request)
{
        $data = $request->validated();
    if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->sessionData->getRule()->getStore(
    )->getBiter()->hasProgressEdit()
        ) {
    return error(getlang('public.authfailed'));
}

        $storeIds = array_column($this->getStores(), 'id');
        $row = $this->service->info('', $data['id'])->whereIn('sid', $storeIds)->first();
if (!$row) {
    return error(getlang('store.uploadprogress.InfoNotExist'));
}

        $res = $this->productService->info($row->pid);
if (!$res['state']) {
    return error(getlang('store.uploadprogress.InfoNotExist'));
}

        $product = $res['data']['info'];

        $product['attribute'] = ($data['attribute'] ?? $product['attribute']);
        $product['variants'] = ($data['variants'] ?? $product['variants']);
        $skuType = ($data['sku_type'] ?? MwsTask::SKU_TYPE_DEFAULT);
DB::beginTransaction();
try
{
    Product::where('id', $row->pid)->update(['variants' => $product['variants']]);

            $row->sid = $data['store_id'];
            $row->sku = $data['sku'];
            $row->product_language = $data['language'];
            $row->sku_type = ($data['sku_type'] ?? 0);
            $row->browser_node_id = ($data['browser_node_id'] ?? '');
            $row->type_data = json_encode($data['types']);
            $row->lead_time = $data['lead_time'];
    if ($row->state == MwsTask::STATE_ERROR) {
                $row->tasks_id = null;
        foreach (MwsTasks::$sessions as $index => $item) {
                    $row->{ 'err_'.$index} = null;
        }
    } else
    {
        if ($row->state == MwsTask::STATE_SUCCESS) {
                    $row->tasks_id = null;
        }
    }

            // 修改后将状态改为待执行.
            $row->state = MwsTask::STATE_WAITING;
            $row->save();
    DB::commit();
}
catch (\Exception $e) {
    DB::rollBack();
    return error('', $e->getMessage());
}//end try

return success('');

}//end edit()

public function info(Request $request)
{
        $data = $request->validated();

        $storeIds = array_column($this->getStores(), 'id');
        $row = $this->service->info('', $data['id'])->whereIn('sid', $storeIds)->first();
    if (!$row) {
    return error(getlang('store.uploadprogress.InfoNotExist'));
}

        $res = $this->productService->info($row->pid);
if (!$res['state']) {
    return error(getlang('store.uploadprogress.ProductInfoNotExist'));
}

        $product = $res['data']['info'];

        $res = (new \App\Service\Product\Product())->info($row->pid);

        if ($product['ext']) {
            foreach ($product['ext'] as $v) {
                if ($v['key'] == 'RecommendedBrowseNodes') {
                    $product['ext'] = $v['value'];
                }
            }
        } else {
            $product['ext'] = null;
        }

//        $origin_node_option = RecommendedBrowseNodesModel::query()->Parent(0)->get();//原始节点 可以用groupBy整理好数据传给前台
//        dd($origin_node_option);
        return success(
            '',
            [
                'store_id'        => $row->sid,
                'types'           => json_decode($row->type_data, true),
                'sku'             => $row->sku,
//                    $row->sku_type === MwsTask::SKU_TYPE_DEFAULT ? (explode(
//                    '-',
//                    $row->sku,
//                    2
//                )[1] ?? explode(
//                    '-',
//                    $row->sku,
//                    2
//                )[0]) : $row->sku,
                'goods_id'        => $row->pid,
                'lp_n'            => $row->product_name,
                'image'           => $row->product_url,
                'sku_type'        => $row->sku_type,
                'browser_node_id' => $row->browser_node_id,
                'language'        => $row->product_language,
                'languagePack'    => ($res['data']['languagePack'] ?? []),
                'attribute'       => $product['attribute'],
                'variants'        => $product['variants'],
                'images'          => $product['images'],
                'node'            => ($product['ext'] ?? null),
                'state'           => $row->state,
                'lead_time'       => $row->lead_time,
                'browser_node_nation' => RecommendedBrowseNodes::BROWSE_NODE_NATION,
//                'origin_node_option' => $origin_node_option,
            ]
        );

    }//end info()

    public function delete(Request $request)
    {
        $input    = $request->validated();
        $storeIds = array_column($this->getStores(), 'id');

        $row   = $this->service->info('', $input['id'])->whereIn('sid', $storeIds)->first();
        $state = MwsTask::exchangeState(intval($row->state), $row->tasks_id, $row->asin);
        if (!in_array(
            $state,
            [
                MwsTask::UI_WAIT_UPLOAD,
                MwsTask::UI_COMPLETE,
                MwsTask::UI_FAILED_UPLOAD,
                MwsTask::UI_FAILED_UPDATE,
            ]
        )
        ) {
            return error(getlang('store.uploadprogress.NoDel'));
        }

        DB::beginTransaction();
        try {
            $row->delete();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return error(getlang('store.uploadprogress.Retry'), $e->getMessage());
        }

        return success();

    }//end delete()



    //进度详情
    public function progress(Request $request)
    {
        $data = $request->validated();
        if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN && !$this->sessionData->getRule()->getStore()->getBiter()->hasProgressPlan()) {
            return error(getlang('public.authfailed'));
        }

        $sessions = MwsTasks::$sessions;
        $storeIds = array_column($this->getStores(), 'id');
        $row      = $this->service->info('', $data['id'])->whereIn('sid', $storeIds)->first();
        if (!$row || !$row->tasks) {
            return error(getlang('store.uploadprogress.InfoNotExist'));
        }

        $res = [];
        foreach ($sessions as $key => $item) {
            $col = 'session_'.$key;
            if ($row->{'err_'.$key}) {
                $res[] = [
                    'key'        => $key,
                    'name'       => $item,
                    'state'      => 'error',
                    'state_text' => $row->{'err_'.$key},
                ];
            } else {
                $json  = json_decode($row->tasks->$col, true);
                $res[] = [
                    'key'        => $key,
                    'name'       => $item,
                    'state'      => $json['state'],
                    'state_text' => MwsTasks::$states[$json['state']],
                ];
            }
        }//end foreach

        return success('', $res);

    }//end progress()


    //获取店铺列表
    public function storeList()
    {

        return success(
            '',
            array_map(
                fn($item) => [
                    'id'    => $item['id'],
                    'name'  => $item['name'],
                    'state' => $item['state'],
                ],
                $this->getStores() ?: []
            )
        );

    }//end storeList()


    //获取店铺
    private function getStores()
    {
        if ($this->sessionData->getConcierge() == UserModel::TYPE_OWN) {
            $stores = $this->getStoreCompanyCache();
        } else {
            $stores = $this->getStorePersonalCache();
        }

        if(checkArr($stores)){
            return array_filter($stores, fn($item) => $item['type'] === 1);
        }

        return [];

    }//end getStores()


   //状态数组
    public function stateList()
    {
        $uiStates = [getlang('store.mwstask.WaitUpload'),getlang('store.mwstask.Uploading'),getlang('store.mwstask.WaitUpdate'),getlang('store.mwstask.Updating'),getlang('store.mwstask.Done'),getlang('store.mwstask.UploadFailed'),getlang('store.mwstask.UpdateFailed')];
        foreach ($uiStates as $key => $value) {
            $data['state'][] = [
                'label' => $value,
                'value' => $key,
            ];
        }

        $data['language'] = $this->getLanguageCache();

        $biter = $this->sessionData->getRule()->getStore()->getBiter();
        if ($this->sessionData->getConcierge() == UserModel::TYPE_OWN) {
            $data['rule'] = [
                'progressAdd'          => true,
                'progressEdit'         => true,
                'progressDistribution' => true,
                'progressUpload'       => true,
                'progressPlan'         => true,
            ];
        } else {
            $data['rule'] = [
                'progressAdd'          => $biter->hasProgressAdd(),
                'progressEdit'         => $biter->hasProgressEdit(),
                'progressDistribution' => $biter->hasProgressDistribution(),
                'progressUpload'       => $biter->hasProgressUpload(),
                'progressPlan'         => $biter->hasProgressPlan(),
            ];
        }

        $store = $this->getStores();
        if (checkArr($store)) {
            foreach ($store as $value) {
                $data['store'][$value['id']] = [
                    'id'    => $value['id'],
                    'name'  => $value['name'],
                    'state' => $value['state'],
                ];
            }
        } else {
            $data['store'] = [];
        }

        return success('', $data);

    }//end stateList()

    //    public function getSkuData($productId)
    //    {
    //        return success('', encodeSku($productId));
    //
    //    }//end getSkuData()


    //分配EAN/UPC
    public function distribution()
    {
        $id = request()->input('id');

        $sku = request()->input('sku');
        if (!$sku) {
            return error('服务器异常: 缺少SKU');
        }

        if ($this->sessionData->getConcierge() !== UserModel::TYPE_OWN
            && !$this->sessionData->getRule()->getStore()->getBiter()->hasProgressDistribution()
        ) {
            return error(getlang('public.authfailed'));
        }

        if (!$id) {
            return error(getlang('store.uploadprogress.LessID'));
        }

        $mode = request()->input('mode');

        if (!$mode) {
            return error(getlang('store.uploadprogress.SelectType'));
        }
        $cover = request()->input('cover');
//        $result = $this->productService->distribution($id, $mode, $cover, true);
        $result = $this->productService->distribution($sku, $mode, $cover, true);

        return $result['state'] ? success('分配成功') : error($result['msg']);

    }//end distribution()


    //修改产品信息后修改上传状态为待上传
    public function editProduct()
    {
        $data = request()->input('data');
        $res  = $this->productService->edit($data, false);

        if (!$res['state']) {
            return error($res['msg']);
        }

        // @todo 添加搜索条件
        $row = MwsTask::where('pid', $data['id'])->first();
        if (!$row) {
            return success('修改成功', '无法找到关联任务');
        }

        DB::beginTransaction();
        try {
            if ($row->state == MwsTask::STATE_ERROR) {
                $row->tasks_id = null;
                $row->state    = MwsTask::STATE_WAITING;
                foreach (MwsTasks::$sessions as $index => $item) {
                    $row->{'err_'.$index} = null;
                }
            } else {
                if ($row->state == MwsTask::STATE_SUCCESS) {
                    $row->tasks_id = null;
                    $row->state    = MwsTask::STATE_WAITING;
                }
            }
            $language = $data['ui_language']?:'default';
            $row->product_name = $data['languages'][$language]['n'];
            $row->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return error(getlang('store.uploadprogress.EditTaskFailed'));
        }

        return success(isset($data['id']) && $data['id'] ? '修改成功' : '添加成功');

    }//end editProduct()

    public function checkEanUpc()
    {
        $input = request()->input();

        $base = Product::find($input['id']);
        $flag = true;
        foreach ($base['variants'] as $item) {
            if (empty($item['code'])) {
                $flag = false;
            }
        }

        return success(
            '',
            ['allDistributed' => $flag]
        );

    }//end checkEanUpc()

    public function syncInfo()
    {
        $input = request()->input();

        $base = MwsTask::find($input['base']);
        $stores = $this->getStores();
        if (!isset($stores[$base['sid']])) {
            return error(getlang('store.syncmws.StoreNotExist'));
        }

        $sellerId      = $stores[$base['sid']]['seller_id'];
        $storeId       = $base['sid'];
        $validStoreIds = array_map(
            fn($item) => $item['id'],
            array_filter($stores, fn($item) => $item['seller_id'] === $sellerId),
        );

//        $list = MwsTask::where('sku', encodeSku($input['pid'], $input['sku']))
        $list = MwsTask::where('sku', $input['sku'])
            ->whereIn('sid', $validStoreIds)->get()->toArray();
        $list = arrayCombine($list, 'id');

        $pids = array_column($list, 'pid');

        $products = Product::whereIn('id', $pids)->get();

        $products = arrayCombine($products->toArray(), 'id');
        foreach ($list as &$item) {
            $item['variant_num']    = count($products[$item['pid']]['variants']);
            $item['hasDistributed'] = false;
            foreach ($products[$item['pid']]['variants'] as $variant) {
                if (!empty($variant['code'])) {
                    $item['hasDistributed'] = true;
                }
            }
        }

        return success('', array_values($list));

    }//end syncInfo()

    public function syncEanUpc()
    {
        $input = request()->input();

        $base = Product::select(['id', 'variants'])->find($input['base']);
        $todo = Product::whereIn('id', $input['todo'])->select(['id', 'variants'])->get();

        DB::beginTransaction();
        try {
            foreach ($todo as $item) {
                $newVariants = json_decode($item->getRawOriginal('variants'), true);
                foreach ($newVariants as $k => &$v) {
                    $v['code'] = $base['variants'][$k]['code'];
                }

                Product::where('id', $item['id'])->update(['variants' => $newVariants]);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return error(getlang('store.uploadprogress.SyncFailed'), $e->getMessage());
        }

        // 一条条更新，还是直接覆盖
        // Product::query()->update($updateData);
        return success();

    }//end syncEanUpc()
    
    //根据parent_id获取下一个节点数据
    public function getNextNodes($pre_node)
    {
        if(isset($pre_node)){
            $result = $this->recommendedBrowseNodesService->getchildnode($pre_node);
        }
        return $result ? success('',$result) : error('');
    }

    public function superSearch()
    {
        $get = request();
        $inputContent = $get['inputContent'] ?? '';
        $selectedNation = $get['selectedNation'] ?? '';
        if(empty($inputContent) || empty($selectedNation)){
            return error(getlang('public.ServerMissingParam'));
        }else{
            $searchResult = $this->recommendedBrowseNodesService->getSearchNode((int)$selectedNation,$inputContent);

            if(isset($searchResult)){
                $searchResult = $searchResult->toArray();
            }else{
                $searchResult = [];
            }
            return success('获取成功',["list" => $searchResult]);
        }
    }
     
    


//    [Route("distribution")]
//    [HttpPost]
//    public async Task<ResultStruct> distribution()
//    {
//        /************************************************  
//        ================/api/uploadProgress/distribution
//select * from `sync_product` where `i_oem_id` = ? and `i_company_id` = ? and `d_sku` = ?
//select * from `product` where `i_oem_id` = ? and `id` in (?)
//select * from `user_online` where `id` = ? and `d_login_time` = ? limit *
//update `user_online` set `user_online`.`updated_at` = ? where `id` = ?
//select `id`, `d_value` from `ean_upc` where (`i_company_id` = ? and `i_oem_id` = ?) and `t_type` = ? limit *
//select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
//delete from `ean_upc` where `id` in (?)
//update `product` set `d_variants` = ?, `product`.`updated_at` = ? where `id` = ?
//select * from `sync_product` where `i_pid` = ? and `i_company_id` = ? and `i_oem_id` = ? limit *
//select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
//update `sync_product` set `t_eanupc_status` = ?, `sync_product`.`updated_at` = ? where `i_pid` = ? and `i_company_id` = ? and `i_oem_id` = ?
//select `d_sku_change_data` from `sync_mws` where `i_spid` in (?) and `i_company_id` = ? and `i_oem_id` = ? limit *
//update `store` set `d_last_allot_time` = ?, `store`.`updated_at` = ? where `id` = ?
//update `sync_mws` set `t_eanupc_status` = ?, `d_sku_change_data` = ?, `i_upload_id` = ?, `t_state` = ?, `sync_mws`.`updated_at` = ? where `i_spid` in (?) and `i_company_id` = ? and `i_oem_id` = ?
//select count(*) as aggregate from `sync_product` where `i_company_id` = ? and `i_oem_id` = ?
//select `id`, `d_last_order_time`, `d_last_ship_time` from `store` where (`id` = ? and `i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_type` = ? and `t_is_verify` = ? and `t_is_del` = ?) limit *
//update `store` set `d_last_auto_time` = ?, `store`.`updated_at` = ? where `id` = ?
//select `id`, `d_title`, `i_user_id`, `t_state` from `work_order` where `i_company_id` = ? and `t_state` in (?, ?) limit *
//select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
//select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *
//select `id`, `d_button_name` from `waybill` where ((`i_company_id` = ? and `i_operate_company_id` = ?) or `i_waybill_company_id` = ?) and `d_button_name` in (?, ?) order by `updated_at` asc limit *
//update `waybill` set `waybill`.`updated_at` = ? where `id` in (?, ?, ?, ?, ?)
//select * from `waybill` where `waybill`.`id` = ? limit *
//select count(*) as aggregate from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
//select `id`, `d_data`, `i_nation_id`, `d_name`, `d_last_order_time`, `d_last_ship_time`, `d_last_auto_ship_time` from `store` where (`i_company_id` = ? and `t_flag` = ? and `t_state` = ? and `t_is_del` = ? and `t_type` = ? and `t_is_verify` = ? and `d_last_allot_time` < ?) and `id` in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) order by `d_last_auto_time` asc limit *

//        ************************************************/
//        return Success();
//    }


   
}
