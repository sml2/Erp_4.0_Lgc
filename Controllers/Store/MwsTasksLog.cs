﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.Controllers.Store;
[Route("api/[controller]")]
[ApiController]
public class MwsTasksLog : BaseController
{
    /**
     * @return JsonResponse
     * @author ryu <mo5467@126.com>
     * @todo 没人调用
     */
    public async Task<int> Index(MwsTasksLogDto mwsTasksLogDto)
    {
        return await Task.FromResult(1);
        //$info = $this->MwsTasksLogService->info([],'',$tasks_id,$state,$user_id,$company_id)->OrderBy("created_at","desc")->paginate($this->limit);
        //return success("",$info);
    }

    public class MwsTasksLogDto
    {
        public int company_id { get; set; }
        public int state { get; set; }
        public int tasks_id { get; set; }
        public int user_id { get; set; }
    }
}
