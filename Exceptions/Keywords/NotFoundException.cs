using ERP.Storage.Services.Internal;

namespace ERP.Exceptions.Keywords
{
    public class NotFoundException : StorageDomainException
    {
        public NotFoundException(string message) : base(message)
        {
        }
    }
}
