using ERP.Storage.Services.Internal;

namespace ERP.Exceptions.Category;

public class DestroyException : StorageDomainException
{
    public DestroyException(string message) : base(message)
    {
    }
}