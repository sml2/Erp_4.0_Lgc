using ERP.Storage.Services.Internal;

namespace ERP.Exceptions.Category
{
    public class NotFoundException : StorageDomainException
    {
        public NotFoundException(string message) : base(message)
        {
        }
    }
}
