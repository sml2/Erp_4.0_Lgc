﻿using ERP.Storage.Services.Internal;

namespace ERP.Exceptions.ImageResource
{
    public class NotFoundException : StorageDomainException
    {
        public NotFoundException(string message) : base(message)
        {
        }
    }
}
