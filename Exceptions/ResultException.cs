﻿namespace ERP.Exceptions
{
    public class ResultException : Exception
    {
        protected ResultException(string message) : base(message)
        {
        }
    }
}
