﻿using ERP.Exceptions.Developers.Base.Git;

namespace ERP.Exceptions.Developers.Git.Push;

public class Onec20220401 : PushOnce
{
    public Onec20220401( Author author) : base(new(2022,4,1), Author.ZY | Author.LYQ)
    {
    }
}
