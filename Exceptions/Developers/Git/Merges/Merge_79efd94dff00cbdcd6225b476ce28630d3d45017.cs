﻿using ERP.Exceptions.Developers.Base.Git;

namespace ERP.Exceptions.Developers.Merges;

public class Merge_79efd94dff00cbdcd6225b476ce28630d3d45017 : Merge
{
    public Merge_79efd94dff00cbdcd6225b476ce28630d3d45017() :
        base(nameof(Merge_79efd94dff00cbdcd6225b476ce28630d3d45017), "物流类库更改导致Test项目编译出错",Author.ZYY){}
}
