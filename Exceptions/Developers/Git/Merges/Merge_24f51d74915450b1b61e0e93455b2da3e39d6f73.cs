﻿using ERP.Exceptions.Developers.Base.Git;

namespace ERP.Exceptions.Developers.Merges;

public class Merge_24f51d74915450b1b61e0e93455b2da3e39d6f73 : Merge
{
    public Merge_24f51d74915450b1b61e0e93455b2da3e39d6f73() :
        base(nameof(Merge_24f51d74915450b1b61e0e93455b2da3e39d6f73), "Store模型里有未实现异常代码导致运行出错",Author.SY){}
}
