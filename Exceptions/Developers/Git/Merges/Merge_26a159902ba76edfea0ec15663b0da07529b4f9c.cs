﻿using ERP.Exceptions.Developers.Base.Git;

namespace ERP.Exceptions.Developers.Merges;

public class Merge_26a159902ba76edfea0ec15663b0da07529b4f9c : Merge
{
    public Merge_26a159902ba76edfea0ec15663b0da07529b4f9c() :
        base(nameof(Merge_26a159902ba76edfea0ec15663b0da07529b4f9c), "ShipOdrer更改导致ERP项目编译出错",Author.ADY){}
}
