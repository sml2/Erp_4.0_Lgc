﻿using ERP.Exceptions.Developers.Base.Wainings;

namespace ERP.Exceptions.Developers.Warnings.CS1998S
{
    /// <summary>
    /// fixed
    /// </summary>
    public class SyncMwsInfo : CS1998
    {
        public SyncMwsInfo() : base(Author.ADY)
        {
        }
    }
}
