﻿using System;
using ERP.Exceptions.Developers.Base.Wainings;

namespace ERP.Exceptions.Developers.Bugs;
/*
严重性	代码	说明	项目	文件	行	禁止显示状态
警告	CS8604	“Task<ResultStruct> BusinessController.GetUserList(string name, int? state, CounterUserBusinessService counterUserBusinessService)”中的形参“name”可能传入 null 引用实参。	Test	D:\ERP\Erp_4.0_Lgc\Test\Controllers\Setting\BusinessControllerTest.cs	108	活动
 */
public class BusinessGetUserList : Warning
{
    public BusinessGetUserList(string CompilerErrorsCode, Author author):base("CS8604", Author.None)
    {
    }
}
