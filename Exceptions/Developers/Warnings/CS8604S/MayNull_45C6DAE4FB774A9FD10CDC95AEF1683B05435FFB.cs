﻿using ERP.Exceptions.Developers.Base.Wainings;

namespace ERP.Exceptions.Developers.Warnings.CS8604S;

/// <summary>
/// FIXED
/// </summary>
public class MayNull_45C6DAE4FB774A9FD10CDC95AEF1683B05435FFB : CS8604
{
    public MayNull_45C6DAE4FB774A9FD10CDC95AEF1683B05435FFB() : base(Author.ADY|Author.ZYY|Author.SY)
    {
    }
}
