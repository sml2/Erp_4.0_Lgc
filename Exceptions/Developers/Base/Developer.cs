﻿namespace ERP.Exceptions.Developers.Base
{
    public abstract class Developer : Exception
    {
        protected Developer(string reason, Author author) : base($"{author} make a exp because {reason}")
        {
        }
    }
}
