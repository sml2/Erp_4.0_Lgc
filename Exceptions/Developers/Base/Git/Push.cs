﻿namespace ERP.Exceptions.Developers.Base.Git;
/// <summary>
/// 未按时提交代码的异常统计
/// </summary>
public abstract class Push : Developer
{
    public Push(DateTime date,int count, Author author) : base($"push less than {count}", author)
    {
    }
}
