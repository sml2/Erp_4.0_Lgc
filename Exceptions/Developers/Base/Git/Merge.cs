﻿namespace ERP.Exceptions.Developers.Base.Git;

public abstract class Merge : Developer
{
    public Merge(string CommitID, string message, Author author) : base($"{message} in {CommitID}", author)
    {

    }
}
