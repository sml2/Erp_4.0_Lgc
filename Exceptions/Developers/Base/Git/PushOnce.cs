﻿namespace ERP.Exceptions.Developers.Base.Git;
/// <summary>
/// 未按时提交代码的异常统计(<1)
/// </summary>
public abstract class PushOnce : Push
{
    public PushOnce(DateTime date, Author author) : base(date,1, author)
    {
    }
}
