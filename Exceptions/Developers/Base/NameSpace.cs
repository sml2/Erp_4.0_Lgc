﻿using System;
namespace ERP.Exceptions.Developers.Base;

public abstract class NameSpace : Developer
{
    public NameSpace(string @old, string @new, Author author) : base($"[{@old}] move to [{@new}]", author)
    {
    }
}
