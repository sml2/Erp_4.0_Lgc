﻿namespace ERP.Exceptions.Developers.Base.Wainings
{
    /*
        严重性	代码	说明	项目	文件	行	禁止显示状态
        警告	CS8619	类型“List<string?>”的值中引用类型的为 Null 性与目标类型“List<string>”不匹配。	ERP	D:\ERP\Erp_4.0_Lgc\Services\Api\Logistics\Detail\K5.cs	155	活动
     */
    public abstract class CS8619 : Warning
    {
        public CS8619(Author author) : base("CS1998", author)
        {
        }
    }
}
