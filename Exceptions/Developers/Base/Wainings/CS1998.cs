﻿namespace ERP.Exceptions.Developers.Base.Wainings
{
    /*
        严重性	代码	说明	项目	文件	行	禁止显示状态
        警告	CS1998	此异步方法缺少 "await" 运算符，将以同步方式运行。请考虑使用 "await" 运算符等待非阻止的 API 调用，或者使用 "await Task.Run(...)" 在后台线程上执行占用大量 CPU 的工作。	ERP	D:\ERP\Erp_4.0_Lgc\Services\DB\Store\SyncMws.cs	16	活动
     */
    public abstract class CS1998 : Warning
    {
        public CS1998(Author author) : base("CS1998", author)
        {
        }
    }
}
