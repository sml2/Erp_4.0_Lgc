﻿namespace ERP.Exceptions.Developers.Base.Wainings
{
    /*
        严重性	代码	说明	项目	文件	行	禁止显示状态
        警告	CA2017	日志记录消息模板中所提供参数的数量与已命名占位符的数量不匹配	ERP	D:\ERP\Erp_4.0_Lgc\Services\Host\Invoker.cs	70	活动
    */
    public abstract class CA2017 : Warning
    {
        public CA2017(Author author) : base("CA2017", author)
        {
        }
    }
}
