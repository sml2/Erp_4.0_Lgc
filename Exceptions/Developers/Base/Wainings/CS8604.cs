﻿namespace ERP.Exceptions.Developers.Base.Wainings;

/*
    严重性	代码	说明	项目	文件	行	禁止显示状态
    警告	CS8604	“ulong Helpers.CreateHashCode(string str)”中的形参“str”可能传入 null 引用实参。	ERP	D:\ERP\Erp_4.0_Lgc\Services\DB\Store\CategoryNodeDefault.cs	173	活动
 */
public abstract class CS8604 : Warning
{
    public CS8604(Author author) : base("CS8604", author)
    {

    }
}
