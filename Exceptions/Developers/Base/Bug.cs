﻿using System;
namespace ERP.Exceptions.Developers.Base;

public abstract class Bug : Developer
{
    public Bug(string reason, Author author) : base(reason, author)
    {

    }
}
