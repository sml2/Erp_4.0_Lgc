﻿using ERP.Exceptions.Developers.Base;

namespace ERP.Exceptions.Developers.NameSpaces
{
    public class UserInfo : NameSpace
    {
        public UserInfo(string old, string @new, Author author) : base("UserInfo", "DB.Users", Author.SY)
        {
        }
    }
}
