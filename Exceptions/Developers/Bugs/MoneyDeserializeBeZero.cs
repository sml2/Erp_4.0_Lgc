﻿using ERP.Exceptions.Developers.Base;

namespace ERP.Exceptions.Developers.Bugs
{
    public class MoneyDeserializeBeZero : Bug
    {
        public MoneyDeserializeBeZero() : base("JSON反序列化为空", Author.LYQ)
        {
        }
    }
}
