using ERP.Storage.Services.Internal;

namespace ERP.Exceptions.SyncExportProduct
{
    public class NotFoundException : StorageDomainException
    {
        public NotFoundException(string message) : base(message)
        {
        }
    }
}
