﻿namespace ERP.Exceptions.Message;

public class MessageDomainException : Exception
{
    public MessageDomainException(string message) : base(message)
    {

    }
}