﻿using ERP.Models.Api.Logistics;

namespace ERP.Exceptions
{
    public class ParameterException : Exception
    {
        public  ParameterException(string message) : base(message)
        {
        }       
    }
}
