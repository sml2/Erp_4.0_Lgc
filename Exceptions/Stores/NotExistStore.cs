﻿namespace ERP.Exceptions.Stores
{
    public class NotExistStore : Exception
    {
        //$"The store[{ID}] doesn't exist"
        public NotExistStore(int ID) : base($"店铺[{ID}]不存在") { }
    }
    public class VerifyFail : Exception//此处要接口配合输出具体原因（超时，参数有误，授权地区不存在，网络异常...）
    {
        public VerifyFail(int ID) : base($"店铺[{ID}]验证失败") { }
    }
    public class VerifySaveFail : Exception
    {
        public VerifySaveFail(int ID) : base($"店铺[{ID}]保存失败") { }
    }

    public class NotExistStoreMarket : Exception
    {
        //$"The store[{ID}] doesn't exist"
        public NotExistStoreMarket(int ID) : base($"店铺[{ID}]下没有国家") { }
    }
}
