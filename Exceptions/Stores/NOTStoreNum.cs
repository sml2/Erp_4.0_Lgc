﻿namespace ERP.Exceptions.Stores;

public class NotStoreNum : Exception
{
    public NotStoreNum(int Num) : base($"可开的店铺数为{Num}"){}
}
