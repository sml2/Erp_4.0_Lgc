﻿namespace ERP.Exceptions
{
    public class OrderException : Exception
    {
        protected OrderException(string message) : base(message)
        {
        }
    }
}
