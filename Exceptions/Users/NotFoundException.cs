using ERP.Storage.Services.Internal;

namespace ERP.Exceptions.Users;

public class NotFoundException : StorageDomainException
{
    public NotFoundException(string message) : base(message)
    {
    }
}