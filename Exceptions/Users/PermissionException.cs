using ERP.Storage.Services.Internal;

namespace ERP.Exceptions.Users;

public class PermissionException : StorageDomainException
{
    public PermissionException(string message) : base(message)
    {
    }
}