using ERP.Storage.Services.Internal;

namespace ERP.Exceptions.Sketches
{
    public class NotFoundException : StorageDomainException
    {
        public NotFoundException(string message) : base(message)
        {
        }
    }
}
