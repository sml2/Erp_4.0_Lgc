using ERP.Storage.Services.Internal;

namespace ERP.Exceptions.Company
{
    public class NotFoundException : StorageDomainException
    {
        public NotFoundException(string message) : base(message)
        {
        }
    }
}
