using ERP.Storage.Services.Internal;

namespace ERP.Exceptions.Expands
{
    public class NotFoundException : StorageDomainException
    {
        public NotFoundException(string message) : base(message)
        {
        }
    }
}
