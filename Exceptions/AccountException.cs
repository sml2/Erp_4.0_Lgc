﻿namespace ERP.Exceptions
{
    public class AccountException : Exception
    {
        protected AccountException(string message) : base(message)
        {
        }
    }
}
