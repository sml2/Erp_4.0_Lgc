using ERP.Storage.Services.Internal;

namespace ERP.Exceptions.IllegalWords
{
    public class NotFoundException : StorageDomainException
    {
        public NotFoundException(string message) : base(message)
        {
        }
    }
}
