using ERP.Storage.Services.Internal;

namespace ERP.Exceptions.SyncExport
{
    public class NotFoundException : StorageDomainException
    {
        public NotFoundException(string message) : base(message)
        {
        }
    }
}
