using ERP.Storage.Services.Internal;

namespace ERP.Exceptions.DataTemplate
{
    public class NotFoundException : StorageDomainException
    {
        public NotFoundException(string message) : base(message)
        {
        }
    }
}
