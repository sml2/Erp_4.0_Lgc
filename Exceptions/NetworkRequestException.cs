﻿namespace ERP.Exceptions
{
    public class NetworkRequestException : Exception
    {
        public NetworkRequestException(string message) : base(message)
        {
        }
    }
}
