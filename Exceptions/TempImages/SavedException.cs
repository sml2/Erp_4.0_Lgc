﻿using ERP.Storage.Services.Internal;

namespace ERP.Exceptions.TempImages
{
    public class SavedException : StorageDomainException
    {
        public SavedException(string message) : base(message)
        {
        }
    }
}
