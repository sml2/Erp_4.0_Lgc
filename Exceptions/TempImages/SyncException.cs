﻿using ERP.Storage.Services.Internal;

namespace ERP.Exceptions.TempImages
{
    public class SyncException : StorageDomainException
    {
        public SyncException(string message) : base(message)
        {
        }
    }
}
