﻿using ERP.Storage.Services.Internal;

namespace ERP.Exceptions.TempImages
{
    public class NotFoundException : StorageDomainException
    {
        public NotFoundException(string message) : base(message)
        {
        }
    }
}
