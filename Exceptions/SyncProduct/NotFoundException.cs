using ERP.Storage.Services.Internal;

namespace ERP.Exceptions.SyncProduct
{
    public class NotFoundException : StorageDomainException
    {
        public NotFoundException(string message) : base(message)
        {
        }
    }
}
