﻿namespace ERP.Exceptions.Logistics
{
    public class OrderDataNotFound : NotFoundException
    {
        public OrderDataNotFound() : base("没有找到订单信息")
        {
        }
    }  
}
