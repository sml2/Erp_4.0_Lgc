﻿namespace ERP.Exceptions.Logistics
{   
    public class OrderAlreadyExists : OrderException
    {
        public OrderAlreadyExists() : base("接口中订单编号已经存在")
        {

        }
    }
}
