﻿namespace ERP.Exceptions.Logistics
{
    public class AccountTokenExpirationError : AccountException
    {
        public AccountTokenExpirationError(string message="物流Token验证异常") : base(message)
        {
        }
    }
}
