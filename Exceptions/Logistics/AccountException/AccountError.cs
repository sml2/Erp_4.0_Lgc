﻿namespace ERP.Exceptions.Logistics
{
    public class AccountError : AccountException
    {
        public AccountError(string message="物流账户验证失败") : base(message)
        {
        }
    }
}
