﻿using ERP.Enums;
using ERP.Enums.Logistics;

namespace ERP.Exceptions.Logistics
{
    public class ParameterError : ParameterException
    {
        public ParameterError(ParameterErrorType type) : base($"物流参数有错误")
        {

        }
        public ParameterError(ParameterErrorType type,string message) : base($"物流参数有错误:{message}")
        {

        }     

        public ParameterError(PrintSettingEnum printSettingEnum, string msg) : base($"物流参数有错误")
        {
        }
    } 
}
