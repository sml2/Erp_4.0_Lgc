﻿namespace ERP.Exceptions.Logistics
{
    public class UnKnownError : UnknownException
    {
        public UnKnownError(string message) : base($"{message}")
        {
        }
    }

}


