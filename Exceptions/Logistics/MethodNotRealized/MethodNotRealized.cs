﻿namespace ERP.Exceptions.Logistics
{    
    public class MethodNotRealized : MethodNotRealizedException
    {
        public MethodNotRealized(string method) : base($"{ method} 系统不支持该方法")
        {

        }
    }
}
