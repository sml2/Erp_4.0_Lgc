namespace ERP.Exceptions.Logistics
{
    public class NotGeneratedTrackError : ResultException
    {
        public NotGeneratedTrackError() : base($"跟踪信息尚未生成")
        {
        }      
    }

    public class NotGeneratedFeeError : ResultException
    {      
        public NotGeneratedFeeError() : base($"费用信息尚未生成")
        {
        }
    }

}
