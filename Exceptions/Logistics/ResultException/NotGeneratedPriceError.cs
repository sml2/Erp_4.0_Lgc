﻿namespace ERP.Exceptions.Logistics
{
    public class NotGeneratedPriceError : ResultException
    {
        public NotGeneratedPriceError() : base($"价格信息尚未生成")
        {
        }
    }
}
