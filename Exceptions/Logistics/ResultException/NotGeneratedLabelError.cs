﻿namespace ERP.Exceptions.Logistics
{
    public class NotGeneratedLabelError : ResultException
    {
        public NotGeneratedLabelError() : base("面单信息尚未生成")
        {
        }
    }

}
