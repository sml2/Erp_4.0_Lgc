﻿namespace ERP.Exceptions.Logistics
{
    public class ServiceResultError : ResultException
    {
        public ServiceResultError(string message="返回数据有误") : base(message)
        {
        }
    }

}
