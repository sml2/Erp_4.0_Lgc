﻿namespace ERP.Exceptions.Logistics
{   
    public class ParseResultError : ResultException
    {
        public  ParseResultError(string type) : base($"数据转换出错")
        {
        }
    }
}
