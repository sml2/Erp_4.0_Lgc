﻿namespace ERP.Exceptions.Logistics
{
    //Logistics information has not been generated

    public class NotGeneratedError : ResultException
    {
        public NotGeneratedError() : base($"data information has not been generated")
        {
        }
    }  
}
