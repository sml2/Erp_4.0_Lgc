﻿
namespace ERP.Exceptions.Logistics
{
    public class NetworkRequestError : NetworkRequestException
    {       
        public NetworkRequestError(string message="网络异常，请重试") : base(message)
        {

        }
    }

}
