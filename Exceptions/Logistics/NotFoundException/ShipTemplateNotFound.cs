﻿namespace ERP.Exceptions.Logistics
{
    public class ShipTemplateNotFound : NotFoundException
    {
        public ShipTemplateNotFound() : base($"没有找到该物流")
        {
        }
    }    
}
