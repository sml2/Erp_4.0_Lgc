namespace ERP.Exceptions.Export;

public class NotFoundException : ExportException
{
    public NotFoundException(string message) : base(message)
    {
    }
}