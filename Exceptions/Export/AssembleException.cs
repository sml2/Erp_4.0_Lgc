namespace ERP.Exceptions.Export;

/// <summary>
/// 导出构建产品异常
/// </summary>
public class AssembleException : ExportException
{
    public AssembleException(string message) : base($"{message}")
    {
    }
}