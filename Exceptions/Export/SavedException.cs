namespace ERP.Exceptions.Export;

public class SavedException : ExportException
{
    public SavedException(string message) : base(message)
    {
    }
}