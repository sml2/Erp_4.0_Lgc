using ERP.Storage.Services.Internal;

namespace ERP.Exceptions.Export;

public class TemplateNotFoundException : ExportException
{
    public TemplateNotFoundException(string message) : base(message)
    {
    }
}