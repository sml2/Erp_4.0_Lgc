﻿namespace ERP.Exceptions
{
    public class SettingException : Exception
    {
        protected SettingException(string message) : base(message)
        {
        }
    }
}
