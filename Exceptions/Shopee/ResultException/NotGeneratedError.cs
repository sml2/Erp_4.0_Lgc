﻿namespace ERP.Exceptions.Shopee
{
    public class NotGeneratedError : ResultException
    {
        public NotGeneratedError() : base("Label information has not been generated")
        {
        }
    }

}
