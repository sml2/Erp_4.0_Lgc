﻿namespace ERP.Exceptions.Shopee
{
    public class NetworkRequestError : NetworkRequestException
    {
        public NetworkRequestError(string code,string message) : base($"Network Request Exception:{code}-{message}")
        {

        }
        public NetworkRequestError(string message) : base($"Network Request Exception:{message}")
        {

        }
    }

}
