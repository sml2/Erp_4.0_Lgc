﻿namespace ERP.Exceptions.Shopee
{ 
    public class UnKnownError : UnknownException
    {
        public UnKnownError(string message) : base($"{message}")
        {
        }
    }

}
