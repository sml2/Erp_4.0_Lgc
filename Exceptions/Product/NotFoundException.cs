﻿using ERP.Storage.Services.Internal;

namespace ERP.Exceptions.Product
{
    public class NotFoundException : StorageDomainException
    {
        public NotFoundException(string message) : base(message)
        {
        }
    }
}
