﻿namespace ERP.Exceptions
{
    public class MethodNotRealizedException : Exception
    {
        protected MethodNotRealizedException(string message) : base(message)
        {
        }
    }
}
