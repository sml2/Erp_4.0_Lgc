﻿namespace ERP.Exceptions.Operation
{
    public class OperationInster : Exception
    {
        public OperationInster(string message) : base(message)
        {
        }
    }
}
