﻿namespace ERP.Exceptions.Amazon
{
    public class NetworkRequestError : NetworkRequestException
    {
        public NetworkRequestError(string code,string message) : base($"Network Request Exception:{code}-{message}")
        {

        }
        public NetworkRequestError(string message) : base($"Network Request Exception:{message}")
        {

        }
    }

}
