﻿namespace ERP.Exceptions.Amazon
{
    public class AmazonException : Exception
    {
        protected AmazonException(string message) : base(message)
        {

        }
    }


    /// <summary>
    /// 亚马逊配额超过
    /// </summary>
    public class AmazonQuotaExceededException : AmazonException
    {
        public AmazonQuotaExceededException(string msg) : base(msg)
        {

        }
    }


    /// <summary>
    /// 亚马逊未经授权
    /// </summary>
    public class AmazonUnauthorizedException : AmazonException
    {
        public AmazonUnauthorizedException(string msg) : base(msg)
        {

        }       
    }

    /// <summary>
    /// 亚马逊无效签名
    /// </summary>
    public class AmazonInvalidSignatureException : AmazonException
    {
        public AmazonInvalidSignatureException(string msg) : base(msg)
        {

        }
    }


    /// <summary>
    /// Amazon无效输入
    /// </summary>
    public class AmazonInvalidInputException : AmazonException
    {
        public AmazonInvalidInputException(string msg) : base(msg)
        {

        }
    }

    public class HttpRequestMessageNull : AmazonException
    {
        public HttpRequestMessageNull() : base("HttpRequestMessage is null")
        {
        }
    }

}
