﻿namespace ERP.Exceptions.Amazon
{
    public class NotGeneratedError : ResultException
    {
        public NotGeneratedError() : base("Label information has not been generated")
        {
        }
    }

}
