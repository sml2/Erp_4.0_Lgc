﻿namespace ERP.Exceptions.Amazon
{   
    public class ParseResultError : ResultException
    {
        public  ParseResultError(string type,string content) : base($"Failed to parse result:[{type}]-{content}")
        {
        }
    }
}
