﻿namespace ERP.Exceptions.Amazon
{
    public class UnKnownError : UnknownException
    {
        public UnKnownError(string message) : base($"{message}")
        {
        }
    }
}
