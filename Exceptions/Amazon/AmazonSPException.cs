﻿using ERP.Exceptions.Amazon;
using Org.BouncyCastle.Asn1.Cmp;
using System.Net;

namespace ERP.Exceptions.AmazonSP
{
    public class AmazonSPException : Exception
    {
        public ExceptionResponse? Response { get; set; }

        public AmazonSPException(string msg, HttpResponseMessage response) : base(msg)
        {
            if (response != null)
            {
                Response = new ExceptionResponse();
                Response.Content = System.Text.Encoding.UTF8.GetString(response.Content.ReadAsByteArrayAsync().Result);
                Response.ResponseCode = response.StatusCode;
            }
        }

    }

    public class AmazonNotFoundException : AmazonSPException
    {
        public AmazonNotFoundException(string msg, HttpResponseMessage response) : base(msg, response)
        {

        }
    }

    public class AmazonUnauthorizedException : AmazonSPException
    {
        public AmazonUnauthorizedException(string msg, HttpResponseMessage response) : base(msg, response)
        {

        }
    }

    public class AmazonInvalidInputException : AmazonSPException
    {
        public AmazonInvalidInputException(string msg, HttpResponseMessage response) : base(msg, response)
        {

        }
    }
    public class AmazonQuotaExceededException : AmazonSPException
    {
        public AmazonQuotaExceededException(string msg, HttpResponseMessage response) : base(msg, response)
        {

        }
    }

    public class AmazonInvalidSignatureException : AmazonSPException
    {
        public AmazonInvalidSignatureException(string msg, HttpResponseMessage response) : base(msg, response)
        {

        }
    }

    public class AmazonProcessingReportDeserializeException : AmazonSPException
    {
        public string ReportContent { get; set; }
        public AmazonProcessingReportDeserializeException(string msg, string reportContent, HttpResponseMessage? response=null) : base(msg, response)
        {
            ReportContent = reportContent;
        }
    }

    public class ExceptionResponse
    {
        public string? Content { get; set; }
        public HttpStatusCode? ResponseCode { get; set; }
    }
}

