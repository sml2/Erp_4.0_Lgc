﻿namespace ERP.Exceptions
{
    public class UnknownException : Exception
    {
        protected UnknownException(string message) : base(message)
        {
        }
    }
}
