namespace ERP.Exceptions;

public class FianceException : Exception
{
    private new string Message;

    protected FianceException(string message) : base(message)
    {
        Message = message;
    }

    public override string ToString()
    {
        return Message;
    }
}