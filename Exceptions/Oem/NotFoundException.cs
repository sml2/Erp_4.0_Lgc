namespace ERP.Exceptions.Oem;

public class NotFoundException : SettingException
{
    public NotFoundException(string message) : base(message)
    {
    }
}