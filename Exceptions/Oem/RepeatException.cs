namespace ERP.Exceptions.Oem;

public class RepeatException : SettingException
{
    public RepeatException(string message) : base(message)
    {
    }
}