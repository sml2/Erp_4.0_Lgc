using ERP.Storage.Services.Internal;

namespace ERP.Exceptions.Statistic;

public class RepairException : StorageDomainException
{
    public RepairException(string message) : base(message)
    {
    }
}