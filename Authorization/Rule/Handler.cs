﻿using Microsoft.AspNetCore.Authorization;

namespace ERP.Authorization.Rule;
using Extensions;
public class Handler : HandlerAbs<Requirement, PermissionAttribute>
{
    protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,
        Requirement requirement,
        IEnumerable<PermissionAttribute> attributes)
    {
        foreach (var item in attributes)
        {
            if (!context.User.HasEditable(item.Type, item.Enum))
            {
                //context.FailureReasons = 
                context.Fail();
                return Task.CompletedTask;
            }
        }

        context.Succeed(requirement);
        return Task.CompletedTask;
    }
}