﻿using Microsoft.AspNetCore.Authorization;

namespace ERP.Authorization.Role;
public class Requirement : IAuthorizationRequirement
{

    public IEnumerable<string> Roles { get; }
    public Requirement(params string[] roles)
    {
        Roles = roles ?? throw new ArgumentNullException(nameof(roles));
    }
}
