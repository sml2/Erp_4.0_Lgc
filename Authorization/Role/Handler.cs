﻿using Microsoft.AspNetCore.Authorization;
namespace ERP.Authorization.Role;
public class Handler : AuthorizationHandler<Requirement>
{
    protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, Requirement requirement)
    {

        foreach (var item in requirement.Roles)
        {
            if (context.User.IsInRole(item))
            {
                context.Succeed(requirement);
                return Task.CompletedTask;
            }
        }
        context.Fail();
        return Task.CompletedTask;
    }
}
