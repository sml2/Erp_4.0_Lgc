﻿using ERP.Controllers;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Text.Encodings.Web;
namespace ERP.Authorization;
public class CookieAuthenticationHandler : Microsoft.AspNetCore.Authentication.Cookies.CookieAuthenticationHandler
{
    public CookieAuthenticationHandler(IOptionsMonitor<CookieAuthenticationOptions> options,
                                       ILoggerFactory logger,
                                       UrlEncoder encoder,
                                       ISystemClock clock) : base(options, logger, encoder, clock) { }
    protected override async Task HandleChallengeAsync(AuthenticationProperties properties)
    {
        //string value = properties.RedirectUri;
        //if (string.IsNullOrEmpty(value))
        //{
        //    value = OriginalPathBase + OriginalPath + Request.QueryString;
        //}
        //string targetPath = Options.LoginPath + QueryString.Create(Options.ReturnUrlParameter, value);
        //RedirectContext<CookieAuthenticationOptions> context = new(Context, Scheme, Options, properties, BuildRedirectUri(targetPath));
        //await Events.RedirectToLogin(context);
        Response.StatusCode = 200;
        var res = JsonConvert.SerializeObject(new ResultStruct()
        {
            Code = Codes.Login6,
            Message = "未授权"
        });
        Response.StatusCode = 200;
        Response.ContentType = "application/json;charset=UTF-8";
        await Response.WriteAsync(res);
    }
}
