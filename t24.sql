﻿START TRANSACTION;

DELETE FROM `PlatformData`
WHERE `ID` = 77;
SELECT ROW_COUNT();


ALTER TABLE `NodeCategory` MODIFY COLUMN `CategoryId` bigint NOT NULL DEFAULT 0 COMMENT '分类目录ID';

ALTER TABLE `NodeCategory` ADD `Flag` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '与productType的对应状态';

UPDATE `PlatformData` SET `Data` = '{"Name":"比利时","AreaName":"Belgium","Short":"BE","Unit":"BEF","Marketplace":"AMEN7PMS3EDWL","State":1,"MWSCanUnion":true,"Continent":1,"Host":"sellingpartnerapi-eu.amazon.com","Org":"https://www.amazon.be","KeyId":1,"MwsSecurity":true,"SellerCentralURL":"https://sellercentral.amazon.com.be","RegionName":"eu-west-1"}'
WHERE `ID` = 78;
SELECT ROW_COUNT();


INSERT INTO `PlatformData` (`ID`, `CreatedAt`, `Data`, `NationId`, `Platform`, `UpdatedAt`)
VALUES (79, TIMESTAMP '2021-11-19 13:38:00', '{"Sort":0,"Platform":1048,"Name":"华磊","Width":500,"Height":200,"Opened":true,"TEL":null,"DocURL":null,"TypeId":"ERP.Models.DB.PlatformData+LogisticsData, ERP, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null"}', NULL, 1048, TIMESTAMP '2021-11-19 13:38:00');

UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-02-09 18:45:56', `PasswordHash` = 'AQAAAAEAACcQAAAAEDue6pLuAYPqje8Lh5e/vQsuC7k58rjHMWtPWIsyius2WnqtdBrhe/xSaqlECSeBXw==', `UpdatedAt` = TIMESTAMP '2023-02-09 18:45:56'
WHERE `Id` = 444445;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-02-09 18:45:56', `PasswordHash` = 'AQAAAAEAACcQAAAAEDttLCnbpaFluwyh3NUpMaubz0lH+gNUFAvDAEnVWhs2zKIatQIB4rFPKNeJ7jEuow==', `UpdatedAt` = TIMESTAMP '2023-02-09 18:45:56'
WHERE `Id` = 444446;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-02-09 18:45:56', `PasswordHash` = 'AQAAAAEAACcQAAAAEFbv9ssyzvzm+FptEprh7UsCXnBSJOfVVu5cxn9T0ByF9/D34a4vyz5n5vC0Omd6fg==', `UpdatedAt` = TIMESTAMP '2023-02-09 18:45:56'
WHERE `Id` = 444447;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-02-09 18:45:56', `PasswordHash` = 'AQAAAAEAACcQAAAAEK0rRCLzrD5pgxI4qf9jRlpUMlmECndoEQFdW7pG14K0vqf2/z90QL0iadZe+hLaMg==', `UpdatedAt` = TIMESTAMP '2023-02-09 18:45:56'
WHERE `Id` = 444448;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-02-09 18:45:56', `PasswordHash` = 'AQAAAAEAACcQAAAAEES+OU8nABcdeEJWlZcNPG8duMkr4T2Cj8+bCd8wc8QQadYtPfheMICHi3s6/bCOGQ==', `UpdatedAt` = TIMESTAMP '2023-02-09 18:45:56'
WHERE `Id` = 444449;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-02-09 18:45:56', `PasswordHash` = 'AQAAAAEAACcQAAAAEPfmncT2TmXjz2alJBrVTkNrIIWchmD27SDIit23DO7OPNPk43xSXrfExNGr/RXABA==', `UpdatedAt` = TIMESTAMP '2023-02-09 18:45:56'
WHERE `Id` = 444450;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-02-09 18:45:56', `PasswordHash` = 'AQAAAAEAACcQAAAAENo6OJenYot5JoIm2xkOI/YxrEZSv6uFlx9KYep8grcuEpfDoGPhi66YucKXLHdP0A==', `UpdatedAt` = TIMESTAMP '2023-02-09 18:45:56'
WHERE `Id` = 444451;
SELECT ROW_COUNT();


UPDATE `User` SET `CreatedAt` = TIMESTAMP '2023-02-09 18:45:56', `PasswordHash` = 'AQAAAAEAACcQAAAAEMEAvqaS+Vo5850319TaAuBI/A7PH07W1CX/YR2iM99eDs7d5TnH9v/pIxfEMQ868w==', `UpdatedAt` = TIMESTAMP '2023-02-09 18:45:56'
WHERE `Id` = 444452;
SELECT ROW_COUNT();


UPDATE `UserGroup` SET `Sort` = -1675939556
WHERE `ID` = 1;
SELECT ROW_COUNT();


UPDATE `UserValidities` SET `CreatedAt` = TIMESTAMP '2023-02-09 18:45:56', `UpdatedAt` = TIMESTAMP '2023-02-09 18:45:56'
WHERE `ID` = 1;
SELECT ROW_COUNT();


UPDATE `UserValidities` SET `CreatedAt` = TIMESTAMP '2023-02-09 18:45:56', `UpdatedAt` = TIMESTAMP '2023-02-09 18:45:56'
WHERE `ID` = 2;
SELECT ROW_COUNT();


UPDATE `UserValidities` SET `CreatedAt` = TIMESTAMP '2023-02-09 18:45:56', `UpdatedAt` = TIMESTAMP '2023-02-09 18:45:56'
WHERE `ID` = 3;
SELECT ROW_COUNT();


INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20230209104601_t31', '6.0.5');

COMMIT;

